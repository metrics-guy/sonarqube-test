#!/bin/bash
readarray -t decompilers_array < decompilers.txt
readarray -t binaries_array < binaries.txt
readarray -t archs_array < archs.txt

echo "REMOVE PREVIOUS BUILD"
rm -rf build

echo "BUILDING"
mkdir build

for arch in ${archs_array[@]}
do

  for decompiler in ${decompilers_array[@]}
  do
    mkdir build/$decompiler

    for binary in ${binaries_array[@]}
    do

      # Create build dir.
      mkdir -p build/$arch/$decompiler/$binary

      # Load from the index file the functions to analyze.
      index_file=index/$arch/$binary.csv
      IFS=','
      [ ! -f $index_file ] && { echo "$index_file file not found"; exit 1; }
      while read function_name address_name
      do

        echo "COMPILING $arch $decompiler $binary $function_name"
        gcc -w -c -o build/$arch/$decompiler/$binary/$function_name src/$arch/$decompiler/$binary/$function_name.c
        RC=$?

        if [ $RC -ne 0 ]; then
          echo "BUILD FAILURE"
        else
          echo "BUILD SUCCESS"
        fi

      done < $index_file

    done

  done

done
