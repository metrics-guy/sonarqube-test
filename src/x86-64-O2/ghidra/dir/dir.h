typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined5;
typedef unsigned long    undefined6;
typedef unsigned long    undefined7;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00403510(void);
void FUN_004035c0(void);
void FUN_004037c0(void);
void entry(void);
void FUN_00403e70(void);
void FUN_00403ee0(void);
void FUN_00403f70(void);
ulong FUN_00403fb0(uint uParm1);
undefined8 FUN_00403ff0(undefined8 uParm1);
undefined FUN_00404000(int iParm1);;
undefined8 FUN_00404010(void);
char * FUN_00404020(char *pcParm1);
void FUN_00404070(void);
ulong FUN_00404140(byte **ppbParm1,byte **ppbParm2,uint uParm3,long *plParm4);
void FUN_00404360(char cParm1);
undefined8 FUN_00404390(undefined8 uParm1);
ulong FUN_004043a0(int iParm1);
void FUN_004043c0(void);
ulong FUN_00404400(char cParm1,ulong uParm2,int iParm3);
void FUN_004044f0(char *pcParm1,char *pcParm2,char *pcParm3);
void FUN_004046c0(undefined8 *puParm1);
void FUN_00404710(void);
void thunk_FUN_004037c0(void);
void FUN_004047e0(undefined8 *puParm1);
uint FUN_00404800(byte bParm1);
void FUN_004048d0(void);
void FUN_00404930(undefined8 uParm1);
ulong FUN_00404960(uint uParm1);
void FUN_004049d0(undefined auParm1 [16],undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8);
void FUN_00404a30(void);
void FUN_00404a50(void);
undefined8 FUN_00404a70(undefined8 uParm1);
undefined8 FUN_00404ae0(long lParm1);
void FUN_00404b90(void);
void FUN_00404cc0(long lParm1,long lParm2,undefined uParm3);
void FUN_00404d30(undefined8 uParm1);
undefined8 FUN_00404ed0(undefined8 *puParm1,undefined8 uParm2);
undefined8 FUN_00404f30(char *pcParm1);
undefined8 FUN_00404f90(void);
void FUN_00405020(void);
ulong * FUN_00405270(long *plParm1,char cParm2);
void FUN_004055d0(char cParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00405610(undefined8 uParm1,long lParm2);
void FUN_004056a0(undefined8 uParm1,long lParm2,char cParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
undefined8 FUN_004056e0(long lParm1,char *pcParm2);
ulong FUN_00405780(char *pcParm1);
ulong FUN_00405860(ulong uParm1);
undefined * FUN_004058a0(char *pcParm1,char cParm2);
ulong FUN_00405950(ulong uParm1);
void FUN_00405990(void);
ulong FUN_00405b10(void);
void FUN_00405b30(undefined8 uParm1,undefined8 uParm2);
void FUN_00405d10(void);
void FUN_00405d20(void);
void FUN_00405d30(long lParm1,undefined8 uParm2,ulong uParm3);
void FUN_00405dc0(ulong uParm1,uint uParm2,char cParm3);
void FUN_00405e10(ulong uParm1,uint uParm2,char cParm3);
undefined8 FUN_00405e60(undefined8 uParm1,long lParm2);
char * thunk_FUN_0040dc10(ulong uParm1,long lParm2);
ulong FUN_00405f10(ulong uParm1,ulong uParm2);
void FUN_00405f60(undefined8 uParm1,undefined8 uParm2);
ulong FUN_004061c0(int iParm1,undefined8 uParm2,uint uParm3);
void FUN_004066b0(long lParm1,byte bParm2);
undefined8 FUN_00406c80(void);
ulong FUN_00406ca0(long *plParm1);
undefined8 FUN_00406d20(void);
char * FUN_00406d40(long lParm1,undefined8 uParm2,ulong *puParm3,char *pcParm4);
void FUN_00407100(undefined8 *puParm1,undefined8 *puParm2);
void FUN_00407170(void);
void FUN_004071d0(void);
void FUN_00407240(void);
void FUN_00407270(void);
undefined FUN_00407320(long lParm1);;
ulong FUN_00407380(long lParm1,char cParm2);
undefined8 FUN_00407400(undefined8 **ppuParm1,int iParm2,byte bParm3,undefined8 *puParm4);
void FUN_00407f90(undefined8 uParm1,ulong uParm2,long lParm3,undefined8 uParm4,uint uParm5);
ulong FUN_00407ff0(void);
char * FUN_00408090(char **ppcParm1,char *pcParm2,undefined8 uParm3,int iParm4,char **ppcParm5,byte *pbParm6);
long FUN_004084a0(char *param_1,undefined8 param_2,ulong param_3,long param_4,char param_5,long param_6,long param_7);
long FUN_00408790(undefined8 *puParm1,byte bParm2,undefined8 uParm3,ulong uParm4);
long FUN_004088a0(long lParm1,undefined8 uParm2);
undefined * FUN_00408a00(ulong uParm1);
long FUN_004091c0(undefined *puParm1,undefined8 uParm2,ulong uParm3);
long FUN_00409220(undefined8 *puParm1);
void FUN_00409800(void);
void FUN_00409900(long lParm1,long lParm2,ulong uParm3);
void FUN_00409d90(void);
void FUN_00409da0(uint uParm1);
undefined * FUN_0040a0a0(ulong uParm1,undefined8 *puParm2);
long FUN_0040ae20(undefined8 uParm1,ulong uParm2);
long FUN_0040af30(undefined8 uParm1,long *plParm2,long lParm3,long lParm4);
void FUN_0040b040(undefined auParm1 [16],undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined auParm8 [16],undefined8 uParm9,undefined8 uParm10,long lParm11,undefined8 uParm12,undefined8 uParm13,undefined8 uParm14);
void FUN_0040b0a0(long *plParm1,long lParm2,long lParm3);
long FUN_0040b180(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
ulong FUN_0040b1f0(int iParm1);
undefined FUN_0040b220(int iParm1);;
undefined FUN_0040b250(int iParm1);;
ulong FUN_0040b260(uint uParm1);
char * FUN_0040b270(char **ppcParm1,undefined8 uParm2,undefined8 uParm3);
char * FUN_0040b2f0(char *pcParm1,ulong uParm2);
void FUN_0040b920(char *pcParm1);
char * FUN_0040b960(char *pcParm1);
void FUN_0040b9c0(long lParm1);
undefined8 FUN_0040b9f0(void);
undefined8 * FUN_0040ba00(long lParm1,undefined8 uParm2,undefined8 *puParm3);
ulong FUN_0040ba90(long lParm1,undefined8 uParm2,undefined8 *puParm3);
undefined8 FUN_0040bad0(undefined8 uParm1);
void FUN_0040bc50(long lParm1,undefined *puParm2);
void FUN_0040bc60(char *pcParm1);
void FUN_0040bc80(char *pcParm1);
long FUN_0040bca0(long lParm1,char *pcParm2,undefined8 *puParm3);
char * FUN_0040bd70(char **ppcParm1);
void FUN_0040c220(undefined8 *puParm1);
ulong FUN_0040c260(ulong uParm1);
ulong FUN_0040c300(ulong uParm1);
ulong FUN_0040c370(ulong uParm1);
undefined * FUN_0040c3e0(long *plParm1,ulong uParm2);
undefined8 FUN_0040c410(float **ppfParm1);
ulong FUN_0040c490(float fParm1,ulong uParm2,char cParm3);
void FUN_0040c520(undefined8 *puParm1,undefined8 *puParm2);
long FUN_0040c540(long lParm1,long lParm2,long **pplParm3,char cParm4);
void FUN_0040c640(long *plParm1);
undefined8 FUN_0040c660(long lParm1,long **pplParm2,char cParm3);
undefined8 FUN_0040c7a0(long lParm1);
long FUN_0040c7b0(long lParm1,long lParm2);
long * FUN_0040c810(undefined8 uParm1,undefined1 *puParm2,undefined *puParm3,undefined *puParm4,long lParm5);
void FUN_0040c910(long **pplParm1);
undefined * FUN_0040c9f0(long *plParm1,undefined8 uParm2);
undefined * FUN_0040cb70(ulong uParm1,undefined8 *puParm2,long *plParm3);
undefined8 FUN_0040cdc0(undefined8 uParm1,undefined8 uParm2);
long FUN_0040ce00(long lParm1,undefined8 uParm2);
ulong FUN_0040cfe0(undefined8 *puParm1,ulong uParm2);
void FUN_0040d050(undefined8 *puParm1);
void FUN_0040d070(void);
void FUN_0040d140(long lParm1,ulong uParm2,byte *pbParm3,undefined8 uParm4);
long FUN_0040d200(void);
undefined8 FUN_0040d230(char *pcParm1,undefined8 *puParm2,uint *puParm3);
char * FUN_0040d360(ulong uParm1,long lParm2,ulong uParm3,ulong uParm4,ulong uParm5);
undefined8 FUN_0040daa0(undefined8 uParm1,undefined8 uParm2,long *plParm3);
uint * FUN_0040dad0(uint uParm1);
uint * FUN_0040db70(uint uParm1);
char * FUN_0040dc10(ulong uParm1,long lParm2);
char * FUN_0040dcb0(ulong uParm1,long lParm2);
void FUN_0040dd00(undefined *puParm1,undefined *puParm2,long lParm3);
undefined8 FUN_0040dd30(int *piParm1);
ulong FUN_0040dd80(int *piParm1,ulong uParm2);
long FUN_0040dde0(long lParm1,long lParm2,long lParm3,ulong *puParm4,int iParm5,ulong uParm6);
ulong FUN_0040e130(byte *pbParm1,long lParm2,ulong uParm3);
ulong FUN_0040e310(byte *pbParm1,ulong uParm2);
void FUN_0040e340(undefined8 *puParm1,ulong uParm2,undefined8 *puParm3,code *pcParm4);
void FUN_0040e420(undefined8 *puParm1,ulong uParm2,undefined8 *puParm3,code *pcParm4);
void FUN_0040e580(undefined8 *puParm1,ulong uParm2,code *pcParm3);
ulong FUN_0040e590(int iParm1,int iParm2);
long FUN_0040e5d0(long lParm1,long lParm2,long lParm3);
long FUN_0040e610(long lParm1,long lParm2,long lParm3);
char * FUN_0040e650(char *pcParm1,long lParm2,char *pcParm3,uint *puParm4,char cParm5,undefined8 uParm6,undefined8 uParm7,uint uParm8);
int * FUN_0040fe80(long lParm1);
int * FUN_0040ff30(int *piParm1,undefined8 *puParm2);
ulong FUN_00410280(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8);
ulong FUN_00410340(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9);
ulong FUN_00410410(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9,byte param_10);
undefined8 FUN_00410500(byte *param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined * FUN_004105b0(char *pcParm1,int iParm2);
undefined8 *FUN_00410680(undefined *puParm1,undefined8 *puParm2,long lParm3,undefined *puParm4,ulong uParm5,uint uParm6,long lParm7,char *pcParm8,char *pcParm9);
undefined1 * FUN_00411770(undefined1 *puParm1,undefined8 *puParm2,undefined8 uParm3,uint *puParm4);
undefined8 FUN_00411910(undefined1 *puParm1);
ulong FUN_00411950(undefined1 *puParm1);
void FUN_00411960(undefined1 *puParm1,undefined4 uParm2);
ulong FUN_00411970(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
undefined8 FUN_004119b0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined1 *puParm5);
void FUN_00411a30(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00411a60(ulong uParm1,undefined8 uParm2);
void FUN_00411b00(uint uParm1,undefined8 uParm2,undefined8 uParm3);
undefined1 * FUN_00411b90(undefined1 *puParm1,undefined8 *puParm2);
undefined1 * FUN_00411ba0(undefined8 *puParm1);
ulong FUN_00411bb0(long lParm1,int iParm2,long lParm3,int iParm4);
void FUN_00411bd0(undefined8 uParm1,long lParm2,long lParm3,long lParm4,long lParm5);
void FUN_00411e20(void);
void FUN_00411e80(void);
void FUN_00411f10(ulong uParm1,ulong uParm2);
void FUN_00411f30(ulong uParm1,ulong uParm2);
void FUN_00411f60(ulong uParm1,ulong uParm2);
long FUN_00411f70(long lParm1,ulong *puParm2);
long FUN_00411fb0(long lParm1,ulong *puParm2,ulong uParm3);
long FUN_00412070(long lParm1,ulong *puParm2);
void FUN_00412080(undefined8 uParm1,undefined8 uParm2);
void FUN_004120b0(undefined8 uParm1);
undefined * FUN_004120d0(void);
ulong FUN_00412100(undefined8 param_1,ulong param_2,ulong param_3,ulong param_4,undefined8 param_5,undefined8 param_6,uint param_7);
long FUN_004121d0(void);
long FUN_00412200(void);
ulong FUN_004122b0(int iParm1);
ulong FUN_004122d0(ulong *puParm1,int iParm2);
ulong FUN_00412300(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_00412330(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined *FUN_00412670(int iParm1,undefined8 *puParm2,undefined uParm3,long lParm4,undefined8 uParm5,ulong uParm6);
ulong FUN_004126e0(int iParm1,undefined8 uParm2,char cParm3,undefined8 uParm4,undefined8 uParm5);
ulong FUN_00412700(int iParm1);
ulong FUN_00412720(ulong *puParm1,int iParm2);
ulong FUN_00412750(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_00412780(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_00412ac0(ulong uParm1,ulong uParm2);
void FUN_00412b10(undefined8 uParm1);
ulong FUN_00412b50(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00412bc0(void);
void FUN_00412be0(void);
void FUN_00412c10(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
int * FUN_00412cf0(int *piParm1);
char * FUN_00412df0(char *pcParm1);
undefined8 FUN_00412f00(int *piParm1);
undefined8 FUN_00412f70(int iParm1,long lParm2,ulong uParm3,ulong uParm4,ulong uParm5,uint uParm6);
ulong FUN_00413560(uint *puParm1,uint *puParm2,uint *puParm3,bool bParm4,uint uParm5);
undefined8 FUN_00413f10(uint uParm1,long lParm2,ulong uParm3,ulong uParm4,ulong uParm5,uint uParm6);
ulong FUN_00414470(byte *pbParm1,byte *pbParm2,byte *pbParm3,byte bParm4,uint uParm5);
ulong FUN_00414e50(undefined8 uParm1,long lParm2,ulong uParm3);
long FUN_004150f0(long lParm1,ulong uParm2);
void FUN_00415570(long lParm1,int *piParm2);
ulong FUN_00415650(int param_1,undefined8 *param_2,char *param_3,char **param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_00415be0(char *pcParm1,int *piParm2,int iParm3);
ulong FUN_00415ca0(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,uint *param_7,uint param_8);
void FUN_00416160(void);
void FUN_004161c0(void);
void FUN_004161e0(void);
void FUN_00416250(void);
undefined8 FUN_00416270(long lParm1,long lParm2);
void FUN_004162e0(long lParm1);
ulong FUN_00416300(uint *puParm1,byte *pbParm2,long lParm3);
ulong * FUN_00416370(ulong *puParm1,undefined8 uParm2,ulong uParm3);
void FUN_00416450(long lParm1,undefined8 uParm2);
void FUN_00416470(long lParm1,undefined8 uParm2);
undefined8 FUN_00416550(long *plParm1,undefined *puParm2,long lParm3,long lParm4,long lParm5);
void FUN_00416570(ulong *puParm1,ulong uParm2);
void FUN_00416660(void);
ulong FUN_00416670(uint param_1,undefined8 param_2,uint param_3,ulong param_4);
undefined8 FUN_00416770(long lParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004167e0(long lParm1,long lParm2);
ulong FUN_00416820(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,long param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00416930(void);
undefined8 FUN_00416950(long lParm1,long lParm2);
char * FUN_004169b0(char *pcParm1,char *pcParm2,ulong uParm3);
ulong FUN_00416ac0(uint uParm1,uint uParm2);
ulong FUN_00416ae0(uint *puParm1,uint *puParm2);
void FUN_00416b30(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_00416b40(void);
ulong FUN_00416b50(char *pcParm1);
ulong FUN_00416b80(long lParm1);
undefined8 * FUN_00416bc0(long lParm1);
undefined8 FUN_00416c60(long *plParm1,char *pcParm2);
void FUN_00416da0(long *plParm1);
long FUN_00416dd0(long lParm1);
ulong FUN_00416e80(long lParm1);
undefined8 FUN_00416ed0(long lParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_00416f70(long lParm1,undefined8 uParm2);
long FUN_00417050(uint *puParm1);
void FUN_00417070(void);
ulong FUN_00417150(char *pcParm1,long lParm2);
char * FUN_00417190(ulong uParm1,long lParm2,long lParm3);
ulong FUN_00417340(void);
undefined * FUN_00417380(undefined8 param_1,undefined8 *param_2);
int * FUN_004175f0(int *param_1,ulong *param_2);
undefined * FUN_00417780(undefined8 uParm1,undefined8 *puParm2);
uint * FUN_00417990(undefined8 uParm1,int *piParm2,ulong *puParm3);
void FUN_00417b00(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5,undefined8 *puParm6);
int * FUN_00417c40(undefined8 *puParm1,long lParm2,undefined8 *puParm3,undefined8 *puParm4,uint **ppuParm5,uint **ppuParm6);
undefined8 FUN_004181f0(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_00418640(uint param_1);
void FUN_00418680(uint uParm1);
ulong FUN_0041d610(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0041d6d0(int *piParm1,long lParm2);
ulong FUN_0041d930(long param_1,char param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined8 FUN_0041d990(char *pcParm1);
ulong FUN_0041d9d0(ulong uParm1);
ulong FUN_0041da30(byte *pbParm1,byte *pbParm2);
ulong FUN_0041da80(ulong uParm1,char cParm2);
undefined8 FUN_0041db60(void);
ulong FUN_0041db70(char *pcParm1,ulong uParm2);
undefined * FUN_0041dbb0(void);
char * FUN_0041df10(void);
double FUN_0041dfd0(int *piParm1);
void FUN_0041e020(int *param_1);
ulong FUN_0041e250(long param_1,long param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10);
undefined8 FUN_0041e2c0(char *param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8);
ulong FUN_0041e300(undefined8 uParm1);
long FUN_0041e650(ulong uParm1,ulong uParm2);
long FUN_0041e670(void);
void FUN_0041e6a0(undefined8 uParm1);
ulong FUN_0041e730(ulong param_1,undefined8 param_2,ulong param_3);
undefined8 FUN_0041e880(ulong uParm1);
void FUN_0041e8f0(undefined8 uParm1);
ulong FUN_0041e900(long lParm1);
undefined8 FUN_0041e990(void);
void FUN_0041e9b0(void);
ulong FUN_0041e9d0(void);
ulong FUN_0041e9f0(ulong uParm1,long lParm2,long lParm3,ulong uParm4);
long FUN_0041eab0(long lParm1,undefined4 uParm2);
ulong FUN_0041eac0(ulong uParm1);
ulong FUN_0041eb40(uint uParm1,uint uParm2);
long FUN_0041eb60(ulong param_1,long param_2,undefined8 param_3,int param_4,int param_5,uint param_6,int param_7,int param_8,int param_9,int param_10);
long FUN_0041ecb0(undefined8 uParm1,undefined8 uParm2);
long FUN_0041ece0(void);
void FUN_0041ed90(code *pcParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
long FUN_0041edb0(code *pcParm1,long *plParm2,undefined8 uParm3,undefined8 uParm4);
long FUN_0041f210(uint *puParm1);
undefined8 FUN_0041f230(uint *puParm1,ulong *puParm2);
undefined8 FUN_0041f450(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_00420120(char *pcParm1,char *pcParm2,ulong uParm3);
ulong FUN_00420250(undefined auParm1 [16]);
ulong FUN_00420270(void);
undefined8 * FUN_004202a0(ulong uParm1);
void FUN_00420320(ulong uParm1);
void FUN_004203b0(void);
undefined8 _DT_FINI(void);

