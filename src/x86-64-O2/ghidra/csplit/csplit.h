typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void entry(void);
void FUN_00402a00(void);
void FUN_00402a70(void);
void FUN_00402b00(void);
void FUN_00402b40(long lParm1);
void FUN_00402b80(void);
long FUN_00402c00(char *pcParm1,uint *puParm2);
long FUN_00402c50(uint uParm1);
void FUN_00402d00(char cParm1);
void FUN_00402dc0(void);
long * FUN_00402e00(void);
long * FUN_00402e20(void);
long * FUN_00402fb0(long lParm1);
void FUN_00403000(ulong uParm1);
void FUN_00403080(byte *pbParm1,ulong uParm2);
ulong FUN_00403130(char *pcParm1);
ulong FUN_00403190(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_004031d0(undefined4 uParm1,undefined uParm2,char *pcParm3);
void FUN_004032e0(undefined8 uParm1,undefined8 uParm2);
long FUN_00403310(undefined8 uParm1,long lParm2);
void FUN_00403370(void);
void FUN_00403450(void);
void FUN_00403470(int iParm1,undefined8 *puParm2,undefined *puParm3,long lParm4);
void FUN_00403540(int iParm1,uint uParm2,long lParm3);
void FUN_004036e0(undefined8 *puParm1,long *plParm2);
void FUN_00403730(long *plParm1,undefined8 uParm2);
void FUN_004037b0(undefined8 uParm1,long lParm2);
void FUN_00403820(undefined8 *puParm1);
void FUN_00403840(void);
void FUN_00403870(long **pplParm1,long **pplParm2,long lParm3,long lParm4);
long FUN_00403910(long lParm1);
void FUN_00403a10(void);
long * FUN_00403bb0(void);
long * FUN_00403bd0(char *pcParm1);
long * FUN_00403ce0(void);
long * FUN_00403e40(void);
void FUN_00403f20(void);
void FUN_00403f50(long lParm1,long lParm2,char cParm3);
long FUN_00403fe0(ulong uParm1);
ulong FUN_004040c0(void);
long * FUN_004040e0(void);
long * FUN_00404130(ulong uParm1,ulong uParm2,int iParm3);
long * FUN_004041c0(long *plParm1,long lParm2);
long * FUN_004043b0(long lParm1,long lParm2);
long * FUN_00404460(void);
void FUN_00404510(uint uParm1);
ulong FUN_004046c0(uint uParm1,undefined8 uParm2,uint uParm3,uint uParm4);
long FUN_00404720(undefined8 uParm1,undefined8 uParm2);
char * FUN_004047c0(ulong uParm1,long lParm2);
int * FUN_00404810(long lParm1);
int * FUN_004048c0(int *piParm1,undefined8 *puParm2);
ulong FUN_00404c10(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8);
ulong FUN_00404cd0(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9);
ulong FUN_00404da0(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9,byte param_10);
undefined8 FUN_00404e90(byte *param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined * FUN_00404f40(char *pcParm1,int iParm2);
undefined8 *FUN_00405010(undefined *puParm1,undefined8 *puParm2,long lParm3,undefined *puParm4,ulong uParm5,uint uParm6,long lParm7,char *pcParm8,char *pcParm9);
undefined1 * FUN_00406100(undefined1 *puParm1,undefined8 *puParm2,undefined8 uParm3,uint *puParm4);
ulong FUN_004062a0(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
void FUN_00406310(ulong uParm1,undefined8 uParm2);
void FUN_004063b0(uint uParm1,undefined8 uParm2,undefined8 uParm3);
undefined1 * FUN_00406450(undefined8 *puParm1);
long FUN_00406460(uint uParm1,undefined8 uParm2,ulong uParm3);
ulong FUN_004064d0(ulong uParm1);
long FUN_004064e0(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long *plParm5,undefined8 uParm6);
void FUN_00406730(void);
void FUN_00406790(void);
void FUN_00406820(long lParm1);
void FUN_00406840(long lParm1);
long FUN_00406850(long lParm1,ulong *puParm2);
long FUN_00406890(long lParm1,ulong *puParm2,ulong uParm3);
void FUN_00406920(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00406a60(long *plParm1,int iParm2);
ulong FUN_00406ab0(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_00406ae0(char *pcParm1,char **ppcParm2,uint uParm3,undefined8 *puParm4,long lParm5);
ulong FUN_00406e00(int iParm1);
ulong FUN_00406e20(ulong *puParm1,int iParm2);
ulong FUN_00406e50(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_00406e80(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_004071c0(ulong uParm1,ulong uParm2);
void FUN_00407210(undefined8 uParm1);
ulong FUN_00407250(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_004072c0(void);
void FUN_004072e0(void);
void FUN_00407310(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_004073f0(undefined8 uParm1);
ulong FUN_00407480(ulong param_1,undefined8 param_2,ulong param_3);
void FUN_004075d0(undefined8 uParm1);
ulong FUN_004075e0(long lParm1);
undefined8 FUN_00407670(void);
void FUN_00407690(long lParm1,int *piParm2);
ulong FUN_00407770(int param_1,undefined8 *param_2,char *param_3,char **param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_00407d00(char *pcParm1,int *piParm2,int iParm3);
ulong FUN_00407dc0(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,uint *param_7,uint param_8);
void FUN_00408280(void);
void FUN_004082e0(void);
void FUN_00408300(long lParm1);
ulong FUN_00408320(uint *puParm1,byte *pbParm2,long lParm3);
ulong * FUN_00408390(ulong *puParm1,undefined8 uParm2,ulong uParm3);
void FUN_00408470(long lParm1,long lParm2);
ulong FUN_004084b0(int iParm1);
void FUN_004084d0(long lParm1,long lParm2);
void FUN_00408510(ulong *puParm1,undefined4 uParm2);
ulong FUN_00408520(long lParm1,long lParm2);
void FUN_00408540(ulong *puParm1);
void FUN_00408560(long lParm1,long lParm2);
void FUN_00408580(long lParm1,long lParm2);
ulong FUN_004085a0(long lParm1,long lParm2);
ulong FUN_004085f0(long lParm1,long lParm2);
void FUN_00408620(undefined8 uParm1,undefined8 uParm2,undefined8 *puParm3,long lParm4,byte bParm5,long lParm6);
void FUN_00408690(long *plParm1);
ulong FUN_004086f0(long lParm1,long lParm2);
long FUN_00408830(long *plParm1,long lParm2,uint uParm3);
undefined8 FUN_004088a0(char *pcParm1,long lParm2,ulong uParm3);
ulong FUN_004089f0(long lParm1,uint uParm2,long lParm3,long lParm4,long lParm5);
ulong FUN_00408c20(long lParm1,long lParm2,long lParm3,long lParm4,long lParm5,long lParm6);
ulong FUN_00408c90(long *plParm1);
void FUN_00408d20(long *plParm1,code *pcParm2,undefined8 uParm3);
void FUN_00408d90(long *plParm1,code *pcParm2,undefined8 uParm3);
undefined8 FUN_00408df0(long lParm1,ulong uParm2);
void FUN_00408e90(long param_1,undefined8 param_2,long param_3,long param_4,undefined8 param_5,byte param_6,long param_7);
undefined8 FUN_00408f40(long *plParm1,undefined8 uParm2);
void FUN_00409080(undefined4 *puParm1);
void FUN_00409090(undefined4 *puParm1);
void FUN_004090a0(undefined8 *puParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
undefined8 FUN_00409150(undefined8 *puParm1,undefined8 uParm2);
ulong FUN_004091b0(long *plParm1,long lParm2);
undefined8 FUN_004092b0(long *plParm1,long lParm2);
undefined8 FUN_00409300(long *plParm1,ulong *puParm2,ulong uParm3);
undefined8 FUN_004093f0(long lParm1,undefined4 uParm2,long lParm3);
undefined8 FUN_00409490(long lParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00409550(long lParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_004095e0(long *plParm1,ulong uParm2);
ulong FUN_004097b0(ulong uParm1,long lParm2);
void FUN_004097e0(long lParm1);
void FUN_00409860(long *plParm1);
long FUN_00409a20(long *plParm1,long lParm2,uint *puParm3);
undefined8 FUN_00409ad0(long *plParm1);
undefined8 FUN_0040a120(undefined8 *puParm1,undefined8 uParm2,long lParm3,long lParm4,byte bParm5,long lParm6);
ulong FUN_0040a280(long *plParm1,int iParm2);
undefined8 FUN_0040a390(long lParm1,long lParm2);
undefined8 FUN_0040a420(long *plParm1,long lParm2);
void FUN_0040a5f0(undefined4 *puParm1,undefined *puParm2);
ulong FUN_0040a610(long lParm1,long lParm2,ulong uParm3);
ulong FUN_0040a6f0(long lParm1,undefined8 *puParm2,long lParm3);
void FUN_0040a810(long lParm1);
void FUN_0040a910(undefined8 *puParm1);
void FUN_0040a930(undefined8 *puParm1);
long FUN_0040a980(long *plParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040abe0(long *plParm1,long lParm2,ulong uParm3);
undefined8 FUN_0040ac70(long *plParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
ulong FUN_0040aeb0(undefined4 *puParm1,long *plParm2,long lParm3,char cParm4);
undefined8 FUN_0040b120(long lParm1);
undefined8 FUN_0040b200(long *plParm1);
void FUN_0040b3f0(long lParm1);
void FUN_0040b450(long lParm1);
void FUN_0040b490(long *plParm1);
undefined8 FUN_0040b610(long lParm1,undefined8 uParm2,long lParm3,long lParm4,long lParm5);
void FUN_0040b740(long lParm1);
undefined8 FUN_0040b800(long *plParm1);
void FUN_0040b870(long lParm1);
undefined8 FUN_0040ba60(ulong *puParm1,long lParm2,ulong uParm3,int iParm4);
undefined8 FUN_0040bc30(ulong uParm1,undefined8 uParm2,long lParm3,long *plParm4,int *piParm5,int *piParm6);
long * FUN_0040bf00(long **pplParm1);
undefined8 FUN_0040c080(byte **ppbParm1,byte *pbParm2,ulong uParm3);
undefined8 FUN_0040c7a0(void);
long FUN_0040c7b0(long lParm1,long *plParm2,long lParm3);
undefined8 FUN_0040c810(long *plParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_0040c910(long *plParm1,long *plParm2,long lParm3);
long FUN_0040c950(long lParm1,long **pplParm2,uint uParm3);
ulong FUN_0040c980(long lParm1,long lParm2,ulong uParm3);
long FUN_0040ca00(long *plParm1,long lParm2,long *plParm3,long lParm4,uint uParm5);
ulong FUN_0040ca50(long lParm1,undefined4 *puParm2,undefined8 uParm3,ulong uParm4);
long FUN_0040cb50(long lParm1,long *plParm2,long lParm3);
undefined8 FUN_0040cbc0(long param_1,long *param_2,long *param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6,undefined8 param_7);
undefined8 FUN_0040ccd0(undefined4 *puParm1,long *plParm2,long lParm3,char *pcParm4);
long FUN_0040cdc0(undefined8 *puParm1,int *piParm2,long *plParm3,long *plParm4,undefined *puParm5);
long ** FUN_0040cea0(long **pplParm1,long lParm2);
void FUN_0040cf50(long lParm1,undefined8 uParm2,undefined8 uParm3,undefined uParm4);
long FUN_0040cf80(undefined4 *puParm1,long *plParm2,long lParm3);
ulong FUN_0040d120(undefined8 uParm1,long lParm2);
undefined8 FUN_0040d1a0(long lParm1,long *plParm2,long *plParm3,undefined8 uParm4);
undefined8 FUN_0040d250(long *plParm1,long *plParm2,undefined8 *puParm3);
undefined8 FUN_0040d2e0(long *plParm1,long lParm2,long lParm3);
undefined8 FUN_0040d480(long *plParm1,undefined8 uParm2,undefined8 uParm3,long lParm4,undefined8 uParm5,long lParm6);
long FUN_0040d570(long *plParm1,long lParm2,ulong uParm3,undefined8 uParm4);
ulong * FUN_0040d7b0(undefined4 *puParm1,long lParm2,long lParm3,ulong uParm4);
ulong FUN_0040d890(long *plParm1);
long FUN_0040da80(long *plParm1,long lParm2,undefined8 uParm3);
ulong * FUN_0040dbf0(undefined4 *puParm1,long lParm2,long lParm3);
ulong FUN_0040dcb0(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_0040dd60(long lParm1,long lParm2,long lParm3,undefined8 uParm4,uint uParm5);
long FUN_0040dff0(long *plParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_0040e4f0(long lParm1,long lParm2);
undefined8 FUN_0040ea10(long *plParm1,long *plParm2,long *plParm3,long *plParm4,long *plParm5);
ulong FUN_0040ebd0(long lParm1,long lParm2,long lParm3);
ulong FUN_0040ecc0(long *plParm1,long lParm2,long lParm3,long lParm4);
void FUN_0040eeb0(long lParm1,long lParm2,long lParm3,long lParm4,long lParm5,long lParm6);
undefined8 FUN_0040ef50(long lParm1,long lParm2,long *plParm3,undefined8 uParm4);
void FUN_0040efd0(long lParm1);
undefined8 FUN_0040f030(long *param_1,long param_2,undefined8 param_3,long *param_4,long *param_5,long param_6,long param_7);
undefined8 FUN_0040f2b0(long *plParm1,long *plParm2,undefined8 *puParm3,long lParm4,undefined8 uParm5,undefined4 *puParm6);
undefined8 FUN_0040f370(undefined8 uParm1,byte *pbParm2);
undefined8 FUN_0040f3b0(long param_1,undefined8 param_2,long *param_3,ulong *param_4,ulong *param_5,byte *param_6,ulong param_7);
long FUN_0040fad0(long lParm1,undefined8 uParm2,undefined8 uParm3,char *pcParm4,byte bParm5,int *piParm6);
void FUN_0040fcf0(long **pplParm1,long *plParm2,long *plParm3,undefined4 *puParm4);
ulong FUN_004101d0(long lParm1,ulong *puParm2,long lParm3,long lParm4,long lParm5);
long FUN_004104a0(long param_1,undefined8 param_2,long param_3,long *param_4,long param_5,long param_6,long param_7);
ulong FUN_004107f0(undefined8 *puParm1,long lParm2,long lParm3,long *plParm4,char cParm5);
ulong FUN_00410b00(long lParm1,long lParm2,long *plParm3,long *plParm4,undefined8 uParm5);
ulong FUN_00410d50(long param_1,long *param_2,long param_3,long param_4,undefined8 param_5,long param_6,uint param_7);
undefined8 FUN_00411240(long lParm1,long *plParm2,undefined8 *puParm3,undefined8 uParm4,long lParm5);
undefined8 FUN_004112c0(long lParm1,long lParm2,long lParm3);
ulong FUN_00411650(long lParm1,long *plParm2,long *plParm3);
long FUN_00411a00(int *piParm1,long lParm2,long lParm3);
long FUN_00411bc0(int *piParm1,long lParm2);
ulong FUN_00411c30(long lParm1,long *plParm2,long *plParm3);
undefined8 FUN_00411ed0(int *piParm1,long lParm2,long lParm3);
long FUN_00411fa0(long lParm1,char cParm2,long *plParm3);
ulong FUN_00412340(long lParm1,long *plParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_004123d0(long lParm1,long *plParm2,long lParm3,undefined8 uParm4);
ulong FUN_00412560(long lParm1,long *plParm2,long lParm3,long *plParm4,long *plParm5);
ulong FUN_004128f0(long lParm1,long *plParm2,long lParm3,long lParm4);
ulong FUN_00412aa0(long lParm1,long *plParm2);
ulong FUN_00412bb0(long lParm1);
ulong FUN_00412df0(long *param_1,long param_2,long param_3,long param_4,long param_5,undefined8 param_6,ulong param_7,ulong *param_8,uint param_9);
undefined8 FUN_00413830(undefined4 *puParm1,long lParm2,undefined *puParm3,int iParm4,undefined8 uParm5,char cParm6);
long FUN_00413910(long lParm1,long lParm2,long lParm3,ulong uParm4,int *piParm5);
ulong FUN_00413ec0(long *plParm1);
undefined8 FUN_00413f50(long *plParm1,long lParm2,ulong uParm3);
void FUN_004145c0(undefined8 uParm1,long lParm2);
long FUN_004145e0(undefined8 uParm1,byte *pbParm2,undefined8 uParm3);
long FUN_00414690(long lParm1,long lParm2,long lParm3,undefined4 *puParm4,ulong uParm5,undefined4 *puParm6);
long FUN_00414a80(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,undefined8 uParm5,int *piParm6);
long FUN_00414b90(long lParm1,long *plParm2,undefined8 *puParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_004150b0(undefined8 uParm1,undefined8 *puParm2,long lParm3,undefined8 uParm4,long lParm5,int *piParm6);
long FUN_00415200(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_00415350(undefined8 uParm1,long *plParm2,ulong uParm3,int *piParm4);
ulong FUN_00415430(long *plParm1,undefined8 uParm2,undefined8 uParm3,ulong uParm4);
undefined1 * FUN_004155d0(undefined8 uParm1,undefined8 uParm2,long lParm3);
undefined8 FUN_00415630(long *plParm1);
long FUN_004156f0(long param_1,undefined8 param_2,long param_3,long param_4,long param_5,undefined8 param_6,ulong *param_7,char param_8);
void FUN_00415970(void);
ulong FUN_00415990(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,long param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_00415aa0(long lParm1);
ulong FUN_00415b70(char *pcParm1,long lParm2);
char * FUN_00415bb0(ulong uParm1,long lParm2,long lParm3);
ulong FUN_00415d60(void);
ulong FUN_00415da0(undefined8 param_1,undefined8 *param_2);
int * FUN_00416010(int *param_1,ulong *param_2);
ulong FUN_004161a0(undefined8 uParm1,undefined8 *puParm2);
uint * FUN_004163b0(undefined8 uParm1,int *piParm2,ulong *puParm3);
void FUN_00416520(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5,undefined8 *puParm6);
int * FUN_00416660(undefined8 *puParm1,long lParm2,undefined8 *puParm3,undefined8 *puParm4,uint **ppuParm5,uint **ppuParm6);
undefined8 FUN_00416c10(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_00417060(uint param_1);
void FUN_004170a0(uint uParm1);
ulong FUN_0041c030(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined FUN_0041c0f0(int iParm1);;
ulong FUN_0041c100(uint uParm1);
ulong FUN_0041c110(byte *pbParm1,byte *pbParm2);
undefined8 FUN_0041c1d0(void);
ulong FUN_0041c1e0(ulong uParm1);
undefined * FUN_0041c280(void);
char * FUN_0041c5e0(void);
double FUN_0041c6a0(int *piParm1);
void FUN_0041c6f0(int *param_1);
long FUN_0041c770(ulong uParm1,ulong uParm2);
long FUN_0041c790(void);
void FUN_0041c7c0(void);
undefined8 FUN_0041c7e0(long lParm1,long lParm2);
undefined8 FUN_0041c850(uint *puParm1,ulong *puParm2);
undefined8 FUN_0041ca70(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_0041d740(undefined auParm1 [16]);
ulong FUN_0041d760(void);
void FUN_0041d790(void);
void FUN_0041d810(void);
undefined8 _DT_FINI(void);

