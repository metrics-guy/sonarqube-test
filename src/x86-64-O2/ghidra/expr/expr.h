typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void entry(void);
void FUN_00402740(void);
void FUN_004027b0(void);
void FUN_00402840(void);
void FUN_00402880(undefined8 *puParm1,undefined8 uParm2);
undefined8 FUN_004028d0(char *pcParm1);
ulong FUN_00402910(void);
ulong FUN_00402920(undefined8 uParm1);
void FUN_00402a50(void);
void FUN_00402a90(int *piParm1);
long FUN_00402ac0(long lParm1,ulong uParm2);
long FUN_00402b40(char *pcParm1,char *pcParm2);
undefined4 * FUN_00402cc0(undefined8 uParm1);
void FUN_00402d00(undefined8 uParm1);
undefined * FUN_00402d30(long lParm1,ulong uParm2,ulong uParm3);
ulong FUN_00402e80(long lParm1);
undefined8 *FUN_00402ea0(int *piParm1,undefined8 *puParm2,undefined8 uParm3,undefined8 uParm4,ulong uParm5);
ulong FUN_00402f30(ulong uParm1);
undefined8 FUN_00402f40(undefined8 uParm1);
long FUN_00402f50(void);
void FUN_00402f90(undefined8 uParm1);
undefined8 * FUN_00402fb0(uint *puParm1,undefined8 *puParm2);
undefined8 FUN_00402fe0(long lParm1,long lParm2);
ulong FUN_004031d0(undefined8 uParm1,undefined8 uParm2);
undefined8 * FUN_00403200(int *piParm1,undefined8 *puParm2);
ulong FUN_00403240(undefined8 uParm1,undefined8 uParm2);
undefined8 * FUN_00403270(int *piParm1,undefined8 *puParm2);
long FUN_004032f0(byte bParm1);
long FUN_004033b0(byte bParm1);
undefined8 FUN_00403620(byte bParm1);
undefined8 FUN_004036c0(byte bParm1);
undefined4 * FUN_00403850(ulong uParm1);
undefined8 FUN_00403a90(byte bParm1);
long FUN_00403b00(byte bParm1);
void FUN_00403c20(void);
void FUN_00403dc0(uint uParm1);
void FUN_00403f00(void);
char * FUN_00403fb0(ulong uParm1,long lParm2);
void FUN_00404050(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,code *param_6);
byte * FUN_00404160(byte *pbParm1,ulong uParm2);
long FUN_00404210(long lParm1);
void FUN_004042a0(char *pcParm1);
undefined8 * FUN_00404420(long lParm1);
undefined8 * FUN_004044d0(int *piParm1,undefined8 *puParm2);
ulong FUN_00404820(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8);
ulong FUN_004048e0(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9);
ulong FUN_004049b0(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9,byte param_10);
undefined8 FUN_00404aa0(byte *param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined * FUN_00404b50(char *pcParm1,int iParm2);
undefined8 *FUN_00404c20(undefined *puParm1,undefined8 *puParm2,long lParm3,undefined *puParm4,ulong uParm5,uint uParm6,long lParm7,char *pcParm8,char *pcParm9);
undefined8 * FUN_00405d10(undefined1 *puParm1,undefined8 *puParm2,undefined8 uParm3,uint *puParm4);
void FUN_00405ef0(uint uParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_00405fb0(long lParm1,long lParm2);
ulong FUN_00405fe0(byte *pbParm1,byte *pbParm2);
ulong thunk_FUN_00405fe0(byte *pbParm1,byte *pbParm2);
void FUN_004062b0(undefined8 uParm1,long lParm2,long lParm3,long lParm4,long lParm5);
void FUN_00406500(void);
void FUN_00406560(long lParm1);
void FUN_00406580(long lParm1);
long FUN_00406590(long lParm1,long lParm2);
void FUN_00406600(undefined8 uParm1);
undefined8 FUN_00406620(void);
undefined8 FUN_00406650(long *plParm1,int iParm2);
ulong FUN_004066a0(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_004066d0(char *pcParm1,char **ppcParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_004069f0(ulong uParm1,ulong uParm2);
void FUN_00406a40(undefined8 uParm1);
ulong FUN_00406a80(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00406af0(void);
void FUN_00406b10(void);
void FUN_00406b40(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00406c20(long lParm1,int *piParm2);
ulong FUN_00406d00(int param_1,undefined8 *param_2,char *param_3,char **param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_00407290(char *pcParm1,int *piParm2,int iParm3);
ulong FUN_00407350(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,uint *param_7,uint param_8);
void FUN_00407810(void);
void FUN_00407870(void);
void FUN_00407890(long lParm1);
ulong FUN_004078b0(uint *puParm1,byte *pbParm2,long lParm3);
ulong * FUN_00407920(ulong *puParm1,undefined8 uParm2,ulong uParm3);
void FUN_00407a00(long lParm1,long lParm2);
ulong FUN_00407a40(int iParm1);
void FUN_00407a60(long lParm1,long lParm2);
void FUN_00407aa0(ulong *puParm1,undefined4 uParm2);
ulong FUN_00407ab0(long lParm1,long lParm2);
void FUN_00407ad0(ulong *puParm1);
void FUN_00407af0(long lParm1,long lParm2);
void FUN_00407b10(long lParm1,long lParm2);
ulong FUN_00407b30(long lParm1,long lParm2);
ulong FUN_00407b80(long lParm1,long lParm2);
void FUN_00407bb0(undefined8 uParm1,undefined8 uParm2,undefined8 *puParm3,long lParm4,byte bParm5,long lParm6);
void FUN_00407c20(long *plParm1);
ulong FUN_00407c80(long lParm1,long lParm2);
long FUN_00407dc0(long *plParm1,long lParm2,uint uParm3);
undefined8 FUN_00407e30(char *pcParm1,long lParm2,ulong uParm3);
ulong FUN_00407f80(long lParm1,uint uParm2,long lParm3,long lParm4,long lParm5);
ulong FUN_004081b0(long lParm1,long lParm2,long lParm3,long lParm4,long lParm5,long lParm6);
ulong FUN_00408220(long *plParm1);
void FUN_004082b0(long *plParm1,code *pcParm2,undefined8 uParm3);
void FUN_00408320(long *plParm1,code *pcParm2,undefined8 uParm3);
undefined8 FUN_00408380(long lParm1,ulong uParm2);
void FUN_00408420(long param_1,undefined8 param_2,long param_3,long param_4,undefined8 param_5,byte param_6,long param_7);
undefined8 FUN_004084d0(long *plParm1,undefined8 uParm2);
void FUN_00408610(undefined4 *puParm1);
void FUN_00408620(undefined4 *puParm1);
void FUN_00408630(undefined8 *puParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
undefined8 FUN_004086e0(undefined8 *puParm1,undefined8 uParm2);
ulong FUN_00408740(long *plParm1,long lParm2);
undefined8 FUN_00408840(long *plParm1,long lParm2);
undefined8 FUN_00408890(long *plParm1,ulong *puParm2,ulong uParm3);
undefined8 FUN_00408980(long lParm1,undefined4 uParm2,long lParm3);
undefined8 FUN_00408a20(long lParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00408ae0(long lParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_00408b70(long *plParm1,ulong uParm2);
ulong FUN_00408d40(ulong uParm1,long lParm2);
void FUN_00408d70(long lParm1);
void FUN_00408df0(long *plParm1);
long FUN_00408fb0(long *plParm1,long lParm2,uint *puParm3);
undefined8 FUN_00409060(long *plParm1);
undefined8 FUN_004096b0(undefined8 *puParm1,undefined8 uParm2,long lParm3,long lParm4,byte bParm5,long lParm6);
ulong FUN_00409810(long *plParm1,int iParm2);
undefined8 FUN_00409920(long lParm1,long lParm2);
undefined8 FUN_004099b0(long *plParm1,long lParm2);
void FUN_00409b80(undefined4 *puParm1,undefined *puParm2);
ulong FUN_00409ba0(long lParm1,long lParm2,ulong uParm3);
ulong FUN_00409c80(long lParm1,undefined8 *puParm2,long lParm3);
void FUN_00409da0(long lParm1);
void FUN_00409ea0(undefined8 *puParm1);
void FUN_00409ec0(undefined8 *puParm1);
long FUN_00409f10(long *plParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040a170(long *plParm1,long lParm2,ulong uParm3);
undefined8 FUN_0040a200(long *plParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
ulong FUN_0040a440(undefined4 *puParm1,long *plParm2,long lParm3,char cParm4);
undefined8 FUN_0040a6b0(long lParm1);
undefined8 FUN_0040a790(long *plParm1);
void FUN_0040a980(long lParm1);
void FUN_0040a9e0(long lParm1);
void FUN_0040aa20(long *plParm1);
undefined8 FUN_0040aba0(long lParm1,undefined8 uParm2,long lParm3,long lParm4,long lParm5);
void FUN_0040acd0(long lParm1);
undefined8 FUN_0040ad90(long *plParm1);
void FUN_0040ae00(long lParm1);
undefined8 FUN_0040aff0(ulong *puParm1,long lParm2,ulong uParm3,int iParm4);
undefined8 FUN_0040b1c0(ulong uParm1,undefined8 uParm2,long lParm3,long *plParm4,int *piParm5,int *piParm6);
long * FUN_0040b490(long **pplParm1);
undefined8 FUN_0040b610(byte **ppbParm1,byte *pbParm2,ulong uParm3);
undefined8 FUN_0040bd30(void);
long FUN_0040bd40(long lParm1,long *plParm2,long lParm3);
undefined8 FUN_0040bda0(long *plParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_0040bea0(long *plParm1,long *plParm2,long lParm3);
long FUN_0040bee0(long lParm1,long **pplParm2,uint uParm3);
ulong FUN_0040bf10(long lParm1,long lParm2,ulong uParm3);
long FUN_0040bf90(long *plParm1,long lParm2,long *plParm3,long lParm4,uint uParm5);
ulong FUN_0040bfe0(long lParm1,undefined4 *puParm2,undefined8 uParm3,ulong uParm4);
long FUN_0040c0e0(long lParm1,long *plParm2,long lParm3);
undefined8 FUN_0040c150(long param_1,long *param_2,long *param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6,undefined8 param_7);
undefined8 FUN_0040c260(undefined4 *puParm1,long *plParm2,long lParm3,char *pcParm4);
long FUN_0040c350(undefined8 *puParm1,int *piParm2,long *plParm3,long *plParm4,undefined *puParm5);
long ** FUN_0040c430(long **pplParm1,long lParm2);
void FUN_0040c4e0(long lParm1,undefined8 uParm2,undefined8 uParm3,undefined uParm4);
long FUN_0040c510(undefined4 *puParm1,long *plParm2,long lParm3);
ulong FUN_0040c6b0(undefined8 uParm1,long lParm2);
undefined8 FUN_0040c730(long lParm1,long *plParm2,long *plParm3,undefined8 uParm4);
undefined8 FUN_0040c7e0(long *plParm1,long *plParm2,undefined8 *puParm3);
undefined8 FUN_0040c870(long *plParm1,long lParm2,long lParm3);
undefined8 FUN_0040ca10(long *plParm1,undefined8 uParm2,undefined8 uParm3,long lParm4,undefined8 uParm5,long lParm6);
long FUN_0040cb00(long *plParm1,long lParm2,ulong uParm3,undefined8 uParm4);
ulong * FUN_0040cd40(undefined4 *puParm1,long lParm2,long lParm3,ulong uParm4);
ulong FUN_0040ce20(long *plParm1);
long FUN_0040d010(long *plParm1,long lParm2,undefined8 uParm3);
ulong * FUN_0040d180(undefined4 *puParm1,long lParm2,long lParm3);
ulong FUN_0040d240(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_0040d2f0(long lParm1,long lParm2,long lParm3,undefined8 uParm4,uint uParm5);
long FUN_0040d580(long *plParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_0040da80(long lParm1,long lParm2);
undefined8 FUN_0040dfa0(long *plParm1,long *plParm2,long *plParm3,long *plParm4,long *plParm5);
ulong FUN_0040e160(long lParm1,long lParm2,long lParm3);
ulong FUN_0040e250(long *plParm1,long lParm2,long lParm3,long lParm4);
void FUN_0040e440(long lParm1,long lParm2,long lParm3,long lParm4,long lParm5,long lParm6);
undefined8 FUN_0040e4e0(long lParm1,long lParm2,long *plParm3,undefined8 uParm4);
void FUN_0040e560(long lParm1);
undefined8 FUN_0040e5c0(long *param_1,long param_2,undefined8 param_3,long *param_4,long *param_5,long param_6,long param_7);
undefined8 FUN_0040e840(long *plParm1,long *plParm2,undefined8 *puParm3,long lParm4,undefined8 uParm5,undefined4 *puParm6);
undefined8 FUN_0040e900(undefined8 uParm1,byte *pbParm2);
undefined8 FUN_0040e940(long param_1,undefined8 param_2,long *param_3,ulong *param_4,ulong *param_5,byte *param_6,ulong param_7);
long FUN_0040f060(long lParm1,undefined8 uParm2,undefined8 uParm3,char *pcParm4,byte bParm5,int *piParm6);
void FUN_0040f280(long **pplParm1,long *plParm2,long *plParm3,undefined4 *puParm4);
ulong FUN_0040f760(long lParm1,ulong *puParm2,long lParm3,long lParm4,long lParm5);
long FUN_0040fa30(long param_1,undefined8 param_2,long param_3,long *param_4,long param_5,long param_6,long param_7);
ulong FUN_0040fd80(undefined8 *puParm1,long lParm2,long lParm3,long *plParm4,char cParm5);
ulong FUN_00410090(long lParm1,long lParm2,long *plParm3,long *plParm4,undefined8 uParm5);
ulong FUN_004102e0(long param_1,long *param_2,long param_3,long param_4,undefined8 param_5,long param_6,uint param_7);
undefined8 FUN_004107d0(long lParm1,long *plParm2,undefined8 *puParm3,undefined8 uParm4,long lParm5);
undefined8 FUN_00410850(long lParm1,long lParm2,long lParm3);
ulong FUN_00410be0(long lParm1,long *plParm2,long *plParm3);
long FUN_00410f90(int *piParm1,long lParm2,long lParm3);
long FUN_00411150(int *piParm1,long lParm2);
ulong FUN_004111c0(long lParm1,long *plParm2,long *plParm3);
undefined8 FUN_00411460(int *piParm1,long lParm2,long lParm3);
long FUN_00411530(long lParm1,char cParm2,long *plParm3);
ulong FUN_004118d0(long lParm1,long *plParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_00411960(long lParm1,long *plParm2,long lParm3,undefined8 uParm4);
ulong FUN_00411af0(long lParm1,long *plParm2,long lParm3,long *plParm4,long *plParm5);
ulong FUN_00411e80(long lParm1,long *plParm2,long lParm3,long lParm4);
ulong FUN_00412030(long lParm1,long *plParm2);
ulong FUN_00412140(long lParm1);
ulong FUN_00412380(long *param_1,long param_2,long param_3,long param_4,long param_5,undefined8 param_6,ulong param_7,ulong *param_8,uint param_9);
undefined8 FUN_00412dc0(undefined4 *puParm1,long lParm2,undefined *puParm3,int iParm4,undefined8 uParm5,char cParm6);
long FUN_00412ea0(long lParm1,long lParm2,long lParm3,ulong uParm4,int *piParm5);
ulong FUN_00413450(long *plParm1);
undefined8 FUN_004134e0(long *plParm1,long lParm2,ulong uParm3);
void FUN_00413b50(undefined8 uParm1,long lParm2);
long FUN_00413b70(undefined8 uParm1,byte *pbParm2,undefined8 uParm3);
long FUN_00413c20(long lParm1,long lParm2,long lParm3,undefined4 *puParm4,ulong uParm5,undefined4 *puParm6);
long FUN_00414010(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,undefined8 uParm5,int *piParm6);
long FUN_00414120(long lParm1,long *plParm2,undefined8 *puParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_00414640(undefined8 uParm1,undefined8 *puParm2,long lParm3,undefined8 uParm4,long lParm5,int *piParm6);
long FUN_00414790(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_004148e0(undefined8 uParm1,long *plParm2,ulong uParm3,int *piParm4);
ulong FUN_004149c0(long *plParm1,undefined8 uParm2,undefined8 uParm3,ulong uParm4);
undefined1 * FUN_00414b60(undefined8 uParm1,undefined8 uParm2,long lParm3);
undefined8 FUN_00414bc0(long *plParm1);
long FUN_00414c80(long param_1,undefined8 param_2,long param_3,long param_4,long param_5,undefined8 param_6,ulong *param_7,char param_8);
void FUN_00414f00(long *plParm1);
void FUN_00414f50(void);
ulong FUN_00414f70(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined FUN_00415030(int iParm1);;
ulong FUN_00415040(uint uParm1);
ulong FUN_00415050(byte *pbParm1,byte *pbParm2);
undefined8 FUN_00415110(void);
ulong FUN_00415120(ulong uParm1);
undefined * FUN_004151c0(void);
char * FUN_00415520(void);
ulong FUN_004155e0(byte bParm1);
void FUN_00415600(undefined8 uParm1);
void FUN_00415690(undefined8 uParm1);
ulong FUN_004156a0(long lParm1);
undefined8 FUN_00415730(void);
ulong FUN_00415750(char *pcParm1,long lParm2);
char * FUN_00415790(ulong uParm1,long lParm2,long lParm3);
ulong FUN_00415940(void);
undefined8 * FUN_00415980(undefined8 param_1,undefined8 *param_2);
undefined8 * FUN_00415bf0(int *param_1,ulong *param_2);
undefined8 * FUN_00415d80(undefined8 uParm1,undefined8 *puParm2);
undefined8 * FUN_00415f90(undefined8 uParm1,int *piParm2,ulong *puParm3);
void FUN_00416100(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5,undefined8 *puParm6);
undefined8 *FUN_00416240(undefined8 *puParm1,long lParm2,undefined8 *puParm3,undefined8 *puParm4,uint **ppuParm5,uint **ppuParm6);
undefined8 FUN_004167f0(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_00416c40(uint param_1);
void FUN_00416c80(uint uParm1);
ulong ** FUN_00416e80(ulong **ppuParm1,ulong **ppuParm2,ulong **ppuParm3,undefined8 uParm4);
double FUN_0041bc10(int *piParm1);
void FUN_0041bc60(int *param_1);
long FUN_0041bce0(ulong uParm1,ulong uParm2);
long FUN_0041bd00(void);
undefined8 FUN_0041bd30(uint *puParm1,ulong *puParm2);
undefined8 FUN_0041bf50(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_0041cc20(undefined auParm1 [16]);
ulong FUN_0041cc40(void);
void FUN_0041cc70(void);
undefined8 _DT_FINI(void);

