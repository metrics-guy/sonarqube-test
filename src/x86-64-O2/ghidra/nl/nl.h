typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined3;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined7;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
undefined8 * FUN_004025f0(uint uParm1,undefined8 *puParm2);
void entry(void);
void FUN_00402b30(void);
void FUN_00402ba0(void);
void FUN_00402c30(void);
void FUN_00402c70(void);
void FUN_00402c90(void);
ulong FUN_00402cb0(void);
ulong FUN_00402d10(char **ppcParm1,undefined8 *puParm2,undefined8 uParm3);
ulong FUN_00402de0(void);
void FUN_00402ed0(void);
void FUN_00402f10(void);
void FUN_00402f50(void);
void FUN_00402f90(void);
void FUN_004030c0(undefined8 uParm1);
undefined8 FUN_00403130(char *pcParm1);
void FUN_00403210(void);
void FUN_004033e0(uint uParm1);
void FUN_004035a0(long lParm1,ulong uParm2);
void FUN_004035d0(undefined4 *puParm1);
long * FUN_00403700(long *plParm1,undefined8 uParm2);
undefined8 * FUN_00403710(long lParm1);
undefined8 * FUN_004037c0(int *piParm1,undefined8 *puParm2);
ulong FUN_00403b10(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8);
ulong FUN_00403bd0(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9);
ulong FUN_00403ca0(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9,byte param_10);
undefined8 FUN_00403d90(byte *param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined * FUN_00403e40(char *pcParm1,int iParm2);
undefined8 *FUN_00403f10(undefined *puParm1,undefined8 *puParm2,long lParm3,undefined *puParm4,ulong uParm5,uint uParm6,long lParm7,char *pcParm8,char *pcParm9);
undefined8 * FUN_00405000(undefined1 *puParm1,undefined8 *puParm2,undefined8 uParm3,uint *puParm4);
ulong FUN_004051a0(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
void FUN_00405270(uint uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 * FUN_00405310(undefined8 *puParm1);
void FUN_00405320(undefined8 uParm1,long lParm2,long lParm3,long lParm4,long lParm5);
void FUN_00405570(void);
void FUN_004055d0(void);
void FUN_00405660(long lParm1);
void FUN_00405680(long lParm1);
long FUN_00405690(long lParm1,ulong *puParm2);
long FUN_00405760(long lParm1,ulong *puParm2);
long FUN_00405770(void);
long FUN_004057a0(undefined8 param_1,ulong param_2,long param_3,long param_4,undefined8 param_5,undefined8 param_6,uint param_7);
void FUN_00405880(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
undefined8 FUN_004058b0(long *plParm1,int iParm2);
ulong FUN_00405900(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_00405930(char *pcParm1,char **ppcParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_00405c50(ulong uParm1,ulong uParm2);
void FUN_00405ca0(undefined8 uParm1);
ulong FUN_00405ce0(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00405d50(void);
void FUN_00405d70(void);
void FUN_00405da0(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00405e80(undefined8 uParm1);
void FUN_00405f10(undefined8 uParm1);
ulong FUN_00405f20(long lParm1);
undefined8 FUN_00405fb0(void);
void FUN_00405fd0(long lParm1,int *piParm2);
ulong FUN_004060b0(int param_1,undefined8 *param_2,char *param_3,char **param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_00406640(char *pcParm1,int *piParm2,int iParm3);
ulong FUN_00406700(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,uint *param_7,uint param_8);
void FUN_00406bc0(void);
void FUN_00406c20(void);
void FUN_00406c40(long lParm1);
ulong FUN_00406c60(uint *puParm1,byte *pbParm2,long lParm3);
void FUN_00406cd0(long lParm1,long lParm2);
ulong FUN_00406d10(int iParm1);
void FUN_00406d30(long lParm1,long lParm2);
void FUN_00406d70(ulong *puParm1,undefined4 uParm2);
ulong FUN_00406d80(long lParm1,long lParm2);
void FUN_00406da0(ulong *puParm1);
void FUN_00406dc0(long lParm1,long lParm2);
void FUN_00406de0(long lParm1,long lParm2);
ulong FUN_00406e00(long lParm1,long lParm2);
ulong FUN_00406e50(long lParm1,long lParm2);
void FUN_00406e80(undefined8 uParm1,undefined8 uParm2,undefined8 *puParm3,long lParm4,byte bParm5,long lParm6);
void FUN_00406ef0(long *plParm1);
ulong FUN_00406f50(long lParm1,long lParm2);
long FUN_00407090(long *plParm1,long lParm2,uint uParm3);
undefined8 FUN_00407100(char *pcParm1,long lParm2,ulong uParm3);
ulong FUN_00407250(long lParm1,uint uParm2,long lParm3,long lParm4,long lParm5);
ulong FUN_00407480(long lParm1,long lParm2,long lParm3,long lParm4,long lParm5,long lParm6);
ulong FUN_004074f0(long *plParm1);
void FUN_00407580(long *plParm1,code *pcParm2,undefined8 uParm3);
void FUN_004075f0(long *plParm1,code *pcParm2,undefined8 uParm3);
undefined8 FUN_00407650(long lParm1,ulong uParm2);
void FUN_004076f0(long param_1,undefined8 param_2,long param_3,long param_4,undefined8 param_5,byte param_6,long param_7);
undefined8 FUN_004077a0(long *plParm1,undefined8 uParm2);
void FUN_004078e0(undefined4 *puParm1);
void FUN_004078f0(undefined4 *puParm1);
void FUN_00407900(undefined8 *puParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
undefined8 FUN_004079b0(undefined8 *puParm1,undefined8 uParm2);
ulong FUN_00407a10(long *plParm1,long lParm2);
undefined8 FUN_00407b10(long *plParm1,long lParm2);
undefined8 FUN_00407b60(long *plParm1,ulong *puParm2,ulong uParm3);
undefined8 FUN_00407c50(long lParm1,undefined4 uParm2,long lParm3);
undefined8 FUN_00407cf0(long lParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00407db0(long lParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_00407e40(long *plParm1,ulong uParm2);
ulong FUN_00408010(ulong uParm1,long lParm2);
void FUN_00408040(long lParm1);
void FUN_004080c0(long *plParm1);
long FUN_00408280(long *plParm1,long lParm2,uint *puParm3);
undefined8 FUN_00408330(long *plParm1);
undefined8 FUN_00408980(undefined8 *puParm1,undefined8 uParm2,long lParm3,long lParm4,byte bParm5,long lParm6);
ulong FUN_00408ae0(long *plParm1,int iParm2);
undefined8 FUN_00408bf0(long lParm1,long lParm2);
undefined8 FUN_00408c80(long *plParm1,long lParm2);
void FUN_00408e50(undefined4 *puParm1,undefined *puParm2);
ulong FUN_00408e70(long lParm1,long lParm2,ulong uParm3);
ulong FUN_00408f50(long lParm1,undefined8 *puParm2,long lParm3);
void FUN_00409070(long lParm1);
void FUN_00409170(undefined8 *puParm1);
void FUN_00409190(undefined8 *puParm1);
long FUN_004091e0(long *plParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00409440(long *plParm1,long lParm2,ulong uParm3);
undefined8 FUN_004094d0(long *plParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
ulong FUN_00409710(undefined4 *puParm1,long *plParm2,long lParm3,char cParm4);
undefined8 FUN_00409980(long lParm1);
undefined8 FUN_00409a60(long *plParm1);
void FUN_00409c50(long lParm1);
void FUN_00409cb0(long lParm1);
void FUN_00409cf0(long *plParm1);
undefined8 FUN_00409e70(long lParm1,undefined8 uParm2,long lParm3,long lParm4,long lParm5);
void FUN_00409fa0(long lParm1);
undefined8 FUN_0040a060(long *plParm1);
void FUN_0040a0d0(long lParm1);
undefined8 FUN_0040a2c0(ulong *puParm1,long lParm2,ulong uParm3,int iParm4);
undefined8 FUN_0040a490(ulong uParm1,undefined8 uParm2,long lParm3,long *plParm4,int *piParm5,int *piParm6);
long * FUN_0040a760(long **pplParm1);
undefined8 FUN_0040a8e0(byte **ppbParm1,byte *pbParm2,ulong uParm3);
undefined8 FUN_0040b000(void);
long FUN_0040b010(long lParm1,long *plParm2,long lParm3);
undefined8 FUN_0040b070(long *plParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_0040b170(long *plParm1,long *plParm2,long lParm3);
long FUN_0040b1b0(long lParm1,long **pplParm2,uint uParm3);
ulong FUN_0040b1e0(long lParm1,long lParm2,ulong uParm3);
long FUN_0040b260(long *plParm1,long lParm2,long *plParm3,long lParm4,uint uParm5);
ulong FUN_0040b2b0(long lParm1,undefined4 *puParm2,undefined8 uParm3,ulong uParm4);
long FUN_0040b3b0(long lParm1,long *plParm2,long lParm3);
undefined8 FUN_0040b420(long param_1,long *param_2,long *param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6,undefined8 param_7);
undefined8 FUN_0040b530(undefined4 *puParm1,long *plParm2,long lParm3,char *pcParm4);
long FUN_0040b620(undefined8 *puParm1,int *piParm2,long *plParm3,long *plParm4,undefined *puParm5);
long ** FUN_0040b700(long **pplParm1,long lParm2);
void FUN_0040b7b0(long lParm1,undefined8 uParm2,undefined8 uParm3,undefined uParm4);
long FUN_0040b7e0(undefined4 *puParm1,long *plParm2,long lParm3);
ulong FUN_0040b980(undefined8 uParm1,long lParm2);
undefined8 FUN_0040ba00(long lParm1,long *plParm2,long *plParm3,undefined8 uParm4);
undefined8 FUN_0040bab0(long *plParm1,long *plParm2,undefined8 *puParm3);
undefined8 FUN_0040bb40(long *plParm1,long lParm2,long lParm3);
undefined8 FUN_0040bce0(long *plParm1,undefined8 uParm2,undefined8 uParm3,long lParm4,undefined8 uParm5,long lParm6);
long FUN_0040bdd0(long *plParm1,long lParm2,ulong uParm3,undefined8 uParm4);
ulong * FUN_0040c010(undefined4 *puParm1,long lParm2,long lParm3,ulong uParm4);
ulong FUN_0040c0f0(long *plParm1);
long FUN_0040c2e0(long *plParm1,long lParm2,undefined8 uParm3);
ulong * FUN_0040c450(undefined4 *puParm1,long lParm2,long lParm3);
ulong FUN_0040c510(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_0040c5c0(long lParm1,long lParm2,long lParm3,undefined8 uParm4,uint uParm5);
long FUN_0040c850(long *plParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_0040cd50(long lParm1,long lParm2);
undefined8 FUN_0040d270(long *plParm1,long *plParm2,long *plParm3,long *plParm4,long *plParm5);
ulong FUN_0040d430(long lParm1,long lParm2,long lParm3);
ulong FUN_0040d520(long *plParm1,long lParm2,long lParm3,long lParm4);
void FUN_0040d710(long lParm1,long lParm2,long lParm3,long lParm4,long lParm5,long lParm6);
undefined8 FUN_0040d7b0(long lParm1,long lParm2,long *plParm3,undefined8 uParm4);
void FUN_0040d830(long lParm1);
undefined8 FUN_0040d890(long *param_1,long param_2,undefined8 param_3,long *param_4,long *param_5,long param_6,long param_7);
undefined8 FUN_0040db10(long *plParm1,long *plParm2,undefined8 *puParm3,long lParm4,undefined8 uParm5,undefined4 *puParm6);
undefined8 FUN_0040dbd0(undefined8 uParm1,byte *pbParm2);
undefined8 FUN_0040dc10(long param_1,undefined8 param_2,long *param_3,ulong *param_4,ulong *param_5,byte *param_6,ulong param_7);
long FUN_0040e330(long lParm1,undefined8 uParm2,undefined8 uParm3,char *pcParm4,byte bParm5,int *piParm6);
void FUN_0040e550(long **pplParm1,long *plParm2,long *plParm3,undefined4 *puParm4);
ulong FUN_0040ea30(long lParm1,ulong *puParm2,long lParm3,long lParm4,long lParm5);
long FUN_0040ed00(long param_1,undefined8 param_2,long param_3,long *param_4,long param_5,long param_6,long param_7);
ulong FUN_0040f050(undefined8 *puParm1,long lParm2,long lParm3,long *plParm4,char cParm5);
ulong FUN_0040f360(long lParm1,long lParm2,long *plParm3,long *plParm4,undefined8 uParm5);
ulong FUN_0040f5b0(long param_1,long *param_2,long param_3,long param_4,undefined8 param_5,long param_6,uint param_7);
undefined8 FUN_0040faa0(long lParm1,long *plParm2,undefined8 *puParm3,undefined8 uParm4,long lParm5);
undefined8 FUN_0040fb20(long lParm1,long lParm2,long lParm3);
ulong FUN_0040feb0(long lParm1,long *plParm2,long *plParm3);
long FUN_00410260(int *piParm1,long lParm2,long lParm3);
long FUN_00410420(int *piParm1,long lParm2);
ulong FUN_00410490(long lParm1,long *plParm2,long *plParm3);
undefined8 FUN_00410730(int *piParm1,long lParm2,long lParm3);
long FUN_00410800(long lParm1,char cParm2,long *plParm3);
ulong FUN_00410ba0(long lParm1,long *plParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_00410c30(long lParm1,long *plParm2,long lParm3,undefined8 uParm4);
ulong FUN_00410dc0(long lParm1,long *plParm2,long lParm3,long *plParm4,long *plParm5);
ulong FUN_00411150(long lParm1,long *plParm2,long lParm3,long lParm4);
ulong FUN_00411300(long lParm1,long *plParm2);
ulong FUN_00411410(long lParm1);
ulong FUN_00411650(long *param_1,long param_2,long param_3,long param_4,long param_5,undefined8 param_6,ulong param_7,ulong *param_8,uint param_9);
undefined8 FUN_00412090(undefined4 *puParm1,long lParm2,undefined *puParm3,int iParm4,undefined8 uParm5,char cParm6);
long FUN_00412170(long lParm1,long lParm2,long lParm3,ulong uParm4,int *piParm5);
ulong FUN_00412720(long *plParm1);
undefined8 FUN_004127b0(long *plParm1,long lParm2,ulong uParm3);
void FUN_00412e20(undefined8 uParm1,long lParm2);
long FUN_00412e40(undefined8 uParm1,byte *pbParm2,undefined8 uParm3);
long FUN_00412ef0(long lParm1,long lParm2,long lParm3,undefined4 *puParm4,ulong uParm5,undefined4 *puParm6);
long FUN_004132e0(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,undefined8 uParm5,int *piParm6);
long FUN_004133f0(long lParm1,long *plParm2,undefined8 *puParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_00413910(undefined8 uParm1,undefined8 *puParm2,long lParm3,undefined8 uParm4,long lParm5,int *piParm6);
long FUN_00413a60(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_00413bb0(undefined8 uParm1,long *plParm2,ulong uParm3,int *piParm4);
ulong FUN_00413c90(long *plParm1,undefined8 uParm2,undefined8 uParm3,ulong uParm4);
undefined1 * FUN_00413e30(undefined8 uParm1,undefined8 uParm2,long lParm3);
undefined8 FUN_00413e90(long *plParm1);
long FUN_00413f50(long param_1,undefined8 param_2,long param_3,long param_4,long param_5,undefined8 param_6,ulong *param_7,char param_8);
void FUN_004141d0(void);
ulong FUN_004141f0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined FUN_004142b0(int iParm1);;
ulong FUN_004142c0(uint uParm1);
ulong FUN_004142d0(byte *pbParm1,byte *pbParm2);
undefined8 FUN_00414390(void);
ulong FUN_004143a0(ulong uParm1);
undefined * FUN_00414440(void);
char * FUN_004147a0(void);
ulong FUN_00414860(char *pcParm1,long lParm2);
char * FUN_004148a0(ulong uParm1,long lParm2,long lParm3);
ulong FUN_00414a50(void);
undefined8 * FUN_00414a90(undefined8 param_1,undefined8 *param_2);
undefined8 * FUN_00414d00(int *param_1,ulong *param_2);
undefined8 * FUN_00414e90(undefined8 uParm1,undefined8 *puParm2);
undefined8 * FUN_004150a0(undefined8 uParm1,int *piParm2,ulong *puParm3);
void FUN_00415210(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5,undefined8 *puParm6);
undefined8 *FUN_00415350(undefined8 *puParm1,char *pcParm2,undefined8 *puParm3,undefined8 *puParm4,uint **ppuParm5,uint **ppuParm6);
undefined8 FUN_00415900(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_00415d50(uint param_1);
void FUN_00415d90(uint uParm1);
ulong ** FUN_00415f90(ulong **ppuParm1,ulong **ppuParm2,ulong **ppuParm3,ulong **ppuParm4);
double FUN_0041ad20(int *piParm1);
void FUN_0041ad70(int *param_1);
long FUN_0041adf0(ulong uParm1,ulong uParm2);
long FUN_0041ae10(void);
undefined8 FUN_0041ae40(uint *puParm1,ulong *puParm2);
undefined8 FUN_0041b060(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_0041bd30(undefined auParm1 [16]);
ulong FUN_0041bd50(void);
void FUN_0041bd80(void);
undefined8 _DT_FINI(void);

