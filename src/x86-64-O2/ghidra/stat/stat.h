typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_004024a0(void);
void entry(void);
void FUN_00402980(void);
void FUN_004029f0(void);
void FUN_00402a80(void);
ulong FUN_00402ac0(uint uParm1);
void FUN_00402ad0(long lParm1,long lParm2);
void FUN_00402af0(void);
void FUN_00402b10(undefined auParm1 [16],undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8);
undefined8 FUN_00402b90(char cParm1,char cParm2,char cParm3);
void FUN_00402c50(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_00402d00(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00402d30(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00402d60(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00402d90(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00402dc0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00402df0(undefined8 uParm1,undefined8 uParm2);
undefined * FUN_00402e20(ulong *puParm1);
void FUN_004039b0(char cParm1);
ulong FUN_00403a80(char *pcParm1,uint uParm2,undefined8 uParm3,code *pcParm4,undefined8 uParm5);
ulong FUN_00403d80(char *pcParm1,undefined8 uParm2);
ulong FUN_00403e40(char *pcParm1,undefined8 uParm2,undefined8 uParm3);
undefined1 * FUN_00403f50(undefined8 uParm1);
char * FUN_00403f70(undefined8 uParm1);
char * thunk_FUN_004069b0(ulong uParm1,long lParm2);
undefined1 * FUN_00404180(undefined8 uParm1,ulong uParm2);
void FUN_00404240(undefined8 uParm1,undefined8 uParm2);
void FUN_00404250(void);
void thunkFUN__00404250(void);
undefined8 FUN_00404270(void);
undefined8 FUN_00404330(char *pcParm1,char *pcParm2,long lParm3,ulong uParm4);
void FUN_00404ad0(void);
void FUN_00404c70(uint uParm1);
long FUN_00404e00(undefined8 uParm1,undefined *puParm2);
long FUN_00405140(undefined8 uParm1,ulong uParm2);
long FUN_00405240(undefined8 uParm1,long *plParm2,long lParm3,long lParm4);
char * FUN_00405350(char **ppcParm1,undefined8 uParm2,undefined8 uParm3);
char * FUN_00405960(char *pcParm1);
void FUN_00405a10(char *pcParm1);
void FUN_00405a30(char *pcParm1);
undefined * FUN_00405a70(undefined8 uParm1);
char * FUN_00405ae0(char *pcParm1);
undefined8 * FUN_00405b40(long lParm1,undefined8 uParm2,undefined8 *puParm3);
ulong FUN_00405bd0(long lParm1,undefined8 uParm2,undefined8 *puParm3);
undefined * FUN_00405c10(long lParm1);
undefined8 FUN_00405ca0(undefined8 uParm1);
void FUN_00405e20(long lParm1,undefined *puParm2);
ulong FUN_00405e30(ulong uParm1);
ulong FUN_00405ea0(ulong uParm1);
ulong FUN_00405f10(long *plParm1,ulong uParm2);
undefined8 FUN_00405f40(float **ppfParm1);
ulong FUN_00405fc0(float fParm1,ulong uParm2,char cParm3);
void FUN_00406050(undefined8 *puParm1,undefined8 *puParm2);
long FUN_00406070(long lParm1,long lParm2,long **pplParm3,char cParm4);
void FUN_00406170(long *plParm1);
undefined8 FUN_00406190(long lParm1,long **pplParm2,char cParm3);
long FUN_004062d0(long lParm1,long lParm2);
long * FUN_00406330(undefined8 uParm1,undefined1 *puParm2,undefined *puParm3,undefined *puParm4,long lParm5);
void FUN_00406430(long **pplParm1);
ulong FUN_00406510(long *plParm1,undefined8 uParm2);
ulong FUN_00406690(ulong uParm1,undefined8 *puParm2,long *plParm3);
undefined8 FUN_004068e0(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00406920(undefined8 *puParm1,ulong uParm2);
void FUN_00406990(undefined8 *puParm1);
char * FUN_004069b0(ulong uParm1,long lParm2);
ulong FUN_00406a50(int iParm1,int iParm2);
long FUN_00406a90(long lParm1,long lParm2,long lParm3);
long FUN_00406ad0(long lParm1,long lParm2,long lParm3);
char * FUN_00406b10(char *pcParm1,long lParm2,char *pcParm3,uint *puParm4,char cParm5,undefined8 uParm6,undefined8 uParm7,uint uParm8);
void FUN_00408320(void);
int * FUN_00408340(long lParm1);
void FUN_004083f0(int *piParm1,undefined8 *puParm2,undefined8 uParm3);
ulong FUN_00408740(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8);
ulong FUN_00408800(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9);
ulong FUN_004088d0(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9,byte param_10);
undefined8 FUN_004089c0(byte *param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined * FUN_00408a70(char *pcParm1,int iParm2);
void FUN_00408b40(undefined *puParm1,undefined8 *puParm2,ulong uParm3,undefined8 *puParm4,ulong uParm5,uint uParm6,long lParm7,char *pcParm8,byte *pbParm9);
undefined1 * FUN_00409c30(undefined1 *puParm1,undefined8 *puParm2,undefined8 uParm3,uint *puParm4);
ulong FUN_00409dd0(undefined1 *puParm1);
void FUN_00409de0(undefined1 *puParm1,undefined4 uParm2);
void FUN_00409e60(ulong uParm1,undefined8 uParm2);
undefined1 * FUN_00409f20(undefined8 *puParm1);
ulong FUN_00409f30(int *piParm1);
ulong FUN_00409f80(uint *puParm1);
void FUN_00409fa0(int *piParm1);
ulong FUN_00409fc0(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long *plParm5,undefined8 uParm6);
void FUN_0040a210(void);
void FUN_0040a270(void);
void FUN_0040a300(long lParm1);
void FUN_0040a320(long lParm1);
long FUN_0040a330(long lParm1,long lParm2);
void FUN_0040a3a0(undefined8 uParm1);
long FUN_0040a3c0(void);
long FUN_0040a3f0(void);
void FUN_0040a420(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0040a4c0(ulong uParm1,ulong uParm2);
long FUN_0040a510(long lParm1);
void FUN_0040a530(undefined4 *puParm1);
void FUN_0040a540(void);
void FUN_0040a550(int iParm1);
undefined8 FUN_0040a590(uint *puParm1,undefined8 uParm2);
void FUN_0040a860(undefined8 uParm1);
ulong FUN_0040a8a0(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_0040a910(void);
void FUN_0040a930(void);
void FUN_0040a960(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
long FUN_0040aa40(long lParm1,ulong uParm2);
void FUN_0040aec0(long lParm1,int *piParm2);
ulong FUN_0040afa0(int param_1,undefined8 *param_2,char *param_3,char **param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_0040b530(char *pcParm1,int *piParm2,int iParm3);
ulong FUN_0040b5f0(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,uint *param_7,uint param_8);
void FUN_0040bab0(void);
void FUN_0040bb10(void);
void FUN_0040bb30(void);
undefined8 FUN_0040bb50(long lParm1,long lParm2);
void FUN_0040bbc0(long lParm1);
ulong FUN_0040bbe0(uint *puParm1,byte *pbParm2,long lParm3);
ulong * FUN_0040bc50(ulong *puParm1,undefined8 uParm2,ulong uParm3);
void FUN_0040bd30(char *pcParm1);
undefined8 FUN_0040bdc0(void);
void FUN_0040bdd0(undefined8 *puParm1);
byte * FUN_0040be20(void);
void FUN_0040c5d0(void);
ulong FUN_0040c5e0(uint param_1,undefined8 param_2,uint param_3,ulong param_4);
undefined8 FUN_0040c6e0(long lParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040c750(long lParm1,long lParm2);
void FUN_0040c790(void);
undefined8 FUN_0040c7b0(long lParm1,long lParm2);
ulong FUN_0040c810(long lParm1,ulong uParm2,long *plParm3);
long FUN_0040c960(long lParm1,long lParm2,long lParm3,ulong uParm4);
char * FUN_0040cea0(char *pcParm1,char *pcParm2);
ulong FUN_0040cf80(uint uParm1,uint uParm2);
ulong FUN_0040cfa0(uint *puParm1,uint *puParm2);
void FUN_0040cff0(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_0040d000(void);
ulong FUN_0040d010(char *pcParm1);
ulong FUN_0040d040(long lParm1);
undefined8 * FUN_0040d080(long lParm1);
undefined8 FUN_0040d120(long *plParm1,char *pcParm2);
void FUN_0040d260(long *plParm1);
long FUN_0040d290(long lParm1);
ulong FUN_0040d340(long lParm1);
undefined8 FUN_0040d390(long lParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_0040d430(long lParm1,undefined8 uParm2);
long FUN_0040d510(uint *puParm1);
void FUN_0040d530(void);
ulong FUN_0040d610(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined FUN_0040d6f0(int iParm1);;
ulong FUN_0040d700(uint uParm1);
ulong FUN_0040d710(byte *pbParm1,byte *pbParm2);
ulong FUN_0040d760(ulong uParm1,char cParm2);
void FUN_0040d840(undefined8 param_1,byte param_2,ulong param_3);
undefined8 FUN_0040d890(void);
ulong FUN_0040d8a0(ulong uParm1);
ulong FUN_0040d940(char *pcParm1,ulong uParm2);
undefined * FUN_0040d980(void);
char * FUN_0040dce0(void);
ulong FUN_0040dda0(uint uParm1);
undefined * FUN_0040dde0(long lParm1,uint *puParm2);
undefined8 FUN_0040df10(char *pcParm1,undefined8 uParm2);
void FUN_0040df90(undefined8 uParm1);
ulong FUN_0040e020(ulong param_1,undefined8 param_2,ulong param_3);
undefined8 FUN_0040e170(ulong uParm1);
void FUN_0040e1e0(undefined8 uParm1);
ulong FUN_0040e1f0(long lParm1);
undefined8 FUN_0040e280(void);
void FUN_0040e2a0(void);
ulong FUN_0040e2c0(void);
ulong FUN_0040e2e0(ulong uParm1,long lParm2,long lParm3,ulong uParm4);
ulong FUN_0040e3a0(long *plParm1,ulong *puParm2,long lParm3);
long FUN_0040e3b0(long lParm1,undefined4 uParm2);
ulong FUN_0040e3c0(ulong uParm1);
ulong FUN_0040e440(uint uParm1,uint uParm2);
long FUN_0040e460(ulong param_1,long param_2,undefined8 param_3,int param_4,int param_5,uint param_6,int param_7,int param_8,int param_9,int param_10);
long FUN_0040e5b0(undefined8 uParm1,undefined8 uParm2);
long FUN_0040e5e0(void);
void FUN_0040e690(code *pcParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
long FUN_0040e6b0(code *pcParm1,long *plParm2,undefined8 uParm3,undefined8 uParm4);
long FUN_0040eb10(uint *puParm1);
ulong FUN_0040eb30(char *pcParm1,char *pcParm2,ulong uParm3);
ulong FUN_0040ec60(char *pcParm1,long lParm2);
char * FUN_0040eca0(ulong uParm1,long lParm2,long lParm3);
ulong FUN_0040ee50(void);
void FUN_0040ee90(undefined8 param_1,undefined8 *param_2);
int * FUN_0040f100(int *param_1,ulong *param_2);
ulong FUN_0040f290(undefined8 uParm1,undefined8 *puParm2);
uint * FUN_0040f4a0(undefined8 uParm1,int *piParm2,ulong *puParm3);
void FUN_0040f610(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5,undefined8 *puParm6);
int * FUN_0040f750(ulong uParm1,long lParm2,ulong uParm3,undefined8 *puParm4,ulong *puParm5,uint **ppuParm6);
undefined8 FUN_0040fd00(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_00410150(uint param_1);
void FUN_00410190(uint uParm1);
ulong ** FUN_00410390(ulong **ppuParm1,ulong **ppuParm2,ulong **ppuParm3,ulong **ppuParm4);
ulong FUN_00415120(long *plParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 * FUN_00415180(ulong uParm1);
void FUN_00415200(ulong uParm1);
double FUN_00415290(int *piParm1);
void FUN_004152e0(int *param_1);
ulong FUN_00415360(ulong uParm1);
long FUN_00415370(ulong uParm1,ulong uParm2);
long FUN_00415390(void);
ulong FUN_004153c0(long *plParm1,ulong *puParm2,int iParm3,long lParm4);
undefined8 FUN_00415510(uint *puParm1,ulong *puParm2);
undefined8 FUN_00415730(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_00416400(undefined auParm1 [16]);
ulong FUN_00416420(void);
void FUN_00416450(void);
undefined8 _DT_FINI(void);

