typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined3;
typedef unsigned int    undefined4;
typedef unsigned long    undefined5;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00403850(void);
undefined * FUN_004038e0(ulong uParm1,undefined8 *puParm2);
void entry(void);
void FUN_00404ad0(void);
void FUN_00404b40(void);
void FUN_00404bd0(void);
ulong FUN_00404c10(uint uParm1);
undefined8 FUN_00404c20(undefined8 uParm1);
void FUN_00404c50(undefined8 uParm1);
ulong FUN_00404cb0(byte **ppbParm1);
ulong FUN_00404d80(char *pcParm1);
ulong FUN_00404dd0(char *pcParm1,char **ppcParm2);
undefined8 FUN_00404eb0(long lParm1);
ulong FUN_00404ed0(long lParm1);
void FUN_00404f20(long lParm1,undefined *puParm2);
void FUN_00405000(char *pcParm1,long lParm2,uint uParm3);
void FUN_004050f0(void);
void FUN_00405110(void);
void FUN_00405140(void);
void FUN_00405160(void);
void FUN_004051d0(undefined8 uParm1,undefined8 uParm2);
void FUN_00405200(long lParm1);
void FUN_00405220(char *pcParm1);
ulong FUN_004052d0(byte bParm1);
void FUN_004052f0(void);
undefined4 * FUN_00405450(undefined4 *puParm1);
ulong FUN_00405480(undefined8 uParm1,undefined8 *puParm2,long lParm3);
void FUN_00405520(uint uParm1,ulong uParm2,undefined8 uParm3);
void FUN_00405660(uint uParm1,char cParm2,undefined8 uParm3);
long FUN_004057f0(ulong uParm1,char cParm2,undefined8 uParm3);
long FUN_00405880(char *pcParm1,char *pcParm2);
void FUN_00405980(undefined8 uParm1,undefined *puParm2);
void FUN_004059d0(undefined8 uParm1);
void FUN_004059f0(undefined8 uParm1,undefined8 uParm2);
void FUN_00405a70(undefined8 uParm1);
void FUN_00405ab0(undefined *puParm1,char cParm2);
long FUN_00406010(char *pcParm1);
long FUN_00406090(long *plParm1,char *pcParm2,char *pcParm3);
long FUN_00406110(char *pcParm1,char *pcParm2);
void FUN_00406150(long lParm1,long lParm2);
void FUN_004061c0(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00406200(long lParm1,long lParm2);
ulong FUN_004062e0(byte *pbParm1,byte *pbParm2);
ulong FUN_00406340(byte *pbParm1,byte *pbParm2);
ulong FUN_004063d0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00406460(undefined *puParm1,ulong uParm2,undefined *puParm3,ulong uParm4);
void FUN_004069e0(char **ppcParm1,long lParm2);
void FUN_00406a40(uint uParm1,uint uParm2);
ulong FUN_00406a60(long lParm1);
undefined8 FUN_00406aa0(undefined4 uParm1);
ulong FUN_00406ae0(ulong uParm1);
ulong FUN_00406ba0(ulong uParm1);
void FUN_00406bc0(long lParm1);
void FUN_00406ca0(void);
void FUN_00406ce0(void);
void FUN_00406d00(void);
ulong FUN_00406d30(uint *puParm1,long lParm2);
void FUN_00406e90(ulong uParm1,uint *puParm2);
ulong FUN_00406f50(long lParm1);
ulong FUN_00406fb0(long lParm1);
long FUN_00407130(undefined8 *puParm1,long lParm2,long *plParm3);
undefined8 * FUN_004071c0(uint *puParm1,char cParm2);
long FUN_00407320(long *plParm1,ulong uParm2);
long FUN_00407410(long *plParm1);
ulong FUN_00407420(void);
long * FUN_00407580(long *plParm1,long *plParm2,long lParm3,ulong uParm4,long lParm5,char cParm6);
undefined8 * FUN_004076b0(long lParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00407750(undefined8 *puParm1,long lParm2);
void FUN_00407780(long lParm1);
void FUN_00407790(undefined8 *puParm1,long lParm2);
void FUN_004077d0(undefined8 *puParm1,long *plParm2);
void FUN_00407810(long lParm1);
void FUN_00407820(undefined8 *puParm1,long lParm2);
long FUN_00407880(undefined8 *puParm1);
long FUN_004078e0(long lParm1,long lParm2);
char * FUN_004078f0(char *pcParm1,long lParm2,long *plParm3);
char * FUN_00407a30(char *pcParm1,long lParm2,long lParm3);
ulong FUN_00407bb0(char **ppcParm1,char **ppcParm2);
ulong FUN_00408280(undefined8 *puParm1,undefined8 *puParm2);
void FUN_00408430(undefined4 *puParm1,ulong uParm2,undefined4 *puParm3,char cParm4);
undefined8 FUN_004085c0(char **ppcParm1,undefined8 uParm2,long lParm3);
void FUN_00408900(byte *pbParm1,long lParm2,long *plParm3);
undefined8 FUN_00408a90(byte **ppbParm1);
char * FUN_00408af0(char **ppcParm1,long lParm2,long lParm3);
char * FUN_00408bc0(char **ppcParm1,long lParm2,long lParm3);
void FUN_00408c30(long *plParm1,ulong uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_004090e0(undefined8 *puParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
undefined8 FUN_00409160(long param_1,ulong param_2,long param_3,long *param_4,undefined8 param_5,undefined8 param_6,undefined8 param_7);
undefined8 FUN_00409370(undefined8 uParm1,char cParm2);
void FUN_004095a0(long lParm1,ulong uParm2,ulong uParm3,undefined8 uParm4,undefined8 uParm5,long lParm6);
ulong FUN_00409d00(long lParm1,undefined8 uParm2,ulong uParm3,long lParm4,undefined8 uParm5);
void FUN_00409d80(long lParm1,ulong uParm2,ulong uParm3,long lParm4);
long FUN_00409ee0(long *plParm1,ulong uParm2,ulong uParm3,ulong uParm4,undefined8 uParm5,long *plParm6);
long FUN_0040a200(undefined8 *puParm1,long lParm2,long lParm3,ulong uParm4);
void FUN_0040a390(undefined8 *puParm1,ulong uParm2,long lParm3,ulong uParm4);
void FUN_0040a6a0(void);
void FUN_0040a840(void);
void FUN_0040a850(uint uParm1);
long FUN_0040aa70(undefined8 uParm1,long *plParm2,long lParm3,long lParm4);
void FUN_0040ab80(undefined auParm1 [16],undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined auParm8 [16],undefined8 uParm9,undefined8 uParm10,long lParm11,undefined8 uParm12,undefined8 uParm13,undefined8 uParm14);
void FUN_0040abe0(long *plParm1,long lParm2,long lParm3);
long FUN_0040acc0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
void FUN_0040add0(undefined4 *puParm1,undefined4 uParm2);
void FUN_0040ade0(undefined8 *puParm1);
void FUN_0040ae50(int *piParm1,ulong uParm2,int *piParm3);
void FUN_0040b5c0(uint *puParm1,undefined8 uParm2);
void FUN_0040b6e0(undefined8 *puParm1,ulong uParm2,long lParm3);
void FUN_0040ba00(long lParm1,ulong uParm2);
char * FUN_0040ba30(char **ppcParm1);
ulong FUN_0040bb00(byte bParm1);
ulong FUN_0040bb60(long lParm1,ulong uParm2,long lParm3,ulong uParm4);
ulong FUN_0040bd20(char *pcParm1,char *pcParm2);
ulong FUN_0040bee0(ulong uParm1);
ulong FUN_0040bf80(ulong uParm1);
ulong FUN_0040bff0(ulong uParm1);
undefined * FUN_0040c060(long *plParm1,ulong uParm2);
undefined8 FUN_0040c090(float **ppfParm1);
ulong FUN_0040c110(float fParm1,ulong uParm2,char cParm3);
void FUN_0040c1a0(undefined8 *puParm1,undefined8 *puParm2);
long FUN_0040c1c0(long lParm1,long lParm2,long **pplParm3,char cParm4);
void FUN_0040c2c0(long *plParm1);
undefined8 FUN_0040c2e0(long lParm1,long **pplParm2,char cParm3);
long * FUN_0040c420(undefined8 uParm1,undefined1 *puParm2,undefined *puParm3,undefined *puParm4,long lParm5);
undefined * FUN_0040c520(long *plParm1,undefined8 uParm2);
undefined *FUN_0040c6a0(ulong uParm1,undefined8 *puParm2,long *plParm3,undefined8 uParm4,undefined8 uParm5,long *plParm6);
undefined8 FUN_0040c8f0(undefined8 uParm1,undefined8 uParm2);
long FUN_0040c930(long lParm1,undefined8 uParm2);
void FUN_0040cb20(long lParm1,ulong uParm2,code *pcParm3);
ulong FUN_0040cba0(long lParm1,ulong uParm2,code *pcParm3);
undefined8 * FUN_0040cc80(undefined *puParm1,long lParm2);
undefined8 FUN_0040ccf0(long *plParm1,undefined8 uParm2);
undefined8 FUN_0040cd60(long *plParm1);
char * FUN_0040cda0(uint uParm1,long lParm2);
char * FUN_0040ce30(ulong uParm1,long lParm2);
char * FUN_0040ce70(ulong uParm1,long lParm2);
ulong FUN_0040cec0(byte *pbParm1,long lParm2,ulong uParm3);
undefined8 FUN_0040d0a0(char *pcParm1);
long FUN_0040d190(void);
long FUN_0040d1e0(int iParm1);
ulong FUN_0040d230(undefined8 uParm1);
void FUN_0040d2b0(void);
void FUN_0040d370(void);
ulong FUN_0040d470(undefined8 *puParm1,uint uParm2);
ulong FUN_0040d640(void);
int * FUN_0040d6c0(long lParm1);
int * FUN_0040d770(int *piParm1,undefined8 *puParm2);
ulong FUN_0040dac0(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8);
ulong FUN_0040db80(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9);
ulong FUN_0040dc50(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9,byte param_10);
undefined8 FUN_0040dd40(byte *param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined * FUN_0040ddf0(char *pcParm1,int iParm2);
undefined8 *FUN_0040dec0(undefined *puParm1,undefined8 *puParm2,long lParm3,undefined *puParm4,ulong uParm5,long *plParm6,long lParm7,char *pcParm8,char *pcParm9);
undefined1 *FUN_0040efb0(undefined1 *puParm1,undefined8 *puParm2,undefined8 uParm3,uint *puParm4,undefined8 uParm5,long *plParm6);
ulong FUN_0040f150(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
void FUN_0040f190(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040f1c0(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_0040f200(ulong uParm1,undefined8 uParm2);
void FUN_0040f2a0(uint uParm1,undefined8 uParm2,undefined8 uParm3);
undefined1 * FUN_0040f330(undefined1 *puParm1,undefined8 *puParm2);
undefined1 * FUN_0040f340(undefined8 *puParm1);
void FUN_0040f350(undefined8 uParm1,undefined8 uParm2);
void FUN_0040f570(undefined4 *puParm1,ulong uParm2);
undefined8 * FUN_0040f780(long lParm1,ulong uParm2);
void FUN_0040f830(long *plParm1,ulong uParm2,ulong uParm3);
undefined8 FUN_0040f850(long *plParm1);
undefined8 FUN_0040f8a0(undefined8 uParm1);
undefined8 FUN_0040f8b0(long lParm1,undefined8 uParm2);
void FUN_0040f8c0(long *plParm1,long *plParm2);
void FUN_0040fb90(ulong *puParm1);
void FUN_0040fe40(long *plParm1);
undefined8 FUN_0040ff30(undefined8 *puParm1);
ulong FUN_0040ffa0(undefined8 uParm1,long lParm2);
ulong FUN_004101b0(undefined8 uParm1,ulong uParm2);
void FUN_004106d0(undefined8 uParm1,long lParm2,long lParm3,long lParm4,long lParm5);
void FUN_00410920(void);
void FUN_00410980(void);
void FUN_00410a10(ulong uParm1,ulong uParm2);
void FUN_00410a30(ulong uParm1,ulong uParm2);
void FUN_00410a60(ulong uParm1,ulong uParm2);
long FUN_00410a70(long lParm1,ulong *puParm2);
long FUN_00410ab0(long lParm1,ulong *puParm2,ulong uParm3);
void FUN_00410b40(ulong uParm1,ulong uParm2);
void FUN_00410b70(undefined8 uParm1,undefined8 uParm2);
void FUN_00410ba0(undefined auParm1 [16],undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined auParm7 [16]);
void FUN_00410bd0(undefined auParm1 [16],undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined auParm7 [16],ulong uParm8,undefined8 uParm9,undefined8 uParm10,undefined8 uParm11,undefined8 uParm12,undefined8 uParm13);
ulong FUN_00410c60(undefined8 uParm1,long lParm2,undefined8 uParm3,long lParm4);
undefined8 FUN_00410cc0(void);
ulong FUN_00410d20(int iParm1);
ulong FUN_00410d40(ulong *puParm1,int iParm2);
ulong FUN_00410d70(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_00410da0(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined *FUN_004110e0(int iParm1,undefined8 *puParm2,undefined uParm3,long lParm4,undefined8 uParm5,long *plParm6);
ulong FUN_00411150(int iParm1,undefined8 uParm2,char cParm3,undefined8 uParm4,undefined8 uParm5);
ulong FUN_00411170(int iParm1);
ulong FUN_00411190(ulong *puParm1,int iParm2);
ulong FUN_004111c0(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_004111f0(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_00411530(ulong uParm1,ulong uParm2);
void FUN_00411580(undefined8 uParm1);
ulong FUN_004115c0(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00411630(void);
void FUN_00411650(void);
void FUN_00411680(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00411760(undefined8 uParm1,undefined8 uParm2);
void FUN_00411780(undefined8 uParm1);
ulong FUN_00411810(ulong param_1,undefined8 param_2,ulong param_3);
void FUN_00411960(undefined8 uParm1);
ulong FUN_00411970(long lParm1);
undefined8 FUN_00411a00(void);
void FUN_00411a20(long lParm1,int *piParm2);
ulong FUN_00411b00(int param_1,undefined8 *param_2,char *param_3,char **param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_00412090(char *pcParm1,int *piParm2,int iParm3);
ulong FUN_00412150(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,uint *param_7,uint param_8);
void FUN_00412610(void);
void FUN_00412670(void);
void FUN_00412690(void);
void FUN_00412700(long lParm1);
ulong FUN_00412720(uint *puParm1,byte *pbParm2,long lParm3);
ulong * FUN_00412790(ulong *puParm1,undefined8 uParm2,ulong uParm3);
undefined8 FUN_00412870(long *plParm1,long *plParm2);
void FUN_00412920(long lParm1,undefined8 uParm2);
void FUN_00412940(long lParm1,undefined8 uParm2);
undefined8 FUN_00412a20(long *plParm1,undefined *puParm2,long lParm3,long lParm4,long lParm5);
void FUN_00412a40(ulong *puParm1,ulong uParm2);
void FUN_00412b30(long lParm1,long lParm2);
void FUN_00412b70(void);
undefined8 FUN_00412b90(long lParm1,long lParm2);
undefined8 FUN_00412bf0(long lParm1);
ulong FUN_00412cc0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00412f50(long param_1,char param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined8 FUN_00412fb0(char *pcParm1);
ulong FUN_00412ff0(ulong uParm1);
ulong FUN_00413050(int iParm1);
undefined FUN_00413080(int iParm1);;
undefined FUN_004130a0(int iParm1);;
undefined FUN_004130b0(int iParm1);;
undefined FUN_004130d0(int iParm1);;
ulong FUN_004130e0(uint uParm1);
ulong FUN_004130f0(byte *pbParm1,byte *pbParm2);
void FUN_004131b0(double dParm1);
ulong FUN_004132c0(ulong uParm1,ulong uParm2);
long FUN_004132e0(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00413380(void);
undefined * FUN_00413390(void);
char * FUN_004136f0(void);
ulong FUN_00413860(long lParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_004138e0(ulong uParm1);
ulong FUN_00413aa0(long param_1,long param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10);
undefined8 FUN_00413b10(char *param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8);
ulong FUN_00413b50(undefined8 uParm1);
void FUN_00413ea0(void);
undefined8 FUN_00413ec0(long lParm1,long lParm2);
ulong FUN_00413f30(char *pcParm1,long lParm2);
char * FUN_00413f70(ulong uParm1,long lParm2,long lParm3);
ulong FUN_00414120(void);
undefined * FUN_00414160(undefined8 param_1,undefined8 *param_2);
int * FUN_004143d0(int *param_1,ulong *param_2);
undefined * FUN_00414560(undefined8 uParm1,undefined8 *puParm2);
uint * FUN_00414770(undefined auParm1 [16],int *piParm2,ulong *puParm3);
void FUN_004148e0(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5,undefined8 *puParm6);
int * FUN_00414a20(uint **ppuParm1,long lParm2,uint **ppuParm3,uint **ppuParm4,long **pplParm5,uint **ppuParm6);
undefined8 FUN_00414fd0(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_00415420(uint param_1);
void FUN_00415460(uint uParm1);
long ** FUN_00415660(long **pplParm1,long **pplParm2,long **pplParm3,long **pplParm4,undefined8 uParm5,long **pplParm6);
double FUN_0041a3f0(int *piParm1);
void FUN_0041a440(int *param_1);
long FUN_0041a4c0(ulong uParm1,ulong uParm2);
long FUN_0041a4e0(void);
undefined8 FUN_0041a510(uint *puParm1,ulong *puParm2);
undefined8 FUN_0041a730(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_0041b400(undefined auParm1 [16]);
ulong FUN_0041b420(void);
void FUN_0041b450(void);
undefined8 _DT_FINI(void);

