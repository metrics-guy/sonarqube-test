typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00402540(void);
void entry(void);
void FUN_00402e00(void);
void FUN_00402e70(void);
void FUN_00402f00(void);
undefined8 FUN_00402f40(long lParm1,long lParm2,long lParm3,char cParm4);
void FUN_00402fa0(void);
void FUN_00402fc0(void);
void FUN_00402fe0(long *plParm1,int iParm2,undefined uParm3);
long FUN_00403080(undefined8 uParm1,char cParm2);
ulong FUN_004030a0(undefined8 uParm1,uint uParm2,ulong **ppuParm3);
ulong FUN_004031c0(undefined8 *puParm1,ulong uParm2);
ulong FUN_004031d0(undefined8 uParm1,char cParm2,ulong uParm3,long lParm4,long *plParm5);
undefined8 FUN_00403360(undefined8 uParm1,long lParm2,long lParm3,long lParm4,char cParm5);
undefined8 FUN_004033e0(undefined8 uParm1,long lParm2,long lParm3,undefined8 uParm4);
undefined8 FUN_00403460(long lParm1,long lParm2,long lParm3);
undefined8 FUN_004034e0(long lParm1,long lParm2,long lParm3);
undefined FUN_00403560(undefined8 uParm1);;
long FUN_00403570(void);
void FUN_004035d0(void);
void FUN_00403770(uint uParm1);
void FUN_004038f0(long lParm1,ulong uParm2);
undefined8 FUN_00403920(uint uParm1);
long FUN_00403980(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00403b60(undefined4 *puParm1);
long * FUN_00403b70(long *plParm1,undefined8 uParm2,char cParm3);
void FUN_00403c90(void);
int * FUN_00403ca0(long lParm1);
int * FUN_00403d50(int *piParm1,undefined8 *puParm2);
ulong FUN_004040a0(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8);
ulong FUN_00404160(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9);
ulong FUN_00404230(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9,byte param_10);
undefined8 FUN_00404320(byte *param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined * FUN_004043d0(char *pcParm1,int iParm2);
undefined8 *FUN_004044a0(undefined *puParm1,undefined8 *puParm2,long lParm3,undefined *puParm4,ulong uParm5,uint uParm6,long lParm7,char *pcParm8,char *pcParm9);
undefined1 * FUN_00405590(undefined1 *puParm1,undefined8 *puParm2,undefined8 uParm3,uint *puParm4);
ulong FUN_00405730(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
void FUN_00405800(uint uParm1,undefined8 uParm2,undefined8 uParm3);
undefined1 * FUN_004058a0(undefined8 *puParm1);
long FUN_004058b0(long lParm1);
void FUN_004058c0(undefined8 uParm1);
long * FUN_004058f0(void);
long FUN_00405a30(long lParm1);
void FUN_00405a60(long lParm1,long lParm2,long lParm3);
long * FUN_00405aa0(undefined8 uParm1);
ulong FUN_00405ac0(undefined8 *puParm1,ulong uParm2);
void FUN_00405ad0(long **pplParm1,long lParm2,long lParm3,long lParm4);
void thunk_FUN_004091f0(long **pplParm1);
ulong FUN_00405bd0(undefined8 uParm1,undefined8 uParm2);
long * FUN_00405bf0(undefined8 uParm1,ulong *puParm2,ulong uParm3);
void FUN_00405d80(undefined8 uParm1,undefined8 uParm2);
void FUN_00405fa0(undefined4 *puParm1,ulong uParm2);
undefined8 * FUN_004061b0(long lParm1,ulong uParm2);
void FUN_00406260(long *plParm1,ulong uParm2,ulong uParm3);
undefined8 FUN_00406280(undefined8 uParm1);
undefined8 FUN_00406290(long lParm1,undefined8 uParm2);
void FUN_004062a0(long *plParm1,long *plParm2);
void FUN_00406570(ulong *puParm1);
long FUN_00406820(undefined8 uParm1,ulong *puParm2);
void FUN_004069c0(undefined8 uParm1,long lParm2,long lParm3,long lParm4,long lParm5);
void FUN_00406c10(void);
void FUN_00406c70(void);
void FUN_00406d00(ulong uParm1,ulong uParm2);
void FUN_00406d20(ulong uParm1,ulong uParm2);
void FUN_00406d50(ulong uParm1,ulong uParm2);
long FUN_00406d60(long lParm1,ulong *puParm2);
long FUN_00406da0(long lParm1,ulong *puParm2,ulong uParm3);
long FUN_00406e60(long lParm1,ulong *puParm2);
undefined * FUN_00406e70(ulong uParm1,ulong uParm2);
undefined * FUN_00406ea0(void);
ulong FUN_00406ed0(undefined8 param_1,ulong param_2,ulong param_3,ulong param_4,undefined8 param_5,undefined8 param_6,uint param_7);
void FUN_00406fa0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_00406fd0(int iParm1);
ulong FUN_00406ff0(ulong *puParm1,int iParm2);
ulong FUN_00407020(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_00407050(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
ulong FUN_00407390(int iParm1);
ulong FUN_004073b0(ulong *puParm1,int iParm2);
ulong FUN_004073e0(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_00407410(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_00407750(ulong uParm1,ulong uParm2);
void FUN_004077a0(undefined8 uParm1);
ulong FUN_004077e0(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00407850(void);
void FUN_00407870(void);
void FUN_004078a0(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00407980(undefined8 uParm1);
void FUN_00407a10(undefined8 uParm1);
ulong FUN_00407a20(long lParm1);
undefined8 FUN_00407ab0(void);
void FUN_00407ad0(long lParm1,int *piParm2);
ulong FUN_00407bb0(int param_1,undefined8 *param_2,char *param_3,char **param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_00408140(char *pcParm1,int *piParm2,int iParm3);
ulong FUN_00408200(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,uint *param_7,uint param_8);
void FUN_004086c0(void);
void FUN_00408720(void);
void FUN_00408740(void);
void FUN_004087b0(long lParm1);
ulong FUN_004087d0(uint *puParm1,byte *pbParm2,long lParm3);
ulong * FUN_00408840(ulong *puParm1,undefined8 uParm2,ulong uParm3);
void FUN_00408920(long lParm1,long lParm2);
ulong FUN_00408960(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined FUN_00408a20(int iParm1);;
ulong FUN_00408a30(uint uParm1);
ulong FUN_00408a40(byte *pbParm1,byte *pbParm2);
long FUN_00408b00(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00408ba0(void);
ulong FUN_00408bb0(ulong uParm1);
ulong FUN_00408c50(ulong uParm1);
ulong FUN_00408cc0(ulong uParm1);
undefined * FUN_00408d30(long *plParm1,ulong uParm2);
undefined8 FUN_00408d60(float **ppfParm1);
ulong FUN_00408de0(float fParm1,ulong uParm2,char cParm3);
void FUN_00408e70(undefined8 *puParm1,undefined8 *puParm2);
long FUN_00408e90(long lParm1,long lParm2,long **pplParm3,char cParm4);
void FUN_00408f90(long *plParm1);
undefined8 FUN_00408fb0(long lParm1,long **pplParm2,char cParm3);
void FUN_004091f0(long **pplParm1);
undefined * FUN_004092d0(long *plParm1,undefined8 uParm2);
undefined * FUN_00409450(ulong uParm1,undefined8 *puParm2,long *plParm3);
undefined8 FUN_004096a0(undefined8 uParm1,undefined8 uParm2);
long FUN_004096e0(long lParm1,undefined8 uParm2);
undefined * FUN_004098c0(void);
char * FUN_00409c20(void);
ulong FUN_00409ce0(ulong uParm1);
ulong FUN_00409cf0(ulong param_1,undefined8 param_2,ulong param_3);
ulong FUN_00409e40(char *pcParm1,long lParm2);
char * FUN_00409e80(ulong uParm1,long lParm2,long lParm3);
ulong FUN_0040a030(void);
undefined * FUN_0040a070(undefined8 param_1,undefined8 *param_2);
int * FUN_0040a2e0(int *param_1,ulong *param_2);
undefined * FUN_0040a470(undefined8 uParm1,undefined8 *puParm2);
uint * FUN_0040a680(undefined8 uParm1,int *piParm2,ulong *puParm3);
void FUN_0040a7f0(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5,undefined8 *puParm6);
int * FUN_0040a930(undefined8 *puParm1,long lParm2,undefined8 *puParm3,undefined8 *puParm4,uint **ppuParm5,uint **ppuParm6);
undefined8 FUN_0040aee0(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_0040b330(uint param_1);
void FUN_0040b370(uint uParm1);
ulong ** FUN_0040b570(ulong **ppuParm1,ulong **ppuParm2,ulong **ppuParm3,ulong **ppuParm4);
double FUN_00410320(int *piParm1);
void FUN_00410370(int *param_1);
long FUN_004103f0(ulong uParm1,ulong uParm2);
long FUN_00410410(void);
undefined8 FUN_00410440(uint *puParm1,ulong *puParm2);
undefined8 FUN_00410660(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_00411330(undefined auParm1 [16]);
ulong FUN_00411350(void);
void FUN_00411380(void);
undefined8 _DT_FINI(void);

