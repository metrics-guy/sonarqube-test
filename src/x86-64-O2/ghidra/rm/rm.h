typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined3;
typedef unsigned int    undefined4;
typedef unsigned long    undefined5;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00402540(void);
void entry(void);
void FUN_00402990(void);
void FUN_00402a00(void);
void FUN_00402a90(void);
undefined8 FUN_00402ad0(undefined8 uParm1);
void FUN_00402ae0(undefined *puParm1);
void FUN_00402b20(int iParm1,undefined8 *puParm2);
void FUN_00402bd0(void);
void FUN_00402da0(uint uParm1);
ulong FUN_00402ec0(char *pcParm1);
long FUN_00402ef0(long lParm1);
void FUN_00402f40(long lParm1);
long FUN_00402f70(undefined8 uParm1);
ulong FUN_00402fb0(undefined8 uParm1,undefined8 uParm2);
undefined8 * FUN_00403020(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00403040(char cParm1,int iParm2);
undefined8 FUN_00403060(long lParm1,long lParm2,byte *pbParm3,char cParm4);
undefined8 FUN_004031f0(undefined8 uParm1,undefined8 uParm2,long lParm3);
undefined8 FUN_00403260(ulong uParm1,undefined8 uParm2,long lParm3);
undefined8 FUN_004032e0(long lParm1,long lParm2,char cParm3,char *pcParm4,int iParm5,int *piParm6);
ulong FUN_00403630(long lParm1,long lParm2,long lParm3);
ulong FUN_00403a10(long *plParm1,long lParm2);
long FUN_00403b70(undefined8 uParm1,long *plParm2,long lParm3,long lParm4);
void FUN_00403c80(undefined auParm1 [16],undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined auParm8 [16],undefined8 uParm9,undefined8 uParm10,long lParm11,undefined8 uParm12,undefined8 uParm13,undefined8 uParm14);
void FUN_00403ce0(long *plParm1,long lParm2,long lParm3);
long FUN_00403dc0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
char * FUN_00403fb0(char *pcParm1);
undefined * FUN_00404010(long lParm1);
undefined8 * FUN_004040a0(long lParm1);
undefined8 * FUN_00404150(int *piParm1,undefined8 *puParm2);
ulong FUN_004044a0(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8);
ulong FUN_00404560(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9);
ulong FUN_00404630(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9,byte param_10);
undefined8 FUN_00404720(byte *param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined * FUN_004047d0(char *pcParm1,int iParm2);
undefined8 *FUN_004048a0(undefined *puParm1,undefined8 *puParm2,long lParm3,undefined *puParm4,ulong uParm5,uint uParm6,long lParm7,char *pcParm8,char *pcParm9);
undefined8 * FUN_00405990(undefined1 *puParm1,undefined8 *puParm2,undefined8 uParm3,uint *puParm4);
ulong FUN_00405b30(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
void FUN_00405b70(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00405ba0(ulong uParm1,undefined8 uParm2);
void FUN_00405c40(uint uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 * FUN_00405cd0(undefined1 *puParm1,undefined8 *puParm2);
undefined8 * FUN_00405ce0(undefined8 *puParm1);
undefined8 * FUN_00405cf0(undefined8 *puParm1);
ulong FUN_00405d40(ulong uParm1,long lParm2,long lParm3);
void FUN_00405d50(undefined8 uParm1,long lParm2,long lParm3,long lParm4,long lParm5);
void FUN_00405fa0(void);
void FUN_00406000(void);
ulong FUN_00406090(void);
void FUN_004060d0(long lParm1);
void FUN_004060f0(long lParm1);
long FUN_00406100(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
void FUN_00406140(void);
void FUN_00406170(undefined8 uParm1,uint uParm2);
ulong FUN_004061c0(void);
undefined8 FUN_00406200(ulong uParm1,ulong uParm2);
void FUN_00406250(undefined8 uParm1);
ulong FUN_00406290(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00406300(void);
void FUN_00406320(void);
void FUN_00406350(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00406430(void);
ulong FUN_00406440(ulong uParm1,long lParm2);
undefined8 FUN_004064c0(ulong uParm1);
void FUN_00406530(undefined8 uParm1);
ulong FUN_00406540(long lParm1);
undefined8 FUN_004065d0(void);
void FUN_004065f0(void);
ulong FUN_00406610(void);
ulong FUN_00406630(ulong uParm1,long lParm2,long lParm3,ulong uParm4);
long FUN_00406770(long *plParm1);
undefined8 * FUN_004067c0(ulong uParm1,undefined8 *puParm2);
ulong FUN_004067f0(long lParm1,long lParm2,char cParm3);
long FUN_00406990(long lParm1,long lParm2,ulong uParm3);
long FUN_00406a90(long lParm1,undefined8 uParm2,long lParm3);
void FUN_00406b30(long lParm1);
void FUN_00406b80(undefined8 uParm1);
long FUN_00406bc0(undefined8 uParm1,undefined8 uParm2,uint uParm3,uint *puParm4);
undefined8 * FUN_00406c20(long lParm1);
ulong FUN_00406d30(void);
ulong FUN_00406d60(void);
undefined8 * FUN_00406dd0(ulong uParm1,ulong uParm2,char cParm3);
ulong FUN_00406e40(long lParm1);
void FUN_00406eb0(undefined4 *puParm1,int iParm2);
void thunk_FUN_00406efb(long lParm1,long lParm2,long lParm3);
void FUN_00406efb(long lParm1,long lParm2,long lParm3);
undefined8 FUN_00406f50(long *plParm1,ulong *puParm2,long lParm3);
ulong FUN_00406fc0(ulong uParm1,ulong *puParm2);
void FUN_00407020(uint param_1,uint param_2,undefined8 param_3,ulong param_4);
ulong FUN_00407070(long lParm1,long lParm2,uint uParm3,char *pcParm4);
void FUN_00407260(ulong uParm1,long **pplParm2);
void FUN_00407290(undefined8 *puParm1,long lParm2);
undefined8 FUN_00407320(ulong uParm1,undefined8 *puParm2,undefined8 *puParm3);
long ** FUN_004073d0(ulong uParm1,long **pplParm2,long lParm3);
long FUN_00407470(long *plParm1,int iParm2);
long * FUN_00407d20(long *plParm1,uint uParm2,long lParm3);
ulong FUN_004080c0(long *plParm1);
undefined8 * FUN_00408260(undefined8 *puParm1,undefined8 *puParm2);
undefined8 FUN_00408940(undefined8 uParm1,long lParm2,uint uParm3);
void FUN_00408970(long lParm1,int *piParm2);
ulong FUN_00408a50(int param_1,undefined8 *param_2,char *param_3,char **param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_00408fe0(char *pcParm1,int *piParm2,int iParm3);
ulong FUN_004090a0(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,uint *param_7,uint param_8);
void FUN_00409560(void);
void FUN_004095c0(void);
void FUN_004095e0(void);
undefined8 FUN_00409600(long lParm1,long lParm2);
void FUN_00409670(long lParm1);
ulong FUN_00409690(uint *puParm1,byte *pbParm2,long lParm3);
void FUN_00409700(void);
ulong FUN_00409710(uint param_1,undefined8 param_2,uint param_3,ulong param_4);
void FUN_00409810(long lParm1,long lParm2);
void FUN_00409850(void);
undefined8 FUN_00409870(long lParm1,long lParm2);
undefined8 FUN_004098d0(uint uParm1,long lParm2,uint uParm3);
ulong FUN_004099c0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined FUN_00409a80(int iParm1);;
ulong FUN_00409a90(uint uParm1);
ulong FUN_00409aa0(byte *pbParm1,byte *pbParm2);
ulong FUN_00409af0(ulong uParm1,char cParm2);
ulong FUN_00409bd0(ulong uParm1);
void FUN_00409be0(long lParm1);
undefined8 FUN_00409bf0(long *plParm1,long *plParm2);
undefined8 FUN_00409cf0(void);
ulong FUN_00409d00(ulong uParm1);
ulong FUN_00409da0(ulong uParm1);
ulong FUN_00409e10(ulong uParm1);
undefined8 * FUN_00409e80(long *plParm1,ulong uParm2);
undefined8 FUN_00409eb0(float **ppfParm1);
ulong FUN_00409f30(float fParm1,ulong uParm2,char cParm3);
void FUN_00409fc0(undefined8 *puParm1,undefined8 *puParm2);
long FUN_00409fe0(long lParm1,long lParm2,long **pplParm3,char cParm4);
void FUN_0040a0e0(long *plParm1);
undefined8 FUN_0040a100(long lParm1,long **pplParm2,char cParm3);
long FUN_0040a240(long lParm1,long lParm2);
long * FUN_0040a2a0(undefined8 uParm1,undefined1 *puParm2,undefined *puParm3,undefined *puParm4,long lParm5);
void FUN_0040a3a0(long **pplParm1);
undefined8 * FUN_0040a480(long *plParm1,undefined8 uParm2);
undefined8 * FUN_0040a600(ulong uParm1,undefined8 *puParm2,long *plParm3);
undefined8 FUN_0040a850(undefined8 uParm1,undefined8 uParm2);
long FUN_0040a890(long lParm1,undefined8 uParm2);
void FUN_0040aa70(undefined4 *puParm1,undefined4 uParm2);
ulong FUN_0040aa90(long lParm1);
ulong FUN_0040aaa0(long lParm1,uint uParm2);
undefined8 * FUN_0040aae0(ulong uParm1,undefined8 *puParm2);
undefined * FUN_0040ab30(void);
char * FUN_0040ae90(void);
void FUN_0040af50(void);
ulong FUN_0040afa0(uint uParm1);
void FUN_0040afe0(undefined8 uParm1);
ulong FUN_0040b070(ulong param_1,undefined8 param_2,ulong param_3);
ulong FUN_0040b1c0(char *pcParm1,long lParm2);
char * FUN_0040b200(ulong uParm1,long lParm2,long lParm3);
ulong FUN_0040b3b0(void);
undefined8 * FUN_0040b3f0(undefined8 param_1,undefined8 *param_2);
undefined8 * FUN_0040b660(int *param_1,ulong *param_2);
undefined8 * FUN_0040b7f0(undefined8 uParm1,undefined8 *puParm2);
undefined8 * FUN_0040ba00(undefined8 uParm1,int *piParm2,ulong *puParm3);
void FUN_0040bb70(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5,undefined8 *puParm6);
undefined8 *FUN_0040bcb0(undefined8 *puParm1,long lParm2,undefined8 *puParm3,undefined8 *puParm4,uint **ppuParm5,uint **ppuParm6);
undefined8 FUN_0040c260(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_0040c6b0(uint param_1);
void FUN_0040c6f0(uint uParm1);
ulong ** FUN_0040c8f0(ulong **ppuParm1,ulong **ppuParm2,ulong **ppuParm3,ulong **ppuParm4);
double FUN_004116a0(int *piParm1);
void FUN_004116f0(int *param_1);
ulong FUN_00411770(ulong uParm1);
long FUN_00411780(ulong uParm1,ulong uParm2);
long FUN_004117a0(void);
undefined8 FUN_004117d0(uint *puParm1,ulong *puParm2);
undefined8 FUN_004119f0(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_004126c0(undefined auParm1 [16]);
ulong FUN_004126e0(void);
void FUN_00412710(void);
undefined8 _DT_FINI(void);

