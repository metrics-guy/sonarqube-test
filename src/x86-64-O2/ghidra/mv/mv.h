typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined3;
typedef unsigned int    undefined4;
typedef unsigned long    undefined5;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00403830(void);
void entry(void);
void FUN_00403db0(void);
void FUN_00403e20(void);
void FUN_00403eb0(void);
void FUN_00403ef0(void);
void FUN_00403f10(void);
ulong FUN_00403f50(undefined8 uParm1);
void FUN_00403fc0(long lParm1);
ulong FUN_00404040(undefined *puParm1);
ulong FUN_004040c0(long lParm1,undefined8 uParm2,long lParm3);
ulong FUN_00404170(long lParm1,undefined8 uParm2,char cParm3,undefined8 uParm4);
void FUN_00404210(void);
void FUN_004043e0(uint uParm1);
ulong FUN_004044d0(char *pcParm1);
long FUN_00404500(long lParm1);
void FUN_00404550(long lParm1);
long FUN_00404580(undefined8 uParm1);
ulong FUN_004045c0(undefined8 uParm1,undefined8 uParm2);
undefined8 * FUN_00404630(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00404650(char cParm1,int iParm2);
undefined8 FUN_00404670(long lParm1,long lParm2,byte *pbParm3,char cParm4);
undefined8 FUN_00404800(undefined8 uParm1,undefined8 uParm2,long lParm3);
undefined8 FUN_00404870(ulong uParm1,undefined8 uParm2,long lParm3);
undefined8 FUN_004048f0(long lParm1,long lParm2,char cParm3,char *pcParm4,int iParm5,int *piParm6);
ulong FUN_00404c40(long lParm1,long lParm2,long lParm3);
ulong FUN_00405020(long *plParm1,long lParm2);
ulong FUN_00405170(char *pcParm1);
long FUN_004051a0(long lParm1,ulong uParm2);
undefined FUN_004051c0(int iParm1);;
ulong FUN_004051d0(int iParm1,undefined8 uParm2,uint uParm3);
ulong FUN_004051e0(long lParm1);
ulong FUN_00405220(long lParm1,uint uParm2);
undefined8 FUN_00405240(uint *puParm1);
ulong FUN_00405350(undefined8 uParm1,ulong *puParm2,undefined8 uParm3,ulong *puParm4,int *piParm5,undefined *puParm6);
ulong FUN_00405850(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00405868(undefined8 uParm1);
void FUN_004058a0(undefined8 uParm1,undefined8 uParm2,long lParm3);
ulong FUN_00405920(undefined8 uParm1,undefined8 uParm2,byte bParm3,byte bParm4,char cParm5);
void FUN_00405a10(undefined8 uParm1,ulong uParm2);
undefined8 FUN_00405a20(uint uParm1,ulong uParm2);
ulong FUN_00405ad0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_00405b20(ulong uParm1,undefined8 uParm2,char cParm3,long lParm4);
ulong FUN_00405bd0(char *pcParm1,ulong uParm2);
ulong FUN_00405c30(uint param_1,uint param_2,long param_3,ulong param_4,ulong param_5,byte param_6,undefined8 param_7,undefined8 param_8,ulong param_9,long *param_10,char *param_11);
void FUN_00406030(undefined8 uParm1,undefined8 uParm2,uint uParm3,undefined8 uParm4);
ulong FUN_00406050(void);
long FUN_00406080(long lParm1);
void FUN_004060b0(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_004060c0(long *plParm1,long *plParm2,long *plParm3);
undefined8 FUN_00406100(void);
undefined8 FUN_00406110(void);
ulong FUN_00406130(void);
ulong FUN_00406160(byte *pbParm1);
void FUN_00406190(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_004061c0(ulong param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,long param_6,int param_7,undefined8 param_8,undefined8 param_9,undefined *param_10);
void FUN_00406590(long lParm1,undefined8 uParm2,uint *puParm3);
ulong FUN_00406680(long lParm1,undefined8 uParm2,long lParm3);
ulong FUN_00406710(long lParm1,long *plParm2,long *plParm3,undefined8 uParm4);
undefined8 FUN_00406830(void);
ulong FUN_00406850(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,ulong uParm4,long lParm5);
ulong FUN_00406a00(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,long lParm4);
void FUN_00406ab0(long lParm1);
void FUN_00406ae0(undefined4 *puParm1);
ulong FUN_00406b20(long lParm1);
ulong FUN_00406b50(long lParm1,undefined8 uParm2,uint uParm3,long lParm4,char cParm5,long lParm6);
ulong FUN_00406d10(void);
ulong FUN_00406d50(undefined8 param_1,char *param_2,long param_3,uint param_4,uint param_5,byte *param_6,long *param_7);
ulong FUN_004078c0(char *param_1,char *param_2,byte param_3,char **param_4,char **param_5,uint *param_6,uint param_7,char *param_8,undefined *param_9,undefined *param_10);
void FUN_00409a10(undefined8 uParm1,undefined8 uParm2,ulong uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_00409aa0(undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined *param_6,byte *param_7,byte *param_8);
void FUN_00409ca0(long lParm1);
void FUN_00409cc0(undefined8 uParm1,undefined8 uParm2);
long FUN_00409d00(undefined8 uParm1,undefined8 uParm2);
long FUN_00409d30(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
long FUN_00409da0(void);
undefined8 FUN_00409de0(void);
void FUN_00409df0(undefined4 uParm1,undefined4 *puParm2);
ulong FUN_00409e20(uint *puParm1);
long FUN_0040a120(long lParm1,long lParm2);
ulong FUN_0040a1b0(undefined4 uParm1,undefined8 uParm2,ulong uParm3,undefined8 uParm4,undefined4 uParm5,char cParm6);
ulong FUN_0040a2e0(undefined8 uParm1,ulong uParm2,undefined8 uParm3,char cParm4);
ulong FUN_0040a3f0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040a470(undefined8 uParm1);
long FUN_0040a4c0(undefined8 uParm1,ulong uParm2);
ulong FUN_0040a5c0(long *plParm1,ulong uParm2,long lParm3,long lParm4,long *plParm5);
void FUN_0040a890(long lParm1,long lParm2);
void FUN_0040a970(char *pcParm1);
long FUN_0040a9d0(long lParm1,int iParm2,char cParm3);
long FUN_0040ac30(long lParm1,int iParm2);
ulong FUN_0040ac90(undefined *puParm1,char *pcParm2);
ulong FUN_0040acc0(ulong uParm1,ulong uParm2,ulong uParm3);
char * FUN_0040ad30(char **ppcParm1,undefined8 uParm2,undefined8 uParm3);
char * FUN_0040b340(char *pcParm1);
long FUN_0040b4d0(void);
void FUN_0040b580(char *pcParm1);
void FUN_0040b5a0(char *pcParm1);
undefined * FUN_0040b5e0(undefined8 uParm1);
char * FUN_0040b650(char *pcParm1);
void FUN_0040b6b0(long lParm1);
undefined FUN_0040b6e0(char *pcParm1);;
void FUN_0040b720(void);
void FUN_0040b730(undefined8 param_1,byte param_2,ulong param_3);
undefined8 * FUN_0040b780(long lParm1,undefined8 uParm2,undefined8 *puParm3);
ulong FUN_0040b810(long lParm1,undefined8 uParm2,undefined8 *puParm3);
undefined * FUN_0040b850(long lParm1);
undefined8 FUN_0040b8e0(undefined8 uParm1);
void FUN_0040b950(uint uParm1,undefined *puParm2);
void FUN_0040ba60(char *pcParm1);
void FUN_0040ba80(char *pcParm1);
long FUN_0040baa0(long lParm1,char *pcParm2,undefined8 *puParm3);
long FUN_0040bb70(uint uParm1,long lParm2,long lParm3);
ulong FUN_0040bbf0(ulong uParm1);
ulong FUN_0040bc60(ulong uParm1);
ulong FUN_0040bcd0(long *plParm1,ulong uParm2);
undefined8 FUN_0040bd00(float **ppfParm1);
ulong FUN_0040bd80(float fParm1,ulong uParm2,char cParm3);
void FUN_0040be10(undefined8 *puParm1,undefined8 *puParm2);
long FUN_0040be30(long lParm1,long lParm2,long **pplParm3,char cParm4);
void FUN_0040bf30(long *plParm1);
undefined8 FUN_0040bf50(long lParm1,long **pplParm2,char cParm3);
long FUN_0040c090(long lParm1,long lParm2);
long * FUN_0040c0f0(undefined8 uParm1,undefined1 *puParm2,undefined *puParm3,undefined *puParm4,long lParm5);
void FUN_0040c1f0(long **pplParm1);
ulong FUN_0040c2d0(long *plParm1,undefined8 uParm2);
ulong FUN_0040c450(ulong uParm1,undefined8 *puParm2,long *plParm3);
undefined8 FUN_0040c6a0(undefined8 uParm1,undefined8 uParm2);
long FUN_0040c6e0(long lParm1,undefined8 uParm2);
ulong FUN_0040c8c0(undefined8 *puParm1,ulong uParm2);
void FUN_0040c960(undefined8 *puParm1);
long FUN_0040c980(long lParm1);
undefined8 FUN_0040ca30(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4,uint uParm5);
void FUN_0040ca80(undefined8 uParm1,ulong uParm2,undefined4 uParm3);
int * FUN_0040caa0(int *piParm1,undefined8 *puParm2);
ulong FUN_0040cdf0(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8);
ulong FUN_0040ceb0(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9);
ulong FUN_0040cf80(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9,byte param_10);
undefined8 FUN_0040d070(byte *param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined * FUN_0040d120(char *pcParm1,int iParm2);
undefined8 *FUN_0040d1f0(undefined *puParm1,undefined8 *puParm2,long lParm3,undefined *puParm4,ulong uParm5,uint uParm6,long lParm7,char *pcParm8,char *pcParm9);
undefined1 * FUN_0040e2e0(undefined1 *puParm1,undefined8 *puParm2,undefined8 uParm3,uint *puParm4);
ulong FUN_0040e480(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
void FUN_0040e4c0(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040e4f0(ulong uParm1,undefined8 uParm2);
void FUN_0040e590(uint uParm1,undefined8 uParm2,undefined8 uParm3);
undefined1 * FUN_0040e620(undefined1 *puParm1,undefined8 *puParm2);
undefined1 * FUN_0040e630(undefined8 *puParm1);
undefined8 FUN_0040e640(undefined4 uParm1);
ulong FUN_0040e660(uint uParm1,long lParm2,uint uParm3,long lParm4,uint uParm5);
undefined8 * FUN_0040ea60(undefined8 *puParm1);
long FUN_0040eab0(uint uParm1,undefined8 uParm2,ulong uParm3);
ulong FUN_0040eb20(undefined8 uParm1,undefined8 uParm2);
undefined * FUN_0040ec50(long lParm1,uint uParm2);
undefined8 FUN_0040eee0(undefined8 uParm1,ulong uParm2);
ulong FUN_0040ef60(ulong uParm1,long lParm2,long lParm3);
undefined FUN_0040ef70(undefined8 uParm1,ulong uParm2);;
ulong FUN_0040f010(long lParm1,int iParm2,undefined8 uParm3,code *pcParm4,long lParm5);
ulong FUN_0040f180(uint uParm1);
undefined8 FUN_0040f1e0(undefined8 uParm1);
ulong FUN_0040f1f0(undefined8 uParm1,undefined8 *puParm2,long lParm3,ulong uParm4);
ulong FUN_0040fa00(undefined8 *puParm1);
void FUN_0040fab0(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_0040fac0(long lParm1,long *plParm2);
ulong FUN_0040fba0(uint uParm1,long lParm2,undefined *puParm3);
ulong FUN_0040ff20(long lParm1,undefined *puParm2);
ulong FUN_0040ff30(undefined8 uParm1,undefined8 *puParm2);
ulong FUN_00410150(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long *plParm5,undefined8 uParm6);
void FUN_004103a0(void);
void FUN_00410400(void);
ulong FUN_00410490(void);
void FUN_004104d0(long lParm1);
void FUN_004104f0(long lParm1);
long FUN_00410500(long lParm1,ulong *puParm2);
long FUN_00410540(long lParm1,ulong *puParm2,ulong uParm3);
long FUN_00410570(long lParm1,ulong *puParm2,ulong uParm3);
void FUN_00410630(undefined8 uParm1);
void FUN_00410650(void);
void FUN_00410680(undefined8 uParm1,uint uParm2);
ulong FUN_004106d0(void);
ulong FUN_00410700(void);
undefined8 FUN_00410740(ulong uParm1,ulong uParm2);
void FUN_00410790(undefined8 uParm1);
ulong FUN_004107d0(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00410840(void);
void FUN_00410860(void);
void FUN_00410890(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00410970(void);
ulong FUN_00410980(ulong uParm1,long lParm2);
ulong FUN_00410a00(ulong param_1,undefined8 param_2,ulong param_3);
undefined8 FUN_00410b50(ulong uParm1);
void FUN_00410bc0(undefined8 uParm1);
ulong FUN_00410bd0(long lParm1);
undefined8 FUN_00410c60(void);
void FUN_00410c80(void);
ulong FUN_00410ca0(void);
ulong FUN_00410cc0(ulong uParm1,long lParm2,long lParm3,ulong uParm4);
long FUN_00410e00(long *plParm1);
ulong FUN_00410e50(ulong uParm1,undefined8 *puParm2);
ulong FUN_00410e80(long lParm1,long lParm2,char cParm3);
long FUN_00411020(long lParm1,long lParm2,ulong uParm3);
long FUN_00411120(long lParm1,undefined8 uParm2,long lParm3);
void FUN_004111c0(long lParm1);
void FUN_00411210(undefined8 uParm1);
long FUN_00411250(undefined8 uParm1,undefined8 uParm2,uint uParm3,uint *puParm4);
ulong FUN_004112b0(long lParm1);
ulong FUN_004113c0(void);
ulong FUN_004113f0(void);
ulong FUN_00411460(ulong uParm1,ulong uParm2,char cParm3);
ulong FUN_004114d0(long lParm1);
void FUN_00411540(undefined4 *puParm1,int iParm2);
void thunk_FUN_0041158b(long lParm1,long lParm2,long lParm3);
void FUN_0041158b(long lParm1,long lParm2,long lParm3);
undefined8 FUN_004115e0(long *plParm1,ulong *puParm2,long lParm3);
ulong FUN_00411650(ulong uParm1,ulong *puParm2);
void FUN_004116b0(uint uParm1,uint uParm2,undefined8 uParm3,ulong uParm4);
ulong FUN_00411700(long lParm1,long lParm2,uint uParm3,char *pcParm4);
void FUN_004118f0(ulong uParm1,long **pplParm2);
void FUN_00411920(undefined8 *puParm1,long lParm2);
undefined8 FUN_004119b0(ulong uParm1,undefined8 *puParm2,undefined8 *puParm3);
long ** FUN_00411a60(ulong uParm1,long **pplParm2,long lParm3);
long FUN_00411b00(long *plParm1,int iParm2);
long * FUN_004123b0(long *plParm1,uint uParm2,long lParm3);
ulong FUN_00412750(long *plParm1);
undefined8 * FUN_004128f0(undefined8 *puParm1,undefined8 *puParm2);
undefined8 FUN_00412fd0(undefined8 uParm1,long lParm2,uint uParm3);
long FUN_00413000(long lParm1,ulong uParm2);
void FUN_00413480(long lParm1,int *piParm2);
ulong FUN_00413560(int param_1,undefined8 *param_2,char *param_3,char **param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_00413af0(char *pcParm1,int *piParm2,int iParm3);
ulong FUN_00413bb0(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,uint *param_7,uint param_8);
void FUN_00414070(void);
void FUN_004140d0(void);
undefined8 FUN_004140f0(long lParm1,uint uParm2,uint uParm3);
ulong FUN_004141d0(uint uParm1,char *pcParm2,uint uParm3,undefined8 uParm4);
ulong FUN_00414320(ulong uParm1,long lParm2,ulong uParm3,long lParm4,uint uParm5);
void FUN_00414490(void);
undefined8 FUN_004144b0(long lParm1,long lParm2);
void FUN_00414520(void);
ulong FUN_00414540(uint *puParm1,byte *pbParm2,long lParm3);
ulong FUN_004145b0(long lParm1,ulong uParm2);
undefined8 FUN_004146b0(long lParm1,ulong uParm2);
undefined8 FUN_00414710(long lParm1,uint uParm2,long lParm3);
void FUN_004147b0(void);
ulong FUN_004147c0(uint param_1,undefined8 param_2,uint param_3,ulong param_4);
undefined8 FUN_004148c0(long lParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00414930(long lParm1,long lParm2);
ulong FUN_00414970(long lParm1,long lParm2);
ulong FUN_00414e90(uint uParm1,long lParm2,uint uParm3,long lParm4);
ulong FUN_00414ea0(long lParm1);
void FUN_00414f30(void);
undefined8 FUN_00414f50(long lParm1,long lParm2);
undefined8 FUN_00414fb0(undefined8 uParm1,ulong uParm2,long lParm3);
undefined8 FUN_00415020(long lParm1);
undefined8 FUN_004150f0(uint uParm1,long lParm2,uint uParm3);
ulong FUN_004151e0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_004152a0(undefined8 uParm1,undefined8 uParm2,undefined4 uParm3,undefined4 *puParm4);
void FUN_004152b0(undefined8 uParm1,int iParm2,uint uParm3);
ulong FUN_004152d0(uint *puParm1,undefined8 uParm2,uint uParm3);
undefined * FUN_00415300(uint uParm1,undefined8 uParm2);
long FUN_00415330(undefined8 uParm1,long *plParm2,long lParm3,long lParm4);
void FUN_00415440(undefined auParm1 [16],undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined auParm8 [16],undefined8 uParm9,undefined8 uParm10,long lParm11,undefined8 uParm12,undefined8 uParm13,undefined8 uParm14);
void FUN_004154a0(long *plParm1,long lParm2,long lParm3);
long FUN_00415580(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
undefined FUN_00415610(int iParm1);;
ulong FUN_00415620(uint uParm1);
ulong FUN_00415630(byte *pbParm1,byte *pbParm2);
ulong FUN_00415840(ulong uParm1,char cParm2);
ulong FUN_00415920(ulong uParm1);
void FUN_00415930(long lParm1);
undefined8 FUN_00415940(long *plParm1,long *plParm2);
undefined8 FUN_004159f0(void);
void FUN_00415a00(undefined8 *puParm1);
ulong FUN_00415a40(ulong uParm1);
ulong FUN_00415ae0(char *pcParm1,ulong uParm2);
void FUN_00415b20(undefined4 *puParm1,undefined4 uParm2);
ulong FUN_00415b40(long lParm1);
ulong FUN_00415b50(long lParm1,uint uParm2);
ulong FUN_00415b90(ulong uParm1,undefined8 *puParm2);
undefined * FUN_00415be0(void);
char * FUN_00415f40(void);
void FUN_00416000(void);
long FUN_00416050(long lParm1);
void FUN_00416060(undefined8 uParm1);
long * FUN_00416090(void);
ulong FUN_004160c0(undefined8 *puParm1,ulong uParm2);
void FUN_004161d0(undefined8 uParm1);
ulong FUN_004161f0(undefined8 *puParm1);
void FUN_00416220(undefined8 uParm1,undefined8 uParm2);
void FUN_00416440(undefined4 *puParm1,ulong uParm2);
undefined8 * FUN_00416650(long lParm1,ulong uParm2);
void FUN_00416700(long *plParm1,ulong uParm2,ulong uParm3);
undefined8 FUN_00416720(long *plParm1);
undefined8 FUN_00416770(undefined8 uParm1);
undefined8 FUN_00416780(long lParm1,undefined8 uParm2);
void FUN_00416790(long *plParm1,long *plParm2);
void FUN_00416a60(ulong *puParm1);
ulong FUN_00416d10(ulong uParm1);
undefined8 FUN_00416d20(long lParm1,uint uParm2,uint uParm3);
void FUN_00416e50(undefined8 uParm1,undefined8 uParm2);
void FUN_00416e70(undefined8 uParm1);
void FUN_00416f00(void);
ulong FUN_00416ff0(char *pcParm1,long lParm2);
char * FUN_00417030(ulong uParm1,long lParm2,long lParm3);
ulong FUN_004171e0(void);
ulong FUN_00417220(undefined8 param_1,undefined8 *param_2);
int * FUN_00417490(int *param_1,ulong *param_2);
ulong FUN_00417620(undefined8 uParm1,undefined8 *puParm2);
uint * FUN_00417830(undefined8 uParm1,int *piParm2,ulong *puParm3);
void FUN_004179a0(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5,undefined8 *puParm6);
int * FUN_00417ae0(undefined8 *puParm1,long lParm2,undefined8 *puParm3,undefined8 *puParm4,uint **ppuParm5,uint **ppuParm6);
undefined8 FUN_00418090(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_004184e0(uint param_1);
void FUN_00418520(uint uParm1);
ulong ** FUN_00418720(ulong **ppuParm1,ulong **ppuParm2,ulong **ppuParm3,ulong **ppuParm4);
long FUN_0041d4b0(undefined8 uParm1,undefined8 uParm2);
double FUN_0041d550(int *piParm1);
void FUN_0041d5a0(int *param_1);
long FUN_0041d620(ulong uParm1,ulong uParm2);
long FUN_0041d640(void);
undefined8 FUN_0041d670(uint *puParm1,ulong *puParm2);
undefined8 FUN_0041d890(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_0041e560(undefined auParm1 [16]);
ulong FUN_0041e580(void);
void FUN_0041e5b0(void);
undefined8 _DT_FINI(void);

