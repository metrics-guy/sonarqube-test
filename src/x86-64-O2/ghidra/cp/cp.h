typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined5;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_004037f0(void);
void entry(void);
void FUN_00403ee0(void);
void FUN_00403f50(void);
void FUN_00403fe0(void);
void FUN_00404020(void);
void FUN_00404040(void);
ulong FUN_00404080(undefined8 uParm1,long lParm2,undefined *puParm3);
void FUN_00404120(long lParm1);
undefined * FUN_004041a0(undefined8 uParm1,undefined8 uParm2,ulong uParm3);
undefined8 FUN_004042e0(undefined8 uParm1,long lParm2,long lParm3,long *plParm4,char *pcParm5,long lParm6);
void FUN_00404850(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00404860(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
undefined8 FUN_00404a90(void);
void FUN_00404ab0(void);
void FUN_00404c80(uint uParm1);
ulong FUN_00404e50(int iParm1,undefined8 *puParm2,long lParm3,char cParm4,undefined1 *puParm5);
ulong FUN_00405290(char *pcParm1);
long FUN_004052c0(long lParm1,ulong uParm2);
undefined FUN_004052e0(int iParm1);;
ulong FUN_004052f0(int iParm1,undefined8 uParm2,uint uParm3);
ulong FUN_00405300(long lParm1);
ulong FUN_00405340(long lParm1,uint uParm2);
undefined8 FUN_00405360(uint *puParm1);
ulong FUN_00405470(undefined8 uParm1,ulong *puParm2,undefined8 uParm3,ulong *puParm4,int *piParm5,undefined *puParm6);
ulong FUN_00405970(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00405988(undefined8 uParm1);
void FUN_004059c0(undefined8 uParm1,undefined8 uParm2,long lParm3);
ulong FUN_00405a40(undefined8 uParm1,undefined8 uParm2,byte bParm3,byte bParm4,char cParm5);
void FUN_00405b30(undefined8 uParm1,ulong uParm2);
undefined8 FUN_00405b40(uint uParm1,ulong uParm2);
ulong FUN_00405bf0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_00405c40(ulong uParm1,undefined8 uParm2,char cParm3,long lParm4);
ulong FUN_00405cf0(char *pcParm1,ulong uParm2);
ulong FUN_00405d50(uint param_1,uint param_2,long param_3,ulong param_4,ulong param_5,byte param_6,undefined8 param_7,undefined8 param_8,ulong param_9,long *param_10,char *param_11);
void FUN_00406150(undefined8 uParm1,undefined8 uParm2,uint uParm3,undefined8 uParm4);
ulong FUN_00406170(void);
long FUN_004061a0(long lParm1);
void FUN_004061d0(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_004061e0(long *plParm1,long *plParm2,long *plParm3);
undefined8 FUN_00406220(void);
undefined8 FUN_00406230(void);
ulong FUN_00406250(void);
ulong FUN_00406280(byte *pbParm1);
void FUN_004062b0(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_004062e0(ulong param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,long param_6,int param_7,undefined8 param_8,undefined8 param_9,undefined *param_10);
void FUN_004066b0(long lParm1,undefined8 uParm2,uint *puParm3);
ulong FUN_004067a0(long lParm1,undefined8 uParm2,long lParm3);
ulong FUN_00406830(long lParm1,long *plParm2,long *plParm3,undefined8 uParm4);
undefined8 FUN_00406950(void);
ulong FUN_00406970(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,ulong uParm4,long lParm5);
ulong FUN_00406b20(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,long lParm4);
void FUN_00406bd0(long lParm1);
void FUN_00406c00(long lParm1);
void FUN_00406c30(undefined4 *puParm1);
ulong FUN_00406c70(long lParm1);
ulong FUN_00406ca0(long lParm1,undefined8 uParm2,uint uParm3,long lParm4,char cParm5,long lParm6);
ulong FUN_00406e60(void);
ulong FUN_00406ea0(undefined8 param_1,char *param_2,long param_3,uint param_4,uint param_5,byte *param_6,long *param_7);
ulong FUN_00407a10(char *param_1,char *param_2,byte param_3,char **param_4,char **param_5,uint *param_6,uint param_7,char *param_8,undefined *param_9,undefined *param_10);
void FUN_00409b60(undefined8 uParm1,undefined8 uParm2,ulong uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_00409bf0(undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined *param_6,byte *param_7,byte *param_8);
void FUN_00409df0(long lParm1);
void FUN_00409e10(undefined8 uParm1,undefined8 uParm2);
long FUN_00409e50(undefined8 uParm1,undefined8 uParm2);
long FUN_00409e80(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
long FUN_00409ef0(void);
undefined8 FUN_00409f30(void);
void FUN_00409f40(undefined4 uParm1,undefined4 *puParm2);
ulong FUN_00409f70(uint *puParm1);
long FUN_0040a270(long lParm1,long lParm2);
ulong FUN_0040a300(undefined4 uParm1,undefined8 uParm2,ulong uParm3,undefined8 uParm4,undefined4 uParm5,char cParm6);
ulong FUN_0040a430(undefined8 uParm1,ulong uParm2,undefined8 uParm3,char cParm4);
ulong FUN_0040a540(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040a5c0(undefined8 uParm1);
long FUN_0040a610(undefined8 uParm1,ulong uParm2);
long FUN_0040a720(undefined8 uParm1,long *plParm2,long lParm3,long lParm4);
void FUN_0040a830(undefined auParm1 [16],undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined auParm8 [16],undefined8 uParm9,undefined8 uParm10,long lParm11,undefined8 uParm12,undefined8 uParm13,undefined8 uParm14);
void FUN_0040a890(long *plParm1,long lParm2,long lParm3);
long FUN_0040a970(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
ulong FUN_0040a9e0(long *plParm1,ulong uParm2,long lParm3,long lParm4,long *plParm5);
void FUN_0040acb0(long lParm1,long lParm2);
void FUN_0040ad90(char *pcParm1);
long FUN_0040adf0(long lParm1,int iParm2,char cParm3);
long FUN_0040b050(long lParm1,int iParm2);
ulong FUN_0040b060(undefined8 uParm1,char *pcParm2);
ulong FUN_0040b0d0(undefined *puParm1,char *pcParm2);
ulong FUN_0040b100(ulong uParm1,ulong uParm2,ulong uParm3);
char * FUN_0040b170(char **ppcParm1,undefined8 uParm2,undefined8 uParm3);
char * FUN_0040b780(char *pcParm1);
long FUN_0040b910(void);
void FUN_0040b9c0(char *pcParm1);
void FUN_0040b9e0(char *pcParm1);
undefined * FUN_0040ba20(undefined8 uParm1);
char * FUN_0040ba90(char *pcParm1);
void FUN_0040baf0(long lParm1);
undefined FUN_0040bb20(char *pcParm1);;
void FUN_0040bb60(void);
void FUN_0040bb70(undefined8 param_1,byte param_2,ulong param_3);
undefined8 * FUN_0040bbc0(long lParm1,undefined8 uParm2,undefined8 *puParm3);
ulong FUN_0040bc50(long lParm1,undefined8 uParm2,undefined8 *puParm3);
undefined8 FUN_0040bc90(undefined8 uParm1);
void FUN_0040bd00(uint uParm1,undefined *puParm2);
void FUN_0040be10(char *pcParm1);
void FUN_0040be30(char *pcParm1);
long FUN_0040be50(long lParm1,char *pcParm2,undefined8 *puParm3);
long FUN_0040bf20(uint uParm1,long lParm2,long lParm3);
ulong FUN_0040bfa0(ulong uParm1);
ulong FUN_0040c010(ulong uParm1);
undefined * FUN_0040c080(long *plParm1,ulong uParm2);
undefined8 FUN_0040c0b0(float **ppfParm1);
ulong FUN_0040c130(float fParm1,ulong uParm2,char cParm3);
void FUN_0040c1c0(undefined8 *puParm1,undefined8 *puParm2);
long FUN_0040c1e0(long lParm1,long lParm2,long **pplParm3,char cParm4);
void FUN_0040c2e0(long *plParm1);
undefined8 FUN_0040c300(long lParm1,long **pplParm2,char cParm3);
long FUN_0040c440(long lParm1,long lParm2);
long * FUN_0040c4a0(undefined8 uParm1,undefined1 *puParm2,undefined *puParm3,undefined *puParm4,long lParm5);
void FUN_0040c5a0(long **pplParm1);
undefined * FUN_0040c680(long *plParm1,undefined8 uParm2);
undefined * FUN_0040c800(ulong uParm1,undefined8 *puParm2,long *plParm3);
undefined8 FUN_0040ca50(undefined8 uParm1,undefined8 uParm2);
long FUN_0040ca90(long lParm1,undefined8 uParm2);
ulong FUN_0040cc70(undefined8 *puParm1,ulong uParm2);
void FUN_0040cd20(undefined8 *puParm1);
long FUN_0040cd40(long lParm1);
undefined8 FUN_0040cdf0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4,uint uParm5);
void FUN_0040ce40(undefined8 uParm1,ulong uParm2,undefined4 uParm3);
int * FUN_0040ce60(int *piParm1,undefined8 *puParm2);
ulong FUN_0040d1b0(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8);
ulong FUN_0040d270(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9);
ulong FUN_0040d340(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9,byte param_10);
undefined8 FUN_0040d430(byte *param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined * FUN_0040d4e0(char *pcParm1,int iParm2);
undefined8 *FUN_0040d5b0(undefined *puParm1,undefined8 *puParm2,long lParm3,undefined *puParm4,ulong uParm5,uint uParm6,long lParm7,char *pcParm8,char *pcParm9);
undefined1 * FUN_0040e6a0(undefined1 *puParm1,undefined8 *puParm2,undefined8 uParm3,uint *puParm4);
ulong FUN_0040e840(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
void FUN_0040e880(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040e8b0(ulong uParm1,undefined8 uParm2);
void FUN_0040e950(uint uParm1,undefined8 uParm2,undefined8 uParm3);
undefined1 * FUN_0040e9e0(undefined1 *puParm1,undefined8 *puParm2);
undefined1 * FUN_0040e9f0(undefined8 *puParm1);
undefined8 FUN_0040ea00(undefined4 uParm1);
ulong FUN_0040ea20(uint uParm1,long lParm2,uint uParm3,long lParm4,uint uParm5);
long FUN_0040ee20(uint uParm1,undefined8 uParm2,ulong uParm3);
ulong FUN_0040ee90(undefined8 uParm1,undefined8 uParm2);
undefined * FUN_0040efc0(long lParm1,uint uParm2);
undefined8 FUN_0040f250(undefined8 uParm1,ulong uParm2);
ulong FUN_0040f2d0(ulong uParm1,long lParm2,long lParm3);
undefined FUN_0040f2e0(undefined8 uParm1,ulong uParm2);;
ulong FUN_0040f380(long lParm1,int iParm2,undefined8 uParm3,code *pcParm4,long lParm5);
ulong FUN_0040f4f0(uint uParm1);
undefined8 FUN_0040f550(undefined8 uParm1);
ulong FUN_0040f560(undefined8 uParm1,undefined8 *puParm2,long lParm3,ulong uParm4);
ulong FUN_0040fd70(undefined8 *puParm1);
void FUN_0040fe20(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_0040fe30(long lParm1,long *plParm2);
ulong FUN_0040ff10(uint uParm1,long lParm2,undefined *puParm3);
ulong FUN_00410290(long lParm1,undefined *puParm2);
ulong FUN_004102a0(undefined8 uParm1,undefined8 *puParm2);
void FUN_004104c0(undefined8 uParm1,long lParm2,long lParm3,long lParm4,long lParm5);
void FUN_00410710(void);
void FUN_00410770(void);
ulong FUN_00410800(void);
void FUN_00410840(long lParm1);
void FUN_00410860(long lParm1);
long FUN_00410870(long lParm1,ulong *puParm2);
long FUN_004108b0(long lParm1,ulong *puParm2,ulong uParm3);
long FUN_004108e0(long lParm1,ulong *puParm2,ulong uParm3);
void FUN_004109a0(undefined8 uParm1);
ulong FUN_004109c0(void);
ulong FUN_004109f0(void);
ulong FUN_00410a20(void);
undefined8 FUN_00410a60(ulong uParm1,ulong uParm2);
void FUN_00410ab0(undefined8 uParm1);
ulong FUN_00410af0(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00410b60(void);
void FUN_00410b80(void);
void FUN_00410bb0(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_00410c90(ulong param_1,undefined8 param_2,ulong param_3);
undefined8 FUN_00410de0(ulong uParm1);
void FUN_00410e50(undefined8 uParm1);
ulong FUN_00410e60(long lParm1);
undefined8 FUN_00410ef0(void);
void FUN_00410f10(void);
ulong FUN_00410f30(void);
ulong FUN_00410f50(ulong uParm1,long lParm2,long lParm3,ulong uParm4);
long FUN_00411010(long lParm1,ulong uParm2);
void FUN_00411490(long lParm1,int *piParm2);
ulong FUN_00411570(int param_1,undefined8 *param_2,char *param_3,char **param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_00411b00(char *pcParm1,int *piParm2,int iParm3);
ulong FUN_00411bc0(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,uint *param_7,uint param_8);
void FUN_00412080(void);
void FUN_004120e0(void);
undefined8 FUN_00412100(long lParm1,uint uParm2,uint uParm3);
ulong FUN_004121e0(uint uParm1,char *pcParm2,uint uParm3,undefined8 uParm4);
ulong FUN_00412330(ulong uParm1,long lParm2,ulong uParm3,long lParm4,uint uParm5);
void FUN_004124a0(void);
undefined8 FUN_004124c0(long lParm1,long lParm2);
void FUN_00412530(void);
ulong FUN_00412550(uint *puParm1,byte *pbParm2,long lParm3);
ulong FUN_004125c0(long lParm1,ulong uParm2);
undefined8 FUN_004126c0(long lParm1,ulong uParm2);
undefined8 FUN_00412720(long lParm1,uint uParm2,long lParm3);
void FUN_004127c0(void);
ulong FUN_004127d0(uint param_1,undefined8 param_2,uint param_3,ulong param_4);
undefined8 FUN_004128d0(long lParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00412940(long lParm1,long lParm2);
ulong FUN_00412980(long lParm1,long lParm2);
ulong FUN_00412ea0(uint uParm1,long lParm2,uint uParm3,long lParm4);
ulong FUN_00412eb0(long lParm1);
void FUN_00412f40(void);
undefined8 FUN_00412f60(long lParm1,long lParm2);
undefined8 FUN_00412fc0(undefined8 uParm1,ulong uParm2,long lParm3);
undefined8 FUN_00413030(long lParm1);
undefined8 FUN_00413100(uint uParm1,long lParm2,uint uParm3);
ulong FUN_004131f0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_004132b0(undefined8 uParm1,undefined8 uParm2,undefined4 uParm3,undefined4 *puParm4);
void FUN_004132c0(undefined8 uParm1,int iParm2,uint uParm3);
ulong FUN_004132e0(uint *puParm1,undefined8 uParm2,uint uParm3);
undefined * FUN_00413310(uint uParm1,undefined8 uParm2);
undefined FUN_00413350(int iParm1);;
ulong FUN_00413360(uint uParm1);
ulong FUN_00413370(byte *pbParm1,byte *pbParm2);
ulong FUN_00413580(ulong uParm1,char cParm2);
undefined8 FUN_00413660(void);
void FUN_00413670(undefined8 *puParm1);
ulong FUN_004136b0(ulong uParm1);
ulong FUN_00413750(char *pcParm1,ulong uParm2);
undefined * FUN_00413790(void);
char * FUN_00413af0(void);
long FUN_00413bb0(long lParm1);
void FUN_00413bc0(undefined8 uParm1);
long * FUN_00413bf0(void);
ulong FUN_00413c20(undefined8 *puParm1,ulong uParm2);
void FUN_00413d30(undefined8 uParm1);
ulong FUN_00413d50(undefined8 *puParm1);
void FUN_00413d80(undefined8 uParm1,undefined8 uParm2);
void FUN_00413fa0(undefined4 *puParm1,ulong uParm2);
undefined8 * FUN_004141b0(long lParm1,ulong uParm2);
void FUN_00414260(long *plParm1,ulong uParm2,ulong uParm3);
undefined8 FUN_00414280(long *plParm1);
undefined8 FUN_004142d0(undefined8 uParm1);
undefined8 FUN_004142e0(long lParm1,undefined8 uParm2);
void FUN_004142f0(long *plParm1,long *plParm2);
void FUN_004145c0(ulong *puParm1);
ulong FUN_00414870(ulong uParm1);
undefined8 FUN_00414880(long lParm1,uint uParm2,uint uParm3);
void FUN_004149b0(undefined8 uParm1,undefined8 uParm2);
void FUN_004149d0(undefined8 uParm1);
void FUN_00414a60(void);
ulong FUN_00414b50(char *pcParm1,long lParm2);
char * FUN_00414b90(ulong uParm1,long lParm2,long lParm3);
ulong FUN_00414d40(void);
undefined * FUN_00414d80(undefined8 param_1,undefined8 *param_2);
int * FUN_00414ff0(int *param_1,ulong *param_2);
undefined * FUN_00415180(undefined8 uParm1,undefined8 *puParm2);
uint * FUN_00415390(undefined8 uParm1,int *piParm2,ulong *puParm3);
void FUN_00415500(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5,undefined8 *puParm6);
int * FUN_00415640(undefined8 *puParm1,long lParm2,undefined8 *puParm3,undefined8 *puParm4,uint **ppuParm5,uint **ppuParm6);
undefined8 FUN_00415bf0(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_00416040(uint param_1);
void FUN_00416080(uint uParm1);
long FUN_0041b010(undefined8 uParm1,undefined8 uParm2);
double FUN_0041b0b0(int *piParm1);
void FUN_0041b100(int *param_1);
long FUN_0041b180(ulong uParm1,ulong uParm2);
long FUN_0041b1a0(void);
undefined8 FUN_0041b1d0(uint *puParm1,ulong *puParm2);
undefined8 FUN_0041b3f0(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_0041c0c0(undefined auParm1 [16]);
ulong FUN_0041c0e0(void);
void FUN_0041c110(void);
undefined8 _DT_FINI(void);

