typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_004023d0(void);
void entry(void);
void FUN_00402d70(void);
void FUN_00402de0(void);
void FUN_00402e70(void);
ulong FUN_00402ec0(void);
void FUN_00402f20(void);
void FUN_004030b0(void);
void FUN_004031f0(void);
void FUN_00403210(void);
void FUN_00403260(ulong uParm1);
void FUN_00403330(int iParm1);
ulong FUN_004035e0(uint uParm1,char cParm2,char *pcParm3);
void FUN_004036e0(undefined *puParm1);
void FUN_00403710(undefined8 uParm1,int iParm2,undefined4 *puParm3,undefined8 uParm4);
void FUN_00403730(undefined8 uParm1);
undefined8 FUN_00403760(char *pcParm1,long *plParm2);
ulong FUN_00403840(byte bParm1);
char * thunk_FUN_00405ad0(ulong uParm1,long lParm2);
ulong FUN_00403a40(undefined8 *puParm1);
void FUN_00403b60(void);
void FUN_00403c00(int iParm1);
ulong FUN_00403c50(void);
ulong FUN_00403d10(void);
void FUN_00404010(int iParm1);
void FUN_00404050(void);
void FUN_00404070(void);
void FUN_00404090(undefined8 uParm1,undefined8 uParm2);
void FUN_004040a0(char *pcParm1,uint uParm2);
undefined8 FUN_00404270(int iParm1,undefined8 *puParm2);
void FUN_00404400(undefined4 *puParm1,undefined4 *puParm2);
void FUN_00404480(undefined8 *puParm1,int iParm2);
ulong FUN_00404640(ulong uParm1);
ulong FUN_004047c0(undefined8 *puParm1);
void FUN_00404860(code **ppcParm1,int iParm2,char *pcParm3);
void FUN_004048a0(code **ppcParm1);
void FUN_004049b0(long lParm1);
undefined8 FUN_00404a10(undefined8 *puParm1);
void FUN_00404d80(void);
void FUN_00404f30(void);
undefined8 FUN_00405020(void);
void FUN_00405360(ulong uParm1,undefined8 uParm2);
void FUN_00405410(void);
void FUN_004055e0(uint uParm1);
void FUN_004057b0(char *pcParm1,char cParm2,char *pcParm3,undefined4 *puParm4);
void FUN_00405920(long lParm1,ulong uParm2);
long FUN_00405950(undefined8 uParm1,undefined8 uParm2);
void FUN_004059f0(undefined8 *puParm1);
ulong FUN_00405a30(ulong uParm1);
char * FUN_00405ad0(ulong uParm1,long lParm2);
ulong FUN_00405d50(byte *pbParm1,ulong uParm2);
ulong FUN_00405d80(int iParm1,int iParm2);
long FUN_00405dc0(long lParm1,long lParm2,long lParm3);
long FUN_00405e00(long lParm1,long lParm2,long lParm3);
char * FUN_00405e40(char *pcParm1,long lParm2,char *pcParm3,uint *puParm4,char cParm5,undefined8 uParm6,undefined8 uParm7,uint uParm8);
void FUN_00407650(void);
int * FUN_00407670(long lParm1);
int * FUN_00407720(int *piParm1,undefined8 *puParm2);
ulong FUN_00407a70(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8);
ulong FUN_00407b30(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9);
ulong FUN_00407c00(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9,byte param_10);
undefined8 FUN_00407cf0(byte *param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined * FUN_00407da0(char *pcParm1,int iParm2);
undefined8 *FUN_00407e70(undefined *puParm1,undefined8 *puParm2,long lParm3,undefined *puParm4,ulong uParm5,uint uParm6,long lParm7,char *pcParm8,char *pcParm9);
undefined1 * FUN_00408f60(undefined1 *puParm1,undefined8 *puParm2,undefined8 uParm3,uint *puParm4);
ulong FUN_00409100(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
void FUN_004091d0(uint uParm1,undefined8 uParm2,undefined8 uParm3);
undefined1 * FUN_00409270(undefined8 *puParm1);
ulong FUN_00409280(ulong uParm1);
void FUN_00409290(undefined8 uParm1,long lParm2,long lParm3,long lParm4,long lParm5);
void FUN_004094e0(void);
void FUN_00409540(void);
void FUN_004095d0(ulong uParm1,ulong uParm2);
void FUN_004095f0(ulong uParm1,ulong uParm2);
void FUN_00409620(ulong uParm1,ulong uParm2);
long FUN_00409630(long lParm1,ulong *puParm2);
long FUN_00409700(long lParm1,ulong *puParm2);
long FUN_00409710(void);
long FUN_00409740(undefined8 param_1,ulong param_2,long param_3,long param_4,undefined8 param_5,undefined8 param_6,uint param_7);
void FUN_00409820(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
undefined8 FUN_00409850(long *plParm1,int iParm2);
ulong FUN_004098a0(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_004098d0(char *pcParm1,char **ppcParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_00409bf0(long *plParm1,int iParm2);
ulong FUN_00409c40(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_00409c70(char *pcParm1,char **ppcParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined *FUN_00409f90(int iParm1,undefined8 *puParm2,undefined uParm3,long lParm4,undefined8 uParm5,ulong uParm6);
ulong FUN_0040a000(int iParm1,undefined8 uParm2,char cParm3,undefined8 uParm4,undefined8 uParm5);
ulong FUN_0040a020(int iParm1);
ulong FUN_0040a040(ulong *puParm1,int iParm2);
ulong FUN_0040a070(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_0040a0a0(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
void FUN_0040a3e0(undefined8 uParm1);
ulong FUN_0040a420(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_0040a490(void);
void FUN_0040a4b0(void);
void FUN_0040a4e0(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_0040a5c0(undefined8 uParm1);
ulong FUN_0040a650(ulong param_1,undefined8 param_2,ulong param_3);
void FUN_0040a7a0(undefined8 uParm1);
ulong FUN_0040a7b0(long lParm1);
undefined8 FUN_0040a840(void);
void FUN_0040a860(long lParm1,int *piParm2);
ulong FUN_0040a940(int param_1,undefined8 *param_2,char *param_3,char **param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_0040aed0(char *pcParm1,int *piParm2,int iParm3);
ulong FUN_0040af90(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,uint *param_7,uint param_8);
void FUN_0040b450(void);
void FUN_0040b4b0(void);
void FUN_0040b4d0(void);
void FUN_0040b540(long lParm1);
ulong FUN_0040b560(uint *puParm1,byte *pbParm2,long lParm3);
void FUN_0040b5d0(long lParm1,long lParm2);
ulong FUN_0040b610(uint uParm1,uint uParm2);
ulong FUN_0040b630(uint *puParm1,uint *puParm2);
void FUN_0040b680(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_0040b690(void);
ulong FUN_0040b6a0(char *pcParm1);
ulong FUN_0040b6d0(long lParm1);
undefined8 * FUN_0040b710(long lParm1);
undefined8 FUN_0040b7b0(long *plParm1,char *pcParm2);
void FUN_0040b8f0(long *plParm1);
long FUN_0040b920(long lParm1);
ulong FUN_0040b9d0(long lParm1);
undefined8 FUN_0040ba20(long lParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_0040bac0(long lParm1,undefined8 uParm2);
long FUN_0040bba0(uint *puParm1);
void FUN_0040bbc0(void);
ulong FUN_0040bca0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040bf30(long param_1,char param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined8 FUN_0040bf90(char *pcParm1);
ulong FUN_0040bfd0(ulong uParm1);
undefined FUN_0040c010(int iParm1);;
ulong FUN_0040c020(uint uParm1);
ulong FUN_0040c030(byte *pbParm1,byte *pbParm2);
undefined8 FUN_0040c0f0(void);
undefined * FUN_0040c100(void);
char * FUN_0040c460(void);
ulong FUN_0040c6d0(long param_1,long param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10);
undefined8 FUN_0040c740(char *param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8);
ulong FUN_0040c780(undefined8 uParm1);
long FUN_0040cad0(long lParm1,undefined4 uParm2);
ulong FUN_0040cae0(ulong uParm1);
ulong FUN_0040cb60(uint uParm1,uint uParm2);
long FUN_0040cb80(ulong param_1,long param_2,undefined8 param_3,int param_4,int param_5,uint param_6,int param_7,int param_8,int param_9,int param_10);
long FUN_0040ccd0(undefined8 uParm1,undefined8 uParm2);
long FUN_0040cd00(void);
void FUN_0040cdb0(code *pcParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
long FUN_0040cdd0(code *pcParm1,long *plParm2,undefined8 uParm3,undefined8 uParm4);
long FUN_0040d230(uint *puParm1);
ulong FUN_0040d250(char *pcParm1,char *pcParm2,ulong uParm3);
ulong FUN_0040d380(char *pcParm1,long lParm2);
char * FUN_0040d3c0(ulong uParm1,long lParm2,long lParm3);
ulong FUN_0040d570(void);
undefined * FUN_0040d5b0(undefined8 param_1,undefined8 *param_2);
int * FUN_0040d820(int *param_1,ulong *param_2);
undefined * FUN_0040d9b0(undefined8 uParm1,undefined8 *puParm2);
uint * FUN_0040dbc0(undefined8 uParm1,int *piParm2,ulong *puParm3);
void FUN_0040dd30(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5,undefined8 *puParm6);
int * FUN_0040de70(undefined8 *puParm1,long lParm2,undefined8 *puParm3,undefined8 *puParm4,uint **ppuParm5,uint **ppuParm6);
undefined8 FUN_0040e420(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_0040e870(uint param_1);
void FUN_0040e8b0(uint uParm1);
ulong ** FUN_0040eab0(ulong **ppuParm1,ulong **ppuParm2,ulong **ppuParm3,ulong **ppuParm4);
undefined8 * FUN_00413840(ulong uParm1);
void FUN_004138c0(ulong uParm1);
double FUN_00413950(int *piParm1);
void FUN_004139a0(int *param_1);
long FUN_00413a20(ulong uParm1,ulong uParm2);
long FUN_00413a40(void);
undefined8 FUN_00413a70(uint *puParm1,ulong *puParm2);
undefined8 FUN_00413c90(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_00414960(undefined auParm1 [16]);
ulong FUN_00414980(void);
void FUN_004149b0(void);
undefined8 _DT_FINI(void);

