typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined5;
typedef unsigned long    undefined6;
typedef unsigned long    undefined7;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00403560(void);
void FUN_00403760(void);
void entry(void);
void FUN_00404230(void);
void FUN_004042a0(void);
void FUN_00404330(void);
void FUN_00404370(undefined8 *puParm1);
void FUN_004043a0(undefined8 *puParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_004043c0(ulong *puParm1,ulong *puParm2);
void FUN_00404420(void);
void FUN_00404440(void);
uint FUN_00404460(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00404490(void);
undefined8 FUN_00404510(long *plParm1);
void FUN_004045a0(long lParm1);
char * thunk_FUN_004099b0(ulong uParm1,long lParm2);
void FUN_00404600(undefined8 uParm1,undefined8 uParm2,ulong uParm3,undefined8 uParm4);
void FUN_004046b0(undefined8 *puParm1,undefined8 uParm2);
void FUN_00404740(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00404750(long lParm1,long lParm2);
ulong FUN_00404d30(ulong uParm1);
undefined * FUN_00404e10(void);
void FUN_00404e20(void);
void FUN_00404ff0(void);
void FUN_00405010(uint uParm1);
long FUN_00405170(undefined8 uParm1,long *plParm2,long lParm3,long lParm4);
void FUN_00405280(undefined auParm1 [16],undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined auParm8 [16],undefined8 uParm9,undefined8 uParm10,long lParm11,undefined8 uParm12,undefined8 uParm13,undefined8 uParm14);
void FUN_004052e0(long *plParm1,long lParm2,long lParm3);
long FUN_004053c0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
void FUN_00405430(undefined8 uParm1);
void FUN_00405460(undefined8 uParm1);
long FUN_004054a0(long *plParm1,int *piParm2);
long FUN_00405530(long *plParm1);
void FUN_00405550(long *plParm1);
void FUN_00405640(long lParm1);
long FUN_00405660(undefined8 *puParm1,long **pplParm2,long lParm3);
long FUN_00405720(long *plParm1,long lParm2);
long * FUN_00405790(void);
void FUN_00405800(undefined8 *puParm1);
undefined * FUN_00405830(long lParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00405890(long lParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00405910(char *pcParm1);
undefined8 FUN_00405b50(ulong uParm1,undefined8 uParm2,long lParm3,undefined8 uParm4);
void FUN_00405c00(undefined8 *puParm1,int iParm2,uint uParm3);
void FUN_00405c80(long lParm1,undefined8 uParm2);
undefined8 FUN_00405cb0(char *pcParm1,ulong uParm2);
void FUN_00405d30(void);
ulong FUN_00405df0(uint *puParm1,char *pcParm2,undefined8 uParm3,undefined8 uParm4,ulong uParm5);
undefined8 FUN_00405e30(long lParm1,long lParm2,undefined8 uParm3);
ulong FUN_00405e90(long **pplParm1,undefined8 uParm2);
void FUN_00405f50(long *plParm1,long lParm2,uint uParm3);
ulong FUN_00406440(int iParm1,int iParm2);
void FUN_00406480(undefined8 uParm1,byte *pbParm2,long lParm3);
void FUN_004064d0(undefined8 uParm1,byte *pbParm2,long lParm3);
char * FUN_00406520(long lParm1,char *pcParm2,uint *puParm3,byte bParm4,undefined8 uParm5,undefined8 uParm6,uint uParm7);
void FUN_00407eb0(void);
undefined8 FUN_00407ed0(uint uParm1);
long FUN_00407f30(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00408110(ulong uParm1);
ulong FUN_00408180(ulong uParm1);
undefined * FUN_004081f0(long *plParm1,uint uParm2);
undefined8 FUN_00408220(float **ppfParm1);
ulong FUN_004082a0(float fParm1,ulong uParm2,char cParm3);
void FUN_00408330(undefined8 *puParm1,undefined8 *puParm2);
long FUN_00408350(long lParm1,long lParm2,long **pplParm3,char cParm4);
void FUN_00408450(long *plParm1);
undefined8 FUN_00408470(long lParm1,long **pplParm2,char cParm3);
long FUN_004085b0(long lParm1,long lParm2);
long * FUN_00408650(undefined8 uParm1,undefined1 *puParm2,undefined *puParm3,undefined *puParm4,long lParm5);
void FUN_00408750(long **pplParm1);
undefined * FUN_00408830(long *plParm1,undefined8 uParm2);
undefined * FUN_004089b0(long lParm1,undefined8 *puParm2,long *plParm3);
undefined8 FUN_00408c00(undefined8 uParm1,undefined8 uParm2);
long FUN_00408c40(long lParm1,undefined8 uParm2);
void FUN_00408e20(void);
void FUN_00408ef0(long lParm1,ulong uParm2,byte *pbParm3,undefined8 uParm4);
long FUN_00408fb0(void);
undefined8 FUN_00408fe0(char *pcParm1,undefined8 *puParm2,uint *puParm3);
char * FUN_00409110(ulong uParm1,long lParm2,ulong uParm3,ulong uParm4,ulong uParm5);
undefined8 FUN_00409850(undefined8 uParm1,undefined8 uParm2,long *plParm3);
long * FUN_004098a0(long lParm1);
char * FUN_004099b0(ulong uParm1,long lParm2);
int * FUN_00409db0(long lParm1);
int * FUN_00409e60(int *piParm1,undefined8 *puParm2);
ulong FUN_0040a1b0(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8);
ulong FUN_0040a270(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9);
ulong FUN_0040a340(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9,byte param_10);
undefined8 FUN_0040a430(byte *param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined * FUN_0040a4e0(char *pcParm1,int iParm2);
undefined8 *FUN_0040a5b0(undefined *puParm1,undefined8 *puParm2,long lParm3,undefined *puParm4,ulong uParm5,uint uParm6,long lParm7,char *pcParm8,char *pcParm9);
undefined1 * FUN_0040b6a0(undefined1 *puParm1,undefined8 *puParm2,undefined8 uParm3,uint *puParm4);
ulong FUN_0040b840(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
void FUN_0040b880(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040b8b0(ulong uParm1,undefined8 uParm2);
void FUN_0040b950(uint uParm1,undefined8 uParm2,undefined8 uParm3);
undefined1 * FUN_0040b9e0(undefined1 *puParm1,undefined8 *puParm2);
undefined1 * FUN_0040b9f0(undefined8 *puParm1);
ulong FUN_0040ba30(long lParm1,int iParm2,long lParm3,int iParm4);
void FUN_0040ba50(undefined8 uParm1,long lParm2,long lParm3,long lParm4,long lParm5);
void FUN_0040bca0(void);
void FUN_0040bd00(void);
void FUN_0040bd90(long lParm1);
void FUN_0040bdb0(long lParm1);
long FUN_0040bdc0(long lParm1,ulong *puParm2);
long FUN_0040be00(long lParm1,ulong *puParm2,ulong uParm3);
long FUN_0040be30(long lParm1,ulong *puParm2,ulong uParm3);
void FUN_0040bed0(undefined8 uParm1);
void FUN_0040bef0(ulong uParm1,ulong uParm2);
void FUN_0040bf50(undefined8 uParm1);
void FUN_0040bf70(void);
void FUN_0040bfa0(undefined8 uParm1,uint uParm2);
ulong FUN_0040bff0(long lParm1,long lParm2);
undefined8 FUN_0040c020(long *plParm1,int iParm2);
ulong FUN_0040c070(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_0040c0a0(char *pcParm1,char **ppcParm2,uint uParm3,undefined8 *puParm4,long lParm5);
ulong FUN_0040c3c0(int iParm1);
ulong FUN_0040c3e0(ulong *puParm1,int iParm2);
ulong FUN_0040c410(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_0040c440(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined *FUN_0040c780(int iParm1,undefined8 *puParm2,undefined uParm3,long lParm4,undefined8 uParm5,ulong uParm6);
ulong FUN_0040c7f0(int iParm1,undefined8 uParm2,char cParm3,undefined8 uParm4,undefined8 uParm5);
ulong FUN_0040c810(int iParm1);
ulong FUN_0040c830(ulong *puParm1,int iParm2);
ulong FUN_0040c860(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_0040c890(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_0040cbd0(ulong uParm1,ulong uParm2);
void FUN_0040cc20(undefined8 uParm1);
ulong FUN_0040cc60(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_0040ccd0(void);
void FUN_0040ccf0(void);
void FUN_0040cd20(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_0040ce00(undefined8 uParm1);
void FUN_0040ce90(undefined8 uParm1);
ulong FUN_0040cea0(long lParm1);
int * FUN_0040cf30(int *piParm1);
char * FUN_0040d030(char *pcParm1);
undefined8 FUN_0040d140(int *piParm1);
undefined8 FUN_0040d1b0(int iParm1,long lParm2,ulong uParm3,ulong uParm4,ulong uParm5,uint uParm6);
ulong FUN_0040d7a0(uint *puParm1,uint *puParm2,uint *puParm3,bool bParm4,uint uParm5);
undefined8 FUN_0040e150(uint uParm1,long lParm2,ulong uParm3,ulong uParm4,ulong uParm5,uint uParm6);
ulong FUN_0040e6b0(byte *pbParm1,byte *pbParm2,byte *pbParm3,byte bParm4,uint uParm5);
ulong FUN_0040f090(undefined8 uParm1,long lParm2,ulong uParm3);
undefined8 FUN_0040f330(void);
long FUN_0040f3d0(long *plParm1);
undefined * FUN_0040f420(long lParm1,undefined8 *puParm2);
ulong FUN_0040f450(long lParm1,long lParm2,char cParm3);
long FUN_0040f5f0(long lParm1,long lParm2,ulong uParm3);
long FUN_0040f6f0(long lParm1,undefined8 uParm2,long lParm3);
void FUN_0040f790(long lParm1);
void FUN_0040f7e0(undefined8 uParm1);
long FUN_0040f820(undefined8 uParm1,undefined8 uParm2,uint uParm3,uint *puParm4);
undefined * FUN_0040f880(long lParm1);
ulong FUN_0040f990(void);
ulong FUN_0040f9c0(void);
undefined * FUN_0040fa30(long lParm1,ulong uParm2,char cParm3);
ulong FUN_0040faa0(long lParm1);
void FUN_0040fb10(undefined4 *puParm1,int iParm2);
void thunk_FUN_0040fb5b(long lParm1,long lParm2,long lParm3);
void FUN_0040fb5b(long lParm1,long lParm2,long lParm3);
undefined8 FUN_0040fbb0(long *plParm1,ulong *puParm2,long lParm3);
ulong FUN_0040fc20(ulong uParm1,ulong *puParm2);
void FUN_0040fc80(uint param_1,uint param_2,undefined8 param_3,ulong param_4);
ulong FUN_0040fcd0(long lParm1,long lParm2,uint uParm3,char *pcParm4);
void FUN_0040fec0(ulong uParm1,long **pplParm2);
void FUN_0040fef0(undefined8 *puParm1,long lParm2);
undefined8 FUN_0040ff80(ulong uParm1,undefined8 *puParm2,undefined8 *puParm3);
long ** FUN_00410030(ulong uParm1,long **pplParm2,long lParm3);
long FUN_004100d0(long *plParm1,int iParm2);
long * FUN_00410980(long *plParm1,uint uParm2,long lParm3);
ulong FUN_00410d20(long *plParm1);
undefined8 * FUN_00410ec0(undefined8 *puParm1,undefined8 *puParm2);
undefined8 FUN_004115a0(undefined8 uParm1,long lParm2,uint uParm3);
ulong FUN_004115d0(long *plParm1,ulong *puParm2,int iParm3,long lParm4);
void FUN_00411720(long lParm1,int *piParm2);
ulong FUN_00411800(int param_1,undefined8 *param_2,char *param_3,char **param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_00411d90(char *pcParm1,int *piParm2,int iParm3);
ulong FUN_00411e50(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,uint *param_7,uint param_8);
void FUN_00412310(void);
void FUN_00412370(void);
void FUN_00412390(void);
undefined8 FUN_004123b0(long lParm1,long lParm2);
void FUN_00412420(long lParm1);
ulong FUN_00412440(uint *puParm1,byte *pbParm2,long lParm3);
ulong * FUN_004124b0(ulong *puParm1,undefined8 uParm2,ulong uParm3);
void FUN_00412590(char *pcParm1);
undefined8 FUN_00412620(void);
void FUN_00412630(undefined8 *puParm1);
byte * FUN_00412680(void);
void FUN_00412e30(long lParm1,long lParm2);
ulong FUN_00412e70(int iParm1);
void FUN_00412e90(long lParm1,long lParm2);
void FUN_00412ed0(ulong *puParm1,undefined4 uParm2);
ulong FUN_00412ee0(long lParm1,long lParm2);
void FUN_00412f00(ulong *puParm1);
void FUN_00412f20(long lParm1,long lParm2);
void FUN_00412f40(long lParm1,long lParm2);
ulong FUN_00412f60(long lParm1,long lParm2);
ulong FUN_00412fb0(long lParm1,long lParm2);
void FUN_00412fe0(undefined8 uParm1,undefined8 uParm2,undefined8 *puParm3,long lParm4,byte bParm5,long lParm6);
void FUN_00413050(long *plParm1);
ulong FUN_004130b0(long lParm1,long lParm2);
long FUN_004131f0(long *plParm1,long lParm2,uint uParm3);
undefined8 FUN_00413260(char *pcParm1,long lParm2,ulong uParm3);
ulong FUN_004133b0(long lParm1,uint uParm2,long lParm3,long lParm4,long lParm5);
ulong FUN_004135e0(long lParm1,long lParm2,long lParm3,long lParm4,long lParm5,long lParm6);
ulong FUN_00413650(long *plParm1);
void FUN_004136e0(long *plParm1,code *pcParm2,undefined8 uParm3);
void FUN_00413750(long *plParm1,code *pcParm2,undefined8 uParm3);
undefined8 FUN_004137b0(long lParm1,ulong uParm2);
void FUN_00413850(long param_1,undefined8 param_2,long param_3,long param_4,undefined8 param_5,byte param_6,long param_7);
undefined8 FUN_00413900(long *plParm1,undefined8 uParm2);
void FUN_00413a40(undefined4 *puParm1);
void FUN_00413a50(undefined4 *puParm1);
void FUN_00413a60(undefined8 *puParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
undefined8 FUN_00413b10(undefined8 *puParm1,undefined8 uParm2);
ulong FUN_00413b70(long *plParm1,long lParm2);
undefined8 FUN_00413c70(long *plParm1,long lParm2);
undefined8 FUN_00413cc0(long *plParm1,ulong *puParm2,ulong uParm3);
undefined8 FUN_00413db0(long lParm1,undefined4 uParm2,long lParm3);
undefined8 FUN_00413e50(long lParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00413f10(long lParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_00413fa0(long *plParm1,ulong uParm2);
ulong FUN_00414170(ulong uParm1,long lParm2);
void FUN_004141a0(long lParm1);
void FUN_00414220(long *plParm1);
long FUN_004143e0(long *plParm1,long lParm2,uint *puParm3);
undefined8 FUN_00414490(long *plParm1);
undefined8 FUN_00414ae0(undefined8 *puParm1,undefined8 uParm2,long lParm3,long lParm4,byte bParm5,long lParm6);
ulong FUN_00414c40(long *plParm1,int iParm2);
undefined8 FUN_00414d50(long lParm1,long lParm2);
undefined8 FUN_00414de0(long *plParm1,long lParm2);
void FUN_00414fb0(undefined4 *puParm1,undefined *puParm2);
ulong FUN_00414fd0(long lParm1,long lParm2,ulong uParm3);
ulong FUN_004150b0(long lParm1,undefined8 *puParm2,long lParm3);
void FUN_004151d0(long lParm1);
void FUN_004152d0(undefined8 *puParm1);
void FUN_004152f0(undefined8 *puParm1);
long FUN_00415340(long *plParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004155a0(long *plParm1,long lParm2,ulong uParm3);
undefined8 FUN_00415630(long *plParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
ulong FUN_00415870(undefined4 *puParm1,long *plParm2,long lParm3,char cParm4);
undefined8 FUN_00415ae0(long lParm1);
undefined8 FUN_00415bc0(long *plParm1);
void FUN_00415db0(long lParm1);
void FUN_00415e10(long lParm1);
void FUN_00415e50(long *plParm1);
undefined8 FUN_00415fd0(long lParm1,undefined8 uParm2,long lParm3,long lParm4,long lParm5);
void FUN_00416100(long lParm1);
undefined8 FUN_004161c0(long *plParm1);
void FUN_00416230(long lParm1);
undefined8 FUN_00416420(ulong uParm1,undefined8 uParm2,long lParm3,long *plParm4,int *piParm5,int *piParm6);
long * FUN_004166f0(long **pplParm1);
undefined8 FUN_00416870(byte **ppbParm1,byte *pbParm2,ulong uParm3);
undefined8 FUN_00416f90(void);
long FUN_00416fa0(long lParm1,long *plParm2,long lParm3);
undefined8 FUN_00417000(long *plParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_00417100(long *plParm1,long *plParm2,long lParm3);
long FUN_00417140(long lParm1,long **pplParm2,uint uParm3);
ulong FUN_00417170(long lParm1,long lParm2,ulong uParm3);
long FUN_004171f0(long *plParm1,long lParm2,long *plParm3,long lParm4,uint uParm5);
ulong FUN_00417240(long lParm1,undefined4 *puParm2,undefined8 uParm3,ulong uParm4);
long FUN_00417340(long lParm1,long *plParm2,long lParm3);
undefined8 FUN_004173b0(long param_1,long *param_2,long *param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6,undefined8 param_7);
undefined8 FUN_004174c0(undefined4 *puParm1,long *plParm2,long lParm3,char *pcParm4);
long FUN_004175b0(undefined8 *puParm1,int *piParm2,long *plParm3,long *plParm4,undefined *puParm5);
long ** FUN_00417690(long **pplParm1,long lParm2);
void FUN_00417740(long lParm1,undefined8 uParm2,undefined8 uParm3,undefined uParm4);
long FUN_00417770(undefined4 *puParm1,long *plParm2,long lParm3);
ulong FUN_00417910(undefined8 uParm1,long lParm2);
undefined8 FUN_00417990(long lParm1,long *plParm2,long *plParm3,undefined8 uParm4);
undefined8 FUN_00417a40(long *plParm1,long *plParm2,undefined8 *puParm3);
undefined8 FUN_00417ad0(long *plParm1,long lParm2,long lParm3);
undefined8 FUN_00417c70(long *plParm1,undefined8 uParm2,undefined8 uParm3,long lParm4,undefined8 uParm5,long lParm6);
long FUN_00417d60(long *plParm1,long lParm2,ulong uParm3,undefined8 uParm4);
ulong * FUN_00417fa0(undefined4 *puParm1,long lParm2,long lParm3,ulong uParm4);
ulong FUN_00418080(long *plParm1);
long FUN_00418270(long *plParm1,long lParm2,undefined8 uParm3);
ulong * FUN_004183e0(undefined4 *puParm1,long lParm2,long lParm3);
ulong FUN_004184a0(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_00418550(long lParm1,long lParm2,long lParm3,undefined8 uParm4,uint uParm5);
long FUN_004187e0(long *plParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_00418ce0(long lParm1,long lParm2);
undefined8 FUN_00419200(long *plParm1,long *plParm2,long *plParm3,long *plParm4,long *plParm5);
ulong FUN_004193c0(long lParm1,long lParm2,long lParm3);
ulong FUN_004194b0(long *plParm1,long lParm2,long lParm3,long lParm4);
void FUN_004196a0(long lParm1,long lParm2,long lParm3,long lParm4,long lParm5,long lParm6);
undefined8 FUN_00419740(long lParm1,long lParm2,long *plParm3,undefined8 uParm4);
void FUN_004197c0(long lParm1);
undefined8 FUN_00419820(long *param_1,long param_2,undefined8 param_3,long *param_4,long *param_5,long param_6,long param_7);
undefined8 FUN_00419aa0(long *plParm1,long *plParm2,undefined8 *puParm3,long lParm4,undefined8 uParm5,undefined4 *puParm6);
undefined8 FUN_00419b60(undefined8 uParm1,byte *pbParm2);
undefined8 FUN_00419ba0(long param_1,undefined8 param_2,long *param_3,ulong *param_4,ulong *param_5,byte *param_6,ulong param_7);
long FUN_0041a2c0(long lParm1,undefined8 uParm2,undefined8 uParm3,char *pcParm4,byte bParm5,int *piParm6);
void FUN_0041a4e0(long **pplParm1,long *plParm2,long *plParm3,undefined4 *puParm4);
ulong FUN_0041a9c0(long lParm1,ulong *puParm2,long lParm3,long lParm4,long lParm5);
long FUN_0041ac90(long param_1,undefined8 param_2,long param_3,long *param_4,long param_5,long param_6,long param_7);
ulong FUN_0041afe0(undefined8 *puParm1,long lParm2,long lParm3,long *plParm4,char cParm5);
ulong FUN_0041b2f0(long lParm1,long lParm2,long *plParm3,long *plParm4,undefined8 uParm5);
ulong FUN_0041b540(long param_1,long *param_2,long param_3,long param_4,undefined8 param_5,long param_6,uint param_7);
undefined8 FUN_0041ba30(long lParm1,long *plParm2,undefined8 *puParm3,undefined8 uParm4,long lParm5);
undefined8 FUN_0041bab0(long lParm1,long lParm2,long lParm3);
ulong FUN_0041be40(long lParm1,long *plParm2,long *plParm3);
long FUN_0041c1f0(int *piParm1,long lParm2,long lParm3);
long FUN_0041c3b0(int *piParm1,long lParm2);
ulong FUN_0041c420(long lParm1,long *plParm2,long *plParm3);
undefined8 FUN_0041c6c0(int *piParm1,long lParm2,long lParm3);
long FUN_0041c790(long lParm1,char cParm2,long *plParm3);
ulong FUN_0041cb30(long lParm1,long *plParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_0041cbc0(long lParm1,long *plParm2,long lParm3,undefined8 uParm4);
ulong FUN_0041cd50(long lParm1,long *plParm2,long lParm3,long *plParm4,long *plParm5);
ulong FUN_0041d0e0(long lParm1,long *plParm2,long lParm3,long lParm4);
ulong FUN_0041d290(long lParm1,long *plParm2);
ulong FUN_0041d3a0(long lParm1);
ulong FUN_0041d5e0(long *param_1,long param_2,long param_3,long param_4,long param_5,undefined8 param_6,ulong param_7,ulong *param_8,uint param_9);
undefined8 FUN_0041e020(undefined4 *puParm1,long lParm2,undefined *puParm3,int iParm4,undefined8 uParm5,char cParm6);
long FUN_0041e100(long lParm1,long lParm2,long lParm3,ulong uParm4,int *piParm5);
ulong FUN_0041e6b0(long *plParm1);
undefined8 FUN_0041e740(long *plParm1,long lParm2,ulong uParm3);
void FUN_0041edb0(undefined8 uParm1,long lParm2);
long FUN_0041edd0(undefined8 uParm1,byte *pbParm2,undefined8 uParm3);
long FUN_0041ee80(long lParm1,long lParm2,long lParm3,undefined4 *puParm4,ulong uParm5,undefined4 *puParm6);
long FUN_0041f270(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,undefined8 uParm5,int *piParm6);
long FUN_0041f380(long lParm1,long *plParm2,undefined8 *puParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_0041f8a0(undefined8 uParm1,undefined8 *puParm2,long lParm3,undefined8 uParm4,long lParm5,int *piParm6);
long FUN_0041f9f0(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_0041fb40(undefined8 uParm1,long *plParm2,ulong uParm3,int *piParm4);
ulong FUN_0041fc20(long *plParm1,undefined8 uParm2,undefined8 uParm3,ulong uParm4);
undefined8 FUN_0041fdc0(long *plParm1);
ulong FUN_0041fe80(undefined8 *puParm1,undefined8 uParm2,ulong uParm3);
ulong FUN_0041ffa0(long lParm1,undefined8 uParm2,undefined8 uParm3,undefined8 *puParm4,ulong uParm5);
void FUN_00420040(void);
undefined8 FUN_00420060(long lParm1,long lParm2);
ulong FUN_004200c0(long lParm1,ulong uParm2,long *plParm3);
long FUN_00420210(long lParm1,long lParm2,long lParm3,ulong uParm4);
char * FUN_00420750(char *pcParm1,char *pcParm2);
ulong FUN_00420830(uint uParm1,uint uParm2);
ulong FUN_00420850(uint *puParm1,uint *puParm2);
void FUN_004208a0(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_004208b0(void);
ulong FUN_004208c0(char *pcParm1);
ulong FUN_004208f0(long lParm1);
undefined8 * FUN_00420930(long lParm1);
undefined8 FUN_004209d0(long *plParm1,char *pcParm2);
void FUN_00420b10(long *plParm1);
long FUN_00420b40(long lParm1);
ulong FUN_00420bf0(long lParm1);
undefined8 FUN_00420c40(long lParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_00420ce0(long lParm1,undefined8 uParm2);
long FUN_00420dc0(uint *puParm1);
void FUN_00420de0(void);
ulong FUN_00420ec0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined FUN_00420fa0(int iParm1);;
ulong FUN_00420fb0(uint uParm1);
ulong FUN_00420fc0(byte *pbParm1,byte *pbParm2);
ulong FUN_00421080(ulong uParm1);
void FUN_00421090(long lParm1);
undefined8 FUN_004210a0(long *plParm1,long *plParm2);
undefined8 FUN_004211a0(void);
ulong FUN_004211b0(ulong uParm1);
void FUN_00421250(undefined4 *puParm1,undefined4 uParm2);
ulong FUN_00421270(long lParm1);
ulong FUN_00421280(long lParm1,uint uParm2);
undefined * FUN_004212c0(long lParm1,undefined8 *puParm2);
undefined * FUN_00421310(void);
char * FUN_00421670(void);
void FUN_00421750(void);
ulong FUN_004217a0(uint uParm1);
ulong FUN_004217e0(ulong param_1,undefined8 param_2,ulong param_3);
undefined8 FUN_00421930(ulong uParm1);
void FUN_004219a0(void);
ulong FUN_004219c0(void);
ulong FUN_004219e0(ulong uParm1,long lParm2,long lParm3,ulong uParm4);
ulong FUN_00421aa0(long *plParm1,ulong *puParm2,long lParm3);
long FUN_00421ab0(long lParm1,undefined4 uParm2);
ulong FUN_00421ac0(ulong uParm1);
ulong FUN_00421b40(uint uParm1,uint uParm2);
long FUN_00421b60(ulong param_1,long param_2,undefined8 param_3,int param_4,int param_5,uint param_6,int param_7,int param_8,int param_9,int param_10);
long FUN_00421cb0(undefined8 uParm1,undefined8 uParm2);
long FUN_00421ce0(void);
void FUN_00421d90(code *pcParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
long FUN_00421db0(code *pcParm1,long *plParm2,undefined8 uParm3,undefined8 uParm4);
long FUN_00422210(uint *puParm1);
void FUN_00422230(void);
ulong FUN_00422240(uint param_1,undefined8 param_2,uint param_3,ulong param_4);
ulong FUN_00422340(char *pcParm1,char *pcParm2,ulong uParm3);
ulong FUN_00422470(char *pcParm1,long lParm2);
char * FUN_004224b0(ulong uParm1,long lParm2,long lParm3);
ulong FUN_00422660(void);
undefined * FUN_004226a0(undefined8 param_1,undefined8 *param_2);
int * FUN_00422910(int *param_1,ulong *param_2);
undefined * FUN_00422aa0(undefined8 uParm1,undefined8 *puParm2);
uint * FUN_00422cb0(undefined8 uParm1,int *piParm2,ulong *puParm3);
void FUN_00422e20(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5,undefined8 *puParm6);
int * FUN_00422f60(undefined8 *puParm1,long lParm2,undefined8 *puParm3,undefined8 *puParm4,uint **ppuParm5,uint **ppuParm6);
undefined8 FUN_00423510(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_00423960(uint param_1);
void FUN_004239a0(uint uParm1);
ulong FUN_00428930(ulong uParm1,char cParm2);
undefined8 * FUN_004289a0(ulong uParm1);
void FUN_00428a20(ulong uParm1);
double FUN_00428ab0(int *piParm1);
void FUN_00428b00(int *param_1);
ulong FUN_00428b80(ulong uParm1);
long FUN_00428b90(ulong uParm1,ulong uParm2);
long FUN_00428bb0(void);
undefined8 FUN_00428be0(uint *puParm1,ulong *puParm2);
undefined8 FUN_00428e00(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_00429ad0(undefined auParm1 [16]);
ulong FUN_00429af0(void);
void FUN_00429b20(void);
undefined8 _DT_FINI(void);

