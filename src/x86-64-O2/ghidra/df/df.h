typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void entry(void);
void FUN_00402df0(void);
void FUN_00402e60(void);
void FUN_00402ef0(void);
ulong FUN_00402f30(uint uParm1);
void FUN_00402f40(void);
undefined FUN_00402fb0(ulong uParm1);;
void FUN_00402fc0(undefined8 *puParm1,undefined8 *puParm2,long lParm3);
void FUN_004030a0(void);
void FUN_004030c0(void);
ulong FUN_004030e0(long lParm1);
ulong FUN_00403130(long lParm1);
void FUN_00403190(undefined8 uParm1);
void FUN_004031f0(int iParm1,long lParm2);
char * FUN_004032b0(uint uParm1,ulong uParm2,undefined8 uParm3,ulong uParm4,ulong uParm5);
char * FUN_00403320(char *pcParm1);
void FUN_00403370(void);
undefined8 * FUN_004035c0(undefined8 uParm1);
void FUN_00403630(undefined8 uParm1);
ulong FUN_00403660(char cParm1);
long FUN_00403980(void);
ulong FUN_004039a0(long lParm1);
void FUN_004039d0(void);
void FUN_00403b70(long *plParm1,long lParm2);
void FUN_00403bf0(char *param_1,char *param_2,undefined *param_3,char *param_4,undefined *param_5,char param_6,uint param_7,undefined *param_8,char param_9);
ulong FUN_00404450(char *pcParm1);
void FUN_004046b0(undefined8 uParm1,ulong *puParm2);
void FUN_004049f0(undefined8 uParm1,ulong *puParm2);
void FUN_00404a40(void);
void FUN_00404aa0(void);
void FUN_00404c70(void);
void FUN_00404c90(uint uParm1);
void FUN_00404dc0(void);
undefined8 FUN_00404ee0(undefined8 uParm1,undefined8 uParm2);
char * FUN_00405090(undefined8 uParm1,undefined *puParm2);
char * FUN_004053d0(char **ppcParm1,undefined8 uParm2,undefined8 uParm3);
char * FUN_00405450(char *pcParm1,ulong uParm2);
char * FUN_004059e0(char *pcParm1);
void FUN_00405a90(char *pcParm1);
void FUN_00405ab0(char *pcParm1);
undefined * FUN_00405af0(undefined8 uParm1);
char * FUN_00405b60(char *pcParm1);
undefined8 * FUN_00405bc0(long lParm1,undefined8 uParm2,undefined8 *puParm3);
ulong FUN_00405c50(long lParm1,undefined8 uParm2,undefined8 *puParm3);
ulong FUN_00405c90(ulong uParm1);
ulong FUN_00405d00(ulong uParm1);
ulong FUN_00405d70(long *plParm1,ulong uParm2);
undefined8 FUN_00405da0(float **ppfParm1);
ulong FUN_00405e20(float fParm1,ulong uParm2,char cParm3);
void FUN_00405eb0(undefined8 *puParm1,undefined8 *puParm2);
long FUN_00405ed0(long lParm1,long lParm2,long **pplParm3,char cParm4);
void FUN_00405fd0(long *plParm1);
undefined8 FUN_00405ff0(long lParm1,long **pplParm2,char cParm3);
long FUN_00406130(long lParm1,long lParm2);
long * FUN_00406190(undefined8 uParm1,undefined1 *puParm2,undefined *puParm3,undefined *puParm4,long lParm5);
void FUN_00406290(long **pplParm1);
ulong FUN_00406370(long *plParm1,undefined8 uParm2);
ulong FUN_004064f0(ulong uParm1,undefined8 *puParm2,long *plParm3);
undefined8 FUN_00406740(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00406780(undefined8 *puParm1,ulong uParm2);
void FUN_004067f0(undefined8 *puParm1);
void FUN_00406810(void);
void FUN_004068e0(long lParm1,ulong uParm2,byte *pbParm3,undefined8 uParm4);
long FUN_004069a0(void);
undefined8 FUN_004069d0(char *pcParm1,undefined8 *puParm2,uint *puParm3);
char * FUN_00406b00(ulong uParm1,long lParm2,ulong uParm3,ulong uParm4,ulong uParm5);
undefined8 FUN_00407240(undefined8 uParm1,undefined8 uParm2,long *plParm3);
char * FUN_00407270(ulong uParm1,long lParm2);
void FUN_004072c0(undefined *puParm1,undefined *puParm2,long lParm3);
undefined8 FUN_004072f0(int *piParm1);
ulong FUN_00407340(int *piParm1,ulong uParm2);
long FUN_004073a0(long lParm1,long lParm2,long lParm3,ulong *puParm4,int iParm5,ulong uParm6);
long FUN_004076f0(undefined8 uParm1,ulong *puParm2,ulong uParm3,uint uParm4);
ulong FUN_00407970(byte *pbParm1,ulong uParm2);
int * FUN_004079a0(long lParm1);
int * FUN_00407a50(int *piParm1,undefined8 *puParm2);
ulong FUN_00407da0(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8);
ulong FUN_00407e60(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9);
ulong FUN_00407f30(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9,byte param_10);
undefined8 FUN_00408020(byte *param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined * FUN_004080d0(char *pcParm1,int iParm2);
undefined8 *FUN_004081a0(undefined *puParm1,undefined8 *puParm2,long lParm3,undefined *puParm4,ulong uParm5,uint uParm6,long lParm7,char *pcParm8,char *pcParm9);
undefined1 * FUN_00409290(undefined1 *puParm1,undefined8 *puParm2,undefined8 uParm3,uint *puParm4);
ulong FUN_00409430(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
void FUN_004094a0(ulong uParm1,undefined8 uParm2);
void FUN_00409540(uint uParm1,undefined8 uParm2,undefined8 uParm3);
undefined1 * FUN_004095e0(undefined8 *puParm1);
ulong FUN_004095f0(int *piParm1);
ulong FUN_00409640(uint *puParm1);
void FUN_00409660(int *piParm1);
ulong FUN_00409680(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long *plParm5,undefined8 uParm6);
void FUN_004098d0(void);
void FUN_00409930(void);
void FUN_004099c0(ulong uParm1,ulong uParm2);
void FUN_004099e0(ulong uParm1,ulong uParm2);
void FUN_00409a10(ulong uParm1,ulong uParm2);
long FUN_00409a20(long lParm1,ulong uParm2);
long FUN_00409a60(long lParm1,ulong uParm2,ulong uParm3);
void FUN_00409ac0(undefined8 uParm1);
ulong FUN_00409ae0(void);
ulong FUN_00409b10(void);
ulong FUN_00409b40(int iParm1,undefined8 *puParm2,undefined uParm3,long lParm4,undefined8 uParm5,ulong uParm6);
ulong FUN_00409bb0(int iParm1,undefined8 uParm2,char cParm3,undefined8 uParm4,undefined8 uParm5);
ulong FUN_00409bd0(int iParm1);
ulong FUN_00409bf0(ulong *puParm1,int iParm2);
ulong FUN_00409c20(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_00409c50(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
void FUN_00409f90(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0040a020(ulong uParm1,ulong uParm2);
long FUN_0040a070(long lParm1);
void FUN_0040a090(undefined4 *puParm1);
void FUN_0040a0a0(void);
void FUN_0040a0b0(int iParm1);
undefined8 FUN_0040a0f0(uint *puParm1,undefined8 uParm2);
void FUN_0040a3c0(undefined8 uParm1);
ulong FUN_0040a400(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_0040a470(void);
void FUN_0040a490(void);
void FUN_0040a4c0(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0040a5a0(void);
undefined8 FUN_0040a5b0(undefined8 uParm1);
long FUN_0040a630(long lParm1,ulong uParm2);
void FUN_0040aab0(long lParm1,int *piParm2);
ulong FUN_0040ab90(int param_1,undefined8 *param_2,char *param_3,char **param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_0040b120(char *pcParm1,int *piParm2,int iParm3);
ulong FUN_0040b1e0(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,uint *param_7,uint param_8);
void FUN_0040b6a0(void);
void FUN_0040b700(void);
void FUN_0040b720(void);
undefined8 FUN_0040b740(long lParm1,long lParm2);
void FUN_0040b7b0(long lParm1);
ulong FUN_0040b7d0(uint *puParm1,byte *pbParm2,long lParm3);
ulong * FUN_0040b840(ulong *puParm1,undefined8 uParm2,ulong uParm3);
void FUN_0040b920(char *pcParm1);
undefined8 FUN_0040b9b0(void);
void FUN_0040b9c0(undefined8 *puParm1);
byte * FUN_0040ba10(void);
void FUN_0040c1c0(void);
ulong FUN_0040c1d0(uint param_1,undefined8 param_2,uint param_3,ulong param_4);
void FUN_0040c2d0(long lParm1,long lParm2);
void FUN_0040c310(void);
undefined8 FUN_0040c330(long lParm1,long lParm2);
ulong FUN_0040c390(long lParm1,ulong uParm2,long *plParm3);
long FUN_0040c4e0(long lParm1,long lParm2,long lParm3,ulong uParm4);
char * FUN_0040ca20(char *pcParm1,char *pcParm2);
ulong FUN_0040cb00(long *plParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040cb60(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040cc20(int *piParm1,long lParm2);
ulong FUN_0040ce80(long param_1,char param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined8 FUN_0040cee0(char *pcParm1);
ulong FUN_0040cf20(ulong uParm1);
long FUN_0040cf60(undefined8 uParm1,ulong uParm2);
long FUN_0040d060(undefined8 uParm1,long *plParm2,long lParm3,long lParm4);
undefined FUN_0040d190(int iParm1);;
ulong FUN_0040d1a0(uint uParm1);
ulong FUN_0040d1b0(byte *pbParm1,byte *pbParm2);
ulong FUN_0040d200(ulong uParm1,char cParm2);
void FUN_0040d2e0(undefined8 param_1,byte param_2,ulong param_3);
undefined8 FUN_0040d330(void);
ulong FUN_0040d340(ulong uParm1);
ulong FUN_0040d3e0(char *pcParm1,ulong uParm2);
undefined * FUN_0040d420(void);
char * FUN_0040d780(void);
ulong FUN_0040d840(uint uParm1);
ulong FUN_0040da30(long param_1,long param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10);
undefined8 FUN_0040daa0(char *param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8);
ulong FUN_0040dae0(undefined8 uParm1);
void FUN_0040de30(undefined8 uParm1);
ulong FUN_0040dec0(ulong param_1,undefined8 param_2,ulong param_3);
undefined8 FUN_0040e010(ulong uParm1);
void FUN_0040e080(undefined8 uParm1);
ulong FUN_0040e090(long lParm1);
undefined8 FUN_0040e120(void);
void FUN_0040e140(void);
ulong FUN_0040e160(void);
ulong FUN_0040e180(ulong uParm1,long lParm2,long lParm3,ulong uParm4);
ulong FUN_0040e240(long *plParm1,ulong *puParm2,long lParm3);
undefined8 FUN_0040e250(long lParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040e2c0(char *pcParm1,long lParm2);
char * FUN_0040e300(ulong uParm1,long lParm2,long lParm3);
ulong FUN_0040e4b0(void);
ulong FUN_0040e4f0(undefined8 param_1,undefined8 *param_2);
int * FUN_0040e760(int *param_1,ulong *param_2);
ulong FUN_0040e8f0(undefined8 uParm1,undefined8 *puParm2);
uint * FUN_0040eb00(undefined8 uParm1,int *piParm2,ulong *puParm3);
void FUN_0040ec70(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5,undefined8 *puParm6);
int * FUN_0040edb0(undefined8 *puParm1,long lParm2,undefined8 *puParm3,undefined8 *puParm4,uint **ppuParm5,uint **ppuParm6);
undefined8 FUN_0040f360(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_0040f7b0(uint param_1);
void FUN_0040f7f0(uint uParm1);
double FUN_00414780(int *piParm1);
void FUN_004147d0(int *param_1);
ulong FUN_00414850(ulong uParm1);
long FUN_00414860(ulong uParm1,ulong uParm2);
long FUN_00414880(void);
ulong FUN_004148b0(long *plParm1,ulong *puParm2,int iParm3,long lParm4);
undefined8 FUN_00414a00(uint *puParm1,ulong *puParm2);
undefined8 FUN_00414c20(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_004158f0(undefined auParm1 [16]);
ulong FUN_00415910(void);
void FUN_00415940(void);
undefined8 _DT_FINI(void);

