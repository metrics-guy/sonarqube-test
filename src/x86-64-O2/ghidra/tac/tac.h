typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void entry(void);
void FUN_00402a10(void);
void FUN_00402a80(void);
void FUN_00402b10(void);
void FUN_00402b50(void);
void FUN_00402b70(void);
void FUN_00402b90(long lParm1,long lParm2);
undefined8 FUN_00402c80(undefined8 uParm1,undefined8 uParm2,ulong uParm3);
undefined8 thunk_FUN_00413f80(long lParm1);
undefined8 FUN_004031d0(long *plParm1,long *plParm2);
long FUN_00403390(undefined8 *puParm1,undefined8 *puParm2,ulong uParm3,undefined8 uParm4);
undefined8 FUN_004034b0(ulong uParm1,undefined8 uParm2);
ulong FUN_00403500(byte *pbParm1);
void FUN_00403620(void);
void FUN_004037c0(uint uParm1);
void FUN_00403920(char *pcParm1);
long FUN_00403940(long lParm1,char *pcParm2,undefined8 *puParm3);
int * FUN_00403a10(long lParm1);
int * FUN_00403ac0(int *piParm1,undefined8 *puParm2);
ulong FUN_00403e10(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8);
ulong FUN_00403ed0(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9);
ulong FUN_00403fa0(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9,byte param_10);
undefined8 FUN_00404090(byte *param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined * FUN_00404140(char *pcParm1,int iParm2);
undefined8 *FUN_00404210(undefined *puParm1,undefined8 *puParm2,long lParm3,undefined *puParm4,ulong uParm5,uint uParm6,long lParm7,char *pcParm8,char *pcParm9);
undefined1 * FUN_00405300(undefined1 *puParm1,undefined8 *puParm2,undefined8 uParm3,uint *puParm4);
ulong FUN_004054a0(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
void FUN_00405510(ulong uParm1,undefined8 uParm2);
void FUN_004055b0(uint uParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_00405660(uint uParm1,undefined8 uParm2,ulong uParm3);
ulong FUN_004056d0(void);
void FUN_00405730(undefined8 uParm1,long lParm2,long lParm3,long lParm4,long lParm5);
void FUN_00405980(void);
void FUN_004059e0(void);
void FUN_00405a70(long lParm1);
void FUN_00405a90(long lParm1);
long FUN_00405aa0(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
undefined8 FUN_00405ae0(void);
undefined8 FUN_00405b10(ulong uParm1,ulong uParm2);
void FUN_00405b60(undefined8 uParm1);
ulong FUN_00405ba0(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00405c10(void);
void FUN_00405c30(void);
void FUN_00405c60(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00405d40(void);
void FUN_00405d50(long lParm1,int *piParm2);
ulong FUN_00405e30(int param_1,undefined8 *param_2,char *param_3,char **param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_004063c0(char *pcParm1,int *piParm2,int iParm3);
ulong FUN_00406480(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,uint *param_7,uint param_8);
void FUN_00406940(void);
void FUN_004069a0(void);
void FUN_004069c0(long lParm1);
ulong FUN_004069e0(uint *puParm1,byte *pbParm2,long lParm3);
void FUN_00406a50(void);
void FUN_00406a60(long lParm1,long lParm2);
ulong FUN_00406aa0(int iParm1);
void FUN_00406ac0(long lParm1,long lParm2);
void FUN_00406b00(ulong *puParm1,undefined4 uParm2);
ulong FUN_00406b10(long lParm1,long lParm2);
void FUN_00406b30(ulong *puParm1);
void FUN_00406b50(long lParm1,long lParm2);
void FUN_00406b70(long lParm1,long lParm2);
ulong FUN_00406b90(long lParm1,long lParm2);
ulong FUN_00406be0(long lParm1,long lParm2);
void FUN_00406c10(undefined8 uParm1,undefined8 uParm2,undefined8 *puParm3,long lParm4,byte bParm5,long lParm6);
void FUN_00406c80(long *plParm1);
ulong FUN_00406ce0(long lParm1,long lParm2);
long FUN_00406e20(long *plParm1,long lParm2,uint uParm3);
undefined8 FUN_00406e90(char *pcParm1,long lParm2,ulong uParm3);
ulong FUN_00406fe0(long lParm1,uint uParm2,long lParm3,long lParm4,long lParm5);
ulong FUN_00407210(long lParm1,long lParm2,long lParm3,long lParm4,long lParm5,long lParm6);
ulong FUN_00407280(long *plParm1);
void FUN_00407310(long *plParm1,code *pcParm2,undefined8 uParm3);
void FUN_00407380(long *plParm1,code *pcParm2,undefined8 uParm3);
undefined8 FUN_004073e0(long lParm1,ulong uParm2);
void FUN_00407480(long param_1,undefined8 param_2,long param_3,long param_4,undefined8 param_5,byte param_6,long param_7);
undefined8 FUN_00407530(long *plParm1,undefined8 uParm2);
void FUN_00407670(undefined4 *puParm1);
void FUN_00407680(undefined4 *puParm1);
void FUN_00407690(undefined8 *puParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
undefined8 FUN_00407740(undefined8 *puParm1,undefined8 uParm2);
ulong FUN_004077a0(long *plParm1,long lParm2);
undefined8 FUN_004078a0(long *plParm1,long lParm2);
undefined8 FUN_004078f0(long *plParm1,ulong *puParm2,ulong uParm3);
undefined8 FUN_004079e0(long lParm1,undefined4 uParm2,long lParm3);
undefined8 FUN_00407a80(long lParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00407b40(long lParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_00407bd0(long *plParm1,ulong uParm2);
ulong FUN_00407da0(ulong uParm1,long lParm2);
void FUN_00407dd0(long lParm1);
void FUN_00407e50(long *plParm1);
long FUN_00408010(long *plParm1,long lParm2,uint *puParm3);
undefined8 FUN_004080c0(long *plParm1);
undefined8 FUN_00408710(undefined8 *puParm1,undefined8 uParm2,long lParm3,long lParm4,byte bParm5,long lParm6);
ulong FUN_00408870(long *plParm1,int iParm2);
undefined8 FUN_00408980(long lParm1,long lParm2);
undefined8 FUN_00408a10(long *plParm1,long lParm2);
void FUN_00408be0(undefined4 *puParm1,undefined *puParm2);
ulong FUN_00408c00(long lParm1,long lParm2,ulong uParm3);
ulong FUN_00408ce0(long lParm1,undefined8 *puParm2,long lParm3);
void FUN_00408e00(long lParm1);
void FUN_00408f00(undefined8 *puParm1);
void FUN_00408f20(undefined8 *puParm1);
long FUN_00408f70(long *plParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004091d0(long *plParm1,long lParm2,ulong uParm3);
undefined8 FUN_00409260(long *plParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
ulong FUN_004094a0(undefined4 *puParm1,long *plParm2,long lParm3,char cParm4);
undefined8 FUN_00409710(long lParm1);
undefined8 FUN_004097f0(long *plParm1);
void FUN_004099e0(long lParm1);
void FUN_00409a40(long lParm1);
void FUN_00409a80(long *plParm1);
undefined8 FUN_00409c00(long lParm1,undefined8 uParm2,long lParm3,long lParm4,long lParm5);
void FUN_00409d30(long lParm1);
undefined8 FUN_00409df0(long *plParm1);
void FUN_00409e60(long lParm1);
undefined8 FUN_0040a050(ulong *puParm1,long lParm2,ulong uParm3,int iParm4);
undefined8 FUN_0040a220(ulong uParm1,undefined8 uParm2,long lParm3,long *plParm4,int *piParm5,int *piParm6);
long * FUN_0040a4f0(long **pplParm1);
undefined8 FUN_0040a670(byte **ppbParm1,byte *pbParm2,ulong uParm3);
undefined8 FUN_0040ad90(void);
long FUN_0040ada0(long lParm1,long *plParm2,long lParm3);
undefined8 FUN_0040ae00(long *plParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_0040af00(long *plParm1,long *plParm2,long lParm3);
long FUN_0040af40(long lParm1,long **pplParm2,uint uParm3);
ulong FUN_0040af70(long lParm1,long lParm2,ulong uParm3);
long FUN_0040aff0(long *plParm1,long lParm2,long *plParm3,long lParm4,uint uParm5);
ulong FUN_0040b040(long lParm1,undefined4 *puParm2,undefined8 uParm3,ulong uParm4);
long FUN_0040b140(long lParm1,long *plParm2,long lParm3);
undefined8 FUN_0040b1b0(long param_1,long *param_2,long *param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6,undefined8 param_7);
undefined8 FUN_0040b2c0(undefined4 *puParm1,long *plParm2,long lParm3,char *pcParm4);
long FUN_0040b3b0(undefined8 *puParm1,int *piParm2,long *plParm3,long *plParm4,undefined *puParm5);
long ** FUN_0040b490(long **pplParm1,long lParm2);
void FUN_0040b540(long lParm1,undefined8 uParm2,undefined8 uParm3,undefined uParm4);
long FUN_0040b570(undefined4 *puParm1,long *plParm2,long lParm3);
ulong FUN_0040b710(undefined8 uParm1,long lParm2);
undefined8 FUN_0040b790(long lParm1,long *plParm2,long *plParm3,undefined8 uParm4);
undefined8 FUN_0040b840(long *plParm1,long *plParm2,undefined8 *puParm3);
undefined8 FUN_0040b8d0(long *plParm1,long lParm2,long lParm3);
undefined8 FUN_0040ba70(long *plParm1,undefined8 uParm2,undefined8 uParm3,long lParm4,undefined8 uParm5,long lParm6);
long FUN_0040bb60(long *plParm1,long lParm2,ulong uParm3,undefined8 uParm4);
ulong * FUN_0040bda0(undefined4 *puParm1,long lParm2,long lParm3,ulong uParm4);
ulong FUN_0040be80(long *plParm1);
long FUN_0040c070(long *plParm1,long lParm2,undefined8 uParm3);
ulong * FUN_0040c1e0(undefined4 *puParm1,long lParm2,long lParm3);
ulong FUN_0040c2a0(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_0040c350(long lParm1,long lParm2,long lParm3,undefined8 uParm4,uint uParm5);
long FUN_0040c5e0(long *plParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_0040cae0(long lParm1,long lParm2);
undefined8 FUN_0040d000(long *plParm1,long *plParm2,long *plParm3,long *plParm4,long *plParm5);
ulong FUN_0040d1c0(long lParm1,long lParm2,long lParm3);
ulong FUN_0040d2b0(long *plParm1,long lParm2,long lParm3,long lParm4);
void FUN_0040d4a0(long lParm1,long lParm2,long lParm3,long lParm4,long lParm5,long lParm6);
undefined8 FUN_0040d540(long lParm1,long lParm2,long *plParm3,undefined8 uParm4);
void FUN_0040d5c0(long lParm1);
undefined8 FUN_0040d620(long *param_1,long param_2,undefined8 param_3,long *param_4,long *param_5,long param_6,long param_7);
undefined8 FUN_0040d8a0(long *plParm1,long *plParm2,undefined8 *puParm3,long lParm4,undefined8 uParm5,undefined4 *puParm6);
undefined8 FUN_0040d960(undefined8 uParm1,byte *pbParm2);
undefined8 FUN_0040d9a0(long param_1,undefined8 param_2,long *param_3,ulong *param_4,ulong *param_5,byte *param_6,ulong param_7);
long FUN_0040e0c0(long lParm1,undefined8 uParm2,undefined8 uParm3,char *pcParm4,byte bParm5,int *piParm6);
void FUN_0040e2e0(long **pplParm1,long *plParm2,long *plParm3,undefined4 *puParm4);
ulong FUN_0040e7c0(long lParm1,ulong *puParm2,long lParm3,long lParm4,long lParm5);
long FUN_0040ea90(long param_1,undefined8 param_2,long param_3,long *param_4,long param_5,long param_6,long param_7);
ulong FUN_0040ede0(undefined8 *puParm1,long lParm2,long lParm3,long *plParm4,char cParm5);
ulong FUN_0040f0f0(long lParm1,long lParm2,long *plParm3,long *plParm4,undefined8 uParm5);
ulong FUN_0040f340(long param_1,long *param_2,long param_3,long param_4,undefined8 param_5,long param_6,uint param_7);
undefined8 FUN_0040f830(long lParm1,long *plParm2,undefined8 *puParm3,undefined8 uParm4,long lParm5);
undefined8 FUN_0040f8b0(long lParm1,long lParm2,long lParm3);
ulong FUN_0040fc40(long lParm1,long *plParm2,long *plParm3);
long FUN_0040fff0(int *piParm1,long lParm2,long lParm3);
long FUN_004101b0(int *piParm1,long lParm2);
ulong FUN_00410220(long lParm1,long *plParm2,long *plParm3);
undefined8 FUN_004104c0(int *piParm1,long lParm2,long lParm3);
long FUN_00410590(long lParm1,char cParm2,long *plParm3);
ulong FUN_00410930(long lParm1,long *plParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_004109c0(long lParm1,long *plParm2,long lParm3,undefined8 uParm4);
ulong FUN_00410b50(long lParm1,long *plParm2,long lParm3,long *plParm4,long *plParm5);
ulong FUN_00410ee0(long lParm1,long *plParm2,long lParm3,long lParm4);
ulong FUN_00411090(long lParm1,long *plParm2);
ulong FUN_004111a0(long lParm1);
ulong FUN_004113e0(long *param_1,long param_2,long param_3,long param_4,long param_5,undefined8 param_6,ulong param_7,ulong *param_8,uint param_9);
undefined8 FUN_00411e20(undefined4 *puParm1,long lParm2,undefined *puParm3,int iParm4,undefined8 uParm5,char cParm6);
long FUN_00411f00(long lParm1,long lParm2,long lParm3,ulong uParm4,int *piParm5);
ulong FUN_004124b0(long *plParm1);
undefined8 FUN_00412540(long *plParm1,long lParm2,ulong uParm3);
void FUN_00412bb0(undefined8 uParm1,long lParm2);
long FUN_00412bd0(undefined8 uParm1,byte *pbParm2,undefined8 uParm3);
long FUN_00412c80(long lParm1,long lParm2,long lParm3,undefined4 *puParm4,ulong uParm5,undefined4 *puParm6);
long FUN_00413070(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,undefined8 uParm5,int *piParm6);
long FUN_00413180(long lParm1,long *plParm2,undefined8 *puParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_004136a0(undefined8 uParm1,undefined8 *puParm2,long lParm3,undefined8 uParm4,long lParm5,int *piParm6);
long FUN_004137f0(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_00413940(undefined8 uParm1,long *plParm2,ulong uParm3,int *piParm4);
ulong FUN_00413a20(long *plParm1,undefined8 uParm2,undefined8 uParm3,ulong uParm4);
undefined1 * FUN_00413bc0(undefined8 uParm1,undefined8 uParm2,long lParm3);
undefined8 FUN_00413c20(long *plParm1);
long FUN_00413ce0(long param_1,undefined8 param_2,long param_3,long param_4,long param_5,undefined8 param_6,ulong *param_7,char param_8);
void FUN_00413f60(void);
undefined8 FUN_00413f80(long lParm1);
ulong FUN_00414050(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined FUN_00414110(int iParm1);;
ulong FUN_00414120(uint uParm1);
ulong FUN_00414130(byte *pbParm1,byte *pbParm2);
char * FUN_004141f0(char *pcParm1);
void FUN_00414250(long lParm1);
undefined8 FUN_00414280(void);
ulong FUN_00414290(ulong uParm1);
undefined * FUN_00414330(void);
char * FUN_00414690(void);
undefined FUN_00414750(undefined8 uParm1,ulong uParm2);;
ulong FUN_004147f0(long lParm1,int iParm2,undefined8 uParm3,code *pcParm4,long lParm5);
ulong FUN_004149c0(ulong uParm1);
void FUN_004149d0(undefined8 uParm1);
ulong FUN_00414a60(ulong param_1,undefined8 param_2,ulong param_3);
void FUN_00414bb0(undefined8 uParm1);
ulong FUN_00414bc0(long lParm1);
undefined8 FUN_00414c50(void);
void FUN_00414c60(void);
undefined8 FUN_00414c80(long lParm1,long lParm2);
void FUN_00414df0(void);
ulong FUN_00414e70(char *pcParm1,long lParm2);
char * FUN_00414eb0(ulong uParm1,long lParm2,long lParm3);
ulong FUN_00415060(void);
undefined * FUN_004150a0(undefined8 param_1,undefined8 *param_2);
int * FUN_00415310(int *param_1,ulong *param_2);
undefined * FUN_004154a0(undefined8 uParm1,undefined8 *puParm2);
uint * FUN_004156b0(undefined8 uParm1,int *piParm2,ulong *puParm3);
void FUN_00415820(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5,undefined8 *puParm6);
int * FUN_00415960(undefined8 *puParm1,long lParm2,undefined8 *puParm3,undefined8 *puParm4,uint **ppuParm5,uint **ppuParm6);
undefined8 FUN_00415f10(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_00416360(uint param_1);
void FUN_004163a0(uint uParm1);
ulong ** FUN_004165a0(ulong **ppuParm1,ulong **ppuParm2,ulong **ppuParm3,undefined8 uParm4);
undefined FUN_0041b330(char *pcParm1);;
double FUN_0041b370(int *piParm1);
void FUN_0041b3c0(int *param_1);
long FUN_0041b440(long lParm1);
void FUN_0041b450(undefined8 uParm1);
long * FUN_0041b480(void);
ulong FUN_0041b4b0(undefined8 *puParm1,ulong uParm2);
void FUN_0041b5c0(undefined8 uParm1);
ulong FUN_0041b5e0(undefined8 *puParm1);
void FUN_0041b610(undefined8 uParm1,undefined8 uParm2);
void FUN_0041b830(undefined4 *puParm1,ulong uParm2);
undefined8 * FUN_0041ba40(long lParm1,ulong uParm2);
void FUN_0041baf0(long *plParm1,ulong uParm2,ulong uParm3);
undefined8 FUN_0041bb10(long *plParm1);
undefined8 FUN_0041bb60(undefined8 uParm1);
undefined8 FUN_0041bb70(long lParm1,undefined8 uParm2);
void FUN_0041bb80(long *plParm1,long *plParm2);
void FUN_0041be50(ulong *puParm1);
long FUN_0041c100(ulong uParm1,ulong uParm2);
long FUN_0041c120(void);
void FUN_0041c150(undefined8 uParm1,undefined8 uParm2);
void FUN_0041c170(void);
undefined8 FUN_0041c1e0(uint *puParm1,ulong *puParm2);
undefined8 FUN_0041c400(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_0041d0d0(undefined auParm1 [16]);
ulong FUN_0041d0f0(void);
long FUN_0041d120(undefined8 uParm1,undefined8 uParm2);
void FUN_0041d1c0(void);
undefined8 _DT_FINI(void);

