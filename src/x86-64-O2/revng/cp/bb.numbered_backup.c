typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r12(void);
extern uint64_t init_r15(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_6(uint64_t param_0);
extern void indirect_placeholder_25(uint64_t param_0, uint64_t param_1);
uint64_t bb_numbered_backup(uint64_t r8, uint64_t rdx, uint64_t rdi, uint64_t rcx, uint64_t rsi) {
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t rax_5;
    uint64_t var_71;
    uint64_t var_72;
    uint32_t var_73;
    bool var_74;
    uint64_t local_sp_4;
    uint64_t *_pre_phi233;
    uint64_t rax_0;
    uint64_t var_67;
    unsigned char *var_68;
    unsigned char var_69;
    unsigned char *_pre_phi;
    uint64_t rax_1;
    unsigned char rdx2_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_70;
    uint64_t r12_1;
    uint64_t r12_2;
    uint64_t *var_59;
    uint64_t local_sp_6;
    uint64_t r15_1;
    uint64_t var_56;
    uint64_t rax_3;
    uint64_t rcx4_5;
    uint64_t rax_6_be;
    uint64_t local_sp_2;
    uint64_t rcx4_5_be;
    uint64_t local_sp_6_be;
    uint64_t rax_6;
    uint64_t var_27;
    uint64_t var_28;
    unsigned char var_29;
    uint64_t var_30;
    uint32_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t rax_2;
    uint64_t r15_0;
    uint64_t rdx2_1;
    uint64_t rcx4_0;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint32_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t rdx2_2;
    uint64_t rcx4_1;
    uint64_t var_41;
    uint64_t rcx4_2;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t local_sp_0;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t *var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t *var_54;
    uint64_t var_55;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t r81_0;
    uint64_t rdx2_3;
    uint64_t local_sp_1;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    unsigned char *var_65;
    unsigned char var_66;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t rax_4;
    uint64_t r12_0;
    uint64_t rcx4_3;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t local_sp_3;
    uint64_t rcx4_4;
    uint64_t local_sp_5;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_14;
    uint16_t *var_15;
    uint16_t var_16;
    uint64_t var_17;
    uint64_t *var_18;
    uint64_t var_19;
    uint64_t var_20;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r12();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_r13();
    var_5 = init_r14();
    var_6 = init_rbp();
    var_7 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_1;
    *(uint64_t *)(var_0 + (-40L)) = var_6;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_8 = *(uint64_t *)rdi;
    var_9 = (uint64_t *)r8;
    var_10 = *var_9;
    *(uint64_t *)(var_0 + (-72L)) = rdi;
    var_11 = var_8 + rcx;
    *(uint64_t *)(var_0 + (-88L)) = rsi;
    *(uint64_t *)(var_0 + (-80L)) = rdx;
    *(uint64_t *)(var_0 + (-112L)) = rcx;
    var_12 = (uint64_t *)(var_0 + (-128L));
    *var_12 = 4237847UL;
    var_13 = indirect_placeholder_6(var_11);
    rax_5 = var_13;
    r12_2 = var_8;
    r15_1 = 1UL;
    r15_0 = 1UL;
    rcx4_4 = rcx;
    if (var_10 == 0UL) {
        var_14 = var_0 + (-136L);
        *(uint64_t *)var_14 = 4237867UL;
        indirect_placeholder();
        local_sp_5 = var_14;
    } else {
        var_15 = (uint16_t *)var_11;
        var_16 = *var_15;
        *var_15 = (uint16_t)(unsigned short)46U;
        var_17 = var_0 + (-136L);
        var_18 = (uint64_t *)var_17;
        *var_18 = 4238390UL;
        var_19 = indirect_placeholder_6(var_8);
        var_20 = var_13 + var_11;
        rax_5 = var_19;
        rcx4_4 = var_20;
        local_sp_5 = var_17;
        if (var_19 != 0UL) {
            *var_12 = var_20;
            var_71 = var_0 + (-144L);
            *(uint64_t *)var_71 = 4238446UL;
            indirect_placeholder();
            var_72 = *var_18;
            var_73 = *(volatile uint32_t *)(uint32_t *)0UL;
            *var_15 = var_16;
            var_74 = (var_73 == 12U);
            *(uint32_t *)var_72 = 2117172782U;
            *(unsigned char *)(var_72 + 4UL) = (unsigned char)'\x00';
            *(uint32_t *)(var_0 + (-124L)) = (var_74 ? 3U : 2U);
            local_sp_4 = var_71;
            return (uint64_t)*(uint32_t *)(local_sp_4 + 20UL);
        }
        *var_15 = var_16;
        *(uint32_t *)var_20 = 2117172782U;
        *(unsigned char *)(var_20 + 4UL) = (unsigned char)'\x00';
        *var_9 = var_19;
    }
    *(uint64_t *)(local_sp_5 + 24UL) = 1UL;
    var_21 = var_13 + 4UL;
    *(uint32_t *)(local_sp_5 + 20UL) = 2U;
    rax_6 = rax_5;
    rcx4_5 = rcx4_4;
    local_sp_6 = local_sp_5;
    while (1U)
        {
            var_22 = local_sp_6 + (-8L);
            *(uint64_t *)var_22 = 4237896UL;
            indirect_placeholder();
            r12_1 = r12_2;
            local_sp_2 = var_22;
            rax_4 = rax_6;
            r12_0 = r12_2;
            rcx4_3 = rcx4_5;
            local_sp_3 = var_22;
            if (rax_6 != 0UL) {
                loop_state_var = 1U;
                break;
            }
            while (1U)
                {
                    var_23 = local_sp_2 + (-8L);
                    *(uint64_t *)var_23 = 4237917UL;
                    indirect_placeholder();
                    var_24 = helper_cc_compute_c_wrapper(rax_4 - var_21, var_21, var_7, 17U);
                    r12_2 = r12_0;
                    rax_6_be = rax_4;
                    rcx4_5_be = rcx4_3;
                    local_sp_6_be = var_23;
                    r81_0 = r12_0;
                    if (var_24 != 0UL) {
                        loop_state_var = 2U;
                        break;
                    }
                    var_25 = *(uint64_t *)local_sp_2;
                    var_26 = local_sp_2 + (-16L);
                    *(uint64_t *)var_26 = 4237946UL;
                    indirect_placeholder();
                    rax_6_be = var_25;
                    local_sp_6_be = var_26;
                    local_sp_0 = var_26;
                    if ((uint64_t)(uint32_t)var_25 != 0UL) {
                        loop_state_var = 2U;
                        break;
                    }
                    var_27 = rax_4 + var_13;
                    var_28 = var_27 + 21UL;
                    var_29 = *(unsigned char *)var_28;
                    rax_6_be = (uint64_t)var_29;
                    if ((uint64_t)(var_29 + '\xcf') <= 8UL) {
                        loop_state_var = 2U;
                        break;
                    }
                    var_30 = (uint64_t)(var_29 + '\xc7');
                    var_31 = (uint32_t)(uint64_t)*(unsigned char *)(var_27 + 22UL);
                    var_32 = (uint64_t)var_31;
                    var_33 = (rcx4_3 & (-256L)) | (var_30 == 0UL);
                    var_34 = (uint64_t)(var_31 + (-48));
                    rax_3 = var_34;
                    rax_2 = var_34;
                    rdx2_1 = var_32;
                    rcx4_0 = var_33;
                    rdx2_2 = var_32;
                    rcx4_1 = var_33;
                    if (var_34 > 9UL) {
                        var_35 = (uint64_t)((uint32_t)rax_2 & (-256)) | ((uint64_t)((unsigned char)rdx2_1 + '\xc7') == 0UL);
                        var_36 = r15_0 + 1UL;
                        var_37 = rcx4_0 & var_35;
                        var_38 = (uint32_t)(uint64_t)*(unsigned char *)(var_36 + var_28);
                        var_39 = (uint64_t)var_38;
                        var_40 = (uint64_t)(var_38 + (-48));
                        r15_1 = var_36;
                        rax_3 = var_40;
                        rax_2 = var_40;
                        r15_0 = var_36;
                        rdx2_1 = var_39;
                        rcx4_0 = var_37;
                        rdx2_2 = var_39;
                        rcx4_1 = var_37;
                        do {
                            var_35 = (uint64_t)((uint32_t)rax_2 & (-256)) | ((uint64_t)((unsigned char)rdx2_1 + '\xc7') == 0UL);
                            var_36 = r15_0 + 1UL;
                            var_37 = rcx4_0 & var_35;
                            var_38 = (uint32_t)(uint64_t)*(unsigned char *)(var_36 + var_28);
                            var_39 = (uint64_t)var_38;
                            var_40 = (uint64_t)(var_38 + (-48));
                            r15_1 = var_36;
                            rax_3 = var_40;
                            rax_2 = var_40;
                            r15_0 = var_36;
                            rdx2_1 = var_39;
                            rcx4_0 = var_37;
                            rdx2_2 = var_39;
                            rcx4_1 = var_37;
                        } while (var_40 <= 9UL);
                    }
                    rax_6_be = rax_3;
                    rcx4_5_be = rcx4_1;
                    rcx4_2 = rcx4_1;
                    if ((uint64_t)((unsigned char)rdx2_2 + '\x82') != 0UL) {
                        loop_state_var = 2U;
                        break;
                    }
                    if (*(unsigned char *)((r15_1 + var_28) + 1UL) != '\x00') {
                        loop_state_var = 2U;
                        break;
                    }
                    var_41 = *(uint64_t *)(local_sp_2 + 8UL);
                    if (r15_1 <= var_41) {
                        var_42 = r15_1 - var_41;
                        *(unsigned char *)(local_sp_2 + 40UL) = (unsigned char)rcx4_1;
                        var_43 = helper_cc_compute_all_wrapper(var_42, var_41, var_7, 17U);
                        if ((var_43 & 64UL) != 0UL) {
                            loop_state_var = 2U;
                            break;
                        }
                        var_44 = *(uint64_t *)(local_sp_2 + 24UL);
                        var_45 = local_sp_2 + (-24L);
                        *(uint64_t *)var_45 = 4238086UL;
                        indirect_placeholder();
                        var_46 = (uint64_t)*(unsigned char *)(local_sp_2 + 32UL);
                        var_47 = helper_cc_compute_all_wrapper(var_44, 0UL, 0UL, 24U);
                        rax_6_be = var_44;
                        rcx4_5_be = var_46;
                        local_sp_6_be = var_45;
                        rcx4_2 = var_46;
                        local_sp_0 = var_45;
                        if ((uint64_t)(((unsigned char)(var_47 >> 4UL) ^ (unsigned char)var_47) & '\xc0') != 0UL) {
                            loop_state_var = 2U;
                            break;
                        }
                    }
                    var_48 = (uint64_t)(unsigned char)rcx4_2;
                    *(uint32_t *)(local_sp_0 + 20UL) = (uint32_t)(unsigned char)(uint32_t)rcx4_2;
                    var_49 = local_sp_0 + 40UL;
                    var_50 = (uint64_t *)var_49;
                    var_51 = *var_50;
                    var_52 = r15_1 + var_48;
                    *(uint64_t *)(local_sp_0 + 24UL) = var_52;
                    var_53 = (var_51 + var_52) + 4UL;
                    var_54 = (uint64_t *)(local_sp_0 + 32UL);
                    var_55 = helper_cc_compute_c_wrapper(*var_54 - var_53, var_53, var_7, 17U);
                    var_57 = var_53;
                    _pre_phi233 = var_50;
                    rdx2_3 = var_48;
                    local_sp_1 = local_sp_0;
                    if (var_55 != 0UL) {
                        var_56 = var_53 << 1UL;
                        r81_0 = var_53;
                        if (((long)var_56 >= (long)0UL) && ((long)var_53 > (long)18446744073709551615UL)) {
                            *var_54 = var_56;
                            var_57 = var_56;
                        } else {
                            *var_54 = var_53;
                        }
                        *(uint64_t *)(local_sp_0 + 56UL) = var_48;
                        var_58 = local_sp_0 + (-8L);
                        *(uint64_t *)var_58 = 4238191UL;
                        indirect_placeholder_25(r12_0, var_57);
                        rdx2_3 = *(uint64_t *)(local_sp_0 + 48UL);
                        local_sp_1 = var_58;
                        local_sp_4 = var_58;
                        if (var_53 != 0UL) {
                            var_59 = *(uint64_t **)var_49;
                            *(uint32_t *)(local_sp_0 + 12UL) = 3U;
                            *var_59 = r12_0;
                            loop_state_var = 0U;
                            break;
                        }
                        _pre_phi233 = (uint64_t *)(var_58 + 40UL);
                    }
                    var_60 = *_pre_phi233;
                    *(uint64_t *)(local_sp_1 + 56UL) = r81_0;
                    var_61 = var_60 + r81_0;
                    *(uint16_t *)var_61 = (uint16_t)(unsigned short)32302U;
                    var_62 = (rdx2_3 + var_61) + 2UL;
                    *(unsigned char *)(var_61 + 2UL) = (unsigned char)'0';
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4238253UL;
                    indirect_placeholder();
                    var_63 = *(uint64_t *)(local_sp_1 + 48UL);
                    var_64 = (r15_1 + var_61) + (-1L);
                    var_65 = (unsigned char *)var_64;
                    var_66 = *var_65;
                    rax_0 = var_64;
                    _pre_phi = var_65;
                    rax_1 = var_64;
                    rdx2_0 = var_66;
                    r12_1 = var_63;
                    r12_0 = var_63;
                    rcx4_3 = var_62;
                    if (var_66 == '9') {
                        *(unsigned char *)rax_0 = (unsigned char)'0';
                        var_67 = rax_0 + (-1L);
                        var_68 = (unsigned char *)var_67;
                        var_69 = *var_68;
                        rax_0 = var_67;
                        _pre_phi = var_68;
                        rax_1 = var_67;
                        rdx2_0 = var_69;
                        do {
                            *(unsigned char *)rax_0 = (unsigned char)'0';
                            var_67 = rax_0 + (-1L);
                            var_68 = (unsigned char *)var_67;
                            var_69 = *var_68;
                            rax_0 = var_67;
                            _pre_phi = var_68;
                            rax_1 = var_67;
                            rdx2_0 = var_69;
                        } while (var_69 != '9');
                    }
                    *_pre_phi = (rdx2_0 + '\x01');
                    var_70 = local_sp_1 + (-16L);
                    *(uint64_t *)var_70 = 4238312UL;
                    indirect_placeholder();
                    rax_4 = rax_1;
                    local_sp_2 = var_70;
                    local_sp_3 = var_70;
                    if (rax_1 == 0UL) {
                        continue;
                    }
                    loop_state_var = 1U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 2U:
                {
                    rax_6 = rax_6_be;
                    rcx4_5 = rcx4_5_be;
                    local_sp_6 = local_sp_6_be;
                    continue;
                }
                break;
              case 0U:
                {
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 1U:
                {
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            return (uint64_t)*(uint32_t *)(local_sp_4 + 20UL);
        }
        break;
      case 1U:
        {
            **(uint64_t **)(local_sp_3 + 48UL) = r12_1;
            local_sp_4 = local_sp_3;
        }
        break;
    }
}
