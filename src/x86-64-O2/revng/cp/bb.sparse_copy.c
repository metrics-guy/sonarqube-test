typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_9_ret_type;
struct indirect_placeholder_11_ret_type;
struct indirect_placeholder_10_ret_type;
struct indirect_placeholder_9_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_11_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_10_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t indirect_placeholder_8(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r12(void);
extern uint64_t init_r15(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_9_ret_type indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_11_ret_type indirect_placeholder_11(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_10_ret_type indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
uint64_t bb_sparse_copy(uint64_t r8, uint64_t rdx, uint64_t rdi, uint64_t rcx, uint64_t rsi, uint64_t r9) {
    uint64_t r81_8;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t *var_14;
    uint64_t r15_3;
    uint64_t r81_9;
    uint64_t r13_1;
    uint64_t r13_0;
    uint64_t r81_0;
    uint64_t local_sp_10;
    uint64_t local_sp_9;
    uint64_t rbp_0;
    uint64_t local_sp_0;
    unsigned char r96_0_in;
    uint64_t r96_0;
    bool var_73;
    bool var_74;
    uint64_t *_pre_phi281;
    uint64_t r81_6_ph;
    uint64_t r12_3;
    uint64_t *_pre280;
    uint64_t rbp_6;
    uint64_t r12_1;
    uint64_t *var_75;
    uint64_t var_76;
    uint64_t r12_0;
    uint64_t r81_1;
    uint64_t rbp_1;
    uint64_t local_sp_1;
    unsigned char r96_1_in;
    uint64_t r96_1;
    uint64_t r96_7;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t rbp_5_ph;
    uint64_t var_21;
    uint64_t local_sp_5;
    uint64_t var_25;
    uint64_t rax_0;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t *var_30;
    uint64_t r12_3_ph;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t r12_2_ph;
    uint64_t r13_3;
    uint64_t r13_4;
    unsigned char _pre_phi;
    unsigned char _pre;
    uint64_t r15_4;
    uint64_t r81_6;
    uint64_t local_sp_7;
    uint64_t *var_31;
    uint64_t r81_7;
    uint64_t r81_2;
    uint64_t rbp_2;
    uint64_t r96_6;
    uint64_t local_sp_2;
    uint64_t r96_2;
    uint64_t rbx_0;
    uint64_t var_48;
    uint64_t r15_0_in;
    uint64_t var_51;
    uint64_t r13_2;
    uint64_t r81_1_pn;
    uint64_t local_sp_3;
    uint64_t r96_3;
    uint64_t r81_3;
    uint64_t r81_4;
    uint64_t rbp_4;
    uint64_t local_sp_4;
    uint64_t r96_4;
    uint64_t var_77;
    uint64_t *var_78;
    uint64_t var_79;
    uint64_t var_80;
    bool var_81;
    uint64_t var_82;
    uint64_t r15_1_ph;
    uint64_t r81_5_ph;
    uint64_t local_sp_5_ph;
    uint64_t r96_5_ph;
    uint64_t *var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t r15_2;
    uint64_t rbx_0_ph;
    uint64_t r15_4_ph;
    uint64_t r13_3_ph;
    uint64_t r14_2_ph;
    uint64_t rbp_6_ph;
    uint64_t local_sp_7_ph;
    uint64_t r96_6_ph;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    bool var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t rbp_7;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint32_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    bool brmerge;
    bool var_45;
    unsigned char var_46;
    uint64_t var_47;
    uint64_t local_sp_8;
    uint64_t var_52;
    uint64_t r12_4;
    uint64_t r12_5;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t rbp_8;
    uint64_t r13_5;
    uint64_t rbp_9;
    uint64_t *var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t *var_64;
    uint64_t var_65;
    uint64_t var_66;
    unsigned char var_67;
    uint64_t var_68;
    uint64_t r13_6;
    uint64_t rbp_10;
    uint64_t r96_8;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    unsigned char var_59;
    uint64_t var_60;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r12();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_r13();
    var_5 = init_r14();
    var_6 = init_rbp();
    var_7 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_1;
    *(uint64_t *)(var_0 + (-40L)) = var_6;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_8 = *(uint64_t *)(var_0 | 8UL);
    *(uint32_t *)(var_0 + (-144L)) = (uint32_t)rsi;
    var_9 = (uint64_t *)(var_0 + (-128L));
    *var_9 = rcx;
    var_10 = *(uint64_t *)(var_0 | 40UL);
    *(uint64_t *)(var_0 + (-80L)) = var_8;
    var_11 = *(uint64_t *)(var_0 | 16UL);
    var_12 = *(uint64_t *)(var_0 | 32UL);
    *(unsigned char *)var_10 = (unsigned char)'\x00';
    *(uint64_t *)(var_0 + (-152L)) = var_11;
    var_13 = *(uint64_t *)(var_0 | 24UL);
    *(uint32_t *)(var_0 + (-64L)) = (uint32_t)rdi;
    *(uint64_t *)(var_0 + (-136L)) = rdx;
    var_14 = (uint64_t *)(var_0 + (-104L));
    *var_14 = r8;
    *(uint32_t *)(var_0 + (-60L)) = (uint32_t)r9;
    *(uint64_t *)(var_0 + (-120L)) = var_13;
    *(uint64_t *)(var_0 + (-96L)) = var_12;
    *(uint64_t *)(var_0 + (-72L)) = var_10;
    *(unsigned char *)(var_0 + (-137L)) = (unsigned char)r9;
    *(uint64_t *)var_12 = 0UL;
    r15_3 = 1UL;
    r12_1 = 0UL;
    r96_7 = 0UL;
    rbp_5_ph = var_6;
    r12_2_ph = 0UL;
    r13_4 = 1UL;
    r81_4 = 0UL;
    r15_1_ph = 0UL;
    r81_5_ph = r8;
    r96_5_ph = r9;
    rbp_7 = 0UL;
    rbp_8 = 0UL;
    r13_5 = 1UL;
    r13_6 = 1UL;
    r96_8 = 0UL;
    if (var_13 == 0UL) {
        return (uint64_t)(uint32_t)r15_3;
    }
    var_15 = var_0 + (-184L);
    var_16 = *var_14;
    *(uint64_t *)(var_0 + (-88L)) = ((var_16 == 0UL) ? *var_9 : var_16);
    local_sp_5_ph = var_15;
    r15_3 = 0UL;
    while (1U)
        {
            local_sp_5 = local_sp_5_ph;
            r12_3_ph = r12_2_ph;
            r15_2 = r15_1_ph;
            r15_4_ph = r15_1_ph;
            rbp_6_ph = rbp_5_ph;
            r96_6_ph = r96_5_ph;
            while (1U)
                {
                    var_17 = (uint64_t *)(local_sp_5 + 64UL);
                    var_18 = *var_17;
                    var_19 = *(uint64_t *)(local_sp_5 + 56UL);
                    var_20 = local_sp_5 + (-8L);
                    *(uint64_t *)var_20 = 4218385UL;
                    indirect_placeholder();
                    *var_17 = var_18;
                    rax_0 = var_18;
                    local_sp_7_ph = var_20;
                    if ((long)var_18 <= (long)18446744073709551615UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_21 = local_sp_5 + (-16L);
                    *(uint64_t *)var_21 = 4218400UL;
                    indirect_placeholder();
                    local_sp_5 = var_21;
                    if (*(uint32_t *)var_18 == 4U) {
                        continue;
                    }
                    var_22 = *(uint64_t *)(local_sp_5 + 88UL);
                    *(uint64_t *)(local_sp_5 + (-24L)) = 4218423UL;
                    var_23 = indirect_placeholder_8(4UL, var_22);
                    *(uint64_t *)(local_sp_5 + (-32L)) = 4218431UL;
                    indirect_placeholder();
                    var_24 = (uint64_t)*(uint32_t *)var_23;
                    *(uint64_t *)(local_sp_5 + (-40L)) = 4218450UL;
                    indirect_placeholder_11(0UL, r81_5_ph, 4321439UL, 0UL, var_23, var_24, r96_5_ph);
                    loop_state_var = 1U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    var_25 = helper_cc_compute_all_wrapper(var_18, var_19, var_7, 25U);
                    r15_3 = 1UL;
                    if ((var_25 & 64UL) != 0UL) {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    var_26 = *(uint64_t *)(local_sp_5 + 40UL);
                    var_27 = *(uint64_t *)(local_sp_5 + 80UL);
                    var_28 = *var_17;
                    var_29 = *(uint64_t *)(local_sp_5 + 88UL);
                    *(uint64_t *)(local_sp_5 + 8UL) = var_26;
                    var_30 = (uint64_t *)var_27;
                    *var_30 = (*var_30 + var_28);
                    *(unsigned char *)(local_sp_5 + 38UL) = (*(uint64_t *)(local_sp_5 + 72UL) != 0UL);
                    rbx_0_ph = var_29;
                    r13_3_ph = var_28;
                    r14_2_ph = var_26;
                    r81_6_ph = var_28;
                    while (1U)
                        {
                            r12_3 = r12_3_ph;
                            rbp_6 = rbp_6_ph;
                            r13_3 = r13_3_ph;
                            r15_4 = r15_4_ph;
                            r81_6 = r81_6_ph;
                            local_sp_7 = local_sp_7_ph;
                            r96_6 = r96_6_ph;
                            rbx_0 = rbx_0_ph;
                            while (1U)
                                {
                                    var_31 = (uint64_t *)local_sp_7;
                                    *var_31 = r81_6;
                                    var_32 = (rbx_0 > r81_6) ? r81_6 : rbx_0;
                                    var_33 = (var_32 != 0UL);
                                    var_34 = rbp_6 & (-256L);
                                    var_35 = (r81_6 <= rbx_0);
                                    var_36 = (r13_3 & (-256L)) | var_35;
                                    var_37 = var_33 & (uint64_t)*(unsigned char *)(local_sp_7 + 46UL);
                                    var_38 = var_34 | var_37;
                                    r15_3 = r15_4;
                                    r12_3 = var_32;
                                    r12_0 = var_32;
                                    r81_7 = r81_6;
                                    r81_2 = r81_6;
                                    local_sp_2 = local_sp_7;
                                    r96_2 = r96_6;
                                    rbx_0 = 0UL;
                                    r13_2 = var_36;
                                    rbx_0_ph = var_32;
                                    local_sp_8 = local_sp_7;
                                    r12_4 = r12_3;
                                    r12_5 = r12_3;
                                    if (var_37 == 0UL) {
                                        var_46 = (unsigned char)r15_4;
                                        _pre_phi = var_46;
                                        rbp_7 = var_38;
                                        if (!(((uint64_t)(var_46 + '\xff') == 0UL) || (var_35 ^ 1))) {
                                            var_47 = (uint64_t)(uint32_t)r15_4;
                                            rbp_2 = var_47;
                                            rbp_7 = var_47;
                                            if (var_32 == 0UL) {
                                                loop_state_var = 2U;
                                                break;
                                            }
                                        }
                                        var_52 = r12_3 + var_32;
                                        r81_8 = r81_7;
                                        r81_9 = r81_7;
                                        local_sp_10 = local_sp_8;
                                        local_sp_9 = local_sp_8;
                                        r12_4 = var_52;
                                        r12_5 = var_52;
                                        rbp_9 = rbp_7;
                                        rbp_10 = rbp_7;
                                        if ((uint64_t)_pre_phi != 0UL) {
                                            var_61 = (uint64_t *)(local_sp_9 + 16UL);
                                            var_62 = *var_61;
                                            var_63 = (uint64_t)*(uint32_t *)(local_sp_9 + 40UL);
                                            var_64 = (uint64_t *)(local_sp_9 + 24UL);
                                            *var_64 = r81_8;
                                            *(unsigned char *)(local_sp_9 + 8UL) = (unsigned char)r96_7;
                                            var_65 = local_sp_9 + (-8L);
                                            *(uint64_t *)var_65 = 4218682UL;
                                            var_66 = indirect_placeholder_1(r12_4, var_63, var_62);
                                            var_67 = *(unsigned char *)local_sp_9;
                                            var_68 = *var_61;
                                            r13_1 = r13_5;
                                            r13_0 = r13_5;
                                            r81_0 = var_68;
                                            rbp_0 = rbp_9;
                                            local_sp_0 = var_65;
                                            r96_0_in = var_67;
                                            r81_1 = var_68;
                                            rbp_1 = rbp_9;
                                            local_sp_1 = var_65;
                                            r96_1_in = var_67;
                                            if (r12_4 != var_66) {
                                                if ((uint64_t)(uint32_t)r13_5 == 0UL) {
                                                    loop_state_var = 0U;
                                                    break;
                                                }
                                                r96_0 = (uint64_t)r96_0_in;
                                                var_73 = (var_32 == 0UL);
                                                var_74 = (r96_0_in == '\x00');
                                                rbp_6 = rbp_0;
                                                r12_0 = 0UL;
                                                r13_3 = r13_0;
                                                r81_6 = r81_0;
                                                local_sp_7 = local_sp_0;
                                                r96_6 = r96_0;
                                                r15_0_in = rbp_0;
                                                r13_2 = r13_0;
                                                local_sp_3 = local_sp_0;
                                                r96_3 = r96_0;
                                                r81_4 = r81_0;
                                                rbp_4 = rbp_0;
                                                local_sp_4 = local_sp_0;
                                                r96_4 = r96_0;
                                                if (var_73) {
                                                    loop_state_var = 1U;
                                                    break;
                                                }
                                                if (var_74) {
                                                    loop_state_var = 3U;
                                                    break;
                                                }
                                                *(uint64_t *)(local_sp_0 + 16UL) = r14_2_ph;
                                                r15_4 = (uint64_t)(uint32_t)rbp_0;
                                                continue;
                                            }
                                            var_69 = (uint64_t)var_67;
                                            var_70 = *var_64;
                                            *(uint64_t *)(local_sp_9 + (-16L)) = 4219097UL;
                                            var_71 = indirect_placeholder_8(4UL, var_70);
                                            *(uint64_t *)(local_sp_9 + (-24L)) = 4219105UL;
                                            indirect_placeholder();
                                            var_72 = (uint64_t)*(uint32_t *)var_71;
                                            *(uint64_t *)(local_sp_9 + (-32L)) = 4219124UL;
                                            indirect_placeholder_9(0UL, var_68, 4321456UL, 0UL, var_71, var_72, var_69);
                                            loop_state_var = 4U;
                                            break;
                                        }
                                    }
                                    *(uint64_t *)(local_sp_7 + 8UL) = r81_6;
                                    var_39 = local_sp_7 + (-8L);
                                    *(uint64_t *)var_39 = 4218588UL;
                                    var_40 = indirect_placeholder_8(r14_2_ph, var_32);
                                    var_41 = *var_31;
                                    var_42 = (uint32_t)var_40;
                                    var_43 = (uint64_t)var_42;
                                    var_44 = (uint64_t)((var_42 ^ (uint32_t)r15_4) & (-255)) & ((var_40 & (-256L)) | (r12_3 != 0UL));
                                    brmerge = (((uint64_t)((unsigned char)var_40 + '\xff') == 0UL) || (var_35 ^ 1));
                                    var_45 = ((var_44 & 1UL) == 0UL);
                                    r81_8 = var_41;
                                    r81_9 = var_41;
                                    local_sp_10 = var_39;
                                    local_sp_9 = var_39;
                                    r96_7 = var_44;
                                    r81_7 = var_41;
                                    r81_2 = var_41;
                                    rbp_2 = var_43;
                                    local_sp_2 = var_39;
                                    r96_2 = var_44;
                                    local_sp_8 = var_39;
                                    r96_8 = var_44;
                                    if (brmerge) {
                                        r13_4 = 0UL;
                                        rbp_8 = var_43;
                                        if (var_45) {
                                            loop_state_var = 2U;
                                            break;
                                        }
                                        r13_5 = r13_4;
                                        rbp_9 = rbp_8;
                                        r13_6 = r13_4;
                                        rbp_10 = rbp_8;
                                        if ((uint64_t)(unsigned char)r15_4 == 0UL) {
                                            var_61 = (uint64_t *)(local_sp_9 + 16UL);
                                            var_62 = *var_61;
                                            var_63 = (uint64_t)*(uint32_t *)(local_sp_9 + 40UL);
                                            var_64 = (uint64_t *)(local_sp_9 + 24UL);
                                            *var_64 = r81_8;
                                            *(unsigned char *)(local_sp_9 + 8UL) = (unsigned char)r96_7;
                                            var_65 = local_sp_9 + (-8L);
                                            *(uint64_t *)var_65 = 4218682UL;
                                            var_66 = indirect_placeholder_1(r12_4, var_63, var_62);
                                            var_67 = *(unsigned char *)local_sp_9;
                                            var_68 = *var_61;
                                            r13_1 = r13_5;
                                            r13_0 = r13_5;
                                            r81_0 = var_68;
                                            rbp_0 = rbp_9;
                                            local_sp_0 = var_65;
                                            r96_0_in = var_67;
                                            r81_1 = var_68;
                                            rbp_1 = rbp_9;
                                            local_sp_1 = var_65;
                                            r96_1_in = var_67;
                                            if (r12_4 != var_66) {
                                                if ((uint64_t)(uint32_t)r13_5 == 0UL) {
                                                    loop_state_var = 0U;
                                                    break;
                                                }
                                                r96_0 = (uint64_t)r96_0_in;
                                                var_73 = (var_32 == 0UL);
                                                var_74 = (r96_0_in == '\x00');
                                                rbp_6 = rbp_0;
                                                r12_0 = 0UL;
                                                r13_3 = r13_0;
                                                r81_6 = r81_0;
                                                local_sp_7 = local_sp_0;
                                                r96_6 = r96_0;
                                                r15_0_in = rbp_0;
                                                r13_2 = r13_0;
                                                local_sp_3 = local_sp_0;
                                                r96_3 = r96_0;
                                                r81_4 = r81_0;
                                                rbp_4 = rbp_0;
                                                local_sp_4 = local_sp_0;
                                                r96_4 = r96_0;
                                                if (var_73) {
                                                    loop_state_var = 1U;
                                                    break;
                                                }
                                                if (var_74) {
                                                    loop_state_var = 3U;
                                                    break;
                                                }
                                                *(uint64_t *)(local_sp_0 + 16UL) = r14_2_ph;
                                                r15_4 = (uint64_t)(uint32_t)rbp_0;
                                                continue;
                                            }
                                            var_69 = (uint64_t)var_67;
                                            var_70 = *var_64;
                                            *(uint64_t *)(local_sp_9 + (-16L)) = 4219097UL;
                                            var_71 = indirect_placeholder_8(4UL, var_70);
                                            *(uint64_t *)(local_sp_9 + (-24L)) = 4219105UL;
                                            indirect_placeholder();
                                            var_72 = (uint64_t)*(uint32_t *)var_71;
                                            *(uint64_t *)(local_sp_9 + (-32L)) = 4219124UL;
                                            indirect_placeholder_9(0UL, var_68, 4321456UL, 0UL, var_71, var_72, var_69);
                                            loop_state_var = 4U;
                                            break;
                                        }
                                    }
                                    if (var_45) {
                                        _pre = (unsigned char)r15_4;
                                        _pre_phi = _pre;
                                        var_52 = r12_3 + var_32;
                                        r81_8 = r81_7;
                                        r81_9 = r81_7;
                                        local_sp_10 = local_sp_8;
                                        local_sp_9 = local_sp_8;
                                        r12_4 = var_52;
                                        r12_5 = var_52;
                                        rbp_9 = rbp_7;
                                        rbp_10 = rbp_7;
                                        if ((uint64_t)_pre_phi == 0UL) {
                                            var_61 = (uint64_t *)(local_sp_9 + 16UL);
                                            var_62 = *var_61;
                                            var_63 = (uint64_t)*(uint32_t *)(local_sp_9 + 40UL);
                                            var_64 = (uint64_t *)(local_sp_9 + 24UL);
                                            *var_64 = r81_8;
                                            *(unsigned char *)(local_sp_9 + 8UL) = (unsigned char)r96_7;
                                            var_65 = local_sp_9 + (-8L);
                                            *(uint64_t *)var_65 = 4218682UL;
                                            var_66 = indirect_placeholder_1(r12_4, var_63, var_62);
                                            var_67 = *(unsigned char *)local_sp_9;
                                            var_68 = *var_61;
                                            r13_1 = r13_5;
                                            r13_0 = r13_5;
                                            r81_0 = var_68;
                                            rbp_0 = rbp_9;
                                            local_sp_0 = var_65;
                                            r96_0_in = var_67;
                                            r81_1 = var_68;
                                            rbp_1 = rbp_9;
                                            local_sp_1 = var_65;
                                            r96_1_in = var_67;
                                            if (r12_4 == var_66) {
                                                if ((uint64_t)(uint32_t)r13_5 != 0UL) {
                                                    loop_state_var = 0U;
                                                    break;
                                                }
                                                r96_0 = (uint64_t)r96_0_in;
                                                var_73 = (var_32 == 0UL);
                                                var_74 = (r96_0_in == '\x00');
                                                rbp_6 = rbp_0;
                                                r12_0 = 0UL;
                                                r13_3 = r13_0;
                                                r81_6 = r81_0;
                                                local_sp_7 = local_sp_0;
                                                r96_6 = r96_0;
                                                r15_0_in = rbp_0;
                                                r13_2 = r13_0;
                                                local_sp_3 = local_sp_0;
                                                r96_3 = r96_0;
                                                r81_4 = r81_0;
                                                rbp_4 = rbp_0;
                                                local_sp_4 = local_sp_0;
                                                r96_4 = r96_0;
                                                if (!var_73) {
                                                    loop_state_var = 1U;
                                                    break;
                                                }
                                                if (!var_74) {
                                                    loop_state_var = 3U;
                                                    break;
                                                }
                                                *(uint64_t *)(local_sp_0 + 16UL) = r14_2_ph;
                                                r15_4 = (uint64_t)(uint32_t)rbp_0;
                                                continue;
                                            }
                                            var_69 = (uint64_t)var_67;
                                            var_70 = *var_64;
                                            *(uint64_t *)(local_sp_9 + (-16L)) = 4219097UL;
                                            var_71 = indirect_placeholder_8(4UL, var_70);
                                            *(uint64_t *)(local_sp_9 + (-24L)) = 4219105UL;
                                            indirect_placeholder();
                                            var_72 = (uint64_t)*(uint32_t *)var_71;
                                            *(uint64_t *)(local_sp_9 + (-32L)) = 4219124UL;
                                            indirect_placeholder_9(0UL, var_68, 4321456UL, 0UL, var_71, var_72, var_69);
                                            loop_state_var = 4U;
                                            break;
                                        }
                                    }
                                    r13_5 = r13_4;
                                    rbp_9 = rbp_8;
                                    r13_6 = r13_4;
                                    rbp_10 = rbp_8;
                                    if ((uint64_t)(unsigned char)r15_4 == 0UL) {
                                        var_53 = (uint64_t)*(unsigned char *)(local_sp_10 + 47UL);
                                        var_54 = *(uint64_t *)(local_sp_10 + 32UL);
                                        *(uint64_t *)(local_sp_10 + 16UL) = r81_9;
                                        var_55 = (uint64_t)*(uint32_t *)(local_sp_10 + 40UL);
                                        var_56 = local_sp_10 + 8UL;
                                        *(unsigned char *)var_56 = (unsigned char)r96_8;
                                        var_57 = local_sp_10 + (-8L);
                                        *(uint64_t *)var_57 = 4218824UL;
                                        var_58 = indirect_placeholder_12(var_53, var_55, r12_5, var_54);
                                        var_59 = *(unsigned char *)local_sp_10;
                                        var_60 = *(uint64_t *)var_56;
                                        r13_1 = r13_6;
                                        r13_0 = r13_6;
                                        r81_0 = var_60;
                                        rbp_0 = rbp_10;
                                        local_sp_0 = var_57;
                                        r96_0_in = var_59;
                                        r81_1 = var_60;
                                        rbp_1 = rbp_10;
                                        local_sp_1 = var_57;
                                        r96_1_in = var_59;
                                        if ((uint64_t)(unsigned char)var_58 != 0UL) {
                                            r15_3 = (uint64_t)(uint32_t)var_58;
                                            loop_state_var = 4U;
                                            break;
                                        }
                                        if ((uint64_t)(uint32_t)r13_6 != 0UL) {
                                            loop_state_var = 0U;
                                            break;
                                        }
                                        r96_0 = (uint64_t)r96_0_in;
                                        var_73 = (var_32 == 0UL);
                                        var_74 = (r96_0_in == '\x00');
                                        rbp_6 = rbp_0;
                                        r12_0 = 0UL;
                                        r13_3 = r13_0;
                                        r81_6 = r81_0;
                                        local_sp_7 = local_sp_0;
                                        r96_6 = r96_0;
                                        r15_0_in = rbp_0;
                                        r13_2 = r13_0;
                                        local_sp_3 = local_sp_0;
                                        r96_3 = r96_0;
                                        r81_4 = r81_0;
                                        rbp_4 = rbp_0;
                                        local_sp_4 = local_sp_0;
                                        r96_4 = r96_0;
                                        if (!var_73) {
                                            loop_state_var = 1U;
                                            break;
                                        }
                                        if (!var_74) {
                                            loop_state_var = 3U;
                                            break;
                                        }
                                        *(uint64_t *)(local_sp_0 + 16UL) = r14_2_ph;
                                        r15_4 = (uint64_t)(uint32_t)rbp_0;
                                        continue;
                                    }
                                    var_61 = (uint64_t *)(local_sp_9 + 16UL);
                                    var_62 = *var_61;
                                    var_63 = (uint64_t)*(uint32_t *)(local_sp_9 + 40UL);
                                    var_64 = (uint64_t *)(local_sp_9 + 24UL);
                                    *var_64 = r81_8;
                                    *(unsigned char *)(local_sp_9 + 8UL) = (unsigned char)r96_7;
                                    var_65 = local_sp_9 + (-8L);
                                    *(uint64_t *)var_65 = 4218682UL;
                                    var_66 = indirect_placeholder_1(r12_4, var_63, var_62);
                                    var_67 = *(unsigned char *)local_sp_9;
                                    var_68 = *var_61;
                                    r13_1 = r13_5;
                                    r13_0 = r13_5;
                                    r81_0 = var_68;
                                    rbp_0 = rbp_9;
                                    local_sp_0 = var_65;
                                    r96_0_in = var_67;
                                    r81_1 = var_68;
                                    rbp_1 = rbp_9;
                                    local_sp_1 = var_65;
                                    r96_1_in = var_67;
                                    if (r12_4 == var_66) {
                                        if ((uint64_t)(uint32_t)r13_5 != 0UL) {
                                            loop_state_var = 0U;
                                            break;
                                        }
                                        r96_0 = (uint64_t)r96_0_in;
                                        var_73 = (var_32 == 0UL);
                                        var_74 = (r96_0_in == '\x00');
                                        rbp_6 = rbp_0;
                                        r12_0 = 0UL;
                                        r13_3 = r13_0;
                                        r81_6 = r81_0;
                                        local_sp_7 = local_sp_0;
                                        r96_6 = r96_0;
                                        r15_0_in = rbp_0;
                                        r13_2 = r13_0;
                                        local_sp_3 = local_sp_0;
                                        r96_3 = r96_0;
                                        r81_4 = r81_0;
                                        rbp_4 = rbp_0;
                                        local_sp_4 = local_sp_0;
                                        r96_4 = r96_0;
                                        if (!var_73) {
                                            loop_state_var = 1U;
                                            break;
                                        }
                                        if (!var_74) {
                                            loop_state_var = 3U;
                                            break;
                                        }
                                        *(uint64_t *)(local_sp_0 + 16UL) = r14_2_ph;
                                        r15_4 = (uint64_t)(uint32_t)rbp_0;
                                        continue;
                                    }
                                    var_69 = (uint64_t)var_67;
                                    var_70 = *var_64;
                                    *(uint64_t *)(local_sp_9 + (-16L)) = 4219097UL;
                                    var_71 = indirect_placeholder_8(4UL, var_70);
                                    *(uint64_t *)(local_sp_9 + (-24L)) = 4219105UL;
                                    indirect_placeholder();
                                    var_72 = (uint64_t)*(uint32_t *)var_71;
                                    *(uint64_t *)(local_sp_9 + (-32L)) = 4219124UL;
                                    indirect_placeholder_9(0UL, var_68, 4321456UL, 0UL, var_71, var_72, var_69);
                                    loop_state_var = 4U;
                                    break;
                                }
                            switch_state_var = 0;
                            switch (loop_state_var) {
                              case 1U:
                                {
                                    if (!var_74) {
                                        loop_state_var = 0U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    var_75 = (uint64_t *)local_sp_0;
                                    *var_75 = 0UL;
                                    _pre_phi281 = var_75;
                                    var_76 = *_pre_phi281;
                                    *(uint64_t *)(local_sp_0 + 16UL) = r14_2_ph;
                                    r81_1_pn = var_76;
                                    r81_3 = r81_1_pn - var_32;
                                    r81_6_ph = r81_3;
                                    r12_1 = r12_0;
                                    r12_3_ph = r12_0;
                                    rbp_4 = r15_0_in;
                                    local_sp_4 = local_sp_3;
                                    r96_4 = r96_3;
                                    r13_3_ph = r13_2;
                                    rbp_6_ph = r15_0_in;
                                    local_sp_7_ph = local_sp_3;
                                    r96_6_ph = r96_3;
                                    if (r81_3 != 0UL) {
                                        loop_state_var = 0U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    r15_4_ph = (uint64_t)(uint32_t)r15_0_in;
                                    r14_2_ph = r14_2_ph + var_32;
                                    continue;
                                }
                                break;
                              case 2U:
                                {
                                    var_48 = helper_cc_compute_c_wrapper((9223372036854775807UL - var_32) - r12_3, r12_3, var_7, 17U);
                                    r15_3 = 0UL;
                                    r15_0_in = rbp_2;
                                    r81_1_pn = r81_2;
                                    local_sp_3 = local_sp_2;
                                    r96_3 = r96_2;
                                    if (var_48 == 0UL) {
                                        var_51 = r12_3 + var_32;
                                        r12_0 = var_51;
                                        r81_3 = r81_1_pn - var_32;
                                        r81_6_ph = r81_3;
                                        r12_1 = r12_0;
                                        r12_3_ph = r12_0;
                                        rbp_4 = r15_0_in;
                                        local_sp_4 = local_sp_3;
                                        r96_4 = r96_3;
                                        r13_3_ph = r13_2;
                                        rbp_6_ph = r15_0_in;
                                        local_sp_7_ph = local_sp_3;
                                        r96_6_ph = r96_3;
                                        if (r81_3 == 0UL) {
                                            loop_state_var = 0U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                        r15_4_ph = (uint64_t)(uint32_t)r15_0_in;
                                        r14_2_ph = r14_2_ph + var_32;
                                        continue;
                                    }
                                    var_49 = *(uint64_t *)(local_sp_2 + 104UL);
                                    *(uint64_t *)(local_sp_2 + (-8L)) = 4219058UL;
                                    var_50 = indirect_placeholder_8(4UL, var_49);
                                    *(uint64_t *)(local_sp_2 + (-16L)) = 4219077UL;
                                    indirect_placeholder_10(0UL, r81_2, 4321473UL, 0UL, var_50, 0UL, r96_2);
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              case 4U:
                                {
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              case 3U:
                              case 0U:
                                {
                                    switch (loop_state_var) {
                                      case 0U:
                                        {
                                            r96_1 = (uint64_t)r96_1_in;
                                            *(uint64_t *)(local_sp_1 + 16UL) = r14_2_ph;
                                            r15_0_in = rbp_1;
                                            r13_2 = r13_1;
                                            r81_1_pn = r81_1;
                                            local_sp_3 = local_sp_1;
                                            r96_3 = r96_1;
                                        }
                                        break;
                                      case 3U:
                                        {
                                            _pre280 = (uint64_t *)local_sp_0;
                                            _pre_phi281 = _pre280;
                                            var_76 = *_pre_phi281;
                                            *(uint64_t *)(local_sp_0 + 16UL) = r14_2_ph;
                                            r81_1_pn = var_76;
                                        }
                                        break;
                                    }
                                    r81_3 = r81_1_pn - var_32;
                                    r81_6_ph = r81_3;
                                    r12_1 = r12_0;
                                    r12_3_ph = r12_0;
                                    rbp_4 = r15_0_in;
                                    local_sp_4 = local_sp_3;
                                    r96_4 = r96_3;
                                    r13_3_ph = r13_2;
                                    rbp_6_ph = r15_0_in;
                                    local_sp_7_ph = local_sp_3;
                                    r96_6_ph = r96_3;
                                    if (r81_3 == 0UL) {
                                        loop_state_var = 0U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    r15_4_ph = (uint64_t)(uint32_t)r15_0_in;
                                    r14_2_ph = r14_2_ph + var_32;
                                    continue;
                                }
                                break;
                            }
                            if (switch_state_var)
                                break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 0U:
                        {
                            var_77 = *(uint64_t *)(local_sp_4 + 72UL);
                            var_78 = (uint64_t *)(local_sp_4 + 64UL);
                            var_79 = *var_78 - var_77;
                            *var_78 = var_79;
                            var_80 = *(uint64_t *)(local_sp_4 + 112UL);
                            *(unsigned char *)var_80 = (unsigned char)rbp_4;
                            var_81 = (var_79 == 0UL);
                            var_82 = (uint64_t)(uint32_t)rbp_4;
                            rbp_5_ph = rbp_4;
                            rax_0 = var_80;
                            r12_2_ph = r12_1;
                            r15_1_ph = var_82;
                            r81_5_ph = r81_4;
                            local_sp_5_ph = local_sp_4;
                            r96_5_ph = r96_4;
                            r15_2 = var_82;
                            if (var_81) {
                                continue;
                            }
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 1U:
                        {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
                break;
              case 1U:
                {
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    if ((uint64_t)(unsigned char)r15_2 != 0UL) {
        return;
    }
    indirect_placeholder();
    return rax_0;
}
