typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_3_ret_type;
struct indirect_placeholder_13_ret_type;
struct indirect_placeholder_5_ret_type;
struct indirect_placeholder_6_ret_type;
struct indirect_placeholder_7_ret_type;
struct indirect_placeholder_9_ret_type;
struct indirect_placeholder_4_ret_type;
struct indirect_placeholder_10_ret_type;
struct indirect_placeholder_11_ret_type;
struct indirect_placeholder_12_ret_type;
struct indirect_placeholder_3_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_13_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_5_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_6_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
    uint64_t field_8;
    uint64_t field_9;
    uint64_t field_10;
    uint64_t field_11;
};
struct indirect_placeholder_7_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
    uint64_t field_8;
    uint64_t field_9;
    uint64_t field_10;
    uint64_t field_11;
};
struct indirect_placeholder_9_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_4_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_10_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
    uint64_t field_8;
    uint64_t field_9;
    uint64_t field_10;
    uint64_t field_11;
};
struct indirect_placeholder_11_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_12_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
    uint64_t field_8;
    uint64_t field_9;
    uint64_t field_10;
    uint64_t field_11;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r12(void);
extern uint64_t init_r15(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern struct indirect_placeholder_3_ret_type indirect_placeholder_3(uint64_t param_0);
extern struct indirect_placeholder_13_ret_type indirect_placeholder_13(uint64_t param_0);
extern struct indirect_placeholder_5_ret_type indirect_placeholder_5(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_6_ret_type indirect_placeholder_6(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_7_ret_type indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_9_ret_type indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_4_ret_type indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_10_ret_type indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_11_ret_type indirect_placeholder_11(uint64_t param_0);
extern struct indirect_placeholder_12_ret_type indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
uint64_t bb_elide_tail_lines_pipe(uint64_t rdx, uint64_t rdi, uint64_t rcx, uint64_t rsi) {
    struct indirect_placeholder_10_ret_type var_51;
    struct indirect_placeholder_7_ret_type var_75;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t *var_10;
    struct indirect_placeholder_3_ret_type var_11;
    uint64_t var_12;
    uint64_t var_13;
    struct indirect_placeholder_13_ret_type var_14;
    uint64_t var_15;
    uint64_t rbx_3_ph_ph;
    uint64_t local_sp_7_ph;
    uint64_t r13_2_ph;
    uint64_t r13_2;
    uint64_t r12_0;
    uint64_t local_sp_0;
    uint64_t r12_1;
    uint64_t r12_2;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t var_107;
    struct indirect_placeholder_5_ret_type var_108;
    uint64_t rdi2_1;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t rbx_0;
    uint64_t rbx_2;
    uint64_t rdi2_0;
    uint64_t local_sp_2;
    uint64_t var_94;
    uint64_t *var_95;
    uint64_t var_96;
    struct indirect_placeholder_6_ret_type var_97;
    uint64_t r12_3_ph;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t var_30;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t *var_68;
    uint64_t *var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t rbx_1;
    uint64_t r14_0;
    uint64_t rbp_0;
    uint64_t local_sp_1;
    uint64_t *var_39;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_83;
    uint64_t r13_0;
    uint64_t r14_1;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t *var_88;
    uint64_t local_sp_3;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t local_sp_5_ph;
    uint64_t local_sp_5;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t var_98;
    struct indirect_placeholder_9_ret_type var_99;
    uint64_t var_100;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t rbp_1_ph_ph;
    uint64_t *var_31;
    uint64_t rdi2_2;
    uint64_t var_32;
    bool var_33;
    uint64_t var_34;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t *var_42;
    uint64_t var_47;
    uint64_t *var_48;
    uint64_t var_49;
    uint64_t *var_50;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t local_sp_7;
    uint64_t rbx_3_ph_ph_be;
    uint64_t r15_0_ph_ph_be;
    uint64_t r8_0_ph_ph_be;
    uint64_t rbp_1_ph_ph_be;
    uint64_t local_sp_7_ph_ph_be;
    uint64_t r9_0_ph_ph_be;
    uint64_t r15_0_ph_ph;
    uint64_t r8_0_ph_ph;
    uint64_t local_sp_7_ph_ph;
    uint64_t r9_0_ph_ph;
    uint64_t rbx_3_ph;
    uint64_t r15_0_ph;
    uint64_t rbp_1_ph;
    uint64_t *var_16;
    uint64_t *var_17;
    uint64_t *var_18;
    uint64_t rbx_3;
    uint64_t var_43;
    uint64_t *var_44;
    struct indirect_placeholder_11_ret_type var_45;
    uint64_t var_46;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t *var_37;
    uint64_t var_38;
    uint64_t r13_3;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t local_sp_6;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t *var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t *var_60;
    uint64_t var_61;
    struct indirect_placeholder_12_ret_type var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t *var_65;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t *var_21;
    uint64_t var_22;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r12();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_r13();
    var_5 = init_r14();
    var_6 = init_r8();
    var_7 = init_rbp();
    var_8 = init_cc_src2();
    var_9 = init_r9();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_1;
    *(uint64_t *)(var_0 + (-40L)) = var_7;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    *(uint64_t *)(var_0 + (-64L)) = rdi;
    *(uint32_t *)(var_0 + (-92L)) = (uint32_t)rsi;
    *(uint64_t *)(var_0 + (-112L)) = rdx;
    *(uint64_t *)(var_0 + (-72L)) = rcx;
    *(uint64_t *)(var_0 + (-88L)) = rcx;
    var_10 = (uint64_t *)(var_0 + (-128L));
    *var_10 = 4205616UL;
    var_11 = indirect_placeholder_3(1048UL);
    var_12 = var_11.field_0;
    *(uint64_t *)(var_12 + 1032UL) = 0UL;
    *(uint64_t *)(var_12 + 1024UL) = 0UL;
    *(uint64_t *)(var_12 + 1040UL) = 0UL;
    *var_10 = var_12;
    var_13 = var_0 + (-136L);
    *(uint64_t *)var_13 = 4205666UL;
    var_14 = indirect_placeholder_13(1048UL);
    var_15 = var_14.field_0;
    *(uint64_t *)(var_0 + (-96L)) = var_12;
    *(uint64_t *)(var_0 + (-120L)) = 0UL;
    rbx_3_ph_ph = var_2;
    r12_0 = 0UL;
    rdi2_0 = 0UL;
    r12_3_ph = 1UL;
    rbp_1_ph_ph = var_15;
    r15_0_ph_ph = var_15;
    r8_0_ph_ph = var_6;
    local_sp_7_ph_ph = var_13;
    r9_0_ph_ph = var_9;
    r13_3 = 0UL;
    while (1U)
        {
            rbx_3_ph = rbx_3_ph_ph;
            r15_0_ph = r15_0_ph_ph;
            rbp_1_ph = rbp_1_ph_ph;
            local_sp_7_ph = local_sp_7_ph_ph;
            while (1U)
                {
                    var_16 = (uint64_t *)(rbp_1_ph + 1024UL);
                    var_17 = (uint64_t *)(rbp_1_ph + 1032UL);
                    var_18 = (uint64_t *)(rbp_1_ph + 1040UL);
                    rdi2_2 = r15_0_ph;
                    rbx_3 = rbx_3_ph;
                    local_sp_7 = local_sp_7_ph;
                    while (1U)
                        {
                            var_19 = (uint64_t)*(uint32_t *)(local_sp_7 + 28UL);
                            var_20 = local_sp_7 + (-8L);
                            var_21 = (uint64_t *)var_20;
                            *var_21 = 4205713UL;
                            var_22 = indirect_placeholder_8(1024UL, var_19, r15_0_ph);
                            local_sp_6 = var_20;
                            if ((var_22 + (-1L)) <= 18446744073709551613UL) {
                                var_63 = *(uint64_t *)(local_sp_7 + 32UL);
                                var_64 = local_sp_7 + (-16L);
                                var_65 = (uint64_t *)var_64;
                                *var_65 = 4206093UL;
                                indirect_placeholder();
                                r13_2_ph = var_63;
                                local_sp_2 = var_64;
                                r14_0 = var_63;
                                local_sp_1 = var_64;
                                r13_0 = var_63;
                                r14_1 = var_63;
                                if (var_22 != 18446744073709551615UL) {
                                    var_98 = *(uint64_t *)(local_sp_7 + 40UL);
                                    *(uint64_t *)(local_sp_7 + (-24L)) = 4206451UL;
                                    var_99 = indirect_placeholder_9(18446744073709551615UL, rbp_1_ph, 4UL, var_98);
                                    var_100 = var_99.field_0;
                                    *(uint64_t *)(local_sp_7 + (-32L)) = 4206459UL;
                                    indirect_placeholder();
                                    var_101 = (uint64_t)*(uint32_t *)var_100;
                                    var_102 = local_sp_7 + (-40L);
                                    *(uint64_t *)var_102 = 4206481UL;
                                    indirect_placeholder_4(0UL, r8_0_ph_ph, 4259977UL, 0UL, var_100, var_101, r9_0_ph_ph);
                                    local_sp_0 = var_102;
                                    r12_3_ph = 0UL;
                                    local_sp_5_ph = var_102;
                                    if (var_63 != 0UL) {
                                        loop_state_var = 4U;
                                        break;
                                    }
                                    loop_state_var = 0U;
                                    break;
                                }
                                var_66 = *var_65;
                                var_67 = *(uint64_t *)(var_66 + 1024UL);
                                if (var_67 != 0UL) {
                                    loop_state_var = 2U;
                                    break;
                                }
                                if ((uint64_t)(*(unsigned char *)((var_67 + var_66) + (-1L)) - *(unsigned char *)4285233UL) != 0UL) {
                                    loop_state_var = 2U;
                                    break;
                                }
                                var_68 = (uint64_t *)(var_66 + 1032UL);
                                *var_68 = (*var_68 + 1UL);
                                var_69 = (uint64_t *)local_sp_7;
                                *var_69 = (*var_69 + 1UL);
                                loop_state_var = 2U;
                                break;
                            }
                            if (*(uint64_t *)local_sp_7 != 0UL) {
                                loop_state_var = 3U;
                                break;
                            }
                            *var_16 = var_22;
                            var_23 = var_22 + r15_0_ph;
                            *var_17 = 0UL;
                            var_24 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)4285233UL;
                            *var_18 = 0UL;
                            var_25 = var_23 - rdi2_2;
                            var_26 = local_sp_6 + (-8L);
                            var_27 = (uint64_t *)var_26;
                            *var_27 = 4205819UL;
                            var_28 = indirect_placeholder_8(var_25, rdi2_2, var_24);
                            var_29 = r13_3 + 1UL;
                            r13_3 = var_29;
                            local_sp_6 = var_26;
                            while (var_28 != 0UL)
                                {
                                    *var_17 = var_29;
                                    rdi2_2 = var_28 + 1UL;
                                    var_25 = var_23 - rdi2_2;
                                    var_26 = local_sp_6 + (-8L);
                                    var_27 = (uint64_t *)var_26;
                                    *var_27 = 4205819UL;
                                    var_28 = indirect_placeholder_8(var_25, rdi2_2, var_24);
                                    var_29 = r13_3 + 1UL;
                                    r13_3 = var_29;
                                    local_sp_6 = var_26;
                                }
                            var_30 = *var_27;
                            var_31 = (uint64_t *)(local_sp_6 + 8UL);
                            var_32 = *var_31 + r13_3;
                            *var_31 = var_32;
                            var_33 = ((var_22 + *(uint64_t *)(var_30 + 1024UL)) > 1023UL);
                            var_34 = *var_27;
                            rbx_3 = var_34;
                            if (!var_33) {
                                loop_state_var = 1U;
                                break;
                            }
                            var_35 = local_sp_6 + (-16L);
                            *(uint64_t *)var_35 = 4206042UL;
                            indirect_placeholder();
                            var_36 = *var_16;
                            var_37 = (uint64_t *)(var_34 + 1024UL);
                            *var_37 = (*var_37 + var_36);
                            var_38 = *var_17;
                            var_39 = (uint64_t *)(var_34 + 1032UL);
                            *var_39 = (*var_39 + var_38);
                            local_sp_7 = var_35;
                            continue;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 1U:
                        {
                            var_40 = *(uint64_t *)(local_sp_6 + 32UL);
                            *(uint64_t *)(var_34 + 1040UL) = rbp_1_ph;
                            var_41 = var_32 - *(uint64_t *)(var_40 + 1032UL);
                            var_42 = (uint64_t *)local_sp_6;
                            rbx_3_ph = var_40;
                            if (var_41 <= *var_42) {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            var_43 = local_sp_6 + (-16L);
                            var_44 = (uint64_t *)var_43;
                            *var_44 = 4206002UL;
                            var_45 = indirect_placeholder_11(1048UL);
                            var_46 = var_45.field_0;
                            *var_44 = rbp_1_ph;
                            r15_0_ph = var_46;
                            rbp_1_ph = var_46;
                            local_sp_7_ph = var_43;
                            continue;
                        }
                        break;
                      case 2U:
                        {
                            var_70 = *(uint64_t *)local_sp_7;
                            var_71 = *var_21;
                            r12_2 = var_71;
                            rbx_1 = var_70;
                            var_82 = var_70;
                            var_83 = var_71;
                            if ((var_70 - *(uint64_t *)(var_63 + 1032UL)) <= var_71) {
                                loop_state_var = 2U;
                                switch_state_var = 1;
                                break;
                            }
                            rbp_0 = *(uint64_t *)(local_sp_7 + 16UL);
                            loop_state_var = 3U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 0U:
                        {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 3U:
                        {
                            loop_state_var = 4U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 4U:
                        {
                            loop_state_var = 5U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 2U:
                {
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 3U:
                {
                    loop_state_var = 2U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 5U:
                {
                    loop_state_var = 3U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 4U:
              case 1U:
                {
                    switch (loop_state_var) {
                      case 1U:
                        {
                            var_47 = *(uint64_t *)(var_40 + 1024UL);
                            var_48 = (uint64_t *)(local_sp_6 + 24UL);
                            *var_48 = (*var_48 + var_47);
                            var_49 = local_sp_6 + (-16L);
                            var_50 = (uint64_t *)var_49;
                            *var_50 = 4205921UL;
                            var_51 = indirect_placeholder_10(var_22, var_40, var_40, var_40, var_47);
                            var_52 = var_51.field_1;
                            var_53 = var_51.field_2;
                            var_54 = var_51.field_4;
                            var_55 = var_51.field_5;
                            var_56 = var_51.field_11;
                            var_57 = *(uint64_t *)(var_52 + 1032UL);
                            var_58 = *(uint64_t *)(var_52 + 1040UL);
                            *var_50 = var_51.field_6;
                            var_59 = var_54 - var_57;
                            *var_48 = var_58;
                            *var_42 = var_59;
                            rbx_3_ph_ph_be = var_52;
                            r15_0_ph_ph_be = var_53;
                            r8_0_ph_ph_be = var_55;
                            rbp_1_ph_ph_be = var_52;
                            local_sp_7_ph_ph_be = var_49;
                            r9_0_ph_ph_be = var_56;
                        }
                        break;
                      case 4U:
                        {
                            var_60 = (uint64_t *)(local_sp_7 + 24UL);
                            *var_60 = (*var_60 + var_22);
                            var_61 = local_sp_7 + (-16L);
                            *(uint64_t *)var_61 = 4205984UL;
                            var_62 = indirect_placeholder_12(var_22, rbx_3, r15_0_ph, var_22);
                            rbx_3_ph_ph_be = var_62.field_1;
                            r15_0_ph_ph_be = var_62.field_2;
                            r8_0_ph_ph_be = var_62.field_5;
                            rbp_1_ph_ph_be = var_62.field_6;
                            local_sp_7_ph_ph_be = var_61;
                            r9_0_ph_ph_be = var_62.field_11;
                        }
                        break;
                    }
                    rbx_3_ph_ph = rbx_3_ph_ph_be;
                    rbp_1_ph_ph = rbp_1_ph_ph_be;
                    r15_0_ph_ph = r15_0_ph_ph_be;
                    r8_0_ph_ph = r8_0_ph_ph_be;
                    local_sp_7_ph_ph = local_sp_7_ph_ph_be;
                    r9_0_ph_ph = r9_0_ph_ph_be;
                    continue;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 3U:
        {
            r12_1 = r12_0;
            if ((long)*(uint64_t *)(local_sp_0 + 48UL) >= (long)0UL) {
                var_105 = *(uint64_t *)(local_sp_0 + 56UL);
                var_106 = *(uint64_t *)(local_sp_0 + 32UL);
                var_107 = (uint64_t)*(uint32_t *)(local_sp_0 + 28UL);
                *(uint64_t *)(local_sp_0 + (-8L)) = 4206369UL;
                var_108 = indirect_placeholder_5(0UL, var_107, var_105, var_106);
                r12_1 = ((long)var_108.field_0 < (long)0UL) ? 0UL : (uint64_t)(uint32_t)r12_0;
            }
            return (uint64_t)(uint32_t)r12_1;
        }
        break;
      case 2U:
      case 1U:
      case 0U:
        {
            switch (loop_state_var) {
              case 0U:
                {
                    r12_0 = r12_3_ph;
                    r13_2 = r13_2_ph;
                    local_sp_5 = local_sp_5_ph;
                    var_103 = *(uint64_t *)(r13_2 + 1040UL);
                    var_104 = local_sp_5 + (-8L);
                    *(uint64_t *)var_104 = 4206335UL;
                    indirect_placeholder();
                    local_sp_0 = var_104;
                    r13_2 = var_103;
                    local_sp_5 = var_104;
                    do {
                        var_103 = *(uint64_t *)(r13_2 + 1040UL);
                        var_104 = local_sp_5 + (-8L);
                        *(uint64_t *)var_104 = 4206335UL;
                        indirect_placeholder();
                        local_sp_0 = var_104;
                        r13_2 = var_103;
                        local_sp_5 = var_104;
                    } while (var_103 != 0UL);
                }
                break;
              case 2U:
              case 1U:
                {
                    switch (loop_state_var) {
                      case 1U:
                        {
                            var_84 = helper_cc_compute_c_wrapper(var_83 - var_82, var_82, var_8, 17U);
                            rdi2_1 = r14_1;
                            local_sp_3 = local_sp_2;
                            r13_2_ph = r13_0;
                            local_sp_5_ph = local_sp_2;
                            if (var_84 != 0UL) {
                                var_85 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)4285233UL;
                                var_86 = var_82 - var_83;
                                var_87 = *(uint64_t *)(r14_1 + 1024UL) + r14_1;
                                var_88 = (uint64_t *)(r14_1 + 1032UL);
                                rbx_2 = var_86;
                                var_89 = var_87 - rdi2_1;
                                var_90 = local_sp_3 + (-8L);
                                *(uint64_t *)var_90 = 4206269UL;
                                var_91 = indirect_placeholder_8(var_89, rdi2_1, var_85);
                                rbx_0 = rbx_2;
                                local_sp_3 = var_90;
                                while (var_91 != 0UL)
                                    {
                                        *var_88 = (*var_88 + 1UL);
                                        var_92 = var_91 + 1UL;
                                        var_93 = rbx_2 + (-1L);
                                        rbx_0 = 0UL;
                                        rdi2_0 = var_92;
                                        rbx_2 = var_93;
                                        rdi2_1 = var_92;
                                        if (var_93 == 0UL) {
                                            break;
                                        }
                                        var_89 = var_87 - rdi2_1;
                                        var_90 = local_sp_3 + (-8L);
                                        *(uint64_t *)var_90 = 4206269UL;
                                        var_91 = indirect_placeholder_8(var_89, rdi2_1, var_85);
                                        rbx_0 = rbx_2;
                                        local_sp_3 = var_90;
                                    }
                                var_94 = rdi2_0 - r14_1;
                                var_95 = (uint64_t *)(local_sp_3 + 24UL);
                                *var_95 = (*var_95 + var_94);
                                var_96 = local_sp_3 + (-16L);
                                *(uint64_t *)var_96 = 4206314UL;
                                var_97 = indirect_placeholder_6(var_87, rbx_0, r14_1, var_94);
                                r13_2_ph = var_97.field_3;
                                local_sp_5_ph = var_96;
                            }
                        }
                        break;
                      case 2U:
                        {
                            var_72 = *(uint64_t *)(r14_0 + 1024UL);
                            var_73 = rbp_0 + var_72;
                            var_74 = local_sp_1 + (-8L);
                            *(uint64_t *)var_74 = 4206178UL;
                            var_75 = indirect_placeholder_7(r12_2, rbx_1, var_73, r14_0, var_72);
                            var_76 = var_75.field_0;
                            var_77 = var_75.field_1;
                            var_78 = var_75.field_4;
                            var_79 = var_77 - *(uint64_t *)(var_78 + 1032UL);
                            var_80 = *(uint64_t *)(var_78 + 1040UL);
                            r12_2 = var_76;
                            local_sp_2 = var_74;
                            var_82 = var_79;
                            rbx_1 = var_79;
                            r14_0 = var_80;
                            local_sp_1 = var_74;
                            r14_1 = var_80;
                            while ((var_79 - *(uint64_t *)(var_80 + 1032UL)) <= var_76)
                                {
                                    rbp_0 = var_75.field_6;
                                    var_72 = *(uint64_t *)(r14_0 + 1024UL);
                                    var_73 = rbp_0 + var_72;
                                    var_74 = local_sp_1 + (-8L);
                                    *(uint64_t *)var_74 = 4206178UL;
                                    var_75 = indirect_placeholder_7(r12_2, rbx_1, var_73, r14_0, var_72);
                                    var_76 = var_75.field_0;
                                    var_77 = var_75.field_1;
                                    var_78 = var_75.field_4;
                                    var_79 = var_77 - *(uint64_t *)(var_78 + 1032UL);
                                    var_80 = *(uint64_t *)(var_78 + 1040UL);
                                    r12_2 = var_76;
                                    local_sp_2 = var_74;
                                    var_82 = var_79;
                                    rbx_1 = var_79;
                                    r14_0 = var_80;
                                    local_sp_1 = var_74;
                                    r14_1 = var_80;
                                }
                            var_81 = var_75.field_3;
                            *(uint64_t *)(local_sp_1 + 24UL) = var_75.field_6;
                            *(uint64_t *)(local_sp_1 + 8UL) = var_79;
                            var_83 = *(uint64_t *)local_sp_1;
                            r13_0 = var_81;
                        }
                        break;
                    }
                }
                break;
            }
        }
        break;
    }
}
