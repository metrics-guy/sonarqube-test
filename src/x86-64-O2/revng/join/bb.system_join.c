typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_61_ret_type;
struct indirect_placeholder_60_ret_type;
struct indirect_placeholder_49_ret_type;
struct indirect_placeholder_51_ret_type;
struct indirect_placeholder_50_ret_type;
struct indirect_placeholder_52_ret_type;
struct indirect_placeholder_53_ret_type;
struct indirect_placeholder_54_ret_type;
struct indirect_placeholder_55_ret_type;
struct indirect_placeholder_56_ret_type;
struct indirect_placeholder_57_ret_type;
struct indirect_placeholder_58_ret_type;
struct indirect_placeholder_59_ret_type;
struct indirect_placeholder_61_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_60_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_49_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_51_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_50_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_52_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_53_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_54_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_55_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_56_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_57_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_58_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_59_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t init_r12(void);
extern uint64_t init_r15(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r9(void);
extern void indirect_placeholder_22(uint64_t param_0);
extern void indirect_placeholder_28(uint64_t param_0, uint64_t param_1);
extern uint64_t init_r8(void);
extern uint64_t init_r10(void);
extern struct indirect_placeholder_61_ret_type indirect_placeholder_61(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_60_ret_type indirect_placeholder_60(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_49_ret_type indirect_placeholder_49(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_51_ret_type indirect_placeholder_51(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_50_ret_type indirect_placeholder_50(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_52_ret_type indirect_placeholder_52(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_53_ret_type indirect_placeholder_53(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_54_ret_type indirect_placeholder_54(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_55_ret_type indirect_placeholder_55(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_56_ret_type indirect_placeholder_56(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_57_ret_type indirect_placeholder_57(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_58_ret_type indirect_placeholder_58(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_59_ret_type indirect_placeholder_59(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
typedef _Bool bool;
void bb_system_join(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_50_ret_type var_75;
    uint64_t r8_7;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t *var_16;
    uint64_t var_17;
    struct indirect_placeholder_61_ret_type var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    struct indirect_placeholder_60_ret_type var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t rbx_4;
    uint64_t rcx_14;
    uint64_t r9_9;
    uint64_t rcx_11_be;
    uint64_t local_sp_15_be;
    uint64_t r15_0;
    uint64_t rbx_6;
    uint64_t r9_12;
    uint64_t rcx_15_be;
    uint64_t local_sp_19_be;
    uint64_t r8_10;
    uint64_t var_166;
    uint64_t var_167;
    uint64_t var_168;
    uint64_t r9_2;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t *var_97;
    unsigned char var_98;
    uint64_t r8_3_ph248;
    uint64_t r10_1;
    uint64_t local_sp_4;
    uint64_t r9_1;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    struct indirect_placeholder_49_ret_type var_85;
    uint64_t r15_2;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_90;
    unsigned char var_91;
    uint64_t r8_0;
    uint64_t rcx_0;
    uint64_t r10_0;
    uint64_t local_sp_0;
    uint64_t r9_0;
    uint64_t rcx_2;
    uint64_t r13_0;
    uint64_t local_sp_3;
    uint64_t rcx_1;
    uint64_t rbx_0;
    uint64_t local_sp_1;
    uint64_t var_99;
    uint64_t var_100;
    uint64_t r14_0;
    uint64_t local_sp_2;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t var_107;
    uint64_t var_108;
    uint64_t r15_1;
    uint64_t r8_1;
    uint64_t rcx_3;
    uint64_t rdi1_1;
    uint64_t r8_2;
    uint64_t rcx_5;
    uint64_t r10_4;
    uint64_t local_sp_9;
    uint64_t var_109;
    uint64_t *var_110;
    uint64_t var_111;
    uint64_t *var_112;
    uint64_t var_113;
    uint64_t *_cast9;
    uint64_t rcx_4;
    uint64_t var_114;
    uint64_t *var_115;
    uint64_t var_116;
    uint64_t *var_117;
    uint64_t var_118;
    uint64_t *_cast8;
    uint64_t rcx_6_ph249_be;
    uint64_t rbx_1_ph;
    uint64_t r9_3;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t *var_72;
    uint64_t r15_2_ph;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    struct indirect_placeholder_51_ret_type var_69;
    uint64_t r10_3;
    uint64_t r10_2_ph;
    uint64_t local_sp_5_ph;
    uint64_t r9_2_ph;
    uint64_t r10_2;
    uint64_t local_sp_5;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_153;
    uint64_t var_154;
    uint64_t var_155;
    uint64_t rax_2;
    uint64_t var_55;
    uint64_t local_sp_6;
    uint64_t r9_6;
    uint64_t var_121;
    uint64_t var_122;
    struct indirect_placeholder_52_ret_type var_123;
    uint64_t var_124;
    uint64_t r8_6;
    uint64_t rcx_9;
    uint64_t var_119;
    uint64_t var_120;
    uint64_t local_sp_7;
    uint64_t var_56;
    uint64_t var_57;
    struct indirect_placeholder_53_ret_type var_58;
    uint64_t local_sp_8;
    uint64_t var_131;
    uint64_t var_132;
    struct indirect_placeholder_54_ret_type var_133;
    uint64_t var_134;
    uint64_t var_135;
    uint64_t var_136;
    uint64_t var_137;
    uint64_t r8_3;
    uint64_t var_129;
    uint64_t var_130;
    uint64_t r10_12;
    uint64_t local_sp_19;
    uint64_t var_161;
    uint64_t r9_4;
    uint64_t var_38;
    uint64_t r8_3_ph;
    uint64_t var_39;
    struct indirect_placeholder_55_ret_type var_40;
    uint64_t var_36;
    struct indirect_placeholder_56_ret_type var_37;
    uint64_t rax_0;
    uint64_t rax_1;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t rdi1_0;
    uint64_t rax_3;
    uint64_t rsi2_0;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t rcx_6_ph;
    uint64_t r10_5_ph;
    uint64_t local_sp_10_ph;
    uint64_t r9_5_ph;
    uint64_t rcx_6_ph249;
    uint64_t r10_5_ph250;
    uint64_t local_sp_10_ph251;
    uint64_t r9_5_ph252;
    uint64_t rcx_6;
    uint64_t r10_5;
    uint64_t local_sp_10;
    uint64_t r9_5;
    uint64_t var_41;
    uint64_t r8_4;
    uint64_t rcx_7;
    uint64_t r10_6;
    uint64_t local_sp_11;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    struct indirect_placeholder_57_ret_type var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t rbx_3;
    uint64_t r10_8;
    uint64_t var_125;
    uint64_t var_126;
    uint64_t var_127;
    uint64_t var_128;
    uint64_t local_sp_13;
    uint64_t r9_8;
    unsigned char var_138;
    uint64_t var_139;
    uint64_t r13_2;
    uint64_t r13_1;
    uint64_t rcx_10;
    uint64_t local_sp_14;
    uint64_t rcx_11;
    uint64_t var_145;
    uint64_t r10_9;
    uint64_t local_sp_15;
    uint64_t var_146;
    struct indirect_placeholder_58_ret_type var_147;
    uint64_t var_148;
    uint64_t var_149;
    uint64_t var_150;
    uint64_t var_151;
    uint64_t var_152;
    uint64_t rax_4;
    unsigned char var_140;
    uint64_t var_141;
    uint64_t var_142;
    uint64_t var_143;
    uint64_t var_144;
    uint64_t r13_3;
    uint64_t r8_8;
    uint64_t rcx_12;
    uint64_t r10_10;
    uint64_t local_sp_16;
    uint64_t r9_10;
    unsigned char var_156;
    uint64_t rbx_5;
    uint64_t r8_9;
    uint64_t rcx_13;
    uint64_t r10_11;
    uint64_t local_sp_17;
    uint64_t r9_11;
    uint64_t local_sp_18;
    uint64_t rcx_15;
    uint64_t var_160;
    struct indirect_placeholder_59_ret_type var_162;
    uint64_t var_163;
    uint64_t var_164;
    uint64_t var_165;
    uint64_t var_157;
    uint64_t var_158;
    uint64_t var_159;
    uint64_t local_sp_20;
    uint64_t var_169;
    uint64_t var_170;
    uint64_t var_171;
    uint64_t var_172;
    uint64_t var_173;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r12();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_r13();
    var_5 = init_r14();
    var_6 = init_r8();
    var_7 = init_rbp();
    var_8 = init_r10();
    var_9 = init_cc_src2();
    var_10 = init_r9();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_1;
    *(uint64_t *)(var_0 + (-40L)) = var_7;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    *(uint64_t *)(var_0 + (-160L)) = 4208894UL;
    var_11 = indirect_placeholder_2(rdi, rdi, 2UL);
    var_12 = (uint64_t *)(var_0 + (-168L));
    *var_12 = 4208907UL;
    var_13 = indirect_placeholder_2(var_11, rsi, 2UL);
    var_14 = var_0 + (-136L);
    var_15 = var_0 + (-176L);
    var_16 = (uint64_t *)var_15;
    *var_16 = 4208917UL;
    indirect_placeholder_22(var_14);
    var_17 = var_0 + (-144L);
    *(uint64_t *)(var_0 + (-184L)) = 4208935UL;
    var_18 = indirect_placeholder_61(var_6, 1UL, var_13, var_8, var_17, var_10);
    var_19 = var_18.field_0;
    var_20 = var_18.field_2;
    var_21 = var_18.field_3;
    var_22 = var_0 + (-120L);
    *(uint64_t *)(var_0 + (-192L)) = 4208945UL;
    indirect_placeholder_22(var_22);
    var_23 = var_0 + (-128L);
    var_24 = var_0 + (-200L);
    *(uint64_t *)var_24 = 4208963UL;
    var_25 = indirect_placeholder_60(var_19, 2UL, rsi, var_20, var_23, var_21);
    var_26 = var_25.field_0;
    var_27 = var_25.field_1;
    var_28 = var_25.field_2;
    var_29 = var_25.field_3;
    r13_0 = 0UL;
    r14_0 = 0UL;
    r8_2 = var_26;
    r10_4 = var_28;
    rbx_1_ph = var_2;
    r15_2_ph = 0UL;
    r8_6 = var_26;
    rcx_9 = var_27;
    r9_4 = var_29;
    r8_3_ph = var_26;
    rax_0 = 0UL;
    rax_1 = 0UL;
    rdi1_0 = 4289472UL;
    rax_3 = 0UL;
    rsi2_0 = 4289472UL;
    rcx_6_ph = var_27;
    r10_5_ph = var_28;
    local_sp_10_ph = var_24;
    r9_5_ph = var_29;
    rbx_3 = var_2;
    r10_8 = var_28;
    local_sp_13 = var_24;
    r9_8 = var_29;
    r13_2 = 0UL;
    r13_1 = 1UL;
    if (*(unsigned char *)4289576UL != '\x00') {
        if (*var_12 == 0UL) {
            rax_0 = *(uint64_t *)(**(uint64_t **)(var_0 + (-152L)) + 24UL);
        }
        *(uint64_t *)4289568UL = rax_0;
        if (*(uint64_t *)var_14 != 0UL) {
            rax_1 = *(uint64_t *)(**(uint64_t **)var_22 + 24UL);
        }
        *(uint64_t *)4289560UL = rax_1;
    }
    if (*(unsigned char *)4289440UL != '\x00') {
        var_30 = *var_12;
        var_31 = *(uint64_t *)var_14;
        if (var_30 == 0UL) {
            if (var_31 != 0UL) {
                *(uint64_t *)(local_sp_13 + 24UL) = 0UL;
                r8_7 = r8_6;
                rbx_4 = rbx_3;
                r9_9 = r9_8;
                rcx_10 = rcx_9;
                local_sp_14 = local_sp_13;
                r10_9 = r10_8;
                r8_8 = r8_6;
                rcx_12 = rcx_9;
                r10_10 = r10_8;
                local_sp_16 = local_sp_13;
                r9_10 = r9_8;
                rbx_5 = rbx_3;
                r8_9 = r8_6;
                rcx_13 = rcx_9;
                r10_11 = r10_8;
                local_sp_17 = local_sp_13;
                r9_11 = r9_8;
                if (*(uint32_t *)4289520UL != 2U) {
                    var_140 = *(unsigned char *)4289597UL;
                    var_141 = (uint64_t)var_140;
                    r13_3 = var_141;
                    r13_3 = 0UL;
                    if (var_140 != '\x00' & rax_3 != 0UL) {
                        var_142 = **(uint64_t **)(local_sp_13 + 48UL);
                        var_143 = local_sp_13 + (-8L);
                        *(uint64_t *)var_143 = 4210391UL;
                        var_144 = indirect_placeholder_1(var_142, 4289472UL);
                        r13_1 = r13_2;
                        rcx_10 = var_144;
                        local_sp_14 = var_143;
                        rcx_11 = rcx_10;
                        local_sp_15 = local_sp_14;
                        r13_3 = r13_1;
                        if (*(uint64_t *)(local_sp_14 + 64UL) == 0UL) {
                            *(unsigned char *)4289594UL = (unsigned char)'\x01';
                        }
                        var_145 = local_sp_14 + 24UL;
                        rbx_4 = var_145;
                        var_146 = local_sp_15 + (-8L);
                        *(uint64_t *)var_146 = 4209376UL;
                        var_147 = indirect_placeholder_58(r8_7, 1UL, var_13, rcx_11, r10_9, var_145, r9_9);
                        var_148 = var_147.field_0;
                        var_149 = var_147.field_1;
                        var_150 = var_147.field_2;
                        var_151 = var_147.field_3;
                        var_152 = var_147.field_4;
                        r8_7 = var_149;
                        r9_9 = var_152;
                        rcx_11_be = var_150;
                        local_sp_15_be = var_146;
                        r10_9 = var_151;
                        r8_8 = var_149;
                        rcx_12 = var_150;
                        r10_10 = var_151;
                        local_sp_16 = var_146;
                        r9_10 = var_152;
                        while ((uint64_t)(unsigned char)var_148 != 0UL)
                            {
                                if (*(unsigned char *)4289597UL == '\x00') {
                                    if (*(unsigned char *)4289592UL == '\x00') {
                                        break;
                                    }
                                }
                                var_153 = *(uint64_t *)(local_sp_15 + 16UL);
                                var_154 = local_sp_15 + (-16L);
                                *(uint64_t *)var_154 = 4210415UL;
                                var_155 = indirect_placeholder_1(var_153, 4289472UL);
                                rcx_11_be = var_155;
                                local_sp_15_be = var_154;
                                rcx_12 = var_155;
                                local_sp_16 = var_154;
                                if (*(unsigned char *)4289592UL != '\x00') {
                                    if (*(unsigned char *)4289597UL == '\x00') {
                                        break;
                                    }
                                }
                                rcx_11 = rcx_11_be;
                                local_sp_15 = local_sp_15_be;
                                var_146 = local_sp_15 + (-8L);
                                *(uint64_t *)var_146 = 4209376UL;
                                var_147 = indirect_placeholder_58(r8_7, 1UL, var_13, rcx_11, r10_9, var_145, r9_9);
                                var_148 = var_147.field_0;
                                var_149 = var_147.field_1;
                                var_150 = var_147.field_2;
                                var_151 = var_147.field_3;
                                var_152 = var_147.field_4;
                                r8_7 = var_149;
                                r9_9 = var_152;
                                rcx_11_be = var_150;
                                local_sp_15_be = var_146;
                                r10_9 = var_151;
                                r8_8 = var_149;
                                rcx_12 = var_150;
                                r10_10 = var_151;
                                local_sp_16 = var_146;
                                r9_10 = var_152;
                            }
                    }
                    var_156 = *(unsigned char *)4289596UL;
                    rbx_6 = rbx_4;
                    rax_4 = (uint64_t)var_156;
                    rbx_5 = rbx_4;
                    r8_9 = r8_8;
                    rcx_13 = rcx_12;
                    r10_11 = r10_10;
                    local_sp_17 = local_sp_16;
                    r9_11 = r9_10;
                    local_sp_20 = local_sp_16;
                    if ((var_156 == '\x00') && ((uint64_t)(unsigned char)r13_3 == 0UL)) {
                        var_169 = local_sp_20 + 24UL;
                        var_170 = *(uint64_t *)var_169;
                        *(uint64_t *)(local_sp_20 + (-8L)) = 4210063UL;
                        indirect_placeholder_1(rbx_6, var_170);
                        *(uint64_t *)(local_sp_20 + (-16L)) = 4210073UL;
                        indirect_placeholder();
                        var_171 = local_sp_20 + 32UL;
                        *(uint64_t *)(local_sp_20 + (-24L)) = 4210088UL;
                        indirect_placeholder_28(var_169, var_171);
                        var_172 = local_sp_20 + 56UL;
                        var_173 = local_sp_20 + 48UL;
                        *(uint64_t *)(local_sp_20 + (-32L)) = 4210103UL;
                        indirect_placeholder_28(var_173, var_172);
                        return;
                    }
                    rcx_14 = rcx_13;
                    rbx_6 = rbx_5;
                    r9_12 = r9_11;
                    r8_10 = r8_9;
                    r10_12 = r10_11;
                    local_sp_18 = local_sp_17;
                    local_sp_20 = local_sp_17;
                    if (*(uint64_t *)(local_sp_17 + 64UL) != 0UL) {
                        if (rax_4 == 0UL) {
                            var_157 = **(uint64_t **)(local_sp_17 + 80UL);
                            var_158 = local_sp_17 + (-8L);
                            *(uint64_t *)var_158 = 4210356UL;
                            var_159 = indirect_placeholder_1(4289472UL, var_157);
                            rcx_14 = var_159;
                            local_sp_18 = var_158;
                        }
                        rcx_15 = rcx_14;
                        local_sp_19 = local_sp_18;
                        if (*(uint64_t *)(local_sp_18 + 32UL) == 0UL) {
                            *(unsigned char *)4289594UL = (unsigned char)'\x01';
                        }
                        var_160 = local_sp_18 + 24UL;
                        rbx_6 = var_160;
                        var_161 = local_sp_19 + (-8L);
                        *(uint64_t *)var_161 = 4210176UL;
                        var_162 = indirect_placeholder_59(r8_10, 2UL, rsi, rcx_15, r10_12, var_160, r9_12);
                        var_163 = var_162.field_1;
                        var_164 = var_162.field_3;
                        var_165 = var_162.field_4;
                        r9_12 = var_165;
                        rcx_15_be = var_162.field_2;
                        local_sp_19_be = var_161;
                        r8_10 = var_163;
                        r10_12 = var_164;
                        local_sp_20 = var_161;
                        while ((uint64_t)(unsigned char)var_162.field_0 != 0UL)
                            {
                                if (*(unsigned char *)4289596UL == '\x00') {
                                    if (*(unsigned char *)4289593UL == '\x00') {
                                        break;
                                    }
                                }
                                var_166 = *(uint64_t *)(local_sp_19 + 16UL);
                                var_167 = local_sp_19 + (-16L);
                                *(uint64_t *)var_167 = 4210271UL;
                                var_168 = indirect_placeholder_1(4289472UL, var_166);
                                rcx_15_be = var_168;
                                local_sp_19_be = var_167;
                                local_sp_20 = var_167;
                                if (*(unsigned char *)4289593UL != '\x00') {
                                    if (*(unsigned char *)4289596UL == '\x00') {
                                        break;
                                    }
                                }
                                rcx_15 = rcx_15_be;
                                local_sp_19 = local_sp_19_be;
                                var_161 = local_sp_19 + (-8L);
                                *(uint64_t *)var_161 = 4210176UL;
                                var_162 = indirect_placeholder_59(r8_10, 2UL, rsi, rcx_15, r10_12, var_160, r9_12);
                                var_163 = var_162.field_1;
                                var_164 = var_162.field_3;
                                var_165 = var_162.field_4;
                                r9_12 = var_165;
                                rcx_15_be = var_162.field_2;
                                local_sp_19_be = var_161;
                                r8_10 = var_163;
                                r10_12 = var_164;
                                local_sp_20 = var_161;
                            }
                    }
                    var_169 = local_sp_20 + 24UL;
                    var_170 = *(uint64_t *)var_169;
                    *(uint64_t *)(local_sp_20 + (-8L)) = 4210063UL;
                    indirect_placeholder_1(rbx_6, var_170);
                    *(uint64_t *)(local_sp_20 + (-16L)) = 4210073UL;
                    indirect_placeholder();
                    var_171 = local_sp_20 + 32UL;
                    *(uint64_t *)(local_sp_20 + (-24L)) = 4210088UL;
                    indirect_placeholder_28(var_169, var_171);
                    var_172 = local_sp_20 + 56UL;
                    var_173 = local_sp_20 + 48UL;
                    *(uint64_t *)(local_sp_20 + (-32L)) = 4210103UL;
                    indirect_placeholder_28(var_173, var_172);
                    return;
                }
                if (*(unsigned char *)4289592UL != '\x00') {
                    if (rax_3 == 0UL) {
                        var_138 = *(unsigned char *)4289597UL;
                        var_139 = (uint64_t)var_138;
                        r13_2 = var_139;
                        if (var_138 == '\x00') {
                            var_142 = **(uint64_t **)(local_sp_13 + 48UL);
                            var_143 = local_sp_13 + (-8L);
                            *(uint64_t *)var_143 = 4210391UL;
                            var_144 = indirect_placeholder_1(var_142, 4289472UL);
                            r13_1 = r13_2;
                            rcx_10 = var_144;
                            local_sp_14 = var_143;
                        }
                        rcx_11 = rcx_10;
                        local_sp_15 = local_sp_14;
                        r13_3 = r13_1;
                        if (*(uint64_t *)(local_sp_14 + 64UL) == 0UL) {
                            *(unsigned char *)4289594UL = (unsigned char)'\x01';
                        }
                        var_145 = local_sp_14 + 24UL;
                        rbx_4 = var_145;
                        var_146 = local_sp_15 + (-8L);
                        *(uint64_t *)var_146 = 4209376UL;
                        var_147 = indirect_placeholder_58(r8_7, 1UL, var_13, rcx_11, r10_9, var_145, r9_9);
                        var_148 = var_147.field_0;
                        var_149 = var_147.field_1;
                        var_150 = var_147.field_2;
                        var_151 = var_147.field_3;
                        var_152 = var_147.field_4;
                        r8_7 = var_149;
                        r9_9 = var_152;
                        rcx_11_be = var_150;
                        local_sp_15_be = var_146;
                        r10_9 = var_151;
                        r8_8 = var_149;
                        rcx_12 = var_150;
                        r10_10 = var_151;
                        local_sp_16 = var_146;
                        r9_10 = var_152;
                        while ((uint64_t)(unsigned char)var_148 != 0UL)
                            {
                                if (*(unsigned char *)4289597UL == '\x00') {
                                    if (*(unsigned char *)4289592UL == '\x00') {
                                        break;
                                    }
                                }
                                var_153 = *(uint64_t *)(local_sp_15 + 16UL);
                                var_154 = local_sp_15 + (-16L);
                                *(uint64_t *)var_154 = 4210415UL;
                                var_155 = indirect_placeholder_1(var_153, 4289472UL);
                                rcx_11_be = var_155;
                                local_sp_15_be = var_154;
                                rcx_12 = var_155;
                                local_sp_16 = var_154;
                                if (*(unsigned char *)4289592UL != '\x00') {
                                    if (*(unsigned char *)4289597UL == '\x00') {
                                        break;
                                    }
                                }
                                rcx_11 = rcx_11_be;
                                local_sp_15 = local_sp_15_be;
                                var_146 = local_sp_15 + (-8L);
                                *(uint64_t *)var_146 = 4209376UL;
                                var_147 = indirect_placeholder_58(r8_7, 1UL, var_13, rcx_11, r10_9, var_145, r9_9);
                                var_148 = var_147.field_0;
                                var_149 = var_147.field_1;
                                var_150 = var_147.field_2;
                                var_151 = var_147.field_3;
                                var_152 = var_147.field_4;
                                r8_7 = var_149;
                                r9_9 = var_152;
                                rcx_11_be = var_150;
                                local_sp_15_be = var_146;
                                r10_9 = var_151;
                                r8_8 = var_149;
                                rcx_12 = var_150;
                                r10_10 = var_151;
                                local_sp_16 = var_146;
                                r9_10 = var_152;
                            }
                        var_156 = *(unsigned char *)4289596UL;
                        rbx_6 = rbx_4;
                        rax_4 = (uint64_t)var_156;
                        rbx_5 = rbx_4;
                        r8_9 = r8_8;
                        rcx_13 = rcx_12;
                        r10_11 = r10_10;
                        local_sp_17 = local_sp_16;
                        r9_11 = r9_10;
                        local_sp_20 = local_sp_16;
                        if ((var_156 == '\x00') && ((uint64_t)(unsigned char)r13_3 == 0UL)) {
                            var_169 = local_sp_20 + 24UL;
                            var_170 = *(uint64_t *)var_169;
                            *(uint64_t *)(local_sp_20 + (-8L)) = 4210063UL;
                            indirect_placeholder_1(rbx_6, var_170);
                            *(uint64_t *)(local_sp_20 + (-16L)) = 4210073UL;
                            indirect_placeholder();
                            var_171 = local_sp_20 + 32UL;
                            *(uint64_t *)(local_sp_20 + (-24L)) = 4210088UL;
                            indirect_placeholder_28(var_169, var_171);
                            var_172 = local_sp_20 + 56UL;
                            var_173 = local_sp_20 + 48UL;
                            *(uint64_t *)(local_sp_20 + (-32L)) = 4210103UL;
                            indirect_placeholder_28(var_173, var_172);
                            return;
                        }
                    }
                    rax_4 = (uint64_t)*(unsigned char *)4289596UL;
                    rcx_14 = rcx_13;
                    rbx_6 = rbx_5;
                    r9_12 = r9_11;
                    r8_10 = r8_9;
                    r10_12 = r10_11;
                    local_sp_18 = local_sp_17;
                    local_sp_20 = local_sp_17;
                    if (*(uint64_t *)(local_sp_17 + 64UL) != 0UL) {
                        if (rax_4 == 0UL) {
                            var_157 = **(uint64_t **)(local_sp_17 + 80UL);
                            var_158 = local_sp_17 + (-8L);
                            *(uint64_t *)var_158 = 4210356UL;
                            var_159 = indirect_placeholder_1(4289472UL, var_157);
                            rcx_14 = var_159;
                            local_sp_18 = var_158;
                        }
                        rcx_15 = rcx_14;
                        local_sp_19 = local_sp_18;
                        if (*(uint64_t *)(local_sp_18 + 32UL) == 0UL) {
                            *(unsigned char *)4289594UL = (unsigned char)'\x01';
                        }
                        var_160 = local_sp_18 + 24UL;
                        rbx_6 = var_160;
                        var_161 = local_sp_19 + (-8L);
                        *(uint64_t *)var_161 = 4210176UL;
                        var_162 = indirect_placeholder_59(r8_10, 2UL, rsi, rcx_15, r10_12, var_160, r9_12);
                        var_163 = var_162.field_1;
                        var_164 = var_162.field_3;
                        var_165 = var_162.field_4;
                        r9_12 = var_165;
                        rcx_15_be = var_162.field_2;
                        local_sp_19_be = var_161;
                        r8_10 = var_163;
                        r10_12 = var_164;
                        local_sp_20 = var_161;
                        while ((uint64_t)(unsigned char)var_162.field_0 != 0UL)
                            {
                                if (*(unsigned char *)4289596UL == '\x00') {
                                    if (*(unsigned char *)4289593UL == '\x00') {
                                        break;
                                    }
                                }
                                var_166 = *(uint64_t *)(local_sp_19 + 16UL);
                                var_167 = local_sp_19 + (-16L);
                                *(uint64_t *)var_167 = 4210271UL;
                                var_168 = indirect_placeholder_1(4289472UL, var_166);
                                rcx_15_be = var_168;
                                local_sp_19_be = var_167;
                                local_sp_20 = var_167;
                                if (*(unsigned char *)4289593UL != '\x00') {
                                    if (*(unsigned char *)4289596UL == '\x00') {
                                        break;
                                    }
                                }
                                rcx_15 = rcx_15_be;
                                local_sp_19 = local_sp_19_be;
                                var_161 = local_sp_19 + (-8L);
                                *(uint64_t *)var_161 = 4210176UL;
                                var_162 = indirect_placeholder_59(r8_10, 2UL, rsi, rcx_15, r10_12, var_160, r9_12);
                                var_163 = var_162.field_1;
                                var_164 = var_162.field_3;
                                var_165 = var_162.field_4;
                                r9_12 = var_165;
                                rcx_15_be = var_162.field_2;
                                local_sp_19_be = var_161;
                                r8_10 = var_163;
                                r10_12 = var_164;
                                local_sp_20 = var_161;
                            }
                    }
                    var_169 = local_sp_20 + 24UL;
                    var_170 = *(uint64_t *)var_169;
                    *(uint64_t *)(local_sp_20 + (-8L)) = 4210063UL;
                    indirect_placeholder_1(rbx_6, var_170);
                    *(uint64_t *)(local_sp_20 + (-16L)) = 4210073UL;
                    indirect_placeholder();
                    var_171 = local_sp_20 + 32UL;
                    *(uint64_t *)(local_sp_20 + (-24L)) = 4210088UL;
                    indirect_placeholder_28(var_169, var_171);
                    var_172 = local_sp_20 + 56UL;
                    var_173 = local_sp_20 + 48UL;
                    *(uint64_t *)(local_sp_20 + (-32L)) = 4210103UL;
                    indirect_placeholder_28(var_173, var_172);
                    return;
                }
                if (*(unsigned char *)4289593UL != '\x00') {
                    var_140 = *(unsigned char *)4289597UL;
                    var_141 = (uint64_t)var_140;
                    r13_3 = var_141;
                    r13_3 = 0UL;
                    if (var_140 != '\x00' & rax_3 != 0UL) {
                        var_142 = **(uint64_t **)(local_sp_13 + 48UL);
                        var_143 = local_sp_13 + (-8L);
                        *(uint64_t *)var_143 = 4210391UL;
                        var_144 = indirect_placeholder_1(var_142, 4289472UL);
                        r13_1 = r13_2;
                        rcx_10 = var_144;
                        local_sp_14 = var_143;
                        rcx_11 = rcx_10;
                        local_sp_15 = local_sp_14;
                        r13_3 = r13_1;
                        if (*(uint64_t *)(local_sp_14 + 64UL) == 0UL) {
                            *(unsigned char *)4289594UL = (unsigned char)'\x01';
                        }
                        var_145 = local_sp_14 + 24UL;
                        rbx_4 = var_145;
                        var_146 = local_sp_15 + (-8L);
                        *(uint64_t *)var_146 = 4209376UL;
                        var_147 = indirect_placeholder_58(r8_7, 1UL, var_13, rcx_11, r10_9, var_145, r9_9);
                        var_148 = var_147.field_0;
                        var_149 = var_147.field_1;
                        var_150 = var_147.field_2;
                        var_151 = var_147.field_3;
                        var_152 = var_147.field_4;
                        r8_7 = var_149;
                        r9_9 = var_152;
                        rcx_11_be = var_150;
                        local_sp_15_be = var_146;
                        r10_9 = var_151;
                        r8_8 = var_149;
                        rcx_12 = var_150;
                        r10_10 = var_151;
                        local_sp_16 = var_146;
                        r9_10 = var_152;
                        while ((uint64_t)(unsigned char)var_148 != 0UL)
                            {
                                if (*(unsigned char *)4289597UL == '\x00') {
                                    if (*(unsigned char *)4289592UL == '\x00') {
                                        break;
                                    }
                                }
                                var_153 = *(uint64_t *)(local_sp_15 + 16UL);
                                var_154 = local_sp_15 + (-16L);
                                *(uint64_t *)var_154 = 4210415UL;
                                var_155 = indirect_placeholder_1(var_153, 4289472UL);
                                rcx_11_be = var_155;
                                local_sp_15_be = var_154;
                                rcx_12 = var_155;
                                local_sp_16 = var_154;
                                if (*(unsigned char *)4289592UL != '\x00') {
                                    if (*(unsigned char *)4289597UL == '\x00') {
                                        break;
                                    }
                                }
                                rcx_11 = rcx_11_be;
                                local_sp_15 = local_sp_15_be;
                                var_146 = local_sp_15 + (-8L);
                                *(uint64_t *)var_146 = 4209376UL;
                                var_147 = indirect_placeholder_58(r8_7, 1UL, var_13, rcx_11, r10_9, var_145, r9_9);
                                var_148 = var_147.field_0;
                                var_149 = var_147.field_1;
                                var_150 = var_147.field_2;
                                var_151 = var_147.field_3;
                                var_152 = var_147.field_4;
                                r8_7 = var_149;
                                r9_9 = var_152;
                                rcx_11_be = var_150;
                                local_sp_15_be = var_146;
                                r10_9 = var_151;
                                r8_8 = var_149;
                                rcx_12 = var_150;
                                r10_10 = var_151;
                                local_sp_16 = var_146;
                                r9_10 = var_152;
                            }
                    }
                    var_156 = *(unsigned char *)4289596UL;
                    rbx_6 = rbx_4;
                    rax_4 = (uint64_t)var_156;
                    rbx_5 = rbx_4;
                    r8_9 = r8_8;
                    rcx_13 = rcx_12;
                    r10_11 = r10_10;
                    local_sp_17 = local_sp_16;
                    r9_11 = r9_10;
                    local_sp_20 = local_sp_16;
                    if ((var_156 == '\x00') && ((uint64_t)(unsigned char)r13_3 == 0UL)) {
                        var_169 = local_sp_20 + 24UL;
                        var_170 = *(uint64_t *)var_169;
                        *(uint64_t *)(local_sp_20 + (-8L)) = 4210063UL;
                        indirect_placeholder_1(rbx_6, var_170);
                        *(uint64_t *)(local_sp_20 + (-16L)) = 4210073UL;
                        indirect_placeholder();
                        var_171 = local_sp_20 + 32UL;
                        *(uint64_t *)(local_sp_20 + (-24L)) = 4210088UL;
                        indirect_placeholder_28(var_169, var_171);
                        var_172 = local_sp_20 + 56UL;
                        var_173 = local_sp_20 + 48UL;
                        *(uint64_t *)(local_sp_20 + (-32L)) = 4210103UL;
                        indirect_placeholder_28(var_173, var_172);
                        return;
                    }
                }
                if (rax_3 != 0UL) {
                    var_138 = *(unsigned char *)4289597UL;
                    var_139 = (uint64_t)var_138;
                    r13_2 = var_139;
                    if (var_138 == '\x00') {
                        var_142 = **(uint64_t **)(local_sp_13 + 48UL);
                        var_143 = local_sp_13 + (-8L);
                        *(uint64_t *)var_143 = 4210391UL;
                        var_144 = indirect_placeholder_1(var_142, 4289472UL);
                        r13_1 = r13_2;
                        rcx_10 = var_144;
                        local_sp_14 = var_143;
                    }
                    rcx_11 = rcx_10;
                    local_sp_15 = local_sp_14;
                    r13_3 = r13_1;
                    if (*(uint64_t *)(local_sp_14 + 64UL) == 0UL) {
                        *(unsigned char *)4289594UL = (unsigned char)'\x01';
                    }
                    var_145 = local_sp_14 + 24UL;
                    rbx_4 = var_145;
                    var_146 = local_sp_15 + (-8L);
                    *(uint64_t *)var_146 = 4209376UL;
                    var_147 = indirect_placeholder_58(r8_7, 1UL, var_13, rcx_11, r10_9, var_145, r9_9);
                    var_148 = var_147.field_0;
                    var_149 = var_147.field_1;
                    var_150 = var_147.field_2;
                    var_151 = var_147.field_3;
                    var_152 = var_147.field_4;
                    r8_7 = var_149;
                    r9_9 = var_152;
                    rcx_11_be = var_150;
                    local_sp_15_be = var_146;
                    r10_9 = var_151;
                    r8_8 = var_149;
                    rcx_12 = var_150;
                    r10_10 = var_151;
                    local_sp_16 = var_146;
                    r9_10 = var_152;
                    while ((uint64_t)(unsigned char)var_148 != 0UL)
                        {
                            if (*(unsigned char *)4289597UL == '\x00') {
                                if (*(unsigned char *)4289592UL == '\x00') {
                                    break;
                                }
                            }
                            var_153 = *(uint64_t *)(local_sp_15 + 16UL);
                            var_154 = local_sp_15 + (-16L);
                            *(uint64_t *)var_154 = 4210415UL;
                            var_155 = indirect_placeholder_1(var_153, 4289472UL);
                            rcx_11_be = var_155;
                            local_sp_15_be = var_154;
                            rcx_12 = var_155;
                            local_sp_16 = var_154;
                            if (*(unsigned char *)4289592UL != '\x00') {
                                if (*(unsigned char *)4289597UL == '\x00') {
                                    break;
                                }
                            }
                            rcx_11 = rcx_11_be;
                            local_sp_15 = local_sp_15_be;
                            var_146 = local_sp_15 + (-8L);
                            *(uint64_t *)var_146 = 4209376UL;
                            var_147 = indirect_placeholder_58(r8_7, 1UL, var_13, rcx_11, r10_9, var_145, r9_9);
                            var_148 = var_147.field_0;
                            var_149 = var_147.field_1;
                            var_150 = var_147.field_2;
                            var_151 = var_147.field_3;
                            var_152 = var_147.field_4;
                            r8_7 = var_149;
                            r9_9 = var_152;
                            rcx_11_be = var_150;
                            local_sp_15_be = var_146;
                            r10_9 = var_151;
                            r8_8 = var_149;
                            rcx_12 = var_150;
                            r10_10 = var_151;
                            local_sp_16 = var_146;
                            r9_10 = var_152;
                        }
                    var_156 = *(unsigned char *)4289596UL;
                    rbx_6 = rbx_4;
                    rax_4 = (uint64_t)var_156;
                    rbx_5 = rbx_4;
                    r8_9 = r8_8;
                    rcx_13 = rcx_12;
                    r10_11 = r10_10;
                    local_sp_17 = local_sp_16;
                    r9_11 = r9_10;
                    local_sp_20 = local_sp_16;
                    if ((var_156 == '\x00') && ((uint64_t)(unsigned char)r13_3 == 0UL)) {
                        var_169 = local_sp_20 + 24UL;
                        var_170 = *(uint64_t *)var_169;
                        *(uint64_t *)(local_sp_20 + (-8L)) = 4210063UL;
                        indirect_placeholder_1(rbx_6, var_170);
                        *(uint64_t *)(local_sp_20 + (-16L)) = 4210073UL;
                        indirect_placeholder();
                        var_171 = local_sp_20 + 32UL;
                        *(uint64_t *)(local_sp_20 + (-24L)) = 4210088UL;
                        indirect_placeholder_28(var_169, var_171);
                        var_172 = local_sp_20 + 56UL;
                        var_173 = local_sp_20 + 48UL;
                        *(uint64_t *)(local_sp_20 + (-32L)) = 4210103UL;
                        indirect_placeholder_28(var_173, var_172);
                        return;
                    }
                }
                rax_4 = (uint64_t)*(unsigned char *)4289596UL;
            }
            rdi1_1 = rdi1_0;
            rsi2_0 = **(uint64_t **)var_22;
        } else {
            var_32 = **(uint64_t **)(var_0 + (-152L));
            rdi1_0 = var_32;
            rdi1_1 = var_32;
            if (var_31 == 0UL) {
                rdi1_1 = rdi1_0;
                rsi2_0 = **(uint64_t **)var_22;
            }
        }
        var_33 = var_0 + (-208L);
        *(uint64_t *)var_33 = 4209075UL;
        var_34 = indirect_placeholder_1(rdi1_1, rsi2_0);
        var_35 = *var_16;
        *(uint64_t *)4289648UL = 0UL;
        *(uint64_t *)4289656UL = 0UL;
        rcx_5 = var_34;
        local_sp_9 = var_33;
        if (var_35 == 0UL) {
            var_36 = var_0 + (-216L);
            *(uint64_t *)var_36 = 4210469UL;
            var_37 = indirect_placeholder_56(1UL, var_13, 1UL, var_28, var_15, var_29);
            r8_2 = var_37.field_2;
            rcx_5 = var_37.field_3;
            r10_4 = var_37.field_4;
            local_sp_9 = var_36;
            r9_4 = var_37.field_5;
        }
        var_38 = local_sp_9 + 64UL;
        r8_3_ph = r8_2;
        rcx_6_ph = rcx_5;
        r10_5_ph = r10_4;
        local_sp_10_ph = local_sp_9;
        r9_5_ph = r9_4;
        if (*(uint64_t *)var_38 == 0UL) {
            var_39 = local_sp_9 + (-8L);
            *(uint64_t *)var_39 = 4210333UL;
            var_40 = indirect_placeholder_55(1UL, rsi, 2UL, r10_4, var_38, r9_4);
            r8_3_ph = var_40.field_2;
            rcx_6_ph = var_40.field_3;
            r10_5_ph = var_40.field_4;
            local_sp_10_ph = var_39;
            r9_5_ph = var_40.field_5;
        }
    }
    r8_3_ph248 = r8_3_ph;
    rcx_6_ph249 = rcx_6_ph;
    r10_5_ph250 = r10_5_ph;
    local_sp_10_ph251 = local_sp_10_ph;
    r9_5_ph252 = r9_5_ph;
    while (1U)
        {
            rbx_0 = rbx_1_ph;
            r8_3 = r8_3_ph248;
            rcx_6 = rcx_6_ph249;
            r10_5 = r10_5_ph250;
            local_sp_10 = local_sp_10_ph251;
            r9_5 = r9_5_ph252;
            rbx_3 = rbx_1_ph;
            while (1U)
                {
                    var_41 = *(uint64_t *)(local_sp_10 + 32UL);
                    rax_2 = var_41;
                    r9_6 = r9_5;
                    r8_6 = r8_3;
                    rcx_9 = rcx_6;
                    r8_4 = r8_3;
                    rcx_7 = rcx_6;
                    r10_6 = r10_5;
                    local_sp_11 = local_sp_10;
                    r10_8 = r10_5;
                    local_sp_13 = local_sp_10;
                    r9_8 = r9_5;
                    if (var_41 != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    while (1U)
                        {
                            r8_6 = r8_4;
                            rcx_9 = rcx_7;
                            rax_3 = rax_2;
                            r10_8 = r10_6;
                            local_sp_13 = local_sp_11;
                            r9_8 = r9_6;
                            if (*(uint64_t *)(local_sp_11 + 64UL) != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            var_42 = *(uint64_t *)4289168UL;
                            var_43 = *(uint64_t *)4289176UL;
                            var_44 = **(uint64_t **)(local_sp_11 + 80UL);
                            var_45 = **(uint64_t **)(local_sp_11 + 48UL);
                            var_46 = var_44 + 40UL;
                            var_47 = *(uint64_t *)(var_44 + 24UL);
                            var_48 = *(uint64_t *)(var_45 + 24UL);
                            var_49 = var_45 + 40UL;
                            var_50 = local_sp_11 + (-8L);
                            *(uint64_t *)var_50 = 4209197UL;
                            var_51 = indirect_placeholder_57(var_43, var_47, var_48, var_46, var_49, var_42);
                            var_52 = var_51.field_0;
                            var_53 = var_51.field_4;
                            var_54 = var_51.field_5;
                            r9_3 = var_54;
                            r10_3 = var_53;
                            local_sp_6 = var_50;
                            local_sp_7 = var_50;
                            local_sp_8 = var_50;
                            rax_3 = 0UL;
                            if ((int)(uint32_t)var_52 >= (int)0U) {
                                loop_state_var = 1U;
                                break;
                            }
                            var_55 = helper_cc_compute_all_wrapper(var_52, 0UL, var_9, 24U);
                            if ((var_55 & 64UL) != 0UL) {
                                loop_state_var = 2U;
                                break;
                            }
                            if (*(unsigned char *)4289596UL == '\x00') {
                                var_119 = **(uint64_t **)(local_sp_11 + 72UL);
                                var_120 = local_sp_11 + (-16L);
                                *(uint64_t *)var_120 = 4209970UL;
                                indirect_placeholder_1(4289472UL, var_119);
                                local_sp_6 = var_120;
                            }
                            var_121 = local_sp_6 + 64UL;
                            var_122 = local_sp_6 + (-8L);
                            *(uint64_t *)var_122 = 4209247UL;
                            var_123 = indirect_placeholder_52(1UL, rsi, 2UL, var_53, var_121, var_54);
                            var_124 = *(uint64_t *)(local_sp_6 + 24UL);
                            *(unsigned char *)4289594UL = (unsigned char)'\x01';
                            rax_2 = var_124;
                            local_sp_11 = var_122;
                            local_sp_13 = var_122;
                            if (var_124 == 0UL) {
                                r8_4 = var_123.field_2;
                                rcx_7 = var_123.field_3;
                                r10_6 = var_123.field_4;
                                r9_6 = var_123.field_5;
                                continue;
                            }
                            var_125 = var_123.field_2;
                            var_126 = var_123.field_3;
                            var_127 = var_123.field_4;
                            var_128 = var_123.field_5;
                            r8_6 = var_125;
                            rcx_9 = var_126;
                            r10_8 = var_127;
                            r9_8 = var_128;
                            loop_state_var = 0U;
                            break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 1U:
                        {
                            if (*(unsigned char *)4289597UL == '\x00') {
                                var_129 = **(uint64_t **)(local_sp_11 + 40UL);
                                var_130 = local_sp_11 + (-16L);
                                *(uint64_t *)var_130 = 4210002UL;
                                indirect_placeholder_1(var_129, 4289472UL);
                                local_sp_8 = var_130;
                            }
                            var_131 = local_sp_8 + 32UL;
                            var_132 = local_sp_8 + (-8L);
                            *(uint64_t *)var_132 = 4209764UL;
                            var_133 = indirect_placeholder_54(1UL, var_13, 1UL, var_53, var_131, var_54);
                            var_134 = var_133.field_2;
                            var_135 = var_133.field_3;
                            var_136 = var_133.field_4;
                            var_137 = var_133.field_5;
                            *(unsigned char *)4289594UL = (unsigned char)'\x01';
                            r8_3 = var_134;
                            rcx_6 = var_135;
                            r10_5 = var_136;
                            local_sp_10 = var_132;
                            r9_5 = var_137;
                            continue;
                        }
                        break;
                      case 0U:
                        {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 2U:
                        {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 1U:
                {
                    while (1U)
                        {
                            var_56 = local_sp_7 + 32UL;
                            var_57 = local_sp_7 + (-8L);
                            *(uint64_t *)var_57 = 4209502UL;
                            var_58 = indirect_placeholder_53(0UL, var_13, 1UL, r10_3, var_56, r9_3);
                            local_sp_5_ph = var_57;
                            if ((uint64_t)(unsigned char)var_58.field_0 != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            var_59 = *(uint64_t *)(local_sp_7 + 24UL);
                            var_60 = *(uint64_t *)4289168UL;
                            var_61 = *(uint64_t *)4289176UL;
                            var_62 = **(uint64_t **)(local_sp_7 + 72UL);
                            var_63 = *(uint64_t *)(((var_59 << 3UL) + *(uint64_t *)(local_sp_7 + 40UL)) + (-8L));
                            var_64 = var_62 + 40UL;
                            var_65 = *(uint64_t *)(var_62 + 24UL);
                            var_66 = *(uint64_t *)(var_63 + 24UL);
                            var_67 = var_63 + 40UL;
                            var_68 = local_sp_7 + (-16L);
                            *(uint64_t *)var_68 = 4209474UL;
                            var_69 = indirect_placeholder_51(var_61, var_65, var_66, var_64, var_67, var_60);
                            local_sp_5_ph = var_68;
                            local_sp_7 = var_68;
                            if ((uint64_t)(uint32_t)var_69.field_0 != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            r10_3 = var_69.field_4;
                            r9_3 = var_69.field_5;
                            continue;
                        }
                    switch (loop_state_var) {
                      case 0U:
                        {
                            var_70 = var_58.field_4;
                            var_71 = var_58.field_5;
                            var_72 = (uint64_t *)(local_sp_7 + 24UL);
                            *var_72 = (*var_72 + 1UL);
                            r15_2_ph = 1UL;
                            r10_2_ph = var_70;
                            r9_2_ph = var_71;
                        }
                        break;
                      case 1U:
                        {
                            r10_2_ph = var_69.field_4;
                            r9_2_ph = var_69.field_5;
                        }
                        break;
                    }
                    r15_2 = r15_2_ph;
                    r10_2 = r10_2_ph;
                    local_sp_5 = local_sp_5_ph;
                    r9_2 = r9_2_ph;
                    while (1U)
                        {
                            var_73 = local_sp_5 + 64UL;
                            var_74 = local_sp_5 + (-8L);
                            *(uint64_t *)var_74 = 4209606UL;
                            var_75 = indirect_placeholder_50(r15_2, 0UL, rsi, 2UL, r10_2, var_73, r9_2);
                            local_sp_0 = var_74;
                            local_sp_4 = var_74;
                            if ((uint64_t)(unsigned char)var_75.field_0 != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            var_76 = *(uint64_t *)4289168UL;
                            var_77 = *(uint64_t *)4289176UL;
                            var_78 = *(uint64_t *)(((*(uint64_t *)(local_sp_5 + 56UL) << 3UL) + *(uint64_t *)(local_sp_5 + 72UL)) + (-8L));
                            var_79 = **(uint64_t **)(local_sp_5 + 40UL);
                            var_80 = var_78 + 40UL;
                            var_81 = *(uint64_t *)(var_78 + 24UL);
                            var_82 = *(uint64_t *)(var_79 + 24UL);
                            var_83 = var_79 + 40UL;
                            var_84 = local_sp_5 + (-16L);
                            *(uint64_t *)var_84 = 4209578UL;
                            var_85 = indirect_placeholder_49(var_77, var_81, var_82, var_80, var_83, var_76);
                            local_sp_0 = var_84;
                            local_sp_4 = var_84;
                            local_sp_5 = var_84;
                            if ((uint64_t)(uint32_t)var_85.field_0 != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            r15_2 = var_85.field_1;
                            r10_2 = var_85.field_4;
                            r9_2 = var_85.field_5;
                            continue;
                        }
                    switch (loop_state_var) {
                      case 0U:
                        {
                            var_92 = var_75.field_1;
                            var_93 = var_75.field_2;
                            var_94 = var_75.field_3;
                            var_95 = var_75.field_4;
                            var_96 = var_75.field_5;
                            var_97 = (uint64_t *)(local_sp_5 + 56UL);
                            *var_97 = (*var_97 + 1UL);
                            var_98 = *(unsigned char *)4289595UL;
                            *(unsigned char *)(local_sp_5 + 7UL) = (unsigned char)'\x01';
                            r15_0 = var_92;
                            r10_1 = var_95;
                            r9_1 = var_96;
                            r8_0 = var_93;
                            rcx_0 = var_94;
                            r10_0 = var_95;
                            r9_0 = var_96;
                            r15_1 = var_92;
                            r8_1 = var_93;
                            rcx_3 = var_94;
                            r10_1 = r10_0;
                            local_sp_4 = local_sp_0;
                            r9_1 = r9_0;
                            rcx_1 = rcx_0;
                            local_sp_1 = local_sp_0;
                            r15_1 = r15_0;
                            r8_1 = r8_0;
                            rcx_3 = rcx_0;
                            if (var_98 != '\x00' & *(uint64_t *)(local_sp_0 + 32UL) == 1UL) {
                                var_107 = *(uint64_t *)(local_sp_3 + 32UL);
                                var_108 = r13_0 + 1UL;
                                local_sp_4 = local_sp_3;
                                r13_0 = var_108;
                                rcx_1 = rcx_2;
                                local_sp_1 = local_sp_3;
                                rcx_3 = rcx_2;
                                do {
                                    var_99 = *(uint64_t *)(local_sp_1 + 64UL);
                                    var_100 = r13_0 << 3UL;
                                    local_sp_2 = local_sp_1;
                                    rcx_2 = rcx_1;
                                    local_sp_3 = local_sp_1;
                                    rbx_0 = var_100;
                                    if (var_99 == 1UL) {
                                        var_101 = *(uint64_t *)((r14_0 << 3UL) + *(uint64_t *)(local_sp_2 + 80UL));
                                        var_102 = *(uint64_t *)(local_sp_2 + 48UL);
                                        var_103 = r14_0 + 1UL;
                                        var_104 = *(uint64_t *)(var_100 + var_102);
                                        var_105 = local_sp_2 + (-8L);
                                        *(uint64_t *)var_105 = 4209915UL;
                                        var_106 = indirect_placeholder_1(var_104, var_101);
                                        r14_0 = var_103;
                                        local_sp_2 = var_105;
                                        rcx_2 = var_106;
                                        local_sp_3 = var_105;
                                        do {
                                            var_101 = *(uint64_t *)((r14_0 << 3UL) + *(uint64_t *)(local_sp_2 + 80UL));
                                            var_102 = *(uint64_t *)(local_sp_2 + 48UL);
                                            var_103 = r14_0 + 1UL;
                                            var_104 = *(uint64_t *)(var_100 + var_102);
                                            var_105 = local_sp_2 + (-8L);
                                            *(uint64_t *)var_105 = 4209915UL;
                                            var_106 = indirect_placeholder_1(var_104, var_101);
                                            r14_0 = var_103;
                                            local_sp_2 = var_105;
                                            rcx_2 = var_106;
                                            local_sp_3 = var_105;
                                        } while ((*(uint64_t *)(local_sp_2 + 56UL) + (-1L)) <= var_103);
                                    }
                                    var_107 = *(uint64_t *)(local_sp_3 + 32UL);
                                    var_108 = r13_0 + 1UL;
                                    local_sp_4 = local_sp_3;
                                    r13_0 = var_108;
                                    rcx_1 = rcx_2;
                                    local_sp_1 = local_sp_3;
                                    rcx_3 = rcx_2;
                                } while ((var_107 + (-1L)) <= var_108);
                            }
                        }
                        break;
                      case 1U:
                        {
                            var_86 = var_85.field_1;
                            var_87 = var_85.field_2;
                            var_88 = var_85.field_3;
                            var_89 = var_85.field_4;
                            var_90 = var_85.field_5;
                            var_91 = *(unsigned char *)4289595UL;
                            *(unsigned char *)(local_sp_5 + (-1L)) = (unsigned char)'\x00';
                            r15_0 = var_86;
                            r10_1 = var_89;
                            r9_1 = var_90;
                            r8_0 = var_87;
                            rcx_0 = var_88;
                            r10_0 = var_89;
                            r9_0 = var_90;
                            r15_1 = var_86;
                            r8_1 = var_87;
                            rcx_3 = var_88;
                            r10_1 = r10_0;
                            local_sp_4 = local_sp_0;
                            r9_1 = r9_0;
                            rcx_1 = rcx_0;
                            local_sp_1 = local_sp_0;
                            r15_1 = r15_0;
                            r8_1 = r8_0;
                            rcx_3 = rcx_0;
                            if (var_91 != '\x00' & *(uint64_t *)(local_sp_0 + 32UL) == 1UL) {
                                var_107 = *(uint64_t *)(local_sp_3 + 32UL);
                                var_108 = r13_0 + 1UL;
                                local_sp_4 = local_sp_3;
                                r13_0 = var_108;
                                rcx_1 = rcx_2;
                                local_sp_1 = local_sp_3;
                                rcx_3 = rcx_2;
                                do {
                                    var_99 = *(uint64_t *)(local_sp_1 + 64UL);
                                    var_100 = r13_0 << 3UL;
                                    local_sp_2 = local_sp_1;
                                    rcx_2 = rcx_1;
                                    local_sp_3 = local_sp_1;
                                    rbx_0 = var_100;
                                    if (var_99 == 1UL) {
                                        var_101 = *(uint64_t *)((r14_0 << 3UL) + *(uint64_t *)(local_sp_2 + 80UL));
                                        var_102 = *(uint64_t *)(local_sp_2 + 48UL);
                                        var_103 = r14_0 + 1UL;
                                        var_104 = *(uint64_t *)(var_100 + var_102);
                                        var_105 = local_sp_2 + (-8L);
                                        *(uint64_t *)var_105 = 4209915UL;
                                        var_106 = indirect_placeholder_1(var_104, var_101);
                                        r14_0 = var_103;
                                        local_sp_2 = var_105;
                                        rcx_2 = var_106;
                                        local_sp_3 = var_105;
                                        do {
                                            var_101 = *(uint64_t *)((r14_0 << 3UL) + *(uint64_t *)(local_sp_2 + 80UL));
                                            var_102 = *(uint64_t *)(local_sp_2 + 48UL);
                                            var_103 = r14_0 + 1UL;
                                            var_104 = *(uint64_t *)(var_100 + var_102);
                                            var_105 = local_sp_2 + (-8L);
                                            *(uint64_t *)var_105 = 4209915UL;
                                            var_106 = indirect_placeholder_1(var_104, var_101);
                                            r14_0 = var_103;
                                            local_sp_2 = var_105;
                                            rcx_2 = var_106;
                                            local_sp_3 = var_105;
                                        } while ((*(uint64_t *)(local_sp_2 + 56UL) + (-1L)) <= var_103);
                                    }
                                    var_107 = *(uint64_t *)(local_sp_3 + 32UL);
                                    var_108 = r13_0 + 1UL;
                                    local_sp_4 = local_sp_3;
                                    r13_0 = var_108;
                                    rcx_1 = rcx_2;
                                    local_sp_1 = local_sp_3;
                                    rcx_3 = rcx_2;
                                } while ((var_107 + (-1L)) <= var_108);
                            }
                        }
                        break;
                    }
                    r8_3_ph248 = r8_1;
                    rcx_4 = rcx_3;
                    rbx_1_ph = rbx_0;
                    r10_5_ph250 = r10_1;
                    local_sp_10_ph251 = local_sp_4;
                    r9_5_ph252 = r9_1;
                    if ((uint64_t)(unsigned char)r15_1 == 0UL) {
                        var_109 = *(uint64_t *)(local_sp_4 + 48UL);
                        var_110 = (uint64_t *)(local_sp_4 + 32UL);
                        var_111 = ((*var_110 << 3UL) + var_109) + (-8L);
                        var_112 = (uint64_t *)var_109;
                        var_113 = *var_112;
                        _cast9 = (uint64_t *)var_111;
                        *var_112 = *_cast9;
                        *_cast9 = var_113;
                        *var_110 = 1UL;
                        rcx_4 = var_113;
                    } else {
                        *(uint64_t *)(local_sp_4 + 32UL) = 0UL;
                    }
                    rcx_6_ph249_be = rcx_4;
                    if (*(unsigned char *)(local_sp_4 + 15UL) == '\x00') {
                        var_114 = *(uint64_t *)(local_sp_4 + 80UL);
                        var_115 = (uint64_t *)(local_sp_4 + 64UL);
                        var_116 = ((*var_115 << 3UL) + var_114) + (-8L);
                        var_117 = (uint64_t *)var_114;
                        var_118 = *var_117;
                        _cast8 = (uint64_t *)var_116;
                        *var_117 = *_cast8;
                        *_cast8 = var_118;
                        *var_115 = 1UL;
                        rcx_6_ph249_be = var_118;
                    } else {
                        *(uint64_t *)(local_sp_4 + 64UL) = 0UL;
                    }
                    rcx_6_ph249 = rcx_6_ph249_be;
                    continue;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    *(uint64_t *)(local_sp_13 + 24UL) = 0UL;
    r8_7 = r8_6;
    rbx_4 = rbx_3;
    r9_9 = r9_8;
    rcx_10 = rcx_9;
    local_sp_14 = local_sp_13;
    r10_9 = r10_8;
    r8_8 = r8_6;
    rcx_12 = rcx_9;
    r10_10 = r10_8;
    local_sp_16 = local_sp_13;
    r9_10 = r9_8;
    rbx_5 = rbx_3;
    r8_9 = r8_6;
    rcx_13 = rcx_9;
    r10_11 = r10_8;
    local_sp_17 = local_sp_13;
    r9_11 = r9_8;
    if (*(uint32_t *)4289520UL != 2U) {
        var_140 = *(unsigned char *)4289597UL;
        var_141 = (uint64_t)var_140;
        r13_3 = var_141;
        r13_3 = 0UL;
        if (var_140 != '\x00' & rax_3 != 0UL) {
            var_142 = **(uint64_t **)(local_sp_13 + 48UL);
            var_143 = local_sp_13 + (-8L);
            *(uint64_t *)var_143 = 4210391UL;
            var_144 = indirect_placeholder_1(var_142, 4289472UL);
            r13_1 = r13_2;
            rcx_10 = var_144;
            local_sp_14 = var_143;
            rcx_11 = rcx_10;
            local_sp_15 = local_sp_14;
            r13_3 = r13_1;
            if (*(uint64_t *)(local_sp_14 + 64UL) == 0UL) {
                *(unsigned char *)4289594UL = (unsigned char)'\x01';
            }
            var_145 = local_sp_14 + 24UL;
            rbx_4 = var_145;
            var_146 = local_sp_15 + (-8L);
            *(uint64_t *)var_146 = 4209376UL;
            var_147 = indirect_placeholder_58(r8_7, 1UL, var_13, rcx_11, r10_9, var_145, r9_9);
            var_148 = var_147.field_0;
            var_149 = var_147.field_1;
            var_150 = var_147.field_2;
            var_151 = var_147.field_3;
            var_152 = var_147.field_4;
            r8_7 = var_149;
            r9_9 = var_152;
            rcx_11_be = var_150;
            local_sp_15_be = var_146;
            r10_9 = var_151;
            r8_8 = var_149;
            rcx_12 = var_150;
            r10_10 = var_151;
            local_sp_16 = var_146;
            r9_10 = var_152;
            while ((uint64_t)(unsigned char)var_148 != 0UL)
                {
                    if (*(unsigned char *)4289597UL == '\x00') {
                        if (*(unsigned char *)4289592UL == '\x00') {
                            break;
                        }
                    }
                    var_153 = *(uint64_t *)(local_sp_15 + 16UL);
                    var_154 = local_sp_15 + (-16L);
                    *(uint64_t *)var_154 = 4210415UL;
                    var_155 = indirect_placeholder_1(var_153, 4289472UL);
                    rcx_11_be = var_155;
                    local_sp_15_be = var_154;
                    rcx_12 = var_155;
                    local_sp_16 = var_154;
                    if (*(unsigned char *)4289592UL != '\x00') {
                        if (*(unsigned char *)4289597UL == '\x00') {
                            break;
                        }
                    }
                    rcx_11 = rcx_11_be;
                    local_sp_15 = local_sp_15_be;
                    var_146 = local_sp_15 + (-8L);
                    *(uint64_t *)var_146 = 4209376UL;
                    var_147 = indirect_placeholder_58(r8_7, 1UL, var_13, rcx_11, r10_9, var_145, r9_9);
                    var_148 = var_147.field_0;
                    var_149 = var_147.field_1;
                    var_150 = var_147.field_2;
                    var_151 = var_147.field_3;
                    var_152 = var_147.field_4;
                    r8_7 = var_149;
                    r9_9 = var_152;
                    rcx_11_be = var_150;
                    local_sp_15_be = var_146;
                    r10_9 = var_151;
                    r8_8 = var_149;
                    rcx_12 = var_150;
                    r10_10 = var_151;
                    local_sp_16 = var_146;
                    r9_10 = var_152;
                }
        }
        var_156 = *(unsigned char *)4289596UL;
        rbx_6 = rbx_4;
        rax_4 = (uint64_t)var_156;
        rbx_5 = rbx_4;
        r8_9 = r8_8;
        rcx_13 = rcx_12;
        r10_11 = r10_10;
        local_sp_17 = local_sp_16;
        r9_11 = r9_10;
        local_sp_20 = local_sp_16;
        if ((var_156 == '\x00') && ((uint64_t)(unsigned char)r13_3 == 0UL)) {
            var_169 = local_sp_20 + 24UL;
            var_170 = *(uint64_t *)var_169;
            *(uint64_t *)(local_sp_20 + (-8L)) = 4210063UL;
            indirect_placeholder_1(rbx_6, var_170);
            *(uint64_t *)(local_sp_20 + (-16L)) = 4210073UL;
            indirect_placeholder();
            var_171 = local_sp_20 + 32UL;
            *(uint64_t *)(local_sp_20 + (-24L)) = 4210088UL;
            indirect_placeholder_28(var_169, var_171);
            var_172 = local_sp_20 + 56UL;
            var_173 = local_sp_20 + 48UL;
            *(uint64_t *)(local_sp_20 + (-32L)) = 4210103UL;
            indirect_placeholder_28(var_173, var_172);
            return;
        }
        rcx_14 = rcx_13;
        rbx_6 = rbx_5;
        r9_12 = r9_11;
        r8_10 = r8_9;
        r10_12 = r10_11;
        local_sp_18 = local_sp_17;
        local_sp_20 = local_sp_17;
        if (*(uint64_t *)(local_sp_17 + 64UL) != 0UL) {
            if (rax_4 == 0UL) {
                var_157 = **(uint64_t **)(local_sp_17 + 80UL);
                var_158 = local_sp_17 + (-8L);
                *(uint64_t *)var_158 = 4210356UL;
                var_159 = indirect_placeholder_1(4289472UL, var_157);
                rcx_14 = var_159;
                local_sp_18 = var_158;
            }
            rcx_15 = rcx_14;
            local_sp_19 = local_sp_18;
            if (*(uint64_t *)(local_sp_18 + 32UL) == 0UL) {
                *(unsigned char *)4289594UL = (unsigned char)'\x01';
            }
            var_160 = local_sp_18 + 24UL;
            rbx_6 = var_160;
            var_161 = local_sp_19 + (-8L);
            *(uint64_t *)var_161 = 4210176UL;
            var_162 = indirect_placeholder_59(r8_10, 2UL, rsi, rcx_15, r10_12, var_160, r9_12);
            var_163 = var_162.field_1;
            var_164 = var_162.field_3;
            var_165 = var_162.field_4;
            r9_12 = var_165;
            rcx_15_be = var_162.field_2;
            local_sp_19_be = var_161;
            r8_10 = var_163;
            r10_12 = var_164;
            local_sp_20 = var_161;
            while ((uint64_t)(unsigned char)var_162.field_0 != 0UL)
                {
                    if (*(unsigned char *)4289596UL == '\x00') {
                        if (*(unsigned char *)4289593UL == '\x00') {
                            break;
                        }
                    }
                    var_166 = *(uint64_t *)(local_sp_19 + 16UL);
                    var_167 = local_sp_19 + (-16L);
                    *(uint64_t *)var_167 = 4210271UL;
                    var_168 = indirect_placeholder_1(4289472UL, var_166);
                    rcx_15_be = var_168;
                    local_sp_19_be = var_167;
                    local_sp_20 = var_167;
                    if (*(unsigned char *)4289593UL != '\x00') {
                        if (*(unsigned char *)4289596UL == '\x00') {
                            break;
                        }
                    }
                    rcx_15 = rcx_15_be;
                    local_sp_19 = local_sp_19_be;
                    var_161 = local_sp_19 + (-8L);
                    *(uint64_t *)var_161 = 4210176UL;
                    var_162 = indirect_placeholder_59(r8_10, 2UL, rsi, rcx_15, r10_12, var_160, r9_12);
                    var_163 = var_162.field_1;
                    var_164 = var_162.field_3;
                    var_165 = var_162.field_4;
                    r9_12 = var_165;
                    rcx_15_be = var_162.field_2;
                    local_sp_19_be = var_161;
                    r8_10 = var_163;
                    r10_12 = var_164;
                    local_sp_20 = var_161;
                }
        }
        var_169 = local_sp_20 + 24UL;
        var_170 = *(uint64_t *)var_169;
        *(uint64_t *)(local_sp_20 + (-8L)) = 4210063UL;
        indirect_placeholder_1(rbx_6, var_170);
        *(uint64_t *)(local_sp_20 + (-16L)) = 4210073UL;
        indirect_placeholder();
        var_171 = local_sp_20 + 32UL;
        *(uint64_t *)(local_sp_20 + (-24L)) = 4210088UL;
        indirect_placeholder_28(var_169, var_171);
        var_172 = local_sp_20 + 56UL;
        var_173 = local_sp_20 + 48UL;
        *(uint64_t *)(local_sp_20 + (-32L)) = 4210103UL;
        indirect_placeholder_28(var_173, var_172);
        return;
    }
    if (*(unsigned char *)4289592UL != '\x00') {
        if (rax_3 == 0UL) {
            var_138 = *(unsigned char *)4289597UL;
            var_139 = (uint64_t)var_138;
            r13_2 = var_139;
            if (var_138 == '\x00') {
                var_142 = **(uint64_t **)(local_sp_13 + 48UL);
                var_143 = local_sp_13 + (-8L);
                *(uint64_t *)var_143 = 4210391UL;
                var_144 = indirect_placeholder_1(var_142, 4289472UL);
                r13_1 = r13_2;
                rcx_10 = var_144;
                local_sp_14 = var_143;
            }
            rcx_11 = rcx_10;
            local_sp_15 = local_sp_14;
            r13_3 = r13_1;
            if (*(uint64_t *)(local_sp_14 + 64UL) == 0UL) {
                *(unsigned char *)4289594UL = (unsigned char)'\x01';
            }
            var_145 = local_sp_14 + 24UL;
            rbx_4 = var_145;
            var_146 = local_sp_15 + (-8L);
            *(uint64_t *)var_146 = 4209376UL;
            var_147 = indirect_placeholder_58(r8_7, 1UL, var_13, rcx_11, r10_9, var_145, r9_9);
            var_148 = var_147.field_0;
            var_149 = var_147.field_1;
            var_150 = var_147.field_2;
            var_151 = var_147.field_3;
            var_152 = var_147.field_4;
            r8_7 = var_149;
            r9_9 = var_152;
            rcx_11_be = var_150;
            local_sp_15_be = var_146;
            r10_9 = var_151;
            r8_8 = var_149;
            rcx_12 = var_150;
            r10_10 = var_151;
            local_sp_16 = var_146;
            r9_10 = var_152;
            while ((uint64_t)(unsigned char)var_148 != 0UL)
                {
                    if (*(unsigned char *)4289597UL == '\x00') {
                        if (*(unsigned char *)4289592UL == '\x00') {
                            break;
                        }
                    }
                    var_153 = *(uint64_t *)(local_sp_15 + 16UL);
                    var_154 = local_sp_15 + (-16L);
                    *(uint64_t *)var_154 = 4210415UL;
                    var_155 = indirect_placeholder_1(var_153, 4289472UL);
                    rcx_11_be = var_155;
                    local_sp_15_be = var_154;
                    rcx_12 = var_155;
                    local_sp_16 = var_154;
                    if (*(unsigned char *)4289592UL != '\x00') {
                        if (*(unsigned char *)4289597UL == '\x00') {
                            break;
                        }
                    }
                    rcx_11 = rcx_11_be;
                    local_sp_15 = local_sp_15_be;
                    var_146 = local_sp_15 + (-8L);
                    *(uint64_t *)var_146 = 4209376UL;
                    var_147 = indirect_placeholder_58(r8_7, 1UL, var_13, rcx_11, r10_9, var_145, r9_9);
                    var_148 = var_147.field_0;
                    var_149 = var_147.field_1;
                    var_150 = var_147.field_2;
                    var_151 = var_147.field_3;
                    var_152 = var_147.field_4;
                    r8_7 = var_149;
                    r9_9 = var_152;
                    rcx_11_be = var_150;
                    local_sp_15_be = var_146;
                    r10_9 = var_151;
                    r8_8 = var_149;
                    rcx_12 = var_150;
                    r10_10 = var_151;
                    local_sp_16 = var_146;
                    r9_10 = var_152;
                }
            var_156 = *(unsigned char *)4289596UL;
            rbx_6 = rbx_4;
            rax_4 = (uint64_t)var_156;
            rbx_5 = rbx_4;
            r8_9 = r8_8;
            rcx_13 = rcx_12;
            r10_11 = r10_10;
            local_sp_17 = local_sp_16;
            r9_11 = r9_10;
            local_sp_20 = local_sp_16;
            if ((var_156 == '\x00') && ((uint64_t)(unsigned char)r13_3 == 0UL)) {
                var_169 = local_sp_20 + 24UL;
                var_170 = *(uint64_t *)var_169;
                *(uint64_t *)(local_sp_20 + (-8L)) = 4210063UL;
                indirect_placeholder_1(rbx_6, var_170);
                *(uint64_t *)(local_sp_20 + (-16L)) = 4210073UL;
                indirect_placeholder();
                var_171 = local_sp_20 + 32UL;
                *(uint64_t *)(local_sp_20 + (-24L)) = 4210088UL;
                indirect_placeholder_28(var_169, var_171);
                var_172 = local_sp_20 + 56UL;
                var_173 = local_sp_20 + 48UL;
                *(uint64_t *)(local_sp_20 + (-32L)) = 4210103UL;
                indirect_placeholder_28(var_173, var_172);
                return;
            }
        }
        rax_4 = (uint64_t)*(unsigned char *)4289596UL;
        rcx_14 = rcx_13;
        rbx_6 = rbx_5;
        r9_12 = r9_11;
        r8_10 = r8_9;
        r10_12 = r10_11;
        local_sp_18 = local_sp_17;
        local_sp_20 = local_sp_17;
        if (*(uint64_t *)(local_sp_17 + 64UL) != 0UL) {
            if (rax_4 == 0UL) {
                var_157 = **(uint64_t **)(local_sp_17 + 80UL);
                var_158 = local_sp_17 + (-8L);
                *(uint64_t *)var_158 = 4210356UL;
                var_159 = indirect_placeholder_1(4289472UL, var_157);
                rcx_14 = var_159;
                local_sp_18 = var_158;
            }
            rcx_15 = rcx_14;
            local_sp_19 = local_sp_18;
            if (*(uint64_t *)(local_sp_18 + 32UL) == 0UL) {
                *(unsigned char *)4289594UL = (unsigned char)'\x01';
            }
            var_160 = local_sp_18 + 24UL;
            rbx_6 = var_160;
            var_161 = local_sp_19 + (-8L);
            *(uint64_t *)var_161 = 4210176UL;
            var_162 = indirect_placeholder_59(r8_10, 2UL, rsi, rcx_15, r10_12, var_160, r9_12);
            var_163 = var_162.field_1;
            var_164 = var_162.field_3;
            var_165 = var_162.field_4;
            r9_12 = var_165;
            rcx_15_be = var_162.field_2;
            local_sp_19_be = var_161;
            r8_10 = var_163;
            r10_12 = var_164;
            local_sp_20 = var_161;
            while ((uint64_t)(unsigned char)var_162.field_0 != 0UL)
                {
                    if (*(unsigned char *)4289596UL == '\x00') {
                        if (*(unsigned char *)4289593UL == '\x00') {
                            break;
                        }
                    }
                    var_166 = *(uint64_t *)(local_sp_19 + 16UL);
                    var_167 = local_sp_19 + (-16L);
                    *(uint64_t *)var_167 = 4210271UL;
                    var_168 = indirect_placeholder_1(4289472UL, var_166);
                    rcx_15_be = var_168;
                    local_sp_19_be = var_167;
                    local_sp_20 = var_167;
                    if (*(unsigned char *)4289593UL != '\x00') {
                        if (*(unsigned char *)4289596UL == '\x00') {
                            break;
                        }
                    }
                    rcx_15 = rcx_15_be;
                    local_sp_19 = local_sp_19_be;
                    var_161 = local_sp_19 + (-8L);
                    *(uint64_t *)var_161 = 4210176UL;
                    var_162 = indirect_placeholder_59(r8_10, 2UL, rsi, rcx_15, r10_12, var_160, r9_12);
                    var_163 = var_162.field_1;
                    var_164 = var_162.field_3;
                    var_165 = var_162.field_4;
                    r9_12 = var_165;
                    rcx_15_be = var_162.field_2;
                    local_sp_19_be = var_161;
                    r8_10 = var_163;
                    r10_12 = var_164;
                    local_sp_20 = var_161;
                }
        }
        var_169 = local_sp_20 + 24UL;
        var_170 = *(uint64_t *)var_169;
        *(uint64_t *)(local_sp_20 + (-8L)) = 4210063UL;
        indirect_placeholder_1(rbx_6, var_170);
        *(uint64_t *)(local_sp_20 + (-16L)) = 4210073UL;
        indirect_placeholder();
        var_171 = local_sp_20 + 32UL;
        *(uint64_t *)(local_sp_20 + (-24L)) = 4210088UL;
        indirect_placeholder_28(var_169, var_171);
        var_172 = local_sp_20 + 56UL;
        var_173 = local_sp_20 + 48UL;
        *(uint64_t *)(local_sp_20 + (-32L)) = 4210103UL;
        indirect_placeholder_28(var_173, var_172);
        return;
    }
    if (*(unsigned char *)4289593UL != '\x00') {
        var_140 = *(unsigned char *)4289597UL;
        var_141 = (uint64_t)var_140;
        r13_3 = var_141;
        r13_3 = 0UL;
        if (var_140 != '\x00' & rax_3 != 0UL) {
            var_142 = **(uint64_t **)(local_sp_13 + 48UL);
            var_143 = local_sp_13 + (-8L);
            *(uint64_t *)var_143 = 4210391UL;
            var_144 = indirect_placeholder_1(var_142, 4289472UL);
            r13_1 = r13_2;
            rcx_10 = var_144;
            local_sp_14 = var_143;
            rcx_11 = rcx_10;
            local_sp_15 = local_sp_14;
            r13_3 = r13_1;
            if (*(uint64_t *)(local_sp_14 + 64UL) == 0UL) {
                *(unsigned char *)4289594UL = (unsigned char)'\x01';
            }
            var_145 = local_sp_14 + 24UL;
            rbx_4 = var_145;
            var_146 = local_sp_15 + (-8L);
            *(uint64_t *)var_146 = 4209376UL;
            var_147 = indirect_placeholder_58(r8_7, 1UL, var_13, rcx_11, r10_9, var_145, r9_9);
            var_148 = var_147.field_0;
            var_149 = var_147.field_1;
            var_150 = var_147.field_2;
            var_151 = var_147.field_3;
            var_152 = var_147.field_4;
            r8_7 = var_149;
            r9_9 = var_152;
            rcx_11_be = var_150;
            local_sp_15_be = var_146;
            r10_9 = var_151;
            r8_8 = var_149;
            rcx_12 = var_150;
            r10_10 = var_151;
            local_sp_16 = var_146;
            r9_10 = var_152;
            while ((uint64_t)(unsigned char)var_148 != 0UL)
                {
                    if (*(unsigned char *)4289597UL == '\x00') {
                        if (*(unsigned char *)4289592UL == '\x00') {
                            break;
                        }
                    }
                    var_153 = *(uint64_t *)(local_sp_15 + 16UL);
                    var_154 = local_sp_15 + (-16L);
                    *(uint64_t *)var_154 = 4210415UL;
                    var_155 = indirect_placeholder_1(var_153, 4289472UL);
                    rcx_11_be = var_155;
                    local_sp_15_be = var_154;
                    rcx_12 = var_155;
                    local_sp_16 = var_154;
                    if (*(unsigned char *)4289592UL != '\x00') {
                        if (*(unsigned char *)4289597UL == '\x00') {
                            break;
                        }
                    }
                    rcx_11 = rcx_11_be;
                    local_sp_15 = local_sp_15_be;
                    var_146 = local_sp_15 + (-8L);
                    *(uint64_t *)var_146 = 4209376UL;
                    var_147 = indirect_placeholder_58(r8_7, 1UL, var_13, rcx_11, r10_9, var_145, r9_9);
                    var_148 = var_147.field_0;
                    var_149 = var_147.field_1;
                    var_150 = var_147.field_2;
                    var_151 = var_147.field_3;
                    var_152 = var_147.field_4;
                    r8_7 = var_149;
                    r9_9 = var_152;
                    rcx_11_be = var_150;
                    local_sp_15_be = var_146;
                    r10_9 = var_151;
                    r8_8 = var_149;
                    rcx_12 = var_150;
                    r10_10 = var_151;
                    local_sp_16 = var_146;
                    r9_10 = var_152;
                }
        }
        var_156 = *(unsigned char *)4289596UL;
        rbx_6 = rbx_4;
        rax_4 = (uint64_t)var_156;
        rbx_5 = rbx_4;
        r8_9 = r8_8;
        rcx_13 = rcx_12;
        r10_11 = r10_10;
        local_sp_17 = local_sp_16;
        r9_11 = r9_10;
        local_sp_20 = local_sp_16;
        if ((var_156 == '\x00') && ((uint64_t)(unsigned char)r13_3 == 0UL)) {
            var_169 = local_sp_20 + 24UL;
            var_170 = *(uint64_t *)var_169;
            *(uint64_t *)(local_sp_20 + (-8L)) = 4210063UL;
            indirect_placeholder_1(rbx_6, var_170);
            *(uint64_t *)(local_sp_20 + (-16L)) = 4210073UL;
            indirect_placeholder();
            var_171 = local_sp_20 + 32UL;
            *(uint64_t *)(local_sp_20 + (-24L)) = 4210088UL;
            indirect_placeholder_28(var_169, var_171);
            var_172 = local_sp_20 + 56UL;
            var_173 = local_sp_20 + 48UL;
            *(uint64_t *)(local_sp_20 + (-32L)) = 4210103UL;
            indirect_placeholder_28(var_173, var_172);
            return;
        }
    }
    if (rax_3 != 0UL) {
        var_138 = *(unsigned char *)4289597UL;
        var_139 = (uint64_t)var_138;
        r13_2 = var_139;
        if (var_138 == '\x00') {
            var_142 = **(uint64_t **)(local_sp_13 + 48UL);
            var_143 = local_sp_13 + (-8L);
            *(uint64_t *)var_143 = 4210391UL;
            var_144 = indirect_placeholder_1(var_142, 4289472UL);
            r13_1 = r13_2;
            rcx_10 = var_144;
            local_sp_14 = var_143;
        }
        rcx_11 = rcx_10;
        local_sp_15 = local_sp_14;
        r13_3 = r13_1;
        if (*(uint64_t *)(local_sp_14 + 64UL) == 0UL) {
            *(unsigned char *)4289594UL = (unsigned char)'\x01';
        }
        var_145 = local_sp_14 + 24UL;
        rbx_4 = var_145;
        var_146 = local_sp_15 + (-8L);
        *(uint64_t *)var_146 = 4209376UL;
        var_147 = indirect_placeholder_58(r8_7, 1UL, var_13, rcx_11, r10_9, var_145, r9_9);
        var_148 = var_147.field_0;
        var_149 = var_147.field_1;
        var_150 = var_147.field_2;
        var_151 = var_147.field_3;
        var_152 = var_147.field_4;
        r8_7 = var_149;
        r9_9 = var_152;
        rcx_11_be = var_150;
        local_sp_15_be = var_146;
        r10_9 = var_151;
        r8_8 = var_149;
        rcx_12 = var_150;
        r10_10 = var_151;
        local_sp_16 = var_146;
        r9_10 = var_152;
        while ((uint64_t)(unsigned char)var_148 != 0UL)
            {
                if (*(unsigned char *)4289597UL == '\x00') {
                    if (*(unsigned char *)4289592UL == '\x00') {
                        break;
                    }
                }
                var_153 = *(uint64_t *)(local_sp_15 + 16UL);
                var_154 = local_sp_15 + (-16L);
                *(uint64_t *)var_154 = 4210415UL;
                var_155 = indirect_placeholder_1(var_153, 4289472UL);
                rcx_11_be = var_155;
                local_sp_15_be = var_154;
                rcx_12 = var_155;
                local_sp_16 = var_154;
                if (*(unsigned char *)4289592UL != '\x00') {
                    if (*(unsigned char *)4289597UL == '\x00') {
                        break;
                    }
                }
                rcx_11 = rcx_11_be;
                local_sp_15 = local_sp_15_be;
                var_146 = local_sp_15 + (-8L);
                *(uint64_t *)var_146 = 4209376UL;
                var_147 = indirect_placeholder_58(r8_7, 1UL, var_13, rcx_11, r10_9, var_145, r9_9);
                var_148 = var_147.field_0;
                var_149 = var_147.field_1;
                var_150 = var_147.field_2;
                var_151 = var_147.field_3;
                var_152 = var_147.field_4;
                r8_7 = var_149;
                r9_9 = var_152;
                rcx_11_be = var_150;
                local_sp_15_be = var_146;
                r10_9 = var_151;
                r8_8 = var_149;
                rcx_12 = var_150;
                r10_10 = var_151;
                local_sp_16 = var_146;
                r9_10 = var_152;
            }
        var_156 = *(unsigned char *)4289596UL;
        rbx_6 = rbx_4;
        rax_4 = (uint64_t)var_156;
        rbx_5 = rbx_4;
        r8_9 = r8_8;
        rcx_13 = rcx_12;
        r10_11 = r10_10;
        local_sp_17 = local_sp_16;
        r9_11 = r9_10;
        local_sp_20 = local_sp_16;
        if ((var_156 == '\x00') && ((uint64_t)(unsigned char)r13_3 == 0UL)) {
            var_169 = local_sp_20 + 24UL;
            var_170 = *(uint64_t *)var_169;
            *(uint64_t *)(local_sp_20 + (-8L)) = 4210063UL;
            indirect_placeholder_1(rbx_6, var_170);
            *(uint64_t *)(local_sp_20 + (-16L)) = 4210073UL;
            indirect_placeholder();
            var_171 = local_sp_20 + 32UL;
            *(uint64_t *)(local_sp_20 + (-24L)) = 4210088UL;
            indirect_placeholder_28(var_169, var_171);
            var_172 = local_sp_20 + 56UL;
            var_173 = local_sp_20 + 48UL;
            *(uint64_t *)(local_sp_20 + (-32L)) = 4210103UL;
            indirect_placeholder_28(var_173, var_172);
            return;
        }
    }
    rax_4 = (uint64_t)*(unsigned char *)4289596UL;
}
