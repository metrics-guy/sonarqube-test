typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_66_ret_type;
struct indirect_placeholder_61_ret_type;
struct indirect_placeholder_58_ret_type;
struct indirect_placeholder_62_ret_type;
struct indirect_placeholder_63_ret_type;
struct indirect_placeholder_64_ret_type;
struct indirect_placeholder_65_ret_type;
struct indirect_placeholder_66_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_61_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_58_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_62_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_63_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_64_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_65_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_rdx(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_rcx(void);
extern struct indirect_placeholder_66_ret_type indirect_placeholder_66(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_61_ret_type indirect_placeholder_61(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_58_ret_type indirect_placeholder_58(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_62_ret_type indirect_placeholder_62(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_63_ret_type indirect_placeholder_63(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_64_ret_type indirect_placeholder_64(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_65_ret_type indirect_placeholder_65(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
uint64_t bb_sync_arg(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_64_ret_type var_13;
    struct indirect_placeholder_61_ret_type var_35;
    struct indirect_placeholder_66_ret_type var_8;
    struct indirect_placeholder_63_ret_type var_20;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint32_t var_9;
    uint64_t local_sp_3;
    uint64_t rbp_1;
    uint64_t r13_1;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t local_sp_0;
    uint64_t rax_0;
    uint64_t r8_0;
    uint64_t rbp_0;
    uint64_t r9_0;
    uint64_t storemerge;
    uint64_t var_32;
    uint64_t var_33;
    struct indirect_placeholder_62_ret_type var_34;
    uint32_t _pre_phi;
    uint64_t local_sp_1;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t local_sp_2;
    uint32_t var_15;
    bool var_16;
    uint64_t var_17;
    uint64_t *var_18;
    uint64_t var_19;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint32_t var_14;
    struct indirect_placeholder_65_ret_type var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r12();
    var_2 = init_rbx();
    var_3 = init_r13();
    var_4 = init_rbp();
    var_5 = init_rdx();
    var_6 = init_rcx();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_2;
    *(uint64_t *)(var_0 + (-48L)) = 4204478UL;
    indirect_placeholder();
    var_7 = var_0 + (-56L);
    *(uint64_t *)var_7 = 4204502UL;
    var_8 = indirect_placeholder_66(0UL, var_5, 0UL, var_6, 3UL);
    var_9 = (uint32_t)var_8.field_0;
    storemerge = 4251936UL;
    r13_1 = 0UL;
    rbp_1 = 0UL;
    local_sp_3 = var_7;
    if ((uint64_t)(var_9 + 1U) == 0UL) {
        *(uint64_t *)(local_sp_3 + (-8L)) = 4204613UL;
        var_26 = indirect_placeholder_65(rsi, 0UL, 4UL, rsi);
        var_27 = var_26.field_0;
        var_28 = var_26.field_1;
        var_29 = var_26.field_2;
        var_30 = var_26.field_5;
        var_31 = local_sp_3 + (-16L);
        *(uint64_t *)var_31 = 4204621UL;
        indirect_placeholder();
        local_sp_0 = var_31;
        rax_0 = var_27;
        r8_0 = var_28;
        rbp_0 = var_29;
        r9_0 = var_30;
        var_32 = (uint64_t)*(uint32_t *)rax_0;
        var_33 = local_sp_0 + (-8L);
        *(uint64_t *)var_33 = 4204643UL;
        var_34 = indirect_placeholder_62(0UL, r8_0, storemerge, 0UL, rax_0, r9_0, var_32);
        _pre_phi = (uint32_t)var_34.field_0;
        local_sp_2 = var_33;
        rbp_1 = rbp_0;
        *(uint64_t *)(local_sp_2 + (-8L)) = 4204650UL;
        indirect_placeholder();
        if ((int)_pre_phi >= (int)0U) {
            return r13_1;
        }
        *(uint64_t *)(local_sp_2 + (-16L)) = 4204784UL;
        var_35 = indirect_placeholder_61(rsi, 0UL, rbp_1, 4UL, rsi);
        var_36 = var_35.field_0;
        var_37 = var_35.field_1;
        var_38 = var_35.field_5;
        *(uint64_t *)(local_sp_2 + (-24L)) = 4204792UL;
        indirect_placeholder();
        var_39 = (uint64_t)*(uint32_t *)var_36;
        *(uint64_t *)(local_sp_2 + (-32L)) = 4204811UL;
        indirect_placeholder_58(0UL, var_37, 4251682UL, 0UL, var_36, var_38, var_39);
        return 0UL;
    }
    var_10 = var_8.field_1;
    var_11 = (uint64_t)(var_9 & (-2049));
    var_12 = var_0 + (-64L);
    *(uint64_t *)var_12 = 4204526UL;
    var_13 = indirect_placeholder_64(0UL, var_11, 0UL, var_10, 4UL);
    var_14 = (uint32_t)var_13.field_0;
    storemerge = 4251665UL;
    _pre_phi = var_14;
    r13_1 = 1UL;
    local_sp_3 = var_12;
    if ((int)var_14 < (int)0U) {
        return;
    }
    var_15 = (uint32_t)rdi;
    if ((uint64_t)(var_15 + (-1)) == 0UL) {
        var_19 = var_0 + (-72L);
        *(uint64_t *)var_19 = 4204759UL;
        indirect_placeholder();
        local_sp_1 = var_19;
    } else {
        var_16 = ((uint64_t)(var_15 + (-2)) == 0UL);
        var_17 = var_0 + (-72L);
        var_18 = (uint64_t *)var_17;
        local_sp_1 = var_17;
        if (var_16) {
            *var_18 = 4204555UL;
            indirect_placeholder();
        } else {
            *var_18 = 4204743UL;
            indirect_placeholder();
        }
    }
    local_sp_2 = local_sp_1;
    if ((int)var_14 <= (int)4294967295U) {
        *(uint64_t *)(local_sp_1 + (-8L)) = 4204578UL;
        var_20 = indirect_placeholder_63(rsi, 1UL, 0UL, 4UL, rsi);
        var_21 = var_20.field_0;
        var_22 = var_20.field_1;
        var_23 = var_20.field_2;
        var_24 = var_20.field_5;
        var_25 = local_sp_1 + (-16L);
        *(uint64_t *)var_25 = 4204586UL;
        indirect_placeholder();
        local_sp_0 = var_25;
        rax_0 = var_21;
        r8_0 = var_22;
        rbp_0 = var_23;
        r9_0 = var_24;
        var_32 = (uint64_t)*(uint32_t *)rax_0;
        var_33 = local_sp_0 + (-8L);
        *(uint64_t *)var_33 = 4204643UL;
        var_34 = indirect_placeholder_62(0UL, r8_0, storemerge, 0UL, rax_0, r9_0, var_32);
        _pre_phi = (uint32_t)var_34.field_0;
        local_sp_2 = var_33;
        rbp_1 = rbp_0;
    }
    *(uint64_t *)(local_sp_2 + (-8L)) = 4204650UL;
    indirect_placeholder();
    if ((int)_pre_phi >= (int)0U) {
        return r13_1;
    }
    *(uint64_t *)(local_sp_2 + (-16L)) = 4204784UL;
    var_35 = indirect_placeholder_61(rsi, 0UL, rbp_1, 4UL, rsi);
    var_36 = var_35.field_0;
    var_37 = var_35.field_1;
    var_38 = var_35.field_5;
    *(uint64_t *)(local_sp_2 + (-24L)) = 4204792UL;
    indirect_placeholder();
    var_39 = (uint64_t)*(uint32_t *)var_36;
    *(uint64_t *)(local_sp_2 + (-32L)) = 4204811UL;
    indirect_placeholder_58(0UL, var_37, 4251682UL, 0UL, var_36, var_38, var_39);
    return 0UL;
}
