typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_27_ret_type;
struct indirect_placeholder_28_ret_type;
struct indirect_placeholder_29_ret_type;
struct indirect_placeholder_30_ret_type;
struct indirect_placeholder_31_ret_type;
struct indirect_placeholder_27_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_28_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_29_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_30_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_31_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t indirect_placeholder_7(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r12(void);
extern uint64_t init_r15(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_1(uint64_t param_0);
extern void indirect_placeholder_6(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_2(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_18(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_27_ret_type indirect_placeholder_27(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_28_ret_type indirect_placeholder_28(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_29_ret_type indirect_placeholder_29(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_30_ret_type indirect_placeholder_30(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_31_ret_type indirect_placeholder_31(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
void bb_set_fields(uint64_t rdi, uint64_t rcx, uint64_t r10, uint64_t rsi, uint64_t r9) {
    struct indirect_placeholder_27_ret_type var_34;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t rbp_1;
    uint64_t rbx_0;
    uint64_t rbp_0;
    uint64_t local_sp_3;
    uint64_t local_sp_6;
    uint64_t rdx_0;
    uint64_t rcx2_0;
    uint64_t local_sp_0;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t rcx2_1;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t local_sp_1;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t *var_64;
    uint64_t var_65;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t local_sp_2;
    uint64_t r8_4;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_72;
    uint64_t r12_6;
    uint64_t r13_1;
    uint64_t r12_1;
    uint64_t r8_0;
    uint64_t rcx2_2;
    uint64_t r103_0;
    uint64_t local_sp_4;
    uint64_t r95_0;
    uint64_t r12_3;
    uint64_t r8_1;
    uint64_t rcx2_4;
    uint64_t r103_2;
    uint64_t r95_2;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_80;
    uint64_t r12_7;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t rdx_3;
    uint64_t rcx2_6;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t r8_3;
    bool var_26;
    uint64_t var_27;
    uint64_t r14_1;
    uint64_t var_28;
    uint64_t rax_0_ph;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t rax_0;
    uint64_t r8_6;
    uint64_t var_33;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    struct indirect_placeholder_28_ret_type var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t storemerge2;
    uint64_t var_10;
    bool var_11;
    uint64_t spec_select;
    uint64_t spec_select234;
    uint64_t storemerge1;
    uint64_t r15_1;
    uint64_t r12_4;
    uint64_t r15_0;
    uint64_t r13_0;
    uint64_t r14_0;
    uint64_t r8_2;
    uint64_t rbp_2;
    uint64_t rdx_2;
    uint64_t r103_4;
    uint64_t rcx2_5;
    uint64_t local_sp_8;
    uint64_t r103_3;
    uint64_t r95_4;
    uint64_t local_sp_7;
    uint64_t r95_3;
    unsigned char *var_12;
    unsigned char var_13;
    uint64_t var_14;
    uint64_t cc_src_1;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    bool var_88;
    uint64_t var_89;
    uint64_t _r8_2;
    uint64_t _;
    uint64_t _236;
    uint64_t r12_5;
    uint64_t var_15;
    uint64_t *var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t rdx_4;
    uint64_t local_sp_9;
    uint64_t r95_5;
    bool var_44;
    bool var_45;
    uint64_t var_46;
    struct indirect_placeholder_29_ret_type var_47;
    uint64_t var_48;
    struct indirect_placeholder_30_ret_type var_49;
    uint64_t var_50;
    struct indirect_placeholder_31_ret_type var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t rcx2_8;
    uint64_t local_sp_11;
    uint64_t r95_7;
    uint64_t storemerge3;
    uint64_t storemerge;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t r8_8;
    uint64_t rdx_6;
    uint64_t rcx2_10;
    uint64_t local_sp_13;
    uint64_t r95_9;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r12();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_r13();
    var_5 = init_r14();
    var_6 = init_rbp();
    var_7 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_1;
    *(uint64_t *)(var_0 + (-40L)) = var_6;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_8 = var_0 + (-88L);
    var_9 = rsi & 1UL;
    *(uint32_t *)(var_0 + (-60L)) = (uint32_t)rsi;
    *(uint32_t *)(var_0 + (-64L)) = (uint32_t)var_9;
    rbp_1 = rdi;
    rbx_0 = 0UL;
    rbp_0 = 16UL;
    rcx2_0 = 4207808UL;
    rcx2_1 = 4207808UL;
    r12_6 = 1UL;
    r13_1 = 0UL;
    rdx_3 = 0UL;
    r8_3 = 0UL;
    r14_1 = 0UL;
    rax_0_ph = 1844674407370955161UL;
    storemerge2 = 0UL;
    storemerge1 = 0UL;
    r15_1 = 0UL;
    r12_4 = 1UL;
    r15_0 = 0UL;
    r14_0 = 0UL;
    rcx2_5 = rcx;
    r103_3 = r10;
    local_sp_7 = var_8;
    r95_3 = r9;
    cc_src_1 = 45UL;
    storemerge3 = 4258880UL;
    storemerge = 4259122UL;
    rdx_6 = 4259244UL;
    if (var_9 != 0UL & *(unsigned char *)rdi == '-') {
        var_10 = rdi + 1UL;
        var_11 = (*(unsigned char *)var_10 == '\x00');
        spec_select = var_11 ? var_10 : rdi;
        spec_select234 = var_11 ? 1UL : 0UL;
        rbp_1 = spec_select;
        storemerge2 = spec_select234;
        storemerge1 = spec_select234;
    }
    r13_0 = storemerge1;
    r8_2 = storemerge2;
    rbp_2 = rbp_1;
    rdx_2 = storemerge1;
    while (1U)
        {
            var_12 = (unsigned char *)rbp_2;
            var_13 = *var_12;
            var_14 = (uint64_t)var_13;
            r8_4 = r8_2;
            r12_1 = r12_4;
            r12_3 = r12_4;
            r12_7 = r12_4;
            rcx2_6 = rcx2_5;
            var_25 = rbp_2;
            r8_6 = r8_2;
            r103_4 = r103_3;
            local_sp_8 = local_sp_7;
            r95_4 = r95_3;
            r12_5 = r12_4;
            rdx_4 = rdx_2;
            local_sp_9 = local_sp_7;
            r95_5 = r95_3;
            rcx2_8 = rcx2_5;
            local_sp_11 = local_sp_7;
            r95_7 = r95_3;
            rcx2_10 = rcx2_5;
            if ((uint64_t)(var_13 + '\xd3') != 0UL) {
                cc_src_1 = 44UL;
                r8_6 = 0UL;
                storemerge3 = 4258912UL;
                storemerge = 4259142UL;
                if ((uint64_t)(var_13 + '\xd4') != 0UL) {
                    var_15 = (uint64_t)(uint32_t)(uint64_t)var_13;
                    *(uint64_t *)(local_sp_7 + 8UL) = r8_2;
                    *(unsigned char *)(local_sp_7 + 19UL) = (unsigned char)rdx_2;
                    *(uint32_t *)(local_sp_7 + 20UL) = (uint32_t)var_13;
                    var_16 = (uint64_t *)(local_sp_7 + (-8L));
                    *var_16 = 4208135UL;
                    var_17 = indirect_placeholder_1(var_15);
                    var_18 = local_sp_7 + (-16L);
                    *(uint64_t *)var_18 = 4208143UL;
                    indirect_placeholder();
                    var_19 = (uint64_t)*(unsigned char *)(local_sp_7 + 3UL);
                    var_20 = *var_16;
                    r8_4 = var_20;
                    r13_1 = r13_0;
                    r14_1 = 1UL;
                    local_sp_8 = var_18;
                    rdx_4 = var_19;
                    local_sp_9 = var_18;
                    r95_5 = var_15;
                    var_21 = local_sp_7 + 4UL;
                    var_22 = (uint64_t)*(uint32_t *)var_21;
                    r95_5 = var_22;
                    if ((uint64_t)(uint32_t)var_17 != 0UL & var_13 != '\x00') {
                        var_23 = var_22 + 4294967248UL;
                        if ((uint64_t)((uint32_t)var_23 & (-2)) > 9UL) {
                            *(uint64_t *)(local_sp_7 + (-24L)) = 4209052UL;
                            var_39 = indirect_placeholder_28(r12_4, var_14, r15_0, r13_0, r14_0, rbp_2, rbp_2);
                            var_40 = var_39.field_0;
                            var_41 = var_39.field_1;
                            var_42 = var_39.field_6;
                            var_43 = ((*(unsigned char *)var_21 & '\x04') == '\x00') ? 4259198UL : 4259000UL;
                            *(uint64_t *)(local_sp_7 + (-32L)) = 4209085UL;
                            indirect_placeholder_2(0UL, var_41, var_43, 0UL, var_40, 0UL, var_42);
                            *(uint64_t *)(local_sp_7 + (-40L)) = 4209095UL;
                            indirect_placeholder_18(r12_4, rbp_2, 1UL);
                            abort();
                        }
                        if ((uint64_t)(unsigned char)r14_0 == 0UL) {
                            *(uint64_t *)4281288UL = rbp_2;
                        } else {
                            var_24 = *(uint64_t *)4281288UL;
                            var_25 = var_24;
                            if (var_24 == 0UL) {
                                *(uint64_t *)4281288UL = rbp_2;
                            }
                        }
                        var_26 = ((uint64_t)(unsigned char)r13_0 == 0UL);
                        var_27 = var_26 ? 1UL : var_19;
                        var_28 = (uint64_t)(uint32_t)(var_26 ? r15_0 : r13_0);
                        var_32 = var_25;
                        r15_1 = var_28;
                        rdx_3 = var_27;
                        if (var_20 <= 1844674407370955161UL) {
                            loop_state_var = 4U;
                            break;
                        }
                        var_29 = (uint64_t)((long)(var_23 << 32UL) >> (long)32UL);
                        var_30 = (var_20 * 10UL) + var_29;
                        rax_0_ph = 18446744073709551615UL;
                        rax_0 = var_30;
                        r8_3 = var_30;
                        r95_4 = var_29;
                        if (var_30 != 18446744073709551615UL) {
                            loop_state_var = 4U;
                            break;
                        }
                        var_31 = helper_cc_compute_c_wrapper(var_30 - var_20, var_20, var_7, 17U);
                        if (var_31 != 0UL) {
                            var_32 = *(uint64_t *)4281288UL;
                            loop_state_var = 3U;
                            break;
                        }
                        r12_4 = r12_5;
                        r15_0 = r15_1;
                        r13_0 = r13_1;
                        r14_0 = r14_1;
                        r8_2 = r8_3;
                        rbp_2 = rbp_2 + 1UL;
                        rdx_2 = rdx_3;
                        rcx2_5 = rcx2_6;
                        r103_3 = r103_4;
                        local_sp_7 = local_sp_8;
                        r95_3 = r95_4;
                        continue;
                    }
                }
                local_sp_11 = local_sp_9;
                r95_7 = r95_5;
                r8_8 = r8_4;
                local_sp_13 = local_sp_9;
                r95_9 = r95_5;
                if ((uint64_t)(unsigned char)r13_0 == 0UL) {
                    if (r8_4 != 0UL) {
                        loop_state_var = 2U;
                        break;
                    }
                    var_50 = local_sp_9 + (-8L);
                    *(uint64_t *)var_50 = 4208340UL;
                    var_51 = indirect_placeholder_31(r8_4, rcx2_5, r103_3, r8_4);
                    var_52 = var_51.field_1;
                    var_53 = var_51.field_2;
                    var_54 = var_51.field_3;
                    local_sp_6 = var_50;
                    rcx2_4 = var_52;
                    r103_2 = var_53;
                    r95_2 = var_54;
                    rcx2_6 = var_52;
                    r103_4 = var_53;
                    local_sp_8 = var_50;
                    r95_4 = var_54;
                    if (*var_12 != '\x00') {
                        r8_1 = var_51.field_0;
                        loop_state_var = 0U;
                        break;
                    }
                }
                var_44 = ((uint64_t)(unsigned char)rdx_4 == 0UL);
                var_45 = ((uint64_t)(unsigned char)r15_0 == 0UL);
                if (var_44) {
                    rdx_6 = 4259088UL;
                    if (var_45) {
                        if (*(uint32_t *)(local_sp_9 + 24UL) != 0U) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_48 = local_sp_9 + (-8L);
                        *(uint64_t *)var_48 = 4208229UL;
                        var_49 = indirect_placeholder_30(r12_6, rcx2_5, r103_3, 18446744073709551615UL);
                        r12_1 = r12_6;
                        r8_0 = var_49.field_0;
                        rcx2_2 = var_49.field_1;
                        r103_0 = var_49.field_2;
                        local_sp_4 = var_48;
                        r95_0 = var_49.field_3;
                    } else {
                        if (r12_4 <= r8_4) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_46 = local_sp_9 + (-8L);
                        *(uint64_t *)var_46 = 4208677UL;
                        var_47 = indirect_placeholder_29(r12_4, rcx2_5, r103_3, r8_4);
                        r8_0 = var_47.field_0;
                        rcx2_2 = var_47.field_1;
                        r103_0 = var_47.field_2;
                        local_sp_4 = var_46;
                        r95_0 = var_47.field_3;
                    }
                } else {
                    r12_6 = r12_4;
                    if (var_45) {
                        var_48 = local_sp_9 + (-8L);
                        *(uint64_t *)var_48 = 4208229UL;
                        var_49 = indirect_placeholder_30(r12_6, rcx2_5, r103_3, 18446744073709551615UL);
                        r12_1 = r12_6;
                        r8_0 = var_49.field_0;
                        rcx2_2 = var_49.field_1;
                        r103_0 = var_49.field_2;
                        local_sp_4 = var_48;
                        r95_0 = var_49.field_3;
                    } else {
                        if (r12_4 <= r8_4) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_46 = local_sp_9 + (-8L);
                        *(uint64_t *)var_46 = 4208677UL;
                        var_47 = indirect_placeholder_29(r12_4, rcx2_5, r103_3, r8_4);
                        r8_0 = var_47.field_0;
                        rcx2_2 = var_47.field_1;
                        r103_0 = var_47.field_2;
                        local_sp_4 = var_46;
                        r95_0 = var_47.field_3;
                    }
                }
                local_sp_6 = local_sp_4;
                r12_3 = r12_1;
                r8_1 = r8_0;
                rcx2_4 = rcx2_2;
                r103_2 = r103_0;
                r95_2 = r95_0;
                rcx2_6 = rcx2_2;
                r103_4 = r103_0;
                local_sp_8 = local_sp_4;
                r95_4 = r95_0;
                r12_5 = r12_1;
                if (*var_12 != '\x00') {
                    loop_state_var = 0U;
                    break;
                }
            }
            r15_1 = r15_0;
            rdx_3 = rdx_2;
            if ((uint64_t)(unsigned char)r13_0 != 0UL) {
                loop_state_var = 2U;
                break;
            }
            var_81 = (r8_2 == 0UL);
            var_82 = rcx2_5 & (-256L);
            var_83 = ((var_82 | var_81) & rdx_2) & 1UL;
            var_84 = var_82 | var_83;
            var_85 = (uint64_t)((uint32_t)var_84 & (-255));
            rcx2_6 = var_84;
            rcx2_8 = var_84;
            storemerge3 = 4258912UL;
            storemerge = 4259142UL;
            if (var_83 != 0UL) {
                loop_state_var = 2U;
                break;
            }
            var_88 = ((uint64_t)(unsigned char)rdx_2 == 0UL);
            var_89 = (uint64_t)(uint32_t)rdx_2;
            _r8_2 = var_88 ? 1UL : r8_2;
            _ = var_88 ? 1UL : var_89;
            _236 = var_88 ? var_89 : var_85;
            r12_5 = _r8_2;
            r13_1 = _;
            r14_1 = _236;
        }
    switch (loop_state_var) {
      case 0U:
        {
            r12_7 = r12_3;
            r8_8 = r8_1;
            rcx2_10 = rcx2_4;
            local_sp_13 = local_sp_6;
            r95_9 = r95_2;
            if (*(uint64_t *)4281736UL == 0UL) {
                var_80 = ((*(unsigned char *)(local_sp_6 + 28UL) & '\x04') == '\x00') ? 4259221UL : 4259040UL;
                rdx_6 = var_80;
                *(uint64_t *)(local_sp_13 + (-8L)) = 4208904UL;
                indirect_placeholder_2(0UL, r8_8, rdx_6, 0UL, rcx2_10, 0UL, r95_9);
                *(uint64_t *)(local_sp_13 + (-16L)) = 4208914UL;
                indirect_placeholder_18(r12_7, rbp_2, 1UL);
                abort();
            }
            var_55 = local_sp_6 + (-8L);
            *(uint64_t *)var_55 = 4208398UL;
            indirect_placeholder();
            var_56 = *(uint64_t *)4281736UL;
            rdx_0 = var_56;
            local_sp_0 = var_55;
            local_sp_2 = var_55;
            if (var_56 == 0UL) {
                var_57 = rbx_0 + 1UL;
                var_58 = helper_cc_compute_c_wrapper(var_57 - rdx_0, rdx_0, var_7, 17U);
                rbx_0 = var_57;
                local_sp_1 = local_sp_0;
                rcx2_1 = rcx2_0;
                local_sp_2 = local_sp_0;
                while (var_58 != 0UL)
                    {
                        var_59 = rbp_0 + (-16L);
                        var_60 = rbp_0 + 16UL;
                        rbp_0 = var_60;
                        while (1U)
                            {
                                var_61 = *(uint64_t *)4281728UL;
                                var_62 = var_59 + var_61;
                                var_63 = rbp_0 + var_61;
                                var_64 = (uint64_t *)(var_62 + 8UL);
                                var_65 = *var_64;
                                rcx2_0 = var_62;
                                local_sp_0 = local_sp_1;
                                rcx2_1 = var_62;
                                local_sp_2 = local_sp_1;
                                if (*(uint64_t *)var_63 <= var_65) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                var_66 = *(uint64_t *)(var_63 + 8UL);
                                var_67 = helper_cc_compute_c_wrapper(var_66 - var_65, var_65, var_7, 17U);
                                *var_64 = ((var_67 == 0UL) ? var_66 : var_65);
                                var_68 = local_sp_1 + (-8L);
                                *(uint64_t *)var_68 = 4208468UL;
                                indirect_placeholder();
                                var_69 = *(uint64_t *)4281736UL + (-1L);
                                *(uint64_t *)4281736UL = var_69;
                                local_sp_1 = var_68;
                                local_sp_2 = var_68;
                                if (var_69 > var_57) {
                                    continue;
                                }
                                loop_state_var = 1U;
                                break;
                            }
                        switch_state_var = 0;
                        switch (loop_state_var) {
                          case 1U:
                            {
                                switch_state_var = 1;
                                break;
                            }
                            break;
                          case 0U:
                            {
                                var_70 = *(uint64_t *)4281736UL;
                                var_71 = helper_cc_compute_c_wrapper(var_57 - var_70, var_70, var_7, 17U);
                                rdx_0 = var_70;
                                if (var_71 == 0UL) {
                                    switch_state_var = 1;
                                    break;
                                }
                                var_57 = rbx_0 + 1UL;
                                var_58 = helper_cc_compute_c_wrapper(var_57 - rdx_0, rdx_0, var_7, 17U);
                                rbx_0 = var_57;
                                local_sp_1 = local_sp_0;
                                rcx2_1 = rcx2_0;
                                local_sp_2 = local_sp_0;
                                continue;
                            }
                            break;
                        }
                        if (switch_state_var)
                            break;
                    }
            }
            local_sp_3 = local_sp_2;
            if ((*(unsigned char *)(local_sp_2 + 28UL) & '\x02') != '\x00') {
                var_72 = local_sp_2 + (-8L);
                *(uint64_t *)var_72 = 4208869UL;
                indirect_placeholder_6(rcx2_1, r103_2);
                local_sp_3 = var_72;
            }
            var_73 = *(uint64_t *)4281728UL;
            var_74 = *(uint64_t *)4281736UL + 1UL;
            *(uint64_t *)4281736UL = var_74;
            var_75 = var_74 << 4UL;
            *(uint64_t *)(local_sp_3 + (-8L)) = 4208589UL;
            var_76 = indirect_placeholder_7(var_73, var_75);
            var_77 = *(uint64_t *)4281736UL;
            *(uint64_t *)4281728UL = var_76;
            var_78 = (var_77 << 4UL) + var_76;
            var_79 = var_78 + (-16L);
            *(uint64_t *)(var_78 + (-8L)) = 18446744073709551615UL;
            *(uint64_t *)var_79 = 18446744073709551615UL;
            return;
        }
        break;
      case 4U:
      case 3U:
        {
            switch (loop_state_var) {
              case 3U:
                {
                    *(uint64_t *)(local_sp_7 + (-24L)) = 4208934UL;
                    indirect_placeholder();
                    *(uint64_t *)(local_sp_7 + (-32L)) = 4208945UL;
                    var_33 = indirect_placeholder_7(var_32, rax_0);
                    *(uint64_t *)(local_sp_7 + (-40L)) = 4208956UL;
                    var_34 = indirect_placeholder_27(r12_4, var_14, var_28, r13_0, r14_0, var_33, var_33);
                    var_35 = var_34.field_0;
                    var_36 = var_34.field_1;
                    var_37 = var_34.field_6;
                    var_38 = ((*(unsigned char *)(local_sp_7 + (-12L)) & '\x04') == '\x00') ? 4259169UL : 4258960UL;
                    *(uint64_t *)(local_sp_7 + (-48L)) = 4208989UL;
                    indirect_placeholder_2(0UL, var_36, var_38, 0UL, var_35, 0UL, var_37);
                    *(uint64_t *)(local_sp_7 + (-56L)) = 4208997UL;
                    indirect_placeholder();
                    *(uint64_t *)(local_sp_7 + (-64L)) = 4209007UL;
                    indirect_placeholder_18(r12_4, var_33, 1UL);
                    abort();
                }
                break;
              case 4U:
                {
                    rax_0 = rax_0_ph;
                }
                break;
            }
        }
        break;
      case 2U:
      case 1U:
        {
            switch (loop_state_var) {
              case 2U:
                {
                    var_86 = helper_cc_compute_all_wrapper((uint64_t)(*(unsigned char *)(local_sp_11 + 28UL) & '\x04'), cc_src_1, var_7, 22U);
                    var_87 = ((var_86 & 64UL) == 0UL) ? storemerge3 : storemerge;
                    r8_8 = r8_6;
                    rdx_6 = var_87;
                    rcx2_10 = rcx2_8;
                    local_sp_13 = local_sp_11;
                    r95_9 = r95_7;
                }
                break;
              case 1U:
                {
                    *(uint64_t *)(local_sp_13 + (-8L)) = 4208904UL;
                    indirect_placeholder_2(0UL, r8_8, rdx_6, 0UL, rcx2_10, 0UL, r95_9);
                    *(uint64_t *)(local_sp_13 + (-16L)) = 4208914UL;
                    indirect_placeholder_18(r12_7, rbp_2, 1UL);
                    abort();
                }
                break;
            }
        }
        break;
    }
}
