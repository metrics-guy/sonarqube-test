typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_22_ret_type;
struct indirect_placeholder_17_ret_type;
struct indirect_placeholder_18_ret_type;
struct indirect_placeholder_19_ret_type;
struct indirect_placeholder_20_ret_type;
struct indirect_placeholder_21_ret_type;
struct indirect_placeholder_22_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_17_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_18_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_19_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_20_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_21_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_5(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_13(uint64_t param_0);
extern uint64_t init_r10(void);
extern struct indirect_placeholder_22_ret_type indirect_placeholder_22(void);
extern struct indirect_placeholder_17_ret_type indirect_placeholder_17(void);
extern struct indirect_placeholder_18_ret_type indirect_placeholder_18(uint64_t param_0);
extern struct indirect_placeholder_19_ret_type indirect_placeholder_19(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
extern struct indirect_placeholder_20_ret_type indirect_placeholder_20(void);
extern struct indirect_placeholder_21_ret_type indirect_placeholder_21(void);
typedef _Bool bool;
uint64_t bb_print_page(uint64_t r9) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    struct indirect_placeholder_22_ret_type var_9;
    uint64_t rbx_9;
    uint64_t r12_7;
    uint64_t r12_8;
    uint64_t r12_0;
    uint64_t r13_10;
    uint64_t r8_5;
    uint64_t rbx_0;
    uint64_t r14_8;
    uint64_t rbp_1;
    uint64_t r13_0;
    uint64_t r10_8;
    uint64_t rbp_0;
    uint64_t r14_0;
    uint64_t local_sp_11;
    uint64_t local_sp_4;
    uint64_t r10_0;
    uint64_t r13_2;
    uint64_t local_sp_0;
    uint64_t r91_0;
    uint64_t r12_2;
    uint64_t r12_1;
    uint64_t rbx_1;
    uint64_t r13_1;
    uint64_t r14_1;
    uint64_t r10_1;
    uint64_t local_sp_1;
    uint64_t r91_1;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t local_sp_2;
    uint64_t var_58;
    uint64_t r12_9;
    uint64_t rbx_2;
    uint64_t r13_3;
    uint64_t local_sp_3;
    uint64_t r12_11;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t rax_0;
    uint32_t *_pre_phi64;
    uint64_t r14_6;
    uint64_t rbx_3;
    uint64_t r13_4;
    uint64_t r14_2;
    uint64_t r8_0;
    uint64_t r91_5;
    uint64_t r10_2;
    uint32_t var_31;
    unsigned char var_32;
    uint64_t var_33;
    uint32_t var_34;
    uint64_t var_35;
    uint64_t rbx_4;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t r8_1;
    uint64_t r10_6;
    uint64_t local_sp_5;
    uint64_t r12_6;
    uint32_t var_40;
    uint64_t r12_4;
    uint64_t r12_5;
    uint64_t var_38;
    struct indirect_placeholder_17_ret_type var_39;
    uint64_t var_28;
    struct indirect_placeholder_18_ret_type var_29;
    uint64_t var_30;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t rbx_7;
    uint64_t rbx_8;
    uint64_t rbx_10;
    uint64_t r12_3;
    uint64_t r14_9;
    uint64_t r13_5;
    uint64_t r10_9;
    uint64_t r14_3;
    uint64_t r8_2;
    uint64_t local_sp_9;
    uint64_t local_sp_12;
    uint64_t r10_3;
    uint64_t local_sp_6;
    uint64_t r91_2;
    uint32_t *var_24;
    uint64_t var_25;
    uint64_t rbx_5;
    uint64_t r13_6;
    uint64_t r14_4;
    uint64_t r8_3;
    uint64_t r10_4;
    uint64_t local_sp_7;
    uint64_t r91_3;
    uint64_t rbx_6;
    uint64_t r13_7;
    uint64_t r14_5;
    uint64_t r8_4;
    uint64_t r10_5;
    uint64_t local_sp_8;
    uint64_t r91_4;
    uint64_t var_41;
    struct indirect_placeholder_19_ret_type var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t r13_8;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t r13_9;
    uint64_t r14_7;
    uint64_t r10_7;
    uint64_t local_sp_10;
    uint64_t r91_6;
    uint64_t var_52;
    uint64_t var_53;
    uint32_t var_26;
    uint64_t var_27;
    uint64_t r91_7;
    uint64_t var_54;
    struct indirect_placeholder_20_ret_type var_55;
    uint64_t var_10;
    unsigned char var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t r13_11;
    uint64_t r91_8;
    uint64_t var_18;
    struct indirect_placeholder_21_ret_type var_19;
    uint64_t var_20;
    uint64_t var_14;
    uint64_t local_sp_15;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t r12_10;
    uint64_t rbx_11;
    uint64_t rdx_0;
    uint64_t local_sp_13;
    uint64_t var_62;
    uint64_t rbx_12;
    uint64_t rax_1;
    uint64_t r13_12;
    uint64_t var_63;
    uint64_t local_sp_14;
    unsigned char var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t local_sp_16;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r12();
    var_2 = init_rbx();
    var_3 = init_r13();
    var_4 = init_r14();
    var_5 = init_rbp();
    var_6 = init_r10();
    var_7 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_2;
    *(uint64_t *)(var_0 + (-48L)) = 4214831UL;
    indirect_placeholder_13(r9);
    var_8 = var_0 + (-56L);
    *(uint64_t *)var_8 = 4214836UL;
    var_9 = indirect_placeholder_22();
    rbp_0 = 1UL;
    r12_9 = 0UL;
    rbx_10 = var_2;
    r14_9 = var_4;
    r10_9 = var_6;
    local_sp_12 = var_8;
    r91_8 = r9;
    local_sp_15 = var_8;
    r12_10 = 0UL;
    rbx_11 = var_2;
    local_sp_13 = var_8;
    r13_12 = 0UL;
    if ((uint64_t)(uint32_t)var_9.field_0 == 0UL) {
        return 0UL;
    }
    if (*(unsigned char *)4322053UL == '\x00') {
        *(unsigned char *)4322520UL = (unsigned char)'\x01';
    }
    var_10 = (uint64_t)*(uint32_t *)4322512UL;
    var_11 = *(unsigned char *)4322424UL;
    *(unsigned char *)4322400UL = (unsigned char)'\x00';
    var_12 = (uint64_t)(uint32_t)(var_10 << (var_11 != '\x00'));
    var_13 = helper_cc_compute_all_wrapper(var_12, 0UL, 0UL, 24U);
    r13_11 = var_12;
    if ((uint64_t)(((unsigned char)(var_13 >> 4UL) ^ (unsigned char)var_13) & '\xc0') == 0UL) {
        var_14 = helper_cc_compute_all_wrapper(var_12, var_13, var_7, 1U);
        if ((var_14 & 64UL) != 0UL) {
            local_sp_16 = local_sp_15;
            if (*(unsigned char *)4322522UL != '\x00' & *(unsigned char *)4322521UL == '\x00') {
                var_67 = local_sp_15 + (-8L);
                *(uint64_t *)var_67 = 4215604UL;
                indirect_placeholder();
                *(unsigned char *)4322521UL = (unsigned char)'\x00';
                local_sp_16 = var_67;
            }
            var_68 = *(uint64_t *)4322456UL + 1UL;
            var_69 = *(uint64_t *)4322016UL;
            *(uint64_t *)4322456UL = var_68;
            if (var_68 > var_69) {
                return 0UL;
            }
            *(uint64_t *)(local_sp_16 + (-8L)) = 4215564UL;
            indirect_placeholder();
            return 1UL;
        }
        var_15 = (uint64_t)*(uint32_t *)4322024UL;
        var_16 = *(uint64_t *)4322568UL;
        var_17 = helper_cc_compute_all_wrapper(var_15, 0UL, 0UL, 24U);
        rax_0 = var_16;
        rdx_0 = var_15;
        if ((uint64_t)(((unsigned char)(var_17 >> 4UL) ^ (unsigned char)var_17) & '\xc0') == 0UL) {
            local_sp_16 = local_sp_15;
            if (*(unsigned char *)4322522UL != '\x00' & *(unsigned char *)4322521UL == '\x00') {
                var_67 = local_sp_15 + (-8L);
                *(uint64_t *)var_67 = 4215604UL;
                indirect_placeholder();
                *(unsigned char *)4322521UL = (unsigned char)'\x00';
                local_sp_16 = var_67;
            }
            var_68 = *(uint64_t *)4322456UL + 1UL;
            var_69 = *(uint64_t *)4322016UL;
            *(uint64_t *)4322456UL = var_68;
            if (var_68 > var_69) {
                return 0UL;
            }
            *(uint64_t *)(local_sp_16 + (-8L)) = 4215564UL;
            indirect_placeholder();
            return 1UL;
        }
        var_62 = ((((rdx_0 << 6UL) + 274877906880UL) & 274877906880UL) + 64UL) + rax_0;
        rax_1 = rax_0;
        r12_11 = r12_10;
        rbx_12 = rbx_11;
        local_sp_14 = local_sp_13;
        var_63 = rax_1 + 64UL;
        rax_1 = var_63;
        do {
            if (*(uint32_t *)(rax_1 + 16UL) == 0U) {
                *(unsigned char *)(rax_1 + 57UL) = (unsigned char)'\x01';
            }
            var_63 = rax_1 + 64UL;
            rax_1 = var_63;
        } while (var_63 != var_62);
    } else {
        while (1U)
            {
                var_18 = local_sp_12 + (-8L);
                *(uint64_t *)var_18 = 4214909UL;
                var_19 = indirect_placeholder_21();
                var_20 = var_19.field_1;
                r12_8 = r12_9;
                r13_10 = r13_11;
                r14_8 = r14_9;
                r10_8 = r10_9;
                local_sp_11 = var_18;
                r12_11 = r12_9;
                r12_3 = r12_9;
                r13_5 = r13_11;
                r14_3 = r14_9;
                r8_2 = var_20;
                r10_3 = r10_9;
                local_sp_6 = var_18;
                r91_2 = r91_8;
                r91_7 = r91_8;
                rbx_12 = rbx_10;
                r13_12 = r13_11;
                local_sp_14 = var_18;
                if ((uint64_t)(uint32_t)var_19.field_0 != 0UL) {
                    loop_state_var = 1U;
                    break;
                }
                var_21 = (uint64_t)*(uint32_t *)4322024UL;
                *(uint32_t *)4322488UL = 0U;
                *(unsigned char *)4322400UL = (unsigned char)'\x00';
                var_22 = *(uint64_t *)4322568UL;
                *(uint32_t *)4322496UL = 0U;
                *(uint32_t *)4322408UL = 0U;
                *(unsigned char *)4322526UL = (unsigned char)'\x00';
                *(unsigned char *)4322525UL = (unsigned char)'\x01';
                var_23 = helper_cc_compute_all_wrapper(var_21, 0UL, 0UL, 24U);
                rbx_4 = var_22;
                rbx_9 = var_22;
                if ((uint64_t)(((unsigned char)(var_23 >> 4UL) ^ (unsigned char)var_23) & '\xc0') != 0UL) {
                    while (1U)
                        {
                            *(uint32_t *)4322484UL = 0U;
                            var_24 = (uint32_t *)(rbx_4 + 48UL);
                            var_25 = helper_cc_compute_all_wrapper((uint64_t)*var_24, 0UL, 0UL, 24U);
                            r8_5 = r8_2;
                            rbp_1 = rbp_0;
                            _pre_phi64 = var_24;
                            r14_6 = r14_3;
                            rbx_3 = rbx_4;
                            r13_4 = r13_5;
                            r14_2 = r14_3;
                            r8_0 = r8_2;
                            r91_5 = r91_2;
                            r10_2 = r10_3;
                            r10_6 = r10_3;
                            r12_6 = r12_3;
                            r12_4 = r12_3;
                            r12_5 = r12_3;
                            rbx_7 = rbx_4;
                            local_sp_9 = local_sp_6;
                            rbx_5 = rbx_4;
                            r13_6 = r13_5;
                            r14_4 = r14_3;
                            r8_3 = r8_2;
                            r10_4 = r10_3;
                            local_sp_7 = local_sp_6;
                            r91_3 = r91_2;
                            rbx_6 = rbx_4;
                            r13_7 = r13_5;
                            r14_5 = r14_3;
                            r8_4 = r8_2;
                            r10_5 = r10_3;
                            local_sp_8 = local_sp_6;
                            r91_4 = r91_2;
                            r13_8 = r13_5;
                            if ((uint64_t)(((unsigned char)(var_25 >> 4UL) ^ (unsigned char)var_25) & '\xc0') != 0UL) {
                                if (*(uint32_t *)(rbx_4 + 16UL) != 1U) {
                                    if (*(unsigned char *)4322527UL != '\x00') {
                                        r12_7 = r12_6;
                                        r12_8 = r12_6;
                                        r13_10 = r13_8;
                                        r14_8 = r14_6;
                                        r10_8 = r10_6;
                                        local_sp_11 = local_sp_9;
                                        r12_3 = r12_6;
                                        r13_5 = r13_8;
                                        r14_3 = r14_6;
                                        r8_2 = r8_5;
                                        r10_3 = r10_6;
                                        local_sp_6 = local_sp_9;
                                        r91_2 = r91_5;
                                        r13_9 = r13_8;
                                        r14_7 = r14_6;
                                        r10_7 = r10_6;
                                        local_sp_10 = local_sp_9;
                                        r91_6 = r91_5;
                                        r91_7 = r91_5;
                                        if (*(unsigned char *)4322416UL == '\x00') {
                                            *(uint32_t *)4322408UL = (*(uint32_t *)4322408UL + 1U);
                                        }
                                        var_50 = rbp_1 + 1UL;
                                        var_51 = rbx_7 + 64UL;
                                        rbx_4 = var_51;
                                        rbx_8 = var_51;
                                        rbx_9 = var_51;
                                        if ((long)(uint64_t)((long)(var_50 << 32UL) >> (long)32UL) <= (long)(uint64_t)*(uint32_t *)4322024UL) {
                                            loop_state_var = 1U;
                                            break;
                                        }
                                        rbp_0 = (uint64_t)(uint32_t)var_50;
                                        continue;
                                    }
                                    if (*(unsigned char *)4322525UL == '\x00') {
                                        var_41 = local_sp_8 + (-8L);
                                        *(uint64_t *)var_41 = 4215472UL;
                                        var_42 = indirect_placeholder_19(r12_5, r13_7, r14_5, r8_4, rbp_0, rbx_6, r10_5, r91_4);
                                        var_43 = var_42.field_0;
                                        var_44 = var_42.field_1;
                                        var_45 = var_42.field_2;
                                        var_46 = var_42.field_3;
                                        var_47 = var_42.field_4;
                                        var_48 = var_42.field_5;
                                        var_49 = var_42.field_6;
                                        r8_5 = var_46;
                                        rbp_1 = var_47;
                                        r14_6 = var_45;
                                        r91_5 = var_49;
                                        r10_6 = var_48;
                                        r12_6 = var_43;
                                        rbx_7 = rbx_6;
                                        local_sp_9 = var_41;
                                        r13_8 = var_44;
                                    } else {
                                        *(unsigned char *)4322526UL = (unsigned char)'\x01';
                                        r8_5 = r8_3;
                                        r14_6 = r14_4;
                                        r91_5 = r91_3;
                                        r10_6 = r10_4;
                                        r12_6 = r12_4;
                                        rbx_7 = rbx_5;
                                        local_sp_9 = local_sp_7;
                                        r13_8 = r13_6;
                                    }
                                    r12_7 = r12_6;
                                    r12_8 = r12_6;
                                    r13_10 = r13_8;
                                    r14_8 = r14_6;
                                    r10_8 = r10_6;
                                    local_sp_11 = local_sp_9;
                                    r12_3 = r12_6;
                                    r13_5 = r13_8;
                                    r14_3 = r14_6;
                                    r8_2 = r8_5;
                                    r10_3 = r10_6;
                                    local_sp_6 = local_sp_9;
                                    r91_2 = r91_5;
                                    r13_9 = r13_8;
                                    r14_7 = r14_6;
                                    r10_7 = r10_6;
                                    local_sp_10 = local_sp_9;
                                    r91_6 = r91_5;
                                    r91_7 = r91_5;
                                    if (*(unsigned char *)4322416UL == '\x00') {
                                        *(uint32_t *)4322408UL = (*(uint32_t *)4322408UL + 1U);
                                    }
                                    var_50 = rbp_1 + 1UL;
                                    var_51 = rbx_7 + 64UL;
                                    rbx_4 = var_51;
                                    rbx_8 = var_51;
                                    rbx_9 = var_51;
                                    if ((long)(uint64_t)((long)(var_50 << 32UL) >> (long)32UL) <= (long)(uint64_t)*(uint32_t *)4322024UL) {
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    rbp_0 = (uint64_t)(uint32_t)var_50;
                                    continue;
                                }
                            }
                            var_26 = *(uint32_t *)(rbx_4 + 52UL);
                            *(unsigned char *)4322524UL = (unsigned char)'\x00';
                            *(uint32_t *)4322404UL = var_26;
                            var_27 = local_sp_6 + (-8L);
                            *(uint64_t *)var_27 = 4215030UL;
                            indirect_placeholder();
                            local_sp_4 = var_27;
                            if ((uint32_t)(unsigned char)var_26 == 0U) {
                                var_28 = local_sp_6 + (-16L);
                                *(uint64_t *)var_28 = 4215440UL;
                                var_29 = indirect_placeholder_18(rbx_4);
                                var_30 = var_29.field_0;
                                local_sp_4 = var_28;
                                _pre_phi64 = (uint32_t *)(var_30 + 48UL);
                                rbx_3 = var_30;
                                r13_4 = var_29.field_2;
                                r14_2 = var_29.field_3;
                                r8_0 = var_29.field_4;
                                r10_2 = var_29.field_5;
                            }
                            var_31 = *_pre_phi64;
                            var_32 = *(unsigned char *)4322400UL;
                            var_33 = (uint64_t)var_32;
                            var_34 = var_31 + (-1);
                            var_35 = (uint64_t)var_34;
                            var_36 = (uint64_t)(uint32_t)r12_3 | var_33;
                            *_pre_phi64 = var_34;
                            var_37 = helper_cc_compute_all_wrapper(var_35, 0UL, 0UL, 24U);
                            r12_7 = var_36;
                            r12_0 = var_36;
                            rbx_0 = rbx_3;
                            r13_0 = r13_4;
                            r14_0 = r14_2;
                            r10_0 = r10_2;
                            r91_0 = var_33;
                            r14_6 = r14_2;
                            r91_5 = var_33;
                            r8_1 = r8_0;
                            r10_6 = r10_2;
                            local_sp_5 = local_sp_4;
                            r12_6 = var_36;
                            r12_4 = var_36;
                            r12_5 = var_36;
                            rbx_7 = rbx_3;
                            rbx_8 = rbx_3;
                            rbx_5 = rbx_3;
                            r13_6 = r13_4;
                            r14_4 = r14_2;
                            r10_4 = r10_2;
                            r91_3 = var_33;
                            rbx_6 = rbx_3;
                            r13_7 = r13_4;
                            r14_5 = r14_2;
                            r10_5 = r10_2;
                            r91_4 = var_33;
                            r13_8 = r13_4;
                            r13_9 = r13_4;
                            r14_7 = r14_2;
                            r10_7 = r10_2;
                            r91_6 = var_33;
                            if ((uint64_t)(((unsigned char)(var_37 >> 4UL) ^ (unsigned char)var_37) & '\xc0') != 0UL) {
                                var_38 = local_sp_4 + (-8L);
                                *(uint64_t *)var_38 = 4215221UL;
                                var_39 = indirect_placeholder_17();
                                local_sp_0 = var_38;
                                r8_1 = var_39.field_1;
                                local_sp_5 = var_38;
                                local_sp_10 = var_38;
                                if ((uint64_t)(uint32_t)var_39.field_0 != 0UL) {
                                    loop_state_var = 0U;
                                    break;
                                }
                            }
                            r8_5 = r8_1;
                            local_sp_9 = local_sp_5;
                            r8_3 = r8_1;
                            local_sp_7 = local_sp_5;
                            r8_4 = r8_1;
                            local_sp_8 = local_sp_5;
                            if (*(unsigned char *)4322527UL != '\x00') {
                                r12_7 = r12_6;
                                r12_8 = r12_6;
                                r13_10 = r13_8;
                                r14_8 = r14_6;
                                r10_8 = r10_6;
                                local_sp_11 = local_sp_9;
                                r12_3 = r12_6;
                                r13_5 = r13_8;
                                r14_3 = r14_6;
                                r8_2 = r8_5;
                                r10_3 = r10_6;
                                local_sp_6 = local_sp_9;
                                r91_2 = r91_5;
                                r13_9 = r13_8;
                                r14_7 = r14_6;
                                r10_7 = r10_6;
                                local_sp_10 = local_sp_9;
                                r91_6 = r91_5;
                                r91_7 = r91_5;
                                if (*(unsigned char *)4322416UL == '\x00') {
                                    *(uint32_t *)4322408UL = (*(uint32_t *)4322408UL + 1U);
                                }
                                var_50 = rbp_1 + 1UL;
                                var_51 = rbx_7 + 64UL;
                                rbx_4 = var_51;
                                rbx_8 = var_51;
                                rbx_9 = var_51;
                                if ((long)(uint64_t)((long)(var_50 << 32UL) >> (long)32UL) <= (long)(uint64_t)*(uint32_t *)4322024UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                rbp_0 = (uint64_t)(uint32_t)var_50;
                                continue;
                            }
                            var_40 = *(uint32_t *)(rbx_3 + 16UL);
                            if (var_40 != 0U) {
                                r12_7 = r12_6;
                                r12_8 = r12_6;
                                r13_10 = r13_8;
                                r14_8 = r14_6;
                                r10_8 = r10_6;
                                local_sp_11 = local_sp_9;
                                r12_3 = r12_6;
                                r13_5 = r13_8;
                                r14_3 = r14_6;
                                r8_2 = r8_5;
                                r10_3 = r10_6;
                                local_sp_6 = local_sp_9;
                                r91_2 = r91_5;
                                r13_9 = r13_8;
                                r14_7 = r14_6;
                                r10_7 = r10_6;
                                local_sp_10 = local_sp_9;
                                r91_6 = r91_5;
                                r91_7 = r91_5;
                                if (*(unsigned char *)4322416UL == '\x00') {
                                    *(uint32_t *)4322408UL = (*(uint32_t *)4322408UL + 1U);
                                }
                                var_50 = rbp_1 + 1UL;
                                var_51 = rbx_7 + 64UL;
                                rbx_4 = var_51;
                                rbx_8 = var_51;
                                rbx_9 = var_51;
                                if ((long)(uint64_t)((long)(var_50 << 32UL) >> (long)32UL) <= (long)(uint64_t)*(uint32_t *)4322024UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                rbp_0 = (uint64_t)(uint32_t)var_50;
                                continue;
                            }
                            if (*(unsigned char *)4322525UL == '\x00') {
                                *(unsigned char *)4322526UL = (unsigned char)'\x01';
                                r8_5 = r8_3;
                                r14_6 = r14_4;
                                r91_5 = r91_3;
                                r10_6 = r10_4;
                                r12_6 = r12_4;
                                rbx_7 = rbx_5;
                                local_sp_9 = local_sp_7;
                                r13_8 = r13_6;
                            } else {
                                if ((uint64_t)(var_40 + (-3)) == 0UL) {
                                    var_41 = local_sp_8 + (-8L);
                                    *(uint64_t *)var_41 = 4215472UL;
                                    var_42 = indirect_placeholder_19(r12_5, r13_7, r14_5, r8_4, rbp_0, rbx_6, r10_5, r91_4);
                                    var_43 = var_42.field_0;
                                    var_44 = var_42.field_1;
                                    var_45 = var_42.field_2;
                                    var_46 = var_42.field_3;
                                    var_47 = var_42.field_4;
                                    var_48 = var_42.field_5;
                                    var_49 = var_42.field_6;
                                    r8_5 = var_46;
                                    rbp_1 = var_47;
                                    r14_6 = var_45;
                                    r91_5 = var_49;
                                    r10_6 = var_48;
                                    r12_6 = var_43;
                                    rbx_7 = rbx_6;
                                    local_sp_9 = var_41;
                                    r13_8 = var_44;
                                } else {
                                    if ((uint64_t)(var_40 + (-2)) != 0UL & *(unsigned char *)4322524UL == '\x00') {
                                        var_41 = local_sp_8 + (-8L);
                                        *(uint64_t *)var_41 = 4215472UL;
                                        var_42 = indirect_placeholder_19(r12_5, r13_7, r14_5, r8_4, rbp_0, rbx_6, r10_5, r91_4);
                                        var_43 = var_42.field_0;
                                        var_44 = var_42.field_1;
                                        var_45 = var_42.field_2;
                                        var_46 = var_42.field_3;
                                        var_47 = var_42.field_4;
                                        var_48 = var_42.field_5;
                                        var_49 = var_42.field_6;
                                        r8_5 = var_46;
                                        rbp_1 = var_47;
                                        r14_6 = var_45;
                                        r91_5 = var_49;
                                        r10_6 = var_48;
                                        r12_6 = var_43;
                                        rbx_7 = rbx_6;
                                        local_sp_9 = var_41;
                                        r13_8 = var_44;
                                    }
                                }
                            }
                            r12_7 = r12_6;
                            r12_8 = r12_6;
                            r13_10 = r13_8;
                            r14_8 = r14_6;
                            r10_8 = r10_6;
                            local_sp_11 = local_sp_9;
                            r12_3 = r12_6;
                            r13_5 = r13_8;
                            r14_3 = r14_6;
                            r8_2 = r8_5;
                            r10_3 = r10_6;
                            local_sp_6 = local_sp_9;
                            r91_2 = r91_5;
                            r13_9 = r13_8;
                            r14_7 = r14_6;
                            r10_7 = r10_6;
                            local_sp_10 = local_sp_9;
                            r91_6 = r91_5;
                            r91_7 = r91_5;
                            if (*(unsigned char *)4322416UL == '\x00') {
                                *(uint32_t *)4322408UL = (*(uint32_t *)4322408UL + 1U);
                            }
                            var_50 = rbp_1 + 1UL;
                            var_51 = rbx_7 + 64UL;
                            rbx_4 = var_51;
                            rbx_8 = var_51;
                            rbx_9 = var_51;
                            if ((long)(uint64_t)((long)(var_50 << 32UL) >> (long)32UL) <= (long)(uint64_t)*(uint32_t *)4322024UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            rbp_0 = (uint64_t)(uint32_t)var_50;
                            continue;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 0U:
                        {
                            if (var_32 != '\x00') {
                                r12_2 = r12_0;
                                r12_1 = r12_0;
                                rbx_1 = rbx_0;
                                r13_1 = r13_0;
                                r14_1 = r14_0;
                                r10_1 = r10_0;
                                local_sp_1 = local_sp_0;
                                r91_1 = r91_0;
                                rbx_2 = rbx_0;
                                r13_3 = r13_0;
                                local_sp_3 = local_sp_0;
                                if (*(unsigned char *)4322053UL != '\x00') {
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                r13_2 = r13_1;
                                r12_2 = r12_1;
                                local_sp_2 = local_sp_1;
                                r12_9 = r12_1;
                                rbx_2 = rbx_1;
                                rbx_10 = rbx_1;
                                r14_9 = r14_1;
                                r10_9 = r10_1;
                                r91_8 = r91_1;
                                if ((*(unsigned char *)4322424UL == '\x00') || ((uint64_t)(unsigned char)r12_1 == 0UL)) {
                                    var_56 = (uint64_t)((uint32_t)r13_1 + (-1));
                                    var_57 = local_sp_1 + (-8L);
                                    *(uint64_t *)var_57 = 4215542UL;
                                    indirect_placeholder();
                                    r13_2 = var_56;
                                    local_sp_2 = var_57;
                                }
                                var_58 = helper_cc_compute_all_wrapper(r13_2, 0UL, 0UL, 24U);
                                r13_3 = r13_2;
                                local_sp_3 = local_sp_2;
                                r13_11 = r13_2;
                                local_sp_12 = local_sp_2;
                                if ((uint64_t)(((unsigned char)(var_58 >> 4UL) ^ (unsigned char)var_58) & '\xc0') == 0UL) {
                                    continue;
                                }
                                loop_state_var = 0U;
                                switch_state_var = 1;
                                break;
                            }
                            var_52 = (uint64_t)((uint32_t)r13_9 + (-1));
                            var_53 = local_sp_10 + (-8L);
                            *(uint64_t *)var_53 = 4215503UL;
                            indirect_placeholder();
                            rbx_9 = rbx_8;
                            r12_8 = r12_7;
                            r13_10 = var_52;
                            r14_8 = r14_7;
                            r10_8 = r10_7;
                            local_sp_11 = var_53;
                            r91_7 = r91_6;
                        }
                        break;
                      case 1U:
                        {
                            if (*(unsigned char *)4322400UL == '\x00') {
                                var_52 = (uint64_t)((uint32_t)r13_9 + (-1));
                                var_53 = local_sp_10 + (-8L);
                                *(uint64_t *)var_53 = 4215503UL;
                                indirect_placeholder();
                                rbx_9 = rbx_8;
                                r12_8 = r12_7;
                                r13_10 = var_52;
                                r14_8 = r14_7;
                                r10_8 = r10_7;
                                local_sp_11 = var_53;
                                r91_7 = r91_6;
                            }
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
                var_54 = local_sp_11 + (-8L);
                *(uint64_t *)var_54 = 4215508UL;
                var_55 = indirect_placeholder_20();
                r12_0 = r12_8;
                rbx_0 = rbx_9;
                r13_0 = r13_10;
                r14_0 = r14_8;
                r10_0 = r10_8;
                local_sp_0 = var_54;
                r91_0 = r91_7;
                r12_1 = r12_8;
                rbx_1 = rbx_9;
                r13_1 = r13_10;
                r14_1 = r14_8;
                r10_1 = r10_8;
                local_sp_1 = var_54;
                r91_1 = r91_7;
                if ((uint64_t)(uint32_t)var_55.field_0 != 0UL) {
                    r12_2 = r12_0;
                    r12_1 = r12_0;
                    rbx_1 = rbx_0;
                    r13_1 = r13_0;
                    r14_1 = r14_0;
                    r10_1 = r10_0;
                    local_sp_1 = local_sp_0;
                    r91_1 = r91_0;
                    rbx_2 = rbx_0;
                    r13_3 = r13_0;
                    local_sp_3 = local_sp_0;
                    if (*(unsigned char *)4322053UL != '\x00') {
                        loop_state_var = 0U;
                        break;
                    }
                }
                r13_2 = r13_1;
                r12_2 = r12_1;
                local_sp_2 = local_sp_1;
                r12_9 = r12_1;
                rbx_2 = rbx_1;
                rbx_10 = rbx_1;
                r14_9 = r14_1;
                r10_9 = r10_1;
                r91_8 = r91_1;
                if ((*(unsigned char *)4322424UL == '\x00') || ((uint64_t)(unsigned char)r12_1 == 0UL)) {
                    var_56 = (uint64_t)((uint32_t)r13_1 + (-1));
                    var_57 = local_sp_1 + (-8L);
                    *(uint64_t *)var_57 = 4215542UL;
                    indirect_placeholder();
                    r13_2 = var_56;
                    local_sp_2 = var_57;
                }
                var_58 = helper_cc_compute_all_wrapper(r13_2, 0UL, 0UL, 24U);
                r13_3 = r13_2;
                local_sp_3 = local_sp_2;
                r13_11 = r13_2;
                local_sp_12 = local_sp_2;
                if ((uint64_t)(((unsigned char)(var_58 >> 4UL) ^ (unsigned char)var_58) & '\xc0') == 0UL) {
                    continue;
                }
                loop_state_var = 0U;
                break;
            }
        r12_11 = r12_2;
        r12_10 = r12_2;
        rbx_11 = rbx_2;
        local_sp_13 = local_sp_3;
        rbx_12 = rbx_2;
        r13_12 = r13_3;
        local_sp_14 = local_sp_3;
        var_59 = (uint64_t)*(uint32_t *)4322024UL;
        var_60 = *(uint64_t *)4322568UL;
        var_61 = helper_cc_compute_all_wrapper(var_59, 0UL, 0UL, 24U);
        rax_0 = var_60;
        rdx_0 = var_59;
        if ((uint64_t)(uint32_t)r13_3 != 0UL & (uint64_t)(((unsigned char)(var_61 >> 4UL) ^ (unsigned char)var_61) & '\xc0') != 0UL) {
            var_62 = ((((rdx_0 << 6UL) + 274877906880UL) & 274877906880UL) + 64UL) + rax_0;
            rax_1 = rax_0;
            r12_11 = r12_10;
            rbx_12 = rbx_11;
            local_sp_14 = local_sp_13;
            var_63 = rax_1 + 64UL;
            rax_1 = var_63;
            do {
                if (*(uint32_t *)(rax_1 + 16UL) == 0U) {
                    *(unsigned char *)(rax_1 + 57UL) = (unsigned char)'\x01';
                }
                var_63 = rax_1 + 64UL;
                rax_1 = var_63;
            } while (var_63 != var_62);
        }
    }
}
