typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_24_ret_type;
struct indirect_placeholder_24_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t init_r15(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern struct indirect_placeholder_24_ret_type indirect_placeholder_24(uint64_t param_0);
uint64_t bb_process_long_option(uint64_t r8, uint64_t rdx, uint64_t rdi, uint64_t rcx, uint64_t r9, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t *var_10;
    unsigned char var_11;
    uint64_t rbp_2;
    uint64_t rbp_3;
    uint64_t local_sp_0;
    uint64_t local_sp_10;
    uint64_t r13_0;
    uint64_t local_sp_1;
    uint64_t var_41;
    uint64_t rax_3;
    uint64_t var_57;
    uint64_t var_42;
    uint64_t local_sp_4;
    uint64_t rax_4;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t local_sp_2;
    uint64_t r15_2;
    uint32_t var_25;
    uint64_t rax_0;
    uint32_t var_26;
    uint64_t var_27;
    uint32_t var_28;
    uint64_t var_29;
    uint32_t *var_30;
    uint64_t r14_0;
    uint64_t rax_1;
    uint64_t r15_0;
    uint64_t rbp_1;
    uint64_t var_31;
    uint64_t var_32;
    struct indirect_placeholder_24_ret_type var_33;
    uint64_t var_34;
    uint64_t local_sp_3;
    uint64_t rbx_3;
    uint64_t rax_2;
    uint64_t rbx_1;
    uint64_t r15_1;
    uint64_t var_37;
    uint64_t var_66;
    uint64_t var_24;
    uint64_t local_sp_11;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t local_sp_5;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint32_t *var_48;
    uint64_t var_43;
    uint64_t r12_1;
    uint64_t local_sp_6;
    uint64_t rbx_4;
    uint64_t r12_0;
    uint64_t rbx_2;
    uint64_t storemerge;
    uint64_t rbp_0;
    uint64_t var_49;
    uint32_t *var_50;
    uint64_t *var_51;
    uint64_t var_52;
    uint32_t var_53;
    uint32_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_62;
    uint64_t *_pre_phi191;
    uint64_t local_sp_7;
    uint64_t var_61;
    uint64_t var_60;
    uint64_t var_63;
    uint64_t var_64;
    uint32_t var_65;
    uint64_t *_pre_phi195;
    uint64_t var_18;
    uint64_t local_sp_14;
    uint64_t local_sp_9;
    uint64_t var_19;
    uint64_t var_20;
    uint32_t var_21;
    uint32_t var_22;
    uint64_t var_23;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t local_sp_12;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t local_sp_13;
    uint64_t var_71;
    uint32_t *var_72;
    uint64_t var_70;
    bool var_15;
    bool var_16;
    uint64_t var_17;
    unsigned int loop_state_var;
    var_13 = *var_10;
    var_14 = *(uint64_t *)var_13;
    *(uint64_t *)(var_0 + (-120L)) = var_14;
    rbp_0 = rbp_3;
    rax_4 = var_14;
    rbx_4 = var_13;
    if (var_14 == 0UL) {
        rax_3 = 4294967295UL;
        local_sp_12 = local_sp_11;
        if (*(uint32_t *)(local_sp_11 + 12UL) != 0U) {
            var_67 = local_sp_11 + 144UL;
            var_68 = *(uint64_t *)(*(uint64_t *)var_67 + 32UL);
            var_69 = local_sp_11 + (-8L);
            *(uint64_t *)var_69 = 4237811UL;
            indirect_placeholder_1();
            local_sp_12 = var_69;
            if (*(unsigned char *)(*(uint64_t *)(((uint64_t)**(uint32_t **)var_67 << 3UL) + *(uint64_t *)(local_sp_11 + 32UL)) + 1UL) != '-' & var_68 == 0UL) {
                return rax_3;
            }
        }
        local_sp_13 = local_sp_12;
        if (*(uint32_t *)(local_sp_12 + 152UL) != 0U) {
            var_70 = local_sp_12 + (-8L);
            *(uint64_t *)var_70 = 4237954UL;
            indirect_placeholder_1();
            local_sp_13 = var_70;
        }
        var_71 = *(uint64_t *)(local_sp_13 + 144UL);
        var_72 = (uint32_t *)var_71;
        *var_72 = (*var_72 + 1U);
        *(uint64_t *)(var_71 + 32UL) = 0UL;
        *(uint32_t *)(var_71 + 8UL) = 0U;
        return 63UL;
    }
    var_15 = ((uint64_t)(uint32_t)var_14 == 0UL);
    var_16 = (var_14 == storemerge);
    while (1U)
        {
            var_17 = local_sp_14 + (-8L);
            *(uint64_t *)var_17 = 4237305UL;
            indirect_placeholder_1();
            r12_0 = r12_1;
            rbx_2 = rbx_4;
            local_sp_9 = var_17;
            if (!var_15) {
                var_18 = local_sp_14 + (-16L);
                *(uint64_t *)var_18 = 4237317UL;
                indirect_placeholder_1();
                local_sp_6 = var_18;
                local_sp_9 = var_18;
                if (!var_16) {
                    loop_state_var = 1U;
                    break;
                }
            }
            var_19 = rbx_4 + 32UL;
            var_20 = *(uint64_t *)var_19;
            var_21 = (uint32_t)r12_1;
            var_22 = var_21 + 1U;
            local_sp_10 = local_sp_9;
            local_sp_14 = local_sp_9;
            r12_1 = (uint64_t)var_22;
            rbx_4 = var_19;
            if (var_20 == 0UL) {
                continue;
            }
            var_23 = *(uint64_t *)local_sp_9;
            *(uint32_t *)(local_sp_9 + 68UL) = var_22;
            *(uint32_t *)(local_sp_9 + 60UL) = var_21;
            *(uint32_t *)(local_sp_9 + 56UL) = 4294967295U;
            *(uint32_t *)(local_sp_9 + 48UL) = 0U;
            *(uint32_t *)(local_sp_9 + 64UL) = 0U;
            *(uint64_t *)(local_sp_9 + 16UL) = rbp_3;
            rbp_1 = var_23;
            while (1U)
                {
                    var_24 = local_sp_10 + (-8L);
                    *(uint64_t *)var_24 = 4237421UL;
                    indirect_placeholder_1();
                    local_sp_2 = var_24;
                    r15_0 = r15_2;
                    local_sp_3 = var_24;
                    rax_2 = rax_4;
                    rbx_1 = rbx_3;
                    r15_1 = r15_2;
                    if ((uint64_t)(uint32_t)rax_4 == 0UL) {
                        var_37 = rbp_1 + 32UL;
                        local_sp_10 = local_sp_3;
                        r13_0 = r15_1;
                        local_sp_4 = local_sp_3;
                        rax_4 = rax_2;
                        r15_2 = r15_1;
                        rbp_1 = var_37;
                        rbx_3 = rbx_1;
                        local_sp_11 = local_sp_3;
                        local_sp_6 = local_sp_3;
                        rbx_2 = rbx_1;
                        if (*(uint64_t *)var_37 == 0UL) {
                            break;
                        }
                        r14_0 = r14_0 + 1UL;
                        continue;
                    }
                    rbx_1 = rbp_1;
                    if (rbx_3 == 0UL) {
                        *(uint32_t *)(local_sp_10 + 48UL) = (uint32_t)r14_0;
                    } else {
                        var_25 = *(uint32_t *)(local_sp_10 + 4UL);
                        rax_0 = (uint64_t)var_25;
                        if (var_25 != 0U) {
                            var_26 = *(uint32_t *)(rbp_1 + 8UL);
                            rax_0 = (uint64_t)var_26;
                            var_27 = *(uint64_t *)(rbp_1 + 16UL);
                            rax_0 = var_27;
                            var_28 = *(uint32_t *)(rbp_1 + 24UL);
                            var_29 = (uint64_t)var_28;
                            rax_0 = var_29;
                            rax_2 = var_29;
                            if ((uint64_t)(*(uint32_t *)(rbx_3 + 8UL) - var_26) != 0UL & *(uint64_t *)(rbx_3 + 16UL) != var_27 & (uint64_t)(*(uint32_t *)(rbx_3 + 24UL) - var_28) != 0UL) {
                                var_37 = rbp_1 + 32UL;
                                local_sp_10 = local_sp_3;
                                r13_0 = r15_1;
                                local_sp_4 = local_sp_3;
                                rax_4 = rax_2;
                                r15_2 = r15_1;
                                rbp_1 = var_37;
                                rbx_3 = rbx_1;
                                local_sp_11 = local_sp_3;
                                local_sp_6 = local_sp_3;
                                rbx_2 = rbx_1;
                                if (*(uint64_t *)var_37 == 0UL) {
                                    break;
                                }
                                r14_0 = r14_0 + 1UL;
                                continue;
                            }
                        }
                        var_30 = (uint32_t *)(local_sp_10 + 40UL);
                        rax_1 = rax_0;
                        rax_2 = rax_0;
                        if (*var_30 != 0U) {
                            rax_2 = 0UL;
                            r15_1 = 0UL;
                            if (*(uint32_t *)(local_sp_10 + 144UL) == 0U) {
                                *var_30 = 1U;
                                if (r15_2 == 0UL) {
                                    *(unsigned char *)(r14_0 + r15_2) = (unsigned char)'\x01';
                                    r15_1 = r15_2;
                                }
                            } else {
                                if (r15_2 == 0UL) {
                                    *(unsigned char *)(r14_0 + r15_0) = (unsigned char)'\x01';
                                    local_sp_3 = local_sp_2;
                                    rax_2 = rax_1;
                                    r15_1 = r15_0;
                                } else {
                                    var_31 = (uint64_t)*(uint32_t *)(local_sp_10 + 60UL);
                                    *(uint64_t *)(local_sp_10 + 64UL) = var_31;
                                    var_32 = local_sp_10 + (-16L);
                                    *(uint64_t *)var_32 = 4238202UL;
                                    var_33 = indirect_placeholder_24(var_31);
                                    var_34 = var_33.field_0;
                                    r15_0 = var_34;
                                    local_sp_3 = var_32;
                                    if (var_34 == 0UL) {
                                        *(uint32_t *)(local_sp_10 + 32UL) = 1U;
                                    } else {
                                        var_35 = local_sp_10 + (-24L);
                                        *(uint64_t *)var_35 = 4238229UL;
                                        indirect_placeholder_1();
                                        var_36 = (uint64_t)*(uint32_t *)(local_sp_10 + 32UL);
                                        *var_30 = 1U;
                                        *(unsigned char *)(var_34 + var_36) = (unsigned char)'\x01';
                                        local_sp_2 = var_35;
                                        rax_1 = var_36;
                                        *(unsigned char *)(r14_0 + r15_0) = (unsigned char)'\x01';
                                        local_sp_3 = local_sp_2;
                                        rax_2 = rax_1;
                                        r15_1 = r15_0;
                                    }
                                }
                            }
                        }
                    }
                }
            rbp_0 = *(uint64_t *)(local_sp_3 + 16UL);
            if (r15_1 != 0UL) {
                loop_state_var = 2U;
                break;
            }
            if (*(uint32_t *)(local_sp_3 + 48UL) != 0U) {
                loop_state_var = 2U;
                break;
            }
            if (rbx_1 != 0UL) {
                loop_state_var = 0U;
                break;
            }
            r12_0 = (uint64_t)*(uint32_t *)(local_sp_3 + 56UL);
            loop_state_var = 1U;
            break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            break;
        }
        break;
      case 2U:
      case 1U:
        {
            switch (loop_state_var) {
              case 1U:
                {
                    var_49 = local_sp_6 + 144UL;
                    var_50 = *(uint32_t **)var_49;
                    var_51 = (uint64_t *)var_49;
                    var_52 = *var_51;
                    var_53 = *(uint32_t *)(rbx_2 + 8UL);
                    var_54 = *var_50;
                    var_55 = (uint64_t)var_54;
                    *(uint64_t *)(var_52 + 32UL) = 0UL;
                    var_56 = var_55 + 1UL;
                    *(uint32_t *)var_52 = (uint32_t)var_56;
                    _pre_phi191 = var_51;
                    local_sp_7 = local_sp_6;
                    _pre_phi195 = var_51;
                    if (*(unsigned char *)rbp_0 == '\x00') {
                        if (var_53 != 1U) {
                            var_58 = (uint64_t)*(uint32_t *)(local_sp_6 + 52UL);
                            var_59 = var_56 << 32UL;
                            if ((long)var_59 >= (long)(var_58 << 32UL)) {
                                if (*(uint32_t *)(local_sp_6 + 152UL) == 0U) {
                                    var_60 = local_sp_6 + (-8L);
                                    *(uint64_t *)var_60 = 4238552UL;
                                    indirect_placeholder_1();
                                    _pre_phi191 = (uint64_t *)(var_60 + 144UL);
                                    local_sp_7 = var_60;
                                }
                                *(uint32_t *)(*_pre_phi191 + 8UL) = *(uint32_t *)(rbx_2 + 24UL);
                                var_61 = (**(unsigned char **)(local_sp_7 + 40UL) == ':') ? 58UL : 63UL;
                                rax_3 = var_61;
                                return rax_3;
                            }
                            var_62 = *var_51;
                            *(uint32_t *)var_62 = (var_54 + 2U);
                            *(uint64_t *)(var_62 + 16UL) = *(uint64_t *)((uint64_t)((long)var_59 >> (long)29UL) + *(uint64_t *)(local_sp_6 + 32UL));
                        }
                    } else {
                        if (var_53 != 0U) {
                            if (*(uint32_t *)(local_sp_6 + 152UL) == 0U) {
                                var_57 = local_sp_6 + (-8L);
                                *(uint64_t *)var_57 = 4238290UL;
                                indirect_placeholder_1();
                                _pre_phi195 = (uint64_t *)(var_57 + 144UL);
                            }
                            *(uint32_t *)(*_pre_phi195 + 8UL) = *(uint32_t *)(rbx_2 + 24UL);
                            return rax_3;
                        }
                        *(uint64_t *)(var_52 + 16UL) = (rbp_0 + 1UL);
                    }
                    var_63 = *(uint64_t *)(local_sp_6 + 24UL);
                    if (var_63 == 0UL) {
                        *(uint32_t *)var_63 = (uint32_t)r12_0;
                    }
                    var_64 = *(uint64_t *)(rbx_2 + 16UL);
                    var_65 = *(uint32_t *)(rbx_2 + 24UL);
                    var_66 = (uint64_t)var_65;
                    rax_3 = var_66;
                    if (var_64 == 0UL) {
                        *(uint32_t *)var_64 = var_65;
                        rax_3 = 0UL;
                    }
                }
                break;
              case 2U:
                {
                    if (*(uint32_t *)(local_sp_3 + 152UL) != 0U) {
                        if (*(uint32_t *)(local_sp_3 + 48UL) == 0U) {
                            var_38 = local_sp_3 + (-8L);
                            *(uint64_t *)var_38 = 4238031UL;
                            indirect_placeholder_1();
                            local_sp_4 = var_38;
                        } else {
                            var_39 = r15_1 + (uint64_t)*(uint32_t *)(local_sp_3 + 60UL);
                            *(uint64_t *)(local_sp_3 + (-8L)) = 4238386UL;
                            indirect_placeholder_1();
                            var_40 = local_sp_3 + (-16L);
                            *(uint64_t *)var_40 = 4238429UL;
                            indirect_placeholder_1();
                            local_sp_0 = var_40;
                            while (1U)
                                {
                                    local_sp_1 = local_sp_0;
                                    if (*(unsigned char *)r13_0 == '\x00') {
                                        var_41 = local_sp_0 + (-8L);
                                        *(uint64_t *)var_41 = 4238486UL;
                                        indirect_placeholder_1();
                                        local_sp_1 = var_41;
                                    }
                                    local_sp_0 = local_sp_1;
                                    if (var_39 == r13_0) {
                                        break;
                                    }
                                    r13_0 = r13_0 + 1UL;
                                    continue;
                                }
                            *(uint64_t *)(local_sp_1 + (-8L)) = 4238501UL;
                            indirect_placeholder_1();
                            var_42 = local_sp_1 + (-16L);
                            *(uint64_t *)var_42 = 4238509UL;
                            indirect_placeholder_1();
                            local_sp_4 = var_42;
                        }
                    }
                    local_sp_5 = local_sp_4;
                    if (*(uint32_t *)(local_sp_4 + 64UL) != 0U) {
                        var_43 = local_sp_4 + (-8L);
                        *(uint64_t *)var_43 = 4238303UL;
                        indirect_placeholder_1();
                        local_sp_5 = var_43;
                    }
                    var_44 = *(uint64_t *)(local_sp_5 + 144UL);
                    var_45 = *(uint64_t *)(var_44 + 32UL);
                    *(uint64_t *)(local_sp_5 + (-8L)) = 4238063UL;
                    indirect_placeholder_1();
                    var_46 = var_45 + var_44;
                    var_47 = *(uint64_t *)(local_sp_5 + 136UL);
                    var_48 = (uint32_t *)var_47;
                    *var_48 = (*var_48 + 1U);
                    *(uint64_t *)(var_47 + 32UL) = var_46;
                    *(uint32_t *)(var_47 + 8UL) = 0U;
                }
                break;
            }
            return rax_3;
        }
        break;
    }
}
