typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_77_ret_type;
struct indirect_placeholder_78_ret_type;
struct indirect_placeholder_79_ret_type;
struct indirect_placeholder_77_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_78_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_79_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern struct indirect_placeholder_77_ret_type indirect_placeholder_77(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_78_ret_type indirect_placeholder_78(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_79_ret_type indirect_placeholder_79(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
void bb_validate_tab_stops(uint64_t r8, uint64_t rdi, uint64_t rcx, uint64_t rsi, uint64_t r9) {
    struct indirect_placeholder_77_ret_type var_4;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t rcx3_1;
    uint64_t rax_0;
    uint64_t rdx_0;
    uint64_t var_2;
    uint64_t local_sp_0;
    uint64_t rcx3_2;
    uint64_t r95_1;
    uint64_t var_5;
    uint64_t var_3;
    uint64_t rax_1;
    uint64_t r81_0;
    uint64_t rcx3_0;
    uint64_t r95_0;
    uint64_t var_6;
    struct indirect_placeholder_78_ret_type var_7;
    uint64_t local_sp_1;
    uint64_t r81_1;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = var_0 + (-8L);
    rcx3_1 = rcx;
    rax_0 = 0UL;
    rdx_0 = 0UL;
    local_sp_0 = var_1;
    r95_1 = r9;
    rax_1 = 0UL;
    r81_0 = r8;
    r95_0 = r9;
    local_sp_1 = var_1;
    r81_1 = r8;
    if (rsi != 0UL) {
        rcx3_2 = rcx3_1;
        if (*(uint64_t *)4281224UL == 0UL) {
            return;
        }
        if (*(uint64_t *)4281232UL != 0UL) {
            return;
        }
        *(uint64_t *)(local_sp_1 + (-8L)) = 4205834UL;
        indirect_placeholder_79(0UL, r81_1, 4256968UL, 1UL, rcx3_2, 0UL, r95_1);
        abort();
    }
    while (1U)
        {
            var_2 = *(uint64_t *)((rdx_0 << 3UL) + rdi);
            rax_0 = var_2;
            rcx3_0 = rax_0;
            rcx3_1 = rax_0;
            if (var_2 != 0UL) {
                loop_state_var = 1U;
                break;
            }
            if (var_2 > rax_0) {
                var_5 = rdx_0 + 1UL;
                rdx_0 = var_5;
                if (var_5 == rsi) {
                    continue;
                }
                loop_state_var = 0U;
                break;
            }
            var_3 = var_0 + (-16L);
            *(uint64_t *)var_3 = 4205798UL;
            var_4 = indirect_placeholder_77(0UL, r8, 4257610UL, 1UL, rax_0, 0UL, r9);
            local_sp_0 = var_3;
            rax_1 = var_4.field_0;
            r81_0 = var_4.field_1;
            rcx3_0 = var_4.field_2;
            r95_0 = var_4.field_3;
            loop_state_var = 1U;
            break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            rcx3_2 = rcx3_1;
            if (*(uint64_t *)4281224UL == 0UL) {
                return;
            }
            if (*(uint64_t *)4281232UL == 0UL) {
                return;
            }
        }
        break;
      case 1U:
        {
            var_6 = local_sp_0 + (-8L);
            *(uint64_t *)var_6 = 4205815UL;
            var_7 = indirect_placeholder_78(rax_1, r81_0, 4257589UL, 1UL, rcx3_0, 0UL, r95_0);
            local_sp_1 = var_6;
            r81_1 = var_7.field_1;
            rcx3_2 = var_7.field_2;
            r95_1 = var_7.field_3;
        }
        break;
    }
}
