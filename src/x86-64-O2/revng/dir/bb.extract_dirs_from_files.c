typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder_1(void);
extern uint64_t init_r12(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_22(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder(uint64_t param_0);
extern uint64_t indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_2(uint64_t param_0);
void bb_extract_dirs_from_files(uint64_t rdi, uint64_t rsi) {
    uint64_t rbx_0_in;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    bool var_8;
    uint64_t local_sp_3;
    uint64_t local_sp_2;
    uint64_t local_sp_4_be;
    uint64_t local_sp_4;
    uint64_t var_16;
    uint64_t local_sp_1;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t local_sp_0;
    uint64_t var_24;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t rax_0;
    uint64_t rdx_0;
    uint64_t var_28;
    uint32_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t rbx_0;
    uint64_t var_11;
    uint32_t *var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r12();
    var_2 = init_rbx();
    var_3 = init_r13();
    var_4 = init_r14();
    var_5 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_4;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    var_6 = (uint64_t)(unsigned char)rsi;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    var_7 = var_0 + (-40L);
    *(uint64_t *)var_7 = var_2;
    var_8 = (rdi == 0UL);
    rdx_0 = 0UL;
    local_sp_3 = var_7;
    if (!var_8 && *(uint64_t *)4401096UL == 0UL) {
        var_9 = var_0 + (-48L);
        *(uint64_t *)var_9 = 4220634UL;
        indirect_placeholder_22(0UL, 0UL, rdi);
        local_sp_3 = var_9;
    }
    var_10 = *(uint64_t *)4401072UL;
    local_sp_4 = local_sp_3;
    rbx_0_in = var_10;
    if (var_10 == 0UL) {
        *(uint64_t *)4401072UL = 0UL;
        return;
    }
    while (1U)
        {
            rbx_0 = rbx_0_in + (-1L);
            var_11 = *(uint64_t *)((rbx_0 << 3UL) + *(uint64_t *)4401056UL);
            var_12 = (uint32_t *)(var_11 + 168UL);
            var_13 = (uint64_t)*var_12;
            var_14 = local_sp_4 + (-8L);
            *(uint64_t *)var_14 = 4220760UL;
            var_15 = indirect_placeholder(var_13);
            local_sp_1 = var_14;
            local_sp_2 = var_14;
            rbx_0_in = rbx_0;
            if ((uint64_t)(unsigned char)var_15 == 0UL) {
                local_sp_4_be = local_sp_2;
                if (rbx_0 == 0UL) {
                    break;
                }
            }
            var_16 = *(uint64_t *)var_11;
            if (var_8) {
                var_22 = *(uint64_t *)(var_11 + 8UL);
                var_23 = local_sp_1 + (-8L);
                *(uint64_t *)var_23 = 4220788UL;
                indirect_placeholder_22(var_6, var_16, var_22);
                local_sp_0 = var_23;
                local_sp_2 = var_23;
                if (*var_12 != 9U) {
                    local_sp_4_be = local_sp_2;
                    if (rbx_0 == 0UL) {
                        break;
                    }
                    local_sp_4 = local_sp_4_be;
                    continue;
                }
                var_24 = local_sp_0 + (-8L);
                *(uint64_t *)var_24 = 4220809UL;
                indirect_placeholder_2(var_11);
                local_sp_4_be = var_24;
                if (rbx_0 == 0UL) {
                    break;
                }
                local_sp_4 = local_sp_4_be;
                continue;
            }
            var_17 = local_sp_4 + (-16L);
            *(uint64_t *)var_17 = 4220664UL;
            var_18 = indirect_placeholder(var_16);
            local_sp_1 = var_17;
            local_sp_2 = var_17;
            if ((uint64_t)(unsigned char)var_18 != 0UL) {
                local_sp_4_be = local_sp_2;
                if (rbx_0 == 0UL) {
                    break;
                }
                local_sp_4 = local_sp_4_be;
                continue;
            }
            if (*(unsigned char *)var_16 != '/') {
                *(uint64_t *)(local_sp_4 + (-24L)) = 4220687UL;
                var_19 = indirect_placeholder_8(0UL, rdi, var_16);
                var_20 = *(uint64_t *)(var_11 + 8UL);
                *(uint64_t *)(local_sp_4 + (-32L)) = 4220705UL;
                indirect_placeholder_22(var_6, var_19, var_20);
                var_21 = local_sp_4 + (-40L);
                *(uint64_t *)var_21 = 4220713UL;
                indirect_placeholder_1();
                local_sp_0 = var_21;
                local_sp_2 = var_21;
                if (*var_12 != 9U) {
                    local_sp_4_be = local_sp_2;
                    if (rbx_0 == 0UL) {
                        break;
                    }
                    local_sp_4 = local_sp_4_be;
                    continue;
                }
                var_24 = local_sp_0 + (-8L);
                *(uint64_t *)var_24 = 4220809UL;
                indirect_placeholder_2(var_11);
                local_sp_4_be = var_24;
                if (rbx_0 == 0UL) {
                    break;
                }
                local_sp_4 = local_sp_4_be;
                continue;
            }
        }
    var_25 = *(uint64_t *)4401072UL;
    if (var_25 == 0UL) {
        return;
    }
    var_26 = *(uint64_t *)4401056UL;
    var_27 = (var_25 << 3UL) + var_26;
    rax_0 = var_26;
    var_28 = *(uint64_t *)rax_0;
    var_29 = *(uint32_t *)(var_28 + 168UL);
    *(uint64_t *)((rdx_0 << 3UL) + var_26) = var_28;
    var_30 = (var_29 != 9U);
    var_31 = rax_0 + 8UL;
    var_32 = rdx_0 + var_30;
    rax_0 = var_31;
    rdx_0 = var_32;
    do {
        var_28 = *(uint64_t *)rax_0;
        var_29 = *(uint32_t *)(var_28 + 168UL);
        *(uint64_t *)((rdx_0 << 3UL) + var_26) = var_28;
        var_30 = (var_29 != 9U);
        var_31 = rax_0 + 8UL;
        var_32 = rdx_0 + var_30;
        rax_0 = var_31;
        rdx_0 = var_32;
    } while (var_27 != var_31);
    *(uint64_t *)4401072UL = var_32;
    return;
}
