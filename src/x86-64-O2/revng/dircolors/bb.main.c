typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_24_ret_type;
struct indirect_placeholder_25_ret_type;
struct indirect_placeholder_26_ret_type;
struct indirect_placeholder_27_ret_type;
struct indirect_placeholder_24_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_25_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_26_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_27_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t indirect_placeholder_6(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t init_r12(void);
extern uint64_t init_r15(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
extern void indirect_placeholder_22(uint64_t param_0);
extern uint32_t init_state_0x82fc(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_11(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern void indirect_placeholder_16(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_23(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_24_ret_type indirect_placeholder_24(void);
extern struct indirect_placeholder_25_ret_type indirect_placeholder_25(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_26_ret_type indirect_placeholder_26(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_27_ret_type indirect_placeholder_27(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_27_ret_type var_13;
    uint64_t var_47;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint32_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t r12_0_ph;
    uint64_t local_sp_7;
    uint64_t r13_3_ph;
    uint64_t local_sp_7_ph;
    uint64_t r13_3;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t rax_0;
    uint64_t rbp_1;
    uint64_t local_sp_0;
    uint64_t storemerge_in;
    uint64_t var_43;
    uint64_t rax_1;
    uint64_t local_sp_1;
    uint64_t local_sp_2;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t local_sp_5;
    uint64_t var_30;
    uint64_t rdi1_0;
    uint64_t rcx_0;
    uint64_t rcx_1;
    unsigned char var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_16;
    uint64_t r13_3_be;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t rbp_0;
    uint64_t local_sp_4;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    bool var_28;
    bool var_29;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t local_sp_6;
    uint64_t var_36;
    struct indirect_placeholder_24_ret_type var_37;
    uint64_t var_53;
    struct indirect_placeholder_25_ret_type var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    struct indirect_placeholder_26_ret_type var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_12;
    uint64_t var_14;
    uint32_t var_15;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r12();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_r13();
    var_5 = init_r14();
    var_6 = init_rbp();
    var_7 = init_cc_src2();
    var_8 = init_state_0x82fc();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_1;
    *(uint64_t *)(var_0 + (-40L)) = var_6;
    var_9 = (uint64_t)(uint32_t)rdi;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_10 = *(uint64_t *)rsi;
    *(uint64_t *)(var_0 + (-64L)) = 4203940UL;
    indirect_placeholder_22(var_10);
    *(uint64_t *)(var_0 + (-72L)) = 4203955UL;
    indirect_placeholder_1();
    var_11 = var_0 + (-80L);
    *(uint64_t *)var_11 = 4203965UL;
    indirect_placeholder_1();
    r12_0_ph = 0UL;
    r13_3_ph = 2UL;
    local_sp_7_ph = var_11;
    rbp_1 = 4266688UL;
    rcx_0 = 18446744073709551615UL;
    rcx_1 = 0UL;
    r13_3_be = 0UL;
    rbp_0 = var_9;
    while (1U)
        {
            r12_0_ph = 1UL;
            r13_3 = r13_3_ph;
            local_sp_7 = local_sp_7_ph;
            while (1U)
                {
                    var_12 = local_sp_7 + (-8L);
                    *(uint64_t *)var_12 = 4203991UL;
                    var_13 = indirect_placeholder_27(0UL, 4264241UL, var_9, 4265792UL, rsi);
                    var_14 = var_13.field_0;
                    var_15 = (uint32_t)var_14;
                    local_sp_7 = var_12;
                    r13_3_ph = r13_3;
                    local_sp_7_ph = var_12;
                    local_sp_5 = var_12;
                    local_sp_4 = var_12;
                    local_sp_6 = var_12;
                    if ((uint64_t)(var_15 + 1U) == 0UL) {
                        var_16 = var_14 + (-98L);
                        if ((uint64_t)(uint32_t)var_16 == 0UL) {
                            r13_3 = r13_3_be;
                            continue;
                        }
                        var_17 = helper_cc_compute_all_wrapper(var_16, 98UL, var_7, 16U);
                        r13_3_be = 1UL;
                        if ((uint64_t)(((unsigned char)(var_17 >> 4UL) ^ (unsigned char)var_17) & '\xc0') == 0UL) {
                            if ((uint64_t)(var_15 + (-99)) != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                        }
                        if ((uint64_t)(var_15 + 131U) == 0UL) {
                            if ((uint64_t)(var_15 + 130U) != 0UL) {
                                loop_state_var = 2U;
                                break;
                            }
                            *(uint64_t *)(local_sp_7 + (-16L)) = 4204118UL;
                            indirect_placeholder_16(r12_0_ph, var_9, 0UL);
                            abort();
                        }
                        var_18 = *(uint64_t *)4292160UL;
                        var_19 = *(uint64_t *)4293280UL;
                        *(uint64_t *)(local_sp_7 + (-16L)) = 4204071UL;
                        indirect_placeholder_23(0UL, 4264226UL, 4264103UL, var_18, var_19, 4263998UL, 0UL);
                        var_20 = local_sp_7 + (-24L);
                        *(uint64_t *)var_20 = 4204078UL;
                        indirect_placeholder_1();
                        local_sp_4 = var_20;
                        loop_state_var = 2U;
                        break;
                    }
                    var_21 = var_13.field_1;
                    var_22 = var_13.field_2;
                    var_23 = var_13.field_3;
                    var_24 = (uint64_t)*(uint32_t *)4293404UL;
                    var_25 = rdi - var_24;
                    var_26 = (uint64_t)(uint32_t)var_25;
                    var_27 = (var_24 << 3UL) + rsi;
                    var_28 = ((uint64_t)((uint32_t)r13_3 + (-2)) == 0UL);
                    var_29 = (r12_0_ph == 0UL);
                    rbp_0 = var_26;
                    if (!(var_28 || var_29)) {
                        var_30 = local_sp_7 + (-16L);
                        *(uint64_t *)var_30 = 4204638UL;
                        indirect_placeholder(0UL, var_21, 4265536UL, 0UL, var_22, 0UL, var_23);
                        local_sp_4 = var_30;
                        loop_state_var = 2U;
                        break;
                    }
                    if ((long)((r12_0_ph << 32UL) ^ 4294967296UL) >= (long)(var_25 << 32UL)) {
                        if (!var_29) {
                            var_31 = (uint64_t)var_8;
                            loop_state_var = 4U;
                            break;
                        }
                        if (!var_28) {
                            loop_state_var = 3U;
                            break;
                        }
                        var_36 = local_sp_7 + (-16L);
                        *(uint64_t *)var_36 = 4204513UL;
                        var_37 = indirect_placeholder_24();
                        local_sp_6 = var_36;
                        if ((uint64_t)((uint32_t)var_37.field_0 + (-2)) != 0UL) {
                            loop_state_var = 3U;
                            break;
                        }
                        var_44 = var_37.field_2;
                        var_45 = var_37.field_1;
                        var_46 = local_sp_7 + (-24L);
                        *(uint64_t *)var_46 = 4204544UL;
                        var_47 = indirect_placeholder(0UL, var_45, 4265640UL, 1UL, var_44, 0UL, var_23);
                        rax_0 = var_47;
                        local_sp_1 = var_46;
                        loop_state_var = 1U;
                        break;
                    }
                    if (var_29) {
                        var_59 = *(uint64_t *)(var_27 + 8UL);
                        *(uint64_t *)(local_sp_7 + (-16L)) = 4204649UL;
                        var_60 = indirect_placeholder_26(0UL, var_27, var_3, r13_3, var_5, var_26, var_59);
                        var_61 = var_60.field_0;
                        var_62 = var_60.field_1;
                        var_63 = var_60.field_6;
                        var_64 = local_sp_7 + (-24L);
                        *(uint64_t *)var_64 = 4204668UL;
                        indirect_placeholder(0UL, var_62, 4264245UL, 0UL, var_61, 0UL, var_63);
                        local_sp_4 = var_64;
                        loop_state_var = 2U;
                        break;
                    }
                    var_53 = *(uint64_t *)var_27;
                    *(uint64_t *)(local_sp_7 + (-16L)) = 4204569UL;
                    var_54 = indirect_placeholder_25(r12_0_ph, var_27, var_3, r13_3, var_5, var_26, var_53);
                    var_55 = var_54.field_0;
                    var_56 = var_54.field_1;
                    var_57 = var_54.field_6;
                    *(uint64_t *)(local_sp_7 + (-24L)) = 4204588UL;
                    indirect_placeholder(0UL, var_56, 4264245UL, 0UL, var_55, 0UL, var_57);
                    var_58 = local_sp_7 + (-32L);
                    *(uint64_t *)var_58 = 4204612UL;
                    indirect_placeholder_1();
                    local_sp_4 = var_58;
                    loop_state_var = 2U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    if ((uint64_t)(var_15 + (-112)) == 0UL) {
                        continue;
                    }
                    loop_state_var = 2U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 3U:
                {
                    *(uint64_t *)(local_sp_6 + (-8L)) = 4204294UL;
                    indirect_placeholder_11(4203776UL, 0UL, 4293568UL, 4230272UL, 0UL);
                    if (var_26 == 0UL) {
                        var_41 = local_sp_6 + (-16L);
                        *(uint64_t *)var_41 = 4204500UL;
                        var_42 = indirect_placeholder_6(0UL, 0UL);
                        local_sp_0 = var_41;
                        storemerge_in = var_42;
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    var_38 = *(uint64_t *)var_27;
                    var_39 = local_sp_6 + (-16L);
                    *(uint64_t *)var_39 = 4204310UL;
                    var_40 = indirect_placeholder_2(var_38);
                    local_sp_0 = var_39;
                    storemerge_in = var_40;
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 1U:
                {
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 2U:
                {
                    loop_state_var = 2U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 4U:
                {
                    loop_state_var = 3U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 2U:
        {
            *(uint64_t *)(local_sp_4 + (-8L)) = 4204622UL;
            indirect_placeholder_16(r12_0_ph, rbp_0, 1UL);
            abort();
        }
        break;
      case 3U:
      case 1U:
      case 0U:
        {
            switch (loop_state_var) {
              case 3U:
                {
                    var_35 = rbp_1 + (rcx_1 ^ (-1L));
                    rbp_1 = var_35;
                    do {
                        var_32 = local_sp_5 + (-8L);
                        *(uint64_t *)var_32 = 4204203UL;
                        indirect_placeholder_1();
                        rdi1_0 = rbp_1;
                        local_sp_5 = var_32;
                        while (rcx_0 != 0UL)
                            {
                                rdi1_0 = rdi1_0 + var_31;
                            }
                        var_35 = rbp_1 + (rcx_1 ^ (-1L));
                        rbp_1 = var_35;
                    } while ((var_35 + (-4266688L)) <= 4172UL);
                }
                break;
              case 0U:
                {
                    local_sp_1 = local_sp_0;
                    local_sp_2 = local_sp_0;
                    if ((uint64_t)(unsigned char)storemerge_in == 0UL) {
                        return;
                    }
                    var_43 = *(uint64_t *)4293592UL;
                    rax_0 = var_43;
                    rax_1 = var_43;
                    if (var_43 == *(uint64_t *)4293584UL) {
                        *(unsigned char *)4293648UL = (*(unsigned char *)4293648UL | '\x02');
                        rax_1 = rax_0;
                        local_sp_2 = local_sp_1;
                    }
                    var_48 = *(uint64_t *)4293616UL;
                    var_49 = *(uint64_t *)4293576UL;
                    var_50 = (rax_1 + var_48) & (var_48 ^ (-1L));
                    var_51 = *(uint64_t *)4293600UL;
                    var_52 = ((var_50 - var_49) > (var_51 - var_49)) ? var_51 : var_50;
                    *(uint64_t *)4293592UL = var_52;
                    *(uint64_t *)4293584UL = var_52;
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4204456UL;
                    indirect_placeholder_1();
                    *(uint64_t *)(local_sp_2 + (-16L)) = 4204475UL;
                    indirect_placeholder_1();
                    *(uint64_t *)(local_sp_2 + (-24L)) = 4204486UL;
                    indirect_placeholder_1();
                }
                break;
              case 1U:
                {
                    *(unsigned char *)4293648UL = (*(unsigned char *)4293648UL | '\x02');
                    rax_1 = rax_0;
                    local_sp_2 = local_sp_1;
                    var_48 = *(uint64_t *)4293616UL;
                    var_49 = *(uint64_t *)4293576UL;
                    var_50 = (rax_1 + var_48) & (var_48 ^ (-1L));
                    var_51 = *(uint64_t *)4293600UL;
                    var_52 = ((var_50 - var_49) > (var_51 - var_49)) ? var_51 : var_50;
                    *(uint64_t *)4293592UL = var_52;
                    *(uint64_t *)4293584UL = var_52;
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4204456UL;
                    indirect_placeholder_1();
                    *(uint64_t *)(local_sp_2 + (-16L)) = 4204475UL;
                    indirect_placeholder_1();
                    *(uint64_t *)(local_sp_2 + (-24L)) = 4204486UL;
                    indirect_placeholder_1();
                }
                break;
            }
            return;
        }
        break;
    }
}
