typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_63_ret_type;
struct indirect_placeholder_63_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t init_r15(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t init_rax(void);
extern uint64_t init_rcx(void);
extern uint64_t init_r10(void);
extern struct indirect_placeholder_63_ret_type indirect_placeholder_63(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
uint64_t bb_readlinebuffer_delim(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint32_t var_10;
    uint32_t var_11;
    uint64_t var_12;
    uint64_t *var_13;
    uint64_t var_14;
    uint64_t *_cast;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t rbx_1;
    unsigned char var_21;
    uint64_t rbp_3;
    uint64_t rax_1;
    uint64_t r15_1;
    uint64_t r8_1;
    uint64_t var_22;
    uint64_t rbp_2;
    uint64_t local_sp_2;
    uint64_t var_20;
    uint64_t rax_0;
    uint64_t rbx_0;
    uint64_t r8_0;
    uint64_t rbp_0;
    uint64_t rdx1_0;
    uint64_t local_sp_0;
    uint64_t var_33;
    uint64_t rax_2;
    uint64_t rdx1_1;
    uint64_t local_sp_1;
    uint64_t var_23;
    uint64_t var_24;
    struct indirect_placeholder_63_ret_type var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_17;
    uint32_t var_18;
    uint64_t var_19;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_r12();
    var_3 = init_rbx();
    var_4 = init_r15();
    var_5 = init_r13();
    var_6 = init_r14();
    var_7 = init_rbp();
    var_8 = init_rcx();
    var_9 = init_r10();
    *(uint64_t *)(var_0 + (-8L)) = var_4;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    var_10 = (uint32_t)rdx;
    var_11 = (uint32_t)((int)(var_10 << 24U) >> (int)24U);
    var_12 = (uint64_t)var_11;
    *(uint64_t *)(var_0 + (-32L)) = var_2;
    *(uint64_t *)(var_0 + (-40L)) = var_7;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_13 = (uint64_t *)(rdi + 16UL);
    var_14 = *var_13;
    _cast = (uint64_t *)rdi;
    var_15 = *_cast;
    *(uint32_t *)(var_0 + (-60L)) = var_10;
    *(unsigned char *)(var_0 + (-66L)) = (unsigned char)var_11;
    var_16 = var_0 + (-80L);
    *(uint64_t *)var_16 = 4207958UL;
    indirect_placeholder();
    rax_2 = var_1;
    r15_1 = var_14;
    rbp_3 = var_14;
    local_sp_2 = var_16;
    if ((uint64_t)(uint32_t)var_1 == 0UL) {
        return 0UL;
    }
    rbx_1 = var_15 + var_14;
    while (1U)
        {
            var_17 = local_sp_2 + (-8L);
            *(uint64_t *)var_17 = 4208003UL;
            indirect_placeholder();
            var_18 = (uint32_t)rax_2;
            var_19 = (uint64_t)var_18;
            rax_1 = r15_1;
            r8_1 = var_19;
            rbp_2 = rbp_3;
            rax_0 = r15_1;
            rbx_0 = rbx_1;
            r8_0 = var_19;
            rbp_0 = rbp_3;
            rdx1_0 = var_19;
            local_sp_0 = var_17;
            rdx1_1 = var_19;
            local_sp_1 = var_17;
            if ((uint64_t)(var_18 + 1U) == 0UL) {
                rdx1_1 = var_12;
                if (rbp_3 != r15_1) {
                    loop_state_var = 1U;
                    break;
                }
                var_20 = local_sp_2 + (-16L);
                *(uint64_t *)var_20 = 4208023UL;
                indirect_placeholder();
                local_sp_1 = var_20;
                if (var_19 != 0UL) {
                    loop_state_var = 1U;
                    break;
                }
                var_21 = *(unsigned char *)(local_sp_2 + (-10L));
                r8_1 = (uint64_t)var_21;
                if ((uint64_t)(*(unsigned char *)(r15_1 + (-1L)) - var_21) != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                if (r15_1 != rbx_1) {
                    var_22 = r15_1 + 1UL;
                    *(unsigned char *)r15_1 = *(unsigned char *)(local_sp_2 + (-4L));
                    rax_1 = var_22;
                    loop_state_var = 0U;
                    break;
                }
            }
            if (r15_1 != rbx_1) {
                *(unsigned char *)rax_0 = (unsigned char)r8_0;
                var_33 = rax_0 + 1UL;
                rbx_1 = rbx_0;
                rbp_3 = rbp_0;
                rax_1 = var_33;
                r15_1 = var_33;
                rbp_2 = rbp_0;
                local_sp_2 = local_sp_0;
                rax_2 = rax_0;
                if ((uint64_t)(var_11 - (uint32_t)rdx1_0) == 0UL) {
                    continue;
                }
                loop_state_var = 0U;
                break;
            }
            var_23 = *_cast;
            *(uint32_t *)(local_sp_1 + 8UL) = (uint32_t)rdx1_1;
            *(unsigned char *)(local_sp_1 + 7UL) = (unsigned char)r8_1;
            var_24 = local_sp_1 + (-8L);
            *(uint64_t *)var_24 = 4208079UL;
            var_25 = indirect_placeholder_63(var_23, rbp_3, rbp_3, var_8, var_9, rdi);
            var_26 = var_25.field_0;
            var_27 = var_25.field_1;
            var_28 = (uint64_t)*(unsigned char *)(local_sp_1 + (-1L));
            var_29 = (uint64_t)*(uint32_t *)local_sp_1;
            var_30 = var_27 + var_26;
            var_31 = *_cast;
            *var_13 = var_26;
            var_32 = var_31 + var_26;
            rax_0 = var_30;
            rbx_0 = var_32;
            r8_0 = var_28;
            rbp_0 = var_26;
            rdx1_0 = var_29;
            local_sp_0 = var_24;
            *(unsigned char *)rax_0 = (unsigned char)r8_0;
            var_33 = rax_0 + 1UL;
            rbx_1 = rbx_0;
            rbp_3 = rbp_0;
            rax_1 = var_33;
            r15_1 = var_33;
            rbp_2 = rbp_0;
            local_sp_2 = local_sp_0;
            rax_2 = rax_0;
            if ((uint64_t)(var_11 - (uint32_t)rdx1_0) == 0UL) {
                continue;
            }
            loop_state_var = 0U;
            break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(uint64_t *)(rdi + 8UL) = (rax_1 - rbp_2);
            return rdi;
        }
        break;
      case 1U:
        {
            return 0UL;
        }
        break;
    }
}
