typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r12(void);
uint64_t bb_mode_adjust(uint64_t r8, uint64_t rdx, uint64_t rdi, uint64_t rcx, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    unsigned char var_5;
    uint64_t var_6;
    uint64_t rbx_2;
    uint64_t var_7;
    bool var_8;
    unsigned char r11_0_be_in;
    uint64_t cc_src2_7;
    uint64_t rbx_0;
    uint64_t rax_1;
    unsigned char r11_0_in;
    uint64_t rcx4_0;
    uint64_t r10_1;
    uint64_t var_19;
    uint64_t rax_2;
    uint64_t cc_src2_0;
    uint64_t cc_src2_5;
    uint64_t rax_0;
    uint32_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t rdi3_1;
    uint64_t rdi3_0;
    uint64_t r10_0;
    uint64_t rdi3_2;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t cc_src2_4;
    uint64_t r9_3;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t r11_1;
    uint64_t r11_2;
    uint64_t r10_2;
    uint64_t cc_src2_1;
    uint64_t r9_0;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t rdi3_3;
    uint64_t r10_3;
    uint64_t cc_src2_2;
    uint64_t r9_1;
    uint64_t var_28;
    unsigned char var_29;
    uint64_t cc_src2_6;
    uint64_t rdi3_5;
    unsigned char _pre_phi;
    uint64_t rdi3_4;
    uint64_t cc_src2_3;
    uint64_t r9_2;
    uint64_t var_24;
    unsigned char var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t r10_4;
    unsigned char var_34;
    uint32_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t rbx_1;
    uint64_t rbx_0_be;
    uint64_t cc_src2_0_be;
    uint64_t rax_0_be;
    uint64_t r9_4;
    uint32_t var_30;
    uint32_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t r9_5;
    uint32_t var_38;
    uint32_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    unsigned char var_42;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r12();
    var_2 = init_rbx();
    var_3 = init_rbp();
    var_4 = init_cc_src2();
    var_5 = *(unsigned char *)(rcx + 1UL);
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_6 = (uint64_t)((uint16_t)rdi & (unsigned short)4095U);
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    rbx_2 = 0UL;
    rbx_0 = 0UL;
    r11_0_in = var_5;
    rcx4_0 = rcx;
    r10_1 = 4294967295UL;
    rax_2 = var_6;
    cc_src2_0 = var_4;
    rax_0 = var_6;
    rdi3_1 = 0UL;
    rdi3_0 = 0UL;
    r10_0 = 4294967295UL;
    rdi3_2 = 0UL;
    r10_2 = 4294967295UL;
    if (var_5 != '\x00') {
        var_7 = (uint64_t)(uint32_t)rdx ^ 4294967295UL;
        var_8 = ((uint64_t)(unsigned char)rsi == 0UL);
        while (1U)
            {
                var_9 = *(uint32_t *)(rcx4_0 + 4UL);
                var_10 = (uint64_t)var_9;
                var_11 = (uint64_t)*(uint32_t *)(rcx4_0 + 8UL);
                rax_1 = rax_0;
                cc_src2_1 = cc_src2_0;
                r9_0 = var_11;
                cc_src2_2 = cc_src2_0;
                cc_src2_3 = cc_src2_0;
                rbx_1 = rbx_0;
                if (!var_8) {
                    var_12 = (uint64_t)*(uint32_t *)(rcx4_0 + 12UL);
                    var_13 = var_12 | 4294964223UL;
                    var_14 = (uint64_t)((uint16_t)var_12 & (unsigned short)3072U) ^ 3072UL;
                    rdi3_0 = var_14;
                    r10_0 = var_13;
                    rdi3_1 = var_14;
                    r10_1 = var_13;
                    if ((uint64_t)(r11_0_in + '\xfe') == 0UL) {
                        var_19 = var_11 | 73UL;
                        var_20 = (uint64_t)*(unsigned char *)rcx4_0;
                        var_21 = var_19 & r10_1;
                        r11_1 = var_20;
                        r11_2 = var_20;
                        rdi3_3 = rdi3_1;
                        r10_3 = r10_1;
                        r9_1 = var_21;
                        rdi3_4 = rdi3_1;
                        r9_2 = var_21;
                        if (var_9 == 0U) {
                            var_28 = r9_1 & var_7;
                            var_29 = (unsigned char)r11_1;
                            cc_src2_5 = cc_src2_2;
                            cc_src2_4 = cc_src2_2;
                            r9_3 = var_28;
                            cc_src2_6 = cc_src2_2;
                            rdi3_5 = rdi3_3;
                            _pre_phi = var_29;
                            r10_4 = r10_3;
                            r9_4 = var_28;
                            r9_5 = var_28;
                            if ((uint64_t)(var_29 + '\xd3') == 0UL) {
                                if ((uint64_t)(var_29 + '\xc3') != 0UL) {
                                    var_34 = *(unsigned char *)(rcx4_0 + 17UL);
                                    var_35 = (uint32_t)rax_0 & (uint32_t)rdi3_5;
                                    var_36 = (uint64_t)(uint32_t)rbx_0 | (uint64_t)((uint16_t)r10_4 & (unsigned short)4095U);
                                    var_37 = (uint64_t)(var_35 | (uint32_t)r9_3);
                                    rbx_2 = var_36;
                                    r11_0_be_in = var_34;
                                    rax_2 = var_37;
                                    rbx_0_be = var_36;
                                    cc_src2_0_be = cc_src2_4;
                                    rax_0_be = var_37;
                                    if (var_34 == '\x00') {
                                        break;
                                    }
                                    rbx_0 = rbx_0_be;
                                    r11_0_in = r11_0_be_in;
                                    rcx4_0 = rcx4_0 + 16UL;
                                    cc_src2_0 = cc_src2_0_be;
                                    rax_0 = rax_0_be;
                                    continue;
                                }
                            }
                            var_38 = (uint32_t)rbx_0;
                            var_39 = (uint32_t)r9_5;
                            var_40 = (uint64_t)(var_38 | var_39);
                            var_41 = rax_0 & ((uint64_t)var_39 ^ 4294967295UL);
                            rbx_1 = var_40;
                            cc_src2_7 = cc_src2_6;
                            rax_1 = var_41;
                            var_42 = *(unsigned char *)(rcx4_0 + 17UL);
                            rbx_2 = rbx_1;
                            r11_0_be_in = var_42;
                            rax_2 = rax_1;
                            rbx_0_be = rbx_1;
                            cc_src2_0_be = cc_src2_7;
                            rax_0_be = rax_1;
                            if (var_42 == '\x00') {
                                break;
                            }
                            rbx_0 = rbx_0_be;
                            r11_0_in = r11_0_be_in;
                            rcx4_0 = rcx4_0 + 16UL;
                            cc_src2_0 = cc_src2_0_be;
                            rax_0 = rax_0_be;
                            continue;
                        }
                        var_24 = r9_2 & var_10;
                        var_25 = (unsigned char)r11_2;
                        cc_src2_5 = cc_src2_3;
                        cc_src2_4 = cc_src2_3;
                        r9_3 = var_24;
                        cc_src2_6 = cc_src2_3;
                        _pre_phi = var_25;
                        r9_4 = var_24;
                        r9_5 = var_24;
                        if ((uint64_t)(var_25 + '\xd3') != 0UL) {
                            var_38 = (uint32_t)rbx_0;
                            var_39 = (uint32_t)r9_5;
                            var_40 = (uint64_t)(var_38 | var_39);
                            var_41 = rax_0 & ((uint64_t)var_39 ^ 4294967295UL);
                            rbx_1 = var_40;
                            cc_src2_7 = cc_src2_6;
                            rax_1 = var_41;
                            var_42 = *(unsigned char *)(rcx4_0 + 17UL);
                            rbx_2 = rbx_1;
                            r11_0_be_in = var_42;
                            rax_2 = rax_1;
                            rbx_0_be = rbx_1;
                            cc_src2_0_be = cc_src2_7;
                            rax_0_be = rax_1;
                            if (var_42 == '\x00') {
                                break;
                            }
                            rbx_0 = rbx_0_be;
                            r11_0_in = r11_0_be_in;
                            rcx4_0 = rcx4_0 + 16UL;
                            cc_src2_0 = cc_src2_0_be;
                            rax_0 = rax_0_be;
                            continue;
                        }
                        if ((uint64_t)(var_25 + '\xc3') != 0UL) {
                            var_26 = (uint64_t)(uint32_t)rdi3_4 | (var_10 ^ 4294967295UL);
                            var_27 = var_26 ^ 4294967295UL;
                            rdi3_5 = var_26;
                            r10_4 = var_27;
                            var_34 = *(unsigned char *)(rcx4_0 + 17UL);
                            var_35 = (uint32_t)rax_0 & (uint32_t)rdi3_5;
                            var_36 = (uint64_t)(uint32_t)rbx_0 | (uint64_t)((uint16_t)r10_4 & (unsigned short)4095U);
                            var_37 = (uint64_t)(var_35 | (uint32_t)r9_3);
                            rbx_2 = var_36;
                            r11_0_be_in = var_34;
                            rax_2 = var_37;
                            rbx_0_be = var_36;
                            cc_src2_0_be = cc_src2_4;
                            rax_0_be = var_37;
                            if (var_34 == '\x00') {
                                break;
                            }
                            rbx_0 = rbx_0_be;
                            r11_0_in = r11_0_be_in;
                            rcx4_0 = rcx4_0 + 16UL;
                            cc_src2_0 = cc_src2_0_be;
                            rax_0 = rax_0_be;
                            continue;
                        }
                        cc_src2_7 = cc_src2_5;
                        if ((uint64_t)(_pre_phi + '\xd5') == 0UL) {
                            var_30 = (uint32_t)rbx_0;
                            var_31 = (uint32_t)r9_4;
                            var_32 = (uint64_t)(var_30 | var_31);
                            var_33 = (uint64_t)((uint32_t)rax_0 | var_31);
                            rbx_1 = var_32;
                            rax_1 = var_33;
                        }
                        var_42 = *(unsigned char *)(rcx4_0 + 17UL);
                        rbx_2 = rbx_1;
                        r11_0_be_in = var_42;
                        rax_2 = rax_1;
                        rbx_0_be = rbx_1;
                        cc_src2_0_be = cc_src2_7;
                        rax_0_be = rax_1;
                        if (var_42 == '\x00') {
                            break;
                        }
                        rbx_0 = rbx_0_be;
                        r11_0_in = r11_0_be_in;
                        rcx4_0 = rcx4_0 + 16UL;
                        cc_src2_0 = cc_src2_0_be;
                        rax_0 = rax_0_be;
                        continue;
                    }
                    rdi3_2 = rdi3_0;
                    r10_2 = r10_0;
                    if ((uint64_t)(r11_0_in + '\xfd') == 0UL) {
                        var_15 = rax_0 & var_11;
                        var_16 = helper_cc_compute_c_wrapper((uint64_t)((uint16_t)var_15 & (unsigned short)292U) + (-1L), 1UL, cc_src2_0, 16U);
                        var_17 = (uint64_t)(((unsigned short)0U - (uint16_t)var_16) & (unsigned short)292U) ^ 292UL;
                        var_18 = ((uint64_t)((unsigned char)var_15 & '\x92') == 0UL) ? var_17 : (var_17 | 146UL);
                        cc_src2_1 = var_16;
                        r9_0 = var_15 | (((var_15 & 73UL) == 0UL) ? var_18 : (var_18 | 73UL));
                    }
                    var_22 = (uint64_t)*(unsigned char *)rcx4_0;
                    var_23 = (uint64_t)((uint32_t)r9_0 & (uint32_t)r10_2);
                    r11_1 = var_22;
                    r11_2 = var_22;
                    rdi3_3 = rdi3_2;
                    r10_3 = r10_2;
                    cc_src2_2 = cc_src2_1;
                    r9_1 = var_23;
                    rdi3_4 = rdi3_2;
                    cc_src2_3 = cc_src2_1;
                    r9_2 = var_23;
                    if (var_9 == 0U) {
                        var_28 = r9_1 & var_7;
                        var_29 = (unsigned char)r11_1;
                        cc_src2_5 = cc_src2_2;
                        cc_src2_4 = cc_src2_2;
                        r9_3 = var_28;
                        cc_src2_6 = cc_src2_2;
                        rdi3_5 = rdi3_3;
                        _pre_phi = var_29;
                        r10_4 = r10_3;
                        r9_4 = var_28;
                        r9_5 = var_28;
                        if ((uint64_t)(var_29 + '\xd3') == 0UL) {
                            if ((uint64_t)(var_29 + '\xc3') != 0UL) {
                                var_34 = *(unsigned char *)(rcx4_0 + 17UL);
                                var_35 = (uint32_t)rax_0 & (uint32_t)rdi3_5;
                                var_36 = (uint64_t)(uint32_t)rbx_0 | (uint64_t)((uint16_t)r10_4 & (unsigned short)4095U);
                                var_37 = (uint64_t)(var_35 | (uint32_t)r9_3);
                                rbx_2 = var_36;
                                r11_0_be_in = var_34;
                                rax_2 = var_37;
                                rbx_0_be = var_36;
                                cc_src2_0_be = cc_src2_4;
                                rax_0_be = var_37;
                                if (var_34 == '\x00') {
                                    break;
                                }
                                rbx_0 = rbx_0_be;
                                r11_0_in = r11_0_be_in;
                                rcx4_0 = rcx4_0 + 16UL;
                                cc_src2_0 = cc_src2_0_be;
                                rax_0 = rax_0_be;
                                continue;
                            }
                        }
                        var_38 = (uint32_t)rbx_0;
                        var_39 = (uint32_t)r9_5;
                        var_40 = (uint64_t)(var_38 | var_39);
                        var_41 = rax_0 & ((uint64_t)var_39 ^ 4294967295UL);
                        rbx_1 = var_40;
                        cc_src2_7 = cc_src2_6;
                        rax_1 = var_41;
                        var_42 = *(unsigned char *)(rcx4_0 + 17UL);
                        rbx_2 = rbx_1;
                        r11_0_be_in = var_42;
                        rax_2 = rax_1;
                        rbx_0_be = rbx_1;
                        cc_src2_0_be = cc_src2_7;
                        rax_0_be = rax_1;
                        if (var_42 == '\x00') {
                            break;
                        }
                        rbx_0 = rbx_0_be;
                        r11_0_in = r11_0_be_in;
                        rcx4_0 = rcx4_0 + 16UL;
                        cc_src2_0 = cc_src2_0_be;
                        rax_0 = rax_0_be;
                        continue;
                    }
                    var_24 = r9_2 & var_10;
                    var_25 = (unsigned char)r11_2;
                    cc_src2_5 = cc_src2_3;
                    cc_src2_4 = cc_src2_3;
                    r9_3 = var_24;
                    cc_src2_6 = cc_src2_3;
                    _pre_phi = var_25;
                    r9_4 = var_24;
                    r9_5 = var_24;
                    if ((uint64_t)(var_25 + '\xd3') != 0UL) {
                        var_38 = (uint32_t)rbx_0;
                        var_39 = (uint32_t)r9_5;
                        var_40 = (uint64_t)(var_38 | var_39);
                        var_41 = rax_0 & ((uint64_t)var_39 ^ 4294967295UL);
                        rbx_1 = var_40;
                        cc_src2_7 = cc_src2_6;
                        rax_1 = var_41;
                        var_42 = *(unsigned char *)(rcx4_0 + 17UL);
                        rbx_2 = rbx_1;
                        r11_0_be_in = var_42;
                        rax_2 = rax_1;
                        rbx_0_be = rbx_1;
                        cc_src2_0_be = cc_src2_7;
                        rax_0_be = rax_1;
                        if (var_42 == '\x00') {
                            break;
                        }
                        rbx_0 = rbx_0_be;
                        r11_0_in = r11_0_be_in;
                        rcx4_0 = rcx4_0 + 16UL;
                        cc_src2_0 = cc_src2_0_be;
                        rax_0 = rax_0_be;
                        continue;
                    }
                    if ((uint64_t)(var_25 + '\xc3') != 0UL) {
                        var_26 = (uint64_t)(uint32_t)rdi3_4 | (var_10 ^ 4294967295UL);
                        var_27 = var_26 ^ 4294967295UL;
                        rdi3_5 = var_26;
                        r10_4 = var_27;
                        var_34 = *(unsigned char *)(rcx4_0 + 17UL);
                        var_35 = (uint32_t)rax_0 & (uint32_t)rdi3_5;
                        var_36 = (uint64_t)(uint32_t)rbx_0 | (uint64_t)((uint16_t)r10_4 & (unsigned short)4095U);
                        var_37 = (uint64_t)(var_35 | (uint32_t)r9_3);
                        rbx_2 = var_36;
                        r11_0_be_in = var_34;
                        rax_2 = var_37;
                        rbx_0_be = var_36;
                        cc_src2_0_be = cc_src2_4;
                        rax_0_be = var_37;
                        if (var_34 == '\x00') {
                            break;
                        }
                        rbx_0 = rbx_0_be;
                        r11_0_in = r11_0_be_in;
                        rcx4_0 = rcx4_0 + 16UL;
                        cc_src2_0 = cc_src2_0_be;
                        rax_0 = rax_0_be;
                        continue;
                    }
                    cc_src2_7 = cc_src2_5;
                    if ((uint64_t)(_pre_phi + '\xd5') == 0UL) {
                        var_30 = (uint32_t)rbx_0;
                        var_31 = (uint32_t)r9_4;
                        var_32 = (uint64_t)(var_30 | var_31);
                        var_33 = (uint64_t)((uint32_t)rax_0 | var_31);
                        rbx_1 = var_32;
                        rax_1 = var_33;
                    }
                    var_42 = *(unsigned char *)(rcx4_0 + 17UL);
                    rbx_2 = rbx_1;
                    r11_0_be_in = var_42;
                    rax_2 = rax_1;
                    rbx_0_be = rbx_1;
                    cc_src2_0_be = cc_src2_7;
                    rax_0_be = rax_1;
                    if (var_42 == '\x00') {
                        break;
                    }
                    rbx_0 = rbx_0_be;
                    r11_0_in = r11_0_be_in;
                    rcx4_0 = rcx4_0 + 16UL;
                    cc_src2_0 = cc_src2_0_be;
                    rax_0 = rax_0_be;
                    continue;
                }
                if ((uint64_t)(r11_0_in + '\xfe') == 0UL) {
                    if ((rax_0 & 73UL) != 0UL) {
                        var_19 = var_11 | 73UL;
                        var_20 = (uint64_t)*(unsigned char *)rcx4_0;
                        var_21 = var_19 & r10_1;
                        r11_1 = var_20;
                        r11_2 = var_20;
                        rdi3_3 = rdi3_1;
                        r10_3 = r10_1;
                        r9_1 = var_21;
                        rdi3_4 = rdi3_1;
                        r9_2 = var_21;
                        if (var_9 == 0U) {
                            var_28 = r9_1 & var_7;
                            var_29 = (unsigned char)r11_1;
                            cc_src2_5 = cc_src2_2;
                            cc_src2_4 = cc_src2_2;
                            r9_3 = var_28;
                            cc_src2_6 = cc_src2_2;
                            rdi3_5 = rdi3_3;
                            _pre_phi = var_29;
                            r10_4 = r10_3;
                            r9_4 = var_28;
                            r9_5 = var_28;
                            if ((uint64_t)(var_29 + '\xd3') == 0UL) {
                                if ((uint64_t)(var_29 + '\xc3') != 0UL) {
                                    var_34 = *(unsigned char *)(rcx4_0 + 17UL);
                                    var_35 = (uint32_t)rax_0 & (uint32_t)rdi3_5;
                                    var_36 = (uint64_t)(uint32_t)rbx_0 | (uint64_t)((uint16_t)r10_4 & (unsigned short)4095U);
                                    var_37 = (uint64_t)(var_35 | (uint32_t)r9_3);
                                    rbx_2 = var_36;
                                    r11_0_be_in = var_34;
                                    rax_2 = var_37;
                                    rbx_0_be = var_36;
                                    cc_src2_0_be = cc_src2_4;
                                    rax_0_be = var_37;
                                    if (var_34 == '\x00') {
                                        break;
                                    }
                                    rbx_0 = rbx_0_be;
                                    r11_0_in = r11_0_be_in;
                                    rcx4_0 = rcx4_0 + 16UL;
                                    cc_src2_0 = cc_src2_0_be;
                                    rax_0 = rax_0_be;
                                    continue;
                                }
                            }
                            var_38 = (uint32_t)rbx_0;
                            var_39 = (uint32_t)r9_5;
                            var_40 = (uint64_t)(var_38 | var_39);
                            var_41 = rax_0 & ((uint64_t)var_39 ^ 4294967295UL);
                            rbx_1 = var_40;
                            cc_src2_7 = cc_src2_6;
                            rax_1 = var_41;
                            var_42 = *(unsigned char *)(rcx4_0 + 17UL);
                            rbx_2 = rbx_1;
                            r11_0_be_in = var_42;
                            rax_2 = rax_1;
                            rbx_0_be = rbx_1;
                            cc_src2_0_be = cc_src2_7;
                            rax_0_be = rax_1;
                            if (var_42 == '\x00') {
                                break;
                            }
                            rbx_0 = rbx_0_be;
                            r11_0_in = r11_0_be_in;
                            rcx4_0 = rcx4_0 + 16UL;
                            cc_src2_0 = cc_src2_0_be;
                            rax_0 = rax_0_be;
                            continue;
                        }
                        var_24 = r9_2 & var_10;
                        var_25 = (unsigned char)r11_2;
                        cc_src2_5 = cc_src2_3;
                        cc_src2_4 = cc_src2_3;
                        r9_3 = var_24;
                        cc_src2_6 = cc_src2_3;
                        _pre_phi = var_25;
                        r9_4 = var_24;
                        r9_5 = var_24;
                        if ((uint64_t)(var_25 + '\xd3') != 0UL) {
                            var_38 = (uint32_t)rbx_0;
                            var_39 = (uint32_t)r9_5;
                            var_40 = (uint64_t)(var_38 | var_39);
                            var_41 = rax_0 & ((uint64_t)var_39 ^ 4294967295UL);
                            rbx_1 = var_40;
                            cc_src2_7 = cc_src2_6;
                            rax_1 = var_41;
                            var_42 = *(unsigned char *)(rcx4_0 + 17UL);
                            rbx_2 = rbx_1;
                            r11_0_be_in = var_42;
                            rax_2 = rax_1;
                            rbx_0_be = rbx_1;
                            cc_src2_0_be = cc_src2_7;
                            rax_0_be = rax_1;
                            if (var_42 == '\x00') {
                                break;
                            }
                            rbx_0 = rbx_0_be;
                            r11_0_in = r11_0_be_in;
                            rcx4_0 = rcx4_0 + 16UL;
                            cc_src2_0 = cc_src2_0_be;
                            rax_0 = rax_0_be;
                            continue;
                        }
                        if ((uint64_t)(var_25 + '\xc3') != 0UL) {
                            var_26 = (uint64_t)(uint32_t)rdi3_4 | (var_10 ^ 4294967295UL);
                            var_27 = var_26 ^ 4294967295UL;
                            rdi3_5 = var_26;
                            r10_4 = var_27;
                            var_34 = *(unsigned char *)(rcx4_0 + 17UL);
                            var_35 = (uint32_t)rax_0 & (uint32_t)rdi3_5;
                            var_36 = (uint64_t)(uint32_t)rbx_0 | (uint64_t)((uint16_t)r10_4 & (unsigned short)4095U);
                            var_37 = (uint64_t)(var_35 | (uint32_t)r9_3);
                            rbx_2 = var_36;
                            r11_0_be_in = var_34;
                            rax_2 = var_37;
                            rbx_0_be = var_36;
                            cc_src2_0_be = cc_src2_4;
                            rax_0_be = var_37;
                            if (var_34 == '\x00') {
                                break;
                            }
                            rbx_0 = rbx_0_be;
                            r11_0_in = r11_0_be_in;
                            rcx4_0 = rcx4_0 + 16UL;
                            cc_src2_0 = cc_src2_0_be;
                            rax_0 = rax_0_be;
                            continue;
                        }
                        cc_src2_7 = cc_src2_5;
                        if ((uint64_t)(_pre_phi + '\xd5') == 0UL) {
                            var_30 = (uint32_t)rbx_0;
                            var_31 = (uint32_t)r9_4;
                            var_32 = (uint64_t)(var_30 | var_31);
                            var_33 = (uint64_t)((uint32_t)rax_0 | var_31);
                            rbx_1 = var_32;
                            rax_1 = var_33;
                        }
                        var_42 = *(unsigned char *)(rcx4_0 + 17UL);
                        rbx_2 = rbx_1;
                        r11_0_be_in = var_42;
                        rax_2 = rax_1;
                        rbx_0_be = rbx_1;
                        cc_src2_0_be = cc_src2_7;
                        rax_0_be = rax_1;
                        if (var_42 == '\x00') {
                            break;
                        }
                        rbx_0 = rbx_0_be;
                        r11_0_in = r11_0_be_in;
                        rcx4_0 = rcx4_0 + 16UL;
                        cc_src2_0 = cc_src2_0_be;
                        rax_0 = rax_0_be;
                        continue;
                    }
                }
                rdi3_2 = rdi3_0;
                r10_2 = r10_0;
                if ((uint64_t)(r11_0_in + '\xfd') == 0UL) {
                    var_15 = rax_0 & var_11;
                    var_16 = helper_cc_compute_c_wrapper((uint64_t)((uint16_t)var_15 & (unsigned short)292U) + (-1L), 1UL, cc_src2_0, 16U);
                    var_17 = (uint64_t)(((unsigned short)0U - (uint16_t)var_16) & (unsigned short)292U) ^ 292UL;
                    var_18 = ((uint64_t)((unsigned char)var_15 & '\x92') == 0UL) ? var_17 : (var_17 | 146UL);
                    cc_src2_1 = var_16;
                    r9_0 = var_15 | (((var_15 & 73UL) == 0UL) ? var_18 : (var_18 | 73UL));
                }
            }
    }
    if (r8 == 0UL) {
        return rax_2;
    }
    *(uint32_t *)r8 = (uint32_t)rbx_2;
    return rax_2;
}
