typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t init_r9(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_3(uint64_t param_0);
extern uint64_t init_rax(void);
extern uint64_t init_r8(void);
extern void indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern uint64_t init_rcx(void);
extern void indirect_placeholder_22(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_print_uptime(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t local_sp_2;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t local_sp_0;
    uint64_t var_32;
    uint64_t rbx_0;
    uint64_t rbp_0;
    uint64_t var_31;
    uint64_t local_sp_3;
    bool var_25;
    uint64_t var_26;
    uint64_t *var_27;
    uint64_t var_28;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t rbp_1;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t rax_0;
    uint64_t local_sp_4;
    uint64_t var_34;
    uint64_t rsi2_0;
    uint64_t rax_1;
    unsigned char var_11;
    uint16_t var_12;
    uint64_t var_13;
    uint64_t rbx_1;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_33;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_r12();
    var_3 = init_rbx();
    var_4 = init_r13();
    var_5 = init_r14();
    var_6 = init_r8();
    var_7 = init_rbp();
    var_8 = init_rcx();
    var_9 = init_cc_src2();
    var_10 = init_r9();
    *(uint64_t *)(var_0 + (-8L)) = var_5;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_7;
    *(uint64_t *)(var_0 + (-40L)) = var_3;
    rax_0 = var_1;
    rbx_0 = 0UL;
    rbp_0 = 0UL;
    rsi2_0 = rsi;
    if (rdi != 0UL) {
        rax_1 = rdi + (-1L);
        while (1U)
            {
                var_11 = *(unsigned char *)(rsi2_0 + 44UL);
                var_12 = *(uint16_t *)rsi2_0;
                rbx_1 = rbx_0;
                rbp_1 = rbp_0;
                if (var_11 != '\x00') {
                    if ((uint64_t)(var_12 + (unsigned short)65529U) != 0UL) {
                        var_13 = rbp_0 + 1UL;
                        rbp_1 = var_13;
                        var_15 = rax_1 + (-1L);
                        rax_0 = var_15;
                        rbx_0 = rbx_1;
                        rbp_0 = rbp_1;
                        rax_1 = var_15;
                        if (rax_1 == 0UL) {
                            break;
                        }
                        rsi2_0 = rsi2_0 + 400UL;
                        continue;
                    }
                }
                if ((uint64_t)(var_12 + (unsigned short)65534U) == 0UL) {
                    var_14 = *(uint64_t *)(rsi2_0 + 344UL);
                    rbx_1 = var_14;
                }
                var_15 = rax_1 + (-1L);
                rax_0 = var_15;
                rbx_0 = rbx_1;
                rbp_0 = rbp_1;
                rax_1 = var_15;
                if (rax_1 == 0UL) {
                    break;
                }
                rsi2_0 = rsi2_0 + 400UL;
                continue;
            }
        var_16 = var_0 + (-96L);
        *(uint64_t *)var_16 = 4204591UL;
        indirect_placeholder_1();
        *(uint64_t *)(var_0 + (-88L)) = var_15;
        local_sp_4 = var_16;
        if (rbx_1 != 0UL) {
            var_17 = var_15 - rbx_1;
            var_18 = (uint64_t)((long)(uint64_t)(((unsigned __int128)var_17 * 1749024623285053783ULL) >> 64ULL) >> (long)13UL) - (uint64_t)((long)var_17 >> (long)63UL);
            var_19 = (var_18 * 18446744073709465216UL) + var_17;
            var_20 = (var_19 - (uint64_t)((long)((uint64_t)((long)((uint64_t)((uint32_t)(uint64_t)(((unsigned __int128)var_19 * 5247073869855161349ULL) >> 74ULL) - (uint32_t)(uint64_t)((long)var_19 >> (long)63UL)) << 32UL) >> (long)32UL) * 15461882265600UL) >> (long)32UL)) * 9838263505978427529UL;
            *(uint64_t *)(var_0 + (-104L)) = 4204741UL;
            var_21 = indirect_placeholder_3(var_20);
            if (var_21 == 0UL) {
                var_24 = var_0 + (-112L);
                *(uint64_t *)var_24 = 4205015UL;
                indirect_placeholder_1();
                local_sp_3 = var_24;
            } else {
                var_22 = *(uint64_t *)4288064UL;
                var_23 = var_0 + (-112L);
                *(uint64_t *)var_23 = 4204775UL;
                indirect_placeholder_22(0UL, var_21, var_22, 0UL, 4264025UL);
                local_sp_3 = var_23;
            }
            var_25 = ((long)var_17 > (long)86399UL);
            var_26 = local_sp_3 + (-8L);
            var_27 = (uint64_t *)var_26;
            local_sp_2 = var_26;
            if (var_25) {
                *var_27 = 4204806UL;
                indirect_placeholder_3(var_18);
                var_28 = local_sp_3 + (-16L);
                *(uint64_t *)var_28 = 4204839UL;
                indirect_placeholder_1();
                local_sp_2 = var_28;
            } else {
                *var_27 = 4205037UL;
                indirect_placeholder_1();
            }
            *(uint64_t *)(local_sp_2 + (-8L)) = 4204847UL;
            indirect_placeholder_3(rbp_1);
            *(uint64_t *)(local_sp_2 + (-16L)) = 4204875UL;
            indirect_placeholder_1();
            var_29 = local_sp_2 + (-24L);
            *(uint64_t *)var_29 = 4204890UL;
            indirect_placeholder_1();
            var_30 = helper_cc_compute_all_wrapper(0UL, 0UL, 0UL, 24U);
            local_sp_0 = var_29;
            if ((uint64_t)(((unsigned char)(var_30 >> 4UL) ^ (unsigned char)var_30) & '\xc0') == 0UL) {
                var_31 = local_sp_2 + (-32L);
                *(uint64_t *)var_31 = 4205001UL;
                indirect_placeholder_1();
                local_sp_0 = var_31;
            }
            var_32 = helper_cc_compute_all_wrapper(18446744073709551615UL, 1UL, var_9, 16U);
            if ((var_32 & 64UL) != 0UL) {
                *(uint64_t *)(local_sp_0 + (-8L)) = 4204918UL;
                indirect_placeholder_1();
            }
            return;
        }
    }
    var_33 = var_0 + (-96L);
    *(uint64_t *)var_33 = 4205066UL;
    indirect_placeholder_1();
    *(uint64_t *)(var_0 + (-88L)) = var_1;
    local_sp_4 = var_33;
}
