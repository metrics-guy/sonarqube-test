typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_divq_EAX_wrapper_ret_type;
struct type_5;
struct indirect_placeholder_127_ret_type;
struct indirect_placeholder_132_ret_type;
struct indirect_placeholder_129_ret_type;
struct indirect_placeholder_130_ret_type;
struct indirect_placeholder_131_ret_type;
struct indirect_placeholder_133_ret_type;
struct indirect_placeholder_135_ret_type;
struct indirect_placeholder_134_ret_type;
struct indirect_placeholder_136_ret_type;
struct indirect_placeholder_138_ret_type;
struct indirect_placeholder_139_ret_type;
struct indirect_placeholder_137_ret_type;
struct indirect_placeholder_140_ret_type;
struct indirect_placeholder_141_ret_type;
struct indirect_placeholder_142_ret_type;
struct indirect_placeholder_143_ret_type;
struct indirect_placeholder_144_ret_type;
struct indirect_placeholder_145_ret_type;
struct indirect_placeholder_146_ret_type;
struct indirect_placeholder_147_ret_type;
struct indirect_placeholder_148_ret_type;
struct indirect_placeholder_149_ret_type;
struct indirect_placeholder_150_ret_type;
struct indirect_placeholder_151_ret_type;
struct indirect_placeholder_152_ret_type;
struct indirect_placeholder_153_ret_type;
struct indirect_placeholder_154_ret_type;
struct indirect_placeholder_128_ret_type;
struct indirect_placeholder_155_ret_type;
struct indirect_placeholder_156_ret_type;
struct indirect_placeholder_157_ret_type;
struct helper_divq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint32_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint32_t field_5;
    uint32_t field_6;
    uint64_t field_7;
    uint32_t field_8;
    uint64_t field_9;
    uint32_t field_10;
    unsigned char field_11;
    uint32_t field_12;
};
struct type_5 {
};
struct indirect_placeholder_127_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_132_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_129_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_130_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_131_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_133_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_135_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_134_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_136_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_138_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_139_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
    uint64_t field_8;
};
struct indirect_placeholder_137_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_140_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_141_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_142_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_143_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_144_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_145_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_146_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_147_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_148_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_149_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_150_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_151_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_152_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_153_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_154_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_128_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_155_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_156_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_157_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_r12(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r15(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint32_t init_state_0x8248(void);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern struct helper_divq_EAX_wrapper_ret_type helper_divq_EAX_wrapper(struct type_5 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint32_t param_13, uint64_t param_14, uint32_t param_15, uint64_t param_16, uint64_t param_17, uint64_t param_18, uint32_t param_19);
extern uint64_t indirect_placeholder_7(uint64_t param_0);
extern struct indirect_placeholder_127_ret_type indirect_placeholder_127(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_96(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_132_ret_type indirect_placeholder_132(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_129_ret_type indirect_placeholder_129(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_130_ret_type indirect_placeholder_130(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_131_ret_type indirect_placeholder_131(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_133_ret_type indirect_placeholder_133(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_135_ret_type indirect_placeholder_135(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_134_ret_type indirect_placeholder_134(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_136_ret_type indirect_placeholder_136(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_138_ret_type indirect_placeholder_138(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_139_ret_type indirect_placeholder_139(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_137_ret_type indirect_placeholder_137(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_140_ret_type indirect_placeholder_140(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_141_ret_type indirect_placeholder_141(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_142_ret_type indirect_placeholder_142(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_143_ret_type indirect_placeholder_143(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_144_ret_type indirect_placeholder_144(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_145_ret_type indirect_placeholder_145(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_146_ret_type indirect_placeholder_146(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_147_ret_type indirect_placeholder_147(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_148_ret_type indirect_placeholder_148(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_149_ret_type indirect_placeholder_149(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_150_ret_type indirect_placeholder_150(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_151_ret_type indirect_placeholder_151(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_152_ret_type indirect_placeholder_152(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_153_ret_type indirect_placeholder_153(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_154_ret_type indirect_placeholder_154(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_128_ret_type indirect_placeholder_128(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_155_ret_type indirect_placeholder_155(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
extern struct indirect_placeholder_156_ret_type indirect_placeholder_156(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_157_ret_type indirect_placeholder_157(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
void bb_scanargs(uint64_t r8, uint64_t rdi, uint64_t r10, uint64_t rsi, uint64_t r9) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint32_t var_8;
    uint64_t var_9;
    uint32_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint32_t var_14;
    uint64_t var_15;
    uint32_t var_16;
    uint64_t local_sp_16;
    uint64_t local_sp_17;
    uint64_t r81_0;
    uint64_t r15_0;
    uint64_t rcx_0;
    uint64_t r95_0;
    uint64_t local_sp_0;
    uint64_t r95_8;
    uint64_t rdi2_5;
    uint64_t var_107;
    uint64_t r15_1;
    uint64_t r15_2;
    uint64_t r15_8;
    uint64_t r13_4;
    uint64_t r14_5;
    uint64_t r81_8;
    uint64_t rbp_2;
    uint64_t rdi2_4;
    uint64_t r103_3;
    uint64_t var_108;
    uint64_t *var_109;
    struct indirect_placeholder_132_ret_type var_110;
    uint64_t var_111;
    uint64_t var_112;
    uint64_t var_102;
    uint64_t *var_103;
    struct indirect_placeholder_129_ret_type var_104;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t var_100;
    uint64_t var_121;
    uint64_t var_122;
    uint64_t var_119;
    struct indirect_placeholder_130_ret_type var_120;
    uint64_t r14_4_ph;
    uint64_t var_118;
    uint64_t var_114;
    struct indirect_placeholder_131_ret_type var_115;
    uint64_t var_116;
    uint64_t var_117;
    uint64_t var_113;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t *var_94;
    struct indirect_placeholder_133_ret_type var_95;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t r81_1;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t rdi2_0;
    uint64_t rax_0;
    uint64_t local_sp_1;
    uint64_t local_sp_2;
    uint64_t r15_3;
    uint64_t var_101;
    uint64_t r81_2;
    uint64_t rdi2_1;
    uint64_t local_sp_3;
    uint32_t var_123;
    uint64_t var_124;
    uint64_t r15_5;
    uint64_t local_sp_4;
    uint64_t r13_1;
    uint64_t r14_1;
    uint64_t rax_2;
    uint64_t rbp_0;
    uint64_t local_sp_10;
    uint64_t rbp_1;
    struct indirect_placeholder_135_ret_type var_125;
    uint64_t var_126;
    uint64_t var_127;
    uint64_t var_128;
    uint64_t var_129;
    uint64_t var_85;
    struct indirect_placeholder_136_ret_type var_86;
    uint64_t var_197;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_62;
    struct indirect_placeholder_138_ret_type var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    struct indirect_placeholder_139_ret_type var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    struct indirect_placeholder_137_ret_type var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_60;
    struct indirect_placeholder_140_ret_type var_61;
    uint64_t r13_0;
    uint64_t rbp_1_ph;
    uint64_t r14_0;
    uint64_t local_sp_5;
    uint64_t var_130;
    struct indirect_placeholder_141_ret_type var_131;
    uint64_t var_132;
    uint64_t var_133;
    uint64_t var_134;
    uint64_t var_135;
    uint64_t var_136;
    uint64_t var_137;
    uint64_t var_57;
    uint64_t var_58;
    struct indirect_placeholder_142_ret_type var_59;
    uint64_t var_49;
    struct indirect_placeholder_143_ret_type var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_47;
    struct indirect_placeholder_144_ret_type var_48;
    uint64_t var_39;
    struct indirect_placeholder_145_ret_type var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_211;
    uint64_t var_212;
    uint64_t var_198;
    uint64_t var_199;
    uint64_t var_200;
    uint64_t local_sp_6;
    uint64_t var_201;
    uint64_t rbp_3;
    uint64_t local_sp_15;
    struct indirect_placeholder_146_ret_type var_202;
    uint64_t var_203;
    uint64_t var_204;
    uint64_t var_205;
    uint64_t var_195;
    uint64_t var_196;
    uint64_t r81_3;
    uint64_t rcx_1;
    uint64_t r95_1;
    uint64_t local_sp_7;
    uint64_t var_208;
    uint64_t var_192;
    uint64_t var_193;
    uint64_t var_194;
    uint64_t r81_4;
    uint64_t rcx_2;
    uint64_t r95_2;
    uint64_t local_sp_8;
    uint64_t var_206;
    struct indirect_placeholder_148_ret_type var_207;
    uint64_t var_189;
    uint64_t var_190;
    uint64_t var_191;
    uint64_t r81_5;
    uint64_t rcx_3;
    uint64_t r95_3;
    uint64_t local_sp_9;
    uint64_t var_186;
    uint64_t var_183;
    uint64_t var_184;
    uint64_t var_185;
    uint64_t var_181;
    struct indirect_placeholder_150_ret_type var_182;
    uint64_t var_36;
    uint64_t rbx_1;
    uint64_t var_37;
    struct indirect_placeholder_151_ret_type var_38;
    uint64_t var_32;
    uint64_t var_33;
    struct indirect_placeholder_152_ret_type var_34;
    uint64_t var_35;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t local_sp_13;
    uint64_t var_26;
    uint64_t var_27;
    struct indirect_placeholder_153_ret_type var_28;
    uint64_t var_29;
    uint64_t r15_4;
    struct indirect_placeholder_154_ret_type var_213;
    uint64_t var_214;
    uint64_t var_215;
    uint64_t var_216;
    uint64_t r13_2;
    uint64_t r14_2;
    uint64_t r81_6;
    uint64_t rdi2_2;
    uint64_t r95_6_ph;
    uint64_t r103_0;
    uint64_t rax_2_ph;
    uint64_t r95_4;
    uint64_t rax_1;
    uint64_t local_sp_11;
    uint64_t var_138;
    uint64_t rbx_0;
    uint64_t rbx_1_ph;
    uint64_t r15_6;
    uint64_t r13_3;
    uint64_t r14_3;
    uint64_t r81_7;
    uint64_t rdi2_3;
    uint64_t r103_1;
    uint64_t r95_5;
    uint64_t local_sp_12;
    uint64_t rbx_2;
    uint64_t rbx_3;
    uint64_t var_17;
    uint32_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t r15_7_ph;
    uint64_t r103_2_ph;
    uint64_t local_sp_13_ph;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t r12_0;
    uint64_t r95_7;
    uint64_t local_sp_14;
    uint64_t r15_9;
    uint64_t r13_5;
    uint64_t r14_6;
    uint64_t r81_9;
    uint64_t r103_4;
    uint64_t r12_1;
    uint32_t var_141;
    uint32_t var_139;
    uint32_t var_140;
    uint32_t var_142;
    uint64_t var_143;
    uint64_t var_144;
    uint64_t var_151;
    bool var_152;
    bool var_153;
    uint32_t state_0x8248_0;
    struct indirect_placeholder_156_ret_type var_210;
    uint64_t var_155;
    uint32_t var_156;
    uint64_t var_157;
    struct helper_divq_EAX_wrapper_ret_type var_154;
    uint32_t var_158;
    uint64_t var_159;
    uint32_t var_160;
    uint64_t state_0x9018_0;
    uint32_t state_0x9010_0;
    uint64_t state_0x82d8_0;
    uint32_t state_0x9080_0;
    uint64_t var_161;
    bool var_162;
    bool var_163;
    uint32_t state_0x8248_1;
    uint64_t var_165;
    uint32_t var_166;
    uint64_t var_167;
    struct helper_divq_EAX_wrapper_ret_type var_164;
    uint32_t var_168;
    uint64_t var_169;
    uint32_t var_170;
    uint64_t state_0x9018_1;
    uint32_t state_0x9010_1;
    uint64_t state_0x82d8_1;
    uint32_t state_0x9080_1;
    uint64_t var_171;
    bool var_172;
    bool var_173;
    uint64_t var_175;
    struct helper_divq_EAX_wrapper_ret_type var_174;
    uint64_t var_176;
    uint64_t var_177;
    uint16_t var_178;
    uint64_t rax_3;
    uint64_t var_179;
    uint64_t var_180;
    uint64_t var_145;
    uint64_t var_146;
    uint64_t var_147;
    struct indirect_placeholder_155_ret_type var_148;
    uint64_t var_149;
    uint64_t var_150;
    uint64_t var_209;
    uint64_t var_187;
    struct indirect_placeholder_157_ret_type var_188;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r12();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_r13();
    var_5 = init_r14();
    var_6 = init_rbp();
    var_7 = init_cc_src2();
    var_8 = init_state_0x8248();
    var_9 = init_state_0x9018();
    var_10 = init_state_0x9010();
    var_11 = init_state_0x8408();
    var_12 = init_state_0x8328();
    var_13 = init_state_0x82d8();
    var_14 = init_state_0x9080();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_1;
    *(uint64_t *)(var_0 + (-40L)) = var_6;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_15 = var_0 + (-104L);
    var_16 = *(uint32_t *)4301692UL;
    rcx_0 = 4268687UL;
    r15_8 = var_3;
    r13_4 = var_4;
    r14_5 = var_5;
    r81_8 = r8;
    rbp_2 = var_6;
    rdi2_4 = rdi;
    r103_3 = r10;
    r14_4_ph = 9223372036854775807UL;
    rbp_1_ph = var_6;
    r95_6_ph = r9;
    rbx_2 = var_2;
    r15_7_ph = 0UL;
    r103_2_ph = r10;
    local_sp_13_ph = var_15;
    r12_0 = var_1;
    r95_7 = r9;
    local_sp_14 = var_15;
    state_0x8248_0 = var_8;
    state_0x9018_0 = var_9;
    state_0x9010_0 = var_10;
    state_0x82d8_0 = var_13;
    state_0x9080_0 = var_14;
    rax_3 = 4210032UL;
    if ((long)((uint64_t)var_16 << 32UL) < (long)(rdi << 32UL)) {
        *(uint64_t *)(var_0 + (-80L)) = 0UL;
        var_17 = (uint64_t)var_16;
        var_18 = var_16 ^ (-1);
        *(uint64_t *)(var_0 + (-96L)) = 0UL;
        var_19 = (uint64_t)(var_18 + (uint32_t)rdi);
        var_20 = (var_17 << 3UL) + rsi;
        *(uint64_t *)(var_0 + (-88L)) = 18446744073709551615UL;
        var_21 = var_19 + var_17;
        var_22 = (var_21 << 3UL) + rsi;
        var_23 = var_22 + 8UL;
        r15_8 = 0UL;
        rax_2_ph = var_21;
        rbx_1_ph = var_20;
        r12_0 = var_23;
        r12_1 = var_23;
        while (1U)
            {
                r15_5 = r15_7_ph;
                r14_1 = r14_4_ph;
                rax_2 = rax_2_ph;
                rbp_1 = rbp_1_ph;
                r14_0 = r14_4_ph;
                rbx_1 = rbx_1_ph;
                local_sp_13 = local_sp_13_ph;
                r15_4 = r15_7_ph;
                r14_2 = r14_4_ph;
                r103_0 = r103_2_ph;
                r95_4 = r95_6_ph;
                r15_6 = r15_7_ph;
                r14_3 = r14_4_ph;
                r103_1 = r103_2_ph;
                r95_5 = r95_6_ph;
                while (1U)
                    {
                        var_24 = *(uint64_t *)rbx_1;
                        var_25 = local_sp_13 + (-8L);
                        *(uint64_t *)var_25 = 4216096UL;
                        indirect_placeholder();
                        r13_1 = var_24;
                        rbp_0 = rbp_1;
                        local_sp_10 = var_25;
                        r13_0 = var_24;
                        r13_2 = var_24;
                        r13_3 = var_24;
                        if (rax_2 != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_26 = rax_2 + 1UL;
                        var_27 = local_sp_13 + (-16L);
                        *(uint64_t *)var_27 = 4216122UL;
                        var_28 = indirect_placeholder_153(var_24, 4268626UL);
                        var_29 = var_28.field_0;
                        rbp_2 = var_26;
                        rbp_0 = var_26;
                        rbp_1 = var_26;
                        rbp_1_ph = var_26;
                        rbp_3 = var_26;
                        rax_1 = var_29;
                        local_sp_11 = var_27;
                        if ((uint64_t)(unsigned char)var_29 != 0UL) {
                            loop_state_var = 2U;
                            break;
                        }
                        var_32 = var_28.field_2;
                        var_33 = local_sp_13 + (-24L);
                        *(uint64_t *)var_33 = 4216136UL;
                        var_34 = indirect_placeholder_152(var_32, 4268629UL);
                        var_35 = var_34.field_0;
                        local_sp_12 = var_33;
                        rax_2 = var_35;
                        local_sp_13 = var_33;
                        if ((uint64_t)(unsigned char)var_35 != 0UL) {
                            loop_state_var = 3U;
                            break;
                        }
                        var_36 = rbx_1 + 8UL;
                        *(uint64_t *)4302560UL = var_26;
                        rbx_0 = var_36;
                        rbx_1 = var_36;
                        if (var_22 == rbx_1) {
                            continue;
                        }
                        r81_7 = var_34.field_1;
                        rdi2_3 = var_34.field_2;
                        loop_state_var = 0U;
                        break;
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 2U:
                    {
                        var_30 = var_28.field_1;
                        var_31 = var_28.field_2;
                        *(uint64_t *)4302568UL = var_26;
                        r81_6 = var_30;
                        rdi2_2 = var_31;
                        var_138 = rbx_1 + 8UL;
                        r14_4_ph = r14_2;
                        r95_6_ph = r95_4;
                        rax_2_ph = rax_1;
                        rbx_0 = var_138;
                        rbx_1_ph = var_138;
                        r15_6 = r15_5;
                        r13_3 = r13_2;
                        r14_3 = r14_2;
                        r81_7 = r81_6;
                        rdi2_3 = rdi2_2;
                        r103_1 = r103_0;
                        r95_5 = r95_4;
                        local_sp_12 = local_sp_11;
                        r15_7_ph = r15_5;
                        r103_2_ph = r103_0;
                        local_sp_13_ph = local_sp_11;
                        if (var_22 == rbx_1) {
                            continue;
                        }
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 3U:
                    {
                        var_37 = var_34.field_2;
                        *(uint64_t *)(local_sp_13 + (-32L)) = 4216698UL;
                        var_38 = indirect_placeholder_151(var_37, 4268632UL);
                        if ((uint64_t)(unsigned char)var_38.field_0 != 0UL) {
                            var_39 = local_sp_13 + (-40L);
                            *(uint64_t *)var_39 = 4216804UL;
                            var_40 = indirect_placeholder_145(0UL, var_26, 4268637UL, 4274688UL);
                            var_41 = var_40.field_0;
                            var_42 = var_40.field_1;
                            var_43 = var_40.field_2;
                            var_44 = var_40.field_3;
                            var_45 = var_40.field_4;
                            var_46 = var_40.field_5;
                            *(uint32_t *)4302476UL = (*(uint32_t *)4302476UL | (uint32_t)var_41);
                            r15_5 = var_42;
                            r81_6 = var_43;
                            rdi2_2 = var_44;
                            r103_0 = var_45;
                            r95_4 = var_46;
                            rax_1 = var_41;
                            local_sp_11 = var_39;
                            var_138 = rbx_1 + 8UL;
                            r14_4_ph = r14_2;
                            r95_6_ph = r95_4;
                            rax_2_ph = rax_1;
                            rbx_0 = var_138;
                            rbx_1_ph = var_138;
                            r15_6 = r15_5;
                            r13_3 = r13_2;
                            r14_3 = r14_2;
                            r81_7 = r81_6;
                            rdi2_3 = rdi2_2;
                            r103_1 = r103_0;
                            r95_5 = r95_4;
                            local_sp_12 = local_sp_11;
                            r15_7_ph = r15_5;
                            r103_2_ph = r103_0;
                            local_sp_13_ph = local_sp_11;
                            if (var_22 == rbx_1) {
                                continue;
                            }
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        var_47 = var_38.field_2;
                        *(uint64_t *)(local_sp_13 + (-40L)) = 4216712UL;
                        var_48 = indirect_placeholder_144(var_47, 4268656UL);
                        if ((uint64_t)(unsigned char)var_48.field_0 == 0UL) {
                            var_57 = var_48.field_2;
                            var_58 = local_sp_13 + (-48L);
                            *(uint64_t *)var_58 = 4216726UL;
                            var_59 = indirect_placeholder_142(var_57, 4268681UL);
                            local_sp_5 = var_58;
                            if ((uint64_t)(unsigned char)var_59.field_0 == 0UL) {
                                var_130 = local_sp_5 + (-8L);
                                *(uint64_t *)var_130 = 4217012UL;
                                var_131 = indirect_placeholder_141(0UL, var_26, 4268687UL, 4274368UL);
                                var_132 = var_131.field_0;
                                var_133 = var_131.field_1;
                                var_134 = var_131.field_2;
                                var_135 = var_131.field_3;
                                var_136 = var_131.field_4;
                                var_137 = var_131.field_5;
                                *(uint32_t *)4302468UL = (*(uint32_t *)4302468UL | (uint32_t)var_132);
                                r15_5 = var_133;
                                r13_2 = r13_0;
                                r14_2 = r14_0;
                                r81_6 = var_134;
                                rdi2_2 = var_135;
                                r103_0 = var_136;
                                r95_4 = var_137;
                                rax_1 = var_132;
                                local_sp_11 = var_130;
                            } else {
                                var_60 = var_59.field_2;
                                *(uint64_t *)(local_sp_13 + (-56L)) = 4216744UL;
                                var_61 = indirect_placeholder_140(var_60, 4268707UL);
                                if ((uint64_t)(unsigned char)var_61.field_0 != 0UL) {
                                    var_62 = local_sp_13 + (-64L);
                                    *(uint64_t *)var_62 = 4216771UL;
                                    var_63 = indirect_placeholder_138(1UL, var_26, 4268714UL, 4274304UL);
                                    var_64 = var_63.field_0;
                                    var_65 = var_63.field_1;
                                    var_66 = var_63.field_2;
                                    var_67 = var_63.field_3;
                                    var_68 = var_63.field_4;
                                    var_69 = var_63.field_5;
                                    *(uint32_t *)4301548UL = (uint32_t)var_64;
                                    r15_5 = var_65;
                                    r81_6 = var_66;
                                    rdi2_2 = var_67;
                                    r103_0 = var_68;
                                    r95_4 = var_69;
                                    rax_1 = var_64;
                                    local_sp_11 = var_62;
                                    var_138 = rbx_1 + 8UL;
                                    r14_4_ph = r14_2;
                                    r95_6_ph = r95_4;
                                    rax_2_ph = rax_1;
                                    rbx_0 = var_138;
                                    rbx_1_ph = var_138;
                                    r15_6 = r15_5;
                                    r13_3 = r13_2;
                                    r14_3 = r14_2;
                                    r81_7 = r81_6;
                                    rdi2_3 = rdi2_2;
                                    r103_1 = r103_0;
                                    r95_5 = r95_4;
                                    local_sp_12 = local_sp_11;
                                    r15_7_ph = r15_5;
                                    r103_2_ph = r103_0;
                                    local_sp_13_ph = local_sp_11;
                                    if (var_22 == rbx_1) {
                                        continue;
                                    }
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                var_70 = local_sp_13 + (-12L);
                                *(uint32_t *)var_70 = 0U;
                                *(uint64_t *)(local_sp_13 + (-64L)) = 4216869UL;
                                var_71 = indirect_placeholder_139(var_26, var_70);
                                var_72 = var_71.field_0;
                                var_73 = var_71.field_1;
                                var_74 = var_71.field_2;
                                var_75 = var_71.field_3;
                                var_76 = var_71.field_6;
                                var_77 = local_sp_13 + (-72L);
                                *(uint64_t *)var_77 = 4216885UL;
                                var_78 = indirect_placeholder_137(var_74, 4268735UL);
                                var_79 = var_78.field_0;
                                var_80 = var_78.field_2;
                                r15_0 = var_73;
                                r15_1 = var_73;
                                r15_2 = var_73;
                                rdi2_0 = var_80;
                                local_sp_1 = var_77;
                                r13_1 = var_74;
                                r14_1 = var_75;
                                r13_0 = var_74;
                                r14_0 = var_75;
                                r15_4 = var_73;
                                r13_2 = var_74;
                                r14_2 = var_75;
                                r103_0 = var_76;
                                r95_4 = var_72;
                                if ((uint64_t)(unsigned char)var_79 == 0UL) {
                                    var_85 = local_sp_13 + (-80L);
                                    *(uint64_t *)var_85 = 4217033UL;
                                    var_86 = indirect_placeholder_136(var_80, 4268739UL);
                                    var_87 = var_86.field_0;
                                    var_88 = var_86.field_2;
                                    r15_0 = var_72;
                                    rdi2_0 = var_88;
                                    local_sp_1 = var_85;
                                    if ((uint64_t)(unsigned char)var_87 == 0UL) {
                                        var_93 = local_sp_13 + (-88L);
                                        var_94 = (uint64_t *)var_93;
                                        *var_94 = 4217262UL;
                                        var_95 = indirect_placeholder_133(var_88, 4268744UL);
                                        var_96 = var_95.field_0;
                                        var_97 = var_95.field_2;
                                        rdi2_0 = var_97;
                                        local_sp_1 = var_93;
                                        if ((uint64_t)(unsigned char)var_96 == 0UL) {
                                            var_102 = local_sp_13 + (-96L);
                                            var_103 = (uint64_t *)var_102;
                                            *var_103 = 4217280UL;
                                            var_104 = indirect_placeholder_129(var_97, 4268743UL);
                                            var_105 = var_104.field_0;
                                            var_106 = var_104.field_2;
                                            local_sp_2 = var_102;
                                            rdi2_1 = var_106;
                                            local_sp_3 = var_102;
                                            if ((uint64_t)(unsigned char)var_105 == 0UL) {
                                                var_108 = local_sp_13 + (-104L);
                                                var_109 = (uint64_t *)var_108;
                                                *var_109 = 4217098UL;
                                                var_110 = indirect_placeholder_132(var_106, 4268345UL);
                                                var_111 = var_110.field_0;
                                                var_112 = var_110.field_2;
                                                rdi2_1 = var_112;
                                                local_sp_3 = var_108;
                                                if ((uint64_t)(unsigned char)var_111 == 0UL) {
                                                    var_113 = var_110.field_1;
                                                    *var_103 = var_72;
                                                    r81_2 = var_113;
                                                } else {
                                                    var_114 = local_sp_13 + (-112L);
                                                    *(uint64_t *)var_114 = 4217116UL;
                                                    var_115 = indirect_placeholder_131(var_112, 4268143UL);
                                                    var_116 = var_115.field_0;
                                                    var_117 = var_115.field_2;
                                                    rdi2_1 = var_117;
                                                    local_sp_3 = var_114;
                                                    if ((uint64_t)(unsigned char)var_116 == 0UL) {
                                                        var_118 = var_115.field_1;
                                                        *var_94 = var_72;
                                                        r81_2 = var_118;
                                                    } else {
                                                        var_119 = local_sp_13 + (-120L);
                                                        *(uint64_t *)var_119 = 4217134UL;
                                                        var_120 = indirect_placeholder_130(var_117, 4268747UL);
                                                        local_sp_3 = var_119;
                                                        local_sp_10 = var_119;
                                                        if ((uint64_t)(unsigned char)var_120.field_0 != 0UL) {
                                                            loop_state_var = 1U;
                                                            switch_state_var = 1;
                                                            break;
                                                        }
                                                        var_121 = var_120.field_2;
                                                        var_122 = var_120.field_1;
                                                        *var_109 = var_72;
                                                        r81_2 = var_122;
                                                        rdi2_1 = var_121;
                                                    }
                                                }
                                                var_123 = *(uint32_t *)(local_sp_3 + 44UL);
                                                var_124 = (uint64_t)var_123;
                                                r15_3 = r15_2;
                                                r15_5 = r15_2;
                                                local_sp_4 = local_sp_3;
                                                r81_6 = r81_2;
                                                rdi2_2 = rdi2_1;
                                                rax_1 = var_124;
                                                local_sp_11 = local_sp_3;
                                                if (var_123 != 0U) {
                                                    var_138 = rbx_1 + 8UL;
                                                    r14_4_ph = r14_2;
                                                    r95_6_ph = r95_4;
                                                    rax_2_ph = rax_1;
                                                    rbx_0 = var_138;
                                                    rbx_1_ph = var_138;
                                                    r15_6 = r15_5;
                                                    r13_3 = r13_2;
                                                    r14_3 = r14_2;
                                                    r81_7 = r81_6;
                                                    rdi2_3 = rdi2_2;
                                                    r103_1 = r103_0;
                                                    r95_5 = r95_4;
                                                    local_sp_12 = local_sp_11;
                                                    r15_7_ph = r15_5;
                                                    r103_2_ph = r103_0;
                                                    local_sp_13_ph = local_sp_11;
                                                    if (var_22 == rbx_1) {
                                                        continue;
                                                    }
                                                    loop_state_var = 0U;
                                                    switch_state_var = 1;
                                                    break;
                                                }
                                            }
                                            var_107 = var_104.field_1;
                                            *(uint64_t *)4302528UL = var_72;
                                            r81_2 = var_107;
                                            if (var_72 != 0UL) {
                                                var_123 = *(uint32_t *)(local_sp_3 + 44UL);
                                                var_124 = (uint64_t)var_123;
                                                r15_3 = r15_2;
                                                r15_5 = r15_2;
                                                local_sp_4 = local_sp_3;
                                                r81_6 = r81_2;
                                                rdi2_2 = rdi2_1;
                                                rax_1 = var_124;
                                                local_sp_11 = local_sp_3;
                                                if (var_123 != 0U) {
                                                    var_138 = rbx_1 + 8UL;
                                                    r14_4_ph = r14_2;
                                                    r95_6_ph = r95_4;
                                                    rax_2_ph = rax_1;
                                                    rbx_0 = var_138;
                                                    rbx_1_ph = var_138;
                                                    r15_6 = r15_5;
                                                    r13_3 = r13_2;
                                                    r14_3 = r14_2;
                                                    r81_7 = r81_6;
                                                    rdi2_3 = rdi2_2;
                                                    r103_1 = r103_0;
                                                    r95_5 = r95_4;
                                                    local_sp_12 = local_sp_11;
                                                    r15_7_ph = r15_5;
                                                    r103_2_ph = r103_0;
                                                    local_sp_13_ph = local_sp_11;
                                                    if (var_22 == rbx_1) {
                                                        continue;
                                                    }
                                                    loop_state_var = 0U;
                                                    switch_state_var = 1;
                                                    break;
                                                }
                                            }
                                            *(uint32_t *)(local_sp_2 + 44UL) = 4U;
                                            r15_3 = r15_1;
                                            local_sp_4 = local_sp_2;
                                        } else {
                                            var_98 = var_95.field_1;
                                            var_99 = 18446744073709551612UL - (-4L);
                                            var_100 = (var_99 > var_75) ? var_75 : var_99;
                                            r81_1 = var_98;
                                            rax_0 = var_100;
                                            r15_1 = r15_0;
                                            r15_2 = r15_0;
                                            local_sp_2 = local_sp_1;
                                            r15_3 = r15_0;
                                            r81_2 = r81_1;
                                            rdi2_1 = rdi2_0;
                                            local_sp_3 = local_sp_1;
                                            local_sp_4 = local_sp_1;
                                            if (var_72 == 0UL) {
                                                var_101 = helper_cc_compute_c_wrapper(rax_0 - var_72, var_72, var_7, 17U);
                                                if (var_101 != 0UL) {
                                                    var_123 = *(uint32_t *)(local_sp_3 + 44UL);
                                                    var_124 = (uint64_t)var_123;
                                                    r15_3 = r15_2;
                                                    r15_5 = r15_2;
                                                    local_sp_4 = local_sp_3;
                                                    r81_6 = r81_2;
                                                    rdi2_2 = rdi2_1;
                                                    rax_1 = var_124;
                                                    local_sp_11 = local_sp_3;
                                                    if (var_123 != 0U) {
                                                        var_138 = rbx_1 + 8UL;
                                                        r14_4_ph = r14_2;
                                                        r95_6_ph = r95_4;
                                                        rax_2_ph = rax_1;
                                                        rbx_0 = var_138;
                                                        rbx_1_ph = var_138;
                                                        r15_6 = r15_5;
                                                        r13_3 = r13_2;
                                                        r14_3 = r14_2;
                                                        r81_7 = r81_6;
                                                        rdi2_3 = rdi2_2;
                                                        r103_1 = r103_0;
                                                        r95_5 = r95_4;
                                                        local_sp_12 = local_sp_11;
                                                        r15_7_ph = r15_5;
                                                        r103_2_ph = r103_0;
                                                        local_sp_13_ph = local_sp_11;
                                                        if (var_22 == rbx_1) {
                                                            continue;
                                                        }
                                                        loop_state_var = 0U;
                                                        switch_state_var = 1;
                                                        break;
                                                    }
                                                }
                                                *(uint32_t *)(local_sp_1 + 44UL) = 1U;
                                            } else {
                                                *(uint32_t *)(local_sp_2 + 44UL) = 4U;
                                                r15_3 = r15_1;
                                                local_sp_4 = local_sp_2;
                                                *(uint64_t *)(local_sp_4 + (-8L)) = 4216949UL;
                                                var_125 = indirect_placeholder_135(var_23, rbx_1, r15_3, var_74, var_75, var_26, var_26);
                                                var_126 = var_125.field_0;
                                                var_127 = var_125.field_6;
                                                var_128 = (*(uint32_t *)(local_sp_4 + 36UL) == 1U) ? 75UL : 0UL;
                                                var_129 = local_sp_4 + (-16L);
                                                *(uint64_t *)var_129 = 4216990UL;
                                                indirect_placeholder_134(0UL, var_126, 4268572UL, 1UL, 4268753UL, var_128, var_127);
                                                local_sp_5 = var_129;
                                            }
                                        }
                                    } else {
                                        var_89 = var_86.field_1;
                                        var_90 = *(uint64_t *)4302552UL;
                                        *(uint64_t *)4302536UL = var_72;
                                        var_91 = 0UL - var_90;
                                        var_92 = (var_75 < var_91) ? var_75 : var_91;
                                        r81_1 = var_89;
                                        rax_0 = var_92;
                                        r15_1 = r15_0;
                                        r15_2 = r15_0;
                                        local_sp_2 = local_sp_1;
                                        r15_3 = r15_0;
                                        r81_2 = r81_1;
                                        rdi2_1 = rdi2_0;
                                        local_sp_3 = local_sp_1;
                                        local_sp_4 = local_sp_1;
                                        if (var_72 != 0UL) {
                                            *(uint32_t *)(local_sp_2 + 44UL) = 4U;
                                            r15_3 = r15_1;
                                            local_sp_4 = local_sp_2;
                                            *(uint64_t *)(local_sp_4 + (-8L)) = 4216949UL;
                                            var_125 = indirect_placeholder_135(var_23, rbx_1, r15_3, var_74, var_75, var_26, var_26);
                                            var_126 = var_125.field_0;
                                            var_127 = var_125.field_6;
                                            var_128 = (*(uint32_t *)(local_sp_4 + 36UL) == 1U) ? 75UL : 0UL;
                                            var_129 = local_sp_4 + (-16L);
                                            *(uint64_t *)var_129 = 4216990UL;
                                            indirect_placeholder_134(0UL, var_126, 4268572UL, 1UL, 4268753UL, var_128, var_127);
                                            local_sp_5 = var_129;
                                            var_130 = local_sp_5 + (-8L);
                                            *(uint64_t *)var_130 = 4217012UL;
                                            var_131 = indirect_placeholder_141(0UL, var_26, 4268687UL, 4274368UL);
                                            var_132 = var_131.field_0;
                                            var_133 = var_131.field_1;
                                            var_134 = var_131.field_2;
                                            var_135 = var_131.field_3;
                                            var_136 = var_131.field_4;
                                            var_137 = var_131.field_5;
                                            *(uint32_t *)4302468UL = (*(uint32_t *)4302468UL | (uint32_t)var_132);
                                            r15_5 = var_133;
                                            r13_2 = r13_0;
                                            r14_2 = r14_0;
                                            r81_6 = var_134;
                                            rdi2_2 = var_135;
                                            r103_0 = var_136;
                                            r95_4 = var_137;
                                            rax_1 = var_132;
                                            local_sp_11 = var_130;
                                            var_138 = rbx_1 + 8UL;
                                            r14_4_ph = r14_2;
                                            r95_6_ph = r95_4;
                                            rax_2_ph = rax_1;
                                            rbx_0 = var_138;
                                            rbx_1_ph = var_138;
                                            r15_6 = r15_5;
                                            r13_3 = r13_2;
                                            r14_3 = r14_2;
                                            r81_7 = r81_6;
                                            rdi2_3 = rdi2_2;
                                            r103_1 = r103_0;
                                            r95_5 = r95_4;
                                            local_sp_12 = local_sp_11;
                                            r15_7_ph = r15_5;
                                            r103_2_ph = r103_0;
                                            local_sp_13_ph = local_sp_11;
                                            if (var_22 == rbx_1) {
                                                continue;
                                            }
                                            loop_state_var = 0U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                        var_101 = helper_cc_compute_c_wrapper(rax_0 - var_72, var_72, var_7, 17U);
                                        if (var_101 != 0UL) {
                                            var_123 = *(uint32_t *)(local_sp_3 + 44UL);
                                            var_124 = (uint64_t)var_123;
                                            r15_3 = r15_2;
                                            r15_5 = r15_2;
                                            local_sp_4 = local_sp_3;
                                            r81_6 = r81_2;
                                            rdi2_2 = rdi2_1;
                                            rax_1 = var_124;
                                            local_sp_11 = local_sp_3;
                                            if (var_123 != 0U) {
                                                var_138 = rbx_1 + 8UL;
                                                r14_4_ph = r14_2;
                                                r95_6_ph = r95_4;
                                                rax_2_ph = rax_1;
                                                rbx_0 = var_138;
                                                rbx_1_ph = var_138;
                                                r15_6 = r15_5;
                                                r13_3 = r13_2;
                                                r14_3 = r14_2;
                                                r81_7 = r81_6;
                                                rdi2_3 = rdi2_2;
                                                r103_1 = r103_0;
                                                r95_5 = r95_4;
                                                local_sp_12 = local_sp_11;
                                                r15_7_ph = r15_5;
                                                r103_2_ph = r103_0;
                                                local_sp_13_ph = local_sp_11;
                                                if (var_22 == rbx_1) {
                                                    continue;
                                                }
                                                loop_state_var = 0U;
                                                switch_state_var = 1;
                                                break;
                                            }
                                        }
                                        *(uint32_t *)(local_sp_1 + 44UL) = 1U;
                                    }
                                } else {
                                    var_81 = var_78.field_1;
                                    var_82 = *(uint64_t *)4302552UL;
                                    *(uint64_t *)4302544UL = var_72;
                                    var_83 = 18446744073709551612UL - (-4L);
                                    var_84 = (var_83 > var_75) ? var_75 : var_83;
                                    r81_1 = var_81;
                                    rax_0 = var_84;
                                    r15_1 = r15_0;
                                    r15_2 = r15_0;
                                    local_sp_2 = local_sp_1;
                                    r15_3 = r15_0;
                                    r81_2 = r81_1;
                                    rdi2_1 = rdi2_0;
                                    local_sp_3 = local_sp_1;
                                    local_sp_4 = local_sp_1;
                                    if (var_72 != 0UL) {
                                        *(uint32_t *)(local_sp_2 + 44UL) = 4U;
                                        r15_3 = r15_1;
                                        local_sp_4 = local_sp_2;
                                        *(uint64_t *)(local_sp_4 + (-8L)) = 4216949UL;
                                        var_125 = indirect_placeholder_135(var_23, rbx_1, r15_3, var_74, var_75, var_26, var_26);
                                        var_126 = var_125.field_0;
                                        var_127 = var_125.field_6;
                                        var_128 = (*(uint32_t *)(local_sp_4 + 36UL) == 1U) ? 75UL : 0UL;
                                        var_129 = local_sp_4 + (-16L);
                                        *(uint64_t *)var_129 = 4216990UL;
                                        indirect_placeholder_134(0UL, var_126, 4268572UL, 1UL, 4268753UL, var_128, var_127);
                                        local_sp_5 = var_129;
                                        var_130 = local_sp_5 + (-8L);
                                        *(uint64_t *)var_130 = 4217012UL;
                                        var_131 = indirect_placeholder_141(0UL, var_26, 4268687UL, 4274368UL);
                                        var_132 = var_131.field_0;
                                        var_133 = var_131.field_1;
                                        var_134 = var_131.field_2;
                                        var_135 = var_131.field_3;
                                        var_136 = var_131.field_4;
                                        var_137 = var_131.field_5;
                                        *(uint32_t *)4302468UL = (*(uint32_t *)4302468UL | (uint32_t)var_132);
                                        r15_5 = var_133;
                                        r13_2 = r13_0;
                                        r14_2 = r14_0;
                                        r81_6 = var_134;
                                        rdi2_2 = var_135;
                                        r103_0 = var_136;
                                        r95_4 = var_137;
                                        rax_1 = var_132;
                                        local_sp_11 = var_130;
                                        var_138 = rbx_1 + 8UL;
                                        r14_4_ph = r14_2;
                                        r95_6_ph = r95_4;
                                        rax_2_ph = rax_1;
                                        rbx_0 = var_138;
                                        rbx_1_ph = var_138;
                                        r15_6 = r15_5;
                                        r13_3 = r13_2;
                                        r14_3 = r14_2;
                                        r81_7 = r81_6;
                                        rdi2_3 = rdi2_2;
                                        r103_1 = r103_0;
                                        r95_5 = r95_4;
                                        local_sp_12 = local_sp_11;
                                        r15_7_ph = r15_5;
                                        r103_2_ph = r103_0;
                                        local_sp_13_ph = local_sp_11;
                                        if (var_22 == rbx_1) {
                                            continue;
                                        }
                                        loop_state_var = 0U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    var_101 = helper_cc_compute_c_wrapper(rax_0 - var_72, var_72, var_7, 17U);
                                    if (var_101 != 0UL) {
                                        var_123 = *(uint32_t *)(local_sp_3 + 44UL);
                                        var_124 = (uint64_t)var_123;
                                        r15_3 = r15_2;
                                        r15_5 = r15_2;
                                        local_sp_4 = local_sp_3;
                                        r81_6 = r81_2;
                                        rdi2_2 = rdi2_1;
                                        rax_1 = var_124;
                                        local_sp_11 = local_sp_3;
                                        if (var_123 != 0U) {
                                            var_138 = rbx_1 + 8UL;
                                            r14_4_ph = r14_2;
                                            r95_6_ph = r95_4;
                                            rax_2_ph = rax_1;
                                            rbx_0 = var_138;
                                            rbx_1_ph = var_138;
                                            r15_6 = r15_5;
                                            r13_3 = r13_2;
                                            r14_3 = r14_2;
                                            r81_7 = r81_6;
                                            rdi2_3 = rdi2_2;
                                            r103_1 = r103_0;
                                            r95_5 = r95_4;
                                            local_sp_12 = local_sp_11;
                                            r15_7_ph = r15_5;
                                            r103_2_ph = r103_0;
                                            local_sp_13_ph = local_sp_11;
                                            if (var_22 == rbx_1) {
                                                continue;
                                            }
                                            loop_state_var = 0U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                    }
                                    *(uint32_t *)(local_sp_1 + 44UL) = 1U;
                                }
                            }
                        } else {
                            var_49 = local_sp_13 + (-48L);
                            *(uint64_t *)var_49 = 4216836UL;
                            var_50 = indirect_placeholder_143(0UL, var_26, 4268662UL, 4274368UL);
                            var_51 = var_50.field_0;
                            var_52 = var_50.field_1;
                            var_53 = var_50.field_2;
                            var_54 = var_50.field_3;
                            var_55 = var_50.field_4;
                            var_56 = var_50.field_5;
                            *(uint32_t *)4302472UL = (*(uint32_t *)4302472UL | (uint32_t)var_51);
                            r15_5 = var_52;
                            r81_6 = var_53;
                            rdi2_2 = var_54;
                            r103_0 = var_55;
                            r95_4 = var_56;
                            rax_1 = var_51;
                            local_sp_11 = var_49;
                        }
                    }
                    break;
                  case 0U:
                    {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 1U:
                    {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
        switch (loop_state_var) {
          case 1U:
            {
                *(uint64_t *)(local_sp_10 + (-8L)) = 4217515UL;
                var_213 = indirect_placeholder_154(var_23, rbx_1, r15_4, r13_1, r14_1, rbp_0, r13_1);
                var_214 = var_213.field_0;
                var_215 = var_213.field_1;
                var_216 = var_213.field_6;
                *(uint64_t *)(local_sp_10 + (-16L)) = 4217534UL;
                indirect_placeholder_128(0UL, var_215, 4268602UL, 0UL, var_214, 0UL, var_216);
                *(uint64_t *)(local_sp_10 + (-24L)) = 4217544UL;
                indirect_placeholder_96(var_23, rbp_0, 1UL);
                abort();
            }
            break;
          case 0U:
            {
                r95_8 = r95_5;
                rdi2_5 = rdi2_3;
                r13_4 = r13_3;
                r14_5 = r14_3;
                r81_8 = r81_7;
                rdi2_4 = rdi2_3;
                r103_3 = r103_1;
                local_sp_15 = local_sp_12;
                rbx_2 = rbx_0;
                rbx_3 = rbx_0;
                r95_7 = r95_5;
                local_sp_14 = local_sp_12;
                r15_9 = r15_6;
                r13_5 = r13_3;
                r14_6 = r14_3;
                r81_9 = r81_7;
                r103_4 = r103_1;
                if (r15_6 == 0UL) {
                    *(uint64_t *)4302536UL = r15_6;
                    *(uint64_t *)4302544UL = r15_6;
                } else {
                    *(uint32_t *)4302476UL = (*(uint32_t *)4302476UL | 2048U);
                    r95_8 = r95_7;
                    rdi2_5 = rdi2_4;
                    rbp_3 = rbp_2;
                    local_sp_15 = local_sp_14;
                    rbx_3 = rbx_2;
                    r15_9 = r15_8;
                    r13_5 = r13_4;
                    r14_6 = r14_5;
                    r81_9 = r81_8;
                    r103_4 = r103_3;
                    r12_1 = r12_0;
                    if (*(uint64_t *)4302544UL == 0UL) {
                        *(uint64_t *)4302544UL = 512UL;
                    }
                    if (*(uint64_t *)4302536UL == 0UL) {
                        *(uint64_t *)4302536UL = 512UL;
                    }
                }
            }
            break;
        }
    } else {
        *(uint64_t *)(var_0 + (-88L)) = 18446744073709551615UL;
        *(uint64_t *)(var_0 + (-80L)) = 0UL;
        *(uint64_t *)(var_0 + (-96L)) = 0UL;
        *(uint32_t *)4302476UL = (*(uint32_t *)4302476UL | 2048U);
        r95_8 = r95_7;
        rdi2_5 = rdi2_4;
        rbp_3 = rbp_2;
        local_sp_15 = local_sp_14;
        rbx_3 = rbx_2;
        r15_9 = r15_8;
        r13_5 = r13_4;
        r14_6 = r14_5;
        r81_9 = r81_8;
        r103_4 = r103_3;
        r12_1 = r12_0;
        if (*(uint64_t *)4302544UL == 0UL) {
            *(uint64_t *)4302544UL = 512UL;
        }
        if (*(uint64_t *)4302536UL == 0UL) {
            *(uint64_t *)4302536UL = 512UL;
        }
        local_sp_16 = local_sp_15;
        local_sp_17 = local_sp_15;
        r81_3 = r81_9;
        r95_1 = r95_8;
        r81_4 = r81_9;
        r95_2 = r95_8;
        r81_5 = r81_9;
        r95_3 = r95_8;
        if (*(uint64_t *)4302528UL == 0UL) {
            *(uint32_t *)4302476UL = (*(uint32_t *)4302476UL & (-25));
        }
        var_139 = *(uint32_t *)4302472UL;
        var_141 = var_139;
        if ((var_139 & 1052672U) == 0U) {
            var_140 = var_139 | 1052672U;
            *(uint32_t *)4302472UL = var_140;
            var_141 = var_140;
        }
        var_142 = *(uint32_t *)4302468UL;
        var_143 = (uint64_t)var_142;
        if ((var_143 & 1UL) != 0UL) {
            var_187 = local_sp_17 + (-8L);
            *(uint64_t *)var_187 = 4217592UL;
            var_188 = indirect_placeholder_157(r12_1, rbx_3, r15_9, r13_5, r14_6, rbp_3, 4268768UL);
            r81_0 = var_188.field_0;
            r95_0 = var_188.field_6;
            local_sp_0 = var_187;
            *(uint64_t *)(local_sp_0 + (-8L)) = 4217616UL;
            indirect_placeholder_127(0UL, r81_0, 4268572UL, 0UL, rcx_0, 0UL, r95_0);
            *(uint64_t *)(local_sp_0 + (-16L)) = 4217626UL;
            indirect_placeholder_96(r12_1, rbp_3, 1UL);
            abort();
        }
        var_144 = (uint64_t)var_141;
        rcx_0 = 4268662UL;
        rcx_1 = var_144;
        rcx_2 = var_144;
        rcx_3 = var_144;
        if ((var_144 & 16UL) == 0UL) {
            if ((var_143 & 12UL) == 0UL) {
                var_151 = *(uint64_t *)(local_sp_15 + 8UL);
                var_152 = ((var_144 & 8UL) == 0UL);
                var_153 = (var_151 == 0UL);
                if (var_152) {
                    if (var_153) {
                        *(uint64_t *)4302520UL = var_151;
                    }
                } else {
                    if (var_153) {
                        var_154 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), *(uint64_t *)4302544UL, 4216286UL, var_151, rbx_3, r81_9, rbp_3, 0UL, rdi2_5, var_144, r103_4, var_143, r95_8, var_8, var_9, var_10, var_11, var_12, var_13, var_14);
                        var_155 = var_154.field_4;
                        var_156 = var_154.field_6;
                        var_157 = var_154.field_7;
                        var_158 = var_154.field_8;
                        var_159 = var_154.field_9;
                        var_160 = var_154.field_10;
                        *(uint64_t *)4302520UL = var_154.field_1;
                        *(uint64_t *)4302512UL = var_155;
                        state_0x8248_0 = var_156;
                        state_0x9018_0 = var_157;
                        state_0x9010_0 = var_158;
                        state_0x82d8_0 = var_159;
                        state_0x9080_0 = var_160;
                    }
                }
                var_161 = *(uint64_t *)(local_sp_15 + 16UL);
                var_162 = ((var_144 & 4UL) == 0UL);
                var_163 = (var_161 == 18446744073709551615UL);
                state_0x8248_1 = state_0x8248_0;
                state_0x9018_1 = state_0x9018_0;
                state_0x9010_1 = state_0x9010_0;
                state_0x82d8_1 = state_0x82d8_0;
                state_0x9080_1 = state_0x9080_0;
                if (var_162) {
                    if (var_163) {
                        *(uint64_t *)4301552UL = var_161;
                    }
                } else {
                    if (var_163) {
                        var_164 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), *(uint64_t *)4302544UL, 4216329UL, var_161, rbx_3, r81_9, rbp_3, 0UL, rdi2_5, var_144, r103_4, var_143, r95_8, state_0x8248_0, state_0x9018_0, state_0x9010_0, var_11, var_12, state_0x82d8_0, state_0x9080_0);
                        var_165 = var_164.field_4;
                        var_166 = var_164.field_6;
                        var_167 = var_164.field_7;
                        var_168 = var_164.field_8;
                        var_169 = var_164.field_9;
                        var_170 = var_164.field_10;
                        *(uint64_t *)4301552UL = var_164.field_1;
                        *(uint64_t *)4302480UL = var_165;
                        state_0x8248_1 = var_166;
                        state_0x9018_1 = var_167;
                        state_0x9010_1 = var_168;
                        state_0x82d8_1 = var_169;
                        state_0x9080_1 = var_170;
                    }
                }
                var_171 = *(uint64_t *)(local_sp_15 + 24UL);
                var_172 = ((var_143 & 16UL) == 0UL);
                var_173 = (var_171 == 0UL);
                if (var_172) {
                    if (var_173) {
                        *(uint64_t *)4302504UL = var_171;
                    }
                } else {
                    if (var_173) {
                        var_174 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), *(uint64_t *)4302536UL, 4216372UL, var_171, rbx_3, r81_9, rbp_3, 0UL, rdi2_5, var_144, r103_4, var_143, r95_8, state_0x8248_1, state_0x9018_1, state_0x9010_1, var_11, var_12, state_0x82d8_1, state_0x9080_1);
                        var_175 = var_174.field_4;
                        *(uint64_t *)4302504UL = var_174.field_1;
                        *(uint64_t *)4302496UL = var_175;
                    }
                }
                var_176 = (uint64_t)*(uint32_t *)4302476UL;
                var_177 = var_144 & 1UL;
                var_178 = (uint16_t)var_176;
                if (((uint64_t)(var_178 & (unsigned short)2048U) | var_177) == 0UL) {
                    *(unsigned char *)4302376UL = (unsigned char)'\x00';
                    rax_3 = (var_177 == 0UL) ? 4210032UL : 4210304UL;
                } else {
                    if (*(uint64_t *)4302520UL == 0UL) {
                        *(unsigned char *)4302376UL = (unsigned char)'\x01';
                    } else {
                        if ((*(uint64_t *)4301552UL + (-1L)) > 18446744073709551613UL) {
                            *(unsigned char *)4302376UL = (unsigned char)'\x01';
                        } else {
                            if ((uint32_t)(((uint16_t)var_142 | (uint16_t)var_141) & (unsigned short)16384U) == 0U) {
                                *(unsigned char *)4302376UL = (unsigned char)'\x00';
                            } else {
                                *(unsigned char *)4302376UL = (unsigned char)'\x01';
                            }
                        }
                    }
                }
                *(uint64_t *)4302176UL = rax_3;
                var_179 = var_176 & 7UL;
                *(uint32_t *)4302472UL = (var_141 & (-2));
                *(uint64_t *)(local_sp_15 + (-8L)) = 4216474UL;
                var_180 = indirect_placeholder_7(var_179);
                if ((uint64_t)(unsigned char)var_180 == 0UL) {
                    var_183 = var_176 & 24UL;
                    var_184 = local_sp_15 + (-16L);
                    *(uint64_t *)var_184 = 4216492UL;
                    var_185 = indirect_placeholder_7(var_183);
                    local_sp_9 = var_184;
                    if ((uint64_t)(unsigned char)var_185 != 0UL) {
                        var_189 = var_176 & 96UL;
                        var_190 = local_sp_15 + (-24L);
                        *(uint64_t *)var_190 = 4216510UL;
                        var_191 = indirect_placeholder_7(var_189);
                        local_sp_8 = var_190;
                        if ((uint64_t)(unsigned char)var_191 != 0UL) {
                            var_206 = local_sp_8 + (-8L);
                            *(uint64_t *)var_206 = 4217664UL;
                            var_207 = indirect_placeholder_148(0UL, r81_4, 4273232UL, 1UL, rcx_2, 0UL, r95_2);
                            r81_3 = var_207.field_1;
                            rcx_1 = var_207.field_3;
                            r95_1 = var_207.field_4;
                            local_sp_7 = var_206;
                            var_208 = local_sp_7 + (-8L);
                            *(uint64_t *)var_208 = 4217683UL;
                            indirect_placeholder_147(0UL, r81_3, 4273264UL, 1UL, rcx_1, 0UL, r95_1);
                            local_sp_16 = var_208;
                            var_209 = local_sp_16 + (-8L);
                            *(uint64_t *)var_209 = 4217693UL;
                            var_210 = indirect_placeholder_156(r12_1, rbx_3, r15_9, r13_5, r14_6, rbp_3, 4268778UL);
                            r81_0 = var_210.field_0;
                            r95_0 = var_210.field_6;
                            local_sp_0 = var_209;
                            *(uint64_t *)(local_sp_0 + (-8L)) = 4217616UL;
                            indirect_placeholder_127(0UL, r81_0, 4268572UL, 0UL, rcx_0, 0UL, r95_0);
                            *(uint64_t *)(local_sp_0 + (-16L)) = 4217626UL;
                            indirect_placeholder_96(r12_1, rbp_3, 1UL);
                            abort();
                        }
                        var_192 = (uint64_t)(var_178 & (unsigned short)12288U);
                        var_193 = local_sp_15 + (-32L);
                        *(uint64_t *)var_193 = 4216531UL;
                        var_194 = indirect_placeholder_7(var_192);
                        local_sp_7 = var_193;
                        if ((uint64_t)(unsigned char)var_194 != 0UL) {
                            var_195 = (uint64_t)((uint16_t)var_144 & (unsigned short)16386U);
                            var_196 = local_sp_15 + (-40L);
                            *(uint64_t *)var_196 = 4216552UL;
                            var_197 = indirect_placeholder_7(var_195);
                            local_sp_6 = var_196;
                            if ((uint64_t)(unsigned char)var_197 != 0UL) {
                                var_198 = (uint64_t)((uint16_t)var_143 & (unsigned short)16386U);
                                var_199 = local_sp_15 + (-48L);
                                *(uint64_t *)var_199 = 4216573UL;
                                var_200 = indirect_placeholder_7(var_198);
                                local_sp_6 = var_199;
                                if ((uint64_t)(unsigned char)var_200 != 0UL) {
                                    if ((var_144 & 2UL) == 0UL) {
                                        var_211 = *(uint64_t *)4301552UL | *(uint64_t *)4302480UL;
                                        *(unsigned char *)4302187UL = (unsigned char)'\x01';
                                        *(unsigned char *)4302185UL = (var_211 == 0UL);
                                        *(uint32_t *)4302472UL = (var_141 & (-4));
                                    }
                                    if ((var_143 & 2UL) != 0UL) {
                                        var_212 = *(uint64_t *)4301552UL | *(uint64_t *)4302480UL;
                                        *(unsigned char *)4302186UL = (unsigned char)'\x01';
                                        *(unsigned char *)4302184UL = (var_212 == 0UL);
                                        *(uint32_t *)4302468UL = (var_142 & (-3));
                                    }
                                    return;
                                }
                            }
                            var_201 = local_sp_6 + (-8L);
                            *(uint64_t *)var_201 = 4217645UL;
                            var_202 = indirect_placeholder_146(0UL, r81_9, 4273296UL, 1UL, var_144, 0UL, r95_8);
                            var_203 = var_202.field_1;
                            var_204 = var_202.field_3;
                            var_205 = var_202.field_4;
                            r81_4 = var_203;
                            rcx_2 = var_204;
                            r95_2 = var_205;
                            local_sp_8 = var_201;
                            var_206 = local_sp_8 + (-8L);
                            *(uint64_t *)var_206 = 4217664UL;
                            var_207 = indirect_placeholder_148(0UL, r81_4, 4273232UL, 1UL, rcx_2, 0UL, r95_2);
                            r81_3 = var_207.field_1;
                            rcx_1 = var_207.field_3;
                            r95_1 = var_207.field_4;
                            local_sp_7 = var_206;
                        }
                    }
                } else {
                    var_181 = local_sp_15 + (-16L);
                    *(uint64_t *)var_181 = 4217563UL;
                    var_182 = indirect_placeholder_150(0UL, r81_9, 4273144UL, 1UL, var_144, 0UL, r95_8);
                    r81_5 = var_182.field_1;
                    rcx_3 = var_182.field_3;
                    r95_3 = var_182.field_4;
                    local_sp_9 = var_181;
                    var_186 = local_sp_9 + (-8L);
                    *(uint64_t *)var_186 = 4217582UL;
                    indirect_placeholder_149(0UL, r81_5, 4273192UL, 1UL, rcx_3, 0UL, r95_3);
                    local_sp_17 = var_186;
                    var_187 = local_sp_17 + (-8L);
                    *(uint64_t *)var_187 = 4217592UL;
                    var_188 = indirect_placeholder_157(r12_1, rbx_3, r15_9, r13_5, r14_6, rbp_3, 4268768UL);
                    r81_0 = var_188.field_0;
                    r95_0 = var_188.field_6;
                    local_sp_0 = var_187;
                }
            } else {
                var_145 = (uint64_t)(var_142 & (-252));
                var_146 = ((var_143 & 4UL) == 0UL) ? 4268591UL : 4268579UL;
                var_147 = local_sp_15 + (-8L);
                *(uint64_t *)var_147 = 4217726UL;
                var_148 = indirect_placeholder_155(r12_1, rbx_3, r15_9, r13_5, r14_6, rbp_3, var_146, var_145);
                var_149 = var_148.field_0;
                var_150 = var_148.field_6;
                r81_0 = var_149;
                r95_0 = var_150;
                local_sp_0 = var_147;
            }
        } else {
            var_209 = local_sp_16 + (-8L);
            *(uint64_t *)var_209 = 4217693UL;
            var_210 = indirect_placeholder_156(r12_1, rbx_3, r15_9, r13_5, r14_6, rbp_3, 4268778UL);
            r81_0 = var_210.field_0;
            r95_0 = var_210.field_6;
            local_sp_0 = var_209;
        }
    }
}
