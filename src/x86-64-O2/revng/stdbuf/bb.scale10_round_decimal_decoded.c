typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_mulss_wrapper_ret_type;
struct type_5;
struct type_7;
struct helper_pxor_xmm_wrapper_84_ret_type;
struct helper_cvtsq2ss_wrapper_ret_type;
struct helper_addss_wrapper_ret_type;
struct helper_ucomiss_wrapper_ret_type;
struct helper_cvttss2sq_wrapper_ret_type;
struct helper_subss_wrapper_ret_type;
struct indirect_placeholder_47_ret_type;
struct indirect_placeholder_48_ret_type;
struct helper_cvttss2si_wrapper_ret_type;
struct indirect_placeholder_49_ret_type;
struct helper_mulss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct type_5 {
};
struct type_7 {
};
struct helper_pxor_xmm_wrapper_84_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_cvtsq2ss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_addss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_ucomiss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvttss2sq_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_subss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct indirect_placeholder_47_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_48_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_cvttss2si_wrapper_ret_type {
    uint32_t field_0;
    unsigned char field_1;
};
struct indirect_placeholder_49_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_state_0x8558(void);
extern unsigned char init_state_0x8549(void);
extern unsigned char init_state_0x854c(void);
extern unsigned char init_state_0x8548(void);
extern unsigned char init_state_0x854b(void);
extern unsigned char init_state_0x8547(void);
extern unsigned char init_state_0x854d(void);
extern uint64_t init_state_0x8560(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct helper_mulss_wrapper_ret_type helper_mulss_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern uint64_t init_r15(void);
extern uint64_t init_state_0x8d58(void);
extern struct helper_pxor_xmm_wrapper_84_ret_type helper_pxor_xmm_wrapper_84(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4);
extern struct helper_cvtsq2ss_wrapper_ret_type helper_cvtsq2ss_wrapper(struct type_5 *param_0, struct type_7 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_addss_wrapper_ret_type helper_addss_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9);
extern struct helper_ucomiss_wrapper_ret_type helper_ucomiss_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_cvttss2sq_wrapper_ret_type helper_cvttss2sq_wrapper(struct type_5 *param_0, struct type_7 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4);
extern struct helper_subss_wrapper_ret_type helper_subss_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct indirect_placeholder_47_ret_type indirect_placeholder_47(uint64_t param_0);
extern struct indirect_placeholder_48_ret_type indirect_placeholder_48(uint64_t param_0);
extern struct helper_cvttss2si_wrapper_ret_type helper_cvttss2si_wrapper(struct type_5 *param_0, struct type_7 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4);
extern struct indirect_placeholder_49_ret_type indirect_placeholder_49(uint64_t param_0);
uint64_t bb_scale10_round_decimal_decoded(uint64_t rcx, uint64_t rdi, uint64_t r8, uint64_t rdx, uint64_t rsi) {
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_81;
    uint32_t var_82;
    uint64_t var_83;
    bool var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_96;
    uint64_t cc_src2_0;
    uint64_t r12_0;
    uint64_t var_154;
    uint64_t rdi2_2;
    struct helper_cvtsq2ss_wrapper_ret_type var_134;
    uint64_t state_0x8558_0;
    unsigned char storemerge;
    uint64_t var_108;
    uint64_t rbx_3;
    uint64_t var_94;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    unsigned char var_11;
    unsigned char var_12;
    unsigned char var_13;
    unsigned char var_14;
    unsigned char var_15;
    unsigned char var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t *var_19;
    uint64_t storemerge8;
    uint64_t var_89;
    uint64_t rax_1;
    uint64_t rax_0;
    uint64_t var_25;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t var_95;
    uint64_t var_123;
    uint64_t var_124;
    uint64_t var_125;
    uint64_t rdi2_0;
    uint64_t rax_2;
    uint64_t var_117;
    bool var_118;
    uint64_t var_119;
    bool var_120;
    uint64_t rdi2_1;
    uint64_t rax_3;
    uint64_t var_126;
    uint64_t var_127;
    uint64_t r83_0;
    uint64_t rax_4;
    uint64_t var_121;
    uint64_t var_122;
    uint64_t var_128;
    uint64_t var_129;
    uint64_t var_130;
    uint64_t local_sp_0;
    uint64_t var_168;
    uint64_t r83_1;
    uint64_t var_152;
    uint64_t rsi5_0;
    uint64_t var_153;
    uint64_t rsi5_2;
    uint64_t rbx_0;
    uint64_t var_73;
    uint64_t rbx_1;
    uint64_t rsi5_1;
    uint64_t rcx1_0;
    uint64_t var_155;
    uint32_t *_cast3;
    uint32_t var_156;
    unsigned __int128 var_157;
    uint64_t var_158;
    uint64_t var_159;
    uint64_t var_160;
    uint64_t var_161;
    uint64_t rbx_2;
    uint64_t rcx1_1;
    uint64_t var_162;
    uint64_t var_163;
    uint64_t var_164;
    uint64_t var_165;
    uint64_t rsi5_3;
    uint64_t var_166;
    uint64_t rsi5_4;
    uint64_t var_167;
    uint64_t var_131;
    uint64_t var_132;
    bool var_133;
    uint64_t cc_dst_1;
    uint64_t var_135;
    struct helper_cvtsq2ss_wrapper_ret_type var_136;
    struct helper_addss_wrapper_ret_type var_137;
    struct helper_mulss_wrapper_ret_type var_138;
    uint64_t var_139;
    struct helper_ucomiss_wrapper_ret_type var_140;
    uint64_t var_141;
    unsigned char var_142;
    uint64_t var_143;
    struct helper_cvttss2sq_wrapper_ret_type var_144;
    uint64_t rax_5;
    struct helper_subss_wrapper_ret_type var_145;
    struct helper_cvttss2sq_wrapper_ret_type var_146;
    uint64_t var_147;
    uint64_t var_148;
    uint64_t var_149;
    struct indirect_placeholder_47_ret_type var_150;
    uint64_t var_151;
    uint64_t rbp_0;
    uint64_t local_sp_1;
    uint64_t r14_2;
    uint64_t r14_1;
    uint64_t rdi2_3;
    uint64_t r14_0;
    uint32_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t rax_6;
    uint64_t var_74;
    uint64_t r9_0;
    uint64_t rax_7;
    uint32_t *var_75;
    uint32_t var_76;
    uint64_t var_77;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t var_112;
    struct indirect_placeholder_48_ret_type var_113;
    uint64_t var_114;
    uint64_t var_115;
    uint64_t var_116;
    uint64_t r14_3;
    uint64_t r9_1;
    uint64_t rdi2_4;
    uint64_t rax_8;
    uint32_t *var_97;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t var_100;
    uint64_t r14_4;
    uint64_t var_101;
    uint64_t rax_9;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t rax_10;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t *var_107;
    uint64_t var_109;
    uint64_t local_sp_2;
    uint64_t var_20;
    uint32_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t rdx4_0;
    uint64_t var_26;
    uint32_t var_27;
    struct helper_pxor_xmm_wrapper_84_ret_type var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t *var_32;
    uint32_t *var_33;
    uint32_t var_34;
    uint32_t *var_35;
    uint32_t var_36;
    uint32_t var_37;
    uint32_t var_38;
    uint64_t var_39;
    uint32_t var_40;
    uint32_t var_41;
    uint64_t var_42;
    struct helper_cvtsq2ss_wrapper_ret_type var_43;
    uint64_t var_44;
    unsigned char var_45;
    uint64_t var_46;
    uint32_t *var_47;
    uint32_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    struct helper_mulss_wrapper_ret_type var_52;
    uint64_t var_53;
    struct helper_cvttss2si_wrapper_ret_type var_54;
    uint32_t var_55;
    unsigned char var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t *var_59;
    struct indirect_placeholder_49_ret_type var_60;
    uint64_t var_61;
    uint32_t *var_62;
    uint32_t var_63;
    uint32_t var_64;
    uint64_t var_65;
    uint32_t var_66;
    uint64_t *var_67;
    uint64_t var_68;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r13();
    var_4 = init_cc_src2();
    var_5 = init_state_0x8558();
    var_6 = init_state_0x8560();
    var_7 = init_r12();
    var_8 = init_r15();
    var_9 = init_r14();
    var_10 = init_state_0x8d58();
    var_11 = init_state_0x8549();
    var_12 = init_state_0x854c();
    var_13 = init_state_0x8548();
    var_14 = init_state_0x854b();
    var_15 = init_state_0x8547();
    var_16 = init_state_0x854d();
    *(uint64_t *)(var_0 + (-8L)) = var_8;
    *(uint64_t *)(var_0 + (-16L)) = var_9;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_7;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    var_17 = var_0 + (-136L);
    var_18 = var_0 + (-96L);
    *(uint64_t *)var_18 = rsi;
    var_19 = (uint64_t *)var_17;
    *var_19 = rcx;
    cc_src2_0 = var_4;
    rbx_3 = 0UL;
    storemerge8 = 0UL;
    rax_1 = 0UL;
    rax_0 = 0UL;
    rax_2 = 0UL;
    rax_3 = 0UL;
    r83_0 = 0UL;
    rax_4 = 0UL;
    r83_1 = r8;
    rcx1_0 = 0UL;
    r14_2 = 1UL;
    rdi2_3 = 0UL;
    r14_0 = 1UL;
    rax_6 = 13UL;
    rax_7 = 0UL;
    rax_8 = 0UL;
    if (rcx == 0UL) {
        return storemerge8;
    }
    var_20 = r8 + rdi;
    var_21 = (uint32_t)var_20;
    var_22 = (uint64_t)var_21;
    var_23 = helper_cc_compute_all_wrapper(var_22, 0UL, 0UL, 24U);
    rdx4_0 = var_22;
    var_24 = helper_cc_compute_all_wrapper(r8, 0UL, 0UL, 24U);
    if ((uint64_t)(((unsigned char)(var_23 >> 4UL) ^ (unsigned char)var_23) & '\xc0') != 0UL & (uint64_t)(((unsigned char)(var_24 >> 4UL) ^ (unsigned char)var_24) & '\xc0') == 0UL) {
        var_25 = ((long)(var_20 << 32UL) > (long)(r8 << 32UL)) ? r8 : var_22;
        var_26 = (uint64_t)((long)(var_25 << 32UL) >> (long)32UL);
        var_27 = (uint32_t)var_25;
        rbx_3 = var_26;
        r83_1 = (uint64_t)((uint32_t)r8 - var_27);
        rdx4_0 = (uint64_t)(var_21 - var_27);
    }
    var_28 = helper_pxor_xmm_wrapper_84((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_5, var_6);
    var_29 = var_28.field_1;
    var_30 = (uint64_t)((long)(r83_1 << 32UL) >> (long)63UL);
    var_31 = var_0 + (-104L);
    var_32 = (uint64_t *)var_31;
    *var_32 = rdx;
    var_33 = (uint32_t *)(var_0 + (-108L));
    var_34 = (uint32_t)r83_1;
    *var_33 = var_34;
    var_35 = (uint32_t *)(var_0 + (-120L));
    var_36 = (uint32_t)rdx4_0;
    *var_35 = var_36;
    var_37 = (uint32_t)var_30;
    var_38 = (var_37 ^ var_34) - var_37;
    var_39 = (uint64_t)var_38;
    var_40 = (uint32_t)(uint64_t)((long)(rdx4_0 << 32UL) >> (long)63UL);
    var_41 = (var_40 ^ var_36) - var_40;
    var_42 = (uint64_t)var_41;
    var_43 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_39, var_11, var_13, var_14, var_15);
    var_44 = var_43.field_0;
    var_45 = var_43.field_1;
    var_46 = var_0 + (-128L);
    var_47 = (uint32_t *)var_46;
    *var_47 = var_41;
    var_48 = (uint32_t)(var_42 >> 5UL) & 134217727U;
    var_49 = (uint64_t)var_48;
    var_50 = (uint64_t)*(uint32_t *)4256340UL;
    var_51 = var_10 & (-4294967296L);
    var_52 = helper_mulss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), var_44, var_51 | var_50, var_45, var_12, var_13, var_14, var_15, var_16);
    var_53 = var_52.field_0;
    var_54 = helper_cvttss2si_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_53, var_52.field_1, var_12);
    var_55 = var_54.field_0;
    var_56 = var_54.field_1;
    var_57 = (((uint64_t)(var_48 + var_55) << 2UL) + 8UL) & 17179869180UL;
    var_58 = var_0 + (-144L);
    var_59 = (uint64_t *)var_58;
    *var_59 = 4221042UL;
    var_60 = indirect_placeholder_49(var_57);
    var_61 = var_60.field_0;
    var_62 = (uint32_t *)var_17;
    var_63 = *var_62;
    var_64 = *var_47;
    var_65 = (uint64_t)var_64;
    var_66 = *(uint32_t *)(var_0 + (-116L));
    var_67 = (uint64_t *)(var_0 + (-112L));
    var_68 = *var_67;
    r9_0 = var_61;
    rdi2_4 = var_61;
    rax_10 = var_49;
    local_sp_2 = var_58;
    if (var_61 == 0UL) {
        *(uint64_t *)(local_sp_2 + (-8L)) = 4222086UL;
        indirect_placeholder();
    } else {
        *(uint32_t *)var_61 = 1U;
        if (var_39 != 0UL) {
            *var_62 = (var_38 + 13U);
            var_81 = helper_cc_compute_c_wrapper(var_71, var_70, var_4, 16U);
            r14_0 = r14_1;
            r14_2 = r14_1;
            do {
                var_69 = (uint32_t)rdi2_3 + 13U;
                var_70 = (uint64_t)var_69;
                var_71 = var_39 - var_70;
                var_72 = helper_cc_compute_c_wrapper(var_71, var_70, var_4, 16U);
                rdi2_3 = var_70;
                r14_1 = r14_0;
                if (var_72 == 0UL) {
                    rax_6 = (uint64_t)(*var_62 - var_69);
                }
                var_73 = (r14_0 << 2UL) + var_61;
                var_74 = (uint64_t)*(uint32_t *)((rax_6 << 2UL) + 4256224UL);
                var_75 = (uint32_t *)r9_0;
                var_76 = *var_75;
                var_77 = r9_0 + 4UL;
                var_78 = rax_7 + (var_74 * (uint64_t)var_76);
                *var_75 = (uint32_t)var_78;
                var_79 = var_78 >> 32UL;
                r9_0 = var_77;
                rax_7 = var_79;
                do {
                    var_75 = (uint32_t *)r9_0;
                    var_76 = *var_75;
                    var_77 = r9_0 + 4UL;
                    var_78 = rax_7 + (var_74 * (uint64_t)var_76);
                    *var_75 = (uint32_t)var_78;
                    var_79 = var_78 >> 32UL;
                    r9_0 = var_77;
                    rax_7 = var_79;
                } while (var_77 != var_73);
                if (var_79 == 0UL) {
                    var_80 = r14_0 + 1UL;
                    *(uint32_t *)var_73 = (uint32_t)var_79;
                    r14_1 = var_80;
                }
                var_81 = helper_cc_compute_c_wrapper(var_71, var_70, var_4, 16U);
                r14_0 = r14_1;
                r14_2 = r14_1;
            } while (var_81 != 0UL);
        }
        var_82 = var_63 & 31U;
        var_83 = (uint64_t)var_82;
        var_84 = ((int)var_66 < (int)0U);
        r9_1 = r14_2;
        r14_3 = r14_2;
        if (var_84) {
            var_96 = helper_cc_compute_all_wrapper(var_65, 0UL, 0UL, 24U);
            if ((uint64_t)(((unsigned char)(var_96 >> 4UL) ^ (unsigned char)var_96) & '\xc0') == 0UL) {
                var_110 = *var_32;
                *(uint64_t *)var_46 = var_68;
                *var_19 = var_49;
                var_111 = ((var_110 + var_49) << 2UL) + 4UL;
                var_112 = var_0 + (-152L);
                *(uint64_t *)var_112 = 4221850UL;
                var_113 = indirect_placeholder_48(var_111);
                var_114 = var_113.field_0;
                var_115 = *var_59;
                var_116 = *var_19;
                rdi2_0 = var_114;
                local_sp_0 = var_112;
                if (var_114 != 0UL) {
                    var_168 = local_sp_0 + (-8L);
                    *(uint64_t *)var_168 = 4222077UL;
                    indirect_placeholder();
                    local_sp_2 = var_168;
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4222086UL;
                    indirect_placeholder();
                    return storemerge8;
                }
                if (var_49 != 0UL) {
                    *(uint32_t *)((rax_2 << 2UL) + var_114) = 0U;
                    var_117 = rax_2 + 1UL;
                    rax_2 = var_117;
                    do {
                        *(uint32_t *)((rax_2 << 2UL) + var_114) = 0U;
                        var_117 = rax_2 + 1UL;
                        rax_2 = var_117;
                    } while (var_115 != var_117);
                    rdi2_0 = (var_115 << 2UL) + var_114;
                }
                var_118 = (var_82 == 0U);
                var_119 = *var_67;
                var_120 = (var_119 == 0UL);
                rdi2_1 = rdi2_0;
                if (var_118) {
                    if (!var_120) {
                        var_126 = rax_3 << 2UL;
                        *(uint32_t *)(var_126 + rdi2_0) = *(uint32_t *)(var_126 + var_116);
                        var_127 = rax_3 + 1UL;
                        rax_3 = var_127;
                        do {
                            var_126 = rax_3 << 2UL;
                            *(uint32_t *)(var_126 + rdi2_0) = *(uint32_t *)(var_126 + var_116);
                            var_127 = rax_3 + 1UL;
                            rax_3 = var_127;
                        } while (var_127 != var_119);
                        rdi2_1 = (var_119 << 2UL) + rdi2_0;
                    }
                } else {
                    if (!var_120) {
                        var_121 = r83_0 << 2UL;
                        var_122 = rax_4 + ((uint64_t)*(uint32_t *)(var_121 + var_116) << var_83);
                        *(uint32_t *)(var_121 + rdi2_0) = (uint32_t)var_122;
                        var_123 = r83_0 + 1UL;
                        var_124 = var_122 >> 32UL;
                        r83_0 = var_123;
                        rax_4 = var_124;
                        do {
                            var_121 = r83_0 << 2UL;
                            var_122 = rax_4 + ((uint64_t)*(uint32_t *)(var_121 + var_116) << var_83);
                            *(uint32_t *)(var_121 + rdi2_0) = (uint32_t)var_122;
                            var_123 = r83_0 + 1UL;
                            var_124 = var_122 >> 32UL;
                            r83_0 = var_123;
                            rax_4 = var_124;
                        } while (var_123 != var_119);
                        var_125 = (var_119 << 2UL) + rdi2_0;
                        rdi2_1 = var_125;
                        if (var_124 == 0UL) {
                            *(uint32_t *)var_125 = (uint32_t)var_124;
                            rdi2_1 = var_125 + 4UL;
                        }
                    }
                }
                var_128 = (uint64_t)((long)(rdi2_1 - var_114) >> (long)2UL);
                *(uint64_t *)(var_0 + (-160L)) = 4221979UL;
                var_129 = indirect_placeholder_2(var_61, var_128, var_31, r14_2, var_114);
                var_130 = var_0 + (-168L);
                *(uint64_t *)var_130 = 4221990UL;
                indirect_placeholder();
                rbp_0 = var_129;
                local_sp_1 = var_130;
            } else {
                if (var_82 != 0U) {
                    var_97 = (uint32_t *)rdi2_4;
                    var_98 = rax_8 + ((uint64_t)*var_97 << var_83);
                    *var_97 = (uint32_t)var_98;
                    var_99 = var_98 >> 32UL;
                    var_100 = r9_1 + (-1L);
                    r9_1 = var_100;
                    rax_8 = var_99;
                    while (var_100 != 0UL)
                        {
                            rdi2_4 = rdi2_4 + 4UL;
                            var_97 = (uint32_t *)rdi2_4;
                            var_98 = rax_8 + ((uint64_t)*var_97 << var_83);
                            *var_97 = (uint32_t)var_98;
                            var_99 = var_98 >> 32UL;
                            var_100 = r9_1 + (-1L);
                            r9_1 = var_100;
                            rax_8 = var_99;
                        }
                    if (var_99 == 0UL) {
                        *(uint32_t *)((r14_2 << 2UL) + var_61) = (uint32_t)var_99;
                        r14_3 = r14_2 + 1UL;
                    }
                }
                rax_9 = r14_3;
                r14_4 = r14_3;
                if (var_49 != 0UL) {
                    var_101 = (var_49 << 2UL) + var_61;
                    if (r14_3 == 0UL) {
                        var_102 = rax_9 + (-1L);
                        var_103 = var_102 << 2UL;
                        *(uint32_t *)(var_103 + var_101) = *(uint32_t *)(var_103 + var_61);
                        rax_9 = var_102;
                        do {
                            var_102 = rax_9 + (-1L);
                            var_103 = var_102 << 2UL;
                            *(uint32_t *)(var_103 + var_101) = *(uint32_t *)(var_103 + var_61);
                            rax_9 = var_102;
                        } while (var_102 != 0UL);
                    }
                    var_104 = rax_10 + (-1L);
                    *(uint32_t *)((var_104 << 2UL) + var_61) = 0U;
                    rax_10 = var_104;
                    do {
                        var_104 = rax_10 + (-1L);
                        *(uint32_t *)((var_104 << 2UL) + var_61) = 0U;
                        rax_10 = var_104;
                    } while (var_104 != 0UL);
                    r14_4 = r14_3 + var_49;
                }
                var_105 = *var_32;
                var_106 = var_0 + (-152L);
                var_107 = (uint64_t *)var_106;
                local_sp_1 = var_106;
                if (var_84) {
                    *var_107 = 4222044UL;
                    var_109 = indirect_placeholder_2(var_61, var_105, var_18, r14_4, var_68);
                    rbp_0 = var_109;
                } else {
                    *var_107 = 4221378UL;
                    var_108 = indirect_placeholder_2(var_61, var_105, var_18, r14_4, var_68);
                    rbp_0 = var_108;
                }
            }
        } else {
            if ((int)var_64 < (int)0U) {
                var_85 = *var_32;
                var_86 = var_0 + (-80L);
                var_87 = var_0 + (-152L);
                *(uint64_t *)var_87 = 4222128UL;
                var_88 = indirect_placeholder_2(var_61, var_85, var_86, r14_2, var_68);
                local_sp_0 = var_87;
                if (var_88 != 0UL) {
                    var_168 = local_sp_0 + (-8L);
                    *(uint64_t *)var_168 = 4222077UL;
                    indirect_placeholder();
                    local_sp_2 = var_168;
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4222086UL;
                    indirect_placeholder();
                    return storemerge8;
                }
                var_89 = (r14_2 << 2UL) + var_61;
                if (var_49 != 0UL) {
                    *(uint32_t *)((rax_0 << 2UL) + var_89) = 0U;
                    var_90 = rax_0 + 1UL;
                    rax_0 = var_90;
                    do {
                        *(uint32_t *)((rax_0 << 2UL) + var_89) = 0U;
                        var_90 = rax_0 + 1UL;
                        rax_0 = var_90;
                    } while (var_90 != var_49);
                    rax_1 = var_49 << 2UL;
                }
                var_91 = *(uint64_t *)(var_0 + (-88L));
                *(uint32_t *)(rax_1 + var_89) = (uint32_t)(1UL << var_83);
                var_92 = *(uint64_t *)var_86;
                var_93 = var_49 + 1UL;
                *(uint64_t *)(var_0 + (-160L)) = 4222219UL;
                var_94 = indirect_placeholder_2(var_89, var_91, var_31, var_93, var_92);
                var_95 = var_0 + (-168L);
                *(uint64_t *)var_95 = 4222230UL;
                indirect_placeholder();
                rbp_0 = var_94;
                local_sp_1 = var_95;
            } else {
                if (var_82 != 0U) {
                    var_97 = (uint32_t *)rdi2_4;
                    var_98 = rax_8 + ((uint64_t)*var_97 << var_83);
                    *var_97 = (uint32_t)var_98;
                    var_99 = var_98 >> 32UL;
                    var_100 = r9_1 + (-1L);
                    r9_1 = var_100;
                    rax_8 = var_99;
                    while (var_100 != 0UL)
                        {
                            rdi2_4 = rdi2_4 + 4UL;
                            var_97 = (uint32_t *)rdi2_4;
                            var_98 = rax_8 + ((uint64_t)*var_97 << var_83);
                            *var_97 = (uint32_t)var_98;
                            var_99 = var_98 >> 32UL;
                            var_100 = r9_1 + (-1L);
                            r9_1 = var_100;
                            rax_8 = var_99;
                        }
                    if (var_99 == 0UL) {
                        *(uint32_t *)((r14_2 << 2UL) + var_61) = (uint32_t)var_99;
                        r14_3 = r14_2 + 1UL;
                    }
                }
                rax_9 = r14_3;
                r14_4 = r14_3;
                if (var_49 != 0UL) {
                    var_101 = (var_49 << 2UL) + var_61;
                    if (r14_3 == 0UL) {
                        var_102 = rax_9 + (-1L);
                        var_103 = var_102 << 2UL;
                        *(uint32_t *)(var_103 + var_101) = *(uint32_t *)(var_103 + var_61);
                        rax_9 = var_102;
                        do {
                            var_102 = rax_9 + (-1L);
                            var_103 = var_102 << 2UL;
                            *(uint32_t *)(var_103 + var_101) = *(uint32_t *)(var_103 + var_61);
                            rax_9 = var_102;
                        } while (var_102 != 0UL);
                    }
                    var_104 = rax_10 + (-1L);
                    *(uint32_t *)((var_104 << 2UL) + var_61) = 0U;
                    rax_10 = var_104;
                    do {
                        var_104 = rax_10 + (-1L);
                        *(uint32_t *)((var_104 << 2UL) + var_61) = 0U;
                        rax_10 = var_104;
                    } while (var_104 != 0UL);
                    r14_4 = r14_3 + var_49;
                }
                var_105 = *var_32;
                var_106 = var_0 + (-152L);
                var_107 = (uint64_t *)var_106;
                local_sp_1 = var_106;
                if (var_84) {
                    *var_107 = 4221378UL;
                    var_108 = indirect_placeholder_2(var_61, var_105, var_18, r14_4, var_68);
                    rbp_0 = var_108;
                } else {
                    *var_107 = 4222044UL;
                    var_109 = indirect_placeholder_2(var_61, var_105, var_18, r14_4, var_68);
                    rbp_0 = var_109;
                }
            }
        }
        *(uint64_t *)(local_sp_1 + (-8L)) = 4221389UL;
        indirect_placeholder();
        *(uint64_t *)(local_sp_1 + (-16L)) = 4221398UL;
        indirect_placeholder();
        if (rbp_0 != 0UL) {
            var_131 = *(uint64_t *)(local_sp_1 + 32UL);
            var_132 = *(uint64_t *)(local_sp_1 + 40UL);
            var_133 = ((long)var_131 < (long)0UL);
            helper_pxor_xmm_wrapper_84((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_53, var_29);
            r12_0 = var_131;
            cc_dst_1 = var_131;
            if (var_133) {
                var_135 = (var_131 >> 1UL) | (var_131 & 1UL);
                var_136 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_135, var_56, var_13, var_14, var_15);
                var_137 = helper_addss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_136.field_0, var_136.field_1, var_12, var_13, var_14, var_15, var_16);
                cc_dst_1 = var_135;
                state_0x8558_0 = var_137.field_0;
                storemerge = var_137.field_1;
            } else {
                var_134 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_131, var_56, var_13, var_14, var_15);
                state_0x8558_0 = var_134.field_0;
                storemerge = var_134.field_1;
            }
            var_138 = helper_mulss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), state_0x8558_0, var_51 | (uint64_t)*(uint32_t *)4256344UL, storemerge, var_12, var_13, var_14, var_15, var_16);
            var_139 = var_138.field_0;
            var_140 = helper_ucomiss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), var_139, var_51 | (uint64_t)*(uint32_t *)4256348UL, var_138.field_1, var_12);
            var_141 = var_140.field_0;
            var_142 = var_140.field_1;
            var_143 = helper_cc_compute_c_wrapper(cc_dst_1, var_141, var_4, 1U);
            if (var_143 == 0UL) {
                var_145 = helper_subss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), var_139, var_51 | (uint64_t)*(uint32_t *)4256348UL, var_142, var_12, var_13, var_14, var_15, var_16);
                var_146 = helper_cvttss2sq_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_145.field_0, var_145.field_1, var_12);
                rax_5 = var_146.field_0 ^ (-9223372036854775808L);
            } else {
                var_144 = helper_cvttss2sq_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_139, var_142, var_12);
                rax_5 = var_144.field_0;
            }
            var_147 = ((rax_5 * 9UL) + 9UL) + rbx_3;
            var_148 = helper_cc_compute_c_wrapper(var_147, rbx_3, var_4, 9U);
            var_149 = (var_148 == 0UL) ? var_147 : 18446744073709551615UL;
            *(uint64_t *)(local_sp_1 + (-24L)) = 4221485UL;
            var_150 = indirect_placeholder_47(var_149);
            var_151 = var_150.field_0;
            rsi5_0 = var_151;
            rbx_0 = var_151;
            storemerge8 = var_151;
            if (var_151 == 0UL) {
                *(uint64_t *)(local_sp_1 + (-32L)) = 4221754UL;
                indirect_placeholder();
            } else {
                if (rbx_3 == 0UL) {
                    if (var_131 != 0UL) {
                        var_167 = var_151 + 1UL;
                        *(unsigned char *)var_151 = (unsigned char)'0';
                        rsi5_4 = var_167;
                        *(unsigned char *)rsi5_4 = (unsigned char)'\x00';
                        *(uint64_t *)(local_sp_1 + (-32L)) = 4221754UL;
                        indirect_placeholder();
                        return storemerge8;
                    }
                }
                var_152 = rbx_3 + var_151;
                rbx_0 = var_152;
                rsi5_2 = var_152;
                var_153 = rsi5_0 + 1UL;
                *(unsigned char *)rsi5_0 = (unsigned char)'0';
                rsi5_0 = var_153;
                do {
                    var_153 = rsi5_0 + 1UL;
                    *(unsigned char *)rsi5_0 = (unsigned char)'0';
                    rsi5_0 = var_153;
                } while (var_153 != var_152);
                if (var_131 == 0UL) {
                    rbx_1 = rbx_0;
                    var_164 = helper_cc_compute_c_wrapper((uint64_t)*(uint32_t *)(var_154 + (-4L)) + (-1L), 1UL, cc_src2_0, 16U);
                    var_165 = r12_0 - var_164;
                    cc_src2_0 = var_164;
                    r12_0 = var_165;
                    do {
                        var_154 = (r12_0 << 2UL) + var_132;
                        rdi2_2 = r12_0;
                        rsi5_1 = var_154;
                        rbx_2 = rbx_1;
                        var_155 = rsi5_1 + (-4L);
                        _cast3 = (uint32_t *)var_155;
                        var_156 = *_cast3;
                        var_157 = ((unsigned __int128)(((rcx1_0 << 32UL) | (uint64_t)var_156) >> 9UL) * 19342813113834067ULL) >> 75ULL;
                        var_158 = (uint64_t)var_157;
                        *_cast3 = (uint32_t)var_157;
                        var_159 = (uint64_t)(((uint32_t)var_158 * 3294967296U) + var_156);
                        var_160 = rdi2_2 + (-1L);
                        rdi2_2 = var_160;
                        rsi5_1 = var_155;
                        rcx1_0 = var_159;
                        rcx1_1 = var_159;
                        do {
                            var_155 = rsi5_1 + (-4L);
                            _cast3 = (uint32_t *)var_155;
                            var_156 = *_cast3;
                            var_157 = ((unsigned __int128)(((rcx1_0 << 32UL) | (uint64_t)var_156) >> 9UL) * 19342813113834067ULL) >> 75ULL;
                            var_158 = (uint64_t)var_157;
                            *_cast3 = (uint32_t)var_157;
                            var_159 = (uint64_t)(((uint32_t)var_158 * 3294967296U) + var_156);
                            var_160 = rdi2_2 + (-1L);
                            rdi2_2 = var_160;
                            rsi5_1 = var_155;
                            rcx1_0 = var_159;
                            rcx1_1 = var_159;
                        } while (var_160 != 0UL);
                        var_161 = rbx_1 + 9UL;
                        rbx_1 = var_161;
                        rsi5_2 = var_161;
                        var_162 = rbx_2 + 1UL;
                        var_163 = (rcx1_1 * 3435973837UL) >> 35UL;
                        *(unsigned char *)rbx_2 = (((unsigned char)rcx1_1 + ((unsigned char)var_163 * '\xf6')) + '0');
                        rbx_2 = var_162;
                        rcx1_1 = var_163;
                        do {
                            var_162 = rbx_2 + 1UL;
                            var_163 = (rcx1_1 * 3435973837UL) >> 35UL;
                            *(unsigned char *)rbx_2 = (((unsigned char)rcx1_1 + ((unsigned char)var_163 * '\xf6')) + '0');
                            rbx_2 = var_162;
                            rcx1_1 = var_163;
                        } while (var_162 != var_161);
                        var_164 = helper_cc_compute_c_wrapper((uint64_t)*(uint32_t *)(var_154 + (-4L)) + (-1L), 1UL, cc_src2_0, 16U);
                        var_165 = r12_0 - var_164;
                        cc_src2_0 = var_164;
                        r12_0 = var_165;
                    } while (var_165 != 0UL);
                    rsi5_3 = rsi5_2;
                    rsi5_4 = rsi5_2;
                    if (rsi5_2 > var_151) {
                        if (*(unsigned char *)(rsi5_2 + (-1L)) == '0') {
                            if (rsi5_2 == var_151) {
                                var_167 = var_151 + 1UL;
                                *(unsigned char *)var_151 = (unsigned char)'0';
                                rsi5_4 = var_167;
                            }
                        } else {
                            while (1U)
                                {
                                    var_166 = rsi5_3 + (-1L);
                                    rsi5_3 = var_166;
                                    rsi5_4 = var_166;
                                    if (var_166 != var_151) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    if (*(unsigned char *)(rsi5_3 + (-2L)) == '0') {
                                        continue;
                                    }
                                    loop_state_var = 1U;
                                    break;
                                }
                            var_167 = var_151 + 1UL;
                            *(unsigned char *)var_151 = (unsigned char)'0';
                            rsi5_4 = var_167;
                        }
                    } else {
                        if (rsi5_2 == var_151) {
                            var_167 = var_151 + 1UL;
                            *(unsigned char *)var_151 = (unsigned char)'0';
                            rsi5_4 = var_167;
                        }
                        *(unsigned char *)rsi5_4 = (unsigned char)'\x00';
                    }
                } else {
                    rsi5_3 = rsi5_2;
                    rsi5_4 = rsi5_2;
                    if (rsi5_2 <= var_151) {
                        if (rsi5_2 == var_151) {
                            var_167 = var_151 + 1UL;
                            *(unsigned char *)var_151 = (unsigned char)'0';
                            rsi5_4 = var_167;
                        }
                        *(unsigned char *)rsi5_4 = (unsigned char)'\x00';
                        *(uint64_t *)(local_sp_1 + (-32L)) = 4221754UL;
                        indirect_placeholder();
                        return storemerge8;
                    }
                    if (*(unsigned char *)(rsi5_2 + (-1L)) == '0') {
                        if (rsi5_2 == var_151) {
                            var_167 = var_151 + 1UL;
                            *(unsigned char *)var_151 = (unsigned char)'0';
                            rsi5_4 = var_167;
                        }
                    } else {
                        while (1U)
                            {
                                var_166 = rsi5_3 + (-1L);
                                rsi5_3 = var_166;
                                rsi5_4 = var_166;
                                if (var_166 != var_151) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                if (*(unsigned char *)(rsi5_3 + (-2L)) == '0') {
                                    continue;
                                }
                                loop_state_var = 1U;
                                break;
                            }
                        var_167 = var_151 + 1UL;
                        *(unsigned char *)var_151 = (unsigned char)'0';
                        rsi5_4 = var_167;
                    }
                }
            }
        }
    }
}
