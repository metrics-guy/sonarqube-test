typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_get_header_ret_type;
struct indirect_placeholder_1_ret_type;
struct indirect_placeholder_4_ret_type;
struct indirect_placeholder_5_ret_type;
struct bb_get_header_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_1_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_4_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_5_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_6(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
extern uint64_t init_rcx(void);
extern void indirect_placeholder_3(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_1_ret_type indirect_placeholder_1(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_4_ret_type indirect_placeholder_4(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_5_ret_type indirect_placeholder_5(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
struct bb_get_header_ret_type bb_get_header(uint64_t r8, uint64_t r9) {
    struct indirect_placeholder_5_ret_type var_41;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t r83_5;
    uint64_t *var_51;
    uint64_t *_pre_phi100;
    uint64_t storemerge1_in_sroa_speculate_load_bb_get_header_0x13a;
    uint64_t rdx_0;
    uint64_t rdx_3;
    uint64_t rax_0;
    uint64_t r83_0;
    uint64_t local_sp_0;
    uint64_t r94_0;
    uint64_t storemerge2;
    uint64_t var_45;
    uint64_t var_46;
    struct indirect_placeholder_1_ret_type var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t r94_1;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t *var_68;
    uint64_t *_pre_phi98;
    uint64_t rax_1;
    uint64_t rdi_0;
    uint64_t rcx_0;
    uint64_t local_sp_1;
    uint64_t r83_2;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t r12_0;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t rax_2;
    uint64_t r83_1;
    uint64_t local_sp_2;
    uint64_t r83_4;
    uint64_t storemerge1_in_sroa_speculated;
    bool var_52;
    uint64_t *var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t rdx_1;
    uint64_t local_sp_3;
    uint64_t r94_2;
    uint64_t var_8;
    uint64_t var_9;
    uint32_t var_10;
    uint64_t var_11;
    uint32_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    struct indirect_placeholder_4_ret_type var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_22;
    uint16_t var_23;
    uint64_t var_24;
    uint64_t rsi_0;
    unsigned __int128 var_25;
    uint16_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t storemerge4;
    uint64_t var_36;
    uint64_t var_38;
    uint64_t rdx_2;
    uint64_t var_37;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_21;
    uint64_t rcx_1;
    uint64_t r94_3;
    struct bb_get_header_ret_type mrv;
    struct bb_get_header_ret_type mrv1;
    struct bb_get_header_ret_type mrv2;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r12();
    var_2 = init_rbx();
    var_3 = init_r13();
    var_4 = init_rbp();
    var_5 = init_rcx();
    var_6 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_2;
    var_7 = var_0 + (-272L);
    *(uint64_t *)var_7 = 4207490UL;
    indirect_placeholder(r8, r9);
    r83_5 = r8;
    storemerge2 = 4287281UL;
    r83_2 = r8;
    r12_0 = 0UL;
    local_sp_3 = var_7;
    r94_2 = r9;
    rcx_1 = var_5;
    r94_3 = r9;
    if (*(uint64_t *)4322960UL == 0UL) {
        mrv.field_0 = r83_5;
        mrv1 = mrv;
        mrv1.field_1 = rcx_1;
        mrv2 = mrv1;
        mrv2.field_2 = r94_3;
        return mrv2;
    }
    rdx_1 = *(uint64_t *)4322968UL;
    while (1U)
        {
            var_8 = r12_0 << 3UL;
            var_9 = *(uint64_t *)(var_8 + rdx_1);
            *(uint64_t *)(local_sp_3 + 8UL) = 0UL;
            var_10 = *(uint32_t *)var_9;
            var_11 = *(uint64_t *)(var_9 + 24UL);
            r94_0 = r94_2;
            r94_1 = r94_2;
            rax_2 = var_9;
            r83_4 = r83_2;
            if (var_10 != 2U) {
                var_21 = local_sp_3 + (-8L);
                *(uint64_t *)var_21 = 4207544UL;
                indirect_placeholder_6();
                *(uint64_t *)local_sp_3 = rax_2;
                r83_1 = r83_4;
                local_sp_2 = var_21;
                storemerge1_in_sroa_speculated = rax_2;
                var_52 = (storemerge1_in_sroa_speculated == 0UL);
                var_53 = (uint64_t *)(local_sp_2 + (-8L));
                r83_2 = r83_1;
                r94_2 = r94_1;
                r83_5 = r83_1;
                r94_3 = r94_1;
                if (var_52) {
                    *var_53 = 4208056UL;
                    indirect_placeholder_3(r83_1, r94_1);
                    abort();
                }
                *var_53 = 4207566UL;
                indirect_placeholder_2(storemerge1_in_sroa_speculated);
                var_54 = *(uint64_t *)4322944UL;
                var_55 = *(uint64_t *)4322952UL;
                var_56 = *(uint64_t *)local_sp_2;
                *(uint64_t *)(var_8 + *(uint64_t *)(((var_54 << 3UL) + var_55) + (-8L))) = var_56;
                var_57 = *(uint64_t *)(*(uint64_t *)(var_8 + *(uint64_t *)4322968UL) + 32UL);
                var_58 = local_sp_2 + (-16L);
                *(uint64_t *)var_58 = 4207616UL;
                var_59 = indirect_placeholder(var_56, 0UL);
                local_sp_1 = var_58;
                if (var_57 > (uint64_t)((long)(var_59 << 32UL) >> (long)32UL)) {
                    var_66 = *(uint64_t *)4322968UL;
                    var_67 = *(uint64_t *)(var_8 + var_66);
                    var_68 = (uint64_t *)(var_67 + 32UL);
                    _pre_phi98 = var_68;
                    rax_1 = *var_68;
                    rdx_0 = var_66;
                    rcx_0 = var_67;
                } else {
                    var_60 = *var_53;
                    var_61 = local_sp_2 + (-24L);
                    *(uint64_t *)var_61 = 4207812UL;
                    var_62 = indirect_placeholder(var_60, 0UL);
                    var_63 = *(uint64_t *)4322968UL;
                    var_64 = (uint64_t)((long)(var_62 << 32UL) >> (long)32UL);
                    var_65 = *(uint64_t *)(var_8 + var_63);
                    rdx_0 = var_63;
                    _pre_phi98 = (uint64_t *)(var_65 + 32UL);
                    rax_1 = var_64;
                    rcx_0 = var_65;
                    local_sp_1 = var_61;
                }
                var_69 = r12_0 + 1UL;
                var_70 = *(uint64_t *)4322960UL;
                *_pre_phi98 = rax_1;
                r12_0 = var_69;
                rdx_1 = rdx_0;
                local_sp_3 = local_sp_1;
                rcx_1 = rcx_0;
                if (var_70 <= var_69) {
                    continue;
                }
                break;
            }
            var_12 = *(uint32_t *)4322976UL;
            var_13 = (uint64_t)var_12;
            var_14 = (uint64_t)*(uint32_t *)4323112UL;
            rax_2 = var_13;
            r83_4 = var_14;
            if (var_12 != 0U) {
                storemerge2 = var_11;
                if ((uint64_t)(var_12 + (-4)) != 0UL) {
                    if ((uint64_t)(var_12 + (-3)) == 0UL) {
                        var_21 = local_sp_3 + (-8L);
                        *(uint64_t *)var_21 = 4207544UL;
                        indirect_placeholder_6();
                        *(uint64_t *)local_sp_3 = rax_2;
                        r83_1 = r83_4;
                        local_sp_2 = var_21;
                        storemerge1_in_sroa_speculated = rax_2;
                    } else {
                        var_15 = *(uint64_t *)4323104UL;
                        var_16 = local_sp_3 + 16UL;
                        var_17 = local_sp_3 + (-8L);
                        *(uint64_t *)var_17 = 4207749UL;
                        var_18 = indirect_placeholder_4(var_15, var_16);
                        var_19 = var_18.field_0;
                        var_20 = var_18.field_1;
                        rax_0 = var_19;
                        r83_0 = var_20;
                        local_sp_0 = var_17;
                        var_45 = local_sp_0 + 8UL;
                        var_46 = local_sp_0 + (-8L);
                        *(uint64_t *)var_46 = 4207772UL;
                        var_47 = indirect_placeholder_1(0UL, r83_0, rax_0, var_45, storemerge2, 4287288UL, r94_0);
                        var_48 = var_47.field_0;
                        var_49 = var_47.field_1;
                        var_50 = var_47.field_2;
                        r83_1 = var_49;
                        local_sp_2 = var_46;
                        r94_1 = var_50;
                        if ((uint64_t)((uint32_t)var_48 + 1U) == 0UL) {
                            var_51 = (uint64_t *)local_sp_0;
                            *var_51 = 0UL;
                            _pre_phi100 = var_51;
                        } else {
                            _pre_phi100 = (uint64_t *)local_sp_0;
                        }
                        storemerge1_in_sroa_speculate_load_bb_get_header_0x13a = *_pre_phi100;
                        storemerge1_in_sroa_speculated = storemerge1_in_sroa_speculate_load_bb_get_header_0x13a;
                    }
                    var_52 = (storemerge1_in_sroa_speculated == 0UL);
                    var_53 = (uint64_t *)(local_sp_2 + (-8L));
                    r83_2 = r83_1;
                    r94_2 = r94_1;
                    r83_5 = r83_1;
                    r94_3 = r94_1;
                    if (var_52) {
                        *var_53 = 4208056UL;
                        indirect_placeholder_3(r83_1, r94_1);
                        abort();
                    }
                    *var_53 = 4207566UL;
                    indirect_placeholder_2(storemerge1_in_sroa_speculated);
                    var_54 = *(uint64_t *)4322944UL;
                    var_55 = *(uint64_t *)4322952UL;
                    var_56 = *(uint64_t *)local_sp_2;
                    *(uint64_t *)(var_8 + *(uint64_t *)(((var_54 << 3UL) + var_55) + (-8L))) = var_56;
                    var_57 = *(uint64_t *)(*(uint64_t *)(var_8 + *(uint64_t *)4322968UL) + 32UL);
                    var_58 = local_sp_2 + (-16L);
                    *(uint64_t *)var_58 = 4207616UL;
                    var_59 = indirect_placeholder(var_56, 0UL);
                    local_sp_1 = var_58;
                    if (var_57 > (uint64_t)((long)(var_59 << 32UL) >> (long)32UL)) {
                        var_66 = *(uint64_t *)4322968UL;
                        var_67 = *(uint64_t *)(var_8 + var_66);
                        var_68 = (uint64_t *)(var_67 + 32UL);
                        _pre_phi98 = var_68;
                        rax_1 = *var_68;
                        rdx_0 = var_66;
                        rcx_0 = var_67;
                    } else {
                        var_60 = *var_53;
                        var_61 = local_sp_2 + (-24L);
                        *(uint64_t *)var_61 = 4207812UL;
                        var_62 = indirect_placeholder(var_60, 0UL);
                        var_63 = *(uint64_t *)4322968UL;
                        var_64 = (uint64_t)((long)(var_62 << 32UL) >> (long)32UL);
                        var_65 = *(uint64_t *)(var_8 + var_63);
                        rdx_0 = var_63;
                        _pre_phi98 = (uint64_t *)(var_65 + 32UL);
                        rax_1 = var_64;
                        rcx_0 = var_65;
                        local_sp_1 = var_61;
                    }
                    var_69 = r12_0 + 1UL;
                    var_70 = *(uint64_t *)4322960UL;
                    *_pre_phi98 = rax_1;
                    r12_0 = var_69;
                    rdx_1 = rdx_0;
                    local_sp_3 = local_sp_1;
                    rcx_1 = rcx_0;
                    if (var_70 <= var_69) {
                        continue;
                    }
                    break;
                }
                if ((var_14 & 16UL) != 0UL) {
                    var_21 = local_sp_3 + (-8L);
                    *(uint64_t *)var_21 = 4207544UL;
                    indirect_placeholder_6();
                    *(uint64_t *)local_sp_3 = rax_2;
                    r83_1 = r83_4;
                    local_sp_2 = var_21;
                    storemerge1_in_sroa_speculated = rax_2;
                    var_52 = (storemerge1_in_sroa_speculated == 0UL);
                    var_53 = (uint64_t *)(local_sp_2 + (-8L));
                    r83_2 = r83_1;
                    r94_2 = r94_1;
                    r83_5 = r83_1;
                    r94_3 = r94_1;
                    if (var_52) {
                        *var_53 = 4208056UL;
                        indirect_placeholder_3(r83_1, r94_1);
                        abort();
                    }
                    *var_53 = 4207566UL;
                    indirect_placeholder_2(storemerge1_in_sroa_speculated);
                    var_54 = *(uint64_t *)4322944UL;
                    var_55 = *(uint64_t *)4322952UL;
                    var_56 = *(uint64_t *)local_sp_2;
                    *(uint64_t *)(var_8 + *(uint64_t *)(((var_54 << 3UL) + var_55) + (-8L))) = var_56;
                    var_57 = *(uint64_t *)(*(uint64_t *)(var_8 + *(uint64_t *)4322968UL) + 32UL);
                    var_58 = local_sp_2 + (-16L);
                    *(uint64_t *)var_58 = 4207616UL;
                    var_59 = indirect_placeholder(var_56, 0UL);
                    local_sp_1 = var_58;
                    if (var_57 > (uint64_t)((long)(var_59 << 32UL) >> (long)32UL)) {
                        var_66 = *(uint64_t *)4322968UL;
                        var_67 = *(uint64_t *)(var_8 + var_66);
                        var_68 = (uint64_t *)(var_67 + 32UL);
                        _pre_phi98 = var_68;
                        rax_1 = *var_68;
                        rdx_0 = var_66;
                        rcx_0 = var_67;
                    } else {
                        var_60 = *var_53;
                        var_61 = local_sp_2 + (-24L);
                        *(uint64_t *)var_61 = 4207812UL;
                        var_62 = indirect_placeholder(var_60, 0UL);
                        var_63 = *(uint64_t *)4322968UL;
                        var_64 = (uint64_t)((long)(var_62 << 32UL) >> (long)32UL);
                        var_65 = *(uint64_t *)(var_8 + var_63);
                        rdx_0 = var_63;
                        _pre_phi98 = (uint64_t *)(var_65 + 32UL);
                        rax_1 = var_64;
                        rcx_0 = var_65;
                        local_sp_1 = var_61;
                    }
                    var_69 = r12_0 + 1UL;
                    var_70 = *(uint64_t *)4322960UL;
                    *_pre_phi98 = rax_1;
                    r12_0 = var_69;
                    rdx_1 = rdx_0;
                    local_sp_3 = local_sp_1;
                    rcx_1 = rcx_0;
                    if (var_70 <= var_69) {
                        continue;
                    }
                    break;
                }
            }
            var_22 = *(uint64_t *)4323104UL;
            var_23 = (uint16_t)var_14;
            var_24 = (uint64_t)(var_23 & (unsigned short)292U);
            rdi_0 = var_22;
            rsi_0 = var_22;
            var_25 = (unsigned __int128)(rsi_0 >> 3UL) * 2361183241434822607ULL;
            var_26 = (uint16_t)rdi_0;
            var_27 = (uint64_t)(var_26 & (unsigned short)1023U);
            var_28 = (uint64_t)(var_25 >> 68ULL);
            var_29 = rsi_0 + (var_28 * 18446744073709550616UL);
            var_30 = var_29 | var_27;
            rsi_0 = var_28;
            while (var_30 != 0UL)
                {
                    rdi_0 = rdi_0 >> 10UL;
                    var_25 = (unsigned __int128)(rsi_0 >> 3UL) * 2361183241434822607ULL;
                    var_26 = (uint16_t)rdi_0;
                    var_27 = (uint64_t)(var_26 & (unsigned short)1023U);
                    var_28 = (uint64_t)(var_25 >> 68ULL);
                    var_29 = rsi_0 + (var_28 * 18446744073709550616UL);
                    var_30 = var_29 | var_27;
                    rsi_0 = var_28;
                }
            var_31 = (var_30 & (-256L)) | (var_29 == 0UL);
            var_32 = (uint64_t)(var_26 & (unsigned short)768U) | (var_27 == 0UL);
            var_33 = var_31 - var_32;
            var_34 = helper_cc_compute_c_wrapper(var_33, var_32, var_6, 14U);
            if (var_34 == 0UL) {
                var_36 = helper_cc_compute_all_wrapper(var_33, var_32, var_6, 14U);
                if ((var_36 & 65UL) == 0UL) {
                    var_38 = (uint64_t)(var_23 & (unsigned short)260U) | 152UL;
                    rdx_2 = var_38;
                    rdx_3 = (rdx_2 & (-65281L)) | ((uint64_t)((uint16_t)rdx_2 & (unsigned short)65024U) | 256UL);
                } else {
                    var_37 = var_24 | 152UL;
                    storemerge4 = var_37;
                    rdx_2 = storemerge4;
                    rdx_3 = storemerge4;
                    if ((storemerge4 & 32UL) == 0UL) {
                        rdx_3 = (rdx_2 & (-65281L)) | ((uint64_t)((uint16_t)rdx_2 & (unsigned short)65024U) | 256UL);
                    }
                }
            } else {
                var_35 = (uint64_t)(var_23 & (unsigned short)260U) | 184UL;
                storemerge4 = var_35;
                rdx_2 = storemerge4;
                rdx_3 = storemerge4;
                if ((storemerge4 & 32UL) == 0UL) {
                    rdx_3 = (rdx_2 & (-65281L)) | ((uint64_t)((uint16_t)rdx_2 & (unsigned short)65024U) | 256UL);
                }
            }
            var_39 = local_sp_3 + 16UL;
            var_40 = local_sp_3 + (-8L);
            *(uint64_t *)var_40 = 4207999UL;
            var_41 = indirect_placeholder_5(1UL, rdx_3, var_22, 1UL, var_39);
            var_42 = var_41.field_0;
            var_43 = var_41.field_1;
            var_44 = var_41.field_2;
            rax_0 = var_42;
            r83_0 = var_43;
            local_sp_0 = var_40;
            r94_0 = var_44;
            var_45 = local_sp_0 + 8UL;
            var_46 = local_sp_0 + (-8L);
            *(uint64_t *)var_46 = 4207772UL;
            var_47 = indirect_placeholder_1(0UL, r83_0, rax_0, var_45, storemerge2, 4287288UL, r94_0);
            var_48 = var_47.field_0;
            var_49 = var_47.field_1;
            var_50 = var_47.field_2;
            r83_1 = var_49;
            local_sp_2 = var_46;
            r94_1 = var_50;
            if ((uint64_t)((uint32_t)var_48 + 1U) == 0UL) {
                var_51 = (uint64_t *)local_sp_0;
                *var_51 = 0UL;
                _pre_phi100 = var_51;
            } else {
                _pre_phi100 = (uint64_t *)local_sp_0;
            }
            storemerge1_in_sroa_speculate_load_bb_get_header_0x13a = *_pre_phi100;
            storemerge1_in_sroa_speculated = storemerge1_in_sroa_speculate_load_bb_get_header_0x13a;
        }
    mrv.field_0 = r83_5;
    mrv1 = mrv;
    mrv1.field_1 = rcx_1;
    mrv2 = mrv1;
    mrv2.field_2 = r94_3;
    return mrv2;
}
