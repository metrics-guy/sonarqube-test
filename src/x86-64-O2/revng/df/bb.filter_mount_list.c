typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_99_ret_type;
struct indirect_placeholder_98_ret_type;
struct indirect_placeholder_100_ret_type;
struct indirect_placeholder_99_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_98_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_100_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_6(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r12(void);
extern uint64_t init_r15(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
extern void indirect_placeholder_15(uint64_t param_0);
extern void indirect_placeholder_3(uint64_t param_0, uint64_t param_1);
extern uint32_t init_state_0x82fc(void);
extern struct indirect_placeholder_99_ret_type indirect_placeholder_99(uint64_t param_0);
extern struct indirect_placeholder_98_ret_type indirect_placeholder_98(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_100_ret_type indirect_placeholder_100(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_filter_mount_list(uint64_t rdi) {
    uint64_t rdi1_2;
    uint64_t rcx_4;
    uint64_t rcx_5;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t rdi1_3;
    uint64_t rcx_6;
    uint64_t rcx_7;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_14;
    struct indirect_placeholder_100_ret_type var_15;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint32_t var_8;
    uint64_t var_9;
    unsigned char *var_10;
    unsigned char var_11;
    uint64_t rdi1_5;
    uint64_t var_62;
    uint64_t r14_0;
    uint64_t var_51;
    uint64_t local_sp_4;
    uint64_t local_sp_1;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t rdi1_0;
    uint64_t local_sp_6;
    uint64_t rcx_0;
    uint64_t rcx_1;
    unsigned char var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t rdi1_1;
    uint64_t rcx_2;
    uint64_t rcx_3;
    unsigned char var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t rax_0;
    uint64_t local_sp_3;
    unsigned char var_63;
    uint64_t var_64;
    uint64_t rbp_2;
    uint64_t var_65;
    uint64_t var_61;
    uint64_t *var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_49;
    uint64_t local_sp_8;
    uint64_t var_50;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t *var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t local_sp_7;
    uint64_t var_26;
    uint64_t var_27;
    unsigned char var_28;
    uint64_t r12_0;
    uint64_t var_72;
    uint64_t rbp_0;
    uint64_t local_sp_5;
    uint64_t r12_2;
    uint64_t local_sp_9;
    uint64_t r12_1;
    uint64_t rbp_1;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_19;
    uint64_t r8_0;
    uint64_t var_20;
    unsigned char *var_21;
    uint64_t var_22;
    struct indirect_placeholder_99_ret_type var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    struct indirect_placeholder_98_ret_type var_71;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_75;
    uint64_t r9_0;
    uint64_t local_sp_10;
    uint64_t rdi1_4;
    uint64_t rax_1;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r12();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_r13();
    var_5 = init_r14();
    var_6 = init_rbp();
    var_7 = init_cc_src2();
    var_8 = init_state_0x82fc();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_1;
    *(uint64_t *)(var_0 + (-40L)) = var_6;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_9 = *(uint64_t *)4323072UL;
    var_10 = (unsigned char *)(var_0 + (-217L));
    var_11 = (unsigned char)rdi;
    *var_10 = var_11;
    rcx_4 = 18446744073709551615UL;
    rcx_5 = 0UL;
    rcx_6 = 18446744073709551615UL;
    rcx_7 = 0UL;
    rdi1_5 = 0UL;
    rcx_0 = 18446744073709551615UL;
    rcx_1 = 0UL;
    rcx_2 = 18446744073709551615UL;
    rcx_3 = 0UL;
    r12_2 = 0UL;
    rbp_1 = 0UL;
    rdi1_4 = 0UL;
    rax_1 = var_9;
    if (var_9 != 0UL) {
        var_12 = *(uint64_t *)(rax_1 + 48UL);
        var_13 = rdi1_4 + 1UL;
        rax_1 = var_12;
        while (var_12 != 0UL)
            {
                rdi1_4 = (uint64_t)(uint32_t)var_13;
                var_12 = *(uint64_t *)(rax_1 + 48UL);
                var_13 = rdi1_4 + 1UL;
                rax_1 = var_12;
            }
        rdi1_5 = (uint64_t)((long)(var_13 << 32UL) >> (long)32UL);
    }
    var_14 = var_0 + (-240L);
    *(uint64_t *)var_14 = 4208310UL;
    var_15 = indirect_placeholder_100(4207264UL, 4206480UL, rdi1_5, 4206496UL, 0UL);
    var_16 = var_15.field_0;
    var_17 = var_15.field_1;
    var_18 = var_15.field_2;
    *(uint64_t *)4323120UL = var_16;
    local_sp_6 = var_14;
    local_sp_9 = var_14;
    r8_0 = var_17;
    r9_0 = var_18;
    local_sp_10 = var_14;
    if (var_16 == 0UL) {
        *(uint64_t *)(local_sp_10 + (-8L)) = 4209015UL;
        indirect_placeholder_3(r8_0, r9_0);
        abort();
    }
    var_19 = *(uint64_t *)4323072UL;
    rbp_2 = var_19;
    if (var_19 == 0UL) {
        if ((uint64_t)var_11 == 0UL) {
            return;
        }
    }
    var_20 = (uint64_t)var_8;
    while (1U)
        {
            var_21 = (unsigned char *)(rbp_2 + 40UL);
            var_22 = (uint64_t)*var_21;
            r14_0 = rbp_2;
            r12_0 = r12_2;
            local_sp_7 = local_sp_6;
            if ((var_22 & 2UL) != 0UL) {
                if (*(unsigned char *)4323117UL != '\x00') {
                    *(uint64_t *)(local_sp_7 + 32UL) = *(uint64_t *)(rbp_2 + 32UL);
                    local_sp_8 = local_sp_7;
                    *(uint64_t *)(local_sp_8 + (-8L)) = 4208371UL;
                    var_66 = indirect_placeholder_99(24UL);
                    var_67 = var_66.field_0;
                    var_68 = *(uint64_t *)4323120UL;
                    *(uint64_t *)(var_67 + 8UL) = rbp_2;
                    var_69 = *(uint64_t *)(local_sp_8 + 24UL);
                    *(uint64_t *)(var_67 + 16UL) = r12_2;
                    *(uint64_t *)var_67 = var_69;
                    var_70 = local_sp_8 + (-16L);
                    *(uint64_t *)var_70 = 4208405UL;
                    var_71 = indirect_placeholder_98(var_68, var_67);
                    r12_0 = var_67;
                    local_sp_5 = var_70;
                    local_sp_10 = var_70;
                    if (var_71.field_0 != 0UL) {
                        r8_0 = var_71.field_1;
                        r9_0 = var_71.field_2;
                        loop_state_var = 0U;
                        break;
                    }
                    var_72 = *(uint64_t *)(rbp_2 + 48UL);
                    rbp_0 = var_72;
                    local_sp_6 = local_sp_5;
                    rbp_2 = rbp_0;
                    r12_2 = r12_0;
                    local_sp_9 = local_sp_5;
                    r12_1 = r12_0;
                    if (rbp_0 == 0UL) {
                        continue;
                    }
                    if ((uint64_t)var_11 != 0UL) {
                        loop_state_var = 2U;
                        break;
                    }
                    *(uint64_t *)4323072UL = 0UL;
                    if (r12_0 != 0UL) {
                        loop_state_var = 3U;
                        break;
                    }
                    loop_state_var = 1U;
                    break;
                }
            }
            if ((var_22 & 1UL) != 0UL) {
                if (*(unsigned char *)4323118UL != '\x00' & *(unsigned char *)4323116UL != '\x00') {
                    *(uint64_t *)(local_sp_7 + 32UL) = *(uint64_t *)(rbp_2 + 32UL);
                    local_sp_8 = local_sp_7;
                    *(uint64_t *)(local_sp_8 + (-8L)) = 4208371UL;
                    var_66 = indirect_placeholder_99(24UL);
                    var_67 = var_66.field_0;
                    var_68 = *(uint64_t *)4323120UL;
                    *(uint64_t *)(var_67 + 8UL) = rbp_2;
                    var_69 = *(uint64_t *)(local_sp_8 + 24UL);
                    *(uint64_t *)(var_67 + 16UL) = r12_2;
                    *(uint64_t *)var_67 = var_69;
                    var_70 = local_sp_8 + (-16L);
                    *(uint64_t *)var_70 = 4208405UL;
                    var_71 = indirect_placeholder_98(var_68, var_67);
                    r12_0 = var_67;
                    local_sp_5 = var_70;
                    local_sp_10 = var_70;
                    if (var_71.field_0 != 0UL) {
                        r8_0 = var_71.field_1;
                        r9_0 = var_71.field_2;
                        loop_state_var = 0U;
                        break;
                    }
                    var_72 = *(uint64_t *)(rbp_2 + 48UL);
                    rbp_0 = var_72;
                    local_sp_6 = local_sp_5;
                    rbp_2 = rbp_0;
                    r12_2 = r12_0;
                    local_sp_9 = local_sp_5;
                    r12_1 = r12_0;
                    if (rbp_0 == 0UL) {
                        continue;
                    }
                    if ((uint64_t)var_11 != 0UL) {
                        loop_state_var = 2U;
                        break;
                    }
                    *(uint64_t *)4323072UL = 0UL;
                    if (r12_0 != 0UL) {
                        loop_state_var = 3U;
                        break;
                    }
                    loop_state_var = 1U;
                    break;
                }
            }
            var_23 = *(uint64_t *)(rbp_2 + 24UL);
            var_24 = local_sp_6 + (-8L);
            *(uint64_t *)var_24 = 4208484UL;
            var_25 = indirect_placeholder_2(var_23);
            local_sp_7 = var_24;
            if ((uint64_t)(unsigned char)var_25 != 0UL) {
                *(uint64_t *)(local_sp_7 + 32UL) = *(uint64_t *)(rbp_2 + 32UL);
                local_sp_8 = local_sp_7;
                *(uint64_t *)(local_sp_8 + (-8L)) = 4208371UL;
                var_66 = indirect_placeholder_99(24UL);
                var_67 = var_66.field_0;
                var_68 = *(uint64_t *)4323120UL;
                *(uint64_t *)(var_67 + 8UL) = rbp_2;
                var_69 = *(uint64_t *)(local_sp_8 + 24UL);
                *(uint64_t *)(var_67 + 16UL) = r12_2;
                *(uint64_t *)var_67 = var_69;
                var_70 = local_sp_8 + (-16L);
                *(uint64_t *)var_70 = 4208405UL;
                var_71 = indirect_placeholder_98(var_68, var_67);
                r12_0 = var_67;
                local_sp_5 = var_70;
                local_sp_10 = var_70;
                if (var_71.field_0 != 0UL) {
                    r8_0 = var_71.field_1;
                    r9_0 = var_71.field_2;
                    loop_state_var = 0U;
                    break;
                }
                var_72 = *(uint64_t *)(rbp_2 + 48UL);
                rbp_0 = var_72;
                local_sp_6 = local_sp_5;
                rbp_2 = rbp_0;
                r12_2 = r12_0;
                local_sp_9 = local_sp_5;
                r12_1 = r12_0;
                if (rbp_0 == 0UL) {
                    continue;
                }
                if ((uint64_t)var_11 != 0UL) {
                    loop_state_var = 2U;
                    break;
                }
                *(uint64_t *)4323072UL = 0UL;
                if (r12_0 != 0UL) {
                    loop_state_var = 3U;
                    break;
                }
                loop_state_var = 1U;
                break;
            }
            var_26 = local_sp_6 + (-16L);
            *(uint64_t *)var_26 = 4208500UL;
            var_27 = indirect_placeholder_2(var_23);
            var_28 = (unsigned char)var_27;
            local_sp_7 = var_26;
            if ((uint64_t)var_28 == 0UL) {
                var_29 = (uint64_t *)(rbp_2 + 8UL);
                var_30 = *var_29;
                var_31 = local_sp_6 + 16UL;
                var_32 = local_sp_6 + (-24L);
                *(uint64_t *)var_32 = 4208524UL;
                var_33 = indirect_placeholder(var_30, var_31);
                local_sp_7 = var_32;
                if ((uint64_t)((uint32_t)var_33 + 1U) != 0UL) {
                    var_34 = *(uint64_t *)(local_sp_6 + 8UL);
                    var_35 = local_sp_6 + (-32L);
                    *(uint64_t *)var_35 = 4208543UL;
                    var_36 = indirect_placeholder_2(var_34);
                    rax_0 = var_36;
                    local_sp_4 = var_35;
                    local_sp_8 = var_35;
                    if (var_36 != 0UL) {
                        var_37 = (uint64_t *)(var_36 + 8UL);
                        var_38 = *var_37;
                        var_39 = *(uint64_t *)(var_38 + 16UL);
                        rdi1_2 = var_39;
                        if (var_39 == 0UL) {
                            *(uint32_t *)var_26 = 0U;
                        } else {
                            var_40 = *(uint64_t *)(rbp_2 + 16UL);
                            rdi1_3 = var_40;
                            if (var_40 == 0UL) {
                                *(uint32_t *)var_26 = 0U;
                            } else {
                                while (rcx_4 != 0UL)
                                    {
                                        var_41 = (uint64_t)(var_28 - *(unsigned char *)rdi1_2);
                                        var_42 = rcx_4 + (-1L);
                                        rcx_4 = var_42;
                                        rcx_5 = var_42;
                                        if (var_41 == 0UL) {
                                            break;
                                        }
                                        rdi1_2 = rdi1_2 + var_20;
                                    }
                                var_43 = 18446744073709551614UL - (-2L);
                                while (rcx_6 != 0UL)
                                    {
                                        var_44 = (uint64_t)(var_28 - *(unsigned char *)rdi1_3);
                                        var_45 = rcx_6 + (-1L);
                                        rcx_6 = var_45;
                                        rcx_7 = var_45;
                                        if (var_44 == 0UL) {
                                            break;
                                        }
                                        rdi1_3 = rdi1_3 + var_20;
                                    }
                                var_46 = 18446744073709551614UL - (-2L);
                                var_47 = helper_cc_compute_c_wrapper(var_43 - var_46, var_46, var_7, 17U);
                                var_48 = (uint64_t)(unsigned char)var_47;
                                *(uint32_t *)var_26 = (uint32_t)var_48;
                                rax_0 = var_48;
                            }
                        }
                        if (*(unsigned char *)4323064UL != '\x00') {
                            var_49 = local_sp_6 + (-40L);
                            *(uint64_t *)var_49 = 4208983UL;
                            indirect_placeholder_6();
                            local_sp_4 = var_49;
                            local_sp_8 = var_49;
                            if ((*var_21 & '\x02') != '\x00' & (*(unsigned char *)(var_38 + 40UL) & '\x02') != '\x00' & (uint64_t)(uint32_t)rax_0 != 0UL) {
                                *(uint64_t *)(local_sp_8 + (-8L)) = 4208371UL;
                                var_66 = indirect_placeholder_99(24UL);
                                var_67 = var_66.field_0;
                                var_68 = *(uint64_t *)4323120UL;
                                *(uint64_t *)(var_67 + 8UL) = rbp_2;
                                var_69 = *(uint64_t *)(local_sp_8 + 24UL);
                                *(uint64_t *)(var_67 + 16UL) = r12_2;
                                *(uint64_t *)var_67 = var_69;
                                var_70 = local_sp_8 + (-16L);
                                *(uint64_t *)var_70 = 4208405UL;
                                var_71 = indirect_placeholder_98(var_68, var_67);
                                r12_0 = var_67;
                                local_sp_5 = var_70;
                                local_sp_10 = var_70;
                                if (var_71.field_0 != 0UL) {
                                    r8_0 = var_71.field_1;
                                    r9_0 = var_71.field_2;
                                    loop_state_var = 0U;
                                    break;
                                }
                                var_72 = *(uint64_t *)(rbp_2 + 48UL);
                                rbp_0 = var_72;
                                local_sp_6 = local_sp_5;
                                rbp_2 = rbp_0;
                                r12_2 = r12_0;
                                local_sp_9 = local_sp_5;
                                r12_1 = r12_0;
                                if (rbp_0 == 0UL) {
                                    continue;
                                }
                                if ((uint64_t)var_11 != 0UL) {
                                    loop_state_var = 2U;
                                    break;
                                }
                                *(uint64_t *)4323072UL = 0UL;
                                if (r12_0 != 0UL) {
                                    loop_state_var = 3U;
                                    break;
                                }
                                loop_state_var = 1U;
                                break;
                            }
                        }
                        var_50 = local_sp_4 + (-8L);
                        *(uint64_t *)var_50 = 4208666UL;
                        indirect_placeholder_6();
                        local_sp_1 = var_50;
                        if (rax_0 == 0UL) {
                            var_51 = local_sp_4 + (-16L);
                            *(uint64_t *)var_51 = 4208684UL;
                            indirect_placeholder_6();
                            local_sp_1 = var_51;
                        }
                        var_52 = *(uint64_t *)(var_38 + 8UL);
                        var_53 = *var_29;
                        rdi1_0 = var_52;
                        rdi1_1 = var_53;
                        local_sp_3 = local_sp_1;
                        while (rcx_0 != 0UL)
                            {
                                var_54 = *(unsigned char *)rdi1_0;
                                var_55 = rcx_0 + (-1L);
                                rcx_0 = var_55;
                                rcx_1 = var_55;
                                if (var_54 == '\x00') {
                                    break;
                                }
                                rdi1_0 = rdi1_0 + var_20;
                            }
                        var_56 = 18446744073709551614UL - (-2L);
                        while (rcx_2 != 0UL)
                            {
                                var_57 = *(unsigned char *)rdi1_1;
                                var_58 = rcx_2 + (-1L);
                                rcx_2 = var_58;
                                rcx_3 = var_58;
                                if (var_57 == '\x00') {
                                    break;
                                }
                                rdi1_1 = rdi1_1 + var_20;
                            }
                        var_59 = 18446744073709551614UL - (-2L);
                        var_60 = local_sp_1 + 16UL;
                        if ((*(uint32_t *)var_60 == 0U) && (var_56 > var_59)) {
                            *var_37 = rbp_2;
                            r14_0 = var_38;
                        } else {
                            *(uint64_t *)(local_sp_1 + 24UL) = var_53;
                            *(uint64_t *)var_60 = var_52;
                            var_61 = local_sp_1 + (-8L);
                            *(uint64_t *)var_61 = 4208920UL;
                            indirect_placeholder_6();
                            local_sp_3 = var_61;
                            if ((uint64_t)(uint32_t)var_59 == 0UL) {
                                var_62 = local_sp_1 + (-16L);
                                *(uint64_t *)var_62 = 4208945UL;
                                indirect_placeholder_6();
                                local_sp_3 = var_62;
                            }
                        }
                        var_63 = *(unsigned char *)(local_sp_3 + 15UL);
                        var_64 = *(uint64_t *)(rbp_2 + 48UL);
                        rbp_0 = var_64;
                        local_sp_5 = local_sp_3;
                        if (var_63 == '\x00') {
                            var_65 = local_sp_3 + (-8L);
                            *(uint64_t *)var_65 = 4208783UL;
                            indirect_placeholder_15(r14_0);
                            local_sp_5 = var_65;
                        }
                        local_sp_6 = local_sp_5;
                        rbp_2 = rbp_0;
                        r12_2 = r12_0;
                        local_sp_9 = local_sp_5;
                        r12_1 = r12_0;
                        if (rbp_0 == 0UL) {
                            continue;
                        }
                        if ((uint64_t)var_11 != 0UL) {
                            loop_state_var = 2U;
                            break;
                        }
                        *(uint64_t *)4323072UL = 0UL;
                        if (r12_0 != 0UL) {
                            loop_state_var = 3U;
                            break;
                        }
                        loop_state_var = 1U;
                        break;
                    }
                }
                *(uint64_t *)(local_sp_7 + 32UL) = *(uint64_t *)(rbp_2 + 32UL);
                local_sp_8 = local_sp_7;
            } else {
                *(uint64_t *)(local_sp_7 + 32UL) = *(uint64_t *)(rbp_2 + 32UL);
                local_sp_8 = local_sp_7;
            }
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(uint64_t *)(local_sp_10 + (-8L)) = 4209015UL;
            indirect_placeholder_3(r8_0, r9_0);
            abort();
        }
        break;
      case 3U:
      case 2U:
      case 1U:
        {
            switch (loop_state_var) {
              case 2U:
                {
                    return;
                }
                break;
              case 3U:
              case 1U:
                {
                    switch (loop_state_var) {
                      case 1U:
                        {
                            var_75 = *(uint64_t *)4323120UL;
                            *(uint64_t *)(local_sp_9 + (-8L)) = 4208857UL;
                            indirect_placeholder_15(var_75);
                            *(uint64_t *)4323120UL = 0UL;
                            return;
                        }
                        break;
                      case 3U:
                        {
                            var_73 = *(uint64_t *)(r12_1 + 8UL);
                            var_74 = *(uint64_t *)(r12_1 + 16UL);
                            *(uint64_t *)(var_73 + 48UL) = rbp_1;
                            r12_1 = var_74;
                            rbp_1 = var_73;
                            do {
                                var_73 = *(uint64_t *)(r12_1 + 8UL);
                                var_74 = *(uint64_t *)(r12_1 + 16UL);
                                *(uint64_t *)(var_73 + 48UL) = rbp_1;
                                r12_1 = var_74;
                                rbp_1 = var_73;
                            } while (var_74 != 0UL);
                            *(uint64_t *)4323072UL = var_73;
                        }
                        break;
                    }
                }
                break;
            }
        }
        break;
    }
}
