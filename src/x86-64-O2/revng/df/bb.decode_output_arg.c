typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_114_ret_type;
struct indirect_placeholder_115_ret_type;
struct indirect_placeholder_116_ret_type;
struct indirect_placeholder_117_ret_type;
struct indirect_placeholder_118_ret_type;
struct indirect_placeholder_119_ret_type;
struct indirect_placeholder_114_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_115_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_116_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_117_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_118_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_119_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder_6(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t init_r15(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t init_r9(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
extern uint64_t init_r8(void);
extern void indirect_placeholder_35(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_114_ret_type indirect_placeholder_114(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_115_ret_type indirect_placeholder_115(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_116_ret_type indirect_placeholder_116(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_117_ret_type indirect_placeholder_117(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_118_ret_type indirect_placeholder_118(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_119_ret_type indirect_placeholder_119(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
void bb_decode_output_arg(uint64_t rdi) {
    struct indirect_placeholder_115_ret_type var_17;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t local_sp_5;
    uint64_t r9_3;
    uint64_t r8_0;
    uint64_t r9_0;
    uint64_t local_sp_1;
    uint64_t r8_1;
    uint32_t var_18;
    uint64_t r9_1;
    uint64_t rbx_0;
    uint64_t var_23;
    uint64_t local_sp_3;
    uint64_t rax_0;
    uint64_t r8_2;
    uint64_t r9_2;
    uint64_t storemerge2;
    uint64_t var_15;
    uint64_t local_sp_4;
    uint64_t var_16;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t storemerge;
    bool var_24;
    uint64_t var_25;
    uint64_t *var_26;
    struct indirect_placeholder_116_ret_type var_27;
    struct indirect_placeholder_117_ret_type var_28;
    uint64_t var_29;
    struct indirect_placeholder_118_ret_type var_30;
    uint64_t var_21;
    struct indirect_placeholder_119_ret_type var_22;
    uint64_t r8_3;
    uint64_t r12_0;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t rax_1;
    uint64_t var_11;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r12();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_r13();
    var_5 = init_r14();
    var_6 = init_r8();
    var_7 = init_rbp();
    var_8 = init_r9();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_1;
    *(uint64_t *)(var_0 + (-40L)) = var_7;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_9 = var_0 + (-64L);
    *(uint64_t *)var_9 = 4214227UL;
    var_10 = indirect_placeholder_2(rdi);
    local_sp_5 = var_9;
    r9_3 = var_8;
    rbx_0 = 0UL;
    var_23 = 11UL;
    storemerge2 = 4286952UL;
    storemerge = var_10;
    r8_3 = var_6;
    r12_0 = 0UL;
    rax_1 = var_10;
    while (1U)
        {
            var_11 = local_sp_5 + (-8L);
            *(uint64_t *)var_11 = 4214256UL;
            indirect_placeholder_6();
            local_sp_4 = var_11;
            if (rax_1 == 0UL) {
                *(unsigned char *)rax_1 = (unsigned char)'\x00';
                r12_0 = rax_1 + 1UL;
            }
            storemerge = r12_0;
            while (1U)
                {
                    var_12 = rbx_0 * 48UL;
                    var_13 = *(uint64_t *)(var_12 + 4322120UL);
                    var_14 = local_sp_4 + (-8L);
                    *(uint64_t *)var_14 = 4214300UL;
                    indirect_placeholder_6();
                    local_sp_4 = var_14;
                    if ((uint64_t)((uint32_t)var_12 & (-16)) != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_15 = rbx_0 + 1UL;
                    storemerge2 = 4287000UL;
                    rbx_0 = var_15;
                    if (rbx_0 == 11UL) {
                        continue;
                    }
                    var_16 = local_sp_4 + (-16L);
                    *(uint64_t *)var_16 = 4214322UL;
                    var_17 = indirect_placeholder_115(r12_0, var_15, storemerge, var_10, var_13, 11UL, storemerge);
                    local_sp_3 = var_16;
                    rax_0 = var_17.field_0;
                    r8_2 = var_17.field_1;
                    r9_2 = var_17.field_6;
                    loop_state_var = 1U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    var_18 = (uint32_t)rbx_0;
                    var_19 = (uint64_t)var_18;
                    var_20 = var_19 * 48UL;
                    var_23 = var_19;
                    rax_1 = var_20;
                    if (*(unsigned char *)(var_20 + 4322156UL) != '\x00') {
                        var_21 = local_sp_4 + (-16L);
                        *(uint64_t *)var_21 = 4214481UL;
                        var_22 = indirect_placeholder_119(r12_0, rbx_0, storemerge, var_10, var_13, var_19, var_13);
                        local_sp_3 = var_21;
                        rax_0 = var_22.field_0;
                        r8_2 = var_22.field_1;
                        r9_2 = var_22.field_6;
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    if ((uint64_t)(var_18 + (-2)) == 0UL) {
                        var_29 = local_sp_4 + (-16L);
                        *(uint64_t *)var_29 = 4214423UL;
                        var_30 = indirect_placeholder_118(r8_3, 2UL, 4287523UL, r9_3);
                        local_sp_1 = var_29;
                        r8_1 = var_30.field_0;
                        r9_1 = var_30.field_1;
                        if (r12_0 != 0UL) {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                    }
                    var_24 = ((uint64_t)(var_18 + (-4)) == 0UL);
                    var_25 = local_sp_4 + (-16L);
                    var_26 = (uint64_t *)var_25;
                    local_sp_1 = var_25;
                    if (var_24) {
                        *var_26 = 4214471UL;
                        var_28 = indirect_placeholder_117(r8_3, 4UL, 4287528UL, r9_3);
                        r8_0 = var_28.field_0;
                        r9_0 = var_28.field_1;
                    } else {
                        *var_26 = 4214390UL;
                        var_27 = indirect_placeholder_116(r8_3, var_19, 0UL, r9_3);
                        r8_0 = var_27.field_0;
                        r9_0 = var_27.field_1;
                    }
                    r8_1 = r8_0;
                    r9_1 = r9_0;
                    if (r12_0 != 0UL) {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    local_sp_5 = local_sp_1;
                    r8_3 = r8_1;
                    r9_3 = r9_1;
                    continue;
                }
                break;
              case 1U:
                {
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(uint64_t *)(local_sp_3 + (-8L)) = 4214341UL;
            indirect_placeholder_114(0UL, r8_2, storemerge2, 0UL, rax_0, 0UL, r9_2);
            *(uint64_t *)(local_sp_3 + (-16L)) = 4214351UL;
            indirect_placeholder_35(r12_0, var_23, 1UL);
            abort();
        }
        break;
      case 1U:
        {
            indirect_placeholder_6();
            return;
        }
        break;
    }
}
