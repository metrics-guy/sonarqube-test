typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_137_ret_type;
struct indirect_placeholder_138_ret_type;
struct indirect_placeholder_137_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_138_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r12(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r15(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder(uint64_t param_0);
extern uint64_t init_rdx(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r9(void);
extern uint64_t indirect_placeholder_8(void);
extern struct indirect_placeholder_137_ret_type indirect_placeholder_137(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_138_ret_type indirect_placeholder_138(uint64_t param_0, uint64_t param_1);
uint64_t bb_yylex(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t r12_4;
    uint64_t var_40;
    uint64_t *var_41;
    uint64_t var_42;
    uint64_t r13_5;
    uint64_t r15_0;
    uint64_t var_43;
    uint64_t var_35;
    struct indirect_placeholder_138_ret_type var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t local_sp_5;
    uint64_t r13_4;
    uint64_t rax_1;
    uint64_t local_sp_1;
    uint64_t local_sp_0;
    uint64_t r12_1;
    uint64_t rax_0;
    uint64_t r14_1;
    uint64_t r12_0;
    uint64_t r13_0;
    uint64_t rdx_1;
    uint64_t r14_0;
    uint64_t rsi2_1;
    uint64_t rdx_0;
    uint64_t rsi2_0;
    uint32_t var_87;
    uint64_t var_88;
    uint64_t var_89;
    unsigned char var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t local_sp_3;
    uint64_t rax_3;
    uint64_t var_94;
    uint64_t local_sp_3_ph;
    uint64_t local_sp_2;
    uint64_t rax_2;
    uint64_t r14_2;
    uint64_t rdx_2;
    uint64_t local_sp_4;
    uint64_t var_114;
    uint64_t var_107;
    uint64_t var_108;
    uint64_t var_109;
    uint64_t *var_110;
    uint64_t var_111;
    uint64_t var_95;
    uint64_t rax_3_ph;
    uint64_t r12_2_ph;
    uint64_t rdx_3_ph;
    uint64_t var_59;
    uint64_t rdx_3;
    uint32_t var_101;
    uint64_t r12_3;
    uint64_t rdx_4;
    uint64_t var_112;
    uint64_t var_113;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t var_98;
    uint64_t *var_99;
    uint64_t var_100;
    uint64_t var_81;
    unsigned char var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t rbx_1;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t rdx_5_be;
    uint64_t r15_2;
    uint64_t r14_3;
    uint64_t rdx_5;
    uint64_t var_47;
    unsigned char var_48;
    uint64_t var_50;
    uint64_t rax_6;
    uint64_t var_49;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t local_sp_10;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t rbx_0_in;
    uint64_t r13_1;
    uint64_t rbx_0;
    uint64_t var_30;
    uint64_t r13_2;
    unsigned char var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    unsigned __int128 var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t local_sp_8;
    uint64_t *_pre_phi;
    uint32_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t *var_77;
    uint64_t var_78;
    uint64_t local_sp_6;
    uint32_t var_115;
    uint64_t var_116;
    uint64_t var_117;
    uint64_t cc_src2_0;
    bool var_118;
    uint64_t var_119;
    uint64_t rax_4_in;
    uint64_t local_sp_10_ph;
    uint64_t rax_5_ph;
    uint64_t r15_1_ph;
    uint64_t r14_5_ph;
    uint64_t rdx_8_ph;
    uint64_t cc_src2_1_ph;
    uint64_t var_60;
    uint64_t local_sp_7;
    uint64_t var_54;
    uint64_t rdx_7;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_24;
    uint64_t r13_3;
    uint64_t r14_4;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    unsigned char var_65;
    uint64_t var_66;
    uint64_t *var_67;
    uint64_t var_68;
    uint64_t *var_69;
    uint64_t var_70;
    uint64_t var_55;
    uint64_t local_sp_9;
    uint64_t rax_4;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t rax_5;
    uint64_t r15_1;
    uint64_t r14_5;
    uint64_t rdx_8;
    uint64_t local_sp_11;
    uint64_t rbx_2;
    uint64_t var_15;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    unsigned char var_22;
    unsigned char var_23;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r12();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_r13();
    var_5 = init_r14();
    var_6 = init_rbp();
    var_7 = init_rdx();
    var_8 = init_r9();
    var_9 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_1;
    *(uint64_t *)(var_0 + (-40L)) = var_6;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_10 = (uint64_t *)rsi;
    var_11 = *var_10;
    *(uint64_t *)(var_0 + (-104L)) = rdi;
    var_12 = (uint64_t)*(unsigned char *)var_11;
    var_13 = var_0 + (-128L);
    *(uint64_t *)var_13 = 4218008UL;
    var_14 = indirect_placeholder(var_12);
    r13_5 = 63UL;
    r13_0 = 8UL;
    rbx_1 = 0UL;
    rdx_5 = 0UL;
    local_sp_10_ph = var_13;
    rax_5_ph = var_14;
    r15_1_ph = var_12;
    r14_5_ph = var_11;
    rdx_8_ph = var_7;
    cc_src2_1_ph = var_9;
    while (1U)
        {
            local_sp_10 = local_sp_10_ph;
            cc_src2_0 = cc_src2_1_ph;
            rax_5 = rax_5_ph;
            r15_1 = r15_1_ph;
            r14_5 = r14_5_ph;
            rdx_8 = rdx_8_ph;
            while (1U)
                {
                    r15_2 = r15_1;
                    rax_6 = rax_5;
                    rdx_8 = 0UL;
                    local_sp_11 = local_sp_10;
                    rbx_2 = r14_5;
                    var_15 = rbx_2 + 1UL;
                    r14_3 = rbx_2;
                    rbx_0_in = rbx_2;
                    rax_4_in = rbx_2;
                    r13_3 = r15_2;
                    r14_4 = rbx_2;
                    rbx_2 = var_15;
                    while ((uint64_t)(unsigned char)rax_6 != 0UL)
                        {
                            *var_10 = var_15;
                            var_16 = (uint64_t)*(unsigned char *)var_15;
                            var_17 = local_sp_11 + (-8L);
                            *(uint64_t *)var_17 = 4218035UL;
                            var_18 = indirect_placeholder(var_16);
                            local_sp_11 = var_17;
                            rax_6 = var_18;
                            r15_2 = var_16;
                            var_15 = rbx_2 + 1UL;
                            r14_3 = rbx_2;
                            rbx_0_in = rbx_2;
                            rax_4_in = rbx_2;
                            r13_3 = r15_2;
                            r14_4 = rbx_2;
                            rbx_2 = var_15;
                        }
                    var_19 = (uint64_t)(uint32_t)r15_2;
                    var_20 = local_sp_11 + (-8L);
                    *(uint64_t *)var_20 = 4218057UL;
                    var_21 = indirect_placeholder(var_19);
                    var_22 = (unsigned char)r15_2;
                    var_23 = var_22 + '\xd5';
                    r12_4 = var_19;
                    local_sp_7 = var_20;
                    local_sp_9 = var_20;
                    if ((uint64_t)(unsigned char)var_21 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    r13_5 = 0UL;
                    if ((uint64_t)(var_23 & '\xfd') != 0UL) {
                        loop_state_var = 2U;
                        break;
                    }
                    var_25 = local_sp_11 + (-16L);
                    *(uint64_t *)var_25 = 4218280UL;
                    var_26 = indirect_placeholder(var_19);
                    local_sp_5 = var_25;
                    if ((uint64_t)(unsigned char)var_26 != 0UL) {
                        var_27 = (uint64_t)(uint32_t)var_21;
                        var_28 = local_sp_11 + 16UL;
                        var_29 = local_sp_11 + 35UL;
                        r13_1 = var_28;
                        loop_state_var = 3U;
                        break;
                    }
                    if ((uint64_t)(var_22 + '\xd8') != 0UL) {
                        *var_10 = var_15;
                        var_44 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)rbx_2;
                        *(uint64_t *)(local_sp_11 + (-24L)) = 4219015UL;
                        var_45 = indirect_placeholder(var_44);
                        var_46 = (uint64_t)(unsigned char)var_45;
                        r13_5 = var_46;
                        loop_state_var = 0U;
                        break;
                    }
                    while (1U)
                        {
                            var_47 = r14_3 + 1UL;
                            *var_10 = var_47;
                            var_48 = *(unsigned char *)r14_3;
                            r14_3 = var_47;
                            r14_5 = var_47;
                            if (var_48 != '\x00') {
                                loop_state_var = 0U;
                                break;
                            }
                            if ((uint64_t)(var_48 + '\xd8') == 0UL) {
                                var_50 = rdx_5 + 1UL;
                                rdx_5_be = var_50;
                                if (var_50 != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                            }
                            var_49 = rdx_5 + ((uint64_t)(var_48 + '\xd7') == 0UL);
                            rdx_5_be = var_49;
                            if (var_49 != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            rdx_5 = rdx_5_be;
                            continue;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 1U:
                        {
                            var_51 = (uint64_t)*(unsigned char *)var_47;
                            var_52 = local_sp_11 + (-24L);
                            *(uint64_t *)var_52 = 4218370UL;
                            var_53 = indirect_placeholder(var_51);
                            local_sp_10 = var_52;
                            rax_5 = var_53;
                            r15_1 = var_51;
                            continue;
                        }
                        break;
                      case 0U:
                        {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    var_24 = (rdx_8 & (-256L)) | ((uint64_t)(var_22 + '\xd3') == 0UL);
                    rdx_7 = var_24;
                    if ((uint64_t)(var_23 & '\xfd') != 0UL) {
                        *(uint32_t *)(local_sp_11 + 4UL) = 0U;
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    var_55 = helper_cc_compute_c_wrapper(rdx_7 + (-1L), 1UL, cc_src2_1_ph, 14U);
                    *(uint32_t *)(local_sp_11 + 4UL) = (((0U - (uint32_t)var_55) & 2U) + (-1));
                    rdx_8_ph = rdx_7;
                    cc_src2_1_ph = var_55;
                    cc_src2_0 = var_55;
                    rax_4 = rax_4_in + 1UL;
                    *var_10 = rax_4;
                    var_56 = (uint64_t)*(unsigned char *)rax_4;
                    var_57 = local_sp_9 + (-8L);
                    *(uint64_t *)var_57 = 4218133UL;
                    var_58 = indirect_placeholder(var_56);
                    rax_4_in = rax_4;
                    r15_1_ph = var_56;
                    r14_5_ph = rax_4;
                    r13_3 = var_56;
                    r14_4 = rax_4;
                    local_sp_9 = var_57;
                    do {
                        rax_4 = rax_4_in + 1UL;
                        *var_10 = rax_4;
                        var_56 = (uint64_t)*(unsigned char *)rax_4;
                        var_57 = local_sp_9 + (-8L);
                        *(uint64_t *)var_57 = 4218133UL;
                        var_58 = indirect_placeholder(var_56);
                        rax_4_in = rax_4;
                        r15_1_ph = var_56;
                        r14_5_ph = rax_4;
                        r13_3 = var_56;
                        r14_4 = rax_4;
                        local_sp_9 = var_57;
                    } while ((uint64_t)(unsigned char)var_58 != 0UL);
                    var_59 = local_sp_9 + (-16L);
                    *(uint64_t *)var_59 = 4218153UL;
                    var_60 = indirect_placeholder(var_56);
                    local_sp_10_ph = var_59;
                    rax_5_ph = var_60;
                    local_sp_7 = var_59;
                    if ((uint64_t)(unsigned char)var_60 == 0UL) {
                        continue;
                    }
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 2U:
                {
                    var_54 = (rdx_8 & (-256L)) | ((uint64_t)(var_22 + '\xd3') == 0UL);
                    rdx_7 = var_54;
                    var_55 = helper_cc_compute_c_wrapper(rdx_7 + (-1L), 1UL, cc_src2_1_ph, 14U);
                    *(uint32_t *)(local_sp_11 + 4UL) = (((0U - (uint32_t)var_55) & 2U) + (-1));
                    rdx_8_ph = rdx_7;
                    cc_src2_1_ph = var_55;
                    cc_src2_0 = var_55;
                    rax_4 = rax_4_in + 1UL;
                    *var_10 = rax_4;
                    var_56 = (uint64_t)*(unsigned char *)rax_4;
                    var_57 = local_sp_9 + (-8L);
                    *(uint64_t *)var_57 = 4218133UL;
                    var_58 = indirect_placeholder(var_56);
                    rax_4_in = rax_4;
                    r15_1_ph = var_56;
                    r14_5_ph = rax_4;
                    r13_3 = var_56;
                    r14_4 = rax_4;
                    local_sp_9 = var_57;
                    do {
                        rax_4 = rax_4_in + 1UL;
                        *var_10 = rax_4;
                        var_56 = (uint64_t)*(unsigned char *)rax_4;
                        var_57 = local_sp_9 + (-8L);
                        *(uint64_t *)var_57 = 4218133UL;
                        var_58 = indirect_placeholder(var_56);
                        rax_4_in = rax_4;
                        r15_1_ph = var_56;
                        r14_5_ph = rax_4;
                        r13_3 = var_56;
                        r14_4 = rax_4;
                        local_sp_9 = var_57;
                    } while ((uint64_t)(unsigned char)var_58 != 0UL);
                    var_59 = local_sp_9 + (-16L);
                    *(uint64_t *)var_59 = 4218153UL;
                    var_60 = indirect_placeholder(var_56);
                    local_sp_10_ph = var_59;
                    rax_5_ph = var_60;
                    local_sp_7 = var_59;
                    if ((uint64_t)(unsigned char)var_60 == 0UL) {
                        continue;
                    }
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 0U:
                {
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 3U:
                {
                    loop_state_var = 2U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            return r13_5;
        }
        break;
      case 1U:
        {
            local_sp_8 = local_sp_7;
            r15_0 = r14_4;
            r13_4 = r13_3;
            while (1U)
                {
                    var_61 = (uint64_t)((long)(((*(uint32_t *)(local_sp_8 + 12UL) == 4294967295U) ? (48UL - r13_4) : (r13_4 + 4294967248UL)) << 32UL) >> (long)32UL);
                    var_62 = rbx_1 + var_61;
                    var_63 = helper_cc_compute_all_wrapper(var_62, var_61, 0UL, 9U);
                    if ((uint64_t)((uint16_t)var_63 & (unsigned short)2048U) != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_64 = r15_0 + 1UL;
                    var_65 = *(unsigned char *)var_64;
                    var_66 = (uint64_t)var_65;
                    var_67 = (uint64_t *)local_sp_8;
                    *var_67 = var_64;
                    var_68 = local_sp_8 + (-8L);
                    var_69 = (uint64_t *)var_68;
                    *var_69 = 4218233UL;
                    var_70 = indirect_placeholder(var_66);
                    _pre_phi = var_69;
                    local_sp_6 = var_68;
                    local_sp_8 = var_68;
                    r13_4 = var_66;
                    if ((uint64_t)(unsigned char)var_70 == 0UL) {
                        var_71 = (unsigned __int128)var_62 * 10ULL;
                        var_72 = (uint64_t)var_71;
                        var_73 = helper_cc_compute_all_wrapper(var_72, (uint64_t)((long)var_72 >> (long)63UL) - (uint64_t)(var_71 >> 64ULL), 0UL, 5U);
                        rbx_1 = var_72;
                        if ((uint64_t)((uint16_t)var_73 & (unsigned short)2048U) != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        r15_0 = *var_69;
                        continue;
                    }
                    if (((var_65 + '\xd4') & '\xfd') != '\x00') {
                        loop_state_var = 2U;
                        break;
                    }
                    var_74 = (uint32_t)(uint64_t)*(unsigned char *)(r15_0 + 2UL);
                    var_75 = (uint64_t)var_74;
                    var_76 = local_sp_8 + (-16L);
                    var_77 = (uint64_t *)var_76;
                    *var_77 = 4218459UL;
                    var_78 = indirect_placeholder(var_75);
                    _pre_phi = var_77;
                    local_sp_6 = var_76;
                    if ((uint64_t)(unsigned char)var_78 != 0UL) {
                        loop_state_var = 2U;
                        break;
                    }
                    var_79 = local_sp_8 + (-24L);
                    *(uint64_t *)var_79 = 4218468UL;
                    var_80 = indirect_placeholder_8();
                    if ((uint64_t)(unsigned char)var_80 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_81 = r15_0 + 3UL;
                    var_82 = *(unsigned char *)var_81;
                    var_83 = (uint64_t)(uint32_t)(uint64_t)var_82;
                    var_84 = (uint64_t)(var_74 + (-48));
                    *var_67 = var_81;
                    *(uint32_t *)var_79 = (uint32_t)var_82;
                    var_85 = local_sp_8 + (-32L);
                    *(uint64_t *)var_85 = 4218519UL;
                    var_86 = indirect_placeholder(var_83);
                    r13_5 = 276UL;
                    local_sp_0 = var_85;
                    rax_0 = var_86;
                    r12_0 = var_84;
                    r14_0 = var_83;
                    rdx_0 = *var_69;
                    rsi2_0 = (uint64_t)*(uint32_t *)var_85;
                    loop_state_var = 0U;
                    break;
                }
            switch (loop_state_var) {
              case 2U:
                {
                    var_115 = *(uint32_t *)(local_sp_6 + 12UL);
                    var_116 = *(uint64_t *)(local_sp_6 + 16UL);
                    var_117 = *_pre_phi;
                    *(uint64_t *)(var_116 + 8UL) = var_62;
                    *(unsigned char *)var_116 = (unsigned char)(var_115 >> 31U);
                    var_118 = (var_115 == 0U);
                    *(uint64_t *)(var_116 + 16UL) = (var_117 - r14_4);
                    var_119 = var_118 ? 275UL : 274UL;
                    *var_10 = var_117;
                    r13_5 = var_119;
                }
                break;
              case 0U:
                {
                    var_94 = (uint64_t)((uint32_t)r13_0 + (-1));
                    local_sp_0 = local_sp_1;
                    rax_0 = rax_1;
                    r12_0 = r12_1;
                    r13_0 = var_94;
                    r14_0 = r14_1;
                    rdx_0 = rdx_1;
                    rsi2_0 = rsi2_1;
                    local_sp_3_ph = local_sp_1;
                    local_sp_2 = local_sp_1;
                    rax_2 = rax_1;
                    r14_2 = r14_1;
                    rdx_2 = rdx_1;
                    rax_3_ph = rax_1;
                    r12_2_ph = r12_1;
                    rdx_3_ph = rdx_1;
                    r12_3 = r12_1;
                    do {
                        var_87 = (uint32_t)r12_0 * 10U;
                        rax_1 = rax_0;
                        local_sp_1 = local_sp_0;
                        r12_1 = (uint64_t)(var_87 & (-2));
                        r14_1 = r14_0;
                        rdx_1 = rdx_0;
                        rsi2_1 = rsi2_0;
                        if ((uint64_t)(unsigned char)rax_0 == 0UL) {
                            var_88 = (uint64_t)(((uint32_t)rsi2_0 + var_87) + (-48));
                            var_89 = rdx_0 + 1UL;
                            var_90 = *(unsigned char *)var_89;
                            var_91 = (uint64_t)(uint32_t)(uint64_t)var_90;
                            *(uint32_t *)local_sp_0 = (uint32_t)var_90;
                            var_92 = local_sp_0 + (-8L);
                            *(uint64_t *)var_92 = 4218565UL;
                            var_93 = indirect_placeholder(var_91);
                            rax_1 = var_93;
                            local_sp_1 = var_92;
                            r12_1 = var_88;
                            r14_1 = var_91;
                            rdx_1 = var_89;
                            rsi2_1 = (uint64_t)*(uint32_t *)var_92;
                        }
                        var_94 = (uint64_t)((uint32_t)r13_0 + (-1));
                        local_sp_0 = local_sp_1;
                        rax_0 = rax_1;
                        r12_0 = r12_1;
                        r13_0 = var_94;
                        r14_0 = r14_1;
                        rdx_0 = rdx_1;
                        rsi2_0 = rsi2_1;
                        local_sp_3_ph = local_sp_1;
                        local_sp_2 = local_sp_1;
                        rax_2 = rax_1;
                        r14_2 = r14_1;
                        rdx_2 = rdx_1;
                        rax_3_ph = rax_1;
                        r12_2_ph = r12_1;
                        rdx_3_ph = rdx_1;
                        r12_3 = r12_1;
                    } while (var_94 != 0UL);
                    if (*(uint32_t *)(local_sp_1 + 12UL) != 4294967295U) {
                        local_sp_3 = local_sp_3_ph;
                        rax_3 = rax_3_ph;
                        rdx_3 = rdx_3_ph;
                        r12_3 = r12_2_ph;
                        local_sp_4 = local_sp_3;
                        rdx_4 = rdx_3;
                        while ((uint64_t)(unsigned char)rax_3 != 0UL)
                            {
                                var_96 = rdx_3 + 1UL;
                                var_97 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)var_96;
                                *(uint64_t *)local_sp_3 = var_96;
                                var_98 = local_sp_3 + (-8L);
                                var_99 = (uint64_t *)var_98;
                                *var_99 = 4218673UL;
                                var_100 = indirect_placeholder(var_97);
                                local_sp_3 = var_98;
                                rax_3 = var_100;
                                rdx_3 = *var_99;
                                local_sp_4 = local_sp_3;
                                rdx_4 = rdx_3;
                            }
                        var_101 = *(uint32_t *)(local_sp_3 + 12UL);
                        if (((int)var_101 > (int)4294967295U) || ((uint64_t)(uint32_t)r12_2_ph == 0UL)) {
                            r13_5 = 63UL;
                            if (var_62 != 9223372036854775808UL) {
                                var_112 = *(uint64_t *)(local_sp_4 + 16UL);
                                var_113 = (uint64_t)((long)(4294967296000000000UL - (r12_3 << 32UL)) >> (long)32UL);
                                *(uint64_t *)var_112 = (var_62 + (-1L));
                                *(uint64_t *)(var_112 + 8UL) = var_113;
                                *var_10 = rdx_4;
                                r13_5 = 276UL;
                            }
                            return r13_5;
                        }
                        var_102 = (uint64_t)var_101 + (-1L);
                        var_103 = *(uint64_t *)(local_sp_3 + 16UL);
                        var_104 = (uint64_t)((long)(r12_2_ph << 32UL) >> (long)32UL);
                        var_105 = helper_cc_compute_c_wrapper(var_102, 1UL, cc_src2_0, 16U);
                        *(uint64_t *)var_103 = var_62;
                        *(uint64_t *)(var_103 + 8UL) = var_104;
                        var_106 = (uint64_t)((uint32_t)var_105 + 276U);
                        *var_10 = rdx_3;
                        r13_5 = var_106;
                        return r13_5;
                    }
                    while (1U)
                        {
                            local_sp_3_ph = local_sp_2;
                            local_sp_4 = local_sp_2;
                            rax_3_ph = rax_2;
                            rdx_3_ph = rdx_2;
                            rdx_4 = rdx_2;
                            if ((uint64_t)(unsigned char)rax_2 != 0UL) {
                                if (r12_1 != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                var_114 = *(uint64_t *)(local_sp_2 + 16UL);
                                *(uint64_t *)var_114 = var_62;
                                *(uint64_t *)(var_114 + 8UL) = 0UL;
                                *var_10 = rdx_2;
                                loop_state_var = 2U;
                                break;
                            }
                            if ((uint64_t)((unsigned char)r14_2 + '\xd0') != 0UL) {
                                var_95 = (uint64_t)((uint32_t)r12_1 + 1U);
                                r12_2_ph = var_95;
                                loop_state_var = 0U;
                                break;
                            }
                            var_107 = rdx_2 + 1UL;
                            var_108 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)var_107;
                            *(uint64_t *)local_sp_2 = var_107;
                            var_109 = local_sp_2 + (-8L);
                            var_110 = (uint64_t *)var_109;
                            *var_110 = 4218798UL;
                            var_111 = indirect_placeholder(var_108);
                            local_sp_2 = var_109;
                            rax_2 = var_111;
                            r14_2 = var_108;
                            rdx_2 = *var_110;
                            continue;
                        }
                    switch (loop_state_var) {
                      case 0U:
                        {
                            local_sp_3 = local_sp_3_ph;
                            rax_3 = rax_3_ph;
                            rdx_3 = rdx_3_ph;
                            r12_3 = r12_2_ph;
                            local_sp_4 = local_sp_3;
                            rdx_4 = rdx_3;
                            while ((uint64_t)(unsigned char)rax_3 != 0UL)
                                {
                                    var_96 = rdx_3 + 1UL;
                                    var_97 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)var_96;
                                    *(uint64_t *)local_sp_3 = var_96;
                                    var_98 = local_sp_3 + (-8L);
                                    var_99 = (uint64_t *)var_98;
                                    *var_99 = 4218673UL;
                                    var_100 = indirect_placeholder(var_97);
                                    local_sp_3 = var_98;
                                    rax_3 = var_100;
                                    rdx_3 = *var_99;
                                    local_sp_4 = local_sp_3;
                                    rdx_4 = rdx_3;
                                }
                            var_101 = *(uint32_t *)(local_sp_3 + 12UL);
                            if (!(((int)var_101 > (int)4294967295U) || ((uint64_t)(uint32_t)r12_2_ph == 0UL))) {
                                var_102 = (uint64_t)var_101 + (-1L);
                                var_103 = *(uint64_t *)(local_sp_3 + 16UL);
                                var_104 = (uint64_t)((long)(r12_2_ph << 32UL) >> (long)32UL);
                                var_105 = helper_cc_compute_c_wrapper(var_102, 1UL, cc_src2_0, 16U);
                                *(uint64_t *)var_103 = var_62;
                                *(uint64_t *)(var_103 + 8UL) = var_104;
                                var_106 = (uint64_t)((uint32_t)var_105 + 276U);
                                *var_10 = rdx_3;
                                r13_5 = var_106;
                                return r13_5;
                            }
                            r13_5 = 63UL;
                            if (var_62 == 9223372036854775808UL) {
                                var_112 = *(uint64_t *)(local_sp_4 + 16UL);
                                var_113 = (uint64_t)((long)(4294967296000000000UL - (r12_3 << 32UL)) >> (long)32UL);
                                *(uint64_t *)var_112 = (var_62 + (-1L));
                                *(uint64_t *)(var_112 + 8UL) = var_113;
                                *var_10 = rdx_4;
                                r13_5 = 276UL;
                            }
                        }
                        break;
                      case 1U:
                        {
                            r13_5 = 63UL;
                            if (var_62 == 9223372036854775808UL) {
                                var_112 = *(uint64_t *)(local_sp_4 + 16UL);
                                var_113 = (uint64_t)((long)(4294967296000000000UL - (r12_3 << 32UL)) >> (long)32UL);
                                *(uint64_t *)var_112 = (var_62 + (-1L));
                                *(uint64_t *)(var_112 + 8UL) = var_113;
                                *var_10 = rdx_4;
                                r13_5 = 276UL;
                            }
                        }
                        break;
                    }
                }
                break;
            }
        }
        break;
      case 2U:
        {
            while (1U)
                {
                    rbx_0 = rbx_0_in + 1UL;
                    var_30 = helper_cc_compute_c_wrapper(r13_1 - var_29, var_29, cc_src2_1_ph, 17U);
                    rbx_0_in = rbx_0;
                    r13_2 = r13_1;
                    if (var_30 == 0UL) {
                        *(unsigned char *)r13_1 = (unsigned char)r12_4;
                        r13_2 = r13_1 + 1UL;
                    }
                    *var_10 = rbx_0;
                    var_31 = *(unsigned char *)rbx_0;
                    var_32 = (uint64_t)var_31;
                    var_33 = local_sp_5 + (-8L);
                    *(uint64_t *)var_33 = 4218944UL;
                    var_34 = indirect_placeholder(var_32);
                    local_sp_5 = var_33;
                    r12_4 = var_32;
                    r13_1 = r13_2;
                    if (!((uint64_t)(unsigned char)var_34 == 0UL && (uint64_t)(var_31 + '\xd2') == 0UL)) {
                        continue;
                    }
                    break;
                }
            *(unsigned char *)r13_2 = (unsigned char)'\x00';
            var_35 = local_sp_5 + 24UL;
            *(uint64_t *)(local_sp_5 + (-16L)) = 4218972UL;
            var_36 = indirect_placeholder_138(rsi, var_35);
            var_37 = var_36.field_0;
            var_38 = var_36.field_1;
            var_39 = var_36.field_2;
            if (var_37 == 0UL) {
                var_40 = (uint64_t)*(uint32_t *)(var_37 + 12UL);
                var_41 = *(uint64_t **)local_sp_5;
                var_42 = (uint64_t)*(uint32_t *)(var_37 + 8UL);
                *var_41 = var_40;
                r13_5 = var_42;
            } else {
                if (*(unsigned char *)(rsi + 217UL) == '\x00') {
                    var_43 = local_sp_5 + 16UL;
                    *(uint64_t *)(local_sp_5 + (-24L)) = 4219067UL;
                    indirect_placeholder_137(0UL, var_27, var_38, 4300805UL, var_39, var_8, var_43);
                }
            }
        }
        break;
    }
}
