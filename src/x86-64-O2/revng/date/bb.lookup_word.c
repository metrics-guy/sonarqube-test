typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_lookup_word_ret_type;
struct bb_lookup_word_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder_1(void);
extern uint64_t init_rax(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r12(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r15(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder(uint64_t param_0);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1);
extern uint64_t init_rdx(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_rcx(void);
extern uint32_t init_state_0x82fc(void);
struct bb_lookup_word_ret_type bb_lookup_word(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint32_t var_11;
    uint64_t var_12;
    unsigned char *var_13;
    unsigned char var_14;
    uint64_t rax_1;
    uint64_t r12_6;
    uint64_t local_sp_0;
    unsigned char var_61;
    uint64_t var_62;
    uint64_t r12_0;
    uint64_t rdx_0;
    uint64_t local_sp_8;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t rax_0;
    uint64_t r15_0;
    uint64_t rdx_1;
    uint64_t rcx_0;
    bool var_65;
    uint64_t var_66;
    uint64_t spec_select;
    uint64_t spec_select121;
    uint64_t var_67;
    unsigned char var_68;
    uint64_t cc_dst_1;
    uint64_t rcx_4;
    uint64_t var_50;
    uint64_t var_69;
    uint64_t local_sp_3;
    uint64_t var_56;
    uint64_t r12_3;
    unsigned char *_cast1;
    uint64_t r12_1;
    uint64_t local_sp_1;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t r12_2;
    uint64_t local_sp_2;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_46;
    uint32_t cc_op_0;
    uint64_t cc_src_0;
    uint64_t cc_dst_0;
    uint64_t rdi3_0;
    uint64_t rcx_3;
    uint64_t rsi4_0;
    uint32_t cc_op_1;
    uint64_t cc_src_1;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    bool var_54;
    uint64_t var_55;
    uint64_t local_sp_5;
    uint64_t rdx_3;
    uint64_t storemerge;
    uint64_t r15_2;
    uint64_t var_43;
    uint64_t r12_5;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t r12_7;
    uint64_t r12_4;
    uint64_t rdx_4;
    uint64_t rcx_5;
    struct bb_lookup_word_ret_type mrv;
    struct bb_lookup_word_ret_type mrv1;
    struct bb_lookup_word_ret_type mrv2;
    uint64_t var_24;
    uint64_t rbx_0;
    uint32_t var_25;
    uint64_t var_26;
    uint32_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    bool var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    bool r14_0;
    uint64_t var_40;
    uint64_t rdx_5;
    uint64_t var_41;
    uint64_t *var_42;
    uint64_t rdi3_1_in;
    uint64_t local_sp_6;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    unsigned char var_21;
    uint64_t local_sp_7;
    bool var_22;
    uint64_t var_23;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_r12();
    var_3 = init_rbx();
    var_4 = init_r15();
    var_5 = init_r13();
    var_6 = init_r14();
    var_7 = init_rbp();
    var_8 = init_rdx();
    var_9 = init_rcx();
    var_10 = init_cc_src2();
    var_11 = init_state_0x82fc();
    *(uint64_t *)(var_0 + (-8L)) = var_4;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_2;
    *(uint64_t *)(var_0 + (-40L)) = var_7;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_12 = var_0 + (-56L);
    var_13 = (unsigned char *)rsi;
    var_14 = *var_13;
    rax_1 = var_1;
    r12_6 = rsi;
    r12_0 = 4305120UL;
    rdx_0 = 65UL;
    rdx_1 = rsi;
    rcx_0 = rsi;
    rcx_4 = 0UL;
    r12_3 = 4306720UL;
    r12_1 = 4306368UL;
    r12_2 = 4306720UL;
    cc_op_0 = 25U;
    cc_src_0 = 16UL;
    cc_dst_0 = 0UL;
    rdi3_0 = 4301037UL;
    rcx_3 = 4UL;
    rsi4_0 = rsi;
    r15_2 = var_4;
    r12_5 = 4306912UL;
    r12_7 = 4307328UL;
    rdx_4 = var_8;
    rcx_5 = var_9;
    rbx_0 = rsi;
    r14_0 = 0;
    rdi3_1_in = (uint64_t)var_14;
    local_sp_6 = var_12;
    local_sp_7 = var_12;
    if ((uint64_t)var_14 == 0UL) {
        var_15 = (uint64_t)(uint32_t)rdi3_1_in;
        *(uint64_t *)(local_sp_6 + (-8L)) = 4216037UL;
        var_16 = indirect_placeholder(var_15);
        var_17 = r12_6 + 1UL;
        var_18 = (uint64_t)(unsigned char)var_16;
        var_19 = local_sp_6 + (-16L);
        *(uint64_t *)var_19 = 4216049UL;
        var_20 = indirect_placeholder(var_18);
        *(unsigned char *)r12_6 = (unsigned char)var_20;
        var_21 = *(unsigned char *)var_17;
        rax_1 = var_20;
        r12_6 = var_17;
        rdi3_1_in = (uint64_t)var_21;
        local_sp_6 = var_19;
        local_sp_7 = var_19;
        do {
            var_15 = (uint64_t)(uint32_t)rdi3_1_in;
            *(uint64_t *)(local_sp_6 + (-8L)) = 4216037UL;
            var_16 = indirect_placeholder(var_15);
            var_17 = r12_6 + 1UL;
            var_18 = (uint64_t)(unsigned char)var_16;
            var_19 = local_sp_6 + (-16L);
            *(uint64_t *)var_19 = 4216049UL;
            var_20 = indirect_placeholder(var_18);
            *(unsigned char *)r12_6 = (unsigned char)var_20;
            var_21 = *(unsigned char *)var_17;
            rax_1 = var_20;
            r12_6 = var_17;
            rdi3_1_in = (uint64_t)var_21;
            local_sp_6 = var_19;
            local_sp_7 = var_19;
        } while ((uint64_t)var_21 != 0UL);
    }
    var_22 = ((uint64_t)(uint32_t)rax_1 == 0UL);
    local_sp_8 = local_sp_7;
    while (1U)
        {
            var_23 = local_sp_8 + (-8L);
            *(uint64_t *)var_23 = 4216102UL;
            indirect_placeholder_1();
            r12_4 = r12_7;
            local_sp_5 = var_23;
            local_sp_8 = var_23;
            if (!var_22) {
                loop_state_var = 1U;
                break;
            }
            var_24 = r12_7 + 16UL;
            r12_7 = var_24;
            if (*(uint64_t *)var_24 == 0UL) {
                continue;
            }
            loop_state_var = 0U;
            break;
        }
    switch (loop_state_var) {
      case 1U:
        {
            mrv.field_0 = r12_4;
            mrv1 = mrv;
            mrv1.field_1 = rdx_4;
            mrv2 = mrv1;
            mrv2.field_2 = rcx_5;
            return mrv2;
        }
        break;
      case 0U:
        {
            var_25 = *(uint32_t *)rbx_0;
            var_26 = rbx_0 + 4UL;
            var_27 = (var_25 + (-16843009)) & (var_25 ^ (-1));
            var_28 = (uint64_t)var_27;
            var_29 = (uint64_t)(var_27 & (-2139062144));
            rbx_0 = var_26;
            do {
                var_25 = *(uint32_t *)rbx_0;
                var_26 = rbx_0 + 4UL;
                var_27 = (var_25 + (-16843009)) & (var_25 ^ (-1));
                var_28 = (uint64_t)var_27;
                var_29 = (uint64_t)(var_27 & (-2139062144));
                rbx_0 = var_26;
            } while (var_29 != 0UL);
            var_30 = var_29 >> 16UL;
            var_31 = ((uint64_t)((uint16_t)var_28 & (unsigned short)32896U) == 0UL);
            var_32 = var_31 ? var_30 : var_29;
            var_33 = rbx_0 + 6UL;
            var_34 = var_31 ? var_33 : var_26;
            var_35 = var_32 << 1UL;
            var_36 = (uint64_t)((uint32_t)var_32 & (-2139062272));
            var_37 = helper_cc_compute_c_wrapper(var_35, var_32, var_10, 6U);
            var_38 = (var_34 + (-3L)) - var_37;
            var_39 = var_38 - rsi;
            rcx_5 = var_36;
            rdx_5 = var_33;
            switch (var_39) {
              case 4UL:
                {
                    r14_0 = (*(unsigned char *)(rsi + 3UL) != '.');
                }
                break;
              case 3UL:
                {
                    var_40 = (var_32 == 0UL);
                    while (1U)
                        {
                            var_41 = local_sp_5 + (-8L);
                            var_42 = (uint64_t *)var_41;
                            rdx_3 = rdx_5;
                            r12_4 = r12_5;
                            local_sp_5 = var_41;
                            if (r14_0) {
                                *var_42 = 4216279UL;
                                indirect_placeholder_1();
                                storemerge = (r15_2 & (-256L)) | var_40;
                            } else {
                                *var_42 = 4216237UL;
                                indirect_placeholder_1();
                                rdx_3 = 3UL;
                                storemerge = (r15_2 & (-256L)) | var_40;
                            }
                            r15_0 = storemerge;
                            rdx_4 = rdx_3;
                            r15_2 = storemerge;
                            rdx_5 = rdx_3;
                            if ((uint64_t)(unsigned char)storemerge != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            var_43 = r12_5 + 16UL;
                            r12_5 = var_43;
                            if (*(uint64_t *)var_43 == 0UL) {
                                continue;
                            }
                            var_44 = var_41 + (-8L);
                            *(uint64_t *)var_44 = 4216299UL;
                            var_45 = indirect_placeholder_2(rdi, rsi);
                            local_sp_3 = var_44;
                            r12_4 = var_45;
                            if (var_45 != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            var_46 = (uint64_t)var_11;
                            r12_4 = 4307312UL;
                            loop_state_var = 0U;
                            break;
                        }
                    cc_op_0 = 14U;
                    cc_op_1 = cc_op_0;
                    cc_src_1 = cc_src_0;
                    cc_dst_1 = cc_dst_0;
                    while (rcx_3 != 0UL)
                        {
                            rdi3_0 = rdi3_0 + var_46;
                            rsi4_0 = rsi4_0 + var_46;
                            cc_op_0 = 14U;
                            cc_op_1 = cc_op_0;
                            cc_src_1 = cc_src_0;
                            cc_dst_1 = cc_dst_0;
                        }
                    var_50 = helper_cc_compute_all_wrapper(cc_dst_1, cc_src_1, var_37, cc_op_1);
                    var_51 = ((var_50 & 65UL) == 0UL);
                    var_52 = helper_cc_compute_c_wrapper(cc_dst_1, var_50, var_37, 1U);
                    var_53 = (uint64_t)((unsigned char)var_51 - (unsigned char)var_52);
                    rcx_5 = rcx_4;
                    if (var_53 != 0UL) {
                        var_54 = ((uint64_t)(uint32_t)var_53 == 0UL);
                        while (1U)
                            {
                                var_55 = local_sp_3 + (-8L);
                                *(uint64_t *)var_55 = 4216390UL;
                                indirect_placeholder_1();
                                local_sp_0 = var_55;
                                local_sp_2 = var_55;
                                local_sp_3 = var_55;
                                r12_4 = r12_3;
                                if (!var_54) {
                                    loop_state_var = 2U;
                                    break;
                                }
                                var_56 = r12_3 + 16UL;
                                r12_3 = var_56;
                                r12_4 = 0UL;
                                rdx_4 = rsi;
                                rcx_5 = rsi;
                                if (*(uint64_t *)var_56 == 0UL) {
                                    continue;
                                }
                                _cast1 = (unsigned char *)(var_38 + (-1L));
                                if (*_cast1 != 'S') {
                                    loop_state_var = 1U;
                                    break;
                                }
                                *_cast1 = (unsigned char)'\x00';
                                loop_state_var = 0U;
                                break;
                            }
                        switch (loop_state_var) {
                          case 1U:
                            {
                                local_sp_1 = local_sp_0;
                                var_59 = local_sp_1 + (-8L);
                                *(uint64_t *)var_59 = 4216470UL;
                                indirect_placeholder_1();
                                var_60 = r12_1 + 16UL;
                                r12_1 = var_60;
                                local_sp_1 = var_59;
                                do {
                                    var_59 = local_sp_1 + (-8L);
                                    *(uint64_t *)var_59 = 4216470UL;
                                    indirect_placeholder_1();
                                    var_60 = r12_1 + 16UL;
                                    r12_1 = var_60;
                                    local_sp_1 = var_59;
                                } while (*(uint64_t *)var_60 != 0UL);
                                var_61 = *var_13;
                                var_62 = (uint64_t)var_61;
                                rax_0 = var_62;
                                if (var_39 != 1UL) {
                                    while (1U)
                                        {
                                            r12_4 = r12_0;
                                            rdx_4 = rdx_0;
                                            rcx_5 = rcx_4;
                                            if ((uint64_t)(var_61 - (unsigned char)rdx_0) != 0UL) {
                                                loop_state_var = 1U;
                                                break;
                                            }
                                            var_63 = r12_0 + 16UL;
                                            var_64 = *(uint64_t *)var_63;
                                            r12_0 = var_63;
                                            if (var_64 != 0UL) {
                                                loop_state_var = 0U;
                                                break;
                                            }
                                            rdx_0 = (uint64_t)*(unsigned char *)var_64;
                                            continue;
                                        }
                                    switch (loop_state_var) {
                                      case 1U:
                                        {
                                            mrv.field_0 = r12_4;
                                            mrv1 = mrv;
                                            mrv1.field_1 = rdx_4;
                                            mrv2 = mrv1;
                                            mrv2.field_2 = rcx_5;
                                            return mrv2;
                                        }
                                        break;
                                      case 0U:
                                        {
                                            break;
                                        }
                                        break;
                                    }
                                }
                                if (var_61 != '\x00') {
                                    var_65 = ((uint64_t)((unsigned char)rax_0 + '\xd2') == 0UL);
                                    var_66 = rcx_0 + 1UL;
                                    spec_select = var_65 ? 1UL : r15_0;
                                    spec_select121 = var_65 ? rcx_0 : var_66;
                                    var_67 = rdx_1 + 1UL;
                                    var_68 = *(unsigned char *)var_67;
                                    *(unsigned char *)spec_select121 = var_68;
                                    r15_0 = spec_select;
                                    rdx_1 = var_67;
                                    rcx_0 = spec_select121;
                                    rdx_4 = var_67;
                                    rcx_5 = spec_select121;
                                    while (var_68 != '\x00')
                                        {
                                            rax_0 = (uint64_t)var_68;
                                            var_65 = ((uint64_t)((unsigned char)rax_0 + '\xd2') == 0UL);
                                            var_66 = rcx_0 + 1UL;
                                            spec_select = var_65 ? 1UL : r15_0;
                                            spec_select121 = var_65 ? rcx_0 : var_66;
                                            var_67 = rdx_1 + 1UL;
                                            var_68 = *(unsigned char *)var_67;
                                            *(unsigned char *)spec_select121 = var_68;
                                            r15_0 = spec_select;
                                            rdx_1 = var_67;
                                            rcx_0 = spec_select121;
                                            rdx_4 = var_67;
                                            rcx_5 = spec_select121;
                                        }
                                    if ((uint64_t)(unsigned char)spec_select == 0UL) {
                                        *(uint64_t *)(local_sp_1 + (-16L)) = 4216607UL;
                                        var_69 = indirect_placeholder_2(rdi, rsi);
                                        r12_4 = var_69;
                                    }
                                }
                            }
                            break;
                          case 0U:
                            {
                                var_57 = local_sp_2 + (-8L);
                                *(uint64_t *)var_57 = 4216582UL;
                                indirect_placeholder_1();
                                var_58 = r12_2 + 16UL;
                                local_sp_0 = var_57;
                                r12_2 = var_58;
                                local_sp_2 = var_57;
                                do {
                                    var_57 = local_sp_2 + (-8L);
                                    *(uint64_t *)var_57 = 4216582UL;
                                    indirect_placeholder_1();
                                    var_58 = r12_2 + 16UL;
                                    local_sp_0 = var_57;
                                    r12_2 = var_58;
                                    local_sp_2 = var_57;
                                } while (*(uint64_t *)var_58 != 0UL);
                                *_cast1 = (unsigned char)'S';
                            }
                            break;
                        }
                    }
                }
                break;
              default:
                {
                    r14_0 = 1;
                }
                break;
            }
        }
        break;
    }
}
