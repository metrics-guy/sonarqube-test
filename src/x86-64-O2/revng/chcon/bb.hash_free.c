typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r12(void);
typedef _Bool bool;
void bb_hash_free(uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t *_cast3;
    uint64_t var_7;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t rax_2;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t rax_0_ph;
    uint64_t rax_1;
    uint64_t var_10;
    uint64_t local_sp_7;
    uint64_t rbp_0_ph;
    uint64_t local_sp_0_ph;
    uint64_t rbp_0;
    uint64_t rbx_0;
    uint64_t local_sp_1;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_15;
    uint64_t local_sp_2;
    uint64_t rbp_1;
    uint64_t local_sp_3;
    uint64_t var_16;
    uint64_t local_sp_6;
    uint64_t rbp_2;
    uint64_t local_sp_4;
    uint64_t var_17;
    uint64_t rbx_1;
    uint64_t local_sp_5;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t local_sp_9;
    uint64_t rbx_2;
    uint64_t local_sp_8;
    uint64_t var_22;
    uint64_t var_23;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r12();
    var_2 = init_rbx();
    var_3 = init_rbp();
    var_4 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    var_5 = var_0 + (-24L);
    *(uint64_t *)var_5 = var_2;
    var_6 = *(uint64_t *)(rdi + 64UL);
    _cast3 = (uint64_t *)rdi;
    var_7 = *_cast3;
    var_8 = (uint64_t *)(rdi + 8UL);
    var_9 = *var_8;
    rax_2 = var_9;
    rax_0_ph = var_9;
    local_sp_7 = var_5;
    rbp_0_ph = var_7;
    local_sp_0_ph = var_5;
    rbp_1 = var_7;
    local_sp_3 = var_5;
    if (var_6 != 0UL & *(uint64_t *)(rdi + 32UL) != 0UL) {
        var_10 = helper_cc_compute_c_wrapper(var_7 - var_9, var_9, var_4, 17U);
        if (var_10 != 0UL) {
            var_21 = *(uint64_t *)(rdi + 72UL);
            rbx_2 = var_21;
            local_sp_8 = local_sp_7;
            local_sp_9 = local_sp_7;
            if (var_21 != 0UL) {
                var_22 = *(uint64_t *)(rbx_2 + 8UL);
                var_23 = local_sp_8 + (-8L);
                *(uint64_t *)var_23 = 4234044UL;
                indirect_placeholder();
                rbx_2 = var_22;
                local_sp_8 = var_23;
                local_sp_9 = var_23;
                do {
                    var_22 = *(uint64_t *)(rbx_2 + 8UL);
                    var_23 = local_sp_8 + (-8L);
                    *(uint64_t *)var_23 = 4234044UL;
                    indirect_placeholder();
                    rbx_2 = var_22;
                    local_sp_8 = var_23;
                    local_sp_9 = var_23;
                } while (var_22 != 0UL);
            }
            *(uint64_t *)(local_sp_9 + (-8L)) = 4234058UL;
            indirect_placeholder();
            indirect_placeholder();
            return;
        }
        while (1U)
            {
                rbp_0 = rbp_0_ph;
                local_sp_1 = local_sp_0_ph;
                rax_1 = rax_0_ph;
                local_sp_2 = local_sp_0_ph;
                while (1U)
                    {
                        rbx_0 = rbp_0;
                        if (*(uint64_t *)rbp_0 != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_15 = rbp_0 + 16UL;
                        rbp_0 = var_15;
                        if (rax_0_ph > var_15) {
                            continue;
                        }
                        loop_state_var = 0U;
                        break;
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 0U:
                    {
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 1U:
                    {
                        var_11 = local_sp_1 + (-8L);
                        *(uint64_t *)var_11 = 4233928UL;
                        indirect_placeholder();
                        var_12 = *(uint64_t *)(rbx_0 + 8UL);
                        local_sp_0_ph = var_11;
                        rbx_0 = var_12;
                        local_sp_1 = var_11;
                        local_sp_2 = var_11;
                        do {
                            var_11 = local_sp_1 + (-8L);
                            *(uint64_t *)var_11 = 4233928UL;
                            indirect_placeholder();
                            var_12 = *(uint64_t *)(rbx_0 + 8UL);
                            local_sp_0_ph = var_11;
                            rbx_0 = var_12;
                            local_sp_1 = var_11;
                            local_sp_2 = var_11;
                        } while (var_12 != 0UL);
                        var_13 = *var_8;
                        var_14 = rbp_0 + 16UL;
                        rax_0_ph = var_13;
                        rbp_0_ph = var_14;
                        rax_1 = var_13;
                        if (var_13 <= var_14) {
                            continue;
                        }
                        switch_state_var = 1;
                        break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
        rax_2 = rax_1;
        rbp_1 = *_cast3;
        local_sp_3 = local_sp_2;
    }
    var_16 = helper_cc_compute_c_wrapper(rbp_1 - rax_2, rax_2, var_4, 17U);
    rbp_2 = rbp_1;
    local_sp_4 = local_sp_3;
    local_sp_7 = local_sp_3;
    if (var_16 == 0UL) {
        var_20 = rbp_2 + 16UL;
        rbp_2 = var_20;
        local_sp_4 = local_sp_6;
        local_sp_7 = local_sp_6;
        do {
            var_17 = *(uint64_t *)(rbp_2 + 8UL);
            rbx_1 = var_17;
            local_sp_5 = local_sp_4;
            local_sp_6 = local_sp_4;
            if (var_17 == 0UL) {
                var_18 = *(uint64_t *)(rbx_1 + 8UL);
                var_19 = local_sp_5 + (-8L);
                *(uint64_t *)var_19 = 4233996UL;
                indirect_placeholder();
                rbx_1 = var_18;
                local_sp_5 = var_19;
                local_sp_6 = var_19;
                do {
                    var_18 = *(uint64_t *)(rbx_1 + 8UL);
                    var_19 = local_sp_5 + (-8L);
                    *(uint64_t *)var_19 = 4233996UL;
                    indirect_placeholder();
                    rbx_1 = var_18;
                    local_sp_5 = var_19;
                    local_sp_6 = var_19;
                } while (var_18 != 0UL);
            }
            var_20 = rbp_2 + 16UL;
            rbp_2 = var_20;
            local_sp_4 = local_sp_6;
            local_sp_7 = local_sp_6;
        } while (*var_8 <= var_20);
    }
    var_21 = *(uint64_t *)(rdi + 72UL);
    rbx_2 = var_21;
    local_sp_8 = local_sp_7;
    local_sp_9 = local_sp_7;
    if (var_21 == 0UL) {
        *(uint64_t *)(local_sp_9 + (-8L)) = 4234058UL;
        indirect_placeholder();
        indirect_placeholder();
        return;
    }
    var_22 = *(uint64_t *)(rbx_2 + 8UL);
    var_23 = local_sp_8 + (-8L);
    *(uint64_t *)var_23 = 4234044UL;
    indirect_placeholder();
    rbx_2 = var_22;
    local_sp_8 = var_23;
    local_sp_9 = var_23;
    do {
        var_22 = *(uint64_t *)(rbx_2 + 8UL);
        var_23 = local_sp_8 + (-8L);
        *(uint64_t *)var_23 = 4234044UL;
        indirect_placeholder();
        rbx_2 = var_22;
        local_sp_8 = var_23;
        local_sp_9 = var_23;
    } while (var_22 != 0UL);
    *(uint64_t *)(local_sp_9 + (-8L)) = 4234058UL;
    indirect_placeholder();
    indirect_placeholder();
    return;
}
