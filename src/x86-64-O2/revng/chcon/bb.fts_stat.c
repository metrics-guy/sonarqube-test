typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_fts_stat_ret_type;
struct bb_fts_stat_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r12(void);
extern uint64_t init_rcx(void);
extern uint64_t indirect_placeholder_25(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint32_t init_state_0x82fc(void);
struct bb_fts_stat_ret_type bb_fts_stat(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint32_t var_6;
    uint64_t var_7;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t rax_1;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t rdi5_0;
    uint64_t rcx_0;
    struct bb_fts_stat_ret_type var_25 = {10UL, 0UL};
    uint64_t rax_0;
    uint64_t local_sp_0;
    uint32_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t rcx_1;
    uint32_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t storemerge;
    uint64_t var_29;
    unsigned char var_30;
    uint64_t rcx_2;
    uint64_t var_31;
    uint64_t var_32;
    struct bb_fts_stat_ret_type mrv2;
    struct bb_fts_stat_ret_type mrv3;
    uint64_t var_15;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_14;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r12();
    var_2 = init_rbx();
    var_3 = init_rbp();
    var_4 = init_rcx();
    var_5 = init_cc_src2();
    var_6 = init_state_0x82fc();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    var_7 = rsi + 120UL;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    var_8 = (uint64_t *)(rsi + 88UL);
    var_9 = *var_8;
    var_10 = rdi + 72UL;
    var_11 = (uint64_t)*(uint32_t *)var_10;
    var_12 = (uint64_t *)(rsi + 48UL);
    var_13 = *var_12;
    rcx_1 = 256UL;
    storemerge = 18446744073709551615UL;
    rax_1 = 13UL;
    rcx_2 = var_4;
    if ((((var_9 != 0UL) || ((var_11 & 1UL) == 0UL)) && ((var_11 & 2UL) == 0UL)) && ((uint64_t)(unsigned char)rdx == 0UL)) {
        var_19 = (uint64_t)*(uint32_t *)(rdi + 44UL);
        var_20 = var_0 + (-32L);
        *(uint64_t *)var_20 = 4217704UL;
        var_21 = indirect_placeholder_25(var_7, var_19, 256UL, var_13);
        rax_0 = var_21;
        local_sp_0 = var_20;
        if ((uint64_t)(uint32_t)var_21 != 0UL) {
            *(uint64_t *)(local_sp_0 + (-8L)) = 4217713UL;
            indirect_placeholder();
            var_22 = *(uint32_t *)rax_0;
            var_23 = (rsi + 128UL) & (-8L);
            *(uint64_t *)var_7 = 0UL;
            *(uint32_t *)(rsi + 64UL) = var_22;
            *(uint64_t *)(rsi + 256UL) = 0UL;
            var_24 = (uint64_t)var_6 << 3UL;
            rdi5_0 = var_23;
            rcx_0 = (uint64_t)((uint32_t)((uint64_t)(((uint32_t)var_7 - (uint32_t)var_23) + 144U) >> 3UL) & 536870911U);
            while (rcx_0 != 0UL)
                {
                    *(uint64_t *)rdi5_0 = 0UL;
                    rdi5_0 = var_24 + rdi5_0;
                    rcx_0 = rcx_0 + (-1L);
                }
            return var_25;
        }
    }
    *(uint64_t *)(var_0 + (-32L)) = 4217608UL;
    var_14 = indirect_placeholder_3(var_13, var_7);
    rax_0 = var_14;
    rcx_1 = var_4;
    if ((uint64_t)(uint32_t)var_14 != 0UL) {
        var_15 = var_0 + (-40L);
        *(uint64_t *)var_15 = 4217901UL;
        indirect_placeholder();
        local_sp_0 = var_15;
        if (*(uint32_t *)var_14 != 2U) {
            var_16 = *var_12;
            var_17 = var_0 + (-48L);
            *(uint64_t *)var_17 = 4217922UL;
            var_18 = indirect_placeholder_3(var_16, var_7);
            rax_0 = var_18;
            local_sp_0 = var_17;
            if ((uint64_t)(uint32_t)var_18 != 0UL) {
                *(uint64_t *)(var_0 + (-56L)) = 4217935UL;
                indirect_placeholder();
                *(uint32_t *)var_18 = 0U;
                mrv2.field_0 = rax_1;
                mrv3 = mrv2;
                mrv3.field_1 = rcx_2;
                return mrv3;
            }
        }
        *(uint64_t *)(local_sp_0 + (-8L)) = 4217713UL;
        indirect_placeholder();
        var_22 = *(uint32_t *)rax_0;
        var_23 = (rsi + 128UL) & (-8L);
        *(uint64_t *)var_7 = 0UL;
        *(uint32_t *)(rsi + 64UL) = var_22;
        *(uint64_t *)(rsi + 256UL) = 0UL;
        var_24 = (uint64_t)var_6 << 3UL;
        rdi5_0 = var_23;
        rcx_0 = (uint64_t)((uint32_t)((uint64_t)(((uint32_t)var_7 - (uint32_t)var_23) + 144U) >> 3UL) & 536870911U);
        while (rcx_0 != 0UL)
            {
                *(uint64_t *)rdi5_0 = 0UL;
                rdi5_0 = var_24 + rdi5_0;
                rcx_0 = rcx_0 + (-1L);
            }
        return var_25;
    }
    var_26 = (uint32_t)((uint16_t)*(uint32_t *)(rsi + 144UL) & (unsigned short)61440U);
    rax_1 = 1UL;
    rcx_2 = rcx_1;
    if ((uint64_t)((var_26 + (-16384)) & (-4096)) != 0UL) {
        rax_1 = 12UL;
        if ((uint64_t)((var_26 + (-40960)) & (-4096)) == 0UL) {
            var_27 = ((uint64_t)((var_26 + (-32768)) & (-4096)) == 0UL) ? 8UL : 3UL;
            rax_1 = var_27;
        }
        mrv2.field_0 = rax_1;
        mrv3 = mrv2;
        mrv3.field_1 = rcx_2;
        return mrv3;
    }
    var_28 = *(uint64_t *)(rsi + 136UL);
    if (var_28 <= 1UL & (long)*var_8 > (long)0UL) {
        storemerge = ((*(unsigned char *)var_10 & ' ') == '\x00') ? (var_28 + (-2L)) : var_28;
    }
    var_29 = rsi + 264UL;
    var_30 = *(unsigned char *)var_29;
    *(uint64_t *)(rsi + 104UL) = storemerge;
    if (var_30 == '.') {
        return;
    }
    if (*(unsigned char *)(rsi + 265UL) != '\x00') {
        if ((*(uint32_t *)var_29 & 16776960U) != 11776U) {
            mrv2.field_0 = rax_1;
            mrv3 = mrv2;
            mrv3.field_1 = rcx_2;
            return mrv3;
        }
    }
    var_31 = helper_cc_compute_c_wrapper(*var_8 + (-1L), 1UL, var_5, 17U);
    var_32 = (uint64_t)((((0U - (uint32_t)var_31) & (-4)) + 5U) & (-3));
    rax_1 = var_32;
}
