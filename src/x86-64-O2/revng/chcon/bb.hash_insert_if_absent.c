typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct helper_pxor_xmm_wrapper_158_ret_type;
struct type_3;
struct type_5;
struct helper_pxor_xmm_wrapper_ret_type;
struct helper_mulss_wrapper_175_ret_type;
struct helper_mulss_wrapper_179_ret_type;
struct helper_comiss_wrapper_180_ret_type;
struct helper_cvtsq2ss_wrapper_ret_type;
struct helper_addss_wrapper_ret_type;
struct helper_comiss_wrapper_176_ret_type;
struct helper_cvttss2sq_wrapper_ret_type;
struct helper_subss_wrapper_ret_type;
struct helper_comiss_wrapper_178_ret_type;
struct helper_cvtsq2ss_wrapper_173_ret_type;
struct helper_addss_wrapper_174_ret_type;
struct helper_mulss_wrapper_181_ret_type;
struct helper_pxor_xmm_wrapper_158_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_3 {
};
struct type_5 {
};
struct helper_pxor_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_mulss_wrapper_175_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_mulss_wrapper_179_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_comiss_wrapper_180_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvtsq2ss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_addss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_comiss_wrapper_176_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvttss2sq_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_subss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_comiss_wrapper_178_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvtsq2ss_wrapper_173_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_addss_wrapper_174_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_mulss_wrapper_181_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern struct helper_pxor_xmm_wrapper_158_ret_type helper_pxor_xmm_wrapper_158(struct type_3 *param_0, struct type_5 *param_1, struct type_5 *param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_state_0x8558(void);
extern unsigned char init_state_0x8549(void);
extern unsigned char init_state_0x854c(void);
extern unsigned char init_state_0x8548(void);
extern unsigned char init_state_0x854b(void);
extern unsigned char init_state_0x8547(void);
extern unsigned char init_state_0x854d(void);
extern uint64_t init_state_0x8560(void);
extern uint64_t init_state_0x8598(void);
extern uint64_t init_state_0x85a0(void);
extern struct helper_pxor_xmm_wrapper_ret_type helper_pxor_xmm_wrapper(struct type_3 *param_0, struct type_5 *param_1, struct type_5 *param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_state_0x8d58(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_44(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct helper_mulss_wrapper_175_ret_type helper_mulss_wrapper_175(struct type_3 *param_0, struct type_5 *param_1, struct type_5 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern uint64_t init_r9(void);
extern struct helper_mulss_wrapper_179_ret_type helper_mulss_wrapper_179(struct type_3 *param_0, struct type_5 *param_1, struct type_5 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_comiss_wrapper_180_ret_type helper_comiss_wrapper_180(struct type_3 *param_0, struct type_5 *param_1, struct type_5 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_cvtsq2ss_wrapper_ret_type helper_cvtsq2ss_wrapper(struct type_3 *param_0, struct type_5 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_addss_wrapper_ret_type helper_addss_wrapper(struct type_3 *param_0, struct type_5 *param_1, struct type_5 *param_2, uint64_t param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9);
extern struct helper_comiss_wrapper_176_ret_type helper_comiss_wrapper_176(struct type_3 *param_0, struct type_5 *param_1, struct type_5 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_cvttss2sq_wrapper_ret_type helper_cvttss2sq_wrapper(struct type_3 *param_0, struct type_5 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4);
extern struct helper_subss_wrapper_ret_type helper_subss_wrapper(struct type_3 *param_0, struct type_5 *param_1, struct type_5 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern uint64_t indirect_placeholder_25(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint64_t indirect_placeholder_8(uint64_t param_0);
extern uint64_t init_r8(void);
extern struct helper_comiss_wrapper_178_ret_type helper_comiss_wrapper_178(struct type_3 *param_0, struct type_5 *param_1, struct type_5 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern uint64_t init_r10(void);
extern struct helper_cvtsq2ss_wrapper_173_ret_type helper_cvtsq2ss_wrapper_173(struct type_3 *param_0, struct type_5 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_addss_wrapper_174_ret_type helper_addss_wrapper_174(struct type_3 *param_0, struct type_5 *param_1, struct type_5 *param_2, uint64_t param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9);
extern struct helper_mulss_wrapper_181_ret_type helper_mulss_wrapper_181(struct type_3 *param_0, struct type_5 *param_1, struct type_5 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
uint64_t bb_hash_insert_if_absent(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    unsigned char storemerge;
    struct helper_mulss_wrapper_179_ret_type var_69;
    struct helper_comiss_wrapper_180_ret_type var_70;
    struct helper_cvtsq2ss_wrapper_ret_type var_61;
    uint64_t *var_95;
    struct helper_pxor_xmm_wrapper_ret_type var_29;
    struct helper_pxor_xmm_wrapper_ret_type var_25;
    struct helper_cvttss2sq_wrapper_ret_type var_85;
    struct helper_cvttss2sq_wrapper_ret_type var_83;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    unsigned char var_13;
    unsigned char var_14;
    unsigned char var_15;
    unsigned char var_16;
    unsigned char var_17;
    unsigned char var_18;
    uint64_t var_19;
    uint64_t local_sp_1;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t local_sp_0;
    unsigned char storemerge2;
    struct helper_cvtsq2ss_wrapper_ret_type var_58;
    uint64_t state_0x8558_2;
    uint64_t var_59;
    uint64_t var_60;
    struct helper_addss_wrapper_ret_type var_62;
    uint64_t rcx_0;
    uint64_t state_0x8558_0;
    unsigned char storemerge1;
    uint64_t var_63;
    uint64_t state_0x85a0_0;
    unsigned char storemerge3;
    struct helper_cvtsq2ss_wrapper_173_ret_type var_64;
    uint64_t rcx_1;
    uint64_t var_65;
    uint64_t var_66;
    struct helper_cvtsq2ss_wrapper_173_ret_type var_67;
    struct helper_addss_wrapper_174_ret_type var_68;
    uint64_t state_0x8598_0;
    struct helper_mulss_wrapper_175_ret_type var_71;
    uint64_t var_72;
    unsigned char var_73;
    unsigned char var_74;
    uint64_t var_75;
    uint64_t state_0x8558_1;
    struct helper_mulss_wrapper_181_ret_type var_76;
    unsigned char state_0x8549_0;
    struct helper_comiss_wrapper_176_ret_type var_77;
    uint64_t var_78;
    struct helper_comiss_wrapper_176_ret_type var_79;
    uint64_t var_80;
    unsigned char var_81;
    uint64_t var_82;
    uint64_t rsi3_0;
    struct helper_subss_wrapper_ret_type var_84;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_92;
    uint64_t *var_93;
    uint64_t *var_94;
    uint64_t merge;
    uint64_t *var_23;
    uint64_t var_24;
    uint64_t var_26;
    struct helper_cvtsq2ss_wrapper_173_ret_type var_27;
    uint64_t state_0x8598_1;
    uint64_t var_28;
    uint64_t var_30;
    struct helper_cvtsq2ss_wrapper_173_ret_type var_31;
    struct helper_addss_wrapper_174_ret_type var_32;
    uint64_t *var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t *var_36;
    uint64_t var_37;
    struct helper_pxor_xmm_wrapper_158_ret_type var_38;
    uint64_t var_39;
    struct helper_cvtsq2ss_wrapper_ret_type var_40;
    uint64_t rcx_2;
    uint64_t var_41;
    struct helper_pxor_xmm_wrapper_158_ret_type var_42;
    uint64_t var_43;
    uint64_t var_44;
    struct helper_cvtsq2ss_wrapper_ret_type var_45;
    struct helper_addss_wrapper_ret_type var_46;
    uint64_t state_0x8560_0;
    uint64_t var_47;
    uint64_t var_48;
    struct helper_mulss_wrapper_175_ret_type var_49;
    uint64_t var_50;
    struct helper_comiss_wrapper_178_ret_type var_51;
    uint64_t var_52;
    unsigned char var_53;
    uint64_t *_cast;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_state_0x8d58();
    var_2 = init_r12();
    var_3 = init_rbx();
    var_4 = init_r8();
    var_5 = init_rbp();
    var_6 = init_r10();
    var_7 = init_cc_src2();
    var_8 = init_state_0x8558();
    var_9 = init_state_0x8560();
    var_10 = init_r9();
    var_11 = init_state_0x8598();
    var_12 = init_state_0x85a0();
    var_13 = init_state_0x8549();
    var_14 = init_state_0x854c();
    var_15 = init_state_0x8548();
    var_16 = init_state_0x854b();
    var_17 = init_state_0x8547();
    var_18 = init_state_0x854d();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    var_19 = var_0 + (-40L);
    merge = 4294967295UL;
    rcx_2 = 0UL;
    local_sp_1 = var_19;
    if (rsi == 0UL) {
        *(uint64_t *)(local_sp_1 + (-8L)) = 4203932UL;
        indirect_placeholder();
        abort();
    }
    var_20 = var_0 + (-32L);
    var_21 = var_0 + (-48L);
    *(uint64_t *)var_21 = 4234502UL;
    var_22 = indirect_placeholder_25(var_20, rdi, 0UL, rsi);
    local_sp_0 = var_21;
    if (var_22 != 0UL) {
        merge = 0UL;
        if (rdx == 0UL) {
            *(uint64_t *)rdx = var_22;
        }
        return merge;
    }
    var_23 = (uint64_t *)(rdi + 24UL);
    var_24 = *var_23;
    if ((long)var_24 < (long)0UL) {
        var_28 = var_24 & 1UL;
        var_29 = helper_pxor_xmm_wrapper((struct type_3 *)(0UL), (struct type_5 *)(840UL), (struct type_5 *)(840UL), var_11, var_12);
        var_30 = var_29.field_1;
        var_31 = helper_cvtsq2ss_wrapper_173((struct type_3 *)(0UL), (struct type_5 *)(840UL), (var_24 >> 1UL) | var_28, var_13, var_15, var_16, var_17);
        var_32 = helper_addss_wrapper_174((struct type_3 *)(0UL), (struct type_5 *)(840UL), (struct type_5 *)(840UL), var_31.field_0, var_31.field_1, var_14, var_15, var_16, var_17, var_18);
        state_0x8598_1 = var_32.field_0;
        state_0x85a0_0 = var_30;
        storemerge3 = var_32.field_1;
    } else {
        var_25 = helper_pxor_xmm_wrapper((struct type_3 *)(0UL), (struct type_5 *)(840UL), (struct type_5 *)(840UL), var_11, var_12);
        var_26 = var_25.field_1;
        var_27 = helper_cvtsq2ss_wrapper_173((struct type_3 *)(0UL), (struct type_5 *)(840UL), var_24, var_13, var_15, var_16, var_17);
        state_0x8598_1 = var_27.field_0;
        state_0x85a0_0 = var_26;
        storemerge3 = var_27.field_1;
    }
    var_33 = (uint64_t *)(rdi + 16UL);
    var_34 = *var_33;
    var_35 = rdi + 40UL;
    var_36 = (uint64_t *)var_35;
    var_37 = *var_36;
    if ((long)var_34 < (long)0UL) {
        var_41 = var_34 & 1UL;
        var_42 = helper_pxor_xmm_wrapper_158((struct type_3 *)(0UL), (struct type_5 *)(776UL), (struct type_5 *)(776UL), var_8, var_9);
        var_43 = var_42.field_1;
        var_44 = (var_34 >> 1UL) | var_41;
        var_45 = helper_cvtsq2ss_wrapper((struct type_3 *)(0UL), (struct type_5 *)(776UL), var_44, storemerge3, var_15, var_16, var_17);
        var_46 = helper_addss_wrapper((struct type_3 *)(0UL), (struct type_5 *)(776UL), (struct type_5 *)(776UL), var_45.field_0, var_45.field_1, var_14, var_15, var_16, var_17, var_18);
        rcx_2 = var_44;
        state_0x8558_2 = var_46.field_0;
        state_0x8560_0 = var_43;
        storemerge2 = var_46.field_1;
    } else {
        var_38 = helper_pxor_xmm_wrapper_158((struct type_3 *)(0UL), (struct type_5 *)(776UL), (struct type_5 *)(776UL), var_8, var_9);
        var_39 = var_38.field_1;
        var_40 = helper_cvtsq2ss_wrapper((struct type_3 *)(0UL), (struct type_5 *)(776UL), var_34, storemerge3, var_15, var_16, var_17);
        state_0x8558_2 = var_40.field_0;
        state_0x8560_0 = var_39;
        storemerge2 = var_40.field_1;
    }
    var_47 = (uint64_t)*(uint32_t *)(var_37 + 8UL);
    var_48 = var_1 & (-4294967296L);
    var_49 = helper_mulss_wrapper_175((struct type_3 *)(0UL), (struct type_5 *)(776UL), (struct type_5 *)(2824UL), var_48 | var_47, state_0x8558_2, storemerge2, var_14, var_15, var_16, var_17, var_18);
    var_50 = var_49.field_0;
    var_51 = helper_comiss_wrapper_178((struct type_3 *)(0UL), (struct type_5 *)(840UL), (struct type_5 *)(776UL), var_50, state_0x8598_1, var_49.field_1, var_14);
    var_52 = var_51.field_0;
    var_53 = var_51.field_1;
    rcx_0 = rcx_2;
    if ((var_52 & 65UL) == 0UL) {
        _cast = (uint64_t *)*(uint64_t *)(local_sp_0 + 8UL);
        if (*_cast == 0UL) {
            *_cast = rsi;
            var_95 = (uint64_t *)(rdi + 32UL);
            *var_95 = (*var_95 + 1UL);
            *var_23 = (*var_23 + 1UL);
            return 1UL;
        }
        var_90 = rdi + 72UL;
        *(uint64_t *)(local_sp_0 + (-8L)) = 4234618UL;
        var_91 = indirect_placeholder_8(var_90);
        if (var_91 != 0UL) {
            var_92 = *(uint64_t *)local_sp_0;
            *(uint64_t *)var_91 = rsi;
            var_93 = (uint64_t *)(var_92 + 8UL);
            *(uint64_t *)(var_91 + 8UL) = *var_93;
            *var_93 = var_91;
            var_94 = (uint64_t *)(rdi + 32UL);
            *var_94 = (*var_94 + 1UL);
            return 1UL;
        }
    }
    var_54 = var_0 + (-56L);
    *(uint64_t *)var_54 = 4234681UL;
    indirect_placeholder_8(var_35);
    var_55 = *var_36;
    var_56 = *var_33;
    var_57 = (uint64_t)*(uint32_t *)(var_55 + 8UL);
    local_sp_0 = var_54;
    if ((long)var_56 < (long)0UL) {
        var_59 = var_56 & 1UL;
        helper_pxor_xmm_wrapper_158((struct type_3 *)(0UL), (struct type_5 *)(776UL), (struct type_5 *)(776UL), var_50, state_0x8560_0);
        var_60 = (var_56 >> 1UL) | var_59;
        var_61 = helper_cvtsq2ss_wrapper((struct type_3 *)(0UL), (struct type_5 *)(776UL), var_60, var_53, var_15, var_16, var_17);
        var_62 = helper_addss_wrapper((struct type_3 *)(0UL), (struct type_5 *)(776UL), (struct type_5 *)(776UL), var_61.field_0, var_61.field_1, var_14, var_15, var_16, var_17, var_18);
        rcx_0 = var_60;
        state_0x8558_0 = var_62.field_0;
        storemerge1 = var_62.field_1;
    } else {
        helper_pxor_xmm_wrapper_158((struct type_3 *)(0UL), (struct type_5 *)(776UL), (struct type_5 *)(776UL), var_50, state_0x8560_0);
        var_58 = helper_cvtsq2ss_wrapper((struct type_3 *)(0UL), (struct type_5 *)(776UL), var_56, var_53, var_15, var_16, var_17);
        state_0x8558_0 = var_58.field_0;
        storemerge1 = var_58.field_1;
    }
    var_63 = *var_23;
    rcx_1 = rcx_0;
    if ((long)var_63 < (long)0UL) {
        var_65 = var_63 & 1UL;
        helper_pxor_xmm_wrapper((struct type_3 *)(0UL), (struct type_5 *)(840UL), (struct type_5 *)(840UL), state_0x8598_1, state_0x85a0_0);
        var_66 = (var_63 >> 1UL) | var_65;
        var_67 = helper_cvtsq2ss_wrapper_173((struct type_3 *)(0UL), (struct type_5 *)(840UL), var_66, storemerge1, var_15, var_16, var_17);
        var_68 = helper_addss_wrapper_174((struct type_3 *)(0UL), (struct type_5 *)(840UL), (struct type_5 *)(840UL), var_67.field_0, var_67.field_1, var_14, var_15, var_16, var_17, var_18);
        rcx_1 = var_66;
        state_0x8598_0 = var_68.field_0;
        storemerge = var_68.field_1;
    } else {
        helper_pxor_xmm_wrapper((struct type_3 *)(0UL), (struct type_5 *)(840UL), (struct type_5 *)(840UL), state_0x8598_1, state_0x85a0_0);
        var_64 = helper_cvtsq2ss_wrapper_173((struct type_3 *)(0UL), (struct type_5 *)(840UL), var_63, storemerge1, var_15, var_16, var_17);
        state_0x8598_0 = var_64.field_0;
        storemerge = var_64.field_1;
    }
    var_69 = helper_mulss_wrapper_179((struct type_3 *)(0UL), (struct type_5 *)(968UL), (struct type_5 *)(776UL), state_0x8558_0, var_57, storemerge, var_14, var_15, var_16, var_17, var_18);
    var_70 = helper_comiss_wrapper_180((struct type_3 *)(0UL), (struct type_5 *)(840UL), (struct type_5 *)(968UL), state_0x8598_0, var_69.field_0, var_69.field_1, var_14);
    if ((var_70.field_0 & 65UL) == 0UL) {
        return;
    }
    var_71 = helper_mulss_wrapper_175((struct type_3 *)(0UL), (struct type_5 *)(776UL), (struct type_5 *)(2824UL), var_48 | (uint64_t)*(uint32_t *)(var_55 + 12UL), state_0x8558_0, var_70.field_1, var_14, var_15, var_16, var_17, var_18);
    var_72 = var_71.field_0;
    var_73 = var_71.field_1;
    var_74 = *(unsigned char *)(var_55 + 16UL);
    var_75 = (uint64_t)var_74;
    state_0x8558_1 = var_72;
    state_0x8549_0 = var_73;
    if (var_74 == '\x00') {
        var_76 = helper_mulss_wrapper_181((struct type_3 *)(0UL), (struct type_5 *)(776UL), (struct type_5 *)(904UL), var_72, var_57, var_73, var_14, var_15, var_16, var_17, var_18);
        state_0x8558_1 = var_76.field_0;
        state_0x8549_0 = var_76.field_1;
    }
    var_77 = helper_comiss_wrapper_176((struct type_3 *)(0UL), (struct type_5 *)(776UL), (struct type_5 *)(2824UL), var_48 | (uint64_t)*(uint32_t *)4281704UL, state_0x8558_1, state_0x8549_0, var_14);
    var_78 = helper_cc_compute_c_wrapper(var_75, var_77.field_0, var_7, 1U);
    if (var_78 == 0UL) {
        return merge;
    }
    var_79 = helper_comiss_wrapper_176((struct type_3 *)(0UL), (struct type_5 *)(776UL), (struct type_5 *)(2824UL), var_48 | (uint64_t)*(uint32_t *)4281708UL, state_0x8558_1, var_77.field_1, var_14);
    var_80 = var_79.field_0;
    var_81 = var_79.field_1;
    var_82 = helper_cc_compute_c_wrapper(var_75, var_80, var_7, 1U);
    if (var_82 == 0UL) {
        var_84 = helper_subss_wrapper((struct type_3 *)(0UL), (struct type_5 *)(776UL), (struct type_5 *)(2824UL), var_48 | (uint64_t)*(uint32_t *)4281708UL, state_0x8558_1, var_81, var_14, var_15, var_16, var_17, var_18);
        var_85 = helper_cvttss2sq_wrapper((struct type_3 *)(0UL), (struct type_5 *)(776UL), var_84.field_0, var_84.field_1, var_14);
        var_86 = var_85.field_0 ^ (-9223372036854775808L);
        helper_cc_compute_all_wrapper(var_75, var_80, var_7, 1U);
        rsi3_0 = var_86;
    } else {
        var_83 = helper_cvttss2sq_wrapper((struct type_3 *)(0UL), (struct type_5 *)(776UL), state_0x8558_1, var_81, var_14);
        rsi3_0 = var_83.field_0;
    }
    *(uint64_t *)(var_0 + (-64L)) = 4234982UL;
    var_87 = indirect_placeholder_44(var_4, rdi, rcx_1, var_6, rsi3_0, var_10);
    if ((uint64_t)(unsigned char)var_87 == 0UL) {
        return merge;
    }
    var_88 = var_0 + (-72L);
    *(uint64_t *)var_88 = 4235008UL;
    var_89 = indirect_placeholder_25(var_54, rdi, 0UL, rsi);
    local_sp_0 = var_88;
    local_sp_1 = var_88;
    if (var_89 == 0UL) {
        *(uint64_t *)(local_sp_1 + (-8L)) = 4203932UL;
        indirect_placeholder();
        abort();
    }
}
