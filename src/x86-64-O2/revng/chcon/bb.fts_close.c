typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_2(uint64_t param_0);
extern void indirect_placeholder_4(uint64_t param_0, uint64_t param_1);
uint64_t bb_fts_close(uint64_t rdi) {
    uint32_t *_pre72;
    uint64_t var_36;
    uint64_t var_41;
    uint64_t var_39;
    uint32_t var_40;
    uint64_t var_42;
    uint64_t local_sp_5;
    uint64_t var_29;
    uint32_t *_pre74;
    uint64_t var_19;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t local_sp_10;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint32_t _pre;
    uint64_t local_sp_6;
    uint64_t var_24;
    uint64_t var_25;
    uint32_t *_pre_phi75;
    uint64_t r13_4;
    uint64_t local_sp_1;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t local_sp_3;
    uint32_t *var_20;
    uint32_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t r13_1;
    uint64_t local_sp_4;
    uint32_t *_pre_phi73;
    uint32_t _pre_phi;
    uint64_t local_sp_2;
    uint64_t rdi1_0;
    uint64_t r13_3;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t local_sp_7;
    uint64_t rbp_0;
    uint64_t local_sp_9;
    uint64_t local_sp_8;
    uint64_t rbp_1;
    uint64_t var_11;
    uint64_t rdi1_1;
    uint64_t var_7;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_8;
    uint64_t rbp_2;
    uint64_t var_12;
    uint64_t local_sp_11;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint32_t *var_16;
    uint32_t var_17;
    uint64_t var_18;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r12();
    var_2 = init_rbx();
    var_3 = init_r13();
    var_4 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    var_5 = var_0 + (-24L);
    *(uint64_t *)var_5 = var_4;
    var_6 = *(uint64_t *)rdi;
    local_sp_10 = var_5;
    r13_4 = 0UL;
    r13_1 = 4294967295UL;
    local_sp_9 = var_5;
    local_sp_8 = var_5;
    rbp_1 = var_6;
    rdi1_1 = var_6;
    rbp_2 = var_4;
    if (var_6 != 0UL) {
        if ((long)*(uint64_t *)(var_6 + 88UL) <= (long)18446744073709551615UL) {
            while (1U)
                {
                    var_7 = *(uint64_t *)(rdi1_1 + 16UL);
                    rbp_0 = var_7;
                    rbp_1 = var_7;
                    if (var_7 == 0UL) {
                        var_9 = *(uint64_t *)(rdi1_1 + 8UL);
                        var_10 = local_sp_9 + (-8L);
                        *(uint64_t *)var_10 = 4223985UL;
                        indirect_placeholder();
                        local_sp_7 = var_10;
                        rbp_0 = var_9;
                        local_sp_8 = var_10;
                        rbp_1 = var_9;
                        if ((long)*(uint64_t *)(var_9 + 88UL) > (long)18446744073709551615UL) {
                            break;
                        }
                    }
                    var_8 = local_sp_9 + (-8L);
                    *(uint64_t *)var_8 = 4223957UL;
                    indirect_placeholder();
                    local_sp_7 = var_8;
                    local_sp_8 = var_8;
                    if ((long)*(uint64_t *)(var_7 + 88UL) < (long)0UL) {
                        break;
                    }
                    local_sp_9 = local_sp_7;
                    rdi1_1 = rbp_0;
                    continue;
                }
        }
        var_11 = local_sp_8 + (-8L);
        *(uint64_t *)var_11 = 4224000UL;
        indirect_placeholder();
        local_sp_10 = var_11;
        rbp_2 = rbp_1;
    }
    var_12 = *(uint64_t *)(rdi + 8UL);
    local_sp_11 = local_sp_10;
    if (var_12 == 0UL) {
        var_13 = local_sp_10 + (-8L);
        *(uint64_t *)var_13 = 4224015UL;
        indirect_placeholder_1(var_2, rbp_2, var_12);
        local_sp_11 = var_13;
    }
    var_14 = rdi + 96UL;
    *(uint64_t *)(local_sp_11 + (-8L)) = 4224030UL;
    indirect_placeholder();
    var_15 = local_sp_11 + (-16L);
    *(uint64_t *)var_15 = 4224040UL;
    indirect_placeholder();
    var_16 = (uint32_t *)(rdi + 72UL);
    var_17 = *var_16;
    var_18 = (uint64_t)var_17;
    local_sp_5 = var_15;
    local_sp_6 = var_15;
    if ((uint64_t)((uint16_t)var_18 & (unsigned short)512U) != 0UL) {
        if ((int)*(uint32_t *)(rdi + 44UL) <= (int)4294967295U) {
            var_34 = local_sp_6 + (-8L);
            *(uint64_t *)var_34 = 4224070UL;
            indirect_placeholder_2(var_14);
            var_35 = *(uint64_t *)(rdi + 80UL);
            local_sp_4 = var_34;
            rdi1_0 = var_35;
            if (var_35 == 0UL) {
                var_41 = *(uint64_t *)(rdi + 88UL);
                var_42 = (uint64_t)*var_16;
                *(uint64_t *)(local_sp_6 + (-16L)) = 4224207UL;
                indirect_placeholder_4(var_42, var_41);
                *(uint64_t *)(local_sp_6 + (-24L)) = 4224215UL;
                indirect_placeholder();
                return 0UL;
            }
            var_36 = local_sp_4 + (-8L);
            *(uint64_t *)var_36 = 4224085UL;
            indirect_placeholder_2(rdi1_0);
            local_sp_3 = var_36;
            r13_3 = r13_4;
            var_37 = (uint64_t)*var_16;
            var_38 = *(uint64_t *)(rdi + 88UL);
            *(uint64_t *)(local_sp_3 + (-8L)) = 4224100UL;
            indirect_placeholder_4(var_37, var_38);
            var_39 = local_sp_3 + (-16L);
            *(uint64_t *)var_39 = 4224108UL;
            indirect_placeholder();
            var_40 = (uint32_t)r13_3;
            r13_1 = r13_3;
            _pre_phi = var_40;
            local_sp_2 = var_39;
            if ((uint64_t)var_40 == 0UL) {
                return (uint64_t)(uint32_t)r13_1;
            }
            _pre72 = (uint32_t *)var_18;
            _pre_phi73 = _pre72;
            *(uint64_t *)(local_sp_2 + (-8L)) = 4224309UL;
            indirect_placeholder();
            *_pre_phi73 = _pre_phi;
            return (uint64_t)(uint32_t)r13_1;
        }
        var_29 = local_sp_5 + (-8L);
        *(uint64_t *)var_29 = 4224156UL;
        indirect_placeholder();
        local_sp_1 = var_29;
        local_sp_6 = var_29;
        if (var_17 == 0U) {
            var_34 = local_sp_6 + (-8L);
            *(uint64_t *)var_34 = 4224070UL;
            indirect_placeholder_2(var_14);
            var_35 = *(uint64_t *)(rdi + 80UL);
            local_sp_4 = var_34;
            rdi1_0 = var_35;
            if (var_35 == 0UL) {
                var_41 = *(uint64_t *)(rdi + 88UL);
                var_42 = (uint64_t)*var_16;
                *(uint64_t *)(local_sp_6 + (-16L)) = 4224207UL;
                indirect_placeholder_4(var_42, var_41);
                *(uint64_t *)(local_sp_6 + (-24L)) = 4224215UL;
                indirect_placeholder();
                return 0UL;
            }
            var_36 = local_sp_4 + (-8L);
            *(uint64_t *)var_36 = 4224085UL;
            indirect_placeholder_2(rdi1_0);
            local_sp_3 = var_36;
            r13_3 = r13_4;
        } else {
            _pre74 = (uint32_t *)var_18;
            _pre_phi75 = _pre74;
            var_30 = local_sp_1 + (-8L);
            *(uint64_t *)var_30 = 4224165UL;
            indirect_placeholder();
            var_31 = (uint64_t)*_pre_phi75;
            var_32 = var_30 + (-8L);
            *(uint64_t *)var_32 = 4224176UL;
            indirect_placeholder_2(var_14);
            var_33 = *(uint64_t *)(rdi + 80UL);
            r13_4 = var_31;
            local_sp_3 = var_32;
            local_sp_4 = var_32;
            rdi1_0 = var_33;
            r13_3 = var_31;
            if (var_33 == 0UL) {
                var_36 = local_sp_4 + (-8L);
                *(uint64_t *)var_36 = 4224085UL;
                indirect_placeholder_2(rdi1_0);
                local_sp_3 = var_36;
                r13_3 = r13_4;
            }
        }
        var_37 = (uint64_t)*var_16;
        var_38 = *(uint64_t *)(rdi + 88UL);
        *(uint64_t *)(local_sp_3 + (-8L)) = 4224100UL;
        indirect_placeholder_4(var_37, var_38);
        var_39 = local_sp_3 + (-16L);
        *(uint64_t *)var_39 = 4224108UL;
        indirect_placeholder();
        var_40 = (uint32_t)r13_3;
        r13_1 = r13_3;
        _pre_phi = var_40;
        local_sp_2 = var_39;
        if ((uint64_t)var_40 == 0UL) {
            return (uint64_t)(uint32_t)r13_1;
        }
        _pre72 = (uint32_t *)var_18;
        _pre_phi73 = _pre72;
        *(uint64_t *)(local_sp_2 + (-8L)) = 4224309UL;
        indirect_placeholder();
        *_pre_phi73 = _pre_phi;
        return (uint64_t)(uint32_t)r13_1;
    }
    if ((var_18 & 4UL) != 0UL) {
        var_34 = local_sp_6 + (-8L);
        *(uint64_t *)var_34 = 4224070UL;
        indirect_placeholder_2(var_14);
        var_35 = *(uint64_t *)(rdi + 80UL);
        local_sp_4 = var_34;
        rdi1_0 = var_35;
        if (var_35 == 0UL) {
            var_41 = *(uint64_t *)(rdi + 88UL);
            var_42 = (uint64_t)*var_16;
            *(uint64_t *)(local_sp_6 + (-16L)) = 4224207UL;
            indirect_placeholder_4(var_42, var_41);
            *(uint64_t *)(local_sp_6 + (-24L)) = 4224215UL;
            indirect_placeholder();
            return 0UL;
        }
        var_36 = local_sp_4 + (-8L);
        *(uint64_t *)var_36 = 4224085UL;
        indirect_placeholder_2(rdi1_0);
        local_sp_3 = var_36;
        r13_3 = r13_4;
        var_37 = (uint64_t)*var_16;
        var_38 = *(uint64_t *)(rdi + 88UL);
        *(uint64_t *)(local_sp_3 + (-8L)) = 4224100UL;
        indirect_placeholder_4(var_37, var_38);
        var_39 = local_sp_3 + (-16L);
        *(uint64_t *)var_39 = 4224108UL;
        indirect_placeholder();
        var_40 = (uint32_t)r13_3;
        r13_1 = r13_3;
        _pre_phi = var_40;
        local_sp_2 = var_39;
        if ((uint64_t)var_40 == 0UL) {
            return (uint64_t)(uint32_t)r13_1;
        }
        _pre72 = (uint32_t *)var_18;
        _pre_phi73 = _pre72;
        *(uint64_t *)(local_sp_2 + (-8L)) = 4224309UL;
        indirect_placeholder();
        *_pre_phi73 = _pre_phi;
        return (uint64_t)(uint32_t)r13_1;
    }
    var_19 = local_sp_11 + (-24L);
    *(uint64_t *)var_19 = 4224142UL;
    indirect_placeholder();
    local_sp_5 = var_19;
    if (var_17 == 0U) {
        return;
    }
    *(uint64_t *)(local_sp_11 + (-32L)) = 4224237UL;
    indirect_placeholder();
    var_20 = (uint32_t *)var_18;
    var_21 = *var_20;
    var_22 = (uint64_t)var_21;
    var_23 = local_sp_11 + (-40L);
    *(uint64_t *)var_23 = 4224250UL;
    indirect_placeholder();
    _pre_phi75 = var_20;
    local_sp_1 = var_23;
    _pre_phi73 = var_20;
    r13_4 = var_22;
    if (var_21 == 0U) {
        var_30 = local_sp_1 + (-8L);
        *(uint64_t *)var_30 = 4224165UL;
        indirect_placeholder();
        var_31 = (uint64_t)*_pre_phi75;
        var_32 = var_30 + (-8L);
        *(uint64_t *)var_32 = 4224176UL;
        indirect_placeholder_2(var_14);
        var_33 = *(uint64_t *)(rdi + 80UL);
        r13_4 = var_31;
        local_sp_3 = var_32;
        local_sp_4 = var_32;
        rdi1_0 = var_33;
        r13_3 = var_31;
        if (var_33 == 0UL) {
            var_36 = local_sp_4 + (-8L);
            *(uint64_t *)var_36 = 4224085UL;
            indirect_placeholder_2(rdi1_0);
            local_sp_3 = var_36;
            r13_3 = r13_4;
        }
    } else {
        var_24 = local_sp_11 + (-48L);
        *(uint64_t *)var_24 = 4224267UL;
        indirect_placeholder_2(var_14);
        var_25 = *(uint64_t *)(rdi + 80UL);
        local_sp_4 = var_24;
        rdi1_0 = var_25;
        if (var_25 != 0UL) {
            var_26 = (uint64_t)*var_16;
            var_27 = *(uint64_t *)(rdi + 88UL);
            *(uint64_t *)(local_sp_11 + (-56L)) = 4224296UL;
            indirect_placeholder_4(var_26, var_27);
            var_28 = local_sp_11 + (-64L);
            *(uint64_t *)var_28 = 4224304UL;
            indirect_placeholder();
            _pre = (uint32_t)var_22;
            _pre_phi = _pre;
            local_sp_2 = var_28;
            *(uint64_t *)(local_sp_2 + (-8L)) = 4224309UL;
            indirect_placeholder();
            *_pre_phi73 = _pre_phi;
            return (uint64_t)(uint32_t)r13_1;
        }
        var_36 = local_sp_4 + (-8L);
        *(uint64_t *)var_36 = 4224085UL;
        indirect_placeholder_2(rdi1_0);
        local_sp_3 = var_36;
        r13_3 = r13_4;
    }
    var_37 = (uint64_t)*var_16;
    var_38 = *(uint64_t *)(rdi + 88UL);
    *(uint64_t *)(local_sp_3 + (-8L)) = 4224100UL;
    indirect_placeholder_4(var_37, var_38);
    var_39 = local_sp_3 + (-16L);
    *(uint64_t *)var_39 = 4224108UL;
    indirect_placeholder();
    var_40 = (uint32_t)r13_3;
    r13_1 = r13_3;
    _pre_phi = var_40;
    local_sp_2 = var_39;
    if ((uint64_t)var_40 == 0UL) {
        return (uint64_t)(uint32_t)r13_1;
    }
    _pre72 = (uint32_t *)var_18;
    _pre_phi73 = _pre72;
    *(uint64_t *)(local_sp_2 + (-8L)) = 4224309UL;
    indirect_placeholder();
    *_pre_phi73 = _pre_phi;
    return (uint64_t)(uint32_t)r13_1;
}
