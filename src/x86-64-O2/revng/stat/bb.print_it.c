typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_32_ret_type;
struct indirect_placeholder_30_ret_type;
struct indirect_placeholder_31_ret_type;
struct indirect_placeholder_32_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_30_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_31_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t init_r15(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_4(uint64_t param_0);
extern uint64_t init_rax(void);
extern void indirect_placeholder_2(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_32_ret_type indirect_placeholder_32(uint64_t param_0);
extern struct indirect_placeholder_30_ret_type indirect_placeholder_30(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_31_ret_type indirect_placeholder_31(uint64_t param_0, uint64_t param_1, uint64_t param_2);
uint64_t bb_print_it(uint64_t r8, uint64_t rdx, uint64_t rdi, uint64_t rcx, uint64_t rsi) {
    struct indirect_placeholder_30_ret_type var_62;
    uint64_t rdi3_0_ph_in;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    struct indirect_placeholder_32_ret_type var_10;
    uint64_t var_11;
    unsigned char var_12;
    uint64_t var_13;
    uint64_t rdi3_0_in;
    uint64_t var_31;
    bool var_32;
    uint64_t r9_1_ph;
    uint64_t rax_0;
    uint64_t r12_0;
    uint64_t r81_1;
    uint64_t rcx4_1;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t r81_2;
    uint64_t var_27;
    uint32_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t r12_1;
    uint64_t r12_2;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t rbx_0;
    uint64_t var_57;
    uint64_t var_58;
    unsigned char *var_59;
    uint64_t var_60;
    uint64_t r81_4_ph;
    uint64_t rcx4_3_ph;
    uint64_t local_sp_3;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_61;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_50;
    uint64_t r15_1;
    uint64_t var_51;
    uint64_t local_sp_0;
    uint64_t var_52;
    uint64_t rax_4;
    uint64_t _pre_phi;
    uint64_t local_sp_1;
    uint64_t r15_2;
    uint64_t rax_1;
    uint64_t r12_3;
    uint64_t local_sp_4;
    uint64_t r15_0;
    uint64_t r81_3;
    uint64_t rcx4_2;
    uint64_t r9_0;
    unsigned char var_68;
    uint64_t local_sp_3_ph;
    unsigned char *_pre_phi176;
    uint64_t local_sp_2;
    uint64_t rax_2_ph;
    uint64_t r15_1_ph;
    uint64_t rax_2;
    uint64_t var_14;
    unsigned char var_15;
    unsigned char *var_17;
    unsigned char var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t _pre;
    uint64_t rax_3;
    uint64_t var_35;
    uint64_t var_36;
    struct indirect_placeholder_31_ret_type var_37;
    uint64_t var_21;
    unsigned char var_22;
    uint32_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_38;
    unsigned char var_39;
    uint64_t var_40;
    unsigned char var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t local_sp_5;
    uint64_t rax_5;
    uint64_t var_44;
    unsigned char var_45;
    uint64_t var_16;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    unsigned char var_49;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_r12();
    var_3 = init_rbx();
    var_4 = init_r15();
    var_5 = init_r13();
    var_6 = init_r14();
    var_7 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_4;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_2;
    *(uint64_t *)(var_0 + (-40L)) = var_7;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    *(uint32_t *)(var_0 + (-84L)) = (uint32_t)rsi;
    *(uint64_t *)(var_0 + (-80L)) = rdx;
    *(uint64_t *)(var_0 + (-72L)) = rcx;
    *(uint64_t *)(var_0 + (-64L)) = r8;
    *(uint64_t *)(var_0 + (-96L)) = 4209321UL;
    indirect_placeholder();
    var_8 = var_1 + 3UL;
    var_9 = var_0 + (-104L);
    *(uint64_t *)var_9 = 4209330UL;
    var_10 = indirect_placeholder_32(var_8);
    var_11 = var_10.field_0;
    var_12 = *(unsigned char *)rdi;
    var_13 = (uint64_t)var_12;
    *(unsigned char *)(var_0 + (-101L)) = (unsigned char)'\x00';
    rdi3_0_ph_in = var_13;
    local_sp_3_ph = var_9;
    local_sp_2 = var_9;
    rax_2_ph = var_11;
    r15_1_ph = rdi;
    rax_5 = 0UL;
    if ((uint64_t)var_12 == 0UL) {
        *(uint64_t *)(local_sp_2 + (-8L)) = 4209495UL;
        indirect_placeholder();
        *(uint64_t *)(local_sp_2 + (-16L)) = 4209514UL;
        indirect_placeholder();
        return (uint64_t)*(unsigned char *)(local_sp_2 + (-13L));
    }
    r81_4_ph = var_10.field_1;
    rcx4_3_ph = var_10.field_2;
    r9_1_ph = var_10.field_3;
    while (1U)
        {
            rdi3_0_in = rdi3_0_ph_in;
            rcx4_1 = rcx4_3_ph;
            local_sp_3 = local_sp_3_ph;
            r15_1 = r15_1_ph;
            r81_3 = r81_4_ph;
            rcx4_2 = rcx4_3_ph;
            r9_0 = r9_1_ph;
            rax_2 = rax_2_ph;
            while (1U)
                {
                    var_14 = r15_1 + 1UL;
                    var_15 = (unsigned char)rdi3_0_in;
                    r15_1 = var_14;
                    rax_1 = rax_2;
                    r12_3 = var_14;
                    local_sp_4 = local_sp_3;
                    r15_0 = var_14;
                    local_sp_5 = local_sp_3;
                    if ((uint64_t)(var_15 + '\xdb') != 0UL) {
                        loop_state_var = 3U;
                        break;
                    }
                    if ((uint64_t)(var_15 + '\xa4') != 0UL) {
                        loop_state_var = 4U;
                        break;
                    }
                    if (*(unsigned char *)4326688UL == '\x00') {
                        _pre_phi176 = (unsigned char *)var_14;
                        rax_5 = rax_2;
                    } else {
                        var_17 = (unsigned char *)var_14;
                        var_18 = *var_17;
                        var_19 = (uint64_t)((uint32_t)(uint64_t)var_18 + (-48));
                        rax_3 = var_19;
                        _pre_phi176 = var_17;
                        if ((uint64_t)((unsigned char)var_19 & '\xf8') != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        if ((uint64_t)(var_18 + '\x88') != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        if (var_18 != '\x00') {
                            loop_state_var = 2U;
                            break;
                        }
                        var_20 = local_sp_3 + (-8L);
                        *(uint64_t *)var_20 = 4209460UL;
                        indirect_placeholder_2(0UL, r81_4_ph, 4289168UL, 0UL, rcx4_3_ph, 0UL, r9_1_ph);
                        local_sp_5 = var_20;
                    }
                    var_44 = local_sp_5 + (-8L);
                    *(uint64_t *)var_44 = 4209476UL;
                    indirect_placeholder();
                    var_45 = *_pre_phi176;
                    local_sp_2 = var_44;
                    local_sp_3 = var_44;
                    rax_2 = rax_5;
                    rdi3_0_in = (uint64_t)var_45;
                    if ((uint64_t)var_45 == 0UL) {
                        continue;
                    }
                    loop_state_var = 5U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 5U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 3U:
                {
                    *(uint64_t *)(local_sp_3 + (-8L)) = 4209549UL;
                    indirect_placeholder();
                    var_46 = (rax_2 + r15_1) + 1UL;
                    var_47 = local_sp_3 + (-16L);
                    *(uint64_t *)var_47 = 4209567UL;
                    indirect_placeholder();
                    var_48 = var_46 + rax_2;
                    var_49 = *(unsigned char *)var_48;
                    local_sp_0 = var_47;
                    r12_2 = var_48;
                    rbx_0 = (uint64_t)var_49;
                    if ((uint64_t)(var_49 + '\xd2') == 0UL) {
                        var_50 = local_sp_3 + (-24L);
                        *(uint64_t *)var_50 = 4209595UL;
                        indirect_placeholder();
                        var_51 = (rax_2 + var_48) + 1UL;
                        local_sp_0 = var_50;
                        r12_2 = var_51;
                        rbx_0 = (uint64_t)*(unsigned char *)var_51;
                    }
                    var_52 = r12_2 - var_14;
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4209629UL;
                    indirect_placeholder();
                    r12_1 = r12_2;
                    r12_3 = r12_2;
                    if (rbx_0 == 0UL) {
                        var_60 = r12_2 + (-1L);
                        r12_1 = var_60;
                        r12_3 = r12_1;
                        if (var_52 == 0UL) {
                            var_61 = var_52 + 1UL;
                            *(unsigned char *)(var_61 + var_11) = (unsigned char)rbx_0;
                            *(unsigned char *)((var_52 + var_11) + 2UL) = (unsigned char)'\x00';
                            *(uint64_t *)(local_sp_0 + (-16L)) = 4210013UL;
                            var_62 = indirect_placeholder_30(r12_1, rbx_0, r15_1, var_61, var_52, var_11, var_11);
                            var_63 = var_62.field_0;
                            var_64 = var_62.field_1;
                            var_65 = var_62.field_6;
                            *(uint64_t *)(local_sp_0 + (-24L)) = 4210035UL;
                            indirect_placeholder_2(0UL, var_64, 4293297UL, 1UL, var_63, 0UL, var_65);
                            abort();
                        }
                        var_66 = r12_1 + 1UL;
                        var_67 = local_sp_0 + (-16L);
                        *(uint64_t *)var_67 = 4209716UL;
                        indirect_placeholder();
                        local_sp_1 = var_67;
                        r15_0 = var_66;
                    } else {
                        if ((uint64_t)((unsigned char)rbx_0 + '\xdb') == 0UL) {
                            var_53 = *(uint64_t *)(local_sp_0 + 16UL);
                            var_54 = *(uint64_t *)local_sp_0;
                            var_55 = (uint64_t)*(uint32_t *)(local_sp_0 + (-4L));
                            var_56 = *(uint64_t *)(local_sp_0 + 8UL);
                            var_57 = r12_2 + 1UL;
                            var_58 = local_sp_0 + (-16L);
                            *(uint64_t *)var_58 = 4209673UL;
                            indirect_placeholder();
                            var_59 = (unsigned char *)(local_sp_0 + (-13L));
                            *var_59 = (*var_59 | (unsigned char)var_56);
                            local_sp_1 = var_58;
                            rax_1 = var_56;
                            r15_0 = var_57;
                            r81_3 = var_54;
                            rcx4_2 = var_55;
                            r9_0 = var_53;
                        } else {
                            r12_3 = r12_1;
                            if (var_52 != 0UL) {
                                var_61 = var_52 + 1UL;
                                *(unsigned char *)(var_61 + var_11) = (unsigned char)rbx_0;
                                *(unsigned char *)((var_52 + var_11) + 2UL) = (unsigned char)'\x00';
                                *(uint64_t *)(local_sp_0 + (-16L)) = 4210013UL;
                                var_62 = indirect_placeholder_30(r12_1, rbx_0, r15_1, var_61, var_52, var_11, var_11);
                                var_63 = var_62.field_0;
                                var_64 = var_62.field_1;
                                var_65 = var_62.field_6;
                                *(uint64_t *)(local_sp_0 + (-24L)) = 4210035UL;
                                indirect_placeholder_2(0UL, var_64, 4293297UL, 1UL, var_63, 0UL, var_65);
                                abort();
                            }
                            var_66 = r12_1 + 1UL;
                            var_67 = local_sp_0 + (-16L);
                            *(uint64_t *)var_67 = 4209716UL;
                            indirect_placeholder();
                            local_sp_1 = var_67;
                            r15_0 = var_66;
                        }
                    }
                    var_68 = *(unsigned char *)(r12_3 + 1UL);
                    rdi3_0_ph_in = (uint64_t)var_68;
                    r9_1_ph = r9_0;
                    r81_4_ph = r81_3;
                    rcx4_3_ph = rcx4_2;
                    local_sp_3_ph = local_sp_1;
                    local_sp_2 = local_sp_1;
                    rax_2_ph = rax_1;
                    r15_1_ph = r15_0;
                    if ((uint64_t)var_68 != 0UL) {
                        continue;
                    }
                    switch_state_var = 1;
                    break;
                }
                break;
              case 4U:
              case 2U:
              case 1U:
              case 0U:
                {
                    switch (loop_state_var) {
                      case 4U:
                        {
                            var_16 = local_sp_3 + (-8L);
                            *(uint64_t *)var_16 = 4209365UL;
                            indirect_placeholder();
                            local_sp_1 = var_16;
                            r12_3 = r15_1;
                        }
                        break;
                      case 2U:
                        {
                            _pre = r15_1 + 2UL;
                            _pre_phi = _pre;
                            var_35 = (uint64_t)(uint32_t)(uint64_t)var_18;
                            var_36 = local_sp_4 + (-8L);
                            *(uint64_t *)var_36 = 4209768UL;
                            var_37 = indirect_placeholder_31(r81_4_ph, var_35, r9_1_ph);
                            local_sp_1 = var_36;
                            rax_1 = rax_3;
                            r15_0 = _pre_phi;
                            r81_3 = var_37.field_0;
                            rcx4_2 = var_37.field_1;
                            r9_0 = var_37.field_2;
                        }
                        break;
                      case 1U:
                        {
                            var_21 = r15_1 + 2UL;
                            var_22 = *(unsigned char *)var_21;
                            var_23 = (uint32_t)(uint64_t)var_22;
                            var_24 = (uint64_t)var_23;
                            *(uint64_t *)(local_sp_3 + (-8L)) = 4209741UL;
                            var_25 = indirect_placeholder_4(var_24);
                            var_26 = local_sp_3 + (-16L);
                            *(uint64_t *)var_26 = 4209749UL;
                            indirect_placeholder();
                            r12_0 = var_21;
                            _pre_phi = var_21;
                            local_sp_4 = var_26;
                            rax_3 = var_25;
                            if ((uint64_t)(uint32_t)var_25 == 0UL) {
                                var_35 = (uint64_t)(uint32_t)(uint64_t)var_18;
                                var_36 = local_sp_4 + (-8L);
                                *(uint64_t *)var_36 = 4209768UL;
                                var_37 = indirect_placeholder_31(r81_4_ph, var_35, r9_1_ph);
                                local_sp_1 = var_36;
                                rax_1 = rax_3;
                                r15_0 = _pre_phi;
                                r81_3 = var_37.field_0;
                                rcx4_2 = var_37.field_1;
                                r9_0 = var_37.field_2;
                            } else {
                                r81_2 = ((uint64_t)((var_22 + '\x9f') & '\xfe') > 5UL) ? (uint64_t)(var_23 + (-65)) : r81_4_ph;
                                var_27 = r15_1 + 3UL;
                                var_28 = (uint32_t)(uint64_t)*(unsigned char *)var_27;
                                var_29 = (uint64_t)var_28;
                                *(uint64_t *)(local_sp_3 + (-24L)) = 4209903UL;
                                var_30 = indirect_placeholder_4(var_29);
                                *(uint64_t *)(local_sp_3 + (-32L)) = 4209911UL;
                                indirect_placeholder();
                                rax_0 = var_30;
                                r81_1 = r81_2;
                                if ((uint64_t)(uint32_t)var_30 != 0UL) {
                                    var_31 = (uint64_t)(var_28 + (-97));
                                    var_32 = ((uint64_t)((unsigned char)var_31 & '\xfe') > 5UL);
                                    rax_0 = var_31;
                                    r12_0 = var_27;
                                    r81_1 = var_32 ? (uint64_t)(var_28 + (-55)) : r81_2;
                                    rcx4_1 = var_32 ? (uint64_t)(var_28 + (-65)) : rcx4_3_ph;
                                }
                                var_33 = r12_0 + 1UL;
                                var_34 = local_sp_3 + (-40L);
                                *(uint64_t *)var_34 = 4209952UL;
                                indirect_placeholder();
                                local_sp_1 = var_34;
                                rax_1 = rax_0;
                                r12_3 = r12_0;
                                r15_0 = var_33;
                                r81_3 = r81_1;
                                rcx4_2 = rcx4_1;
                            }
                        }
                        break;
                      case 0U:
                        {
                            var_38 = r15_1 + 2UL;
                            var_39 = *(unsigned char *)var_38;
                            r15_2 = var_38;
                            if ((uint64_t)((var_39 + '\xd0') & '\xf8') == 0UL) {
                                var_40 = r15_1 + 3UL;
                                var_41 = *(unsigned char *)var_40;
                                rax_4 = (uint64_t)(uint32_t)(uint64_t)var_41;
                                r15_2 = ((uint64_t)((var_41 + '\xd0') & '\xf8') == 0UL) ? (r15_1 + 4UL) : var_40;
                            } else {
                                rax_4 = (uint64_t)(uint32_t)(uint64_t)var_39;
                            }
                            var_42 = local_sp_3 + (-8L);
                            *(uint64_t *)var_42 = 4209833UL;
                            indirect_placeholder();
                            var_43 = r15_2 + (-1L);
                            local_sp_1 = var_42;
                            rax_1 = rax_4;
                            r12_3 = var_43;
                            r15_0 = r15_2;
                        }
                        break;
                    }
                    var_68 = *(unsigned char *)(r12_3 + 1UL);
                    rdi3_0_ph_in = (uint64_t)var_68;
                    r9_1_ph = r9_0;
                    r81_4_ph = r81_3;
                    rcx4_3_ph = rcx4_2;
                    local_sp_3_ph = local_sp_1;
                    local_sp_2 = local_sp_1;
                    rax_2_ph = rax_1;
                    r15_1_ph = r15_0;
                    if ((uint64_t)var_68 != 0UL) {
                        continue;
                    }
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    *(uint64_t *)(local_sp_2 + (-8L)) = 4209495UL;
    indirect_placeholder();
    *(uint64_t *)(local_sp_2 + (-16L)) = 4209514UL;
    indirect_placeholder();
    return (uint64_t)*(unsigned char *)(local_sp_2 + (-13L));
}
