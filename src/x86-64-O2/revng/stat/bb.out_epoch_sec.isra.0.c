typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_36_ret_type;
struct helper_idivq_EAX_wrapper_ret_type;
struct type_6;
struct helper_idivl_EAX_wrapper_ret_type;
struct indirect_placeholder_36_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct helper_idivq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint32_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint32_t field_5;
    uint32_t field_6;
    uint64_t field_7;
    uint32_t field_8;
    uint64_t field_9;
    uint32_t field_10;
    unsigned char field_11;
    uint32_t field_12;
};
struct type_6 {
};
struct helper_idivl_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint32_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint32_t field_5;
    uint32_t field_6;
    uint64_t field_7;
    uint32_t field_8;
    uint64_t field_9;
    uint32_t field_10;
    unsigned char field_11;
    uint32_t field_12;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r12(void);
extern uint64_t init_r15(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint32_t init_state_0x8248(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern struct indirect_placeholder_36_ret_type indirect_placeholder_36(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct helper_idivq_EAX_wrapper_ret_type helper_idivq_EAX_wrapper(struct type_6 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint32_t param_13, uint64_t param_14, uint64_t param_15, uint32_t param_16, uint64_t param_17, uint64_t param_18, uint32_t param_19);
extern struct helper_idivl_EAX_wrapper_ret_type helper_idivl_EAX_wrapper(struct type_6 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint32_t param_13, uint64_t param_14, uint64_t param_15, uint32_t param_16, uint64_t param_17, uint64_t param_18, uint32_t param_19);
void bb_out_epoch_sec_isra_0(uint64_t rdx, uint64_t rdi, uint64_t rcx, uint64_t r10, uint64_t rsi, uint64_t r9) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint32_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint32_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint32_t var_14;
    uint64_t var_15;
    uint64_t *var_16;
    uint64_t var_17;
    struct indirect_placeholder_36_ret_type var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t rbp_2;
    uint64_t r12_0;
    uint64_t local_sp_0;
    uint64_t rdi2_2;
    uint64_t rbp_1;
    uint64_t r15_2;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint32_t var_39;
    uint64_t r104_1;
    uint64_t var_40;
    uint64_t r15_0;
    uint64_t rsi5_1;
    uint64_t rdi2_1;
    uint64_t rdi2_0;
    uint64_t rsi5_0;
    uint64_t rax_1;
    unsigned char var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t r15_1;
    uint64_t rbp_0;
    uint64_t r104_0;
    uint64_t local_sp_1;
    uint64_t var_45;
    uint64_t r15_3;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t r104_2;
    uint64_t local_sp_2;
    uint64_t r13_1;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint32_t *var_34;
    uint32_t var_35;
    uint64_t var_46;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_47;
    uint64_t local_sp_3;
    uint64_t rcx3_0;
    uint64_t rsi5_2;
    uint32_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    struct helper_idivq_EAX_wrapper_ret_type var_53;
    uint64_t r13_2;
    uint64_t rbp_3;
    uint64_t r104_3;
    uint64_t rsi5_3;
    uint32_t state_0x8248_0;
    uint64_t state_0x9018_0;
    uint32_t state_0x9010_0;
    uint64_t local_sp_4;
    uint64_t state_0x82d8_0;
    uint32_t state_0x9080_0;
    uint64_t var_54;
    struct helper_idivl_EAX_wrapper_ret_type var_55;
    uint32_t var_56;
    struct helper_idivq_EAX_wrapper_ret_type var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r12();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_r13();
    var_5 = init_r14();
    var_6 = init_rbp();
    var_7 = init_cc_src2();
    var_8 = init_state_0x8248();
    var_9 = init_state_0x8328();
    var_10 = init_state_0x9018();
    var_11 = init_state_0x9010();
    var_12 = init_state_0x8408();
    var_13 = init_state_0x82d8();
    var_14 = init_state_0x9080();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_1;
    *(uint64_t *)(var_0 + (-40L)) = var_6;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_15 = var_0 + (-80L);
    var_16 = (uint64_t *)var_15;
    *var_16 = rcx;
    var_17 = var_0 + (-96L);
    *(uint64_t *)var_17 = 4211548UL;
    var_18 = indirect_placeholder_36(rsi, rdi, 46UL);
    var_19 = var_18.field_0;
    var_20 = var_18.field_1;
    var_21 = var_18.field_4;
    rbp_2 = 0UL;
    r12_0 = rdx;
    rdi2_2 = 1UL;
    r15_2 = rsi;
    r15_0 = 0UL;
    rsi5_1 = 1UL;
    rdi2_0 = rdi;
    rsi5_0 = 0UL;
    rax_1 = rdi;
    local_sp_2 = var_17;
    r13_1 = var_19;
    local_sp_3 = var_17;
    rsi5_2 = 1UL;
    rbp_3 = 9UL;
    rsi5_3 = 1UL;
    state_0x8248_0 = var_8;
    state_0x9018_0 = var_10;
    state_0x9010_0 = var_11;
    local_sp_4 = var_17;
    state_0x82d8_0 = var_13;
    state_0x9080_0 = var_14;
    if (var_19 == 0UL) {
        var_47 = var_18.field_3;
        *(uint32_t *)var_15 = 0U;
        r104_2 = var_47;
        r15_3 = r15_2;
        rcx3_0 = (uint64_t)(uint32_t)rbp_2;
        rbp_3 = rbp_2;
        r104_3 = r104_2;
        local_sp_4 = local_sp_3;
        var_48 = (uint32_t)rcx3_0;
        var_49 = (uint64_t)(var_48 + 1U);
        var_50 = (uint64_t)(((uint32_t)rsi5_2 * 10U) & (-2));
        rcx3_0 = var_49;
        rsi5_2 = var_50;
        rsi5_3 = var_50;
        do {
            var_48 = (uint32_t)rcx3_0;
            var_49 = (uint64_t)(var_48 + 1U);
            var_50 = (uint64_t)(((uint32_t)rsi5_2 * 10U) & (-2));
            rcx3_0 = var_49;
            rsi5_2 = var_50;
            rsi5_3 = var_50;
        } while ((uint64_t)(var_48 + (-8)) != 0UL);
        var_51 = *(uint64_t *)(local_sp_3 + 8UL);
        var_52 = (uint64_t)((long)(rsi5_2 * 42949672960UL) >> (long)32UL);
        var_53 = helper_idivq_EAX_wrapper((struct type_6 *)(0UL), var_52, 4211943UL, var_51, rdx, var_20, rbp_2, (uint64_t)((long)var_51 >> (long)63UL), var_52, var_49, r104_2, var_50, var_21, var_8, var_9, var_10, var_11, var_12, var_13, var_14);
        rdi2_2 = var_52;
        r13_2 = var_53.field_1;
        state_0x8248_0 = var_53.field_6;
        state_0x9018_0 = var_53.field_7;
        state_0x9010_0 = var_53.field_8;
        state_0x82d8_0 = var_53.field_9;
        state_0x9080_0 = var_53.field_10;
    } else {
        *(unsigned char *)(rsi + rdi) = (unsigned char)'\x00';
        var_22 = (uint64_t)*(unsigned char *)(var_19 + 1UL);
        var_23 = var_19 - rdi;
        var_24 = (uint64_t)((uint32_t)var_22 + (-48));
        rbp_1 = var_24;
        r104_1 = var_23;
        rbp_0 = var_24;
        r15_3 = var_23;
        r104_3 = var_23;
        if (var_24 > 9UL) {
            rbp_1 = 9UL;
            if ((uint64_t)(((uint32_t)(uint64_t)*(unsigned char *)(var_19 + (-1L)) + (-48)) & (-2)) > 9UL) {
                *(uint32_t *)var_15 = 0U;
                var_46 = *(uint64_t *)(var_0 + (-88L));
                r13_2 = var_46;
                if ((long)rdx > (long)18446744073709551615UL) {
                    var_60 = local_sp_4 + (-8L);
                    *(uint64_t *)var_60 = 4211982UL;
                    indirect_placeholder_12(r12_0, rdi, r15_3);
                    local_sp_0 = var_60;
                } else {
                    var_54 = *(uint64_t *)(local_sp_4 + 8UL);
                    if (var_54 == 0UL) {
                        var_60 = local_sp_4 + (-8L);
                        *(uint64_t *)var_60 = 4211982UL;
                        indirect_placeholder_12(r12_0, rdi, r15_3);
                        local_sp_0 = var_60;
                    } else {
                        var_55 = helper_idivl_EAX_wrapper((struct type_6 *)(0UL), rsi5_3, 4212118UL, 1000000000UL, rdx, var_20, rbp_3, 0UL, rdi2_2, var_54, r104_3, rsi5_3, var_21, state_0x8248_0, var_9, state_0x9018_0, state_0x9010_0, var_12, state_0x82d8_0, state_0x9080_0);
                        var_56 = (uint32_t)var_55.field_1 - (uint32_t)r13_2;
                        var_57 = helper_idivq_EAX_wrapper((struct type_6 *)(0UL), rdi2_2, 4212131UL, var_54, rdx, var_20, rbp_3, (uint64_t)((long)var_54 >> (long)63UL), rdi2_2, var_54, r104_3, rsi5_3, var_21, var_55.field_6, var_9, var_55.field_7, var_55.field_8, var_12, var_55.field_9, var_55.field_10);
                        var_58 = ((uint64_t)(var_56 + (uint32_t)(var_57.field_4 != 0UL)) != 0UL) + rdx;
                        r12_0 = var_58;
                        if (var_58 == 0UL) {
                            var_59 = local_sp_4 + (-8L);
                            *(uint64_t *)var_59 = 4212172UL;
                            indirect_placeholder_1(rdi, r15_3);
                            local_sp_0 = var_59;
                        } else {
                            var_60 = local_sp_4 + (-8L);
                            *(uint64_t *)var_60 = 4211982UL;
                            indirect_placeholder_12(r12_0, rdi, r15_3);
                            local_sp_0 = var_60;
                        }
                    }
                }
                if ((uint64_t)(uint32_t)rbp_3 != 0UL) {
                    *(uint64_t *)(local_sp_0 + (-16L)) = 0UL;
                    *(uint64_t *)(local_sp_0 + (-24L)) = 4212078UL;
                    indirect_placeholder();
                }
                return;
            }
            *(unsigned char *)var_19 = (unsigned char)'\x00';
            rbp_0 = rbp_1;
            var_28 = (uint64_t)*(unsigned char *)(r13_1 + (-2L));
            var_29 = r13_1 + (-1L);
            var_30 = (uint64_t)((uint32_t)var_28 + (-48));
            r13_1 = var_29;
            do {
                var_28 = (uint64_t)*(unsigned char *)(r13_1 + (-2L));
                var_29 = r13_1 + (-1L);
                var_30 = (uint64_t)((uint32_t)var_28 + (-48));
                r13_1 = var_29;
            } while (var_30 <= 9UL);
            *(uint64_t *)(local_sp_2 + 24UL) = r104_1;
            var_31 = local_sp_2 + (-8L);
            *(uint64_t *)var_31 = 4211653UL;
            indirect_placeholder();
            var_32 = *(uint64_t *)(local_sp_2 + 16UL);
            var_33 = (var_30 < 2147483647UL) ? var_30 : 2147483647UL;
            var_34 = (uint32_t *)(local_sp_2 + 8UL);
            var_35 = (uint32_t)var_33;
            *var_34 = var_35;
            r15_1 = var_32;
            r104_0 = var_32;
            local_sp_1 = var_31;
            var_36 = var_29 + (*(unsigned char *)var_29 == '0');
            var_37 = *(uint64_t *)4326672UL;
            var_38 = var_36 - rdi;
            r15_1 = var_38;
            var_39 = var_35 - (uint32_t)var_37;
            if (var_35 <= 1U & var_33 <= var_37 & (int)var_39 <= (int)1U & (int)(var_39 - (uint32_t)rbp_1) <= (int)1U) {
                var_40 = helper_cc_compute_c_wrapper(rdi - var_36, var_36, var_7, 17U);
                if (var_40 == 0UL) {
                    var_44 = local_sp_2 + (-16L);
                    *(uint64_t *)var_44 = 4212266UL;
                    indirect_placeholder();
                    r15_1 = r15_0;
                    local_sp_1 = var_44;
                } else {
                    var_42 = rax_1 + 1UL;
                    rdi2_0 = rdi2_1;
                    rsi5_0 = rsi5_1;
                    rax_1 = var_42;
                    do {
                        var_41 = *(unsigned char *)rax_1;
                        rdi2_1 = rdi2_0;
                        if (var_41 == '-') {
                            *(unsigned char *)rdi2_0 = var_41;
                            rdi2_1 = rdi2_0 + 1UL;
                            rsi5_1 = rsi5_0;
                        }
                        var_42 = rax_1 + 1UL;
                        rdi2_0 = rdi2_1;
                        rsi5_0 = rsi5_1;
                        rax_1 = var_42;
                    } while (var_36 != var_42);
                    var_43 = rdi2_1 - rdi;
                    r15_0 = var_43;
                    r15_1 = var_43;
                    if ((uint64_t)(unsigned char)rsi5_1 == 0UL) {
                        var_44 = local_sp_2 + (-16L);
                        *(uint64_t *)var_44 = 4212266UL;
                        indirect_placeholder();
                        r15_1 = r15_0;
                        local_sp_1 = var_44;
                    }
                }
            }
        } else {
            *var_16 = var_23;
            var_25 = var_0 + (-104L);
            *(uint64_t *)var_25 = 4211837UL;
            indirect_placeholder();
            var_26 = var_0 + (-88L);
            var_27 = *(uint64_t *)var_26;
            r15_2 = var_27;
            r104_1 = var_27;
            r15_1 = var_27;
            r104_0 = var_27;
            local_sp_1 = var_25;
            r104_2 = var_27;
            local_sp_2 = var_25;
            local_sp_3 = var_25;
            if (var_24 != 0UL) {
                *(uint32_t *)var_26 = 0U;
                r15_3 = r15_2;
                rcx3_0 = (uint64_t)(uint32_t)rbp_2;
                rbp_3 = rbp_2;
                r104_3 = r104_2;
                local_sp_4 = local_sp_3;
                var_48 = (uint32_t)rcx3_0;
                var_49 = (uint64_t)(var_48 + 1U);
                var_50 = (uint64_t)(((uint32_t)rsi5_2 * 10U) & (-2));
                rcx3_0 = var_49;
                rsi5_2 = var_50;
                rsi5_3 = var_50;
                do {
                    var_48 = (uint32_t)rcx3_0;
                    var_49 = (uint64_t)(var_48 + 1U);
                    var_50 = (uint64_t)(((uint32_t)rsi5_2 * 10U) & (-2));
                    rcx3_0 = var_49;
                    rsi5_2 = var_50;
                    rsi5_3 = var_50;
                } while ((uint64_t)(var_48 + (-8)) != 0UL);
                var_51 = *(uint64_t *)(local_sp_3 + 8UL);
                var_52 = (uint64_t)((long)(rsi5_2 * 42949672960UL) >> (long)32UL);
                var_53 = helper_idivq_EAX_wrapper((struct type_6 *)(0UL), var_52, 4211943UL, var_51, rdx, var_20, rbp_2, (uint64_t)((long)var_51 >> (long)63UL), var_52, var_49, r104_2, var_50, var_21, var_8, var_9, var_10, var_11, var_12, var_13, var_14);
                rdi2_2 = var_52;
                r13_2 = var_53.field_1;
                state_0x8248_0 = var_53.field_6;
                state_0x9018_0 = var_53.field_7;
                state_0x9010_0 = var_53.field_8;
                state_0x82d8_0 = var_53.field_9;
                state_0x9080_0 = var_53.field_10;
                if ((long)rdx > (long)18446744073709551615UL) {
                    var_60 = local_sp_4 + (-8L);
                    *(uint64_t *)var_60 = 4211982UL;
                    indirect_placeholder_12(r12_0, rdi, r15_3);
                    local_sp_0 = var_60;
                } else {
                    var_54 = *(uint64_t *)(local_sp_4 + 8UL);
                    if (var_54 == 0UL) {
                        var_60 = local_sp_4 + (-8L);
                        *(uint64_t *)var_60 = 4211982UL;
                        indirect_placeholder_12(r12_0, rdi, r15_3);
                        local_sp_0 = var_60;
                    } else {
                        var_55 = helper_idivl_EAX_wrapper((struct type_6 *)(0UL), rsi5_3, 4212118UL, 1000000000UL, rdx, var_20, rbp_3, 0UL, rdi2_2, var_54, r104_3, rsi5_3, var_21, state_0x8248_0, var_9, state_0x9018_0, state_0x9010_0, var_12, state_0x82d8_0, state_0x9080_0);
                        var_56 = (uint32_t)var_55.field_1 - (uint32_t)r13_2;
                        var_57 = helper_idivq_EAX_wrapper((struct type_6 *)(0UL), rdi2_2, 4212131UL, var_54, rdx, var_20, rbp_3, (uint64_t)((long)var_54 >> (long)63UL), rdi2_2, var_54, r104_3, rsi5_3, var_21, var_55.field_6, var_9, var_55.field_7, var_55.field_8, var_12, var_55.field_9, var_55.field_10);
                        var_58 = ((uint64_t)(var_56 + (uint32_t)(var_57.field_4 != 0UL)) != 0UL) + rdx;
                        r12_0 = var_58;
                        if (var_58 == 0UL) {
                            var_59 = local_sp_4 + (-8L);
                            *(uint64_t *)var_59 = 4212172UL;
                            indirect_placeholder_1(rdi, r15_3);
                            local_sp_0 = var_59;
                        } else {
                            var_60 = local_sp_4 + (-8L);
                            *(uint64_t *)var_60 = 4211982UL;
                            indirect_placeholder_12(r12_0, rdi, r15_3);
                            local_sp_0 = var_60;
                        }
                    }
                }
                if ((uint64_t)(uint32_t)rbp_3 != 0UL) {
                    *(uint64_t *)(local_sp_0 + (-16L)) = 0UL;
                    *(uint64_t *)(local_sp_0 + (-24L)) = 4212078UL;
                    indirect_placeholder();
                }
                return;
            }
            if ((uint64_t)(((uint32_t)(uint64_t)*(unsigned char *)(var_19 + (-1L)) + (-48)) & (-2)) > 9UL) {
                *(uint32_t *)var_26 = 0U;
            } else {
                *(unsigned char *)var_19 = (unsigned char)'\x00';
                rbp_0 = rbp_1;
                var_28 = (uint64_t)*(unsigned char *)(r13_1 + (-2L));
                var_29 = r13_1 + (-1L);
                var_30 = (uint64_t)((uint32_t)var_28 + (-48));
                r13_1 = var_29;
                do {
                    var_28 = (uint64_t)*(unsigned char *)(r13_1 + (-2L));
                    var_29 = r13_1 + (-1L);
                    var_30 = (uint64_t)((uint32_t)var_28 + (-48));
                    r13_1 = var_29;
                } while (var_30 <= 9UL);
                *(uint64_t *)(local_sp_2 + 24UL) = r104_1;
                var_31 = local_sp_2 + (-8L);
                *(uint64_t *)var_31 = 4211653UL;
                indirect_placeholder();
                var_32 = *(uint64_t *)(local_sp_2 + 16UL);
                var_33 = (var_30 < 2147483647UL) ? var_30 : 2147483647UL;
                var_34 = (uint32_t *)(local_sp_2 + 8UL);
                var_35 = (uint32_t)var_33;
                *var_34 = var_35;
                r15_1 = var_32;
                r104_0 = var_32;
                local_sp_1 = var_31;
                var_36 = var_29 + (*(unsigned char *)var_29 == '0');
                var_37 = *(uint64_t *)4326672UL;
                var_38 = var_36 - rdi;
                r15_1 = var_38;
                var_39 = var_35 - (uint32_t)var_37;
                if (var_35 <= 1U & var_33 <= var_37 & (int)var_39 <= (int)1U & (int)(var_39 - (uint32_t)rbp_1) <= (int)1U) {
                    var_40 = helper_cc_compute_c_wrapper(rdi - var_36, var_36, var_7, 17U);
                    if (var_40 == 0UL) {
                        var_44 = local_sp_2 + (-16L);
                        *(uint64_t *)var_44 = 4212266UL;
                        indirect_placeholder();
                        r15_1 = r15_0;
                        local_sp_1 = var_44;
                    } else {
                        var_42 = rax_1 + 1UL;
                        rdi2_0 = rdi2_1;
                        rsi5_0 = rsi5_1;
                        rax_1 = var_42;
                        do {
                            var_41 = *(unsigned char *)rax_1;
                            rdi2_1 = rdi2_0;
                            if (var_41 == '-') {
                                *(unsigned char *)rdi2_0 = var_41;
                                rdi2_1 = rdi2_0 + 1UL;
                                rsi5_1 = rsi5_0;
                            }
                            var_42 = rax_1 + 1UL;
                            rdi2_0 = rdi2_1;
                            rsi5_0 = rsi5_1;
                            rax_1 = var_42;
                        } while (var_36 != var_42);
                        var_43 = rdi2_1 - rdi;
                        r15_0 = var_43;
                        r15_1 = var_43;
                        if ((uint64_t)(unsigned char)rsi5_1 == 0UL) {
                            var_44 = local_sp_2 + (-16L);
                            *(uint64_t *)var_44 = 4212266UL;
                            indirect_placeholder();
                            r15_1 = r15_0;
                            local_sp_1 = var_44;
                        }
                    }
                }
            }
        }
        rbp_2 = rbp_0;
        r15_2 = r15_1;
        r15_3 = r15_1;
        r104_2 = r104_0;
        local_sp_3 = local_sp_1;
        rbp_3 = rbp_0;
        r104_3 = r104_0;
        local_sp_4 = local_sp_1;
        if ((int)(uint32_t)rbp_0 > (int)8U) {
            var_45 = *(uint64_t *)(local_sp_1 + 8UL);
            r13_2 = var_45;
        } else {
            r15_3 = r15_2;
            rcx3_0 = (uint64_t)(uint32_t)rbp_2;
            rbp_3 = rbp_2;
            r104_3 = r104_2;
            local_sp_4 = local_sp_3;
            var_48 = (uint32_t)rcx3_0;
            var_49 = (uint64_t)(var_48 + 1U);
            var_50 = (uint64_t)(((uint32_t)rsi5_2 * 10U) & (-2));
            rcx3_0 = var_49;
            rsi5_2 = var_50;
            rsi5_3 = var_50;
            do {
                var_48 = (uint32_t)rcx3_0;
                var_49 = (uint64_t)(var_48 + 1U);
                var_50 = (uint64_t)(((uint32_t)rsi5_2 * 10U) & (-2));
                rcx3_0 = var_49;
                rsi5_2 = var_50;
                rsi5_3 = var_50;
            } while ((uint64_t)(var_48 + (-8)) != 0UL);
            var_51 = *(uint64_t *)(local_sp_3 + 8UL);
            var_52 = (uint64_t)((long)(rsi5_2 * 42949672960UL) >> (long)32UL);
            var_53 = helper_idivq_EAX_wrapper((struct type_6 *)(0UL), var_52, 4211943UL, var_51, rdx, var_20, rbp_2, (uint64_t)((long)var_51 >> (long)63UL), var_52, var_49, r104_2, var_50, var_21, var_8, var_9, var_10, var_11, var_12, var_13, var_14);
            rdi2_2 = var_52;
            r13_2 = var_53.field_1;
            state_0x8248_0 = var_53.field_6;
            state_0x9018_0 = var_53.field_7;
            state_0x9010_0 = var_53.field_8;
            state_0x82d8_0 = var_53.field_9;
            state_0x9080_0 = var_53.field_10;
        }
    }
    if ((long)rdx > (long)18446744073709551615UL) {
        var_60 = local_sp_4 + (-8L);
        *(uint64_t *)var_60 = 4211982UL;
        indirect_placeholder_12(r12_0, rdi, r15_3);
        local_sp_0 = var_60;
    } else {
        var_59 = local_sp_4 + (-8L);
        *(uint64_t *)var_59 = 4212172UL;
        indirect_placeholder_1(rdi, r15_3);
        local_sp_0 = var_59;
    }
    if ((uint64_t)(uint32_t)rbp_3 == 0UL) {
        return;
    }
    *(uint64_t *)(local_sp_0 + (-16L)) = 0UL;
    *(uint64_t *)(local_sp_0 + (-24L)) = 4212078UL;
    indirect_placeholder();
    return;
}
