typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_transit_state_bkref_isra_0_ret_type;
struct indirect_placeholder_256_ret_type;
struct indirect_placeholder_257_ret_type;
struct indirect_placeholder_258_ret_type;
struct indirect_placeholder_259_ret_type;
struct indirect_placeholder_260_ret_type;
struct indirect_placeholder_261_ret_type;
struct indirect_placeholder_262_ret_type;
struct bb_transit_state_bkref_isra_0_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_256_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_257_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_258_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_259_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_260_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_261_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_262_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r15(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_r9(void);
extern uint64_t init_r10(void);
extern uint64_t init_r8(void);
extern struct indirect_placeholder_256_ret_type indirect_placeholder_256(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_257_ret_type indirect_placeholder_257(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_258_ret_type indirect_placeholder_258(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_259_ret_type indirect_placeholder_259(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_260_ret_type indirect_placeholder_260(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_261_ret_type indirect_placeholder_261(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_262_ret_type indirect_placeholder_262(uint64_t param_0, uint64_t param_1, uint64_t param_2);
struct bb_transit_state_bkref_isra_0_ret_type bb_transit_state_bkref_isra_0(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    struct indirect_placeholder_257_ret_type var_101;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t rax_0;
    uint64_t r8_2;
    uint64_t var_113;
    uint64_t r8_3;
    uint64_t local_sp_4;
    uint64_t local_sp_3;
    uint64_t local_sp_2_be;
    uint64_t r14_0_be;
    uint64_t r10_3;
    uint64_t r8_2_be;
    uint64_t r9_3;
    uint64_t r10_2_be;
    uint64_t r9_2_be;
    uint64_t local_sp_2;
    uint64_t r8_1;
    uint64_t var_105;
    struct indirect_placeholder_256_ret_type var_106;
    uint64_t var_107;
    uint64_t var_108;
    uint64_t var_109;
    uint64_t var_110;
    uint32_t *var_111;
    uint32_t var_112;
    uint64_t r14_0;
    uint64_t local_sp_0;
    uint64_t r8_0;
    uint64_t rdx4_0;
    uint64_t r10_0;
    uint64_t r9_0;
    uint32_t var_96;
    uint64_t local_sp_1;
    uint64_t rdx4_1;
    uint64_t r10_7;
    uint64_t r9_7;
    uint64_t r10_1;
    uint64_t r9_1;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t var_100;
    uint64_t var_102;
    uint32_t *var_103;
    uint32_t var_104;
    uint64_t var_79;
    uint64_t var_80;
    struct indirect_placeholder_258_ret_type var_81;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_78;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint32_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint32_t *var_76;
    uint32_t var_77;
    uint64_t var_88;
    uint64_t var_89;
    struct indirect_placeholder_259_ret_type var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t local_sp_7;
    uint64_t var_46;
    uint64_t r8_8;
    uint64_t r9_5_ph;
    uint64_t r10_2;
    uint64_t r9_2;
    uint64_t var_47;
    uint64_t *var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t *var_54;
    uint64_t *_pn_in_in;
    uint64_t storemerge;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    struct indirect_placeholder_260_ret_type var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t *var_62;
    uint32_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_97;
    uint64_t r8_4;
    uint64_t r10_4;
    uint64_t r9_4;
    uint64_t *var_114;
    uint64_t var_115;
    uint64_t local_sp_5_ph;
    uint64_t local_sp_6;
    uint64_t var_14;
    uint64_t *var_15;
    uint32_t *var_16;
    uint64_t *var_17;
    uint64_t *var_18;
    uint64_t *var_19;
    uint64_t *var_20;
    uint64_t *var_21;
    uint64_t *var_22;
    uint64_t r14_1_ph;
    uint64_t r8_5_ph;
    uint64_t r10_5_ph;
    uint64_t r8_6;
    uint64_t local_sp_5;
    uint64_t r14_1;
    uint64_t r8_5;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint32_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    struct indirect_placeholder_261_ret_type var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint16_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    struct indirect_placeholder_262_ret_type var_38;
    uint64_t var_39;
    uint32_t *var_40;
    uint32_t var_41;
    bool var_42;
    uint64_t var_43;
    uint64_t *var_44;
    uint64_t var_45;
    struct bb_transit_state_bkref_isra_0_ret_type mrv;
    struct bb_transit_state_bkref_isra_0_ret_type mrv1;
    struct bb_transit_state_bkref_isra_0_ret_type mrv2;
    struct bb_transit_state_bkref_isra_0_ret_type mrv3;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r15();
    var_2 = init_r12();
    var_3 = init_rbx();
    var_4 = init_r13();
    var_5 = init_r14();
    var_6 = init_r8();
    var_7 = init_rbp();
    var_8 = init_r10();
    var_9 = init_r9();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_2;
    *(uint64_t *)(var_0 + (-40L)) = var_7;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_10 = *(uint64_t *)(rdi + 72UL);
    var_11 = *(uint64_t *)(rdi + 152UL);
    *(uint64_t *)(var_0 + (-168L)) = rsi;
    *(uint64_t *)(var_0 + (-192L)) = var_10;
    var_12 = var_10 << 3UL;
    var_13 = *(uint64_t *)rsi;
    *(uint64_t *)(var_0 + (-184L)) = rdx;
    *(uint64_t *)(var_0 + (-152L)) = var_12;
    rax_0 = 0UL;
    r10_7 = var_8;
    r9_7 = var_9;
    r8_8 = var_6;
    r9_5_ph = var_9;
    r14_1_ph = 0UL;
    r8_5_ph = var_6;
    r10_5_ph = var_8;
    if ((long)var_13 > (long)0UL) {
        mrv.field_0 = rax_0;
        mrv1 = mrv;
        mrv1.field_1 = r8_8;
        mrv2 = mrv1;
        mrv2.field_2 = r10_7;
        mrv3 = mrv2;
        mrv3.field_3 = r9_7;
        return mrv3;
    }
    var_14 = var_0 + (-200L);
    var_15 = (uint64_t *)var_11;
    var_16 = (uint32_t *)(rdi + 160UL);
    var_17 = (uint64_t *)(rdi + 200UL);
    var_18 = (uint64_t *)(rdi + 216UL);
    var_19 = (uint64_t *)(var_11 + 48UL);
    var_20 = (uint64_t *)(var_11 + 24UL);
    var_21 = (uint64_t *)(var_11 + 40UL);
    var_22 = (uint64_t *)(rdi + 184UL);
    local_sp_5_ph = var_14;
    while (1U)
        {
            r10_7 = r10_5_ph;
            r9_7 = r9_5_ph;
            r10_2 = r10_5_ph;
            r9_2 = r9_5_ph;
            local_sp_5 = local_sp_5_ph;
            r14_1 = r14_1_ph;
            r8_5 = r8_5_ph;
            while (1U)
                {
                    var_23 = *(uint64_t *)((r14_1 << 3UL) + **(uint64_t **)(local_sp_5 + 16UL));
                    var_24 = (var_23 << 4UL) + *var_15;
                    var_25 = var_24 + 8UL;
                    local_sp_6 = local_sp_5;
                    local_sp_7 = local_sp_5;
                    r8_6 = r8_5;
                    if (*(unsigned char *)var_25 == '\x04') {
                        var_44 = *(uint64_t **)(local_sp_7 + 32UL);
                        var_45 = r14_1 + 1UL;
                        local_sp_5 = local_sp_7;
                        r14_1 = var_45;
                        r8_5 = r8_6;
                        r8_8 = r8_6;
                        if ((long)var_45 < (long)*var_44) {
                            continue;
                        }
                        loop_state_var = 1U;
                        break;
                    }
                    var_26 = *(uint32_t *)var_25;
                    var_27 = (uint64_t)var_26;
                    if ((uint64_t)(var_26 & 261888U) == 0UL) {
                        var_35 = *(uint64_t *)(local_sp_6 + 8UL);
                        var_36 = *var_17;
                        var_37 = local_sp_6 + (-8L);
                        *(uint64_t *)var_37 = 4265800UL;
                        var_38 = indirect_placeholder_262(var_35, rdi, var_23);
                        var_39 = var_38.field_0;
                        var_40 = (uint32_t *)(local_sp_6 + 100UL);
                        var_41 = (uint32_t)var_39;
                        *var_40 = var_41;
                        var_42 = ((uint64_t)var_41 == 0UL);
                        var_43 = var_38.field_1;
                        rax_0 = var_39;
                        r8_2 = var_43;
                        local_sp_2 = var_37;
                        r14_0 = var_36;
                        local_sp_7 = var_37;
                        r8_8 = var_43;
                        r8_6 = var_43;
                        if (!var_42) {
                            loop_state_var = 1U;
                            break;
                        }
                        rax_0 = 0UL;
                        if ((long)var_36 >= (long)*var_17) {
                            loop_state_var = 0U;
                            break;
                        }
                    }
                    var_28 = (uint64_t)*var_16;
                    var_29 = *(uint64_t *)(local_sp_5 + 8UL);
                    var_30 = local_sp_5 + (-8L);
                    *(uint64_t *)var_30 = 4265746UL;
                    var_31 = indirect_placeholder_261(var_24, var_28, rdi, var_29);
                    var_32 = var_31.field_0;
                    var_33 = var_31.field_1;
                    var_34 = (uint16_t)var_27;
                    local_sp_6 = var_30;
                    local_sp_7 = var_30;
                    r8_6 = var_33;
                    if ((uint64_t)(var_34 & (unsigned short)1024U) == 0UL) {
                        if (!(((uint64_t)(var_34 & (unsigned short)2048U) == 0UL) || ((var_32 & 1UL) == 0UL))) {
                            var_44 = *(uint64_t **)(local_sp_7 + 32UL);
                            var_45 = r14_1 + 1UL;
                            local_sp_5 = local_sp_7;
                            r14_1 = var_45;
                            r8_5 = r8_6;
                            r8_8 = r8_6;
                            if ((long)var_45 < (long)*var_44) {
                                continue;
                            }
                            loop_state_var = 1U;
                            break;
                        }
                    }
                    if (!(((var_32 & 1UL) != 0UL) && ((uint64_t)(var_34 & (unsigned short)2048U) == 0UL))) {
                        var_44 = *(uint64_t **)(local_sp_7 + 32UL);
                        var_45 = r14_1 + 1UL;
                        local_sp_5 = local_sp_7;
                        r14_1 = var_45;
                        r8_5 = r8_6;
                        r8_8 = r8_6;
                        if ((long)var_45 < (long)*var_44) {
                            continue;
                        }
                        loop_state_var = 1U;
                        break;
                    }
                    if (!(((uint64_t)(var_34 & (unsigned short)8192U) != 0UL) && ((var_32 & 2UL) == 0UL))) {
                        var_44 = *(uint64_t **)(local_sp_7 + 32UL);
                        var_45 = r14_1 + 1UL;
                        local_sp_5 = local_sp_7;
                        r14_1 = var_45;
                        r8_5 = r8_6;
                        r8_8 = r8_6;
                        if ((long)var_45 < (long)*var_44) {
                            continue;
                        }
                        loop_state_var = 1U;
                        break;
                    }
                    if (!(((short)(uint16_t)var_26 <= (short)(unsigned short)65535U) && ((var_32 & 8UL) == 0UL))) {
                        var_44 = *(uint64_t **)(local_sp_7 + 32UL);
                        var_45 = r14_1 + 1UL;
                        local_sp_5 = local_sp_7;
                        r14_1 = var_45;
                        r8_5 = r8_6;
                        r8_8 = r8_6;
                        if ((long)var_45 < (long)*var_44) {
                            continue;
                        }
                        loop_state_var = 1U;
                        break;
                    }
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 0U:
                {
                    var_46 = var_23 << 3UL;
                    *(uint64_t *)(local_sp_6 + 80UL) = r14_1;
                    *(uint64_t *)(local_sp_6 + 64UL) = var_46;
                    *(uint64_t *)(local_sp_6 + 72UL) = (var_23 * 24UL);
                    while (1U)
                        {
                            var_47 = (r14_0 * 40UL) + *var_18;
                            r8_3 = r8_2;
                            local_sp_3 = local_sp_2;
                            r10_3 = r10_2;
                            r9_3 = r9_2;
                            r9_7 = r9_2;
                            if (var_23 == *(uint64_t *)var_47) {
                                var_97 = r14_0 + 1UL;
                                local_sp_4 = local_sp_3;
                                local_sp_2_be = local_sp_3;
                                r14_0_be = var_97;
                                r8_2_be = r8_3;
                                r10_2_be = r10_3;
                                r9_2_be = r9_3;
                                r8_4 = r8_3;
                                r10_4 = r10_3;
                                r9_4 = r9_3;
                                if ((long)var_97 >= (long)*var_17) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                r8_2 = r8_2_be;
                                local_sp_2 = local_sp_2_be;
                                r14_0 = r14_0_be;
                                r10_2 = r10_2_be;
                                r9_2 = r9_2_be;
                                continue;
                            }
                            var_48 = (uint64_t *)(local_sp_2 + 8UL);
                            if (*var_48 != *(uint64_t *)(var_47 + 8UL)) {
                                var_49 = *(uint64_t *)(var_47 + 24UL);
                                var_50 = *(uint64_t *)(var_47 + 16UL);
                                var_51 = *var_19;
                                var_52 = var_49 - var_50;
                                var_53 = local_sp_2 + 40UL;
                                var_54 = (uint64_t *)var_53;
                                *var_54 = var_52;
                                if (var_52 == 0UL) {
                                    _pn_in_in = *(uint64_t **)((*(uint64_t *)(local_sp_2 + 80UL) + *var_21) + 16UL);
                                } else {
                                    _pn_in_in = (uint64_t *)(*(uint64_t *)(local_sp_2 + 72UL) + *var_20);
                                }
                                storemerge = (*_pn_in_in * 24UL) + var_51;
                                var_55 = (uint64_t)*var_16;
                                var_56 = (var_49 + *var_48) - var_50;
                                var_57 = var_56 + (-1L);
                                *(uint64_t *)(local_sp_2 + (-8L)) = 4265925UL;
                                var_58 = indirect_placeholder_260(var_50, var_55, rdi, var_57);
                                var_59 = var_58.field_0;
                                var_60 = *var_54;
                                var_61 = var_56 << 3UL;
                                var_62 = (uint64_t *)(local_sp_2 + 16UL);
                                *var_62 = 0UL;
                                var_63 = (uint32_t)var_59;
                                var_64 = (uint64_t)var_63;
                                var_65 = *var_22;
                                var_66 = var_61 + var_65;
                                var_67 = *(uint64_t *)(var_60 + var_65);
                                var_68 = *(uint64_t *)var_66;
                                r10_7 = var_66;
                                if (var_67 == 0UL) {
                                    *var_62 = *(uint64_t *)(var_67 + 16UL);
                                }
                                if (var_68 == 0UL) {
                                    var_88 = local_sp_2 + 100UL;
                                    *(uint64_t *)(local_sp_2 + 48UL) = var_66;
                                    var_89 = local_sp_2 + (-16L);
                                    *(uint64_t *)var_89 = 4266277UL;
                                    var_90 = indirect_placeholder_259(storemerge, var_88, var_64, var_11);
                                    var_91 = var_90.field_0;
                                    var_92 = var_90.field_1;
                                    var_93 = var_90.field_3;
                                    var_94 = *var_54;
                                    var_95 = *var_22;
                                    *(uint64_t *)var_94 = var_91;
                                    r8_1 = var_92;
                                    local_sp_0 = var_89;
                                    r8_0 = var_92;
                                    rdx4_0 = var_95;
                                    r10_0 = var_94;
                                    r9_0 = var_93;
                                    local_sp_1 = var_89;
                                    rdx4_1 = var_95;
                                    r10_1 = var_94;
                                    r9_1 = var_93;
                                    var_96 = *(uint32_t *)(local_sp_0 + 108UL);
                                    r8_1 = r8_0;
                                    local_sp_1 = local_sp_0;
                                    rdx4_1 = rdx4_0;
                                    r10_7 = r10_0;
                                    r9_7 = r9_0;
                                    r10_1 = r10_0;
                                    r9_1 = r9_0;
                                    r8_8 = r8_0;
                                    if (*(uint64_t *)(var_61 + var_95) != 0UL & var_96 != 0U) {
                                        rax_0 = (uint64_t)var_96;
                                        loop_state_var = 0U;
                                        break;
                                    }
                                }
                                var_69 = *(uint64_t *)(var_68 + 80UL);
                                var_70 = local_sp_2 + 104UL;
                                var_71 = local_sp_2 + 48UL;
                                *(uint32_t *)var_71 = var_63;
                                *(uint64_t *)(local_sp_2 + 56UL) = var_61;
                                *(uint64_t *)(local_sp_2 + (-16L)) = 4266017UL;
                                var_72 = indirect_placeholder_7(storemerge, var_70, var_69);
                                var_73 = *(uint32_t *)var_53;
                                var_74 = *(uint64_t *)var_71;
                                var_75 = local_sp_2 + 92UL;
                                var_76 = (uint32_t *)var_75;
                                var_77 = (uint32_t)var_72;
                                *var_76 = var_77;
                                if ((uint64_t)var_77 != 0UL) {
                                    var_78 = var_58.field_1;
                                    *(uint64_t *)(local_sp_2 + (-24L)) = 4266477UL;
                                    indirect_placeholder();
                                    rax_0 = (uint64_t)*(uint32_t *)(local_sp_2 + 84UL);
                                    r8_8 = var_78;
                                    loop_state_var = 0U;
                                    break;
                                }
                                var_79 = (uint64_t)var_73;
                                var_80 = local_sp_2 + 96UL;
                                *var_54 = (var_74 + *var_22);
                                *(uint64_t *)(local_sp_2 + (-24L)) = 4266069UL;
                                var_81 = indirect_placeholder_258(var_80, var_75, var_79, var_11);
                                var_82 = var_81.field_0;
                                var_83 = var_81.field_1;
                                var_84 = var_81.field_2;
                                var_85 = var_81.field_3;
                                **(uint64_t **)(local_sp_2 + 32UL) = var_82;
                                var_86 = local_sp_2 + (-32L);
                                *(uint64_t *)var_86 = 4266090UL;
                                indirect_placeholder();
                                var_87 = *var_22;
                                r8_1 = var_83;
                                local_sp_0 = var_86;
                                r8_0 = var_83;
                                rdx4_0 = var_87;
                                r10_0 = var_84;
                                r9_0 = var_85;
                                local_sp_1 = var_86;
                                rdx4_1 = var_87;
                                r10_1 = var_84;
                                r9_1 = var_85;
                                if (*(uint64_t *)(var_61 + var_87) != 0UL) {
                                    var_96 = *(uint32_t *)(local_sp_0 + 108UL);
                                    r8_1 = r8_0;
                                    local_sp_1 = local_sp_0;
                                    rdx4_1 = rdx4_0;
                                    r10_7 = r10_0;
                                    r9_7 = r9_0;
                                    r10_1 = r10_0;
                                    r9_1 = r9_0;
                                    r8_8 = r8_0;
                                    if (var_96 == 0U) {
                                        rax_0 = (uint64_t)var_96;
                                        loop_state_var = 0U;
                                        break;
                                    }
                                }
                                r8_3 = r8_1;
                                local_sp_3 = local_sp_1;
                                r10_3 = r10_1;
                                r9_3 = r9_1;
                                r8_8 = r8_1;
                                if (*(uint64_t *)(local_sp_1 + 40UL) != 0UL & (long)*(uint64_t *)(*(uint64_t *)(*(uint64_t *)(local_sp_1 + 48UL) + rdx4_1) + 16UL) <= (long)*(uint64_t *)(local_sp_1 + 24UL)) {
                                    var_98 = storemerge + 8UL;
                                    var_99 = *(uint64_t *)(local_sp_1 + 8UL);
                                    var_100 = storemerge + 16UL;
                                    *(uint64_t *)(local_sp_1 + (-8L)) = 4266363UL;
                                    var_101 = indirect_placeholder_257(r8_1, var_100, rdi, var_99, r10_1, var_98, r9_1);
                                    var_102 = var_101.field_0;
                                    var_103 = (uint32_t *)(local_sp_1 + 100UL);
                                    var_104 = (uint32_t)var_102;
                                    *var_103 = var_104;
                                    rax_0 = var_102;
                                    if ((uint64_t)var_104 != 0UL) {
                                        r10_7 = var_101.field_1;
                                        r9_7 = var_101.field_2;
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    var_105 = local_sp_1 + (-16L);
                                    *(uint64_t *)var_105 = 4266385UL;
                                    var_106 = indirect_placeholder_256(var_100, rdi, var_98);
                                    var_107 = var_106.field_0;
                                    var_108 = var_106.field_1;
                                    var_109 = var_106.field_2;
                                    var_110 = var_106.field_3;
                                    var_111 = (uint32_t *)(local_sp_1 + 92UL);
                                    var_112 = (uint32_t)var_107;
                                    *var_111 = var_112;
                                    rax_0 = var_107;
                                    local_sp_4 = var_105;
                                    local_sp_2_be = var_105;
                                    r8_2_be = var_108;
                                    r10_2_be = var_109;
                                    r9_2_be = var_110;
                                    r10_7 = var_109;
                                    r9_7 = var_110;
                                    r8_8 = var_108;
                                    r8_4 = var_108;
                                    r10_4 = var_109;
                                    r9_4 = var_110;
                                    if ((uint64_t)var_112 != 0UL) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    var_113 = r14_0 + 1UL;
                                    r14_0_be = var_113;
                                    if ((long)var_113 >= (long)*var_17) {
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    r8_2 = r8_2_be;
                                    local_sp_2 = local_sp_2_be;
                                    r14_0 = r14_0_be;
                                    r10_2 = r10_2_be;
                                    r9_2 = r9_2_be;
                                    continue;
                                }
                            }
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 0U:
                        {
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 1U:
                        {
                            var_114 = *(uint64_t **)(local_sp_4 + 32UL);
                            var_115 = *(uint64_t *)(local_sp_4 + 88UL) + 1UL;
                            r10_7 = r10_4;
                            r9_7 = r9_4;
                            r8_8 = r8_4;
                            r9_5_ph = r9_4;
                            local_sp_5_ph = local_sp_4;
                            r14_1_ph = var_115;
                            r8_5_ph = r8_4;
                            r10_5_ph = r10_4;
                            if ((long)var_115 >= (long)*var_114) {
                                continue;
                            }
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    mrv.field_0 = rax_0;
    mrv1 = mrv;
    mrv1.field_1 = r8_8;
    mrv2 = mrv1;
    mrv2.field_2 = r10_7;
    mrv3 = mrv2;
    mrv3.field_3 = r9_7;
    return mrv3;
}
