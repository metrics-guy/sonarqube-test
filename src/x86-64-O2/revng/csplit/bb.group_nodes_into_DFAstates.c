typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_pcmpeql_xmm_wrapper_ret_type;
struct type_4;
struct type_6;
struct indirect_placeholder_211_ret_type;
struct indirect_placeholder_212_ret_type;
struct indirect_placeholder_210_ret_type;
struct helper_pcmpeql_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_4 {
};
struct type_6 {
};
struct indirect_placeholder_211_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_212_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_210_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r15(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t init_state_0x8558(void);
extern uint64_t init_state_0x8560(void);
extern uint64_t indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_8(uint64_t param_0);
extern void indirect_placeholder_39(uint64_t param_0, uint64_t param_1);
extern uint64_t init_rsi(void);
extern struct helper_pcmpeql_xmm_wrapper_ret_type helper_pcmpeql_xmm_wrapper(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_211_ret_type indirect_placeholder_211(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_212_ret_type indirect_placeholder_212(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_210_ret_type indirect_placeholder_210(uint64_t param_0, uint64_t param_1);
uint64_t bb_group_nodes_into_DFAstates(uint64_t rdx, uint64_t rdi, uint64_t rcx) {
    struct helper_pcmpeql_xmm_wrapper_ret_type var_24;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t r12_9;
    uint64_t state_0x8560_5;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t local_sp_7;
    uint64_t rax_2;
    uint64_t rsi_0;
    uint64_t r13_2;
    uint64_t var_117;
    uint64_t local_sp_8;
    uint64_t local_sp_14;
    uint64_t local_sp_13;
    uint64_t r13_0;
    uint64_t local_sp_12;
    uint64_t local_sp_0;
    uint64_t r12_0;
    uint64_t local_sp_1;
    uint64_t rbp_0;
    uint64_t var_115;
    uint64_t var_116;
    uint64_t local_sp_2;
    uint64_t var_35;
    uint64_t _pre;
    uint64_t local_sp_5;
    uint64_t var_13;
    uint64_t _pre_phi;
    uint64_t local_sp_16;
    uint64_t var_12;
    uint64_t r12_8;
    uint64_t local_sp_3;
    uint64_t r14_4;
    uint64_t r12_1;
    uint64_t state_0x8558_4;
    uint64_t r14_0;
    uint64_t state_0x8560_4;
    uint64_t state_0x8558_0;
    uint64_t state_0x8560_0;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint32_t var_19;
    uint64_t var_20;
    uint32_t var_21;
    bool var_22;
    uint64_t var_23;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_36;
    uint64_t local_sp_4;
    uint64_t state_0x8558_1;
    uint64_t state_0x8560_1;
    uint64_t local_sp_15;
    uint64_t local_sp_6;
    uint64_t local_sp_17;
    uint64_t rax_0;
    uint64_t state_0x8558_2;
    uint64_t state_0x8560_2;
    uint64_t state_0x8558_3;
    uint64_t state_0x8560_3;
    uint64_t local_sp_9;
    uint64_t var_52;
    uint64_t var_57;
    uint64_t rdx1_0;
    uint64_t rcx3_0;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t *var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t rax_1;
    uint64_t rcx3_1;
    uint64_t var_53;
    uint64_t *var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_63;
    uint64_t var_69;
    uint64_t r12_2;
    uint64_t rdx1_1;
    uint64_t rcx3_2;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t *var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t rdx1_2;
    uint64_t rcx3_3;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t *var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t r14_1;
    uint64_t var_118;
    uint64_t var_119;
    uint64_t var_45;
    struct indirect_placeholder_211_ret_type var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t r12_5;
    uint64_t local_sp_10;
    uint64_t r12_6;
    uint64_t r12_3;
    uint64_t r14_2;
    uint64_t var_78;
    uint64_t local_sp_11;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t rax_3;
    uint64_t r15_0;
    uint64_t rdi2_0;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t *var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t *var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t var_100;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t r12_4;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t var_107;
    uint64_t var_108;
    uint64_t r13_1;
    uint64_t var_79;
    uint64_t var_80;
    struct indirect_placeholder_212_ret_type var_81;
    uint64_t r12_7;
    uint64_t r14_3;
    uint64_t var_109;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t var_112;
    uint64_t var_113;
    struct indirect_placeholder_210_ret_type var_114;
    uint64_t *var_120;
    uint64_t var_121;
    uint64_t state_0x8558_5;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_29;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r15();
    var_2 = init_r12();
    var_3 = init_rbx();
    var_4 = init_r13();
    var_5 = init_r14();
    var_6 = init_rbp();
    var_7 = init_state_0x8558();
    var_8 = init_state_0x8560();
    var_9 = init_rsi();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_2;
    *(uint64_t *)(var_0 + (-40L)) = var_6;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    *(uint64_t *)(var_0 + (-192L)) = rdi;
    var_10 = var_0 + (-152L);
    *(uint64_t *)(var_0 + (-168L)) = rdx;
    *(uint64_t *)(var_0 + (-160L)) = rcx;
    var_11 = var_0 + (-224L);
    *(uint64_t *)var_11 = 4251674UL;
    indirect_placeholder_8(var_10);
    r12_9 = 0UL;
    rax_2 = 0UL;
    rsi_0 = 0UL;
    r13_2 = 0UL;
    r13_0 = 0UL;
    rbp_0 = 0UL;
    var_12 = 0UL;
    local_sp_3 = var_11;
    r12_1 = 0UL;
    r14_0 = var_9;
    state_0x8558_0 = var_7;
    state_0x8560_0 = var_8;
    rdx1_0 = 0UL;
    rcx3_0 = 0UL;
    rax_1 = 0UL;
    rcx3_1 = 0UL;
    rdx1_1 = 0UL;
    rcx3_2 = 0UL;
    rdx1_2 = 0UL;
    rcx3_3 = 0UL;
    rax_3 = 0UL;
    r15_0 = 0UL;
    rdi2_0 = 0UL;
    if ((long)*(uint64_t *)(var_9 + 16UL) > (long)0UL) {
        return r12_9;
    }
    *(uint64_t *)(var_0 + (-216L)) = 0UL;
    r12_9 = 18446744073709551615UL;
    while (1U)
        {
            var_13 = var_12 << 3UL;
            *(uint64_t *)(local_sp_3 + 32UL) = var_13;
            var_14 = *(uint64_t *)(var_13 + *(uint64_t *)(r14_0 + 24UL));
            var_15 = local_sp_3 + 24UL;
            var_16 = (var_14 << 4UL) + **(uint64_t **)var_15;
            var_17 = var_16 + 8UL;
            var_18 = (uint64_t)*(unsigned char *)var_17;
            var_19 = *(uint32_t *)var_17 >> 8U;
            var_20 = (uint64_t)(uint32_t)((uint16_t)var_19 & (unsigned short)1023U);
            var_21 = (uint32_t)var_18;
            var_22 = ((uint64_t)(var_21 + (-1)) == 0UL);
            local_sp_5 = local_sp_3;
            local_sp_16 = local_sp_3;
            r12_8 = r12_1;
            r14_4 = r14_0;
            state_0x8558_4 = state_0x8558_0;
            state_0x8560_4 = state_0x8560_0;
            local_sp_4 = local_sp_3;
            state_0x8558_1 = state_0x8558_0;
            state_0x8560_1 = state_0x8560_0;
            state_0x8558_2 = state_0x8558_0;
            state_0x8560_2 = state_0x8560_0;
            state_0x8558_3 = state_0x8558_0;
            state_0x8560_3 = state_0x8560_0;
            r12_2 = r12_1;
            r14_1 = r14_0;
            r12_3 = r12_1;
            r12_7 = r12_1;
            r14_3 = r14_0;
            if (var_22) {
                var_42 = (uint64_t)*(unsigned char *)var_16;
                var_43 = local_sp_3 + 64UL;
                var_44 = local_sp_3 + (-8L);
                *(uint64_t *)var_44 = 4252517UL;
                indirect_placeholder_1(var_43, var_42);
                local_sp_6 = var_44;
            } else {
                if ((uint64_t)(var_21 + (-3)) == 0UL) {
                    var_39 = *(uint64_t *)var_16;
                    var_40 = local_sp_3 + 64UL;
                    var_41 = local_sp_3 + (-8L);
                    *(uint64_t *)var_41 = 4252653UL;
                    indirect_placeholder_39(var_40, var_39);
                    local_sp_6 = var_41;
                } else {
                    if ((uint64_t)(var_21 + (-5)) == 0UL) {
                        var_29 = *(uint64_t *)var_15;
                        if ((int)*(uint32_t *)(var_29 + 180UL) > (int)1U) {
                            var_32 = *(uint64_t *)(var_29 + 120UL);
                            var_33 = local_sp_3 + 64UL;
                            var_34 = local_sp_3 + (-8L);
                            *(uint64_t *)var_34 = 4251864UL;
                            indirect_placeholder_39(var_33, var_32);
                            local_sp_2 = var_34;
                        } else {
                            var_30 = local_sp_3 + 64UL;
                            var_31 = local_sp_3 + (-8L);
                            *(uint64_t *)var_31 = 4252803UL;
                            indirect_placeholder_8(var_30);
                            local_sp_2 = var_31;
                        }
                        var_35 = *(uint64_t *)(*(uint64_t *)(local_sp_2 + 24UL) + 216UL);
                        local_sp_4 = local_sp_2;
                        local_sp_5 = local_sp_2;
                        rax_0 = var_35;
                        if ((var_35 & 64UL) == 0UL) {
                            _pre = local_sp_2 + 64UL;
                            _pre_phi = _pre;
                            var_36 = local_sp_4 + (-8L);
                            *(uint64_t *)var_36 = 4252702UL;
                            indirect_placeholder_39(_pre_phi, 10UL);
                            state_0x8560_5 = state_0x8560_1;
                            local_sp_6 = var_36;
                            local_sp_17 = var_36;
                            state_0x8558_3 = state_0x8558_1;
                            state_0x8560_3 = state_0x8560_1;
                            state_0x8558_5 = state_0x8558_1;
                            if ((signed char)(unsigned char)*(uint64_t *)(*(uint64_t *)(local_sp_4 + 16UL) + 216UL) > '\xff') {
                                var_37 = local_sp_17 + 64UL;
                                var_38 = local_sp_17 + (-8L);
                                *(uint64_t *)var_38 = 4252734UL;
                                indirect_placeholder_39(var_37, 0UL);
                                local_sp_6 = var_38;
                                state_0x8558_3 = state_0x8558_5;
                                state_0x8560_3 = state_0x8560_5;
                            }
                        } else {
                            state_0x8560_5 = state_0x8560_2;
                            local_sp_6 = local_sp_5;
                            local_sp_17 = local_sp_5;
                            state_0x8558_3 = state_0x8558_2;
                            state_0x8560_3 = state_0x8560_2;
                            state_0x8558_5 = state_0x8558_2;
                            if ((signed char)(unsigned char)rax_0 > '\xff') {
                                var_37 = local_sp_17 + 64UL;
                                var_38 = local_sp_17 + (-8L);
                                *(uint64_t *)var_38 = 4252734UL;
                                indirect_placeholder_39(var_37, 0UL);
                                local_sp_6 = var_38;
                                state_0x8558_3 = state_0x8558_5;
                                state_0x8560_3 = state_0x8560_5;
                            }
                        }
                    } else {
                        if ((uint64_t)(var_21 + (-7)) != 0UL) {
                            var_120 = (uint64_t *)(local_sp_16 + 8UL);
                            var_121 = *var_120 + 1UL;
                            *var_120 = var_121;
                            r12_9 = r12_8;
                            var_12 = var_121;
                            local_sp_3 = local_sp_16;
                            r12_1 = r12_8;
                            r14_0 = r14_4;
                            state_0x8558_0 = state_0x8558_4;
                            state_0x8560_0 = state_0x8560_4;
                            if ((long)*(uint64_t *)(r14_4 + 16UL) > (long)var_121) {
                                continue;
                            }
                            loop_state_var = 1U;
                            break;
                        }
                        var_23 = *(uint64_t *)var_15;
                        var_24 = helper_pcmpeql_xmm_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(776UL), state_0x8558_0, state_0x8560_0);
                        var_25 = var_24.field_0;
                        var_26 = var_24.field_1;
                        var_27 = local_sp_3 + 64UL;
                        *(uint64_t *)var_27 = var_25;
                        *(uint64_t *)(local_sp_3 + 72UL) = var_26;
                        var_28 = *(uint64_t *)(var_23 + 216UL);
                        _pre_phi = var_27;
                        state_0x8558_1 = var_25;
                        state_0x8560_1 = var_26;
                        rax_0 = var_28;
                        state_0x8558_2 = var_25;
                        state_0x8560_2 = var_26;
                        if ((var_28 & 64UL) == 0UL) {
                            state_0x8560_5 = state_0x8560_2;
                            local_sp_6 = local_sp_5;
                            local_sp_17 = local_sp_5;
                            state_0x8558_3 = state_0x8558_2;
                            state_0x8560_3 = state_0x8560_2;
                            state_0x8558_5 = state_0x8558_2;
                            if ((signed char)(unsigned char)rax_0 > '\xff') {
                                var_37 = local_sp_17 + 64UL;
                                var_38 = local_sp_17 + (-8L);
                                *(uint64_t *)var_38 = 4252734UL;
                                indirect_placeholder_39(var_37, 0UL);
                                local_sp_6 = var_38;
                                state_0x8558_3 = state_0x8558_5;
                                state_0x8560_3 = state_0x8560_5;
                            }
                        } else {
                            var_36 = local_sp_4 + (-8L);
                            *(uint64_t *)var_36 = 4252702UL;
                            indirect_placeholder_39(_pre_phi, 10UL);
                            state_0x8560_5 = state_0x8560_1;
                            local_sp_6 = var_36;
                            local_sp_17 = var_36;
                            state_0x8558_3 = state_0x8558_1;
                            state_0x8560_3 = state_0x8560_1;
                            state_0x8558_5 = state_0x8558_1;
                            if ((signed char)(unsigned char)*(uint64_t *)(*(uint64_t *)(local_sp_4 + 16UL) + 216UL) > '\xff') {
                                var_37 = local_sp_17 + 64UL;
                                var_38 = local_sp_17 + (-8L);
                                *(uint64_t *)var_38 = 4252734UL;
                                indirect_placeholder_39(var_37, 0UL);
                                local_sp_6 = var_38;
                                state_0x8558_3 = state_0x8558_5;
                                state_0x8560_3 = state_0x8560_5;
                            }
                        }
                    }
                }
            }
            local_sp_7 = local_sp_6;
            local_sp_9 = local_sp_6;
            state_0x8558_4 = state_0x8558_3;
            state_0x8560_4 = state_0x8560_3;
            if ((uint64_t)((uint16_t)var_20 & (unsigned short)1023U) != 0UL) {
                if ((var_20 & 32UL) != 0UL) {
                    var_45 = local_sp_6 + 64UL;
                    *(uint64_t *)(local_sp_6 + (-8L)) = 4252599UL;
                    var_46 = indirect_placeholder_211(var_45, 10UL);
                    var_47 = var_46.field_0;
                    var_48 = local_sp_6 + 56UL;
                    var_49 = local_sp_6 + (-16L);
                    *(uint64_t *)var_49 = 4252609UL;
                    indirect_placeholder_8(var_48);
                    local_sp_16 = var_49;
                    if ((uint64_t)(unsigned char)var_47 != 0UL) {
                        var_120 = (uint64_t *)(local_sp_16 + 8UL);
                        var_121 = *var_120 + 1UL;
                        *var_120 = var_121;
                        r12_9 = r12_8;
                        var_12 = var_121;
                        local_sp_3 = local_sp_16;
                        r12_1 = r12_8;
                        r14_0 = r14_4;
                        state_0x8558_0 = state_0x8558_4;
                        state_0x8560_0 = state_0x8560_4;
                        if ((long)*(uint64_t *)(r14_4 + 16UL) > (long)var_121) {
                            continue;
                        }
                        loop_state_var = 1U;
                        break;
                    }
                    var_50 = local_sp_6 + 48UL;
                    var_51 = local_sp_6 + (-24L);
                    *(uint64_t *)var_51 = 4252632UL;
                    indirect_placeholder_1(var_50, 10UL);
                    local_sp_7 = var_51;
                }
                local_sp_8 = local_sp_7;
                local_sp_9 = local_sp_7;
                local_sp_16 = local_sp_7;
                if ((signed char)(unsigned char)var_19 <= '\xff') {
                    var_118 = local_sp_8 + 64UL;
                    var_119 = local_sp_8 + (-8L);
                    *(uint64_t *)var_119 = 4252496UL;
                    indirect_placeholder_8(var_118);
                    local_sp_16 = var_119;
                    r12_8 = r12_2;
                    r14_4 = r14_1;
                    var_120 = (uint64_t *)(local_sp_16 + 8UL);
                    var_121 = *var_120 + 1UL;
                    *var_120 = var_121;
                    r12_9 = r12_8;
                    var_12 = var_121;
                    local_sp_3 = local_sp_16;
                    r12_1 = r12_8;
                    r14_0 = r14_4;
                    state_0x8558_0 = state_0x8558_4;
                    state_0x8560_0 = state_0x8560_4;
                    if ((long)*(uint64_t *)(r14_4 + 16UL) > (long)var_121) {
                        continue;
                    }
                    loop_state_var = 1U;
                    break;
                }
                if ((var_20 & 4UL) != 0UL) {
                    if (!var_22) {
                        if ((*(unsigned char *)(var_16 + 10UL) & '@') != '\x00') {
                            var_118 = local_sp_8 + 64UL;
                            var_119 = local_sp_8 + (-8L);
                            *(uint64_t *)var_119 = 4252496UL;
                            indirect_placeholder_8(var_118);
                            local_sp_16 = var_119;
                            r12_8 = r12_2;
                            r14_4 = r14_1;
                            var_120 = (uint64_t *)(local_sp_16 + 8UL);
                            var_121 = *var_120 + 1UL;
                            *var_120 = var_121;
                            r12_9 = r12_8;
                            var_12 = var_121;
                            local_sp_3 = local_sp_16;
                            r12_1 = r12_8;
                            r14_0 = r14_4;
                            state_0x8558_0 = state_0x8558_4;
                            state_0x8560_0 = state_0x8560_4;
                            if ((long)*(uint64_t *)(r14_4 + 16UL) > (long)var_121) {
                                continue;
                            }
                            loop_state_var = 1U;
                            break;
                        }
                    }
                    var_52 = *(uint64_t *)(local_sp_7 + 24UL);
                    if ((int)*(uint32_t *)(var_52 + 180UL) <= (int)1U) {
                        var_53 = rax_1 << 3UL;
                        var_54 = (uint64_t *)((var_53 + local_sp_7) + 64UL);
                        var_55 = *var_54 & *(uint64_t *)((var_53 + var_52) + 184UL);
                        *var_54 = var_55;
                        var_56 = rcx3_1 | var_55;
                        rcx3_1 = var_56;
                        while (rax_1 != 3UL)
                            {
                                rax_1 = rax_1 + 1UL;
                                var_53 = rax_1 << 3UL;
                                var_54 = (uint64_t *)((var_53 + local_sp_7) + 64UL);
                                var_55 = *var_54 & *(uint64_t *)((var_53 + var_52) + 184UL);
                                *var_54 = var_55;
                                var_56 = rcx3_1 | var_55;
                                rcx3_1 = var_56;
                            }
                        if (var_56 != 0UL) {
                            var_120 = (uint64_t *)(local_sp_16 + 8UL);
                            var_121 = *var_120 + 1UL;
                            *var_120 = var_121;
                            r12_9 = r12_8;
                            var_12 = var_121;
                            local_sp_3 = local_sp_16;
                            r12_1 = r12_8;
                            r14_0 = r14_4;
                            state_0x8558_0 = state_0x8558_4;
                            state_0x8560_0 = state_0x8560_4;
                            if ((long)*(uint64_t *)(r14_4 + 16UL) > (long)var_121) {
                                continue;
                            }
                            loop_state_var = 1U;
                            break;
                        }
                    }
                    var_57 = *(uint64_t *)(var_52 + 120UL);
                    var_58 = rdx1_0 << 3UL;
                    var_59 = *(uint64_t *)((var_58 + var_52) + 184UL) | (*(uint64_t *)(var_58 + var_57) ^ (-1L));
                    var_60 = (uint64_t *)((var_58 + local_sp_7) + 64UL);
                    var_61 = var_59 & *var_60;
                    *var_60 = var_61;
                    var_62 = rcx3_0 | var_61;
                    rcx3_0 = var_62;
                    while (rdx1_0 != 3UL)
                        {
                            rdx1_0 = rdx1_0 + 1UL;
                            var_58 = rdx1_0 << 3UL;
                            var_59 = *(uint64_t *)((var_58 + var_52) + 184UL) | (*(uint64_t *)(var_58 + var_57) ^ (-1L));
                            var_60 = (uint64_t *)((var_58 + local_sp_7) + 64UL);
                            var_61 = var_59 & *var_60;
                            *var_60 = var_61;
                            var_62 = rcx3_0 | var_61;
                            rcx3_0 = var_62;
                        }
                    if (var_62 != 0UL) {
                        var_120 = (uint64_t *)(local_sp_16 + 8UL);
                        var_121 = *var_120 + 1UL;
                        *var_120 = var_121;
                        r12_9 = r12_8;
                        var_12 = var_121;
                        local_sp_3 = local_sp_16;
                        r12_1 = r12_8;
                        r14_0 = r14_4;
                        state_0x8558_0 = state_0x8558_4;
                        state_0x8560_0 = state_0x8560_4;
                        if ((long)*(uint64_t *)(r14_4 + 16UL) > (long)var_121) {
                            continue;
                        }
                        loop_state_var = 1U;
                        break;
                    }
                }
                if ((var_20 & 8UL) != 0UL) {
                    if (!var_22) {
                        if ((*(unsigned char *)(var_16 + 10UL) & '@') != '\x00') {
                            var_118 = local_sp_8 + 64UL;
                            var_119 = local_sp_8 + (-8L);
                            *(uint64_t *)var_119 = 4252496UL;
                            indirect_placeholder_8(var_118);
                            local_sp_16 = var_119;
                            r12_8 = r12_2;
                            r14_4 = r14_1;
                            var_120 = (uint64_t *)(local_sp_16 + 8UL);
                            var_121 = *var_120 + 1UL;
                            *var_120 = var_121;
                            r12_9 = r12_8;
                            var_12 = var_121;
                            local_sp_3 = local_sp_16;
                            r12_1 = r12_8;
                            r14_0 = r14_4;
                            state_0x8558_0 = state_0x8558_4;
                            state_0x8560_0 = state_0x8560_4;
                            if ((long)*(uint64_t *)(r14_4 + 16UL) > (long)var_121) {
                                continue;
                            }
                            loop_state_var = 1U;
                            break;
                        }
                    }
                    var_63 = *(uint64_t *)(local_sp_7 + 24UL);
                    if ((int)*(uint32_t *)(var_63 + 180UL) <= (int)1U) {
                        var_64 = rdx1_2 << 3UL;
                        var_65 = *(uint64_t *)((var_64 + var_63) + 184UL) ^ (-1L);
                        var_66 = (uint64_t *)((var_64 + local_sp_7) + 64UL);
                        var_67 = *var_66 & var_65;
                        *var_66 = var_67;
                        var_68 = rcx3_3 | var_67;
                        rcx3_3 = var_68;
                        while (rdx1_2 != 3UL)
                            {
                                rdx1_2 = rdx1_2 + 1UL;
                                var_64 = rdx1_2 << 3UL;
                                var_65 = *(uint64_t *)((var_64 + var_63) + 184UL) ^ (-1L);
                                var_66 = (uint64_t *)((var_64 + local_sp_7) + 64UL);
                                var_67 = *var_66 & var_65;
                                *var_66 = var_67;
                                var_68 = rcx3_3 | var_67;
                                rcx3_3 = var_68;
                            }
                        if (var_68 != 0UL) {
                            var_120 = (uint64_t *)(local_sp_16 + 8UL);
                            var_121 = *var_120 + 1UL;
                            *var_120 = var_121;
                            r12_9 = r12_8;
                            var_12 = var_121;
                            local_sp_3 = local_sp_16;
                            r12_1 = r12_8;
                            r14_0 = r14_4;
                            state_0x8558_0 = state_0x8558_4;
                            state_0x8560_0 = state_0x8560_4;
                            if ((long)*(uint64_t *)(r14_4 + 16UL) > (long)var_121) {
                                continue;
                            }
                            loop_state_var = 1U;
                            break;
                        }
                    }
                    var_69 = *(uint64_t *)(var_63 + 120UL);
                    var_70 = rdx1_1 << 3UL;
                    var_71 = (*(uint64_t *)((var_70 + var_63) + 184UL) & *(uint64_t *)(var_70 + var_69)) ^ (-1L);
                    var_72 = (uint64_t *)((var_70 + local_sp_7) + 64UL);
                    var_73 = *var_72 & var_71;
                    *var_72 = var_73;
                    var_74 = rcx3_2 | var_73;
                    rcx3_2 = var_74;
                    while (rdx1_1 != 3UL)
                        {
                            rdx1_1 = rdx1_1 + 1UL;
                            var_70 = rdx1_1 << 3UL;
                            var_71 = (*(uint64_t *)((var_70 + var_63) + 184UL) & *(uint64_t *)(var_70 + var_69)) ^ (-1L);
                            var_72 = (uint64_t *)((var_70 + local_sp_7) + 64UL);
                            var_73 = *var_72 & var_71;
                            *var_72 = var_73;
                            var_74 = rcx3_2 | var_73;
                            rcx3_2 = var_74;
                        }
                    if (var_74 != 0UL) {
                        var_120 = (uint64_t *)(local_sp_16 + 8UL);
                        var_121 = *var_120 + 1UL;
                        *var_120 = var_121;
                        r12_9 = r12_8;
                        var_12 = var_121;
                        local_sp_3 = local_sp_16;
                        r12_1 = r12_8;
                        r14_0 = r14_4;
                        state_0x8558_0 = state_0x8558_4;
                        state_0x8560_0 = state_0x8560_4;
                        if ((long)*(uint64_t *)(r14_4 + 16UL) > (long)var_121) {
                            continue;
                        }
                        loop_state_var = 1U;
                        break;
                    }
                }
            }
            var_75 = *(uint64_t *)(local_sp_9 + 48UL);
            var_76 = helper_cc_compute_all_wrapper(r12_1, 0UL, 0UL, 25U);
            local_sp_10 = local_sp_9;
            r14_2 = var_75;
            local_sp_15 = local_sp_9;
            if ((uint64_t)(((unsigned char)(var_76 >> 4UL) ^ (unsigned char)var_76) & '\xc0') == 0UL) {
                r12_0 = r12_7;
                local_sp_16 = local_sp_15;
                r12_8 = r12_7;
                r14_4 = r14_3;
                r14_1 = r14_3;
                if (r12_7 != r13_2) {
                    var_109 = local_sp_15 + 64UL;
                    var_110 = (r12_7 << 5UL) + *(uint64_t *)(local_sp_15 + 56UL);
                    *(uint64_t *)(local_sp_15 + (-8L)) = 4252447UL;
                    indirect_placeholder_39(var_110, var_109);
                    var_111 = *(uint64_t *)(*(uint64_t *)(local_sp_15 + 24UL) + *(uint64_t *)(r14_3 + 24UL));
                    var_112 = (r12_7 * 24UL) + *(uint64_t *)(local_sp_15 + 40UL);
                    var_113 = local_sp_15 + (-16L);
                    *(uint64_t *)var_113 = 4252478UL;
                    var_114 = indirect_placeholder_210(var_112, var_111);
                    local_sp_0 = var_113;
                    local_sp_8 = var_113;
                    if ((uint64_t)(uint32_t)var_114.field_0 != 0UL) {
                        if (r12_7 != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        loop_state_var = 0U;
                        break;
                    }
                    var_117 = r12_7 + 1UL;
                    r12_2 = var_117;
                    var_118 = local_sp_8 + 64UL;
                    var_119 = local_sp_8 + (-8L);
                    *(uint64_t *)var_119 = 4252496UL;
                    indirect_placeholder_8(var_118);
                    local_sp_16 = var_119;
                    r12_8 = r12_2;
                    r14_4 = r14_1;
                }
                var_120 = (uint64_t *)(local_sp_16 + 8UL);
                var_121 = *var_120 + 1UL;
                *var_120 = var_121;
                r12_9 = r12_8;
                var_12 = var_121;
                local_sp_3 = local_sp_16;
                r12_1 = r12_8;
                r14_0 = r14_4;
                state_0x8558_0 = state_0x8558_4;
                state_0x8560_0 = state_0x8560_4;
                if ((long)*(uint64_t *)(r14_4 + 16UL) > (long)var_121) {
                    continue;
                }
                loop_state_var = 1U;
                break;
            }
            *(uint64_t *)(local_sp_9 + 16UL) = var_16;
            var_77 = *(uint64_t *)(local_sp_9 + 56UL);
            *(uint64_t *)(local_sp_9 + 40UL) = r14_0;
            while (1U)
                {
                    var_78 = (r13_0 << 5UL) + var_77;
                    r12_0 = r12_3;
                    r12_5 = r12_3;
                    local_sp_11 = local_sp_10;
                    r12_4 = r12_3;
                    r13_1 = r13_0;
                    if (!var_22) {
                        var_79 = (uint64_t)**(unsigned char **)(local_sp_10 + 16UL);
                        var_80 = local_sp_10 + (-8L);
                        *(uint64_t *)var_80 = 4252160UL;
                        var_81 = indirect_placeholder_212(var_78, var_79);
                        local_sp_11 = var_80;
                        local_sp_13 = var_80;
                        if ((uint64_t)(unsigned char)var_81.field_0 != 0UL) {
                            var_108 = r13_0 + 1UL;
                            local_sp_14 = local_sp_13;
                            r13_0 = var_108;
                            local_sp_10 = local_sp_13;
                            r12_6 = r12_5;
                            r12_3 = r12_5;
                            r13_1 = var_108;
                            if ((long)r12_5 <= (long)var_108) {
                                loop_state_var = 1U;
                                break;
                            }
                            r14_2 = r14_2 + 24UL;
                            continue;
                        }
                    }
                    local_sp_12 = local_sp_11;
                    local_sp_13 = local_sp_11;
                    var_82 = rax_2 << 3UL;
                    var_83 = var_82 + local_sp_11;
                    var_84 = *(uint64_t *)(var_83 + 64UL) & *(uint64_t *)(var_82 + var_78);
                    *(uint64_t *)(var_83 + 96UL) = var_84;
                    var_85 = rsi_0 | var_84;
                    rsi_0 = var_85;
                    while (rax_2 != 3UL)
                        {
                            rax_2 = rax_2 + 1UL;
                            var_82 = rax_2 << 3UL;
                            var_83 = var_82 + local_sp_11;
                            var_84 = *(uint64_t *)(var_83 + 64UL) & *(uint64_t *)(var_82 + var_78);
                            *(uint64_t *)(var_83 + 96UL) = var_84;
                            var_85 = rsi_0 | var_84;
                            rsi_0 = var_85;
                        }
                    if (var_85 == 0UL) {
                        var_108 = r13_0 + 1UL;
                        local_sp_14 = local_sp_13;
                        r13_0 = var_108;
                        local_sp_10 = local_sp_13;
                        r12_6 = r12_5;
                        r12_3 = r12_5;
                        r13_1 = var_108;
                        if ((long)r12_5 <= (long)var_108) {
                            loop_state_var = 1U;
                            break;
                        }
                        r14_2 = r14_2 + 24UL;
                        continue;
                    }
                    var_86 = rax_3 << 3UL;
                    var_87 = var_86 + local_sp_11;
                    var_88 = (uint64_t *)(var_87 + 64UL);
                    var_89 = *var_88;
                    var_90 = var_89 ^ (-1L);
                    var_91 = (uint64_t *)(var_86 + var_78);
                    var_92 = *var_91 & var_90;
                    *(uint64_t *)(var_87 + 128UL) = var_92;
                    var_93 = *var_91;
                    var_94 = rdi2_0 | var_92;
                    var_95 = var_89 & (var_93 ^ (-1L));
                    *var_88 = var_95;
                    var_96 = r15_0 | var_95;
                    r15_0 = var_96;
                    rdi2_0 = var_94;
                    while (rax_3 != 3UL)
                        {
                            rax_3 = rax_3 + 1UL;
                            var_86 = rax_3 << 3UL;
                            var_87 = var_86 + local_sp_11;
                            var_88 = (uint64_t *)(var_87 + 64UL);
                            var_89 = *var_88;
                            var_90 = var_89 ^ (-1L);
                            var_91 = (uint64_t *)(var_86 + var_78);
                            var_92 = *var_91 & var_90;
                            *(uint64_t *)(var_87 + 128UL) = var_92;
                            var_93 = *var_91;
                            var_94 = rdi2_0 | var_92;
                            var_95 = var_89 & (var_93 ^ (-1L));
                            *var_88 = var_95;
                            var_96 = r15_0 | var_95;
                            r15_0 = var_96;
                            rdi2_0 = var_94;
                        }
                    if (var_94 != 0UL) {
                        var_97 = local_sp_11 + 128UL;
                        var_98 = (r12_3 << 5UL) + var_77;
                        *(uint64_t *)(local_sp_11 + (-8L)) = 4252321UL;
                        indirect_placeholder_39(var_98, var_97);
                        var_99 = local_sp_11 + 88UL;
                        *(uint64_t *)(local_sp_11 + (-16L)) = 4252334UL;
                        indirect_placeholder_39(var_78, var_99);
                        var_100 = r14_2 + 16UL;
                        var_101 = r14_2 + 8UL;
                        var_102 = (r12_3 * 24UL) + *(uint64_t *)(local_sp_11 + 32UL);
                        var_103 = local_sp_11 + (-24L);
                        *(uint64_t *)var_103 = 4252360UL;
                        var_104 = indirect_placeholder_7(var_100, var_102, var_101);
                        local_sp_0 = var_103;
                        local_sp_12 = var_103;
                        if ((uint64_t)(uint32_t)var_104 != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        r12_4 = r12_3 + 1UL;
                    }
                    var_105 = *(uint64_t *)(*(uint64_t *)(local_sp_12 + 32UL) + *(uint64_t *)(*(uint64_t *)(local_sp_12 + 40UL) + 24UL));
                    var_106 = local_sp_12 + (-8L);
                    *(uint64_t *)var_106 = 4252398UL;
                    var_107 = indirect_placeholder_1(r14_2, var_105);
                    local_sp_14 = var_106;
                    local_sp_13 = var_106;
                    local_sp_0 = var_106;
                    r12_0 = r12_4;
                    r12_5 = r12_4;
                    r12_6 = r12_4;
                    if ((uint64_t)(unsigned char)var_107 != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    if (var_96 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    local_sp_15 = local_sp_14;
                    r12_7 = r12_6;
                    r13_2 = r13_1;
                    r14_3 = *(uint64_t *)(local_sp_14 + 40UL);
                }
                break;
              case 0U:
                {
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    local_sp_1 = local_sp_0;
    var_115 = rbp_0 + 1UL;
    var_116 = local_sp_1 + (-8L);
    *(uint64_t *)var_116 = 4252560UL;
    indirect_placeholder();
    local_sp_1 = var_116;
    rbp_0 = var_115;
    do {
        var_115 = rbp_0 + 1UL;
        var_116 = local_sp_1 + (-8L);
        *(uint64_t *)var_116 = 4252560UL;
        indirect_placeholder();
        local_sp_1 = var_116;
        rbp_0 = var_115;
    } while ((long)r12_0 <= (long)var_115);
}
