typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_165_ret_type;
struct indirect_placeholder_166_ret_type;
struct indirect_placeholder_165_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_166_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rax(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r15(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct indirect_placeholder_165_ret_type indirect_placeholder_165(uint64_t param_0);
extern struct indirect_placeholder_166_ret_type indirect_placeholder_166(uint64_t param_0);
uint64_t bb_re_copy_regs(uint64_t rdx, uint64_t rdi, uint64_t rcx, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint32_t var_11;
    uint64_t var_30;
    uint64_t *_pre_phi73;
    uint64_t rbx_0;
    uint64_t r13_4;
    uint64_t *_pre68;
    struct indirect_placeholder_165_ret_type var_27;
    uint64_t var_28;
    uint64_t *var_29;
    uint64_t *var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t r13_0;
    uint64_t local_sp_0;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t *var_14;
    uint64_t var_15;
    uint64_t *var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_22;
    uint64_t *_pre_phi69;
    uint64_t r13_1;
    uint64_t *_pre72;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t rax_0;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t r13_3;
    uint64_t *var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t rbx_1;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_23;
    struct indirect_placeholder_166_ret_type var_24;
    uint64_t var_25;
    uint64_t *var_26;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_r15();
    var_3 = init_r12();
    var_4 = init_rbx();
    var_5 = init_r13();
    var_6 = init_r14();
    var_7 = init_rbp();
    var_8 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    var_9 = rdx + 1UL;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_3;
    *(uint64_t *)(var_0 + (-40L)) = var_7;
    *(uint64_t *)(var_0 + (-48L)) = var_4;
    var_10 = var_0 + (-72L);
    var_11 = (uint32_t)rcx;
    rbx_0 = 0UL;
    r13_4 = 0UL;
    r13_0 = 1UL;
    local_sp_0 = var_10;
    r13_1 = 1UL;
    rax_0 = 0UL;
    r13_3 = 1UL;
    if ((uint64_t)var_11 == 0UL) {
        var_23 = var_9 << 3UL;
        *(uint64_t *)(var_0 + (-80L)) = 4242352UL;
        var_24 = indirect_placeholder_166(var_23);
        var_25 = var_24.field_0;
        var_26 = (uint64_t *)(rdi + 8UL);
        *var_26 = var_25;
        _pre_phi69 = var_26;
        if (var_25 == 0UL) {
            return (uint64_t)(uint32_t)r13_4;
        }
        *(uint64_t *)(var_0 + (-88L)) = 4242370UL;
        var_27 = indirect_placeholder_165(var_23);
        var_28 = var_27.field_0;
        var_29 = (uint64_t *)(rdi + 16UL);
        *var_29 = var_28;
        _pre_phi73 = var_29;
        if (var_28 != 0UL) {
            *(uint64_t *)(var_0 + (-96L)) = 4242456UL;
            indirect_placeholder();
            return (uint64_t)(uint32_t)r13_4;
        }
        *(uint64_t *)rdi = var_9;
        var_30 = helper_cc_compute_all_wrapper(rdx, 0UL, 0UL, 25U);
        if ((uint64_t)(((unsigned char)(var_30 >> 4UL) ^ (unsigned char)var_30) & '\xc0') != 0UL) {
            var_31 = *_pre_phi69;
            var_32 = *_pre_phi73;
            var_33 = rdx << 3UL;
            rbx_0 = rdx;
            r13_3 = r13_1;
            var_34 = (rax_0 << 1UL) + rsi;
            *(uint64_t *)(rax_0 + var_31) = *(uint64_t *)var_34;
            *(uint64_t *)(rax_0 + var_32) = *(uint64_t *)(var_34 + 8UL);
            var_35 = rax_0 + 8UL;
            rax_0 = var_35;
            do {
                var_34 = (rax_0 << 1UL) + rsi;
                *(uint64_t *)(rax_0 + var_31) = *(uint64_t *)var_34;
                *(uint64_t *)(rax_0 + var_32) = *(uint64_t *)(var_34 + 8UL);
                var_35 = rax_0 + 8UL;
                rax_0 = var_35;
            } while (var_33 != var_35);
        }
    } else {
        if ((uint64_t)(var_11 + (-1)) == 0UL) {
            var_14 = (uint64_t *)rdi;
            var_15 = helper_cc_compute_c_wrapper(*var_14 - var_9, var_9, var_8, 17U);
            if (var_15 != 0UL) {
                var_16 = (uint64_t *)(rdi + 8UL);
                var_17 = *var_16;
                var_18 = var_9 << 3UL;
                *(uint64_t *)(var_0 + (-64L)) = var_18;
                *(uint64_t *)(var_0 + (-80L)) = 4242277UL;
                indirect_placeholder_1(var_17, var_18);
                if (var_1 != 0UL) {
                    return (uint64_t)(uint32_t)r13_4;
                }
                var_19 = (uint64_t *)(rdi + 16UL);
                var_20 = *var_19;
                var_21 = *(uint64_t *)var_10;
                *(uint64_t *)(var_0 + (-88L)) = 4242304UL;
                indirect_placeholder_1(var_20, var_21);
                *var_16 = var_1;
                *var_19 = var_1;
                *var_14 = var_9;
            }
        } else {
            r13_0 = 2UL;
            if ((uint64_t)(var_11 + (-2)) == 0UL) {
                var_12 = var_0 + (-80L);
                *(uint64_t *)var_12 = 4242090UL;
                indirect_placeholder();
                local_sp_0 = var_12;
            }
            var_13 = helper_cc_compute_c_wrapper(*(uint64_t *)rdi - rdx, rdx, var_8, 17U);
            if (var_13 == 0UL) {
                *(uint64_t *)(local_sp_0 + (-8L)) = 4242441UL;
                indirect_placeholder();
            }
        }
        var_22 = helper_cc_compute_all_wrapper(rdx, 0UL, 0UL, 25U);
        r13_1 = r13_0;
        r13_3 = r13_0;
        if ((uint64_t)(((unsigned char)(var_22 >> 4UL) ^ (unsigned char)var_22) & '\xc0') != 0UL) {
            _pre68 = (uint64_t *)(rdi + 8UL);
            _pre72 = (uint64_t *)(rdi + 16UL);
            _pre_phi73 = _pre72;
            _pre_phi69 = _pre68;
            var_31 = *_pre_phi69;
            var_32 = *_pre_phi73;
            var_33 = rdx << 3UL;
            rbx_0 = rdx;
            r13_3 = r13_1;
            var_34 = (rax_0 << 1UL) + rsi;
            *(uint64_t *)(rax_0 + var_31) = *(uint64_t *)var_34;
            *(uint64_t *)(rax_0 + var_32) = *(uint64_t *)(var_34 + 8UL);
            var_35 = rax_0 + 8UL;
            rax_0 = var_35;
            do {
                var_34 = (rax_0 << 1UL) + rsi;
                *(uint64_t *)(rax_0 + var_31) = *(uint64_t *)var_34;
                *(uint64_t *)(rax_0 + var_32) = *(uint64_t *)(var_34 + 8UL);
                var_35 = rax_0 + 8UL;
                rax_0 = var_35;
            } while (var_33 != var_35);
        }
    }
    var_36 = (uint64_t *)rdi;
    rbx_1 = rbx_0;
    r13_4 = r13_3;
    if (*var_36 > rbx_0) {
        return (uint64_t)(uint32_t)r13_4;
    }
    var_37 = *(uint64_t *)(rdi + 16UL);
    var_38 = *(uint64_t *)(rdi + 8UL);
    var_39 = rbx_1 + 1UL;
    var_40 = rbx_1 << 3UL;
    *(uint64_t *)(var_40 + var_37) = 18446744073709551615UL;
    *(uint64_t *)(var_40 + var_38) = 18446744073709551615UL;
    rbx_1 = var_39;
    do {
        var_39 = rbx_1 + 1UL;
        var_40 = rbx_1 << 3UL;
        *(uint64_t *)(var_40 + var_37) = 18446744073709551615UL;
        *(uint64_t *)(var_40 + var_38) = 18446744073709551615UL;
        rbx_1 = var_39;
    } while (*var_36 <= var_39);
    return (uint64_t)(uint32_t)r13_4;
}
