typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct helper_pxor_xmm_wrapper_328_ret_type;
struct type_3;
struct type_5;
struct indirect_placeholder_239_ret_type;
struct helper_pxor_xmm_wrapper_328_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_3 {
};
struct type_5 {
};
struct indirect_placeholder_239_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r15(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t init_state_0x8558(void);
extern uint64_t init_state_0x8560(void);
extern uint64_t indirect_placeholder_14(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct helper_pxor_xmm_wrapper_328_ret_type helper_pxor_xmm_wrapper_328(struct type_3 *param_0, struct type_5 *param_1, struct type_5 *param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_239_ret_type indirect_placeholder_239(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
uint64_t bb_check_arrival_add_next_nodes_isra_0(uint64_t r8, uint64_t rdx, uint64_t rdi, uint64_t rcx, uint64_t rsi) {
    struct indirect_placeholder_239_ret_type var_40;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    struct helper_pxor_xmm_wrapper_328_ret_type var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t local_sp_8;
    uint64_t r12_1;
    uint64_t local_sp_0;
    uint64_t rbx_0;
    uint64_t rdx2_0;
    uint64_t *var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t *var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t *var_57;
    uint64_t var_58;
    uint64_t rbp_3;
    uint64_t local_sp_6;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t rbx_3;
    uint64_t var_37;
    uint64_t r12_0;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint32_t *var_46;
    uint32_t var_47;
    uint64_t r81_0;
    uint64_t rbp_0;
    uint64_t rcx4_0;
    uint64_t local_sp_1;
    uint64_t *var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t local_sp_2;
    uint64_t local_sp_3;
    uint64_t local_sp_7;
    uint64_t var_29;
    uint64_t rbx_1;
    uint64_t rbp_1;
    uint64_t rcx4_1;
    uint64_t local_sp_4;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t r12_2;
    uint64_t rbx_2;
    uint64_t rbp_2;
    uint64_t local_sp_5;
    uint64_t *var_65;
    uint64_t var_66;
    uint64_t r12_3;
    uint64_t var_15;
    uint64_t *var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t rsi5_0;
    uint64_t *var_21;
    uint64_t var_22;
    uint64_t *var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t *var_26;
    uint64_t var_27;
    uint32_t var_28;
    uint64_t var_59;
    uint64_t var_60;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r15();
    var_2 = init_r12();
    var_3 = init_rbx();
    var_4 = init_r13();
    var_5 = init_r14();
    var_6 = init_rbp();
    var_7 = init_state_0x8558();
    var_8 = init_state_0x8560();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_9 = helper_pxor_xmm_wrapper_328((struct type_3 *)(0UL), (struct type_5 *)(776UL), (struct type_5 *)(776UL), var_7, var_8);
    var_10 = var_9.field_0;
    var_11 = var_9.field_1;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_2;
    *(uint64_t *)(var_0 + (-40L)) = var_6;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_12 = var_0 + (-152L);
    var_13 = *(uint64_t *)rdx;
    var_14 = *(uint64_t *)(rdi + 152UL);
    *(uint64_t *)var_12 = rdx;
    *(uint64_t *)(var_0 + (-144L)) = r8;
    *(uint32_t *)(var_0 + (-92L)) = 0U;
    *(uint64_t *)(var_0 + (-72L)) = 0UL;
    *(uint64_t *)(var_0 + (-88L)) = var_10;
    *(uint64_t *)(var_0 + (-80L)) = var_11;
    local_sp_8 = var_12;
    rbp_3 = var_14;
    local_sp_6 = var_12;
    rbx_3 = 0UL;
    r12_3 = rdi;
    if ((long)var_13 > (long)0UL) {
        *(uint64_t *)(local_sp_8 + (-8L)) = 4263226UL;
        indirect_placeholder();
        return 0UL;
    }
    *(uint64_t *)(var_0 + (-128L)) = (var_14 + 216UL);
    while (1U)
        {
            var_15 = *(uint64_t *)rcx;
            var_16 = (uint64_t *)rbp_3;
            var_17 = *var_16;
            var_18 = *(uint64_t *)((rbx_3 << 3UL) + var_15);
            var_19 = var_18 << 4UL;
            var_20 = var_19 + var_17;
            r12_1 = r12_3;
            rbx_0 = rbx_3;
            r12_0 = r12_3;
            rbp_0 = rbp_3;
            local_sp_7 = local_sp_6;
            rbx_1 = rbx_3;
            rbp_1 = rbp_3;
            r12_2 = r12_3;
            rbx_2 = rbx_3;
            rbp_2 = rbp_3;
            rsi5_0 = var_20;
            if ((*(unsigned char *)(var_20 + 10UL) & '\x10') != '\x00') {
                var_59 = local_sp_7 + (-8L);
                *(uint64_t *)var_59 = 4262763UL;
                var_60 = indirect_placeholder_7(rsi, r12_3, rsi5_0);
                local_sp_3 = var_59;
                local_sp_5 = var_59;
                if ((uint64_t)(unsigned char)var_60 == 0UL) {
                    var_65 = *(uint64_t **)local_sp_5;
                    var_66 = rbx_2 + 1UL;
                    local_sp_8 = local_sp_5;
                    rbp_3 = rbp_2;
                    local_sp_6 = local_sp_5;
                    rbx_3 = var_66;
                    r12_3 = r12_2;
                    if ((long)var_66 < (long)*var_65) {
                        continue;
                    }
                    loop_state_var = 1U;
                    break;
                }
                rcx4_1 = var_18 << 3UL;
                local_sp_4 = local_sp_3;
                var_61 = *(uint64_t *)(local_sp_4 + 8UL);
                var_62 = *(uint64_t *)(rcx4_1 + *(uint64_t *)(rbp_1 + 24UL));
                var_63 = local_sp_4 + (-8L);
                *(uint64_t *)var_63 = 4262793UL;
                var_64 = indirect_placeholder_1(var_61, var_62);
                local_sp_2 = var_63;
                r12_2 = r12_1;
                rbx_2 = rbx_1;
                rbp_2 = rbp_1;
                local_sp_5 = var_63;
                if ((uint64_t)(unsigned char)var_64 != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                var_65 = *(uint64_t **)local_sp_5;
                var_66 = rbx_2 + 1UL;
                local_sp_8 = local_sp_5;
                rbp_3 = rbp_2;
                local_sp_6 = local_sp_5;
                rbx_3 = var_66;
                r12_3 = r12_2;
                if ((long)var_66 < (long)*var_65) {
                    continue;
                }
                loop_state_var = 1U;
                break;
            }
            var_21 = (uint64_t *)(local_sp_6 + 24UL);
            var_22 = *var_21;
            var_23 = (uint64_t *)(local_sp_6 + 16UL);
            *var_23 = var_19;
            var_24 = local_sp_6 + (-8L);
            *(uint64_t *)var_24 = 4262870UL;
            var_25 = indirect_placeholder_14(rsi, var_18, var_17, r12_3, var_22);
            var_26 = (uint64_t *)(local_sp_6 + 8UL);
            var_27 = *var_26;
            var_28 = (uint32_t)var_25;
            local_sp_1 = var_24;
            local_sp_3 = var_24;
            local_sp_7 = var_24;
            if ((int)var_28 > (int)1U) {
                var_30 = *(uint64_t *)(rbp_3 + 24UL);
                var_31 = (uint64_t)((long)(var_25 << 32UL) >> (long)32UL);
                var_32 = var_18 << 3UL;
                var_33 = *(uint64_t *)(var_32 + var_30);
                var_34 = var_31 + rsi;
                var_35 = local_sp_6 + 56UL;
                var_36 = var_34 << 3UL;
                *var_26 = var_36;
                var_37 = *(uint64_t *)(var_36 + *(uint64_t *)(r12_3 + 184UL));
                *(uint64_t *)(local_sp_6 + 64UL) = 0UL;
                r81_0 = var_33;
                rdx2_0 = var_35;
                rcx4_0 = var_32;
                if (var_37 != 0UL) {
                    var_38 = var_37 + 8UL;
                    *(uint64_t *)(local_sp_6 + 32UL) = var_32;
                    *var_21 = var_33;
                    *var_26 = var_35;
                    var_39 = local_sp_6 + (-16L);
                    *(uint64_t *)var_39 = 4262982UL;
                    var_40 = indirect_placeholder_239(r12_3, rbx_3, rbp_3, var_35, var_38);
                    var_41 = var_40.field_1;
                    var_42 = var_40.field_2;
                    var_43 = var_40.field_3;
                    var_44 = *(uint64_t *)local_sp_6;
                    var_45 = *var_23;
                    var_46 = (uint32_t *)(local_sp_6 + 44UL);
                    var_47 = (uint32_t)var_40.field_0;
                    *var_46 = var_47;
                    local_sp_0 = var_39;
                    rbx_0 = var_42;
                    rdx2_0 = var_44;
                    r12_0 = var_41;
                    r81_0 = var_45;
                    rbp_0 = var_43;
                    rcx4_0 = *var_21;
                    local_sp_1 = var_39;
                    if ((uint64_t)var_47 != 0UL) {
                        loop_state_var = 2U;
                        break;
                    }
                }
                *(uint64_t *)(local_sp_1 + 32UL) = rcx4_0;
                var_48 = (uint64_t *)(local_sp_1 + 16UL);
                *var_48 = rdx2_0;
                var_49 = local_sp_1 + (-8L);
                *(uint64_t *)var_49 = 4263026UL;
                var_50 = indirect_placeholder_1(rdx2_0, r81_0);
                local_sp_2 = var_49;
                r12_1 = r12_0;
                rbx_1 = rbx_0;
                rbp_1 = rbp_0;
                if ((uint64_t)(unsigned char)var_50 != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                var_51 = (uint64_t *)(local_sp_1 + 8UL);
                var_52 = *var_51;
                var_53 = local_sp_1 + 52UL;
                var_54 = (uint64_t *)(r12_0 + 184UL);
                *var_51 = (var_36 + *var_54);
                var_55 = local_sp_1 + (-16L);
                *(uint64_t *)var_55 = 4263083UL;
                var_56 = indirect_placeholder_7(var_52, var_53, rbp_0);
                var_57 = *(uint64_t **)local_sp_1;
                var_58 = *var_48;
                *var_57 = var_56;
                local_sp_0 = var_55;
                rcx4_1 = var_58;
                local_sp_4 = var_55;
                if (*(uint64_t *)(var_36 + *var_54) != 0UL & *(uint32_t *)(local_sp_1 + 44UL) != 0U) {
                    loop_state_var = 2U;
                    break;
                }
            }
            if ((uint64_t)var_28 != 0UL) {
                var_29 = *var_16 + var_27;
                rsi5_0 = var_29;
                var_59 = local_sp_7 + (-8L);
                *(uint64_t *)var_59 = 4262763UL;
                var_60 = indirect_placeholder_7(rsi, r12_3, rsi5_0);
                local_sp_3 = var_59;
                local_sp_5 = var_59;
                if ((uint64_t)(unsigned char)var_60 == 0UL) {
                    var_65 = *(uint64_t **)local_sp_5;
                    var_66 = rbx_2 + 1UL;
                    local_sp_8 = local_sp_5;
                    rbp_3 = rbp_2;
                    local_sp_6 = local_sp_5;
                    rbx_3 = var_66;
                    r12_3 = r12_2;
                    if ((long)var_66 < (long)*var_65) {
                        continue;
                    }
                    loop_state_var = 1U;
                    break;
                }
            }
            rcx4_1 = var_18 << 3UL;
            local_sp_4 = local_sp_3;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(uint64_t *)(local_sp_2 + (-8L)) = 4263194UL;
            indirect_placeholder();
            return 12UL;
        }
        break;
      case 1U:
        {
            *(uint64_t *)(local_sp_8 + (-8L)) = 4263226UL;
            indirect_placeholder();
            return 0UL;
        }
        break;
      case 2U:
        {
            *(uint64_t *)(local_sp_0 + (-8L)) = 4263137UL;
            indirect_placeholder();
            return (uint64_t)*(uint32_t *)(local_sp_0 + 52UL);
        }
        break;
    }
}
