typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_232_ret_type;
struct indirect_placeholder_233_ret_type;
struct indirect_placeholder_234_ret_type;
struct indirect_placeholder_235_ret_type;
struct indirect_placeholder_232_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_233_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_234_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_235_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r15(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_14(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern uint64_t indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct indirect_placeholder_232_ret_type indirect_placeholder_232(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_233_ret_type indirect_placeholder_233(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_234_ret_type indirect_placeholder_234(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_235_ret_type indirect_placeholder_235(uint64_t param_0, uint64_t param_1, uint64_t param_2);
uint64_t bb_proceed_next_node(uint64_t r8, uint64_t rdx, uint64_t rdi, uint64_t rcx, uint64_t rsi, uint64_t r9) {
    uint64_t var_41;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t var_12;
    unsigned char var_13;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t *_pre_phi;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t *var_75;
    struct indirect_placeholder_232_ret_type var_76;
    uint64_t r12_3;
    uint64_t r12_1;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t r14_0;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t *var_28;
    struct indirect_placeholder_233_ret_type var_29;
    uint64_t local_sp_0;
    uint64_t r12_0;
    uint64_t var_34;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t *_pre_phi166;
    uint64_t local_sp_2;
    uint64_t r14_1;
    uint64_t var_23;
    uint64_t var_24;
    struct indirect_placeholder_234_ret_type var_25;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t local_sp_1;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_84;
    uint64_t r10_0;
    uint64_t var_85;
    uint64_t local_sp_4;
    uint64_t r13_0;
    uint64_t var_50;
    uint64_t rsi5_0;
    uint64_t r96_0;
    uint64_t var_81;
    uint64_t *var_82;
    uint64_t var_83;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t *var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t local_sp_3;
    uint64_t var_61;
    uint64_t *_pre;
    uint64_t r10_1;
    uint64_t r96_1;
    uint64_t r13_1;
    uint64_t r14_2;
    uint64_t r10_2;
    uint64_t r96_2;
    uint64_t r12_2_in_in;
    uint64_t storemerge;
    uint64_t r12_2;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_89;
    struct indirect_placeholder_235_ret_type var_90;
    uint64_t *var_62;
    uint64_t *var_63;
    uint64_t *var_64;
    uint64_t var_65;
    uint64_t *var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t *var_38;
    uint64_t var_39;
    uint64_t *var_40;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t *var_14;
    uint64_t var_15;
    uint64_t var_18;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r15();
    var_2 = init_r12();
    var_3 = init_rbx();
    var_4 = init_r13();
    var_5 = init_r14();
    var_6 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    var_7 = r8 << 4UL;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_2;
    *(uint64_t *)(var_0 + (-40L)) = var_6;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_8 = var_0 + (-88L);
    var_9 = *(uint64_t *)(rdi + 152UL);
    var_10 = (uint64_t *)var_8;
    *var_10 = rsi;
    var_11 = *(uint64_t *)var_9;
    var_12 = var_7 + var_11;
    var_13 = *(unsigned char *)(var_12 + 8UL);
    _pre_phi = var_10;
    r12_3 = 18446744073709551615UL;
    r12_1 = 18446744073709551615UL;
    r14_0 = 0UL;
    local_sp_2 = var_8;
    local_sp_1 = var_8;
    r10_0 = r8;
    rsi5_0 = var_12;
    r96_0 = var_9;
    r10_1 = r8;
    r96_1 = var_9;
    if (((uint64_t)var_13 & 8UL) == 0UL) {
        if ((*(unsigned char *)(var_12 + 10UL) & '\x10') == '\x00') {
            if ((uint64_t)(var_13 + '\xfc') == 0UL) {
                var_51 = ((*(uint64_t *)var_12 << 4UL) + 16UL) + rdx;
                var_52 = *(uint64_t *)(var_51 + 8UL);
                var_53 = *(uint64_t *)var_51;
                var_54 = var_52 - var_53;
                r14_1 = var_54;
                r14_2 = var_54;
                if (*(uint64_t *)(var_0 | 8UL) == 0UL) {
                    if (var_54 != 0UL) {
                        _pre = (uint64_t *)rcx;
                        _pre_phi166 = _pre;
                        var_61 = *_pre_phi166;
                        local_sp_3 = local_sp_2;
                        r13_1 = var_61;
                        r14_2 = r14_1;
                        r10_2 = r10_1;
                        r96_2 = r96_1;
                        local_sp_4 = local_sp_3;
                        r12_2_in_in = (r10_2 << 3UL) + *(uint64_t *)(r96_2 + 24UL);
                        storemerge = r14_2 + r13_1;
                        r12_2 = *(uint64_t *)r12_2_in_in;
                        var_86 = *(uint64_t *)(local_sp_4 + 96UL);
                        *(uint64_t *)rcx = storemerge;
                        r12_3 = r12_2;
                        if (var_86 == 0UL) {
                            *(uint64_t *)(r9 + 8UL) = 0UL;
                            return r12_3;
                        }
                        if ((long)*(uint64_t *)(rdi + 168UL) < (long)storemerge) {
                            return r12_3;
                        }
                        var_87 = *(uint64_t *)((storemerge << 3UL) + *(uint64_t *)(rdi + 184UL));
                        if (var_87 == 0UL) {
                            return r12_3;
                        }
                        var_88 = *(uint64_t *)(var_87 + 16UL);
                        var_89 = var_87 + 24UL;
                        *(uint64_t *)(local_sp_4 + (-8L)) = 4261220UL;
                        var_90 = indirect_placeholder_235(r12_2, var_88, var_89);
                        if (var_90.field_0 == 0UL) {
                            return r12_3;
                        }
                    }
                }
                if ((var_53 == 18446744073709551615UL) || (var_52 == 18446744073709551615UL)) {
                    return r12_3;
                }
                if (var_54 != 0UL) {
                    var_55 = *(uint64_t *)(rdi + 8UL);
                    var_56 = *(uint64_t *)rcx;
                    *var_10 = var_9;
                    *(uint64_t *)(var_0 + (-80L)) = r8;
                    var_57 = var_0 + (-96L);
                    var_58 = (uint64_t *)var_57;
                    *var_58 = 4261684UL;
                    indirect_placeholder();
                    var_59 = *var_58;
                    var_60 = *var_10;
                    local_sp_3 = var_57;
                    r13_1 = var_56;
                    r10_2 = var_60;
                    r96_2 = var_59;
                    if ((uint64_t)(uint32_t)var_55 == 0UL) {
                        return r12_3;
                    }
                    local_sp_4 = local_sp_3;
                    r12_2_in_in = (r10_2 << 3UL) + *(uint64_t *)(r96_2 + 24UL);
                    storemerge = r14_2 + r13_1;
                    r12_2 = *(uint64_t *)r12_2_in_in;
                    var_86 = *(uint64_t *)(local_sp_4 + 96UL);
                    *(uint64_t *)rcx = storemerge;
                    r12_3 = r12_2;
                    if (var_86 == 0UL) {
                        *(uint64_t *)(r9 + 8UL) = 0UL;
                        return r12_3;
                    }
                    if ((long)*(uint64_t *)(rdi + 168UL) < (long)storemerge) {
                        return r12_3;
                    }
                    var_87 = *(uint64_t *)((storemerge << 3UL) + *(uint64_t *)(rdi + 184UL));
                    if (var_87 == 0UL) {
                        return r12_3;
                    }
                    var_88 = *(uint64_t *)(var_87 + 16UL);
                    var_89 = var_87 + 24UL;
                    *(uint64_t *)(local_sp_4 + (-8L)) = 4261220UL;
                    var_90 = indirect_placeholder_235(r12_2, var_88, var_89);
                    if (var_90.field_0 == 0UL) {
                        return r12_3;
                    }
                }
                var_62 = (uint64_t *)(var_0 + (-80L));
                *var_62 = var_9;
                var_63 = (uint64_t *)(var_0 + (-72L));
                *var_63 = var_7;
                *var_10 = r8;
                var_64 = (uint64_t *)(var_0 + (-96L));
                *var_64 = 4261745UL;
                var_65 = indirect_placeholder_1(r9, r8);
                if ((uint64_t)(unsigned char)var_65 != 0UL) {
                    r12_3 = 18446744073709551614UL;
                    return r12_3;
                }
                var_66 = *var_64;
                var_67 = *var_10;
                var_68 = *(uint64_t *)rcx;
                *var_63 = var_66;
                var_69 = (var_66 * 24UL) + *(uint64_t *)(var_67 + 40UL);
                *var_64 = var_67;
                var_70 = **(uint64_t **)(var_69 + 16UL);
                var_71 = *(uint64_t *)((var_68 << 3UL) + *(uint64_t *)(rdi + 184UL));
                var_72 = *(uint64_t *)(var_71 + 16UL);
                var_73 = var_71 + 24UL;
                var_74 = var_0 + (-104L);
                var_75 = (uint64_t *)var_74;
                *var_75 = 4261820UL;
                var_76 = indirect_placeholder_232(var_70, var_72, var_73);
                _pre_phi = var_75;
                local_sp_1 = var_74;
                r13_0 = var_68;
                r12_3 = var_70;
                if (var_76.field_0 != 0UL) {
                    return r12_3;
                }
                var_77 = *var_10;
                var_78 = *var_75;
                var_79 = *var_62;
                var_80 = var_77 + *(uint64_t *)var_78;
                r10_0 = var_79;
                rsi5_0 = var_80;
                r96_0 = var_78;
            } else {
                var_50 = *(uint64_t *)rcx;
                r13_0 = var_50;
                *(uint64_t *)(local_sp_1 + 8UL) = r10_0;
                *_pre_phi = r96_0;
                var_81 = local_sp_1 + (-8L);
                var_82 = (uint64_t *)var_81;
                *var_82 = 4261131UL;
                var_83 = indirect_placeholder_7(r13_0, rdi, rsi5_0);
                local_sp_4 = var_81;
                if ((uint64_t)(unsigned char)var_83 == 0UL) {
                    return r12_3;
                }
                var_84 = (*_pre_phi << 3UL) + *(uint64_t *)(*var_82 + 24UL);
                var_85 = r13_0 + 1UL;
                r12_2_in_in = var_84;
                storemerge = var_85;
            }
        } else {
            var_35 = (uint64_t *)rcx;
            var_36 = *var_35;
            *(uint64_t *)(var_0 + (-72L)) = var_7;
            var_37 = var_9 + 216UL;
            var_38 = (uint64_t *)(var_0 + (-80L));
            *var_38 = var_9;
            *var_10 = r8;
            var_39 = var_0 + (-96L);
            var_40 = (uint64_t *)var_39;
            *var_40 = 4261299UL;
            var_41 = indirect_placeholder_14(var_36, r8, var_11, rdi, var_37);
            var_42 = *var_40;
            var_43 = *var_10;
            var_44 = var_41 << 32UL;
            var_45 = (uint64_t)((long)var_44 >> (long)32UL);
            _pre_phi = var_40;
            _pre_phi166 = var_35;
            local_sp_2 = var_39;
            r14_1 = var_45;
            local_sp_1 = var_39;
            r10_0 = var_42;
            r96_0 = var_43;
            r10_1 = var_42;
            r96_1 = var_43;
            if (var_44 == 0UL) {
                var_46 = *var_38;
                var_47 = *(uint64_t *)var_43;
                var_48 = *var_35;
                var_49 = var_47 + var_46;
                r13_0 = var_48;
                rsi5_0 = var_49;
                *(uint64_t *)(local_sp_1 + 8UL) = r10_0;
                *_pre_phi = r96_0;
                var_81 = local_sp_1 + (-8L);
                var_82 = (uint64_t *)var_81;
                *var_82 = 4261131UL;
                var_83 = indirect_placeholder_7(r13_0, rdi, rsi5_0);
                local_sp_4 = var_81;
                if ((uint64_t)(unsigned char)var_83 != 0UL) {
                    return r12_3;
                }
                var_84 = (*_pre_phi << 3UL) + *(uint64_t *)(*var_82 + 24UL);
                var_85 = r13_0 + 1UL;
                r12_2_in_in = var_84;
                storemerge = var_85;
            } else {
                var_61 = *_pre_phi166;
                local_sp_3 = local_sp_2;
                r13_1 = var_61;
                r14_2 = r14_1;
                r10_2 = r10_1;
                r96_2 = r96_1;
                local_sp_4 = local_sp_3;
                r12_2_in_in = (r10_2 << 3UL) + *(uint64_t *)(r96_2 + 24UL);
                storemerge = r14_2 + r13_1;
            }
            r12_2 = *(uint64_t *)r12_2_in_in;
            var_86 = *(uint64_t *)(local_sp_4 + 96UL);
            *(uint64_t *)rcx = storemerge;
            r12_3 = r12_2;
            if (var_86 != 0UL) {
                if ((long)*(uint64_t *)(rdi + 168UL) < (long)storemerge) {
                    return r12_3;
                }
                var_87 = *(uint64_t *)((storemerge << 3UL) + *(uint64_t *)(rdi + 184UL));
                if (var_87 == 0UL) {
                    return r12_3;
                }
                var_88 = *(uint64_t *)(var_87 + 16UL);
                var_89 = var_87 + 24UL;
                *(uint64_t *)(local_sp_4 + (-8L)) = 4261220UL;
                var_90 = indirect_placeholder_235(r12_2, var_88, var_89);
                if (var_90.field_0 != 0UL) {
                    return r12_3;
                }
            }
            *(uint64_t *)(r9 + 8UL) = 0UL;
        }
    } else {
        var_14 = (uint64_t *)rcx;
        var_15 = *(uint64_t *)((*var_14 << 3UL) + *(uint64_t *)(rdi + 184UL));
        var_16 = (r8 * 24UL) + *(uint64_t *)(var_9 + 40UL);
        var_17 = var_0 + (-96L);
        *(uint64_t *)var_17 = 4261381UL;
        var_18 = indirect_placeholder_1(r9, r8);
        local_sp_0 = var_17;
        if ((uint64_t)(unsigned char)var_18 != 0UL) {
            r12_3 = 18446744073709551614UL;
            return r12_3;
        }
        var_19 = *(uint64_t *)(var_16 + 8UL);
        var_20 = helper_cc_compute_all_wrapper(var_19, 0UL, 0UL, 25U);
        if ((uint64_t)(((unsigned char)(var_20 >> 4UL) ^ (unsigned char)var_20) & '\xc0') != 0UL) {
            var_21 = *(uint64_t *)(var_15 + 16UL);
            var_22 = var_15 + 24UL;
            while (1U)
                {
                    var_23 = *(uint64_t *)((r14_0 << 3UL) + *(uint64_t *)(var_16 + 16UL));
                    var_24 = local_sp_0 + (-8L);
                    *(uint64_t *)var_24 = 4261466UL;
                    var_25 = indirect_placeholder_234(var_23, var_21, var_22);
                    r12_0 = r12_1;
                    local_sp_0 = var_24;
                    r12_3 = var_23;
                    if (var_25.field_0 == 0UL) {
                        var_34 = r14_0 + 1UL;
                        r12_1 = r12_0;
                        r14_0 = var_34;
                        r12_3 = r12_0;
                        if (var_34 == var_19) {
                            continue;
                        }
                        loop_state_var = 1U;
                        break;
                    }
                    r12_0 = var_23;
                    if (r12_1 != 18446744073709551615UL) {
                        var_26 = *(uint64_t *)(r9 + 8UL);
                        var_27 = r9 + 16UL;
                        var_28 = (uint64_t *)(local_sp_0 + (-16L));
                        *var_28 = 4261493UL;
                        var_29 = indirect_placeholder_233(r12_1, var_26, var_27);
                        if (var_29.field_0 != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_30 = *(uint64_t *)(local_sp_0 + 80UL);
                        r12_3 = r12_1;
                        if (var_30 != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_31 = *var_28;
                        var_32 = *var_14;
                        *(uint64_t *)(local_sp_0 + (-24L)) = 4261540UL;
                        var_33 = indirect_placeholder_13(rdx, var_23, var_30, var_31, var_32, r9);
                        if ((uint64_t)(uint32_t)var_33 != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        loop_state_var = 0U;
                        break;
                    }
                }
            r12_3 = 18446744073709551614UL;
        }
    }
}
