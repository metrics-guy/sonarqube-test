typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_186_ret_type;
struct indirect_placeholder_186_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
extern struct indirect_placeholder_186_ret_type indirect_placeholder_186(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_8(uint64_t param_0);
uint64_t bb_create_ci_newstate(uint64_t r8, uint64_t rdx, uint64_t rdi, uint64_t rcx, uint64_t r10, uint64_t rsi, uint64_t r9) {
    struct indirect_placeholder_186_ret_type var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t r13_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t *var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t *var_17;
    unsigned char *var_18;
    uint64_t rsi6_0_be;
    uint64_t rsi6_0;
    uint64_t var_19;
    uint64_t var_20;
    uint32_t var_21;
    unsigned char var_24;
    unsigned char var_25;
    unsigned char var_22;
    unsigned char var_23;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r12();
    var_2 = init_rbx();
    var_3 = init_r13();
    var_4 = init_r14();
    var_5 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_4;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    *(uint64_t *)(var_0 + (-48L)) = 4250272UL;
    var_6 = indirect_placeholder_186(rsi, r8, rdi, 112UL, rcx, r10, 1UL, r9);
    var_7 = var_6.field_0;
    var_8 = var_6.field_1;
    var_9 = var_6.field_3;
    rsi6_0 = 0UL;
    r13_0 = 0UL;
    if (var_7 == 0UL) {
        return r13_0;
    }
    var_10 = var_7 + 8UL;
    var_11 = var_8 + 16UL;
    var_12 = var_8 + 8UL;
    *(uint64_t *)(var_0 + (-56L)) = 4250304UL;
    var_13 = indirect_placeholder_7(var_11, var_10, var_12);
    r13_0 = var_7;
    if ((uint64_t)(uint32_t)var_13 == 0UL) {
        *(uint64_t *)(var_0 + (-64L)) = 4250595UL;
        indirect_placeholder();
    } else {
        var_14 = (uint64_t *)var_12;
        var_15 = *var_14;
        *(uint64_t *)(var_7 + 80UL) = var_10;
        if ((long)var_15 <= (long)0UL) {
            var_16 = *(uint64_t *)var_9;
            var_17 = (uint64_t *)var_11;
            var_18 = (unsigned char *)(var_7 + 104UL);
            while (1U)
                {
                    var_19 = (*(uint64_t *)((rsi6_0 << 3UL) + *var_17) << 4UL) + var_16;
                    var_20 = var_19 + 8UL;
                    var_21 = (uint32_t)(uint64_t)*(unsigned char *)var_20;
                    if ((uint64_t)(var_21 + (-1)) != 0UL) {
                        var_22 = *var_18;
                        var_23 = (unsigned char)((((uint64_t)(*(unsigned char *)(var_19 + 10UL) >> '\x04') << 5UL) | (uint64_t)var_22) & 32UL) | (var_22 & '\xdf');
                        *var_18 = var_23;
                        var_25 = var_23;
                        if ((uint64_t)(var_21 + (-2)) != 0UL) {
                            *var_18 = (var_23 | '\x10');
                            var_27 = rsi6_0 + 1UL;
                            rsi6_0_be = var_27;
                            if ((long)*var_14 > (long)var_27) {
                                break;
                            }
                            rsi6_0 = rsi6_0_be;
                            continue;
                        }
                        if ((uint64_t)(var_21 + (-4)) == 0UL) {
                            if ((uint64_t)(var_21 + (-12)) != 0UL & (*(uint32_t *)var_20 & 261888U) != 0U) {
                                var_27 = rsi6_0 + 1UL;
                                rsi6_0_be = var_27;
                                if ((long)*var_14 > (long)var_27) {
                                    break;
                                }
                                rsi6_0 = rsi6_0_be;
                                continue;
                            }
                        }
                        *var_18 = (var_23 | '@');
                        var_27 = rsi6_0 + 1UL;
                        rsi6_0_be = var_27;
                        if ((long)*var_14 > (long)var_27) {
                            break;
                        }
                        rsi6_0 = rsi6_0_be;
                        continue;
                    }
                    if ((*(uint32_t *)var_20 & 261888U) != 0U) {
                        var_27 = rsi6_0 + 1UL;
                        rsi6_0_be = var_27;
                        if ((long)*var_14 > (long)var_27) {
                            break;
                        }
                        rsi6_0 = rsi6_0_be;
                        continue;
                    }
                    var_24 = ((*(unsigned char *)(var_19 + 10UL) << '\x01') & ' ') | *var_18;
                    *var_18 = var_24;
                    var_25 = var_24;
                }
        }
        *(uint64_t *)(var_0 + (-64L)) = 4250508UL;
        var_28 = indirect_placeholder_7(rdx, var_9, var_7);
        if ((uint64_t)(uint32_t)var_28 == 0UL) {
            *(uint64_t *)(var_0 + (-72L)) = 4250579UL;
            indirect_placeholder_8(var_7);
            r13_0 = 0UL;
        }
    }
    return r13_0;
}
