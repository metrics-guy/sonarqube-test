typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_191_ret_type;
struct indirect_placeholder_190_ret_type;
struct indirect_placeholder_189_ret_type;
struct indirect_placeholder_192_ret_type;
struct indirect_placeholder_193_ret_type;
struct indirect_placeholder_194_ret_type;
struct indirect_placeholder_191_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_190_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_189_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_192_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_193_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_194_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct indirect_placeholder_191_ret_type indirect_placeholder_191(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_190_ret_type indirect_placeholder_190(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_189_ret_type indirect_placeholder_189(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_192_ret_type indirect_placeholder_192(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_193_ret_type indirect_placeholder_193(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_194_ret_type indirect_placeholder_194(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
typedef _Bool bool;
uint64_t bb_create_initial_state(uint64_t rdi) {
    struct indirect_placeholder_192_ret_type var_35;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint32_t *var_12;
    uint32_t var_13;
    uint64_t var_14;
    uint64_t r12_2;
    uint64_t local_sp_0;
    uint64_t r12_1;
    uint64_t var_48;
    uint64_t var_49;
    struct indirect_placeholder_191_ret_type var_50;
    uint64_t var_51;
    uint64_t *var_52;
    uint64_t var_53;
    struct indirect_placeholder_190_ret_type var_54;
    uint64_t var_55;
    uint64_t *var_56;
    uint64_t var_57;
    uint64_t var_58;
    struct indirect_placeholder_189_ret_type var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t rbx_1;
    uint64_t local_sp_4;
    uint64_t local_sp_1;
    uint64_t var_41;
    uint64_t local_sp_2_ph;
    uint64_t r8_0_ph;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t r9_0;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t rbx_0_ph;
    uint64_t r9_0_ph;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t r12_0_ph;
    uint64_t rbp_0_ph;
    uint64_t *var_17;
    uint64_t *var_18;
    uint64_t local_sp_3;
    uint64_t local_sp_2;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t *var_24;
    uint64_t rdx_0;
    uint64_t var_25;
    uint64_t rdx_1;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    struct indirect_placeholder_193_ret_type var_29;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    struct indirect_placeholder_194_ret_type var_46;
    uint64_t var_47;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r12();
    var_2 = init_rbx();
    var_3 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_4 = *(uint64_t *)(*(uint64_t *)(*(uint64_t *)(rdi + 104UL) + 24UL) + 56UL);
    *(uint64_t *)(rdi + 144UL) = var_4;
    var_5 = *(uint64_t *)(rdi + 48UL);
    var_6 = var_0 + (-56L);
    var_7 = (var_4 * 24UL) + var_5;
    var_8 = var_7 + 16UL;
    var_9 = var_7 + 8UL;
    var_10 = var_0 + (-80L);
    *(uint64_t *)var_10 = 4249803UL;
    var_11 = indirect_placeholder_7(var_8, var_6, var_9);
    var_12 = (uint32_t *)(var_0 + (-68L));
    var_13 = (uint32_t)var_11;
    *var_12 = var_13;
    var_14 = (uint64_t)var_13;
    r12_2 = var_14;
    r12_1 = 0UL;
    rbx_1 = rdi;
    local_sp_4 = var_10;
    local_sp_2_ph = var_10;
    rbx_0_ph = rdi;
    r9_0_ph = 0UL;
    r12_0_ph = 0UL;
    rbp_0_ph = var_3;
    rdx_0 = 0UL;
    if (var_14 == 0UL) {
        return (uint64_t)(uint32_t)r12_2;
    }
    if ((long)*(uint64_t *)(rdi + 152UL) <= (long)0UL) {
        var_15 = *(uint64_t *)var_6;
        var_16 = helper_cc_compute_all_wrapper(var_15, 0UL, 0UL, 25U);
        r8_0_ph = var_15;
        if ((uint64_t)(((unsigned char)(var_16 >> 4UL) ^ (unsigned char)var_16) & '\xc0') != 0UL) {
            while (1U)
                {
                    var_17 = (uint64_t *)rbx_0_ph;
                    var_18 = (uint64_t *)(rbx_0_ph + 40UL);
                    r12_1 = r12_0_ph;
                    rbx_1 = rbx_0_ph;
                    r9_0 = r9_0_ph;
                    r9_0_ph = 1UL;
                    local_sp_2 = local_sp_2_ph;
                    while (1U)
                        {
                            var_19 = local_sp_2 + 32UL;
                            var_20 = *(uint64_t *)var_19;
                            var_21 = *var_17;
                            var_22 = *(uint64_t *)((r9_0 << 3UL) + var_20);
                            var_23 = (var_22 << 4UL) + var_21;
                            local_sp_3 = local_sp_2;
                            if (*(unsigned char *)(var_23 + 8UL) == '\x04') {
                                var_42 = r9_0 + 1UL;
                                local_sp_2 = local_sp_3;
                                r9_0 = var_42;
                                local_sp_4 = local_sp_3;
                                if ((long)r8_0_ph > (long)var_42) {
                                    continue;
                                }
                                loop_state_var = 1U;
                                break;
                            }
                            var_24 = (uint64_t *)var_23;
                            while (1U)
                                {
                                    var_25 = (*(uint64_t *)((rdx_0 << 3UL) + var_20) << 4UL) + var_21;
                                    rdx_1 = rdx_0;
                                    if (*(unsigned char *)(var_25 + 8UL) != '\t') {
                                        if (*(uint64_t *)var_25 == *var_24) {
                                            break;
                                        }
                                    }
                                    var_26 = rdx_0 + 1UL;
                                    rdx_0 = var_26;
                                    rdx_1 = var_26;
                                    if ((long)r8_0_ph <= (long)var_26) {
                                        continue;
                                    }
                                    break;
                                }
                            var_27 = **(uint64_t **)(((var_22 * 24UL) + *var_18) + 16UL);
                            var_28 = local_sp_2 + (-8L);
                            *(uint64_t *)var_28 = 4249975UL;
                            var_29 = indirect_placeholder_193(var_27, r8_0_ph, var_19);
                            local_sp_3 = var_28;
                            if (rdx_1 != r8_0_ph & var_29.field_0 != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 0U:
                        {
                            var_30 = var_29.field_1;
                            var_31 = *(uint64_t *)(rbx_0_ph + 48UL);
                            var_32 = local_sp_2 + 8UL;
                            var_33 = (var_30 * 24UL) + var_31;
                            var_34 = local_sp_2 + (-16L);
                            *(uint64_t *)var_34 = 4250002UL;
                            var_35 = indirect_placeholder_192(r12_0_ph, rbx_0_ph, rbp_0_ph, var_32, var_33);
                            var_36 = var_35.field_0;
                            var_37 = var_35.field_1;
                            var_38 = var_35.field_2;
                            var_39 = var_35.field_3;
                            var_40 = (uint64_t)(uint32_t)var_36;
                            r12_2 = var_40;
                            r12_1 = var_37;
                            rbx_1 = var_38;
                            local_sp_4 = var_34;
                            local_sp_2_ph = var_34;
                            rbx_0_ph = var_38;
                            r12_0_ph = var_37;
                            rbp_0_ph = var_39;
                            if (var_40 != 0UL) {
                                loop_state_var = 0U;
                                switch_state_var = 1;
                                break;
                            }
                            var_41 = *(uint64_t *)var_32;
                            r8_0_ph = var_41;
                            if ((long)var_41 > (long)1UL) {
                                continue;
                            }
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 1U:
                        {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            switch (loop_state_var) {
              case 0U:
                {
                    return (uint64_t)(uint32_t)r12_2;
                }
                break;
              case 1U:
                {
                    break;
                }
                break;
            }
        }
    }
    var_43 = local_sp_4 + 16UL;
    var_44 = local_sp_4 + 12UL;
    var_45 = local_sp_4 + (-8L);
    *(uint64_t *)var_45 = 4250052UL;
    var_46 = indirect_placeholder_194(var_43, var_44, 0UL, rbx_1);
    var_47 = var_46.field_0;
    *(uint64_t *)(rbx_1 + 72UL) = var_47;
    local_sp_0 = var_45;
    local_sp_1 = var_45;
    r12_2 = r12_1;
    if (var_47 == 0UL) {
        return (uint64_t)*(uint32_t *)(local_sp_1 + 12UL);
    }
    if ((signed char)*(unsigned char *)(var_47 + 104UL) < '\x00') {
        var_48 = local_sp_4 + 8UL;
        var_49 = local_sp_4 + 4UL;
        *(uint64_t *)(local_sp_4 + (-16L)) = 4250127UL;
        var_50 = indirect_placeholder_191(var_48, var_49, 1UL, rbx_1);
        var_51 = var_50.field_0;
        var_52 = (uint64_t *)(rbx_1 + 80UL);
        *var_52 = var_51;
        var_53 = local_sp_4 + (-4L);
        *(uint64_t *)(local_sp_4 + (-24L)) = 4250154UL;
        var_54 = indirect_placeholder_190(local_sp_4, var_53, 2UL, rbx_1);
        var_55 = var_54.field_0;
        var_56 = (uint64_t *)(rbx_1 + 88UL);
        *var_56 = var_55;
        var_57 = local_sp_4 + (-12L);
        var_58 = local_sp_4 + (-32L);
        *(uint64_t *)var_58 = 4250181UL;
        var_59 = indirect_placeholder_189(var_45, var_57, 6UL, rbx_1);
        var_60 = var_59.field_0;
        var_61 = *var_52;
        *(uint64_t *)(rbx_1 + 96UL) = var_60;
        local_sp_0 = var_58;
        local_sp_1 = var_58;
        if (var_61 == 0UL) {
            return (uint64_t)*(uint32_t *)(local_sp_1 + 12UL);
        }
        if (!((*var_56 == 0UL) || (var_60 == 0UL))) {
            return (uint64_t)*(uint32_t *)(local_sp_1 + 12UL);
        }
    }
    *(uint64_t *)(rbx_1 + 96UL) = var_47;
    *(uint64_t *)(rbx_1 + 88UL) = var_47;
    *(uint64_t *)(rbx_1 + 80UL) = var_47;
    *(uint64_t *)(local_sp_0 + (-8L)) = 4250093UL;
    indirect_placeholder();
}
