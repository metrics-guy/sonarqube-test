typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_get_subexp_ret_type;
struct indirect_placeholder_249_ret_type;
struct indirect_placeholder_250_ret_type;
struct indirect_placeholder_251_ret_type;
struct indirect_placeholder_252_ret_type;
struct indirect_placeholder_253_ret_type;
struct indirect_placeholder_254_ret_type;
struct indirect_placeholder_255_ret_type;
struct bb_get_subexp_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_249_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_250_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_251_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_252_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_253_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_254_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_255_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r15(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern uint64_t init_r8(void);
extern struct indirect_placeholder_249_ret_type indirect_placeholder_249(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_250_ret_type indirect_placeholder_250(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
extern struct indirect_placeholder_251_ret_type indirect_placeholder_251(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_252_ret_type indirect_placeholder_252(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_253_ret_type indirect_placeholder_253(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_254_ret_type indirect_placeholder_254(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_255_ret_type indirect_placeholder_255(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
struct bb_get_subexp_ret_type bb_get_subexp(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_255_ret_type var_66;
    struct indirect_placeholder_253_ret_type var_49;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_90;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t *var_12;
    struct indirect_placeholder_249_ret_type var_13;
    uint64_t var_14;
    uint64_t r14_2;
    uint64_t local_sp_9;
    uint64_t local_sp_0;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t rbp_2;
    uint64_t var_73;
    uint64_t r15_5;
    uint64_t var_74;
    struct indirect_placeholder_250_ret_type var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t _pre_phi223;
    uint64_t r8_10;
    uint64_t rbx_0;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t rbp_0;
    uint64_t rcx_0;
    uint64_t local_sp_11;
    uint64_t rax_2;
    uint64_t rsi4_0;
    uint64_t var_78;
    uint64_t *var_79;
    uint64_t var_80;
    uint64_t *var_81;
    struct indirect_placeholder_252_ret_type var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint32_t var_89;
    uint64_t r13_0;
    uint64_t rcx_1;
    uint64_t var_32;
    uint64_t r14_0;
    uint64_t var_52;
    uint64_t local_sp_2;
    uint64_t local_sp_5;
    uint64_t rax_0;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t r15_6;
    uint64_t local_sp_1;
    uint64_t rbx_4;
    uint64_t r15_0;
    uint64_t r8_8;
    uint64_t rbx_1;
    uint64_t r8_1;
    uint64_t rdx2_0;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t local_sp_12;
    uint64_t *var_23;
    uint64_t var_24;
    uint64_t var_25;
    bool var_26;
    uint64_t var_27;
    uint64_t local_sp_7;
    uint64_t *var_28;
    uint64_t *var_29;
    uint64_t *var_30;
    uint64_t *var_31;
    uint64_t r12_0;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t local_sp_6;
    uint64_t *_pre_phi;
    uint64_t local_sp_3;
    uint64_t r8_2;
    uint64_t rcx_2;
    uint64_t r9_0;
    uint64_t *var_43;
    uint64_t *var_44;
    uint64_t var_45;
    uint64_t local_sp_4;
    uint64_t *var_38;
    uint64_t *var_39;
    uint64_t var_40;
    uint64_t *var_41;
    uint64_t var_42;
    uint64_t r8_3;
    uint64_t rcx_3;
    uint64_t r9_1;
    uint64_t r15_1;
    uint64_t r12_1;
    uint64_t r14_1;
    uint64_t r8_4;
    uint64_t var_53;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t r15_2;
    uint64_t r8_5;
    uint64_t rcx_4;
    uint64_t r9_2;
    uint64_t rbx_3;
    uint64_t local_sp_8;
    uint64_t r12_3;
    uint64_t r15_3;
    uint64_t r12_2;
    uint64_t r8_7;
    uint64_t rbx_2;
    uint64_t r8_6;
    uint64_t rbp_1;
    uint64_t *var_54;
    uint64_t var_55;
    uint64_t local_sp_10;
    uint64_t r15_4;
    uint64_t var_56;
    uint64_t var_57;
    struct indirect_placeholder_254_ret_type var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_93;
    uint64_t *var_94;
    uint64_t var_95;
    struct bb_get_subexp_ret_type mrv;
    struct bb_get_subexp_ret_type mrv1;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r15();
    var_2 = init_r12();
    var_3 = init_rbx();
    var_4 = init_r13();
    var_5 = init_r14();
    var_6 = init_r8();
    var_7 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_8 = rdi + 216UL;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_2;
    *(uint64_t *)(var_0 + (-40L)) = var_7;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_9 = *(uint64_t *)(rdi + 200UL);
    var_10 = (uint64_t *)(var_0 + (-88L));
    *var_10 = rdx;
    var_11 = var_0 + (-128L);
    var_12 = (uint64_t *)var_11;
    *var_12 = 4264684UL;
    var_13 = indirect_placeholder_249(rdx, var_9, var_8);
    var_14 = var_13.field_0;
    _pre_phi223 = 0UL;
    r8_10 = var_6;
    rax_2 = 0UL;
    r13_0 = 0UL;
    local_sp_1 = var_11;
    rbx_1 = rdi;
    r8_1 = var_6;
    if (var_14 != 18446744073709551615UL) {
        rax_0 = (var_14 * 40UL) + *(uint64_t *)var_8;
        while (1U)
            {
                if (*(uint64_t *)rax_0 != rsi) {
                    loop_state_var = 1U;
                    break;
                }
                if (*(unsigned char *)(rax_0 + 32UL) != '\x00') {
                    loop_state_var = 0U;
                    break;
                }
                rax_0 = rax_0 + 40UL;
                continue;
            }
        switch (loop_state_var) {
          case 1U:
            {
                mrv.field_0 = rax_2;
                mrv1 = mrv;
                mrv1.field_1 = r8_10;
                return mrv1;
            }
            break;
          case 0U:
            {
                break;
            }
            break;
        }
    }
    var_15 = *(uint64_t *)(rdi + 152UL);
    var_16 = *(uint64_t *)var_15;
    *(uint64_t *)(var_0 + (-72L)) = var_15;
    var_17 = *(uint64_t *)(rdi + 232UL);
    *(uint64_t *)(var_0 + (-80L)) = *(uint64_t *)((rsi << 4UL) + var_16);
    rdx2_0 = var_16;
    if ((long)var_17 > (long)0UL) {
        mrv.field_0 = rax_2;
        mrv1 = mrv;
        mrv1.field_1 = r8_10;
        return mrv1;
    }
    var_18 = *(uint64_t *)(rdi + 8UL);
    *var_12 = 0UL;
    *var_10 = rsi;
    var_19 = *var_12;
    r15_0 = var_18;
    rax_2 = 12UL;
    while (1U)
        {
            var_20 = *(uint64_t *)(rbx_1 + 248UL);
            var_21 = *(uint64_t *)(local_sp_1 + 48UL);
            var_22 = *(uint64_t *)((var_19 << 3UL) + var_20);
            rcx_1 = r15_0;
            local_sp_2 = local_sp_1;
            r15_6 = r15_0;
            rbx_4 = rbx_1;
            r8_8 = r8_1;
            local_sp_12 = local_sp_1;
            local_sp_7 = local_sp_1;
            r15_2 = r15_0;
            r8_5 = r8_1;
            rbx_2 = rbx_1;
            rbp_1 = var_22;
            if (*(uint64_t *)((*(uint64_t *)(var_22 + 8UL) << 4UL) + rdx2_0) == var_21) {
                var_94 = (uint64_t *)local_sp_12;
                var_95 = *var_94 + 1UL;
                *var_94 = var_95;
                r8_10 = r8_8;
                rax_2 = 0UL;
                var_19 = var_95;
                local_sp_1 = local_sp_12;
                r15_0 = r15_6;
                rbx_1 = rbx_4;
                r8_1 = r8_8;
                if ((long)*(uint64_t *)(rbx_4 + 232UL) > (long)var_95) {
                    break;
                }
                rdx2_0 = **(uint64_t **)(local_sp_12 + 56UL);
                continue;
            }
            var_23 = (uint64_t *)(var_22 + 32UL);
            var_24 = *var_23;
            var_25 = *(uint64_t *)var_22;
            var_26 = ((long)var_24 > (long)0UL);
            var_27 = *(uint64_t *)(local_sp_1 + 32UL);
            r12_0 = var_27;
            r14_0 = var_25;
            rcx_4 = var_27;
            r9_2 = var_25;
            if (var_26) {
                r14_2 = r9_2;
                r15_6 = r15_2;
                r8_8 = r8_5;
                local_sp_12 = local_sp_7;
                local_sp_8 = local_sp_7;
                r15_3 = r15_2;
                r12_2 = rcx_4;
                r8_6 = r8_5;
                if ((long)r9_2 <= (long)*(uint64_t *)(local_sp_7 + 32UL)) {
                    while (1U)
                        {
                            var_54 = (uint64_t *)rbp_1;
                            var_55 = helper_cc_compute_all_wrapper(r14_2 - *var_54, 0UL, 0UL, 25U);
                            local_sp_9 = local_sp_8;
                            rbp_2 = rbp_1;
                            r15_5 = r15_3;
                            r8_10 = r8_6;
                            rbx_0 = rbx_2;
                            rbp_0 = rbp_1;
                            r15_6 = r15_3;
                            rbx_4 = rbx_2;
                            r8_8 = r8_6;
                            local_sp_12 = local_sp_8;
                            rbx_3 = rbx_2;
                            r12_3 = r12_2;
                            r8_7 = r8_6;
                            local_sp_10 = local_sp_8;
                            r15_4 = r15_3;
                            if ((uint64_t)(((unsigned char)(var_55 >> 4UL) ^ (unsigned char)var_55) & '\xc0') != 0UL) {
                                if ((long)*(uint64_t *)(rbx_2 + 48UL) <= (long)r12_2) {
                                    if ((long)*(uint64_t *)(rbx_2 + 88UL) <= (long)r12_2) {
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    var_56 = (uint64_t)((uint32_t)r12_2 + 1U);
                                    var_57 = local_sp_8 + (-8L);
                                    *(uint64_t *)var_57 = 4265495UL;
                                    var_58 = indirect_placeholder_254(rbx_2, var_56);
                                    var_59 = var_58.field_0;
                                    local_sp_9 = var_57;
                                    rax_2 = var_59;
                                    if ((uint64_t)(uint32_t)var_59 != 0UL) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    r15_4 = *(uint64_t *)(rbx_2 + 8UL);
                                }
                                r15_5 = r15_4;
                                r15_6 = r15_4;
                                local_sp_12 = local_sp_9;
                                r12_3 = r12_2 + 1UL;
                                local_sp_10 = local_sp_9;
                                if ((uint64_t)(*(unsigned char *)(r12_2 + r15_4) - *(unsigned char *)((r14_2 + r15_4) + (-1L))) != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                            }
                            var_60 = *(uint64_t *)((r14_2 << 3UL) + *(uint64_t *)(rbx_2 + 184UL));
                            r15_3 = r15_5;
                            r12_2 = r12_3;
                            local_sp_11 = local_sp_10;
                            r15_6 = r15_5;
                            var_61 = *(uint64_t *)(var_60 + 16UL);
                            var_62 = *(uint64_t *)(local_sp_10 + 48UL);
                            var_63 = var_60 + 24UL;
                            var_64 = *(uint64_t *)(local_sp_10 + 56UL);
                            var_65 = local_sp_10 + (-8L);
                            *(uint64_t *)var_65 = 4265360UL;
                            var_66 = indirect_placeholder_255(9UL, var_63, var_64, var_62, var_61);
                            var_67 = var_66.field_0;
                            var_68 = var_66.field_1;
                            var_69 = var_66.field_3;
                            var_70 = var_66.field_4;
                            local_sp_0 = var_65;
                            local_sp_11 = var_65;
                            r8_7 = var_68;
                            if (var_60 != 0UL & var_67 != 18446744073709551615UL) {
                                var_71 = *(uint64_t *)(rbp_1 + 16UL);
                                var_72 = *var_54;
                                rcx_0 = var_72;
                                rsi4_0 = var_71;
                                if (var_71 != 0UL) {
                                    var_73 = (r14_2 - var_72) + 1UL;
                                    var_74 = local_sp_10 + (-16L);
                                    *(uint64_t *)var_74 = 4265406UL;
                                    var_75 = indirect_placeholder_250(rbx_2, var_68, rbp_1, 24UL, var_72, var_69, var_73, var_70);
                                    var_76 = var_75.field_0;
                                    var_77 = var_75.field_3;
                                    *(uint64_t *)(var_77 + 16UL) = var_76;
                                    local_sp_0 = var_74;
                                    rbp_0 = var_77;
                                    rsi4_0 = var_76;
                                    if (var_76 != 0UL) {
                                        r8_10 = var_75.field_2;
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    rbx_0 = var_75.field_1;
                                    rcx_0 = *(uint64_t *)var_77;
                                }
                                var_78 = *(uint64_t *)(rbp_0 + 8UL);
                                var_79 = (uint64_t *)(local_sp_0 + (-16L));
                                *var_79 = 9UL;
                                var_80 = local_sp_0 + (-24L);
                                var_81 = (uint64_t *)var_80;
                                *var_81 = 4265192UL;
                                var_82 = indirect_placeholder_252(var_67, var_78, rbx_0, rcx_0, rsi4_0, r14_2);
                                var_83 = var_82.field_0;
                                var_84 = var_82.field_1;
                                var_85 = var_82.field_2;
                                var_86 = var_82.field_3;
                                var_87 = *var_79;
                                var_88 = local_sp_0 + (-8L);
                                var_89 = (uint32_t)var_83;
                                rbp_2 = rbp_0;
                                r8_10 = var_84;
                                local_sp_11 = var_88;
                                rax_2 = var_83;
                                rbx_3 = rbx_0;
                                r8_7 = var_84;
                                if ((uint64_t)(var_89 + (-1)) != 0UL) {
                                    local_sp_11 = var_80;
                                    if ((uint64_t)var_89 != 0UL) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    *var_79 = 4265221UL;
                                    var_90 = indirect_placeholder_12(var_84, r14_2, rbp_0, var_87, var_85, var_67, var_86);
                                    rax_2 = 12UL;
                                    if (var_90 != 0UL) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    var_91 = *(uint64_t *)(local_sp_0 + 16UL);
                                    var_92 = *(uint64_t *)(local_sp_0 + 24UL);
                                    *var_81 = 4265254UL;
                                    indirect_placeholder_251(var_91, var_90, rbx_0, var_92, rbp_0);
                                    r8_7 = var_91;
                                }
                            }
                            var_93 = r14_2 + 1UL;
                            r14_2 = var_93;
                            rbx_4 = rbx_3;
                            r8_8 = r8_7;
                            local_sp_12 = local_sp_11;
                            local_sp_8 = local_sp_11;
                            rbx_2 = rbx_3;
                            r8_6 = r8_7;
                            rbp_1 = rbp_2;
                            if ((long)*(uint64_t *)(local_sp_11 + 32UL) < (long)var_93) {
                                continue;
                            }
                            loop_state_var = 1U;
                            break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 0U:
                        {
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 1U:
                        {
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            }
            var_28 = (uint64_t *)(var_22 + 40UL);
            var_29 = (uint64_t *)(rbx_1 + 48UL);
            var_30 = (uint64_t *)(rbx_1 + 88UL);
            var_31 = (uint64_t *)(rbx_1 + 8UL);
            while (1U)
                {
                    var_32 = *var_28;
                    var_33 = *(uint64_t *)((r13_0 << 3UL) + var_32);
                    var_34 = *(uint64_t *)(var_33 + 8UL);
                    var_35 = var_34 - r14_0;
                    var_36 = r12_0 + var_35;
                    var_37 = helper_cc_compute_all_wrapper(var_35, 0UL, 0UL, 25U);
                    r8_10 = r12_0;
                    r14_0 = var_34;
                    r12_0 = var_36;
                    local_sp_6 = local_sp_2;
                    local_sp_3 = local_sp_2;
                    r8_2 = r12_0;
                    rcx_2 = rcx_1;
                    r9_0 = r14_0;
                    local_sp_4 = local_sp_2;
                    r8_3 = r12_0;
                    rcx_3 = rcx_1;
                    r9_1 = r14_0;
                    r12_1 = var_36;
                    r14_1 = var_34;
                    if ((uint64_t)(((unsigned char)(var_37 >> 4UL) ^ (unsigned char)var_37) & '\xc0') == 0UL) {
                        var_46 = *(uint64_t *)(local_sp_6 + 40UL);
                        var_47 = *(uint64_t *)(local_sp_6 + 32UL);
                        var_48 = local_sp_6 + (-8L);
                        *(uint64_t *)var_48 = 4265039UL;
                        var_49 = indirect_placeholder_253(var_47, var_33, rbx_1, var_46, var_22);
                        var_50 = var_49.field_0;
                        var_51 = *var_31;
                        r8_10 = var_47;
                        rax_2 = var_50;
                        rcx_1 = var_51;
                        local_sp_2 = var_48;
                        local_sp_5 = var_48;
                        r15_1 = var_51;
                        r8_4 = var_47;
                        if ((uint64_t)((uint32_t)var_50 & (-2)) != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        var_52 = r13_0 + 1UL;
                        r13_0 = var_52;
                        if ((long)*var_23 > (long)var_52) {
                            continue;
                        }
                        loop_state_var = 1U;
                        break;
                    }
                    if ((long)*var_29 < (long)var_36) {
                        _pre_phi223 = (uint64_t)(uint32_t)var_32;
                        _pre_phi = (uint64_t *)local_sp_2;
                    } else {
                        var_38 = (uint64_t *)(local_sp_2 + 8UL);
                        *var_38 = var_35;
                        if ((long)*var_30 >= (long)var_36) {
                            loop_state_var = 2U;
                            break;
                        }
                        *(uint64_t *)(local_sp_2 + 24UL) = r12_0;
                        var_39 = (uint64_t *)(local_sp_2 + 16UL);
                        *var_39 = r14_0;
                        var_40 = local_sp_2 + (-8L);
                        var_41 = (uint64_t *)var_40;
                        *var_41 = 4265100UL;
                        var_42 = indirect_placeholder_1(rbx_1, var_36);
                        _pre_phi = var_41;
                        local_sp_3 = var_40;
                        rax_2 = var_42;
                        if ((uint64_t)(uint32_t)var_42 != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        r8_2 = *var_39;
                        rcx_2 = *var_31;
                        r9_0 = *var_38;
                    }
                    *(uint64_t *)(local_sp_3 + 24UL) = r9_0;
                    var_43 = (uint64_t *)(local_sp_3 + 16UL);
                    *var_43 = r8_2;
                    var_44 = (uint64_t *)(local_sp_3 + 8UL);
                    *var_44 = rcx_2;
                    var_45 = local_sp_3 + (-8L);
                    *(uint64_t *)var_45 = 4264992UL;
                    indirect_placeholder();
                    local_sp_6 = var_45;
                    local_sp_4 = var_45;
                    r8_3 = *var_44;
                    rcx_3 = *_pre_phi;
                    r9_1 = *var_43;
                    if (_pre_phi223 != 0UL) {
                        loop_state_var = 2U;
                        break;
                    }
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 1U:
                {
                    var_53 = r14_1 + 1UL;
                    local_sp_7 = local_sp_5;
                    r15_2 = r15_1;
                    r8_5 = r8_4;
                    rcx_4 = r12_1;
                    r9_2 = var_53;
                    r14_2 = r9_2;
                    r15_6 = r15_2;
                    r8_8 = r8_5;
                    local_sp_12 = local_sp_7;
                    local_sp_8 = local_sp_7;
                    r15_3 = r15_2;
                    r12_2 = rcx_4;
                    r8_6 = r8_5;
                    if ((long)r9_2 > (long)*(uint64_t *)(local_sp_7 + 32UL)) {
                        var_94 = (uint64_t *)local_sp_12;
                        var_95 = *var_94 + 1UL;
                        *var_94 = var_95;
                        r8_10 = r8_8;
                        rax_2 = 0UL;
                        var_19 = var_95;
                        local_sp_1 = local_sp_12;
                        r15_0 = r15_6;
                        rbx_1 = rbx_4;
                        r8_1 = r8_8;
                        if ((long)*(uint64_t *)(rbx_4 + 232UL) > (long)var_95) {
                            switch_state_var = 1;
                            break;
                        }
                        rdx2_0 = **(uint64_t **)(local_sp_12 + 56UL);
                        continue;
                    }
                    while (1U)
                        {
                            var_54 = (uint64_t *)rbp_1;
                            var_55 = helper_cc_compute_all_wrapper(r14_2 - *var_54, 0UL, 0UL, 25U);
                            local_sp_9 = local_sp_8;
                            rbp_2 = rbp_1;
                            r15_5 = r15_3;
                            r8_10 = r8_6;
                            rbx_0 = rbx_2;
                            rbp_0 = rbp_1;
                            r15_6 = r15_3;
                            rbx_4 = rbx_2;
                            r8_8 = r8_6;
                            local_sp_12 = local_sp_8;
                            rbx_3 = rbx_2;
                            r12_3 = r12_2;
                            r8_7 = r8_6;
                            local_sp_10 = local_sp_8;
                            r15_4 = r15_3;
                            if ((uint64_t)(((unsigned char)(var_55 >> 4UL) ^ (unsigned char)var_55) & '\xc0') != 0UL) {
                                if ((long)*(uint64_t *)(rbx_2 + 48UL) <= (long)r12_2) {
                                    if ((long)*(uint64_t *)(rbx_2 + 88UL) <= (long)r12_2) {
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    var_56 = (uint64_t)((uint32_t)r12_2 + 1U);
                                    var_57 = local_sp_8 + (-8L);
                                    *(uint64_t *)var_57 = 4265495UL;
                                    var_58 = indirect_placeholder_254(rbx_2, var_56);
                                    var_59 = var_58.field_0;
                                    local_sp_9 = var_57;
                                    rax_2 = var_59;
                                    if ((uint64_t)(uint32_t)var_59 != 0UL) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    r15_4 = *(uint64_t *)(rbx_2 + 8UL);
                                }
                                r15_5 = r15_4;
                                r15_6 = r15_4;
                                local_sp_12 = local_sp_9;
                                r12_3 = r12_2 + 1UL;
                                local_sp_10 = local_sp_9;
                                if ((uint64_t)(*(unsigned char *)(r12_2 + r15_4) - *(unsigned char *)((r14_2 + r15_4) + (-1L))) != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                            }
                            var_60 = *(uint64_t *)((r14_2 << 3UL) + *(uint64_t *)(rbx_2 + 184UL));
                            r15_3 = r15_5;
                            r12_2 = r12_3;
                            local_sp_11 = local_sp_10;
                            r15_6 = r15_5;
                            var_61 = *(uint64_t *)(var_60 + 16UL);
                            var_62 = *(uint64_t *)(local_sp_10 + 48UL);
                            var_63 = var_60 + 24UL;
                            var_64 = *(uint64_t *)(local_sp_10 + 56UL);
                            var_65 = local_sp_10 + (-8L);
                            *(uint64_t *)var_65 = 4265360UL;
                            var_66 = indirect_placeholder_255(9UL, var_63, var_64, var_62, var_61);
                            var_67 = var_66.field_0;
                            var_68 = var_66.field_1;
                            var_69 = var_66.field_3;
                            var_70 = var_66.field_4;
                            local_sp_0 = var_65;
                            local_sp_11 = var_65;
                            r8_7 = var_68;
                            if (var_60 != 0UL & var_67 != 18446744073709551615UL) {
                                var_71 = *(uint64_t *)(rbp_1 + 16UL);
                                var_72 = *var_54;
                                rcx_0 = var_72;
                                rsi4_0 = var_71;
                                if (var_71 != 0UL) {
                                    var_73 = (r14_2 - var_72) + 1UL;
                                    var_74 = local_sp_10 + (-16L);
                                    *(uint64_t *)var_74 = 4265406UL;
                                    var_75 = indirect_placeholder_250(rbx_2, var_68, rbp_1, 24UL, var_72, var_69, var_73, var_70);
                                    var_76 = var_75.field_0;
                                    var_77 = var_75.field_3;
                                    *(uint64_t *)(var_77 + 16UL) = var_76;
                                    local_sp_0 = var_74;
                                    rbp_0 = var_77;
                                    rsi4_0 = var_76;
                                    if (var_76 != 0UL) {
                                        r8_10 = var_75.field_2;
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    rbx_0 = var_75.field_1;
                                    rcx_0 = *(uint64_t *)var_77;
                                }
                                var_78 = *(uint64_t *)(rbp_0 + 8UL);
                                var_79 = (uint64_t *)(local_sp_0 + (-16L));
                                *var_79 = 9UL;
                                var_80 = local_sp_0 + (-24L);
                                var_81 = (uint64_t *)var_80;
                                *var_81 = 4265192UL;
                                var_82 = indirect_placeholder_252(var_67, var_78, rbx_0, rcx_0, rsi4_0, r14_2);
                                var_83 = var_82.field_0;
                                var_84 = var_82.field_1;
                                var_85 = var_82.field_2;
                                var_86 = var_82.field_3;
                                var_87 = *var_79;
                                var_88 = local_sp_0 + (-8L);
                                var_89 = (uint32_t)var_83;
                                rbp_2 = rbp_0;
                                r8_10 = var_84;
                                local_sp_11 = var_88;
                                rax_2 = var_83;
                                rbx_3 = rbx_0;
                                r8_7 = var_84;
                                if ((uint64_t)(var_89 + (-1)) != 0UL) {
                                    local_sp_11 = var_80;
                                    if ((uint64_t)var_89 != 0UL) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    *var_79 = 4265221UL;
                                    var_90 = indirect_placeholder_12(var_84, r14_2, rbp_0, var_87, var_85, var_67, var_86);
                                    rax_2 = 12UL;
                                    if (var_90 != 0UL) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    var_91 = *(uint64_t *)(local_sp_0 + 16UL);
                                    var_92 = *(uint64_t *)(local_sp_0 + 24UL);
                                    *var_81 = 4265254UL;
                                    indirect_placeholder_251(var_91, var_90, rbx_0, var_92, rbp_0);
                                    r8_7 = var_91;
                                }
                            }
                            var_93 = r14_2 + 1UL;
                            r14_2 = var_93;
                            rbx_4 = rbx_3;
                            r8_8 = r8_7;
                            local_sp_12 = local_sp_11;
                            local_sp_8 = local_sp_11;
                            rbx_2 = rbx_3;
                            r8_6 = r8_7;
                            rbp_1 = rbp_2;
                            if ((long)*(uint64_t *)(local_sp_11 + 32UL) < (long)var_93) {
                                continue;
                            }
                            loop_state_var = 1U;
                            break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 0U:
                        {
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 1U:
                        {
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
                break;
              case 2U:
                {
                    local_sp_5 = local_sp_4;
                    r15_6 = rcx_3;
                    r8_8 = r8_3;
                    local_sp_12 = local_sp_4;
                    local_sp_7 = local_sp_4;
                    r15_1 = rcx_3;
                    r12_1 = r8_3;
                    r14_1 = r9_1;
                    r8_4 = r8_3;
                    r15_2 = rcx_3;
                    r8_5 = r8_3;
                    rcx_4 = r8_3;
                    r9_2 = r9_1;
                    if ((long)*var_23 <= (long)r13_0) {
                        var_94 = (uint64_t *)local_sp_12;
                        var_95 = *var_94 + 1UL;
                        *var_94 = var_95;
                        r8_10 = r8_8;
                        rax_2 = 0UL;
                        var_19 = var_95;
                        local_sp_1 = local_sp_12;
                        r15_0 = r15_6;
                        rbx_1 = rbx_4;
                        r8_1 = r8_8;
                        if ((long)*(uint64_t *)(rbx_4 + 232UL) > (long)var_95) {
                            switch_state_var = 1;
                            break;
                        }
                        rdx2_0 = **(uint64_t **)(local_sp_12 + 56UL);
                        continue;
                    }
                    if (r13_0 == 0UL) {
                        var_53 = r14_1 + 1UL;
                        local_sp_7 = local_sp_5;
                        r15_2 = r15_1;
                        r8_5 = r8_4;
                        rcx_4 = r12_1;
                        r9_2 = var_53;
                    }
                    r14_2 = r9_2;
                    r15_6 = r15_2;
                    r8_8 = r8_5;
                    local_sp_12 = local_sp_7;
                    local_sp_8 = local_sp_7;
                    r15_3 = r15_2;
                    r12_2 = rcx_4;
                    r8_6 = r8_5;
                    if ((long)r9_2 > (long)*(uint64_t *)(local_sp_7 + 32UL)) {
                        var_94 = (uint64_t *)local_sp_12;
                        var_95 = *var_94 + 1UL;
                        *var_94 = var_95;
                        r8_10 = r8_8;
                        rax_2 = 0UL;
                        var_19 = var_95;
                        local_sp_1 = local_sp_12;
                        r15_0 = r15_6;
                        rbx_1 = rbx_4;
                        r8_1 = r8_8;
                        if ((long)*(uint64_t *)(rbx_4 + 232UL) > (long)var_95) {
                            switch_state_var = 1;
                            break;
                        }
                        rdx2_0 = **(uint64_t **)(local_sp_12 + 56UL);
                        continue;
                    }
                    while (1U)
                        {
                            var_54 = (uint64_t *)rbp_1;
                            var_55 = helper_cc_compute_all_wrapper(r14_2 - *var_54, 0UL, 0UL, 25U);
                            local_sp_9 = local_sp_8;
                            rbp_2 = rbp_1;
                            r15_5 = r15_3;
                            r8_10 = r8_6;
                            rbx_0 = rbx_2;
                            rbp_0 = rbp_1;
                            r15_6 = r15_3;
                            rbx_4 = rbx_2;
                            r8_8 = r8_6;
                            local_sp_12 = local_sp_8;
                            rbx_3 = rbx_2;
                            r12_3 = r12_2;
                            r8_7 = r8_6;
                            local_sp_10 = local_sp_8;
                            r15_4 = r15_3;
                            if ((uint64_t)(((unsigned char)(var_55 >> 4UL) ^ (unsigned char)var_55) & '\xc0') != 0UL) {
                                if ((long)*(uint64_t *)(rbx_2 + 48UL) <= (long)r12_2) {
                                    if ((long)*(uint64_t *)(rbx_2 + 88UL) <= (long)r12_2) {
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    var_56 = (uint64_t)((uint32_t)r12_2 + 1U);
                                    var_57 = local_sp_8 + (-8L);
                                    *(uint64_t *)var_57 = 4265495UL;
                                    var_58 = indirect_placeholder_254(rbx_2, var_56);
                                    var_59 = var_58.field_0;
                                    local_sp_9 = var_57;
                                    rax_2 = var_59;
                                    if ((uint64_t)(uint32_t)var_59 != 0UL) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    r15_4 = *(uint64_t *)(rbx_2 + 8UL);
                                }
                                r15_5 = r15_4;
                                r15_6 = r15_4;
                                local_sp_12 = local_sp_9;
                                r12_3 = r12_2 + 1UL;
                                local_sp_10 = local_sp_9;
                                if ((uint64_t)(*(unsigned char *)(r12_2 + r15_4) - *(unsigned char *)((r14_2 + r15_4) + (-1L))) != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                            }
                            var_60 = *(uint64_t *)((r14_2 << 3UL) + *(uint64_t *)(rbx_2 + 184UL));
                            r15_3 = r15_5;
                            r12_2 = r12_3;
                            local_sp_11 = local_sp_10;
                            r15_6 = r15_5;
                            var_61 = *(uint64_t *)(var_60 + 16UL);
                            var_62 = *(uint64_t *)(local_sp_10 + 48UL);
                            var_63 = var_60 + 24UL;
                            var_64 = *(uint64_t *)(local_sp_10 + 56UL);
                            var_65 = local_sp_10 + (-8L);
                            *(uint64_t *)var_65 = 4265360UL;
                            var_66 = indirect_placeholder_255(9UL, var_63, var_64, var_62, var_61);
                            var_67 = var_66.field_0;
                            var_68 = var_66.field_1;
                            var_69 = var_66.field_3;
                            var_70 = var_66.field_4;
                            local_sp_0 = var_65;
                            local_sp_11 = var_65;
                            r8_7 = var_68;
                            if (var_60 != 0UL & var_67 != 18446744073709551615UL) {
                                var_71 = *(uint64_t *)(rbp_1 + 16UL);
                                var_72 = *var_54;
                                rcx_0 = var_72;
                                rsi4_0 = var_71;
                                if (var_71 != 0UL) {
                                    var_73 = (r14_2 - var_72) + 1UL;
                                    var_74 = local_sp_10 + (-16L);
                                    *(uint64_t *)var_74 = 4265406UL;
                                    var_75 = indirect_placeholder_250(rbx_2, var_68, rbp_1, 24UL, var_72, var_69, var_73, var_70);
                                    var_76 = var_75.field_0;
                                    var_77 = var_75.field_3;
                                    *(uint64_t *)(var_77 + 16UL) = var_76;
                                    local_sp_0 = var_74;
                                    rbp_0 = var_77;
                                    rsi4_0 = var_76;
                                    if (var_76 != 0UL) {
                                        r8_10 = var_75.field_2;
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    rbx_0 = var_75.field_1;
                                    rcx_0 = *(uint64_t *)var_77;
                                }
                                var_78 = *(uint64_t *)(rbp_0 + 8UL);
                                var_79 = (uint64_t *)(local_sp_0 + (-16L));
                                *var_79 = 9UL;
                                var_80 = local_sp_0 + (-24L);
                                var_81 = (uint64_t *)var_80;
                                *var_81 = 4265192UL;
                                var_82 = indirect_placeholder_252(var_67, var_78, rbx_0, rcx_0, rsi4_0, r14_2);
                                var_83 = var_82.field_0;
                                var_84 = var_82.field_1;
                                var_85 = var_82.field_2;
                                var_86 = var_82.field_3;
                                var_87 = *var_79;
                                var_88 = local_sp_0 + (-8L);
                                var_89 = (uint32_t)var_83;
                                rbp_2 = rbp_0;
                                r8_10 = var_84;
                                local_sp_11 = var_88;
                                rax_2 = var_83;
                                rbx_3 = rbx_0;
                                r8_7 = var_84;
                                if ((uint64_t)(var_89 + (-1)) != 0UL) {
                                    local_sp_11 = var_80;
                                    if ((uint64_t)var_89 != 0UL) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    *var_79 = 4265221UL;
                                    var_90 = indirect_placeholder_12(var_84, r14_2, rbp_0, var_87, var_85, var_67, var_86);
                                    rax_2 = 12UL;
                                    if (var_90 != 0UL) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    var_91 = *(uint64_t *)(local_sp_0 + 16UL);
                                    var_92 = *(uint64_t *)(local_sp_0 + 24UL);
                                    *var_81 = 4265254UL;
                                    indirect_placeholder_251(var_91, var_90, rbx_0, var_92, rbp_0);
                                    r8_7 = var_91;
                                }
                            }
                            var_93 = r14_2 + 1UL;
                            r14_2 = var_93;
                            rbx_4 = rbx_3;
                            r8_8 = r8_7;
                            local_sp_12 = local_sp_11;
                            local_sp_8 = local_sp_11;
                            rbx_2 = rbx_3;
                            r8_6 = r8_7;
                            rbp_1 = rbp_2;
                            if ((long)*(uint64_t *)(local_sp_11 + 32UL) < (long)var_93) {
                                continue;
                            }
                            loop_state_var = 1U;
                            break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 0U:
                        {
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 1U:
                        {
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    mrv.field_0 = rax_2;
    mrv1 = mrv;
    mrv1.field_1 = r8_10;
    return mrv1;
}
