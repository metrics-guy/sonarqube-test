typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_285_ret_type;
struct indirect_placeholder_284_ret_type;
struct indirect_placeholder_286_ret_type;
struct indirect_placeholder_283_ret_type;
struct indirect_placeholder_287_ret_type;
struct indirect_placeholder_285_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_284_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_286_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_283_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_287_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r15(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_14(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_285_ret_type indirect_placeholder_285(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_284_ret_type indirect_placeholder_284(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_286_ret_type indirect_placeholder_286(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_283_ret_type indirect_placeholder_283(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_287_ret_type indirect_placeholder_287(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
uint64_t bb_transit_state_mb_isra_0(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t *var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint32_t *var_62;
    uint32_t var_63;
    uint64_t var_39;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t local_sp_3;
    uint64_t r8_0;
    uint64_t local_sp_0;
    uint32_t var_86;
    uint64_t rax_0;
    uint64_t var_64;
    uint64_t var_65;
    struct indirect_placeholder_285_ret_type var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    struct indirect_placeholder_284_ret_type var_71;
    uint64_t var_72;
    uint64_t *var_73;
    uint64_t local_sp_2;
    uint64_t var_40;
    uint64_t var_41;
    uint32_t var_42;
    uint64_t var_43;
    uint64_t *var_44;
    uint64_t var_45;
    uint32_t *var_46;
    uint32_t var_47;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t *var_79;
    struct indirect_placeholder_286_ret_type var_80;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t var_83;
    struct indirect_placeholder_283_ret_type var_84;
    uint64_t var_85;
    uint64_t var_10;
    uint64_t *var_11;
    uint64_t *var_12;
    uint64_t *var_13;
    uint32_t *var_14;
    uint32_t *var_15;
    uint64_t *var_16;
    uint64_t *var_17;
    uint64_t *var_18;
    uint64_t local_sp_1;
    uint64_t r15_0;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint32_t var_23;
    uint64_t var_24;
    uint32_t var_25;
    uint64_t var_26;
    uint64_t *var_27;
    uint64_t var_28;
    struct indirect_placeholder_287_ret_type var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint32_t var_33;
    uint64_t var_34;
    uint64_t r10_0;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t *var_38;
    uint64_t var_87;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r15();
    var_2 = init_r12();
    var_3 = init_rbx();
    var_4 = init_r13();
    var_5 = init_r14();
    var_6 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_2;
    *(uint64_t *)(var_0 + (-40L)) = var_6;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_7 = *(uint64_t *)(rdi + 152UL);
    var_8 = (uint64_t *)rsi;
    var_9 = *var_8;
    *(uint64_t *)(var_0 + (-112L)) = (var_7 + 216UL);
    rax_0 = 0UL;
    r15_0 = 0UL;
    if ((long)var_9 > (long)0UL) {
        return rax_0;
    }
    var_10 = var_0 + (-136L);
    var_11 = (uint64_t *)rdx;
    var_12 = (uint64_t *)var_7;
    var_13 = (uint64_t *)(rdi + 72UL);
    var_14 = (uint32_t *)(rdi + 160UL);
    var_15 = (uint32_t *)(rdi + 224UL);
    var_16 = (uint64_t *)(var_7 + 24UL);
    var_17 = (uint64_t *)(rdi + 184UL);
    var_18 = (uint64_t *)(var_7 + 48UL);
    local_sp_1 = var_10;
    while (1U)
        {
            var_19 = *var_11;
            var_20 = *var_12;
            var_21 = *(uint64_t *)((r15_0 << 3UL) + var_19);
            var_22 = (var_21 << 4UL) + var_20;
            local_sp_2 = local_sp_1;
            r10_0 = var_20;
            local_sp_3 = local_sp_1;
            if ((*(unsigned char *)(var_22 + 10UL) & '\x10') == '\x00') {
                var_87 = r15_0 + 1UL;
                local_sp_1 = local_sp_3;
                r15_0 = var_87;
                if ((long)var_87 >= (long)*var_8) {
                    continue;
                }
                break;
            }
            var_23 = *(uint32_t *)(var_22 + 8UL);
            var_24 = *var_13;
            var_25 = var_23 & 261888U;
            *(uint32_t *)local_sp_1 = var_23;
            r8_0 = var_24;
            if (var_25 != 0U) {
                var_26 = (uint64_t)*var_14;
                *(uint64_t *)(local_sp_1 + 16UL) = var_20;
                var_27 = (uint64_t *)(local_sp_1 + 8UL);
                *var_27 = var_24;
                var_28 = local_sp_1 + (-8L);
                *(uint64_t *)var_28 = 4267515UL;
                var_29 = indirect_placeholder_287(var_22, var_26, rdi, var_24);
                var_30 = var_29.field_0;
                var_31 = *(uint64_t *)local_sp_1;
                var_32 = *var_27;
                var_33 = *(uint32_t *)var_28 >> 8U;
                var_34 = (uint64_t)var_33;
                local_sp_2 = var_28;
                r8_0 = var_31;
                r10_0 = var_32;
                local_sp_3 = var_28;
                if ((var_34 & 4UL) == 0UL) {
                    if (!(((var_34 & 8UL) == 0UL) || ((var_30 & 1UL) == 0UL))) {
                        var_87 = r15_0 + 1UL;
                        local_sp_1 = local_sp_3;
                        r15_0 = var_87;
                        if ((long)var_87 >= (long)*var_8) {
                            continue;
                        }
                        break;
                    }
                }
                if (!(((var_30 & 1UL) != 0UL) && ((var_34 & 8UL) == 0UL))) {
                    var_87 = r15_0 + 1UL;
                    local_sp_1 = local_sp_3;
                    r15_0 = var_87;
                    if ((long)var_87 >= (long)*var_8) {
                        continue;
                    }
                    break;
                }
                if (!(((var_34 & 32UL) != 0UL) && ((var_30 & 2UL) == 0UL))) {
                    var_87 = r15_0 + 1UL;
                    local_sp_1 = local_sp_3;
                    r15_0 = var_87;
                    if ((long)var_87 >= (long)*var_8) {
                        continue;
                    }
                    break;
                }
                if (!(((signed char)(unsigned char)var_33 <= '\xff') && ((var_30 & 8UL) == 0UL))) {
                    var_87 = r15_0 + 1UL;
                    local_sp_1 = local_sp_3;
                    r15_0 = var_87;
                    if ((long)var_87 >= (long)*var_8) {
                        continue;
                    }
                    break;
                }
            }
            var_35 = local_sp_2 + 24UL;
            var_36 = *(uint64_t *)var_35;
            var_37 = local_sp_2 + (-8L);
            var_38 = (uint64_t *)var_37;
            *var_38 = 4267187UL;
            var_39 = indirect_placeholder_14(r8_0, var_21, r10_0, rdi, var_36);
            local_sp_3 = var_37;
            if ((uint64_t)(uint32_t)var_39 != 0UL) {
                var_40 = (uint64_t)((long)(var_39 << 32UL) >> (long)32UL);
                var_41 = var_40 + *var_13;
                var_42 = *var_15;
                var_43 = ((long)var_40 > (long)(uint64_t)var_42) ? var_39 : (uint64_t)var_42;
                *var_38 = var_41;
                *var_15 = (uint32_t)var_43;
                var_44 = (uint64_t *)(local_sp_2 + (-16L));
                *var_44 = 4267236UL;
                var_45 = indirect_placeholder_1(rdi, var_41);
                var_46 = (uint32_t *)(local_sp_2 + 28UL);
                var_47 = (uint32_t)var_45;
                *var_46 = var_47;
                rax_0 = var_45;
                if ((uint64_t)var_47 == 0UL) {
                    break;
                }
                var_48 = *var_16;
                var_49 = *var_44;
                var_50 = *var_17;
                var_51 = *(uint64_t *)((var_21 << 3UL) + var_48);
                var_52 = var_49 << 3UL;
                var_53 = var_50 + var_52;
                var_54 = *var_18;
                *var_44 = var_53;
                var_55 = (var_51 * 24UL) + var_54;
                var_56 = *(uint64_t *)var_53;
                if (var_56 == 0UL) {
                    var_74 = *(uint64_t *)var_55;
                    var_75 = *(uint64_t *)(var_55 + 8UL);
                    var_76 = var_49 + (-1L);
                    *var_38 = var_49;
                    *(uint64_t *)(local_sp_2 + 32UL) = var_74;
                    *(uint64_t *)(local_sp_2 + 40UL) = var_75;
                    var_77 = *(uint64_t *)(var_55 + 16UL);
                    var_78 = (uint64_t)*var_14;
                    *(uint64_t *)(local_sp_2 + 48UL) = var_77;
                    var_79 = (uint64_t *)(local_sp_2 + (-24L));
                    *var_79 = 4267617UL;
                    var_80 = indirect_placeholder_286(var_77, var_78, rdi, var_76);
                    var_81 = local_sp_2 + 20UL;
                    var_82 = (uint64_t)(uint32_t)var_80.field_0;
                    var_83 = local_sp_2 + (-32L);
                    *(uint64_t *)var_83 = 4267637UL;
                    var_84 = indirect_placeholder_283(var_35, var_81, var_82, var_7);
                    var_85 = *var_79;
                    **(uint64_t **)var_83 = var_84.field_0;
                    local_sp_0 = var_83;
                    local_sp_3 = var_83;
                    if (*(uint64_t *)((var_85 << 3UL) + *var_17) != 0UL) {
                        var_87 = r15_0 + 1UL;
                        local_sp_1 = local_sp_3;
                        r15_0 = var_87;
                        if ((long)var_87 >= (long)*var_8) {
                            continue;
                        }
                        break;
                    }
                }
                var_57 = *(uint64_t *)(var_56 + 80UL);
                var_58 = local_sp_2 + 32UL;
                *var_44 = var_49;
                var_59 = (uint64_t *)(local_sp_2 + (-24L));
                *var_59 = 4267324UL;
                var_60 = indirect_placeholder_7(var_55, var_58, var_57);
                var_61 = *var_59;
                var_62 = (uint32_t *)(local_sp_2 + 20UL);
                var_63 = (uint32_t)var_60;
                *var_62 = var_63;
                rax_0 = var_60;
                if ((uint64_t)var_63 == 0UL) {
                    break;
                }
                var_64 = (uint64_t)*var_14;
                var_65 = var_61 + (-1L);
                *var_59 = var_61;
                *(uint64_t *)(local_sp_2 + (-32L)) = 4267362UL;
                var_66 = indirect_placeholder_285(var_60, var_64, rdi, var_65);
                var_67 = local_sp_2 + 16UL;
                var_68 = local_sp_2 + 12UL;
                var_69 = (uint64_t)(uint32_t)var_66.field_0;
                var_70 = var_52 + *var_17;
                *(uint64_t *)(local_sp_2 + (-40L)) = 4267389UL;
                var_71 = indirect_placeholder_284(var_67, var_68, var_69, var_7);
                *(uint64_t *)var_70 = var_71.field_0;
                var_72 = local_sp_2 + (-48L);
                var_73 = (uint64_t *)var_72;
                *var_73 = 4267402UL;
                indirect_placeholder();
                local_sp_0 = var_72;
                local_sp_3 = var_72;
                if (*(uint64_t *)((*var_73 << 3UL) + *var_17) != 0UL) {
                    var_87 = r15_0 + 1UL;
                    local_sp_1 = local_sp_3;
                    r15_0 = var_87;
                    if ((long)var_87 >= (long)*var_8) {
                        continue;
                    }
                    break;
                }
                var_86 = *(uint32_t *)(local_sp_0 + 44UL);
                local_sp_3 = local_sp_0;
                if (var_86 != 0U) {
                    rax_0 = (uint64_t)var_86;
                    break;
                }
            }
        }
    return rax_0;
}
