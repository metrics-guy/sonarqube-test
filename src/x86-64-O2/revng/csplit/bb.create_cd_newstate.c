typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_179_ret_type;
struct indirect_placeholder_180_ret_type;
struct indirect_placeholder_179_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_180_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r15(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_23(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_8(uint64_t param_0);
extern struct indirect_placeholder_179_ret_type indirect_placeholder_179(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
extern struct indirect_placeholder_180_ret_type indirect_placeholder_180(uint64_t param_0);
uint64_t bb_create_cd_newstate(uint64_t r8, uint64_t rdx, uint64_t rdi, uint64_t rcx, uint64_t r10, uint64_t rsi, uint64_t r9) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t *var_8;
    struct indirect_placeholder_179_ret_type var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t r15_0;
    uint64_t r81_0;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t local_sp_4;
    uint64_t local_sp_0;
    unsigned char *var_17;
    unsigned char var_18;
    uint64_t *var_19;
    uint64_t *var_20;
    uint64_t *var_21;
    uint64_t *var_22;
    bool var_23;
    bool var_24;
    bool var_25;
    uint64_t var_26;
    uint64_t r105_2;
    uint64_t r13_0;
    uint64_t local_sp_3;
    uint64_t r105_0;
    uint64_t local_sp_1;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint32_t var_30;
    uint64_t var_31;
    uint32_t var_32;
    uint64_t var_33;
    uint32_t var_34;
    bool var_35;
    bool var_36;
    unsigned char var_37;
    unsigned char var_38;
    uint64_t r105_1;
    uint64_t local_sp_2;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_39;
    struct indirect_placeholder_180_ret_type var_40;
    uint64_t var_41;
    uint32_t *var_42;
    uint32_t var_43;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r15();
    var_2 = init_r12();
    var_3 = init_rbx();
    var_4 = init_r13();
    var_5 = init_r14();
    var_6 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_2;
    *(uint64_t *)(var_0 + (-40L)) = var_6;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_7 = (uint64_t)(uint32_t)rdx;
    *(uint64_t *)(var_0 + (-64L)) = rcx;
    var_8 = (uint64_t *)(var_0 + (-112L));
    *var_8 = 4248986UL;
    var_9 = indirect_placeholder_179(var_7, r8, var_6, 112UL, rcx, r10, 1UL, r9);
    var_10 = var_9.field_0;
    var_11 = var_9.field_1;
    r13_0 = 0UL;
    r105_0 = 0UL;
    r15_0 = 0UL;
    if (var_10 == 0UL) {
        return r15_0;
    }
    var_12 = var_10 + 8UL;
    var_13 = rsi + 16UL;
    var_14 = rsi + 8UL;
    *(uint64_t *)(var_0 + (-80L)) = var_13;
    *(uint64_t *)(var_0 + (-88L)) = var_14;
    var_15 = var_0 + (-120L);
    *(uint64_t *)var_15 = 4249033UL;
    var_16 = indirect_placeholder_7(var_13, var_12, var_14);
    local_sp_1 = var_15;
    local_sp_4 = var_15;
    r15_0 = var_10;
    if ((uint64_t)(uint32_t)var_16 == 0UL) {
        *(uint64_t *)(var_0 + (-128L)) = 4249505UL;
        indirect_placeholder();
    } else {
        var_17 = (unsigned char *)(var_10 + 104UL);
        var_18 = *var_17;
        var_19 = (uint64_t *)(var_10 + 80UL);
        *var_19 = var_12;
        *var_17 = ((var_18 & '\xf0') | ((unsigned char)var_11 & '\x0f'));
        var_20 = (uint64_t *)var_14;
        if ((long)*var_20 <= (long)0UL) {
            *var_8 = (var_10 + 24UL);
            var_21 = (uint64_t *)var_13;
            var_22 = (uint64_t *)rdi;
            var_23 = ((var_11 & 1UL) == 0UL);
            var_24 = ((var_11 & 2UL) == 0UL);
            var_25 = ((var_11 & 4UL) == 0UL);
            var_26 = var_10 + 16UL;
            while (1U)
                {
                    var_27 = (*(uint64_t *)((r13_0 << 3UL) + *var_21) << 4UL) + *var_22;
                    var_28 = var_27 + 8UL;
                    var_29 = (uint64_t)*(unsigned char *)var_28;
                    var_30 = *(uint32_t *)var_28 >> 8U;
                    var_31 = (uint64_t)((uint16_t)(uint64_t)var_30 & (unsigned short)1023U);
                    var_32 = var_30 & 16712703U;
                    var_33 = (uint64_t)var_32;
                    var_34 = (uint32_t)var_29;
                    var_35 = ((uint64_t)(var_34 + (-1)) == 0UL);
                    var_36 = (var_31 == 0UL);
                    r81_0 = var_33;
                    r105_2 = r105_0;
                    local_sp_3 = local_sp_1;
                    r105_1 = r105_0;
                    local_sp_2 = local_sp_1;
                    if (var_35 && var_36) {
                        var_52 = r13_0 + 1UL;
                        r13_0 = var_52;
                        r105_0 = r105_2;
                        local_sp_1 = local_sp_3;
                        local_sp_4 = local_sp_3;
                        if ((long)*var_20 > (long)var_52) {
                            continue;
                        }
                        loop_state_var = 1U;
                        break;
                    }
                    var_37 = *var_17;
                    var_38 = (var_37 & '\xdf') | (unsigned char)((((uint64_t)(*(unsigned char *)(var_27 + 10UL) >> '\x04') << 5UL) | (uint64_t)var_37) & 32UL);
                    *var_17 = var_38;
                    if ((uint64_t)(var_34 + (-2)) == 0UL) {
                        *var_17 = (var_38 | '\x10');
                    } else {
                        if ((uint64_t)(var_34 + (-4)) == 0UL) {
                            *var_17 = (var_38 | '@');
                        }
                    }
                    if (!var_36) {
                        if (var_12 != *var_19) {
                            *(uint32_t *)(local_sp_1 + 20UL) = var_32;
                            var_39 = local_sp_1 + (-8L);
                            *(uint64_t *)var_39 = 4249375UL;
                            var_40 = indirect_placeholder_180(24UL);
                            var_41 = var_40.field_0;
                            var_42 = (uint32_t *)(local_sp_1 + 12UL);
                            var_43 = *var_42;
                            *var_19 = var_41;
                            local_sp_0 = var_39;
                            r105_1 = 0UL;
                            if (var_41 != 0UL) {
                                loop_state_var = 2U;
                                break;
                            }
                            var_44 = *(uint64_t *)(local_sp_1 + 24UL);
                            var_45 = *(uint64_t *)(local_sp_1 + 16UL);
                            *var_42 = var_43;
                            var_46 = local_sp_1 + (-16L);
                            *(uint64_t *)var_46 = 4249412UL;
                            var_47 = indirect_placeholder_7(var_44, var_41, var_45);
                            local_sp_2 = var_46;
                            if ((uint64_t)(uint32_t)var_47 != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            *var_17 = (*var_17 | '\x80');
                            r81_0 = (uint64_t)*(uint32_t *)(local_sp_1 + 4UL);
                        }
                        r105_2 = r105_1;
                        local_sp_3 = local_sp_2;
                        if ((r81_0 & 1UL) == 0UL) {
                            if (!(((r81_0 & 2UL) == 0UL) || var_23) && !(((r81_0 & 16UL) == 0UL) || (var_24 ^ 1)) && !(((r81_0 & 64UL) == 0UL) || (var_25 ^ 1))) {
                                var_52 = r13_0 + 1UL;
                                r13_0 = var_52;
                                r105_0 = r105_2;
                                local_sp_1 = local_sp_3;
                                local_sp_4 = local_sp_3;
                                if ((long)*var_20 > (long)var_52) {
                                    continue;
                                }
                                loop_state_var = 1U;
                                break;
                            }
                        }
                        if (!((var_23 ^ 1) && ((r81_0 & 2UL) == 0UL))) {
                            if (!(((r81_0 & 16UL) == 0UL) || (var_24 ^ 1))) {
                                if (((r81_0 & 64UL) == 0UL) || (var_25 ^ 1)) {
                                    var_52 = r13_0 + 1UL;
                                    r13_0 = var_52;
                                    r105_0 = r105_2;
                                    local_sp_1 = local_sp_3;
                                    local_sp_4 = local_sp_3;
                                    if ((long)*var_20 > (long)var_52) {
                                        continue;
                                    }
                                    loop_state_var = 1U;
                                    break;
                                }
                            }
                            var_48 = *(uint64_t *)(local_sp_2 + 8UL);
                            var_49 = r13_0 - r105_1;
                            var_50 = local_sp_2 + (-8L);
                            *(uint64_t *)var_50 = 4249196UL;
                            indirect_placeholder_23(var_49, var_26, var_48);
                            var_51 = r105_1 + 1UL;
                            r105_2 = var_51;
                            local_sp_3 = var_50;
                            var_52 = r13_0 + 1UL;
                            r13_0 = var_52;
                            r105_0 = r105_2;
                            local_sp_1 = local_sp_3;
                            local_sp_4 = local_sp_3;
                            if ((long)*var_20 > (long)var_52) {
                                continue;
                            }
                            loop_state_var = 1U;
                            break;
                        }
                        var_48 = *(uint64_t *)(local_sp_2 + 8UL);
                        var_49 = r13_0 - r105_1;
                        var_50 = local_sp_2 + (-8L);
                        *(uint64_t *)var_50 = 4249196UL;
                        indirect_placeholder_23(var_49, var_26, var_48);
                        var_51 = r105_1 + 1UL;
                        r105_2 = var_51;
                        local_sp_3 = var_50;
                    }
                }
            switch (loop_state_var) {
              case 0U:
                {
                    return r15_0;
                }
                break;
              case 2U:
                {
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4249489UL;
                    indirect_placeholder_8(var_10);
                }
                break;
              case 1U:
                {
                    var_53 = *(uint64_t *)(local_sp_4 + 40UL);
                    var_54 = local_sp_4 + (-8L);
                    *(uint64_t *)var_54 = 4249459UL;
                    var_55 = indirect_placeholder_7(var_53, rdi, var_10);
                    local_sp_0 = var_54;
                    if ((uint64_t)(uint32_t)var_55 != 0UL) {
                        return r15_0;
                    }
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4249489UL;
                    indirect_placeholder_8(var_10);
                }
                break;
            }
        }
        var_53 = *(uint64_t *)(local_sp_4 + 40UL);
        var_54 = local_sp_4 + (-8L);
        *(uint64_t *)var_54 = 4249459UL;
        var_55 = indirect_placeholder_7(var_53, rdi, var_10);
        local_sp_0 = var_54;
        if ((uint64_t)(uint32_t)var_55 != 0UL) {
            return r15_0;
        }
        *(uint64_t *)(local_sp_0 + (-8L)) = 4249489UL;
        indirect_placeholder_8(var_10);
    }
    return r15_0;
}
