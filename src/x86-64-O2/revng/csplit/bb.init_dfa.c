typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_50_ret_type;
struct indirect_placeholder_51_ret_type;
struct indirect_placeholder_52_ret_type;
struct indirect_placeholder_50_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_51_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_52_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r9(void);
extern uint32_t init_state_0x82fc(void);
extern uint64_t init_r10(void);
extern uint64_t init_r8(void);
extern struct indirect_placeholder_50_ret_type indirect_placeholder_50(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
extern struct indirect_placeholder_51_ret_type indirect_placeholder_51(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
extern struct indirect_placeholder_52_ret_type indirect_placeholder_52(uint64_t param_0);
uint64_t bb_init_dfa(uint64_t rdi, uint64_t rsi) {
    uint64_t rbx_1;
    uint64_t rsi2_1;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint32_t var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t local_sp_1;
    uint64_t var_43;
    uint64_t local_sp_0;
    uint64_t *var_44;
    uint64_t r14_0;
    uint32_t var_45;
    uint32_t var_46;
    uint64_t rbp_0;
    uint64_t r12_0;
    uint64_t rbx_0;
    uint32_t var_39;
    bool var_40;
    unsigned char *var_41;
    uint64_t var_42;
    uint64_t var_29;
    unsigned char *var_30;
    unsigned char *var_31;
    unsigned char var_32;
    uint32_t var_33;
    uint64_t var_34;
    struct indirect_placeholder_50_ret_type var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t *var_38;
    uint64_t merge;
    uint64_t rsi2_0;
    uint64_t var_18;
    uint64_t var_19;
    struct indirect_placeholder_51_ret_type var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint32_t *var_28;
    uint64_t rdi1_0;
    uint64_t rcx_0;
    uint64_t var_15;
    uint64_t var_16;
    struct indirect_placeholder_52_ret_type var_17;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r12();
    var_2 = init_rbx();
    var_3 = init_r13();
    var_4 = init_r14();
    var_5 = init_r8();
    var_6 = init_rbp();
    var_7 = init_r10();
    var_8 = init_cc_src2();
    var_9 = init_r9();
    var_10 = init_state_0x82fc();
    *(uint64_t *)(var_0 + (-8L)) = var_4;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_6;
    var_11 = rdi + 8UL;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    var_12 = (uint64_t *)rdi;
    *var_12 = 0UL;
    *(uint64_t *)(rdi + 224UL) = 0UL;
    var_13 = var_11 & (-8L);
    var_14 = (uint64_t)var_10 << 3UL;
    rbx_1 = 0UL;
    rsi2_1 = 1UL;
    r14_0 = 0UL;
    r12_0 = 0UL;
    merge = 0UL;
    rsi2_0 = 1UL;
    rdi1_0 = var_13;
    rcx_0 = (uint64_t)((uint32_t)((uint64_t)(((uint32_t)rdi - (uint32_t)var_13) + 232U) >> 3UL) & 536870911U);
    while (rcx_0 != 0UL)
        {
            *(uint64_t *)rdi1_0 = 0UL;
            rdi1_0 = rdi1_0 + var_14;
            rcx_0 = rcx_0 + (-1L);
        }
    *(uint32_t *)(rdi + 128UL) = 15U;
    if (rsi > 384307168202282324UL) {
        merge = 12UL;
    } else {
        var_15 = rsi + 1UL;
        *(uint64_t *)var_11 = var_15;
        var_16 = var_15 << 4UL;
        *(uint64_t *)(var_0 + (-48L)) = 4232779UL;
        var_17 = indirect_placeholder_52(var_16);
        *var_12 = var_17.field_0;
        if (rsi != 0UL) {
            var_18 = rsi2_0 << 1UL;
            var_19 = helper_cc_compute_c_wrapper(rsi - var_18, var_18, var_8, 17U);
            rsi2_0 = var_18;
            rsi2_1 = var_18;
            do {
                var_18 = rsi2_0 << 1UL;
                var_19 = helper_cc_compute_c_wrapper(rsi - var_18, var_18, var_8, 17U);
                rsi2_0 = var_18;
                rsi2_1 = var_18;
            } while (var_19 != 0UL);
            rbx_1 = var_18 + (-1L);
        }
        *(uint64_t *)(var_0 + (-56L)) = 4232822UL;
        var_20 = indirect_placeholder_51(rbx_1, var_5, rdi, 24UL, 0UL, var_7, rsi2_1, var_9);
        var_21 = var_20.field_0;
        var_22 = var_20.field_1;
        var_23 = var_20.field_2;
        var_24 = var_20.field_3;
        var_25 = var_20.field_4;
        var_26 = var_20.field_5;
        var_27 = var_20.field_6;
        *(uint64_t *)(var_24 + 136UL) = var_22;
        *(uint64_t *)(var_24 + 64UL) = var_21;
        *(uint64_t *)(var_0 + (-64L)) = 4232838UL;
        indirect_placeholder();
        var_28 = (uint32_t *)(var_24 + 180UL);
        *var_28 = (uint32_t)var_21;
        *(uint64_t *)(var_0 + (-72L)) = 4232854UL;
        indirect_placeholder();
        rbp_0 = var_24;
        var_29 = var_21 + (*(unsigned char *)(var_21 + 3UL) == '-');
        if ((*(unsigned char *)var_21 & '\xdf') != 'U' & (*(unsigned char *)(var_21 + 1UL) & '\xdf') != 'T' & (*(unsigned char *)(var_21 + 2UL) & '\xdf') != 'F' & *(unsigned char *)(var_29 + 3UL) != '8' & *(unsigned char *)(var_29 + 4UL) == '\x00') {
            var_30 = (unsigned char *)(var_24 + 176UL);
            *var_30 = (*var_30 | '\x04');
        }
        var_31 = (unsigned char *)(var_24 + 176UL);
        var_32 = *var_31;
        var_33 = *var_28;
        *var_31 = (var_32 & '\xf7');
        if ((int)var_33 <= (int)1U) {
            if ((var_32 & '\x04') != '\x00') {
                var_34 = var_0 + (-80L);
                *(uint64_t *)var_34 = 4233021UL;
                var_35 = indirect_placeholder_50(var_22, var_23, var_24, 32UL, var_25, var_26, 1UL, var_27);
                var_36 = var_35.field_0;
                var_37 = var_35.field_3;
                var_38 = (uint64_t *)(var_37 + 120UL);
                *var_38 = var_36;
                local_sp_0 = var_34;
                rbp_0 = var_37;
                if (var_36 != 0UL) {
                    merge = 12UL;
                    return merge;
                }
                var_39 = (uint32_t)var_36;
                var_40 = ((uint64_t)(var_39 + 1U) == 0UL);
                var_41 = (unsigned char *)(var_37 + 176UL);
                while (1U)
                    {
                        rbx_0 = (uint64_t)((uint32_t)(r12_0 << 3UL) & (-64));
                        local_sp_1 = local_sp_0;
                        while (1U)
                            {
                                var_42 = local_sp_1 + (-8L);
                                *(uint64_t *)var_42 = 4233063UL;
                                indirect_placeholder();
                                local_sp_0 = var_42;
                                local_sp_1 = var_42;
                                if (var_40) {
                                    var_43 = 1UL << (r14_0 & 63UL);
                                    var_44 = (uint64_t *)(*var_38 + r12_0);
                                    *var_44 = (*var_44 | var_43);
                                }
                                var_45 = (uint32_t)rbx_0;
                                if ((uint64_t)(var_45 & (-128)) != 0UL & (uint64_t)(var_45 - var_39) == 0UL) {
                                    *var_41 = (*var_41 | '\b');
                                }
                                var_46 = (uint32_t)r14_0;
                                if ((uint64_t)(var_46 + (-63)) == 0UL) {
                                    break;
                                }
                                rbx_0 = (uint64_t)(var_45 + 1U);
                                r14_0 = (uint64_t)(var_46 + 1U);
                                continue;
                            }
                        if (r12_0 == 24UL) {
                            break;
                        }
                        r12_0 = r12_0 + 8UL;
                        continue;
                    }
            }
            *(uint64_t *)(var_24 + 120UL) = 4330176UL;
        }
        if (*(uint64_t *)rbp_0 == 0UL) {
            merge = 12UL;
        } else {
            if (*(uint64_t *)(rbp_0 + 64UL) == 0UL) {
                merge = 12UL;
            }
        }
    }
    return merge;
}
