typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern void indirect_placeholder_5(uint64_t param_0);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
uint64_t bb_get_line(uint64_t rdi, uint64_t rsi) {
    uint64_t rdx_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t local_sp_1;
    uint64_t _pre_phi59;
    unsigned char var_37;
    uint64_t _pre58;
    bool var_30;
    uint64_t var_31;
    uint64_t var_32;
    bool var_33;
    uint64_t var_34;
    unsigned char var_35;
    unsigned char var_36;
    unsigned char var_40;
    uint64_t rax_3;
    uint64_t var_38;
    uint64_t rax_0;
    uint64_t local_sp_0;
    uint64_t local_sp_1_be;
    uint64_t rdx_1_be;
    unsigned char var_39;
    uint64_t var_42;
    uint64_t var_41;
    uint64_t rax_2;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t local_sp_2;
    uint64_t local_sp_4;
    uint64_t rbp_0_in;
    uint64_t rdx_1;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t rax_1;
    uint64_t rbp_1;
    uint64_t local_sp_3;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_16;
    uint32_t var_17;
    uint32_t var_18;
    uint32_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint32_t var_22;
    uint64_t var_23;
    uint32_t var_24;
    uint32_t *var_25;
    uint32_t var_26;
    bool var_27;
    unsigned char *var_28;
    unsigned char var_29;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r12();
    var_2 = init_rbx();
    var_3 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_4 = *(uint64_t *)4281216UL;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    var_5 = var_0 + (-24L);
    *(uint64_t *)var_5 = var_2;
    rdx_0 = 0UL;
    local_sp_1 = var_5;
    rbp_0_in = rsi;
    rdx_1 = var_4;
    while (1U)
        {
            var_6 = (uint64_t)(uint32_t)rbp_0_in;
            var_7 = *(uint64_t *)4321248UL;
            *(uint64_t *)rdx_1 = var_7;
            local_sp_2 = local_sp_1;
            rax_1 = var_7;
            rbp_1 = var_6;
            while (1U)
                {
                    local_sp_3 = local_sp_2;
                    rax_2 = rax_1;
                    if (rax_1 == 4326280UL) {
                        *(uint64_t *)(local_sp_2 + (-8L)) = 4207701UL;
                        indirect_placeholder_5(1UL);
                        var_8 = local_sp_2 + (-16L);
                        *(uint64_t *)var_8 = 4207706UL;
                        indirect_placeholder_1();
                        local_sp_3 = var_8;
                        rax_2 = *(uint64_t *)4321248UL;
                    }
                    *(unsigned char *)rax_2 = (unsigned char)rbp_1;
                    var_9 = rax_2 + 1UL;
                    *(uint64_t *)4321248UL = var_9;
                    var_10 = local_sp_3 + (-8L);
                    *(uint64_t *)var_10 = 4207654UL;
                    indirect_placeholder_1();
                    var_11 = (uint64_t)(uint32_t)rax_2;
                    rbp_1 = var_11;
                    local_sp_4 = var_10;
                    if ((uint64_t)(uint32_t)var_9 != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_12 = local_sp_3 + (-16L);
                    *(uint64_t *)var_12 = 4207668UL;
                    var_13 = indirect_placeholder_2(var_11);
                    var_14 = (uint64_t)(uint32_t)var_13;
                    var_15 = *(uint64_t *)4321248UL;
                    local_sp_2 = var_12;
                    rax_1 = var_15;
                    local_sp_4 = var_12;
                    rax_3 = var_15;
                    if (var_14 == 0UL) {
                        continue;
                    }
                    loop_state_var = 1U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    rax_3 = *(uint64_t *)4321248UL;
                }
                break;
              case 1U:
                {
                    var_16 = *(uint64_t *)4281216UL;
                    var_17 = *(uint32_t *)4326284UL;
                    var_18 = (uint32_t)rax_3 - (uint32_t)*(uint64_t *)var_16;
                    *(uint32_t *)(var_16 + 8UL) = var_18;
                    var_19 = var_18 + var_17;
                    *(uint32_t *)4326284UL = var_19;
                    *(uint64_t *)(local_sp_4 + (-8L)) = 4207759UL;
                    indirect_placeholder_5(var_16);
                    var_20 = local_sp_4 + (-16L);
                    *(uint64_t *)var_20 = 4207769UL;
                    var_21 = indirect_placeholder(rdi, var_11);
                    var_22 = *(uint32_t *)4326284UL;
                    var_23 = *(uint64_t *)4281216UL;
                    var_24 = var_22 - var_19;
                    var_25 = (uint32_t *)(var_23 + 12UL);
                    *var_25 = var_24;
                    var_26 = (uint32_t)var_21;
                    var_27 = ((uint64_t)(var_26 + 1U) == 0UL);
                    var_28 = (unsigned char *)(var_23 + 16UL);
                    var_29 = *var_28;
                    local_sp_1_be = var_20;
                    local_sp_0 = var_20;
                    rax_0 = var_23;
                    rbp_0_in = var_21;
                    if (!var_27) {
                        if ((var_29 & '\x02') == '\x00') {
                            _pre58 = (uint64_t)(var_26 + (-10));
                            _pre_phi59 = _pre58;
                            var_36 = (var_29 & '\xf7') | ((unsigned char)rdx_0 << '\x03');
                            *var_28 = var_36;
                            var_37 = var_36;
                            var_40 = var_36;
                            if (_pre_phi59 == 0UL) {
                                *var_25 = (uint32_t)(((var_40 >> '\x03') & '\x01') + '\x01');
                                if (var_23 == 4321168UL) {
                                    *(uint64_t *)(local_sp_4 + (-24L)) = 4207970UL;
                                    indirect_placeholder_5(1UL);
                                    var_41 = local_sp_4 + (-32L);
                                    *(uint64_t *)var_41 = 4207975UL;
                                    indirect_placeholder_1();
                                    local_sp_0 = var_41;
                                    rax_0 = *(uint64_t *)4281216UL;
                                }
                                var_42 = rax_0 + 40UL;
                                *(uint64_t *)4281216UL = var_42;
                                local_sp_1_be = local_sp_0;
                                rdx_1_be = var_42;
                                if (((uint64_t)(var_26 + (-10)) == 0UL) || var_27) {
                                    switch_state_var = 1;
                                    break;
                                }
                                local_sp_1 = local_sp_1_be;
                                rdx_1 = rdx_1_be;
                                continue;
                            }
                            var_40 = var_37;
                            if (*(unsigned char *)4326320UL == '\x00') {
                                if (var_23 == 4321168UL) {
                                    var_38 = var_23 + 40UL;
                                    *(uint64_t *)4281216UL = var_38;
                                    rdx_1_be = var_38;
                                    local_sp_1 = local_sp_1_be;
                                    rdx_1 = rdx_1_be;
                                    continue;
                                }
                                *(uint64_t *)(local_sp_4 + (-24L)) = 4207970UL;
                                indirect_placeholder_5(1UL);
                                var_41 = local_sp_4 + (-32L);
                                *(uint64_t *)var_41 = 4207975UL;
                                indirect_placeholder_1();
                                local_sp_0 = var_41;
                                rax_0 = *(uint64_t *)4281216UL;
                                var_42 = rax_0 + 40UL;
                                *(uint64_t *)4281216UL = var_42;
                                local_sp_1_be = local_sp_0;
                                rdx_1_be = var_42;
                                if (((uint64_t)(var_26 + (-10)) == 0UL) || var_27) {
                                    switch_state_var = 1;
                                    break;
                                }
                                local_sp_1 = local_sp_1_be;
                                rdx_1 = rdx_1_be;
                                continue;
                            }
                        }
                        var_30 = ((int)var_24 > (int)1U);
                        var_31 = (uint64_t)(var_24 & (-256)) | var_30;
                        var_32 = (uint64_t)(var_26 + (-10));
                        var_33 = (var_32 == 0UL);
                        var_34 = var_31 | var_33;
                        _pre_phi59 = var_32;
                        rdx_0 = var_34;
                        if (var_30 || var_33) {
                            var_36 = (var_29 & '\xf7') | ((unsigned char)rdx_0 << '\x03');
                            *var_28 = var_36;
                            var_37 = var_36;
                            var_40 = var_36;
                            var_40 = var_37;
                            if (_pre_phi59 != 0UL & *(unsigned char *)4326320UL != '\x00') {
                                if (var_23 != 4321168UL) {
                                    var_38 = var_23 + 40UL;
                                    *(uint64_t *)4281216UL = var_38;
                                    rdx_1_be = var_38;
                                    local_sp_1 = local_sp_1_be;
                                    rdx_1 = rdx_1_be;
                                    continue;
                                }
                                *(uint64_t *)(local_sp_4 + (-24L)) = 4207970UL;
                                indirect_placeholder_5(1UL);
                                var_41 = local_sp_4 + (-32L);
                                *(uint64_t *)var_41 = 4207975UL;
                                indirect_placeholder_1();
                                local_sp_0 = var_41;
                                rax_0 = *(uint64_t *)4281216UL;
                                var_42 = rax_0 + 40UL;
                                *(uint64_t *)4281216UL = var_42;
                                local_sp_1_be = local_sp_0;
                                rdx_1_be = var_42;
                                if (((uint64_t)(var_26 + (-10)) == 0UL) || var_27) {
                                    switch_state_var = 1;
                                    break;
                                }
                                local_sp_1 = local_sp_1_be;
                                rdx_1 = rdx_1_be;
                                continue;
                            }
                        }
                        var_35 = var_29 & '\xf7';
                        *var_28 = var_35;
                        var_37 = var_35;
                        var_40 = var_37;
                        if (*(unsigned char *)4326320UL != '\x00') {
                            if (var_23 == 4321168UL) {
                                var_38 = var_23 + 40UL;
                                *(uint64_t *)4281216UL = var_38;
                                rdx_1_be = var_38;
                                local_sp_1 = local_sp_1_be;
                                rdx_1 = rdx_1_be;
                                continue;
                            }
                            *(uint64_t *)(local_sp_4 + (-24L)) = 4207970UL;
                            indirect_placeholder_5(1UL);
                            var_41 = local_sp_4 + (-32L);
                            *(uint64_t *)var_41 = 4207975UL;
                            indirect_placeholder_1();
                            local_sp_0 = var_41;
                            rax_0 = *(uint64_t *)4281216UL;
                            var_42 = rax_0 + 40UL;
                            *(uint64_t *)4281216UL = var_42;
                            local_sp_1_be = local_sp_0;
                            rdx_1_be = var_42;
                            if (((uint64_t)(var_26 + (-10)) == 0UL) || var_27) {
                                switch_state_var = 1;
                                break;
                            }
                            local_sp_1 = local_sp_1_be;
                            rdx_1 = rdx_1_be;
                            continue;
                        }
                    }
                    var_39 = var_29 | '\b';
                    *var_28 = var_39;
                    var_40 = var_39;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    indirect_placeholder_1();
    return rax_0;
}
