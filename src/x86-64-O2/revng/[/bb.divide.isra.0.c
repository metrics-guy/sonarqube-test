typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_12_ret_type;
struct helper_divq_EAX_wrapper_ret_type;
struct type_6;
struct indirect_placeholder_13_ret_type;
struct indirect_placeholder_12_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_divq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint32_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint32_t field_5;
    uint64_t field_6;
    uint32_t field_7;
    uint64_t field_8;
    uint32_t field_9;
    uint32_t field_10;
    unsigned char field_11;
    uint32_t field_12;
};
struct type_6 {
};
struct indirect_placeholder_13_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_cc_src2(void);
extern uint64_t init_r12(void);
extern uint64_t init_r15(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t helper_clz_wrapper(uint64_t param_0);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern uint32_t init_state_0x8248(void);
extern struct indirect_placeholder_12_ret_type indirect_placeholder_12(uint64_t param_0);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct helper_divq_EAX_wrapper_ret_type helper_divq_EAX_wrapper(struct type_6 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint64_t param_13, uint32_t param_14, uint64_t param_15, uint64_t param_16, uint64_t param_17, uint32_t param_18, uint32_t param_19);
extern struct indirect_placeholder_13_ret_type indirect_placeholder_13(uint64_t param_0);
uint64_t bb_divide_isra_0(uint64_t r8, uint64_t rdx, uint64_t rdi, uint64_t rcx, uint64_t rsi, uint64_t r9) {
    uint64_t *var_71;
    uint64_t var_72;
    uint64_t var_28;
    uint64_t rbx_0;
    uint64_t r13_0;
    uint64_t var_29;
    uint32_t var_30;
    uint64_t var_33;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t r96_1;
    uint64_t local_sp_1;
    uint64_t rax_9;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint32_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_155;
    uint64_t var_156;
    uint32_t var_13;
    uint32_t var_14;
    uint64_t var_15;
    uint64_t *var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t *var_19;
    uint64_t *var_20;
    uint64_t var_21;
    uint64_t *var_22;
    struct indirect_placeholder_12_ret_type var_23;
    uint64_t var_24;
    uint64_t r12_5;
    uint64_t var_45;
    uint64_t rdx2_0;
    uint64_t rax_0;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t r13_0_in;
    uint64_t r13_3;
    uint64_t r14_7;
    uint64_t r96_3;
    uint64_t local_sp_7;
    uint64_t _pre_phi345;
    uint64_t rsi5_0;
    uint64_t rax_1;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t rdx2_1;
    uint32_t rax_2;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t _pre_phi336;
    uint64_t r81_0;
    uint64_t rbp_0;
    uint64_t r10_0;
    uint64_t r96_0;
    uint64_t rax_3;
    uint64_t local_sp_0;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t *var_63;
    uint64_t var_64;
    uint64_t *var_65;
    uint64_t var_66;
    uint64_t *var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t *var_70;
    uint64_t var_81;
    uint64_t r12_1;
    uint64_t *var_25;
    uint64_t r14_0;
    uint64_t r14_2;
    uint64_t *var_73;
    uint64_t var_74;
    uint64_t *var_75;
    uint64_t var_76;
    uint64_t r81_1;
    uint64_t rsi5_3;
    uint64_t rdi3_0;
    uint64_t state_0x9018_5;
    uint64_t rsi5_1;
    uint32_t state_0x9010_5;
    uint64_t state_0x9018_0;
    uint64_t state_0x82d8_5;
    uint32_t state_0x9010_0;
    uint64_t rax_4;
    uint32_t state_0x9080_5;
    uint64_t state_0x82d8_0;
    uint32_t state_0x8248_5;
    uint32_t state_0x9080_0;
    uint32_t state_0x8248_0;
    uint64_t var_77;
    uint64_t r15_0;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint32_t var_89;
    struct helper_divq_EAX_wrapper_ret_type var_85;
    uint64_t var_90;
    uint32_t var_91;
    uint32_t var_92;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t r14_1;
    uint64_t rdx2_2;
    uint64_t state_0x9018_1;
    uint32_t state_0x9010_1;
    uint64_t rax_5;
    uint64_t state_0x82d8_1;
    uint32_t state_0x9080_1;
    uint32_t state_0x8248_1;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t state_0x9018_2;
    uint32_t state_0x9010_2;
    uint64_t state_0x82d8_2;
    uint32_t state_0x9080_2;
    uint32_t state_0x8248_2;
    uint32_t *_pre_phi341;
    uint64_t state_0x9018_3;
    uint32_t *_pre340;
    uint32_t state_0x9010_3;
    uint64_t state_0x82d8_3;
    uint32_t state_0x9080_3;
    uint32_t state_0x8248_3;
    uint64_t r14_3;
    uint64_t state_0x9018_4;
    uint32_t state_0x9010_4;
    uint64_t state_0x82d8_4;
    uint32_t state_0x9080_4;
    uint32_t state_0x8248_4;
    uint64_t var_100;
    uint64_t rcx4_0;
    uint64_t rax_6;
    uint64_t var_101;
    uint64_t var_102;
    uint32_t *var_103;
    uint32_t var_104;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t var_107;
    uint64_t var_108;
    uint32_t *var_109;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t r14_4;
    uint64_t storemerge;
    uint64_t rsi5_2;
    uint64_t rax_7;
    uint64_t var_112;
    uint32_t *var_113;
    uint32_t var_114;
    uint64_t var_115;
    uint32_t var_116;
    uint64_t var_117;
    uint32_t var_118;
    uint64_t var_119;
    uint32_t var_120;
    uint64_t r13_4;
    uint64_t rdx2_8;
    uint64_t rax_11;
    uint32_t var_153;
    uint64_t var_154;
    uint64_t var_121;
    uint64_t var_122;
    uint64_t rdi3_1;
    uint64_t var_123;
    uint64_t var_124;
    uint64_t var_125;
    uint64_t var_126;
    uint64_t var_127;
    uint32_t var_128;
    uint64_t var_129;
    uint64_t rdx2_4;
    uint64_t rdx2_3;
    uint64_t var_130;
    uint64_t r12_0;
    uint64_t var_131;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t r81_3;
    uint64_t r81_2;
    uint64_t var_40;
    struct indirect_placeholder_13_ret_type var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_132;
    uint64_t var_133;
    uint64_t var_134;
    uint64_t r13_1;
    uint64_t rcx4_1;
    uint64_t state_0x9018_6;
    uint32_t state_0x9010_6;
    uint64_t rax_8;
    uint64_t state_0x82d8_6;
    uint32_t state_0x9080_6;
    uint32_t state_0x8248_6;
    uint64_t var_135;
    struct helper_divq_EAX_wrapper_ret_type var_136;
    uint64_t var_137;
    bool var_138;
    uint32_t var_139;
    uint64_t var_140;
    uint64_t rdx2_5;
    uint64_t var_141;
    uint64_t r14_5;
    uint64_t rbp_1;
    uint64_t rdx2_7;
    uint64_t var_142;
    uint64_t var_143;
    uint64_t var_144;
    uint64_t rcx4_3;
    uint64_t _pre_phi339;
    uint64_t var_145;
    uint64_t rcx4_2;
    uint64_t _pre338;
    uint64_t var_146;
    uint64_t var_147;
    uint64_t var_148;
    uint64_t rcx4_4;
    uint64_t r13_2;
    uint64_t r14_6;
    uint64_t r96_2;
    uint64_t local_sp_2;
    uint64_t r12_3;
    uint64_t var_149;
    uint64_t r12_2;
    uint64_t var_150;
    uint64_t var_151;
    uint64_t rsi5_4;
    uint64_t r12_4;
    uint64_t var_152;
    uint64_t rax_10;
    uint64_t local_sp_3;
    uint64_t var_157;
    uint64_t var_158;
    uint64_t r14_8;
    uint64_t r96_4;
    uint64_t rax_12;
    uint64_t local_sp_4;
    uint64_t r13_5;
    uint64_t r14_9;
    uint64_t r96_5;
    uint64_t local_sp_5;
    uint64_t local_sp_6;
    uint64_t var_159;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_160;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r12();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_r13();
    var_5 = init_r14();
    var_6 = init_rbp();
    var_7 = init_cc_src2();
    var_8 = init_state_0x9018();
    var_9 = init_state_0x9010();
    var_10 = init_state_0x8408();
    var_11 = init_state_0x8328();
    var_12 = init_state_0x82d8();
    var_13 = init_state_0x9080();
    var_14 = init_state_0x8248();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_1;
    *(uint64_t *)(var_0 + (-40L)) = var_6;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_15 = var_0 + (-120L);
    var_16 = (uint64_t *)var_15;
    *var_16 = r8;
    var_17 = (rdi << 2UL) + 8UL;
    var_18 = var_0 + (-112L);
    var_19 = (uint64_t *)var_18;
    *var_19 = r9;
    var_20 = (uint64_t *)(var_0 + (-104L));
    *var_20 = rcx;
    *(uint64_t *)(var_0 + (-128L)) = rdi;
    var_21 = var_0 + (-144L);
    var_22 = (uint64_t *)var_21;
    *var_22 = 4225918UL;
    var_23 = indirect_placeholder_12(var_17);
    var_24 = var_23.field_0;
    rbx_0 = rdx;
    r96_1 = 0UL;
    local_sp_1 = var_21;
    r12_5 = 0UL;
    rdx2_0 = 0UL;
    rax_0 = 0UL;
    local_sp_7 = var_21;
    rsi5_0 = 0UL;
    rax_1 = 0UL;
    rdx2_1 = var_24;
    rax_2 = 0U;
    rbp_0 = rcx;
    r96_0 = 0UL;
    rax_3 = 0UL;
    r14_0 = 4294967295UL;
    r14_2 = 4294967295UL;
    state_0x9018_0 = var_8;
    state_0x9010_0 = var_9;
    state_0x82d8_0 = var_12;
    state_0x9080_0 = var_13;
    state_0x8248_0 = var_14;
    r15_0 = 4294967295UL;
    r14_3 = 4294967295UL;
    rcx4_0 = 0UL;
    rax_6 = 0UL;
    r14_4 = 0UL;
    rsi5_2 = 0UL;
    rax_7 = 0UL;
    rdx2_8 = 0UL;
    r12_0 = var_24;
    r81_3 = 0UL;
    state_0x9018_6 = var_8;
    state_0x9010_6 = var_9;
    rax_8 = 0UL;
    state_0x82d8_6 = var_12;
    state_0x9080_6 = var_13;
    state_0x8248_6 = var_14;
    rdx2_5 = 1UL;
    rbp_1 = rcx;
    rdx2_7 = 0UL;
    rcx4_2 = 0UL;
    rcx4_4 = 0UL;
    r14_8 = 1UL;
    r14_9 = 0UL;
    if (var_24 == 0UL) {
        return r12_5;
    }
    var_25 = (uint64_t *)(var_0 + (-136L));
    var_26 = *var_25;
    var_27 = *var_19;
    r81_2 = var_26;
    if (var_26 == 0UL) {
        r81_3 = r81_2;
        while (*(uint32_t *)(((r81_2 << 2UL) + rsi) + (-4L)) != 0U)
            {
                var_28 = r81_2 + (-1L);
                r81_2 = var_28;
                r81_3 = 0UL;
                if (var_28 == 0UL) {
                    break;
                }
                r81_3 = r81_2;
            }
    }
    rcx4_1 = r81_3;
    r14_5 = r81_3;
    if (rdx == 0UL) {
        *(uint64_t *)(local_sp_7 + (-8L)) = 4203650UL;
        indirect_placeholder();
        abort();
    }
    r13_0_in = rdx << 2UL;
    while (1U)
        {
            r13_0 = r13_0_in + (-4L);
            var_29 = r13_0 + var_27;
            var_30 = *(uint32_t *)var_29;
            rdx2_3 = rbx_0;
            rdx2_4 = rbx_0;
            r13_0_in = r13_0;
            rax_9 = rbx_0;
            if (var_30 == 0U) {
                var_160 = rbx_0 + (-1L);
                rbx_0 = var_160;
                if (var_160 == 0UL) {
                    continue;
                }
                loop_state_var = 5U;
                break;
            }
            var_31 = (uint64_t)var_30;
            var_32 = rbx_0 << 2UL;
            var_33 = helper_cc_compute_c_wrapper(r81_3 - rbx_0, rbx_0, var_7, 17U);
            if (var_33 != 0UL) {
                var_34 = r81_3 << 2UL;
                *var_25 = r81_3;
                var_35 = var_34 + var_24;
                var_36 = var_0 + (-152L);
                *(uint64_t *)var_36 = 4227017UL;
                indirect_placeholder();
                var_37 = *var_22;
                r13_1 = var_35;
                r14_5 = 0UL;
                rdx2_7 = var_37;
                local_sp_1 = var_36;
                loop_state_var = 0U;
                break;
            }
            if (rbx_0 == 1UL) {
                var_132 = (uint64_t)*(uint32_t *)var_27;
                var_133 = var_24 + 4UL;
                var_134 = r81_3 << 2UL;
                r13_1 = var_133;
                rdx2_7 = 1UL;
                if (r81_3 != 0UL) {
                    loop_state_var = 4U;
                    break;
                }
                loop_state_var = 1U;
                break;
            }
            var_38 = helper_clz_wrapper(var_31);
            var_39 = (uint64_t)(uint32_t)var_38 ^ 32UL;
            if (var_39 == 0UL) {
                *var_20 = var_27;
                *(uint32_t *)var_18 = (uint32_t)var_39;
                *var_25 = r81_3;
                var_40 = var_0 + (-152L);
                *(uint64_t *)var_40 = 4227142UL;
                var_41 = indirect_placeholder_13(var_32);
                var_42 = var_41.field_0;
                var_43 = *var_22;
                var_44 = *var_19;
                local_sp_7 = var_40;
                r81_0 = var_43;
                rbp_0 = var_42;
                r96_0 = var_42;
                local_sp_0 = var_40;
                if (var_42 == 0UL) {
                    var_45 = (uint64_t)*(uint32_t *)var_15 & 63UL;
                    loop_state_var = 3U;
                    break;
                }
                *(uint64_t *)(var_0 + (-160L)) = 4227283UL;
                indirect_placeholder();
                loop_state_var = 6U;
                break;
            }
            *var_19 = var_29;
            *var_25 = r81_3;
            var_57 = var_0 + (-152L);
            *(uint64_t *)var_57 = 4226086UL;
            indirect_placeholder();
            var_58 = *var_22;
            var_59 = *var_16;
            var_60 = (var_58 << 2UL) + var_24;
            *(uint32_t *)var_60 = 0U;
            _pre_phi336 = var_60;
            r81_0 = var_58;
            r10_0 = var_59;
            local_sp_0 = var_57;
            loop_state_var = 2U;
            break;
        }
    switch (loop_state_var) {
      case 5U:
        {
            *(uint64_t *)(local_sp_7 + (-8L)) = 4203650UL;
            indirect_placeholder();
            abort();
        }
        break;
      case 3U:
        {
            var_46 = rdx2_0 << 2UL;
            var_47 = rax_0 + ((uint64_t)*(uint32_t *)(var_46 + var_44) << var_45);
            *(uint32_t *)(var_46 + var_42) = (uint32_t)var_47;
            var_48 = rdx2_0 + 1UL;
            var_49 = var_47 >> 32UL;
            rdx2_0 = var_48;
            rax_0 = var_49;
            do {
                var_46 = rdx2_0 << 2UL;
                var_47 = rax_0 + ((uint64_t)*(uint32_t *)(var_46 + var_44) << var_45);
                *(uint32_t *)(var_46 + var_42) = (uint32_t)var_47;
                var_48 = rdx2_0 + 1UL;
                var_49 = var_47 >> 32UL;
                rdx2_0 = var_48;
                rax_0 = var_49;
            } while (rbx_0 != var_48);
            if (var_49 == 0UL) {
                *(uint64_t *)(local_sp_7 + (-8L)) = 4203650UL;
                indirect_placeholder();
                abort();
            }
            if (var_43 == 0UL) {
                _pre_phi345 = (var_43 << 2UL) + var_24;
            } else {
                var_50 = rsi5_0 << 2UL;
                var_51 = rax_1 + ((uint64_t)*(uint32_t *)(var_50 + rsi) << var_45);
                *(uint32_t *)(var_50 + var_24) = (uint32_t)var_51;
                var_52 = rsi5_0 + 1UL;
                var_53 = var_51 >> 32UL;
                rsi5_0 = var_52;
                rax_1 = var_53;
                do {
                    var_50 = rsi5_0 << 2UL;
                    var_51 = rax_1 + ((uint64_t)*(uint32_t *)(var_50 + rsi) << var_45);
                    *(uint32_t *)(var_50 + var_24) = (uint32_t)var_51;
                    var_52 = rsi5_0 + 1UL;
                    var_53 = var_51 >> 32UL;
                    rsi5_0 = var_52;
                    rax_1 = var_53;
                } while (var_43 != var_52);
                var_54 = (var_43 << 2UL) + var_24;
                _pre_phi345 = var_54;
                rdx2_1 = var_54;
                rax_2 = (uint32_t)var_53;
            }
            *(uint32_t *)rdx2_1 = rax_2;
            var_55 = r13_0 + var_42;
            var_56 = (uint64_t)*(uint32_t *)_pre_phi345;
            _pre_phi336 = _pre_phi345;
            r10_0 = var_55;
            rax_3 = var_56;
            var_61 = (uint64_t)*(uint32_t *)r10_0;
            var_62 = var_32 + var_24;
            var_63 = (uint64_t *)(local_sp_0 + 64UL);
            *var_63 = r96_0;
            var_64 = r81_0 - rbx_0;
            var_65 = (uint64_t *)(local_sp_0 + 56UL);
            *var_65 = var_62;
            var_66 = var_64 + 1UL;
            var_67 = (uint64_t *)(local_sp_0 + 40UL);
            *var_67 = var_64;
            var_68 = (uint64_t)*(uint32_t *)((var_32 + rbp_0) + (-8L));
            var_69 = var_61 << 32UL;
            var_70 = (uint64_t *)(local_sp_0 + 72UL);
            *var_70 = var_24;
            var_71 = (uint64_t *)(local_sp_0 + 48UL);
            *var_71 = var_66;
            var_72 = var_69 | var_68;
            var_73 = (uint64_t *)(local_sp_0 + 8UL);
            *var_73 = var_72;
            var_74 = 0UL - var_32;
            var_75 = (uint64_t *)(local_sp_0 + 32UL);
            *var_75 = var_74;
            var_76 = var_68 * 4294967295UL;
            local_sp_1 = local_sp_0;
            r81_1 = _pre_phi336;
            rdi3_0 = var_68;
            rsi5_1 = var_76;
            rax_4 = rax_3;
            rax_5 = var_76;
            rbp_1 = rbp_0;
            local_sp_2 = local_sp_0;
            while (1U)
                {
                    rsi5_3 = rsi5_1;
                    state_0x9018_1 = state_0x9018_0;
                    state_0x9010_1 = state_0x9010_0;
                    state_0x82d8_1 = state_0x82d8_0;
                    state_0x9080_1 = state_0x9080_0;
                    state_0x8248_1 = state_0x8248_0;
                    state_0x9018_3 = state_0x9018_0;
                    state_0x9010_3 = state_0x9010_0;
                    state_0x82d8_3 = state_0x82d8_0;
                    state_0x9080_3 = state_0x9080_0;
                    state_0x8248_3 = state_0x8248_0;
                    state_0x9018_4 = state_0x9018_0;
                    state_0x9010_4 = state_0x9010_0;
                    state_0x82d8_4 = state_0x82d8_0;
                    state_0x9080_4 = state_0x9080_0;
                    state_0x8248_4 = state_0x8248_0;
                    rdi3_1 = rdi3_0;
                    if ((uint64_t)(uint32_t)rax_4 < var_61) {
                        var_82 = (uint64_t)*(uint32_t *)(r81_1 + (-4L));
                        var_83 = rax_4 << 32UL;
                        var_84 = (uint64_t)*(uint32_t *)(r81_1 + (-8L));
                        var_85 = helper_divq_EAX_wrapper((struct type_6 *)(0UL), var_61, var_83 | var_82, 4226249UL, rbx_0, r81_1, rbp_0, 0UL, rdi3_0, var_84, var_61, rsi5_1, var_61, state_0x9018_0, state_0x9010_0, var_10, var_11, state_0x82d8_0, state_0x9080_0, state_0x8248_0);
                        var_86 = var_85.field_0;
                        var_87 = var_85.field_4;
                        var_88 = var_85.field_6;
                        var_89 = var_85.field_7;
                        var_90 = var_85.field_8;
                        var_91 = var_85.field_9;
                        var_92 = var_85.field_10;
                        var_93 = (uint64_t)(uint32_t)var_86;
                        var_94 = var_87 << 32UL;
                        var_95 = var_93 * var_68;
                        var_96 = var_94 | var_84;
                        var_97 = helper_cc_compute_c_wrapper(var_96 - var_95, var_95, var_7, 17U);
                        r14_0 = var_93;
                        r14_1 = var_93;
                        rdx2_2 = var_96;
                        state_0x9018_1 = var_88;
                        state_0x9010_1 = var_89;
                        rax_5 = var_95;
                        state_0x82d8_1 = var_90;
                        state_0x9080_1 = var_91;
                        state_0x8248_1 = var_92;
                        state_0x9018_2 = var_88;
                        state_0x9010_2 = var_89;
                        state_0x82d8_2 = var_90;
                        state_0x9080_2 = var_91;
                        state_0x8248_2 = var_92;
                        if (var_97 == 0UL) {
                            var_98 = rax_5 - rdx2_2;
                            var_99 = helper_cc_compute_c_wrapper(*var_73 - var_98, var_98, var_7, 17U);
                            r14_1 = (uint64_t)(((uint32_t)r14_0 + ((uint32_t)(uint64_t)(unsigned char)var_99 ^ 1U)) + (-2));
                            state_0x9018_2 = state_0x9018_1;
                            state_0x9010_2 = state_0x9010_1;
                            state_0x82d8_2 = state_0x82d8_1;
                            state_0x9080_2 = state_0x9080_1;
                            state_0x8248_2 = state_0x8248_1;
                        }
                        r14_2 = r14_1;
                        state_0x9018_5 = state_0x9018_2;
                        state_0x9010_5 = state_0x9010_2;
                        state_0x82d8_5 = state_0x82d8_2;
                        state_0x9080_5 = state_0x9080_2;
                        state_0x8248_5 = state_0x8248_2;
                        state_0x9018_3 = state_0x9018_2;
                        state_0x9010_3 = state_0x9010_2;
                        state_0x82d8_3 = state_0x82d8_2;
                        state_0x9080_3 = state_0x9080_2;
                        state_0x8248_3 = state_0x8248_2;
                        if (r14_1 != 0UL) {
                            _pre340 = (uint32_t *)r81_1;
                            _pre_phi341 = _pre340;
                            *_pre_phi341 = (uint32_t)r14_4;
                            var_123 = r81_1 + (-4L);
                            r81_1 = var_123;
                            rdi3_0 = rdi3_1;
                            rsi5_1 = rsi5_3;
                            state_0x9018_0 = state_0x9018_5;
                            state_0x9010_0 = state_0x9010_5;
                            state_0x82d8_0 = state_0x82d8_5;
                            state_0x9080_0 = state_0x9080_5;
                            state_0x8248_0 = state_0x8248_5;
                            if (var_62 == r81_1) {
                                break;
                            }
                            rax_4 = (uint64_t)*(uint32_t *)var_123;
                            continue;
                        }
                        r15_0 = (uint64_t)(uint32_t)r14_2;
                        r14_3 = r14_2;
                        state_0x9018_4 = state_0x9018_3;
                        state_0x9010_4 = state_0x9010_3;
                        state_0x82d8_4 = state_0x82d8_3;
                        state_0x9080_4 = state_0x9080_3;
                        state_0x8248_4 = state_0x8248_3;
                    } else {
                        var_77 = helper_cc_compute_c_wrapper(var_61 - rax_4, rax_4, var_7, 16U);
                        var_78 = (uint64_t)*(uint32_t *)(r81_1 + (-4L));
                        var_79 = var_61 + var_78;
                        var_80 = helper_cc_compute_c_wrapper(var_79, var_78, var_7, 8U);
                        if (var_77 != 0UL & var_80 != 0UL) {
                            var_81 = (var_79 << 32UL) | (uint64_t)*(uint32_t *)(r81_1 + (-8L));
                            rdx2_2 = var_81;
                            if (var_76 <= var_81) {
                                var_98 = rax_5 - rdx2_2;
                                var_99 = helper_cc_compute_c_wrapper(*var_73 - var_98, var_98, var_7, 17U);
                                r14_1 = (uint64_t)(((uint32_t)r14_0 + ((uint32_t)(uint64_t)(unsigned char)var_99 ^ 1U)) + (-2));
                                state_0x9018_2 = state_0x9018_1;
                                state_0x9010_2 = state_0x9010_1;
                                state_0x82d8_2 = state_0x82d8_1;
                                state_0x9080_2 = state_0x9080_1;
                                state_0x8248_2 = state_0x8248_1;
                                r14_2 = r14_1;
                                state_0x9018_5 = state_0x9018_2;
                                state_0x9010_5 = state_0x9010_2;
                                state_0x82d8_5 = state_0x82d8_2;
                                state_0x9080_5 = state_0x9080_2;
                                state_0x8248_5 = state_0x8248_2;
                                state_0x9018_3 = state_0x9018_2;
                                state_0x9010_3 = state_0x9010_2;
                                state_0x82d8_3 = state_0x82d8_2;
                                state_0x9080_3 = state_0x9080_2;
                                state_0x8248_3 = state_0x8248_2;
                                if (r14_1 == 0UL) {
                                    _pre340 = (uint32_t *)r81_1;
                                    _pre_phi341 = _pre340;
                                    *_pre_phi341 = (uint32_t)r14_4;
                                    var_123 = r81_1 + (-4L);
                                    r81_1 = var_123;
                                    rdi3_0 = rdi3_1;
                                    rsi5_1 = rsi5_3;
                                    state_0x9018_0 = state_0x9018_5;
                                    state_0x9010_0 = state_0x9010_5;
                                    state_0x82d8_0 = state_0x82d8_5;
                                    state_0x9080_0 = state_0x9080_5;
                                    state_0x8248_0 = state_0x8248_5;
                                    if (var_62 == r81_1) {
                                        break;
                                    }
                                    rax_4 = (uint64_t)*(uint32_t *)var_123;
                                    continue;
                                }
                            }
                            r15_0 = (uint64_t)(uint32_t)r14_2;
                            r14_3 = r14_2;
                            state_0x9018_4 = state_0x9018_3;
                            state_0x9010_4 = state_0x9010_3;
                            state_0x82d8_4 = state_0x82d8_3;
                            state_0x9080_4 = state_0x9080_3;
                            state_0x8248_4 = state_0x8248_3;
                        }
                    }
                }
            var_124 = *var_71;
            var_125 = *var_65;
            var_126 = *var_70;
            var_127 = *var_63;
            var_128 = *(uint32_t *)((var_32 + var_126) + (-4L));
            var_129 = (*(uint32_t *)(((var_124 << 2UL) + var_125) + (-4L)) == 0U) ? *var_67 : var_124;
            r96_1 = var_127;
            r12_1 = var_126;
            r12_0 = var_126;
            r13_1 = var_125;
            r14_5 = var_129;
            r13_2 = var_125;
            r14_6 = var_129;
            r96_2 = var_127;
            if (var_128 == 0U) {
                while (1U)
                    {
                        var_130 = rdx2_3 + (-1L);
                        rdx2_3 = var_130;
                        rdx2_4 = var_130;
                        if (var_130 != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        if (*(uint32_t *)(((var_130 << 2UL) + var_126) + (-4L)) == 0U) {
                            continue;
                        }
                        loop_state_var = 1U;
                        break;
                    }
                var_131 = helper_cc_compute_c_wrapper(rbx_0 - rdx2_4, rdx2_4, var_7, 17U);
                rdx2_7 = rdx2_4;
                if (var_131 != 0UL) {
                    r13_3 = r13_2;
                    r14_7 = r14_6;
                    r96_3 = r96_2;
                    r13_4 = r13_2;
                    r12_3 = r12_1;
                    r12_2 = r12_1;
                    local_sp_3 = local_sp_2;
                    r96_4 = r96_2;
                    rax_12 = r13_2;
                    local_sp_4 = local_sp_2;
                    if (r14_6 != 0UL) {
                        *(uint32_t *)rax_12 = 1U;
                        r12_4 = r12_3;
                        r13_5 = r13_4;
                        r14_9 = r14_8;
                        r96_5 = r96_4;
                        local_sp_5 = local_sp_4;
                        local_sp_6 = local_sp_5;
                        r12_5 = r12_4;
                        if (r96_5 == 0UL) {
                            var_159 = local_sp_5 + (-8L);
                            *(uint64_t *)var_159 = 4226556UL;
                            indirect_placeholder();
                            local_sp_6 = var_159;
                        }
                        **(uint64_t **)(local_sp_6 + 24UL) = r13_5;
                        **(uint64_t **)(local_sp_6 + 16UL) = r14_9;
                        return r12_5;
                    }
                    var_149 = (uint64_t)*(uint32_t *)r13_2;
                    rax_10 = var_149;
                    r13_4 = r13_3;
                    rax_11 = rax_10;
                    r12_3 = r12_2;
                    r12_4 = r12_2;
                    r96_4 = r96_3;
                    local_sp_4 = local_sp_3;
                    r13_5 = r13_3;
                    r14_9 = r14_7;
                    r96_5 = r96_3;
                    local_sp_5 = local_sp_3;
                    while (1U)
                        {
                            var_153 = (uint32_t)rax_11 + 1U;
                            var_154 = (uint64_t)var_153;
                            *(uint32_t *)((rdx2_8 << 2UL) + r13_3) = var_153;
                            if (var_154 != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            var_155 = rdx2_8 + 1UL;
                            var_156 = helper_cc_compute_c_wrapper(var_155 - r14_7, r14_7, var_7, 17U);
                            rdx2_8 = var_155;
                            if (var_156 != 0UL) {
                                var_157 = (r14_7 << 2UL) + r13_3;
                                var_158 = r14_7 + 1UL;
                                r14_8 = var_158;
                                rax_12 = var_157;
                                loop_state_var = 1U;
                                break;
                            }
                            rax_11 = (uint64_t)*(uint32_t *)((var_155 << 2UL) + r13_3);
                            continue;
                        }
                    switch (loop_state_var) {
                      case 1U:
                        {
                            *(uint32_t *)rax_12 = 1U;
                            r12_4 = r12_3;
                            r13_5 = r13_4;
                            r14_9 = r14_8;
                            r96_5 = r96_4;
                            local_sp_5 = local_sp_4;
                        }
                        break;
                      case 0U:
                        {
                            local_sp_6 = local_sp_5;
                            r12_5 = r12_4;
                            if (r96_5 == 0UL) {
                                var_159 = local_sp_5 + (-8L);
                                *(uint64_t *)var_159 = 4226556UL;
                                indirect_placeholder();
                                local_sp_6 = var_159;
                            }
                            **(uint64_t **)(local_sp_6 + 24UL) = r13_5;
                            **(uint64_t **)(local_sp_6 + 16UL) = r14_9;
                            return r12_5;
                        }
                        break;
                    }
                }
            } else {
                var_131 = helper_cc_compute_c_wrapper(rbx_0 - rdx2_4, rdx2_4, var_7, 17U);
                rdx2_7 = rdx2_4;
                if (var_131 == 0UL) {
                    r13_3 = r13_1;
                    r14_7 = r14_5;
                    r96_3 = r96_1;
                    r12_1 = r12_0;
                    r13_2 = r13_1;
                    r14_6 = r14_5;
                    r96_2 = r96_1;
                    local_sp_2 = local_sp_1;
                    r12_2 = r12_0;
                    r12_4 = r12_0;
                    local_sp_3 = local_sp_1;
                    r13_5 = r13_1;
                    r96_5 = r96_1;
                    local_sp_5 = local_sp_1;
                    while (1U)
                        {
                            if (rax_9 > rdx2_7) {
                                var_150 = helper_cc_compute_c_wrapper(rax_9 - rbx_0, rbx_0, var_7, 17U);
                                if (var_150 != 0UL) {
                                    var_151 = (uint64_t)*(uint32_t *)((rax_9 << 2UL) + rbp_1);
                                    rsi5_4 = var_151;
                                    r14_9 = r14_5;
                                    if (rsi5_4 <= (uint64_t)(uint32_t)rcx4_4) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                }
                            }
                            if (rax_9 == 0UL) {
                                var_145 = helper_cc_compute_c_wrapper(0UL - rdx2_7, rdx2_7, var_7, 17U);
                                if (var_145 != 0UL) {
                                    var_150 = helper_cc_compute_c_wrapper(rax_9 - rbx_0, rbx_0, var_7, 17U);
                                    if (var_150 != 0UL) {
                                        var_151 = (uint64_t)*(uint32_t *)((rax_9 << 2UL) + rbp_1);
                                        rsi5_4 = var_151;
                                        r14_9 = r14_5;
                                        if (rsi5_4 <= (uint64_t)(uint32_t)rcx4_4) {
                                            loop_state_var = 0U;
                                            break;
                                        }
                                    }
                                    if (rax_9 == 0UL) {
                                        rax_9 = rax_9 + (-1L);
                                        continue;
                                    }
                                    if (r14_5 != 0UL) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    var_152 = (uint64_t)*(uint32_t *)r13_1;
                                    rax_10 = var_152;
                                    r14_9 = r14_5;
                                    if ((var_152 & 1UL) != 0UL) {
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    loop_state_var = 0U;
                                    break;
                                }
                                _pre338 = 0UL + r12_0;
                                _pre_phi339 = _pre338;
                                rcx4_3 = rcx4_2 | (uint64_t)((uint32_t)((uint64_t)*(uint32_t *)_pre_phi339 << 1UL) & (-2));
                            } else {
                                var_142 = (rax_9 << 2UL) + r12_0;
                                var_143 = (uint64_t)(*(uint32_t *)(var_142 + (-4L)) >> 31U);
                                var_144 = helper_cc_compute_c_wrapper(rax_9 - rdx2_7, rdx2_7, var_7, 17U);
                                _pre_phi339 = var_142;
                                rcx4_2 = var_143;
                                rcx4_3 = var_143;
                                if (var_144 == 0UL) {
                                    rcx4_3 = rcx4_2 | (uint64_t)((uint32_t)((uint64_t)*(uint32_t *)_pre_phi339 << 1UL) & (-2));
                                }
                            }
                            var_146 = helper_cc_compute_c_wrapper(rax_9 - rbx_0, rbx_0, var_7, 17U);
                            rcx4_4 = rcx4_3;
                            if (var_146 == 0UL) {
                                if ((uint64_t)(uint32_t)rcx4_3 == 0UL) {
                                    loop_state_var = 2U;
                                    break;
                                }
                            }
                            var_147 = (uint64_t)*(uint32_t *)((rax_9 << 2UL) + rbp_1);
                            var_148 = helper_cc_compute_c_wrapper(var_147 - rcx4_3, rcx4_3, var_7, 16U);
                            rsi5_4 = var_147;
                            if (var_148 == 0UL) {
                                loop_state_var = 2U;
                                break;
                            }
                            r14_9 = r14_5;
                            if (rsi5_4 <= (uint64_t)(uint32_t)rcx4_4) {
                                loop_state_var = 0U;
                                break;
                            }
                            if (rax_9 == 0UL) {
                                rax_9 = rax_9 + (-1L);
                                continue;
                            }
                            if (r14_5 != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            var_152 = (uint64_t)*(uint32_t *)r13_1;
                            rax_10 = var_152;
                            r14_9 = r14_5;
                            if ((var_152 & 1UL) != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            loop_state_var = 0U;
                            break;
                        }
                    switch (loop_state_var) {
                      case 0U:
                        {
                            local_sp_6 = local_sp_5;
                            r12_5 = r12_4;
                            if (r96_5 == 0UL) {
                                var_159 = local_sp_5 + (-8L);
                                *(uint64_t *)var_159 = 4226556UL;
                                indirect_placeholder();
                                local_sp_6 = var_159;
                            }
                            **(uint64_t **)(local_sp_6 + 24UL) = r13_5;
                            **(uint64_t **)(local_sp_6 + 16UL) = r14_9;
                            return r12_5;
                        }
                        break;
                      case 2U:
                        {
                            r13_3 = r13_2;
                            r14_7 = r14_6;
                            r96_3 = r96_2;
                            r13_4 = r13_2;
                            r12_3 = r12_1;
                            r12_2 = r12_1;
                            local_sp_3 = local_sp_2;
                            r96_4 = r96_2;
                            rax_12 = r13_2;
                            local_sp_4 = local_sp_2;
                            if (r14_6 != 0UL) {
                                *(uint32_t *)rax_12 = 1U;
                                r12_4 = r12_3;
                                r13_5 = r13_4;
                                r14_9 = r14_8;
                                r96_5 = r96_4;
                                local_sp_5 = local_sp_4;
                                local_sp_6 = local_sp_5;
                                r12_5 = r12_4;
                                if (r96_5 == 0UL) {
                                    var_159 = local_sp_5 + (-8L);
                                    *(uint64_t *)var_159 = 4226556UL;
                                    indirect_placeholder();
                                    local_sp_6 = var_159;
                                }
                                **(uint64_t **)(local_sp_6 + 24UL) = r13_5;
                                **(uint64_t **)(local_sp_6 + 16UL) = r14_9;
                                return r12_5;
                            }
                            var_149 = (uint64_t)*(uint32_t *)r13_2;
                            rax_10 = var_149;
                            r13_4 = r13_3;
                            rax_11 = rax_10;
                            r12_3 = r12_2;
                            r12_4 = r12_2;
                            r96_4 = r96_3;
                            local_sp_4 = local_sp_3;
                            r13_5 = r13_3;
                            r14_9 = r14_7;
                            r96_5 = r96_3;
                            local_sp_5 = local_sp_3;
                            while (1U)
                                {
                                    var_153 = (uint32_t)rax_11 + 1U;
                                    var_154 = (uint64_t)var_153;
                                    *(uint32_t *)((rdx2_8 << 2UL) + r13_3) = var_153;
                                    if (var_154 != 0UL) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    var_155 = rdx2_8 + 1UL;
                                    var_156 = helper_cc_compute_c_wrapper(var_155 - r14_7, r14_7, var_7, 17U);
                                    rdx2_8 = var_155;
                                    if (var_156 != 0UL) {
                                        var_157 = (r14_7 << 2UL) + r13_3;
                                        var_158 = r14_7 + 1UL;
                                        r14_8 = var_158;
                                        rax_12 = var_157;
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    rax_11 = (uint64_t)*(uint32_t *)((var_155 << 2UL) + r13_3);
                                    continue;
                                }
                            *(uint32_t *)rax_12 = 1U;
                            r12_4 = r12_3;
                            r13_5 = r13_4;
                            r14_9 = r14_8;
                            r96_5 = r96_4;
                            local_sp_5 = local_sp_4;
                        }
                        break;
                      case 1U:
                        {
                            r13_4 = r13_3;
                            rax_11 = rax_10;
                            r12_3 = r12_2;
                            r12_4 = r12_2;
                            r96_4 = r96_3;
                            local_sp_4 = local_sp_3;
                            r13_5 = r13_3;
                            r14_9 = r14_7;
                            r96_5 = r96_3;
                            local_sp_5 = local_sp_3;
                            while (1U)
                                {
                                    var_153 = (uint32_t)rax_11 + 1U;
                                    var_154 = (uint64_t)var_153;
                                    *(uint32_t *)((rdx2_8 << 2UL) + r13_3) = var_153;
                                    if (var_154 != 0UL) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    var_155 = rdx2_8 + 1UL;
                                    var_156 = helper_cc_compute_c_wrapper(var_155 - r14_7, r14_7, var_7, 17U);
                                    rdx2_8 = var_155;
                                    if (var_156 != 0UL) {
                                        var_157 = (r14_7 << 2UL) + r13_3;
                                        var_158 = r14_7 + 1UL;
                                        r14_8 = var_158;
                                        rax_12 = var_157;
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    rax_11 = (uint64_t)*(uint32_t *)((var_155 << 2UL) + r13_3);
                                    continue;
                                }
                            *(uint32_t *)rax_12 = 1U;
                            r12_4 = r12_3;
                            r13_5 = r13_4;
                            r14_9 = r14_8;
                            r96_5 = r96_4;
                            local_sp_5 = local_sp_4;
                        }
                        break;
                    }
                } else {
                    r13_3 = r13_2;
                    r14_7 = r14_6;
                    r96_3 = r96_2;
                    r13_4 = r13_2;
                    r12_3 = r12_1;
                    r12_2 = r12_1;
                    local_sp_3 = local_sp_2;
                    r96_4 = r96_2;
                    rax_12 = r13_2;
                    local_sp_4 = local_sp_2;
                    if (r14_6 == 0UL) {
                        *(uint32_t *)rax_12 = 1U;
                        r12_4 = r12_3;
                        r13_5 = r13_4;
                        r14_9 = r14_8;
                        r96_5 = r96_4;
                        local_sp_5 = local_sp_4;
                        local_sp_6 = local_sp_5;
                        r12_5 = r12_4;
                        if (r96_5 == 0UL) {
                            var_159 = local_sp_5 + (-8L);
                            *(uint64_t *)var_159 = 4226556UL;
                            indirect_placeholder();
                            local_sp_6 = var_159;
                        }
                        **(uint64_t **)(local_sp_6 + 24UL) = r13_5;
                        **(uint64_t **)(local_sp_6 + 16UL) = r14_9;
                        return r12_5;
                    }
                    var_149 = (uint64_t)*(uint32_t *)r13_2;
                    rax_10 = var_149;
                    r13_4 = r13_3;
                    rax_11 = rax_10;
                    r12_3 = r12_2;
                    r12_4 = r12_2;
                    r96_4 = r96_3;
                    local_sp_4 = local_sp_3;
                    r13_5 = r13_3;
                    r14_9 = r14_7;
                    r96_5 = r96_3;
                    local_sp_5 = local_sp_3;
                    while (1U)
                        {
                            var_153 = (uint32_t)rax_11 + 1U;
                            var_154 = (uint64_t)var_153;
                            *(uint32_t *)((rdx2_8 << 2UL) + r13_3) = var_153;
                            if (var_154 != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            var_155 = rdx2_8 + 1UL;
                            var_156 = helper_cc_compute_c_wrapper(var_155 - r14_7, r14_7, var_7, 17U);
                            rdx2_8 = var_155;
                            if (var_156 != 0UL) {
                                var_157 = (r14_7 << 2UL) + r13_3;
                                var_158 = r14_7 + 1UL;
                                r14_8 = var_158;
                                rax_12 = var_157;
                                loop_state_var = 1U;
                                break;
                            }
                            rax_11 = (uint64_t)*(uint32_t *)((var_155 << 2UL) + r13_3);
                            continue;
                        }
                    switch (loop_state_var) {
                      case 1U:
                        {
                            *(uint32_t *)rax_12 = 1U;
                            r12_4 = r12_3;
                            r13_5 = r13_4;
                            r14_9 = r14_8;
                            r96_5 = r96_4;
                            local_sp_5 = local_sp_4;
                        }
                        break;
                      case 0U:
                        {
                            local_sp_6 = local_sp_5;
                            r12_5 = r12_4;
                            if (r96_5 == 0UL) {
                                var_159 = local_sp_5 + (-8L);
                                *(uint64_t *)var_159 = 4226556UL;
                                indirect_placeholder();
                                local_sp_6 = var_159;
                            }
                            **(uint64_t **)(local_sp_6 + 24UL) = r13_5;
                            **(uint64_t **)(local_sp_6 + 16UL) = r14_9;
                            return r12_5;
                        }
                        break;
                    }
                }
            }
        }
        break;
      case 6U:
      case 4U:
      case 2U:
      case 1U:
      case 0U:
        {
            switch (loop_state_var) {
              case 6U:
                {
                    return r12_5;
                }
                break;
              case 4U:
              case 2U:
              case 1U:
              case 0U:
                {
                    switch (loop_state_var) {
                      case 4U:
                      case 1U:
                      case 0U:
                        {
                            switch (loop_state_var) {
                              case 0U:
                                {
                                    r13_3 = r13_1;
                                    r14_7 = r14_5;
                                    r96_3 = r96_1;
                                    r12_1 = r12_0;
                                    r13_2 = r13_1;
                                    r14_6 = r14_5;
                                    r96_2 = r96_1;
                                    local_sp_2 = local_sp_1;
                                    r12_2 = r12_0;
                                    r12_4 = r12_0;
                                    local_sp_3 = local_sp_1;
                                    r13_5 = r13_1;
                                    r96_5 = r96_1;
                                    local_sp_5 = local_sp_1;
                                    while (1U)
                                        {
                                            if (rax_9 > rdx2_7) {
                                                var_150 = helper_cc_compute_c_wrapper(rax_9 - rbx_0, rbx_0, var_7, 17U);
                                                if (var_150 != 0UL) {
                                                    var_151 = (uint64_t)*(uint32_t *)((rax_9 << 2UL) + rbp_1);
                                                    rsi5_4 = var_151;
                                                    r14_9 = r14_5;
                                                    if (rsi5_4 <= (uint64_t)(uint32_t)rcx4_4) {
                                                        loop_state_var = 0U;
                                                        break;
                                                    }
                                                }
                                            }
                                            if (rax_9 == 0UL) {
                                                var_145 = helper_cc_compute_c_wrapper(0UL - rdx2_7, rdx2_7, var_7, 17U);
                                                if (var_145 != 0UL) {
                                                    var_150 = helper_cc_compute_c_wrapper(rax_9 - rbx_0, rbx_0, var_7, 17U);
                                                    if (var_150 != 0UL) {
                                                        var_151 = (uint64_t)*(uint32_t *)((rax_9 << 2UL) + rbp_1);
                                                        rsi5_4 = var_151;
                                                        r14_9 = r14_5;
                                                        if (rsi5_4 <= (uint64_t)(uint32_t)rcx4_4) {
                                                            loop_state_var = 0U;
                                                            break;
                                                        }
                                                    }
                                                    if (rax_9 == 0UL) {
                                                        rax_9 = rax_9 + (-1L);
                                                        continue;
                                                    }
                                                    if (r14_5 != 0UL) {
                                                        loop_state_var = 0U;
                                                        break;
                                                    }
                                                    var_152 = (uint64_t)*(uint32_t *)r13_1;
                                                    rax_10 = var_152;
                                                    r14_9 = r14_5;
                                                    if ((var_152 & 1UL) != 0UL) {
                                                        loop_state_var = 1U;
                                                        break;
                                                    }
                                                    loop_state_var = 0U;
                                                    break;
                                                }
                                                _pre338 = 0UL + r12_0;
                                                _pre_phi339 = _pre338;
                                                rcx4_3 = rcx4_2 | (uint64_t)((uint32_t)((uint64_t)*(uint32_t *)_pre_phi339 << 1UL) & (-2));
                                            } else {
                                                var_142 = (rax_9 << 2UL) + r12_0;
                                                var_143 = (uint64_t)(*(uint32_t *)(var_142 + (-4L)) >> 31U);
                                                var_144 = helper_cc_compute_c_wrapper(rax_9 - rdx2_7, rdx2_7, var_7, 17U);
                                                _pre_phi339 = var_142;
                                                rcx4_2 = var_143;
                                                rcx4_3 = var_143;
                                                if (var_144 == 0UL) {
                                                    rcx4_3 = rcx4_2 | (uint64_t)((uint32_t)((uint64_t)*(uint32_t *)_pre_phi339 << 1UL) & (-2));
                                                }
                                            }
                                            var_146 = helper_cc_compute_c_wrapper(rax_9 - rbx_0, rbx_0, var_7, 17U);
                                            rcx4_4 = rcx4_3;
                                            if (var_146 == 0UL) {
                                                if ((uint64_t)(uint32_t)rcx4_3 == 0UL) {
                                                    loop_state_var = 2U;
                                                    break;
                                                }
                                            }
                                            var_147 = (uint64_t)*(uint32_t *)((rax_9 << 2UL) + rbp_1);
                                            var_148 = helper_cc_compute_c_wrapper(var_147 - rcx4_3, rcx4_3, var_7, 16U);
                                            rsi5_4 = var_147;
                                            if (var_148 == 0UL) {
                                                loop_state_var = 2U;
                                                break;
                                            }
                                            r14_9 = r14_5;
                                            if (rsi5_4 <= (uint64_t)(uint32_t)rcx4_4) {
                                                loop_state_var = 0U;
                                                break;
                                            }
                                            if (rax_9 == 0UL) {
                                                rax_9 = rax_9 + (-1L);
                                                continue;
                                            }
                                            if (r14_5 != 0UL) {
                                                loop_state_var = 0U;
                                                break;
                                            }
                                            var_152 = (uint64_t)*(uint32_t *)r13_1;
                                            rax_10 = var_152;
                                            r14_9 = r14_5;
                                            if ((var_152 & 1UL) != 0UL) {
                                                loop_state_var = 1U;
                                                break;
                                            }
                                            loop_state_var = 0U;
                                            break;
                                        }
                                    switch (loop_state_var) {
                                      case 0U:
                                        {
                                            local_sp_6 = local_sp_5;
                                            r12_5 = r12_4;
                                            if (r96_5 == 0UL) {
                                                var_159 = local_sp_5 + (-8L);
                                                *(uint64_t *)var_159 = 4226556UL;
                                                indirect_placeholder();
                                                local_sp_6 = var_159;
                                            }
                                            **(uint64_t **)(local_sp_6 + 24UL) = r13_5;
                                            **(uint64_t **)(local_sp_6 + 16UL) = r14_9;
                                            return r12_5;
                                        }
                                        break;
                                      case 1U:
                                        {
                                            r13_4 = r13_3;
                                            rax_11 = rax_10;
                                            r12_3 = r12_2;
                                            r12_4 = r12_2;
                                            r96_4 = r96_3;
                                            local_sp_4 = local_sp_3;
                                            r13_5 = r13_3;
                                            r14_9 = r14_7;
                                            r96_5 = r96_3;
                                            local_sp_5 = local_sp_3;
                                            while (1U)
                                                {
                                                    var_153 = (uint32_t)rax_11 + 1U;
                                                    var_154 = (uint64_t)var_153;
                                                    *(uint32_t *)((rdx2_8 << 2UL) + r13_3) = var_153;
                                                    if (var_154 != 0UL) {
                                                        loop_state_var = 0U;
                                                        break;
                                                    }
                                                    var_155 = rdx2_8 + 1UL;
                                                    var_156 = helper_cc_compute_c_wrapper(var_155 - r14_7, r14_7, var_7, 17U);
                                                    rdx2_8 = var_155;
                                                    if (var_156 != 0UL) {
                                                        var_157 = (r14_7 << 2UL) + r13_3;
                                                        var_158 = r14_7 + 1UL;
                                                        r14_8 = var_158;
                                                        rax_12 = var_157;
                                                        loop_state_var = 1U;
                                                        break;
                                                    }
                                                    rax_11 = (uint64_t)*(uint32_t *)((var_155 << 2UL) + r13_3);
                                                    continue;
                                                }
                                            *(uint32_t *)rax_12 = 1U;
                                            r12_4 = r12_3;
                                            r13_5 = r13_4;
                                            r14_9 = r14_8;
                                            r96_5 = r96_4;
                                            local_sp_5 = local_sp_4;
                                        }
                                        break;
                                      case 2U:
                                        {
                                            r13_3 = r13_2;
                                            r14_7 = r14_6;
                                            r96_3 = r96_2;
                                            r13_4 = r13_2;
                                            r12_3 = r12_1;
                                            r12_2 = r12_1;
                                            local_sp_3 = local_sp_2;
                                            r96_4 = r96_2;
                                            rax_12 = r13_2;
                                            local_sp_4 = local_sp_2;
                                            if (r14_6 != 0UL) {
                                                *(uint32_t *)rax_12 = 1U;
                                                r12_4 = r12_3;
                                                r13_5 = r13_4;
                                                r14_9 = r14_8;
                                                r96_5 = r96_4;
                                                local_sp_5 = local_sp_4;
                                                local_sp_6 = local_sp_5;
                                                r12_5 = r12_4;
                                                if (r96_5 == 0UL) {
                                                    var_159 = local_sp_5 + (-8L);
                                                    *(uint64_t *)var_159 = 4226556UL;
                                                    indirect_placeholder();
                                                    local_sp_6 = var_159;
                                                }
                                                **(uint64_t **)(local_sp_6 + 24UL) = r13_5;
                                                **(uint64_t **)(local_sp_6 + 16UL) = r14_9;
                                                return r12_5;
                                            }
                                            var_149 = (uint64_t)*(uint32_t *)r13_2;
                                            rax_10 = var_149;
                                            r13_4 = r13_3;
                                            rax_11 = rax_10;
                                            r12_3 = r12_2;
                                            r12_4 = r12_2;
                                            r96_4 = r96_3;
                                            local_sp_4 = local_sp_3;
                                            r13_5 = r13_3;
                                            r14_9 = r14_7;
                                            r96_5 = r96_3;
                                            local_sp_5 = local_sp_3;
                                            while (1U)
                                                {
                                                    var_153 = (uint32_t)rax_11 + 1U;
                                                    var_154 = (uint64_t)var_153;
                                                    *(uint32_t *)((rdx2_8 << 2UL) + r13_3) = var_153;
                                                    if (var_154 != 0UL) {
                                                        loop_state_var = 0U;
                                                        break;
                                                    }
                                                    var_155 = rdx2_8 + 1UL;
                                                    var_156 = helper_cc_compute_c_wrapper(var_155 - r14_7, r14_7, var_7, 17U);
                                                    rdx2_8 = var_155;
                                                    if (var_156 != 0UL) {
                                                        var_157 = (r14_7 << 2UL) + r13_3;
                                                        var_158 = r14_7 + 1UL;
                                                        r14_8 = var_158;
                                                        rax_12 = var_157;
                                                        loop_state_var = 1U;
                                                        break;
                                                    }
                                                    rax_11 = (uint64_t)*(uint32_t *)((var_155 << 2UL) + r13_3);
                                                    continue;
                                                }
                                            *(uint32_t *)rax_12 = 1U;
                                            r12_4 = r12_3;
                                            r13_5 = r13_4;
                                            r14_9 = r14_8;
                                            r96_5 = r96_4;
                                            local_sp_5 = local_sp_4;
                                        }
                                        break;
                                    }
                                }
                                break;
                              case 4U:
                                {
                                    var_135 = rcx4_1 << 2UL;
                                    var_136 = helper_divq_EAX_wrapper((struct type_6 *)(0UL), var_132, (rax_8 << 32UL) | (uint64_t)*(uint32_t *)((var_135 + rsi) + (-4L)), 4226654UL, 1UL, r81_3, rcx, 0UL, var_134, rcx4_1, var_29, var_132, var_133, state_0x9018_6, state_0x9010_6, var_10, var_11, state_0x82d8_6, state_0x9080_6, state_0x8248_6);
                                    *(uint32_t *)(var_135 + var_24) = (uint32_t)var_136.field_0;
                                    var_137 = rcx4_1 + (-1L);
                                    var_138 = (var_137 == 0UL);
                                    var_139 = (uint32_t)var_136.field_4;
                                    var_140 = (uint64_t)var_139;
                                    rcx4_1 = var_137;
                                    rax_8 = var_140;
                                    while (!var_138)
                                        {
                                            state_0x9018_6 = var_136.field_6;
                                            state_0x9010_6 = var_136.field_7;
                                            state_0x82d8_6 = var_136.field_8;
                                            state_0x9080_6 = var_136.field_9;
                                            state_0x8248_6 = var_136.field_10;
                                            var_135 = rcx4_1 << 2UL;
                                            var_136 = helper_divq_EAX_wrapper((struct type_6 *)(0UL), var_132, (rax_8 << 32UL) | (uint64_t)*(uint32_t *)((var_135 + rsi) + (-4L)), 4226654UL, 1UL, r81_3, rcx, 0UL, var_134, rcx4_1, var_29, var_132, var_133, state_0x9018_6, state_0x9010_6, var_10, var_11, state_0x82d8_6, state_0x9080_6, state_0x8248_6);
                                            *(uint32_t *)(var_135 + var_24) = (uint32_t)var_136.field_0;
                                            var_137 = rcx4_1 + (-1L);
                                            var_138 = (var_137 == 0UL);
                                            var_139 = (uint32_t)var_136.field_4;
                                            var_140 = (uint64_t)var_139;
                                            rcx4_1 = var_137;
                                            rax_8 = var_140;
                                        }
                                    if (var_140 == 0UL) {
                                        rdx2_5 = 0UL;
                                        if (*(uint32_t *)(var_134 + var_24) == 0U) {
                                            var_141 = r81_3 + (-1L);
                                            r14_5 = var_141;
                                            rdx2_7 = rdx2_5;
                                        }
                                    } else {
                                        *(uint32_t *)var_24 = var_139;
                                        if (*(uint32_t *)(var_134 + var_24) == 0U) {
                                            var_141 = r81_3 + (-1L);
                                            r14_5 = var_141;
                                            rdx2_7 = rdx2_5;
                                        }
                                    }
                                }
                                break;
                              case 1U:
                                {
                                    rdx2_5 = 0UL;
                                    if (*(uint32_t *)(var_134 + var_24) == 0U) {
                                        var_141 = r81_3 + (-1L);
                                        r14_5 = var_141;
                                        rdx2_7 = rdx2_5;
                                    }
                                }
                                break;
                            }
                        }
                        break;
                      case 2U:
                        {
                            var_61 = (uint64_t)*(uint32_t *)r10_0;
                            var_62 = var_32 + var_24;
                            var_63 = (uint64_t *)(local_sp_0 + 64UL);
                            *var_63 = r96_0;
                            var_64 = r81_0 - rbx_0;
                            var_65 = (uint64_t *)(local_sp_0 + 56UL);
                            *var_65 = var_62;
                            var_66 = var_64 + 1UL;
                            var_67 = (uint64_t *)(local_sp_0 + 40UL);
                            *var_67 = var_64;
                            var_68 = (uint64_t)*(uint32_t *)((var_32 + rbp_0) + (-8L));
                            var_69 = var_61 << 32UL;
                            var_70 = (uint64_t *)(local_sp_0 + 72UL);
                            *var_70 = var_24;
                            var_71 = (uint64_t *)(local_sp_0 + 48UL);
                            *var_71 = var_66;
                            var_72 = var_69 | var_68;
                            var_73 = (uint64_t *)(local_sp_0 + 8UL);
                            *var_73 = var_72;
                            var_74 = 0UL - var_32;
                            var_75 = (uint64_t *)(local_sp_0 + 32UL);
                            *var_75 = var_74;
                            var_76 = var_68 * 4294967295UL;
                            local_sp_1 = local_sp_0;
                            r81_1 = _pre_phi336;
                            rdi3_0 = var_68;
                            rsi5_1 = var_76;
                            rax_4 = rax_3;
                            rax_5 = var_76;
                            rbp_1 = rbp_0;
                            local_sp_2 = local_sp_0;
                            while (1U)
                                {
                                    rsi5_3 = rsi5_1;
                                    state_0x9018_1 = state_0x9018_0;
                                    state_0x9010_1 = state_0x9010_0;
                                    state_0x82d8_1 = state_0x82d8_0;
                                    state_0x9080_1 = state_0x9080_0;
                                    state_0x8248_1 = state_0x8248_0;
                                    state_0x9018_3 = state_0x9018_0;
                                    state_0x9010_3 = state_0x9010_0;
                                    state_0x82d8_3 = state_0x82d8_0;
                                    state_0x9080_3 = state_0x9080_0;
                                    state_0x8248_3 = state_0x8248_0;
                                    state_0x9018_4 = state_0x9018_0;
                                    state_0x9010_4 = state_0x9010_0;
                                    state_0x82d8_4 = state_0x82d8_0;
                                    state_0x9080_4 = state_0x9080_0;
                                    state_0x8248_4 = state_0x8248_0;
                                    rdi3_1 = rdi3_0;
                                    if ((uint64_t)(uint32_t)rax_4 < var_61) {
                                        var_82 = (uint64_t)*(uint32_t *)(r81_1 + (-4L));
                                        var_83 = rax_4 << 32UL;
                                        var_84 = (uint64_t)*(uint32_t *)(r81_1 + (-8L));
                                        var_85 = helper_divq_EAX_wrapper((struct type_6 *)(0UL), var_61, var_83 | var_82, 4226249UL, rbx_0, r81_1, rbp_0, 0UL, rdi3_0, var_84, var_61, rsi5_1, var_61, state_0x9018_0, state_0x9010_0, var_10, var_11, state_0x82d8_0, state_0x9080_0, state_0x8248_0);
                                        var_86 = var_85.field_0;
                                        var_87 = var_85.field_4;
                                        var_88 = var_85.field_6;
                                        var_89 = var_85.field_7;
                                        var_90 = var_85.field_8;
                                        var_91 = var_85.field_9;
                                        var_92 = var_85.field_10;
                                        var_93 = (uint64_t)(uint32_t)var_86;
                                        var_94 = var_87 << 32UL;
                                        var_95 = var_93 * var_68;
                                        var_96 = var_94 | var_84;
                                        var_97 = helper_cc_compute_c_wrapper(var_96 - var_95, var_95, var_7, 17U);
                                        r14_0 = var_93;
                                        r14_1 = var_93;
                                        rdx2_2 = var_96;
                                        state_0x9018_1 = var_88;
                                        state_0x9010_1 = var_89;
                                        rax_5 = var_95;
                                        state_0x82d8_1 = var_90;
                                        state_0x9080_1 = var_91;
                                        state_0x8248_1 = var_92;
                                        state_0x9018_2 = var_88;
                                        state_0x9010_2 = var_89;
                                        state_0x82d8_2 = var_90;
                                        state_0x9080_2 = var_91;
                                        state_0x8248_2 = var_92;
                                        if (var_97 == 0UL) {
                                            var_98 = rax_5 - rdx2_2;
                                            var_99 = helper_cc_compute_c_wrapper(*var_73 - var_98, var_98, var_7, 17U);
                                            r14_1 = (uint64_t)(((uint32_t)r14_0 + ((uint32_t)(uint64_t)(unsigned char)var_99 ^ 1U)) + (-2));
                                            state_0x9018_2 = state_0x9018_1;
                                            state_0x9010_2 = state_0x9010_1;
                                            state_0x82d8_2 = state_0x82d8_1;
                                            state_0x9080_2 = state_0x9080_1;
                                            state_0x8248_2 = state_0x8248_1;
                                        }
                                        r14_2 = r14_1;
                                        state_0x9018_5 = state_0x9018_2;
                                        state_0x9010_5 = state_0x9010_2;
                                        state_0x82d8_5 = state_0x82d8_2;
                                        state_0x9080_5 = state_0x9080_2;
                                        state_0x8248_5 = state_0x8248_2;
                                        state_0x9018_3 = state_0x9018_2;
                                        state_0x9010_3 = state_0x9010_2;
                                        state_0x82d8_3 = state_0x82d8_2;
                                        state_0x9080_3 = state_0x9080_2;
                                        state_0x8248_3 = state_0x8248_2;
                                        if (r14_1 != 0UL) {
                                            _pre340 = (uint32_t *)r81_1;
                                            _pre_phi341 = _pre340;
                                            *_pre_phi341 = (uint32_t)r14_4;
                                            var_123 = r81_1 + (-4L);
                                            r81_1 = var_123;
                                            rdi3_0 = rdi3_1;
                                            rsi5_1 = rsi5_3;
                                            state_0x9018_0 = state_0x9018_5;
                                            state_0x9010_0 = state_0x9010_5;
                                            state_0x82d8_0 = state_0x82d8_5;
                                            state_0x9080_0 = state_0x9080_5;
                                            state_0x8248_0 = state_0x8248_5;
                                            if (var_62 == r81_1) {
                                                break;
                                            }
                                            rax_4 = (uint64_t)*(uint32_t *)var_123;
                                            continue;
                                        }
                                        r15_0 = (uint64_t)(uint32_t)r14_2;
                                        r14_3 = r14_2;
                                        state_0x9018_4 = state_0x9018_3;
                                        state_0x9010_4 = state_0x9010_3;
                                        state_0x82d8_4 = state_0x82d8_3;
                                        state_0x9080_4 = state_0x9080_3;
                                        state_0x8248_4 = state_0x8248_3;
                                    } else {
                                        var_77 = helper_cc_compute_c_wrapper(var_61 - rax_4, rax_4, var_7, 16U);
                                        var_78 = (uint64_t)*(uint32_t *)(r81_1 + (-4L));
                                        var_79 = var_61 + var_78;
                                        var_80 = helper_cc_compute_c_wrapper(var_79, var_78, var_7, 8U);
                                        if (var_77 != 0UL & var_80 != 0UL) {
                                            var_81 = (var_79 << 32UL) | (uint64_t)*(uint32_t *)(r81_1 + (-8L));
                                            rdx2_2 = var_81;
                                            if (var_76 <= var_81) {
                                                var_98 = rax_5 - rdx2_2;
                                                var_99 = helper_cc_compute_c_wrapper(*var_73 - var_98, var_98, var_7, 17U);
                                                r14_1 = (uint64_t)(((uint32_t)r14_0 + ((uint32_t)(uint64_t)(unsigned char)var_99 ^ 1U)) + (-2));
                                                state_0x9018_2 = state_0x9018_1;
                                                state_0x9010_2 = state_0x9010_1;
                                                state_0x82d8_2 = state_0x82d8_1;
                                                state_0x9080_2 = state_0x9080_1;
                                                state_0x8248_2 = state_0x8248_1;
                                                r14_2 = r14_1;
                                                state_0x9018_5 = state_0x9018_2;
                                                state_0x9010_5 = state_0x9010_2;
                                                state_0x82d8_5 = state_0x82d8_2;
                                                state_0x9080_5 = state_0x9080_2;
                                                state_0x8248_5 = state_0x8248_2;
                                                state_0x9018_3 = state_0x9018_2;
                                                state_0x9010_3 = state_0x9010_2;
                                                state_0x82d8_3 = state_0x82d8_2;
                                                state_0x9080_3 = state_0x9080_2;
                                                state_0x8248_3 = state_0x8248_2;
                                                if (r14_1 == 0UL) {
                                                    _pre340 = (uint32_t *)r81_1;
                                                    _pre_phi341 = _pre340;
                                                    *_pre_phi341 = (uint32_t)r14_4;
                                                    var_123 = r81_1 + (-4L);
                                                    r81_1 = var_123;
                                                    rdi3_0 = rdi3_1;
                                                    rsi5_1 = rsi5_3;
                                                    state_0x9018_0 = state_0x9018_5;
                                                    state_0x9010_0 = state_0x9010_5;
                                                    state_0x82d8_0 = state_0x82d8_5;
                                                    state_0x9080_0 = state_0x9080_5;
                                                    state_0x8248_0 = state_0x8248_5;
                                                    if (var_62 == r81_1) {
                                                        break;
                                                    }
                                                    rax_4 = (uint64_t)*(uint32_t *)var_123;
                                                    continue;
                                                }
                                            }
                                            r15_0 = (uint64_t)(uint32_t)r14_2;
                                            r14_3 = r14_2;
                                            state_0x9018_4 = state_0x9018_3;
                                            state_0x9010_4 = state_0x9010_3;
                                            state_0x82d8_4 = state_0x82d8_3;
                                            state_0x9080_4 = state_0x9080_3;
                                            state_0x8248_4 = state_0x8248_3;
                                        }
                                    }
                                }
                            var_124 = *var_71;
                            var_125 = *var_65;
                            var_126 = *var_70;
                            var_127 = *var_63;
                            var_128 = *(uint32_t *)((var_32 + var_126) + (-4L));
                            var_129 = (*(uint32_t *)(((var_124 << 2UL) + var_125) + (-4L)) == 0U) ? *var_67 : var_124;
                            r96_1 = var_127;
                            r12_1 = var_126;
                            r12_0 = var_126;
                            r13_1 = var_125;
                            r14_5 = var_129;
                            r13_2 = var_125;
                            r14_6 = var_129;
                            r96_2 = var_127;
                            if (var_128 == 0U) {
                                while (1U)
                                    {
                                        var_130 = rdx2_3 + (-1L);
                                        rdx2_3 = var_130;
                                        rdx2_4 = var_130;
                                        if (var_130 != 0UL) {
                                            loop_state_var = 0U;
                                            break;
                                        }
                                        if (*(uint32_t *)(((var_130 << 2UL) + var_126) + (-4L)) == 0U) {
                                            continue;
                                        }
                                        loop_state_var = 1U;
                                        break;
                                    }
                                switch (loop_state_var) {
                                  case 0U:
                                    {
                                        r13_3 = r13_1;
                                        r14_7 = r14_5;
                                        r96_3 = r96_1;
                                        r12_1 = r12_0;
                                        r13_2 = r13_1;
                                        r14_6 = r14_5;
                                        r96_2 = r96_1;
                                        local_sp_2 = local_sp_1;
                                        r12_2 = r12_0;
                                        r12_4 = r12_0;
                                        local_sp_3 = local_sp_1;
                                        r13_5 = r13_1;
                                        r96_5 = r96_1;
                                        local_sp_5 = local_sp_1;
                                        while (1U)
                                            {
                                                if (rax_9 > rdx2_7) {
                                                    var_150 = helper_cc_compute_c_wrapper(rax_9 - rbx_0, rbx_0, var_7, 17U);
                                                    if (var_150 != 0UL) {
                                                        var_151 = (uint64_t)*(uint32_t *)((rax_9 << 2UL) + rbp_1);
                                                        rsi5_4 = var_151;
                                                        r14_9 = r14_5;
                                                        if (rsi5_4 <= (uint64_t)(uint32_t)rcx4_4) {
                                                            loop_state_var = 0U;
                                                            break;
                                                        }
                                                    }
                                                }
                                                if (rax_9 == 0UL) {
                                                    var_145 = helper_cc_compute_c_wrapper(0UL - rdx2_7, rdx2_7, var_7, 17U);
                                                    if (var_145 != 0UL) {
                                                        var_150 = helper_cc_compute_c_wrapper(rax_9 - rbx_0, rbx_0, var_7, 17U);
                                                        if (var_150 != 0UL) {
                                                            var_151 = (uint64_t)*(uint32_t *)((rax_9 << 2UL) + rbp_1);
                                                            rsi5_4 = var_151;
                                                            r14_9 = r14_5;
                                                            if (rsi5_4 <= (uint64_t)(uint32_t)rcx4_4) {
                                                                loop_state_var = 0U;
                                                                break;
                                                            }
                                                        }
                                                        if (rax_9 == 0UL) {
                                                            rax_9 = rax_9 + (-1L);
                                                            continue;
                                                        }
                                                        if (r14_5 != 0UL) {
                                                            loop_state_var = 0U;
                                                            break;
                                                        }
                                                        var_152 = (uint64_t)*(uint32_t *)r13_1;
                                                        rax_10 = var_152;
                                                        r14_9 = r14_5;
                                                        if ((var_152 & 1UL) != 0UL) {
                                                            loop_state_var = 1U;
                                                            break;
                                                        }
                                                        loop_state_var = 0U;
                                                        break;
                                                    }
                                                    _pre338 = 0UL + r12_0;
                                                    _pre_phi339 = _pre338;
                                                    rcx4_3 = rcx4_2 | (uint64_t)((uint32_t)((uint64_t)*(uint32_t *)_pre_phi339 << 1UL) & (-2));
                                                } else {
                                                    var_142 = (rax_9 << 2UL) + r12_0;
                                                    var_143 = (uint64_t)(*(uint32_t *)(var_142 + (-4L)) >> 31U);
                                                    var_144 = helper_cc_compute_c_wrapper(rax_9 - rdx2_7, rdx2_7, var_7, 17U);
                                                    _pre_phi339 = var_142;
                                                    rcx4_2 = var_143;
                                                    rcx4_3 = var_143;
                                                    if (var_144 == 0UL) {
                                                        rcx4_3 = rcx4_2 | (uint64_t)((uint32_t)((uint64_t)*(uint32_t *)_pre_phi339 << 1UL) & (-2));
                                                    }
                                                }
                                                var_146 = helper_cc_compute_c_wrapper(rax_9 - rbx_0, rbx_0, var_7, 17U);
                                                rcx4_4 = rcx4_3;
                                                if (var_146 == 0UL) {
                                                    if ((uint64_t)(uint32_t)rcx4_3 == 0UL) {
                                                        loop_state_var = 2U;
                                                        break;
                                                    }
                                                }
                                                var_147 = (uint64_t)*(uint32_t *)((rax_9 << 2UL) + rbp_1);
                                                var_148 = helper_cc_compute_c_wrapper(var_147 - rcx4_3, rcx4_3, var_7, 16U);
                                                rsi5_4 = var_147;
                                                if (var_148 == 0UL) {
                                                    loop_state_var = 2U;
                                                    break;
                                                }
                                                r14_9 = r14_5;
                                                if (rsi5_4 <= (uint64_t)(uint32_t)rcx4_4) {
                                                    loop_state_var = 0U;
                                                    break;
                                                }
                                                if (rax_9 == 0UL) {
                                                    rax_9 = rax_9 + (-1L);
                                                    continue;
                                                }
                                                if (r14_5 != 0UL) {
                                                    loop_state_var = 0U;
                                                    break;
                                                }
                                                var_152 = (uint64_t)*(uint32_t *)r13_1;
                                                rax_10 = var_152;
                                                r14_9 = r14_5;
                                                if ((var_152 & 1UL) != 0UL) {
                                                    loop_state_var = 1U;
                                                    break;
                                                }
                                                loop_state_var = 0U;
                                                break;
                                            }
                                        switch (loop_state_var) {
                                          case 0U:
                                            {
                                                local_sp_6 = local_sp_5;
                                                r12_5 = r12_4;
                                                if (r96_5 == 0UL) {
                                                    var_159 = local_sp_5 + (-8L);
                                                    *(uint64_t *)var_159 = 4226556UL;
                                                    indirect_placeholder();
                                                    local_sp_6 = var_159;
                                                }
                                                **(uint64_t **)(local_sp_6 + 24UL) = r13_5;
                                                **(uint64_t **)(local_sp_6 + 16UL) = r14_9;
                                                return r12_5;
                                            }
                                            break;
                                          case 2U:
                                            {
                                                r13_3 = r13_2;
                                                r14_7 = r14_6;
                                                r96_3 = r96_2;
                                                r13_4 = r13_2;
                                                r12_3 = r12_1;
                                                r12_2 = r12_1;
                                                local_sp_3 = local_sp_2;
                                                r96_4 = r96_2;
                                                rax_12 = r13_2;
                                                local_sp_4 = local_sp_2;
                                                if (r14_6 == 0UL) {
                                                    *(uint32_t *)rax_12 = 1U;
                                                    r12_4 = r12_3;
                                                    r13_5 = r13_4;
                                                    r14_9 = r14_8;
                                                    r96_5 = r96_4;
                                                    local_sp_5 = local_sp_4;
                                                    local_sp_6 = local_sp_5;
                                                    r12_5 = r12_4;
                                                    if (r96_5 == 0UL) {
                                                        var_159 = local_sp_5 + (-8L);
                                                        *(uint64_t *)var_159 = 4226556UL;
                                                        indirect_placeholder();
                                                        local_sp_6 = var_159;
                                                    }
                                                    **(uint64_t **)(local_sp_6 + 24UL) = r13_5;
                                                    **(uint64_t **)(local_sp_6 + 16UL) = r14_9;
                                                    return r12_5;
                                                }
                                                var_149 = (uint64_t)*(uint32_t *)r13_2;
                                                rax_10 = var_149;
                                                r13_4 = r13_3;
                                                rax_11 = rax_10;
                                                r12_3 = r12_2;
                                                r12_4 = r12_2;
                                                r96_4 = r96_3;
                                                local_sp_4 = local_sp_3;
                                                r13_5 = r13_3;
                                                r14_9 = r14_7;
                                                r96_5 = r96_3;
                                                local_sp_5 = local_sp_3;
                                                while (1U)
                                                    {
                                                        var_153 = (uint32_t)rax_11 + 1U;
                                                        var_154 = (uint64_t)var_153;
                                                        *(uint32_t *)((rdx2_8 << 2UL) + r13_3) = var_153;
                                                        if (var_154 != 0UL) {
                                                            loop_state_var = 0U;
                                                            break;
                                                        }
                                                        var_155 = rdx2_8 + 1UL;
                                                        var_156 = helper_cc_compute_c_wrapper(var_155 - r14_7, r14_7, var_7, 17U);
                                                        rdx2_8 = var_155;
                                                        if (var_156 != 0UL) {
                                                            var_157 = (r14_7 << 2UL) + r13_3;
                                                            var_158 = r14_7 + 1UL;
                                                            r14_8 = var_158;
                                                            rax_12 = var_157;
                                                            loop_state_var = 1U;
                                                            break;
                                                        }
                                                        rax_11 = (uint64_t)*(uint32_t *)((var_155 << 2UL) + r13_3);
                                                        continue;
                                                    }
                                                *(uint32_t *)rax_12 = 1U;
                                                r12_4 = r12_3;
                                                r13_5 = r13_4;
                                                r14_9 = r14_8;
                                                r96_5 = r96_4;
                                                local_sp_5 = local_sp_4;
                                            }
                                            break;
                                          case 1U:
                                            {
                                                r13_4 = r13_3;
                                                rax_11 = rax_10;
                                                r12_3 = r12_2;
                                                r12_4 = r12_2;
                                                r96_4 = r96_3;
                                                local_sp_4 = local_sp_3;
                                                r13_5 = r13_3;
                                                r14_9 = r14_7;
                                                r96_5 = r96_3;
                                                local_sp_5 = local_sp_3;
                                                while (1U)
                                                    {
                                                        var_153 = (uint32_t)rax_11 + 1U;
                                                        var_154 = (uint64_t)var_153;
                                                        *(uint32_t *)((rdx2_8 << 2UL) + r13_3) = var_153;
                                                        if (var_154 != 0UL) {
                                                            loop_state_var = 0U;
                                                            break;
                                                        }
                                                        var_155 = rdx2_8 + 1UL;
                                                        var_156 = helper_cc_compute_c_wrapper(var_155 - r14_7, r14_7, var_7, 17U);
                                                        rdx2_8 = var_155;
                                                        if (var_156 != 0UL) {
                                                            var_157 = (r14_7 << 2UL) + r13_3;
                                                            var_158 = r14_7 + 1UL;
                                                            r14_8 = var_158;
                                                            rax_12 = var_157;
                                                            loop_state_var = 1U;
                                                            break;
                                                        }
                                                        rax_11 = (uint64_t)*(uint32_t *)((var_155 << 2UL) + r13_3);
                                                        continue;
                                                    }
                                                *(uint32_t *)rax_12 = 1U;
                                                r12_4 = r12_3;
                                                r13_5 = r13_4;
                                                r14_9 = r14_8;
                                                r96_5 = r96_4;
                                                local_sp_5 = local_sp_4;
                                            }
                                            break;
                                        }
                                    }
                                    break;
                                  case 1U:
                                    {
                                        var_131 = helper_cc_compute_c_wrapper(rbx_0 - rdx2_4, rdx2_4, var_7, 17U);
                                        rdx2_7 = rdx2_4;
                                        if (var_131 == 0UL) {
                                            r13_3 = r13_2;
                                            r14_7 = r14_6;
                                            r96_3 = r96_2;
                                            r13_4 = r13_2;
                                            r12_3 = r12_1;
                                            r12_2 = r12_1;
                                            local_sp_3 = local_sp_2;
                                            r96_4 = r96_2;
                                            rax_12 = r13_2;
                                            local_sp_4 = local_sp_2;
                                            if (r14_6 == 0UL) {
                                                *(uint32_t *)rax_12 = 1U;
                                                r12_4 = r12_3;
                                                r13_5 = r13_4;
                                                r14_9 = r14_8;
                                                r96_5 = r96_4;
                                                local_sp_5 = local_sp_4;
                                                local_sp_6 = local_sp_5;
                                                r12_5 = r12_4;
                                                if (r96_5 == 0UL) {
                                                    var_159 = local_sp_5 + (-8L);
                                                    *(uint64_t *)var_159 = 4226556UL;
                                                    indirect_placeholder();
                                                    local_sp_6 = var_159;
                                                }
                                                **(uint64_t **)(local_sp_6 + 24UL) = r13_5;
                                                **(uint64_t **)(local_sp_6 + 16UL) = r14_9;
                                                return r12_5;
                                            }
                                            var_149 = (uint64_t)*(uint32_t *)r13_2;
                                            rax_10 = var_149;
                                            r13_4 = r13_3;
                                            rax_11 = rax_10;
                                            r12_3 = r12_2;
                                            r12_4 = r12_2;
                                            r96_4 = r96_3;
                                            local_sp_4 = local_sp_3;
                                            r13_5 = r13_3;
                                            r14_9 = r14_7;
                                            r96_5 = r96_3;
                                            local_sp_5 = local_sp_3;
                                            while (1U)
                                                {
                                                    var_153 = (uint32_t)rax_11 + 1U;
                                                    var_154 = (uint64_t)var_153;
                                                    *(uint32_t *)((rdx2_8 << 2UL) + r13_3) = var_153;
                                                    if (var_154 != 0UL) {
                                                        loop_state_var = 0U;
                                                        break;
                                                    }
                                                    var_155 = rdx2_8 + 1UL;
                                                    var_156 = helper_cc_compute_c_wrapper(var_155 - r14_7, r14_7, var_7, 17U);
                                                    rdx2_8 = var_155;
                                                    if (var_156 != 0UL) {
                                                        var_157 = (r14_7 << 2UL) + r13_3;
                                                        var_158 = r14_7 + 1UL;
                                                        r14_8 = var_158;
                                                        rax_12 = var_157;
                                                        loop_state_var = 1U;
                                                        break;
                                                    }
                                                    rax_11 = (uint64_t)*(uint32_t *)((var_155 << 2UL) + r13_3);
                                                    continue;
                                                }
                                            switch (loop_state_var) {
                                              case 1U:
                                                {
                                                    *(uint32_t *)rax_12 = 1U;
                                                    r12_4 = r12_3;
                                                    r13_5 = r13_4;
                                                    r14_9 = r14_8;
                                                    r96_5 = r96_4;
                                                    local_sp_5 = local_sp_4;
                                                }
                                                break;
                                              case 0U:
                                                {
                                                    local_sp_6 = local_sp_5;
                                                    r12_5 = r12_4;
                                                    if (r96_5 == 0UL) {
                                                        var_159 = local_sp_5 + (-8L);
                                                        *(uint64_t *)var_159 = 4226556UL;
                                                        indirect_placeholder();
                                                        local_sp_6 = var_159;
                                                    }
                                                    **(uint64_t **)(local_sp_6 + 24UL) = r13_5;
                                                    **(uint64_t **)(local_sp_6 + 16UL) = r14_9;
                                                    return r12_5;
                                                }
                                                break;
                                            }
                                        }
                                    }
                                    break;
                                }
                            } else {
                                var_131 = helper_cc_compute_c_wrapper(rbx_0 - rdx2_4, rdx2_4, var_7, 17U);
                                rdx2_7 = rdx2_4;
                                if (var_131 == 0UL) {
                                    r13_3 = r13_2;
                                    r14_7 = r14_6;
                                    r96_3 = r96_2;
                                    r13_4 = r13_2;
                                    r12_3 = r12_1;
                                    r12_2 = r12_1;
                                    local_sp_3 = local_sp_2;
                                    r96_4 = r96_2;
                                    rax_12 = r13_2;
                                    local_sp_4 = local_sp_2;
                                    if (r14_6 == 0UL) {
                                        *(uint32_t *)rax_12 = 1U;
                                        r12_4 = r12_3;
                                        r13_5 = r13_4;
                                        r14_9 = r14_8;
                                        r96_5 = r96_4;
                                        local_sp_5 = local_sp_4;
                                        local_sp_6 = local_sp_5;
                                        r12_5 = r12_4;
                                        if (r96_5 == 0UL) {
                                            var_159 = local_sp_5 + (-8L);
                                            *(uint64_t *)var_159 = 4226556UL;
                                            indirect_placeholder();
                                            local_sp_6 = var_159;
                                        }
                                        **(uint64_t **)(local_sp_6 + 24UL) = r13_5;
                                        **(uint64_t **)(local_sp_6 + 16UL) = r14_9;
                                        return r12_5;
                                    }
                                    var_149 = (uint64_t)*(uint32_t *)r13_2;
                                    rax_10 = var_149;
                                    r13_4 = r13_3;
                                    rax_11 = rax_10;
                                    r12_3 = r12_2;
                                    r12_4 = r12_2;
                                    r96_4 = r96_3;
                                    local_sp_4 = local_sp_3;
                                    r13_5 = r13_3;
                                    r14_9 = r14_7;
                                    r96_5 = r96_3;
                                    local_sp_5 = local_sp_3;
                                    while (1U)
                                        {
                                            var_153 = (uint32_t)rax_11 + 1U;
                                            var_154 = (uint64_t)var_153;
                                            *(uint32_t *)((rdx2_8 << 2UL) + r13_3) = var_153;
                                            if (var_154 != 0UL) {
                                                loop_state_var = 0U;
                                                break;
                                            }
                                            var_155 = rdx2_8 + 1UL;
                                            var_156 = helper_cc_compute_c_wrapper(var_155 - r14_7, r14_7, var_7, 17U);
                                            rdx2_8 = var_155;
                                            if (var_156 != 0UL) {
                                                var_157 = (r14_7 << 2UL) + r13_3;
                                                var_158 = r14_7 + 1UL;
                                                r14_8 = var_158;
                                                rax_12 = var_157;
                                                loop_state_var = 1U;
                                                break;
                                            }
                                            rax_11 = (uint64_t)*(uint32_t *)((var_155 << 2UL) + r13_3);
                                            continue;
                                        }
                                    switch (loop_state_var) {
                                      case 1U:
                                        {
                                            *(uint32_t *)rax_12 = 1U;
                                            r12_4 = r12_3;
                                            r13_5 = r13_4;
                                            r14_9 = r14_8;
                                            r96_5 = r96_4;
                                            local_sp_5 = local_sp_4;
                                        }
                                        break;
                                      case 0U:
                                        {
                                            local_sp_6 = local_sp_5;
                                            r12_5 = r12_4;
                                            if (r96_5 == 0UL) {
                                                var_159 = local_sp_5 + (-8L);
                                                *(uint64_t *)var_159 = 4226556UL;
                                                indirect_placeholder();
                                                local_sp_6 = var_159;
                                            }
                                            **(uint64_t **)(local_sp_6 + 24UL) = r13_5;
                                            **(uint64_t **)(local_sp_6 + 16UL) = r14_9;
                                            return r12_5;
                                        }
                                        break;
                                    }
                                } else {
                                    r13_3 = r13_1;
                                    r14_7 = r14_5;
                                    r96_3 = r96_1;
                                    r12_1 = r12_0;
                                    r13_2 = r13_1;
                                    r14_6 = r14_5;
                                    r96_2 = r96_1;
                                    local_sp_2 = local_sp_1;
                                    r12_2 = r12_0;
                                    r12_4 = r12_0;
                                    local_sp_3 = local_sp_1;
                                    r13_5 = r13_1;
                                    r96_5 = r96_1;
                                    local_sp_5 = local_sp_1;
                                    while (1U)
                                        {
                                            if (rax_9 > rdx2_7) {
                                                var_150 = helper_cc_compute_c_wrapper(rax_9 - rbx_0, rbx_0, var_7, 17U);
                                                if (var_150 != 0UL) {
                                                    var_151 = (uint64_t)*(uint32_t *)((rax_9 << 2UL) + rbp_1);
                                                    rsi5_4 = var_151;
                                                    r14_9 = r14_5;
                                                    if (rsi5_4 <= (uint64_t)(uint32_t)rcx4_4) {
                                                        loop_state_var = 0U;
                                                        break;
                                                    }
                                                }
                                            }
                                            if (rax_9 == 0UL) {
                                                var_145 = helper_cc_compute_c_wrapper(0UL - rdx2_7, rdx2_7, var_7, 17U);
                                                if (var_145 != 0UL) {
                                                    var_150 = helper_cc_compute_c_wrapper(rax_9 - rbx_0, rbx_0, var_7, 17U);
                                                    if (var_150 != 0UL) {
                                                        var_151 = (uint64_t)*(uint32_t *)((rax_9 << 2UL) + rbp_1);
                                                        rsi5_4 = var_151;
                                                        r14_9 = r14_5;
                                                        if (rsi5_4 <= (uint64_t)(uint32_t)rcx4_4) {
                                                            loop_state_var = 0U;
                                                            break;
                                                        }
                                                    }
                                                    if (rax_9 == 0UL) {
                                                        rax_9 = rax_9 + (-1L);
                                                        continue;
                                                    }
                                                    if (r14_5 != 0UL) {
                                                        loop_state_var = 0U;
                                                        break;
                                                    }
                                                    var_152 = (uint64_t)*(uint32_t *)r13_1;
                                                    rax_10 = var_152;
                                                    r14_9 = r14_5;
                                                    if ((var_152 & 1UL) != 0UL) {
                                                        loop_state_var = 1U;
                                                        break;
                                                    }
                                                    loop_state_var = 0U;
                                                    break;
                                                }
                                                _pre338 = 0UL + r12_0;
                                                _pre_phi339 = _pre338;
                                                rcx4_3 = rcx4_2 | (uint64_t)((uint32_t)((uint64_t)*(uint32_t *)_pre_phi339 << 1UL) & (-2));
                                            } else {
                                                var_142 = (rax_9 << 2UL) + r12_0;
                                                var_143 = (uint64_t)(*(uint32_t *)(var_142 + (-4L)) >> 31U);
                                                var_144 = helper_cc_compute_c_wrapper(rax_9 - rdx2_7, rdx2_7, var_7, 17U);
                                                _pre_phi339 = var_142;
                                                rcx4_2 = var_143;
                                                rcx4_3 = var_143;
                                                if (var_144 == 0UL) {
                                                    rcx4_3 = rcx4_2 | (uint64_t)((uint32_t)((uint64_t)*(uint32_t *)_pre_phi339 << 1UL) & (-2));
                                                }
                                            }
                                            var_146 = helper_cc_compute_c_wrapper(rax_9 - rbx_0, rbx_0, var_7, 17U);
                                            rcx4_4 = rcx4_3;
                                            if (var_146 == 0UL) {
                                                if ((uint64_t)(uint32_t)rcx4_3 == 0UL) {
                                                    loop_state_var = 2U;
                                                    break;
                                                }
                                            }
                                            var_147 = (uint64_t)*(uint32_t *)((rax_9 << 2UL) + rbp_1);
                                            var_148 = helper_cc_compute_c_wrapper(var_147 - rcx4_3, rcx4_3, var_7, 16U);
                                            rsi5_4 = var_147;
                                            if (var_148 == 0UL) {
                                                loop_state_var = 2U;
                                                break;
                                            }
                                            r14_9 = r14_5;
                                            if (rsi5_4 <= (uint64_t)(uint32_t)rcx4_4) {
                                                loop_state_var = 0U;
                                                break;
                                            }
                                            if (rax_9 == 0UL) {
                                                rax_9 = rax_9 + (-1L);
                                                continue;
                                            }
                                            if (r14_5 != 0UL) {
                                                loop_state_var = 0U;
                                                break;
                                            }
                                            var_152 = (uint64_t)*(uint32_t *)r13_1;
                                            rax_10 = var_152;
                                            r14_9 = r14_5;
                                            if ((var_152 & 1UL) != 0UL) {
                                                loop_state_var = 1U;
                                                break;
                                            }
                                            loop_state_var = 0U;
                                            break;
                                        }
                                    switch (loop_state_var) {
                                      case 0U:
                                        {
                                            local_sp_6 = local_sp_5;
                                            r12_5 = r12_4;
                                            if (r96_5 == 0UL) {
                                                var_159 = local_sp_5 + (-8L);
                                                *(uint64_t *)var_159 = 4226556UL;
                                                indirect_placeholder();
                                                local_sp_6 = var_159;
                                            }
                                            **(uint64_t **)(local_sp_6 + 24UL) = r13_5;
                                            **(uint64_t **)(local_sp_6 + 16UL) = r14_9;
                                            return r12_5;
                                        }
                                        break;
                                      case 2U:
                                        {
                                            r13_3 = r13_2;
                                            r14_7 = r14_6;
                                            r96_3 = r96_2;
                                            r13_4 = r13_2;
                                            r12_3 = r12_1;
                                            r12_2 = r12_1;
                                            local_sp_3 = local_sp_2;
                                            r96_4 = r96_2;
                                            rax_12 = r13_2;
                                            local_sp_4 = local_sp_2;
                                            if (r14_6 == 0UL) {
                                                *(uint32_t *)rax_12 = 1U;
                                                r12_4 = r12_3;
                                                r13_5 = r13_4;
                                                r14_9 = r14_8;
                                                r96_5 = r96_4;
                                                local_sp_5 = local_sp_4;
                                                local_sp_6 = local_sp_5;
                                                r12_5 = r12_4;
                                                if (r96_5 == 0UL) {
                                                    var_159 = local_sp_5 + (-8L);
                                                    *(uint64_t *)var_159 = 4226556UL;
                                                    indirect_placeholder();
                                                    local_sp_6 = var_159;
                                                }
                                                **(uint64_t **)(local_sp_6 + 24UL) = r13_5;
                                                **(uint64_t **)(local_sp_6 + 16UL) = r14_9;
                                                return r12_5;
                                            }
                                            var_149 = (uint64_t)*(uint32_t *)r13_2;
                                            rax_10 = var_149;
                                            r13_4 = r13_3;
                                            rax_11 = rax_10;
                                            r12_3 = r12_2;
                                            r12_4 = r12_2;
                                            r96_4 = r96_3;
                                            local_sp_4 = local_sp_3;
                                            r13_5 = r13_3;
                                            r14_9 = r14_7;
                                            r96_5 = r96_3;
                                            local_sp_5 = local_sp_3;
                                            while (1U)
                                                {
                                                    var_153 = (uint32_t)rax_11 + 1U;
                                                    var_154 = (uint64_t)var_153;
                                                    *(uint32_t *)((rdx2_8 << 2UL) + r13_3) = var_153;
                                                    if (var_154 != 0UL) {
                                                        loop_state_var = 0U;
                                                        break;
                                                    }
                                                    var_155 = rdx2_8 + 1UL;
                                                    var_156 = helper_cc_compute_c_wrapper(var_155 - r14_7, r14_7, var_7, 17U);
                                                    rdx2_8 = var_155;
                                                    if (var_156 != 0UL) {
                                                        var_157 = (r14_7 << 2UL) + r13_3;
                                                        var_158 = r14_7 + 1UL;
                                                        r14_8 = var_158;
                                                        rax_12 = var_157;
                                                        loop_state_var = 1U;
                                                        break;
                                                    }
                                                    rax_11 = (uint64_t)*(uint32_t *)((var_155 << 2UL) + r13_3);
                                                    continue;
                                                }
                                            *(uint32_t *)rax_12 = 1U;
                                            r12_4 = r12_3;
                                            r13_5 = r13_4;
                                            r14_9 = r14_8;
                                            r96_5 = r96_4;
                                            local_sp_5 = local_sp_4;
                                        }
                                        break;
                                      case 1U:
                                        {
                                            r13_4 = r13_3;
                                            rax_11 = rax_10;
                                            r12_3 = r12_2;
                                            r12_4 = r12_2;
                                            r96_4 = r96_3;
                                            local_sp_4 = local_sp_3;
                                            r13_5 = r13_3;
                                            r14_9 = r14_7;
                                            r96_5 = r96_3;
                                            local_sp_5 = local_sp_3;
                                            while (1U)
                                                {
                                                    var_153 = (uint32_t)rax_11 + 1U;
                                                    var_154 = (uint64_t)var_153;
                                                    *(uint32_t *)((rdx2_8 << 2UL) + r13_3) = var_153;
                                                    if (var_154 != 0UL) {
                                                        loop_state_var = 0U;
                                                        break;
                                                    }
                                                    var_155 = rdx2_8 + 1UL;
                                                    var_156 = helper_cc_compute_c_wrapper(var_155 - r14_7, r14_7, var_7, 17U);
                                                    rdx2_8 = var_155;
                                                    if (var_156 != 0UL) {
                                                        var_157 = (r14_7 << 2UL) + r13_3;
                                                        var_158 = r14_7 + 1UL;
                                                        r14_8 = var_158;
                                                        rax_12 = var_157;
                                                        loop_state_var = 1U;
                                                        break;
                                                    }
                                                    rax_11 = (uint64_t)*(uint32_t *)((var_155 << 2UL) + r13_3);
                                                    continue;
                                                }
                                            *(uint32_t *)rax_12 = 1U;
                                            r12_4 = r12_3;
                                            r13_5 = r13_4;
                                            r14_9 = r14_8;
                                            r96_5 = r96_4;
                                            local_sp_5 = local_sp_4;
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                        break;
                    }
                }
                break;
            }
        }
        break;
    }
}
