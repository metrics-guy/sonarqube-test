typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_10_ret_type;
struct indirect_placeholder_11_ret_type;
struct indirect_placeholder_10_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_11_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t init_r15(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t init_r9(void);
extern struct indirect_placeholder_10_ret_type indirect_placeholder_10(uint64_t param_0);
extern struct indirect_placeholder_11_ret_type indirect_placeholder_11(uint64_t param_0);
uint64_t bb_multiply_isra_0(uint64_t r8, uint64_t rdx, uint64_t rdi, uint64_t rcx, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    bool var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t *var_17;
    bool var_18;
    uint64_t rdx2_0;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t r81_0_be;
    uint64_t r81_0;
    uint64_t rdi3_0;
    uint64_t var_21;
    uint64_t r12_1;
    uint64_t rdx2_1;
    uint64_t rsi5_0;
    uint64_t var_22;
    uint64_t var_23;
    uint32_t *var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t r12_0;
    uint64_t var_30;
    uint64_t rax_0;
    struct indirect_placeholder_10_ret_type var_31;
    uint64_t var_32;
    uint64_t var_13;
    uint64_t var_14;
    struct indirect_placeholder_11_ret_type var_15;
    uint64_t var_16;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r12();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_r13();
    var_5 = init_r14();
    var_6 = init_rbp();
    var_7 = init_r9();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_1;
    *(uint64_t *)(var_0 + (-40L)) = var_6;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_8 = (rdi > rdx);
    var_9 = var_8 ? rdx : rdi;
    var_10 = var_8 ? rcx : rsi;
    var_11 = var_8 ? rdi : rdx;
    var_12 = var_8 ? rsi : rcx;
    rdx2_0 = var_11;
    r81_0 = 0UL;
    r12_1 = 0UL;
    rdx2_1 = 0UL;
    rsi5_0 = 0UL;
    rax_0 = 0UL;
    if (var_9 != 0UL) {
        var_13 = var_11 + var_9;
        *(uint64_t *)(var_0 + (-64L)) = var_7;
        var_14 = var_13 << 2UL;
        *(uint64_t *)(var_0 + (-80L)) = 4225658UL;
        var_15 = indirect_placeholder_11(var_14);
        var_16 = var_15.field_0;
        rdi3_0 = var_16;
        r12_0 = var_13;
        if (var_16 != 0UL) {
            var_17 = *(uint64_t **)(var_0 + (-72L));
            var_18 = (var_11 == 0UL);
            if (var_18) {
                var_19 = rdx2_0 + (-1L);
                *(uint32_t *)((var_19 << 2UL) + var_16) = 0U;
                rdx2_0 = var_19;
                do {
                    var_19 = rdx2_0 + (-1L);
                    *(uint32_t *)((var_19 << 2UL) + var_16) = 0U;
                    rdx2_0 = var_19;
                } while (var_19 != 0UL);
            }
            var_20 = var_11 << 2UL;
            while (1U)
                {
                    var_21 = (uint64_t)*(uint32_t *)((r81_0 << 2UL) + var_10);
                    if (var_18) {
                        var_29 = r81_0 + 1UL;
                        *(uint32_t *)(var_20 + rdi3_0) = 0U;
                        r81_0_be = var_29;
                        if (var_9 == var_29) {
                            break;
                        }
                    }
                    var_22 = rsi5_0 << 2UL;
                    var_23 = (uint64_t)*(uint32_t *)(var_22 + var_12);
                    var_24 = (uint32_t *)(var_22 + rdi3_0);
                    var_25 = rdx2_1 + ((var_21 * var_23) + (uint64_t)*var_24);
                    *var_24 = (uint32_t)var_25;
                    var_26 = rsi5_0 + 1UL;
                    var_27 = var_25 >> 32UL;
                    rdx2_1 = var_27;
                    rsi5_0 = var_26;
                    do {
                        var_22 = rsi5_0 << 2UL;
                        var_23 = (uint64_t)*(uint32_t *)(var_22 + var_12);
                        var_24 = (uint32_t *)(var_22 + rdi3_0);
                        var_25 = rdx2_1 + ((var_21 * var_23) + (uint64_t)*var_24);
                        *var_24 = (uint32_t)var_25;
                        var_26 = rsi5_0 + 1UL;
                        var_27 = var_25 >> 32UL;
                        rdx2_1 = var_27;
                        rsi5_0 = var_26;
                    } while (var_11 != var_26);
                    var_28 = r81_0 + 1UL;
                    *(uint32_t *)(var_20 + rdi3_0) = (uint32_t)var_27;
                    r81_0_be = var_28;
                    if (var_9 == var_28) {
                        break;
                    }
                    r81_0 = r81_0_be;
                    rdi3_0 = rdi3_0 + 4UL;
                    continue;
                }
            if (var_13 != 0UL) {
                r12_1 = r12_0;
                while (*(uint32_t *)(((r12_0 << 2UL) + var_16) + (-4L)) != 0U)
                    {
                        var_30 = r12_0 + (-1L);
                        r12_0 = var_30;
                        r12_1 = 0UL;
                        if (var_30 == 0UL) {
                            break;
                        }
                        r12_1 = r12_0;
                    }
            }
            *(uint64_t *)r8 = r12_1;
            *var_17 = var_16;
            return var_16;
        }
    }
    *(uint64_t *)r8 = 0UL;
    *(uint64_t *)(var_0 + (-64L)) = var_7;
    *(uint64_t *)(var_0 + (-80L)) = 4225608UL;
    var_31 = indirect_placeholder_10(1UL);
    var_32 = var_31.field_0;
    **(uint64_t **)(var_0 + (-72L)) = var_32;
    rax_0 = var_32;
}
