typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_pxor_xmm_wrapper_134_ret_type;
struct type_2;
struct type_4;
struct helper_mulss_wrapper_ret_type;
struct helper_cvtsq2ss_wrapper_ret_type;
struct helper_addss_wrapper_ret_type;
struct helper_comiss_wrapper_ret_type;
struct helper_cvttss2sq_wrapper_ret_type;
struct helper_subss_wrapper_ret_type;
struct indirect_placeholder_27_ret_type;
struct helper_pxor_xmm_wrapper_134_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_2 {
};
struct type_4 {
};
struct helper_mulss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvtsq2ss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_addss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_comiss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvttss2sq_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_subss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct indirect_placeholder_27_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern struct helper_pxor_xmm_wrapper_134_ret_type helper_pxor_xmm_wrapper_134(struct type_2 *param_0, struct type_4 *param_1, struct type_4 *param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_state_0x8558(void);
extern unsigned char init_state_0x8549(void);
extern unsigned char init_state_0x854c(void);
extern unsigned char init_state_0x8548(void);
extern unsigned char init_state_0x854b(void);
extern unsigned char init_state_0x8547(void);
extern unsigned char init_state_0x854d(void);
extern uint64_t init_state_0x8560(void);
extern uint64_t indirect_placeholder_6(uint64_t param_0, uint64_t param_1);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_state_0x8d58(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct helper_mulss_wrapper_ret_type helper_mulss_wrapper(struct type_2 *param_0, struct type_4 *param_1, struct type_4 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct helper_cvtsq2ss_wrapper_ret_type helper_cvtsq2ss_wrapper(struct type_2 *param_0, struct type_4 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_addss_wrapper_ret_type helper_addss_wrapper(struct type_2 *param_0, struct type_4 *param_1, struct type_4 *param_2, uint64_t param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9);
extern struct helper_comiss_wrapper_ret_type helper_comiss_wrapper(struct type_2 *param_0, struct type_4 *param_1, struct type_4 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_cvttss2sq_wrapper_ret_type helper_cvttss2sq_wrapper(struct type_2 *param_0, struct type_4 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4);
extern struct helper_subss_wrapper_ret_type helper_subss_wrapper(struct type_2 *param_0, struct type_4 *param_1, struct type_4 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct indirect_placeholder_27_ret_type indirect_placeholder_27(uint64_t param_0);
uint64_t bb_convert_to_decimal(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t r12_0;
    uint64_t rsi3_2;
    uint64_t rsi3_0;
    uint64_t rbp_0_ph;
    uint64_t rdi2_0_ph_in;
    uint32_t *var_38;
    uint64_t rdi2_0_in;
    uint64_t rdi2_0;
    uint64_t rax_1;
    uint32_t *var_39;
    uint32_t var_40;
    unsigned __int128 var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t rsi3_1;
    uint64_t rax_2;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t rsi3_4;
    uint64_t rsi3_3;
    uint64_t var_48;
    uint64_t rsi3_6;
    uint64_t cc_dst_0;
    uint64_t var_17;
    struct helper_cvtsq2ss_wrapper_ret_type var_18;
    struct helper_addss_wrapper_ret_type var_19;
    uint64_t state_0x8558_0;
    unsigned char storemerge;
    uint64_t var_20;
    uint64_t var_21;
    struct helper_cvtsq2ss_wrapper_ret_type var_16;
    struct helper_cvttss2sq_wrapper_ret_type var_28;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    unsigned char var_9;
    unsigned char var_10;
    unsigned char var_11;
    unsigned char var_12;
    unsigned char var_13;
    unsigned char var_14;
    bool var_15;
    uint64_t var_36;
    uint64_t rax_0;
    uint64_t var_37;
    struct helper_mulss_wrapper_ret_type var_22;
    uint64_t var_23;
    struct helper_comiss_wrapper_ret_type var_24;
    uint64_t var_25;
    unsigned char var_26;
    uint64_t var_27;
    uint64_t rax_3;
    struct helper_subss_wrapper_ret_type var_29;
    struct helper_cvttss2sq_wrapper_ret_type var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    struct indirect_placeholder_27_ret_type var_34;
    uint64_t var_35;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r12();
    var_2 = init_rbx();
    var_3 = init_r13();
    var_4 = init_rbp();
    var_5 = init_cc_src2();
    var_6 = init_state_0x8558();
    var_7 = init_state_0x8560();
    var_8 = init_state_0x8d58();
    var_9 = init_state_0x8549();
    var_10 = init_state_0x854c();
    var_11 = init_state_0x8548();
    var_12 = init_state_0x854b();
    var_13 = init_state_0x8547();
    var_14 = init_state_0x854d();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_2;
    var_15 = ((long)rdi < (long)0UL);
    helper_pxor_xmm_wrapper_134((struct type_2 *)(0UL), (struct type_4 *)(776UL), (struct type_4 *)(776UL), var_6, var_7);
    rbp_0_ph = rdi;
    rax_1 = 0UL;
    cc_dst_0 = rdi;
    if (var_15) {
        var_17 = (rdi >> 1UL) | (rdi & 1UL);
        var_18 = helper_cvtsq2ss_wrapper((struct type_2 *)(0UL), (struct type_4 *)(776UL), var_17, var_9, var_11, var_12, var_13);
        var_19 = helper_addss_wrapper((struct type_2 *)(0UL), (struct type_4 *)(776UL), (struct type_4 *)(776UL), var_18.field_0, var_18.field_1, var_10, var_11, var_12, var_13, var_14);
        cc_dst_0 = var_17;
        state_0x8558_0 = var_19.field_0;
        storemerge = var_19.field_1;
    } else {
        var_16 = helper_cvtsq2ss_wrapper((struct type_2 *)(0UL), (struct type_4 *)(776UL), rdi, var_9, var_11, var_12, var_13);
        state_0x8558_0 = var_16.field_0;
        storemerge = var_16.field_1;
    }
    var_20 = (uint64_t)*(uint32_t *)4266180UL;
    var_21 = var_8 & (-4294967296L);
    var_22 = helper_mulss_wrapper((struct type_2 *)(0UL), (struct type_4 *)(776UL), (struct type_4 *)(2824UL), state_0x8558_0, var_21 | var_20, storemerge, var_10, var_11, var_12, var_13, var_14);
    var_23 = var_22.field_0;
    var_24 = helper_comiss_wrapper((struct type_2 *)(0UL), (struct type_4 *)(776UL), (struct type_4 *)(2824UL), var_23, var_21 | (uint64_t)*(uint32_t *)4266184UL, var_22.field_1, var_10);
    var_25 = var_24.field_0;
    var_26 = var_24.field_1;
    var_27 = helper_cc_compute_c_wrapper(cc_dst_0, var_25, var_5, 1U);
    if (var_27 == 0UL) {
        var_29 = helper_subss_wrapper((struct type_2 *)(0UL), (struct type_4 *)(776UL), (struct type_4 *)(2824UL), var_23, var_21 | (uint64_t)*(uint32_t *)4266184UL, var_26, var_10, var_11, var_12, var_13, var_14);
        var_30 = helper_cvttss2sq_wrapper((struct type_2 *)(0UL), (struct type_4 *)(776UL), var_29.field_0, var_29.field_1, var_10);
        var_31 = var_30.field_0 ^ (-9223372036854775808L);
        helper_cc_compute_all_wrapper(cc_dst_0, var_25, var_5, 1U);
        rax_3 = var_31;
    } else {
        var_28 = helper_cvttss2sq_wrapper((struct type_2 *)(0UL), (struct type_4 *)(776UL), var_23, var_26, var_10);
        rax_3 = var_28.field_0;
    }
    var_32 = (rax_3 * 9UL) + 9UL;
    *(uint64_t *)(var_0 + (-48L)) = 4223199UL;
    var_33 = indirect_placeholder_6(var_32, rdx);
    *(uint64_t *)(var_0 + (-56L)) = 4223207UL;
    var_34 = indirect_placeholder_27(var_33);
    var_35 = var_34.field_0;
    rax_0 = var_35;
    rsi3_0 = var_35;
    if (var_35 == 0UL) {
        return var_35;
    }
    if (rdx == 0UL) {
        if (rdi != 0UL) {
            *(unsigned char *)var_35 = (unsigned char)'0';
            rsi3_6 = var_35 + 1UL;
            *(unsigned char *)rsi3_6 = (unsigned char)'\x00';
            return var_35;
        }
    }
    var_36 = var_35 + rdx;
    rsi3_0 = var_36;
    rsi3_2 = var_36;
    var_37 = rax_0 + 1UL;
    *(unsigned char *)rax_0 = (unsigned char)'0';
    rax_0 = var_37;
    do {
        var_37 = rax_0 + 1UL;
        *(unsigned char *)rax_0 = (unsigned char)'0';
        rax_0 = var_37;
    } while (var_37 != var_36);
    if (rdi != 0UL) {
        rsi3_3 = rsi3_2;
        rsi3_4 = rsi3_2;
        if (rsi3_2 > var_35) {
            rsi3_6 = rsi3_4;
            if (var_35 == rsi3_4) {
                *(unsigned char *)var_35 = (unsigned char)'0';
                rsi3_6 = var_35 + 1UL;
            }
            *(unsigned char *)rsi3_6 = (unsigned char)'\x00';
            return var_35;
        }
        while (1U)
            {
                var_48 = rsi3_3 + (-1L);
                rsi3_3 = var_48;
                rsi3_4 = rsi3_3;
                if (*(unsigned char *)var_48 != '0') {
                    loop_state_var = 0U;
                    break;
                }
                if (var_35 == var_48) {
                    continue;
                }
                loop_state_var = 1U;
                break;
            }
        switch (loop_state_var) {
          case 0U:
            {
                break;
            }
            break;
          case 1U:
            {
                *(unsigned char *)var_35 = (unsigned char)'0';
                rsi3_6 = var_35 + 1UL;
                *(unsigned char *)rsi3_6 = (unsigned char)'\x00';
                return var_35;
            }
            break;
        }
    }
    rdi2_0_ph_in = rsi3_0;
    var_47 = rbp_0_ph + (-1L);
    rbp_0_ph = var_47;
    do {
        var_38 = (uint32_t *)(((rbp_0_ph << 2UL) + rsi) + (-4L));
        rdi2_0_in = rdi2_0_ph_in;
        r12_0 = rbp_0_ph;
        do {
            rdi2_0 = rdi2_0_in + 9UL;
            rdi2_0_ph_in = rdi2_0;
            rdi2_0_in = rdi2_0;
            rsi3_1 = rdi2_0_in;
            rsi3_2 = rdi2_0;
            var_39 = (uint32_t *)(((r12_0 << 2UL) + rsi) + (-4L));
            var_40 = *var_39;
            var_41 = ((unsigned __int128)(((rax_1 << 32UL) | (uint64_t)var_40) >> 9UL) * 19342813113834067ULL) >> 75ULL;
            var_42 = (uint64_t)var_41;
            *var_39 = (uint32_t)var_41;
            var_43 = (uint64_t)(((uint32_t)var_42 * 3294967296U) + var_40);
            var_44 = r12_0 + (-1L);
            r12_0 = var_44;
            rax_1 = var_43;
            rax_2 = var_43;
            do {
                var_39 = (uint32_t *)(((r12_0 << 2UL) + rsi) + (-4L));
                var_40 = *var_39;
                var_41 = ((unsigned __int128)(((rax_1 << 32UL) | (uint64_t)var_40) >> 9UL) * 19342813113834067ULL) >> 75ULL;
                var_42 = (uint64_t)var_41;
                *var_39 = (uint32_t)var_41;
                var_43 = (uint64_t)(((uint32_t)var_42 * 3294967296U) + var_40);
                var_44 = r12_0 + (-1L);
                r12_0 = var_44;
                rax_1 = var_43;
                rax_2 = var_43;
            } while (var_44 != 0UL);
            var_45 = rsi3_1 + 1UL;
            var_46 = (rax_2 * 3435973837UL) >> 35UL;
            *(unsigned char *)rsi3_1 = (((unsigned char)rax_2 + ((unsigned char)var_46 * '\xf6')) + '0');
            rsi3_1 = var_45;
            rax_2 = var_46;
            do {
                var_45 = rsi3_1 + 1UL;
                var_46 = (rax_2 * 3435973837UL) >> 35UL;
                *(unsigned char *)rsi3_1 = (((unsigned char)rax_2 + ((unsigned char)var_46 * '\xf6')) + '0');
                rsi3_1 = var_45;
                rax_2 = var_46;
            } while (var_45 != rdi2_0);
        } while (*var_38 != 0U);
        var_47 = rbp_0_ph + (-1L);
        rbp_0_ph = var_47;
    } while (var_47 != 0UL);
    rsi3_3 = rsi3_2;
    rsi3_4 = rsi3_2;
    if (rsi3_2 <= var_35) {
        while (1U)
            {
                var_48 = rsi3_3 + (-1L);
                rsi3_3 = var_48;
                rsi3_4 = rsi3_3;
                if (*(unsigned char *)var_48 != '0') {
                    loop_state_var = 0U;
                    break;
                }
                if (var_35 == var_48) {
                    continue;
                }
                loop_state_var = 1U;
                break;
            }
        switch (loop_state_var) {
          case 0U:
            {
                break;
            }
            break;
          case 1U:
            {
                *(unsigned char *)var_35 = (unsigned char)'0';
                rsi3_6 = var_35 + 1UL;
                *(unsigned char *)rsi3_6 = (unsigned char)'\x00';
                return var_35;
            }
            break;
        }
    }
    rsi3_6 = rsi3_4;
    if (var_35 == rsi3_4) {
        *(unsigned char *)var_35 = (unsigned char)'0';
        rsi3_6 = var_35 + 1UL;
    }
    *(unsigned char *)rsi3_6 = (unsigned char)'\x00';
}
