typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_pxor_xmm_wrapper_134_ret_type;
struct type_3;
struct type_5;
struct indirect_placeholder_16_ret_type;
struct helper_cvtsi2ss_wrapper_ret_type;
struct helper_mulss_wrapper_ret_type;
struct helper_cvttss2si_wrapper_ret_type;
struct indirect_placeholder_18_ret_type;
struct helper_pxor_xmm_wrapper_134_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_3 {
};
struct type_5 {
};
struct indirect_placeholder_16_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_cvtsi2ss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_mulss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvttss2si_wrapper_ret_type {
    uint32_t field_0;
    unsigned char field_1;
};
struct indirect_placeholder_18_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern struct helper_pxor_xmm_wrapper_134_ret_type helper_pxor_xmm_wrapper_134(struct type_3 *param_0, struct type_5 *param_1, struct type_5 *param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_state_0x8558(void);
extern unsigned char init_state_0x8549(void);
extern unsigned char init_state_0x854c(void);
extern unsigned char init_state_0x8548(void);
extern unsigned char init_state_0x854b(void);
extern unsigned char init_state_0x8547(void);
extern unsigned char init_state_0x854d(void);
extern uint64_t init_state_0x8560(void);
extern uint64_t indirect_placeholder_17(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_r12(void);
extern uint64_t init_r15(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t init_state_0x8d58(void);
extern uint64_t indirect_placeholder_14(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern uint64_t indirect_placeholder_15(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct indirect_placeholder_16_ret_type indirect_placeholder_16(uint64_t param_0);
extern struct helper_cvtsi2ss_wrapper_ret_type helper_cvtsi2ss_wrapper(struct type_3 *param_0, struct type_5 *param_1, uint32_t param_2, unsigned char param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_mulss_wrapper_ret_type helper_mulss_wrapper(struct type_3 *param_0, struct type_5 *param_1, struct type_5 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_cvttss2si_wrapper_ret_type helper_cvttss2si_wrapper(struct type_3 *param_0, struct type_5 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4);
extern struct indirect_placeholder_18_ret_type indirect_placeholder_18(uint64_t param_0);
uint64_t bb_scale10_round_decimal_decoded(uint64_t r8, uint64_t rdx, uint64_t rdi, uint64_t rcx, uint64_t rsi) {
    uint64_t var_118;
    uint64_t var_113;
    uint64_t var_114;
    uint64_t var_59;
    uint64_t rdx2_3;
    uint64_t rcx4_0;
    uint64_t rax_5;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t r13_2;
    struct helper_cvtsi2ss_wrapper_ret_type var_36;
    uint64_t var_93;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    unsigned char var_10;
    unsigned char var_11;
    unsigned char var_12;
    unsigned char var_13;
    unsigned char var_14;
    unsigned char var_15;
    uint64_t var_16;
    uint64_t *_cast;
    uint64_t r12_0;
    uint32_t var_72;
    uint64_t var_73;
    uint64_t rdi3_0;
    uint64_t rax_0;
    uint64_t var_74;
    bool var_75;
    bool var_76;
    uint64_t rdx2_0;
    uint64_t rax_1;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t rsi5_0;
    uint64_t rax_2;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_122;
    uint64_t var_123;
    uint64_t var_124;
    uint64_t var_125;
    uint64_t rdx2_1;
    uint64_t r13_0;
    uint64_t local_sp_0;
    uint32_t var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t rax_3;
    uint64_t var_97;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t r13_3;
    uint64_t var_100;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t local_sp_1;
    uint64_t r81_0;
    uint32_t *var_55;
    uint64_t rbx_0;
    uint64_t r13_1;
    uint64_t rdx2_2;
    uint64_t rax_4;
    uint32_t var_56;
    uint64_t var_57;
    bool var_58;
    uint64_t rdi3_1;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t rbx_1;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    struct indirect_placeholder_16_ret_type var_70;
    uint64_t var_71;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t r13_4;
    uint64_t r13_5;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t rdx2_4;
    uint64_t rax_6;
    uint32_t *var_106;
    uint64_t var_107;
    uint64_t var_108;
    uint64_t var_109;
    uint64_t var_110;
    uint64_t r13_6;
    uint64_t var_111;
    uint64_t var_112;
    uint64_t rax_7;
    uint64_t rax_8;
    uint64_t var_115;
    uint64_t var_116;
    uint64_t var_117;
    uint64_t var_119;
    uint64_t var_120;
    uint64_t var_121;
    uint32_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint32_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint32_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t r15_0;
    uint32_t *var_29;
    uint32_t var_30;
    uint32_t var_31;
    uint32_t var_32;
    uint32_t var_33;
    uint64_t var_34;
    uint32_t var_35;
    struct helper_mulss_wrapper_ret_type var_37;
    uint64_t var_38;
    unsigned char var_39;
    uint32_t var_40;
    uint64_t var_41;
    uint32_t *var_42;
    uint32_t var_43;
    uint64_t var_44;
    struct helper_cvttss2si_wrapper_ret_type var_45;
    uint64_t var_46;
    uint64_t *var_47;
    struct indirect_placeholder_18_ret_type var_48;
    uint64_t var_49;
    uint32_t *var_50;
    uint32_t var_51;
    uint64_t var_52;
    uint32_t var_53;
    uint64_t var_54;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r12();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_r13();
    var_5 = init_r14();
    var_6 = init_rbp();
    var_7 = init_state_0x8558();
    var_8 = init_state_0x8560();
    var_9 = init_state_0x8d58();
    var_10 = init_state_0x8549();
    var_11 = init_state_0x854c();
    var_12 = init_state_0x8548();
    var_13 = init_state_0x854b();
    var_14 = init_state_0x8547();
    var_15 = init_state_0x854d();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_1;
    *(uint64_t *)(var_0 + (-40L)) = var_6;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_16 = var_0 + (-120L);
    _cast = (uint64_t *)var_16;
    *_cast = rcx;
    rax_5 = 0UL;
    r12_0 = 0UL;
    rax_0 = 0UL;
    rax_1 = 0UL;
    rsi5_0 = 0UL;
    rax_2 = 0UL;
    r13_3 = 1UL;
    r13_1 = 1UL;
    rdx2_2 = 1UL;
    rax_4 = 0UL;
    rdi3_1 = 1220703125UL;
    rax_6 = 0UL;
    if (rcx == 0UL) {
        return r12_0;
    }
    var_17 = (uint32_t)r8;
    var_18 = (uint64_t)var_17;
    var_19 = r8 + rdi;
    var_20 = (uint32_t)var_19;
    var_21 = (uint64_t)var_20;
    var_22 = helper_cc_compute_all_wrapper(var_21, 0UL, 0UL, 24U);
    r15_0 = var_18;
    r81_0 = var_21;
    if ((uint64_t)(((unsigned char)(var_22 >> 4UL) ^ (unsigned char)var_22) & '\xc0') == 0UL) {
        *(uint64_t *)(var_0 + (-96L)) = 0UL;
    } else {
        var_23 = helper_cc_compute_all_wrapper(var_18, 0UL, 0UL, 24U);
        if ((uint64_t)(((unsigned char)(var_23 >> 4UL) ^ (unsigned char)var_23) & '\xc0') == 0UL) {
            var_24 = ((long)(r8 << 32UL) > (long)(var_19 << 32UL)) ? var_21 : var_18;
            var_25 = (uint64_t)((long)(var_24 << 32UL) >> (long)32UL);
            var_26 = (uint32_t)var_24;
            var_27 = (uint64_t)(var_20 - var_26);
            var_28 = (uint64_t)(var_17 - var_26);
            *(uint64_t *)(var_0 + (-96L)) = var_25;
            r15_0 = var_28;
            r81_0 = var_27;
        } else {
            *(uint64_t *)(var_0 + (-96L)) = 0UL;
        }
    }
    helper_pxor_xmm_wrapper_134((struct type_3 *)(0UL), (struct type_5 *)(776UL), (struct type_5 *)(776UL), var_7, var_8);
    var_29 = (uint32_t *)(var_0 + (-100L));
    var_30 = (uint32_t)r81_0;
    *var_29 = var_30;
    var_31 = (uint32_t)(uint64_t)((long)(r15_0 << 32UL) >> (long)63UL);
    var_32 = (uint32_t)r15_0;
    var_33 = (var_31 ^ var_32) - var_31;
    var_34 = (uint64_t)var_33;
    var_35 = (uint32_t)(uint64_t)((long)(r81_0 << 32UL) >> (long)63UL);
    var_36 = helper_cvtsi2ss_wrapper((struct type_3 *)(0UL), (struct type_5 *)(776UL), var_33, var_10, var_12, var_13, var_14);
    var_37 = helper_mulss_wrapper((struct type_3 *)(0UL), (struct type_5 *)(776UL), (struct type_5 *)(2824UL), var_36.field_0, (var_9 & (-4294967296L)) | (uint64_t)*(uint32_t *)4266232UL, var_36.field_1, var_11, var_12, var_13, var_14, var_15);
    var_38 = var_37.field_0;
    var_39 = var_37.field_1;
    var_40 = (var_35 ^ var_30) - var_35;
    var_41 = (uint64_t)var_40;
    var_42 = (uint32_t *)(var_0 + (-112L));
    *var_42 = var_40;
    var_43 = (uint32_t)(var_41 >> 5UL) & 134217727U;
    var_44 = var_0 + (-104L);
    *(uint32_t *)var_44 = var_43;
    var_45 = helper_cvttss2si_wrapper((struct type_3 *)(0UL), (struct type_5 *)(776UL), var_38, var_39, var_11);
    var_46 = (((uint64_t)(var_43 + var_45.field_0) << 2UL) + 8UL) & 17179869180UL;
    var_47 = (uint64_t *)(var_0 + (-128L));
    *var_47 = 4227473UL;
    var_48 = indirect_placeholder_18(var_46);
    var_49 = var_48.field_0;
    var_50 = (uint32_t *)var_16;
    var_51 = *var_50;
    var_52 = (uint64_t)var_51;
    var_53 = *(uint32_t *)(var_0 + (-108L));
    var_54 = (uint64_t)var_53;
    rcx4_0 = var_49;
    rdx2_4 = var_49;
    if (var_49 == 0UL) {
        *(uint64_t *)(var_0 + (-136L)) = 4228380UL;
        indirect_placeholder();
    } else {
        var_55 = (uint32_t *)var_49;
        *var_55 = 1U;
        if (r15_0 == 0UL) {
            var_90 = var_52 & 31UL;
            rbx_0 = var_90;
            rbx_1 = rbx_0;
            r13_4 = r13_3;
            if ((int)var_53 <= (int)4294967295U) {
                var_91 = var_0 + (-80L);
                var_92 = var_0 + (-136L);
                *(uint64_t *)var_92 = 4227695UL;
                var_93 = indirect_placeholder_17(var_91, r13_3, rsi, var_49, rdx);
                local_sp_1 = var_92;
                if (var_93 == 0UL) {
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4228085UL;
                    indirect_placeholder();
                    *(uint64_t *)(local_sp_1 + (-16L)) = 4228094UL;
                    indirect_placeholder();
                    return r12_0;
                }
                var_94 = *var_50;
                var_95 = (r13_3 << 2UL) + var_49;
                rax_3 = var_95;
                rdx2_1 = var_95;
                if (var_94 != 0U) {
                    var_96 = ((uint64_t)var_94 << 2UL) + var_95;
                    rdx2_1 = var_96;
                    *(uint32_t *)rax_3 = 0U;
                    var_97 = rax_3 + 4UL;
                    rax_3 = var_97;
                    do {
                        *(uint32_t *)rax_3 = 0U;
                        var_97 = rax_3 + 4UL;
                        rax_3 = var_97;
                    } while (var_96 != var_97);
                }
                var_98 = *(uint64_t *)(var_0 + (-88L));
                var_99 = *(uint64_t *)var_91;
                var_100 = var_0 + (-96L);
                *(uint32_t *)rdx2_1 = (uint32_t)(1UL << rbx_0);
                var_101 = (uint64_t)(*var_50 + 1U);
                *(uint64_t *)(var_0 + (-144L)) = 4227797UL;
                var_102 = indirect_placeholder_14(var_44, var_101, var_98, var_95, var_99, var_100);
                var_103 = var_0 + (-152L);
                *(uint64_t *)var_103 = 4227808UL;
                indirect_placeholder();
                r13_0 = var_102;
                local_sp_0 = var_103;
                *(uint64_t *)(local_sp_0 + (-8L)) = 4227816UL;
                indirect_placeholder();
                *(uint64_t *)(local_sp_0 + (-16L)) = 4227825UL;
                indirect_placeholder();
                if (r13_0 == 0UL) {
                    var_122 = *(uint64_t *)(local_sp_0 + 16UL);
                    var_123 = *(uint64_t *)(local_sp_0 + 8UL);
                    var_124 = *(uint64_t *)(local_sp_0 + 24UL);
                    *(uint64_t *)(local_sp_0 + (-24L)) = 4227854UL;
                    var_125 = indirect_placeholder_15(var_123, var_122, var_124);
                    *(uint64_t *)(local_sp_0 + (-32L)) = 4227865UL;
                    indirect_placeholder();
                    r12_0 = var_125;
                }
                return r12_0;
            }
        }
        while (1U)
            {
                var_56 = (uint32_t)rax_4;
                var_57 = (uint64_t)(var_56 + 13U);
                var_58 = (var_57 > var_34);
                rax_4 = var_57;
                rdx2_3 = rdx2_2;
                r13_2 = r13_1;
                if (var_58) {
                    rdi3_1 = (uint64_t)*(uint32_t *)((((uint64_t)(var_33 - var_56) << 2UL) & 17179869180UL) + 4266112UL);
                }
                var_59 = var_49 + (r13_1 << 2UL);
                var_60 = rdi3_1 * rdx2_3;
                var_61 = rcx4_0 + 4UL;
                var_62 = rax_5 + var_60;
                *(uint32_t *)rcx4_0 = (uint32_t)var_62;
                var_63 = var_62 >> 32UL;
                rcx4_0 = var_61;
                rax_5 = var_63;
                while (var_59 != var_61)
                    {
                        rdx2_3 = (uint64_t)*(uint32_t *)var_61;
                        var_60 = rdi3_1 * rdx2_3;
                        var_61 = rcx4_0 + 4UL;
                        var_62 = rax_5 + var_60;
                        *(uint32_t *)rcx4_0 = (uint32_t)var_62;
                        var_63 = var_62 >> 32UL;
                        rcx4_0 = var_61;
                        rax_5 = var_63;
                    }
                if (var_63 == 0UL) {
                    *(uint32_t *)var_59 = (uint32_t)var_63;
                    r13_2 = r13_1 + 1UL;
                }
                r13_1 = r13_2;
                r13_3 = r13_2;
                r13_4 = r13_2;
                if (var_58) {
                    break;
                }
                rdx2_2 = (uint64_t)*var_55;
                continue;
            }
        var_64 = var_52 & 31UL;
        rbx_0 = var_64;
        rbx_1 = var_64;
        if ((int)var_32 > (int)4294967295U) {
            rbx_1 = rbx_0;
            r13_4 = r13_3;
            if ((int)var_53 > (int)4294967295U) {
                var_91 = var_0 + (-80L);
                var_92 = var_0 + (-136L);
                *(uint64_t *)var_92 = 4227695UL;
                var_93 = indirect_placeholder_17(var_91, r13_3, rsi, var_49, rdx);
                local_sp_1 = var_92;
                if (var_93 != 0UL) {
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4228085UL;
                    indirect_placeholder();
                    *(uint64_t *)(local_sp_1 + (-16L)) = 4228094UL;
                    indirect_placeholder();
                    return r12_0;
                }
                var_94 = *var_50;
                var_95 = (r13_3 << 2UL) + var_49;
                rax_3 = var_95;
                rdx2_1 = var_95;
                if (var_94 != 0U) {
                    var_96 = ((uint64_t)var_94 << 2UL) + var_95;
                    rdx2_1 = var_96;
                    *(uint32_t *)rax_3 = 0U;
                    var_97 = rax_3 + 4UL;
                    rax_3 = var_97;
                    do {
                        *(uint32_t *)rax_3 = 0U;
                        var_97 = rax_3 + 4UL;
                        rax_3 = var_97;
                    } while (var_96 != var_97);
                }
                var_98 = *(uint64_t *)(var_0 + (-88L));
                var_99 = *(uint64_t *)var_91;
                var_100 = var_0 + (-96L);
                *(uint32_t *)rdx2_1 = (uint32_t)(1UL << rbx_0);
                var_101 = (uint64_t)(*var_50 + 1U);
                *(uint64_t *)(var_0 + (-144L)) = 4227797UL;
                var_102 = indirect_placeholder_14(var_44, var_101, var_98, var_95, var_99, var_100);
                var_103 = var_0 + (-152L);
                *(uint64_t *)var_103 = 4227808UL;
                indirect_placeholder();
                r13_0 = var_102;
                local_sp_0 = var_103;
                *(uint64_t *)(local_sp_0 + (-8L)) = 4227816UL;
                indirect_placeholder();
                *(uint64_t *)(local_sp_0 + (-16L)) = 4227825UL;
                indirect_placeholder();
                if (r13_0 != 0UL) {
                    var_122 = *(uint64_t *)(local_sp_0 + 16UL);
                    var_123 = *(uint64_t *)(local_sp_0 + 8UL);
                    var_124 = *(uint64_t *)(local_sp_0 + 24UL);
                    *(uint64_t *)(local_sp_0 + (-24L)) = 4227854UL;
                    var_125 = indirect_placeholder_15(var_123, var_122, var_124);
                    *(uint64_t *)(local_sp_0 + (-32L)) = 4227865UL;
                    indirect_placeholder();
                    r12_0 = var_125;
                }
                return r12_0;
            }
        }
        var_65 = helper_cc_compute_all_wrapper(var_54, 0UL, 0UL, 24U);
        if ((uint64_t)(((unsigned char)(var_65 >> 4UL) ^ (unsigned char)var_65) & '\xc0') != 0UL) {
            var_66 = (uint64_t)*var_42;
            var_67 = var_66 + rsi;
            *_cast = var_66;
            var_68 = (var_67 << 2UL) + 4UL;
            var_69 = var_0 + (-136L);
            *(uint64_t *)var_69 = 4228125UL;
            var_70 = indirect_placeholder_16(var_68);
            var_71 = var_70.field_0;
            rdi3_0 = var_71;
            local_sp_1 = var_69;
            if (var_71 != 0UL) {
                *(uint64_t *)(local_sp_1 + (-8L)) = 4228085UL;
                indirect_placeholder();
                *(uint64_t *)(local_sp_1 + (-16L)) = 4228094UL;
                indirect_placeholder();
                return r12_0;
            }
            var_72 = *var_50;
            var_73 = *var_47;
            if (var_72 != 0U) {
                *(uint32_t *)((rax_0 << 2UL) + var_71) = 0U;
                var_74 = rax_0 + 1UL;
                rax_0 = var_74;
                do {
                    *(uint32_t *)((rax_0 << 2UL) + var_71) = 0U;
                    var_74 = rax_0 + 1UL;
                    rax_0 = var_74;
                } while (var_73 != var_74);
                rdi3_0 = (var_73 << 2UL) + var_71;
            }
            var_75 = (var_64 == 0UL);
            var_76 = (rsi == 0UL);
            rdx2_0 = rdi3_0;
            if (var_75) {
                if (!var_76) {
                    var_84 = rax_1 << 2UL;
                    *(uint32_t *)(var_84 + rdi3_0) = *(uint32_t *)(var_84 + rdx);
                    var_85 = rax_1 + 1UL;
                    rax_1 = var_85;
                    do {
                        var_84 = rax_1 << 2UL;
                        *(uint32_t *)(var_84 + rdi3_0) = *(uint32_t *)(var_84 + rdx);
                        var_85 = rax_1 + 1UL;
                        rax_1 = var_85;
                    } while (var_85 != rsi);
                    rdx2_0 = (rsi << 2UL) + rdi3_0;
                }
            } else {
                if (!var_76) {
                    var_77 = rsi5_0 << 2UL;
                    var_78 = rax_2 + ((uint64_t)*(uint32_t *)(var_77 + rdx) << var_64);
                    *(uint32_t *)(var_77 + rdi3_0) = (uint32_t)var_78;
                    var_79 = rsi5_0 + 1UL;
                    var_80 = var_78 >> 32UL;
                    rsi5_0 = var_79;
                    rax_2 = var_80;
                    do {
                        var_77 = rsi5_0 << 2UL;
                        var_78 = rax_2 + ((uint64_t)*(uint32_t *)(var_77 + rdx) << var_64);
                        *(uint32_t *)(var_77 + rdi3_0) = (uint32_t)var_78;
                        var_79 = rsi5_0 + 1UL;
                        var_80 = var_78 >> 32UL;
                        rsi5_0 = var_79;
                        rax_2 = var_80;
                    } while (var_79 != rsi);
                    var_81 = rsi << 2UL;
                    var_82 = var_81 + rdi3_0;
                    rdx2_0 = var_82;
                    if (var_80 == 0UL) {
                        var_83 = var_81 + (-4L);
                        *(uint32_t *)var_82 = (uint32_t)var_80;
                        rdx2_0 = (var_83 + rdi3_0) + 8UL;
                    }
                }
            }
            var_86 = var_0 + (-96L);
            var_87 = (uint64_t)((long)(rdx2_0 - var_71) >> (long)2UL);
            *(uint64_t *)(var_0 + (-144L)) = 4228254UL;
            var_88 = indirect_placeholder_14(var_44, r13_2, var_87, var_49, var_71, var_86);
            var_89 = var_0 + (-152L);
            *(uint64_t *)var_89 = 4228265UL;
            indirect_placeholder();
            r13_0 = var_88;
            local_sp_0 = var_89;
            *(uint64_t *)(local_sp_0 + (-8L)) = 4227816UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_0 + (-16L)) = 4227825UL;
            indirect_placeholder();
            if (r13_0 != 0UL) {
                var_122 = *(uint64_t *)(local_sp_0 + 16UL);
                var_123 = *(uint64_t *)(local_sp_0 + 8UL);
                var_124 = *(uint64_t *)(local_sp_0 + 24UL);
                *(uint64_t *)(local_sp_0 + (-24L)) = 4227854UL;
                var_125 = indirect_placeholder_15(var_123, var_122, var_124);
                *(uint64_t *)(local_sp_0 + (-32L)) = 4227865UL;
                indirect_placeholder();
                r12_0 = var_125;
            }
            return r12_0;
        }
        r13_5 = r13_4;
        if ((uint64_t)(uint32_t)rbx_1 != 0UL) {
            var_104 = (r13_4 << 2UL) + var_49;
            var_105 = rbx_1 & 63UL;
            var_106 = (uint32_t *)rdx2_4;
            var_107 = (uint64_t)*var_106;
            var_108 = rdx2_4 + 4UL;
            var_109 = rax_6 + (var_107 << var_105);
            *var_106 = (uint32_t)var_109;
            var_110 = var_109 >> 32UL;
            rdx2_4 = var_108;
            rax_6 = var_110;
            do {
                var_106 = (uint32_t *)rdx2_4;
                var_107 = (uint64_t)*var_106;
                var_108 = rdx2_4 + 4UL;
                var_109 = rax_6 + (var_107 << var_105);
                *var_106 = (uint32_t)var_109;
                var_110 = var_109 >> 32UL;
                rdx2_4 = var_108;
                rax_6 = var_110;
            } while (var_108 != var_104);
            if (var_110 == 0UL) {
                *(uint32_t *)var_104 = (uint32_t)var_110;
                r13_5 = r13_4 + 1UL;
            }
        }
        rax_7 = r13_5;
        r13_6 = r13_5;
        if ((int)var_51 <= (int)31U) {
            var_111 = (uint64_t)*var_42;
            var_112 = (var_111 << 2UL) + var_49;
            rax_8 = var_111;
            var_113 = rax_7 + (-1L);
            var_114 = var_113 << 2UL;
            *(uint32_t *)(var_114 + var_112) = *(uint32_t *)(var_114 + var_49);
            rax_7 = var_113;
            do {
                var_113 = rax_7 + (-1L);
                var_114 = var_113 << 2UL;
                *(uint32_t *)(var_114 + var_112) = *(uint32_t *)(var_114 + var_49);
                rax_7 = var_113;
            } while (var_113 != 0UL);
            if (*var_42 != 0U) {
                var_115 = rax_8 + (-1L);
                *(uint32_t *)((var_115 << 2UL) + var_49) = 0U;
                rax_8 = var_115;
                do {
                    var_115 = rax_8 + (-1L);
                    *(uint32_t *)((var_115 << 2UL) + var_49) = 0U;
                    rax_8 = var_115;
                } while (var_115 != 0UL);
            }
            r13_6 = r13_5 + var_111;
        }
        var_116 = var_0 + (-96L);
        if ((int)var_32 < (int)0U) {
            var_119 = var_0 + (-88L);
            var_120 = var_0 + (-136L);
            *(uint64_t *)var_120 = 4228286UL;
            var_121 = indirect_placeholder_14(var_116, r13_6, rsi, var_49, rdx, var_119);
            r13_0 = var_121;
            local_sp_0 = var_120;
        } else {
            var_117 = var_0 + (-136L);
            *(uint64_t *)var_117 = 4228069UL;
            var_118 = indirect_placeholder_17(var_116, r13_6, rsi, var_49, rdx);
            r13_0 = var_118;
            local_sp_0 = var_117;
        }
        *(uint64_t *)(local_sp_0 + (-8L)) = 4227816UL;
        indirect_placeholder();
        *(uint64_t *)(local_sp_0 + (-16L)) = 4227825UL;
        indirect_placeholder();
        if (r13_0 == 0UL) {
            var_122 = *(uint64_t *)(local_sp_0 + 16UL);
            var_123 = *(uint64_t *)(local_sp_0 + 8UL);
            var_124 = *(uint64_t *)(local_sp_0 + 24UL);
            *(uint64_t *)(local_sp_0 + (-24L)) = 4227854UL;
            var_125 = indirect_placeholder_15(var_123, var_122, var_124);
            *(uint64_t *)(local_sp_0 + (-32L)) = 4227865UL;
            indirect_placeholder();
            r12_0 = var_125;
        }
    }
}
