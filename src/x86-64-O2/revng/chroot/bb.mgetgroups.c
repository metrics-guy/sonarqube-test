typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_6(void);
uint64_t bb_mgetgroups(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint32_t *var_47;
    uint32_t var_48;
    uint64_t var_49;
    uint64_t rax_5;
    uint64_t var_46;
    uint64_t var_34;
    uint32_t _pre_phi;
    uint64_t rax_0;
    uint64_t rbp_0;
    uint32_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t rbp_2;
    uint64_t rax_2;
    uint64_t rax_1;
    uint64_t rbp_1;
    uint64_t rdx1_0;
    uint32_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint32_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint32_t var_33;
    uint64_t local_sp_0;
    uint64_t rbp_3;
    uint64_t var_35;
    uint64_t var_36;
    uint32_t var_37;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t rsi3_0;
    uint64_t local_sp_1;
    uint64_t rax_3;
    uint32_t *var_38;
    uint32_t var_39;
    uint64_t rax_4;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint32_t *var_12;
    uint32_t var_13;
    uint64_t var_14;
    uint32_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t local_sp_2;
    uint32_t var_8;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_20;
    uint32_t *var_21;
    uint32_t var_22;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r12();
    var_2 = init_rbx();
    var_3 = init_r13();
    var_4 = init_r14();
    var_5 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_4;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    rax_3 = 0UL;
    rax_5 = 4294967295UL;
    if (rdi != 0UL) {
        *(uint32_t *)(var_0 + (-44L)) = 10U;
        var_6 = var_0 + (-64L);
        *(uint64_t *)var_6 = 4230770UL;
        var_7 = indirect_placeholder_2(0UL, 10UL);
        local_sp_2 = var_6;
        rax_4 = var_7;
        if (var_7 != 0UL) {
            while (1U)
                {
                    var_8 = *(uint32_t *)(local_sp_2 | 12UL);
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4230802UL;
                    indirect_placeholder_1();
                    if ((int)(uint32_t)rax_4 >= (int)0U) {
                        var_9 = (uint64_t)*(uint32_t *)(local_sp_2 | 4UL);
                        var_10 = local_sp_2 + (-16L);
                        *(uint64_t *)var_10 = 4230819UL;
                        var_11 = indirect_placeholder_2(rax_4, var_9);
                        local_sp_1 = var_10;
                        if (var_11 != 0UL) {
                            break;
                        }
                        *(uint64_t *)rdx = var_11;
                        return (uint64_t)*(uint32_t *)(local_sp_2 + (-4L));
                    }
                    var_12 = (uint32_t *)(local_sp_2 | 4UL);
                    var_13 = *var_12;
                    var_14 = (uint64_t)var_13;
                    rsi3_0 = var_14;
                    if ((uint64_t)(var_13 - var_8) == 0UL) {
                        var_15 = (uint32_t)(var_14 << 1UL);
                        var_16 = (uint64_t)(var_15 & (-2));
                        *var_12 = var_15;
                        rsi3_0 = var_16;
                    }
                    var_17 = (uint64_t)((long)(rsi3_0 << 32UL) >> (long)32UL);
                    var_18 = local_sp_2 + (-16L);
                    *(uint64_t *)var_18 = 4230890UL;
                    var_19 = indirect_placeholder_2(rax_4, var_17);
                    local_sp_1 = var_18;
                    local_sp_2 = var_18;
                    rax_4 = var_19;
                    if (var_19 != 0UL) {
                        continue;
                    }
                    break;
                }
            *(uint64_t *)(local_sp_1 + (-8L)) = 4230829UL;
            indirect_placeholder_1();
            var_38 = (uint32_t *)rax_3;
            var_39 = *var_38;
            *(uint64_t *)(local_sp_1 + (-16L)) = 4230839UL;
            indirect_placeholder_1();
            *(uint64_t *)(local_sp_1 + (-24L)) = 4230844UL;
            indirect_placeholder_1();
            *var_38 = var_39;
        }
        return rax_5;
    }
    *(uint64_t *)(var_0 + (-64L)) = 4230937UL;
    var_20 = indirect_placeholder_6();
    var_21 = (uint32_t *)(var_0 + (-52L));
    var_22 = (uint32_t)var_20;
    *var_21 = var_22;
    if ((int)var_22 < (int)0U) {
        *(uint64_t *)(var_0 + (-72L)) = 4231125UL;
        indirect_placeholder_1();
        *(uint64_t *)(var_0 + (-80L)) = 4231146UL;
        var_46 = indirect_placeholder_2(0UL, 1UL);
        if (*(uint32_t *)var_20 != 38U & var_46 == 0UL) {
            *(uint64_t *)rdx = var_46;
            var_47 = (uint32_t *)var_46;
            var_48 = (uint32_t)rsi;
            *var_47 = var_48;
            var_49 = ((uint64_t)(var_48 + 1U) != 0UL);
            rax_5 = var_49;
        }
    } else {
        if ((uint64_t)var_22 == 0UL) {
            if ((uint64_t)((uint32_t)rsi + 1U) == 0UL) {
                var_23 = (uint64_t)((long)(var_20 << 32UL) >> (long)32UL);
                var_24 = var_0 + (-72L);
                *(uint64_t *)var_24 = 4231185UL;
                var_25 = indirect_placeholder_2(0UL, var_23);
                local_sp_0 = var_24;
                rbp_3 = var_25;
                if (var_25 == 0UL) {
                    return rax_5;
                }
                var_35 = local_sp_0 + (-8L);
                *(uint64_t *)var_35 = 4231216UL;
                var_36 = indirect_placeholder_6();
                var_37 = (uint32_t)var_36;
                _pre_phi = var_37;
                rax_0 = var_36;
                rbp_0 = rbp_3;
                local_sp_1 = var_35;
                rax_3 = var_36;
                if ((int)var_37 > (int)4294967295U) {
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4230829UL;
                    indirect_placeholder_1();
                    var_38 = (uint32_t *)rax_3;
                    var_39 = *var_38;
                    *(uint64_t *)(local_sp_1 + (-16L)) = 4230839UL;
                    indirect_placeholder_1();
                    *(uint64_t *)(local_sp_1 + (-24L)) = 4230844UL;
                    indirect_placeholder_1();
                    *var_38 = var_39;
                    return rax_5;
                }
            }
            var_26 = var_20 + 1UL;
            var_27 = (uint64_t)((long)(var_26 << 32UL) >> (long)32UL);
            *var_21 = (uint32_t)var_26;
            var_28 = var_0 + (-72L);
            *(uint64_t *)var_28 = 4230987UL;
            var_29 = indirect_placeholder_2(0UL, var_27);
            rbp_0 = var_29;
            local_sp_0 = var_28;
            rbp_3 = var_29;
            if (var_29 == 0UL) {
                return rax_5;
            }
            var_30 = (uint32_t)rsi;
            if ((uint64_t)(var_30 + 1U) == 0UL) {
                var_35 = local_sp_0 + (-8L);
                *(uint64_t *)var_35 = 4231216UL;
                var_36 = indirect_placeholder_6();
                var_37 = (uint32_t)var_36;
                _pre_phi = var_37;
                rax_0 = var_36;
                rbp_0 = rbp_3;
                local_sp_1 = var_35;
                rax_3 = var_36;
                if ((int)var_37 > (int)4294967295U) {
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4230829UL;
                    indirect_placeholder_1();
                    var_38 = (uint32_t *)rax_3;
                    var_39 = *var_38;
                    *(uint64_t *)(local_sp_1 + (-16L)) = 4230839UL;
                    indirect_placeholder_1();
                    *(uint64_t *)(local_sp_1 + (-24L)) = 4230844UL;
                    indirect_placeholder_1();
                    *var_38 = var_39;
                    return rax_5;
                }
            }
            var_31 = var_0 + (-80L);
            *(uint64_t *)var_31 = 4231025UL;
            var_32 = indirect_placeholder_6();
            var_33 = (uint32_t)var_32;
            local_sp_1 = var_31;
            rax_3 = var_32;
            if ((int)var_33 >= (int)0U) {
                *(uint64_t *)(local_sp_1 + (-8L)) = 4230829UL;
                indirect_placeholder_1();
                var_38 = (uint32_t *)rax_3;
                var_39 = *var_38;
                *(uint64_t *)(local_sp_1 + (-16L)) = 4230839UL;
                indirect_placeholder_1();
                *(uint64_t *)(local_sp_1 + (-24L)) = 4230844UL;
                indirect_placeholder_1();
                *var_38 = var_39;
                return rax_5;
            }
            *(uint32_t *)var_29 = var_30;
            var_34 = (uint64_t)(var_33 + 1U);
            _pre_phi = (uint32_t)var_34;
            rax_0 = var_34;
            *(uint64_t *)rdx = rbp_0;
            rax_1 = rax_0;
            rbp_1 = rbp_0;
            rax_5 = rax_0;
            var_40 = *(uint32_t *)rbp_0;
            var_41 = (uint64_t)((long)(rax_0 << 32UL) >> (long)30UL) + rbp_0;
            var_42 = rbp_0 + 4UL;
            rdx1_0 = var_42;
            if ((int)_pre_phi <= (int)1U & var_41 > var_42) {
                var_45 = rdx1_0 + 4UL;
                rax_1 = rax_2;
                rbp_1 = rbp_2;
                rdx1_0 = var_45;
                rax_5 = rax_2;
                do {
                    var_43 = *(uint32_t *)rdx1_0;
                    rax_2 = rax_1;
                    rbp_2 = rbp_1;
                    if ((uint64_t)(var_43 - var_40) == 0UL) {
                        rax_2 = (uint64_t)((uint32_t)rax_1 + (-1));
                    } else {
                        if ((uint64_t)(var_43 - *(uint32_t *)rbp_1) == 0UL) {
                            rax_2 = (uint64_t)((uint32_t)rax_1 + (-1));
                        } else {
                            var_44 = rbp_1 + 4UL;
                            *(uint32_t *)var_44 = var_43;
                            rbp_2 = var_44;
                        }
                    }
                    var_45 = rdx1_0 + 4UL;
                    rax_1 = rax_2;
                    rbp_1 = rbp_2;
                    rdx1_0 = var_45;
                    rax_5 = rax_2;
                } while (var_41 <= var_45);
            }
        } else {
            var_26 = var_20 + 1UL;
            var_27 = (uint64_t)((long)(var_26 << 32UL) >> (long)32UL);
            *var_21 = (uint32_t)var_26;
            var_28 = var_0 + (-72L);
            *(uint64_t *)var_28 = 4230987UL;
            var_29 = indirect_placeholder_2(0UL, var_27);
            rbp_0 = var_29;
            local_sp_0 = var_28;
            rbp_3 = var_29;
            if (var_29 == 0UL) {
                return rax_5;
            }
            var_30 = (uint32_t)rsi;
            if ((uint64_t)(var_30 + 1U) != 0UL) {
                var_35 = local_sp_0 + (-8L);
                *(uint64_t *)var_35 = 4231216UL;
                var_36 = indirect_placeholder_6();
                var_37 = (uint32_t)var_36;
                _pre_phi = var_37;
                rax_0 = var_36;
                rbp_0 = rbp_3;
                local_sp_1 = var_35;
                rax_3 = var_36;
                if ((int)var_37 <= (int)4294967295U) {
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4230829UL;
                    indirect_placeholder_1();
                    var_38 = (uint32_t *)rax_3;
                    var_39 = *var_38;
                    *(uint64_t *)(local_sp_1 + (-16L)) = 4230839UL;
                    indirect_placeholder_1();
                    *(uint64_t *)(local_sp_1 + (-24L)) = 4230844UL;
                    indirect_placeholder_1();
                    *var_38 = var_39;
                    return rax_5;
                }
            }
            var_31 = var_0 + (-80L);
            *(uint64_t *)var_31 = 4231025UL;
            var_32 = indirect_placeholder_6();
            var_33 = (uint32_t)var_32;
            local_sp_1 = var_31;
            rax_3 = var_32;
            if ((int)var_33 >= (int)0U) {
                *(uint64_t *)(local_sp_1 + (-8L)) = 4230829UL;
                indirect_placeholder_1();
                var_38 = (uint32_t *)rax_3;
                var_39 = *var_38;
                *(uint64_t *)(local_sp_1 + (-16L)) = 4230839UL;
                indirect_placeholder_1();
                *(uint64_t *)(local_sp_1 + (-24L)) = 4230844UL;
                indirect_placeholder_1();
                *var_38 = var_39;
                return rax_5;
            }
            *(uint32_t *)var_29 = var_30;
            var_34 = (uint64_t)(var_33 + 1U);
            _pre_phi = (uint32_t)var_34;
            rax_0 = var_34;
        }
    }
}
