typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_3_ret_type;
struct indirect_placeholder_4_ret_type;
struct indirect_placeholder_5_ret_type;
struct indirect_placeholder_3_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_4_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_5_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t init_rax(void);
extern void indirect_placeholder_6(uint64_t param_0, uint64_t param_1);
extern uint64_t init_r10(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
extern struct indirect_placeholder_3_ret_type indirect_placeholder_3(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_4_ret_type indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_5_ret_type indirect_placeholder_5(uint64_t param_0, uint64_t param_1);
void bb_next_file_name(uint64_t r8, uint64_t rcx, uint64_t r9) {
    uint64_t rbx_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t r81_1;
    uint64_t r13_0;
    uint64_t var_67;
    uint64_t local_sp_0;
    uint64_t rdx_0_in;
    uint64_t rdx_0;
    uint64_t local_sp_2;
    uint64_t var_66;
    uint64_t var_65;
    uint64_t r93_0;
    uint64_t var_61;
    uint64_t local_sp_1;
    uint64_t var_62;
    uint64_t r13_1;
    uint64_t rax_2;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t rbx_1;
    uint64_t var_31;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_50;
    uint64_t local_sp_3;
    uint64_t r93_2;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t local_sp_4;
    struct indirect_placeholder_3_ret_type var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_12;
    uint64_t r81_0;
    uint64_t var_13;
    unsigned char var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    bool var_19;
    unsigned char *var_20;
    uint64_t rcx2_1;
    uint64_t rcx2_0;
    uint64_t rax_1;
    uint64_t *var_21;
    uint64_t var_22;
    bool var_23;
    uint64_t var_24;
    unsigned char var_25;
    unsigned char *var_26;
    uint64_t r93_1;
    unsigned char var_27;
    unsigned char *var_28;
    unsigned char var_29;
    uint64_t var_30;
    bool var_32;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    struct indirect_placeholder_5_ret_type var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t *var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_r12();
    var_3 = init_rbx();
    var_4 = init_r13();
    var_5 = init_r14();
    var_6 = init_rbp();
    var_7 = init_r10();
    var_8 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_5;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_6;
    var_9 = *(uint64_t *)4298392UL;
    var_10 = var_0 + (-40L);
    *(uint64_t *)var_10 = var_3;
    var_11 = *(uint64_t *)4298376UL;
    r81_1 = r8;
    rax_2 = var_1;
    r93_2 = r9;
    local_sp_4 = var_10;
    r81_0 = r8;
    rcx2_1 = rcx;
    r93_1 = r9;
    if (var_9 != 0UL) {
        var_31 = *(uint64_t *)4298168UL;
        var_32 = (var_31 == 0UL);
        if (var_32) {
            var_48 = var_0 + (-48L);
            *(uint64_t *)var_48 = 4207772UL;
            indirect_placeholder();
            var_49 = *(uint64_t *)4298360UL;
            *(uint64_t *)4298160UL = rax_2;
            local_sp_3 = var_48;
            if (var_49 == 0UL) {
                var_50 = var_0 + (-56L);
                *(uint64_t *)var_50 = 4207801UL;
                indirect_placeholder();
                local_sp_3 = var_50;
            }
            var_51 = var_11 + rax_2;
            *(uint64_t *)4298152UL = 0UL;
            var_52 = var_51 + 1UL;
            *(uint64_t *)4298168UL = var_51;
            local_sp_4 = local_sp_3;
            if (rax_2 <= var_52) {
                *(uint64_t *)(local_sp_4 + (-8L)) = 4208239UL;
                indirect_placeholder_6(r81_1, r93_2);
                abort();
            }
            *(uint64_t *)(local_sp_3 + (-8L)) = 4207842UL;
            var_53 = indirect_placeholder_3(var_9, var_52);
            var_54 = var_53.field_0;
            var_55 = var_53.field_1;
            var_56 = *(uint64_t *)4298160UL;
            *(uint64_t *)4298392UL = var_54;
            var_57 = local_sp_3 + (-16L);
            *(uint64_t *)var_57 = 4207877UL;
            indirect_placeholder();
            rbx_1 = var_56;
            r13_1 = var_54;
            local_sp_2 = var_57;
            r93_0 = var_55;
        } else {
            var_33 = var_11 + 1UL;
            var_34 = var_31 + 2UL;
            var_35 = var_31 + 3UL;
            var_36 = *(uint64_t *)4298160UL;
            var_37 = var_35 - var_36;
            *(uint64_t *)4298168UL = var_34;
            *(uint64_t *)4298376UL = var_33;
            var_38 = helper_cc_compute_c_wrapper(var_37, var_36, var_8, 17U);
            if (var_38 != 0UL) {
                *(uint64_t *)(local_sp_4 + (-8L)) = 4208239UL;
                indirect_placeholder_6(r81_1, r93_2);
                abort();
            }
            var_39 = var_0 + (-48L);
            *(uint64_t *)var_39 = 4207945UL;
            var_40 = indirect_placeholder_5(var_9, var_35);
            var_41 = var_40.field_0;
            var_42 = var_40.field_1;
            var_43 = *(uint64_t **)4298144UL;
            var_44 = *(uint64_t *)4298160UL;
            *(uint64_t *)4298392UL = var_41;
            var_45 = *(uint64_t *)4297448UL;
            var_46 = *var_43;
            var_47 = var_44 + 1UL;
            *(uint64_t *)4298160UL = var_47;
            *(unsigned char *)(var_44 + var_41) = *(unsigned char *)(var_46 + var_45);
            rbx_1 = var_47;
            r13_1 = var_41;
            local_sp_2 = var_39;
            r93_0 = var_42;
        }
        var_58 = *(uint64_t *)4298376UL;
        var_59 = rbx_1 + r13_1;
        *(uint64_t *)4298384UL = var_59;
        var_60 = local_sp_2 + (-8L);
        *(uint64_t *)var_60 = 4208032UL;
        indirect_placeholder();
        local_sp_1 = var_60;
        if (*(uint64_t *)4298360UL == 0UL) {
            var_61 = local_sp_2 + (-16L);
            *(uint64_t *)var_61 = 4208060UL;
            indirect_placeholder();
            local_sp_1 = var_61;
        }
        *(unsigned char *)(*(uint64_t *)4298168UL + r13_1) = (unsigned char)'\x00';
        *(uint64_t *)(local_sp_1 + (-8L)) = 4208081UL;
        indirect_placeholder();
        var_62 = local_sp_1 + (-16L);
        *(uint64_t *)var_62 = 4208094UL;
        var_63 = indirect_placeholder_2(rbx_1, r81_1, var_58, var_58, var_59, var_7, 8UL, r93_0);
        var_64 = *(uint64_t *)4298368UL;
        *(uint64_t *)4298144UL = var_63;
        rdx_0_in = var_63;
        rbx_0 = var_63;
        r13_0 = var_64;
        local_sp_0 = var_62;
        if (var_64 != 0UL) {
            if (var_32) {
                var_65 = local_sp_1 + (-24L);
                *(uint64_t *)var_65 = 4208264UL;
                indirect_placeholder();
                rbx_0 = *(uint64_t *)4298144UL;
                r13_0 = *(uint64_t *)4298368UL;
                local_sp_0 = var_65;
            }
            *(uint64_t *)(local_sp_0 + (-8L)) = 4208133UL;
            indirect_placeholder();
            var_66 = *(uint64_t *)4298376UL;
            *(uint64_t *)(local_sp_0 + (-16L)) = 4208171UL;
            indirect_placeholder();
            if (var_63 != 0UL) {
                var_67 = ((var_66 - var_63) << 3UL) + rbx_0;
                rdx_0 = rdx_0_in + (-1L);
                *(uint64_t *)((rdx_0 << 3UL) + var_67) = (uint64_t)((long)(((uint64_t)*(unsigned char *)(rdx_0 + r13_0) << 32UL) + (-206158430208L)) >> (long)32UL);
                rdx_0_in = rdx_0;
                do {
                    rdx_0 = rdx_0_in + (-1L);
                    *(uint64_t *)((rdx_0 << 3UL) + var_67) = (uint64_t)((long)(((uint64_t)*(unsigned char *)(rdx_0 + r13_0) << 32UL) + (-206158430208L)) >> (long)32UL);
                    rdx_0_in = rdx_0;
                } while (rdx_0 != 0UL);
            }
        }
        return;
    }
    var_12 = var_11 + (-1L);
    rax_1 = var_12;
    if (var_11 == 0UL) {
        *(uint64_t *)(var_0 + (-48L)) = 4208314UL;
        indirect_placeholder_4(0UL, r81_0, 4268376UL, 1UL, rcx2_1, 0UL, r93_1);
        abort();
    }
    var_13 = *(uint64_t *)4298144UL;
    var_14 = *(unsigned char *)4297456UL;
    var_15 = (uint64_t)var_14;
    var_16 = *(uint64_t *)4297448UL;
    var_17 = *(uint64_t *)4298384UL;
    var_18 = (var_12 << 3UL) + var_13;
    var_19 = (var_14 == '\x00');
    var_20 = (unsigned char *)var_16;
    r81_1 = var_15;
    r93_2 = var_13;
    r81_0 = var_15;
    rcx2_0 = var_18;
    r93_1 = var_13;
    while (1U)
        {
            var_21 = (uint64_t *)rcx2_0;
            var_22 = *var_21 + 1UL;
            *var_21 = var_22;
            var_23 = (rax_1 == 0UL);
            rcx2_1 = rcx2_0;
            if ((var_23 ^ 1) || var_19) {
                var_27 = *(unsigned char *)(var_22 + var_16);
                var_28 = (unsigned char *)(rax_1 + var_17);
                *var_28 = var_27;
                if (var_27 != '\x00') {
                    loop_state_var = 2U;
                    break;
                }
                *var_21 = 0UL;
                var_29 = *var_20;
                var_30 = rcx2_0 + (-8L);
                *var_28 = var_29;
                rcx2_0 = var_30;
                rcx2_1 = var_30;
                if (!var_23) {
                    loop_state_var = 1U;
                    break;
                }
                rax_1 = rax_1 + (-1L);
                continue;
            }
            var_24 = *(uint64_t *)var_13;
            rax_2 = var_24;
            if (*(unsigned char *)((var_24 + var_16) + 1UL) != '\x00') {
                loop_state_var = 0U;
                break;
            }
            var_25 = *(unsigned char *)(var_22 + var_16);
            var_26 = (unsigned char *)var_17;
            *var_26 = var_25;
            if (var_25 != '\x00') {
                loop_state_var = 2U;
                break;
            }
            *var_21 = 0UL;
            *var_26 = *var_20;
            loop_state_var = 1U;
            break;
        }
    switch (loop_state_var) {
      case 1U:
        {
            *(uint64_t *)(var_0 + (-48L)) = 4208314UL;
            indirect_placeholder_4(0UL, r81_0, 4268376UL, 1UL, rcx2_1, 0UL, r93_1);
            abort();
        }
        break;
      case 2U:
        {
            return;
        }
        break;
      case 0U:
        {
            var_31 = *(uint64_t *)4298168UL;
            var_32 = (var_31 == 0UL);
            if (var_32) {
                var_48 = var_0 + (-48L);
                *(uint64_t *)var_48 = 4207772UL;
                indirect_placeholder();
                var_49 = *(uint64_t *)4298360UL;
                *(uint64_t *)4298160UL = rax_2;
                local_sp_3 = var_48;
                if (var_49 == 0UL) {
                    var_50 = var_0 + (-56L);
                    *(uint64_t *)var_50 = 4207801UL;
                    indirect_placeholder();
                    local_sp_3 = var_50;
                }
                var_51 = var_11 + rax_2;
                *(uint64_t *)4298152UL = 0UL;
                var_52 = var_51 + 1UL;
                *(uint64_t *)4298168UL = var_51;
                local_sp_4 = local_sp_3;
                if (rax_2 <= var_52) {
                    *(uint64_t *)(local_sp_4 + (-8L)) = 4208239UL;
                    indirect_placeholder_6(r81_1, r93_2);
                    abort();
                }
                *(uint64_t *)(local_sp_3 + (-8L)) = 4207842UL;
                var_53 = indirect_placeholder_3(var_9, var_52);
                var_54 = var_53.field_0;
                var_55 = var_53.field_1;
                var_56 = *(uint64_t *)4298160UL;
                *(uint64_t *)4298392UL = var_54;
                var_57 = local_sp_3 + (-16L);
                *(uint64_t *)var_57 = 4207877UL;
                indirect_placeholder();
                rbx_1 = var_56;
                r13_1 = var_54;
                local_sp_2 = var_57;
                r93_0 = var_55;
            } else {
                var_33 = var_11 + 1UL;
                var_34 = var_31 + 2UL;
                var_35 = var_31 + 3UL;
                var_36 = *(uint64_t *)4298160UL;
                var_37 = var_35 - var_36;
                *(uint64_t *)4298168UL = var_34;
                *(uint64_t *)4298376UL = var_33;
                var_38 = helper_cc_compute_c_wrapper(var_37, var_36, var_8, 17U);
                if (var_38 != 0UL) {
                    *(uint64_t *)(local_sp_4 + (-8L)) = 4208239UL;
                    indirect_placeholder_6(r81_1, r93_2);
                    abort();
                }
                var_39 = var_0 + (-48L);
                *(uint64_t *)var_39 = 4207945UL;
                var_40 = indirect_placeholder_5(var_9, var_35);
                var_41 = var_40.field_0;
                var_42 = var_40.field_1;
                var_43 = *(uint64_t **)4298144UL;
                var_44 = *(uint64_t *)4298160UL;
                *(uint64_t *)4298392UL = var_41;
                var_45 = *(uint64_t *)4297448UL;
                var_46 = *var_43;
                var_47 = var_44 + 1UL;
                *(uint64_t *)4298160UL = var_47;
                *(unsigned char *)(var_44 + var_41) = *(unsigned char *)(var_46 + var_45);
                rbx_1 = var_47;
                r13_1 = var_41;
                local_sp_2 = var_39;
                r93_0 = var_42;
            }
            var_58 = *(uint64_t *)4298376UL;
            var_59 = rbx_1 + r13_1;
            *(uint64_t *)4298384UL = var_59;
            var_60 = local_sp_2 + (-8L);
            *(uint64_t *)var_60 = 4208032UL;
            indirect_placeholder();
            local_sp_1 = var_60;
            if (*(uint64_t *)4298360UL == 0UL) {
                var_61 = local_sp_2 + (-16L);
                *(uint64_t *)var_61 = 4208060UL;
                indirect_placeholder();
                local_sp_1 = var_61;
            }
            *(unsigned char *)(*(uint64_t *)4298168UL + r13_1) = (unsigned char)'\x00';
            *(uint64_t *)(local_sp_1 + (-8L)) = 4208081UL;
            indirect_placeholder();
            var_62 = local_sp_1 + (-16L);
            *(uint64_t *)var_62 = 4208094UL;
            var_63 = indirect_placeholder_2(rbx_1, r81_1, var_58, var_58, var_59, var_7, 8UL, r93_0);
            var_64 = *(uint64_t *)4298368UL;
            *(uint64_t *)4298144UL = var_63;
            rdx_0_in = var_63;
            rbx_0 = var_63;
            r13_0 = var_64;
            local_sp_0 = var_62;
            if (var_64 != 0UL) {
                if (var_32) {
                    var_65 = local_sp_1 + (-24L);
                    *(uint64_t *)var_65 = 4208264UL;
                    indirect_placeholder();
                    rbx_0 = *(uint64_t *)4298144UL;
                    r13_0 = *(uint64_t *)4298368UL;
                    local_sp_0 = var_65;
                }
                *(uint64_t *)(local_sp_0 + (-8L)) = 4208133UL;
                indirect_placeholder();
                var_66 = *(uint64_t *)4298376UL;
                *(uint64_t *)(local_sp_0 + (-16L)) = 4208171UL;
                indirect_placeholder();
                if (var_63 != 0UL) {
                    var_67 = ((var_66 - var_63) << 3UL) + rbx_0;
                    rdx_0 = rdx_0_in + (-1L);
                    *(uint64_t *)((rdx_0 << 3UL) + var_67) = (uint64_t)((long)(((uint64_t)*(unsigned char *)(rdx_0 + r13_0) << 32UL) + (-206158430208L)) >> (long)32UL);
                    rdx_0_in = rdx_0;
                    do {
                        rdx_0 = rdx_0_in + (-1L);
                        *(uint64_t *)((rdx_0 << 3UL) + var_67) = (uint64_t)((long)(((uint64_t)*(unsigned char *)(rdx_0 + r13_0) << 32UL) + (-206158430208L)) >> (long)32UL);
                        rdx_0_in = rdx_0;
                    } while (rdx_0 != 0UL);
                }
            }
            return;
        }
        break;
    }
}
