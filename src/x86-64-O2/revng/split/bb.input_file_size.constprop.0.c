typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder_12(uint64_t param_0);
extern uint64_t init_rax(void);
typedef _Bool bool;
uint64_t bb_input_file_size_constprop_0(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t local_sp_2;
    uint64_t r14_0;
    uint64_t r14_1;
    uint64_t var_19;
    uint64_t local_sp_0;
    uint64_t rbp_0;
    uint64_t var_20;
    uint64_t local_sp_1;
    uint64_t var_18;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t rax_0;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint32_t *var_21;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_r12();
    var_3 = init_rbx();
    var_4 = init_r13();
    var_5 = init_r14();
    var_6 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_5;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_6;
    *(uint64_t *)(var_0 + (-40L)) = var_3;
    var_7 = var_0 + (-48L);
    *(uint64_t *)var_7 = 4215148UL;
    indirect_placeholder();
    r14_0 = 18446744073709551615UL;
    local_sp_2 = var_7;
    r14_1 = 0UL;
    if ((long)var_1 < (long)0UL) {
        *(uint64_t *)(var_0 + (-56L)) = 4215357UL;
        indirect_placeholder();
        var_21 = (uint32_t *)var_1;
        if (*var_21 == 29U) {
            *(uint64_t *)(var_0 + (-64L)) = 4215367UL;
            indirect_placeholder();
            *var_21 = 0U;
        }
    } else {
        while (1U)
            {
                var_8 = r14_1 + rdi;
                var_9 = rsi - r14_1;
                var_10 = local_sp_2 + (-8L);
                *(uint64_t *)var_10 = 4215207UL;
                var_11 = indirect_placeholder_9(var_9, 0UL, var_8);
                local_sp_1 = var_10;
                rax_0 = var_11;
                r14_0 = r14_1;
                local_sp_2 = var_10;
                switch_state_var = 0;
                switch (var_11) {
                  case 18446744073709551615UL:
                    {
                        r14_0 = 18446744073709551615UL;
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 0UL:
                    {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  default:
                    {
                        var_12 = var_11 + r14_1;
                        r14_0 = 18446744073709551615UL;
                        r14_1 = var_12;
                        if (var_12 < rsi) {
                            continue;
                        }
                        var_13 = *(uint64_t *)4298256UL;
                        rbp_0 = var_13;
                        if (var_13 != 0UL) {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        var_14 = (uint64_t)*(uint32_t *)4298232UL;
                        var_15 = var_1 + var_12;
                        var_16 = local_sp_2 + (-16L);
                        *(uint64_t *)var_16 = 4215250UL;
                        var_17 = indirect_placeholder_12(var_14);
                        local_sp_0 = var_16;
                        rax_0 = 9223372036854775807UL;
                        if (!(((long)var_13 < (long)var_15) || ((uint64_t)(unsigned char)var_17 == 0UL))) {
                            loop_state_var = 2U;
                            switch_state_var = 1;
                            break;
                        }
                        var_18 = local_sp_2 + (-24L);
                        *(uint64_t *)var_18 = 4215273UL;
                        indirect_placeholder();
                        local_sp_0 = var_18;
                        rbp_0 = var_15;
                        if ((long)var_17 >= (long)0UL) {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        if (var_15 != var_17) {
                            loop_state_var = 2U;
                            switch_state_var = 1;
                            break;
                        }
                        var_19 = local_sp_2 + (-32L);
                        *(uint64_t *)var_19 = 4215298UL;
                        indirect_placeholder();
                        local_sp_0 = var_19;
                        rbp_0 = ((long)var_17 < (long)var_15) ? var_15 : var_17;
                        loop_state_var = 2U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
        switch (loop_state_var) {
          case 0U:
            {
                break;
            }
            break;
          case 1U:
            {
                *(uint64_t *)(local_sp_1 + (-8L)) = 4215337UL;
                indirect_placeholder();
                *(uint32_t *)rax_0 = 75U;
            }
            break;
          case 2U:
            {
                var_20 = (rbp_0 - var_15) + var_12;
                local_sp_1 = local_sp_0;
                r14_0 = var_20;
                if (var_20 == 9223372036854775807UL) {
                    return r14_0;
                }
                *(uint64_t *)(local_sp_1 + (-8L)) = 4215337UL;
                indirect_placeholder();
                *(uint32_t *)rax_0 = 75U;
            }
            break;
        }
    }
}
