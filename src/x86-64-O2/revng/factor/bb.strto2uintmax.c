typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
typedef _Bool bool;
uint64_t bb_strto2uintmax(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t r8_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    unsigned char var_4;
    uint64_t var_5;
    uint64_t rdx1_0;
    uint64_t rdx1_1;
    uint64_t var_6;
    unsigned char var_7;
    uint64_t rax_0;
    uint64_t var_8;
    uint64_t rbx_1;
    uint64_t rcx_0;
    uint32_t var_9;
    uint64_t rbx_2;
    uint64_t rbx_0;
    uint64_t rbp_0;
    uint64_t rdx1_2;
    uint64_t cc_src2_0;
    uint32_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint32_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t rbp_1;
    uint64_t rbp_2;
    uint64_t rax_1;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_cc_src2();
    var_4 = *(unsigned char *)rdx;
    var_5 = (uint64_t)var_4;
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    rdx1_0 = rdx;
    rdx1_1 = rdx;
    rax_0 = var_5;
    rcx_0 = 4UL;
    rbx_2 = 0UL;
    rbx_0 = 0UL;
    rbp_0 = 0UL;
    cc_src2_0 = var_3;
    rbp_2 = 0UL;
    if ((uint64_t)(var_4 + '\xe0') != 0UL) {
        var_6 = rdx1_0 + 1UL;
        var_7 = *(unsigned char *)var_6;
        rdx1_0 = var_6;
        rdx1_1 = var_6;
        do {
            var_6 = rdx1_0 + 1UL;
            var_7 = *(unsigned char *)var_6;
            rdx1_0 = var_6;
            rdx1_1 = var_6;
        } while (var_7 != ' ');
        rax_0 = (uint64_t)var_7;
    }
    var_8 = rdx1_1 + ((uint64_t)((unsigned char)rax_0 + '\xd5') == 0UL);
    r8_0 = var_8;
    rdx1_2 = var_8;
    while (1U)
        {
            var_9 = (uint32_t)(uint64_t)*(unsigned char *)r8_0;
            r8_0 = r8_0 + 1UL;
            rcx_0 = 0UL;
            rax_1 = rcx_0;
            if ((uint64_t)var_9 == 0UL) {
                rax_1 = 4UL;
                if ((uint64_t)((var_9 + (-48)) & (-2)) <= 9UL) {
                    continue;
                }
                break;
            }
            if (rcx_0 == 0UL) {
                break;
            }
            while (1U)
                {
                    var_10 = (uint32_t)(uint64_t)*(unsigned char *)rdx1_2;
                    var_11 = rdx1_2 + 1UL;
                    rbx_1 = rbx_0;
                    rbx_2 = rbx_0;
                    rdx1_2 = var_11;
                    rbp_1 = rbp_0;
                    rbp_2 = rbp_0;
                    rax_1 = 0UL;
                    if ((uint64_t)var_10 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    if (rbx_0 <= 1844674407370955161UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_12 = (uint64_t)(var_10 + (-48));
                    var_13 = rbp_0 << 1UL;
                    var_14 = rbp_0 >> 61UL;
                    var_15 = rbp_0 * 10UL;
                    var_16 = rbp_0 >> 63UL;
                    var_17 = helper_cc_compute_c_wrapper(rbp_0 << 3UL, var_13, cc_src2_0, 17U);
                    var_18 = ((uint32_t)var_16 + (uint32_t)var_14) + (uint32_t)var_17;
                    var_19 = var_12 + var_15;
                    var_20 = helper_cc_compute_c_wrapper(var_19, var_15, var_17, 9U);
                    var_21 = (uint64_t)(var_18 + (uint32_t)var_20);
                    var_22 = rbx_0 * 10UL;
                    var_23 = var_21 + var_22;
                    var_24 = helper_cc_compute_c_wrapper(var_23, var_22, var_20, 9U);
                    rbx_1 = var_23;
                    rbx_0 = var_23;
                    rbp_0 = var_19;
                    cc_src2_0 = var_20;
                    rbp_1 = var_19;
                    if (var_24 == 0UL) {
                        continue;
                    }
                    loop_state_var = 0U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    *(uint64_t *)rdi = rbx_1;
                    *(uint64_t *)rsi = rbp_1;
                    return 1UL;
                }
                break;
              case 1U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    *(uint64_t *)rdi = rbx_2;
    *(uint64_t *)rsi = rbp_2;
    return rax_1;
}
