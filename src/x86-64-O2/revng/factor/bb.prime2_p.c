typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_49_ret_type;
struct indirect_placeholder_50_ret_type;
struct indirect_placeholder_51_ret_type;
struct indirect_placeholder_52_ret_type;
struct indirect_placeholder_53_ret_type;
struct indirect_placeholder_54_ret_type;
struct indirect_placeholder_49_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_50_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_51_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_52_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_53_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_54_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
    uint64_t field_8;
    uint64_t field_9;
    uint64_t field_10;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r12(void);
extern uint64_t init_r15(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_25(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern uint64_t init_rcx(void);
extern struct indirect_placeholder_49_ret_type indirect_placeholder_49(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_50_ret_type indirect_placeholder_50(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_51_ret_type indirect_placeholder_51(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_52_ret_type indirect_placeholder_52(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t helper_ctz_wrapper(uint64_t param_0);
extern struct indirect_placeholder_53_ret_type indirect_placeholder_53(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_54_ret_type indirect_placeholder_54(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
uint64_t bb_prime2_p(uint64_t rdi, uint64_t rsi) {
    uint64_t local_sp_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t local_sp_3;
    uint64_t var_135;
    uint64_t r12_2;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t local_sp_1;
    uint64_t var_84;
    uint64_t var_133;
    uint64_t r12_1;
    uint64_t var_134;
    uint64_t local_sp_5;
    uint64_t rbp_5;
    uint64_t r12_5;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t local_sp_4;
    uint64_t var_108;
    uint64_t var_109;
    uint64_t rax_1;
    uint64_t var_120;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t var_107;
    uint64_t rbx_5;
    uint64_t rbx_2;
    uint64_t r12_0;
    uint64_t r13_2;
    uint64_t rbx_0;
    uint64_t r13_5;
    uint64_t r15_0;
    uint64_t cc_src2_7;
    uint64_t rbp_2;
    uint64_t r13_0;
    uint64_t rbp_0;
    uint64_t cc_src2_0;
    uint64_t storemerge;
    uint64_t rbx_1;
    uint64_t r15_1;
    uint64_t r13_1;
    uint64_t rbp_1;
    uint64_t cc_src2_1;
    uint64_t var_110;
    bool var_111;
    uint64_t var_112;
    uint64_t var_113;
    uint64_t rdx_1;
    uint64_t rax_0;
    uint64_t cc_src2_3;
    uint64_t rdx_0;
    uint64_t rcx_0;
    uint64_t cc_src2_2;
    uint64_t var_114;
    uint64_t var_115;
    uint64_t var_116;
    uint64_t var_117;
    uint64_t var_118;
    uint64_t var_119;
    uint64_t var_18;
    uint64_t var_121;
    uint64_t var_122;
    uint64_t var_123;
    uint64_t var_124;
    uint64_t var_125;
    uint64_t var_126;
    uint64_t var_127;
    uint64_t *var_128;
    struct indirect_placeholder_49_ret_type var_129;
    uint64_t var_130;
    uint64_t var_131;
    uint64_t var_132;
    uint64_t local_sp_2;
    uint64_t cc_src2_4;
    unsigned char var_81;
    uint64_t r15_2;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t r12_3;
    uint64_t r15_5;
    uint64_t rbx_3;
    uint64_t r15_3;
    uint64_t r13_3;
    uint64_t rbp_3;
    uint64_t cc_src2_5;
    uint64_t var_87;
    uint64_t local_sp_6;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t r12_4;
    uint64_t rbx_4;
    uint64_t r15_4;
    uint64_t r13_4;
    uint64_t rbp_4;
    uint64_t rsi2_0;
    uint64_t cc_src2_6;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t var_98;
    struct indirect_placeholder_50_ret_type var_99;
    uint64_t var_100;
    bool var_101;
    uint64_t var_102;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t *var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    struct indirect_placeholder_51_ret_type var_78;
    uint64_t var_79;
    bool var_80;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t *var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t cc_src2_10;
    uint64_t local_sp_7;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t rdx_3;
    uint64_t rax_2;
    uint64_t cc_src2_9;
    uint64_t rdx_2;
    uint64_t rcx_1;
    uint64_t cc_src2_8;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t rax_3;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t *var_42;
    uint64_t var_43;
    uint64_t *var_44;
    uint64_t _pre_phi286;
    uint64_t _pre;
    uint64_t _pre285;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t _pre_phi;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t *var_54;
    struct indirect_placeholder_53_ret_type var_55;
    unsigned char *var_56;
    unsigned char var_57;
    struct indirect_placeholder_54_ret_type var_136;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r12();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_r13();
    var_5 = init_r14();
    var_6 = init_rbp();
    var_7 = init_rcx();
    var_8 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_1;
    *(uint64_t *)(var_0 + (-40L)) = var_6;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    r12_2 = var_1;
    rbx_2 = rdi;
    rbp_2 = 2UL;
    r13_3 = 1UL;
    rsi2_0 = 0UL;
    if (rdi == 0UL) {
        var_135 = var_0 + (-480L);
        *(uint64_t *)var_135 = 4213036UL;
        var_136 = indirect_placeholder_54(var_1, var_2, var_3, var_4, rsi, var_6, rsi);
        *(unsigned char *)(var_0 + (-417L)) = (unsigned char)var_136.field_0;
        local_sp_7 = var_135;
    } else {
        var_9 = (rsi == 0UL) + rdi;
        var_10 = (uint64_t *)(var_0 + (-440L));
        *var_10 = var_9;
        var_11 = rsi + (-1L);
        var_12 = (uint64_t *)(var_0 + (-448L));
        *var_12 = var_11;
        if (var_11 == 0UL) {
            var_18 = helper_ctz_wrapper(var_9);
            var_19 = (var_9 == 0UL) ? var_7 : var_18;
            *(uint64_t *)(var_0 + (-400L)) = 0UL;
            *(uint64_t *)(var_0 + (-408L)) = (var_9 >> (var_19 & 63UL));
            *(uint32_t *)(var_0 + (-416L)) = ((uint32_t)var_19 + 64U);
        } else {
            var_13 = *var_10;
            var_14 = helper_ctz_wrapper(var_11);
            var_15 = 0UL - var_14;
            *(uint32_t *)(var_0 + (-416L)) = (uint32_t)var_14;
            var_16 = var_13 << (var_15 & 63UL);
            var_17 = var_14 & 63UL;
            *(uint64_t *)(var_0 + (-408L)) = (var_16 | (var_11 >> var_17));
            *(uint64_t *)(var_0 + (-400L)) = (var_13 >> var_17);
        }
        var_20 = (uint64_t)*(unsigned char *)(((rsi >> 1UL) & 127UL) + 4265184UL);
        var_21 = (var_20 << 1UL) - ((var_20 * var_20) * rsi);
        var_22 = (var_21 << 1UL) - ((var_21 * var_21) * rsi);
        var_23 = (var_22 << 1UL) - ((var_22 * var_22) * rsi);
        var_24 = rdi + (-2L);
        var_25 = helper_cc_compute_c_wrapper(var_24, 2UL, var_8, 17U);
        var_26 = ((0UL - var_25) & 64UL) | 63UL;
        var_27 = helper_cc_compute_c_wrapper(var_24, 2UL, var_25, 17U);
        var_28 = (uint64_t)(unsigned char)var_27;
        var_29 = helper_cc_compute_c_wrapper(var_24, 2UL, var_25, 17U);
        r13_2 = var_23;
        rax_2 = 1UL - var_29;
        rdx_2 = var_28;
        rcx_1 = var_26;
        cc_src2_8 = var_29;
        while (1U)
            {
                var_30 = rax_2 << 1UL;
                var_31 = rdx_2 << 1UL;
                var_32 = var_30 | (rdx_2 >> 63UL);
                var_33 = rdi - var_32;
                var_34 = helper_cc_compute_c_wrapper(var_33, var_32, cc_src2_8, 17U);
                rax_3 = var_32;
                rdx_3 = var_31;
                cc_src2_9 = cc_src2_8;
                if (var_34 == 0UL) {
                    var_36 = var_31 - rsi;
                    var_37 = helper_cc_compute_c_wrapper(var_36, rsi, cc_src2_8, 17U);
                    rax_3 = (var_32 - rdi) - var_37;
                    rdx_3 = var_36;
                    cc_src2_9 = var_37;
                } else {
                    var_35 = helper_cc_compute_all_wrapper(var_33, var_32, cc_src2_8, 17U);
                    if (((var_35 & 64UL) == 0UL) || (var_31 < rsi)) {
                        var_36 = var_31 - rsi;
                        var_37 = helper_cc_compute_c_wrapper(var_36, rsi, cc_src2_8, 17U);
                        rax_3 = (var_32 - rdi) - var_37;
                        rdx_3 = var_36;
                        cc_src2_9 = var_37;
                    }
                }
                rax_2 = rax_3;
                rdx_2 = rdx_3;
                cc_src2_8 = cc_src2_9;
                if (rcx_1 == 0UL) {
                    break;
                }
                rcx_1 = rcx_1 + (-1L);
                continue;
            }
        *(uint64_t *)(var_0 + (-368L)) = rax_3;
        var_38 = var_0 + (-376L);
        *(uint64_t *)var_38 = rdx_3;
        var_39 = rdx_3 << 1UL;
        var_40 = helper_cc_compute_c_wrapper(var_39, rdx_3, cc_src2_9, 9U);
        var_41 = (rax_3 << 1UL) + var_40;
        var_42 = (uint64_t *)(var_0 + (-384L));
        *var_42 = var_41;
        var_43 = var_0 + (-392L);
        var_44 = (uint64_t *)var_43;
        *var_44 = var_39;
        cc_src2_10 = var_40;
        if (var_41 > rdi) {
            _pre = var_39 - rsi;
            _pre285 = var_41 - rdi;
            _pre_phi286 = _pre285;
            _pre_phi = _pre;
            var_49 = helper_cc_compute_c_wrapper(_pre_phi, rsi, var_40, 17U);
            *var_42 = (_pre_phi286 - var_49);
            *var_44 = _pre_phi;
            cc_src2_10 = var_49;
        } else {
            var_45 = var_41 - rdi;
            var_46 = helper_cc_compute_all_wrapper(var_45, rdi, var_40, 17U);
            _pre_phi286 = var_45;
            var_47 = var_39 - rsi;
            var_48 = helper_cc_compute_c_wrapper(var_47, rsi, var_40, 17U);
            _pre_phi = var_47;
            if ((var_46 & 64UL) != 0UL & var_48 == 0UL) {
                var_49 = helper_cc_compute_c_wrapper(_pre_phi, rsi, var_40, 17U);
                *var_42 = (_pre_phi286 - var_49);
                *var_44 = _pre_phi;
                cc_src2_10 = var_49;
            }
        }
        var_50 = (uint64_t)*(uint32_t *)(var_0 + (-416L));
        var_51 = var_0 + (-408L);
        var_52 = var_0 + (-360L);
        *(uint64_t *)var_52 = rsi;
        *(uint64_t *)(var_0 + (-352L)) = rdi;
        var_53 = var_0 + (-480L);
        var_54 = (uint64_t *)var_53;
        *var_54 = 4212051UL;
        var_55 = indirect_placeholder_53(var_50, var_43, var_52, var_51, var_23, var_38);
        var_56 = (unsigned char *)(var_0 + (-417L));
        var_57 = (unsigned char)var_55.field_0;
        *var_56 = var_57;
        cc_src2_4 = cc_src2_10;
        local_sp_7 = var_53;
        if ((uint64_t)var_57 != 0UL) {
            var_58 = *(uint64_t *)(var_0 + (-456L));
            var_59 = *var_12;
            var_60 = var_0 + (-320L);
            var_61 = var_0 + (-488L);
            var_62 = (uint64_t *)var_61;
            *var_62 = 4212092UL;
            indirect_placeholder_52(var_60, var_59, var_58);
            var_63 = var_59 << 63UL;
            var_64 = var_58 >> 1UL;
            *var_62 = 4276832UL;
            var_65 = var_59 >> 1UL;
            *var_54 = (var_63 | var_64);
            *(uint64_t *)(var_0 + (-472L)) = var_65;
            local_sp_2 = var_61;
            loop_state_var = 2U;
            while (1U)
                {
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 1U:
                        {
                            var_94 = local_sp_6 + 96UL;
                            var_95 = local_sp_6 + 112UL;
                            var_96 = local_sp_6 + 128UL;
                            var_97 = local_sp_6 + 80UL;
                            var_98 = local_sp_6 + (-8L);
                            *(uint64_t *)var_98 = 4212367UL;
                            var_99 = indirect_placeholder_50(rbp_5, var_96, r15_5, var_95, var_97, var_94);
                            var_100 = var_99.field_0;
                            *(uint64_t *)(local_sp_6 + 136UL) = var_100;
                            var_101 = (var_100 == *(uint64_t *)(local_sp_6 + 88UL));
                            var_102 = (uint64_t)*(unsigned char *)(local_sp_6 + 402UL);
                            local_sp_0 = var_98;
                            local_sp_1 = var_98;
                            r12_1 = r12_5;
                            local_sp_4 = var_98;
                            r12_0 = r12_5;
                            r15_0 = r15_5;
                            r13_0 = rbp_5;
                            cc_src2_0 = cc_src2_7;
                            r15_1 = r15_5;
                            r13_1 = rbp_5;
                            cc_src2_1 = cc_src2_7;
                            r12_3 = r12_5;
                            rbx_3 = rbx_5;
                            r15_3 = r15_5;
                            rbp_3 = rbp_5;
                            cc_src2_5 = cc_src2_7;
                            local_sp_7 = var_98;
                            if (!var_101) {
                                if ((uint64_t)(uint32_t)r13_5 < var_102) {
                                    switch_state_var = 1;
                                    break;
                                }
                                r13_3 = r13_5 + 1UL;
                                loop_state_var = 2U;
                                continue;
                            }
                            var_103 = *(uint64_t *)(local_sp_6 + 144UL);
                            var_104 = *(uint64_t *)var_94;
                            if ((uint64_t)(uint32_t)r13_5 >= var_102) {
                                var_105 = *(uint64_t *)(local_sp_6 + 32UL);
                                var_106 = *(uint64_t *)(local_sp_6 + 40UL);
                                var_107 = (var_103 != var_104);
                                rbx_0 = var_106;
                                rbp_0 = var_105;
                                storemerge = var_107;
                                local_sp_1 = local_sp_0;
                                r12_1 = r12_0;
                                rbx_1 = rbx_0;
                                r15_1 = r15_0;
                                r13_1 = r13_0;
                                rbp_1 = rbp_0;
                                cc_src2_1 = cc_src2_0;
                                local_sp_7 = local_sp_0;
                                if ((uint64_t)(unsigned char)storemerge == 0UL) {
                                    switch_state_var = 1;
                                    break;
                                }
                            }
                            if (var_103 != var_104) {
                                r13_3 = r13_5 + 1UL;
                                loop_state_var = 2U;
                                continue;
                            }
                            var_108 = *(uint64_t *)(local_sp_6 + 40UL);
                            var_109 = *(uint64_t *)(local_sp_6 + 32UL);
                            rbx_1 = var_108;
                            rbp_1 = var_109;
                        }
                        break;
                      case 0U:
                        {
                            local_sp_3 = local_sp_2;
                            r12_1 = r12_2;
                            r12_0 = r12_2;
                            rbx_0 = rbx_2;
                            r13_0 = r13_2;
                            rbp_0 = rbp_2;
                            cc_src2_0 = cc_src2_4;
                            rbx_1 = rbx_2;
                            r13_1 = r13_2;
                            rbp_1 = rbp_2;
                            cc_src2_1 = cc_src2_4;
                            rbp_3 = r13_2;
                            cc_src2_5 = cc_src2_4;
                            local_sp_7 = local_sp_2;
                            if (*(uint64_t *)(local_sp_2 + 168UL) != 0UL) {
                                var_66 = *(uint64_t *)(local_sp_2 + 160UL);
                                var_67 = local_sp_2 + 96UL;
                                var_68 = local_sp_2 + 80UL;
                                var_69 = local_sp_2 + 152UL;
                                var_70 = (uint64_t *)(local_sp_2 + 136UL);
                                *var_70 = 0UL;
                                var_71 = (uint64_t)*(unsigned char *)(((var_66 >> 1UL) & 127UL) + 4265184UL);
                                var_72 = (var_71 << 1UL) - (var_66 * (var_71 * var_71));
                                var_73 = (var_72 << 1UL) - (var_66 * (var_72 * var_72));
                                var_74 = local_sp_2 + 112UL;
                                var_75 = ((var_73 << 1UL) - (var_66 * (var_73 * var_73))) * *(uint64_t *)(local_sp_2 + 24UL);
                                var_76 = local_sp_2 + 128UL;
                                *(uint64_t *)var_76 = var_75;
                                var_77 = local_sp_2 + (-8L);
                                *(uint64_t *)var_77 = 4212932UL;
                                var_78 = indirect_placeholder_51(r13_2, var_76, var_69, var_74, var_68, var_67);
                                var_79 = var_78.field_0;
                                *var_70 = var_79;
                                var_80 = (var_79 == *(uint64_t *)(local_sp_2 + 88UL));
                                var_81 = *(unsigned char *)(local_sp_2 + 402UL);
                                local_sp_0 = var_77;
                                local_sp_3 = var_77;
                                local_sp_1 = var_77;
                                r15_0 = var_69;
                                r15_1 = var_69;
                                r15_2 = var_69;
                                local_sp_7 = var_77;
                                if (!var_80) {
                                    if (var_81 == '\x00') {
                                        switch_state_var = 1;
                                        break;
                                    }
                                }
                                var_82 = *(uint64_t *)(local_sp_2 + 144UL);
                                var_83 = *(uint64_t *)var_67;
                                if (var_81 != '\x00') {
                                    var_84 = (var_83 & (-256L)) | (var_82 != var_83);
                                    storemerge = var_84;
                                    local_sp_1 = local_sp_0;
                                    r12_1 = r12_0;
                                    rbx_1 = rbx_0;
                                    r15_1 = r15_0;
                                    r13_1 = r13_0;
                                    rbp_1 = rbp_0;
                                    cc_src2_1 = cc_src2_0;
                                    local_sp_7 = local_sp_0;
                                    if ((uint64_t)(unsigned char)storemerge == 0UL) {
                                        switch_state_var = 1;
                                        break;
                                    }
                                    var_110 = rbp_1 + (uint64_t)**(unsigned char **)local_sp_1;
                                    var_111 = (var_110 < rbx_1);
                                    var_112 = var_111 ? 0UL : var_110;
                                    var_113 = helper_cc_compute_c_wrapper(var_110 - rbx_1, rbx_1, cc_src2_1, 17U);
                                    r12_2 = r12_1;
                                    rbx_2 = rbx_1;
                                    r13_2 = r13_1;
                                    rbp_2 = var_110;
                                    rax_0 = var_111 ? var_110 : 0UL;
                                    rdx_0 = var_112;
                                    rcx_0 = ((0UL - var_113) & (-64L)) + 127UL;
                                    cc_src2_2 = var_113;
                                    r12_4 = r12_1;
                                    rbx_4 = rbx_1;
                                    r15_4 = r15_1;
                                    r13_4 = r13_1;
                                    rbp_4 = var_110;
                                    while (1U)
                                        {
                                            var_114 = rax_0 << 1UL;
                                            var_115 = rdx_0 << 1UL;
                                            var_116 = var_114 | (rdx_0 >> 63UL);
                                            var_117 = rbx_1 - var_116;
                                            var_118 = helper_cc_compute_c_wrapper(var_117, var_116, cc_src2_2, 17U);
                                            rax_1 = var_116;
                                            rdx_1 = var_115;
                                            cc_src2_3 = cc_src2_2;
                                            if (var_118 == 0UL) {
                                                var_120 = var_115 - rsi;
                                                var_121 = helper_cc_compute_c_wrapper(var_120, rsi, cc_src2_2, 17U);
                                                rax_1 = (var_116 - rbx_1) - var_121;
                                                rdx_1 = var_120;
                                                cc_src2_3 = var_121;
                                            } else {
                                                var_119 = helper_cc_compute_all_wrapper(var_117, var_116, cc_src2_2, 17U);
                                                if (((var_119 & 64UL) == 0UL) || (var_115 < rsi)) {
                                                    var_120 = var_115 - rsi;
                                                    var_121 = helper_cc_compute_c_wrapper(var_120, rsi, cc_src2_2, 17U);
                                                    rax_1 = (var_116 - rbx_1) - var_121;
                                                    rdx_1 = var_120;
                                                    cc_src2_3 = var_121;
                                                }
                                            }
                                            rax_0 = rax_1;
                                            rdx_0 = rdx_1;
                                            cc_src2_2 = cc_src2_3;
                                            cc_src2_4 = cc_src2_3;
                                            cc_src2_6 = cc_src2_3;
                                            if (rcx_0 == 0UL) {
                                                break;
                                            }
                                            rcx_0 = rcx_0 + (-1L);
                                            continue;
                                        }
                                    var_122 = (uint64_t)*(uint32_t *)(local_sp_1 + 56UL);
                                    var_123 = local_sp_1 + 80UL;
                                    *(uint64_t *)var_123 = rdx_1;
                                    var_124 = local_sp_1 + 96UL;
                                    var_125 = local_sp_1 + 64UL;
                                    *(uint64_t *)(local_sp_1 + 88UL) = rax_1;
                                    var_126 = local_sp_1 + 112UL;
                                    var_127 = local_sp_1 + (-8L);
                                    var_128 = (uint64_t *)var_127;
                                    *var_128 = 4212679UL;
                                    var_129 = indirect_placeholder_49(var_122, var_123, var_126, var_125, r13_1, var_124);
                                    var_130 = var_129.field_1;
                                    var_131 = var_129.field_2;
                                    var_132 = var_129.field_3;
                                    local_sp_2 = var_127;
                                    local_sp_7 = var_127;
                                    if ((uint64_t)(unsigned char)var_129.field_0 != 0UL) {
                                        *(unsigned char *)(local_sp_1 + 55UL) = (unsigned char)'\x00';
                                        switch_state_var = 1;
                                        break;
                                    }
                                    var_133 = *var_128;
                                    *var_128 = (var_133 + 1UL);
                                    if (var_133 != 4277499UL) {
                                        loop_state_var = 0U;
                                        continue;
                                    }
                                    *(uint64_t *)(local_sp_1 + (-16L)) = 4212722UL;
                                    indirect_placeholder_25(0UL, var_130, 4264384UL, 0UL, var_131, 0UL, var_132);
                                    var_134 = local_sp_1 + (-24L);
                                    *(uint64_t *)var_134 = 4212727UL;
                                    indirect_placeholder();
                                    local_sp_5 = var_134;
                                    *(uint64_t *)(local_sp_5 + 128UL) = rsi2_0;
                                    *(uint64_t *)(local_sp_5 + 136UL) = 0UL;
                                    rbp_5 = rbp_4;
                                    r12_5 = r12_4;
                                    rbx_5 = rbx_4;
                                    r13_5 = r13_4;
                                    cc_src2_7 = cc_src2_6;
                                    r15_5 = r15_4;
                                    local_sp_6 = local_sp_5;
                                    loop_state_var = 1U;
                                    continue;
                                }
                                if (var_82 != var_83) {
                                    var_110 = rbp_1 + (uint64_t)**(unsigned char **)local_sp_1;
                                    var_111 = (var_110 < rbx_1);
                                    var_112 = var_111 ? 0UL : var_110;
                                    var_113 = helper_cc_compute_c_wrapper(var_110 - rbx_1, rbx_1, cc_src2_1, 17U);
                                    r12_2 = r12_1;
                                    rbx_2 = rbx_1;
                                    r13_2 = r13_1;
                                    rbp_2 = var_110;
                                    rax_0 = var_111 ? var_110 : 0UL;
                                    rdx_0 = var_112;
                                    rcx_0 = ((0UL - var_113) & (-64L)) + 127UL;
                                    cc_src2_2 = var_113;
                                    r12_4 = r12_1;
                                    rbx_4 = rbx_1;
                                    r15_4 = r15_1;
                                    r13_4 = r13_1;
                                    rbp_4 = var_110;
                                    while (1U)
                                        {
                                            var_114 = rax_0 << 1UL;
                                            var_115 = rdx_0 << 1UL;
                                            var_116 = var_114 | (rdx_0 >> 63UL);
                                            var_117 = rbx_1 - var_116;
                                            var_118 = helper_cc_compute_c_wrapper(var_117, var_116, cc_src2_2, 17U);
                                            rax_1 = var_116;
                                            rdx_1 = var_115;
                                            cc_src2_3 = cc_src2_2;
                                            if (var_118 == 0UL) {
                                                var_120 = var_115 - rsi;
                                                var_121 = helper_cc_compute_c_wrapper(var_120, rsi, cc_src2_2, 17U);
                                                rax_1 = (var_116 - rbx_1) - var_121;
                                                rdx_1 = var_120;
                                                cc_src2_3 = var_121;
                                            } else {
                                                var_119 = helper_cc_compute_all_wrapper(var_117, var_116, cc_src2_2, 17U);
                                                if (((var_119 & 64UL) == 0UL) || (var_115 < rsi)) {
                                                    var_120 = var_115 - rsi;
                                                    var_121 = helper_cc_compute_c_wrapper(var_120, rsi, cc_src2_2, 17U);
                                                    rax_1 = (var_116 - rbx_1) - var_121;
                                                    rdx_1 = var_120;
                                                    cc_src2_3 = var_121;
                                                }
                                            }
                                            rax_0 = rax_1;
                                            rdx_0 = rdx_1;
                                            cc_src2_2 = cc_src2_3;
                                            cc_src2_4 = cc_src2_3;
                                            cc_src2_6 = cc_src2_3;
                                            if (rcx_0 == 0UL) {
                                                break;
                                            }
                                            rcx_0 = rcx_0 + (-1L);
                                            continue;
                                        }
                                    var_122 = (uint64_t)*(uint32_t *)(local_sp_1 + 56UL);
                                    var_123 = local_sp_1 + 80UL;
                                    *(uint64_t *)var_123 = rdx_1;
                                    var_124 = local_sp_1 + 96UL;
                                    var_125 = local_sp_1 + 64UL;
                                    *(uint64_t *)(local_sp_1 + 88UL) = rax_1;
                                    var_126 = local_sp_1 + 112UL;
                                    var_127 = local_sp_1 + (-8L);
                                    var_128 = (uint64_t *)var_127;
                                    *var_128 = 4212679UL;
                                    var_129 = indirect_placeholder_49(var_122, var_123, var_126, var_125, r13_1, var_124);
                                    var_130 = var_129.field_1;
                                    var_131 = var_129.field_2;
                                    var_132 = var_129.field_3;
                                    local_sp_2 = var_127;
                                    local_sp_7 = var_127;
                                    if ((uint64_t)(unsigned char)var_129.field_0 != 0UL) {
                                        *(unsigned char *)(local_sp_1 + 55UL) = (unsigned char)'\x00';
                                        switch_state_var = 1;
                                        break;
                                    }
                                    var_133 = *var_128;
                                    *var_128 = (var_133 + 1UL);
                                    if (var_133 != 4277499UL) {
                                        loop_state_var = 0U;
                                        continue;
                                    }
                                    *(uint64_t *)(local_sp_1 + (-16L)) = 4212722UL;
                                    indirect_placeholder_25(0UL, var_130, 4264384UL, 0UL, var_131, 0UL, var_132);
                                    var_134 = local_sp_1 + (-24L);
                                    *(uint64_t *)var_134 = 4212727UL;
                                    indirect_placeholder();
                                    local_sp_5 = var_134;
                                    *(uint64_t *)(local_sp_5 + 128UL) = rsi2_0;
                                    *(uint64_t *)(local_sp_5 + 136UL) = 0UL;
                                    rbp_5 = rbp_4;
                                    r12_5 = r12_4;
                                    rbx_5 = rbx_4;
                                    r13_5 = r13_4;
                                    cc_src2_7 = cc_src2_6;
                                    r15_5 = r15_4;
                                    local_sp_6 = local_sp_5;
                                    loop_state_var = 1U;
                                    continue;
                                }
                            }
                            if (*(unsigned char *)(local_sp_2 + 410UL) != '\x00') {
                                switch_state_var = 1;
                                break;
                            }
                            r15_2 = local_sp_2 + 152UL;
                        }
                        break;
                      case 2U:
                        {
                            var_87 = *(uint64_t *)(((r13_3 << 3UL) + local_sp_4) + 168UL);
                            local_sp_5 = local_sp_4;
                            rbp_5 = rbp_3;
                            r12_5 = r12_3;
                            rbx_5 = rbx_3;
                            r13_5 = r13_3;
                            cc_src2_7 = cc_src2_5;
                            r15_5 = r15_3;
                            local_sp_6 = local_sp_4;
                            r12_4 = r12_3;
                            rbx_4 = rbx_3;
                            r15_4 = r15_3;
                            r13_4 = r13_3;
                            rbp_4 = rbp_3;
                            cc_src2_6 = cc_src2_5;
                            if (var_87 == 2UL) {
                                *(uint64_t *)(local_sp_4 + 128UL) = *(uint64_t *)(local_sp_4 + 8UL);
                                *(uint64_t *)(local_sp_4 + 136UL) = *(uint64_t *)(local_sp_4 + 16UL);
                            } else {
                                var_88 = (uint64_t)*(unsigned char *)(((var_87 >> 1UL) & 127UL) + 4265184UL);
                                var_89 = (var_88 << 1UL) - (var_87 * (var_88 * var_88));
                                var_90 = (var_89 << 1UL) - (var_87 * (var_89 * var_89));
                                var_91 = (var_90 << 1UL) - (var_87 * (var_90 * var_90));
                                var_92 = var_91 * rbx_3;
                                var_93 = helper_cc_compute_c_wrapper(r12_3 - var_87, var_87, cc_src2_5, 17U);
                                rsi2_0 = var_92;
                                if (var_93 != 0UL) {
                                    *(uint64_t *)(local_sp_5 + 128UL) = rsi2_0;
                                    *(uint64_t *)(local_sp_5 + 136UL) = 0UL;
                                    rbp_5 = rbp_4;
                                    r12_5 = r12_4;
                                    rbx_5 = rbx_4;
                                    r13_5 = r13_4;
                                    cc_src2_7 = cc_src2_6;
                                    r15_5 = r15_4;
                                    local_sp_6 = local_sp_5;
                                    loop_state_var = 1U;
                                    continue;
                                }
                                *(uint64_t *)(local_sp_4 + 128UL) = var_92;
                                *(uint64_t *)(local_sp_4 + 136UL) = ((r12_3 - (uint64_t)(((unsigned __int128)var_87 * (unsigned __int128)var_92) >> 64ULL)) * var_91);
                            }
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
        }
    }
    return (uint64_t)*(unsigned char *)(local_sp_7 + 63UL);
}
