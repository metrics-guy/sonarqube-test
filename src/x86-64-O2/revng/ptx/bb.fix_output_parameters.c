typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_73_ret_type;
struct indirect_placeholder_73_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint32_t init_state_0x82fc(void);
extern uint64_t indirect_placeholder_4(uint64_t param_0);
extern struct indirect_placeholder_73_ret_type indirect_placeholder_73(uint64_t param_0);
void bb_fix_output_parameters(void) {
    uint64_t rcx_2;
    uint64_t rdx_0;
    uint64_t rax_1;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint32_t var_4;
    uint64_t var_5;
    uint64_t local_sp_2;
    uint64_t var_8;
    uint64_t rbp_0;
    uint64_t local_sp_0;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t local_sp_7;
    uint64_t var_12;
    uint64_t var_13;
    uint32_t var_35;
    uint64_t local_sp_5;
    uint64_t rdi_0;
    uint64_t local_sp_1;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    unsigned char var_39;
    uint64_t var_40;
    uint64_t local_sp_4_ph;
    uint64_t local_sp_3;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t local_sp_4;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t rdi_1;
    uint64_t rcx_0;
    uint64_t rcx_1;
    unsigned char var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint32_t var_6;
    uint64_t local_sp_6;
    uint64_t var_14;
    uint64_t var_15;
    struct indirect_placeholder_73_ret_type var_16;
    unsigned char var_17;
    uint64_t var_7;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r12();
    var_2 = init_rbx();
    var_3 = init_rbp();
    var_4 = init_state_0x82fc();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    var_5 = var_0 + (-56L);
    rcx_2 = 0UL;
    rdx_0 = 4325555UL;
    rax_1 = 128UL;
    local_sp_2 = var_5;
    rbp_0 = 0UL;
    local_sp_7 = var_5;
    var_13 = 0UL;
    rdi_0 = 36UL;
    rcx_0 = 18446744073709551615UL;
    rcx_1 = 0UL;
    local_sp_6 = var_5;
    if (*(unsigned char *)4365194UL == '\x00') {
        var_6 = *(uint32_t *)4363800UL;
        *(uint64_t *)4363864UL = 0UL;
        if (var_6 == 0U) {
            var_11 = (uint64_t)*(uint32_t *)4363800UL;
            var_12 = rbp_0 + 1UL;
            var_13 = var_10;
            rbp_0 = var_12;
            do {
                var_7 = local_sp_7 + (-8L);
                *(uint64_t *)var_7 = 4208015UL;
                indirect_placeholder();
                local_sp_0 = var_7;
                if (*(uint64_t *)((rbp_0 << 3UL) + *(uint64_t *)4363784UL) == 0UL) {
                    var_8 = local_sp_7 + (-16L);
                    *(uint64_t *)var_8 = 4208049UL;
                    indirect_placeholder();
                    local_sp_0 = var_8;
                }
                var_9 = *(uint64_t *)4363864UL;
                var_10 = var_9;
                local_sp_6 = local_sp_0;
                local_sp_7 = local_sp_0;
                if ((long)var_9 < (long)0UL) {
                    *(uint64_t *)4363864UL = 0UL;
                    var_10 = 0UL;
                }
                var_11 = (uint64_t)*(uint32_t *)4363800UL;
                var_12 = rbp_0 + 1UL;
                var_13 = var_10;
                rbp_0 = var_12;
            } while (var_12 >= var_11);
        }
        var_14 = var_13 + 2UL;
        *(uint64_t *)4363864UL = (var_13 + 1UL);
        var_15 = local_sp_6 + (-8L);
        *(uint64_t *)var_15 = 4207906UL;
        var_16 = indirect_placeholder_73(var_14);
        var_17 = *(unsigned char *)4365194UL;
        *(uint64_t *)4363296UL = var_16.field_0;
        local_sp_2 = var_15;
        local_sp_3 = var_15;
        if (var_17 == '\x00') {
            local_sp_3 = local_sp_2;
            local_sp_4_ph = local_sp_2;
            if (*(unsigned char *)4365193UL == '\x00') {
                var_19 = *(uint64_t *)4363032UL;
                local_sp_4 = local_sp_4_ph;
            } else {
                local_sp_4_ph = local_sp_3;
                local_sp_4 = local_sp_3;
                if (*(unsigned char *)4365192UL == '\x00') {
                    var_18 = *(uint64_t *)4363032UL - (*(uint64_t *)4363024UL + *(uint64_t *)4363864UL);
                    *(uint64_t *)4363032UL = var_18;
                    var_19 = var_18;
                } else {
                    var_19 = *(uint64_t *)4363032UL;
                    local_sp_4 = local_sp_4_ph;
                }
            }
        } else {
            local_sp_4_ph = local_sp_3;
            local_sp_4 = local_sp_3;
            if (*(unsigned char *)4365192UL == '\x00') {
                var_18 = *(uint64_t *)4363032UL - (*(uint64_t *)4363024UL + *(uint64_t *)4363864UL);
                *(uint64_t *)4363032UL = var_18;
                var_19 = var_18;
            } else {
                var_19 = *(uint64_t *)4363032UL;
                local_sp_4 = local_sp_4_ph;
            }
        }
    } else {
        local_sp_3 = local_sp_2;
        local_sp_4_ph = local_sp_2;
        if (*(unsigned char *)4365193UL == '\x00') {
            var_19 = *(uint64_t *)4363032UL;
            local_sp_4 = local_sp_4_ph;
        } else {
            local_sp_4_ph = local_sp_3;
            local_sp_4 = local_sp_3;
            if (*(unsigned char *)4365192UL == '\x00') {
                var_18 = *(uint64_t *)4363032UL - (*(uint64_t *)4363024UL + *(uint64_t *)4363864UL);
                *(uint64_t *)4363032UL = var_18;
                var_19 = var_18;
            } else {
                var_19 = *(uint64_t *)4363032UL;
                local_sp_4 = local_sp_4_ph;
            }
        }
    }
    var_20 = var_19;
    local_sp_5 = local_sp_4;
    if ((long)var_19 > (long)18446744073709551615UL) {
        *(uint64_t *)4363032UL = 0UL;
        var_20 = 0UL;
    }
    var_21 = *(uint64_t *)4363016UL;
    var_22 = (uint64_t)((long)((var_20 >> 63UL) + var_20) >> (long)1UL);
    var_23 = var_22 - *(uint64_t *)4363024UL;
    *(uint64_t *)4363464UL = var_22;
    *(uint64_t *)4363456UL = var_23;
    *(uint64_t *)4363448UL = var_22;
    rdi_1 = var_21;
    if (var_21 == 0UL) {
        *(uint64_t *)4363016UL = 0UL;
        var_28 = *(uint64_t *)4363440UL;
    } else {
        var_24 = (uint64_t)var_4;
        while (rcx_0 != 0UL)
            {
                var_25 = *(unsigned char *)rdi_1;
                var_26 = rcx_0 + (-1L);
                rcx_0 = var_26;
                rcx_1 = var_26;
                if (var_25 == '\x00') {
                    break;
                }
                rdi_1 = rdi_1 + var_24;
            }
        var_27 = 18446744073709551614UL - (-2L);
        *(uint64_t *)4363440UL = var_27;
        var_28 = var_27;
    }
    var_29 = var_28 << 1UL;
    if (*(unsigned char *)4363040UL == '\x00') {
        *(uint64_t *)4363448UL = (var_22 - (var_29 | 1UL));
    } else {
        var_30 = var_23 - var_29;
        var_31 = ((long)var_30 > (long)0UL) ? var_30 : 0UL;
        *(uint64_t *)4363448UL = (var_22 - var_29);
        *(uint64_t *)4363456UL = var_31;
    }
    var_32 = (uint64_t)(uint32_t)rcx_2;
    var_33 = local_sp_5 + (-8L);
    *(uint64_t *)var_33 = 4207719UL;
    var_34 = indirect_placeholder_4(var_32);
    *(unsigned char *)(rcx_2 + 4363488UL) = ((uint64_t)(uint32_t)var_34 != 0UL);
    local_sp_1 = var_33;
    local_sp_5 = var_33;
    while (rcx_2 != 255UL)
        {
            rcx_2 = rcx_2 + 1UL;
            var_32 = (uint64_t)(uint32_t)rcx_2;
            var_33 = local_sp_5 + (-8L);
            *(uint64_t *)var_33 = 4207719UL;
            var_34 = indirect_placeholder_4(var_32);
            *(unsigned char *)(rcx_2 + 4363488UL) = ((uint64_t)(uint32_t)var_34 != 0UL);
            local_sp_1 = var_33;
            local_sp_5 = var_33;
        }
    var_35 = *(uint32_t *)4365188UL;
    *(unsigned char *)4363500UL = (unsigned char)'\x01';
    if ((uint64_t)(var_35 + (-2)) != 0UL) {
        if ((uint64_t)(var_35 + (-3)) != 0UL) {
            var_36 = local_sp_1 + (-8L);
            *(uint64_t *)var_36 = 4207789UL;
            var_37 = indirect_placeholder_4(rdi_0);
            var_38 = rdx_0 + 1UL;
            var_39 = *(unsigned char *)var_38;
            var_40 = (uint64_t)var_39;
            *(unsigned char *)((uint64_t)(unsigned char)var_37 + 4363488UL) = (unsigned char)'\x01';
            rdx_0 = var_38;
            local_sp_1 = var_36;
            while ((uint64_t)var_39 != 0UL)
                {
                    rdi_0 = (uint64_t)(uint32_t)var_40;
                    var_36 = local_sp_1 + (-8L);
                    *(uint64_t *)var_36 = 4207789UL;
                    var_37 = indirect_placeholder_4(rdi_0);
                    var_38 = rdx_0 + 1UL;
                    var_39 = *(unsigned char *)var_38;
                    var_40 = (uint64_t)var_39;
                    *(unsigned char *)((uint64_t)(unsigned char)var_37 + 4363488UL) = (unsigned char)'\x01';
                    rdx_0 = var_38;
                    local_sp_1 = var_36;
                }
            *(unsigned char *)(rax_1 + 4363488UL) = (*(unsigned char *)(rax_1 + 4326304UL) != '\x00');
            while (rax_1 != 255UL)
                {
                    rax_1 = rax_1 + 1UL;
                    *(unsigned char *)(rax_1 + 4363488UL) = (*(unsigned char *)(rax_1 + 4326304UL) != '\x00');
                }
        }
    }
    *(unsigned char *)4363522UL = (unsigned char)'\x01';
    return;
}
