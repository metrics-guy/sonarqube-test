typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rax(void);
extern void indirect_placeholder_17(uint64_t param_0);
extern uint32_t init_state_0x82fc(void);
void bb_initialize_regex(void) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint32_t var_3;
    unsigned char var_4;
    uint64_t var_5;
    uint64_t local_sp_1;
    uint64_t local_sp_0;
    uint64_t rbx_0;
    unsigned char var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t rax_0;
    uint64_t var_9;
    uint64_t rdi_0;
    uint64_t rcx_0;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbx();
    var_3 = init_state_0x82fc();
    var_4 = *(unsigned char *)4365184UL;
    var_5 = var_0 + (-8L);
    *(uint64_t *)var_5 = var_2;
    local_sp_1 = var_5;
    local_sp_0 = var_5;
    rbx_0 = 0UL;
    rax_0 = 0UL;
    rdi_0 = 4363904UL;
    rcx_0 = 32UL;
    if (var_4 != '\x00') {
        var_6 = (unsigned char)var_1;
        var_7 = local_sp_0 + (-8L);
        *(uint64_t *)var_7 = 4207243UL;
        indirect_placeholder();
        *(unsigned char *)(rbx_0 + 4364224UL) = var_6;
        local_sp_0 = var_7;
        local_sp_1 = var_7;
        while (rbx_0 != 255UL)
            {
                rbx_0 = rbx_0 + 1UL;
                var_7 = local_sp_0 + (-8L);
                *(uint64_t *)var_7 = 4207243UL;
                indirect_placeholder();
                *(unsigned char *)(rbx_0 + 4364224UL) = var_6;
                local_sp_0 = var_7;
                local_sp_1 = var_7;
            }
    }
    var_8 = *(uint64_t *)4364832UL;
    if (var_8 == 0UL) {
        if (*(unsigned char *)4363040UL == '\x00') {
            *(uint64_t *)4364832UL = 4332710UL;
        } else {
            if (*(unsigned char *)4365193UL == '\x00') {
                *(uint64_t *)4364832UL = 4321456UL;
            } else {
                *(uint64_t *)4364832UL = 4332710UL;
            }
        }
        *(uint64_t *)(local_sp_1 + (-8L)) = 4207285UL;
        indirect_placeholder_17(4364832UL);
    } else {
        if (*(unsigned char *)var_8 == '\x00') {
            *(uint64_t *)4364832UL = 0UL;
        } else {
            *(uint64_t *)(local_sp_1 + (-8L)) = 4207285UL;
            indirect_placeholder_17(4364832UL);
        }
    }
    if (*(uint64_t *)4364480UL == 0UL) {
        indirect_placeholder();
        return;
    }
    if (*(uint64_t *)4365176UL == 0UL) {
        return;
    }
    if (*(unsigned char *)4363040UL == '\x00') {
        var_9 = (uint64_t)var_3 << 3UL;
        while (rcx_0 != 0UL)
            {
                *(uint64_t *)rdi_0 = 72340172838076673UL;
                rdi_0 = var_9 + rdi_0;
                rcx_0 = rcx_0 + (-1L);
            }
        *(unsigned char *)4363936UL = (unsigned char)'\x00';
        *(uint16_t *)4363913UL = (uint16_t)(unsigned short)0U;
        return;
    }
    *(unsigned char *)(rax_0 + 4363904UL) = ((uint64_t)((((uint32_t)rax_0 | 32U) + (-97)) & (-2)) < 26UL);
    while (rax_0 != 255UL)
        {
            rax_0 = rax_0 + 1UL;
            *(unsigned char *)(rax_0 + 4363904UL) = ((uint64_t)((((uint32_t)rax_0 | 32U) + (-97)) & (-2)) < 26UL);
        }
    return;
}
