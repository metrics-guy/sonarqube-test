typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_output_one_dumb_line_ret_type;
struct indirect_placeholder_41_ret_type;
struct indirect_placeholder_40_ret_type;
struct indirect_placeholder_43_ret_type;
struct indirect_placeholder_42_ret_type;
struct indirect_placeholder_44_ret_type;
struct indirect_placeholder_45_ret_type;
struct indirect_placeholder_46_ret_type;
struct bb_output_one_dumb_line_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_41_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_40_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_43_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_42_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_44_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_45_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_46_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct indirect_placeholder_41_ret_type indirect_placeholder_41(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_40_ret_type indirect_placeholder_40(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_43_ret_type indirect_placeholder_43(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_42_ret_type indirect_placeholder_42(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_44_ret_type indirect_placeholder_44(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_45_ret_type indirect_placeholder_45(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_46_ret_type indirect_placeholder_46(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
struct bb_output_one_dumb_line_ret_type bb_output_one_dumb_line(uint64_t r12, uint64_t rbx, uint64_t rbp) {
    struct indirect_placeholder_40_ret_type var_90;
    struct indirect_placeholder_41_ret_type var_80;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t r124_4;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t rdi_0;
    uint64_t var_38;
    uint64_t r124_6;
    uint64_t var_68;
    uint64_t _pre_phi;
    uint64_t rdi_3;
    uint64_t local_sp_0;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t r124_0_ph;
    uint64_t r124_1_ph;
    uint64_t var_76;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    unsigned char var_35;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t rdi_2;
    uint64_t local_sp_1;
    uint64_t rbx5_2;
    uint64_t rdi_1;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t rbx5_0_ph;
    uint64_t local_sp_2_ph;
    struct bb_output_one_dumb_line_ret_type mrv;
    struct bb_output_one_dumb_line_ret_type mrv1;
    uint64_t rbx5_1_ph;
    uint64_t local_sp_3_ph;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_91;
    struct bb_output_one_dumb_line_ret_type mrv2;
    struct bb_output_one_dumb_line_ret_type mrv3;
    uint64_t local_sp_5;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t rbx5_4;
    uint64_t r124_3;
    uint64_t rbx5_6;
    uint64_t rbx5_7;
    uint64_t r124_2;
    uint64_t local_sp_7;
    uint64_t rbx5_3;
    uint64_t local_sp_6;
    uint64_t var_50;
    uint64_t var_51;
    struct indirect_placeholder_43_ret_type var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    struct indirect_placeholder_42_ret_type var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_49;
    unsigned char var_3;
    uint64_t var_4;
    uint64_t var_5;
    bool var_6;
    uint64_t *var_7;
    struct indirect_placeholder_44_ret_type var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    struct indirect_placeholder_45_ret_type var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t r124_5;
    uint64_t r124_7;
    uint64_t rbx5_5;
    uint64_t local_sp_8;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t local_sp_9;
    uint64_t var_43;
    uint64_t rdi_4;
    uint64_t local_sp_10;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t rbx5_8;
    uint64_t rdi_5;
    uint64_t local_sp_11;
    uint64_t var_26;
    uint64_t var_27;
    struct indirect_placeholder_46_ret_type var_28;
    uint64_t var_29;
    uint64_t var_30;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_cc_src2();
    var_2 = var_0 + (-8L);
    r124_4 = r12;
    rbx5_5 = rbx;
    local_sp_8 = var_2;
    if (*(unsigned char *)4365192UL == '\x00') {
        var_3 = *(unsigned char *)4365194UL;
        var_4 = *(uint64_t *)4363296UL;
        var_5 = *(uint64_t *)4363304UL;
        var_6 = (var_3 == '\x00');
        var_7 = (uint64_t *)(var_0 + (-16L));
        if (var_6) {
            *var_7 = 4212621UL;
            var_17 = indirect_placeholder_45(r12, rbx, rbp, var_4, var_5);
            var_18 = var_17.field_0;
            var_19 = var_17.field_1;
            var_20 = (*(uint64_t *)4363024UL + *(uint64_t *)4363864UL) + (*(uint64_t *)4363296UL - *(uint64_t *)4363304UL);
            var_21 = var_0 + (-24L);
            *(uint64_t *)var_21 = 4212657UL;
            var_22 = indirect_placeholder_1(var_19, var_20);
            var_23 = *(uint64_t *)4363424UL;
            var_24 = *(uint64_t *)4363432UL;
            var_25 = helper_cc_compute_c_wrapper(var_23 - var_24, var_24, var_1, 17U);
            rbx5_6 = var_22;
            r124_5 = var_18;
            r124_7 = var_18;
            local_sp_9 = var_21;
            rbx5_8 = var_22;
            rdi_5 = var_23;
            local_sp_11 = var_21;
            if (var_25 == 0UL) {
                var_43 = (*(uint64_t *)4363464UL - *(uint64_t *)4363024UL) + (*(uint64_t *)4363392UL - *(uint64_t *)4363400UL);
                r124_6 = r124_5;
                r124_3 = r124_5;
                rbx5_7 = rbx5_6;
                r124_2 = r124_5;
                rdi_4 = var_43;
                local_sp_10 = local_sp_9;
                if (*(unsigned char *)4363376UL == '\x00') {
                    var_47 = local_sp_10 + (-8L);
                    *(uint64_t *)var_47 = 4212340UL;
                    var_48 = indirect_placeholder_1(rbx5_7, rdi_4);
                    rbx5_4 = var_48;
                    r124_3 = r124_6;
                    r124_2 = r124_6;
                    local_sp_7 = var_47;
                    rbx5_3 = var_48;
                    local_sp_6 = var_47;
                    if (*(unsigned char *)4363376UL == '\x00') {
                        var_49 = local_sp_7 + (-8L);
                        *(uint64_t *)var_49 = 4212604UL;
                        indirect_placeholder();
                        r124_2 = r124_3;
                        rbx5_3 = rbx5_4;
                        local_sp_6 = var_49;
                    }
                } else {
                    var_44 = var_43 - *(uint64_t *)4363440UL;
                    var_45 = local_sp_9 + (-8L);
                    *(uint64_t *)var_45 = 4212572UL;
                    var_46 = indirect_placeholder_1(rbx5_6, var_44);
                    rbx5_3 = var_46;
                    local_sp_6 = var_45;
                    rbx5_4 = var_46;
                    local_sp_7 = var_45;
                    if (*(unsigned char *)4363376UL == '\x00') {
                        var_49 = local_sp_7 + (-8L);
                        *(uint64_t *)var_49 = 4212604UL;
                        indirect_placeholder();
                        r124_2 = r124_3;
                        rbx5_3 = rbx5_4;
                        local_sp_6 = var_49;
                    }
                }
            } else {
                var_26 = *(uint64_t *)4363432UL;
                var_27 = local_sp_11 + (-8L);
                *(uint64_t *)var_27 = 4212689UL;
                var_28 = indirect_placeholder_46(r124_7, rbx5_8, rbp, rdi_5, var_26);
                var_29 = var_28.field_0;
                var_30 = var_28.field_1;
                r124_6 = var_29;
                rbx5_7 = var_30;
                local_sp_10 = var_27;
                if (*(unsigned char *)4363408UL == '\x00') {
                    var_39 = (*(uint64_t *)4363464UL - *(uint64_t *)4363024UL) + (*(uint64_t *)4363392UL - *(uint64_t *)4363400UL);
                    var_40 = *(uint64_t *)4363432UL - *(uint64_t *)4363424UL;
                    var_41 = var_39 - var_40;
                    rdi_4 = var_41;
                    if (*(unsigned char *)4363376UL == '\x00') {
                        var_42 = (var_39 - *(uint64_t *)4363440UL) - var_40;
                        rdi_4 = var_42;
                    }
                } else {
                    var_31 = local_sp_11 + (-16L);
                    *(uint64_t *)var_31 = 4213123UL;
                    indirect_placeholder();
                    var_32 = (*(uint64_t *)4363464UL - *(uint64_t *)4363024UL) + (*(uint64_t *)4363392UL - *(uint64_t *)4363400UL);
                    var_33 = *(uint64_t *)4363432UL;
                    var_34 = *(uint64_t *)4363424UL;
                    var_35 = *(unsigned char *)4363408UL;
                    rdi_0 = var_32;
                    local_sp_10 = var_31;
                    if (*(unsigned char *)4363376UL == '\x00') {
                        var_37 = *(uint64_t *)4363440UL;
                    } else {
                        var_36 = *(uint64_t *)4363440UL;
                        var_37 = var_36;
                        rdi_0 = var_32 - var_36;
                    }
                    var_38 = (rdi_0 + (var_34 - var_33)) - ((var_35 == '\x00') ? 0UL : var_37);
                    rdi_4 = var_38;
                }
                var_47 = local_sp_10 + (-8L);
                *(uint64_t *)var_47 = 4212340UL;
                var_48 = indirect_placeholder_1(rbx5_7, rdi_4);
                rbx5_4 = var_48;
                r124_3 = r124_6;
                r124_2 = r124_6;
                local_sp_7 = var_47;
                rbx5_3 = var_48;
                local_sp_6 = var_47;
                if (*(unsigned char *)4363376UL == '\x00') {
                    var_49 = local_sp_7 + (-8L);
                    *(uint64_t *)var_49 = 4212604UL;
                    indirect_placeholder();
                    r124_2 = r124_3;
                    rbx5_3 = rbx5_4;
                    local_sp_6 = var_49;
                }
            }
        } else {
            *var_7 = 4212221UL;
            var_8 = indirect_placeholder_44(r12, rbx, rbp, var_4, var_5);
            var_9 = var_8.field_0;
            var_10 = var_8.field_1;
            *(uint64_t *)(var_0 + (-24L)) = 4212231UL;
            indirect_placeholder();
            var_11 = (*(uint64_t *)4363024UL + *(uint64_t *)4363864UL) + (*(uint64_t *)4363296UL + (*(uint64_t *)4363304UL ^ (-1L)));
            var_12 = var_0 + (-32L);
            *(uint64_t *)var_12 = 4212271UL;
            var_13 = indirect_placeholder_1(var_10, var_11);
            r124_4 = var_9;
            rbx5_5 = var_13;
            local_sp_8 = var_12;
            var_14 = *(uint64_t *)4363424UL;
            var_15 = *(uint64_t *)4363432UL;
            var_16 = helper_cc_compute_c_wrapper(var_14 - var_15, var_15, var_1, 17U);
            rbx5_6 = rbx5_5;
            r124_5 = r124_4;
            r124_7 = r124_4;
            local_sp_9 = local_sp_8;
            rbx5_8 = rbx5_5;
            rdi_5 = var_14;
            local_sp_11 = local_sp_8;
            if (var_16 == 0UL) {
                var_26 = *(uint64_t *)4363432UL;
                var_27 = local_sp_11 + (-8L);
                *(uint64_t *)var_27 = 4212689UL;
                var_28 = indirect_placeholder_46(r124_7, rbx5_8, rbp, rdi_5, var_26);
                var_29 = var_28.field_0;
                var_30 = var_28.field_1;
                r124_6 = var_29;
                rbx5_7 = var_30;
                local_sp_10 = var_27;
                if (*(unsigned char *)4363408UL == '\x00') {
                    var_31 = local_sp_11 + (-16L);
                    *(uint64_t *)var_31 = 4213123UL;
                    indirect_placeholder();
                    var_32 = (*(uint64_t *)4363464UL - *(uint64_t *)4363024UL) + (*(uint64_t *)4363392UL - *(uint64_t *)4363400UL);
                    var_33 = *(uint64_t *)4363432UL;
                    var_34 = *(uint64_t *)4363424UL;
                    var_35 = *(unsigned char *)4363408UL;
                    rdi_0 = var_32;
                    local_sp_10 = var_31;
                    if (*(unsigned char *)4363376UL == '\x00') {
                        var_36 = *(uint64_t *)4363440UL;
                        var_37 = var_36;
                        rdi_0 = var_32 - var_36;
                    } else {
                        var_37 = *(uint64_t *)4363440UL;
                    }
                    var_38 = (rdi_0 + (var_34 - var_33)) - ((var_35 == '\x00') ? 0UL : var_37);
                    rdi_4 = var_38;
                } else {
                    var_39 = (*(uint64_t *)4363464UL - *(uint64_t *)4363024UL) + (*(uint64_t *)4363392UL - *(uint64_t *)4363400UL);
                    var_40 = *(uint64_t *)4363432UL - *(uint64_t *)4363424UL;
                    var_41 = var_39 - var_40;
                    rdi_4 = var_41;
                    if (*(unsigned char *)4363376UL == '\x00') {
                        var_42 = (var_39 - *(uint64_t *)4363440UL) - var_40;
                        rdi_4 = var_42;
                    }
                }
                var_47 = local_sp_10 + (-8L);
                *(uint64_t *)var_47 = 4212340UL;
                var_48 = indirect_placeholder_1(rbx5_7, rdi_4);
                rbx5_4 = var_48;
                r124_3 = r124_6;
                r124_2 = r124_6;
                local_sp_7 = var_47;
                rbx5_3 = var_48;
                local_sp_6 = var_47;
                if (*(unsigned char *)4363376UL == '\x00') {
                    var_49 = local_sp_7 + (-8L);
                    *(uint64_t *)var_49 = 4212604UL;
                    indirect_placeholder();
                    r124_2 = r124_3;
                    rbx5_3 = rbx5_4;
                    local_sp_6 = var_49;
                }
            } else {
                var_43 = (*(uint64_t *)4363464UL - *(uint64_t *)4363024UL) + (*(uint64_t *)4363392UL - *(uint64_t *)4363400UL);
                r124_6 = r124_5;
                r124_3 = r124_5;
                rbx5_7 = rbx5_6;
                r124_2 = r124_5;
                rdi_4 = var_43;
                local_sp_10 = local_sp_9;
                if (*(unsigned char *)4363376UL == '\x00') {
                    var_44 = var_43 - *(uint64_t *)4363440UL;
                    var_45 = local_sp_9 + (-8L);
                    *(uint64_t *)var_45 = 4212572UL;
                    var_46 = indirect_placeholder_1(rbx5_6, var_44);
                    rbx5_3 = var_46;
                    local_sp_6 = var_45;
                    rbx5_4 = var_46;
                    local_sp_7 = var_45;
                    if (*(unsigned char *)4363376UL == '\x00') {
                        var_49 = local_sp_7 + (-8L);
                        *(uint64_t *)var_49 = 4212604UL;
                        indirect_placeholder();
                        r124_2 = r124_3;
                        rbx5_3 = rbx5_4;
                        local_sp_6 = var_49;
                    }
                } else {
                    var_47 = local_sp_10 + (-8L);
                    *(uint64_t *)var_47 = 4212340UL;
                    var_48 = indirect_placeholder_1(rbx5_7, rdi_4);
                    rbx5_4 = var_48;
                    r124_3 = r124_6;
                    r124_2 = r124_6;
                    local_sp_7 = var_47;
                    rbx5_3 = var_48;
                    local_sp_6 = var_47;
                    if (*(unsigned char *)4363376UL == '\x00') {
                        var_49 = local_sp_7 + (-8L);
                        *(uint64_t *)var_49 = 4212604UL;
                        indirect_placeholder();
                        r124_2 = r124_3;
                        rbx5_3 = rbx5_4;
                        local_sp_6 = var_49;
                    }
                }
            }
        }
    } else {
        var_14 = *(uint64_t *)4363424UL;
        var_15 = *(uint64_t *)4363432UL;
        var_16 = helper_cc_compute_c_wrapper(var_14 - var_15, var_15, var_1, 17U);
        rbx5_6 = rbx5_5;
        r124_5 = r124_4;
        r124_7 = r124_4;
        local_sp_9 = local_sp_8;
        rbx5_8 = rbx5_5;
        rdi_5 = var_14;
        local_sp_11 = local_sp_8;
        if (var_16 == 0UL) {
            var_26 = *(uint64_t *)4363432UL;
            var_27 = local_sp_11 + (-8L);
            *(uint64_t *)var_27 = 4212689UL;
            var_28 = indirect_placeholder_46(r124_7, rbx5_8, rbp, rdi_5, var_26);
            var_29 = var_28.field_0;
            var_30 = var_28.field_1;
            r124_6 = var_29;
            rbx5_7 = var_30;
            local_sp_10 = var_27;
            if (*(unsigned char *)4363408UL == '\x00') {
                var_31 = local_sp_11 + (-16L);
                *(uint64_t *)var_31 = 4213123UL;
                indirect_placeholder();
                var_32 = (*(uint64_t *)4363464UL - *(uint64_t *)4363024UL) + (*(uint64_t *)4363392UL - *(uint64_t *)4363400UL);
                var_33 = *(uint64_t *)4363432UL;
                var_34 = *(uint64_t *)4363424UL;
                var_35 = *(unsigned char *)4363408UL;
                rdi_0 = var_32;
                local_sp_10 = var_31;
                if (*(unsigned char *)4363376UL == '\x00') {
                    var_36 = *(uint64_t *)4363440UL;
                    var_37 = var_36;
                    rdi_0 = var_32 - var_36;
                } else {
                    var_37 = *(uint64_t *)4363440UL;
                }
                var_38 = (rdi_0 + (var_34 - var_33)) - ((var_35 == '\x00') ? 0UL : var_37);
                rdi_4 = var_38;
            } else {
                var_39 = (*(uint64_t *)4363464UL - *(uint64_t *)4363024UL) + (*(uint64_t *)4363392UL - *(uint64_t *)4363400UL);
                var_40 = *(uint64_t *)4363432UL - *(uint64_t *)4363424UL;
                var_41 = var_39 - var_40;
                rdi_4 = var_41;
                if (*(unsigned char *)4363376UL == '\x00') {
                    var_42 = (var_39 - *(uint64_t *)4363440UL) - var_40;
                    rdi_4 = var_42;
                }
            }
            var_47 = local_sp_10 + (-8L);
            *(uint64_t *)var_47 = 4212340UL;
            var_48 = indirect_placeholder_1(rbx5_7, rdi_4);
            rbx5_4 = var_48;
            r124_3 = r124_6;
            r124_2 = r124_6;
            local_sp_7 = var_47;
            rbx5_3 = var_48;
            local_sp_6 = var_47;
            if (*(unsigned char *)4363376UL == '\x00') {
                var_49 = local_sp_7 + (-8L);
                *(uint64_t *)var_49 = 4212604UL;
                indirect_placeholder();
                r124_2 = r124_3;
                rbx5_3 = rbx5_4;
                local_sp_6 = var_49;
            }
        } else {
            var_43 = (*(uint64_t *)4363464UL - *(uint64_t *)4363024UL) + (*(uint64_t *)4363392UL - *(uint64_t *)4363400UL);
            r124_6 = r124_5;
            r124_3 = r124_5;
            rbx5_7 = rbx5_6;
            r124_2 = r124_5;
            rdi_4 = var_43;
            local_sp_10 = local_sp_9;
            if (*(unsigned char *)4363376UL == '\x00') {
                var_44 = var_43 - *(uint64_t *)4363440UL;
                var_45 = local_sp_9 + (-8L);
                *(uint64_t *)var_45 = 4212572UL;
                var_46 = indirect_placeholder_1(rbx5_6, var_44);
                rbx5_3 = var_46;
                local_sp_6 = var_45;
                rbx5_4 = var_46;
                local_sp_7 = var_45;
                if (*(unsigned char *)4363376UL == '\x00') {
                    var_49 = local_sp_7 + (-8L);
                    *(uint64_t *)var_49 = 4212604UL;
                    indirect_placeholder();
                    r124_2 = r124_3;
                    rbx5_3 = rbx5_4;
                    local_sp_6 = var_49;
                }
            } else {
                var_47 = local_sp_10 + (-8L);
                *(uint64_t *)var_47 = 4212340UL;
                var_48 = indirect_placeholder_1(rbx5_7, rdi_4);
                rbx5_4 = var_48;
                r124_3 = r124_6;
                r124_2 = r124_6;
                local_sp_7 = var_47;
                rbx5_3 = var_48;
                local_sp_6 = var_47;
                if (*(unsigned char *)4363376UL == '\x00') {
                    var_49 = local_sp_7 + (-8L);
                    *(uint64_t *)var_49 = 4212604UL;
                    indirect_placeholder();
                    r124_2 = r124_3;
                    rbx5_3 = rbx5_4;
                    local_sp_6 = var_49;
                }
            }
        }
    }
    var_50 = *(uint64_t *)4363400UL;
    var_51 = *(uint64_t *)4363392UL;
    *(uint64_t *)(local_sp_6 + (-8L)) = 4212372UL;
    var_52 = indirect_placeholder_43(r124_2, rbx5_3, rbp, var_51, var_50);
    var_53 = var_52.field_0;
    var_54 = var_52.field_1;
    var_55 = *(uint64_t *)4363024UL;
    *(uint64_t *)(local_sp_6 + (-16L)) = 4212384UL;
    var_56 = indirect_placeholder_1(var_54, var_55);
    var_57 = *(uint64_t *)4363360UL;
    var_58 = *(uint64_t *)4363368UL;
    var_59 = local_sp_6 + (-24L);
    *(uint64_t *)var_59 = 4212403UL;
    var_60 = indirect_placeholder_42(var_53, var_56, rbp, var_57, var_58);
    var_61 = var_60.field_0;
    var_62 = var_60.field_1;
    r124_0_ph = var_61;
    r124_1_ph = var_61;
    local_sp_1 = var_59;
    rbx5_2 = var_62;
    local_sp_5 = var_59;
    if (*(unsigned char *)4363344UL == '\x00') {
        var_69 = *(uint64_t *)4363328UL;
        var_70 = *(uint64_t *)4363336UL;
        var_71 = var_69 - var_70;
        var_72 = helper_cc_compute_c_wrapper(var_71, var_70, var_1, 17U);
        _pre_phi = var_71;
        if (var_72 != 0UL) {
            if (*(unsigned char *)4365194UL == '\x00') {
                if (*(unsigned char *)4365193UL == '\x00') {
                    indirect_placeholder();
                    mrv2.field_0 = rbx5_2;
                    mrv3 = mrv2;
                    mrv3.field_1 = rbp;
                    return mrv3;
                }
                if (*(unsigned char *)4365192UL == '\x00') {
                    indirect_placeholder();
                    mrv2.field_0 = rbx5_2;
                    mrv3 = mrv2;
                    mrv3.field_1 = rbp;
                    return mrv3;
                }
            }
            if (*(unsigned char *)4365192UL == '\x00') {
                indirect_placeholder();
                mrv2.field_0 = rbx5_2;
                mrv3 = mrv2;
                mrv3.field_1 = rbp;
                return mrv3;
            }
            var_83 = *(uint64_t *)4363464UL + (*(uint64_t *)4363360UL - *(uint64_t *)4363368UL);
            rdi_1 = var_83;
            if (*(unsigned char *)4363344UL == '\x00') {
                rdi_1 = var_83 - *(uint64_t *)4363440UL;
            }
            var_84 = local_sp_1 + (-8L);
            *(uint64_t *)var_84 = 4212510UL;
            var_85 = indirect_placeholder_1(var_62, rdi_1);
            rbx5_0_ph = var_85;
            local_sp_2_ph = var_84;
            rbx5_1_ph = var_85;
            local_sp_3_ph = var_84;
            if (*(unsigned char *)4365194UL != '\x00') {
                r124_1_ph = r124_0_ph;
                rbx5_1_ph = rbx5_0_ph;
                local_sp_3_ph = local_sp_2_ph;
                if (*(unsigned char *)4365193UL == '\x00') {
                    indirect_placeholder();
                    mrv.field_0 = rbx5_0_ph;
                    mrv1 = mrv;
                    mrv1.field_1 = rbp;
                    return mrv1;
                }
            }
            rbx5_2 = rbx5_1_ph;
            if (*(unsigned char *)4365192UL != '\x00') {
                var_86 = *(uint64_t *)4363024UL;
                *(uint64_t *)(local_sp_3_ph + (-8L)) = 4212933UL;
                var_87 = indirect_placeholder_1(rbx5_1_ph, var_86);
                var_88 = *(uint64_t *)4363296UL;
                var_89 = *(uint64_t *)4363304UL;
                *(uint64_t *)(local_sp_3_ph + (-16L)) = 4212952UL;
                var_90 = indirect_placeholder_40(r124_1_ph, var_87, rbp, var_88, var_89);
                var_91 = var_90.field_1;
                rbx5_2 = var_91;
            }
            indirect_placeholder();
            mrv2.field_0 = rbx5_2;
            mrv3 = mrv2;
            mrv3.field_1 = rbp;
            return mrv3;
        }
        rdi_2 = *(uint64_t *)4363464UL + (*(uint64_t *)4363360UL - *(uint64_t *)4363368UL);
    } else {
        var_63 = local_sp_6 + (-32L);
        *(uint64_t *)var_63 = 4212995UL;
        indirect_placeholder();
        var_64 = *(uint64_t *)4363328UL;
        var_65 = *(uint64_t *)4363336UL;
        var_66 = var_64 - var_65;
        var_67 = helper_cc_compute_c_wrapper(var_66, var_65, var_1, 17U);
        local_sp_1 = var_63;
        _pre_phi = var_66;
        local_sp_5 = var_63;
        if (var_67 != 0UL) {
            if (*(unsigned char *)4365194UL == '\x00') {
                if (*(unsigned char *)4365193UL == '\x00') {
                    indirect_placeholder();
                    mrv2.field_0 = rbx5_2;
                    mrv3 = mrv2;
                    mrv3.field_1 = rbp;
                    return mrv3;
                }
                if (*(unsigned char *)4365192UL == '\x00') {
                    indirect_placeholder();
                    mrv2.field_0 = rbx5_2;
                    mrv3 = mrv2;
                    mrv3.field_1 = rbp;
                    return mrv3;
                }
            }
            if (*(unsigned char *)4365192UL == '\x00') {
                indirect_placeholder();
                mrv2.field_0 = rbx5_2;
                mrv3 = mrv2;
                mrv3.field_1 = rbp;
                return mrv3;
            }
            var_83 = *(uint64_t *)4363464UL + (*(uint64_t *)4363360UL - *(uint64_t *)4363368UL);
            rdi_1 = var_83;
            if (*(unsigned char *)4363344UL == '\x00') {
                rdi_1 = var_83 - *(uint64_t *)4363440UL;
            }
            var_84 = local_sp_1 + (-8L);
            *(uint64_t *)var_84 = 4212510UL;
            var_85 = indirect_placeholder_1(var_62, rdi_1);
            rbx5_0_ph = var_85;
            local_sp_2_ph = var_84;
            rbx5_1_ph = var_85;
            local_sp_3_ph = var_84;
            if (*(unsigned char *)4365194UL != '\x00') {
                r124_1_ph = r124_0_ph;
                rbx5_1_ph = rbx5_0_ph;
                local_sp_3_ph = local_sp_2_ph;
                if (*(unsigned char *)4365193UL == '\x00') {
                    indirect_placeholder();
                    mrv.field_0 = rbx5_0_ph;
                    mrv1 = mrv;
                    mrv1.field_1 = rbp;
                    return mrv1;
                }
            }
            rbx5_2 = rbx5_1_ph;
            if (*(unsigned char *)4365192UL != '\x00') {
                var_86 = *(uint64_t *)4363024UL;
                *(uint64_t *)(local_sp_3_ph + (-8L)) = 4212933UL;
                var_87 = indirect_placeholder_1(rbx5_1_ph, var_86);
                var_88 = *(uint64_t *)4363296UL;
                var_89 = *(uint64_t *)4363304UL;
                *(uint64_t *)(local_sp_3_ph + (-16L)) = 4212952UL;
                var_90 = indirect_placeholder_40(r124_1_ph, var_87, rbp, var_88, var_89);
                var_91 = var_90.field_1;
                rbx5_2 = var_91;
            }
            indirect_placeholder();
            mrv2.field_0 = rbx5_2;
            mrv3 = mrv2;
            mrv3.field_1 = rbp;
            return mrv3;
        }
        var_68 = *(uint64_t *)4363464UL + (*(uint64_t *)4363360UL - *(uint64_t *)4363368UL);
        rdi_2 = var_68;
        if (*(unsigned char *)4363344UL == '\x00') {
            rdi_2 = var_68 - *(uint64_t *)4363440UL;
        }
    }
    var_73 = rdi_2 + _pre_phi;
    rdi_3 = var_73;
    if (*(unsigned char *)4363312UL == '\x00') {
        rdi_3 = var_73 - *(uint64_t *)4363440UL;
    }
    var_74 = local_sp_5 + (-8L);
    *(uint64_t *)var_74 = 4212867UL;
    var_75 = indirect_placeholder_1(var_62, rdi_3);
    local_sp_0 = var_74;
    if (*(unsigned char *)4363312UL == '\x00') {
        var_76 = local_sp_5 + (-16L);
        *(uint64_t *)var_76 = 4213091UL;
        indirect_placeholder();
        local_sp_0 = var_76;
    }
    var_77 = *(uint64_t *)4363328UL;
    var_78 = *(uint64_t *)4363336UL;
    var_79 = local_sp_0 + (-8L);
    *(uint64_t *)var_79 = 4212899UL;
    var_80 = indirect_placeholder_41(var_61, var_75, rbp, var_77, var_78);
    var_81 = var_80.field_0;
    var_82 = var_80.field_1;
    r124_0_ph = var_81;
    r124_1_ph = var_81;
    rbx5_0_ph = var_82;
    local_sp_2_ph = var_79;
    rbx5_1_ph = var_82;
    local_sp_3_ph = var_79;
    if (*(unsigned char *)4365194UL != '\x00') {
        r124_1_ph = r124_0_ph;
        rbx5_1_ph = rbx5_0_ph;
        local_sp_3_ph = local_sp_2_ph;
        if (*(unsigned char *)4365193UL == '\x00') {
            indirect_placeholder();
            mrv.field_0 = rbx5_0_ph;
            mrv1 = mrv;
            mrv1.field_1 = rbp;
            return mrv1;
        }
    }
    rbx5_2 = rbx5_1_ph;
    if (*(unsigned char *)4365192UL == '\x00') {
        indirect_placeholder();
        mrv2.field_0 = rbx5_2;
        mrv3 = mrv2;
        mrv3.field_1 = rbp;
        return mrv3;
    }
    var_86 = *(uint64_t *)4363024UL;
    *(uint64_t *)(local_sp_3_ph + (-8L)) = 4212933UL;
    var_87 = indirect_placeholder_1(rbx5_1_ph, var_86);
    var_88 = *(uint64_t *)4363296UL;
    var_89 = *(uint64_t *)4363304UL;
    *(uint64_t *)(local_sp_3_ph + (-16L)) = 4212952UL;
    var_90 = indirect_placeholder_40(r124_1_ph, var_87, rbp, var_88, var_89);
    var_91 = var_90.field_1;
    rbx5_2 = var_91;
    indirect_placeholder();
    mrv2.field_0 = rbx5_2;
    mrv3 = mrv2;
    mrv3.field_1 = rbp;
    return mrv3;
}
