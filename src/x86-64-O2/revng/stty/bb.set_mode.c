typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_25(uint64_t param_0);
extern uint32_t init_state_0x82fc(void);
uint64_t bb_set_mode(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint32_t var_6;
    uint64_t var_7;
    bool var_8;
    uint32_t var_32;
    uint32_t *var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint32_t cc_op_0;
    uint64_t cc_src_0;
    uint64_t cc_dst_0;
    uint32_t cc_op_2;
    uint64_t rdi2_0;
    uint64_t rcx_0;
    uint64_t rsi3_0;
    uint32_t cc_op_1;
    uint64_t cc_src_1;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t cc_dst_1;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t cc_dst_5;
    uint64_t var_50;
    uint64_t cc_src_2;
    uint64_t cc_dst_2;
    uint64_t rdi2_1;
    uint64_t rcx_1;
    uint64_t rsi3_1;
    uint32_t cc_op_3;
    uint64_t cc_src_3;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t cc_dst_3;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint32_t cc_op_4;
    uint64_t cc_src_4;
    uint64_t cc_dst_4;
    uint64_t rdi2_2;
    uint64_t rcx_2;
    uint64_t rsi3_2;
    uint32_t cc_op_5;
    uint64_t cc_src_5;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t cc_dst_10;
    uint64_t var_66;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t r13_0;
    uint64_t cc_src2_0;
    uint64_t local_sp_0;
    uint64_t var_53;
    uint32_t *var_96;
    uint32_t var_97;
    uint32_t *var_98;
    uint64_t cc_dst_14;
    uint64_t var_78;
    uint32_t var_99;
    uint64_t rax_0;
    uint64_t *_cast;
    uint64_t var_54;
    uint32_t cc_op_6;
    uint64_t var_55;
    unsigned char var_56;
    uint64_t cc_src_6;
    uint64_t cc_dst_6;
    uint64_t var_57;
    uint32_t cc_op_7;
    uint64_t cc_src_7;
    uint64_t cc_dst_7;
    uint64_t rdi2_3;
    uint64_t rcx_3;
    uint64_t rsi3_3;
    uint32_t cc_op_8;
    uint64_t cc_src_8;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t cc_dst_8;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint32_t cc_op_9;
    uint64_t cc_src_9;
    uint64_t cc_dst_9;
    uint64_t rdi2_4;
    uint64_t rcx_4;
    uint64_t rsi3_4;
    uint32_t cc_op_10;
    uint64_t cc_src_10;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint32_t *var_94;
    uint32_t var_95;
    uint32_t cc_op_11;
    uint64_t cc_src_11;
    uint64_t cc_dst_11;
    uint64_t rdi2_5;
    uint64_t rcx_5;
    uint64_t rsi3_5;
    uint32_t cc_op_12;
    uint64_t cc_src_12;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t cc_dst_12;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint32_t *var_87;
    uint32_t var_88;
    uint32_t *var_89;
    uint32_t var_90;
    uint32_t var_91;
    uint32_t cc_op_13;
    uint32_t var_92;
    uint32_t var_93;
    uint64_t cc_src_13;
    uint64_t cc_dst_13;
    uint64_t rdi2_6;
    uint64_t rcx_6;
    uint64_t rsi3_6;
    uint32_t cc_op_14;
    uint64_t cc_src_14;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_79;
    uint64_t var_80;
    uint32_t *var_81;
    uint32_t var_82;
    uint64_t *var_83;
    uint64_t var_84;
    uint32_t var_85;
    uint32_t var_86;
    uint32_t *var_100;
    uint32_t *var_101;
    uint64_t var_11;
    uint32_t var_12;
    uint32_t *var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint32_t cc_op_15;
    uint64_t cc_src_15;
    uint64_t cc_dst_15;
    uint64_t rdi2_7;
    uint64_t rcx_7;
    uint64_t rsi3_7;
    uint32_t cc_op_16;
    uint64_t cc_src_16;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t cc_dst_16;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint32_t cc_op_17;
    uint64_t cc_src_17;
    uint64_t cc_dst_17;
    uint64_t rdi2_8;
    uint64_t rcx_8;
    uint64_t rsi3_8;
    uint32_t cc_op_18;
    uint64_t cc_src_18;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t cc_dst_18;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint32_t *var_28;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r12();
    var_2 = init_rbx();
    var_3 = init_r13();
    var_4 = init_rbp();
    var_5 = init_cc_src2();
    var_6 = init_state_0x82fc();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    var_7 = (uint64_t)(uint32_t)rsi;
    *(uint64_t *)(var_0 + (-32L)) = var_2;
    var_8 = ((uint64_t)(unsigned char)rsi == 0UL);
    cc_op_0 = 25U;
    cc_src_0 = 8UL;
    cc_dst_0 = 0UL;
    cc_op_2 = 22U;
    rdi2_0 = 4264038UL;
    rcx_0 = 6UL;
    cc_src_2 = 0UL;
    rdi2_1 = 4264044UL;
    rcx_1 = 7UL;
    cc_op_4 = 22U;
    cc_src_4 = 0UL;
    rdi2_2 = 4264051UL;
    rcx_2 = 5UL;
    rax_0 = 1UL;
    cc_op_6 = 16U;
    cc_src_6 = 101UL;
    rdi2_3 = 4263981UL;
    rcx_3 = 5UL;
    cc_op_9 = 22U;
    cc_src_9 = 0UL;
    rdi2_4 = 4263986UL;
    rcx_4 = 7UL;
    cc_op_11 = 22U;
    cc_src_11 = 0UL;
    rdi2_5 = 4263993UL;
    rcx_5 = 6UL;
    cc_op_13 = 22U;
    cc_src_13 = 0UL;
    rdi2_6 = 4263999UL;
    rcx_6 = 7UL;
    cc_op_15 = 25U;
    cc_src_15 = 8UL;
    cc_dst_15 = 0UL;
    rdi2_7 = 4264038UL;
    rcx_7 = 6UL;
    cc_op_17 = 22U;
    cc_src_17 = 0UL;
    rdi2_8 = 4264044UL;
    rcx_8 = 7UL;
    if (var_8) {
        var_29 = (uint64_t)*(uint32_t *)(rdi + 8UL);
        var_30 = var_0 + (-48L);
        *(uint64_t *)var_30 = 4206635UL;
        var_31 = indirect_placeholder(var_29, rdx);
        local_sp_0 = var_30;
        if (var_31 == 0UL) {
            var_32 = *(uint32_t *)(rdi + 24UL) ^ (-1);
            var_33 = (uint32_t *)var_31;
            *var_33 = ((*var_33 & var_32) | *(uint32_t *)(rdi + 16UL));
            return 1UL;
        }
        var_34 = *(uint64_t *)rdi;
        var_35 = (uint64_t)var_6;
        rsi3_0 = var_34;
        rsi3_1 = var_34;
        rsi3_2 = var_34;
        r13_0 = var_34;
        cc_op_0 = 14U;
        cc_op_1 = cc_op_0;
        cc_src_1 = cc_src_0;
        cc_dst_1 = cc_dst_0;
        while (rcx_0 != 0UL)
            {
                var_36 = (uint64_t)*(unsigned char *)rdi2_0;
                var_37 = (uint64_t)*(unsigned char *)rsi3_0 - var_36;
                cc_src_0 = var_36;
                cc_dst_0 = var_37;
                cc_op_1 = 14U;
                cc_src_1 = var_36;
                cc_dst_1 = var_37;
                if ((uint64_t)(unsigned char)var_37 == 0UL) {
                    break;
                }
                rdi2_0 = rdi2_0 + var_35;
                rcx_0 = rcx_0 + (-1L);
                rsi3_0 = rsi3_0 + var_35;
                cc_op_0 = 14U;
                cc_op_1 = cc_op_0;
                cc_src_1 = cc_src_0;
                cc_dst_1 = cc_dst_0;
            }
        var_38 = helper_cc_compute_all_wrapper(cc_dst_1, cc_src_1, var_5, cc_op_1);
        var_39 = ((var_38 & 65UL) == 0UL);
        var_40 = helper_cc_compute_c_wrapper(cc_dst_1, var_38, var_5, 1U);
        var_41 = (uint64_t)((unsigned char)var_39 - (unsigned char)var_40);
        cc_dst_2 = var_41;
        if (var_41 != 0UL) {
            var_101 = (uint32_t *)(rdx + 8UL);
            *var_101 = ((*var_101 & (-817)) | 288U);
            return rax_0;
        }
        cc_op_2 = 14U;
        cc_op_3 = cc_op_2;
        cc_src_3 = cc_src_2;
        cc_dst_3 = cc_dst_2;
        while (rcx_1 != 0UL)
            {
                var_42 = (uint64_t)*(unsigned char *)rdi2_1;
                var_43 = (uint64_t)*(unsigned char *)rsi3_1 - var_42;
                cc_src_2 = var_42;
                cc_dst_2 = var_43;
                cc_op_3 = 14U;
                cc_src_3 = var_42;
                cc_dst_3 = var_43;
                if ((uint64_t)(unsigned char)var_43 == 0UL) {
                    break;
                }
                rdi2_1 = rdi2_1 + var_35;
                rcx_1 = rcx_1 + (-1L);
                rsi3_1 = rsi3_1 + var_35;
                cc_op_2 = 14U;
                cc_op_3 = cc_op_2;
                cc_src_3 = cc_src_2;
                cc_dst_3 = cc_dst_2;
            }
        var_44 = helper_cc_compute_all_wrapper(cc_dst_3, cc_src_3, var_40, cc_op_3);
        var_45 = ((var_44 & 65UL) == 0UL);
        var_46 = helper_cc_compute_c_wrapper(cc_dst_3, var_44, var_40, 1U);
        var_47 = (uint64_t)((unsigned char)var_45 - (unsigned char)var_46);
        cc_dst_4 = var_47;
        if (var_47 != 0UL) {
            var_101 = (uint32_t *)(rdx + 8UL);
            *var_101 = ((*var_101 & (-817)) | 288U);
            return rax_0;
        }
        cc_op_4 = 14U;
        cc_op_5 = cc_op_4;
        cc_src_5 = cc_src_4;
        cc_dst_5 = cc_dst_4;
        while (rcx_2 != 0UL)
            {
                var_48 = (uint64_t)*(unsigned char *)rdi2_2;
                var_49 = (uint64_t)*(unsigned char *)rsi3_2 - var_48;
                cc_dst_5 = var_49;
                cc_src_4 = var_48;
                cc_dst_4 = var_49;
                cc_op_5 = 14U;
                cc_src_5 = var_48;
                if ((uint64_t)(unsigned char)var_49 == 0UL) {
                    break;
                }
                rdi2_2 = rdi2_2 + var_35;
                rcx_2 = rcx_2 + (-1L);
                rsi3_2 = rsi3_2 + var_35;
                cc_op_4 = 14U;
                cc_op_5 = cc_op_4;
                cc_src_5 = cc_src_4;
                cc_dst_5 = cc_dst_4;
            }
        var_50 = helper_cc_compute_all_wrapper(cc_dst_5, cc_src_5, var_46, cc_op_5);
        var_51 = ((var_50 & 65UL) == 0UL);
        var_52 = helper_cc_compute_c_wrapper(cc_dst_5, var_50, var_46, 1U);
        cc_src2_0 = var_52;
        if ((uint64_t)((unsigned char)var_51 - (unsigned char)var_52) != 0UL) {
            var_100 = (uint32_t *)(rdx + 8UL);
            *var_100 = ((*var_100 & (-817)) | 800U);
            return rax_0;
        }
    }
    rax_0 = 0UL;
    if ((*(unsigned char *)(rdi + 12UL) & '\x04') == '\x00') {
        return rax_0;
    }
    var_9 = (uint64_t)*(uint32_t *)(rdi + 8UL);
    *(uint64_t *)(var_0 + (-48L)) = 4206586UL;
    var_10 = indirect_placeholder_7(0UL, var_9, rdx);
    rax_0 = var_7;
    if (var_10 != 0UL) {
        var_11 = *(uint64_t *)(rdi + 16UL);
        var_12 = *(uint32_t *)(rdi + 24UL);
        var_13 = (uint32_t *)var_10;
        *var_13 = (*var_13 & ((var_12 | (uint32_t)var_11) ^ (-1)));
        return rax_0;
    }
    var_14 = *(uint64_t *)rdi;
    var_15 = (uint64_t)var_6;
    r13_0 = var_14;
    rsi3_7 = var_14;
    rsi3_8 = var_14;
    cc_op_15 = 14U;
    cc_op_16 = cc_op_15;
    cc_src_16 = cc_src_15;
    cc_dst_16 = cc_dst_15;
    while (rcx_7 != 0UL)
        {
            var_16 = (uint64_t)*(unsigned char *)rdi2_7;
            var_17 = (uint64_t)*(unsigned char *)rsi3_7 - var_16;
            cc_src_15 = var_16;
            cc_dst_15 = var_17;
            cc_op_16 = 14U;
            cc_src_16 = var_16;
            cc_dst_16 = var_17;
            if ((uint64_t)(unsigned char)var_17 == 0UL) {
                break;
            }
            rdi2_7 = rdi2_7 + var_15;
            rcx_7 = rcx_7 + (-1L);
            rsi3_7 = rsi3_7 + var_15;
            cc_op_15 = 14U;
            cc_op_16 = cc_op_15;
            cc_src_16 = cc_src_15;
            cc_dst_16 = cc_dst_15;
        }
    var_18 = helper_cc_compute_all_wrapper(cc_dst_16, cc_src_16, var_5, cc_op_16);
    var_19 = ((var_18 & 65UL) == 0UL);
    var_20 = helper_cc_compute_c_wrapper(cc_dst_16, var_18, var_5, 1U);
    var_21 = (uint64_t)((unsigned char)var_19 - (unsigned char)var_20);
    cc_dst_17 = var_21;
    if (var_21 == 0UL) {
        var_28 = (uint32_t *)(rdx + 8UL);
        *var_28 = ((*var_28 & (-305)) | 48U);
        return var_7;
    }
    cc_op_17 = 14U;
    cc_op_18 = cc_op_17;
    cc_src_18 = cc_src_17;
    cc_dst_18 = cc_dst_17;
    while (rcx_8 != 0UL)
        {
            var_22 = (uint64_t)*(unsigned char *)rdi2_8;
            var_23 = (uint64_t)*(unsigned char *)rsi3_8 - var_22;
            cc_src_17 = var_22;
            cc_dst_17 = var_23;
            cc_op_18 = 14U;
            cc_src_18 = var_22;
            cc_dst_18 = var_23;
            if ((uint64_t)(unsigned char)var_23 == 0UL) {
                break;
            }
            rdi2_8 = rdi2_8 + var_15;
            rcx_8 = rcx_8 + (-1L);
            rsi3_8 = rsi3_8 + var_15;
            cc_op_17 = 14U;
            cc_op_18 = cc_op_17;
            cc_src_18 = cc_src_17;
            cc_dst_18 = cc_dst_17;
        }
    var_24 = helper_cc_compute_all_wrapper(cc_dst_18, cc_src_18, var_20, cc_op_18);
    var_25 = ((var_24 & 65UL) == 0UL);
    var_26 = helper_cc_compute_c_wrapper(cc_dst_18, var_24, var_20, 1U);
    cc_src2_0 = var_26;
    if ((uint64_t)((unsigned char)var_25 - (unsigned char)var_26) == 0UL) {
        var_28 = (uint32_t *)(rdx + 8UL);
        *var_28 = ((*var_28 & (-305)) | 48U);
        return var_7;
    }
    var_27 = var_0 + (-56L);
    *(uint64_t *)var_27 = 4207069UL;
    indirect_placeholder_1();
    local_sp_0 = var_27;
    var_53 = (uint64_t)*(unsigned char *)r13_0;
    rax_0 = var_7;
    rsi3_3 = r13_0;
    rsi3_4 = r13_0;
    rsi3_5 = r13_0;
    rsi3_6 = r13_0;
    if ((uint64_t)((uint32_t)var_53 + (-110)) != 0UL) {
        if (*(unsigned char *)(r13_0 + 1UL) != 'l' & *(unsigned char *)(r13_0 + 2UL) != '\x00') {
            if (var_8) {
                _cast = (uint64_t *)rdx;
                *_cast = (*_cast & (-17179869441L));
            } else {
                var_96 = (uint32_t *)rdx;
                var_97 = *var_96;
                var_98 = (uint32_t *)(rdx + 4UL);
                var_99 = (*var_98 & (-45)) | 4U;
                *var_96 = ((var_97 & (-65473)) | ((uint32_t)((uint16_t)var_97 & (unsigned short)65024U) | 256U));
                *var_98 = var_99;
            }
            return rax_0;
        }
    }
    var_54 = var_53 + (-101L);
    cc_dst_6 = var_54;
    rax_0 = 1UL;
    if ((uint64_t)(uint32_t)var_54 != 0UL) {
        var_55 = (uint64_t)*(unsigned char *)(r13_0 + 1UL) + (-107L);
        cc_op_6 = 14U;
        cc_src_6 = 107UL;
        cc_dst_6 = var_55;
        var_56 = *(unsigned char *)(r13_0 + 2UL);
        cc_src_6 = 0UL;
        cc_dst_6 = (uint64_t)var_56;
        if ((uint64_t)(unsigned char)var_55 != 0UL & var_56 != '\x00') {
            *(uint16_t *)(rdx + 19UL) = (uint16_t)(unsigned short)5503U;
            return rax_0;
        }
    }
    var_57 = (uint64_t)var_6;
    cc_op_7 = cc_op_6;
    cc_src_7 = cc_src_6;
    cc_dst_7 = cc_dst_6;
    cc_op_7 = 14U;
    cc_op_8 = cc_op_7;
    cc_src_8 = cc_src_7;
    cc_dst_8 = cc_dst_7;
    while (rcx_3 != 0UL)
        {
            var_58 = (uint64_t)*(unsigned char *)rdi2_3;
            var_59 = (uint64_t)*(unsigned char *)rsi3_3 - var_58;
            cc_src_7 = var_58;
            cc_dst_7 = var_59;
            cc_op_8 = 14U;
            cc_src_8 = var_58;
            cc_dst_8 = var_59;
            if ((uint64_t)(unsigned char)var_59 == 0UL) {
                break;
            }
            rdi2_3 = rdi2_3 + var_57;
            rcx_3 = rcx_3 + (-1L);
            rsi3_3 = rsi3_3 + var_57;
            cc_op_7 = 14U;
            cc_op_8 = cc_op_7;
            cc_src_8 = cc_src_7;
            cc_dst_8 = cc_dst_7;
        }
    var_60 = helper_cc_compute_all_wrapper(cc_dst_8, cc_src_8, cc_src2_0, cc_op_8);
    var_61 = ((var_60 & 65UL) == 0UL);
    var_62 = helper_cc_compute_c_wrapper(cc_dst_8, var_60, cc_src2_0, 1U);
    var_63 = (uint64_t)((unsigned char)var_61 - (unsigned char)var_62);
    cc_dst_9 = var_63;
    if (var_63 == 0UL) {
        *(uint64_t *)(local_sp_0 + (-8L)) = 4207185UL;
        indirect_placeholder_25(rdx);
    } else {
        cc_dst_10 = cc_dst_9;
        rax_0 = var_7;
        cc_op_9 = 14U;
        cc_op_10 = cc_op_9;
        cc_src_10 = cc_src_9;
        while (rcx_4 != 0UL)
            {
                var_64 = (uint64_t)*(unsigned char *)rdi2_4;
                var_65 = (uint64_t)*(unsigned char *)rsi3_4 - var_64;
                cc_dst_10 = var_65;
                cc_src_9 = var_64;
                cc_dst_9 = var_65;
                cc_op_10 = 14U;
                cc_src_10 = var_64;
                if ((uint64_t)(unsigned char)var_65 == 0UL) {
                    break;
                }
                rdi2_4 = rdi2_4 + var_57;
                rcx_4 = rcx_4 + (-1L);
                rsi3_4 = rsi3_4 + var_57;
                cc_dst_10 = cc_dst_9;
                rax_0 = var_7;
                cc_op_9 = 14U;
                cc_op_10 = cc_op_9;
                cc_src_10 = cc_src_9;
            }
        var_66 = helper_cc_compute_all_wrapper(cc_dst_10, cc_src_10, var_62, cc_op_10);
        var_67 = ((var_66 & 65UL) == 0UL);
        var_68 = helper_cc_compute_c_wrapper(cc_dst_10, var_66, var_62, 1U);
        var_69 = (uint64_t)((unsigned char)var_67 - (unsigned char)var_68);
        cc_dst_11 = var_69;
        if (var_69 == 0UL) {
            var_94 = (uint32_t *)(rdx + 12UL);
            var_95 = *var_94;
            if (var_8) {
                *var_94 = (var_95 & (-3));
            } else {
                *var_94 = (var_95 | 2U);
            }
        } else {
            cc_op_11 = 14U;
            cc_op_12 = cc_op_11;
            cc_src_12 = cc_src_11;
            cc_dst_12 = cc_dst_11;
            while (rcx_5 != 0UL)
                {
                    var_70 = (uint64_t)*(unsigned char *)rdi2_5;
                    var_71 = (uint64_t)*(unsigned char *)rsi3_5 - var_70;
                    cc_src_11 = var_70;
                    cc_dst_11 = var_71;
                    cc_op_12 = 14U;
                    cc_src_12 = var_70;
                    cc_dst_12 = var_71;
                    if ((uint64_t)(unsigned char)var_71 == 0UL) {
                        break;
                    }
                    rdi2_5 = rdi2_5 + var_57;
                    rcx_5 = rcx_5 + (-1L);
                    rsi3_5 = rsi3_5 + var_57;
                    cc_op_11 = 14U;
                    cc_op_12 = cc_op_11;
                    cc_src_12 = cc_src_11;
                    cc_dst_12 = cc_dst_11;
                }
            var_72 = helper_cc_compute_all_wrapper(cc_dst_12, cc_src_12, var_68, cc_op_12);
            var_73 = ((var_72 & 65UL) == 0UL);
            var_74 = helper_cc_compute_c_wrapper(cc_dst_12, var_72, var_68, 1U);
            var_75 = (uint64_t)((unsigned char)var_73 - (unsigned char)var_74);
            cc_dst_13 = var_75;
            if (var_75 == 0UL) {
                var_87 = (uint32_t *)(rdx + 8UL);
                var_88 = *var_87;
                var_89 = (uint32_t *)rdx;
                var_90 = *var_89;
                var_91 = var_88 & (-305);
                if (var_8) {
                    var_93 = var_90 & (-33);
                    *var_87 = (var_91 | 48U);
                    *var_89 = var_93;
                } else {
                    var_92 = var_90 | 32U;
                    *var_87 = (var_91 | 288U);
                    *var_89 = var_92;
                }
            } else {
                cc_op_13 = 14U;
                cc_op_14 = cc_op_13;
                cc_src_14 = cc_src_13;
                cc_dst_14 = cc_dst_13;
                while (rcx_6 != 0UL)
                    {
                        var_76 = (uint64_t)*(unsigned char *)rdi2_6;
                        var_77 = (uint64_t)*(unsigned char *)rsi3_6 - var_76;
                        cc_dst_14 = var_77;
                        cc_src_13 = var_76;
                        cc_dst_13 = var_77;
                        cc_op_14 = 14U;
                        cc_src_14 = var_76;
                        if ((uint64_t)(unsigned char)var_77 == 0UL) {
                            break;
                        }
                        rdi2_6 = rdi2_6 + var_57;
                        rcx_6 = rcx_6 + (-1L);
                        rsi3_6 = rsi3_6 + var_57;
                        cc_op_13 = 14U;
                        cc_op_14 = cc_op_13;
                        cc_src_14 = cc_src_13;
                        cc_dst_14 = cc_dst_13;
                    }
                var_78 = helper_cc_compute_all_wrapper(cc_dst_14, cc_src_14, var_74, cc_op_14);
                var_79 = ((var_78 & 65UL) == 0UL);
                var_80 = helper_cc_compute_c_wrapper(cc_dst_14, var_78, var_74, 1U);
                if ((uint64_t)((unsigned char)var_79 - (unsigned char)var_80) == 0UL) {
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4207413UL;
                    indirect_placeholder_1();
                    *(uint64_t *)(local_sp_0 + (-16L)) = 4207430UL;
                    indirect_placeholder_1();
                    *(uint64_t *)(local_sp_0 + (-24L)) = 4207447UL;
                    indirect_placeholder_1();
                    *(uint64_t *)(local_sp_0 + (-32L)) = 4207606UL;
                    indirect_placeholder_1();
                    *(uint64_t *)(local_sp_0 + (-40L)) = 4207683UL;
                    indirect_placeholder_1();
                    *(uint64_t *)(local_sp_0 + (-48L)) = 4207719UL;
                    indirect_placeholder_1();
                } else {
                    var_81 = (uint32_t *)(rdx + 8UL);
                    var_82 = *var_81 & (-305);
                    var_83 = (uint64_t *)rdx;
                    var_84 = *var_83;
                    if (var_8) {
                        var_86 = var_82 | 48U;
                        *var_83 = (var_84 & (-4294967329L));
                        *var_81 = var_86;
                    } else {
                        var_85 = var_82 | 288U;
                        *var_83 = (var_84 | 4294967328UL);
                        *var_81 = var_85;
                    }
                }
            }
        }
    }
    return rax_0;
}
