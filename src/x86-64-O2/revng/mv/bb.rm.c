typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_84_ret_type;
struct indirect_placeholder_85_ret_type;
struct indirect_placeholder_86_ret_type;
struct indirect_placeholder_87_ret_type;
struct indirect_placeholder_88_ret_type;
struct indirect_placeholder_89_ret_type;
struct indirect_placeholder_84_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_85_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_86_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_87_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_88_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_89_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r9(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0);
extern uint64_t init_r8(void);
extern struct indirect_placeholder_84_ret_type indirect_placeholder_84(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_85_ret_type indirect_placeholder_85(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_86_ret_type indirect_placeholder_86(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_87_ret_type indirect_placeholder_87(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_88_ret_type indirect_placeholder_88(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_89_ret_type indirect_placeholder_89(uint64_t param_0, uint64_t param_1, uint64_t param_2);
uint64_t bb_rm(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_85_ret_type var_34;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t rcx_2;
    uint64_t r12_2;
    uint64_t r12_0;
    uint64_t local_sp_0;
    uint64_t rax_0;
    uint64_t r8_0;
    uint64_t local_sp_3;
    uint64_t rcx_0;
    uint64_t r9_0;
    uint64_t var_42;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t local_sp_1;
    uint64_t local_sp_3_be;
    uint64_t rbx_0;
    uint32_t var_23;
    uint64_t var_24;
    uint64_t rbx_1;
    uint64_t var_25;
    struct indirect_placeholder_86_ret_type var_26;
    uint64_t var_27;
    bool var_28;
    uint64_t var_29;
    uint64_t local_sp_2;
    uint64_t var_30;
    uint64_t r12_3_be;
    uint64_t r12_1;
    uint64_t rcx_1;
    uint64_t rsi2_0;
    uint64_t var_18;
    struct indirect_placeholder_87_ret_type var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint32_t var_22;
    uint32_t var_31;
    uint64_t r12_3;
    uint64_t var_13;
    struct indirect_placeholder_88_ret_type var_14;
    uint64_t var_15;
    bool var_16;
    uint64_t var_17;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    struct indirect_placeholder_89_ret_type var_11;
    uint64_t var_12;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r12();
    var_2 = init_rbx();
    var_3 = init_r13();
    var_4 = init_r8();
    var_5 = init_rbp();
    var_6 = init_cc_src2();
    var_7 = init_r9();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_2;
    r12_0 = 4UL;
    r8_0 = var_4;
    r9_0 = var_7;
    rbx_1 = rsi;
    r12_3 = 2UL;
    if (*(uint64_t *)rdi == 0UL) {
        return 2UL;
    }
    var_8 = helper_cc_compute_c_wrapper((uint64_t)*(unsigned char *)(rsi + 8UL) + (-1L), 1UL, var_6, 14U);
    var_9 = (uint64_t)((((0U - (uint32_t)var_8) & (-64)) + 600U) & (-40));
    var_10 = var_0 + (-48L);
    *(uint64_t *)var_10 = 4214873UL;
    var_11 = indirect_placeholder_89(0UL, rdi, var_9);
    var_12 = var_11.field_0;
    local_sp_3 = var_10;
    var_13 = local_sp_3 + (-8L);
    *(uint64_t *)var_13 = 4214888UL;
    var_14 = indirect_placeholder_88(rbx_1, var_12);
    var_15 = var_14.field_0;
    var_16 = (var_15 == 0UL);
    var_17 = var_14.field_2;
    rcx_2 = var_17;
    r12_2 = r12_3;
    local_sp_1 = var_13;
    local_sp_2 = var_13;
    r12_1 = r12_3;
    rcx_1 = var_17;
    rsi2_0 = var_15;
    while (!var_16)
        {
            rbx_0 = var_14.field_1;
            while (1U)
                {
                    var_18 = local_sp_1 + (-8L);
                    *(uint64_t *)var_18 = 4214907UL;
                    var_19 = indirect_placeholder_87(rbx_0, var_12, rcx_1, rsi2_0);
                    var_20 = var_19.field_0;
                    var_21 = var_19.field_1;
                    var_22 = (uint32_t)var_20;
                    local_sp_3_be = var_18;
                    r12_3_be = r12_1;
                    rbx_1 = var_21;
                    if ((uint64_t)(var_22 + (-2)) <= 2UL) {
                        loop_state_var = 2U;
                        break;
                    }
                    r12_3_be = 4UL;
                    if ((uint64_t)(var_22 + (-4)) != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    r12_3_be = r12_1;
                    if ((uint64_t)(var_22 + (-3)) != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_23 = (uint32_t)r12_1;
                    var_24 = ((uint64_t)(var_23 + (-2)) == 0UL) ? 3UL : (uint64_t)var_23;
                    var_25 = local_sp_1 + (-16L);
                    *(uint64_t *)var_25 = 4214941UL;
                    var_26 = indirect_placeholder_86(var_21, var_12);
                    var_27 = var_26.field_0;
                    var_28 = (var_27 == 0UL);
                    var_29 = var_26.field_2;
                    rcx_2 = var_29;
                    r12_2 = var_24;
                    local_sp_1 = var_25;
                    local_sp_2 = var_25;
                    r12_1 = var_24;
                    rcx_1 = var_29;
                    rsi2_0 = var_27;
                    if (!var_28) {
                        loop_state_var = 1U;
                        break;
                    }
                    rbx_0 = var_26.field_1;
                    continue;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 2U:
              case 0U:
                {
                    switch (loop_state_var) {
                      case 2U:
                        {
                            var_30 = local_sp_1 + (-16L);
                            *(uint64_t *)var_30 = 4215033UL;
                            indirect_placeholder();
                            local_sp_3_be = var_30;
                        }
                        break;
                      case 0U:
                        {
                            local_sp_3 = local_sp_3_be;
                            r12_3 = r12_3_be;
                            var_13 = local_sp_3 + (-8L);
                            *(uint64_t *)var_13 = 4214888UL;
                            var_14 = indirect_placeholder_88(rbx_1, var_12);
                            var_15 = var_14.field_0;
                            var_16 = (var_15 == 0UL);
                            var_17 = var_14.field_2;
                            rcx_2 = var_17;
                            r12_2 = r12_3;
                            local_sp_1 = var_13;
                            local_sp_2 = var_13;
                            r12_1 = r12_3;
                            rcx_1 = var_17;
                            rsi2_0 = var_15;
                            continue;
                        }
                        break;
                    }
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    *(uint64_t *)(local_sp_2 + (-8L)) = 4214954UL;
    indirect_placeholder();
    var_31 = *(volatile uint32_t *)(uint32_t *)0UL;
    rcx_0 = rcx_2;
    if (var_31 == 0U) {
        var_40 = local_sp_2 + (-16L);
        *(uint64_t *)var_40 = 4214968UL;
        var_41 = indirect_placeholder_5(var_12);
        r12_0 = r12_2;
        local_sp_0 = var_40;
        rax_0 = var_41;
        if ((uint64_t)(uint32_t)var_41 != 0UL) {
            return (uint64_t)(uint32_t)r12_0;
        }
    }
    var_32 = (uint64_t)var_31;
    *(uint64_t *)(local_sp_2 + (-16L)) = 4215045UL;
    indirect_placeholder();
    var_33 = (uint64_t)*(uint32_t *)var_32;
    *(uint64_t *)(local_sp_2 + (-24L)) = 4215067UL;
    var_34 = indirect_placeholder_85(0UL, var_4, 4325140UL, 0UL, rcx_2, var_33, var_7);
    var_35 = var_34.field_0;
    var_36 = var_34.field_1;
    var_37 = var_34.field_2;
    var_38 = local_sp_2 + (-32L);
    *(uint64_t *)var_38 = 4215075UL;
    var_39 = indirect_placeholder_5(var_12);
    local_sp_0 = var_38;
    rax_0 = var_39;
    r8_0 = var_35;
    rcx_0 = var_36;
    r9_0 = var_37;
    if ((uint64_t)(uint32_t)var_39 != 0UL) {
        return (uint64_t)(uint32_t)r12_0;
    }
    *(uint64_t *)(local_sp_0 + (-8L)) = 4215084UL;
    indirect_placeholder();
    var_42 = (uint64_t)*(uint32_t *)rax_0;
    *(uint64_t *)(local_sp_0 + (-16L)) = 4215106UL;
    indirect_placeholder_84(0UL, r8_0, 4325186UL, 0UL, rcx_0, var_42, r9_0);
    return 4UL;
    *(uint64_t *)(local_sp_0 + (-8L)) = 4215084UL;
    indirect_placeholder();
    var_42 = (uint64_t)*(uint32_t *)rax_0;
    *(uint64_t *)(local_sp_0 + (-16L)) = 4215106UL;
    indirect_placeholder_84(0UL, r8_0, 4325186UL, 0UL, rcx_0, var_42, r9_0);
    return 4UL;
}
