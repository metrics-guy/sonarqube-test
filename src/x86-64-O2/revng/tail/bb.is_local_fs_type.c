typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
uint64_t bb_is_local_fs_type(uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_24;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_50;
    uint64_t var_52;
    uint64_t var_51;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t merge;
    uint64_t var_35;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_25;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_13;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_64;
    uint64_t var_65;
    revng_init_local_sp(0UL);
    var_0 = init_cc_src2();
    var_1 = rdi + (-1702057283L);
    merge = 0UL;
    if (var_1 == 0UL) {
        return merge;
    }
    var_2 = helper_cc_compute_all_wrapper(var_1, 1702057283UL, var_0, 17U);
    if ((var_2 & 65UL) == 0UL) {
        var_60 = rdi + (-2088527475L);
        if (var_60 != 0UL) {
            var_61 = helper_cc_compute_all_wrapper(var_60, 2088527475UL, var_0, 17U);
            merge = 1UL;
            if ((var_61 & 65UL) != 0UL) {
                var_62 = rdi + (-1935894131L);
                if (var_62 != 0UL) {
                    var_63 = helper_cc_compute_all_wrapper(var_62, 1935894131UL, var_0, 17U);
                    merge = 0UL;
                    if ((var_63 & 65UL) != 0UL) {
                        var_64 = rdi + (-1799439955L);
                        if (var_64 != 0UL) {
                            var_65 = helper_cc_compute_all_wrapper(var_64, 1799439955UL, var_0, 17U);
                            if ((var_65 & 65UL) != 0UL) {
                                merge = 4294967295UL;
                                merge = 1UL;
                                if (rdi != 1702057286UL & rdi <= 1702057285UL & rdi == 1733912937UL) {
                                    return (rdi == 1746473250UL) ? 1UL : 4294967295UL;
                                }
                            }
                            if (rdi == 1852207972UL) {
                                return (rdi == 1853056627UL) ? 1UL : 4294967295UL;
                            }
                        }
                    }
                    var_66 = rdi + (-1952539503L);
                    if (var_66 != 0UL) {
                        var_67 = helper_cc_compute_all_wrapper(var_66, 1952539503UL, var_0, 17U);
                        merge = 1UL;
                        if ((var_67 & 65UL) != 0UL) {
                            var_68 = rdi + (-1937076805L);
                            merge = 0UL;
                            var_69 = helper_cc_compute_all_wrapper(var_68, 1937076805UL, var_0, 17U);
                            merge = 4294967295UL;
                            merge = 1UL;
                            if (var_68 != 0UL & (var_69 & 65UL) != 0UL & rdi == 1936814952UL) {
                                return (rdi == 1936880249UL) ? 1UL : 4294967295UL;
                            }
                        }
                        if (rdi == 1953653091UL) {
                            return (rdi == 2035054128UL) ? 0UL : 4294967295UL;
                        }
                    }
                }
            }
            var_70 = rdi + (-3203391149L);
            if (var_70 != 0UL) {
                var_71 = helper_cc_compute_all_wrapper(var_70, 3203391149UL, var_0, 17U);
                if ((var_71 & 65UL) != 0UL) {
                    var_72 = rdi + (-2866260714L);
                    if (var_72 != 0UL) {
                        var_73 = helper_cc_compute_all_wrapper(var_72, 2866260714UL, var_0, 17U);
                        merge = 1UL;
                        if ((var_73 & 65UL) == 0UL) {
                            if (rdi == 2881100148UL) {
                                return (rdi == 3133910204UL) ? 0UL : 4294967295UL;
                            }
                        }
                        var_74 = rdi + (-2768370933L);
                        merge = 0UL;
                        var_75 = helper_cc_compute_all_wrapper(var_74, 2768370933UL, var_0, 17U);
                        merge = 4294967295UL;
                        merge = 1UL;
                        if (var_74 != 0UL & (var_75 & 65UL) != 0UL & rdi == 2435016766UL) {
                            return ((rdi & (-268435457L)) == 2240043254UL) ? 1UL : 4294967295UL;
                        }
                    }
                }
                var_76 = rdi + (-4266872130L);
                if (var_76 != 0UL) {
                    var_77 = helper_cc_compute_all_wrapper(var_76, 4266872130UL, var_0, 17U);
                    merge = 1UL;
                    if ((var_77 & 65UL) == 0UL) {
                        return (rdi == 4283649346UL) ? 0UL : 4294967295UL;
                    }
                    var_78 = rdi + (-4076150800L);
                    if (var_78 != 0UL) {
                        var_79 = helper_cc_compute_all_wrapper(var_78, 4076150800UL, var_0, 17U);
                        if ((var_79 & 65UL) == 0UL) {
                            if (rdi == 4185718668UL) {
                                return (rdi == 4187351113UL) ? 1UL : 4294967295UL;
                            }
                        }
                        break;
                    }
                }
            }
        }
    }
    var_3 = rdi + (-198183888L);
    if (var_3 == 0UL) {
        return;
    }
    var_4 = helper_cc_compute_all_wrapper(var_3, 198183888UL, var_0, 17U);
    merge = 1UL;
    if ((var_4 & 65UL) == 0UL) {
        var_36 = rdi + (-1397109069L);
        if (var_36 != 0UL) {
            var_37 = helper_cc_compute_all_wrapper(var_36, 1397109069UL, var_0, 17U);
            merge = 0UL;
            if ((var_37 & 65UL) == 0UL) {
                var_50 = rdi + (-1633904243L);
                if (var_50 != 0UL) {
                    var_51 = helper_cc_compute_all_wrapper(var_50, 1633904243UL, var_0, 17U);
                    if ((var_51 & 65UL) == 0UL) {
                        var_56 = rdi + (-1635083891L);
                        if (var_56 != 0UL) {
                            var_57 = helper_cc_compute_all_wrapper(var_56, 1635083891UL, var_0, 17U);
                            merge = 1UL;
                            if ((var_57 & 65UL) == 0UL) {
                                return (rdi == 1634035564UL) ? 1UL : 4294967295UL;
                            }
                            var_58 = rdi + (-1667723888L);
                            if (var_58 != 0UL) {
                                var_59 = helper_cc_compute_all_wrapper(var_58, 1667723888UL, var_0, 17U);
                                if ((var_59 & 65UL) != 0UL) {
                                    if (((rdi + (-1650812272L)) & (-3L)) == 0UL) {
                                        return (rdi == 1650746742UL) ? 1UL : 4294967295UL;
                                    }
                                }
                                if (rdi == 1684170528UL) {
                                    return (rdi == 1684300152UL) ? 1UL : 4294967295UL;
                                }
                            }
                        }
                    }
                    merge = 4294967295UL;
                    var_52 = rdi + (-1448756819L);
                    merge = 1UL;
                    if (rdi != 1397113167UL & rdi <= 1397113166UL & var_52 != 0UL) {
                        var_53 = helper_cc_compute_all_wrapper(var_52, 1448756819UL, var_0, 17U);
                        if ((var_53 & 65UL) == 0UL) {
                            break;
                        }
                        var_54 = rdi + (-1397703499L);
                        if (var_54 != 0UL) {
                            var_55 = helper_cc_compute_all_wrapper(var_54, 1397703499UL, var_0, 17U);
                            if ((var_55 & 65UL) == 0UL) {
                                return (rdi == 1410924800UL) ? 1UL : 4294967295UL;
                            }
                            if (rdi == 1397114950UL) {
                                return (rdi == 1397118030UL) ? 1UL : 4294967295UL;
                            }
                        }
                    }
                }
            }
            var_38 = rdi + (-1196443219L);
            if (var_38 != 0UL) {
                var_39 = helper_cc_compute_all_wrapper(var_38, 1196443219UL, var_0, 17U);
                if ((var_39 & 65UL) == 0UL) {
                    if (rdi == 1346981957UL) {
                        return (rdi == 1382369651UL) ? 1UL : 4294967295UL;
                    }
                }
                var_40 = rdi + (-428016422L);
                if (var_40 != 0UL) {
                    var_41 = helper_cc_compute_all_wrapper(var_40, 428016422UL, var_0, 17U);
                    merge = 1UL;
                    if ((var_41 & 65UL) != 0UL) {
                        var_42 = rdi + (-352400198L);
                        if (var_42 != 0UL) {
                            var_43 = helper_cc_compute_all_wrapper(var_42, 352400198UL, var_0, 17U);
                            if ((var_43 & 65UL) == 0UL) {
                                return (rdi == 427819522UL) ? 1UL : 4294967295UL;
                            }
                            if (rdi == 288389204UL) {
                                return (rdi == 325456742UL) ? 1UL : 4294967295UL;
                            }
                        }
                    }
                    var_44 = rdi + (-827541066L);
                    if (var_44 != 0UL) {
                        var_45 = helper_cc_compute_all_wrapper(var_44, 827541066UL, var_0, 17U);
                        if ((var_45 & 65UL) == 0UL) {
                            var_48 = rdi + (-1128357203L);
                            if (var_48 != 0UL) {
                                var_49 = helper_cc_compute_all_wrapper(var_48, 1128357203UL, var_0, 17U);
                                if ((var_49 & 65UL) == 0UL) {
                                    return (rdi == 1161678120UL) ? 1UL : 4294967295UL;
                                }
                                if (rdi == 1111905073UL) {
                                    return (rdi == 1112100429UL) ? 1UL : 4294967295UL;
                                }
                            }
                        }
                        var_46 = rdi + (-684539205L);
                        if (var_46 != 0UL) {
                            var_47 = helper_cc_compute_all_wrapper(var_46, 684539205UL, var_0, 17U);
                            if ((var_47 & 65UL) != 0UL) {
                                if (rdi == 464386766UL) {
                                    return (rdi == 604313861UL) ? 1UL : 4294967295UL;
                                }
                            }
                            if (rdi == 732765674UL) {
                                return (rdi == 801189825UL) ? 1UL : 4294967295UL;
                            }
                        }
                    }
                }
            }
        }
    }
    var_5 = rdi + (-29301L);
    if (var_5 != 0UL) {
        var_6 = helper_cc_compute_all_wrapper(var_5, 29301UL, var_0, 17U);
        merge = 0UL;
        if ((var_6 & 65UL) == 0UL) {
            var_20 = rdi + (-18225520L);
            if (var_20 != 0UL) {
                var_21 = helper_cc_compute_all_wrapper(var_20, 18225520UL, var_0, 17U);
                if ((var_21 & 65UL) == 0UL) {
                    var_33 = rdi + (-19993000L);
                    if (var_33 != 0UL) {
                        var_34 = helper_cc_compute_all_wrapper(var_33, 19993000UL, var_0, 17U);
                        merge = 1UL;
                        if ((var_34 & 65UL) == 0UL) {
                            break;
                        }
                        if (rdi == 19911021UL) {
                            var_35 = helper_cc_compute_c_wrapper(rdi + (-19920824L), 4UL, var_0, 17U);
                            return (uint64_t)((uint32_t)((0UL - var_35) & 2UL) + (-1));
                        }
                    }
                }
                var_22 = rdi + (-12805120L);
                if (var_22 != 0UL) {
                    var_23 = helper_cc_compute_all_wrapper(var_22, 12805120UL, var_0, 17U);
                    merge = 1UL;
                    if ((var_23 & 65UL) == 0UL) {
                        if (rdi == 16914836UL) {
                            return (rdi == 16914839UL) ? 1UL : 4294967295UL;
                        }
                    }
                    var_24 = rdi + (-61267L);
                    if (var_24 != 0UL) {
                        var_25 = helper_cc_compute_all_wrapper(var_24, 61267UL, var_0, 17U);
                        if ((var_25 & 65UL) == 0UL) {
                            var_31 = rdi + (-2613483L);
                            if (var_31 != 0UL) {
                                var_32 = helper_cc_compute_all_wrapper(var_31, 2613483UL, var_0, 17U);
                                if ((var_32 & 65UL) != 0UL) {
                                    if (rdi == 61791UL) {
                                        return (rdi == 72020UL) ? 1UL : 4294967295UL;
                                    }
                                }
                                if (rdi == 4278867UL) {
                                    return (rdi == 12648430UL) ? 1UL : 4294967295UL;
                                }
                            }
                        }
                        var_26 = rdi + (-44533L);
                        if (var_26 != 0UL) {
                            var_27 = helper_cc_compute_all_wrapper(var_26, 44533UL, var_0, 17U);
                            if ((var_27 & 65UL) == 0UL) {
                                if (rdi == 44543UL) {
                                    return (rdi == 61265UL) ? 1UL : 4294967295UL;
                                }
                            }
                            var_28 = rdi + (-38496L);
                            if (var_28 != 0UL) {
                                var_29 = helper_cc_compute_all_wrapper(var_28, 38496UL, var_0, 17U);
                                if ((var_29 & 65UL) != 0UL) {
                                    return (rdi == 29366UL) ? 1UL : 4294967295UL;
                                }
                                var_30 = helper_cc_compute_c_wrapper(rdi + (-40867L), 3UL, var_0, 17U);
                                return (uint64_t)((uint32_t)((0UL - var_30) & 2UL) + (-1));
                            }
                        }
                    }
                }
            }
        }
        var_7 = rdi + (-22092L);
        if (var_7 != 0UL) {
            var_8 = helper_cc_compute_all_wrapper(var_7, 22092UL, var_0, 17U);
            merge = 1UL;
            if ((var_8 & 65UL) == 0UL) {
                if (rdi == 24053UL) {
                    return (rdi == 26985UL) ? 0UL : 4294967295UL;
                }
            }
            var_9 = rdi + (-20859L);
            merge = 0UL;
            var_10 = helper_cc_compute_all_wrapper(var_9, 20859UL, var_0, 17U);
            merge = 4294967295UL;
            var_11 = rdi + (-9336L);
            merge = 1UL;
            if (var_9 != 0UL & (var_10 & 65UL) != 0UL & var_11 != 0UL) {
                var_12 = helper_cc_compute_all_wrapper(var_11, 9336UL, var_0, 17U);
                if ((var_12 & 65UL) == 0UL) {
                    var_14 = rdi + (-16964L);
                    if (var_14 != 0UL) {
                        var_15 = helper_cc_compute_all_wrapper(var_14, 16964UL, var_0, 17U);
                        if ((var_15 & 65UL) == 0UL) {
                            var_18 = rdi + (-19780L);
                            if (var_18 != 0UL) {
                                var_19 = helper_cc_compute_all_wrapper(var_18, 19780UL, var_0, 17U);
                                if ((var_19 & 65UL) == 0UL) {
                                    return (rdi == 19802UL) ? 1UL : 4294967295UL;
                                }
                                if (rdi == 18475UL) {
                                    return (rdi == 18520UL) ? 1UL : 4294967295UL;
                                }
                            }
                        }
                        var_16 = rdi + (-16384L);
                        merge = 4294967295UL;
                        if (rdi != 13364UL & var_16 > 6UL) {
                            var_17 = helper_cc_compute_c_wrapper(((1UL << (var_16 & 63UL)) & 81UL) + (-1L), 1UL, var_0, 17U);
                            return (uint64_t)((0U - (uint32_t)var_17) & (-2)) | 1UL;
                        }
                    }
                }
                if (rdi > 5007UL) {
                    if (rdi == 7377UL) {
                        return (rdi == 9320UL) ? 1UL : 4294967295UL;
                    }
                }
                if (rdi <= 4978UL) {
                    break;
                }
                var_13 = helper_cc_compute_c_wrapper((uint64_t)((uint32_t)(1UL << ((rdi + 13UL) & 63UL)) & 268440577U) + (-1L), 1UL, var_0, 17U);
                return (uint64_t)((0U - (uint32_t)var_13) & (-2)) | 1UL;
            }
        }
    }
}
