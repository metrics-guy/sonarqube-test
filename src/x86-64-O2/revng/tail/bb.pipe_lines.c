typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_35_ret_type;
struct indirect_placeholder_36_ret_type;
struct indirect_placeholder_33_ret_type;
struct indirect_placeholder_32_ret_type;
struct indirect_placeholder_38_ret_type;
struct indirect_placeholder_34_ret_type;
struct indirect_placeholder_37_ret_type;
struct indirect_placeholder_35_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_36_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
};
struct indirect_placeholder_33_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_32_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_38_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_34_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_37_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r12(void);
extern uint64_t init_r15(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_16(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_35_ret_type indirect_placeholder_35(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_36_ret_type indirect_placeholder_36(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_33_ret_type indirect_placeholder_33(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_32_ret_type indirect_placeholder_32(uint64_t param_0);
extern struct indirect_placeholder_38_ret_type indirect_placeholder_38(uint64_t param_0);
extern struct indirect_placeholder_34_ret_type indirect_placeholder_34(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_37_ret_type indirect_placeholder_37(uint64_t param_0);
uint64_t bb_pipe_lines(uint64_t rdx, uint64_t rdi, uint64_t rcx, uint64_t rsi) {
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_67;
    uint64_t r14_1;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t rsi4_0_in;
    uint64_t local_sp_5;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t *var_73;
    uint64_t var_74;
    uint64_t rdi2_2;
    uint64_t local_sp_6;
    uint64_t var_81;
    uint64_t var_82;
    struct indirect_placeholder_35_ret_type var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_90;
    uint64_t local_sp_8_ph;
    struct indirect_placeholder_36_ret_type var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t var_31;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t *var_8;
    struct indirect_placeholder_32_ret_type var_9;
    uint64_t var_10;
    uint64_t var_11;
    struct indirect_placeholder_38_ret_type var_12;
    uint64_t var_13;
    uint64_t r12_7_ph_ph;
    uint64_t r12_0;
    uint64_t rbx_0;
    uint64_t local_sp_0;
    uint64_t var_86;
    uint64_t var_87;
    struct indirect_placeholder_34_ret_type var_88;
    uint64_t var_89;
    uint64_t r12_3_ph;
    uint64_t var_77;
    uint64_t *var_78;
    uint64_t var_79;
    uint64_t rdi2_1;
    uint64_t var_80;
    uint64_t r12_6;
    uint64_t r12_1;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t rdi2_0;
    uint64_t local_sp_2;
    uint64_t r12_5;
    uint64_t var_49;
    uint64_t *var_50;
    uint64_t var_51;
    bool var_52;
    uint64_t var_53;
    bool var_54;
    uint64_t local_sp_8;
    uint64_t r12_2;
    uint64_t local_sp_3;
    uint64_t local_sp_8_ph_ph;
    uint64_t r12_4;
    uint64_t r13_1_ph;
    uint64_t local_sp_4_ph;
    uint64_t r13_1;
    uint64_t local_sp_4;
    uint64_t var_97;
    uint64_t var_98;
    unsigned char var_55;
    uint64_t var_56;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t *var_57;
    uint64_t var_58;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_66;
    uint64_t rax_0;
    uint64_t r14_0;
    uint64_t var_32;
    uint64_t *var_33;
    uint64_t rdi2_3;
    bool var_34;
    uint64_t var_35;
    uint64_t var_41;
    uint64_t var_46;
    uint64_t r15_0_ph_ph;
    uint64_t r13_2_ph_ph;
    uint64_t *var_14;
    uint64_t r12_7_ph;
    uint64_t r15_0_ph;
    uint64_t *var_15;
    uint64_t *var_16;
    uint64_t *var_17;
    uint64_t r12_7;
    uint64_t var_42;
    uint64_t *var_43;
    struct indirect_placeholder_37_ret_type var_44;
    uint64_t var_45;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t *var_38;
    uint64_t var_39;
    uint64_t *var_40;
    uint64_t r14_2;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t *var_24;
    uint64_t var_25;
    uint64_t local_sp_7;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t *var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_47;
    uint64_t *var_48;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t *var_20;
    uint64_t var_21;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r12();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_r13();
    var_5 = init_r14();
    var_6 = init_rbp();
    var_7 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_1;
    *(uint64_t *)(var_0 + (-40L)) = var_6;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    *(uint64_t *)(var_0 + (-64L)) = rdi;
    *(uint32_t *)(var_0 + (-76L)) = (uint32_t)rsi;
    *(uint64_t *)(var_0 + (-88L)) = rdx;
    *(uint64_t *)(var_0 + (-72L)) = rcx;
    var_8 = (uint64_t *)(var_0 + (-112L));
    *var_8 = 4210843UL;
    var_9 = indirect_placeholder_32(1048UL);
    var_10 = var_9.field_0;
    *(uint64_t *)(var_10 + 1032UL) = 0UL;
    *(uint64_t *)(var_10 + 1024UL) = 0UL;
    *(uint64_t *)(var_10 + 1040UL) = 0UL;
    *var_8 = var_10;
    var_11 = var_0 + (-120L);
    *(uint64_t *)var_11 = 4210893UL;
    var_12 = indirect_placeholder_38(1048UL);
    var_13 = var_12.field_0;
    *var_8 = 0UL;
    r12_7_ph_ph = var_1;
    r12_3_ph = 1UL;
    r12_2 = 0UL;
    local_sp_8_ph_ph = var_11;
    r15_0_ph_ph = var_13;
    r13_2_ph_ph = var_10;
    r14_2 = 0UL;
    while (1U)
        {
            var_14 = (uint64_t *)(r13_2_ph_ph + 1032UL);
            r14_1 = r13_2_ph_ph;
            local_sp_8_ph = local_sp_8_ph_ph;
            r13_1_ph = r13_2_ph_ph;
            r14_0 = r13_2_ph_ph;
            r15_0_ph_ph = r13_2_ph_ph;
            r12_7_ph = r12_7_ph_ph;
            r15_0_ph = r15_0_ph_ph;
            while (1U)
                {
                    var_15 = (uint64_t *)(r15_0_ph + 1024UL);
                    var_16 = (uint64_t *)(r15_0_ph + 1032UL);
                    var_17 = (uint64_t *)(r15_0_ph + 1040UL);
                    rdi2_3 = r15_0_ph;
                    r12_7 = r12_7_ph;
                    local_sp_8 = local_sp_8_ph;
                    while (1U)
                        {
                            var_18 = (uint64_t)*(uint32_t *)(local_sp_8 + 28UL);
                            var_19 = local_sp_8 + (-8L);
                            var_20 = (uint64_t *)var_19;
                            *var_20 = 4210929UL;
                            var_21 = indirect_placeholder_16(1024UL, var_18, r15_0_ph);
                            local_sp_7 = var_19;
                            if ((var_21 + (-1L)) <= 18446744073709551613UL) {
                                var_47 = local_sp_8 + (-16L);
                                var_48 = (uint64_t *)var_47;
                                *var_48 = 4211240UL;
                                indirect_placeholder_1();
                                local_sp_3 = var_47;
                                local_sp_5 = var_47;
                                local_sp_6 = var_47;
                                if (var_21 == 18446744073709551615UL) {
                                    var_49 = *var_48;
                                    var_50 = (uint64_t *)local_sp_8;
                                    var_51 = *var_50;
                                    var_52 = (var_51 == 0UL);
                                    var_53 = *(uint64_t *)(var_49 + 1024UL);
                                    var_54 = var_52 || (var_53 == 0UL);
                                    r12_2 = var_54 | 1024UL;
                                    var_59 = var_51;
                                    if (!var_54) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    var_55 = *(unsigned char *)4310008UL;
                                    var_56 = (uint64_t)var_55;
                                    rsi4_0_in = var_56;
                                    if ((uint64_t)(*(unsigned char *)((var_53 + var_49) + (-1L)) - var_55) == 0UL) {
                                        var_60 = *var_20;
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    var_57 = (uint64_t *)(var_49 + 1032UL);
                                    *var_57 = (*var_57 + 1UL);
                                    var_58 = *var_20 + 1UL;
                                    *var_20 = var_58;
                                    var_59 = *var_50;
                                    var_60 = var_58;
                                    loop_state_var = 1U;
                                    break;
                                }
                                var_90 = *(uint64_t *)(local_sp_8 + 24UL);
                                *(uint64_t *)(local_sp_8 + (-24L)) = 4211613UL;
                                var_91 = indirect_placeholder_36(r12_7, r15_0_ph, 4UL, var_90);
                                var_92 = var_91.field_0;
                                var_93 = var_91.field_2;
                                var_94 = var_91.field_7;
                                *(uint64_t *)(local_sp_8 + (-32L)) = 4211621UL;
                                indirect_placeholder_1();
                                var_95 = (uint64_t)*(uint32_t *)var_92;
                                var_96 = local_sp_8 + (-40L);
                                *(uint64_t *)var_96 = 4211643UL;
                                indirect_placeholder_33(0UL, var_93, 4276317UL, 0UL, var_92, var_95, var_94);
                                local_sp_3 = var_96;
                                loop_state_var = 0U;
                                break;
                            }
                            var_22 = *(uint64_t *)(local_sp_8 + 24UL);
                            *var_15 = var_21;
                            var_23 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)4310008UL;
                            var_24 = (uint64_t *)var_22;
                            *var_24 = (*var_24 + var_21);
                            var_25 = var_21 + r15_0_ph;
                            *var_16 = 0UL;
                            *var_17 = 0UL;
                            r12_7_ph_ph = var_23;
                            r12_7_ph = var_23;
                            r12_7 = var_23;
                            var_26 = var_25 - rdi2_3;
                            var_27 = local_sp_7 + (-8L);
                            var_28 = (uint64_t *)var_27;
                            *var_28 = 4211036UL;
                            var_29 = indirect_placeholder_16(var_26, rdi2_3, var_23);
                            var_30 = r14_2 + 1UL;
                            local_sp_8_ph_ph = var_27;
                            r14_2 = var_30;
                            local_sp_7 = var_27;
                            while (var_29 != 0UL)
                                {
                                    *var_16 = var_30;
                                    rdi2_3 = var_29 + 1UL;
                                    var_26 = var_25 - rdi2_3;
                                    var_27 = local_sp_7 + (-8L);
                                    var_28 = (uint64_t *)var_27;
                                    *var_28 = 4211036UL;
                                    var_29 = indirect_placeholder_16(var_26, rdi2_3, var_23);
                                    var_30 = r14_2 + 1UL;
                                    local_sp_8_ph_ph = var_27;
                                    r14_2 = var_30;
                                    local_sp_7 = var_27;
                                }
                            var_31 = *var_28;
                            var_32 = *var_15;
                            var_33 = (uint64_t *)local_sp_7;
                            *var_33 = (*var_33 + r14_2);
                            var_34 = ((*(uint64_t *)(var_31 + 1024UL) + var_32) > 1023UL);
                            var_35 = *var_28;
                            if (!var_34) {
                                loop_state_var = 2U;
                                break;
                            }
                            var_36 = local_sp_7 + (-16L);
                            *(uint64_t *)var_36 = 4211159UL;
                            indirect_placeholder_1();
                            var_37 = *var_15;
                            var_38 = (uint64_t *)(var_35 + 1024UL);
                            *var_38 = (*var_38 + var_37);
                            var_39 = *var_16;
                            var_40 = (uint64_t *)(var_35 + 1032UL);
                            *var_40 = (*var_40 + var_39);
                            local_sp_8 = var_36;
                            continue;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 2U:
                        {
                            *(uint64_t *)(var_35 + 1040UL) = r15_0_ph;
                            var_41 = *var_33 - *var_14;
                            if (var_41 <= *(uint64_t *)(local_sp_7 + 8UL)) {
                                loop_state_var = 4U;
                                switch_state_var = 1;
                                break;
                            }
                            var_42 = local_sp_7 + (-16L);
                            var_43 = (uint64_t *)var_42;
                            *var_43 = 4211210UL;
                            var_44 = indirect_placeholder_37(1048UL);
                            var_45 = var_44.field_0;
                            *var_43 = r15_0_ph;
                            r15_0_ph = var_45;
                            local_sp_8_ph = var_42;
                            continue;
                        }
                        break;
                      case 0U:
                        {
                            r12_3_ph = r12_2;
                            local_sp_4_ph = local_sp_3;
                            r12_4 = r12_2;
                            if (r13_2_ph_ph != 0UL) {
                                loop_state_var = 3U;
                                switch_state_var = 1;
                                break;
                            }
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 1U:
                        {
                            var_61 = var_60 - *var_14;
                            var_62 = helper_cc_compute_c_wrapper(var_59 - var_61, var_61, var_7, 17U);
                            rax_0 = var_61;
                            if (var_62 != 0UL) {
                                loop_state_var = 2U;
                                switch_state_var = 1;
                                break;
                            }
                            var_66 = *var_20;
                            var_67 = *var_50;
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 4U:
                {
                    *var_28 = r15_0_ph;
                    var_46 = *(uint64_t *)(r13_2_ph_ph + 1040UL);
                    *var_33 = var_41;
                    r13_2_ph_ph = var_46;
                    continue;
                }
                break;
              case 0U:
                {
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 1U:
                {
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 2U:
                {
                    loop_state_var = 2U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 3U:
                {
                    loop_state_var = 3U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            return (uint64_t)(uint32_t)r12_4;
        }
        break;
      case 3U:
      case 2U:
      case 1U:
        {
            switch (loop_state_var) {
              case 3U:
                {
                    r13_1 = r13_1_ph;
                    local_sp_4 = local_sp_4_ph;
                    r12_4 = r12_3_ph;
                    var_97 = *(uint64_t *)(r13_1 + 1040UL);
                    var_98 = local_sp_4 + (-8L);
                    *(uint64_t *)var_98 = 4211311UL;
                    indirect_placeholder_1();
                    r13_1 = var_97;
                    local_sp_4 = var_98;
                    do {
                        var_97 = *(uint64_t *)(r13_1 + 1040UL);
                        var_98 = local_sp_4 + (-8L);
                        *(uint64_t *)var_98 = 4211311UL;
                        indirect_placeholder_1();
                        r13_1 = var_97;
                        local_sp_4 = var_98;
                    } while (var_97 != 0UL);
                }
                break;
              case 2U:
              case 1U:
                {
                    switch (loop_state_var) {
                      case 2U:
                        {
                            var_63 = *(uint64_t *)(r14_0 + 1040UL);
                            *var_20 = rax_0;
                            var_64 = rax_0 - *(uint64_t *)(var_63 + 1032UL);
                            var_65 = *var_50;
                            var_67 = var_65;
                            r14_1 = var_63;
                            var_66 = rax_0;
                            rax_0 = var_64;
                            r14_0 = var_63;
                            do {
                                var_63 = *(uint64_t *)(r14_0 + 1040UL);
                                *var_20 = rax_0;
                                var_64 = rax_0 - *(uint64_t *)(var_63 + 1032UL);
                                var_65 = *var_50;
                                var_67 = var_65;
                                r14_1 = var_63;
                                var_66 = rax_0;
                                rax_0 = var_64;
                                r14_0 = var_63;
                            } while (var_64 <= var_65);
                        }
                        break;
                      case 1U:
                        {
                            var_68 = *(uint64_t *)(r14_1 + 1024UL) + r14_1;
                            var_69 = helper_cc_compute_c_wrapper(var_67 - var_66, var_66, var_7, 17U);
                            rdi2_1 = r14_1;
                            r12_6 = var_66;
                            rdi2_2 = r14_1;
                            if (var_69 != 0UL) {
                                r12_5 = var_66 - var_67;
                                r12_6 = 0UL;
                                while (1U)
                                    {
                                        var_70 = (uint64_t)(uint32_t)rsi4_0_in;
                                        var_71 = var_68 - rdi2_1;
                                        var_72 = local_sp_5 + (-8L);
                                        var_73 = (uint64_t *)var_72;
                                        *var_73 = 4211484UL;
                                        var_74 = indirect_placeholder_16(var_71, rdi2_1, var_70);
                                        local_sp_2 = var_72;
                                        local_sp_6 = var_72;
                                        if (var_74 == 0UL) {
                                            *var_73 = 0UL;
                                            var_77 = local_sp_5 + (-16L);
                                            var_78 = (uint64_t *)var_77;
                                            *var_78 = 4211518UL;
                                            indirect_placeholder_1();
                                            var_79 = *var_78 + 1UL;
                                            var_80 = r12_5 + (-1L);
                                            rdi2_2 = var_79;
                                            local_sp_6 = var_77;
                                            r12_1 = var_80;
                                            rdi2_0 = var_79;
                                            local_sp_2 = var_77;
                                            if (var_80 == 0UL) {
                                                break;
                                            }
                                        }
                                        var_75 = var_74 + 1UL;
                                        var_76 = r12_5 + (-1L);
                                        r12_1 = var_76;
                                        rdi2_0 = var_75;
                                        rdi2_2 = var_75;
                                        if (var_76 == 0UL) {
                                            break;
                                        }
                                        r12_5 = r12_1;
                                        rdi2_1 = rdi2_0;
                                        rsi4_0_in = (uint64_t)*(unsigned char *)4310008UL;
                                        local_sp_5 = local_sp_2;
                                        continue;
                                    }
                            }
                            var_81 = var_68 - rdi2_2;
                            var_82 = local_sp_6 + (-8L);
                            *(uint64_t *)var_82 = 4211543UL;
                            var_83 = indirect_placeholder_35(r12_6, var_49, var_81);
                            var_84 = var_83.field_3;
                            var_85 = *(uint64_t *)(var_83.field_4 + 1040UL);
                            rbx_0 = var_85;
                            local_sp_0 = var_82;
                            r13_1_ph = var_84;
                            local_sp_4_ph = var_82;
                            if (var_85 != 0UL) {
                                r12_0 = var_83.field_0;
                                var_86 = *(uint64_t *)(rbx_0 + 1024UL);
                                var_87 = local_sp_0 + (-8L);
                                *(uint64_t *)var_87 = 4211575UL;
                                var_88 = indirect_placeholder_34(r12_0, rbx_0, rbx_0, var_86);
                                var_89 = *(uint64_t *)(var_88.field_1 + 1040UL);
                                rbx_0 = var_89;
                                local_sp_0 = var_87;
                                local_sp_4_ph = var_87;
                                while (var_89 != 0UL)
                                    {
                                        r12_0 = var_88.field_0;
                                        var_86 = *(uint64_t *)(rbx_0 + 1024UL);
                                        var_87 = local_sp_0 + (-8L);
                                        *(uint64_t *)var_87 = 4211575UL;
                                        var_88 = indirect_placeholder_34(r12_0, rbx_0, rbx_0, var_86);
                                        var_89 = *(uint64_t *)(var_88.field_1 + 1040UL);
                                        rbx_0 = var_89;
                                        local_sp_0 = var_87;
                                        local_sp_4_ph = var_87;
                                    }
                                r13_1_ph = var_88.field_3;
                            }
                        }
                        break;
                    }
                }
                break;
            }
        }
        break;
    }
}
