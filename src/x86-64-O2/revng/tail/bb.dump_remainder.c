typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_dump_remainder_ret_type;
struct indirect_placeholder_50_ret_type;
struct indirect_placeholder_49_ret_type;
struct indirect_placeholder_51_ret_type;
struct bb_dump_remainder_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_50_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
};
struct indirect_placeholder_49_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_51_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t init_r15(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_16(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_52(uint64_t param_0);
extern struct indirect_placeholder_50_ret_type indirect_placeholder_50(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_49_ret_type indirect_placeholder_49(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_51_ret_type indirect_placeholder_51(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
typedef _Bool bool;
struct bb_dump_remainder_ret_type bb_dump_remainder(uint64_t rdx, uint64_t rdi, uint64_t rcx, uint64_t rsi) {
    struct indirect_placeholder_51_ret_type var_27;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t local_sp_1;
    uint64_t r12_1;
    uint64_t var_16;
    uint64_t rbx_1;
    struct indirect_placeholder_50_ret_type var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_32;
    uint64_t local_sp_0;
    uint64_t rbx_0;
    uint64_t rbp_0;
    uint64_t var_10;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t r13_0;
    uint64_t r14_0;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t r12_0;
    struct bb_dump_remainder_ret_type mrv;
    struct bb_dump_remainder_ret_type mrv1;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t *var_14;
    uint64_t var_15;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r12();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_r13();
    var_5 = init_r14();
    var_6 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    var_7 = (uint64_t)(uint32_t)rdx;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_1;
    *(uint64_t *)(var_0 + (-40L)) = var_6;
    var_8 = (uint64_t)(uint32_t)rdi;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_9 = var_0 + (-1096L);
    *(uint64_t *)(var_0 + (-1088L)) = rsi;
    local_sp_1 = var_9;
    r12_1 = 0UL;
    rbx_1 = rcx;
    rbp_0 = var_8;
    r13_0 = rcx;
    r14_0 = var_7;
    while (1U)
        {
            var_10 = local_sp_1 + 16UL;
            var_11 = (uint64_t)(uint32_t)r14_0;
            var_12 = (rbx_1 < 1024UL) ? rbx_1 : 1024UL;
            var_13 = local_sp_1 + (-8L);
            var_14 = (uint64_t *)var_13;
            *var_14 = 4209157UL;
            var_15 = indirect_placeholder_16(var_12, var_11, var_10);
            local_sp_0 = var_13;
            r12_0 = r12_1;
            rbp_0 = 0UL;
            switch_state_var = 0;
            switch (var_15) {
              case 0UL:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 18446744073709551615UL:
                {
                    *(uint64_t *)(local_sp_1 + (-16L)) = 4209221UL;
                    indirect_placeholder_1();
                    if (*(uint32_t *)18446744073709551615UL != 11U) {
                        switch_state_var = 1;
                        break;
                    }
                    var_16 = *var_14;
                    *(uint64_t *)(local_sp_1 + (-24L)) = 4209275UL;
                    var_17 = indirect_placeholder_50(r12_1, rbp_0, 4UL, var_16);
                    var_18 = var_17.field_0;
                    var_19 = var_17.field_2;
                    var_20 = var_17.field_7;
                    *(uint64_t *)(local_sp_1 + (-32L)) = 4209283UL;
                    indirect_placeholder_1();
                    var_21 = (uint64_t)*(uint32_t *)var_18;
                    *(uint64_t *)(local_sp_1 + (-40L)) = 4209305UL;
                    indirect_placeholder_49(0UL, var_19, 4276317UL, 1UL, var_18, var_21, var_20);
                    abort();
                }
                break;
              default:
                {
                    if ((uint64_t)(unsigned char)rbp_0 == 0UL) {
                        var_22 = *(uint64_t *)local_sp_1;
                        var_23 = local_sp_1 + (-16L);
                        *(uint64_t *)var_23 = 4209258UL;
                        indirect_placeholder_52(var_22);
                        local_sp_0 = var_23;
                    }
                    var_24 = local_sp_0 + 16UL;
                    var_25 = r12_1 + var_15;
                    var_26 = local_sp_0 + (-8L);
                    *(uint64_t *)var_26 = 4209192UL;
                    var_27 = indirect_placeholder_51(var_25, rbx_1, r13_0, var_24, var_15);
                    var_28 = var_27.field_0;
                    var_29 = var_27.field_1;
                    var_30 = var_27.field_3;
                    var_31 = var_27.field_4;
                    local_sp_1 = var_26;
                    r12_1 = var_28;
                    rbx_0 = var_29;
                    r13_0 = var_30;
                    r14_0 = var_31;
                    r12_0 = var_28;
                    if (var_30 != 18446744073709551615UL) {
                        var_32 = var_29 - var_27.field_2;
                        rbx_0 = var_32;
                        if ((var_32 == 0UL) || (var_30 == 18446744073709551614UL)) {
                            switch_state_var = 1;
                            break;
                        }
                    }
                    rbx_1 = rbx_0;
                    continue;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    mrv.field_0 = r12_0;
    mrv1 = mrv;
    mrv1.field_1 = rcx;
    return mrv1;
}
