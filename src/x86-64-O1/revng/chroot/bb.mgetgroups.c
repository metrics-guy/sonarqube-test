typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_25(void);
uint64_t bb_mgetgroups(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint32_t *var_50;
    uint32_t var_51;
    uint64_t var_52;
    uint64_t rax_5;
    uint64_t var_49;
    uint64_t var_34;
    uint32_t _pre_phi;
    uint64_t rax_0;
    uint64_t local_sp_1;
    uint64_t rbx_0;
    uint32_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t rbx_2;
    uint64_t rax_2;
    uint64_t rax_1;
    uint64_t rdx1_0;
    uint64_t rbx_1;
    uint32_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t rax_3;
    uint64_t local_sp_0;
    uint32_t *var_41;
    uint32_t var_42;
    uint32_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint32_t var_33;
    uint64_t rbx_4;
    uint64_t var_38;
    uint64_t var_39;
    uint32_t var_40;
    uint64_t var_25;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_13;
    uint64_t local_sp_3;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t local_sp_2;
    uint32_t *var_14;
    uint32_t var_15;
    uint32_t var_17;
    uint32_t var_21;
    uint32_t var_16;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t rax_6;
    uint32_t var_9;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_22;
    uint32_t *var_23;
    uint32_t var_24;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_rbp();
    var_4 = init_r12();
    var_5 = init_r14();
    var_6 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_5;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_3;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    rax_5 = 4294967295UL;
    if (rdi == 0UL) {
        *(uint64_t *)(var_0 + (-64L)) = 4228992UL;
        var_22 = indirect_placeholder_25();
        var_23 = (uint32_t *)(var_0 + (-52L));
        var_24 = (uint32_t)var_22;
        *var_23 = var_24;
        if ((int)var_24 < (int)0U) {
            *(uint64_t *)(var_0 + (-72L)) = 4229125UL;
            indirect_placeholder();
            *(uint64_t *)(var_0 + (-80L)) = 4229145UL;
            var_49 = indirect_placeholder_1(0UL, 1UL);
            if (*(uint32_t *)var_22 != 38U & var_49 == 0UL) {
                *(uint64_t *)rdx = var_49;
                var_50 = (uint32_t *)var_49;
                var_51 = (uint32_t)rsi;
                *var_50 = var_51;
                var_52 = ((uint64_t)(var_51 + 1U) != 0UL);
                rax_5 = var_52;
            }
        } else {
            var_25 = helper_cc_compute_all_wrapper(var_22, 16UL, var_6, 24U);
            if ((var_25 & 64UL) == 0UL) {
                if ((uint64_t)((uint32_t)rsi + 1U) == 0UL) {
                    var_35 = (uint64_t)*var_23;
                    var_36 = var_0 + (-72L);
                    *(uint64_t *)var_36 = 4229262UL;
                    var_37 = indirect_placeholder_1(0UL, var_35);
                    rbx_4 = var_37;
                    local_sp_1 = var_36;
                    if (var_37 == 0UL) {
                        return rax_5;
                    }
                    var_38 = local_sp_1 + (-8L);
                    *(uint64_t *)var_38 = 4229282UL;
                    var_39 = indirect_placeholder_25();
                    var_40 = (uint32_t)var_39;
                    _pre_phi = var_40;
                    rax_0 = var_39;
                    rbx_0 = rbx_4;
                    rax_3 = var_39;
                    local_sp_0 = var_38;
                    if ((int)var_40 > (int)4294967295U) {
                        *(uint64_t *)(local_sp_0 + (-8L)) = 4229295UL;
                        indirect_placeholder();
                        var_41 = (uint32_t *)rax_3;
                        var_42 = *var_41;
                        *(uint64_t *)(local_sp_0 + (-16L)) = 4229305UL;
                        indirect_placeholder();
                        *(uint64_t *)(local_sp_0 + (-24L)) = 4229310UL;
                        indirect_placeholder();
                        *var_41 = var_42;
                        return rax_5;
                    }
                }
                var_26 = var_22 + 1UL;
                *var_23 = (uint32_t)var_26;
                var_27 = (uint64_t)((long)(var_26 << 32UL) >> (long)32UL);
                var_28 = var_0 + (-72L);
                *(uint64_t *)var_28 = 4229032UL;
                var_29 = indirect_placeholder_1(0UL, var_27);
                rbx_0 = var_29;
                rbx_4 = var_29;
                local_sp_1 = var_28;
                if (var_29 == 0UL) {
                    return rax_5;
                }
                var_30 = (uint32_t)rsi;
                if ((uint64_t)(var_30 + 1U) == 0UL) {
                    var_38 = local_sp_1 + (-8L);
                    *(uint64_t *)var_38 = 4229282UL;
                    var_39 = indirect_placeholder_25();
                    var_40 = (uint32_t)var_39;
                    _pre_phi = var_40;
                    rax_0 = var_39;
                    rbx_0 = rbx_4;
                    rax_3 = var_39;
                    local_sp_0 = var_38;
                    if ((int)var_40 > (int)4294967295U) {
                        *(uint64_t *)(local_sp_0 + (-8L)) = 4229295UL;
                        indirect_placeholder();
                        var_41 = (uint32_t *)rax_3;
                        var_42 = *var_41;
                        *(uint64_t *)(local_sp_0 + (-16L)) = 4229305UL;
                        indirect_placeholder();
                        *(uint64_t *)(local_sp_0 + (-24L)) = 4229310UL;
                        indirect_placeholder();
                        *var_41 = var_42;
                        return rax_5;
                    }
                }
                var_31 = var_0 + (-80L);
                *(uint64_t *)var_31 = 4229070UL;
                var_32 = indirect_placeholder_25();
                var_33 = (uint32_t)var_32;
                rax_3 = var_32;
                local_sp_0 = var_31;
                if ((int)var_33 >= (int)0U) {
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4229295UL;
                    indirect_placeholder();
                    var_41 = (uint32_t *)rax_3;
                    var_42 = *var_41;
                    *(uint64_t *)(local_sp_0 + (-16L)) = 4229305UL;
                    indirect_placeholder();
                    *(uint64_t *)(local_sp_0 + (-24L)) = 4229310UL;
                    indirect_placeholder();
                    *var_41 = var_42;
                    return rax_5;
                }
                *(uint32_t *)var_29 = var_30;
                var_34 = (uint64_t)(var_33 + 1U);
                _pre_phi = (uint32_t)var_34;
                rax_0 = var_34;
                *(uint64_t *)rdx = rbx_0;
                rax_1 = rax_0;
                rbx_1 = rbx_0;
                rax_5 = rax_0;
                var_43 = *(uint32_t *)rbx_0;
                var_44 = (uint64_t)((long)(rax_0 << 32UL) >> (long)30UL) + rbx_0;
                var_45 = rbx_0 + 4UL;
                rdx1_0 = var_45;
                if ((int)_pre_phi <= (int)1U & var_44 > var_45) {
                    var_48 = rdx1_0 + 4UL;
                    rax_1 = rax_2;
                    rdx1_0 = var_48;
                    rbx_1 = rbx_2;
                    rax_5 = rax_2;
                    do {
                        var_46 = *(uint32_t *)rdx1_0;
                        rax_2 = rax_1;
                        rbx_2 = rbx_1;
                        if ((uint64_t)(var_46 - var_43) == 0UL) {
                            rax_2 = (uint64_t)((uint32_t)rax_1 + (-1));
                        } else {
                            if ((uint64_t)(var_46 - *(uint32_t *)rbx_1) == 0UL) {
                                rax_2 = (uint64_t)((uint32_t)rax_1 + (-1));
                            } else {
                                var_47 = rbx_1 + 4UL;
                                *(uint32_t *)var_47 = var_46;
                                rbx_2 = var_47;
                            }
                        }
                        var_48 = rdx1_0 + 4UL;
                        rax_1 = rax_2;
                        rdx1_0 = var_48;
                        rbx_1 = rbx_2;
                        rax_5 = rax_2;
                    } while (var_44 <= var_48);
                }
            } else {
                var_26 = var_22 + 1UL;
                *var_23 = (uint32_t)var_26;
                var_27 = (uint64_t)((long)(var_26 << 32UL) >> (long)32UL);
                var_28 = var_0 + (-72L);
                *(uint64_t *)var_28 = 4229032UL;
                var_29 = indirect_placeholder_1(0UL, var_27);
                rbx_0 = var_29;
                rbx_4 = var_29;
                local_sp_1 = var_28;
                if (var_29 == 0UL) {
                    return rax_5;
                }
                var_30 = (uint32_t)rsi;
                if ((uint64_t)(var_30 + 1U) != 0UL) {
                    var_38 = local_sp_1 + (-8L);
                    *(uint64_t *)var_38 = 4229282UL;
                    var_39 = indirect_placeholder_25();
                    var_40 = (uint32_t)var_39;
                    _pre_phi = var_40;
                    rax_0 = var_39;
                    rbx_0 = rbx_4;
                    rax_3 = var_39;
                    local_sp_0 = var_38;
                    if ((int)var_40 <= (int)4294967295U) {
                        *(uint64_t *)(local_sp_0 + (-8L)) = 4229295UL;
                        indirect_placeholder();
                        var_41 = (uint32_t *)rax_3;
                        var_42 = *var_41;
                        *(uint64_t *)(local_sp_0 + (-16L)) = 4229305UL;
                        indirect_placeholder();
                        *(uint64_t *)(local_sp_0 + (-24L)) = 4229310UL;
                        indirect_placeholder();
                        *var_41 = var_42;
                        return rax_5;
                    }
                }
                var_31 = var_0 + (-80L);
                *(uint64_t *)var_31 = 4229070UL;
                var_32 = indirect_placeholder_25();
                var_33 = (uint32_t)var_32;
                rax_3 = var_32;
                local_sp_0 = var_31;
                if ((int)var_33 >= (int)0U) {
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4229295UL;
                    indirect_placeholder();
                    var_41 = (uint32_t *)rax_3;
                    var_42 = *var_41;
                    *(uint64_t *)(local_sp_0 + (-16L)) = 4229305UL;
                    indirect_placeholder();
                    *(uint64_t *)(local_sp_0 + (-24L)) = 4229310UL;
                    indirect_placeholder();
                    *var_41 = var_42;
                    return rax_5;
                }
                *(uint32_t *)var_29 = var_30;
                var_34 = (uint64_t)(var_33 + 1U);
                _pre_phi = (uint32_t)var_34;
                rax_0 = var_34;
            }
        }
    } else {
        *(uint32_t *)(var_0 + (-44L)) = 10U;
        var_7 = var_0 + (-64L);
        *(uint64_t *)var_7 = 4228828UL;
        var_8 = indirect_placeholder_1(0UL, 10UL);
        rax_6 = var_8;
        local_sp_3 = var_7;
        if (var_8 == 0UL) {
            return rax_5;
        }
        while (1U)
            {
                var_9 = *(uint32_t *)(local_sp_3 | 12UL);
                *(uint64_t *)(local_sp_3 + (-8L)) = 4228897UL;
                indirect_placeholder();
                if ((int)(uint32_t)rax_6 < (int)0U) {
                    var_14 = (uint32_t *)(local_sp_3 | 4UL);
                    var_15 = *var_14;
                    var_17 = var_15;
                    if ((uint64_t)(var_15 - var_9) == 0UL) {
                        var_16 = var_15 << 1U;
                        *var_14 = var_16;
                        var_17 = var_16;
                    }
                    var_18 = (uint64_t)var_17;
                    var_19 = local_sp_3 + (-16L);
                    *(uint64_t *)var_19 = 4228865UL;
                    var_20 = indirect_placeholder_1(rax_6, var_18);
                    local_sp_2 = var_19;
                    rax_6 = var_20;
                    local_sp_3 = var_19;
                    if (var_20 == 0UL) {
                        continue;
                    }
                    loop_state_var = 0U;
                    break;
                }
                var_10 = (uint64_t)*(uint32_t *)(local_sp_3 | 4UL);
                var_11 = local_sp_3 + (-16L);
                *(uint64_t *)var_11 = 4228914UL;
                var_12 = indirect_placeholder_1(rax_6, var_10);
                local_sp_2 = var_11;
                if (var_12 != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                *(uint64_t *)rdx = var_12;
                var_13 = (uint64_t)*(uint32_t *)(local_sp_3 + (-4L));
                rax_5 = var_13;
                loop_state_var = 1U;
                break;
            }
        *(uint64_t *)(local_sp_2 + (-8L)) = 4228924UL;
        indirect_placeholder();
        var_21 = *(volatile uint32_t *)(uint32_t *)0UL;
        *(uint64_t *)(local_sp_2 + (-16L)) = 4228934UL;
        indirect_placeholder();
        *(uint64_t *)(local_sp_2 + (-24L)) = 4228939UL;
        indirect_placeholder();
        *(volatile uint32_t *)(uint32_t *)0UL = var_21;
    }
}
