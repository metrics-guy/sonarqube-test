typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_41_ret_type;
struct indirect_placeholder_42_ret_type;
struct indirect_placeholder_43_ret_type;
struct indirect_placeholder_41_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_42_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_43_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r10(void);
extern uint32_t init_state_0x82fc(void);
extern void indirect_placeholder_4(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_5(uint64_t param_0);
extern struct indirect_placeholder_41_ret_type indirect_placeholder_41(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_42_ret_type indirect_placeholder_42(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_43_ret_type indirect_placeholder_43(uint64_t param_0, uint64_t param_1, uint64_t param_2);
void bb_add_exclude(uint64_t rdi, uint64_t rdx, uint64_t rsi) {
    struct indirect_placeholder_41_ret_type var_27;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint32_t var_8;
    uint64_t var_9;
    uint32_t var_10;
    uint64_t var_11;
    uint64_t local_sp_3;
    uint64_t local_sp_0;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_50;
    uint64_t var_20;
    uint64_t rbx_0_in;
    uint64_t r12_0;
    uint64_t *var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_19;
    uint64_t rcx_0;
    uint64_t r10_0;
    uint64_t local_sp_1;
    uint64_t var_21;
    uint64_t *var_22;
    uint64_t var_23;
    uint64_t *_pre_phi112;
    uint64_t var_29;
    uint64_t rbx_1;
    uint64_t local_sp_2;
    uint64_t var_30;
    uint32_t *_cast;
    uint32_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t rcx_1;
    uint64_t rdi1_0;
    uint64_t rcx_2;
    unsigned char var_34;
    uint64_t var_35;
    uint64_t storemerge;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t *var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_28;
    uint64_t var_17;
    struct indirect_placeholder_42_ret_type var_18;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t *var_44;
    uint64_t var_45;
    uint64_t var_47;
    uint64_t local_sp_4;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_46;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_r10();
    var_8 = init_state_0x82fc();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_9 = var_0 + (-56L);
    var_10 = (uint32_t)rdx;
    var_11 = (uint64_t)var_10;
    local_sp_3 = var_9;
    r12_0 = rsi;
    r10_0 = var_7;
    rbx_1 = var_11;
    rcx_1 = 18446744073709551615UL;
    rdi1_0 = rsi;
    rcx_2 = 0UL;
    if ((uint64_t)(var_10 & 402653184U) == 0UL) {
        var_44 = (uint64_t *)rdi;
        var_45 = *var_44;
        var_47 = var_45;
        local_sp_4 = local_sp_3;
        if (var_45 == 0UL) {
            var_46 = local_sp_3 + (-8L);
            *(uint64_t *)var_46 = 4218374UL;
            indirect_placeholder_43(rdi, var_11, 0UL);
            var_47 = *var_44;
            local_sp_4 = var_46;
        } else {
            if (*(uint32_t *)(var_45 + 8UL) == 0U) {
                var_46 = local_sp_3 + (-8L);
                *(uint64_t *)var_46 = 4218374UL;
                indirect_placeholder_43(rdi, var_11, 0UL);
                var_47 = *var_44;
                local_sp_4 = var_46;
            } else {
                if ((uint64_t)((*(uint32_t *)(var_45 + 12UL) ^ var_10) & 1610612760U) == 0UL) {
                    var_46 = local_sp_3 + (-8L);
                    *(uint64_t *)var_46 = 4218374UL;
                    indirect_placeholder_43(rdi, var_11, 0UL);
                    var_47 = *var_44;
                    local_sp_4 = var_46;
                }
            }
        }
        var_48 = local_sp_4 + (-8L);
        *(uint64_t *)var_48 = 4218386UL;
        var_49 = indirect_placeholder_3(rsi);
        local_sp_0 = var_48;
        if ((uint64_t)(((var_10 & 268435458U) + (-268435456)) & (-268435454)) == 0UL) {
            var_50 = local_sp_4 + (-16L);
            *(uint64_t *)var_50 = 4218451UL;
            indirect_placeholder_5(var_49);
            local_sp_0 = var_50;
        }
        var_51 = *(uint64_t *)(var_47 + 16UL);
        *(uint64_t *)(local_sp_0 + (-8L)) = 4218415UL;
        var_52 = indirect_placeholder_1(var_51, var_49);
        if (var_49 != var_52) {
            *(uint64_t *)(local_sp_0 + (-16L)) = 4218428UL;
            indirect_placeholder();
        }
        return;
    }
    var_12 = var_0 + (-64L);
    *(uint64_t *)var_12 = 4217983UL;
    var_13 = indirect_placeholder_1(rsi, var_11);
    local_sp_1 = var_12;
    local_sp_3 = var_12;
    if ((uint64_t)(unsigned char)var_13 == 0UL) {
        return;
    }
    var_14 = (uint64_t *)rdi;
    var_15 = *var_14;
    var_19 = var_15;
    if (var_15 == 0UL) {
        var_17 = var_0 + (-72L);
        *(uint64_t *)var_17 = 4218021UL;
        var_18 = indirect_placeholder_42(rdi, var_11, 1UL);
        var_19 = *var_14;
        rcx_0 = var_18.field_0;
        r10_0 = var_18.field_1;
        local_sp_1 = var_17;
    } else {
        if (*(uint32_t *)(var_15 + 8UL) == 1U) {
            var_17 = var_0 + (-72L);
            *(uint64_t *)var_17 = 4218021UL;
            var_18 = indirect_placeholder_42(rdi, var_11, 1UL);
            var_19 = *var_14;
            rcx_0 = var_18.field_0;
            r10_0 = var_18.field_1;
            local_sp_1 = var_17;
        } else {
            var_16 = var_11 ^ (uint64_t)*(uint32_t *)(var_15 + 12UL);
            rcx_0 = var_16;
            if ((uint64_t)((uint32_t)var_16 & 536870912U) == 0UL) {
                var_17 = var_0 + (-72L);
                *(uint64_t *)var_17 = 4218021UL;
                var_18 = indirect_placeholder_42(rdi, var_11, 1UL);
                var_19 = *var_14;
                rcx_0 = var_18.field_0;
                r10_0 = var_18.field_1;
                local_sp_1 = var_17;
            }
        }
    }
    var_20 = var_19 + 24UL;
    var_21 = *(uint64_t *)var_20;
    var_22 = (uint64_t *)(var_19 + 32UL);
    var_23 = *var_22;
    var_29 = var_23;
    local_sp_2 = local_sp_1;
    if (var_23 == var_21) {
        var_24 = (uint64_t *)(var_19 + 16UL);
        var_25 = *var_24;
        var_26 = local_sp_1 + (-8L);
        *(uint64_t *)var_26 = 4218158UL;
        var_27 = indirect_placeholder_41(rcx_0, var_25, 72UL, var_11, rdi, var_20, r10_0);
        var_28 = var_27.field_1;
        *var_24 = var_27.field_0;
        _pre_phi112 = var_24;
        var_29 = *var_22;
        rbx_1 = var_28;
        local_sp_2 = var_26;
    } else {
        _pre_phi112 = (uint64_t *)(var_19 + 16UL);
    }
    *var_22 = (var_29 + 1UL);
    var_30 = (var_29 * 72UL) + *_pre_phi112;
    _cast = (uint32_t *)var_30;
    var_31 = (uint32_t)rbx_1;
    *_cast = var_31;
    if ((uint64_t)(var_31 & 134217728U) == 0UL) {
        if ((uint64_t)(var_31 & 67108864U) != 0UL) {
            *(uint64_t *)(local_sp_2 + (-8L)) = 4218314UL;
            var_43 = indirect_placeholder_3(rsi);
            *(uint64_t *)(local_sp_2 + (-16L)) = 4218328UL;
            indirect_placeholder_4(rdi, var_43);
            r12_0 = var_43;
        }
        *(uint64_t *)(var_30 + 8UL) = r12_0;
    } else {
        var_32 = ((rbx_1 >> 3UL) & 2UL) | 9UL;
        if ((rbx_1 & 8UL) != 0UL) {
            var_33 = (uint64_t)var_8;
            while (rcx_1 != 0UL)
                {
                    var_34 = *(unsigned char *)rdi1_0;
                    var_35 = rcx_1 + (-1L);
                    rcx_1 = var_35;
                    rcx_2 = var_35;
                    if (var_34 == '\x00') {
                        break;
                    }
                    rdi1_0 = rdi1_0 + var_33;
                }
            storemerge = 18446744073709551614UL - (-2L);
            while (1U)
                {
                    if (storemerge != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    if (*(unsigned char *)((storemerge + rsi) + (-1L)) == '/') {
                        storemerge = storemerge + (-1L);
                        continue;
                    }
                    var_36 = storemerge + 7UL;
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4218196UL;
                    var_37 = indirect_placeholder_3(var_36);
                    *(uint64_t *)(local_sp_2 + (-16L)) = 4218213UL;
                    indirect_placeholder();
                    var_38 = storemerge + var_37;
                    *(uint32_t *)var_38 = 707669800U;
                    *(uint16_t *)(var_38 + 4UL) = (uint16_t)(unsigned short)16169U;
                    *(unsigned char *)(var_38 + 6UL) = (unsigned char)'\x00';
                    var_39 = var_30 + 8UL;
                    *(uint64_t *)(local_sp_2 + (-24L)) = 4218246UL;
                    var_40 = indirect_placeholder_12(var_39, var_32, var_37);
                    *(uint64_t *)(local_sp_2 + (-32L)) = 4218256UL;
                    indirect_placeholder();
                    rbx_0_in = var_40;
                    loop_state_var = 1U;
                    break;
                }
            switch (loop_state_var) {
              case 1U:
                {
                    break;
                }
                break;
              case 0U:
                {
                    *var_22 = (*var_22 + (-1L));
                    return;
                }
                break;
            }
        }
        var_41 = var_30 + 8UL;
        *(uint64_t *)(local_sp_2 + (-8L)) = 4218272UL;
        var_42 = indirect_placeholder_12(var_41, var_32, rsi);
        rbx_0_in = var_42;
    }
}
