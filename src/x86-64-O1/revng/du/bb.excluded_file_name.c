typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_excluded_file_name_ret_type;
struct bb_excluded_file_name_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0);
extern uint64_t init_r13(void);
extern uint64_t init_r9(void);
extern uint64_t indirect_placeholder_36(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint64_t init_r8(void);
extern uint32_t init_state_0x82fc(void);
struct bb_excluded_file_name_ret_type bb_excluded_file_name(uint64_t rdi, uint64_t r15, uint64_t r14, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint32_t var_7;
    uint64_t var_8;
    uint64_t storemerge;
    uint64_t local_sp_2;
    uint64_t local_sp_3;
    uint64_t rbp_3;
    uint64_t rbp_0;
    uint64_t local_sp_0;
    uint64_t rbp_2;
    uint64_t var_19;
    uint64_t rcx_1;
    uint64_t r12_0;
    uint64_t rbx_0;
    uint64_t local_sp_1;
    uint64_t var_9;
    uint64_t rdi4_0;
    uint64_t rcx_0;
    unsigned char var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_10;
    uint64_t var_11;
    struct bb_excluded_file_name_ret_type mrv;
    struct bb_excluded_file_name_ret_type mrv1;
    struct bb_excluded_file_name_ret_type mrv2;
    struct bb_excluded_file_name_ret_type mrv3;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_rbp();
    var_4 = init_r12();
    var_5 = init_r8();
    var_6 = init_r9();
    var_7 = init_state_0x82fc();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_2;
    var_8 = *(uint64_t *)rdi;
    storemerge = 0UL;
    rbp_2 = 0UL;
    rcx_1 = 0UL;
    r12_0 = 0UL;
    rbx_0 = var_8;
    rdi4_0 = rsi;
    rcx_0 = 18446744073709551615UL;
    if (var_8 == 0UL) {
        mrv.field_0 = storemerge;
        mrv1 = mrv;
        mrv1.field_1 = r15;
        mrv2 = mrv1;
        mrv2.field_2 = var_5;
        mrv3 = mrv2;
        mrv3.field_3 = var_6;
        return mrv3;
    }
    var_9 = (uint64_t)var_7;
    local_sp_2 = var_0 + (-40L);
    while (1U)
        {
            rbp_0 = rbp_2;
            rbp_3 = rbp_2;
            local_sp_3 = local_sp_2;
            if (*(uint32_t *)(rbx_0 + 8UL) != 0U) {
                var_10 = local_sp_2 + (-8L);
                *(uint64_t *)var_10 = 4217869UL;
                var_11 = indirect_placeholder_36(rbx_0, r15, r14, rsi);
                local_sp_0 = var_10;
                local_sp_1 = var_10;
                if ((uint64_t)(unsigned char)var_11 == 0UL) {
                    break;
                }
                var_19 = *(uint64_t *)rbx_0;
                local_sp_2 = local_sp_0;
                rbp_2 = rbp_0;
                r12_0 = 1UL;
                rbx_0 = var_19;
                local_sp_1 = local_sp_0;
                if (var_19 != 0UL) {
                    continue;
                }
                break;
            }
            if (rbp_2 != 0UL) {
                while (rcx_0 != 0UL)
                    {
                        var_12 = *(unsigned char *)rdi4_0;
                        var_13 = rcx_0 + (-1L);
                        rcx_0 = var_13;
                        rcx_1 = var_13;
                        if (var_12 == '\x00') {
                            break;
                        }
                        rdi4_0 = rdi4_0 + var_9;
                    }
                var_14 = rcx_1 ^ (-1L);
                var_15 = local_sp_2 + (-8L);
                *(uint64_t *)var_15 = 4217853UL;
                var_16 = indirect_placeholder_3(var_14);
                rbp_3 = var_16;
                local_sp_3 = var_15;
            }
            var_17 = local_sp_3 + (-8L);
            *(uint64_t *)var_17 = 4217803UL;
            var_18 = indirect_placeholder_12(rbx_0, rbp_3, rsi);
            rbp_0 = rbp_3;
            local_sp_0 = var_17;
            local_sp_1 = var_17;
            if ((uint64_t)(unsigned char)var_18 == 0UL) {
                break;
            }
        }
    *(uint64_t *)(local_sp_1 + (-8L)) = 4217895UL;
    indirect_placeholder();
    storemerge = ((uint64_t)((unsigned char)(uint64_t)(uint32_t)((unsigned char)((*(uint32_t *)(rbx_0 + 12UL) >> 29U) & 1U) ^ '\x01') - (unsigned char)r12_0) != 0UL);
    mrv.field_0 = storemerge;
    mrv1 = mrv;
    mrv1.field_1 = r15;
    mrv2 = mrv1;
    mrv2.field_2 = var_5;
    mrv3 = mrv2;
    mrv3.field_3 = var_6;
    return mrv3;
}
