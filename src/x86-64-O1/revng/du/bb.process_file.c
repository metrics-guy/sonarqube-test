typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_72_ret_type;
struct indirect_placeholder_71_ret_type;
struct indirect_placeholder_77_ret_type;
struct indirect_placeholder_73_ret_type;
struct indirect_placeholder_78_ret_type;
struct indirect_placeholder_75_ret_type;
struct indirect_placeholder_76_ret_type;
struct indirect_placeholder_74_ret_type;
struct indirect_placeholder_80_ret_type;
struct indirect_placeholder_81_ret_type;
struct indirect_placeholder_82_ret_type;
struct indirect_placeholder_83_ret_type;
struct indirect_placeholder_84_ret_type;
struct indirect_placeholder_85_ret_type;
struct indirect_placeholder_79_ret_type;
struct indirect_placeholder_72_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_71_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_77_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_73_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_78_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_75_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_76_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_74_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_80_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_81_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_82_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_83_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_84_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_85_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_79_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r10(void);
extern uint64_t indirect_placeholder_36(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern void indirect_placeholder_4(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_5(uint64_t param_0);
extern struct indirect_placeholder_72_ret_type indirect_placeholder_72(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_71_ret_type indirect_placeholder_71(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_77_ret_type indirect_placeholder_77(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_73_ret_type indirect_placeholder_73(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_78_ret_type indirect_placeholder_78(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_75_ret_type indirect_placeholder_75(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_76_ret_type indirect_placeholder_76(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_74_ret_type indirect_placeholder_74(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_80_ret_type indirect_placeholder_80(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
extern struct indirect_placeholder_81_ret_type indirect_placeholder_81(uint64_t param_0);
extern struct indirect_placeholder_82_ret_type indirect_placeholder_82(uint64_t param_0);
extern struct indirect_placeholder_83_ret_type indirect_placeholder_83(uint64_t param_0);
extern struct indirect_placeholder_84_ret_type indirect_placeholder_84(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_85_ret_type indirect_placeholder_85(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_79_ret_type indirect_placeholder_79(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
uint64_t bb_process_file(uint64_t rdi, uint64_t rsi, uint64_t r8, uint64_t r9) {
    uint64_t var_80;
    uint64_t var_31;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint16_t *var_10;
    uint64_t var_11;
    uint32_t var_12;
    uint64_t r83_1;
    uint64_t rdx_3;
    uint64_t rbp_2;
    uint64_t local_sp_11;
    uint64_t _pre_phi240;
    uint64_t local_sp_0;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t rcx_0;
    uint64_t var_108;
    uint64_t var_59;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t local_sp_2;
    uint64_t _pre_phi238;
    uint64_t local_sp_1;
    uint64_t var_107;
    uint64_t local_sp_3;
    uint64_t local_sp_12;
    uint64_t var_109;
    uint64_t r14_1;
    uint64_t var_110;
    uint64_t rdx_0;
    uint64_t var_111;
    uint64_t rbp_3;
    struct indirect_placeholder_72_ret_type var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_23;
    uint64_t local_sp_4;
    uint64_t var_24;
    uint32_t _pre_phi;
    uint64_t rbp_0;
    uint64_t local_sp_5;
    uint64_t r83_0;
    uint64_t r94_0;
    bool var_25;
    uint64_t var_26;
    uint64_t local_sp_6;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t r15_0;
    struct indirect_placeholder_77_ret_type var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    struct indirect_placeholder_73_ret_type var_39;
    uint64_t var_40;
    struct indirect_placeholder_78_ret_type var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_19;
    struct indirect_placeholder_76_ret_type var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint32_t _pre_phi224;
    uint64_t local_sp_7;
    struct indirect_placeholder_74_ret_type var_32;
    uint64_t r14_0;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t rdx_1;
    uint64_t local_sp_8;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t r13_0;
    uint64_t local_sp_9;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t r94_1;
    uint64_t rdx_2;
    uint64_t local_sp_10;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_88;
    uint64_t _pre237;
    uint64_t var_100;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t var_96;
    uint64_t var_97;
    struct indirect_placeholder_80_ret_type var_98;
    uint64_t var_99;
    uint64_t var_62;
    struct indirect_placeholder_81_ret_type var_63;
    uint64_t rdx_4;
    uint64_t var_64;
    struct indirect_placeholder_82_ret_type var_65;
    uint64_t var_60;
    struct indirect_placeholder_83_ret_type var_61;
    uint64_t rax_1;
    uint64_t local_sp_13;
    uint64_t var_66;
    uint64_t rsi2_0;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t *var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_13;
    uint64_t var_14;
    struct indirect_placeholder_84_ret_type var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    struct indirect_placeholder_85_ret_type var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    struct indirect_placeholder_79_ret_type var_58;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_r10();
    var_8 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_9 = *(uint64_t *)(rsi + 56UL);
    var_10 = (uint16_t *)(rsi + 112UL);
    var_11 = (uint64_t)*var_10;
    var_12 = (uint32_t)var_11;
    r83_1 = r8;
    rdx_3 = 0UL;
    r14_1 = 1UL;
    rbp_3 = var_11;
    _pre_phi = var_12;
    rbp_0 = var_11;
    r15_0 = var_9;
    _pre_phi224 = var_12;
    r14_0 = 1UL;
    r94_1 = r9;
    if ((uint64_t)(var_12 + (-4)) == 0UL) {
        *(uint64_t *)(var_0 + (-128L)) = 4210499UL;
        var_52 = indirect_placeholder_85(4UL, var_9);
        var_53 = var_52.field_0;
        var_54 = var_52.field_1;
        var_55 = var_52.field_2;
        var_56 = (uint64_t)*(uint32_t *)(rsi + 64UL);
        var_57 = var_0 + (-136L);
        *(uint64_t *)var_57 = 4210525UL;
        var_58 = indirect_placeholder_79(0UL, var_53, 0UL, 4366878UL, var_56, var_54, var_55);
        r14_1 = 0UL;
        local_sp_12 = var_57;
        r83_1 = var_58.field_0;
        r94_1 = var_58.field_1;
    } else {
        local_sp_12 = var_0 + (-120L);
        if ((uint64_t)(var_12 + (-6)) != 0UL) {
            var_13 = *(uint64_t *)4420896UL;
            var_14 = var_0 + (-128L);
            *(uint64_t *)var_14 = 4210551UL;
            var_15 = indirect_placeholder_84(var_13, var_9, 1UL, var_9);
            var_16 = var_15.field_0;
            var_17 = var_15.field_1;
            var_18 = (uint64_t)(uint32_t)var_16;
            local_sp_5 = var_14;
            local_sp_7 = var_14;
            r15_0 = var_17;
            if ((uint64_t)(unsigned char)var_16 != 0UL) {
                *(uint64_t *)(local_sp_7 + (-8L)) = 4210879UL;
                indirect_placeholder_1(4UL, rsi);
                *(uint64_t *)(local_sp_7 + (-16L)) = 4210887UL;
                var_32 = indirect_placeholder_74(rdi, rsi);
                if ((uint64_t)(_pre_phi224 + (-1)) != 0UL & var_32.field_0 == rsi) {
                    *(uint64_t *)(local_sp_7 + (-24L)) = 4210921UL;
                    indirect_placeholder();
                }
                return (uint64_t)(uint32_t)r14_0;
            }
            r83_0 = var_15.field_2;
            r94_0 = var_15.field_3;
            r14_0 = var_18;
            if ((uint64_t)(var_12 + (-11)) != 0UL) {
                *(uint64_t *)(var_0 + (-136L)) = 4210754UL;
                indirect_placeholder_1(1UL, rsi);
                var_19 = var_0 + (-144L);
                *(uint64_t *)var_19 = 4210762UL;
                var_20 = indirect_placeholder_76(rdi, rsi);
                var_21 = var_20.field_1;
                var_22 = var_20.field_2;
                local_sp_4 = var_19;
                r83_0 = var_21;
                r94_0 = var_22;
                if (var_20.field_0 != rsi) {
                    var_23 = var_0 + (-152L);
                    *(uint64_t *)var_23 = 4210792UL;
                    indirect_placeholder();
                    local_sp_4 = var_23;
                }
                var_24 = (uint64_t)*var_10;
                _pre_phi = (uint32_t)var_24;
                rbp_0 = var_24;
                local_sp_5 = local_sp_4;
            }
            var_25 = (((uint64_t)(_pre_phi + (-13)) == 0UL) || ((uint64_t)(_pre_phi + (-10)) == 0UL));
            var_26 = var_25;
            r83_1 = r83_0;
            r14_1 = var_26;
            rbp_3 = rbp_0;
            local_sp_6 = local_sp_5;
            _pre_phi224 = _pre_phi;
            local_sp_7 = local_sp_5;
            r94_1 = r94_0;
            if (!var_25) {
                *(uint64_t *)(local_sp_5 + (-8L)) = 4210814UL;
                var_47 = indirect_placeholder_78(4UL, var_17);
                var_48 = var_47.field_0;
                var_49 = var_47.field_1;
                var_50 = var_47.field_2;
                var_51 = (uint64_t)*(uint32_t *)(rsi + 64UL);
                *(uint64_t *)(local_sp_5 + (-16L)) = 4210840UL;
                indirect_placeholder_75(0UL, var_48, 0UL, 4366921UL, var_51, var_49, var_50);
                return (uint64_t)(uint32_t)r14_0;
            }
            if ((*(unsigned char *)(rdi + 72UL) & '@') != '\x00') {
                if ((long)*(uint64_t *)(rsi + 88UL) <= (long)0UL & *(uint64_t *)(rdi + 24UL) != *(uint64_t *)(rsi + 120UL)) {
                    *(uint64_t *)(local_sp_7 + (-8L)) = 4210879UL;
                    indirect_placeholder_1(4UL, rsi);
                    *(uint64_t *)(local_sp_7 + (-16L)) = 4210887UL;
                    var_32 = indirect_placeholder_74(rdi, rsi);
                    if ((uint64_t)(_pre_phi224 + (-1)) != 0UL & var_32.field_0 == rsi) {
                        *(uint64_t *)(local_sp_7 + (-24L)) = 4210921UL;
                        indirect_placeholder();
                    }
                    return (uint64_t)(uint32_t)r14_0;
                }
            }
            if (*(unsigned char *)4420964UL != '\x00') {
                if (*(unsigned char *)4420963UL != '\x00') {
                    var_27 = *(uint64_t *)(rsi + 120UL);
                    var_28 = *(uint64_t *)(rsi + 128UL);
                    var_29 = *(uint64_t *)4420984UL;
                    var_30 = local_sp_5 + (-8L);
                    *(uint64_t *)var_30 = 4210692UL;
                    var_31 = indirect_placeholder_8(var_29, var_27, var_28, r83_0, r94_0);
                    local_sp_6 = var_30;
                    local_sp_7 = var_30;
                    if ((uint64_t)(unsigned char)var_31 != 0UL) {
                        *(uint64_t *)(local_sp_7 + (-8L)) = 4210879UL;
                        indirect_placeholder_1(4UL, rsi);
                        *(uint64_t *)(local_sp_7 + (-16L)) = 4210887UL;
                        var_32 = indirect_placeholder_74(rdi, rsi);
                        if ((uint64_t)(_pre_phi224 + (-1)) != 0UL & var_32.field_0 == rsi) {
                            *(uint64_t *)(local_sp_7 + (-24L)) = 4210921UL;
                            indirect_placeholder();
                        }
                        return (uint64_t)(uint32_t)r14_0;
                    }
                }
                var_27 = *(uint64_t *)(rsi + 120UL);
                var_28 = *(uint64_t *)(rsi + 128UL);
                var_29 = *(uint64_t *)4420984UL;
                var_30 = local_sp_5 + (-8L);
                *(uint64_t *)var_30 = 4210692UL;
                var_31 = indirect_placeholder_8(var_29, var_27, var_28, r83_0, r94_0);
                local_sp_6 = var_30;
                local_sp_7 = var_30;
                if ((uint32_t)((uint16_t)*(uint32_t *)(rsi + 144UL) & (unsigned short)61440U) != 16384U & *(uint64_t *)(rsi + 136UL) <= 1UL & (uint64_t)(unsigned char)var_31 != 0UL) {
                    *(uint64_t *)(local_sp_7 + (-8L)) = 4210879UL;
                    indirect_placeholder_1(4UL, rsi);
                    *(uint64_t *)(local_sp_7 + (-16L)) = 4210887UL;
                    var_32 = indirect_placeholder_74(rdi, rsi);
                    if ((uint64_t)(_pre_phi224 + (-1)) != 0UL & var_32.field_0 == rsi) {
                        *(uint64_t *)(local_sp_7 + (-24L)) = 4210921UL;
                        indirect_placeholder();
                    }
                    return (uint64_t)(uint32_t)r14_0;
                }
            }
            local_sp_12 = local_sp_6;
            if ((uint64_t)(_pre_phi + (-2)) != 0UL) {
                *(uint64_t *)(local_sp_6 + (-8L)) = 4210986UL;
                var_40 = indirect_placeholder_1(rdi, rsi);
                *(uint64_t *)(local_sp_6 + (-16L)) = 4211008UL;
                var_41 = indirect_placeholder_3(rsi);
                var_42 = (uint64_t)(uint32_t)var_41;
                r14_0 = var_42;
                if ((uint64_t)(unsigned char)var_40 != 0UL & (uint64_t)(unsigned char)var_41 == 0UL) {
                    *(uint64_t *)(local_sp_6 + (-24L)) = 4211037UL;
                    var_43 = indirect_placeholder_72(0UL, var_17, 3UL);
                    var_44 = var_43.field_0;
                    var_45 = var_43.field_1;
                    var_46 = var_43.field_2;
                    *(uint64_t *)(local_sp_6 + (-32L)) = 4211065UL;
                    indirect_placeholder_71(0UL, var_44, 0UL, 4362904UL, 0UL, var_45, var_46);
                }
                return (uint64_t)(uint32_t)r14_0;
            }
            if ((uint64_t)(_pre_phi + (-7)) != 0UL) {
                r14_1 = 1UL;
                if ((uint64_t)(_pre_phi + (-1)) == 0UL) {
                    return (uint64_t)(uint32_t)r14_0;
                }
            }
            *(uint64_t *)(local_sp_6 + (-8L)) = 4210944UL;
            var_33 = indirect_placeholder_77(0UL, var_17, 3UL);
            var_34 = var_33.field_0;
            var_35 = var_33.field_1;
            var_36 = var_33.field_2;
            var_37 = (uint64_t)*(uint32_t *)(rsi + 64UL);
            var_38 = local_sp_6 + (-16L);
            *(uint64_t *)var_38 = 4210970UL;
            var_39 = indirect_placeholder_73(0UL, var_34, 0UL, 4369436UL, var_37, var_35, var_36);
            local_sp_12 = var_38;
            r83_1 = var_39.field_0;
            r94_1 = var_39.field_1;
        }
    }
}
