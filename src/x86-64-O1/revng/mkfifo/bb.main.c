typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_4_ret_type;
struct indirect_placeholder_5_ret_type;
struct indirect_placeholder_8_ret_type;
struct indirect_placeholder_10_ret_type;
struct indirect_placeholder_12_ret_type;
struct indirect_placeholder_14_ret_type;
struct indirect_placeholder_15_ret_type;
struct indirect_placeholder_16_ret_type;
struct indirect_placeholder_6_ret_type;
struct indirect_placeholder_4_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_5_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_8_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_10_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_12_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_14_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_15_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_16_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_6_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_11(void);
extern void indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_3(uint64_t param_0);
extern void indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_4_ret_type indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_5_ret_type indirect_placeholder_5(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_8_ret_type indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern uint64_t indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
extern struct indirect_placeholder_10_ret_type indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_12_ret_type indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_14_ret_type indirect_placeholder_14(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_15_ret_type indirect_placeholder_15(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_16_ret_type indirect_placeholder_16(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_6_ret_type indirect_placeholder_6(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
void bb_main(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_15_ret_type var_13;
    struct indirect_placeholder_4_ret_type var_72;
    uint64_t r9_5;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint32_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t r15_2;
    uint64_t var_68;
    uint64_t r8_5;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t local_sp_10;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t r13_1;
    uint64_t rax_2;
    uint32_t var_66;
    uint64_t var_67;
    uint64_t local_sp_5_ph164;
    uint64_t r12_0_ph163188;
    uint64_t rbx_5;
    uint64_t rbx_1;
    uint64_t r13_0;
    uint64_t r15_1;
    uint64_t rbx_0;
    uint64_t local_sp_1;
    uint64_t r9_1;
    uint64_t r15_0;
    uint64_t r9_0;
    uint64_t local_sp_0;
    uint64_t r8_0;
    uint64_t var_75;
    uint32_t var_76;
    uint64_t r12_0_ph163187;
    uint64_t r12_0_ph163189;
    uint64_t local_sp_5;
    uint64_t r8_1;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    struct indirect_placeholder_5_ret_type var_65;
    uint64_t var_50;
    struct indirect_placeholder_8_ret_type var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_45;
    uint64_t var_46;
    uint32_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t r13_3;
    uint64_t rbx_2;
    uint64_t rcx_0;
    uint64_t r9_2;
    uint64_t local_sp_2;
    uint64_t r8_2;
    uint64_t var_54;
    struct indirect_placeholder_10_ret_type var_55;
    uint64_t var_33;
    uint64_t rax_1;
    uint64_t r12_0_ph;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t r13_2_ph;
    uint64_t local_sp_5_ph;
    uint64_t r12_0_ph163;
    uint64_t local_sp_5_be;
    uint64_t var_28;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t local_sp_4;
    uint64_t var_24;
    uint64_t var_25;
    bool var_26;
    uint64_t var_27;
    uint64_t local_sp_7;
    uint64_t var_29;
    bool var_30;
    uint64_t var_31;
    uint64_t *var_32;
    uint64_t var_12;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint32_t var_18;
    uint64_t local_sp_6;
    uint64_t var_40;
    struct indirect_placeholder_16_ret_type var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t rbx_4;
    uint64_t r9_4;
    uint64_t local_sp_9;
    uint64_t r8_4;
    uint64_t var_56;
    uint64_t r13_4;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    struct indirect_placeholder_6_ret_type var_39;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_8 = (uint32_t)rdi;
    var_9 = (uint64_t)var_8;
    var_10 = *(uint64_t *)rsi;
    *(uint64_t *)(var_0 + (-64L)) = 4204734UL;
    indirect_placeholder_3(var_10);
    *(uint64_t *)(var_0 + (-72L)) = 4204749UL;
    indirect_placeholder_1();
    var_11 = var_0 + (-80L);
    *(uint64_t *)var_11 = 4204759UL;
    indirect_placeholder_1();
    r15_2 = 1UL;
    r12_0_ph163188 = 0UL;
    rbx_1 = rsi;
    r15_1 = var_3;
    r13_3 = 438UL;
    rbx_2 = rsi;
    r12_0_ph = 0UL;
    r13_2_ph = 0UL;
    local_sp_5_ph = var_11;
    rbx_4 = rsi;
    while (1U)
        {
            r12_0_ph163 = r12_0_ph;
            local_sp_5_ph164 = local_sp_5_ph;
            while (1U)
                {
                    r12_0_ph163189 = r12_0_ph163;
                    r12_0_ph = r12_0_ph163;
                    local_sp_5 = local_sp_5_ph164;
                    r12_0_ph163187 = r12_0_ph163;
                    while (1U)
                        {
                            var_12 = local_sp_5 + (-8L);
                            *(uint64_t *)var_12 = 4204797UL;
                            var_13 = indirect_placeholder_15(4256883UL, var_9, rsi, 4256992UL, 0UL);
                            var_14 = var_13.field_0;
                            var_15 = var_13.field_1;
                            var_16 = var_13.field_2;
                            var_17 = var_13.field_3;
                            var_18 = (uint32_t)var_14;
                            local_sp_5_ph164 = var_12;
                            local_sp_4 = var_12;
                            local_sp_7 = var_12;
                            r9_4 = var_16;
                            r8_4 = var_17;
                            if ((uint64_t)(var_18 + 1U) == 0UL) {
                                var_19 = var_14 + (-90L);
                                if ((uint64_t)(uint32_t)var_19 != 0UL) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                var_24 = local_sp_5 + (-16L);
                                *(uint64_t *)var_24 = 4204915UL;
                                var_25 = indirect_placeholder_11();
                                var_26 = ((uint64_t)(unsigned char)var_25 == 0UL);
                                var_27 = *(uint64_t *)4277496UL;
                                r13_2_ph = var_27;
                                local_sp_5_ph = var_24;
                                local_sp_5_be = var_24;
                                if (!var_26) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                if (var_27 == 0UL) {
                                    var_28 = local_sp_5 + (-24L);
                                    *(uint64_t *)var_28 = 4204970UL;
                                    indirect_placeholder_12(0UL, 4256544UL, 0UL, 0UL, var_15, var_16, var_17);
                                    local_sp_5_be = var_28;
                                }
                                local_sp_5 = local_sp_5_be;
                                continue;
                            }
                            if ((uint64_t)(*(uint32_t *)4276924UL - var_8) == 0UL) {
                                *(uint64_t *)(local_sp_5 + (-16L)) = 4205200UL;
                                indirect_placeholder_14(0UL, 4256887UL, 0UL, 0UL, var_15, var_16, var_17);
                                *(uint64_t *)(local_sp_5 + (-24L)) = 4205210UL;
                                indirect_placeholder_7(rsi, 1UL, var_9);
                                abort();
                            }
                            if (r13_2_ph != 0UL) {
                                loop_state_var = 2U;
                                break;
                            }
                            *(uint64_t *)(local_sp_5 + (-16L)) = 4205007UL;
                            var_29 = indirect_placeholder_11();
                            var_30 = ((uint64_t)(unsigned char)var_29 == 0UL);
                            var_31 = local_sp_5 + (-24L);
                            var_32 = (uint64_t *)var_31;
                            local_sp_6 = var_31;
                            if (!var_30) {
                                *var_32 = 4205023UL;
                                var_33 = indirect_placeholder_11();
                                rax_1 = var_33;
                                loop_state_var = 3U;
                                break;
                            }
                            *var_32 = 4205218UL;
                            indirect_placeholder_2(r13_2_ph);
                            var_34 = local_sp_5 + (-32L);
                            *(uint64_t *)var_34 = 4205226UL;
                            var_35 = indirect_placeholder_11();
                            rax_1 = var_35;
                            local_sp_6 = var_34;
                            loop_state_var = 3U;
                            break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 0U:
                        {
                            var_20 = helper_cc_compute_all_wrapper(var_19, 90UL, var_7, 16U);
                            if ((uint64_t)(((unsigned char)(var_20 >> 4UL) ^ (unsigned char)var_20) & '\xc0') == 0UL) {
                                if ((uint64_t)(var_18 + (-109)) != 0UL) {
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                                r12_0_ph163 = *(uint64_t *)4277496UL;
                                continue;
                            }
                            if ((uint64_t)(var_18 + 131U) == 0UL) {
                                if ((uint64_t)(var_18 + 130U) != 0UL) {
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                                *(uint64_t *)(local_sp_5 + (-16L)) = 4204893UL;
                                indirect_placeholder_7(rsi, 0UL, var_9);
                                abort();
                            }
                            var_21 = *(uint64_t *)4276800UL;
                            var_22 = *(uint64_t *)4275776UL;
                            *(uint64_t *)(local_sp_5 + (-16L)) = 4204866UL;
                            indirect_placeholder_13(0UL, 4256819UL, var_22, 4256860UL, var_21, 0UL, 4256867UL);
                            var_23 = local_sp_5 + (-24L);
                            *(uint64_t *)var_23 = 4204876UL;
                            indirect_placeholder_1();
                            local_sp_4 = var_23;
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 3U:
                        {
                            local_sp_7 = local_sp_6;
                            if ((int)(uint32_t)rax_1 >= (int)0U) {
                                loop_state_var = 3U;
                                switch_state_var = 1;
                                break;
                            }
                            *(uint64_t *)(local_sp_6 + (-8L)) = 4205239UL;
                            var_36 = indirect_placeholder_2(r13_2_ph);
                            *(uint64_t *)(local_sp_6 + (-16L)) = 4205247UL;
                            indirect_placeholder_1();
                            var_37 = (uint64_t)*(uint32_t *)var_36;
                            var_38 = local_sp_6 + (-24L);
                            *(uint64_t *)var_38 = 4205272UL;
                            var_39 = indirect_placeholder_6(0UL, 4256624UL, 1UL, var_37, var_36, var_16, var_17);
                            rbx_2 = var_36;
                            rcx_0 = var_39.field_0;
                            r9_2 = var_39.field_1;
                            local_sp_2 = var_38;
                            r8_2 = var_39.field_2;
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 1U:
                        {
                            loop_state_var = 2U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 2U:
                        {
                            loop_state_var = 3U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 2U:
                {
                    continue;
                }
                break;
              case 1U:
                {
                    *(uint64_t *)(local_sp_4 + (-8L)) = 4204985UL;
                    indirect_placeholder_7(rsi, 1UL, var_9);
                    abort();
                }
                break;
              case 3U:
                {
                    local_sp_9 = local_sp_7;
                    if (r12_0_ph163 != 0UL) {
                        loop_state_var = 2U;
                        switch_state_var = 1;
                        break;
                    }
                    var_40 = local_sp_7 + (-8L);
                    *(uint64_t *)var_40 = 4205048UL;
                    var_41 = indirect_placeholder_16(r12_0_ph163, var_16, var_17);
                    var_42 = var_41.field_0;
                    var_43 = var_41.field_1;
                    var_44 = var_41.field_2;
                    rcx_0 = var_43;
                    r9_2 = var_44;
                    local_sp_2 = var_40;
                    r8_2 = var_41.field_3;
                    r9_4 = var_44;
                    r8_4 = 0UL;
                    if (var_42 != 0UL) {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    *(uint64_t *)(local_sp_7 + (-16L)) = 4205070UL;
                    indirect_placeholder_1();
                    var_45 = (uint64_t)(uint32_t)var_42;
                    *(uint64_t *)(local_sp_7 + (-24L)) = 4205080UL;
                    indirect_placeholder_1();
                    *(uint64_t *)(local_sp_7 + (-32L)) = 4205107UL;
                    var_46 = indirect_placeholder_9(var_45, rsi, 438UL, var_9, r12_0_ph163, 0UL, var_42, 0UL);
                    var_47 = (uint32_t)var_46;
                    var_48 = (uint64_t)var_47;
                    var_49 = local_sp_7 + (-40L);
                    *(uint64_t *)var_49 = 4205118UL;
                    indirect_placeholder_1();
                    r13_1 = var_48;
                    r13_3 = var_48;
                    local_sp_9 = var_49;
                    if ((uint64_t)(var_47 & (-512)) != 0UL) {
                        loop_state_var = 2U;
                        switch_state_var = 1;
                        break;
                    }
                    var_50 = local_sp_7 + (-48L);
                    *(uint64_t *)var_50 = 4205333UL;
                    var_51 = indirect_placeholder_8(0UL, 4256680UL, 1UL, 0UL, var_42, var_44, 0UL);
                    var_52 = var_51.field_1;
                    var_53 = var_51.field_2;
                    r9_1 = var_52;
                    local_sp_1 = var_50;
                    r8_1 = var_53;
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 0U:
                {
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            var_61 = *(uint64_t *)(((uint64_t)*(uint32_t *)4276924UL << 3UL) + rbx_1);
            *(uint64_t *)(local_sp_1 + (-8L)) = 4205354UL;
            var_62 = indirect_placeholder(4UL, var_61);
            *(uint64_t *)(local_sp_1 + (-16L)) = 4205362UL;
            indirect_placeholder_1();
            var_63 = (uint64_t)*(uint32_t *)var_62;
            var_64 = local_sp_1 + (-24L);
            *(uint64_t *)var_64 = 4205387UL;
            var_65 = indirect_placeholder_5(0UL, 4256916UL, 0UL, var_63, var_62, r9_1, r8_1);
            r12_0_ph163188 = r12_0_ph163189;
            r13_0 = r13_1;
            rbx_0 = rbx_1;
            r15_0 = r15_1;
            r9_0 = var_65.field_1;
            local_sp_0 = var_64;
            r8_0 = var_65.field_2;
            var_75 = (uint64_t)*(uint32_t *)4276924UL + 1UL;
            var_76 = (uint32_t)var_75;
            *(uint32_t *)4276924UL = var_76;
            r9_5 = r9_0;
            r15_2 = r15_0;
            r8_5 = r8_0;
            local_sp_10 = local_sp_0;
            rbx_5 = rbx_0;
            r12_0_ph163187 = r12_0_ph163188;
            r13_4 = r13_0;
            if ((long)(var_75 << 32UL) >= (long)(rdi << 32UL)) {
                return;
            }
            rax_2 = (uint64_t)var_76;
        }
        break;
      case 2U:
      case 1U:
        {
            switch (loop_state_var) {
              case 1U:
                {
                    var_54 = local_sp_2 + (-8L);
                    *(uint64_t *)var_54 = 4205297UL;
                    var_55 = indirect_placeholder_10(0UL, 4256903UL, 1UL, 0UL, rcx_0, r9_2, r8_2);
                    rbx_4 = rbx_2;
                    r9_4 = var_55.field_1;
                    local_sp_9 = var_54;
                    r8_4 = var_55.field_2;
                }
                break;
              case 2U:
                {
                    var_56 = (uint64_t)*(uint32_t *)4276924UL;
                    r9_5 = r9_4;
                    r8_5 = r8_4;
                    local_sp_10 = local_sp_9;
                    rax_2 = var_56;
                    rbx_5 = rbx_4;
                    r13_4 = r13_3;
                    if ((long)(rdi << 32UL) > (long)(var_56 << 32UL)) {
                        return;
                    }
                }
                break;
            }
        }
        break;
    }
}
