typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_107_ret_type;
struct indirect_placeholder_108_ret_type;
struct indirect_placeholder_107_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_108_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_r13(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_107_ret_type indirect_placeholder_107(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_108_ret_type indirect_placeholder_108(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
void bb_debug_print_current_time(uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t rcx, uint64_t r8, uint64_t r9) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    unsigned char var_5;
    uint64_t var_6;
    struct indirect_placeholder_108_ret_type var_8;
    uint64_t rbp_0;
    uint64_t local_sp_0;
    uint64_t local_sp_2;
    uint64_t local_sp_1;
    uint64_t local_sp_4;
    uint64_t var_26;
    uint64_t var_25;
    uint64_t var_17;
    unsigned char *_pre_phi82;
    unsigned char *var_11;
    uint64_t var_12;
    uint64_t var_13;
    unsigned char *var_14;
    unsigned char var_15;
    unsigned char *var_16;
    uint64_t local_sp_3;
    unsigned char *var_18;
    unsigned char var_19;
    unsigned char *var_20;
    unsigned char *_pre_phi80;
    uint64_t r85_0;
    uint64_t var_21;
    uint64_t r85_1;
    uint64_t rbp_1;
    unsigned char *var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t r85_2;
    uint64_t local_sp_6;
    unsigned char *var_27;
    uint64_t local_sp_5;
    uint64_t var_29;
    uint64_t var_28;
    uint64_t rbp_2;
    uint64_t local_sp_7;
    unsigned char *var_30;
    uint64_t var_31;
    uint64_t rbp_3;
    unsigned char *var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t local_sp_10;
    uint64_t local_sp_9;
    uint64_t local_sp_8;
    uint64_t var_36;
    uint64_t var_35;
    uint64_t var_7;
    uint64_t var_9;
    uint64_t var_10;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_rbp();
    var_4 = init_r12();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_2;
    var_5 = *(unsigned char *)(rsi + 217UL);
    var_6 = (uint64_t)var_5;
    rbp_0 = var_6;
    if (var_5 == '\x00') {
        return;
    }
    var_7 = var_0 + (-160L);
    *(uint64_t *)var_7 = 4219318UL;
    var_8 = indirect_placeholder_108(0UL, rdx, 4301751UL, rdi, rcx, r8, r9);
    var_9 = var_8.field_5;
    var_10 = var_8.field_6;
    local_sp_2 = var_7;
    r85_0 = var_9;
    local_sp_3 = var_7;
    r85_1 = var_9;
    if (*(uint64_t *)(rsi + 168UL) == 0UL) {
        var_11 = (unsigned char *)(rsi + 218UL);
        if (*var_11 == '\x00') {
            var_12 = *(uint64_t *)(rsi + 64UL);
            var_13 = var_0 + (-168L);
            *(uint64_t *)var_13 = 4219733UL;
            indirect_placeholder();
            *var_11 = (unsigned char)'\x01';
            var_14 = (unsigned char *)(rsi + 223UL);
            var_15 = *var_14;
            var_16 = (unsigned char *)(rsi + 216UL);
            _pre_phi82 = var_14;
            local_sp_3 = var_13;
            _pre_phi80 = var_16;
            r85_0 = var_12;
            r85_1 = var_12;
            if ((uint64_t)(*var_16 - var_15) != 0UL) {
                var_17 = var_0 + (-176L);
                *(uint64_t *)var_17 = 4219768UL;
                indirect_placeholder();
                local_sp_2 = var_17;
                var_21 = local_sp_2 + (-8L);
                *(uint64_t *)var_21 = 4219387UL;
                indirect_placeholder();
                *_pre_phi82 = *_pre_phi80;
                local_sp_3 = var_21;
                r85_1 = r85_0;
            }
        } else {
            var_18 = (unsigned char *)(rsi + 223UL);
            var_19 = *var_18;
            var_20 = (unsigned char *)(rsi + 216UL);
            _pre_phi82 = var_18;
            _pre_phi80 = var_20;
            rbp_0 = 0UL;
            if ((uint64_t)(*var_20 - var_19) == 0UL) {
                var_21 = local_sp_2 + (-8L);
                *(uint64_t *)var_21 = 4219387UL;
                indirect_placeholder();
                *_pre_phi82 = *_pre_phi80;
                local_sp_3 = var_21;
                r85_1 = r85_0;
            }
        }
    } else {
        var_18 = (unsigned char *)(rsi + 223UL);
        var_19 = *var_18;
        var_20 = (unsigned char *)(rsi + 216UL);
        _pre_phi82 = var_18;
        _pre_phi80 = var_20;
        rbp_0 = 0UL;
        if ((uint64_t)(*var_20 - var_19) == 0UL) {
            var_21 = local_sp_2 + (-8L);
            *(uint64_t *)var_21 = 4219387UL;
            indirect_placeholder();
            *_pre_phi82 = *_pre_phi80;
            local_sp_3 = var_21;
            r85_1 = r85_0;
        }
    }
    local_sp_4 = local_sp_3;
    rbp_1 = rbp_0;
    r85_2 = r85_1;
    var_22 = (unsigned char *)(rsi + 221UL);
    if (*(uint64_t *)(rsi + 200UL) != 0UL & *var_22 != '\x00') {
        var_23 = *(uint64_t *)(rsi + 88UL);
        var_24 = local_sp_3 + (-8L);
        *(uint64_t *)var_24 = 4219827UL;
        indirect_placeholder();
        local_sp_0 = var_24;
        rbp_1 = var_6;
        r85_2 = var_23;
        if (*(uint64_t *)(rsi + 96UL) == 0UL) {
            var_25 = local_sp_3 + (-16L);
            *(uint64_t *)var_25 = 4219875UL;
            indirect_placeholder();
            local_sp_0 = var_25;
        }
        local_sp_1 = local_sp_0;
        if (*(uint32_t *)(rsi + 28UL) == 1U) {
            var_26 = local_sp_0 + (-8L);
            *(uint64_t *)var_26 = 4219900UL;
            indirect_placeholder();
            local_sp_1 = var_26;
        }
        *var_22 = (unsigned char)'\x01';
        local_sp_4 = local_sp_1;
    }
    local_sp_5 = local_sp_4;
    local_sp_6 = local_sp_4;
    rbp_2 = rbp_1;
    var_27 = (unsigned char *)(rsi + 219UL);
    if (*(uint64_t *)(rsi + 176UL) != 0UL & *var_27 != '\x00') {
        rbp_2 = var_6;
        if ((uint64_t)(unsigned char)rbp_1 != 0UL) {
            var_28 = local_sp_4 + (-8L);
            *(uint64_t *)var_28 = 4219919UL;
            indirect_placeholder();
            local_sp_5 = var_28;
        }
        *(uint64_t *)(local_sp_5 + (-8L)) = 4219478UL;
        indirect_placeholder_107(100UL, rsi, local_sp_5, r85_2, var_10);
        var_29 = local_sp_5 + (-16L);
        *(uint64_t *)var_29 = 4219509UL;
        indirect_placeholder();
        *var_27 = (unsigned char)'\x01';
        local_sp_6 = var_29;
    }
    local_sp_7 = local_sp_6;
    rbp_3 = rbp_2;
    var_30 = (unsigned char *)(rsi + 220UL);
    if (*(uint64_t *)(rsi + 184UL) != 0UL & *var_30 == '\x00') {
        var_31 = local_sp_6 + (-8L);
        *(uint64_t *)var_31 = 4219594UL;
        indirect_placeholder();
        *var_30 = (unsigned char)'\x01';
        local_sp_7 = var_31;
        rbp_3 = var_6;
    }
    local_sp_8 = local_sp_7;
    local_sp_9 = local_sp_7;
    local_sp_10 = local_sp_7;
    if (*(uint64_t *)(rsi + 208UL) != 0UL) {
        var_32 = (unsigned char *)(rsi + 222UL);
        if (*var_32 != '\x00') {
            var_33 = (uint64_t)*(uint32_t *)(rsi + 24UL);
            *(uint64_t *)(local_sp_7 + (-8L)) = 4219935UL;
            indirect_placeholder_3(var_33, local_sp_7);
            var_34 = local_sp_7 + (-16L);
            *(uint64_t *)var_34 = 4219969UL;
            indirect_placeholder();
            *var_32 = (unsigned char)'\x01';
            local_sp_9 = var_34;
            local_sp_10 = var_34;
            if (*(unsigned char *)(rsi + 160UL) != '\x00') {
                *(uint64_t *)(local_sp_10 + (-8L)) = 4219691UL;
                indirect_placeholder();
                return;
            }
            var_35 = local_sp_9 + (-8L);
            *(uint64_t *)var_35 = 4220010UL;
            indirect_placeholder();
            local_sp_8 = var_35;
            var_36 = local_sp_8 + (-8L);
            *(uint64_t *)var_36 = 4219674UL;
            indirect_placeholder();
            local_sp_10 = var_36;
            *(uint64_t *)(local_sp_10 + (-8L)) = 4219691UL;
            indirect_placeholder();
            return;
        }
    }
    if (*(unsigned char *)(rsi + 160UL) != '\x00') {
        *(uint64_t *)(local_sp_10 + (-8L)) = 4219691UL;
        indirect_placeholder();
        return;
    }
    if ((uint64_t)(unsigned char)rbp_3 == 0UL) {
        var_35 = local_sp_9 + (-8L);
        *(uint64_t *)var_35 = 4220010UL;
        indirect_placeholder();
        local_sp_8 = var_35;
    }
    var_36 = local_sp_8 + (-8L);
    *(uint64_t *)var_36 = 4219674UL;
    indirect_placeholder();
    local_sp_10 = var_36;
    *(uint64_t *)(local_sp_10 + (-8L)) = 4219691UL;
    indirect_placeholder();
}
