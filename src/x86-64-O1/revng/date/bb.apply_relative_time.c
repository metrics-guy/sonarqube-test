typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
uint64_t bb_apply_relative_time(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    bool var_9;
    uint32_t *var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t *var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t *var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t *var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t *var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t *var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t *var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t storemerge_in_in;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t *var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t *var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t *var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t *var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t *var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t *var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t rdx_0;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = *(uint64_t *)(var_0 | 8UL);
    var_3 = *(uint64_t *)(var_0 | 16UL);
    var_4 = *(uint64_t *)(var_0 | 24UL);
    var_5 = *(uint64_t *)(var_0 | 32UL);
    var_6 = *(uint64_t *)(var_0 | 40UL);
    var_7 = *(uint64_t *)(var_0 | 48UL);
    var_8 = (uint64_t)*(uint32_t *)(var_0 | 56UL);
    var_9 = ((int)(uint32_t)rsi < (int)0U);
    var_10 = (uint32_t *)(rdi + 152UL);
    var_11 = (uint64_t)*var_10;
    rdx_0 = 0UL;
    if (var_9) {
        var_38 = var_11 - var_8;
        var_39 = helper_cc_compute_all_wrapper(var_38, var_8, 0UL, 16U);
        *var_10 = (uint32_t)var_38;
        var_40 = (uint64_t *)(rdi + 144UL);
        var_41 = *var_40 - var_7;
        var_42 = helper_cc_compute_all_wrapper(var_41, var_7, 0UL, 17U);
        *var_40 = var_41;
        var_43 = (uint64_t *)(rdi + 136UL);
        var_44 = *var_43 - var_6;
        var_45 = helper_cc_compute_all_wrapper(var_44, var_6, 0UL, 17U);
        *var_43 = var_44;
        var_46 = (uint64_t *)(rdi + 128UL);
        var_47 = *var_46 - var_5;
        var_48 = helper_cc_compute_all_wrapper(var_47, var_5, 0UL, 17U);
        *var_46 = var_47;
        var_49 = (uint64_t *)(rdi + 120UL);
        var_50 = *var_49 - var_4;
        var_51 = helper_cc_compute_all_wrapper(var_50, var_4, 0UL, 17U);
        *var_49 = var_50;
        var_52 = (uint64_t *)(rdi + 112UL);
        var_53 = *var_52 - var_3;
        var_54 = helper_cc_compute_all_wrapper(var_53, var_3, 0UL, 17U);
        *var_52 = var_53;
        var_55 = (uint64_t *)(rdi + 104UL);
        var_56 = *var_55 - var_2;
        var_57 = helper_cc_compute_all_wrapper(var_56, var_2, 0UL, 17U);
        *var_55 = var_56;
        storemerge_in_in = (((var_48 | ((var_39 | var_42) | var_45)) | var_51) | var_54) | var_57;
    } else {
        var_12 = var_8 + var_11;
        var_13 = helper_cc_compute_all_wrapper(var_12, var_11, 0UL, 8U);
        *var_10 = (uint32_t)var_12;
        var_14 = (uint64_t *)(rdi + 144UL);
        var_15 = *var_14;
        var_16 = var_7 + var_15;
        var_17 = helper_cc_compute_all_wrapper(var_16, var_15, 0UL, 9U);
        *var_14 = var_16;
        var_18 = (uint64_t *)(rdi + 136UL);
        var_19 = *var_18;
        var_20 = var_6 + var_19;
        var_21 = helper_cc_compute_all_wrapper(var_20, var_19, 0UL, 9U);
        *var_18 = var_20;
        var_22 = (uint64_t *)(rdi + 128UL);
        var_23 = *var_22;
        var_24 = var_5 + var_23;
        var_25 = helper_cc_compute_all_wrapper(var_24, var_23, 0UL, 9U);
        *var_22 = var_24;
        var_26 = (uint64_t *)(rdi + 120UL);
        var_27 = *var_26;
        var_28 = var_4 + var_27;
        var_29 = helper_cc_compute_all_wrapper(var_28, var_27, 0UL, 9U);
        *var_26 = var_28;
        var_30 = (uint64_t *)(rdi + 112UL);
        var_31 = *var_30;
        var_32 = var_3 + var_31;
        var_33 = helper_cc_compute_all_wrapper(var_32, var_31, 0UL, 9U);
        *var_30 = var_32;
        var_34 = (uint64_t *)(rdi + 104UL);
        var_35 = *var_34;
        var_36 = var_2 + var_35;
        var_37 = helper_cc_compute_all_wrapper(var_36, var_35, 0UL, 9U);
        *var_34 = var_36;
        storemerge_in_in = (((var_25 | ((var_13 | var_17) | var_21)) | var_29) | var_33) | var_37;
    }
    if ((uint64_t)((uint16_t)storemerge_in_in & (unsigned short)2048U) != 0UL) {
        return rdx_0;
    }
    *(unsigned char *)(rdi + 161UL) = (unsigned char)'\x01';
    rdx_0 = 1UL;
    return rdx_0;
}
