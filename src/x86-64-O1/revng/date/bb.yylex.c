typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_110_ret_type;
struct indirect_placeholder_111_ret_type;
struct indirect_placeholder_110_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_111_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r15(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern uint64_t indirect_placeholder_15(void);
extern struct indirect_placeholder_110_ret_type indirect_placeholder_110(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_111_ret_type indirect_placeholder_111(uint64_t param_0, uint64_t param_1);
uint64_t bb_yylex(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t *var_11;
    unsigned __int128 var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t local_sp_7;
    uint64_t rax_0;
    uint64_t var_47;
    uint64_t *_pre_phi;
    uint64_t local_sp_9_ph;
    uint64_t local_sp_0;
    uint64_t cc_src2_1_ph;
    uint64_t var_12;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_37;
    uint64_t rbx_0;
    uint64_t var_31;
    uint64_t var_32;
    struct indirect_placeholder_111_ret_type var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    unsigned char var_85;
    uint64_t var_98;
    uint64_t rbp_1;
    uint64_t var_99;
    uint64_t var_100;
    uint64_t local_sp_2;
    uint64_t local_sp_1;
    uint64_t var_90;
    uint64_t local_sp_2_ph;
    uint64_t r13_0;
    uint64_t r14_0;
    uint64_t r14_3;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t var_91;
    uint64_t r13_4;
    uint64_t local_sp_3;
    uint64_t r13_1;
    uint64_t r14_1;
    uint32_t *var_82;
    uint32_t var_83;
    uint64_t var_84;
    uint64_t r13_2;
    uint64_t rbp_2;
    uint64_t var_55;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t *var_88;
    uint64_t var_89;
    uint64_t r13_3_ph;
    uint64_t r14_2_ph;
    uint64_t r13_3;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t *var_94;
    uint64_t var_95;
    uint64_t rbx_2;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t r13_5;
    uint32_t var_76;
    uint64_t var_77;
    unsigned char var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t var_72;
    uint64_t var_73;
    uint32_t var_68;
    uint64_t var_69;
    uint64_t *var_70;
    uint64_t var_71;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t storemerge8;
    uint64_t var_41;
    uint64_t rdx_0;
    uint64_t var_42;
    unsigned char var_43;
    uint64_t var_44;
    uint64_t local_sp_9_be;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_24;
    uint64_t rbx_1;
    uint64_t local_sp_5;
    unsigned char rbp_0;
    uint64_t var_25;
    uint64_t var_26;
    unsigned char var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint32_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_45;
    uint64_t rax_1;
    uint64_t var_20;
    uint64_t local_sp_6;
    uint64_t r15_0;
    uint64_t cc_src2_0;
    uint64_t var_53;
    bool var_54;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    unsigned char var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t *var_63;
    uint64_t var_64;
    uint64_t var_46;
    uint64_t local_sp_8;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_17;
    uint64_t local_sp_9;
    unsigned char *_cast1;
    unsigned char var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_r8();
    var_8 = init_r9();
    var_9 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_10 = var_0 + (-104L);
    *(uint64_t *)var_10 = rdi;
    var_11 = (uint64_t *)rsi;
    rax_0 = 63UL;
    local_sp_9_ph = var_10;
    cc_src2_1_ph = var_9;
    rbx_2 = 0UL;
    rdx_0 = 0UL;
    r15_0 = 0UL;
    while (1U)
        {
            cc_src2_0 = cc_src2_1_ph;
            var_12 = *var_11;
            local_sp_9 = local_sp_9_ph;
            while (1U)
                {
                    _cast1 = (unsigned char *)var_12;
                    var_13 = *_cast1;
                    var_14 = (uint64_t)var_13;
                    var_15 = local_sp_9 + (-8L);
                    *(uint64_t *)var_15 = 4218401UL;
                    var_16 = indirect_placeholder_1(var_14);
                    local_sp_9_be = var_15;
                    rbx_0 = var_15;
                    rbp_0 = var_13;
                    rbp_1 = var_14;
                    if ((uint64_t)(unsigned char)var_16 != 0UL) {
                        var_17 = var_12 + 1UL;
                        *var_11 = var_17;
                        var_44 = var_17;
                        var_12 = var_44;
                        local_sp_9 = local_sp_9_be;
                        continue;
                    }
                    var_18 = local_sp_9 + (-16L);
                    *(uint64_t *)var_18 = 4218413UL;
                    var_19 = indirect_placeholder_1(var_14);
                    local_sp_6 = var_18;
                    local_sp_8 = var_18;
                    if ((uint64_t)(unsigned char)var_19 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_21 = (uint32_t)var_14 + (-43);
                    rax_0 = 0UL;
                    if ((uint64_t)((unsigned char)(uint64_t)var_21 & '\xfd') != 0UL) {
                        loop_state_var = 2U;
                        break;
                    }
                    var_22 = local_sp_9 + (-24L);
                    *(uint64_t *)var_22 = 4218436UL;
                    var_23 = indirect_placeholder_1(var_14);
                    local_sp_9_be = var_22;
                    local_sp_5 = var_22;
                    if ((uint64_t)(unsigned char)var_23 != 0UL) {
                        var_24 = local_sp_9 + 11UL;
                        loop_state_var = 3U;
                        break;
                    }
                    if ((uint64_t)(var_13 + '\xd8') != 0UL) {
                        *var_11 = (var_12 + 1UL);
                        var_38 = (uint64_t)(uint32_t)(uint64_t)*_cast1;
                        *(uint64_t *)(local_sp_9 + (-32L)) = 4219030UL;
                        var_39 = indirect_placeholder_1(var_38);
                        var_40 = (uint64_t)(unsigned char)var_39;
                        rax_0 = var_40;
                        loop_state_var = 0U;
                        break;
                    }
                    var_41 = *var_11;
                    while (1U)
                        {
                            var_42 = var_41 + 1UL;
                            *var_11 = var_42;
                            var_43 = *(unsigned char *)var_41;
                            var_41 = var_42;
                            var_44 = var_42;
                            if (var_43 != '\x00') {
                                loop_state_var = 0U;
                                break;
                            }
                            if ((uint64_t)(var_43 + '\xd8') == 0UL) {
                                storemerge8 = rdx_0 + 1UL;
                            } else {
                                storemerge8 = rdx_0 + ((uint64_t)(var_43 + '\xd7') == 0UL);
                            }
                            rdx_0 = storemerge8;
                            if (storemerge8 == 0UL) {
                                continue;
                            }
                            loop_state_var = 1U;
                            break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 1U:
                        {
                            break;
                        }
                        break;
                      case 0U:
                        {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    var_20 = (var_19 & (-256L)) | ((uint64_t)(var_13 + '\xd3') == 0UL);
                    rax_1 = var_20;
                    if ((uint64_t)((var_13 + '\xd5') & '\xfd') != 0UL) {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    var_46 = helper_cc_compute_c_wrapper(rax_1 + (-1L), 1UL, cc_src2_1_ph, 14U);
                    cc_src2_1_ph = var_46;
                    cc_src2_0 = var_46;
                    var_47 = *var_11 + 1UL;
                    *var_11 = var_47;
                    var_48 = (uint64_t)*(unsigned char *)var_47;
                    var_49 = local_sp_8 + (-8L);
                    *(uint64_t *)var_49 = 4219158UL;
                    var_50 = indirect_placeholder_1(var_48);
                    rbp_1 = var_48;
                    local_sp_8 = var_49;
                    do {
                        var_47 = *var_11 + 1UL;
                        *var_11 = var_47;
                        var_48 = (uint64_t)*(unsigned char *)var_47;
                        var_49 = local_sp_8 + (-8L);
                        *(uint64_t *)var_49 = 4219158UL;
                        var_50 = indirect_placeholder_1(var_48);
                        rbp_1 = var_48;
                        local_sp_8 = var_49;
                    } while ((uint64_t)(unsigned char)var_50 != 0UL);
                    var_51 = local_sp_8 + (-16L);
                    *(uint64_t *)var_51 = 4219169UL;
                    var_52 = indirect_placeholder_1(var_48);
                    local_sp_9_ph = var_51;
                    local_sp_6 = var_51;
                    if ((uint64_t)(unsigned char)var_52 == 0UL) {
                        continue;
                    }
                    r15_0 = (uint64_t)((uint32_t)((0UL - var_46) & 2UL) + (-1));
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 2U:
                {
                    var_45 = (uint64_t)(var_21 & (-256)) | (var_13 == '-');
                    rax_1 = var_45;
                    var_46 = helper_cc_compute_c_wrapper(rax_1 + (-1L), 1UL, cc_src2_1_ph, 14U);
                    cc_src2_1_ph = var_46;
                    cc_src2_0 = var_46;
                    var_47 = *var_11 + 1UL;
                    *var_11 = var_47;
                    var_48 = (uint64_t)*(unsigned char *)var_47;
                    var_49 = local_sp_8 + (-8L);
                    *(uint64_t *)var_49 = 4219158UL;
                    var_50 = indirect_placeholder_1(var_48);
                    rbp_1 = var_48;
                    local_sp_8 = var_49;
                    do {
                        var_47 = *var_11 + 1UL;
                        *var_11 = var_47;
                        var_48 = (uint64_t)*(unsigned char *)var_47;
                        var_49 = local_sp_8 + (-8L);
                        *(uint64_t *)var_49 = 4219158UL;
                        var_50 = indirect_placeholder_1(var_48);
                        rbp_1 = var_48;
                        local_sp_8 = var_49;
                    } while ((uint64_t)(unsigned char)var_50 != 0UL);
                    var_51 = local_sp_8 + (-16L);
                    *(uint64_t *)var_51 = 4219169UL;
                    var_52 = indirect_placeholder_1(var_48);
                    local_sp_9_ph = var_51;
                    local_sp_6 = var_51;
                    if ((uint64_t)(unsigned char)var_52 == 0UL) {
                        continue;
                    }
                    r15_0 = (uint64_t)((uint32_t)((0UL - var_46) & 2UL) + (-1));
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 0U:
                {
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 3U:
                {
                    loop_state_var = 2U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            return rax_0;
        }
        break;
      case 1U:
        {
            var_53 = *var_11;
            var_54 = ((int)(uint32_t)r15_0 > (int)4294967295U);
            local_sp_7 = local_sp_6;
            r13_5 = var_53;
            rbp_2 = rbp_1;
            while (1U)
                {
                    var_55 = (uint64_t)(unsigned char)rbp_2;
                    var_56 = (uint64_t)((long)((var_54 ? (var_55 + 4294967248UL) : (48UL - var_55)) << 32UL) >> (long)32UL);
                    var_57 = rbx_2 + var_56;
                    var_58 = helper_cc_compute_all_wrapper(var_57, var_56, 0UL, 9U);
                    if ((uint64_t)((uint16_t)var_58 & (unsigned short)2048U) != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_59 = r13_5 + 1UL;
                    var_60 = *(unsigned char *)var_59;
                    var_61 = (uint64_t)var_60;
                    var_62 = local_sp_7 + (-8L);
                    var_63 = (uint64_t *)var_62;
                    *var_63 = 4219239UL;
                    var_64 = indirect_placeholder_1(var_61);
                    _pre_phi = var_63;
                    local_sp_7 = var_62;
                    r13_5 = var_59;
                    rbp_2 = var_61;
                    if ((uint64_t)(unsigned char)var_64 == 0UL) {
                        var_65 = (unsigned __int128)var_57 * 10ULL;
                        var_66 = (uint64_t)var_65;
                        var_67 = helper_cc_compute_all_wrapper(var_66, (uint64_t)((long)var_66 >> (long)63UL) - (uint64_t)(var_65 >> 64ULL), 0UL, 5U);
                        rbx_2 = var_66;
                        if ((uint64_t)((uint16_t)var_67 & (unsigned short)2048U) == 0UL) {
                            continue;
                        }
                        loop_state_var = 0U;
                        break;
                    }
                    if (((var_60 + '\xd4') & '\xfd') != '\x00') {
                        loop_state_var = 1U;
                        break;
                    }
                    var_68 = (uint32_t)(uint64_t)*(unsigned char *)(r13_5 + 2UL);
                    var_69 = (uint64_t)var_68;
                    var_70 = (uint64_t *)(local_sp_7 + (-16L));
                    *var_70 = 4218521UL;
                    var_71 = indirect_placeholder_1(var_69);
                    _pre_phi = var_70;
                    if ((uint64_t)(unsigned char)var_71 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_72 = local_sp_7 + (-24L);
                    *(uint64_t *)var_72 = 4218537UL;
                    var_73 = indirect_placeholder_15();
                    local_sp_3 = var_72;
                    if ((uint64_t)(unsigned char)var_73 != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_74 = r13_5 + 3UL;
                    var_75 = (uint64_t)(var_68 + (-48));
                    *(uint32_t *)(local_sp_7 + (-12L)) = 8U;
                    r13_4 = var_74;
                    r14_3 = var_75;
                    rax_0 = 276UL;
                    loop_state_var = 2U;
                    break;
                }
            switch (loop_state_var) {
              case 1U:
                {
                    var_101 = r15_0 >> 31UL;
                    var_102 = *_pre_phi;
                    *(unsigned char *)var_102 = (unsigned char)var_101;
                    *(uint64_t *)(var_102 + 8UL) = var_57;
                    *(uint64_t *)(var_102 + 16UL) = (var_59 - *var_11);
                    *var_11 = var_59;
                    var_103 = (r15_0 == 0UL) ? 275UL : 274UL;
                    rax_0 = var_103;
                }
                break;
              case 2U:
                {
                    var_82 = (uint32_t *)(local_sp_3 + 4UL);
                    var_83 = *var_82 + (-1);
                    var_84 = (uint64_t)var_83;
                    *var_82 = var_83;
                    r14_0 = r14_1;
                    r14_3 = r14_1;
                    r13_4 = r13_1;
                    r13_2 = r13_1;
                    r13_3_ph = r13_1;
                    r14_2_ph = r14_1;
                    do {
                        var_76 = (uint32_t)r14_3 * 10U;
                        var_77 = (uint64_t)(var_76 & (-2));
                        var_78 = *(unsigned char *)r13_4;
                        var_79 = (uint64_t)(uint32_t)(uint64_t)var_78;
                        *(uint32_t *)(local_sp_3 + 8UL) = (uint32_t)var_78;
                        var_80 = local_sp_3 + (-8L);
                        *(uint64_t *)var_80 = 4218602UL;
                        var_81 = indirect_placeholder_1(var_79);
                        local_sp_1 = var_80;
                        local_sp_2_ph = var_80;
                        local_sp_3 = var_80;
                        r13_1 = r13_4;
                        r14_1 = var_77;
                        if ((uint64_t)(unsigned char)var_81 == 0UL) {
                            r13_1 = r13_4 + 1UL;
                            r14_1 = (uint64_t)((var_76 + *(uint32_t *)local_sp_3) + (-48));
                        }
                        var_82 = (uint32_t *)(local_sp_3 + 4UL);
                        var_83 = *var_82 + (-1);
                        var_84 = (uint64_t)var_83;
                        *var_82 = var_83;
                        r14_0 = r14_1;
                        r14_3 = r14_1;
                        r13_4 = r13_1;
                        r13_2 = r13_1;
                        r13_3_ph = r13_1;
                        r14_2_ph = r14_1;
                    } while (var_84 != 0UL);
                    if (!var_54) {
                        r14_0 = r14_2_ph;
                        local_sp_2 = local_sp_2_ph;
                        r13_3 = r13_3_ph;
                        var_92 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)r13_3;
                        var_93 = local_sp_2 + (-8L);
                        var_94 = (uint64_t *)var_93;
                        *var_94 = 4218714UL;
                        var_95 = indirect_placeholder_1(var_92);
                        local_sp_0 = var_93;
                        r13_0 = r13_3;
                        local_sp_2 = var_93;
                        while ((uint64_t)(unsigned char)var_95 != 0UL)
                            {
                                r13_3 = r13_3 + 1UL;
                                var_92 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)r13_3;
                                var_93 = local_sp_2 + (-8L);
                                var_94 = (uint64_t *)var_93;
                                *var_94 = 4218714UL;
                                var_95 = indirect_placeholder_1(var_92);
                                local_sp_0 = var_93;
                                r13_0 = r13_3;
                                local_sp_2 = var_93;
                            }
                        if (var_54 || ((uint64_t)(uint32_t)r14_2_ph == 0UL)) {
                            rax_0 = 63UL;
                            if (var_57 != 9223372036854775808UL) {
                                var_96 = var_57 + (-1L);
                                var_97 = *(uint64_t *)local_sp_0;
                                *(uint64_t *)var_97 = var_96;
                                *(uint64_t *)(var_97 + 8UL) = (uint64_t)((long)(4294967296000000000UL - (r14_0 << 32UL)) >> (long)32UL);
                                *var_11 = r13_0;
                                rax_0 = 276UL;
                            }
                            return rax_0;
                        }
                        var_98 = *var_94;
                        *(uint64_t *)var_98 = var_57;
                        *(uint64_t *)(var_98 + 8UL) = (uint64_t)((long)(r14_2_ph << 32UL) >> (long)32UL);
                        *var_11 = r13_3;
                        var_99 = helper_cc_compute_c_wrapper(r15_0 + (-1L), 1UL, cc_src2_0, 16U);
                        var_100 = (uint64_t)((uint32_t)var_99 + 276U);
                        rax_0 = var_100;
                        return rax_0;
                    }
                    while (1U)
                        {
                            var_85 = *(unsigned char *)r13_2;
                            var_86 = (uint64_t)(uint32_t)(uint64_t)var_85;
                            var_87 = local_sp_1 + (-8L);
                            var_88 = (uint64_t *)var_87;
                            *var_88 = 4218640UL;
                            var_89 = indirect_placeholder_1(var_86);
                            local_sp_0 = var_87;
                            local_sp_1 = var_87;
                            local_sp_2_ph = var_87;
                            r13_0 = r13_2;
                            r13_3_ph = r13_2;
                            if ((uint64_t)(unsigned char)var_89 == 0UL) {
                                if (var_85 == '0') {
                                    r13_2 = r13_2 + 1UL;
                                    continue;
                                }
                                var_90 = (uint64_t)((uint32_t)r14_1 + 1U);
                                r14_2_ph = var_90;
                                loop_state_var = 1U;
                                break;
                            }
                            if (r14_1 != 0UL) {
                                loop_state_var = 2U;
                                break;
                            }
                            var_91 = *var_88;
                            *(uint64_t *)var_91 = var_57;
                            *(uint64_t *)(var_91 + 8UL) = 0UL;
                            *var_11 = r13_2;
                            loop_state_var = 0U;
                            break;
                        }
                    switch (loop_state_var) {
                      case 1U:
                        {
                            r14_0 = r14_2_ph;
                            local_sp_2 = local_sp_2_ph;
                            r13_3 = r13_3_ph;
                            var_92 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)r13_3;
                            var_93 = local_sp_2 + (-8L);
                            var_94 = (uint64_t *)var_93;
                            *var_94 = 4218714UL;
                            var_95 = indirect_placeholder_1(var_92);
                            local_sp_0 = var_93;
                            r13_0 = r13_3;
                            local_sp_2 = var_93;
                            while ((uint64_t)(unsigned char)var_95 != 0UL)
                                {
                                    r13_3 = r13_3 + 1UL;
                                    var_92 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)r13_3;
                                    var_93 = local_sp_2 + (-8L);
                                    var_94 = (uint64_t *)var_93;
                                    *var_94 = 4218714UL;
                                    var_95 = indirect_placeholder_1(var_92);
                                    local_sp_0 = var_93;
                                    r13_0 = r13_3;
                                    local_sp_2 = var_93;
                                }
                            if (!(var_54 || ((uint64_t)(uint32_t)r14_2_ph == 0UL))) {
                                var_98 = *var_94;
                                *(uint64_t *)var_98 = var_57;
                                *(uint64_t *)(var_98 + 8UL) = (uint64_t)((long)(r14_2_ph << 32UL) >> (long)32UL);
                                *var_11 = r13_3;
                                var_99 = helper_cc_compute_c_wrapper(r15_0 + (-1L), 1UL, cc_src2_0, 16U);
                                var_100 = (uint64_t)((uint32_t)var_99 + 276U);
                                rax_0 = var_100;
                                return rax_0;
                            }
                            rax_0 = 63UL;
                            if (var_57 == 9223372036854775808UL) {
                                var_96 = var_57 + (-1L);
                                var_97 = *(uint64_t *)local_sp_0;
                                *(uint64_t *)var_97 = var_96;
                                *(uint64_t *)(var_97 + 8UL) = (uint64_t)((long)(4294967296000000000UL - (r14_0 << 32UL)) >> (long)32UL);
                                *var_11 = r13_0;
                                rax_0 = 276UL;
                            }
                        }
                        break;
                      case 2U:
                        {
                            rax_0 = 63UL;
                            if (var_57 == 9223372036854775808UL) {
                                var_96 = var_57 + (-1L);
                                var_97 = *(uint64_t *)local_sp_0;
                                *(uint64_t *)var_97 = var_96;
                                *(uint64_t *)(var_97 + 8UL) = (uint64_t)((long)(4294967296000000000UL - (r14_0 << 32UL)) >> (long)32UL);
                                *var_11 = r13_0;
                                rax_0 = 276UL;
                            }
                        }
                        break;
                    }
                }
                break;
            }
        }
        break;
      case 2U:
        {
            var_26 = *var_11 + 1UL;
            *var_11 = var_26;
            var_27 = *(unsigned char *)var_26;
            var_28 = (uint64_t)var_27;
            var_29 = local_sp_5 + (-8L);
            *(uint64_t *)var_29 = 4218924UL;
            var_30 = indirect_placeholder_1(var_28);
            local_sp_5 = var_29;
            rbx_0 = rbx_1;
            rbp_0 = var_27;
            do {
                var_25 = helper_cc_compute_c_wrapper(rbx_0 - var_24, var_24, cc_src2_1_ph, 17U);
                rbx_1 = rbx_0;
                if (var_25 == 0UL) {
                    *(unsigned char *)rbx_0 = rbp_0;
                    rbx_1 = rbx_0 + 1UL;
                }
                var_26 = *var_11 + 1UL;
                *var_11 = var_26;
                var_27 = *(unsigned char *)var_26;
                var_28 = (uint64_t)var_27;
                var_29 = local_sp_5 + (-8L);
                *(uint64_t *)var_29 = 4218924UL;
                var_30 = indirect_placeholder_1(var_28);
                local_sp_5 = var_29;
                rbx_0 = rbx_1;
                rbp_0 = var_27;
            } while (!(((uint64_t)(var_27 + '\xd2') != 0UL) && ((uint64_t)(unsigned char)var_30 == 0UL)));
            *(unsigned char *)rbx_1 = (unsigned char)'\x00';
            var_31 = local_sp_5 + 8UL;
            var_32 = local_sp_5 + (-16L);
            *(uint64_t *)var_32 = 4218950UL;
            var_33 = indirect_placeholder_111(rsi, var_31);
            var_34 = var_33.field_0;
            var_35 = var_33.field_1;
            var_36 = var_33.field_2;
            if (var_34 == 0UL) {
                **(uint64_t **)var_32 = (uint64_t)*(uint32_t *)(var_34 + 12UL);
                var_37 = (uint64_t)*(uint32_t *)(var_34 + 8UL);
                rax_0 = var_37;
            } else {
                if (*(unsigned char *)(rsi + 217UL) == '\x00') {
                    *(uint64_t *)(local_sp_5 + (-24L)) = 4219007UL;
                    indirect_placeholder_110(0UL, var_35, 4301720UL, local_sp_5, var_36, var_7, var_8);
                }
            }
        }
        break;
    }
}
