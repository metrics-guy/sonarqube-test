typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_tzalloc_ret_type;
struct bb_tzalloc_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint32_t init_state_0x82fc(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0);
extern void indirect_placeholder_2(uint64_t param_0, uint64_t param_1);
extern uint64_t init_rcx(void);
extern uint64_t init_rdx(void);
struct bb_tzalloc_ret_type bb_tzalloc(uint64_t rdi) {
    struct bb_tzalloc_ret_type mrv2;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint32_t var_6;
    uint64_t var_7;
    uint64_t var_15;
    uint64_t rcx_2;
    uint64_t rdx_0;
    uint64_t rbx_0;
    uint64_t rcx_0;
    struct bb_tzalloc_ret_type mrv;
    struct bb_tzalloc_ret_type mrv1;
    uint64_t var_8;
    uint64_t rdi3_0;
    uint64_t rcx_1;
    unsigned char var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_16;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rdx();
    var_2 = init_rbx();
    var_3 = init_rbp();
    var_4 = init_r12();
    var_5 = init_rcx();
    var_6 = init_state_0x82fc();
    var_7 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_4;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    rcx_2 = 0UL;
    rdx_0 = var_1;
    rbx_0 = 0UL;
    rcx_0 = var_5;
    rdi3_0 = rdi;
    rcx_1 = 18446744073709551615UL;
    if (rdi == 0UL) {
        *(uint64_t *)(var_0 + (-32L)) = 4248881UL;
        var_16 = indirect_placeholder_1(128UL);
        if (var_16 == 0UL) {
            *(uint64_t *)var_16 = 0UL;
            *(unsigned char *)(var_16 + 8UL) = (unsigned char)'\x00';
            *(unsigned char *)(var_16 + 9UL) = (unsigned char)'\x00';
            rbx_0 = var_16;
        }
    } else {
        var_8 = (uint64_t)var_6;
        while (rcx_1 != 0UL)
            {
                var_9 = *(unsigned char *)rdi3_0;
                var_10 = rcx_1 + (-1L);
                rcx_1 = var_10;
                rcx_2 = var_10;
                if (var_9 == '\x00') {
                    break;
                }
                rdi3_0 = rdi3_0 + var_8;
            }
        var_11 = rcx_2 ^ (-1L);
        var_12 = helper_cc_compute_c_wrapper(18446744073709551497UL - (-119L), 118UL, var_7, 17U);
        var_13 = (var_12 == 0UL) ? ((16UL - rcx_2) & (-8L)) : 128UL;
        *(uint64_t *)(var_0 + (-32L)) = 4248825UL;
        var_14 = indirect_placeholder_1(var_13);
        rcx_0 = var_11;
        if (var_14 == 0UL) {
            *(uint64_t *)var_14 = 0UL;
            *(unsigned char *)(var_14 + 8UL) = (unsigned char)'\x01';
            var_15 = var_14 + 9UL;
            *(unsigned char *)var_15 = (unsigned char)'\x00';
            *(uint64_t *)(var_0 + (-40L)) = 4248863UL;
            indirect_placeholder_2(var_11, var_15);
            rdx_0 = var_11;
            rbx_0 = var_14;
        }
    }
    mrv.field_0 = rbx_0;
    mrv1 = mrv;
    mrv1.field_1 = rdx_0;
    mrv2 = mrv1;
    mrv2.field_2 = rcx_0;
    return mrv2;
}
