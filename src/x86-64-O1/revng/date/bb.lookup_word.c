typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_lookup_word_ret_type;
struct bb_lookup_word_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_rax(void);
extern uint64_t init_r13(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r15(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint32_t init_state_0x82fc(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0);
extern uint64_t init_rcx(void);
extern uint64_t init_rdx(void);
struct bb_lookup_word_ret_type bb_lookup_word(uint64_t rdi, uint64_t rsi) {
    unsigned char var_19;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint32_t var_10;
    uint64_t var_11;
    unsigned char var_12;
    uint64_t rax_2;
    uint64_t rbx_6;
    uint64_t rdx_1;
    uint64_t rdx_3;
    uint64_t local_sp_8;
    uint64_t var_22;
    uint64_t rbx_5;
    uint64_t rbx_7;
    uint64_t var_23;
    uint64_t rdi3_1;
    uint64_t rcx_4;
    uint64_t rcx_5;
    unsigned char var_24;
    uint64_t var_25;
    uint64_t var_26;
    bool r14_0;
    uint64_t *var_27;
    uint64_t rcx_6;
    struct bb_lookup_word_ret_type mrv;
    struct bb_lookup_word_ret_type mrv1;
    struct bb_lookup_word_ret_type mrv2;
    unsigned char rdi3_2_in;
    uint64_t local_sp_6;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t local_sp_7;
    bool var_20;
    uint64_t var_21;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rdx();
    var_3 = init_r13();
    var_4 = init_rbx();
    var_5 = init_r15();
    var_6 = init_rbp();
    var_7 = init_r12();
    var_8 = init_r14();
    var_9 = init_rcx();
    var_10 = init_state_0x82fc();
    *(uint64_t *)(var_0 + (-8L)) = var_5;
    *(uint64_t *)(var_0 + (-16L)) = var_8;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_7;
    *(uint64_t *)(var_0 + (-40L)) = var_6;
    *(uint64_t *)(var_0 + (-48L)) = var_4;
    var_11 = var_0 + (-56L);
    var_12 = *(unsigned char *)rsi;
    rax_2 = var_1;
    rbx_6 = rsi;
    rdx_1 = var_2;
    rdx_3 = var_2;
    rbx_7 = 4308320UL;
    rdi3_1 = rsi;
    rcx_4 = 18446744073709551615UL;
    rcx_5 = 0UL;
    r14_0 = 0;
    rcx_6 = var_9;
    rdi3_2_in = var_12;
    local_sp_6 = var_11;
    local_sp_7 = var_11;
    if (var_12 == '\x00') {
        var_13 = (uint64_t)(uint32_t)(uint64_t)rdi3_2_in;
        *(uint64_t *)(local_sp_6 + (-8L)) = 4216941UL;
        var_14 = indirect_placeholder_1(var_13);
        var_15 = (uint64_t)(unsigned char)var_14;
        var_16 = local_sp_6 + (-16L);
        *(uint64_t *)var_16 = 4216949UL;
        var_17 = indirect_placeholder_1(var_15);
        *(unsigned char *)rbx_6 = (unsigned char)var_17;
        var_18 = rbx_6 + 1UL;
        var_19 = *(unsigned char *)var_18;
        rax_2 = var_17;
        rbx_6 = var_18;
        rdi3_2_in = var_19;
        local_sp_6 = var_16;
        local_sp_7 = var_16;
        do {
            var_13 = (uint64_t)(uint32_t)(uint64_t)rdi3_2_in;
            *(uint64_t *)(local_sp_6 + (-8L)) = 4216941UL;
            var_14 = indirect_placeholder_1(var_13);
            var_15 = (uint64_t)(unsigned char)var_14;
            var_16 = local_sp_6 + (-16L);
            *(uint64_t *)var_16 = 4216949UL;
            var_17 = indirect_placeholder_1(var_15);
            *(unsigned char *)rbx_6 = (unsigned char)var_17;
            var_18 = rbx_6 + 1UL;
            var_19 = *(unsigned char *)var_18;
            rax_2 = var_17;
            rbx_6 = var_18;
            rdi3_2_in = var_19;
            local_sp_6 = var_16;
            local_sp_7 = var_16;
        } while (var_19 != '\x00');
    }
    var_20 = ((uint64_t)(uint32_t)rax_2 == 0UL);
    local_sp_8 = local_sp_7;
    while (1U)
        {
            var_21 = local_sp_8 + (-8L);
            *(uint64_t *)var_21 = 4216981UL;
            indirect_placeholder();
            rbx_5 = rbx_7;
            local_sp_8 = var_21;
            if (!var_20) {
                loop_state_var = 0U;
                break;
            }
            var_22 = rbx_7 + 16UL;
            rbx_5 = 4307904UL;
            rbx_7 = var_22;
            if (*(uint64_t *)var_22 == 0UL) {
                continue;
            }
            var_23 = (uint64_t)var_10;
            while (rcx_4 != 0UL)
                {
                    var_24 = *(unsigned char *)rdi3_1;
                    var_25 = rcx_4 + (-1L);
                    rcx_4 = var_25;
                    rcx_5 = var_25;
                    if (var_24 == '\x00') {
                        break;
                    }
                    rdi3_1 = rdi3_1 + var_23;
                }
            var_26 = rcx_5 ^ (-1L);
            rcx_6 = var_26;
            switch_state_var = 0;
            switch (rcx_5) {
              case 18446744073709551610UL:
                {
                    r14_0 = (*(unsigned char *)(rsi + 3UL) != '.');
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 18446744073709551611UL:
                {
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
              default:
                {
                    r14_0 = 1;
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            mrv.field_0 = rbx_5;
            mrv1 = mrv;
            mrv1.field_1 = rdx_3;
            mrv2 = mrv1;
            mrv2.field_2 = rcx_6;
            return mrv2;
        }
        break;
      case 1U:
        {
            var_27 = (uint64_t *)(var_21 + (-8L));
            if (r14_0) {
                *var_27 = 4217087UL;
                indirect_placeholder();
            } else {
                *var_27 = 4217128UL;
                indirect_placeholder();
                rdx_1 = 3UL;
            }
            rdx_3 = rdx_1;
        }
        break;
    }
}
