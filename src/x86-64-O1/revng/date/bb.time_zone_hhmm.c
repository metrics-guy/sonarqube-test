typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
void bb_time_zone_hhmm(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t rcx_1;
    unsigned __int128 var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t rcx_0;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t storemerge_in_in;
    uint64_t rcx_2;
    uint64_t var_9;
    uint64_t var_10;
    var_0 = revng_init_local_sp(0UL);
    var_1 = *(uint64_t *)(var_0 | 16UL);
    rcx_1 = var_1;
    if (((rsi >> 63UL) & ((long)*(uint64_t *)(var_0 | 24UL) < (long)3UL)) == 0UL) {
        if ((long)rsi >= (long)0UL) {
            var_2 = (unsigned __int128)var_1 * 60ULL;
            var_3 = (uint64_t)var_2;
            var_4 = helper_cc_compute_all_wrapper(var_3, (uint64_t)((long)var_3 >> (long)63UL) - (uint64_t)(var_2 >> 64ULL), 0UL, 5U);
            if (*(unsigned char *)(var_0 | 8UL) == '\x00') {
                var_7 = var_3 + rsi;
                var_8 = helper_cc_compute_all_wrapper(var_7, rsi, 0UL, 9U);
                rcx_0 = var_7;
                storemerge_in_in = var_8;
            } else {
                var_5 = var_3 - rsi;
                var_6 = helper_cc_compute_all_wrapper(var_5, rsi, 0UL, 17U);
                rcx_0 = var_5;
                storemerge_in_in = var_6;
            }
            rcx_2 = rcx_0;
            if ((uint64_t)(((uint16_t)var_4 | (uint16_t)storemerge_in_in) & (unsigned short)2048U) == 0UL) {
                return;
            }
            if ((rcx_2 + 1440UL) <= 2880UL) {
                *(uint32_t *)(rdi + 24UL) = ((uint32_t)rcx_2 * 60U);
            }
            return;
        }
    }
    rcx_1 = var_1 * 100UL;
    var_9 = (uint64_t)((long)(rcx_1 + (uint64_t)(((unsigned __int128)rcx_1 * 18446744073709551615ULL) >> 64ULL)) >> (long)6UL) - (uint64_t)((long)rcx_1 >> (long)63UL);
    var_10 = (var_9 * 60UL) + (rcx_1 + (var_9 * 18446744073709551516UL));
    rcx_2 = var_10;
    if ((rcx_2 + 1440UL) > 2880UL) {
        return;
    }
    *(uint32_t *)(rdi + 24UL) = ((uint32_t)rcx_2 * 60U);
    return;
}
