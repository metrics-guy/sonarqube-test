typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_52_ret_type;
struct indirect_placeholder_53_ret_type;
struct indirect_placeholder_52_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_53_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint32_t init_state_0x82fc(void);
extern struct indirect_placeholder_52_ret_type indirect_placeholder_52(uint64_t param_0);
extern struct indirect_placeholder_53_ret_type indirect_placeholder_53(uint64_t param_0);
void bb_input_from_argv(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint32_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t rbp_0;
    uint64_t rbp_1;
    uint64_t rbx_0;
    uint64_t local_sp_0;
    uint64_t var_22;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t rdx1_0;
    uint64_t rsi3_0;
    uint64_t rdi2_0;
    uint64_t rcx_0;
    uint64_t rcx_1;
    unsigned char var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    struct indirect_placeholder_52_ret_type var_18;
    uint64_t var_19;
    uint64_t var_20;
    unsigned char *_cast;
    unsigned char var_21;
    struct indirect_placeholder_53_ret_type var_11;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_state_0x82fc();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_8 = rsi << 32UL;
    var_9 = (uint64_t)((long)var_8 >> (long)32UL);
    var_10 = helper_cc_compute_all_wrapper(rsi, 0UL, 0UL, 24U);
    rbx_0 = rdi;
    rdx1_0 = rdi;
    rsi3_0 = var_9;
    rcx_0 = 18446744073709551615UL;
    rcx_1 = 0UL;
    if ((uint64_t)(((unsigned char)(var_10 >> 4UL) ^ (unsigned char)var_10) & '\xc0') != 0UL) {
        var_12 = (((rsi << 3UL) + 34359738360UL) & 34359738360UL) + rdi;
        var_13 = (uint64_t)var_7;
        while (1U)
            {
                rdi2_0 = *(uint64_t *)rdx1_0;
                while (rcx_0 != 0UL)
                    {
                        var_14 = *(unsigned char *)rdi2_0;
                        var_15 = rcx_0 + (-1L);
                        rcx_0 = var_15;
                        rcx_1 = var_15;
                        if (var_14 == '\x00') {
                            break;
                        }
                        rdi2_0 = rdi2_0 + var_13;
                    }
                var_16 = (rsi3_0 + (rcx_1 ^ (-1L))) + (-1L);
                rsi3_0 = var_16;
                if (rdx1_0 == var_12) {
                    break;
                }
                rdx1_0 = rdx1_0 + 8UL;
                continue;
            }
        var_17 = var_0 + (-64L);
        *(uint64_t *)var_17 = 4204987UL;
        var_18 = indirect_placeholder_52(var_16);
        var_19 = var_18.field_0;
        var_20 = var_19 + 1UL;
        _cast = (unsigned char *)var_19;
        var_21 = (unsigned char)rdx;
        rbp_0 = var_20;
        rbp_1 = var_19;
        local_sp_0 = var_17;
        var_22 = local_sp_0 + (-8L);
        *(uint64_t *)var_22 = 4205001UL;
        indirect_placeholder_1();
        *(uint64_t *)rbx_0 = rbp_1;
        *_cast = var_21;
        rbp_1 = var_20;
        local_sp_0 = var_22;
        while (rbx_0 != var_12)
            {
                rbx_0 = rbx_0 + 8UL;
                var_22 = local_sp_0 + (-8L);
                *(uint64_t *)var_22 = 4205001UL;
                indirect_placeholder_1();
                *(uint64_t *)rbx_0 = rbp_1;
                *_cast = var_21;
                rbp_1 = var_20;
                local_sp_0 = var_22;
            }
    }
    *(uint64_t *)(var_0 + (-64L)) = 4205047UL;
    var_11 = indirect_placeholder_53(var_9);
    rbp_0 = var_11.field_0;
    *(uint64_t *)((uint64_t)((long)var_8 >> (long)29UL) + rdi) = rbp_0;
    return;
}
