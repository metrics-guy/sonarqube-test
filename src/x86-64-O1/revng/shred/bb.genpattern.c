typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_23_ret_type;
struct indirect_placeholder_24_ret_type;
struct indirect_placeholder_23_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_24_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern struct indirect_placeholder_23_ret_type indirect_placeholder_23(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_24_ret_type indirect_placeholder_24(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_genpattern(uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t r10, uint64_t r9, uint64_t r8) {
    struct indirect_placeholder_24_ret_type var_38;
    struct indirect_placeholder_23_ret_type var_17;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t rbp_0_ph_ph;
    uint64_t r12_0_ph_ph;
    uint64_t r14_0_ph_ph;
    uint64_t local_sp_0_ph_ph;
    uint64_t *var_9;
    uint64_t local_sp_4;
    uint64_t rbp_0_ph;
    uint64_t r12_0_ph;
    uint64_t rbp_0;
    uint32_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t r86_290;
    uint64_t rbx_0;
    uint64_t rbp_1;
    uint64_t r104_3;
    uint64_t r12_1;
    uint64_t r12_285;
    uint64_t r14_1;
    uint64_t r14_286;
    uint64_t r104_0;
    uint64_t r104_287;
    uint64_t local_sp_1;
    uint64_t local_sp_388;
    uint64_t r95_0;
    uint64_t r95_289;
    uint64_t r86_0;
    uint64_t r104_1;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t local_sp_2;
    uint64_t r95_1;
    uint64_t r86_1;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t *_pre_phi103;
    uint64_t var_25;
    uint64_t var_26;
    bool var_27;
    uint64_t var_28;
    uint64_t r95_3;
    uint64_t r86_3;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t r13_0;
    uint64_t rbx_1;
    uint64_t r104_5;
    uint64_t r12_3;
    uint64_t local_sp_6;
    uint64_t r104_4;
    uint64_t r95_5;
    uint64_t local_sp_5;
    uint64_t r86_5;
    uint64_t r95_4;
    uint64_t r86_4;
    uint64_t var_34;
    uint64_t var_45;
    uint32_t *var_46;
    uint64_t r13_1;
    uint64_t r12_4;
    uint64_t var_47;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint32_t *var_43;
    uint32_t var_44;
    uint32_t *_cast;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    *(uint64_t *)(var_0 + (-80L)) = rsi;
    *(uint64_t *)(var_0 + (-64L)) = rdx;
    rbp_0_ph_ph = 4272416UL;
    r12_0_ph_ph = rsi;
    r14_0_ph_ph = rdi;
    r104_3 = r10;
    r104_0 = r10;
    r95_0 = r9;
    r86_0 = r8;
    r95_3 = r9;
    r86_3 = r8;
    rbx_1 = 0UL;
    if (rsi == 0UL) {
        return;
    }
    var_8 = var_0 + (-88L);
    *(uint64_t *)(var_0 + (-72L)) = 0UL;
    local_sp_0_ph_ph = var_8;
    while (1U)
        {
            var_9 = (uint64_t *)(local_sp_0_ph_ph + 16UL);
            local_sp_4 = local_sp_0_ph_ph;
            rbp_0_ph = rbp_0_ph_ph;
            r12_0_ph = r12_0_ph_ph;
            r14_1 = r14_0_ph_ph;
            local_sp_1 = local_sp_0_ph_ph;
            _pre_phi103 = var_9;
            while (1U)
                {
                    rbp_0 = rbp_0_ph;
                    r12_1 = r12_0_ph;
                    var_10 = *(uint32_t *)rbp_0;
                    rbp_0 = 4272416UL;
                    do {
                        var_10 = *(uint32_t *)rbp_0;
                        rbp_0 = 4272416UL;
                    } while (var_10 != 0U);
                    var_11 = rbp_0 + 4UL;
                    rbp_0_ph = var_11;
                    rbp_1 = var_11;
                    if ((int)var_10 >= (int)0U) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_25 = (uint64_t)((long)(0UL - ((uint64_t)var_10 << 32UL)) >> (long)32UL);
                    var_26 = helper_cc_compute_c_wrapper(var_25 - r12_0_ph, r12_0_ph, var_7, 17U);
                    var_27 = (var_26 == 0UL);
                    var_28 = *var_9;
                    if (var_27) {
                        *var_9 = (var_28 + var_25);
                        r12_0_ph = r12_0_ph - var_25;
                        continue;
                    }
                    *var_9 = (var_28 + r12_0_ph);
                    loop_state_var = 1U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    var_12 = (uint64_t)var_10;
                    rbx_0 = var_12;
                    if (r12_0_ph >= var_12) {
                        if (!((r12_0_ph <= 1UL) || ((r12_0_ph * 3UL) < var_12))) {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        *var_9 = (*var_9 + r12_0_ph);
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    var_13 = var_12 << 2UL;
                    var_14 = local_sp_0_ph_ph + (-8L);
                    *(uint64_t *)var_14 = 4205387UL;
                    indirect_placeholder();
                    rbp_0_ph_ph = var_11 + var_13;
                    r12_0_ph_ph = r12_0_ph - var_12;
                    r14_0_ph_ph = r14_0_ph_ph + var_13;
                    local_sp_0_ph_ph = var_14;
                    continue;
                }
                break;
              case 1U:
                {
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            var_29 = *(uint64_t *)(local_sp_4 + 8UL);
            var_30 = *_pre_phi103;
            var_31 = var_29 - var_30;
            var_32 = var_30 + (-1L);
            var_33 = var_29 + (-1L);
            r13_0 = var_32;
            r12_3 = var_31;
            r104_4 = r104_3;
            local_sp_5 = local_sp_4;
            r95_4 = r95_3;
            r86_4 = r86_3;
            while (1U)
                {
                    var_34 = helper_cc_compute_c_wrapper(var_32 - r13_0, r13_0, var_7, 17U);
                    r104_5 = r104_4;
                    local_sp_6 = local_sp_5;
                    r95_5 = r95_4;
                    r86_5 = r86_4;
                    r13_1 = r13_0;
                    r12_4 = r12_3;
                    if (var_34 == 0UL) {
                        var_45 = r13_0 + var_33;
                        var_46 = (uint32_t *)((rbx_1 << 2UL) + rdi);
                        *(uint32_t *)((r12_3 << 2UL) + rdi) = *var_46;
                        *var_46 = 4294967295U;
                        r13_1 = var_45;
                        r12_4 = r12_3 + 1UL;
                    } else {
                        var_35 = r12_3 - rbx_1;
                        var_36 = *(uint64_t *)(local_sp_5 + 24UL);
                        var_37 = local_sp_5 + (-8L);
                        *(uint64_t *)var_37 = 4205547UL;
                        var_38 = indirect_placeholder_24(var_36, var_35, r104_4, r95_4, r86_4);
                        var_39 = var_38.field_0;
                        var_40 = var_38.field_1;
                        var_41 = var_38.field_2;
                        var_42 = var_38.field_3;
                        var_43 = (uint32_t *)((rbx_1 << 2UL) + rdi);
                        var_44 = *var_43;
                        _cast = (uint32_t *)(((var_39 + rbx_1) << 2UL) + rdi);
                        *var_43 = *_cast;
                        *_cast = var_44;
                        r104_5 = var_40;
                        local_sp_6 = var_37;
                        r95_5 = var_41;
                        r86_5 = var_42;
                    }
                    var_47 = rbx_1 + 1UL;
                    rbx_1 = var_47;
                    r12_3 = r12_4;
                    r104_4 = r104_5;
                    local_sp_5 = local_sp_6;
                    r95_4 = r95_5;
                    r86_4 = r86_5;
                    if (*(uint64_t *)(local_sp_6 + 8UL) == var_47) {
                        break;
                    }
                    r13_0 = r13_1 - var_32;
                    continue;
                }
        }
        break;
      case 1U:
        {
            while (1U)
                {
                    r12_285 = r12_1;
                    r14_286 = r14_1;
                    r104_1 = r104_0;
                    local_sp_2 = local_sp_1;
                    r95_1 = r95_0;
                    r86_1 = r86_0;
                    if (r12_1 != rbx_0) {
                        var_15 = *(uint64_t *)(local_sp_1 + 24UL);
                        var_16 = local_sp_1 + (-8L);
                        *(uint64_t *)var_16 = 4205524UL;
                        var_17 = indirect_placeholder_23(var_15, rbx_0, r104_0, r95_0, r86_0);
                        var_18 = var_17.field_0;
                        var_19 = var_17.field_1;
                        var_20 = var_17.field_2;
                        var_21 = var_17.field_3;
                        var_22 = helper_cc_compute_c_wrapper(var_18 - r12_1, r12_1, var_7, 17U);
                        r86_290 = var_21;
                        r104_287 = var_19;
                        local_sp_388 = var_16;
                        r95_289 = var_20;
                        r104_1 = var_19;
                        local_sp_2 = var_16;
                        r95_1 = var_20;
                        r86_1 = var_21;
                        if (var_22 != 0UL) {
                            rbx_0 = rbx_0 + (-1L);
                            rbp_1 = rbp_1 + 4UL;
                            r12_1 = r12_285;
                            r14_1 = r14_286;
                            r104_0 = r104_287;
                            local_sp_1 = local_sp_388;
                            r95_0 = r95_289;
                            r86_0 = r86_290;
                            continue;
                        }
                    }
                    *(uint32_t *)r14_1 = *(uint32_t *)rbp_1;
                    var_23 = r12_1 + (-1L);
                    var_24 = r14_1 + 4UL;
                    local_sp_4 = local_sp_2;
                    r86_290 = r86_1;
                    r104_3 = r104_1;
                    r12_285 = var_23;
                    r14_286 = var_24;
                    r104_287 = r104_1;
                    local_sp_388 = local_sp_2;
                    r95_289 = r95_1;
                    r95_3 = r95_1;
                    r86_3 = r86_1;
                    if (var_23 == 0UL) {
                        break;
                    }
                    rbx_0 = rbx_0 + (-1L);
                    rbp_1 = rbp_1 + 4UL;
                    r12_1 = r12_285;
                    r14_1 = r14_286;
                    r104_0 = r104_287;
                    local_sp_1 = local_sp_388;
                    r95_0 = r95_289;
                    r86_0 = r86_290;
                    continue;
                }
            _pre_phi103 = (uint64_t *)(local_sp_2 + 16UL);
        }
        break;
    }
}
