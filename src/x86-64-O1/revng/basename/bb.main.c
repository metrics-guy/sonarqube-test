typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_15_ret_type;
struct indirect_placeholder_16_ret_type;
struct indirect_placeholder_15_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_16_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern void indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_13(uint64_t param_0);
extern void indirect_placeholder_14(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_15_ret_type indirect_placeholder_15(uint64_t param_0);
extern struct indirect_placeholder_16_ret_type indirect_placeholder_16(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
typedef _Bool bool;
void bb_main(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_16_ret_type var_12;
    uint64_t local_sp_3_ph96;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t r13_0_ph;
    uint64_t r14_0_ph95;
    uint64_t r15_0_ph;
    uint64_t local_sp_3;
    uint64_t r14_0_ph;
    uint64_t local_sp_3_ph;
    uint64_t local_sp_2;
    uint64_t r15_0_ph94;
    uint64_t r14_0;
    uint64_t var_15;
    uint64_t var_16;
    uint32_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint32_t var_27;
    uint64_t local_sp_0;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint32_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t _pre_phi120;
    uint64_t var_34;
    uint64_t rsi2_0;
    uint64_t var_35;
    uint64_t var_36;
    struct indirect_placeholder_15_ret_type var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_11;
    uint64_t var_13;
    uint32_t var_14;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_8 = (uint64_t)(uint32_t)rdi;
    var_9 = *(uint64_t *)rsi;
    *(uint64_t *)(var_0 + (-64L)) = 4204888UL;
    indirect_placeholder_13(var_9);
    *(uint64_t *)(var_0 + (-72L)) = 4204903UL;
    indirect_placeholder_1();
    var_10 = var_0 + (-80L);
    *(uint64_t *)var_10 = 4204913UL;
    indirect_placeholder_1();
    r13_0_ph = 0UL;
    r15_0_ph = 0UL;
    r14_0_ph = 0UL;
    local_sp_3_ph = var_10;
    rsi2_0 = 0UL;
    while (1U)
        {
            r13_0_ph = 1UL;
            r15_0_ph94 = r15_0_ph;
            r14_0_ph95 = r14_0_ph;
            local_sp_3_ph96 = local_sp_3_ph;
            while (1U)
                {
                    r15_0_ph = r15_0_ph94;
                    r14_0_ph95 = 1UL;
                    r14_0 = r14_0_ph95;
                    local_sp_3 = local_sp_3_ph96;
                    while (1U)
                        {
                            var_11 = local_sp_3 + (-8L);
                            *(uint64_t *)var_11 = 4205058UL;
                            var_12 = indirect_placeholder_16(4252770UL, var_8, rsi, 4252864UL, 0UL);
                            var_13 = var_12.field_0;
                            var_14 = (uint32_t)var_13;
                            local_sp_3_ph96 = var_11;
                            local_sp_3 = var_11;
                            r14_0_ph = r14_0;
                            local_sp_3_ph = var_11;
                            local_sp_2 = var_11;
                            r14_0 = 1UL;
                            local_sp_0 = var_11;
                            if ((uint64_t)(var_14 + 1U) == 0UL) {
                                var_15 = var_13 + (-97L);
                                if ((uint64_t)(uint32_t)var_15 == 0UL) {
                                    continue;
                                }
                                loop_state_var = 2U;
                                break;
                            }
                            var_20 = *(uint32_t *)4272796UL;
                            var_21 = (uint64_t)var_20;
                            var_22 = var_21 << 32UL;
                            var_23 = rdi << 32UL;
                            var_27 = var_20;
                            if ((long)var_22 < (long)var_23) {
                                var_24 = var_12.field_3;
                                var_25 = var_12.field_2;
                                var_26 = var_12.field_1;
                                *(uint64_t *)(local_sp_3 + (-16L)) = 4205185UL;
                                indirect_placeholder_14(0UL, 4252792UL, 0UL, 0UL, var_26, var_25, var_24);
                                *(uint64_t *)(local_sp_3 + (-24L)) = 4205195UL;
                                indirect_placeholder_9(rsi, 1UL, var_8);
                                abort();
                            }
                            if ((uint64_t)(unsigned char)r14_0 != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            var_32 = var_21 + 2UL;
                            if ((long)(var_32 << 32UL) >= (long)var_23) {
                                var_33 = helper_cc_compute_all_wrapper((uint64_t)(uint32_t)var_32 - var_8, var_8, var_7, 16U);
                                if ((var_33 & 64UL) != 0UL) {
                                    _pre_phi120 = (uint64_t)((long)var_22 >> (long)29UL) + rsi;
                                    loop_state_var = 0U;
                                    break;
                                }
                                var_34 = (uint64_t)((long)var_22 >> (long)29UL) + rsi;
                                _pre_phi120 = var_34;
                                rsi2_0 = *(uint64_t *)(var_34 + 8UL);
                                loop_state_var = 0U;
                                break;
                            }
                            var_36 = *(uint64_t *)(((uint64_t)((long)var_22 >> (long)29UL) + rsi) + 16UL);
                            *(uint64_t *)(local_sp_3 + (-16L)) = 4205256UL;
                            var_37 = indirect_placeholder_15(var_36);
                            var_38 = var_37.field_0;
                            var_39 = var_37.field_1;
                            var_40 = var_37.field_2;
                            *(uint64_t *)(local_sp_3 + (-24L)) = 4205284UL;
                            indirect_placeholder_14(0UL, 4252808UL, 0UL, 0UL, var_38, var_39, var_40);
                            *(uint64_t *)(local_sp_3 + (-32L)) = 4205294UL;
                            indirect_placeholder_9(rsi, 1UL, var_8);
                            abort();
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 2U:
                        {
                            var_16 = helper_cc_compute_all_wrapper(var_15, 97UL, var_7, 16U);
                            if ((uint64_t)(((unsigned char)(var_16 >> 4UL) ^ (unsigned char)var_16) & '\xc0') == 0UL) {
                                if ((uint64_t)(var_14 + (-115)) != 0UL) {
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                r15_0_ph94 = *(uint64_t *)4273368UL;
                                continue;
                            }
                            if ((uint64_t)(var_14 + 131U) != 0UL) {
                                if ((uint64_t)(var_14 + 130U) != 0UL) {
                                    loop_state_var = 3U;
                                    switch_state_var = 1;
                                    break;
                                }
                                *(uint64_t *)(local_sp_3 + (-16L)) = 4205024UL;
                                indirect_placeholder_9(rsi, 0UL, var_8);
                                abort();
                            }
                            var_17 = *(uint64_t *)4272672UL;
                            var_18 = *(uint64_t *)4271680UL;
                            *(uint64_t *)(local_sp_3 + (-16L)) = 4204997UL;
                            indirect_placeholder_14(0UL, 4252720UL, var_18, 4252761UL, var_17, 4252776UL, 0UL);
                            var_19 = local_sp_3 + (-24L);
                            *(uint64_t *)var_19 = 4205007UL;
                            indirect_placeholder_1();
                            local_sp_2 = var_19;
                            loop_state_var = 3U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 0U:
                        {
                            var_35 = *(uint64_t *)_pre_phi120;
                            *(uint64_t *)(local_sp_3 + (-16L)) = 4205224UL;
                            indirect_placeholder_9(r13_0_ph, var_35, rsi2_0);
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 1U:
                        {
                            loop_state_var = 2U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    if ((uint64_t)(var_14 + (-122)) == 0UL) {
                        continue;
                    }
                    loop_state_var = 2U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 1U:
                {
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 2U:
                {
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 3U:
                {
                    loop_state_var = 2U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 2U:
        {
            *(uint64_t *)(local_sp_2 + (-8L)) = 4205105UL;
            indirect_placeholder_9(rsi, 1UL, var_8);
            abort();
        }
        break;
      case 1U:
      case 0U:
        {
            switch (loop_state_var) {
              case 1U:
                {
                    var_28 = (uint64_t)var_27 << 32UL;
                    while ((long)var_28 >= (long)var_23)
                        {
                            var_29 = *(uint64_t *)((uint64_t)((long)var_28 >> (long)29UL) + rsi);
                            var_30 = local_sp_0 + (-8L);
                            *(uint64_t *)var_30 = 4205151UL;
                            indirect_placeholder_9(r13_0_ph, var_29, r15_0_ph94);
                            var_31 = *(uint32_t *)4272796UL + 1U;
                            *(uint32_t *)4272796UL = var_31;
                            var_27 = var_31;
                            local_sp_0 = var_30;
                            var_28 = (uint64_t)var_27 << 32UL;
                        }
                }
                break;
              case 0U:
                {
                    return;
                }
                break;
            }
        }
        break;
    }
}
