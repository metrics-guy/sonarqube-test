typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern void indirect_placeholder_7(uint64_t param_0);
uint64_t bb_copy_rest(uint64_t rdi, uint64_t rsi) {
    uint32_t var_18;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint32_t var_4;
    uint64_t var_5;
    uint32_t var_6;
    uint64_t var_7;
    uint64_t rax_2;
    uint64_t local_sp_0;
    uint64_t var_30;
    uint64_t rdi1_0_in;
    uint64_t local_sp_1;
    uint32_t var_19;
    uint64_t local_sp_2;
    uint64_t rax_0_ph;
    uint64_t rdx_0_ph;
    uint64_t local_sp_1_ph;
    uint64_t rax_0;
    uint64_t rdx_0;
    uint64_t r12_0;
    uint32_t var_25;
    uint64_t var_20;
    uint32_t var_21;
    uint64_t var_22;
    uint32_t var_23;
    uint64_t var_24;
    uint64_t rax_1;
    uint64_t rdx_1;
    uint64_t rdx_2;
    uint64_t local_sp_3;
    uint32_t var_26;
    uint32_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    bool or_cond;
    uint64_t var_8;
    uint32_t var_9;
    uint64_t var_10;
    uint32_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint32_t var_14;
    uint64_t var_15;
    uint32_t var_16;
    uint64_t var_17;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r12();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    var_4 = (uint32_t)rsi;
    var_5 = (uint64_t)var_4;
    *(uint32_t *)4326280UL = 0U;
    var_6 = *(uint32_t *)4281188UL;
    var_7 = (uint64_t)var_6;
    rdi1_0_in = var_5;
    if ((int)var_6 < (int)*(uint32_t *)4326284UL) {
        var_13 = var_0 + (-32L);
        *(uint64_t *)var_13 = 4206285UL;
        indirect_placeholder_7(var_7);
        var_14 = *(uint32_t *)4326280UL;
        var_15 = (uint64_t)var_14;
        var_16 = *(uint32_t *)4326284UL;
        var_17 = (uint64_t)var_16;
        var_18 = var_16;
        local_sp_2 = var_13;
        rax_0_ph = var_15;
        rdx_0_ph = var_17;
        local_sp_1_ph = var_13;
        var_25 = var_16;
        rax_1 = var_17;
        rdx_1 = var_17;
        if ((uint64_t)(var_14 - var_16) != 0UL) {
            local_sp_1 = local_sp_1_ph;
            var_19 = var_18;
            rax_0 = rax_0_ph;
            rdx_0 = rdx_0_ph;
            r12_0 = *(uint64_t *)4326312UL;
            var_25 = var_19;
            rax_1 = rax_0;
            rdx_1 = rdx_0;
            local_sp_2 = local_sp_1;
            while (*(unsigned char *)r12_0 != '\x00')
                {
                    var_20 = local_sp_1 + (-8L);
                    *(uint64_t *)var_20 = 4206214UL;
                    indirect_placeholder_1();
                    var_21 = *(uint32_t *)4326280UL + 1U;
                    var_22 = (uint64_t)var_21;
                    *(uint32_t *)4326280UL = var_21;
                    var_23 = *(uint32_t *)4326284UL;
                    var_24 = (uint64_t)var_23;
                    local_sp_1 = var_20;
                    var_19 = var_23;
                    local_sp_2 = var_20;
                    rax_0 = var_22;
                    rdx_0 = var_24;
                    var_25 = var_23;
                    rax_1 = var_24;
                    rdx_1 = var_24;
                    if ((uint64_t)(var_21 - var_23) == 0UL) {
                        break;
                    }
                    r12_0 = r12_0 + 1UL;
                    var_25 = var_19;
                    rax_1 = rax_0;
                    rdx_1 = rdx_0;
                    local_sp_2 = local_sp_1;
                }
        }
    }
    if ((uint64_t)(var_4 + (-10)) == 0UL) {
        return (uint64_t)(uint32_t)rdi1_0_in;
    }
    if ((uint64_t)(var_4 + 1U) == 0UL) {
        return (uint64_t)(uint32_t)rdi1_0_in;
    }
    var_8 = var_0 + (-32L);
    *(uint64_t *)var_8 = 4206164UL;
    indirect_placeholder_7(var_7);
    var_9 = *(uint32_t *)4326280UL;
    var_10 = (uint64_t)var_9;
    var_11 = *(uint32_t *)4326284UL;
    var_12 = (uint64_t)var_11;
    var_18 = var_11;
    rax_2 = var_12;
    rax_0_ph = var_10;
    rdx_0_ph = var_12;
    local_sp_1_ph = var_8;
    rdx_2 = var_12;
    local_sp_3 = var_8;
    if ((uint64_t)(var_9 - var_11) == 0UL) {
        var_26 = (uint32_t)rdx_2;
        var_27 = (uint32_t)rax_2;
        var_28 = (uint64_t)(var_26 - var_27);
        var_29 = local_sp_3 + (-8L);
        *(uint64_t *)var_29 = 4206339UL;
        indirect_placeholder_7(var_28);
        or_cond = (((uint64_t)(var_27 + (-10)) == 0UL) || ((uint64_t)(var_27 + 1U) == 0UL));
        local_sp_0 = var_29;
        rdi1_0_in = rax_2;
        *(uint64_t *)(local_sp_0 + (-8L)) = 4206346UL;
        indirect_placeholder_1();
        var_30 = local_sp_0 + (-16L);
        *(uint64_t *)var_30 = 4206354UL;
        indirect_placeholder_1();
        local_sp_0 = var_30;
        do {
            *(uint64_t *)(local_sp_0 + (-8L)) = 4206346UL;
            indirect_placeholder_1();
            var_30 = local_sp_0 + (-16L);
            *(uint64_t *)var_30 = 4206354UL;
            indirect_placeholder_1();
            local_sp_0 = var_30;
        } while (!or_cond);
        return (uint64_t)(uint32_t)rdi1_0_in;
    }
    local_sp_1 = local_sp_1_ph;
    var_19 = var_18;
    rax_0 = rax_0_ph;
    rdx_0 = rdx_0_ph;
    r12_0 = *(uint64_t *)4326312UL;
    var_25 = var_19;
    rax_1 = rax_0;
    rdx_1 = rdx_0;
    local_sp_2 = local_sp_1;
    while (*(unsigned char *)r12_0 != '\x00')
        {
            var_20 = local_sp_1 + (-8L);
            *(uint64_t *)var_20 = 4206214UL;
            indirect_placeholder_1();
            var_21 = *(uint32_t *)4326280UL + 1U;
            var_22 = (uint64_t)var_21;
            *(uint32_t *)4326280UL = var_21;
            var_23 = *(uint32_t *)4326284UL;
            var_24 = (uint64_t)var_23;
            local_sp_1 = var_20;
            var_19 = var_23;
            local_sp_2 = var_20;
            rax_0 = var_22;
            rdx_0 = var_24;
            var_25 = var_23;
            rax_1 = var_24;
            rdx_1 = var_24;
            if ((uint64_t)(var_21 - var_23) == 0UL) {
                break;
            }
            r12_0 = r12_0 + 1UL;
            var_25 = var_19;
            rax_1 = rax_0;
            rdx_1 = rdx_0;
            local_sp_2 = local_sp_1;
        }
    rax_2 = rax_1;
    rdx_2 = rdx_1;
    local_sp_3 = local_sp_2;
    if ((uint64_t)(var_4 + 1U) != 0UL) {
        if ((long)(((uint64_t)*(uint32_t *)4326292UL + (uint64_t)*(uint32_t *)4281188UL) << 32UL) <= (long)((uint64_t)var_25 << 32UL)) {
            *(uint64_t *)(local_sp_2 + (-8L)) = 4206322UL;
            indirect_placeholder_1();
        }
        return (uint64_t)(uint32_t)rdi1_0_in;
    }
    if ((uint64_t)(var_4 + (-10)) != 0UL) {
        if (0) {
            return (uint64_t)(uint32_t)rdi1_0_in;
        }
        if ((long)(((uint64_t)*(uint32_t *)4326292UL + (uint64_t)*(uint32_t *)4281188UL) << 32UL) <= (long)((uint64_t)var_25 << 32UL)) {
            *(uint64_t *)(local_sp_2 + (-8L)) = 4206322UL;
            indirect_placeholder_1();
        }
        return (uint64_t)(uint32_t)rdi1_0_in;
    }
    var_26 = (uint32_t)rdx_2;
    var_27 = (uint32_t)rax_2;
    var_28 = (uint64_t)(var_26 - var_27);
    var_29 = local_sp_3 + (-8L);
    *(uint64_t *)var_29 = 4206339UL;
    indirect_placeholder_7(var_28);
    or_cond = (((uint64_t)(var_27 + (-10)) == 0UL) || ((uint64_t)(var_27 + 1U) == 0UL));
    local_sp_0 = var_29;
    rdi1_0_in = rax_2;
    *(uint64_t *)(local_sp_0 + (-8L)) = 4206346UL;
    indirect_placeholder_1();
    var_30 = local_sp_0 + (-16L);
    *(uint64_t *)var_30 = 4206354UL;
    indirect_placeholder_1();
    local_sp_0 = var_30;
    do {
        *(uint64_t *)(local_sp_0 + (-8L)) = 4206346UL;
        indirect_placeholder_1();
        var_30 = local_sp_0 + (-16L);
        *(uint64_t *)var_30 = 4206354UL;
        indirect_placeholder_1();
        local_sp_0 = var_30;
    } while (!or_cond);
    return (uint64_t)(uint32_t)rdi1_0_in;
}
