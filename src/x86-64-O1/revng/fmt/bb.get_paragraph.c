typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_10(uint64_t param_0);
extern void indirect_placeholder_7(uint64_t param_0);
uint64_t bb_get_paragraph(uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t local_sp_4;
    uint64_t rbp_2;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t local_sp_2;
    uint64_t local_sp_0;
    uint64_t rbp_0;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_29;
    uint64_t storemerge;
    uint64_t local_sp_1;
    uint64_t rbp_1;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t local_sp_3;
    uint64_t rbp_3;
    uint64_t var_30;
    unsigned char *var_31;
    uint64_t var_33;
    uint64_t var_34;
    uint32_t storemerge2;
    uint64_t storemerge1;
    uint32_t var_4;
    uint32_t var_5;
    uint64_t var_6;
    uint32_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_32;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    var_3 = var_0 + (-24L);
    *(uint32_t *)4281184UL = 0U;
    storemerge2 = 4294967295U;
    storemerge1 = 1UL;
    local_sp_4 = var_3;
    storemerge = (uint64_t)*(uint32_t *)4281192UL;
    while (1U)
        {
            var_4 = (uint32_t)storemerge;
            if ((uint64_t)(var_4 + (-10)) == 0UL) {
                *(uint64_t *)(local_sp_4 + (-8L)) = 4207255UL;
                var_32 = indirect_placeholder(rdi, storemerge);
                storemerge1 = 0UL;
                if ((uint64_t)((uint32_t)var_32 + 1U) != 0UL) {
                    loop_state_var = 3U;
                    break;
                }
                *(uint64_t *)(local_sp_4 + (-16L)) = 4207274UL;
                indirect_placeholder_1();
                var_33 = local_sp_4 + (-24L);
                *(uint64_t *)var_33 = 4207282UL;
                var_34 = indirect_placeholder_10(rdi);
                local_sp_4 = var_33;
                storemerge = (uint64_t)(uint32_t)var_34;
                continue;
            }
            var_5 = *(uint32_t *)4281188UL;
            var_6 = (uint64_t)var_5;
            var_7 = *(uint32_t *)4326284UL;
            if ((uint64_t)(var_4 + 1U) != 0UL & (long)(var_6 << 32UL) >= (long)((uint64_t)*(uint32_t *)4326296UL << 32UL) & (long)((var_6 + (uint64_t)*(uint32_t *)4326300UL) << 32UL) <= (long)((uint64_t)var_7 << 32UL)) {
                *(uint32_t *)4281204UL = var_5;
                *(uint32_t *)4281200UL = var_7;
                *(uint64_t *)4321248UL = 4321280UL;
                *(uint64_t *)4281216UL = 4281248UL;
                *(uint64_t *)(local_sp_4 + (-8L)) = 4207368UL;
                var_8 = indirect_placeholder(rdi, storemerge);
                var_9 = (uint64_t)(uint32_t)var_8;
                *(uint64_t *)(local_sp_4 + (-16L)) = 4207377UL;
                var_10 = indirect_placeholder_10(var_9);
                var_11 = (uint64_t)(unsigned char)var_10;
                var_12 = local_sp_4 + (-24L);
                *(uint64_t *)var_12 = 4207385UL;
                indirect_placeholder_7(var_11);
                rbp_2 = var_9;
                local_sp_2 = var_12;
                rbp_0 = var_9;
                rbp_1 = var_9;
                local_sp_3 = var_12;
                rbp_3 = var_9;
                if (*(unsigned char *)4326321UL != '\x00') {
                    loop_state_var = 0U;
                    break;
                }
                if (*(unsigned char *)4326323UL != '\x00') {
                    var_13 = local_sp_4 + (-32L);
                    *(uint64_t *)var_13 = 4207410UL;
                    var_14 = indirect_placeholder_10(var_9);
                    local_sp_1 = var_13;
                    local_sp_3 = var_13;
                    if ((uint64_t)(unsigned char)var_14 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    loop_state_var = 0U;
                    break;
                }
                if (*(unsigned char *)4326322UL != '\x00') {
                    loop_state_var = 2U;
                    break;
                }
                var_19 = local_sp_4 + (-32L);
                *(uint64_t *)var_19 = 4207530UL;
                var_20 = indirect_placeholder_10(var_9);
                local_sp_0 = var_19;
                local_sp_3 = var_19;
                if ((uint64_t)(unsigned char)var_20 != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                if ((uint64_t)(*(uint32_t *)4326284UL - *(uint32_t *)4281200UL) != 0UL) {
                    loop_state_var = 4U;
                    break;
                }
                loop_state_var = 0U;
                break;
            }
        }
    switch (loop_state_var) {
      case 3U:
        {
            *(uint32_t *)4281192UL = storemerge2;
            return storemerge1;
        }
        break;
      case 4U:
      case 2U:
      case 1U:
      case 0U:
        {
            switch (loop_state_var) {
              case 1U:
                {
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4207424UL;
                    var_15 = indirect_placeholder(rdi, rbp_1);
                    var_16 = (uint64_t)(uint32_t)var_15;
                    var_17 = local_sp_1 + (-16L);
                    *(uint64_t *)var_17 = 4207433UL;
                    var_18 = indirect_placeholder_10(var_16);
                    local_sp_1 = var_17;
                    rbp_1 = var_16;
                    local_sp_3 = var_17;
                    rbp_3 = var_16;
                    do {
                        *(uint64_t *)(local_sp_1 + (-8L)) = 4207424UL;
                        var_15 = indirect_placeholder(rdi, rbp_1);
                        var_16 = (uint64_t)(uint32_t)var_15;
                        var_17 = local_sp_1 + (-16L);
                        *(uint64_t *)var_17 = 4207433UL;
                        var_18 = indirect_placeholder_10(var_16);
                        local_sp_1 = var_17;
                        rbp_1 = var_16;
                        local_sp_3 = var_17;
                        rbp_3 = var_16;
                    } while ((uint64_t)(unsigned char)var_18 != 0UL);
                }
                break;
              case 2U:
                {
                    var_25 = local_sp_2 + (-8L);
                    *(uint64_t *)var_25 = 4207609UL;
                    var_26 = indirect_placeholder_10(rbp_2);
                    local_sp_3 = var_25;
                    rbp_3 = rbp_2;
                    while ((uint64_t)(unsigned char)var_26 != 0UL)
                        {
                            var_27 = local_sp_2 + (-16L);
                            *(uint64_t *)var_27 = 4207600UL;
                            var_28 = indirect_placeholder(rdi, rbp_2);
                            local_sp_2 = var_27;
                            rbp_2 = (uint64_t)(uint32_t)var_28;
                            var_25 = local_sp_2 + (-8L);
                            *(uint64_t *)var_25 = 4207609UL;
                            var_26 = indirect_placeholder_10(rbp_2);
                            local_sp_3 = var_25;
                            rbp_3 = rbp_2;
                        }
                }
                break;
              case 4U:
                {
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4207558UL;
                    var_21 = indirect_placeholder(rdi, rbp_0);
                    var_22 = (uint64_t)(uint32_t)var_21;
                    var_23 = local_sp_0 + (-16L);
                    *(uint64_t *)var_23 = 4207567UL;
                    var_24 = indirect_placeholder_10(var_22);
                    local_sp_0 = var_23;
                    rbp_0 = var_22;
                    local_sp_3 = var_23;
                    rbp_3 = var_22;
                    do {
                        *(uint64_t *)(local_sp_0 + (-8L)) = 4207558UL;
                        var_21 = indirect_placeholder(rdi, rbp_0);
                        var_22 = (uint64_t)(uint32_t)var_21;
                        var_23 = local_sp_0 + (-16L);
                        *(uint64_t *)var_23 = 4207567UL;
                        var_24 = indirect_placeholder_10(var_22);
                        local_sp_0 = var_23;
                        rbp_0 = var_22;
                        local_sp_3 = var_23;
                        rbp_3 = var_22;
                    } while ((uint64_t)(unsigned char)var_24 != 0UL);
                }
                break;
              case 0U:
                {
                    var_29 = *(uint64_t *)4281216UL;
                    var_30 = var_29;
                    if (var_29 <= 4281248UL) {
                        *(uint64_t *)(local_sp_3 + (-8L)) = 4207661UL;
                        indirect_placeholder_1();
                        var_30 = *(uint64_t *)4281216UL;
                    }
                    var_31 = (unsigned char *)(var_30 + (-24L));
                    *var_31 = (*var_31 | '\n');
                    storemerge2 = (uint32_t)rbp_3;
                }
                break;
            }
        }
        break;
    }
}
