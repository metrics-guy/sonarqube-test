typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern void function_dispatcher(unsigned char *param_0);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_11(uint64_t param_0);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_6(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
typedef _Bool bool;
uint64_t bb_mode_compile(uint64_t rdi, uint64_t r9, uint64_t r8) {
    unsigned char *var_15;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    unsigned char var_8;
    uint64_t rdi1_2;
    uint64_t r13_0;
    uint64_t rbx_0;
    uint64_t storemerge;
    uint64_t local_sp_0;
    uint64_t storemerge6;
    uint64_t local_sp_3;
    uint64_t rcx_3;
    uint64_t rbx_1;
    uint64_t rcx_0;
    unsigned char var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t rdi1_0;
    uint64_t rbp_0;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t rdi1_1;
    unsigned char var_22;
    uint64_t rcx_2;
    uint64_t rbx_2;
    uint64_t r12_0;
    uint64_t r14_0_in;
    uint64_t local_sp_2;
    uint64_t rcx_1;
    uint64_t local_sp_1;
    uint64_t r14_0;
    uint64_t var_23;
    unsigned char *var_24;
    unsigned char var_25;
    uint64_t var_26;
    uint64_t rax_1;
    uint64_t var_27;
    unsigned char var_28;
    uint64_t var_29;
    uint64_t var_30;
    unsigned char *var_31;
    unsigned char var_32;
    uint64_t var_33;
    uint64_t var_34;
    unsigned char *_pre_phi143;
    uint32_t *var_35;
    uint32_t var_36;
    unsigned char *_pre142_pre_phi;
    uint64_t rbx_3;
    uint32_t *var_37;
    uint32_t var_38;
    uint32_t _pre_phi;
    uint64_t rbx_4;
    uint64_t rax_2;
    uint64_t var_39;
    uint64_t rbx_5;
    unsigned char var_40;
    uint64_t rcx_4;
    uint64_t rdx_2;
    unsigned char var_9;
    uint64_t var_10;
    uint64_t var_11;
    unsigned char var_12;
    uint64_t var_13;
    uint64_t var_14;
    unsigned char var_41;
    uint64_t rdi1_3;
    uint64_t rbp_1;
    uint64_t var_42;
    uint64_t var_43;
    unsigned char var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_8 = *(unsigned char *)rdi;
    rdi1_2 = 1UL;
    r13_0 = 0UL;
    rbx_0 = rdi;
    storemerge = 4095UL;
    storemerge6 = 0UL;
    rcx_3 = 4095UL;
    rcx_0 = 0UL;
    rdi1_0 = 0UL;
    rdi1_1 = 1UL;
    rax_1 = 7UL;
    rax_2 = 0UL;
    rcx_4 = rdi;
    rdx_2 = (uint64_t)var_8;
    var_41 = var_8;
    rdi1_3 = 0UL;
    rbp_1 = rdi;
    if ((uint64_t)((var_8 + '\xd0') & '\xf8') != 0UL) {
        var_42 = rbp_1 + 1UL;
        var_43 = (uint64_t)(((uint32_t)(rdi1_3 << 3UL) + (uint32_t)(uint64_t)var_41) + (-48));
        rdi1_3 = var_43;
        rbp_1 = var_42;
        while (var_43 <= 4095UL)
            {
                var_44 = *(unsigned char *)var_42;
                var_41 = var_44;
                if ((var_44 & '\xf8') == '0') {
                    var_42 = rbp_1 + 1UL;
                    var_43 = (uint64_t)(((uint32_t)(rdi1_3 << 3UL) + (uint32_t)(uint64_t)var_41) + (-48));
                    rdi1_3 = var_43;
                    rbp_1 = var_42;
                    continue;
                }
                if (var_44 == '\x00') {
                    break;
                }
                *(uint64_t *)(var_0 + (-64L)) = 4208533UL;
                var_45 = indirect_placeholder_11(var_43);
                var_46 = (uint64_t)(uint32_t)var_45;
                var_47 = ((long)(var_42 - rdi) < (long)5UL) ? ((uint64_t)((uint16_t)var_45 & (unsigned short)3072U) | 1023UL) : 4095UL;
                *(uint64_t *)(var_0 + (-72L)) = 4208569UL;
                var_48 = indirect_placeholder(var_46, var_47);
                r13_0 = var_48;
                break;
            }
        return r13_0;
    }
    if (var_8 == '\x00') {
        var_9 = (unsigned char)rdx_2;
        var_10 = rdi1_1 + (((uint64_t)((var_9 & '\xef') + '\xd3') == 0UL) || ((uint64_t)(var_9 + '\xd5') == 0UL));
        var_11 = rcx_4 + 1UL;
        var_12 = *(unsigned char *)var_11;
        rdi1_1 = var_10;
        rcx_4 = var_11;
        rdi1_2 = var_10;
        while (var_12 != '\x00')
            {
                rdx_2 = (uint64_t)var_12;
                var_9 = (unsigned char)rdx_2;
                var_10 = rdi1_1 + (((uint64_t)((var_9 & '\xef') + '\xd3') == 0UL) || ((uint64_t)(var_9 + '\xd5') == 0UL));
                var_11 = rcx_4 + 1UL;
                var_12 = *(unsigned char *)var_11;
                rdi1_1 = var_10;
                rcx_4 = var_11;
                rdi1_2 = var_10;
            }
    }
    var_13 = var_0 + (-64L);
    *(uint64_t *)var_13 = 4208455UL;
    var_14 = indirect_placeholder_6(rdi1_2, 16UL, r9, r8);
    local_sp_0 = var_13;
    r13_0 = var_14;
    while (1U)
        {
            rbx_1 = rbx_0;
            r14_0_in = storemerge6;
            local_sp_1 = local_sp_0;
            local_sp_3 = local_sp_0;
            while (1U)
                {
                    var_15 = (unsigned char *)rbx_1;
                    var_16 = *var_15;
                    var_17 = (uint64_t)var_16;
                    var_18 = var_17 + (-103L);
                    rbx_2 = rbx_1;
                    rcx_1 = rcx_0;
                    if ((uint64_t)(unsigned char)var_18 == 0UL) {
                        rcx_3 = (uint64_t)((uint32_t)rcx_0 & (-1081)) | 1080UL;
                    } else {
                        var_19 = helper_cc_compute_all_wrapper(var_18, 103UL, var_7, 14U);
                        if ((uint64_t)(((unsigned char)(var_19 >> 4UL) ^ (unsigned char)var_19) & '\xc0') != 0UL) {
                            var_20 = var_17 + (-97L);
                            if ((uint64_t)(unsigned char)var_20 != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                        }
                        if ((uint64_t)(var_16 + '\x91') == 0UL) {
                            rcx_3 = (uint64_t)((uint32_t)rcx_0 & (-520)) | 519UL;
                        } else {
                            if ((uint64_t)(var_16 + '\x8b') != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            rcx_3 = (uint64_t)((uint32_t)rcx_0 & (-2497)) | 2496UL;
                        }
                    }
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    var_21 = helper_cc_compute_all_wrapper(var_20, 97UL, var_7, 14U);
                    if ((uint64_t)(((unsigned char)(var_21 >> 4UL) ^ (unsigned char)var_21) & '\xc0') != 0UL) {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    if ((uint64_t)((var_16 & '\xef') + '\xd3') != 0UL) {
                        if ((uint64_t)(var_16 + '\xd5') != 0UL) {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                    }
                    var_22 = *var_15;
                    r12_0 = (storemerge6 << 4UL) + var_14;
                    while (1U)
                        {
                            r14_0 = r14_0_in + 1UL;
                            var_23 = rbx_2 + 1UL;
                            var_24 = (unsigned char *)var_23;
                            var_25 = *var_24;
                            var_26 = (uint64_t)var_25 + (-111L);
                            storemerge6 = r14_0;
                            local_sp_3 = local_sp_1;
                            rbp_0 = var_23;
                            rcx_2 = rcx_1;
                            r14_0_in = r14_0;
                            local_sp_2 = local_sp_1;
                            _pre142_pre_phi = var_24;
                            rbx_4 = var_23;
                            if ((uint64_t)(unsigned char)var_26 == 0UL) {
                                rbx_3 = rbx_2 + 2UL;
                                *(unsigned char *)r12_0 = var_22;
                                *(unsigned char *)(r12_0 + 1UL) = (unsigned char)'\x03';
                                var_37 = (uint32_t *)(r12_0 + 4UL);
                                var_38 = (uint32_t)rcx_1;
                                *var_37 = var_38;
                                *(uint32_t *)(r12_0 + 8UL) = (uint32_t)rax_1;
                                _pre142_pre_phi = (unsigned char *)rbx_3;
                                _pre_phi = var_38;
                                rbx_4 = rbx_3;
                                rax_2 = rax_1;
                            } else {
                                var_27 = helper_cc_compute_all_wrapper(var_26, 111UL, var_7, 14U);
                                rax_1 = 56UL;
                                rcx_2 = 4095UL;
                                if ((uint64_t)(((unsigned char)(var_27 >> 4UL) ^ (unsigned char)var_27) & '\xc0') != 0UL) {
                                    rax_1 = 448UL;
                                    if ((uint64_t)(var_25 + '\x8b') == 0UL) {
                                        rbx_3 = rbx_2 + 2UL;
                                        *(unsigned char *)r12_0 = var_22;
                                        *(unsigned char *)(r12_0 + 1UL) = (unsigned char)'\x03';
                                        var_37 = (uint32_t *)(r12_0 + 4UL);
                                        var_38 = (uint32_t)rcx_1;
                                        *var_37 = var_38;
                                        *(uint32_t *)(r12_0 + 8UL) = (uint32_t)rax_1;
                                        _pre142_pre_phi = (unsigned char *)rbx_3;
                                        _pre_phi = var_38;
                                        rbx_4 = rbx_3;
                                        rax_2 = rax_1;
                                    } else {
                                        if ((*var_24 + '\xa8') <= ' ') {
                                            function_dispatcher((unsigned char *)(0UL));
                                            return 0UL;
                                        }
                                        *(unsigned char *)r12_0 = var_22;
                                        *(unsigned char *)(r12_0 + 1UL) = (unsigned char)'\x01';
                                        var_35 = (uint32_t *)(r12_0 + 4UL);
                                        var_36 = (uint32_t)rcx_1;
                                        *var_35 = var_36;
                                        *(uint32_t *)(r12_0 + 8UL) = 0U;
                                        _pre_phi = var_36;
                                    }
                                    var_39 = (uint64_t)((uint32_t)rax_2 & (uint32_t)(((uint64_t)_pre_phi == 0UL) ? 4294967295UL : rcx_1));
                                    _pre_phi143 = _pre142_pre_phi;
                                    rbx_5 = rbx_4;
                                    storemerge = var_39;
                                    *(uint32_t *)(r12_0 + 12UL) = (uint32_t)storemerge;
                                    var_40 = *_pre_phi143;
                                    local_sp_0 = local_sp_2;
                                    local_sp_3 = local_sp_2;
                                    var_22 = var_40;
                                    rbx_2 = rbx_5;
                                    r12_0 = r12_0 + 16UL;
                                    rcx_1 = rcx_2;
                                    local_sp_1 = local_sp_2;
                                    if ((uint64_t)((var_40 & '\xef') + '\xd3') != 0UL) {
                                        continue;
                                    }
                                    if ((uint64_t)(var_40 + '\xd5') == 0UL) {
                                        continue;
                                    }
                                    loop_state_var = 0U;
                                    break;
                                }
                                if ((signed char)var_25 <= '7') {
                                    if ((signed char)var_25 > '/') {
                                        if ((*var_24 + '\xa8') > ' ') {
                                            function_dispatcher((unsigned char *)(0UL));
                                            return 0UL;
                                        }
                                        *(unsigned char *)r12_0 = var_22;
                                        *(unsigned char *)(r12_0 + 1UL) = (unsigned char)'\x01';
                                        var_35 = (uint32_t *)(r12_0 + 4UL);
                                        var_36 = (uint32_t)rcx_1;
                                        *var_35 = var_36;
                                        *(uint32_t *)(r12_0 + 8UL) = 0U;
                                        _pre_phi = var_36;
                                        var_39 = (uint64_t)((uint32_t)rax_2 & (uint32_t)(((uint64_t)_pre_phi == 0UL) ? 4294967295UL : rcx_1));
                                        _pre_phi143 = _pre142_pre_phi;
                                        rbx_5 = rbx_4;
                                        storemerge = var_39;
                                        *(uint32_t *)(r12_0 + 12UL) = (uint32_t)storemerge;
                                        var_40 = *_pre_phi143;
                                        local_sp_0 = local_sp_2;
                                        local_sp_3 = local_sp_2;
                                        var_22 = var_40;
                                        rbx_2 = rbx_5;
                                        r12_0 = r12_0 + 16UL;
                                        rcx_1 = rcx_2;
                                        local_sp_1 = local_sp_2;
                                        if ((uint64_t)((var_40 & '\xef') + '\xd3') != 0UL) {
                                            continue;
                                        }
                                        if ((uint64_t)(var_40 + '\xd5') == 0UL) {
                                            continue;
                                        }
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    var_28 = *var_24;
                                    while (1U)
                                        {
                                            var_29 = rbp_0 + 1UL;
                                            var_30 = (uint64_t)(((uint32_t)(rdi1_0 << 3UL) + (uint32_t)(uint64_t)var_28) + (-48));
                                            rdi1_0 = var_30;
                                            rbp_0 = var_29;
                                            rbx_5 = var_29;
                                            if (var_30 <= 4095UL) {
                                                loop_state_var = 1U;
                                                break;
                                            }
                                            var_31 = (unsigned char *)var_29;
                                            var_32 = *var_31;
                                            var_28 = var_32;
                                            _pre_phi143 = var_31;
                                            if ((var_32 & '\xf8') == '0') {
                                                continue;
                                            }
                                            loop_state_var = 0U;
                                            break;
                                        }
                                    switch_state_var = 0;
                                    switch (loop_state_var) {
                                      case 0U:
                                        {
                                            if ((uint64_t)(uint32_t)rcx_1 != 0UL) {
                                                loop_state_var = 1U;
                                                switch_state_var = 1;
                                                break;
                                            }
                                            var_33 = local_sp_1 + (-8L);
                                            *(uint64_t *)var_33 = 4208953UL;
                                            var_34 = indirect_placeholder_11(var_30);
                                            *(unsigned char *)r12_0 = var_22;
                                            *(unsigned char *)(r12_0 + 1UL) = (unsigned char)'\x01';
                                            *(uint32_t *)(r12_0 + 4UL) = 4095U;
                                            *(uint32_t *)(r12_0 + 8UL) = (uint32_t)var_34;
                                            local_sp_2 = var_33;
                                            *(uint32_t *)(r12_0 + 12UL) = (uint32_t)storemerge;
                                            var_40 = *_pre_phi143;
                                            local_sp_0 = local_sp_2;
                                            local_sp_3 = local_sp_2;
                                            var_22 = var_40;
                                            rbx_2 = rbx_5;
                                            r12_0 = r12_0 + 16UL;
                                            rcx_1 = rcx_2;
                                            local_sp_1 = local_sp_2;
                                            if ((uint64_t)((var_40 & '\xef') + '\xd3') != 0UL) {
                                                continue;
                                            }
                                            if ((uint64_t)(var_40 + '\xd5') == 0UL) {
                                                continue;
                                            }
                                            loop_state_var = 0U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                        break;
                                      case 1U:
                                        {
                                            loop_state_var = 1U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                        break;
                                    }
                                    if (switch_state_var)
                                        break;
                                }
                                if ((uint64_t)(var_25 + '\x99') == 0UL) {
                                    rbx_3 = rbx_2 + 2UL;
                                    *(unsigned char *)r12_0 = var_22;
                                    *(unsigned char *)(r12_0 + 1UL) = (unsigned char)'\x03';
                                    var_37 = (uint32_t *)(r12_0 + 4UL);
                                    var_38 = (uint32_t)rcx_1;
                                    *var_37 = var_38;
                                    *(uint32_t *)(r12_0 + 8UL) = (uint32_t)rax_1;
                                    _pre142_pre_phi = (unsigned char *)rbx_3;
                                    _pre_phi = var_38;
                                    rbx_4 = rbx_3;
                                    rax_2 = rax_1;
                                } else {
                                    if ((*var_24 + '\xa8') <= ' ') {
                                        function_dispatcher((unsigned char *)(0UL));
                                        return 0UL;
                                    }
                                    *(unsigned char *)r12_0 = var_22;
                                    *(unsigned char *)(r12_0 + 1UL) = (unsigned char)'\x01';
                                    var_35 = (uint32_t *)(r12_0 + 4UL);
                                    var_36 = (uint32_t)rcx_1;
                                    *var_35 = var_36;
                                    *(uint32_t *)(r12_0 + 8UL) = 0U;
                                    _pre_phi = var_36;
                                }
                            }
                            var_39 = (uint64_t)((uint32_t)rax_2 & (uint32_t)(((uint64_t)_pre_phi == 0UL) ? 4294967295UL : rcx_1));
                            _pre_phi143 = _pre142_pre_phi;
                            rbx_5 = rbx_4;
                            storemerge = var_39;
                            *(uint32_t *)(r12_0 + 12UL) = (uint32_t)storemerge;
                            var_40 = *_pre_phi143;
                            local_sp_0 = local_sp_2;
                            local_sp_3 = local_sp_2;
                            var_22 = var_40;
                            rbx_2 = rbx_5;
                            r12_0 = r12_0 + 16UL;
                            rcx_1 = rcx_2;
                            local_sp_1 = local_sp_2;
                            if ((uint64_t)((var_40 & '\xef') + '\xd3') != 0UL) {
                                continue;
                            }
                            if ((uint64_t)(var_40 + '\xd5') == 0UL) {
                                continue;
                            }
                            loop_state_var = 0U;
                            break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 0U:
                        {
                            switch_state_var = 0;
                            switch (var_40) {
                              case ',':
                                {
                                    rbx_0 = rbx_5 + 1UL;
                                    continue;
                                }
                                break;
                              case '\x00':
                                {
                                    *(unsigned char *)(((r14_0 << 4UL) + var_14) + 1UL) = (unsigned char)'\x00';
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              default:
                                {
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                            }
                            if (switch_state_var)
                                break;
                        }
                        break;
                      case 1U:
                        {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
                break;
              case 1U:
                {
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    *(uint64_t *)(local_sp_3 + (-8L)) = 4208588UL;
    indirect_placeholder_1();
}
