typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_8_ret_type;
struct indirect_placeholder_9_ret_type;
struct indirect_placeholder_10_ret_type;
struct indirect_placeholder_8_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_9_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_10_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r10(void);
extern uint64_t init_rax(void);
extern void indirect_placeholder_6(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_8_ret_type indirect_placeholder_8(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_9_ret_type indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_10_ret_type indirect_placeholder_10(uint64_t param_0, uint64_t param_1);
typedef _Bool bool;
uint64_t bb_readtoken(uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t rcx) {
    struct indirect_placeholder_9_ret_type var_25;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t local_sp_5;
    uint64_t r14_1;
    uint64_t var_31;
    uint64_t local_sp_1;
    uint64_t rbp_0;
    uint64_t var_32;
    uint64_t rbp_1;
    uint64_t rbx_0_in;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t local_sp_2;
    uint64_t var_19;
    uint64_t rbx_2_in;
    uint64_t local_sp_3;
    uint64_t *var_20;
    uint64_t var_21;
    uint64_t *_cast1;
    uint64_t r14_0;
    uint64_t rcx4_0;
    uint64_t local_sp_0;
    uint64_t storemerge;
    uint64_t rbx_1;
    uint64_t var_26;
    uint64_t var_27;
    struct indirect_placeholder_8_ret_type var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_24;
    uint64_t var_15;
    uint64_t var_16;
    struct indirect_placeholder_10_ret_type var_17;
    uint64_t var_18;
    uint64_t var_10;
    uint64_t rbx_3;
    uint64_t local_sp_4;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r15();
    var_5 = init_rbp();
    var_6 = init_r12();
    var_7 = init_r14();
    var_8 = init_r10();
    *(uint64_t *)(var_0 + (-8L)) = var_4;
    *(uint64_t *)(var_0 + (-16L)) = var_7;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_6;
    *(uint64_t *)(var_0 + (-40L)) = var_5;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_9 = var_0 + (-104L);
    *(uint64_t *)var_9 = 0UL;
    *(uint64_t *)(var_0 + (-96L)) = 0UL;
    *(uint64_t *)(var_0 + (-88L)) = 0UL;
    *(uint64_t *)(var_0 + (-80L)) = 0UL;
    local_sp_5 = var_9;
    rbp_1 = 18446744073709551615UL;
    rbx_2_in = var_1;
    storemerge = 0UL;
    rbx_3 = rsi;
    local_sp_4 = var_9;
    if (rdx != 0UL) {
        var_10 = rdx + rsi;
        var_11 = (uint64_t)*(unsigned char *)rbx_3;
        var_12 = local_sp_4 + (-8L);
        *(uint64_t *)var_12 = 4214393UL;
        indirect_placeholder_6(var_11, local_sp_4);
        var_13 = rbx_3 + 1UL;
        rbx_3 = var_13;
        local_sp_4 = var_12;
        local_sp_5 = var_12;
        do {
            var_11 = (uint64_t)*(unsigned char *)rbx_3;
            var_12 = local_sp_4 + (-8L);
            *(uint64_t *)var_12 = 4214393UL;
            indirect_placeholder_6(var_11, local_sp_4);
            var_13 = rbx_3 + 1UL;
            rbx_3 = var_13;
            local_sp_4 = var_12;
            local_sp_5 = var_12;
        } while (var_13 != var_10);
    }
    var_14 = local_sp_5 + (-8L);
    *(uint64_t *)var_14 = 4214410UL;
    indirect_placeholder_1();
    local_sp_3 = var_14;
    if ((int)(uint32_t)var_1 < (int)0U) {
        return rbp_1;
    }
    while (1U)
        {
            var_15 = (uint64_t)((long)(rbx_2_in << 32UL) >> (long)32UL);
            var_16 = local_sp_3 + (-8L);
            *(uint64_t *)var_16 = 4214427UL;
            var_17 = indirect_placeholder_10(var_15, local_sp_3);
            var_18 = var_17.field_0;
            rbx_0_in = rbx_2_in;
            local_sp_0 = var_16;
            rbx_2_in = var_18;
            if ((uint64_t)(unsigned char)var_18 == 0UL) {
                var_19 = local_sp_3 + (-16L);
                *(uint64_t *)var_19 = 4214439UL;
                indirect_placeholder_1();
                local_sp_3 = var_19;
                if ((int)(uint32_t)var_18 <= (int)4294967295U) {
                    continue;
                }
                break;
            }
            var_20 = (uint64_t *)(rcx + 8UL);
            var_21 = *var_20;
            _cast1 = (uint64_t *)rcx;
            *(uint64_t *)(local_sp_3 + 32UL) = *_cast1;
            r14_0 = var_21;
            if ((int)(uint32_t)rbx_2_in < (int)0U) {
                break;
            }
            rcx4_0 = var_17.field_1;
            while (1U)
                {
                    var_22 = (uint64_t)(uint32_t)rbx_0_in;
                    var_23 = local_sp_0 + 40UL;
                    rbx_1 = var_22;
                    rbp_0 = storemerge;
                    r14_1 = r14_0;
                    local_sp_1 = local_sp_0;
                    if (*(uint64_t *)var_23 == storemerge) {
                        var_24 = local_sp_0 + (-8L);
                        *(uint64_t *)var_24 = 4214500UL;
                        var_25 = indirect_placeholder_9(1UL, var_22, r14_0, storemerge, var_23, rcx4_0, var_8);
                        rbx_1 = var_25.field_1;
                        rbp_0 = var_25.field_2;
                        r14_1 = var_25.field_0;
                        local_sp_1 = var_24;
                    }
                    r14_0 = r14_1;
                    local_sp_2 = local_sp_1;
                    rbp_1 = rbp_0;
                    if ((int)(uint32_t)rbx_1 >= (int)0U) {
                        *(unsigned char *)(rbp_0 + r14_1) = (unsigned char)'\x00';
                        loop_state_var = 0U;
                        break;
                    }
                    var_26 = (uint64_t)((long)(rbx_1 << 32UL) >> (long)32UL);
                    var_27 = local_sp_1 + (-8L);
                    *(uint64_t *)var_27 = 4214573UL;
                    var_28 = indirect_placeholder_8(var_26, local_sp_1);
                    var_29 = var_28.field_0;
                    var_30 = var_28.field_1;
                    rbx_0_in = var_29;
                    rcx4_0 = var_30;
                    local_sp_2 = var_27;
                    rbp_1 = 18446744073709551615UL;
                    if ((uint64_t)(unsigned char)var_29 == 0UL) {
                        var_31 = rbp_0 + 1UL;
                        *(unsigned char *)(rbp_0 + r14_1) = (unsigned char)rbx_1;
                        var_32 = local_sp_1 + (-16L);
                        *(uint64_t *)var_32 = 4214593UL;
                        indirect_placeholder_1();
                        local_sp_0 = var_32;
                        storemerge = var_31;
                        if (((int)(uint32_t)var_29 <= (int)4294967295U) && (var_31 == 0UL)) {
                            continue;
                        }
                        loop_state_var = 1U;
                        break;
                    }
                    *(unsigned char *)(rbp_0 + r14_1) = (unsigned char)'\x00';
                    loop_state_var = 0U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 0U:
                {
                    *var_20 = r14_1;
                    *_cast1 = *(uint64_t *)(local_sp_2 + 40UL);
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    return rbp_1;
}
