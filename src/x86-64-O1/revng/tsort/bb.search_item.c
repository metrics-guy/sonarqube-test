typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_rax(void);
uint64_t bb_search_item(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t local_sp_4;
    uint64_t local_sp_2;
    uint64_t storemerge1_in_in;
    uint64_t var_29;
    uint64_t local_sp_0;
    uint64_t storemerge1;
    uint64_t local_sp_1;
    uint64_t rbx_0;
    uint64_t var_30;
    uint32_t *var_31;
    uint64_t r15_2;
    uint64_t var_26;
    uint64_t r15_0;
    uint64_t var_27;
    uint32_t *var_32;
    uint32_t var_33;
    uint32_t *_pre_phi125;
    uint64_t local_sp_3;
    uint32_t *var_34;
    uint32_t var_35;
    uint32_t var_36;
    uint32_t *var_37;
    bool var_38;
    bool var_39;
    uint64_t *var_40;
    uint64_t var_41;
    uint64_t *var_42;
    uint64_t *var_43;
    uint64_t rax_0;
    uint64_t *var_44;
    uint64_t var_45;
    uint64_t *var_46;
    uint64_t *var_47;
    uint32_t *var_48;
    uint32_t var_49;
    uint32_t var_50;
    uint64_t rax_1;
    uint64_t local_sp_5;
    uint64_t *var_51;
    uint64_t *var_52;
    uint64_t *var_53;
    uint64_t var_28;
    uint64_t var_24;
    bool var_25;
    uint64_t var_23;
    uint64_t var_20;
    uint32_t var_21;
    bool var_22;
    uint64_t cc_src_0;
    uint64_t r13_0;
    uint64_t var_14;
    bool var_15;
    uint64_t rbp_0;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t storemerge;
    bool var_18;
    uint64_t rax_2;
    uint64_t var_19;
    uint64_t r15_1;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t var_54;
    uint64_t var_12;
    uint32_t var_13;
    uint64_t var_9;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r15();
    var_5 = init_rbp();
    var_6 = init_r12();
    var_7 = init_r14();
    var_8 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_4;
    *(uint64_t *)(var_0 + (-16L)) = var_7;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_6;
    *(uint64_t *)(var_0 + (-40L)) = var_5;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    local_sp_4 = var_0 + (-72L);
    rax_2 = var_1;
    cc_src_0 = 24UL;
    rbp_0 = rdi;
    if (rdi == 0UL) {
        var_9 = var_0 + (-80L);
        *(uint64_t *)var_9 = 4205347UL;
        indirect_placeholder_1();
        local_sp_4 = var_9;
    }
    var_10 = (uint64_t *)(rdi + 16UL);
    var_11 = *var_10;
    r13_0 = var_11;
    r15_2 = var_11;
    local_sp_5 = local_sp_4;
    if (var_11 == 0UL) {
        *(uint64_t *)(local_sp_4 + (-8L)) = 4205310UL;
        var_54 = indirect_placeholder_5(rsi);
        *var_10 = var_54;
        r15_1 = var_54;
    } else {
        while (1U)
            {
                var_12 = local_sp_5 + (-8L);
                *(uint64_t *)var_12 = 4205384UL;
                indirect_placeholder_1();
                var_13 = (uint32_t)rax_2;
                r15_1 = r15_2;
                cc_src_0 = 0UL;
                local_sp_5 = var_12;
                if ((uint64_t)var_13 != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                var_14 = helper_cc_compute_all_wrapper(rax_2, cc_src_0, var_8, 24U);
                var_15 = ((signed char)(unsigned char)var_14 < '\x00');
                var_16 = r15_2 + 16UL;
                var_17 = r15_2 + 8UL;
                storemerge = *(uint64_t *)(var_15 ? var_17 : var_16);
                rax_2 = storemerge;
                r15_2 = storemerge;
                if (storemerge == 0UL) {
                    var_18 = (*(uint32_t *)(storemerge + 24UL) == 0U);
                    r13_0 = var_18 ? r13_0 : storemerge;
                    rbp_0 = var_18 ? rbp_0 : r15_2;
                    continue;
                }
                *(uint64_t *)(local_sp_5 + (-16L)) = 4205410UL;
                var_19 = indirect_placeholder_5(rsi);
                r15_1 = var_19;
                if ((int)var_13 >= (int)0U) {
                    *(uint64_t *)var_16 = var_19;
                    loop_state_var = 1U;
                    break;
                }
                *(uint64_t *)var_17 = var_19;
                loop_state_var = 1U;
                break;
            }
        var_20 = local_sp_5 + (-24L);
        *(uint64_t *)var_20 = 4205437UL;
        indirect_placeholder_1();
        var_21 = (uint32_t)var_19;
        var_22 = ((uint64_t)var_21 == 0UL);
        local_sp_3 = var_20;
        if (var_22) {
            var_23 = local_sp_5 + (-32L);
            *(uint64_t *)var_23 = 4205668UL;
            indirect_placeholder_1();
            local_sp_3 = var_23;
        }
        var_24 = local_sp_3 + (-8L);
        *(uint64_t *)var_24 = 4205457UL;
        indirect_placeholder_1();
        var_25 = ((int)var_21 < (int)0U);
        local_sp_1 = var_24;
        local_sp_2 = var_24;
        if (var_25) {
            var_27 = *(uint64_t *)(r13_0 + 8UL);
            *(uint32_t *)(local_sp_3 + 4UL) = 4294967295U;
            r15_0 = var_27;
        } else {
            var_26 = *(uint64_t *)(r13_0 + 16UL);
            *(uint32_t *)(local_sp_3 + 4UL) = 1U;
            r15_0 = var_26;
        }
        rax_1 = r15_0;
        rbx_0 = r15_0;
        if (var_19 == r15_0) {
            storemerge1 = *(uint64_t *)storemerge1_in_in;
            rbx_0 = storemerge1;
            do {
                var_28 = local_sp_2 + (-8L);
                *(uint64_t *)var_28 = 4205748UL;
                indirect_placeholder_1();
                local_sp_0 = var_28;
                if (var_22) {
                    var_29 = local_sp_2 + (-16L);
                    *(uint64_t *)var_29 = 4205715UL;
                    indirect_placeholder_1();
                    local_sp_0 = var_29;
                }
                var_30 = local_sp_0 + (-8L);
                *(uint64_t *)var_30 = 4205763UL;
                indirect_placeholder_1();
                var_31 = (uint32_t *)(rbx_0 + 24UL);
                local_sp_1 = var_30;
                local_sp_2 = var_30;
                if (var_25) {
                    *var_31 = 4294967295U;
                    storemerge1_in_in = rbx_0 + 8UL;
                } else {
                    *var_31 = 1U;
                    storemerge1_in_in = rbx_0 + 16UL;
                }
                storemerge1 = *(uint64_t *)storemerge1_in_in;
                rbx_0 = storemerge1;
            } while (storemerge1 != var_19);
        }
        var_32 = (uint32_t *)(r13_0 + 24UL);
        var_33 = *var_32;
        if (var_33 != 0U) {
            var_34 = (uint32_t *)(local_sp_1 + 12UL);
            var_35 = *var_34;
            var_36 = 0U - var_35;
            _pre_phi125 = var_34;
            if ((uint64_t)(var_33 - var_36) != 0UL) {
                var_37 = (uint32_t *)(r15_0 + 24UL);
                var_38 = ((uint64_t)(*var_37 - var_35) == 0UL);
                var_39 = ((int)var_35 < (int)0U);
                if (var_38) {
                    if (var_39) {
                        var_52 = (uint64_t *)(r15_0 + 16UL);
                        *(uint64_t *)(r13_0 + 8UL) = *var_52;
                        *var_52 = r13_0;
                    } else {
                        var_51 = (uint64_t *)(r15_0 + 8UL);
                        *(uint64_t *)(r13_0 + 16UL) = *var_51;
                        *var_51 = r13_0;
                    }
                    *var_37 = 0U;
                    *var_32 = 0U;
                } else {
                    if (var_39) {
                        var_44 = (uint64_t *)(r15_0 + 16UL);
                        var_45 = *var_44;
                        var_46 = (uint64_t *)(var_45 + 8UL);
                        *var_44 = *var_46;
                        *var_46 = r15_0;
                        var_47 = (uint64_t *)(var_45 + 16UL);
                        *(uint64_t *)(r13_0 + 8UL) = *var_47;
                        *var_47 = r13_0;
                        rax_0 = var_45;
                    } else {
                        var_40 = (uint64_t *)(r15_0 + 8UL);
                        var_41 = *var_40;
                        var_42 = (uint64_t *)(var_41 + 16UL);
                        *var_40 = *var_42;
                        *var_42 = r15_0;
                        var_43 = (uint64_t *)(var_41 + 8UL);
                        *(uint64_t *)(r13_0 + 16UL) = *var_43;
                        *var_43 = r13_0;
                        rax_0 = var_41;
                    }
                    *var_32 = 0U;
                    *var_37 = 0U;
                    var_48 = (uint32_t *)(rax_0 + 24UL);
                    var_49 = *var_48;
                    var_50 = *var_34;
                    rax_1 = rax_0;
                    if ((uint64_t)(var_49 - var_50) == 0UL) {
                        *var_32 = var_36;
                    } else {
                        if ((uint64_t)(var_36 - var_49) == 0UL) {
                            *var_37 = var_50;
                        }
                    }
                    *var_48 = 0U;
                }
                var_53 = (uint64_t *)(rbp_0 + 16UL);
                if (*var_53 == r13_0) {
                    *var_53 = rax_1;
                } else {
                    *(uint64_t *)(rbp_0 + 8UL) = rax_1;
                }
                return r15_1;
            }
        }
        _pre_phi125 = (uint32_t *)(local_sp_1 + 12UL);
    }
}
