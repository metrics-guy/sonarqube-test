typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_48_ret_type;
struct indirect_placeholder_48_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_4(uint64_t param_0);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_rax(void);
extern void indirect_placeholder_3(uint64_t param_0);
extern uint32_t init_state_0x82fc(void);
extern void indirect_placeholder_23(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_48_ret_type indirect_placeholder_48(uint64_t param_0, uint64_t param_1, uint64_t param_2);
uint64_t bb_fts_open(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    unsigned char rax_0;
    uint64_t r12_0;
    bool var_40;
    uint64_t var_41;
    uint64_t local_sp_2;
    uint64_t r13_3;
    uint64_t rdx1_1;
    uint64_t r13_0;
    uint64_t local_sp_1;
    uint64_t local_sp_7;
    uint64_t local_sp_9;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint32_t var_9;
    uint32_t var_10;
    uint64_t local_sp_0;
    uint64_t var_57;
    uint64_t var_53;
    uint64_t var_54;
    uint32_t *var_55;
    uint32_t var_56;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t local_sp_3;
    uint64_t var_58;
    uint64_t r13_1;
    uint64_t *var_44;
    uint64_t local_sp_4;
    uint64_t rsi3_0;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t r13_2;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_42;
    struct indirect_placeholder_48_ret_type var_43;
    uint64_t r15_2;
    uint64_t r15_1;
    uint64_t local_sp_5;
    uint64_t var_59;
    uint64_t *var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t local_sp_6;
    uint64_t var_32;
    uint64_t local_sp_8;
    uint64_t rdi2_0;
    uint64_t rcx_0;
    uint64_t rcx_1;
    unsigned char var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t rdx1_0;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t *var_38;
    uint64_t var_39;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t local_sp_10;
    uint64_t *var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t rdi2_1;
    uint64_t rcx_2;
    uint64_t var_20;
    uint64_t var_21;
    uint32_t *var_22;
    uint64_t *var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t rbp_0;
    uint64_t var_11;
    bool var_12;
    uint64_t *var_13;
    uint64_t var_14;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r15();
    var_5 = init_rbp();
    var_6 = init_r12();
    var_7 = init_r14();
    var_8 = init_cc_src2();
    var_9 = init_state_0x82fc();
    *(uint64_t *)(var_0 + (-8L)) = var_4;
    *(uint64_t *)(var_0 + (-16L)) = var_7;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_6;
    *(uint64_t *)(var_0 + (-40L)) = var_5;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    *(uint64_t *)(var_0 + (-80L)) = rdx;
    var_10 = (uint32_t)rsi;
    rax_0 = (unsigned char)'\x01';
    r12_0 = 0UL;
    r13_3 = 0UL;
    r13_2 = 0UL;
    r15_1 = 0UL;
    rcx_0 = 18446744073709551615UL;
    rcx_1 = 0UL;
    rbp_0 = 0UL;
    if ((uint64_t)(var_10 & (-8192)) != 0UL) {
        *(uint64_t *)(var_0 + (-96L)) = 4222149UL;
        indirect_placeholder_1();
        *(uint32_t *)var_1 = 22U;
        return rbp_0;
    }
    var_11 = (uint64_t)((uint16_t)rsi & (unsigned short)516U);
    if ((uint64_t)(((uint32_t)var_11 + (-516)) & (-4)) == 0UL) {
        *(uint64_t *)(var_0 + (-96L)) = 4222170UL;
        indirect_placeholder_1();
        *(uint32_t *)var_11 = 22U;
    } else {
        var_12 = ((rsi & 18UL) == 0UL);
        var_13 = (uint64_t *)(var_0 + (-96L));
        if (var_12) {
            *var_13 = 4222191UL;
            indirect_placeholder_1();
            *(uint32_t *)var_11 = 22U;
        } else {
            *var_13 = 4221938UL;
            var_14 = indirect_placeholder_4(128UL);
            if (var_14 != 0UL) {
                var_15 = (uint64_t *)var_14;
                *var_15 = 0UL;
                *(uint64_t *)(var_14 + 120UL) = 0UL;
                var_16 = (var_14 + 8UL) & (-8L);
                var_17 = (uint64_t)((uint32_t)((uint64_t)(((uint32_t)var_14 - (uint32_t)var_16) + 128U) >> 3UL) & 536870911U);
                var_18 = (uint64_t)var_9;
                var_19 = var_18 << 3UL;
                rdi2_1 = var_16;
                rcx_2 = var_17;
                rbp_0 = var_14;
                while (rcx_2 != 0UL)
                    {
                        *(uint64_t *)rdi2_1 = 0UL;
                        rdi2_1 = rdi2_1 + var_19;
                        rcx_2 = rcx_2 + (-1L);
                    }
                var_20 = *(uint64_t *)(var_0 + (-88L));
                *(uint64_t *)(var_14 + 64UL) = var_20;
                var_21 = ((rsi & 2UL) == 0UL) ? rsi : ((uint64_t)(var_10 & (-517)) | 4UL);
                var_22 = (uint32_t *)(var_14 + 72UL);
                *var_22 = (uint32_t)var_21;
                *(uint32_t *)(var_14 + 44UL) = 4294967196U;
                var_23 = (uint64_t *)(var_0 + (-104L));
                *var_23 = 4222036UL;
                var_24 = indirect_placeholder_4(rdi);
                var_25 = helper_cc_compute_c_wrapper(var_24 + (-4096L), 4096UL, var_8, 17U);
                var_26 = (var_25 == 0UL) ? var_24 : 4096UL;
                var_27 = var_0 + (-112L);
                *(uint64_t *)var_27 = 4222059UL;
                var_28 = indirect_placeholder_5(var_14, var_26);
                local_sp_6 = var_27;
                local_sp_9 = var_27;
                local_sp_10 = var_27;
                if ((uint64_t)(unsigned char)var_28 == 0UL) {
                    *(uint64_t *)(local_sp_10 + (-8L)) = 4222651UL;
                    indirect_placeholder_1();
                } else {
                    var_29 = (uint64_t *)rdi;
                    if (*var_29 == 0UL) {
                        if (*var_23 != 0UL) {
                            var_49 = local_sp_9 + (-8L);
                            *(uint64_t *)var_49 = 4222522UL;
                            var_50 = indirect_placeholder_10(0UL, var_14, 4281393UL);
                            *var_15 = var_50;
                            r13_0 = r13_3;
                            local_sp_1 = var_49;
                            if (var_50 != 0UL) {
                                *(uint64_t *)(var_50 + 16UL) = r13_3;
                                *(uint16_t *)(*var_15 + 112UL) = (uint16_t)(unsigned short)9U;
                                var_51 = local_sp_9 + (-16L);
                                *(uint64_t *)var_51 = 4222553UL;
                                var_52 = indirect_placeholder_4(var_14);
                                local_sp_0 = var_51;
                                local_sp_1 = var_51;
                                if ((uint64_t)(unsigned char)var_52 != 0UL) {
                                    var_53 = local_sp_9 + (-24L);
                                    *(uint64_t *)var_53 = 4222605UL;
                                    var_54 = indirect_placeholder_5(var_14, 4281337UL);
                                    var_55 = (uint32_t *)(var_14 + 40UL);
                                    var_56 = (uint32_t)var_54;
                                    *var_55 = var_56;
                                    local_sp_0 = var_53;
                                    if ((uint32_t)((uint16_t)*var_22 & (unsigned short)516U) != 0U & (int)var_56 > (int)4294967295U) {
                                        *var_22 = (*var_22 | 4U);
                                    }
                                    var_57 = var_14 + 96UL;
                                    *(uint64_t *)(local_sp_0 + (-8L)) = 4222580UL;
                                    indirect_placeholder_23(var_57, 4294967295UL);
                                    return rbp_0;
                                }
                            }
                            *(uint64_t *)(local_sp_1 + (-8L)) = 4222626UL;
                            indirect_placeholder_3(r13_0);
                            var_58 = local_sp_1 + (-16L);
                            *(uint64_t *)var_58 = 4222634UL;
                            indirect_placeholder_1();
                            local_sp_5 = var_58;
                            var_59 = local_sp_5 + (-8L);
                            *(uint64_t *)var_59 = 4222643UL;
                            indirect_placeholder_1();
                            local_sp_10 = var_59;
                            *(uint64_t *)(local_sp_10 + (-8L)) = 4222651UL;
                            indirect_placeholder_1();
                            return rbp_0;
                        }
                        r15_2 = r15_1;
                        rax_0 = (unsigned char)(*var_22 >> 10U) & '\x01';
                        local_sp_7 = local_sp_6;
                    } else {
                        var_30 = var_0 + (-120L);
                        *(uint64_t *)var_30 = 4222097UL;
                        var_31 = indirect_placeholder_10(0UL, var_14, 4281393UL);
                        local_sp_7 = var_30;
                        r15_2 = var_31;
                        r15_1 = var_31;
                        local_sp_5 = var_30;
                        local_sp_6 = var_30;
                        if (var_31 != 0UL) {
                            var_59 = local_sp_5 + (-8L);
                            *(uint64_t *)var_59 = 4222643UL;
                            indirect_placeholder_1();
                            local_sp_10 = var_59;
                            *(uint64_t *)(local_sp_10 + (-8L)) = 4222651UL;
                            indirect_placeholder_1();
                            return rbp_0;
                        }
                        *(uint64_t *)(var_31 + 88UL) = 18446744073709551615UL;
                        *(uint64_t *)(var_31 + 104UL) = 18446744073709551615UL;
                        if (var_20 == 0UL) {
                            r15_2 = r15_1;
                            rax_0 = (unsigned char)(*var_22 >> 10U) & '\x01';
                            local_sp_7 = local_sp_6;
                        }
                    }
                    *(unsigned char *)(local_sp_7 + 23UL) = rax_0;
                    var_32 = *var_29;
                    rsi3_0 = var_32;
                    local_sp_8 = local_sp_7;
                    local_sp_9 = local_sp_7;
                    if (var_32 != 0UL) {
                        *(uint64_t *)(local_sp_7 + 24UL) = 0UL;
                        *(unsigned char *)(local_sp_7 + 22UL) = (((unsigned char)(rsi >> 12UL) & '\x01') ^ '\x01');
                        while (1U)
                            {
                                r13_0 = r13_2;
                                rdi2_0 = rsi3_0;
                                while (rcx_0 != 0UL)
                                    {
                                        var_33 = *(unsigned char *)rdi2_0;
                                        var_34 = rcx_0 + (-1L);
                                        rcx_0 = var_34;
                                        rcx_1 = var_34;
                                        if (var_33 == '\x00') {
                                            break;
                                        }
                                        rdi2_0 = rdi2_0 + var_18;
                                    }
                                var_35 = 18446744073709551614UL - (-2L);
                                rdx1_0 = var_35;
                                rdx1_1 = var_35;
                                if (!((*(unsigned char *)(local_sp_8 + 22UL) != '\x00') && (var_35 > 2UL)) && *(unsigned char *)((var_35 + rsi3_0) + (-1L)) == '/') {
                                    rdx1_1 = rdx1_0;
                                    while (*(unsigned char *)((rdx1_0 + rsi3_0) + (-2L)) != '/')
                                        {
                                            var_36 = rdx1_0 + (-1L);
                                            rdx1_0 = var_36;
                                            rdx1_1 = var_36;
                                            if (var_36 > 1UL) {
                                                break;
                                            }
                                            rdx1_1 = rdx1_0;
                                        }
                                }
                                var_37 = local_sp_8 + (-8L);
                                var_38 = (uint64_t *)var_37;
                                *var_38 = 4222334UL;
                                var_39 = indirect_placeholder_10(rdx1_1, var_14, rsi3_0);
                                local_sp_1 = var_37;
                                r13_1 = var_39;
                                if (var_39 != 0UL) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                *(uint64_t *)(var_39 + 88UL) = 0UL;
                                *(uint64_t *)(var_39 + 8UL) = r15_2;
                                *(uint64_t *)(var_39 + 48UL) = (var_39 + 264UL);
                                var_40 = (r13_2 == 0UL);
                                if (!var_40) {
                                    if (*(unsigned char *)(local_sp_8 + 15UL) != '\x00') {
                                        *(uint16_t *)(var_39 + 112UL) = (uint16_t)(unsigned short)11U;
                                        var_41 = local_sp_8 + (-16L);
                                        *(uint64_t *)var_41 = 4222408UL;
                                        indirect_placeholder_23(var_39, 1UL);
                                        local_sp_2 = var_41;
                                        local_sp_3 = var_41;
                                        if (*var_38 == 0UL) {
                                            *(uint64_t *)(var_39 + 16UL) = r13_2;
                                            local_sp_4 = local_sp_2;
                                        } else {
                                            *(uint64_t *)(var_39 + 16UL) = 0UL;
                                            var_44 = (uint64_t *)(local_sp_3 + 24UL);
                                            *(uint64_t *)(*var_44 + 16UL) = var_39;
                                            *var_44 = var_39;
                                            r13_1 = r13_2;
                                            local_sp_4 = local_sp_3;
                                        }
                                        var_45 = r12_0 + 1UL;
                                        var_46 = *(uint64_t *)((var_45 << 3UL) + rdi);
                                        r12_0 = var_45;
                                        r13_3 = r13_1;
                                        local_sp_9 = local_sp_4;
                                        rsi3_0 = var_46;
                                        r13_2 = r13_1;
                                        local_sp_8 = local_sp_4;
                                        if (var_46 == 0UL) {
                                            continue;
                                        }
                                        if (!((*(uint64_t *)(local_sp_4 + 8UL) != 0UL) && (var_45 > 1UL))) {
                                            loop_state_var = 1U;
                                            break;
                                        }
                                        var_47 = local_sp_4 + (-8L);
                                        *(uint64_t *)var_47 = 4222501UL;
                                        var_48 = indirect_placeholder_10(var_45, var_14, r13_1);
                                        r13_3 = var_48;
                                        local_sp_9 = var_47;
                                        loop_state_var = 1U;
                                        break;
                                    }
                                }
                                var_42 = local_sp_8 + (-16L);
                                *(uint64_t *)var_42 = 4222249UL;
                                var_43 = indirect_placeholder_48(0UL, var_14, var_39);
                                *(uint16_t *)(var_39 + 112UL) = (uint16_t)var_43.field_0;
                                local_sp_2 = var_42;
                                local_sp_3 = var_42;
                                local_sp_4 = var_42;
                                if (*var_38 == 0UL) {
                                    *(uint64_t *)(var_39 + 16UL) = r13_2;
                                    local_sp_4 = local_sp_2;
                                } else {
                                    *(uint64_t *)(var_39 + 16UL) = 0UL;
                                    if (var_40) {
                                        *(uint64_t *)(local_sp_8 + 8UL) = var_39;
                                    } else {
                                        var_44 = (uint64_t *)(local_sp_3 + 24UL);
                                        *(uint64_t *)(*var_44 + 16UL) = var_39;
                                        *var_44 = var_39;
                                        r13_1 = r13_2;
                                        local_sp_4 = local_sp_3;
                                    }
                                }
                                var_45 = r12_0 + 1UL;
                                var_46 = *(uint64_t *)((var_45 << 3UL) + rdi);
                                r12_0 = var_45;
                                r13_3 = r13_1;
                                local_sp_9 = local_sp_4;
                                rsi3_0 = var_46;
                                r13_2 = r13_1;
                                local_sp_8 = local_sp_4;
                                if (var_46 == 0UL) {
                                    continue;
                                }
                                if (!((*(uint64_t *)(local_sp_4 + 8UL) != 0UL) && (var_45 > 1UL))) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                var_47 = local_sp_4 + (-8L);
                                *(uint64_t *)var_47 = 4222501UL;
                                var_48 = indirect_placeholder_10(var_45, var_14, r13_1);
                                r13_3 = var_48;
                                local_sp_9 = var_47;
                                loop_state_var = 1U;
                                break;
                            }
                        switch (loop_state_var) {
                          case 1U:
                            {
                                break;
                            }
                            break;
                          case 0U:
                            {
                                *(uint64_t *)(local_sp_1 + (-8L)) = 4222626UL;
                                indirect_placeholder_3(r13_0);
                                var_58 = local_sp_1 + (-16L);
                                *(uint64_t *)var_58 = 4222634UL;
                                indirect_placeholder_1();
                                local_sp_5 = var_58;
                                var_59 = local_sp_5 + (-8L);
                                *(uint64_t *)var_59 = 4222643UL;
                                indirect_placeholder_1();
                                local_sp_10 = var_59;
                                *(uint64_t *)(local_sp_10 + (-8L)) = 4222651UL;
                                indirect_placeholder_1();
                                return rbp_0;
                            }
                            break;
                        }
                    }
                    var_49 = local_sp_9 + (-8L);
                    *(uint64_t *)var_49 = 4222522UL;
                    var_50 = indirect_placeholder_10(0UL, var_14, 4281393UL);
                    *var_15 = var_50;
                    r13_0 = r13_3;
                    local_sp_1 = var_49;
                    if (var_50 != 0UL) {
                        *(uint64_t *)(var_50 + 16UL) = r13_3;
                        *(uint16_t *)(*var_15 + 112UL) = (uint16_t)(unsigned short)9U;
                        var_51 = local_sp_9 + (-16L);
                        *(uint64_t *)var_51 = 4222553UL;
                        var_52 = indirect_placeholder_4(var_14);
                        local_sp_0 = var_51;
                        local_sp_1 = var_51;
                        if ((uint64_t)(unsigned char)var_52 != 0UL) {
                            var_53 = local_sp_9 + (-24L);
                            *(uint64_t *)var_53 = 4222605UL;
                            var_54 = indirect_placeholder_5(var_14, 4281337UL);
                            var_55 = (uint32_t *)(var_14 + 40UL);
                            var_56 = (uint32_t)var_54;
                            *var_55 = var_56;
                            local_sp_0 = var_53;
                            if ((uint32_t)((uint16_t)*var_22 & (unsigned short)516U) != 0U & (int)var_56 > (int)4294967295U) {
                                *var_22 = (*var_22 | 4U);
                            }
                            var_57 = var_14 + 96UL;
                            *(uint64_t *)(local_sp_0 + (-8L)) = 4222580UL;
                            indirect_placeholder_23(var_57, 4294967295UL);
                            return rbp_0;
                        }
                    }
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4222626UL;
                    indirect_placeholder_3(r13_0);
                    var_58 = local_sp_1 + (-16L);
                    *(uint64_t *)var_58 = 4222634UL;
                    indirect_placeholder_1();
                    local_sp_5 = var_58;
                    var_59 = local_sp_5 + (-8L);
                    *(uint64_t *)var_59 = 4222643UL;
                    indirect_placeholder_1();
                    local_sp_10 = var_59;
                }
            }
        }
    }
}
