typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_98_ret_type;
struct indirect_placeholder_99_ret_type;
struct indirect_placeholder_98_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_99_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_4(uint64_t param_0);
extern uint64_t init_r13(void);
extern uint64_t init_rax(void);
extern void indirect_placeholder_3(uint64_t param_0);
extern uint32_t init_state_0x82fc(void);
extern struct indirect_placeholder_98_ret_type indirect_placeholder_98(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_99_ret_type indirect_placeholder_99(uint64_t param_0, uint64_t param_1, uint64_t param_2);
uint64_t bb_chdir_long(uint64_t rdi) {
    uint64_t local_sp_0;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint32_t var_6;
    uint64_t var_7;
    uint64_t rbp_0;
    uint64_t rcx_3;
    uint64_t rax_3;
    uint64_t rbx_2;
    uint64_t local_sp_4;
    uint64_t rcx_5;
    uint64_t local_sp_7;
    uint64_t var_20;
    unsigned char *var_21;
    uint64_t rcx_6;
    uint64_t rcx_0;
    uint64_t var_22;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t rax_1;
    struct indirect_placeholder_98_ret_type var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t local_sp_6;
    uint64_t var_42;
    uint32_t _pre_phi;
    uint64_t rax_0;
    uint64_t local_sp_1;
    uint32_t *var_39;
    uint32_t var_40;
    uint64_t var_41;
    uint64_t rcx_7;
    uint64_t local_sp_8;
    uint64_t var_17;
    uint64_t rcx_2;
    uint64_t local_sp_3;
    uint64_t rbx_1;
    uint64_t rcx_4;
    uint64_t local_sp_5;
    uint32_t _pre;
    uint64_t var_32;
    uint64_t var_33;
    struct indirect_placeholder_99_ret_type var_34;
    uint64_t var_35;
    uint32_t var_36;
    uint64_t rax_2;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_19;
    uint64_t var_18;
    uint64_t local_sp_9;
    uint64_t var_16;
    uint64_t var_15;
    uint64_t var_8;
    uint64_t rdi1_0;
    uint64_t rcx_8;
    unsigned char var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_state_0x82fc();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_3;
    *(uint64_t *)(var_0 + (-64L)) = 4234999UL;
    indirect_placeholder_1();
    var_7 = (uint64_t)(uint32_t)var_1;
    rbp_0 = 0UL;
    rbx_2 = rdi;
    rcx_6 = 4282104UL;
    rcx_7 = 18446744073709551615UL;
    rbx_1 = rdi;
    rdi1_0 = rdi;
    rcx_8 = 0UL;
    if (var_7 == 0UL) {
        return rbp_0;
    }
    *(uint64_t *)(var_0 + (-72L)) = 4235023UL;
    indirect_placeholder_1();
    rbp_0 = var_7;
    if (*(uint32_t *)var_1 == 36U) {
        return;
    }
    var_8 = (uint64_t)var_6;
    rbp_0 = 0UL;
    while (rcx_7 != 0UL)
        {
            var_9 = *(unsigned char *)rdi1_0;
            var_10 = rcx_7 + (-1L);
            rcx_7 = var_10;
            rcx_8 = var_10;
            if (var_9 == '\x00') {
                break;
            }
            rdi1_0 = rdi1_0 + var_8;
        }
    var_11 = 18446744073709551614UL - (-2L);
    var_12 = var_11 + rdi;
    var_13 = var_0 + (-60L);
    var_14 = var_0 + (-80L);
    *(uint64_t *)var_14 = 4235066UL;
    indirect_placeholder_3(var_13);
    rax_3 = var_11;
    local_sp_8 = var_14;
    rax_1 = var_11;
    local_sp_9 = var_14;
    if (var_11 == 0UL) {
        var_15 = var_0 + (-88L);
        *(uint64_t *)var_15 = 4235260UL;
        indirect_placeholder_1();
        local_sp_9 = var_15;
        var_16 = local_sp_9 + (-8L);
        *(uint64_t *)var_16 = 4235285UL;
        indirect_placeholder_1();
        local_sp_8 = var_16;
    } else {
        rcx_6 = rcx_8 ^ (-1L);
        if (var_11 > 4095UL) {
            var_16 = local_sp_9 + (-8L);
            *(uint64_t *)var_16 = 4235285UL;
            indirect_placeholder_1();
            local_sp_8 = var_16;
        }
    }
    var_17 = local_sp_8 + (-8L);
    *(uint64_t *)var_17 = 4235101UL;
    indirect_placeholder_1();
    rcx_2 = rcx_6;
    local_sp_3 = var_17;
    if (*(unsigned char *)rdi == '/') {
        var_18 = local_sp_8 + (-16L);
        *(uint64_t *)var_18 = 4235438UL;
        indirect_placeholder_1();
        rcx_2 = 4282104UL;
        local_sp_3 = var_18;
    }
    rcx_3 = rcx_2;
    local_sp_4 = local_sp_3;
    if (var_12 < rdi) {
        var_19 = local_sp_3 + (-8L);
        *(uint64_t *)var_19 = 4235468UL;
        indirect_placeholder_1();
        rcx_3 = 4282104UL;
        local_sp_4 = var_19;
    }
    rcx_5 = rcx_3;
    local_sp_7 = local_sp_4;
    rcx_4 = rcx_3;
    local_sp_5 = local_sp_4;
    if ((long)var_11 <= (long)4095UL) {
        while (1U)
            {
                var_20 = local_sp_7 + (-8L);
                *(uint64_t *)var_20 = 4235592UL;
                indirect_placeholder_1();
                var_21 = (unsigned char *)rax_3;
                *var_21 = (unsigned char)'\x00';
                rcx_0 = rcx_5;
                local_sp_0 = var_20;
                if ((long)(rax_3 - rbx_2) > (long)4095UL) {
                    var_22 = local_sp_7 + (-16L);
                    *(uint64_t *)var_22 = 4235519UL;
                    indirect_placeholder_1();
                    rcx_0 = 4282104UL;
                    local_sp_0 = var_22;
                }
                var_23 = local_sp_0 + 12UL;
                var_24 = local_sp_0 + (-8L);
                *(uint64_t *)var_24 = 4235532UL;
                var_25 = indirect_placeholder_98(var_23, rbx_2, rcx_0);
                var_26 = var_25.field_0;
                var_27 = var_25.field_1;
                *var_21 = (unsigned char)'/';
                rcx_5 = var_27;
                rax_0 = var_26;
                local_sp_1 = var_24;
                rcx_4 = var_27;
                if ((uint64_t)(uint32_t)var_26 != 0UL) {
                    loop_state_var = 1U;
                    break;
                }
                var_28 = rax_3 + 1UL;
                var_29 = local_sp_0 + (-16L);
                *(uint64_t *)var_29 = 4235553UL;
                var_30 = indirect_placeholder_4(var_28);
                var_31 = var_12 - var_30;
                rax_3 = var_31;
                rbx_2 = var_30;
                local_sp_7 = var_29;
                rax_1 = var_31;
                rbx_1 = var_30;
                local_sp_5 = var_29;
                if ((long)var_31 > (long)4095UL) {
                    continue;
                }
                loop_state_var = 0U;
                break;
            }
        switch (loop_state_var) {
          case 0U:
            {
                break;
            }
            break;
          case 1U:
            {
                *(uint64_t *)(local_sp_1 + (-8L)) = 4235206UL;
                indirect_placeholder_1();
                var_39 = (uint32_t *)rax_0;
                var_40 = *var_39;
                var_41 = local_sp_1 + 4UL;
                *(uint64_t *)(local_sp_1 + (-16L)) = 4235218UL;
                indirect_placeholder_3(var_41);
                *(uint64_t *)(local_sp_1 + (-24L)) = 4235223UL;
                indirect_placeholder_1();
                *var_39 = var_40;
                rbp_0 = 4294967295UL;
                return rbp_0;
            }
            break;
        }
    }
    rax_2 = rax_1;
    local_sp_6 = local_sp_5;
    if (var_12 > rbx_1) {
        var_32 = local_sp_5 + 12UL;
        var_33 = local_sp_5 + (-8L);
        *(uint64_t *)var_33 = 4235177UL;
        var_34 = indirect_placeholder_99(var_32, rbx_1, rcx_4);
        var_35 = var_34.field_0;
        var_36 = (uint32_t)var_35;
        local_sp_6 = var_33;
        _pre_phi = var_36;
        rax_0 = var_35;
        local_sp_1 = var_33;
        rax_2 = var_35;
        var_37 = local_sp_6 + 12UL;
        var_38 = local_sp_6 + (-8L);
        *(uint64_t *)var_38 = 4235191UL;
        indirect_placeholder_3(var_37);
        rax_0 = rax_2;
        local_sp_1 = var_38;
        if ((uint64_t)var_36 != 0UL & (uint64_t)_pre_phi != 0UL) {
            var_42 = local_sp_6 + 4UL;
            *(uint64_t *)(local_sp_6 + (-16L)) = 4235626UL;
            indirect_placeholder_3(var_42);
            return rbp_0;
        }
    }
    _pre = (uint32_t)rax_1;
    _pre_phi = _pre;
    var_37 = local_sp_6 + 12UL;
    var_38 = local_sp_6 + (-8L);
    *(uint64_t *)var_38 = 4235191UL;
    indirect_placeholder_3(var_37);
    rax_0 = rax_2;
    local_sp_1 = var_38;
    if ((uint64_t)_pre_phi != 0UL) {
        var_42 = local_sp_6 + 4UL;
        *(uint64_t *)(local_sp_6 + (-16L)) = 4235626UL;
        indirect_placeholder_3(var_42);
        return rbp_0;
    }
    *(uint64_t *)(local_sp_1 + (-8L)) = 4235206UL;
    indirect_placeholder_1();
    var_39 = (uint32_t *)rax_0;
    var_40 = *var_39;
    var_41 = local_sp_1 + 4UL;
    *(uint64_t *)(local_sp_1 + (-16L)) = 4235218UL;
    indirect_placeholder_3(var_41);
    *(uint64_t *)(local_sp_1 + (-24L)) = 4235223UL;
    indirect_placeholder_1();
    *var_39 = var_40;
    rbp_0 = 4294967295UL;
}
