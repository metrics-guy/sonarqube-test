typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_78_ret_type;
struct indirect_placeholder_79_ret_type;
struct indirect_placeholder_78_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_79_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint32_t init_state_0x82fc(void);
extern struct indirect_placeholder_78_ret_type indirect_placeholder_78(uint64_t param_0);
extern struct indirect_placeholder_79_ret_type indirect_placeholder_79(uint64_t param_0, uint64_t param_1, uint64_t param_2);
uint64_t bb_rpl_fstatat(uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t rcx) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint32_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    struct indirect_placeholder_78_ret_type var_10;
    uint64_t var_11;
    uint64_t r12_0;
    uint64_t var_12;
    uint64_t rdi2_0;
    uint64_t rcx4_0;
    uint64_t rcx4_1;
    unsigned char var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint32_t *_pre_phi30;
    uint32_t *var_16;
    uint32_t var_17;
    uint64_t var_18;
    bool var_19;
    uint64_t var_20;
    uint64_t *var_21;
    struct indirect_placeholder_79_ret_type var_22;
    uint64_t var_23;
    uint64_t local_sp_0;
    uint32_t var_24;
    uint64_t var_25;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_state_0x82fc();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_8 = (uint64_t)(uint32_t)rdi;
    var_9 = var_0 + (-64L);
    *(uint64_t *)var_9 = 4236476UL;
    var_10 = indirect_placeholder_78(rdx);
    var_11 = (uint64_t)(uint32_t)var_10.field_0;
    r12_0 = var_11;
    rdi2_0 = rsi;
    rcx4_0 = 18446744073709551615UL;
    rcx4_1 = 0UL;
    local_sp_0 = var_9;
    if (var_11 == 0UL) {
        return r12_0;
    }
    var_12 = (uint64_t)var_7;
    r12_0 = 0UL;
    while (rcx4_0 != 0UL)
        {
            var_13 = *(unsigned char *)rdi2_0;
            var_14 = rcx4_0 + (-1L);
            rcx4_0 = var_14;
            rcx4_1 = var_14;
            if (var_13 == '\x00') {
                break;
            }
            rdi2_0 = rdi2_0 + var_12;
        }
    var_15 = 18446744073709551614UL - (-2L);
    if ((uint64_t)((uint16_t)rcx & (unsigned short)256U) != 0UL) {
        if (*(unsigned char *)((var_15 + rsi) + (-1L)) == '/') {
            return r12_0;
        }
        var_16 = (uint32_t *)(rdx + 24UL);
        var_17 = (uint32_t)((uint16_t)*var_16 & (unsigned short)61440U);
        var_18 = (uint64_t)var_17;
        _pre_phi30 = var_16;
        if ((uint64_t)((var_17 + (-16384)) & (-4096)) == 0UL) {
            return r12_0;
        }
        var_19 = ((uint64_t)((var_17 + (-40960)) & (-4096)) == 0UL);
        var_20 = var_0 + (-72L);
        var_21 = (uint64_t *)var_20;
        local_sp_0 = var_20;
        r12_0 = 4294967295UL;
        if (!var_19) {
            *var_21 = 4236595UL;
            indirect_placeholder_1();
            *(uint32_t *)var_18 = 20U;
            return r12_0;
        }
        *var_21 = 4236565UL;
        var_22 = indirect_placeholder_79(rdx, var_8, rsi);
        var_23 = (uint64_t)(uint32_t)var_22.field_0;
        r12_0 = var_23;
        if (var_23 == 0UL) {
            return r12_0;
        }
    }
    _pre_phi30 = (uint32_t *)(rdx + 24UL);
}
