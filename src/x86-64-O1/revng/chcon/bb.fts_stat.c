typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_fts_stat_ret_type;
struct bb_fts_stat_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t init_rcx(void);
extern uint32_t init_state_0x82fc(void);
extern uint64_t indirect_placeholder_20(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
struct bb_fts_stat_ret_type bb_fts_stat(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint32_t var_6;
    uint64_t var_7;
    uint64_t *var_8;
    unsigned char *_pre50;
    unsigned char *_pre_phi51;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t rdi3_0;
    uint64_t rcx_0;
    uint64_t rax_2;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint32_t *_pre_phi53;
    uint64_t local_sp_0;
    uint64_t rcx_1;
    uint32_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t rax_1;
    uint64_t var_26;
    uint64_t rcx_2;
    uint64_t var_27;
    uint64_t var_28;
    struct bb_fts_stat_ret_type mrv;
    struct bb_fts_stat_ret_type mrv1;
    uint64_t var_13;
    uint32_t *var_14;
    unsigned char *var_9;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t var_12;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r12();
    var_4 = init_rcx();
    var_5 = init_cc_src2();
    var_6 = init_state_0x82fc();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    var_7 = rsi + 120UL;
    var_8 = (uint64_t *)(rsi + 88UL);
    rcx_1 = 256UL;
    rax_1 = 18446744073709551615UL;
    rax_2 = 13UL;
    rcx_2 = var_4;
    if (*var_8 == 0UL) {
        var_9 = (unsigned char *)(rdi + 72UL);
        _pre_phi51 = var_9;
        if ((*var_9 & '\x01') == '\x00') {
            if (((*_pre_phi51 & '\x02') == '\x00') && ((uint64_t)(unsigned char)rdx == 0UL)) {
                var_18 = *(uint64_t *)(rsi + 48UL);
                var_19 = (uint64_t)*(uint32_t *)(rdi + 44UL);
                *(uint64_t *)(var_0 + (-32L)) = 4217453UL;
                var_20 = indirect_placeholder_20(var_7, var_19, var_18, 256UL);
                if ((uint64_t)(uint32_t)var_20 != 0UL) {
                    *(uint64_t *)(var_0 + (-40L)) = 4217466UL;
                    indirect_placeholder_1();
                    *(uint32_t *)(rsi + 64UL) = *(uint32_t *)var_20;
                    *(uint64_t *)var_7 = 0UL;
                    *(uint64_t *)(rsi + 256UL) = 0UL;
                    var_21 = (rsi + 128UL) & (-8L);
                    var_22 = (uint64_t)var_6 << 3UL;
                    rdi3_0 = var_21;
                    rcx_0 = (uint64_t)((uint32_t)((uint64_t)(((uint32_t)var_7 - (uint32_t)var_21) + 144U) >> 3UL) & 536870911U);
                    rax_2 = 10UL;
                    rcx_2 = 0UL;
                    while (rcx_0 != 0UL)
                        {
                            *(uint64_t *)rdi3_0 = 0UL;
                            rdi3_0 = var_22 + rdi3_0;
                            rcx_0 = rcx_0 + (-1L);
                        }
                    mrv.field_0 = rax_2;
                    mrv1 = mrv;
                    mrv1.field_1 = rcx_2;
                    return mrv1;
                }
            }
            var_10 = (uint64_t *)(rsi + 48UL);
            var_11 = *var_10;
            *(uint64_t *)(var_0 + (-32L)) = 4217264UL;
            var_12 = indirect_placeholder_5(var_11, var_7);
            rcx_1 = var_4;
            if ((uint64_t)(uint32_t)var_12 != 0UL) {
                var_13 = var_0 + (-40L);
                *(uint64_t *)var_13 = 4217326UL;
                indirect_placeholder_1();
                var_14 = (uint32_t *)var_12;
                _pre_phi53 = var_14;
                local_sp_0 = var_13;
                if (*var_14 != 2U) {
                    var_15 = *var_10;
                    var_16 = var_0 + (-48L);
                    *(uint64_t *)var_16 = 4217343UL;
                    var_17 = indirect_placeholder_5(var_15, var_7);
                    local_sp_0 = var_16;
                    if ((uint64_t)(uint32_t)var_17 != 0UL) {
                        *(uint64_t *)(var_0 + (-56L)) = 4217418UL;
                        indirect_placeholder_1();
                        *(uint32_t *)var_17 = 0U;
                        mrv.field_0 = rax_2;
                        mrv1 = mrv;
                        mrv1.field_1 = rcx_2;
                        return mrv1;
                    }
                    _pre_phi53 = (uint32_t *)var_17;
                }
                *(uint64_t *)(local_sp_0 + (-8L)) = 4217352UL;
                indirect_placeholder_1();
                *(uint32_t *)(rsi + 64UL) = *_pre_phi53;
                *(uint64_t *)var_7 = 0UL;
                *(uint64_t *)(rsi + 256UL) = 0UL;
                var_21 = (rsi + 128UL) & (-8L);
                var_22 = (uint64_t)var_6 << 3UL;
                rdi3_0 = var_21;
                rcx_0 = (uint64_t)((uint32_t)((uint64_t)(((uint32_t)var_7 - (uint32_t)var_21) + 144U) >> 3UL) & 536870911U);
                rax_2 = 10UL;
                rcx_2 = 0UL;
                while (rcx_0 != 0UL)
                    {
                        *(uint64_t *)rdi3_0 = 0UL;
                        rdi3_0 = var_22 + rdi3_0;
                        rcx_0 = rcx_0 + (-1L);
                    }
                mrv.field_0 = rax_2;
                mrv1 = mrv;
                mrv1.field_1 = rcx_2;
                return mrv1;
            }
        }
        var_10 = (uint64_t *)(rsi + 48UL);
        var_11 = *var_10;
        *(uint64_t *)(var_0 + (-32L)) = 4217264UL;
        var_12 = indirect_placeholder_5(var_11, var_7);
        rcx_1 = var_4;
        if ((uint64_t)(uint32_t)var_12 != 0UL) {
            var_13 = var_0 + (-40L);
            *(uint64_t *)var_13 = 4217326UL;
            indirect_placeholder_1();
            var_14 = (uint32_t *)var_12;
            _pre_phi53 = var_14;
            local_sp_0 = var_13;
            if (*var_14 != 2U) {
                var_15 = *var_10;
                var_16 = var_0 + (-48L);
                *(uint64_t *)var_16 = 4217343UL;
                var_17 = indirect_placeholder_5(var_15, var_7);
                local_sp_0 = var_16;
                if ((uint64_t)(uint32_t)var_17 != 0UL) {
                    *(uint64_t *)(var_0 + (-56L)) = 4217418UL;
                    indirect_placeholder_1();
                    *(uint32_t *)var_17 = 0U;
                    mrv.field_0 = rax_2;
                    mrv1 = mrv;
                    mrv1.field_1 = rcx_2;
                    return mrv1;
                }
                _pre_phi53 = (uint32_t *)var_17;
            }
            *(uint64_t *)(local_sp_0 + (-8L)) = 4217352UL;
            indirect_placeholder_1();
            *(uint32_t *)(rsi + 64UL) = *_pre_phi53;
            *(uint64_t *)var_7 = 0UL;
            *(uint64_t *)(rsi + 256UL) = 0UL;
            var_21 = (rsi + 128UL) & (-8L);
            var_22 = (uint64_t)var_6 << 3UL;
            rdi3_0 = var_21;
            rcx_0 = (uint64_t)((uint32_t)((uint64_t)(((uint32_t)var_7 - (uint32_t)var_21) + 144U) >> 3UL) & 536870911U);
            rax_2 = 10UL;
            rcx_2 = 0UL;
            while (rcx_0 != 0UL)
                {
                    *(uint64_t *)rdi3_0 = 0UL;
                    rdi3_0 = var_22 + rdi3_0;
                    rcx_0 = rcx_0 + (-1L);
                }
            mrv.field_0 = rax_2;
            mrv1 = mrv;
            mrv1.field_1 = rcx_2;
            return mrv1;
        }
        var_23 = (uint32_t)((uint16_t)*(uint32_t *)(rsi + 144UL) & (unsigned short)61440U);
        rax_2 = 1UL;
        rcx_2 = rcx_1;
        if ((uint64_t)((var_23 + (-16384)) & (-4096)) == 0UL) {
            rax_2 = 12UL;
            if ((uint64_t)((var_23 + (-40960)) & (-4096)) == 0UL) {
                var_24 = ((uint64_t)((var_23 + (-32768)) & (-4096)) == 0UL) ? 8UL : 3UL;
                rax_2 = var_24;
            }
        } else {
            var_25 = *(uint64_t *)(rsi + 136UL);
            if (var_25 <= 1UL & (long)*var_8 > (long)0UL) {
                rax_1 = var_25 - (uint64_t)(((*(unsigned char *)(rdi + 72UL) >> '\x04') & '\x02') ^ '\x02');
            }
            *(uint64_t *)(rsi + 104UL) = rax_1;
            var_26 = rsi + 264UL;
            if (*(unsigned char *)var_26 != '.') {
                if (*(unsigned char *)(rsi + 265UL) != '\x00') {
                    if ((*(uint32_t *)var_26 & 16776960U) == 11776U) {
                        mrv.field_0 = rax_2;
                        mrv1 = mrv;
                        mrv1.field_1 = rcx_2;
                        return mrv1;
                    }
                }
                var_27 = helper_cc_compute_c_wrapper(*var_8 + (-1L), 1UL, var_5, 17U);
                var_28 = (uint64_t)((((0U - (uint32_t)var_27) & (-4)) + 5U) & (-3));
                rax_2 = var_28;
            }
        }
    } else {
        _pre50 = (unsigned char *)(rdi + 72UL);
        _pre_phi51 = _pre50;
        if (((*_pre_phi51 & '\x02') == '\x00') && ((uint64_t)(unsigned char)rdx == 0UL)) {
            var_10 = (uint64_t *)(rsi + 48UL);
            var_11 = *var_10;
            *(uint64_t *)(var_0 + (-32L)) = 4217264UL;
            var_12 = indirect_placeholder_5(var_11, var_7);
            rcx_1 = var_4;
            if ((uint64_t)(uint32_t)var_12 == 0UL) {
                var_13 = var_0 + (-40L);
                *(uint64_t *)var_13 = 4217326UL;
                indirect_placeholder_1();
                var_14 = (uint32_t *)var_12;
                _pre_phi53 = var_14;
                local_sp_0 = var_13;
                if (*var_14 != 2U) {
                    var_15 = *var_10;
                    var_16 = var_0 + (-48L);
                    *(uint64_t *)var_16 = 4217343UL;
                    var_17 = indirect_placeholder_5(var_15, var_7);
                    local_sp_0 = var_16;
                    if ((uint64_t)(uint32_t)var_17 != 0UL) {
                        *(uint64_t *)(var_0 + (-56L)) = 4217418UL;
                        indirect_placeholder_1();
                        *(uint32_t *)var_17 = 0U;
                        mrv.field_0 = rax_2;
                        mrv1 = mrv;
                        mrv1.field_1 = rcx_2;
                        return mrv1;
                    }
                    _pre_phi53 = (uint32_t *)var_17;
                }
                *(uint64_t *)(local_sp_0 + (-8L)) = 4217352UL;
                indirect_placeholder_1();
                *(uint32_t *)(rsi + 64UL) = *_pre_phi53;
                *(uint64_t *)var_7 = 0UL;
                *(uint64_t *)(rsi + 256UL) = 0UL;
                var_21 = (rsi + 128UL) & (-8L);
                var_22 = (uint64_t)var_6 << 3UL;
                rdi3_0 = var_21;
                rcx_0 = (uint64_t)((uint32_t)((uint64_t)(((uint32_t)var_7 - (uint32_t)var_21) + 144U) >> 3UL) & 536870911U);
                rax_2 = 10UL;
                rcx_2 = 0UL;
                while (rcx_0 != 0UL)
                    {
                        *(uint64_t *)rdi3_0 = 0UL;
                        rdi3_0 = var_22 + rdi3_0;
                        rcx_0 = rcx_0 + (-1L);
                    }
                mrv.field_0 = rax_2;
                mrv1 = mrv;
                mrv1.field_1 = rcx_2;
                return mrv1;
            }
        }
        var_18 = *(uint64_t *)(rsi + 48UL);
        var_19 = (uint64_t)*(uint32_t *)(rdi + 44UL);
        *(uint64_t *)(var_0 + (-32L)) = 4217453UL;
        var_20 = indirect_placeholder_20(var_7, var_19, var_18, 256UL);
        if ((uint64_t)(uint32_t)var_20 != 0UL) {
            *(uint64_t *)(var_0 + (-40L)) = 4217466UL;
            indirect_placeholder_1();
            *(uint32_t *)(rsi + 64UL) = *(uint32_t *)var_20;
            *(uint64_t *)var_7 = 0UL;
            *(uint64_t *)(rsi + 256UL) = 0UL;
            var_21 = (rsi + 128UL) & (-8L);
            var_22 = (uint64_t)var_6 << 3UL;
            rdi3_0 = var_21;
            rcx_0 = (uint64_t)((uint32_t)((uint64_t)(((uint32_t)var_7 - (uint32_t)var_21) + 144U) >> 3UL) & 536870911U);
            rax_2 = 10UL;
            rcx_2 = 0UL;
            while (rcx_0 != 0UL)
                {
                    *(uint64_t *)rdi3_0 = 0UL;
                    rdi3_0 = var_22 + rdi3_0;
                    rcx_0 = rcx_0 + (-1L);
                }
            mrv.field_0 = rax_2;
            mrv1 = mrv;
            mrv1.field_1 = rcx_2;
            return mrv1;
        }
        var_23 = (uint32_t)((uint16_t)*(uint32_t *)(rsi + 144UL) & (unsigned short)61440U);
        rax_2 = 1UL;
        rcx_2 = rcx_1;
        if ((uint64_t)((var_23 + (-16384)) & (-4096)) != 0UL) {
            rax_2 = 12UL;
            if ((uint64_t)((var_23 + (-40960)) & (-4096)) == 0UL) {
                var_24 = ((uint64_t)((var_23 + (-32768)) & (-4096)) == 0UL) ? 8UL : 3UL;
                rax_2 = var_24;
            }
            mrv.field_0 = rax_2;
            mrv1 = mrv;
            mrv1.field_1 = rcx_2;
            return mrv1;
        }
        var_25 = *(uint64_t *)(rsi + 136UL);
        if (var_25 <= 1UL & (long)*var_8 > (long)0UL) {
            rax_1 = var_25 - (uint64_t)(((*(unsigned char *)(rdi + 72UL) >> '\x04') & '\x02') ^ '\x02');
        }
        *(uint64_t *)(rsi + 104UL) = rax_1;
        var_26 = rsi + 264UL;
        if (*(unsigned char *)var_26 != '.') {
            if (*(unsigned char *)(rsi + 265UL) != '\x00') {
                if ((*(uint32_t *)var_26 & 16776960U) != 11776U) {
                    mrv.field_0 = rax_2;
                    mrv1 = mrv;
                    mrv1.field_1 = rcx_2;
                    return mrv1;
                }
            }
            var_27 = helper_cc_compute_c_wrapper(*var_8 + (-1L), 1UL, var_5, 17U);
            var_28 = (uint64_t)((((0U - (uint32_t)var_27) & (-4)) + 5U) & (-3));
            rax_2 = var_28;
        }
    }
}
