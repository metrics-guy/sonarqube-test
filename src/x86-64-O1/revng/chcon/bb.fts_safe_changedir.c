typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_4(uint64_t param_0);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern void indirect_placeholder_26(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint32_t init_state_0x82fc(void);
uint64_t bb_fts_safe_changedir(uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t rcx) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint32_t var_8;
    uint64_t var_9;
    uint32_t var_10;
    uint64_t var_11;
    uint64_t var_32;
    uint64_t rax_0;
    uint64_t var_33;
    uint64_t rbp_0;
    uint64_t var_34;
    uint64_t rax_3;
    uint64_t rax_2;
    uint64_t local_sp_1;
    uint64_t storemerge;
    uint64_t r14_0;
    uint64_t rbx_0;
    uint32_t *var_37;
    uint32_t var_38;
    uint64_t var_36;
    uint64_t var_35;
    uint64_t local_sp_3;
    uint64_t var_25;
    uint64_t var_22;
    uint64_t var_23;
    uint32_t var_24;
    uint64_t local_sp_4;
    uint64_t var_12;
    uint64_t storemerge1;
    uint64_t var_27;
    uint64_t cc_src_0;
    uint64_t cc_dst_0;
    uint32_t cc_op_0;
    uint64_t rdi2_0;
    uint64_t rsi3_0;
    uint64_t rcx4_0;
    uint64_t cc_src_1;
    uint64_t cc_dst_1;
    uint64_t var_13;
    uint64_t var_14;
    uint32_t cc_op_1;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t rax_4;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_26;
    uint64_t r15_0;
    uint64_t var_30;
    uint64_t *var_31;
    uint64_t var_28;
    uint32_t var_29;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_cc_src2();
    var_8 = init_state_0x82fc();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_9 = var_0 + (-200L);
    var_10 = (uint32_t)rdx;
    var_11 = (uint64_t)var_10;
    rbp_0 = var_11;
    storemerge = 4294967295UL;
    r14_0 = var_11;
    rbx_0 = 0UL;
    local_sp_3 = var_9;
    local_sp_4 = var_9;
    storemerge1 = 1UL;
    cc_src_0 = 152UL;
    cc_dst_0 = rcx;
    cc_op_0 = 25U;
    rdi2_0 = 4281336UL;
    rsi3_0 = rcx;
    rcx4_0 = 3UL;
    r15_0 = 1UL;
    if (rcx != 0UL) {
        var_26 = (uint64_t)*(uint32_t *)(rdi + 72UL);
        rax_3 = var_26;
        r15_0 = 0UL;
        storemerge1 = 0UL;
        rax_4 = var_26;
        if ((var_26 & 4UL) == 0UL) {
            if (((uint64_t)((uint16_t)rax_4 & (unsigned short)512U) != 0UL) && ((int)var_10 > (int)4294967295U)) {
                *(uint64_t *)(var_0 + (-208L)) = 4219423UL;
                indirect_placeholder_1();
            }
            return (uint64_t)(uint32_t)rbx_0;
        }
        if ((int)var_10 >= (int)0U) {
            var_27 = local_sp_4 + (-8L);
            *(uint64_t *)var_27 = 4219376UL;
            var_28 = indirect_placeholder_5(rdi, rcx);
            var_29 = (uint32_t)var_28;
            rax_3 = var_28;
            r14_0 = (uint64_t)var_29;
            rbx_0 = 4294967295UL;
            local_sp_3 = var_27;
            r15_0 = storemerge1;
            if ((int)var_29 > (int)4294967295U) {
                return (uint64_t)(uint32_t)rbx_0;
            }
        }
        var_30 = local_sp_3 + (-8L);
        var_31 = (uint64_t *)var_30;
        *var_31 = 4219582UL;
        indirect_placeholder_1();
        rax_2 = rax_3;
        local_sp_1 = var_30;
        if ((uint64_t)(uint32_t)rax_3 != 0UL) {
            var_32 = *var_31;
            rax_0 = var_32;
            if (*(uint64_t *)(rsi + 120UL) == var_32) {
                var_34 = local_sp_3 + (-16L);
                *(uint64_t *)var_34 = 4219621UL;
                indirect_placeholder_1();
                *(uint32_t *)rax_0 = 2U;
                rax_2 = rax_0;
                local_sp_1 = var_34;
            } else {
                var_33 = *(uint64_t *)local_sp_3;
                rax_0 = var_33;
                rax_2 = var_33;
                if (*(uint64_t *)(rsi + 128UL) == var_33) {
                    var_34 = local_sp_3 + (-16L);
                    *(uint64_t *)var_34 = 4219621UL;
                    indirect_placeholder_1();
                    *(uint32_t *)rax_0 = 2U;
                    rax_2 = rax_0;
                    local_sp_1 = var_34;
                } else {
                    if ((*(unsigned char *)(rdi + 73UL) & '\x02') != '\x00') {
                        var_35 = (uint64_t)(unsigned char)r15_0 ^ 1UL;
                        *(uint64_t *)(local_sp_3 + (-16L)) = 4219519UL;
                        indirect_placeholder_26(var_35, rdi, r14_0);
                        return (uint64_t)(uint32_t)rbx_0;
                    }
                    var_36 = local_sp_3 + (-16L);
                    *(uint64_t *)var_36 = 4219493UL;
                    indirect_placeholder_1();
                    local_sp_1 = var_36;
                    storemerge = (uint64_t)(uint32_t)var_33;
                }
            }
        }
        rbx_0 = storemerge;
        if ((int)(uint32_t)rbp_0 < (int)0U) {
            *(uint64_t *)(local_sp_1 + (-8L)) = 4219526UL;
            indirect_placeholder_1();
            var_37 = (uint32_t *)rax_2;
            var_38 = *var_37;
            *(uint64_t *)(local_sp_1 + (-16L)) = 4219536UL;
            indirect_placeholder_1();
            *(uint64_t *)(local_sp_1 + (-24L)) = 4219541UL;
            indirect_placeholder_1();
            *var_37 = var_38;
        }
        return (uint64_t)(uint32_t)rbx_0;
    }
    var_12 = (uint64_t)var_8;
    cc_op_0 = 14U;
    cc_src_1 = cc_src_0;
    cc_dst_1 = cc_dst_0;
    cc_op_1 = cc_op_0;
    while (rcx4_0 != 0UL)
        {
            var_13 = (uint64_t)*(unsigned char *)rdi2_0;
            var_14 = (uint64_t)*(unsigned char *)rsi3_0 - var_13;
            cc_src_0 = var_13;
            cc_dst_0 = var_14;
            cc_src_1 = var_13;
            cc_dst_1 = var_14;
            cc_op_1 = 14U;
            if ((uint64_t)(unsigned char)var_14 == 0UL) {
                break;
            }
            rdi2_0 = rdi2_0 + var_12;
            rsi3_0 = rsi3_0 + var_12;
            rcx4_0 = rcx4_0 + (-1L);
            cc_op_0 = 14U;
            cc_src_1 = cc_src_0;
            cc_dst_1 = cc_dst_0;
            cc_op_1 = cc_op_0;
        }
    var_15 = helper_cc_compute_all_wrapper(cc_dst_1, cc_src_1, var_7, cc_op_1);
    var_16 = ((var_15 & 65UL) == 0UL);
    var_17 = helper_cc_compute_c_wrapper(cc_dst_1, var_15, var_7, 1U);
    if ((uint64_t)((unsigned char)var_16 - (unsigned char)var_17) != 0UL) {
        var_26 = (uint64_t)*(uint32_t *)(rdi + 72UL);
        rax_3 = var_26;
        r15_0 = 0UL;
        storemerge1 = 0UL;
        rax_4 = var_26;
        if ((var_26 & 4UL) != 0UL) {
            if (((uint64_t)((uint16_t)rax_4 & (unsigned short)512U) != 0UL) && ((int)var_10 > (int)4294967295U)) {
                *(uint64_t *)(var_0 + (-208L)) = 4219423UL;
                indirect_placeholder_1();
            }
            return (uint64_t)(uint32_t)rbx_0;
        }
        if ((int)var_10 >= (int)0U) {
            var_27 = local_sp_4 + (-8L);
            *(uint64_t *)var_27 = 4219376UL;
            var_28 = indirect_placeholder_5(rdi, rcx);
            var_29 = (uint32_t)var_28;
            rax_3 = var_28;
            r14_0 = (uint64_t)var_29;
            rbx_0 = 4294967295UL;
            local_sp_3 = var_27;
            r15_0 = storemerge1;
            if ((int)var_29 > (int)4294967295U) {
                return (uint64_t)(uint32_t)rbx_0;
            }
        }
        var_30 = local_sp_3 + (-8L);
        var_31 = (uint64_t *)var_30;
        *var_31 = 4219582UL;
        indirect_placeholder_1();
        rax_2 = rax_3;
        local_sp_1 = var_30;
        if ((uint64_t)(uint32_t)rax_3 != 0UL) {
            var_32 = *var_31;
            rax_0 = var_32;
            if (*(uint64_t *)(rsi + 120UL) == var_32) {
                var_34 = local_sp_3 + (-16L);
                *(uint64_t *)var_34 = 4219621UL;
                indirect_placeholder_1();
                *(uint32_t *)rax_0 = 2U;
                rax_2 = rax_0;
                local_sp_1 = var_34;
            } else {
                var_33 = *(uint64_t *)local_sp_3;
                rax_0 = var_33;
                rax_2 = var_33;
                if (*(uint64_t *)(rsi + 128UL) == var_33) {
                    var_34 = local_sp_3 + (-16L);
                    *(uint64_t *)var_34 = 4219621UL;
                    indirect_placeholder_1();
                    *(uint32_t *)rax_0 = 2U;
                    rax_2 = rax_0;
                    local_sp_1 = var_34;
                } else {
                    if ((*(unsigned char *)(rdi + 73UL) & '\x02') != '\x00') {
                        var_35 = (uint64_t)(unsigned char)r15_0 ^ 1UL;
                        *(uint64_t *)(local_sp_3 + (-16L)) = 4219519UL;
                        indirect_placeholder_26(var_35, rdi, r14_0);
                        return (uint64_t)(uint32_t)rbx_0;
                    }
                    var_36 = local_sp_3 + (-16L);
                    *(uint64_t *)var_36 = 4219493UL;
                    indirect_placeholder_1();
                    local_sp_1 = var_36;
                    storemerge = (uint64_t)(uint32_t)var_33;
                }
            }
        }
        rbx_0 = storemerge;
        if ((int)(uint32_t)rbp_0 < (int)0U) {
            *(uint64_t *)(local_sp_1 + (-8L)) = 4219526UL;
            indirect_placeholder_1();
            var_37 = (uint32_t *)rax_2;
            var_38 = *var_37;
            *(uint64_t *)(local_sp_1 + (-16L)) = 4219536UL;
            indirect_placeholder_1();
            *(uint64_t *)(local_sp_1 + (-24L)) = 4219541UL;
            indirect_placeholder_1();
            *var_37 = var_38;
        }
        return (uint64_t)(uint32_t)rbx_0;
    }
    var_18 = (uint64_t)*(uint32_t *)(rdi + 72UL);
    rax_3 = var_18;
    rax_4 = var_18;
    if ((var_18 & 4UL) != 0UL) {
        if (((uint64_t)((uint16_t)rax_4 & (unsigned short)512U) != 0UL) && ((int)var_10 > (int)4294967295U)) {
            *(uint64_t *)(var_0 + (-208L)) = 4219423UL;
            indirect_placeholder_1();
        }
        return (uint64_t)(uint32_t)rbx_0;
    }
    if ((int)var_10 > (int)4294967295U) {
        return;
    }
    if ((uint64_t)((uint16_t)var_18 & (unsigned short)512U) != 0UL) {
        var_27 = local_sp_4 + (-8L);
        *(uint64_t *)var_27 = 4219376UL;
        var_28 = indirect_placeholder_5(rdi, rcx);
        var_29 = (uint32_t)var_28;
        rax_3 = var_28;
        r14_0 = (uint64_t)var_29;
        rbx_0 = 4294967295UL;
        local_sp_3 = var_27;
        r15_0 = storemerge1;
        if ((int)var_29 > (int)4294967295U) {
            return (uint64_t)(uint32_t)rbx_0;
        }
    }
    var_19 = rdi + 96UL;
    var_20 = var_0 + (-208L);
    *(uint64_t *)var_20 = 4219355UL;
    var_21 = indirect_placeholder_4(var_19);
    local_sp_4 = var_20;
    if ((uint64_t)(unsigned char)var_21 != 0UL) {
        var_27 = local_sp_4 + (-8L);
        *(uint64_t *)var_27 = 4219376UL;
        var_28 = indirect_placeholder_5(rdi, rcx);
        var_29 = (uint32_t)var_28;
        rax_3 = var_28;
        r14_0 = (uint64_t)var_29;
        rbx_0 = 4294967295UL;
        local_sp_3 = var_27;
        r15_0 = storemerge1;
        if ((int)var_29 > (int)4294967295U) {
            return (uint64_t)(uint32_t)rbx_0;
        }
    }
    var_22 = var_0 + (-216L);
    *(uint64_t *)var_22 = 4219441UL;
    var_23 = indirect_placeholder_4(var_19);
    var_24 = (uint32_t)var_23;
    rax_3 = var_23;
    local_sp_3 = var_22;
    local_sp_4 = var_22;
    if ((int)var_24 >= (int)0U) {
        var_27 = local_sp_4 + (-8L);
        *(uint64_t *)var_27 = 4219376UL;
        var_28 = indirect_placeholder_5(rdi, rcx);
        var_29 = (uint32_t)var_28;
        rax_3 = var_28;
        r14_0 = (uint64_t)var_29;
        rbx_0 = 4294967295UL;
        local_sp_3 = var_27;
        r15_0 = storemerge1;
        if ((int)var_29 > (int)4294967295U) {
            return (uint64_t)(uint32_t)rbx_0;
        }
    }
    var_25 = (uint64_t)var_24;
    rbp_0 = var_25;
    r14_0 = var_25;
}
