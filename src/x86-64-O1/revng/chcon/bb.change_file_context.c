typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_13_ret_type;
struct indirect_placeholder_12_ret_type;
struct indirect_placeholder_14_ret_type;
struct indirect_placeholder_11_ret_type;
struct indirect_placeholder_9_ret_type;
struct indirect_placeholder_8_ret_type;
struct indirect_placeholder_13_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_12_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_14_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_11_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_9_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_8_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_4(uint64_t param_0);
extern struct indirect_placeholder_13_ret_type indirect_placeholder_13(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_12_ret_type indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_14_ret_type indirect_placeholder_14(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_11_ret_type indirect_placeholder_11(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_9_ret_type indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_8_ret_type indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
uint64_t bb_change_file_context(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t local_sp_3;
    struct indirect_placeholder_13_ret_type var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t rax_2;
    uint64_t var_25;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t rax_0;
    uint64_t local_sp_1;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t *var_21;
    uint64_t var_22;
    struct indirect_placeholder_14_ret_type var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_13;
    uint64_t local_sp_2;
    uint64_t rax_1;
    uint64_t var_34;
    struct indirect_placeholder_9_ret_type var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t rbx_0;
    bool var_26;
    uint64_t *var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    bool var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t var_12;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r12();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    var_4 = var_0 + (-40L);
    var_5 = (uint64_t)(uint32_t)rdi;
    var_6 = var_0 + (-32L);
    *(uint64_t *)var_6 = 0UL;
    var_7 = *(uint64_t *)4301792UL;
    rax_2 = 1UL;
    local_sp_3 = var_4;
    rbx_0 = var_7;
    if (var_7 == 0UL) {
        var_26 = (*(unsigned char *)4301810UL == '\x00');
        var_27 = (uint64_t *)(local_sp_3 + (-8L));
        if (var_26) {
            *var_27 = 4205540UL;
            var_31 = indirect_placeholder_4(rbx_0);
            var_32 = local_sp_3 + (-16L);
            *(uint64_t *)var_32 = 4205554UL;
            var_33 = indirect_placeholder_10(var_31, var_5, rsi);
            local_sp_2 = var_32;
            rax_1 = var_33;
        } else {
            *var_27 = 4205271UL;
            var_28 = indirect_placeholder_4(rbx_0);
            var_29 = local_sp_3 + (-16L);
            *(uint64_t *)var_29 = 4205285UL;
            var_30 = indirect_placeholder_10(var_28, var_5, rsi);
            local_sp_2 = var_29;
            rax_1 = var_30;
        }
        rax_2 = rax_1;
        if ((uint64_t)(uint32_t)rax_1 != 0UL) {
            *(uint64_t *)(local_sp_2 + (-8L)) = 4205572UL;
            var_34 = indirect_placeholder_5(1UL, rbx_0);
            *(uint64_t *)(local_sp_2 + (-16L)) = 4205593UL;
            var_35 = indirect_placeholder_9(rsi, 0UL, 4UL);
            var_36 = var_35.field_0;
            var_37 = var_35.field_1;
            *(uint64_t *)(local_sp_2 + (-24L)) = 4205601UL;
            indirect_placeholder_1();
            var_38 = (uint64_t)*(uint32_t *)var_36;
            *(uint64_t *)(local_sp_2 + (-32L)) = 4205629UL;
            indirect_placeholder_8(0UL, 4272600UL, 0UL, var_38, var_36, var_37, var_34);
            rax_2 = 1UL;
        }
        return rax_2;
    }
    var_8 = (*(unsigned char *)4301810UL == '\x00');
    var_9 = var_0 + (-48L);
    var_10 = (uint64_t *)var_9;
    local_sp_1 = var_9;
    if (var_8) {
        *var_10 = 4205413UL;
        var_12 = indirect_placeholder_10(var_6, rdi, rsi);
        rax_0 = var_12;
    } else {
        *var_10 = 4205321UL;
        var_11 = indirect_placeholder_10(var_6, rdi, rsi);
        rax_0 = var_11;
    }
    if ((int)(uint32_t)rax_0 < (int)0U) {
        var_19 = *(uint64_t *)(local_sp_1 + 8UL);
        if (var_19 == 0UL) {
            var_20 = local_sp_1 + 4UL;
            var_21 = (uint64_t *)(local_sp_1 + (-8L));
            *var_21 = 4205349UL;
            var_22 = indirect_placeholder_5(var_19, var_20);
            if ((uint64_t)(uint32_t)var_22 == 0UL) {
                return rax_2;
            }
            var_23 = local_sp_1 + (-16L);
            *(uint64_t *)var_23 = 4205369UL;
            var_24 = indirect_placeholder_4(1UL);
            rax_2 = var_24;
            local_sp_3 = var_23;
            rbx_0 = var_24;
            var_25 = local_sp_1 + (-24L);
            *(uint64_t *)var_25 = 4205394UL;
            indirect_placeholder_1();
            local_sp_3 = var_25;
            if (*var_21 != 0UL & (uint64_t)(uint32_t)var_24 == 0UL) {
                return rax_2;
            }
        }
        *(uint64_t *)(local_sp_1 + (-8L)) = 4205494UL;
        var_39 = indirect_placeholder_14(4UL, rsi);
        var_40 = var_39.field_0;
        var_41 = var_39.field_1;
        var_42 = var_39.field_2;
        *(uint64_t *)(local_sp_1 + (-16L)) = 4205522UL;
        indirect_placeholder_11(0UL, 4272544UL, 0UL, 0UL, var_40, var_41, var_42);
        return rax_2;
    }
    var_13 = var_9 + (-8L);
    *(uint64_t *)var_13 = 4205420UL;
    indirect_placeholder_1();
    local_sp_1 = var_13;
    if (*(uint32_t *)rax_0 == 61U) {
        return;
    }
    *(uint64_t *)(var_9 + (-16L)) = 4205438UL;
    var_14 = indirect_placeholder_13(4UL, rsi);
    var_15 = var_14.field_0;
    var_16 = var_14.field_1;
    var_17 = var_14.field_2;
    *(uint64_t *)(var_9 + (-24L)) = 4205446UL;
    indirect_placeholder_1();
    var_18 = (uint64_t)*(uint32_t *)var_15;
    *(uint64_t *)(var_9 + (-32L)) = 4205471UL;
    indirect_placeholder_12(0UL, 4272504UL, 0UL, var_18, var_15, var_16, var_17);
    return rax_2;
}
