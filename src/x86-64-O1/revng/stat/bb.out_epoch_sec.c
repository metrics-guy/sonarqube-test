typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct helper_idivl_EAX_wrapper_ret_type;
struct type_5;
struct indirect_placeholder_61_ret_type;
struct helper_idivq_EAX_wrapper_ret_type;
struct helper_idivl_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint32_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint32_t field_5;
    uint64_t field_6;
    uint32_t field_7;
    uint64_t field_8;
    uint32_t field_9;
    uint32_t field_10;
    unsigned char field_11;
    uint32_t field_12;
};
struct type_5 {
};
struct indirect_placeholder_61_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct helper_idivq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint32_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint32_t field_5;
    uint64_t field_6;
    uint32_t field_7;
    uint64_t field_8;
    uint32_t field_9;
    uint32_t field_10;
    unsigned char field_11;
    uint32_t field_12;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern uint32_t init_state_0x8248(void);
extern struct helper_idivl_EAX_wrapper_ret_type helper_idivl_EAX_wrapper(struct type_5 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint64_t param_13, uint32_t param_14, uint64_t param_15, uint64_t param_16, uint64_t param_17, uint32_t param_18, uint32_t param_19);
extern struct indirect_placeholder_61_ret_type indirect_placeholder_61(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct helper_idivq_EAX_wrapper_ret_type helper_idivq_EAX_wrapper(struct type_5 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint64_t param_13, uint32_t param_14, uint64_t param_15, uint64_t param_16, uint64_t param_17, uint32_t param_18, uint32_t param_19);
void bb_out_epoch_sec(uint64_t rdi, uint64_t rsi, uint64_t rcx, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint32_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint32_t var_13;
    uint32_t var_14;
    uint64_t var_15;
    struct indirect_placeholder_61_ret_type var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t rbx_5;
    uint64_t *_pre_phi136;
    uint64_t local_sp_0;
    uint64_t rcx3_1;
    uint64_t rsi2_0;
    uint64_t r12_1;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint32_t var_35;
    uint64_t local_sp_3;
    uint64_t var_36;
    uint64_t rbx_2;
    uint64_t rsi2_1;
    uint64_t rdi1_1;
    uint64_t rdi1_0;
    uint64_t rbx_4;
    uint64_t rax_1;
    unsigned char var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t rdi1_3;
    uint64_t r84_1;
    uint64_t var_40;
    uint64_t rbx_3;
    uint64_t r15_1;
    uint64_t rdi1_4;
    uint64_t r12_0;
    uint64_t r84_2;
    uint64_t local_sp_2;
    uint64_t rbx_7;
    uint64_t rbx_6;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint32_t *var_30;
    uint32_t var_31;
    uint64_t var_25;
    uint64_t rdi1_5;
    uint64_t r12_2;
    uint64_t local_sp_4;
    uint64_t r15_2;
    uint64_t rdi1_6;
    uint64_t r12_3;
    uint64_t r84_3;
    uint64_t local_sp_5;
    uint64_t rcx3_0;
    uint64_t rax_2_in;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t r15_3;
    uint64_t rdi1_7;
    uint64_t r12_4;
    uint64_t r84_4;
    uint64_t local_sp_6;
    uint64_t var_44;
    uint64_t *var_45;
    uint64_t var_46;
    uint64_t var_48;
    uint32_t var_54;
    struct helper_idivq_EAX_wrapper_ret_type var_47;
    uint64_t var_49;
    uint32_t var_50;
    uint64_t var_51;
    uint32_t var_52;
    uint32_t var_53;
    uint64_t *_pre135;
    uint64_t var_56;
    uint32_t var_57;
    uint64_t var_58;
    struct helper_idivl_EAX_wrapper_ret_type var_55;
    uint32_t var_59;
    uint32_t var_60;
    uint32_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    struct helper_idivq_EAX_wrapper_ret_type var_64;
    uint64_t var_65;
    uint64_t *var_66;
    uint64_t var_69;
    uint64_t var_67;
    uint64_t var_68;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_cc_src2();
    var_8 = init_state_0x9018();
    var_9 = init_state_0x9010();
    var_10 = init_state_0x8408();
    var_11 = init_state_0x8328();
    var_12 = init_state_0x82d8();
    var_13 = init_state_0x9080();
    var_14 = init_state_0x8248();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    *(uint64_t *)(var_0 + (-72L)) = rcx;
    *(uint64_t *)(var_0 + (-80L)) = r8;
    var_15 = var_0 + (-96L);
    *(uint64_t *)var_15 = 4210562UL;
    var_16 = indirect_placeholder_61(rsi, rdi, 46UL);
    var_17 = var_16.field_0;
    var_18 = var_16.field_1;
    var_19 = var_16.field_3;
    var_20 = var_16.field_4;
    var_21 = var_16.field_5;
    rbx_5 = var_2;
    rcx3_1 = 1UL;
    rsi2_0 = 0UL;
    local_sp_3 = var_15;
    rbx_2 = 0UL;
    rsi2_1 = 1UL;
    rdi1_0 = rdi;
    rbx_4 = var_17;
    rax_1 = rdi;
    rdi1_3 = rdi;
    r84_1 = var_21;
    rbx_3 = var_17;
    r84_2 = var_21;
    rbx_7 = var_17;
    rdi1_5 = var_18;
    r12_2 = rsi;
    local_sp_4 = var_15;
    r12_3 = 0UL;
    r84_3 = var_21;
    rcx3_0 = 1UL;
    rdi1_7 = var_18;
    r12_4 = 9UL;
    r84_4 = var_21;
    local_sp_6 = var_15;
    if (var_17 == 0UL) {
        *(uint32_t *)(local_sp_4 + 28UL) = 0U;
        rbx_6 = rbx_5;
        r15_2 = r12_2;
        rdi1_6 = rdi1_5;
        local_sp_5 = local_sp_4;
        rbx_7 = rbx_6;
        rax_2_in = r12_3;
        r15_3 = r15_2;
        rdi1_7 = rdi1_6;
        r12_4 = r12_3;
        r84_4 = r84_3;
        local_sp_6 = local_sp_5;
        var_41 = (uint64_t)(uint32_t)rax_2_in;
        var_42 = (uint64_t)(((uint32_t)rcx3_0 * 10U) & (-2));
        var_43 = var_41 + 1UL;
        rcx3_0 = var_42;
        rax_2_in = var_43;
        rcx3_1 = var_42;
        do {
            var_41 = (uint64_t)(uint32_t)rax_2_in;
            var_42 = (uint64_t)(((uint32_t)rcx3_0 * 10U) & (-2));
            var_43 = var_41 + 1UL;
            rcx3_0 = var_42;
            rax_2_in = var_43;
            rcx3_1 = var_42;
        } while ((int)(uint32_t)var_43 <= (int)8U);
    } else {
        var_22 = var_17 - rdi;
        *(unsigned char *)(rsi + rdi) = (unsigned char)'\x00';
        var_23 = var_17 + 1UL;
        var_24 = (uint64_t)((uint32_t)(uint64_t)*(unsigned char *)var_23 + (-48));
        rbx_5 = var_17;
        r12_1 = var_24;
        r15_1 = var_22;
        rdi1_4 = var_23;
        r12_0 = var_24;
        rdi1_5 = var_23;
        r12_2 = var_22;
        r15_3 = var_22;
        if (var_24 > 9UL) {
            r12_1 = 9UL;
            if ((uint64_t)(((uint32_t)(uint64_t)*(unsigned char *)(var_17 + (-1L)) + (-48)) & (-2)) > 9UL) {
                *(uint32_t *)(var_0 + (-68L)) = 0U;
                var_44 = (uint64_t)((long)(rcx3_1 << 32UL) >> (long)32UL);
                var_45 = (uint64_t *)(local_sp_6 + 8UL);
                var_46 = *var_45;
                var_47 = helper_idivq_EAX_wrapper((struct type_5 *)(0UL), var_44, 4210878UL, var_46, (uint64_t)((long)var_46 >> (long)63UL), rbx_7, rdi1_7, rdi, var_44, rcx3_1, var_19, var_20, r84_4, var_8, var_9, var_10, var_11, var_12, var_13, var_14);
                var_48 = var_47.field_2;
                var_49 = var_47.field_6;
                var_50 = var_47.field_7;
                var_51 = var_47.field_8;
                var_52 = var_47.field_9;
                var_53 = var_47.field_10;
                var_54 = (uint32_t)var_48;
                if ((r8 != 0UL) && ((long)rcx < (long)0UL)) {
                    var_55 = helper_idivl_EAX_wrapper((struct type_5 *)(0UL), rcx3_1, 4211144UL, 1000000000UL, 0UL, (uint64_t)var_54, var_48, rdi, var_44, rcx3_1, var_19, var_20, r84_4, var_49, var_50, var_10, var_11, var_51, var_52, var_53);
                    var_56 = var_55.field_6;
                    var_57 = var_55.field_7;
                    var_58 = var_55.field_8;
                    var_59 = var_55.field_9;
                    var_60 = var_55.field_10;
                    var_61 = (uint32_t)var_55.field_2 - var_54;
                    var_62 = (uint64_t)var_61;
                    var_63 = *var_45;
                    var_64 = helper_idivq_EAX_wrapper((struct type_5 *)(0UL), var_44, 4211157UL, var_63, (uint64_t)((long)var_63 >> (long)63UL), var_62, var_48, rdi, var_44, rcx3_1, var_19, var_20, r84_4, var_56, var_57, var_10, var_11, var_58, var_59, var_60);
                    var_65 = ((uint64_t)(var_61 + (uint32_t)(var_64.field_3 != 0UL)) != 0UL) + rcx;
                    var_66 = (uint64_t *)(local_sp_6 + 16UL);
                    *var_66 = var_65;
                    _pre_phi136 = var_66;
                    if (var_65 == 0UL) {
                        var_69 = local_sp_6 + (-8L);
                        *(uint64_t *)var_69 = 4211202UL;
                        indirect_placeholder_1(rdi, r15_3);
                        local_sp_0 = var_69;
                    } else {
                        var_67 = *_pre_phi136;
                        var_68 = local_sp_6 + (-8L);
                        *(uint64_t *)var_68 = 4210916UL;
                        indirect_placeholder_3(var_67, rdi, r15_3);
                        local_sp_0 = var_68;
                    }
                } else {
                    _pre135 = (uint64_t *)(local_sp_6 + 16UL);
                    _pre_phi136 = _pre135;
                    var_67 = *_pre_phi136;
                    var_68 = local_sp_6 + (-8L);
                    *(uint64_t *)var_68 = 4210916UL;
                    indirect_placeholder_3(var_67, rdi, r15_3);
                    local_sp_0 = var_68;
                }
                if ((uint64_t)(uint32_t)r12_4 != 0UL) {
                    *(uint64_t *)(local_sp_0 + (-16L)) = 0UL;
                    *(uint64_t *)(local_sp_0 + (-24L)) = 4211028UL;
                    indirect_placeholder();
                }
                return;
            }
            *(unsigned char *)var_17 = (unsigned char)'\x00';
            r12_0 = r12_1;
            var_26 = rbx_4 + (-1L);
            var_27 = (uint64_t)((uint32_t)(uint64_t)*(unsigned char *)(rbx_4 + (-2L)) + (-48));
            rbx_3 = var_26;
            rdi1_4 = var_26;
            rbx_4 = var_26;
            do {
                var_26 = rbx_4 + (-1L);
                var_27 = (uint64_t)((uint32_t)(uint64_t)*(unsigned char *)(rbx_4 + (-2L)) + (-48));
                rbx_3 = var_26;
                rdi1_4 = var_26;
                rbx_4 = var_26;
            } while (var_27 <= 9UL);
            var_28 = local_sp_3 + (-8L);
            *(uint64_t *)var_28 = 4210661UL;
            indirect_placeholder();
            var_29 = (var_27 < 2147483647UL) ? var_27 : 2147483647UL;
            var_30 = (uint32_t *)(local_sp_3 + 20UL);
            var_31 = (uint32_t)var_29;
            *var_30 = var_31;
            local_sp_2 = var_28;
            var_32 = var_26 + (*(unsigned char *)var_26 == '0');
            var_33 = var_32 - rdi;
            var_34 = *(uint64_t *)4326672UL;
            rbx_3 = var_32;
            r15_1 = var_33;
            var_35 = var_31 - (uint32_t)var_34;
            if (var_31 <= 1U & var_29 <= var_34 & (int)var_35 <= (int)1U & (int)(var_35 - (uint32_t)r12_1) <= (int)1U) {
                var_36 = helper_cc_compute_c_wrapper(rdi - var_32, var_32, var_7, 17U);
                r84_2 = 1UL;
                if (var_36 == 0UL) {
                    var_40 = local_sp_3 + (-16L);
                    *(uint64_t *)var_40 = 4211123UL;
                    indirect_placeholder();
                    rbx_3 = rbx_2;
                    r15_1 = rbx_2;
                    rdi1_4 = rdi1_3;
                    r84_2 = r84_1;
                    local_sp_2 = var_40;
                } else {
                    var_38 = rax_1 + 1UL;
                    rsi2_0 = rsi2_1;
                    rdi1_0 = rdi1_1;
                    rax_1 = var_38;
                    rdi1_3 = rdi1_1;
                    rdi1_4 = rdi1_1;
                    do {
                        var_37 = *(unsigned char *)rax_1;
                        rdi1_1 = rdi1_0;
                        r84_1 = 1UL;
                        if (var_37 == '-') {
                            *(unsigned char *)rdi1_0 = var_37;
                            rdi1_1 = rdi1_0 + 1UL;
                            rsi2_1 = rsi2_0;
                        }
                        var_38 = rax_1 + 1UL;
                        rsi2_0 = rsi2_1;
                        rdi1_0 = rdi1_1;
                        rax_1 = var_38;
                        rdi1_3 = rdi1_1;
                        rdi1_4 = rdi1_1;
                    } while (var_32 != var_38);
                    var_39 = rdi1_1 - rdi;
                    rbx_2 = var_39;
                    rbx_3 = var_39;
                    r15_1 = var_39;
                    if ((uint64_t)(unsigned char)rsi2_1 == 0UL) {
                        var_40 = local_sp_3 + (-16L);
                        *(uint64_t *)var_40 = 4211123UL;
                        indirect_placeholder();
                        rbx_3 = rbx_2;
                        r15_1 = rbx_2;
                        rdi1_4 = rdi1_3;
                        r84_2 = r84_1;
                        local_sp_2 = var_40;
                    }
                }
            }
        } else {
            var_25 = var_0 + (-104L);
            *(uint64_t *)var_25 = 4210787UL;
            indirect_placeholder();
            local_sp_2 = var_25;
            local_sp_3 = var_25;
            local_sp_4 = var_25;
            if (var_24 != 0UL) {
                *(uint32_t *)(local_sp_4 + 28UL) = 0U;
                rbx_6 = rbx_5;
                r15_2 = r12_2;
                rdi1_6 = rdi1_5;
                local_sp_5 = local_sp_4;
                rbx_7 = rbx_6;
                rax_2_in = r12_3;
                r15_3 = r15_2;
                rdi1_7 = rdi1_6;
                r12_4 = r12_3;
                r84_4 = r84_3;
                local_sp_6 = local_sp_5;
                var_41 = (uint64_t)(uint32_t)rax_2_in;
                var_42 = (uint64_t)(((uint32_t)rcx3_0 * 10U) & (-2));
                var_43 = var_41 + 1UL;
                rcx3_0 = var_42;
                rax_2_in = var_43;
                rcx3_1 = var_42;
                do {
                    var_41 = (uint64_t)(uint32_t)rax_2_in;
                    var_42 = (uint64_t)(((uint32_t)rcx3_0 * 10U) & (-2));
                    var_43 = var_41 + 1UL;
                    rcx3_0 = var_42;
                    rax_2_in = var_43;
                    rcx3_1 = var_42;
                } while ((int)(uint32_t)var_43 <= (int)8U);
                var_44 = (uint64_t)((long)(rcx3_1 << 32UL) >> (long)32UL);
                var_45 = (uint64_t *)(local_sp_6 + 8UL);
                var_46 = *var_45;
                var_47 = helper_idivq_EAX_wrapper((struct type_5 *)(0UL), var_44, 4210878UL, var_46, (uint64_t)((long)var_46 >> (long)63UL), rbx_7, rdi1_7, rdi, var_44, rcx3_1, var_19, var_20, r84_4, var_8, var_9, var_10, var_11, var_12, var_13, var_14);
                var_48 = var_47.field_2;
                var_49 = var_47.field_6;
                var_50 = var_47.field_7;
                var_51 = var_47.field_8;
                var_52 = var_47.field_9;
                var_53 = var_47.field_10;
                var_54 = (uint32_t)var_48;
                if ((r8 != 0UL) && ((long)rcx < (long)0UL)) {
                    var_55 = helper_idivl_EAX_wrapper((struct type_5 *)(0UL), rcx3_1, 4211144UL, 1000000000UL, 0UL, (uint64_t)var_54, var_48, rdi, var_44, rcx3_1, var_19, var_20, r84_4, var_49, var_50, var_10, var_11, var_51, var_52, var_53);
                    var_56 = var_55.field_6;
                    var_57 = var_55.field_7;
                    var_58 = var_55.field_8;
                    var_59 = var_55.field_9;
                    var_60 = var_55.field_10;
                    var_61 = (uint32_t)var_55.field_2 - var_54;
                    var_62 = (uint64_t)var_61;
                    var_63 = *var_45;
                    var_64 = helper_idivq_EAX_wrapper((struct type_5 *)(0UL), var_44, 4211157UL, var_63, (uint64_t)((long)var_63 >> (long)63UL), var_62, var_48, rdi, var_44, rcx3_1, var_19, var_20, r84_4, var_56, var_57, var_10, var_11, var_58, var_59, var_60);
                    var_65 = ((uint64_t)(var_61 + (uint32_t)(var_64.field_3 != 0UL)) != 0UL) + rcx;
                    var_66 = (uint64_t *)(local_sp_6 + 16UL);
                    *var_66 = var_65;
                    _pre_phi136 = var_66;
                    if (var_65 == 0UL) {
                        var_69 = local_sp_6 + (-8L);
                        *(uint64_t *)var_69 = 4211202UL;
                        indirect_placeholder_1(rdi, r15_3);
                        local_sp_0 = var_69;
                    } else {
                        var_67 = *_pre_phi136;
                        var_68 = local_sp_6 + (-8L);
                        *(uint64_t *)var_68 = 4210916UL;
                        indirect_placeholder_3(var_67, rdi, r15_3);
                        local_sp_0 = var_68;
                    }
                } else {
                    _pre135 = (uint64_t *)(local_sp_6 + 16UL);
                    _pre_phi136 = _pre135;
                    var_67 = *_pre_phi136;
                    var_68 = local_sp_6 + (-8L);
                    *(uint64_t *)var_68 = 4210916UL;
                    indirect_placeholder_3(var_67, rdi, r15_3);
                    local_sp_0 = var_68;
                }
                if ((uint64_t)(uint32_t)r12_4 != 0UL) {
                    *(uint64_t *)(local_sp_0 + (-16L)) = 0UL;
                    *(uint64_t *)(local_sp_0 + (-24L)) = 4211028UL;
                    indirect_placeholder();
                }
                return;
            }
            if ((uint64_t)(((uint32_t)(uint64_t)*(unsigned char *)(var_17 + (-1L)) + (-48)) & (-2)) > 9UL) {
                *(uint32_t *)(var_0 + (-76L)) = 0U;
            } else {
                *(unsigned char *)var_17 = (unsigned char)'\x00';
                r12_0 = r12_1;
                var_26 = rbx_4 + (-1L);
                var_27 = (uint64_t)((uint32_t)(uint64_t)*(unsigned char *)(rbx_4 + (-2L)) + (-48));
                rbx_3 = var_26;
                rdi1_4 = var_26;
                rbx_4 = var_26;
                do {
                    var_26 = rbx_4 + (-1L);
                    var_27 = (uint64_t)((uint32_t)(uint64_t)*(unsigned char *)(rbx_4 + (-2L)) + (-48));
                    rbx_3 = var_26;
                    rdi1_4 = var_26;
                    rbx_4 = var_26;
                } while (var_27 <= 9UL);
                var_28 = local_sp_3 + (-8L);
                *(uint64_t *)var_28 = 4210661UL;
                indirect_placeholder();
                var_29 = (var_27 < 2147483647UL) ? var_27 : 2147483647UL;
                var_30 = (uint32_t *)(local_sp_3 + 20UL);
                var_31 = (uint32_t)var_29;
                *var_30 = var_31;
                local_sp_2 = var_28;
                var_32 = var_26 + (*(unsigned char *)var_26 == '0');
                var_33 = var_32 - rdi;
                var_34 = *(uint64_t *)4326672UL;
                rbx_3 = var_32;
                r15_1 = var_33;
                var_35 = var_31 - (uint32_t)var_34;
                if (var_31 <= 1U & var_29 <= var_34 & (int)var_35 <= (int)1U & (int)(var_35 - (uint32_t)r12_1) <= (int)1U) {
                    var_36 = helper_cc_compute_c_wrapper(rdi - var_32, var_32, var_7, 17U);
                    r84_2 = 1UL;
                    if (var_36 == 0UL) {
                        var_40 = local_sp_3 + (-16L);
                        *(uint64_t *)var_40 = 4211123UL;
                        indirect_placeholder();
                        rbx_3 = rbx_2;
                        r15_1 = rbx_2;
                        rdi1_4 = rdi1_3;
                        r84_2 = r84_1;
                        local_sp_2 = var_40;
                    } else {
                        var_38 = rax_1 + 1UL;
                        rsi2_0 = rsi2_1;
                        rdi1_0 = rdi1_1;
                        rax_1 = var_38;
                        rdi1_3 = rdi1_1;
                        rdi1_4 = rdi1_1;
                        do {
                            var_37 = *(unsigned char *)rax_1;
                            rdi1_1 = rdi1_0;
                            r84_1 = 1UL;
                            if (var_37 == '-') {
                                *(unsigned char *)rdi1_0 = var_37;
                                rdi1_1 = rdi1_0 + 1UL;
                                rsi2_1 = rsi2_0;
                            }
                            var_38 = rax_1 + 1UL;
                            rsi2_0 = rsi2_1;
                            rdi1_0 = rdi1_1;
                            rax_1 = var_38;
                            rdi1_3 = rdi1_1;
                            rdi1_4 = rdi1_1;
                        } while (var_32 != var_38);
                        var_39 = rdi1_1 - rdi;
                        rbx_2 = var_39;
                        rbx_3 = var_39;
                        r15_1 = var_39;
                        if ((uint64_t)(unsigned char)rsi2_1 == 0UL) {
                            var_40 = local_sp_3 + (-16L);
                            *(uint64_t *)var_40 = 4211123UL;
                            indirect_placeholder();
                            rbx_3 = rbx_2;
                            r15_1 = rbx_2;
                            rdi1_4 = rdi1_3;
                            r84_2 = r84_1;
                            local_sp_2 = var_40;
                        }
                    }
                }
            }
        }
        rbx_7 = rbx_3;
        rbx_6 = rbx_3;
        r15_2 = r15_1;
        rdi1_6 = rdi1_4;
        r12_3 = r12_0;
        r84_3 = r84_2;
        local_sp_5 = local_sp_2;
        r15_3 = r15_1;
        rdi1_7 = rdi1_4;
        r12_4 = r12_0;
        r84_4 = r84_2;
        local_sp_6 = local_sp_2;
        if ((int)(uint32_t)r12_0 <= (int)8U) {
            rbx_7 = rbx_6;
            rax_2_in = r12_3;
            r15_3 = r15_2;
            rdi1_7 = rdi1_6;
            r12_4 = r12_3;
            r84_4 = r84_3;
            local_sp_6 = local_sp_5;
            var_41 = (uint64_t)(uint32_t)rax_2_in;
            var_42 = (uint64_t)(((uint32_t)rcx3_0 * 10U) & (-2));
            var_43 = var_41 + 1UL;
            rcx3_0 = var_42;
            rax_2_in = var_43;
            rcx3_1 = var_42;
            do {
                var_41 = (uint64_t)(uint32_t)rax_2_in;
                var_42 = (uint64_t)(((uint32_t)rcx3_0 * 10U) & (-2));
                var_43 = var_41 + 1UL;
                rcx3_0 = var_42;
                rax_2_in = var_43;
                rcx3_1 = var_42;
            } while ((int)(uint32_t)var_43 <= (int)8U);
        }
    }
    var_44 = (uint64_t)((long)(rcx3_1 << 32UL) >> (long)32UL);
    var_45 = (uint64_t *)(local_sp_6 + 8UL);
    var_46 = *var_45;
    var_47 = helper_idivq_EAX_wrapper((struct type_5 *)(0UL), var_44, 4210878UL, var_46, (uint64_t)((long)var_46 >> (long)63UL), rbx_7, rdi1_7, rdi, var_44, rcx3_1, var_19, var_20, r84_4, var_8, var_9, var_10, var_11, var_12, var_13, var_14);
    var_48 = var_47.field_2;
    var_49 = var_47.field_6;
    var_50 = var_47.field_7;
    var_51 = var_47.field_8;
    var_52 = var_47.field_9;
    var_53 = var_47.field_10;
    var_54 = (uint32_t)var_48;
    if ((r8 != 0UL) && ((long)rcx < (long)0UL)) {
        var_55 = helper_idivl_EAX_wrapper((struct type_5 *)(0UL), rcx3_1, 4211144UL, 1000000000UL, 0UL, (uint64_t)var_54, var_48, rdi, var_44, rcx3_1, var_19, var_20, r84_4, var_49, var_50, var_10, var_11, var_51, var_52, var_53);
        var_56 = var_55.field_6;
        var_57 = var_55.field_7;
        var_58 = var_55.field_8;
        var_59 = var_55.field_9;
        var_60 = var_55.field_10;
        var_61 = (uint32_t)var_55.field_2 - var_54;
        var_62 = (uint64_t)var_61;
        var_63 = *var_45;
        var_64 = helper_idivq_EAX_wrapper((struct type_5 *)(0UL), var_44, 4211157UL, var_63, (uint64_t)((long)var_63 >> (long)63UL), var_62, var_48, rdi, var_44, rcx3_1, var_19, var_20, r84_4, var_56, var_57, var_10, var_11, var_58, var_59, var_60);
        var_65 = ((uint64_t)(var_61 + (uint32_t)(var_64.field_3 != 0UL)) != 0UL) + rcx;
        var_66 = (uint64_t *)(local_sp_6 + 16UL);
        *var_66 = var_65;
        _pre_phi136 = var_66;
        if (var_65 == 0UL) {
            var_69 = local_sp_6 + (-8L);
            *(uint64_t *)var_69 = 4211202UL;
            indirect_placeholder_1(rdi, r15_3);
            local_sp_0 = var_69;
        } else {
            var_67 = *_pre_phi136;
            var_68 = local_sp_6 + (-8L);
            *(uint64_t *)var_68 = 4210916UL;
            indirect_placeholder_3(var_67, rdi, r15_3);
            local_sp_0 = var_68;
        }
    } else {
        _pre135 = (uint64_t *)(local_sp_6 + 16UL);
        _pre_phi136 = _pre135;
        var_67 = *_pre_phi136;
        var_68 = local_sp_6 + (-8L);
        *(uint64_t *)var_68 = 4210916UL;
        indirect_placeholder_3(var_67, rdi, r15_3);
        local_sp_0 = var_68;
    }
    if ((uint64_t)(uint32_t)r12_4 == 0UL) {
        return;
    }
    *(uint64_t *)(local_sp_0 + (-16L)) = 0UL;
    *(uint64_t *)(local_sp_0 + (-24L)) = 4211028UL;
    indirect_placeholder();
    return;
}
