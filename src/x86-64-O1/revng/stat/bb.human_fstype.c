typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_cc_src2(void);
extern void indirect_placeholder(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
void bb_human_fstype(uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_54;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_53;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_41;
    uint64_t var_42;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_cc_src2();
    var_2 = *(uint64_t *)rdi;
    var_3 = var_2 + (-732765674L);
    if (var_3 == 0UL) {
        return;
    }
    var_4 = helper_cc_compute_all_wrapper(var_3, 732765674UL, var_1, 17U);
    if ((var_4 & 65UL) == 0UL) {
        var_35 = var_2 + (-1702057286L);
        if (var_35 == 0UL) {
            return;
        }
        var_36 = helper_cc_compute_all_wrapper(var_35, 1702057286UL, var_1, 17U);
        if ((var_36 & 65UL) != 0UL) {
            var_37 = var_2 + (-1410924800L);
            if (var_37 == 0UL) {
                return;
            }
            var_38 = helper_cc_compute_all_wrapper(var_37, 1410924800UL, var_1, 17U);
            if ((var_38 & 65UL) != 0UL) {
                var_39 = var_2 + (-1346981957L);
                if (var_39 == 0UL) {
                    return;
                }
                var_40 = helper_cc_compute_all_wrapper(var_39, 1346981957UL, var_1, 17U);
                if ((var_40 & 65UL) != 0UL) {
                    var_41 = var_2 + (-1112100429L);
                    if (var_41 == 0UL) {
                        return;
                    }
                    var_42 = helper_cc_compute_all_wrapper(var_41, 1112100429UL, var_1, 17U);
                    if ((var_42 & 65UL) != 0UL) {
                        return;
                    }
                    return;
                }
                var_43 = var_2 + (-1397114950L);
                if (var_43 == 0UL) {
                    return;
                }
                var_44 = helper_cc_compute_all_wrapper(var_43, 1397114950UL, var_1, 17U);
                if ((var_44 & 65UL) != 0UL) {
                    return;
                }
                return;
            }
            var_45 = var_2 + (-1650746742L);
            if (var_45 == 0UL) {
                return;
            }
            var_46 = helper_cc_compute_all_wrapper(var_45, 1650746742UL, var_1, 17U);
            if ((var_46 & 65UL) != 0UL) {
                var_47 = var_2 + (-1513908720L);
                if (var_47 == 0UL) {
                    return;
                }
                var_48 = helper_cc_compute_all_wrapper(var_47, 1513908720UL, var_1, 17U);
                if ((var_48 & 65UL) != 0UL) {
                    return;
                }
                return;
            }
            var_49 = var_2 + (-1684170528L);
            if (var_49 == 0UL) {
                return;
            }
            var_50 = helper_cc_compute_all_wrapper(var_49, 1684170528UL, var_1, 17U);
            if ((var_50 & 65UL) != 0UL) {
                return;
            }
            return;
        }
        if (var_2 == 2435016766UL) {
            return;
        }
        if (var_2 <= 2435016766UL) {
            var_51 = var_2 + (-1936880249L);
            if (var_51 == 0UL) {
                return;
            }
            var_52 = helper_cc_compute_all_wrapper(var_51, 1936880249UL, var_1, 17U);
            if ((var_52 & 65UL) != 0UL) {
                var_53 = var_2 + (-1852207972L);
                if (var_53 == 0UL) {
                    return;
                }
                var_54 = helper_cc_compute_all_wrapper(var_53, 1852207972UL, var_1, 17U);
                if ((var_54 & 65UL) != 0UL) {
                    return;
                }
                return;
            }
            var_55 = var_2 + (-2035054128L);
            if (var_55 == 0UL) {
                return;
            }
            var_56 = helper_cc_compute_all_wrapper(var_55, 2035054128UL, var_1, 17U);
            if ((var_56 & 65UL) != 0UL) {
                return;
            }
            return;
        }
        if (var_2 == 3405662737UL) {
            return;
        }
        if (var_2 <= 3405662737UL) {
            if (var_2 == 2881100148UL) {
                return;
            }
            if (var_2 <= 2881100148UL) {
                return;
            }
            return;
        }
        if (var_2 == 4187351113UL) {
            return;
        }
        if (var_2 <= 4187351113UL) {
            return;
        }
        return;
    }
    var_5 = var_2 + (-44543L);
    if (var_5 == 0UL) {
        return;
    }
    var_6 = helper_cc_compute_all_wrapper(var_5, 44543UL, var_1, 17U);
    if ((var_6 & 65UL) == 0UL) {
        var_21 = var_2 + (-19920822L);
        if (var_21 == 0UL) {
            return;
        }
        var_22 = helper_cc_compute_all_wrapper(var_21, 19920822UL, var_1, 17U);
        if ((var_22 & 65UL) != 0UL) {
            var_23 = var_2 + (-12805120L);
            if (var_23 == 0UL) {
                return;
            }
            var_24 = helper_cc_compute_all_wrapper(var_23, 12805120UL, var_1, 17U);
            if ((var_24 & 65UL) == 0UL) {
                var_27 = var_2 + (-19911021L);
                if (var_27 == 0UL) {
                    return;
                }
                var_28 = helper_cc_compute_all_wrapper(var_27, 19911021UL, var_1, 17U);
                if ((var_28 & 65UL) != 0UL) {
                    return;
                }
                if ((var_2 + (-19920820L)) < 2UL) {
                    return;
                }
            }
            var_25 = var_2 + (-72020L);
            if (var_25 == 0UL) {
                return;
            }
            var_26 = helper_cc_compute_all_wrapper(var_25, 72020UL, var_1, 17U);
            if ((var_26 & 65UL) != 0UL) {
                return;
            }
            return;
        }
        var_29 = var_2 + (-325456742L);
        if (var_29 == 0UL) {
            return;
        }
        var_30 = helper_cc_compute_all_wrapper(var_29, 325456742UL, var_1, 17U);
        if ((var_30 & 65UL) != 0UL) {
            var_31 = var_2 + (-151263540L);
            if (var_31 == 0UL) {
                return;
            }
            var_32 = helper_cc_compute_all_wrapper(var_31, 151263540UL, var_1, 17U);
            if ((var_32 & 65UL) != 0UL) {
                return;
            }
            return;
        }
        var_33 = var_2 + (-464386766L);
        if (var_33 == 0UL) {
            return;
        }
        var_34 = helper_cc_compute_all_wrapper(var_33, 464386766UL, var_1, 17U);
        if ((var_34 & 65UL) != 0UL) {
            return;
        }
        return;
    }
    var_7 = var_2 + (-18475L);
    if (var_7 == 0UL) {
        return;
    }
    var_8 = helper_cc_compute_all_wrapper(var_7, 18475UL, var_1, 17U);
    if ((var_8 & 65UL) == 0UL) {
        var_15 = var_2 + (-29301L);
        if (var_15 == 0UL) {
            return;
        }
        var_16 = helper_cc_compute_all_wrapper(var_15, 29301UL, var_1, 17U);
        if ((var_16 & 65UL) != 0UL) {
            var_17 = var_2 + (-20859L);
            if (var_17 == 0UL) {
                return;
            }
            var_18 = helper_cc_compute_all_wrapper(var_17, 20859UL, var_1, 17U);
            if ((var_18 & 65UL) != 0UL) {
                return;
            }
            return;
        }
        var_19 = var_2 + (-40865L);
        if (var_19 == 0UL) {
            return;
        }
        var_20 = helper_cc_compute_all_wrapper(var_19, 40865UL, var_1, 17U);
        if ((var_20 & 65UL) != 0UL) {
            return;
        }
        return;
    }
    var_9 = var_2 + (-7377L);
    if (var_9 == 0UL) {
        return;
    }
    var_10 = helper_cc_compute_all_wrapper(var_9, 7377UL, var_1, 17U);
    if ((var_10 & 65UL) == 0UL) {
        var_13 = var_2 + (-16384L);
        if (var_13 == 0UL) {
            return;
        }
        var_14 = helper_cc_compute_all_wrapper(var_13, 16384UL, var_1, 17U);
        if ((var_14 & 65UL) != 0UL) {
            return;
        }
        return;
    }
    var_11 = var_2 + (-4979L);
    if (var_11 == 0UL) {
        return;
    }
    var_12 = helper_cc_compute_all_wrapper(var_11, 4979UL, var_1, 17U);
    if ((var_12 & 65UL) != 0UL) {
        return;
    }
    return;
    return;
}
