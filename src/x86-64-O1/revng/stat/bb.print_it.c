typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_46_ret_type;
struct indirect_placeholder_45_ret_type;
struct indirect_placeholder_47_ret_type;
struct indirect_placeholder_48_ret_type;
struct indirect_placeholder_49_ret_type;
struct indirect_placeholder_46_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_45_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_47_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_48_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_49_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint32_t init_state_0x82fc(void);
extern struct indirect_placeholder_46_ret_type indirect_placeholder_46(uint64_t param_0);
extern struct indirect_placeholder_45_ret_type indirect_placeholder_45(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_47_ret_type indirect_placeholder_47(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_48_ret_type indirect_placeholder_48(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_49_ret_type indirect_placeholder_49(uint64_t param_0);
typedef _Bool bool;
uint64_t bb_print_it(uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t rcx, uint64_t r8) {
    uint64_t rax_4;
    uint64_t var_0;
    uint32_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_52;
    uint64_t rcx4_1;
    uint64_t rax_1;
    uint64_t rbx_0;
    uint64_t r85_1;
    uint64_t r85_2;
    uint64_t var_53;
    uint64_t var_47;
    unsigned char var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t rax_6;
    uint64_t local_sp_4;
    uint64_t rbx_3;
    uint64_t local_sp_0;
    unsigned char rdi2_0_in;
    uint64_t rcx4_2;
    uint64_t rbx_4;
    uint64_t rax_2;
    uint64_t rbx_1;
    uint64_t r9_1;
    uint64_t r9_2;
    uint64_t rcx4_0;
    uint64_t r9_0;
    uint64_t r85_0;
    uint64_t var_74;
    unsigned char var_75;
    uint64_t local_sp_5;
    uint64_t local_sp_2;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    unsigned char *var_73;
    uint64_t rbx_2;
    uint64_t var_67;
    struct indirect_placeholder_46_ret_type var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    struct indirect_placeholder_45_ret_type var_34;
    uint64_t local_sp_3;
    uint64_t rax_3;
    unsigned char var_26;
    uint64_t local_sp_1;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_24;
    uint64_t var_25;
    unsigned char _pre;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t rbp_0;
    uint64_t var_19;
    uint64_t var_35;
    unsigned char var_36;
    uint64_t var_37;
    struct indirect_placeholder_47_ret_type var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t rax_5;
    uint64_t var_54;
    uint64_t var_55;
    struct indirect_placeholder_48_ret_type var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_60;
    unsigned char var_61;
    uint64_t var_62;
    unsigned char var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    unsigned char var_23;
    uint64_t rdi2_1;
    uint64_t rcx4_3;
    uint64_t rcx4_4;
    unsigned char var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    struct indirect_placeholder_49_ret_type var_13;
    uint64_t var_14;
    unsigned char var_15;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_state_0x82fc();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r15();
    var_5 = init_rbp();
    var_6 = init_r12();
    var_7 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_4;
    *(uint64_t *)(var_0 + (-16L)) = var_7;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_6;
    *(uint64_t *)(var_0 + (-40L)) = var_5;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    *(uint32_t *)(var_0 + (-84L)) = (uint32_t)rsi;
    *(uint64_t *)(var_0 + (-80L)) = rdx;
    *(uint64_t *)(var_0 + (-72L)) = rcx;
    *(uint64_t *)(var_0 + (-64L)) = r8;
    var_8 = (uint64_t)var_1;
    rax_4 = 0UL;
    rbp_0 = rdi;
    rdi2_1 = rdi;
    rcx4_3 = 18446744073709551615UL;
    rcx4_4 = 0UL;
    while (rcx4_3 != 0UL)
        {
            rdi2_1 = rdi2_1 + var_8;
        }
    var_11 = 1UL - rcx4_4;
    var_12 = var_0 + (-96L);
    *(uint64_t *)var_12 = 4208677UL;
    var_13 = indirect_placeholder_49(var_11);
    var_14 = var_13.field_0;
    var_15 = *(unsigned char *)rdi;
    local_sp_2 = var_12;
    rax_3 = var_14;
    rdi2_0_in = var_15;
    local_sp_5 = var_12;
    if (var_15 == '\x00') {
        *(unsigned char *)(var_0 + (-93L)) = (unsigned char)'\x00';
    } else {
        var_16 = var_13.field_3;
        var_17 = var_13.field_2;
        var_18 = var_13.field_1;
        *(unsigned char *)(var_0 + (-93L)) = (unsigned char)'\x00';
        rcx4_1 = var_18;
        r9_1 = var_17;
        r85_1 = var_16;
        while (1U)
            {
                r85_2 = r85_1;
                rcx4_2 = rcx4_1;
                rax_2 = rax_3;
                rbx_1 = rbp_0;
                r9_2 = r9_1;
                rcx4_0 = rcx4_1;
                r9_0 = r9_1;
                r85_0 = r85_1;
                local_sp_3 = local_sp_2;
                if ((uint64_t)(rdi2_0_in + '\xdb') == 0UL) {
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4208720UL;
                    indirect_placeholder();
                    var_20 = (rax_3 + rbp_0) + 1UL;
                    var_21 = local_sp_2 + (-16L);
                    *(uint64_t *)var_21 = 4208738UL;
                    indirect_placeholder();
                    var_22 = var_20 + rax_3;
                    var_23 = *(unsigned char *)var_22;
                    var_26 = var_23;
                    local_sp_1 = var_21;
                    rbx_3 = var_22;
                    if (var_23 == '.') {
                        var_24 = local_sp_2 + (-24L);
                        *(uint64_t *)var_24 = 4208836UL;
                        indirect_placeholder();
                        var_25 = (rax_3 + var_22) + 1UL;
                        _pre = *(unsigned char *)var_25;
                        var_26 = _pre;
                        local_sp_1 = var_24;
                        rbx_3 = var_25;
                    }
                    switch_state_var = 0;
                    switch (var_26) {
                      case '\x00':
                        {
                            rbx_2 = rbx_3 + (-1L);
                        }
                        break;
                      case '%':
                        {
                            rbx_1 = rbx_2;
                            if (var_27 != 0UL) {
                                var_67 = local_sp_1 + (-16L);
                                *(uint64_t *)var_67 = 4208862UL;
                                indirect_placeholder();
                                local_sp_0 = var_67;
                                var_74 = rbx_1 + 1UL;
                                var_75 = *(unsigned char *)var_74;
                                rcx4_1 = rcx4_0;
                                r85_1 = r85_0;
                                rdi2_0_in = var_75;
                                r9_1 = r9_0;
                                local_sp_5 = local_sp_0;
                                local_sp_2 = local_sp_0;
                                rax_3 = rax_2;
                                rbp_0 = var_74;
                                if (var_75 != '\x00') {
                                    continue;
                                }
                                switch_state_var = 1;
                                break;
                            }
                            *(unsigned char *)(var_28 + var_14) = var_26;
                            *(unsigned char *)((var_27 + var_14) + 2UL) = (unsigned char)'\x00';
                            *(uint64_t *)(local_sp_1 + (-16L)) = 4208927UL;
                            var_29 = indirect_placeholder_46(var_14);
                            var_30 = var_29.field_0;
                            var_31 = var_29.field_1;
                            var_32 = var_29.field_2;
                            var_33 = local_sp_1 + (-24L);
                            *(uint64_t *)var_33 = 4208955UL;
                            var_34 = indirect_placeholder_45(0UL, 4293433UL, 1UL, 0UL, var_30, var_31, var_32);
                            local_sp_3 = var_33;
                            rcx4_2 = var_34.field_0;
                            r9_2 = var_34.field_1;
                            r85_2 = var_34.field_2;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                } else {
                    rax_4 = rax_3;
                    if ((uint64_t)(rdi2_0_in + '\xa4') != 0UL) {
                        var_19 = local_sp_2 + (-8L);
                        *(uint64_t *)var_19 = 4208904UL;
                        indirect_placeholder();
                        local_sp_0 = var_19;
                        var_74 = rbx_1 + 1UL;
                        var_75 = *(unsigned char *)var_74;
                        rcx4_1 = rcx4_0;
                        r85_1 = r85_0;
                        rdi2_0_in = var_75;
                        r9_1 = r9_0;
                        local_sp_5 = local_sp_0;
                        local_sp_2 = local_sp_0;
                        rax_3 = rax_2;
                        rbp_0 = var_74;
                        if (var_75 != '\x00') {
                            continue;
                        }
                        break;
                    }
                    local_sp_4 = local_sp_3;
                    rax_2 = rax_4;
                    rcx4_0 = rcx4_2;
                    r9_0 = r9_2;
                    r85_0 = r85_2;
                    if (*(unsigned char *)4326688UL == '\x00') {
                        var_66 = local_sp_3 + (-8L);
                        *(uint64_t *)var_66 = 4209050UL;
                        indirect_placeholder();
                        local_sp_0 = var_66;
                    } else {
                        var_35 = rbp_0 + 1UL;
                        var_36 = *(unsigned char *)var_35;
                        var_37 = (uint64_t)((uint32_t)(uint64_t)var_36 + (-48));
                        rax_2 = 0UL;
                        rbx_1 = var_35;
                        rax_5 = var_37;
                        if ((uint64_t)((unsigned char)var_37 & '\xf8') == 0UL) {
                            var_60 = rbp_0 + 2UL;
                            var_61 = *(unsigned char *)var_60;
                            rax_6 = (uint64_t)var_61;
                            rbx_4 = var_60;
                            var_62 = rbp_0 + 3UL;
                            var_63 = *(unsigned char *)var_62;
                            rax_6 = (uint64_t)var_63;
                            rbx_4 = var_62;
                            if ((uint64_t)((var_61 + '\xd0') & '\xf8') != 0UL & (uint64_t)((var_63 + '\xd0') & '\xf8') == 0UL) {
                                rax_6 = (uint64_t)(uint32_t)(uint64_t)var_63;
                                rbx_4 = rbp_0 + 4UL;
                            }
                            var_64 = local_sp_3 + (-8L);
                            *(uint64_t *)var_64 = 4209120UL;
                            indirect_placeholder();
                            var_65 = rbx_4 + (-1L);
                            local_sp_0 = var_64;
                            rax_2 = rax_6;
                            rbx_1 = var_65;
                        } else {
                            if ((uint64_t)(var_36 + '\x88') == 0UL) {
                                var_43 = rbp_0 + 2UL;
                                var_44 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)var_43;
                                *(uint64_t *)(local_sp_3 + (-8L)) = 4209145UL;
                                var_45 = indirect_placeholder_2(var_44);
                                var_46 = local_sp_3 + (-16L);
                                *(uint64_t *)var_46 = 4209153UL;
                                indirect_placeholder();
                                rbx_0 = var_43;
                                local_sp_4 = var_46;
                                rax_5 = var_45;
                                if ((uint64_t)(uint32_t)var_45 != 0UL) {
                                    var_47 = rbp_0 + 3UL;
                                    var_48 = *(unsigned char *)var_47;
                                    var_49 = (uint64_t)var_48;
                                    var_50 = (uint64_t)(uint32_t)var_49;
                                    *(uint64_t *)(local_sp_3 + (-24L)) = 4209186UL;
                                    var_51 = indirect_placeholder_2(var_50);
                                    *(uint64_t *)(local_sp_3 + (-32L)) = 4209194UL;
                                    indirect_placeholder();
                                    rax_1 = var_51;
                                    if ((uint64_t)(uint32_t)var_51 == 0UL) {
                                        var_52 = (uint64_t)var_48 + 4294967199UL;
                                        rax_1 = (uint64_t)(uint32_t)(((uint64_t)((unsigned char)var_52 & '\xfe') > 5UL) ? (var_49 + 4294967241UL) : var_52);
                                        rbx_0 = var_47;
                                    }
                                    var_53 = local_sp_3 + (-40L);
                                    *(uint64_t *)var_53 = 4209234UL;
                                    indirect_placeholder();
                                    local_sp_0 = var_53;
                                    rax_2 = rax_1;
                                    rbx_1 = rbx_0;
                                    var_74 = rbx_1 + 1UL;
                                    var_75 = *(unsigned char *)var_74;
                                    rcx4_1 = rcx4_0;
                                    r85_1 = r85_0;
                                    rdi2_0_in = var_75;
                                    r9_1 = r9_0;
                                    local_sp_5 = local_sp_0;
                                    local_sp_2 = local_sp_0;
                                    rax_3 = rax_2;
                                    rbp_0 = var_74;
                                    if (var_75 != '\x00') {
                                        continue;
                                    }
                                    break;
                                }
                            }
                            rbx_1 = rbp_0;
                            if (var_36 != '\x00') {
                                *(uint64_t *)(local_sp_3 + (-8L)) = 4209022UL;
                                var_38 = indirect_placeholder_47(0UL, 4289368UL, 0UL, 0UL, rcx4_2, r9_2, r85_2);
                                var_39 = var_38.field_0;
                                var_40 = var_38.field_1;
                                var_41 = var_38.field_2;
                                var_42 = local_sp_3 + (-16L);
                                *(uint64_t *)var_42 = 4209032UL;
                                indirect_placeholder();
                                local_sp_0 = var_42;
                                rcx4_0 = var_39;
                                r9_0 = var_40;
                                r85_0 = var_41;
                                var_74 = rbx_1 + 1UL;
                                var_75 = *(unsigned char *)var_74;
                                rcx4_1 = rcx4_0;
                                r85_1 = r85_0;
                                rdi2_0_in = var_75;
                                r9_1 = r9_0;
                                local_sp_5 = local_sp_0;
                                local_sp_2 = local_sp_0;
                                rax_3 = rax_2;
                                rbp_0 = var_74;
                                if (var_75 != '\x00') {
                                    continue;
                                }
                                break;
                            }
                            var_54 = (uint64_t)(uint32_t)(uint64_t)var_36;
                            var_55 = local_sp_4 + (-8L);
                            *(uint64_t *)var_55 = 4209296UL;
                            var_56 = indirect_placeholder_48(var_54, r9_2, r85_2);
                            var_57 = var_56.field_0;
                            var_58 = var_56.field_1;
                            var_59 = var_56.field_2;
                            local_sp_0 = var_55;
                            rax_2 = rax_5;
                            rcx4_0 = var_57;
                            r9_0 = var_58;
                            r85_0 = var_59;
                        }
                    }
                }
            }
    }
    *(uint64_t *)(local_sp_5 + (-8L)) = 4209314UL;
    indirect_placeholder();
    *(uint64_t *)(local_sp_5 + (-16L)) = 4209333UL;
    indirect_placeholder();
    return (uint64_t)*(unsigned char *)(local_sp_5 + (-13L));
}
