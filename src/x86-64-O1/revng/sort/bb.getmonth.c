typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_6(uint64_t param_0);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
typedef _Bool bool;
uint64_t bb_getmonth(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t r14_0;
    uint64_t var_30;
    unsigned char rbx_0_in;
    uint64_t var_31;
    uint64_t local_sp_1;
    uint64_t var_32;
    unsigned char var_33;
    uint64_t r13_1;
    uint64_t r13_0;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t r12_0;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t storemerge;
    uint64_t var_13;
    uint64_t local_sp_3;
    uint64_t local_sp_0;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    unsigned char var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t *var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t *var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t local_sp_2;
    uint64_t var_38;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t *var_11;
    uint64_t var_12;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_8 = var_0 + (-72L);
    *(uint64_t *)(var_0 + (-56L)) = rsi;
    var_13 = 0UL;
    storemerge = 0UL;
    r14_0 = rdi;
    local_sp_3 = var_8;
    var_9 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)r14_0;
    var_10 = local_sp_3 + (-8L);
    var_11 = (uint64_t *)var_10;
    *var_11 = 4210146UL;
    var_12 = indirect_placeholder_6(var_9);
    local_sp_0 = var_10;
    r13_0 = r14_0;
    r13_1 = r14_0;
    local_sp_3 = var_10;
    while (*(unsigned char *)((uint64_t)(unsigned char)var_12 | 4352768UL) != '\x00')
        {
            r14_0 = r14_0 + 1UL;
            var_9 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)r14_0;
            var_10 = local_sp_3 + (-8L);
            var_11 = (uint64_t *)var_10;
            *var_11 = 4210146UL;
            var_12 = indirect_placeholder_6(var_9);
            local_sp_0 = var_10;
            r13_0 = r14_0;
            r13_1 = r14_0;
            local_sp_3 = var_10;
        }
    *var_11 = 12UL;
    *(uint64_t *)local_sp_3 = 0UL;
    while (1U)
        {
            var_14 = (var_13 + *(uint64_t *)local_sp_0) >> 1UL;
            var_15 = var_14 << 4UL;
            var_16 = *(uint64_t *)(var_15 + 4351104UL);
            var_17 = *(unsigned char *)var_16;
            rbx_0_in = var_17;
            local_sp_1 = local_sp_0;
            r12_0 = var_16;
            var_36 = var_14;
            local_sp_2 = local_sp_0;
            if (var_17 != '\x00') {
                loop_state_var = 0U;
                break;
            }
            while (1U)
                {
                    var_18 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)r13_0;
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4210266UL;
                    var_19 = indirect_placeholder_6(var_18);
                    var_20 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)((uint64_t)(unsigned char)var_19 | 4352000UL);
                    var_21 = (uint64_t *)(local_sp_1 + (-16L));
                    *var_21 = 4210281UL;
                    var_22 = indirect_placeholder_6(var_20);
                    var_23 = (uint64_t)(uint32_t)var_22;
                    var_24 = (uint64_t)(uint32_t)(uint64_t)rbx_0_in;
                    var_25 = local_sp_1 + (-24L);
                    var_26 = (uint64_t *)var_25;
                    *var_26 = 4210291UL;
                    var_27 = indirect_placeholder_6(var_24);
                    var_28 = var_23 - var_27;
                    var_29 = helper_cc_compute_c_wrapper(var_28, var_27, var_7, 14U);
                    local_sp_0 = var_25;
                    local_sp_1 = var_25;
                    local_sp_2 = var_25;
                    if (var_29 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_30 = helper_cc_compute_all_wrapper(var_28, var_27, var_7, 14U);
                    if ((var_30 & 65UL) != 0UL) {
                        loop_state_var = 2U;
                        break;
                    }
                    var_31 = r13_0 + 1UL;
                    var_32 = r12_0 + 1UL;
                    var_33 = *(unsigned char *)var_32;
                    r13_0 = var_31;
                    rbx_0_in = var_33;
                    r12_0 = var_32;
                    r13_1 = var_31;
                    if (var_33 == '\x00') {
                        continue;
                    }
                    loop_state_var = 0U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 2U:
              case 1U:
                {
                    switch (loop_state_var) {
                      case 2U:
                        {
                            var_34 = var_14 + 1UL;
                            *var_21 = var_34;
                            var_35 = var_34;
                            var_36 = *var_26;
                        }
                        break;
                      case 1U:
                        {
                            *var_26 = var_14;
                            var_35 = *var_21;
                        }
                        break;
                    }
                    var_37 = helper_cc_compute_c_wrapper(var_35 - var_36, var_36, var_7, 17U);
                    if (var_37 != 0UL) {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    var_13 = *var_21;
                    continue;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 1U:
        {
            return storemerge;
        }
        break;
      case 0U:
        {
            var_38 = *(uint64_t *)(local_sp_2 + 16UL);
            if (var_38 != 0UL) {
                *(uint64_t *)var_38 = r13_1;
            }
            storemerge = (uint64_t)*(uint32_t *)(var_15 + 4351112UL);
        }
        break;
    }
}
