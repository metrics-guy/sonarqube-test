typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_64_ret_type;
struct indirect_placeholder_63_ret_type;
struct indirect_placeholder_64_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_63_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint32_t init_state_0x82fc(void);
extern void indirect_placeholder_11(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_64_ret_type indirect_placeholder_64(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_63_ret_type indirect_placeholder_63(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
uint64_t bb_stream_open(uint64_t rdi, uint64_t rsi) {
    uint64_t rbp_1;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint32_t var_5;
    uint64_t var_6;
    unsigned char var_7;
    uint64_t var_8;
    uint64_t var_22;
    uint64_t rbp_0;
    struct indirect_placeholder_64_ret_type var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t local_sp_0;
    uint64_t var_15;
    uint64_t var_19;
    uint64_t var_16;
    uint64_t cc_src_0;
    uint64_t cc_dst_0;
    uint64_t rdi1_0;
    uint64_t rsi2_0;
    uint64_t rcx_0;
    uint64_t cc_src_1;
    uint64_t cc_dst_1;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t local_sp_1;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r12();
    var_4 = init_cc_src2();
    var_5 = init_state_0x82fc();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_6 = var_0 + (-24L);
    *(uint64_t *)var_6 = var_1;
    var_7 = *(unsigned char *)rsi;
    var_8 = (uint64_t)var_7 + (-114L);
    rbp_1 = var_2;
    rbp_0 = 0UL;
    local_sp_0 = var_6;
    cc_src_0 = 114UL;
    cc_dst_0 = var_8;
    rdi1_0 = 4320782UL;
    rsi2_0 = rdi;
    rcx_0 = 2UL;
    local_sp_1 = var_6;
    if ((uint64_t)(unsigned char)var_8 == 0UL) {
        var_16 = (uint64_t)var_5;
        cc_src_1 = cc_src_0;
        cc_dst_1 = cc_dst_0;
        while (rcx_0 != 0UL)
            {
                var_17 = (uint64_t)*(unsigned char *)rdi1_0;
                var_18 = (uint64_t)*(unsigned char *)rsi2_0 - var_17;
                cc_src_0 = var_17;
                cc_dst_0 = var_18;
                cc_src_1 = var_17;
                cc_dst_1 = var_18;
                if ((uint64_t)(unsigned char)var_18 == 0UL) {
                    break;
                }
                rdi1_0 = rdi1_0 + var_16;
                rsi2_0 = rsi2_0 + var_16;
                rcx_0 = rcx_0 + (-1L);
                cc_src_1 = cc_src_0;
                cc_dst_1 = cc_dst_0;
            }
        var_19 = helper_cc_compute_all_wrapper(cc_dst_1, cc_src_1, var_4, 14U);
        var_20 = ((var_19 & 65UL) == 0UL);
        var_21 = helper_cc_compute_c_wrapper(cc_dst_1, var_19, var_4, 1U);
        if ((uint64_t)((unsigned char)var_20 - (unsigned char)var_21) == 0UL) {
            *(unsigned char *)4351960UL = (unsigned char)'\x01';
            rbp_0 = *(uint64_t *)4349512UL;
        } else {
            *(uint64_t *)(var_0 + (-32L)) = 4213348UL;
            indirect_placeholder();
            var_22 = var_0 + (-40L);
            *(uint64_t *)var_22 = 4213367UL;
            indirect_placeholder();
            local_sp_1 = var_22;
        }
        *(uint64_t *)(local_sp_1 + (-8L)) = 4213322UL;
        indirect_placeholder_11(rsi, rbp_0, 2UL);
        rbp_1 = rbp_0;
    } else {
        if ((uint64_t)(var_7 + '\x89') != 0UL) {
            if (rdi != 0UL) {
                var_15 = *(uint64_t *)4349504UL;
                rbp_1 = var_15;
                return rbp_1;
            }
            *(uint64_t *)(var_0 + (-32L)) = 4213255UL;
            indirect_placeholder();
            if (var_7 != '\x00') {
                var_15 = *(uint64_t *)4349504UL;
                rbp_1 = var_15;
                return rbp_1;
            }
            *(uint64_t *)(var_0 + (-40L)) = 4213390UL;
            var_9 = indirect_placeholder_64(rdi, 0UL, 3UL);
            var_10 = var_9.field_0;
            var_11 = var_9.field_1;
            var_12 = var_9.field_2;
            *(uint64_t *)(var_0 + (-48L)) = 4213398UL;
            indirect_placeholder();
            var_13 = (uint64_t)*(uint32_t *)var_10;
            var_14 = var_0 + (-56L);
            *(uint64_t *)var_14 = 4213423UL;
            indirect_placeholder_63(0UL, 4313363UL, 2UL, var_13, var_10, var_11, var_12);
            local_sp_0 = var_14;
        }
        *(uint64_t *)(local_sp_0 + (-8L)) = 4213448UL;
        indirect_placeholder();
    }
    return rbp_1;
}
