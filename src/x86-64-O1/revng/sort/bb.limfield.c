typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_limfield_ret_type;
struct bb_limfield_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_6(uint64_t param_0);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t init_rcx(void);
struct bb_limfield_ret_type bb_limfield(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint32_t var_15;
    bool var_16;
    uint64_t var_17;
    bool var_18;
    uint64_t local_sp_1;
    uint64_t var_26;
    uint64_t rbx_2;
    uint64_t rbx_3;
    uint64_t rbx_0;
    uint64_t local_sp_0;
    uint64_t var_30;
    uint64_t rbx_2_ph;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t local_sp_3;
    uint64_t var_35;
    uint64_t rbx_5;
    uint64_t rbx_4;
    uint64_t var_19;
    uint64_t r12_0_ph_in;
    uint64_t rbx_1_ph;
    uint64_t rcx_0_ph;
    struct bb_limfield_ret_type mrv1;
    uint64_t rbx_1;
    uint64_t var_20;
    uint64_t rbx_6;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t local_sp_1_ph;
    uint64_t r12_0_ph;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t local_sp_2;
    uint64_t rcx_1;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t rcx_2;
    struct bb_limfield_ret_type mrv;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_rbp();
    var_4 = init_r12();
    var_5 = init_r14();
    var_6 = init_rcx();
    var_7 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_5;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_3;
    var_8 = var_0 + (-40L);
    *(uint64_t *)var_8 = var_2;
    var_9 = *(uint64_t *)rdi;
    var_10 = (*(uint64_t *)(rdi + 8UL) + var_9) + (-1L);
    var_11 = *(uint64_t *)(rsi + 16UL);
    var_12 = *(uint64_t *)(rsi + 24UL);
    var_13 = helper_cc_compute_c_wrapper(var_12 + (-1L), 1UL, var_7, 17U);
    var_14 = var_11 + var_13;
    var_15 = *(uint32_t *)4351076UL;
    var_16 = ((uint64_t)(var_15 + (-128)) == 0UL);
    var_17 = helper_cc_compute_c_wrapper(var_9 - var_10, var_10, var_13, 17U);
    var_18 = (var_17 == 0UL);
    rbx_3 = var_9;
    rbx_2_ph = var_9;
    r12_0_ph_in = var_14;
    rbx_1_ph = var_9;
    local_sp_1_ph = var_8;
    local_sp_2 = var_8;
    rcx_1 = var_6;
    if (var_16) {
        if (var_18 || (var_14 == 0UL)) {
            while (1U)
                {
                    r12_0_ph = r12_0_ph_in + (-1L);
                    r12_0_ph_in = r12_0_ph;
                    rbx_2 = rbx_2_ph;
                    local_sp_1 = local_sp_1_ph;
                    while (1U)
                        {
                            var_23 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)rbx_2;
                            var_24 = local_sp_1 + (-8L);
                            *(uint64_t *)var_24 = 4209819UL;
                            var_25 = indirect_placeholder_6(var_23);
                            local_sp_1 = var_24;
                            rbx_3 = rbx_2;
                            rbx_0 = rbx_2;
                            local_sp_0 = var_24;
                            local_sp_2 = var_24;
                            if (*(unsigned char *)((uint64_t)(unsigned char)var_25 | 4352768UL) != '\x00') {
                                loop_state_var = 1U;
                                break;
                            }
                            var_26 = rbx_2 + 1UL;
                            rbx_2 = var_26;
                            rbx_3 = var_26;
                            if (var_10 > var_26) {
                                continue;
                            }
                            loop_state_var = 0U;
                            break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 0U:
                        {
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 1U:
                        {
                            if (var_10 > rbx_2) {
                                switch_state_var = 1;
                                break;
                            }
                            while (1U)
                                {
                                    var_27 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)rbx_0;
                                    var_28 = local_sp_0 + (-8L);
                                    *(uint64_t *)var_28 = 4209767UL;
                                    var_29 = indirect_placeholder_6(var_27);
                                    rbx_3 = rbx_0;
                                    local_sp_0 = var_28;
                                    rbx_2_ph = rbx_0;
                                    local_sp_1_ph = var_28;
                                    local_sp_2 = var_28;
                                    if (*(unsigned char *)((uint64_t)(unsigned char)var_29 | 4352768UL) != '\x00') {
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    var_30 = rbx_0 + 1UL;
                                    rbx_0 = var_30;
                                    rbx_3 = var_10;
                                    if (var_10 == var_30) {
                                        continue;
                                    }
                                    loop_state_var = 0U;
                                    break;
                                }
                            switch_state_var = 0;
                            switch (loop_state_var) {
                              case 0U:
                                {
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              case 1U:
                                {
                                    if (!((var_10 <= rbx_0) || (r12_0_ph == 0UL))) {
                                        continue;
                                    }
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                            }
                            if (switch_state_var)
                                break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
        }
    }
    var_19 = var_14 + (-1L);
    rcx_0_ph = var_19;
    rcx_1 = var_19;
    if (!(!var_18 & var_14 != 0UL)) {
        return;
    }
    while (1U)
        {
            rbx_1 = rbx_1_ph;
            rbx_3 = var_10;
            rcx_1 = rcx_0_ph;
            rcx_2 = rcx_0_ph;
            while (1U)
                {
                    rbx_6 = rbx_1;
                    if ((uint64_t)(var_15 - (uint32_t)(uint64_t)*(unsigned char *)rbx_1) != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_20 = rbx_1 + 1UL;
                    rbx_1 = var_20;
                    if (var_10 == var_20) {
                        continue;
                    }
                    loop_state_var = 1U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    rbx_3 = rbx_1;
                    if (var_10 <= rbx_1) {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    if ((var_12 | rcx_0_ph) != 0UL) {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    var_21 = rbx_1 + 1UL;
                    rbx_1_ph = var_21;
                    rbx_3 = var_21;
                    if (var_10 <= var_21) {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    var_22 = rcx_0_ph + (-1L);
                    rcx_0_ph = var_22;
                    rcx_1 = var_22;
                    if (rcx_0_ph == 0UL) {
                        continue;
                    }
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 1U:
                {
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            mrv.field_0 = rbx_6;
            mrv1 = mrv;
            mrv1.field_1 = rcx_2;
            return mrv1;
        }
        break;
      case 1U:
        {
            break;
        }
        break;
    }
}
