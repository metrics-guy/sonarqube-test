typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern void indirect_placeholder_11(uint64_t param_0, uint64_t param_1, uint64_t param_2);
void bb_mergelines_node(uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t rcx) {
    uint64_t rsi3_2;
    uint64_t *var_72;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint32_t var_12;
    uint64_t var_13;
    uint64_t var_73;
    uint64_t local_sp_5;
    uint64_t rax_6;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t local_sp_0;
    uint64_t var_43;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_67;
    uint64_t var_50;
    uint64_t var_19;
    uint64_t _pre178;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t rdx1_0_pre_phi;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_18;
    uint64_t local_sp_1;
    uint64_t _pre176;
    uint64_t **var_14;
    uint64_t var_15;
    uint64_t *var_16;
    uint64_t var_17;
    uint64_t rax_0;
    uint64_t r13_0;
    uint64_t rcx4_0;
    uint64_t rax_1;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t rax_4;
    uint64_t r12_4;
    uint64_t rax_2_in;
    uint64_t r12_0;
    uint64_t rax_3;
    uint64_t rsi3_0_in;
    uint64_t rsi3_0;
    uint64_t rax_2;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t rdi2_1;
    uint64_t r12_1;
    uint64_t rdx1_1;
    uint64_t rdi2_2;
    uint64_t r12_2;
    uint64_t var_44;
    uint64_t rax_5_in;
    uint64_t r12_3;
    uint64_t rcx4_1_in;
    uint64_t rcx4_1;
    uint64_t rax_5;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t *var_48;
    uint64_t var_49;
    uint64_t local_sp_2;
    uint64_t rdx1_2;
    uint64_t r14_0;
    uint64_t local_sp_4;
    uint64_t var_51;
    uint64_t local_sp_3;
    uint64_t var_52;
    uint64_t rax_7;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t rdi2_3_pn;
    uint64_t r14_1;
    uint64_t rsi3_1;
    uint64_t *var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t local_sp_6;
    uint64_t rdi2_4;
    uint64_t r14_2_in;
    uint64_t r14_2;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t *var_76;
    uint64_t *var_77;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_7 = var_0 + (-72L);
    var_8 = (uint64_t *)rdi;
    var_9 = *var_8;
    var_10 = (uint64_t *)(rdi + 8UL);
    var_11 = *var_10;
    var_12 = *(uint32_t *)(rdi + 80UL);
    var_13 = (rsi >> ((((uint64_t)var_12 << 1UL) + 2UL) & 62UL)) + 1UL;
    rax_6 = var_13;
    var_43 = var_11;
    var_67 = var_11;
    var_19 = var_9;
    var_18 = var_11;
    local_sp_1 = var_7;
    rax_0 = var_13;
    rax_3 = var_13;
    rdi2_1 = var_9;
    local_sp_2 = var_7;
    rdx1_2 = var_9;
    r14_0 = var_13;
    var_51 = var_11;
    local_sp_3 = var_7;
    var_52 = var_9;
    if ((uint64_t)(var_12 & (-2)) == 0UL) {
        var_48 = (uint64_t *)(rdi + 16UL);
        var_49 = *var_48;
        var_50 = var_49;
        if (var_9 != var_49) {
            local_sp_4 = local_sp_2;
            rdi2_3_pn = rdx1_2;
            r14_1 = r14_0;
            rsi3_1 = var_67;
            if (*(uint64_t *)(rdi + 48UL) == (uint64_t)((long)(var_11 - var_67) >> (long)5UL)) {
                var_74 = (uint64_t)((long)(var_11 - *var_10) >> (long)5UL);
                var_75 = (uint64_t)((long)(var_9 - *var_8) >> (long)5UL);
                var_76 = (uint64_t *)(rdi + 40UL);
                *var_76 = (*var_76 - var_75);
                var_77 = (uint64_t *)(rdi + 48UL);
                *var_77 = (*var_77 - var_74);
                return;
            }
            local_sp_5 = local_sp_4;
            rsi3_2 = rsi3_1;
            var_68 = (uint64_t *)(rdi + 24UL);
            *(uint64_t *)(local_sp_4 + 8UL) = (r14_1 + (-1L));
            if (*(uint64_t *)(rdi + 40UL) != (uint64_t)((long)(var_9 - rdi2_3_pn) >> (long)5UL) & rsi3_1 != *var_68 & r14_1 == 0UL) {
                var_69 = rsi3_2 + (-32L);
                *var_10 = var_69;
                var_70 = local_sp_5 + (-8L);
                *(uint64_t *)var_70 = 4221528UL;
                indirect_placeholder_11(rcx, var_69, rdx);
                var_71 = *var_10;
                local_sp_5 = var_70;
                rsi3_2 = var_71;
                while (var_71 != *var_68)
                    {
                        var_72 = (uint64_t *)local_sp_5;
                        var_73 = *var_72;
                        *var_72 = (var_73 + (-1L));
                        if (var_73 == 0UL) {
                            break;
                        }
                        var_69 = rsi3_2 + (-32L);
                        *var_10 = var_69;
                        var_70 = local_sp_5 + (-8L);
                        *(uint64_t *)var_70 = 4221528UL;
                        indirect_placeholder_11(rcx, var_69, rdx);
                        var_71 = *var_10;
                        local_sp_5 = var_70;
                        rsi3_2 = var_71;
                    }
            }
        }
        while (1U)
            {
                local_sp_4 = local_sp_3;
                rax_7 = rax_6;
                rdi2_3_pn = var_52;
                rsi3_1 = var_51;
                local_sp_6 = local_sp_3;
                rdi2_4 = var_52;
                if (var_51 != *(uint64_t *)(rdi + 24UL)) {
                    loop_state_var = 1U;
                    break;
                }
                var_53 = rax_6 + (-1L);
                r14_0 = var_53;
                rax_6 = var_53;
                rax_7 = 18446744073709551615UL;
                if (rax_6 != 0UL) {
                    loop_state_var = 1U;
                    break;
                }
                var_54 = var_51 + (-32L);
                var_55 = var_52 + (-32L);
                *(uint64_t *)(local_sp_3 + (-8L)) = 4221406UL;
                var_56 = indirect_placeholder_2(var_55, var_54);
                var_57 = helper_cc_compute_all_wrapper(var_56, 0UL, 0UL, 24U);
                if ((uint64_t)(((unsigned char)(var_57 >> 4UL) ^ (unsigned char)var_57) & '\xc0') == 0UL) {
                    var_60 = *var_10 + (-32L);
                    *var_10 = var_60;
                    var_61 = local_sp_3 + (-16L);
                    *(uint64_t *)var_61 = 4221433UL;
                    indirect_placeholder_11(rcx, var_60, rdx);
                    local_sp_0 = var_61;
                } else {
                    var_58 = *var_8 + (-32L);
                    *var_8 = var_58;
                    var_59 = local_sp_3 + (-16L);
                    *(uint64_t *)var_59 = 4221355UL;
                    indirect_placeholder_11(rcx, var_58, rdx);
                    local_sp_0 = var_59;
                }
                var_62 = *var_8;
                var_63 = *var_48;
                var_50 = var_63;
                local_sp_2 = local_sp_0;
                rdx1_2 = var_62;
                local_sp_3 = local_sp_0;
                var_52 = var_62;
                if (var_62 == var_63) {
                    var_51 = *var_10;
                    continue;
                }
                _pre178 = *var_10;
                var_67 = _pre178;
                loop_state_var = 0U;
                break;
            }
        switch (loop_state_var) {
          case 0U:
            {
                local_sp_4 = local_sp_2;
                rdi2_3_pn = rdx1_2;
                r14_1 = r14_0;
                rsi3_1 = var_67;
                if (*(uint64_t *)(rdi + 48UL) == (uint64_t)((long)(var_11 - var_67) >> (long)5UL)) {
                    var_74 = (uint64_t)((long)(var_11 - *var_10) >> (long)5UL);
                    var_75 = (uint64_t)((long)(var_9 - *var_8) >> (long)5UL);
                    var_76 = (uint64_t *)(rdi + 40UL);
                    *var_76 = (*var_76 - var_75);
                    var_77 = (uint64_t *)(rdi + 48UL);
                    *var_77 = (*var_77 - var_74);
                    return;
                }
            }
            break;
          case 1U:
            {
                r14_1 = rax_7;
                r14_2_in = rax_7;
                if (*(uint64_t *)(rdi + 48UL) != (uint64_t)((long)(var_11 - var_51) >> (long)5UL)) {
                    if (!((var_52 == var_50) || (rax_7 == 0UL))) {
                        r14_2 = r14_2_in + (-1L);
                        var_64 = rdi2_4 + (-32L);
                        *var_8 = var_64;
                        var_65 = local_sp_6 + (-8L);
                        *(uint64_t *)var_65 = 4221608UL;
                        indirect_placeholder_11(rcx, var_64, rdx);
                        var_66 = *var_8;
                        local_sp_6 = var_65;
                        rdi2_4 = var_66;
                        r14_2_in = r14_2;
                        do {
                            r14_2 = r14_2_in + (-1L);
                            var_64 = rdi2_4 + (-32L);
                            *var_8 = var_64;
                            var_65 = local_sp_6 + (-8L);
                            *(uint64_t *)var_65 = 4221608UL;
                            indirect_placeholder_11(rcx, var_64, rdx);
                            var_66 = *var_8;
                            local_sp_6 = var_65;
                            rdi2_4 = var_66;
                            r14_2_in = r14_2;
                        } while (!((var_66 == *var_48) || (r14_2 == 0UL)));
                    }
                    var_74 = (uint64_t)((long)(var_11 - *var_10) >> (long)5UL);
                    var_75 = (uint64_t)((long)(var_9 - *var_8) >> (long)5UL);
                    var_76 = (uint64_t *)(rdi + 40UL);
                    *var_76 = (*var_76 - var_75);
                    var_77 = (uint64_t *)(rdi + 48UL);
                    *var_77 = (*var_77 - var_74);
                    return;
                }
            }
            break;
        }
    }
    var_14 = (uint64_t **)(rdi + 32UL);
    var_15 = **var_14;
    var_16 = (uint64_t *)(rdi + 16UL);
    var_17 = *var_16;
    r13_0 = var_15;
    rcx4_0 = var_17;
    r12_1 = var_15;
    if (var_9 != var_17) {
        rax_4 = rax_3;
        r12_4 = r12_1;
        rdx1_1 = var_43;
        rdi2_2 = rdi2_1;
        r12_2 = r12_1;
        if ((uint64_t)((long)(var_11 - var_43) >> (long)5UL) != *(uint64_t *)(rdi + 48UL)) {
            **var_14 = r12_4;
            var_74 = (uint64_t)((long)(var_11 - *var_10) >> (long)5UL);
            var_75 = (uint64_t)((long)(var_9 - *var_8) >> (long)5UL);
            var_76 = (uint64_t *)(rdi + 40UL);
            *var_76 = (*var_76 - var_75);
            var_77 = (uint64_t *)(rdi + 48UL);
            *var_77 = (*var_77 - var_74);
            return;
        }
        rax_5_in = rdx1_1;
        r12_3 = r12_2;
        rcx4_1_in = rax_4;
        r12_4 = r12_2;
        var_44 = *(uint64_t *)(rdi + 24UL);
        if (*(uint64_t *)(rdi + 40UL) != (uint64_t)((long)(var_9 - rdi2_2) >> (long)5UL) & (var_44 == rdx1_1) || (rax_4 == 0UL)) {
            rcx4_1 = rcx4_1_in + (-1L);
            rax_5 = rax_5_in + (-32L);
            *var_10 = rax_5;
            var_45 = r12_3 + (-32L);
            var_46 = *(uint64_t *)(rax_5_in + (-24L));
            *(uint64_t *)var_45 = *(uint64_t *)rax_5;
            *(uint64_t *)(r12_3 + (-24L)) = var_46;
            var_47 = *(uint64_t *)(rax_5_in + (-8L));
            *(uint64_t *)(r12_3 + (-16L)) = *(uint64_t *)(rax_5_in + (-16L));
            *(uint64_t *)(r12_3 + (-8L)) = var_47;
            rax_5_in = rax_5;
            r12_3 = var_45;
            rcx4_1_in = rcx4_1;
            r12_4 = var_45;
            do {
                rcx4_1 = rcx4_1_in + (-1L);
                rax_5 = rax_5_in + (-32L);
                *var_10 = rax_5;
                var_45 = r12_3 + (-32L);
                var_46 = *(uint64_t *)(rax_5_in + (-24L));
                *(uint64_t *)var_45 = *(uint64_t *)rax_5;
                *(uint64_t *)(r12_3 + (-24L)) = var_46;
                var_47 = *(uint64_t *)(rax_5_in + (-8L));
                *(uint64_t *)(r12_3 + (-16L)) = *(uint64_t *)(rax_5_in + (-16L));
                *(uint64_t *)(r12_3 + (-8L)) = var_47;
                rax_5_in = rax_5;
                r12_3 = var_45;
                rcx4_1_in = rcx4_1;
                r12_4 = var_45;
            } while (!((rax_5 == var_44) || (rcx4_1 == 0UL)));
        }
        **var_14 = r12_4;
        var_74 = (uint64_t)((long)(var_11 - *var_10) >> (long)5UL);
        var_75 = (uint64_t)((long)(var_9 - *var_8) >> (long)5UL);
        var_76 = (uint64_t *)(rdi + 40UL);
        *var_76 = (*var_76 - var_75);
        var_77 = (uint64_t *)(rdi + 48UL);
        *var_77 = (*var_77 - var_74);
        return;
    }
    while (1U)
        {
            rax_1 = rax_0;
            r12_4 = r13_0;
            rax_2_in = var_19;
            r12_0 = r13_0;
            rdx1_1 = var_18;
            rdi2_2 = var_19;
            r12_2 = r13_0;
            if (var_18 != *(uint64_t *)(rdi + 24UL)) {
                loop_state_var = 1U;
                break;
            }
            var_20 = rax_0 + (-1L);
            rax_0 = var_20;
            rax_1 = 18446744073709551615UL;
            rax_3 = var_20;
            if (rax_0 != 0UL) {
                loop_state_var = 1U;
                break;
            }
            var_21 = var_18 + (-32L);
            var_22 = var_19 + (-32L);
            var_23 = local_sp_1 + (-8L);
            *(uint64_t *)var_23 = 4221051UL;
            var_24 = indirect_placeholder_2(var_22, var_21);
            var_25 = helper_cc_compute_all_wrapper(var_24, 0UL, 0UL, 24U);
            local_sp_1 = var_23;
            if ((uint64_t)(((unsigned char)(var_25 >> 4UL) ^ (unsigned char)var_25) & '\xc0') == 0UL) {
                var_32 = *var_10;
                var_33 = var_32 + (-32L);
                *var_10 = var_33;
                var_34 = *(uint64_t *)var_33;
                var_35 = *(uint64_t *)(var_32 + (-24L));
                var_36 = r13_0 + (-32L);
                *(uint64_t *)var_36 = var_34;
                *(uint64_t *)(r13_0 + (-24L)) = var_35;
                var_37 = *(uint64_t *)(var_32 + (-8L));
                *(uint64_t *)(r13_0 + (-16L)) = *(uint64_t *)(var_32 + (-16L));
                *(uint64_t *)(r13_0 + (-8L)) = var_37;
                rdx1_0_pre_phi = var_36;
            } else {
                var_26 = *var_8;
                var_27 = var_26 + (-32L);
                *var_8 = var_27;
                var_28 = *(uint64_t *)var_27;
                var_29 = *(uint64_t *)(var_26 + (-24L));
                var_30 = r13_0 + (-32L);
                *(uint64_t *)var_30 = var_28;
                *(uint64_t *)(r13_0 + (-24L)) = var_29;
                var_31 = *(uint64_t *)(var_26 + (-8L));
                *(uint64_t *)(r13_0 + (-16L)) = *(uint64_t *)(var_26 + (-16L));
                *(uint64_t *)(r13_0 + (-8L)) = var_31;
                rdx1_0_pre_phi = var_30;
            }
            var_38 = *var_8;
            var_39 = *var_16;
            var_19 = var_38;
            r13_0 = rdx1_0_pre_phi;
            rcx4_0 = var_39;
            rdi2_1 = var_38;
            r12_1 = rdx1_0_pre_phi;
            if (var_38 == var_39) {
                var_18 = *var_10;
                continue;
            }
            _pre176 = *var_10;
            var_43 = _pre176;
            loop_state_var = 0U;
            break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            rax_4 = rax_3;
            r12_4 = r12_1;
            rdx1_1 = var_43;
            rdi2_2 = rdi2_1;
            r12_2 = r12_1;
            if ((uint64_t)((long)(var_11 - var_43) >> (long)5UL) != *(uint64_t *)(rdi + 48UL)) {
                **var_14 = r12_4;
                var_74 = (uint64_t)((long)(var_11 - *var_10) >> (long)5UL);
                var_75 = (uint64_t)((long)(var_9 - *var_8) >> (long)5UL);
                var_76 = (uint64_t *)(rdi + 40UL);
                *var_76 = (*var_76 - var_75);
                var_77 = (uint64_t *)(rdi + 48UL);
                *var_77 = (*var_77 - var_74);
                return;
            }
        }
        break;
      case 1U:
        {
            rsi3_0_in = rax_1;
            rax_4 = rax_1;
            if (*(uint64_t *)(rdi + 48UL) != (uint64_t)((long)(var_11 - var_18) >> (long)5UL)) {
                if ((var_19 == rcx4_0) || (rax_1 == 0UL)) {
                    rsi3_0 = rsi3_0_in + (-1L);
                    rax_2 = rax_2_in + (-32L);
                    *var_8 = rax_2;
                    var_40 = r12_0 + (-32L);
                    var_41 = *(uint64_t *)(rax_2_in + (-24L));
                    *(uint64_t *)var_40 = *(uint64_t *)rax_2;
                    *(uint64_t *)(r12_0 + (-24L)) = var_41;
                    var_42 = *(uint64_t *)(rax_2_in + (-8L));
                    *(uint64_t *)(r12_0 + (-16L)) = *(uint64_t *)(rax_2_in + (-16L));
                    *(uint64_t *)(r12_0 + (-8L)) = var_42;
                    rax_2_in = rax_2;
                    r12_0 = var_40;
                    rsi3_0_in = rsi3_0;
                    r12_4 = var_40;
                    do {
                        rsi3_0 = rsi3_0_in + (-1L);
                        rax_2 = rax_2_in + (-32L);
                        *var_8 = rax_2;
                        var_40 = r12_0 + (-32L);
                        var_41 = *(uint64_t *)(rax_2_in + (-24L));
                        *(uint64_t *)var_40 = *(uint64_t *)rax_2;
                        *(uint64_t *)(r12_0 + (-24L)) = var_41;
                        var_42 = *(uint64_t *)(rax_2_in + (-8L));
                        *(uint64_t *)(r12_0 + (-16L)) = *(uint64_t *)(rax_2_in + (-16L));
                        *(uint64_t *)(r12_0 + (-8L)) = var_42;
                        rax_2_in = rax_2;
                        r12_0 = var_40;
                        rsi3_0_in = rsi3_0;
                        r12_4 = var_40;
                    } while (!((rax_2 == rcx4_0) || (rsi3_0 == 0UL)));
                }
                **var_14 = r12_4;
                var_74 = (uint64_t)((long)(var_11 - *var_10) >> (long)5UL);
                var_75 = (uint64_t)((long)(var_9 - *var_8) >> (long)5UL);
                var_76 = (uint64_t *)(rdi + 40UL);
                *var_76 = (*var_76 - var_75);
                var_77 = (uint64_t *)(rdi + 48UL);
                *var_77 = (*var_77 - var_74);
                return;
            }
        }
        break;
    }
}
