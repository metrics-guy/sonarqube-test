typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_fpop_wrapper_ret_type;
struct type_6;
struct helper_fmov_STN_ST0_wrapper_ret_type;
struct indirect_placeholder_45_ret_type;
struct indirect_placeholder_46_ret_type;
struct helper_fpop_wrapper_ret_type {
    uint32_t field_0;
    unsigned char field_1;
    unsigned char field_2;
    unsigned char field_3;
    unsigned char field_4;
    unsigned char field_5;
    unsigned char field_6;
    unsigned char field_7;
    unsigned char field_8;
};
struct type_6 {
};
struct helper_fmov_STN_ST0_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
    uint16_t field_8;
    uint16_t field_9;
    uint16_t field_10;
    uint16_t field_11;
    uint16_t field_12;
    uint16_t field_13;
    uint16_t field_14;
    uint16_t field_15;
};
struct indirect_placeholder_45_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_46_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_state_0x8490(void);
extern uint64_t init_state_0x84a0(void);
extern uint16_t init_state_0x84b8(void);
extern uint64_t init_state_0x84b0(void);
extern uint64_t init_state_0x84c0(void);
extern uint64_t init_state_0x84d0(void);
extern uint64_t init_state_0x84e0(void);
extern uint64_t init_state_0x84f0(void);
extern uint64_t init_state_0x8500(void);
extern uint16_t init_state_0x8498(void);
extern uint16_t init_state_0x84a8(void);
extern uint16_t init_state_0x84c8(void);
extern uint16_t init_state_0x84d8(void);
extern uint16_t init_state_0x84e8(void);
extern uint16_t init_state_0x84f8(void);
extern uint16_t init_state_0x8508(void);
extern uint32_t init_state_0x8480(void);
extern struct helper_fpop_wrapper_ret_type helper_fpop_wrapper(struct type_6 *param_0, uint32_t param_1);
extern struct helper_fmov_STN_ST0_wrapper_ret_type helper_fmov_STN_ST0_wrapper(struct type_6 *param_0, uint32_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint16_t param_10, uint16_t param_11, uint16_t param_12, uint16_t param_13, uint16_t param_14, uint16_t param_15, uint16_t param_16, uint16_t param_17, uint32_t param_18);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_6(uint64_t param_0);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern void indirect_placeholder_3(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_45_ret_type indirect_placeholder_45(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_46_ret_type indirect_placeholder_46(uint64_t param_0, uint64_t param_1);
void bb_debug_key(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint16_t var_16;
    uint16_t var_17;
    uint16_t var_18;
    uint16_t var_19;
    uint16_t var_20;
    uint16_t var_21;
    uint16_t var_22;
    uint16_t var_23;
    uint32_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t rbx_5;
    uint64_t var_51;
    uint64_t rdx_0;
    uint64_t *_pre_phi;
    uint64_t local_sp_4;
    uint64_t local_sp_3;
    uint64_t local_sp_0;
    uint64_t var_47;
    uint64_t rax_0;
    uint64_t var_48;
    uint64_t *var_49;
    uint64_t var_50;
    unsigned char *var_38;
    uint64_t var_52;
    uint64_t var_45;
    uint64_t *var_46;
    uint64_t var_43;
    uint64_t *var_44;
    uint64_t rbx_3;
    bool var_28;
    uint64_t var_29;
    struct indirect_placeholder_45_ret_type var_30;
    uint64_t var_31;
    uint64_t rbx_1;
    uint64_t rbx_4;
    uint64_t local_sp_5;
    uint64_t rbx_0;
    uint64_t r12_0;
    uint64_t local_sp_1;
    uint64_t rbx_2;
    uint64_t r12_1;
    uint64_t local_sp_2;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t r12_2;
    unsigned char *var_36;
    unsigned char var_37;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t *var_41;
    uint64_t var_42;
    uint64_t var_32;
    struct indirect_placeholder_46_ret_type var_33;
    uint64_t r12_3;
    uint64_t local_sp_6;
    uint64_t var_53;
    uint64_t var_54;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_cc_src2();
    var_8 = init_state_0x8490();
    var_9 = init_state_0x84a0();
    var_10 = init_state_0x84b0();
    var_11 = init_state_0x84c0();
    var_12 = init_state_0x84d0();
    var_13 = init_state_0x84e0();
    var_14 = init_state_0x84f0();
    var_15 = init_state_0x8500();
    var_16 = init_state_0x8498();
    var_17 = init_state_0x84a8();
    var_18 = init_state_0x84b8();
    var_19 = init_state_0x84c8();
    var_20 = init_state_0x84d8();
    var_21 = init_state_0x84e8();
    var_22 = init_state_0x84f8();
    var_23 = init_state_0x8508();
    var_24 = init_state_0x8480();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_25 = var_0 + (-72L);
    var_26 = *(uint64_t *)rdi;
    var_27 = (*(uint64_t *)(rdi + 8UL) + var_26) + (-1L);
    rbx_5 = var_26;
    rdx_0 = 0UL;
    rax_0 = 0UL;
    rbx_4 = var_26;
    local_sp_5 = var_25;
    rbx_0 = var_26;
    r12_0 = var_27;
    local_sp_1 = var_25;
    r12_1 = var_27;
    r12_3 = var_27;
    local_sp_6 = var_25;
    if (rsi == 0UL) {
        *(uint64_t *)(local_sp_6 + (-8L)) = 4215534UL;
        var_53 = indirect_placeholder_2(var_26, rbx_5);
        *(uint64_t *)(local_sp_6 + (-16L)) = 4215548UL;
        var_54 = indirect_placeholder_2(rbx_5, r12_3);
        *(uint64_t *)(local_sp_6 + (-24L)) = 4215559UL;
        indirect_placeholder_3(var_53, var_54);
        return;
    }
    var_28 = (*(uint64_t *)rsi == 18446744073709551615UL);
    if (var_28) {
        if (*(uint64_t *)(rsi + 16UL) == 18446744073709551615UL) {
            var_32 = local_sp_5 + (-8L);
            *(uint64_t *)var_32 = 4215424UL;
            var_33 = indirect_placeholder_46(rdi, rsi);
            rbx_0 = rbx_4;
            r12_0 = var_33.field_0;
            local_sp_1 = var_32;
        }
        local_sp_3 = local_sp_1;
        rbx_1 = rbx_0;
        rbx_2 = rbx_0;
        r12_1 = r12_0;
        local_sp_2 = local_sp_1;
        r12_2 = r12_0;
        if (!((*(unsigned char *)(rsi + 48UL) == '\x00') || (var_28 ^ 1))) {
            var_36 = (unsigned char *)r12_2;
            var_37 = *var_36;
            *var_36 = (unsigned char)'\x00';
            rbx_3 = rbx_2;
            local_sp_4 = local_sp_3;
            var_38 = (unsigned char *)rbx_3;
            var_39 = (uint64_t)(uint32_t)(uint64_t)*var_38;
            var_40 = local_sp_4 + (-8L);
            var_41 = (uint64_t *)var_40;
            *var_41 = 4215490UL;
            var_42 = indirect_placeholder_6(var_39);
            _pre_phi = var_41;
            local_sp_0 = var_40;
            local_sp_4 = var_40;
            rbx_5 = rbx_3;
            while (*(unsigned char *)((uint64_t)(unsigned char)var_42 | 4352768UL) != '\x00')
                {
                    rbx_3 = rbx_3 + 1UL;
                    var_38 = (unsigned char *)rbx_3;
                    var_39 = (uint64_t)(uint32_t)(uint64_t)*var_38;
                    var_40 = local_sp_4 + (-8L);
                    var_41 = (uint64_t *)var_40;
                    *var_41 = 4215490UL;
                    var_42 = indirect_placeholder_6(var_39);
                    _pre_phi = var_41;
                    local_sp_0 = var_40;
                    local_sp_4 = var_40;
                    rbx_5 = rbx_3;
                }
            *var_41 = rbx_3;
            if (rbx_3 > r12_2) {
                *var_41 = r12_2;
            } else {
                if (*(unsigned char *)(rsi + 54UL) == '\x00') {
                    var_43 = local_sp_4 + (-16L);
                    var_44 = (uint64_t *)var_43;
                    *var_44 = 4215715UL;
                    indirect_placeholder_2(rbx_3, var_40);
                    _pre_phi = var_44;
                    local_sp_0 = var_43;
                } else {
                    if (*(unsigned char *)(rsi + 52UL) == '\x00') {
                        var_45 = local_sp_4 + (-16L);
                        var_46 = (uint64_t *)var_45;
                        *var_46 = 4215731UL;
                        indirect_placeholder();
                        helper_fmov_STN_ST0_wrapper((struct type_6 *)(0UL), 0U, var_8, var_9, var_10, var_11, var_12, var_13, var_14, var_15, var_16, var_17, var_18, var_19, var_20, var_21, var_22, var_23, var_24);
                        helper_fpop_wrapper((struct type_6 *)(0UL), var_24);
                        _pre_phi = var_46;
                        local_sp_0 = var_45;
                    } else {
                        if ((*(uint64_t *)(rsi + 48UL) & 280375481794560UL) == 0UL) {
                            *var_41 = r12_2;
                        } else {
                            var_47 = helper_cc_compute_c_wrapper(rbx_3 - r12_2, r12_2, var_7, 17U);
                            if (var_47 == 0UL) {
                                rax_0 = (*var_38 == '-');
                            }
                            *(uint64_t *)local_sp_4 = (rax_0 + rbx_3);
                            var_48 = local_sp_4 + (-16L);
                            var_49 = (uint64_t *)var_48;
                            *var_49 = 4215649UL;
                            var_50 = indirect_placeholder_6(local_sp_4);
                            _pre_phi = var_49;
                            local_sp_0 = var_48;
                            if ((uint64_t)((unsigned char)var_50 & '\xf0') <= 47UL) {
                                var_51 = *var_41;
                                if (*(unsigned char *)(rsi + 53UL) == '\x00') {
                                    rdx_0 = (*(unsigned char *)((uint64_t)*(unsigned char *)var_51 | 4306432UL) != '\x00');
                                }
                                *var_49 = (var_51 + rdx_0);
                            }
                        }
                    }
                }
            }
            *var_36 = var_37;
            var_52 = *_pre_phi;
            r12_3 = var_52;
            local_sp_6 = local_sp_0;
            *(uint64_t *)(local_sp_6 + (-8L)) = 4215534UL;
            var_53 = indirect_placeholder_2(var_26, rbx_5);
            *(uint64_t *)(local_sp_6 + (-16L)) = 4215548UL;
            var_54 = indirect_placeholder_2(rbx_5, r12_3);
            *(uint64_t *)(local_sp_6 + (-24L)) = 4215559UL;
            indirect_placeholder_3(var_53, var_54);
            return;
        }
        rbx_5 = rbx_1;
        local_sp_3 = local_sp_2;
        rbx_2 = rbx_1;
        r12_2 = r12_1;
        r12_3 = r12_1;
        if (*(unsigned char *)(rsi + 54UL) != '\x00') {
            var_34 = local_sp_2 + (-8L);
            *(uint64_t *)var_34 = 4215453UL;
            var_35 = indirect_placeholder_6(rsi);
            local_sp_3 = var_34;
            local_sp_6 = var_34;
            if ((uint64_t)(unsigned char)var_35 == 0UL) {
                *(uint64_t *)(local_sp_6 + (-8L)) = 4215534UL;
                var_53 = indirect_placeholder_2(var_26, rbx_5);
                *(uint64_t *)(local_sp_6 + (-16L)) = 4215548UL;
                var_54 = indirect_placeholder_2(rbx_5, r12_3);
                *(uint64_t *)(local_sp_6 + (-24L)) = 4215559UL;
                indirect_placeholder_3(var_53, var_54);
                return;
            }
        }
        var_36 = (unsigned char *)r12_2;
        var_37 = *var_36;
        *var_36 = (unsigned char)'\x00';
        rbx_3 = rbx_2;
        local_sp_4 = local_sp_3;
        var_38 = (unsigned char *)rbx_3;
        var_39 = (uint64_t)(uint32_t)(uint64_t)*var_38;
        var_40 = local_sp_4 + (-8L);
        var_41 = (uint64_t *)var_40;
        *var_41 = 4215490UL;
        var_42 = indirect_placeholder_6(var_39);
        _pre_phi = var_41;
        local_sp_0 = var_40;
        local_sp_4 = var_40;
        rbx_5 = rbx_3;
        while (*(unsigned char *)((uint64_t)(unsigned char)var_42 | 4352768UL) != '\x00')
            {
                rbx_3 = rbx_3 + 1UL;
                var_38 = (unsigned char *)rbx_3;
                var_39 = (uint64_t)(uint32_t)(uint64_t)*var_38;
                var_40 = local_sp_4 + (-8L);
                var_41 = (uint64_t *)var_40;
                *var_41 = 4215490UL;
                var_42 = indirect_placeholder_6(var_39);
                _pre_phi = var_41;
                local_sp_0 = var_40;
                local_sp_4 = var_40;
                rbx_5 = rbx_3;
            }
        *var_41 = rbx_3;
        if (rbx_3 > r12_2) {
            *var_41 = r12_2;
        } else {
            if (*(unsigned char *)(rsi + 54UL) == '\x00') {
                var_43 = local_sp_4 + (-16L);
                var_44 = (uint64_t *)var_43;
                *var_44 = 4215715UL;
                indirect_placeholder_2(rbx_3, var_40);
                _pre_phi = var_44;
                local_sp_0 = var_43;
            } else {
                if (*(unsigned char *)(rsi + 52UL) == '\x00') {
                    var_45 = local_sp_4 + (-16L);
                    var_46 = (uint64_t *)var_45;
                    *var_46 = 4215731UL;
                    indirect_placeholder();
                    helper_fmov_STN_ST0_wrapper((struct type_6 *)(0UL), 0U, var_8, var_9, var_10, var_11, var_12, var_13, var_14, var_15, var_16, var_17, var_18, var_19, var_20, var_21, var_22, var_23, var_24);
                    helper_fpop_wrapper((struct type_6 *)(0UL), var_24);
                    _pre_phi = var_46;
                    local_sp_0 = var_45;
                } else {
                    if ((*(uint64_t *)(rsi + 48UL) & 280375481794560UL) == 0UL) {
                        *var_41 = r12_2;
                    } else {
                        var_47 = helper_cc_compute_c_wrapper(rbx_3 - r12_2, r12_2, var_7, 17U);
                        if (var_47 == 0UL) {
                            rax_0 = (*var_38 == '-');
                        }
                        *(uint64_t *)local_sp_4 = (rax_0 + rbx_3);
                        var_48 = local_sp_4 + (-16L);
                        var_49 = (uint64_t *)var_48;
                        *var_49 = 4215649UL;
                        var_50 = indirect_placeholder_6(local_sp_4);
                        _pre_phi = var_49;
                        local_sp_0 = var_48;
                        if ((uint64_t)((unsigned char)var_50 & '\xf0') <= 47UL) {
                            var_51 = *var_41;
                            if (*(unsigned char *)(rsi + 53UL) == '\x00') {
                                rdx_0 = (*(unsigned char *)((uint64_t)*(unsigned char *)var_51 | 4306432UL) != '\x00');
                            }
                            *var_49 = (var_51 + rdx_0);
                        }
                    }
                }
            }
        }
        *var_36 = var_37;
        var_52 = *_pre_phi;
        r12_3 = var_52;
        local_sp_6 = local_sp_0;
        *(uint64_t *)(local_sp_6 + (-8L)) = 4215534UL;
        var_53 = indirect_placeholder_2(var_26, rbx_5);
        *(uint64_t *)(local_sp_6 + (-16L)) = 4215548UL;
        var_54 = indirect_placeholder_2(rbx_5, r12_3);
        *(uint64_t *)(local_sp_6 + (-24L)) = 4215559UL;
        indirect_placeholder_3(var_53, var_54);
        return;
    }
    var_29 = var_0 + (-80L);
    *(uint64_t *)var_29 = 4215403UL;
    var_30 = indirect_placeholder_45(rdi, rsi);
    var_31 = var_30.field_0;
    rbx_1 = var_31;
    local_sp_2 = var_29;
    rbx_4 = var_31;
    local_sp_5 = var_29;
    if (*(uint64_t *)(rsi + 16UL) == 18446744073709551615UL) {
        rbx_5 = rbx_1;
        local_sp_3 = local_sp_2;
        rbx_2 = rbx_1;
        r12_2 = r12_1;
        r12_3 = r12_1;
        if (*(unsigned char *)(rsi + 54UL) != '\x00') {
            var_34 = local_sp_2 + (-8L);
            *(uint64_t *)var_34 = 4215453UL;
            var_35 = indirect_placeholder_6(rsi);
            local_sp_3 = var_34;
            local_sp_6 = var_34;
            if ((uint64_t)(unsigned char)var_35 == 0UL) {
                *(uint64_t *)(local_sp_6 + (-8L)) = 4215534UL;
                var_53 = indirect_placeholder_2(var_26, rbx_5);
                *(uint64_t *)(local_sp_6 + (-16L)) = 4215548UL;
                var_54 = indirect_placeholder_2(rbx_5, r12_3);
                *(uint64_t *)(local_sp_6 + (-24L)) = 4215559UL;
                indirect_placeholder_3(var_53, var_54);
                return;
            }
        }
        var_36 = (unsigned char *)r12_2;
        var_37 = *var_36;
        *var_36 = (unsigned char)'\x00';
        rbx_3 = rbx_2;
        local_sp_4 = local_sp_3;
        var_38 = (unsigned char *)rbx_3;
        var_39 = (uint64_t)(uint32_t)(uint64_t)*var_38;
        var_40 = local_sp_4 + (-8L);
        var_41 = (uint64_t *)var_40;
        *var_41 = 4215490UL;
        var_42 = indirect_placeholder_6(var_39);
        _pre_phi = var_41;
        local_sp_0 = var_40;
        local_sp_4 = var_40;
        rbx_5 = rbx_3;
        while (*(unsigned char *)((uint64_t)(unsigned char)var_42 | 4352768UL) != '\x00')
            {
                rbx_3 = rbx_3 + 1UL;
                var_38 = (unsigned char *)rbx_3;
                var_39 = (uint64_t)(uint32_t)(uint64_t)*var_38;
                var_40 = local_sp_4 + (-8L);
                var_41 = (uint64_t *)var_40;
                *var_41 = 4215490UL;
                var_42 = indirect_placeholder_6(var_39);
                _pre_phi = var_41;
                local_sp_0 = var_40;
                local_sp_4 = var_40;
                rbx_5 = rbx_3;
            }
        *var_41 = rbx_3;
        if (rbx_3 > r12_2) {
            *var_41 = r12_2;
        } else {
            if (*(unsigned char *)(rsi + 54UL) == '\x00') {
                var_43 = local_sp_4 + (-16L);
                var_44 = (uint64_t *)var_43;
                *var_44 = 4215715UL;
                indirect_placeholder_2(rbx_3, var_40);
                _pre_phi = var_44;
                local_sp_0 = var_43;
            } else {
                if (*(unsigned char *)(rsi + 52UL) == '\x00') {
                    var_45 = local_sp_4 + (-16L);
                    var_46 = (uint64_t *)var_45;
                    *var_46 = 4215731UL;
                    indirect_placeholder();
                    helper_fmov_STN_ST0_wrapper((struct type_6 *)(0UL), 0U, var_8, var_9, var_10, var_11, var_12, var_13, var_14, var_15, var_16, var_17, var_18, var_19, var_20, var_21, var_22, var_23, var_24);
                    helper_fpop_wrapper((struct type_6 *)(0UL), var_24);
                    _pre_phi = var_46;
                    local_sp_0 = var_45;
                } else {
                    if ((*(uint64_t *)(rsi + 48UL) & 280375481794560UL) == 0UL) {
                        *var_41 = r12_2;
                    } else {
                        var_47 = helper_cc_compute_c_wrapper(rbx_3 - r12_2, r12_2, var_7, 17U);
                        if (var_47 == 0UL) {
                            rax_0 = (*var_38 == '-');
                        }
                        *(uint64_t *)local_sp_4 = (rax_0 + rbx_3);
                        var_48 = local_sp_4 + (-16L);
                        var_49 = (uint64_t *)var_48;
                        *var_49 = 4215649UL;
                        var_50 = indirect_placeholder_6(local_sp_4);
                        _pre_phi = var_49;
                        local_sp_0 = var_48;
                        if ((uint64_t)((unsigned char)var_50 & '\xf0') <= 47UL) {
                            var_51 = *var_41;
                            if (*(unsigned char *)(rsi + 53UL) != '\x00') {
                                rdx_0 = (*(unsigned char *)((uint64_t)*(unsigned char *)var_51 | 4306432UL) != '\x00');
                            }
                            *var_49 = (var_51 + rdx_0);
                        }
                    }
                }
            }
        }
        *var_36 = var_37;
        var_52 = *_pre_phi;
        r12_3 = var_52;
        local_sp_6 = local_sp_0;
        *(uint64_t *)(local_sp_6 + (-8L)) = 4215534UL;
        var_53 = indirect_placeholder_2(var_26, rbx_5);
        *(uint64_t *)(local_sp_6 + (-16L)) = 4215548UL;
        var_54 = indirect_placeholder_2(rbx_5, r12_3);
        *(uint64_t *)(local_sp_6 + (-24L)) = 4215559UL;
        indirect_placeholder_3(var_53, var_54);
        return;
    }
    var_32 = local_sp_5 + (-8L);
    *(uint64_t *)var_32 = 4215424UL;
    var_33 = indirect_placeholder_46(rdi, rsi);
    rbx_0 = rbx_4;
    r12_0 = var_33.field_0;
    local_sp_1 = var_32;
    local_sp_3 = local_sp_1;
    rbx_1 = rbx_0;
    rbx_2 = rbx_0;
    r12_1 = r12_0;
    local_sp_2 = local_sp_1;
    r12_2 = r12_0;
    if (!((*(unsigned char *)(rsi + 48UL) == '\x00') || (var_28 ^ 1))) {
        rbx_5 = rbx_1;
        local_sp_3 = local_sp_2;
        rbx_2 = rbx_1;
        r12_2 = r12_1;
        r12_3 = r12_1;
        var_34 = local_sp_2 + (-8L);
        *(uint64_t *)var_34 = 4215453UL;
        var_35 = indirect_placeholder_6(rsi);
        local_sp_3 = var_34;
        local_sp_6 = var_34;
        if (*(unsigned char *)(rsi + 54UL) != '\x00' & (uint64_t)(unsigned char)var_35 == 0UL) {
            *(uint64_t *)(local_sp_6 + (-8L)) = 4215534UL;
            var_53 = indirect_placeholder_2(var_26, rbx_5);
            *(uint64_t *)(local_sp_6 + (-16L)) = 4215548UL;
            var_54 = indirect_placeholder_2(rbx_5, r12_3);
            *(uint64_t *)(local_sp_6 + (-24L)) = 4215559UL;
            indirect_placeholder_3(var_53, var_54);
            return;
        }
    }
    var_36 = (unsigned char *)r12_2;
    var_37 = *var_36;
    *var_36 = (unsigned char)'\x00';
    rbx_3 = rbx_2;
    local_sp_4 = local_sp_3;
    var_38 = (unsigned char *)rbx_3;
    var_39 = (uint64_t)(uint32_t)(uint64_t)*var_38;
    var_40 = local_sp_4 + (-8L);
    var_41 = (uint64_t *)var_40;
    *var_41 = 4215490UL;
    var_42 = indirect_placeholder_6(var_39);
    _pre_phi = var_41;
    local_sp_0 = var_40;
    local_sp_4 = var_40;
    rbx_5 = rbx_3;
    while (*(unsigned char *)((uint64_t)(unsigned char)var_42 | 4352768UL) != '\x00')
        {
            rbx_3 = rbx_3 + 1UL;
            var_38 = (unsigned char *)rbx_3;
            var_39 = (uint64_t)(uint32_t)(uint64_t)*var_38;
            var_40 = local_sp_4 + (-8L);
            var_41 = (uint64_t *)var_40;
            *var_41 = 4215490UL;
            var_42 = indirect_placeholder_6(var_39);
            _pre_phi = var_41;
            local_sp_0 = var_40;
            local_sp_4 = var_40;
            rbx_5 = rbx_3;
        }
    *var_41 = rbx_3;
    if (rbx_3 > r12_2) {
        *var_41 = r12_2;
    } else {
        if (*(unsigned char *)(rsi + 54UL) == '\x00') {
            var_43 = local_sp_4 + (-16L);
            var_44 = (uint64_t *)var_43;
            *var_44 = 4215715UL;
            indirect_placeholder_2(rbx_3, var_40);
            _pre_phi = var_44;
            local_sp_0 = var_43;
        } else {
            if (*(unsigned char *)(rsi + 52UL) == '\x00') {
                var_45 = local_sp_4 + (-16L);
                var_46 = (uint64_t *)var_45;
                *var_46 = 4215731UL;
                indirect_placeholder();
                helper_fmov_STN_ST0_wrapper((struct type_6 *)(0UL), 0U, var_8, var_9, var_10, var_11, var_12, var_13, var_14, var_15, var_16, var_17, var_18, var_19, var_20, var_21, var_22, var_23, var_24);
                helper_fpop_wrapper((struct type_6 *)(0UL), var_24);
                _pre_phi = var_46;
                local_sp_0 = var_45;
            } else {
                if ((*(uint64_t *)(rsi + 48UL) & 280375481794560UL) == 0UL) {
                    *var_41 = r12_2;
                } else {
                    var_47 = helper_cc_compute_c_wrapper(rbx_3 - r12_2, r12_2, var_7, 17U);
                    if (var_47 == 0UL) {
                        rax_0 = (*var_38 == '-');
                    }
                    *(uint64_t *)local_sp_4 = (rax_0 + rbx_3);
                    var_48 = local_sp_4 + (-16L);
                    var_49 = (uint64_t *)var_48;
                    *var_49 = 4215649UL;
                    var_50 = indirect_placeholder_6(local_sp_4);
                    _pre_phi = var_49;
                    local_sp_0 = var_48;
                    if ((uint64_t)((unsigned char)var_50 & '\xf0') <= 47UL) {
                        var_51 = *var_41;
                        if (*(unsigned char *)(rsi + 53UL) == '\x00') {
                            rdx_0 = (*(unsigned char *)((uint64_t)*(unsigned char *)var_51 | 4306432UL) != '\x00');
                        }
                        *var_49 = (var_51 + rdx_0);
                    }
                }
            }
        }
    }
    *var_36 = var_37;
    var_52 = *_pre_phi;
    r12_3 = var_52;
    local_sp_6 = local_sp_0;
}
