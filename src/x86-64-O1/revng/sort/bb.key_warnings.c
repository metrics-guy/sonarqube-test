typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_key_warnings_ret_type;
struct indirect_placeholder_47_ret_type;
struct indirect_placeholder_55_ret_type;
struct indirect_placeholder_50_ret_type;
struct indirect_placeholder_51_ret_type;
struct indirect_placeholder_52_ret_type;
struct indirect_placeholder_49_ret_type;
struct indirect_placeholder_48_ret_type;
struct indirect_placeholder_53_ret_type;
struct indirect_placeholder_54_ret_type;
struct bb_key_warnings_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_47_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_55_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_50_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_51_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_52_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_49_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_48_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_53_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_54_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_6(uint64_t param_0);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r10(void);
extern uint32_t init_state_0x82fc(void);
extern void indirect_placeholder_3(uint64_t param_0, uint64_t param_1);
extern uint64_t init_rcx(void);
extern struct indirect_placeholder_47_ret_type indirect_placeholder_47(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_55_ret_type indirect_placeholder_55(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_50_ret_type indirect_placeholder_50(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_51_ret_type indirect_placeholder_51(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_52_ret_type indirect_placeholder_52(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_49_ret_type indirect_placeholder_49(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_48_ret_type indirect_placeholder_48(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_53_ret_type indirect_placeholder_53(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_54_ret_type indirect_placeholder_54(uint64_t param_0, uint64_t param_1);
struct bb_key_warnings_ret_type bb_key_warnings(uint64_t rdi, uint64_t rsi, uint64_t r9, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint32_t var_10;
    struct indirect_placeholder_47_ret_type var_57;
    struct indirect_placeholder_55_ret_type var_100;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t rcx_9;
    uint64_t rcx_7;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t r10_3;
    uint64_t var_56;
    uint64_t local_sp_4;
    uint64_t r87_3;
    uint64_t local_sp_2;
    uint64_t r13_0;
    uint64_t rcx_8;
    uint64_t rdx_0;
    uint64_t *var_48;
    uint64_t r96_6;
    uint64_t rcx_5;
    uint64_t r10_6;
    uint64_t var_95;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t rcx_0;
    uint64_t r96_3;
    uint64_t rbp_0;
    uint64_t r10_0;
    uint64_t r87_6;
    uint64_t r96_0;
    uint64_t local_sp_6;
    uint64_t r87_0;
    uint64_t local_sp_0;
    uint64_t *var_58;
    uint64_t var_59;
    unsigned char _pr;
    uint64_t rcx_4;
    unsigned char _pr_pre;
    uint64_t rbx_0;
    uint64_t rcx_1;
    uint64_t rdi4_0;
    uint64_t rcx_2;
    unsigned char var_93;
    uint64_t var_94;
    uint64_t r96_4;
    uint64_t r87_4;
    uint64_t rbx_1;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t r87_7;
    uint64_t local_sp_7;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t var_104;
    unsigned char var_87;
    uint64_t r10_2;
    unsigned char var_88;
    uint64_t local_sp_1185;
    uint64_t r10_7;
    uint64_t rcx_3_ph;
    uint64_t r96_7;
    uint64_t r10_1_ph;
    uint64_t r96_1_ph;
    uint64_t r87_1_ph;
    uint64_t local_sp_1_ph;
    uint64_t r87_1184;
    uint64_t r96_1183;
    uint64_t r10_1182;
    uint64_t rcx_3181;
    struct indirect_placeholder_50_ret_type var_105;
    uint64_t var_106;
    uint64_t var_107;
    uint64_t var_108;
    uint64_t var_109;
    uint64_t r96_2;
    uint64_t r87_2;
    struct bb_key_warnings_ret_type mrv;
    struct bb_key_warnings_ret_type mrv1;
    struct bb_key_warnings_ret_type mrv2;
    struct bb_key_warnings_ret_type mrv3;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t *var_60;
    uint64_t var_61;
    unsigned char var_62;
    unsigned char *var_63;
    unsigned char var_64;
    unsigned char *var_65;
    unsigned char var_66;
    unsigned char *var_67;
    unsigned char var_68;
    unsigned char *var_69;
    unsigned char var_70;
    unsigned char *var_71;
    unsigned char var_72;
    unsigned char *var_73;
    unsigned char var_74;
    unsigned char *var_75;
    unsigned char var_76;
    unsigned char *var_77;
    unsigned char var_78;
    unsigned char *var_79;
    uint64_t var_80;
    uint64_t rcx_6;
    uint64_t var_49;
    struct indirect_placeholder_51_ret_type var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t local_sp_3;
    uint64_t var_34;
    struct indirect_placeholder_52_ret_type var_35;
    uint64_t var_36;
    uint64_t var_37;
    struct indirect_placeholder_49_ret_type var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    struct indirect_placeholder_48_ret_type var_42;
    uint64_t *_pre_phi;
    bool var_17;
    uint64_t r10_4;
    uint64_t *var_18;
    uint64_t var_19;
    uint64_t *var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t *var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t *var_29;
    uint64_t r10_5;
    uint64_t r96_5;
    uint64_t r87_5;
    uint64_t local_sp_5;
    uint64_t var_43;
    uint64_t var_44;
    struct indirect_placeholder_53_ret_type var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_81;
    uint64_t var_82;
    struct indirect_placeholder_54_ret_type var_83;
    bool var_84;
    unsigned char *var_85;
    unsigned char var_86;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_rcx();
    var_8 = init_r10();
    var_9 = init_cc_src2();
    var_10 = init_state_0x82fc();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_11 = var_0 + (-280L);
    var_12 = *(uint64_t *)(rdi + 8UL);
    *(uint64_t *)(var_0 + (-136L)) = *(uint64_t *)rdi;
    *(uint64_t *)(var_0 + (-128L)) = var_12;
    var_13 = *(uint64_t *)(rdi + 24UL);
    *(uint64_t *)(var_0 + (-120L)) = *(uint64_t *)(rdi + 16UL);
    *(uint64_t *)(var_0 + (-112L)) = var_13;
    var_14 = *(uint64_t *)(rdi + 40UL);
    *(uint64_t *)(var_0 + (-104L)) = *(uint64_t *)(rdi + 32UL);
    *(uint64_t *)(var_0 + (-96L)) = var_14;
    var_15 = *(uint64_t *)(rdi + 56UL);
    *(uint64_t *)(var_0 + (-88L)) = *(uint64_t *)(rdi + 48UL);
    *(uint64_t *)(var_0 + (-80L)) = var_15;
    *(uint64_t *)(var_0 + (-72L)) = *(uint64_t *)(rdi + 64UL);
    var_16 = *(uint64_t *)4351952UL;
    rcx_9 = var_7;
    local_sp_4 = var_11;
    r13_0 = 1UL;
    rdx_0 = 1UL;
    rbp_0 = 0UL;
    rcx_1 = 18446744073709551615UL;
    rcx_2 = 0UL;
    r96_4 = r9;
    r87_4 = r8;
    rbx_1 = var_16;
    r87_7 = r8;
    local_sp_7 = var_11;
    r10_7 = var_8;
    r96_7 = r9;
    rcx_6 = var_7;
    r10_4 = var_8;
    if (var_16 != 0UL) {
        var_17 = ((uint64_t)(unsigned char)rsi == 0UL);
        while (1U)
            {
                rcx_7 = rcx_6;
                r10_5 = r10_4;
                r96_5 = r96_4;
                r87_5 = r87_4;
                local_sp_5 = local_sp_4;
                if (*(unsigned char *)(rbx_1 + 57UL) == '\x00') {
                    _pre_phi = (uint64_t *)rbx_1;
                } else {
                    var_18 = (uint64_t *)rbx_1;
                    var_19 = *var_18;
                    var_20 = (uint64_t *)(rbx_1 + 16UL);
                    *(uint64_t *)local_sp_4 = *var_20;
                    var_21 = (var_19 == 18446744073709551615UL) ? 0UL : var_19;
                    var_22 = local_sp_4 + 16UL;
                    *(uint64_t *)(local_sp_4 + (-8L)) = 4214650UL;
                    var_23 = indirect_placeholder_2(var_21, var_22);
                    *(uint16_t *)(local_sp_4 + 40UL) = (uint16_t)(unsigned short)43U;
                    var_24 = local_sp_4 + (-16L);
                    *(uint64_t *)var_24 = 4214670UL;
                    indirect_placeholder();
                    var_25 = var_21 + 1UL;
                    var_26 = (uint64_t *)(local_sp_4 + (-24L));
                    *var_26 = 4214687UL;
                    var_27 = indirect_placeholder_2(var_25, local_sp_4);
                    *(uint32_t *)(local_sp_4 + 72UL) = 2124589U;
                    var_28 = local_sp_4 + (-32L);
                    var_29 = (uint64_t *)var_28;
                    *var_29 = 4214708UL;
                    indirect_placeholder();
                    *var_26 = var_27;
                    local_sp_3 = var_28;
                    _pre_phi = var_18;
                    if (*var_20 != 18446744073709551615UL) {
                        var_30 = *var_29 + 1UL;
                        *(uint64_t *)(local_sp_4 + (-40L)) = 4213859UL;
                        indirect_placeholder_2(var_30, var_24);
                        *(uint16_t *)var_23 = (uint16_t)(unsigned short)11552U;
                        *(unsigned char *)(var_23 + 2UL) = (unsigned char)'\x00';
                        var_31 = local_sp_4 + (-48L);
                        *(uint64_t *)var_31 = 4213885UL;
                        indirect_placeholder();
                        var_32 = var_30 + (*(uint64_t *)(rbx_1 + 24UL) == 18446744073709551615UL);
                        *(uint64_t *)(local_sp_4 + (-56L)) = 4213911UL;
                        indirect_placeholder_2(var_32, var_28);
                        **(uint16_t **)var_31 = (uint16_t)(unsigned short)44U;
                        var_33 = local_sp_4 + (-64L);
                        *(uint64_t *)var_33 = 4213933UL;
                        indirect_placeholder();
                        local_sp_3 = var_33;
                    }
                    var_34 = local_sp_3 + 96UL;
                    *(uint64_t *)(local_sp_3 + (-8L)) = 4214739UL;
                    var_35 = indirect_placeholder_52(1UL, var_34);
                    var_36 = var_35.field_0;
                    var_37 = local_sp_3 + 40UL;
                    *(uint64_t *)(local_sp_3 + (-16L)) = 4214757UL;
                    var_38 = indirect_placeholder_49(0UL, var_37);
                    var_39 = var_38.field_0;
                    var_40 = var_38.field_1;
                    var_41 = local_sp_3 + (-24L);
                    *(uint64_t *)var_41 = 4214788UL;
                    var_42 = indirect_placeholder_48(0UL, 4308424UL, 0UL, 0UL, var_39, var_40, var_36);
                    rcx_7 = var_42.field_2;
                    r10_5 = var_42.field_3;
                    r96_5 = var_42.field_4;
                    r87_5 = var_42.field_5;
                    local_sp_5 = var_41;
                }
                var_43 = *_pre_phi;
                rcx_8 = rcx_7;
                r96_6 = r96_5;
                r10_6 = r10_5;
                r87_6 = r87_5;
                local_sp_6 = local_sp_5;
                if (var_43 != 18446744073709551615UL & var_43 > *(uint64_t *)(rbx_1 + 16UL)) {
                    var_44 = local_sp_5 + (-8L);
                    *(uint64_t *)var_44 = 4214386UL;
                    var_45 = indirect_placeholder_53(0UL, 4308648UL, 0UL, 0UL, r13_0, r96_5, r87_5);
                    rcx_8 = var_45.field_2;
                    r96_6 = var_45.field_4;
                    r10_6 = var_45.field_3;
                    rbp_0 = 1UL;
                    r87_6 = var_45.field_5;
                    local_sp_6 = var_44;
                }
                var_46 = local_sp_6 + (-8L);
                *(uint64_t *)var_46 = 4214820UL;
                var_47 = indirect_placeholder_6(rbx_1);
                r10_3 = r10_6;
                r87_3 = r87_6;
                local_sp_2 = var_46;
                rcx_5 = rcx_8;
                rcx_0 = rcx_8;
                r96_3 = r96_6;
                r10_0 = r10_6;
                r96_0 = r96_6;
                r87_0 = r87_6;
                local_sp_0 = var_46;
                if ((uint64_t)(unsigned char)var_47 == 0UL) {
                    rdx_0 = (uint64_t)*(unsigned char *)(rbx_1 + 54UL);
                }
                var_48 = (uint64_t *)(rbx_1 + 16UL);
                if (*var_48 == 0UL) {
                    if (*(uint64_t *)(rbx_1 + 24UL) == 0UL) {
                        if (var_17 && (rbp_0 == 0UL)) {
                            if (*(uint32_t *)4351076UL != 128U) {
                                if (*(unsigned char *)(rbx_1 + 48UL) == '\x00') {
                                    if ((rdx_0 & 1UL) == 0UL) {
                                        var_49 = local_sp_6 + (-16L);
                                        *(uint64_t *)var_49 = 4214005UL;
                                        var_50 = indirect_placeholder_51(0UL, 4308472UL, 0UL, 0UL, r13_0, r96_6, r87_6);
                                        r10_3 = var_50.field_3;
                                        r87_3 = var_50.field_5;
                                        local_sp_2 = var_49;
                                        rcx_5 = var_50.field_2;
                                        r96_3 = var_50.field_4;
                                    } else {
                                        if (*(uint64_t *)(rbx_1 + 8UL) == 0UL) {
                                            var_49 = local_sp_6 + (-16L);
                                            *(uint64_t *)var_49 = 4214005UL;
                                            var_50 = indirect_placeholder_51(0UL, 4308472UL, 0UL, 0UL, r13_0, r96_6, r87_6);
                                            r10_3 = var_50.field_3;
                                            r87_3 = var_50.field_5;
                                            local_sp_2 = var_49;
                                            rcx_5 = var_50.field_2;
                                            r96_3 = var_50.field_4;
                                        } else {
                                            if (*(unsigned char *)(rbx_1 + 49UL) != '\x00' & *(uint64_t *)(rbx_1 + 24UL) == 0UL) {
                                                var_49 = local_sp_6 + (-16L);
                                                *(uint64_t *)var_49 = 4214005UL;
                                                var_50 = indirect_placeholder_51(0UL, 4308472UL, 0UL, 0UL, r13_0, r96_6, r87_6);
                                                r10_3 = var_50.field_3;
                                                r87_3 = var_50.field_5;
                                                local_sp_2 = var_49;
                                                rcx_5 = var_50.field_2;
                                                r96_3 = var_50.field_4;
                                            }
                                        }
                                    }
                                } else {
                                    if (*(unsigned char *)(rbx_1 + 49UL) != '\x00' & *(uint64_t *)(rbx_1 + 24UL) == 0UL) {
                                        var_49 = local_sp_6 + (-16L);
                                        *(uint64_t *)var_49 = 4214005UL;
                                        var_50 = indirect_placeholder_51(0UL, 4308472UL, 0UL, 0UL, r13_0, r96_6, r87_6);
                                        r10_3 = var_50.field_3;
                                        r87_3 = var_50.field_5;
                                        local_sp_2 = var_49;
                                        rcx_5 = var_50.field_2;
                                        r96_3 = var_50.field_4;
                                    }
                                }
                            }
                            var_51 = local_sp_2 + (-8L);
                            *(uint64_t *)var_51 = 4214893UL;
                            var_52 = indirect_placeholder_6(rbx_1);
                            rcx_0 = rcx_5;
                            r10_0 = r10_3;
                            r96_0 = r96_3;
                            r87_0 = r87_3;
                            local_sp_0 = var_51;
                            var_53 = *var_48 + 1UL;
                            var_54 = *_pre_phi + 1UL;
                            var_55 = helper_cc_compute_c_wrapper(((var_54 == 0UL) ? 1UL : var_54) - var_53, var_53, var_9, 17U);
                            rcx_0 = 1UL;
                            if ((uint64_t)(unsigned char)var_52 != 0UL & (var_55 != 0UL) || (var_53 == 0UL)) {
                                var_56 = local_sp_2 + (-16L);
                                *(uint64_t *)var_56 = 4214967UL;
                                var_57 = indirect_placeholder_47(0UL, 4308544UL, 0UL, 0UL, r13_0, r96_3, r87_3);
                                rcx_0 = var_57.field_2;
                                r10_0 = var_57.field_3;
                                r96_0 = var_57.field_4;
                                r87_0 = var_57.field_5;
                                local_sp_0 = var_56;
                            }
                        } else {
                            var_51 = local_sp_2 + (-8L);
                            *(uint64_t *)var_51 = 4214893UL;
                            var_52 = indirect_placeholder_6(rbx_1);
                            rcx_0 = rcx_5;
                            r10_0 = r10_3;
                            r96_0 = r96_3;
                            r87_0 = r87_3;
                            local_sp_0 = var_51;
                            var_53 = *var_48 + 1UL;
                            var_54 = *_pre_phi + 1UL;
                            var_55 = helper_cc_compute_c_wrapper(((var_54 == 0UL) ? 1UL : var_54) - var_53, var_53, var_9, 17U);
                            rcx_0 = 1UL;
                            if (!var_17 & (uint64_t)(unsigned char)var_52 != 0UL && (var_55 != 0UL) || (var_53 == 0UL)) {
                                var_56 = local_sp_2 + (-16L);
                                *(uint64_t *)var_56 = 4214967UL;
                                var_57 = indirect_placeholder_47(0UL, 4308544UL, 0UL, 0UL, r13_0, r96_3, r87_3);
                                rcx_0 = var_57.field_2;
                                r10_0 = var_57.field_3;
                                r96_0 = var_57.field_4;
                                r87_0 = var_57.field_5;
                                local_sp_0 = var_56;
                            }
                        }
                    } else {
                        if (var_17 && (rbp_0 == 0UL)) {
                            var_51 = local_sp_2 + (-8L);
                            *(uint64_t *)var_51 = 4214893UL;
                            var_52 = indirect_placeholder_6(rbx_1);
                            rcx_0 = rcx_5;
                            r10_0 = r10_3;
                            r96_0 = r96_3;
                            r87_0 = r87_3;
                            local_sp_0 = var_51;
                            var_53 = *var_48 + 1UL;
                            var_54 = *_pre_phi + 1UL;
                            var_55 = helper_cc_compute_c_wrapper(((var_54 == 0UL) ? 1UL : var_54) - var_53, var_53, var_9, 17U);
                            rcx_0 = 1UL;
                            if (!var_17 & (uint64_t)(unsigned char)var_52 != 0UL && (var_55 != 0UL) || (var_53 == 0UL)) {
                                var_56 = local_sp_2 + (-16L);
                                *(uint64_t *)var_56 = 4214967UL;
                                var_57 = indirect_placeholder_47(0UL, 4308544UL, 0UL, 0UL, r13_0, r96_3, r87_3);
                                rcx_0 = var_57.field_2;
                                r10_0 = var_57.field_3;
                                r96_0 = var_57.field_4;
                                r87_0 = var_57.field_5;
                                local_sp_0 = var_56;
                            }
                        } else {
                            var_51 = local_sp_2 + (-8L);
                            *(uint64_t *)var_51 = 4214893UL;
                            var_52 = indirect_placeholder_6(rbx_1);
                            rcx_0 = rcx_5;
                            r10_0 = r10_3;
                            r96_0 = r96_3;
                            r87_0 = r87_3;
                            local_sp_0 = var_51;
                            var_53 = *var_48 + 1UL;
                            var_54 = *_pre_phi + 1UL;
                            var_55 = helper_cc_compute_c_wrapper(((var_54 == 0UL) ? 1UL : var_54) - var_53, var_53, var_9, 17U);
                            rcx_0 = 1UL;
                            if ((uint64_t)(unsigned char)var_52 != 0UL & (var_55 != 0UL) || (var_53 == 0UL)) {
                                var_56 = local_sp_2 + (-16L);
                                *(uint64_t *)var_56 = 4214967UL;
                                var_57 = indirect_placeholder_47(0UL, 4308544UL, 0UL, 0UL, r13_0, r96_3, r87_3);
                                rcx_0 = var_57.field_2;
                                r10_0 = var_57.field_3;
                                r96_0 = var_57.field_4;
                                r87_0 = var_57.field_5;
                                local_sp_0 = var_56;
                            }
                        }
                    }
                } else {
                    if (var_17 && (rbp_0 == 0UL)) {
                        var_51 = local_sp_2 + (-8L);
                        *(uint64_t *)var_51 = 4214893UL;
                        var_52 = indirect_placeholder_6(rbx_1);
                        rcx_0 = rcx_5;
                        r10_0 = r10_3;
                        r96_0 = r96_3;
                        r87_0 = r87_3;
                        local_sp_0 = var_51;
                        var_53 = *var_48 + 1UL;
                        var_54 = *_pre_phi + 1UL;
                        var_55 = helper_cc_compute_c_wrapper(((var_54 == 0UL) ? 1UL : var_54) - var_53, var_53, var_9, 17U);
                        rcx_0 = 1UL;
                        if (!var_17 & (uint64_t)(unsigned char)var_52 != 0UL && (var_55 != 0UL) || (var_53 == 0UL)) {
                            var_56 = local_sp_2 + (-16L);
                            *(uint64_t *)var_56 = 4214967UL;
                            var_57 = indirect_placeholder_47(0UL, 4308544UL, 0UL, 0UL, r13_0, r96_3, r87_3);
                            rcx_0 = var_57.field_2;
                            r10_0 = var_57.field_3;
                            r96_0 = var_57.field_4;
                            r87_0 = var_57.field_5;
                            local_sp_0 = var_56;
                        }
                    } else {
                        if (*(uint32_t *)4351076UL != 128U) {
                            if (*(unsigned char *)(rbx_1 + 48UL) == '\x00') {
                                if ((rdx_0 & 1UL) == 0UL) {
                                    var_49 = local_sp_6 + (-16L);
                                    *(uint64_t *)var_49 = 4214005UL;
                                    var_50 = indirect_placeholder_51(0UL, 4308472UL, 0UL, 0UL, r13_0, r96_6, r87_6);
                                    r10_3 = var_50.field_3;
                                    r87_3 = var_50.field_5;
                                    local_sp_2 = var_49;
                                    rcx_5 = var_50.field_2;
                                    r96_3 = var_50.field_4;
                                } else {
                                    if (*(uint64_t *)(rbx_1 + 8UL) == 0UL) {
                                        var_49 = local_sp_6 + (-16L);
                                        *(uint64_t *)var_49 = 4214005UL;
                                        var_50 = indirect_placeholder_51(0UL, 4308472UL, 0UL, 0UL, r13_0, r96_6, r87_6);
                                        r10_3 = var_50.field_3;
                                        r87_3 = var_50.field_5;
                                        local_sp_2 = var_49;
                                        rcx_5 = var_50.field_2;
                                        r96_3 = var_50.field_4;
                                    } else {
                                        if (*(unsigned char *)(rbx_1 + 49UL) != '\x00' & *(uint64_t *)(rbx_1 + 24UL) == 0UL) {
                                            var_49 = local_sp_6 + (-16L);
                                            *(uint64_t *)var_49 = 4214005UL;
                                            var_50 = indirect_placeholder_51(0UL, 4308472UL, 0UL, 0UL, r13_0, r96_6, r87_6);
                                            r10_3 = var_50.field_3;
                                            r87_3 = var_50.field_5;
                                            local_sp_2 = var_49;
                                            rcx_5 = var_50.field_2;
                                            r96_3 = var_50.field_4;
                                        }
                                    }
                                }
                            } else {
                                if (*(unsigned char *)(rbx_1 + 49UL) != '\x00' & *(uint64_t *)(rbx_1 + 24UL) == 0UL) {
                                    var_49 = local_sp_6 + (-16L);
                                    *(uint64_t *)var_49 = 4214005UL;
                                    var_50 = indirect_placeholder_51(0UL, 4308472UL, 0UL, 0UL, r13_0, r96_6, r87_6);
                                    r10_3 = var_50.field_3;
                                    r87_3 = var_50.field_5;
                                    local_sp_2 = var_49;
                                    rcx_5 = var_50.field_2;
                                    r96_3 = var_50.field_4;
                                }
                            }
                        }
                        var_51 = local_sp_2 + (-8L);
                        *(uint64_t *)var_51 = 4214893UL;
                        var_52 = indirect_placeholder_6(rbx_1);
                        rcx_0 = rcx_5;
                        r10_0 = r10_3;
                        r96_0 = r96_3;
                        r87_0 = r87_3;
                        local_sp_0 = var_51;
                        var_53 = *var_48 + 1UL;
                        var_54 = *_pre_phi + 1UL;
                        var_55 = helper_cc_compute_c_wrapper(((var_54 == 0UL) ? 1UL : var_54) - var_53, var_53, var_9, 17U);
                        rcx_0 = 1UL;
                        if ((uint64_t)(unsigned char)var_52 != 0UL & (var_55 != 0UL) || (var_53 == 0UL)) {
                            var_56 = local_sp_2 + (-16L);
                            *(uint64_t *)var_56 = 4214967UL;
                            var_57 = indirect_placeholder_47(0UL, 4308544UL, 0UL, 0UL, r13_0, r96_3, r87_3);
                            rcx_0 = var_57.field_2;
                            r10_0 = var_57.field_3;
                            r96_0 = var_57.field_4;
                            r87_0 = var_57.field_5;
                            local_sp_0 = var_56;
                        }
                    }
                }
                var_58 = (uint64_t *)(local_sp_0 + 176UL);
                var_59 = *var_58;
                rcx_9 = rcx_0;
                local_sp_4 = local_sp_0;
                r96_4 = r96_0;
                r87_4 = r87_0;
                r87_7 = r87_0;
                local_sp_7 = local_sp_0;
                r10_7 = r10_0;
                r96_7 = r96_0;
                rcx_6 = rcx_0;
                r10_4 = r10_0;
                if (var_59 != 0UL & var_59 == *(uint64_t *)(rbx_1 + 32UL)) {
                    *var_58 = 0UL;
                }
                var_60 = (uint64_t *)(local_sp_0 + 184UL);
                var_61 = *var_60;
                if (var_61 != 0UL & var_61 == *(uint64_t *)(rbx_1 + 40UL)) {
                    *var_60 = 0UL;
                }
                var_62 = *(unsigned char *)(rbx_1 + 48UL) ^ '\x01';
                var_63 = (unsigned char *)(local_sp_0 + 192UL);
                *var_63 = (var_62 & *var_63);
                var_64 = *(unsigned char *)(rbx_1 + 49UL) ^ '\x01';
                var_65 = (unsigned char *)(local_sp_0 + 193UL);
                *var_65 = (var_64 & *var_65);
                var_66 = *(unsigned char *)(rbx_1 + 54UL) ^ '\x01';
                var_67 = (unsigned char *)(local_sp_0 + 198UL);
                *var_67 = (var_66 & *var_67);
                var_68 = *(unsigned char *)(rbx_1 + 50UL) ^ '\x01';
                var_69 = (unsigned char *)(local_sp_0 + 194UL);
                *var_69 = (var_68 & *var_69);
                var_70 = *(unsigned char *)(rbx_1 + 52UL) ^ '\x01';
                var_71 = (unsigned char *)(local_sp_0 + 196UL);
                *var_71 = (var_70 & *var_71);
                var_72 = *(unsigned char *)(rbx_1 + 53UL) ^ '\x01';
                var_73 = (unsigned char *)(local_sp_0 + 197UL);
                *var_73 = (var_72 & *var_73);
                var_74 = *(unsigned char *)(rbx_1 + 51UL) ^ '\x01';
                var_75 = (unsigned char *)(local_sp_0 + 195UL);
                *var_75 = (var_74 & *var_75);
                var_76 = *(unsigned char *)(rbx_1 + 56UL) ^ '\x01';
                var_77 = (unsigned char *)(local_sp_0 + 200UL);
                *var_77 = (var_76 & *var_77);
                var_78 = *(unsigned char *)(rbx_1 + 55UL) ^ '\x01';
                var_79 = (unsigned char *)(local_sp_0 + 199UL);
                *var_79 = (var_78 & *var_79);
                var_80 = *(uint64_t *)(rbx_1 + 64UL);
                rbx_1 = var_80;
                if (var_80 == 0UL) {
                    break;
                }
                r13_0 = r13_0 + 1UL;
                continue;
            }
    }
    var_81 = local_sp_7 + 144UL;
    var_82 = local_sp_7 + (-8L);
    *(uint64_t *)var_82 = 4214049UL;
    var_83 = indirect_placeholder_54(0UL, var_81);
    var_84 = ((uint64_t)(unsigned char)var_83.field_0 == 0UL);
    var_85 = (unsigned char *)(local_sp_7 + 191UL);
    var_86 = *var_85;
    rcx_4 = rcx_9;
    r10_2 = r10_7;
    local_sp_1185 = var_82;
    rcx_3_ph = rcx_9;
    r10_1_ph = r10_7;
    r96_1_ph = r96_7;
    r87_1_ph = r87_7;
    local_sp_1_ph = var_82;
    r87_1184 = r87_7;
    r96_1183 = r96_7;
    r10_1182 = r10_7;
    rcx_3181 = rcx_9;
    r96_2 = r96_7;
    r87_2 = r87_7;
    if (!var_84) {
        if (var_86 == '\x00') {
            mrv.field_0 = rcx_4;
            mrv1 = mrv;
            mrv1.field_1 = r10_2;
            mrv2 = mrv1;
            mrv2.field_2 = r96_2;
            mrv3 = mrv2;
            mrv3.field_3 = r87_2;
            return mrv3;
        }
        var_87 = *(unsigned char *)4351962UL;
        if (var_87 != '\x00') {
            rbx_0 = (uint64_t)var_87;
            if (*(uint64_t *)4351952UL == 0UL) {
                mrv.field_0 = rcx_4;
                mrv1 = mrv;
                mrv1.field_1 = r10_2;
                mrv2 = mrv1;
                mrv2.field_2 = r96_2;
                mrv3 = mrv2;
                mrv3.field_3 = r87_2;
                return mrv3;
            }
        }
        var_88 = *(unsigned char *)4351961UL;
        _pr = var_88;
        if (var_88 != '\x00') {
            rcx_4 = rcx_3181;
            r10_2 = r10_1182;
            r96_2 = r96_1183;
            r87_2 = r87_1184;
            if (*(uint64_t *)4351952UL != 0UL) {
                *(uint64_t *)(local_sp_1185 + (-8L)) = 4214149UL;
                var_105 = indirect_placeholder_50(0UL, 4308592UL, 0UL, 0UL, rcx_3181, r96_1183, r87_1184);
                var_106 = var_105.field_2;
                var_107 = var_105.field_3;
                var_108 = var_105.field_4;
                var_109 = var_105.field_5;
                rcx_4 = var_106;
                r10_2 = var_107;
                r96_2 = var_108;
                r87_2 = var_109;
            }
            mrv.field_0 = rcx_4;
            mrv1 = mrv;
            mrv1.field_1 = r10_2;
            mrv2 = mrv1;
            mrv2.field_2 = r96_2;
            mrv3 = mrv2;
            mrv3.field_3 = r87_2;
            return mrv3;
        }
        rbx_0 = (uint64_t)var_88;
        if (*(uint64_t *)4351952UL != 0UL) {
            rcx_4 = rcx_3_ph;
            r10_2 = r10_1_ph;
            local_sp_1185 = local_sp_1_ph;
            r87_1184 = r87_1_ph;
            r96_1183 = r96_1_ph;
            r10_1182 = r10_1_ph;
            rcx_3181 = rcx_3_ph;
            r96_2 = r96_1_ph;
            r87_2 = r87_1_ph;
            if (_pr == '\x00') {
                mrv.field_0 = rcx_4;
                mrv1 = mrv;
                mrv1.field_1 = r10_2;
                mrv2 = mrv1;
                mrv2.field_2 = r96_2;
                mrv3 = mrv2;
                mrv3.field_3 = r87_2;
                return mrv3;
            }
            rcx_4 = rcx_3181;
            r10_2 = r10_1182;
            r96_2 = r96_1183;
            r87_2 = r87_1184;
            if (*(uint64_t *)4351952UL != 0UL) {
                *(uint64_t *)(local_sp_1185 + (-8L)) = 4214149UL;
                var_105 = indirect_placeholder_50(0UL, 4308592UL, 0UL, 0UL, rcx_3181, r96_1183, r87_1184);
                var_106 = var_105.field_2;
                var_107 = var_105.field_3;
                var_108 = var_105.field_4;
                var_109 = var_105.field_5;
                rcx_4 = var_106;
                r10_2 = var_107;
                r96_2 = var_108;
                r87_2 = var_109;
            }
            mrv.field_0 = rcx_4;
            mrv1 = mrv;
            mrv1.field_1 = r10_2;
            mrv2 = mrv1;
            mrv2.field_2 = r96_2;
            mrv3 = mrv2;
            mrv3.field_3 = r87_2;
            return mrv3;
        }
    }
    var_89 = (uint64_t)var_86;
    rbx_0 = var_89;
    if (~(*(unsigned char *)4351962UL != '\x00' & *(unsigned char *)4351961UL == '\x00')) {
        return;
    }
    *var_85 = (unsigned char)'\x00';
}
