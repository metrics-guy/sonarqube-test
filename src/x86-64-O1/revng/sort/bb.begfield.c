typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_begfield_ret_type;
struct bb_begfield_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_6(uint64_t param_0);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t init_rcx(void);
struct bb_begfield_ret_type bb_begfield(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint32_t var_13;
    bool var_14;
    uint64_t var_15;
    bool var_16;
    uint64_t local_sp_3;
    uint64_t var_24;
    uint64_t rbx_4;
    uint64_t rbx_5;
    uint64_t rbx_0;
    uint64_t local_sp_0;
    uint64_t var_28;
    uint64_t rbx_2;
    uint64_t rbx_4_ph;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t local_sp_2;
    uint64_t var_32;
    uint64_t rbx_3;
    uint64_t var_17;
    uint64_t rcx_1;
    uint64_t rbx_1_ph;
    uint64_t rcx_0_ph;
    uint64_t local_sp_1;
    uint64_t rbx_1;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t r12_0_ph_in;
    uint64_t local_sp_3_ph;
    uint64_t r12_0_ph;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t rcx_2;
    uint64_t var_33;
    struct bb_begfield_ret_type mrv;
    struct bb_begfield_ret_type mrv1;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_rbp();
    var_4 = init_r12();
    var_5 = init_r14();
    var_6 = init_rcx();
    var_7 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_5;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_3;
    var_8 = var_0 + (-40L);
    *(uint64_t *)var_8 = var_2;
    var_9 = *(uint64_t *)rdi;
    var_10 = (*(uint64_t *)(rdi + 8UL) + var_9) + (-1L);
    var_11 = *(uint64_t *)rsi;
    var_12 = *(uint64_t *)(rsi + 8UL);
    var_13 = *(uint32_t *)4351076UL;
    var_14 = ((uint64_t)(var_13 + (-128)) == 0UL);
    var_15 = helper_cc_compute_c_wrapper(var_9 - var_10, var_10, var_7, 17U);
    var_16 = (var_15 == 0UL);
    rbx_5 = var_9;
    rbx_2 = var_9;
    rbx_4_ph = var_9;
    rcx_1 = var_6;
    rbx_1_ph = var_9;
    local_sp_1 = var_8;
    r12_0_ph_in = var_11;
    local_sp_3_ph = var_8;
    rcx_2 = var_6;
    if (var_14) {
        if (var_16) {
            var_33 = rbx_5 + var_12;
            mrv.field_0 = (var_33 > var_10) ? var_10 : var_33;
            mrv1 = mrv;
            mrv1.field_1 = rcx_2;
            return mrv1;
        }
        if (var_11 != 0UL) {
            while (1U)
                {
                    r12_0_ph = r12_0_ph_in + (-1L);
                    r12_0_ph_in = r12_0_ph;
                    rbx_4 = rbx_4_ph;
                    local_sp_3 = local_sp_3_ph;
                    while (1U)
                        {
                            var_21 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)rbx_4;
                            var_22 = local_sp_3 + (-8L);
                            *(uint64_t *)var_22 = 4209515UL;
                            var_23 = indirect_placeholder_6(var_21);
                            rbx_0 = rbx_4;
                            local_sp_0 = var_22;
                            local_sp_3 = var_22;
                            rbx_5 = rbx_4;
                            if (*(unsigned char *)((uint64_t)(unsigned char)var_23 | 4352768UL) != '\x00') {
                                loop_state_var = 1U;
                                break;
                            }
                            var_24 = rbx_4 + 1UL;
                            rbx_4 = var_24;
                            rbx_5 = var_24;
                            if (var_10 > var_24) {
                                continue;
                            }
                            loop_state_var = 0U;
                            break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 1U:
                        {
                            if (var_10 <= rbx_4) {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            while (1U)
                                {
                                    var_25 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)rbx_0;
                                    var_26 = local_sp_0 + (-8L);
                                    *(uint64_t *)var_26 = 4209459UL;
                                    var_27 = indirect_placeholder_6(var_25);
                                    rbx_5 = rbx_0;
                                    local_sp_0 = var_26;
                                    rbx_2 = rbx_0;
                                    rbx_4_ph = rbx_0;
                                    local_sp_1 = var_26;
                                    local_sp_3_ph = var_26;
                                    if (*(unsigned char *)((uint64_t)(unsigned char)var_27 | 4352768UL) != '\x00') {
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    var_28 = rbx_0 + 1UL;
                                    rbx_0 = var_28;
                                    rbx_5 = var_10;
                                    if (var_10 == var_28) {
                                        continue;
                                    }
                                    loop_state_var = 0U;
                                    break;
                                }
                            switch_state_var = 0;
                            switch (loop_state_var) {
                              case 1U:
                                {
                                    if (var_10 <= rbx_0) {
                                        loop_state_var = 1U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    if (r12_0_ph == 0UL) {
                                        continue;
                                    }
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              case 0U:
                                {
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                            }
                            if (switch_state_var)
                                break;
                        }
                        break;
                      case 0U:
                        {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            switch (loop_state_var) {
              case 1U:
                {
                    var_33 = rbx_5 + var_12;
                    mrv.field_0 = (var_33 > var_10) ? var_10 : var_33;
                    mrv1 = mrv;
                    mrv1.field_1 = rcx_2;
                    return mrv1;
                }
                break;
              case 0U:
                {
                    break;
                }
                break;
            }
        }
    }
    if (var_16) {
        var_33 = rbx_5 + var_12;
        mrv.field_0 = (var_33 > var_10) ? var_10 : var_33;
        mrv1 = mrv;
        mrv1.field_1 = rcx_2;
        return mrv1;
    }
    var_17 = var_11 + (-1L);
    rcx_0_ph = var_17;
    rcx_1 = var_17;
    rbx_5 = var_10;
    if (var_11 == 0UL) {
        rbx_3 = rbx_2;
        local_sp_2 = local_sp_1;
        rbx_5 = rbx_2;
        rcx_2 = rcx_1;
        if (*(unsigned char *)(rsi + 48UL) != '\x00') {
            var_29 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)rbx_3;
            var_30 = local_sp_2 + (-8L);
            *(uint64_t *)var_30 = 4209333UL;
            var_31 = indirect_placeholder_6(var_29);
            local_sp_2 = var_30;
            rbx_5 = rbx_3;
            while (*(unsigned char *)((uint64_t)(unsigned char)var_31 | 4352768UL) != '\x00')
                {
                    var_32 = rbx_3 + 1UL;
                    rbx_3 = var_32;
                    rbx_5 = var_32;
                    if (var_10 > var_32) {
                        break;
                    }
                    var_29 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)rbx_3;
                    var_30 = local_sp_2 + (-8L);
                    *(uint64_t *)var_30 = 4209333UL;
                    var_31 = indirect_placeholder_6(var_29);
                    local_sp_2 = var_30;
                    rbx_5 = rbx_3;
                }
        }
        var_33 = rbx_5 + var_12;
        mrv.field_0 = (var_33 > var_10) ? var_10 : var_33;
        mrv1 = mrv;
        mrv1.field_1 = rcx_2;
        return mrv1;
    }
    while (1U)
        {
            rbx_1 = rbx_1_ph;
            rcx_2 = rcx_0_ph;
            while (1U)
                {
                    if ((uint64_t)(var_13 - (uint32_t)(uint64_t)*(unsigned char *)rbx_1) != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_18 = rbx_1 + 1UL;
                    rbx_1 = var_18;
                    if (var_10 == var_18) {
                        continue;
                    }
                    loop_state_var = 0U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    rbx_5 = rbx_1;
                    if (var_10 <= rbx_1) {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    var_19 = rbx_1 + 1UL;
                    rbx_1_ph = var_19;
                    rbx_2 = var_19;
                    rbx_5 = var_19;
                    if (var_10 <= var_19) {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    var_20 = rcx_0_ph + (-1L);
                    rcx_0_ph = var_20;
                    rcx_1 = var_20;
                    if (rcx_0_ph == 0UL) {
                        continue;
                    }
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 0U:
                {
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 1U:
        {
            var_33 = rbx_5 + var_12;
            mrv.field_0 = (var_33 > var_10) ? var_10 : var_33;
            mrv1 = mrv;
            mrv1.field_1 = rcx_2;
            return mrv1;
        }
        break;
      case 0U:
        {
            break;
        }
        break;
    }
}
