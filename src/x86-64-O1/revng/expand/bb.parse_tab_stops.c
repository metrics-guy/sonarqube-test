typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_25_ret_type;
struct indirect_placeholder_24_ret_type;
struct indirect_placeholder_26_ret_type;
struct indirect_placeholder_21_ret_type;
struct indirect_placeholder_27_ret_type;
struct indirect_placeholder_22_ret_type;
struct indirect_placeholder_28_ret_type;
struct indirect_placeholder_23_ret_type;
struct indirect_placeholder_29_ret_type;
struct indirect_placeholder_30_ret_type;
struct indirect_placeholder_31_ret_type;
struct indirect_placeholder_32_ret_type;
struct indirect_placeholder_33_ret_type;
struct indirect_placeholder_34_ret_type;
struct indirect_placeholder_25_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_24_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_26_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_21_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_27_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_22_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_28_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_23_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_29_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_30_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_31_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_32_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_33_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_34_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_19(uint64_t param_0);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern struct indirect_placeholder_25_ret_type indirect_placeholder_25(uint64_t param_0);
extern struct indirect_placeholder_24_ret_type indirect_placeholder_24(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_26_ret_type indirect_placeholder_26(uint64_t param_0);
extern struct indirect_placeholder_21_ret_type indirect_placeholder_21(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_27_ret_type indirect_placeholder_27(uint64_t param_0);
extern struct indirect_placeholder_22_ret_type indirect_placeholder_22(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_28_ret_type indirect_placeholder_28(uint64_t param_0);
extern struct indirect_placeholder_23_ret_type indirect_placeholder_23(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_29_ret_type indirect_placeholder_29(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_30_ret_type indirect_placeholder_30(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_31_ret_type indirect_placeholder_31(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_32_ret_type indirect_placeholder_32(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_33_ret_type indirect_placeholder_33(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_34_ret_type indirect_placeholder_34(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
uint64_t bb_parse_tab_stops(uint64_t rdi, uint64_t rcx, uint64_t r10, uint64_t r9, uint64_t r8) {
    uint64_t r13_2;
    uint64_t var_91;
    struct indirect_placeholder_24_ret_type var_23;
    uint64_t var_18;
    struct indirect_placeholder_25_ret_type var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    struct indirect_placeholder_26_ret_type var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    struct indirect_placeholder_21_ret_type var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t r13_3;
    unsigned char var_41;
    uint64_t r14_3;
    uint64_t rbp_1;
    uint64_t rcx2_4;
    uint64_t r14_1;
    uint64_t var_52;
    struct indirect_placeholder_27_ret_type var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    struct indirect_placeholder_22_ret_type var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    unsigned char var_53;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    unsigned char var_8;
    uint64_t r103_5;
    uint64_t r103_2;
    uint64_t var_84;
    uint64_t rax_1;
    uint64_t r14_2;
    unsigned char rbx_0_in;
    uint64_t rcx2_2;
    uint64_t rbp_0;
    uint64_t rax_0;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t r85_2;
    struct indirect_placeholder_28_ret_type var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    struct indirect_placeholder_23_ret_type var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_9;
    uint64_t r15_2;
    uint64_t r13_0;
    uint64_t r15_0;
    uint64_t r14_0;
    uint64_t rcx2_0;
    uint64_t local_sp_3;
    uint64_t r103_0;
    uint64_t r94_2;
    uint64_t local_sp_0;
    uint64_t r94_0;
    uint64_t r85_0;
    uint64_t local_sp_1;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t r15_3;
    bool var_73;
    uint64_t var_74;
    uint64_t *var_75;
    struct indirect_placeholder_29_ret_type var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_81;
    unsigned char *var_82;
    unsigned char var_83;
    struct indirect_placeholder_30_ret_type var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_65;
    struct indirect_placeholder_31_ret_type var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t r13_1;
    uint64_t rbp_2;
    uint64_t r15_1;
    uint64_t r103_4;
    uint64_t rcx2_1;
    uint64_t local_sp_5;
    uint64_t r103_1;
    uint64_t local_sp_2;
    uint64_t r85_4;
    uint64_t r94_1;
    uint64_t r85_1;
    unsigned char var_92;
    uint64_t rcx2_3;
    bool var_99;
    uint64_t var_100;
    uint64_t *var_101;
    struct indirect_placeholder_32_ret_type var_102;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t var_106;
    struct indirect_placeholder_33_ret_type var_107;
    uint64_t var_93;
    struct indirect_placeholder_34_ret_type var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t var_98;
    uint64_t r103_3;
    uint64_t local_sp_4;
    uint64_t r94_3;
    uint64_t r85_3;
    uint64_t r94_4;
    uint64_t var_108;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_8 = *(unsigned char *)rdi;
    r103_5 = r10;
    rbx_0_in = var_8;
    rbp_0 = rdi;
    rax_0 = 1844674407370955161UL;
    r13_0 = 0UL;
    r15_0 = 0UL;
    r14_0 = 0UL;
    rcx2_0 = rcx;
    r103_0 = r10;
    r94_0 = r9;
    r85_0 = r8;
    if (var_8 == '\x00') {
        return r103_5;
    }
    var_9 = var_0 + (-72L);
    *(unsigned char *)(var_0 + (-65L)) = (unsigned char)'\x01';
    *(uint64_t *)(var_0 + (-64L)) = 0UL;
    *(unsigned char *)(var_0 + (-66L)) = (unsigned char)'\x00';
    local_sp_0 = var_9;
    loop_state_var = 1U;
    while (1U)
        {
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    r13_2 = r13_0;
                    r13_3 = r13_0;
                    r14_3 = r14_0;
                    rbp_1 = rbp_0;
                    r14_1 = r14_0;
                    r103_2 = r103_0;
                    rax_1 = rbp_0;
                    r14_2 = r14_0;
                    rcx2_2 = rcx2_0;
                    r85_2 = r85_0;
                    r15_2 = r15_0;
                    r94_2 = r94_0;
                    local_sp_1 = local_sp_0;
                    r15_3 = r15_0;
                    r13_1 = r13_0;
                    rbp_2 = rbp_0;
                    r15_1 = r15_0;
                    rcx2_1 = rcx2_0;
                    r103_1 = r103_0;
                    r94_1 = r94_0;
                    r85_1 = r85_0;
                    if ((uint64_t)(rbx_0_in + '\xd4') != 0UL) {
                        local_sp_2 = local_sp_1;
                        r13_2 = 0UL;
                        if ((uint64_t)(unsigned char)r13_0 != 0UL) {
                            r13_2 = r13_1;
                            r103_2 = r103_1;
                            rax_1 = rbp_1;
                            r14_2 = r14_1;
                            rcx2_2 = rcx2_1;
                            r85_2 = r85_1;
                            r15_2 = r15_1;
                            local_sp_3 = local_sp_2;
                            r94_2 = r94_1;
                            var_91 = rax_1 + 1UL;
                            var_92 = *(unsigned char *)var_91;
                            r13_3 = r13_2;
                            r14_3 = r14_2;
                            rbx_0_in = var_92;
                            rbp_0 = var_91;
                            r13_0 = r13_2;
                            r15_0 = r15_2;
                            r14_0 = r14_2;
                            rcx2_0 = rcx2_2;
                            r103_0 = r103_2;
                            local_sp_0 = local_sp_3;
                            r94_0 = r94_2;
                            r85_0 = r85_2;
                            r15_3 = r15_2;
                            rbp_2 = var_91;
                            rcx2_3 = rcx2_2;
                            r103_3 = r103_2;
                            local_sp_4 = local_sp_3;
                            r94_3 = r94_2;
                            r85_3 = r85_2;
                            if (var_92 == '\x00') {
                                loop_state_var = 0U;
                                continue;
                            }
                            if ((uint64_t)(unsigned char)r13_2 != 0UL & *(unsigned char *)(local_sp_3 + 7UL) != '\x00') {
                                if ((uint64_t)(unsigned char)r14_2 == 0UL) {
                                    var_93 = local_sp_3 + (-8L);
                                    *(uint64_t *)var_93 = 4206629UL;
                                    var_94 = indirect_placeholder_34(r15_2, rcx2_2, r94_2, r85_2);
                                    var_95 = var_94.field_1;
                                    var_96 = var_94.field_2;
                                    var_97 = var_94.field_3;
                                    var_98 = var_94.field_4;
                                    *(unsigned char *)(local_sp_3 + (-1L)) = (unsigned char)var_94.field_0;
                                    rcx2_3 = var_95;
                                    r103_3 = var_96;
                                    local_sp_4 = var_93;
                                    r94_3 = var_97;
                                    r85_3 = var_98;
                                } else {
                                    var_99 = (*(unsigned char *)(local_sp_3 + 6UL) == '\x00');
                                    var_100 = local_sp_3 + (-8L);
                                    var_101 = (uint64_t *)var_100;
                                    local_sp_4 = var_100;
                                    if (!var_99) {
                                        *var_101 = 4206643UL;
                                        var_107 = indirect_placeholder_33(r15_2, rcx2_2, r103_2);
                                        r103_5 = var_107.field_1;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    *var_101 = 4206591UL;
                                    var_102 = indirect_placeholder_32(r15_2, rcx2_2, r94_2, r85_2);
                                    var_103 = var_102.field_1;
                                    var_104 = var_102.field_2;
                                    var_105 = var_102.field_3;
                                    var_106 = var_102.field_4;
                                    *(unsigned char *)(local_sp_3 + (-1L)) = (unsigned char)var_102.field_0;
                                    rcx2_3 = var_103;
                                    r103_3 = var_104;
                                    r94_3 = var_105;
                                    r85_3 = var_106;
                                }
                            }
                            rcx2_4 = rcx2_3;
                            r103_5 = r103_3;
                            r103_4 = r103_3;
                            local_sp_5 = local_sp_4;
                            r85_4 = r85_3;
                            r94_4 = r94_3;
                            if (*(unsigned char *)(local_sp_4 + 7UL) == '\x00') {
                                switch_state_var = 1;
                                break;
                            }
                            var_108 = local_sp_5 + (-8L);
                            *(uint64_t *)var_108 = 4206454UL;
                            indirect_placeholder_1();
                            rbp_1 = rbp_2;
                            r14_1 = r14_3;
                            r13_1 = r13_3;
                            r15_1 = r15_3;
                            rcx2_1 = rcx2_4;
                            r103_1 = r103_4;
                            local_sp_2 = var_108;
                            r94_1 = r94_4;
                            r85_1 = r85_4;
                            loop_state_var = 1U;
                            continue;
                        }
                        if ((uint64_t)(unsigned char)r14_0 == 0UL) {
                            var_65 = local_sp_1 + (-8L);
                            *(uint64_t *)var_65 = 4206030UL;
                            var_66 = indirect_placeholder_31(r15_0, rcx2_0, r94_0, r85_0);
                            var_67 = var_66.field_0;
                            var_68 = var_66.field_1;
                            var_69 = var_66.field_2;
                            var_70 = var_66.field_3;
                            var_71 = var_66.field_4;
                            var_72 = (uint64_t)(uint32_t)var_67;
                            r14_3 = var_72;
                            rcx2_4 = var_68;
                            r103_2 = var_69;
                            r14_2 = var_72;
                            rcx2_2 = var_68;
                            r85_2 = var_71;
                            local_sp_3 = var_65;
                            r94_2 = var_70;
                            r103_4 = var_69;
                            local_sp_5 = var_65;
                            r85_4 = var_71;
                            r94_4 = var_70;
                            if ((uint64_t)(unsigned char)var_67 == 0UL) {
                                var_108 = local_sp_5 + (-8L);
                                *(uint64_t *)var_108 = 4206454UL;
                                indirect_placeholder_1();
                                rbp_1 = rbp_2;
                                r14_1 = r14_3;
                                r13_1 = r13_3;
                                r15_1 = r15_3;
                                rcx2_1 = rcx2_4;
                                r103_1 = r103_4;
                                local_sp_2 = var_108;
                                r94_1 = r94_4;
                                r85_1 = r85_4;
                                loop_state_var = 1U;
                                continue;
                            }
                        }
                        var_73 = (*(unsigned char *)(local_sp_1 + 6UL) == '\x00');
                        var_74 = local_sp_1 + (-8L);
                        var_75 = (uint64_t *)var_74;
                        local_sp_3 = var_74;
                        local_sp_5 = var_74;
                        if (var_73) {
                            *var_75 = 4206063UL;
                            var_85 = indirect_placeholder_30(r15_0, rcx2_0, r103_0);
                            var_86 = var_85.field_0;
                            var_87 = var_85.field_1;
                            var_88 = var_85.field_2;
                            var_89 = var_85.field_3;
                            var_90 = (uint64_t)*(unsigned char *)(local_sp_1 + (-2L));
                            r13_2 = var_90;
                            r103_2 = var_87;
                            r14_2 = var_90;
                            rcx2_2 = var_86;
                            r85_2 = var_89;
                            r94_2 = var_88;
                        } else {
                            *var_75 = 4206535UL;
                            var_76 = indirect_placeholder_29(r15_0, rcx2_0, r94_0, r85_0);
                            var_77 = var_76.field_0;
                            var_78 = var_76.field_1;
                            var_79 = var_76.field_2;
                            var_80 = var_76.field_3;
                            var_81 = var_76.field_4;
                            var_82 = (unsigned char *)(local_sp_1 + (-2L));
                            var_83 = (unsigned char)var_77;
                            *var_82 = var_83;
                            rcx2_4 = var_78;
                            r103_2 = var_79;
                            rcx2_2 = var_78;
                            r85_2 = var_81;
                            r94_2 = var_80;
                            r103_4 = var_79;
                            r85_4 = var_81;
                            r94_4 = var_80;
                            if ((uint64_t)var_83 != 0UL) {
                                var_108 = local_sp_5 + (-8L);
                                *(uint64_t *)var_108 = 4206454UL;
                                indirect_placeholder_1();
                                rbp_1 = rbp_2;
                                r14_1 = r14_3;
                                r13_1 = r13_3;
                                r15_1 = r15_3;
                                rcx2_1 = rcx2_4;
                                r103_1 = r103_4;
                                local_sp_2 = var_108;
                                r94_1 = r94_4;
                                r85_1 = r85_4;
                                loop_state_var = 1U;
                                continue;
                            }
                            var_84 = (uint64_t)(uint32_t)r14_0;
                            r13_2 = var_84;
                        }
                        var_91 = rax_1 + 1UL;
                        var_92 = *(unsigned char *)var_91;
                        r13_3 = r13_2;
                        r14_3 = r14_2;
                        rbx_0_in = var_92;
                        rbp_0 = var_91;
                        r13_0 = r13_2;
                        r15_0 = r15_2;
                        r14_0 = r14_2;
                        rcx2_0 = rcx2_2;
                        r103_0 = r103_2;
                        local_sp_0 = local_sp_3;
                        r94_0 = r94_2;
                        r85_0 = r85_2;
                        r15_3 = r15_2;
                        rbp_2 = var_91;
                        rcx2_3 = rcx2_2;
                        r103_3 = r103_2;
                        local_sp_4 = local_sp_3;
                        r94_3 = r94_2;
                        r85_3 = r85_2;
                        if (var_92 != '\x00') {
                            loop_state_var = 0U;
                            continue;
                        }
                        if ((uint64_t)(unsigned char)r13_2 != 0UL & *(unsigned char *)(local_sp_3 + 7UL) != '\x00') {
                            if ((uint64_t)(unsigned char)r14_2 == 0UL) {
                                var_93 = local_sp_3 + (-8L);
                                *(uint64_t *)var_93 = 4206629UL;
                                var_94 = indirect_placeholder_34(r15_2, rcx2_2, r94_2, r85_2);
                                var_95 = var_94.field_1;
                                var_96 = var_94.field_2;
                                var_97 = var_94.field_3;
                                var_98 = var_94.field_4;
                                *(unsigned char *)(local_sp_3 + (-1L)) = (unsigned char)var_94.field_0;
                                rcx2_3 = var_95;
                                r103_3 = var_96;
                                local_sp_4 = var_93;
                                r94_3 = var_97;
                                r85_3 = var_98;
                            } else {
                                var_99 = (*(unsigned char *)(local_sp_3 + 6UL) == '\x00');
                                var_100 = local_sp_3 + (-8L);
                                var_101 = (uint64_t *)var_100;
                                local_sp_4 = var_100;
                                if (!var_99) {
                                    *var_101 = 4206643UL;
                                    var_107 = indirect_placeholder_33(r15_2, rcx2_2, r103_2);
                                    r103_5 = var_107.field_1;
                                    switch_state_var = 1;
                                    break;
                                }
                                *var_101 = 4206591UL;
                                var_102 = indirect_placeholder_32(r15_2, rcx2_2, r94_2, r85_2);
                                var_103 = var_102.field_1;
                                var_104 = var_102.field_2;
                                var_105 = var_102.field_3;
                                var_106 = var_102.field_4;
                                *(unsigned char *)(local_sp_3 + (-1L)) = (unsigned char)var_102.field_0;
                                rcx2_3 = var_103;
                                r103_3 = var_104;
                                r94_3 = var_105;
                                r85_3 = var_106;
                            }
                        }
                        rcx2_4 = rcx2_3;
                        r103_5 = r103_3;
                        r103_4 = r103_3;
                        local_sp_5 = local_sp_4;
                        r85_4 = r85_3;
                        r94_4 = r94_3;
                        if (*(unsigned char *)(local_sp_4 + 7UL) == '\x00') {
                            switch_state_var = 1;
                            break;
                        }
                        var_108 = local_sp_5 + (-8L);
                        *(uint64_t *)var_108 = 4206454UL;
                        indirect_placeholder_1();
                        rbp_1 = rbp_2;
                        r14_1 = r14_3;
                        r13_1 = r13_3;
                        r15_1 = r15_3;
                        rcx2_1 = rcx2_4;
                        r103_1 = r103_4;
                        local_sp_2 = var_108;
                        r94_1 = r94_4;
                        r85_1 = r85_4;
                        loop_state_var = 1U;
                        continue;
                    }
                    var_10 = (uint64_t)rbx_0_in;
                    var_11 = (uint64_t)(uint32_t)var_10;
                    var_12 = (uint64_t *)(local_sp_0 + (-8L));
                    *var_12 = 4206486UL;
                    var_13 = indirect_placeholder_19(var_11);
                    var_14 = local_sp_0 + (-16L);
                    *(uint64_t *)var_14 = 4206494UL;
                    indirect_placeholder_1();
                    local_sp_1 = var_14;
                    r14_2 = 1UL;
                    local_sp_3 = var_14;
                    if ((uint64_t)(uint32_t)var_13 == 0UL) {
                        if ((uint64_t)(rbx_0_in + '\xd1') == 0UL) {
                            var_53 = (unsigned char)r13_0;
                            if ((uint64_t)var_53 == 0UL) {
                                *(unsigned char *)(local_sp_0 + (-10L)) = var_53;
                            } else {
                                *(uint64_t *)(local_sp_0 + (-24L)) = 4206287UL;
                                var_54 = indirect_placeholder_28(rbp_0);
                                var_55 = var_54.field_0;
                                var_56 = var_54.field_1;
                                var_57 = var_54.field_2;
                                var_58 = local_sp_0 + (-32L);
                                *(uint64_t *)var_58 = 4206315UL;
                                var_59 = indirect_placeholder_23(0UL, 4257016UL, 0UL, 0UL, var_55, var_56, var_57);
                                var_60 = var_59.field_0;
                                var_61 = var_59.field_1;
                                var_62 = var_59.field_2;
                                var_63 = var_59.field_3;
                                var_64 = (uint64_t)(uint32_t)r13_0;
                                *(unsigned char *)(local_sp_0 + (-25L)) = (unsigned char)'\x00';
                                *(unsigned char *)(local_sp_0 + (-26L)) = (unsigned char)'\x00';
                                r103_2 = var_61;
                                r14_2 = var_64;
                                rcx2_2 = var_60;
                                r85_2 = var_63;
                                local_sp_3 = var_58;
                                r94_2 = var_62;
                            }
                        } else {
                            r13_2 = 1UL;
                            r14_2 = 0UL;
                            if ((uint64_t)(rbx_0_in + '\xd5') == 0UL) {
                                var_41 = (unsigned char)r13_0;
                                if ((uint64_t)var_41 == 0UL) {
                                    var_52 = (uint64_t)(uint32_t)r13_0;
                                    *(unsigned char *)(local_sp_0 + (-10L)) = (unsigned char)'\x01';
                                    r14_2 = var_52;
                                } else {
                                    *(uint64_t *)(local_sp_0 + (-24L)) = 4206359UL;
                                    var_42 = indirect_placeholder_27(rbp_0);
                                    var_43 = var_42.field_0;
                                    var_44 = var_42.field_1;
                                    var_45 = var_42.field_2;
                                    var_46 = local_sp_0 + (-32L);
                                    *(uint64_t *)var_46 = 4206387UL;
                                    var_47 = indirect_placeholder_22(0UL, 4257064UL, 0UL, 0UL, var_43, var_44, var_45);
                                    var_48 = var_47.field_0;
                                    var_49 = var_47.field_1;
                                    var_50 = var_47.field_2;
                                    var_51 = var_47.field_3;
                                    *(unsigned char *)(local_sp_0 + (-26L)) = var_41;
                                    *(unsigned char *)(local_sp_0 + (-25L)) = (unsigned char)'\x00';
                                    r103_2 = var_49;
                                    rcx2_2 = var_48;
                                    r85_2 = var_51;
                                    local_sp_3 = var_46;
                                    r94_2 = var_50;
                                }
                            } else {
                                if (((uint32_t)rbx_0_in + (-48)) <= 9U) {
                                    *(uint64_t *)(local_sp_0 + (-24L)) = 4206416UL;
                                    var_31 = indirect_placeholder_26(rbp_0);
                                    var_32 = var_31.field_0;
                                    var_33 = var_31.field_1;
                                    var_34 = var_31.field_2;
                                    var_35 = local_sp_0 + (-32L);
                                    *(uint64_t *)var_35 = 4206444UL;
                                    var_36 = indirect_placeholder_21(0UL, 4257112UL, 0UL, 0UL, var_32, var_33, var_34);
                                    var_37 = var_36.field_0;
                                    var_38 = var_36.field_1;
                                    var_39 = var_36.field_2;
                                    var_40 = var_36.field_3;
                                    rcx2_4 = var_37;
                                    r103_4 = var_38;
                                    local_sp_5 = var_35;
                                    r85_4 = var_40;
                                    r94_4 = var_39;
                                    var_108 = local_sp_5 + (-8L);
                                    *(uint64_t *)var_108 = 4206454UL;
                                    indirect_placeholder_1();
                                    rbp_1 = rbp_2;
                                    r14_1 = r14_3;
                                    r13_1 = r13_3;
                                    r15_1 = r15_3;
                                    rcx2_1 = rcx2_4;
                                    r103_1 = r103_4;
                                    local_sp_2 = var_108;
                                    r94_1 = r94_4;
                                    r85_1 = r85_4;
                                    loop_state_var = 1U;
                                    continue;
                                }
                                if ((uint64_t)(unsigned char)r13_0 == 0UL) {
                                    var_30 = (uint64_t)((long)((var_10 << 32UL) + (-206158430208L)) >> (long)32UL);
                                    *var_12 = rbp_0;
                                    r15_2 = var_30;
                                } else {
                                    if (r15_0 > 1844674407370955161UL) {
                                        var_17 = *var_12;
                                        *(uint64_t *)(local_sp_0 + (-24L)) = 4206179UL;
                                        indirect_placeholder_1();
                                        *(uint64_t *)(local_sp_0 + (-32L)) = 4206193UL;
                                        var_18 = indirect_placeholder(var_17, rax_0);
                                        *(uint64_t *)(local_sp_0 + (-40L)) = 4206204UL;
                                        var_19 = indirect_placeholder_25(var_18);
                                        var_20 = var_19.field_0;
                                        var_21 = var_19.field_1;
                                        var_22 = var_19.field_2;
                                        *(uint64_t *)(local_sp_0 + (-48L)) = 4206232UL;
                                        var_23 = indirect_placeholder_24(0UL, 4257649UL, 0UL, 0UL, var_20, var_21, var_22);
                                        var_24 = var_23.field_0;
                                        var_25 = var_23.field_1;
                                        var_26 = var_23.field_2;
                                        var_27 = var_23.field_3;
                                        var_28 = local_sp_0 + (-56L);
                                        *(uint64_t *)var_28 = 4206240UL;
                                        indirect_placeholder_1();
                                        var_29 = (rax_0 + var_17) + (-1L);
                                        *(unsigned char *)(local_sp_0 + (-49L)) = (unsigned char)'\x00';
                                        r103_2 = var_25;
                                        rax_1 = var_29;
                                        rcx2_2 = var_24;
                                        r85_2 = var_27;
                                        local_sp_3 = var_28;
                                        r94_2 = var_26;
                                    } else {
                                        var_15 = (r15_0 * 10UL) + (uint64_t)((long)((var_10 << 32UL) + (-206158430208L)) >> (long)32UL);
                                        var_16 = helper_cc_compute_c_wrapper(var_15 - r15_0, r15_0, var_7, 17U);
                                        rax_0 = var_15;
                                        r15_2 = var_15;
                                        if (var_16 == 0UL) {
                                            var_17 = *var_12;
                                            *(uint64_t *)(local_sp_0 + (-24L)) = 4206179UL;
                                            indirect_placeholder_1();
                                            *(uint64_t *)(local_sp_0 + (-32L)) = 4206193UL;
                                            var_18 = indirect_placeholder(var_17, rax_0);
                                            *(uint64_t *)(local_sp_0 + (-40L)) = 4206204UL;
                                            var_19 = indirect_placeholder_25(var_18);
                                            var_20 = var_19.field_0;
                                            var_21 = var_19.field_1;
                                            var_22 = var_19.field_2;
                                            *(uint64_t *)(local_sp_0 + (-48L)) = 4206232UL;
                                            var_23 = indirect_placeholder_24(0UL, 4257649UL, 0UL, 0UL, var_20, var_21, var_22);
                                            var_24 = var_23.field_0;
                                            var_25 = var_23.field_1;
                                            var_26 = var_23.field_2;
                                            var_27 = var_23.field_3;
                                            var_28 = local_sp_0 + (-56L);
                                            *(uint64_t *)var_28 = 4206240UL;
                                            indirect_placeholder_1();
                                            var_29 = (rax_0 + var_17) + (-1L);
                                            *(unsigned char *)(local_sp_0 + (-49L)) = (unsigned char)'\x00';
                                            r103_2 = var_25;
                                            rax_1 = var_29;
                                            rcx2_2 = var_24;
                                            r85_2 = var_27;
                                            local_sp_3 = var_28;
                                            r94_2 = var_26;
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        local_sp_2 = local_sp_1;
                        r13_2 = 0UL;
                        if ((uint64_t)(unsigned char)r13_0 == 0UL) {
                            r13_2 = r13_1;
                            r103_2 = r103_1;
                            rax_1 = rbp_1;
                            r14_2 = r14_1;
                            rcx2_2 = rcx2_1;
                            r85_2 = r85_1;
                            r15_2 = r15_1;
                            local_sp_3 = local_sp_2;
                            r94_2 = r94_1;
                        } else {
                            if ((uint64_t)(unsigned char)r14_0 != 0UL) {
                                var_65 = local_sp_1 + (-8L);
                                *(uint64_t *)var_65 = 4206030UL;
                                var_66 = indirect_placeholder_31(r15_0, rcx2_0, r94_0, r85_0);
                                var_67 = var_66.field_0;
                                var_68 = var_66.field_1;
                                var_69 = var_66.field_2;
                                var_70 = var_66.field_3;
                                var_71 = var_66.field_4;
                                var_72 = (uint64_t)(uint32_t)var_67;
                                r14_3 = var_72;
                                rcx2_4 = var_68;
                                r103_2 = var_69;
                                r14_2 = var_72;
                                rcx2_2 = var_68;
                                r85_2 = var_71;
                                local_sp_3 = var_65;
                                r94_2 = var_70;
                                r103_4 = var_69;
                                local_sp_5 = var_65;
                                r85_4 = var_71;
                                r94_4 = var_70;
                                if ((uint64_t)(unsigned char)var_67 != 0UL) {
                                    var_108 = local_sp_5 + (-8L);
                                    *(uint64_t *)var_108 = 4206454UL;
                                    indirect_placeholder_1();
                                    rbp_1 = rbp_2;
                                    r14_1 = r14_3;
                                    r13_1 = r13_3;
                                    r15_1 = r15_3;
                                    rcx2_1 = rcx2_4;
                                    r103_1 = r103_4;
                                    local_sp_2 = var_108;
                                    r94_1 = r94_4;
                                    r85_1 = r85_4;
                                    loop_state_var = 1U;
                                    continue;
                                }
                            }
                            var_73 = (*(unsigned char *)(local_sp_1 + 6UL) == '\x00');
                            var_74 = local_sp_1 + (-8L);
                            var_75 = (uint64_t *)var_74;
                            local_sp_3 = var_74;
                            local_sp_5 = var_74;
                            if (var_73) {
                                *var_75 = 4206063UL;
                                var_85 = indirect_placeholder_30(r15_0, rcx2_0, r103_0);
                                var_86 = var_85.field_0;
                                var_87 = var_85.field_1;
                                var_88 = var_85.field_2;
                                var_89 = var_85.field_3;
                                var_90 = (uint64_t)*(unsigned char *)(local_sp_1 + (-2L));
                                r13_2 = var_90;
                                r103_2 = var_87;
                                r14_2 = var_90;
                                rcx2_2 = var_86;
                                r85_2 = var_89;
                                r94_2 = var_88;
                            } else {
                                *var_75 = 4206535UL;
                                var_76 = indirect_placeholder_29(r15_0, rcx2_0, r94_0, r85_0);
                                var_77 = var_76.field_0;
                                var_78 = var_76.field_1;
                                var_79 = var_76.field_2;
                                var_80 = var_76.field_3;
                                var_81 = var_76.field_4;
                                var_82 = (unsigned char *)(local_sp_1 + (-2L));
                                var_83 = (unsigned char)var_77;
                                *var_82 = var_83;
                                rcx2_4 = var_78;
                                r103_2 = var_79;
                                rcx2_2 = var_78;
                                r85_2 = var_81;
                                r94_2 = var_80;
                                r103_4 = var_79;
                                r85_4 = var_81;
                                r94_4 = var_80;
                                if ((uint64_t)var_83 != 0UL) {
                                    var_108 = local_sp_5 + (-8L);
                                    *(uint64_t *)var_108 = 4206454UL;
                                    indirect_placeholder_1();
                                    rbp_1 = rbp_2;
                                    r14_1 = r14_3;
                                    r13_1 = r13_3;
                                    r15_1 = r15_3;
                                    rcx2_1 = rcx2_4;
                                    r103_1 = r103_4;
                                    local_sp_2 = var_108;
                                    r94_1 = r94_4;
                                    r85_1 = r85_4;
                                    loop_state_var = 1U;
                                    continue;
                                }
                                var_84 = (uint64_t)(uint32_t)r14_0;
                                r13_2 = var_84;
                            }
                        }
                    }
                }
                break;
              case 1U:
                {
                    r13_2 = r13_1;
                    r103_2 = r103_1;
                    rax_1 = rbp_1;
                    r14_2 = r14_1;
                    rcx2_2 = rcx2_1;
                    r85_2 = r85_1;
                    r15_2 = r15_1;
                    local_sp_3 = local_sp_2;
                    r94_2 = r94_1;
                    var_91 = rax_1 + 1UL;
                    var_92 = *(unsigned char *)var_91;
                    r13_3 = r13_2;
                    r14_3 = r14_2;
                    rbx_0_in = var_92;
                    rbp_0 = var_91;
                    r13_0 = r13_2;
                    r15_0 = r15_2;
                    r14_0 = r14_2;
                    rcx2_0 = rcx2_2;
                    r103_0 = r103_2;
                    local_sp_0 = local_sp_3;
                    r94_0 = r94_2;
                    r85_0 = r85_2;
                    r15_3 = r15_2;
                    rbp_2 = var_91;
                    rcx2_3 = rcx2_2;
                    r103_3 = r103_2;
                    local_sp_4 = local_sp_3;
                    r94_3 = r94_2;
                    r85_3 = r85_2;
                    if (var_92 == '\x00') {
                        loop_state_var = 0U;
                        continue;
                    }
                    if ((uint64_t)(unsigned char)r13_2 != 0UL & *(unsigned char *)(local_sp_3 + 7UL) != '\x00') {
                        if ((uint64_t)(unsigned char)r14_2 == 0UL) {
                            var_93 = local_sp_3 + (-8L);
                            *(uint64_t *)var_93 = 4206629UL;
                            var_94 = indirect_placeholder_34(r15_2, rcx2_2, r94_2, r85_2);
                            var_95 = var_94.field_1;
                            var_96 = var_94.field_2;
                            var_97 = var_94.field_3;
                            var_98 = var_94.field_4;
                            *(unsigned char *)(local_sp_3 + (-1L)) = (unsigned char)var_94.field_0;
                            rcx2_3 = var_95;
                            r103_3 = var_96;
                            local_sp_4 = var_93;
                            r94_3 = var_97;
                            r85_3 = var_98;
                        } else {
                            var_99 = (*(unsigned char *)(local_sp_3 + 6UL) == '\x00');
                            var_100 = local_sp_3 + (-8L);
                            var_101 = (uint64_t *)var_100;
                            local_sp_4 = var_100;
                            if (!var_99) {
                                *var_101 = 4206643UL;
                                var_107 = indirect_placeholder_33(r15_2, rcx2_2, r103_2);
                                r103_5 = var_107.field_1;
                                switch_state_var = 1;
                                break;
                            }
                            *var_101 = 4206591UL;
                            var_102 = indirect_placeholder_32(r15_2, rcx2_2, r94_2, r85_2);
                            var_103 = var_102.field_1;
                            var_104 = var_102.field_2;
                            var_105 = var_102.field_3;
                            var_106 = var_102.field_4;
                            *(unsigned char *)(local_sp_3 + (-1L)) = (unsigned char)var_102.field_0;
                            rcx2_3 = var_103;
                            r103_3 = var_104;
                            r94_3 = var_105;
                            r85_3 = var_106;
                        }
                    }
                    rcx2_4 = rcx2_3;
                    r103_5 = r103_3;
                    r103_4 = r103_3;
                    local_sp_5 = local_sp_4;
                    r85_4 = r85_3;
                    r94_4 = r94_3;
                    if (*(unsigned char *)(local_sp_4 + 7UL) == '\x00') {
                        switch_state_var = 1;
                        break;
                    }
                    var_108 = local_sp_5 + (-8L);
                    *(uint64_t *)var_108 = 4206454UL;
                    indirect_placeholder_1();
                    rbp_1 = rbp_2;
                    r14_1 = r14_3;
                    r13_1 = r13_3;
                    r15_1 = r15_3;
                    rcx2_1 = rcx2_4;
                    r103_1 = r103_4;
                    local_sp_2 = var_108;
                    r94_1 = r94_4;
                    r85_1 = r85_4;
                    loop_state_var = 1U;
                    continue;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    return r103_5;
}
