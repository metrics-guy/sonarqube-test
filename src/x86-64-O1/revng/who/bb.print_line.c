typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_15_ret_type;
struct indirect_placeholder_15_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint32_t init_state_0x82fc(void);
extern void indirect_placeholder_6(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_15_ret_type indirect_placeholder_15(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
void bb_print_line(uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t rcx, uint64_t r8, uint64_t r9) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint32_t var_7;
    struct indirect_placeholder_15_ret_type var_34;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t rdi2_0;
    uint64_t local_sp_0;
    uint64_t rcx4_0;
    uint64_t rcx4_1;
    unsigned char var_37;
    uint64_t var_38;
    uint64_t rax_0;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t *var_27;
    uint64_t var_28;
    uint64_t *var_29;
    uint64_t var_30;
    uint64_t *var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_24;
    uint64_t var_11;
    uint64_t rdi2_1;
    uint64_t rcx4_2;
    uint64_t rcx4_3;
    unsigned char var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t local_sp_1;
    uint64_t var_15;
    uint64_t rdi2_2;
    uint64_t rcx4_4;
    uint64_t rcx4_5;
    unsigned char var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t local_sp_2;
    uint64_t rdi2_4;
    uint64_t var_19;
    uint64_t rdi2_3;
    uint64_t rcx4_6;
    uint64_t rcx4_7;
    unsigned char var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_state_0x82fc();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_8 = var_0 + (-88L);
    var_9 = (uint64_t)(uint32_t)rdi;
    var_10 = (uint64_t)(uint32_t)rcx;
    *(unsigned char *)4285065UL = (unsigned char)rdx;
    rcx4_0 = 18446744073709551615UL;
    rcx4_1 = 0UL;
    rcx4_2 = 18446744073709551615UL;
    rcx4_3 = 0UL;
    local_sp_1 = var_8;
    rcx4_4 = 18446744073709551615UL;
    rcx4_5 = 0UL;
    rdi2_4 = 1UL;
    rcx4_6 = 18446744073709551615UL;
    rcx4_7 = 0UL;
    if (*(unsigned char *)4285459UL == '\x00') {
        *(unsigned char *)(var_0 + (-72L)) = (unsigned char)'\x00';
    } else {
        var_11 = (uint64_t)var_7;
        rdi2_1 = *(uint64_t *)(var_0 | 8UL);
        while (rcx4_2 != 0UL)
            {
                var_12 = *(unsigned char *)rdi2_1;
                var_13 = rcx4_2 + (-1L);
                rcx4_2 = var_13;
                rcx4_3 = var_13;
                if (var_12 == '\x00') {
                    break;
                }
                rdi2_1 = rdi2_1 + var_11;
            }
        if ((18446744073709551614UL - (-2L)) > 6UL) {
            *(unsigned char *)(var_0 + (-72L)) = (unsigned char)'\x00';
        } else {
            var_14 = var_0 + (-96L);
            *(uint64_t *)var_14 = 4205258UL;
            indirect_placeholder();
            local_sp_1 = var_14;
        }
    }
    local_sp_2 = local_sp_1;
    if (*(unsigned char *)4285460UL == '\x00') {
        *(unsigned char *)(local_sp_1 + 3UL) = (unsigned char)'\x00';
    } else {
        var_15 = (uint64_t)var_7;
        rdi2_2 = *(uint64_t *)(local_sp_1 + 104UL);
        while (rcx4_4 != 0UL)
            {
                var_16 = *(unsigned char *)rdi2_2;
                var_17 = rcx4_4 + (-1L);
                rcx4_4 = var_17;
                rcx4_5 = var_17;
                if (var_16 == '\x00') {
                    break;
                }
                rdi2_2 = rdi2_2 + var_15;
            }
        if ((18446744073709551614UL - (-2L)) > 11UL) {
            *(unsigned char *)(local_sp_1 + 3UL) = (unsigned char)'\x00';
        } else {
            var_18 = local_sp_1 + (-8L);
            *(uint64_t *)var_18 = 4205288UL;
            indirect_placeholder();
            local_sp_2 = var_18;
        }
    }
    if (*(unsigned char *)4285456UL != '\x00') {
        var_19 = (uint64_t)var_7;
        rdi2_3 = *(uint64_t *)(local_sp_2 + 120UL);
        while (rcx4_6 != 0UL)
            {
                var_20 = *(unsigned char *)rdi2_3;
                var_21 = rcx4_6 + (-1L);
                rcx4_6 = var_21;
                rcx4_7 = var_21;
                if (var_20 == '\x00') {
                    break;
                }
                rdi2_3 = rdi2_3 + var_19;
            }
        rdi2_4 = ((18446744073709551614UL - (-2L)) > 11UL) ? (0UL - rcx4_7) : 14UL;
    }
    var_22 = local_sp_2 + (-8L);
    *(uint64_t *)var_22 = 4205015UL;
    var_23 = indirect_placeholder_3(rdi2_4);
    local_sp_0 = var_22;
    if (*(unsigned char *)4285456UL == '\x00') {
        *(unsigned char *)var_23 = (unsigned char)'\x00';
    } else {
        var_24 = local_sp_2 + (-16L);
        *(uint64_t *)var_24 = 4205316UL;
        indirect_placeholder();
        local_sp_0 = var_24;
    }
    var_25 = (*(unsigned char *)4285457UL == '\x00') ? 4267804UL : 4285064UL;
    var_26 = (rsi == 0UL) ? 4259995UL : rsi;
    var_27 = (uint64_t *)(local_sp_0 + (-16L));
    *var_27 = var_23;
    var_28 = *(uint64_t *)(local_sp_0 + 112UL);
    var_29 = (uint64_t *)(local_sp_0 + (-24L));
    *var_29 = var_28;
    var_30 = local_sp_0 + 3UL;
    var_31 = (uint64_t *)(local_sp_0 + (-32L));
    *var_31 = var_30;
    var_32 = local_sp_0 + 16UL;
    *(uint64_t *)(local_sp_0 + (-40L)) = var_32;
    *(uint64_t *)(local_sp_0 + (-48L)) = r9;
    *(uint64_t *)(local_sp_0 + (-56L)) = (uint64_t)*(uint32_t *)4285432UL;
    *(uint64_t *)(local_sp_0 + (-64L)) = r8;
    var_33 = local_sp_0 + 24UL;
    *(uint64_t *)(local_sp_0 + (-72L)) = 4205132UL;
    var_34 = indirect_placeholder_15(0UL, var_9, var_33, 4260672UL, var_26, var_25, var_10);
    if ((uint64_t)((uint32_t)var_34.field_0 + 1U) == 0UL) {
        var_40 = var_34.field_2;
        var_41 = var_34.field_1;
        *var_27 = 4205326UL;
        indirect_placeholder_6(var_41, var_40);
        abort();
    }
    var_35 = *(uint64_t *)var_32;
    var_36 = (uint64_t)var_7;
    rdi2_0 = var_35;
    while (rcx4_0 != 0UL)
        {
            var_37 = *(unsigned char *)rdi2_0;
            var_38 = rcx4_0 + (-1L);
            rcx4_0 = var_38;
            rcx4_1 = var_38;
            if (var_37 == '\x00') {
                break;
            }
            rdi2_0 = rdi2_0 + var_36;
        }
    rax_0 = (var_35 + (rcx4_1 ^ (-1L))) + (-1L);
    var_39 = rax_0 + (-1L);
    rax_0 = var_39;
    do {
        var_39 = rax_0 + (-1L);
        rax_0 = var_39;
    } while (*(unsigned char *)var_39 != ' ');
    *(unsigned char *)rax_0 = (unsigned char)'\x00';
    *var_27 = 4205200UL;
    indirect_placeholder();
    *var_29 = 4205210UL;
    indirect_placeholder();
    *var_31 = 4205218UL;
    indirect_placeholder();
    return;
}
