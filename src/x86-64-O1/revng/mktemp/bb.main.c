typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_28_ret_type;
struct indirect_placeholder_11_ret_type;
struct indirect_placeholder_10_ret_type;
struct indirect_placeholder_21_ret_type;
struct indirect_placeholder_20_ret_type;
struct indirect_placeholder_12_ret_type;
struct indirect_placeholder_13_ret_type;
struct indirect_placeholder_15_ret_type;
struct indirect_placeholder_14_ret_type;
struct indirect_placeholder_18_ret_type;
struct indirect_placeholder_17_ret_type;
struct indirect_placeholder_27_ret_type;
struct indirect_placeholder_30_ret_type;
struct indirect_placeholder_16_ret_type;
struct indirect_placeholder_31_ret_type;
struct indirect_placeholder_19_ret_type;
struct indirect_placeholder_28_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_11_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_10_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_21_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_20_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_12_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_13_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_15_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_14_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_18_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_17_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_27_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_30_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_16_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_31_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_19_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern void function_dispatcher(unsigned char *param_0);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_22(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_6(uint64_t param_0);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_29(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_9(uint64_t param_0);
extern void indirect_placeholder_23(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint32_t init_state_0x82fc(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern void indirect_placeholder_25(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_28_ret_type indirect_placeholder_28(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_11_ret_type indirect_placeholder_11(uint64_t param_0);
extern struct indirect_placeholder_10_ret_type indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_21_ret_type indirect_placeholder_21(uint64_t param_0);
extern struct indirect_placeholder_20_ret_type indirect_placeholder_20(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_12_ret_type indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_13_ret_type indirect_placeholder_13(uint64_t param_0);
extern struct indirect_placeholder_15_ret_type indirect_placeholder_15(uint64_t param_0);
extern struct indirect_placeholder_14_ret_type indirect_placeholder_14(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_18_ret_type indirect_placeholder_18(uint64_t param_0);
extern struct indirect_placeholder_17_ret_type indirect_placeholder_17(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_27_ret_type indirect_placeholder_27(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_30_ret_type indirect_placeholder_30(uint64_t param_0);
extern struct indirect_placeholder_16_ret_type indirect_placeholder_16(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_31_ret_type indirect_placeholder_31(uint64_t param_0);
extern struct indirect_placeholder_19_ret_type indirect_placeholder_19(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
void bb_main(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_28_ret_type var_14;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint32_t var_7;
    uint32_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t *var_11;
    uint64_t var_12;
    uint64_t *var_13;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint32_t var_19;
    uint64_t var_30;
    uint64_t rbp_7;
    uint64_t rbx_2;
    struct indirect_placeholder_11_ret_type var_81;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t local_sp_13;
    uint64_t r8_0;
    uint64_t r9_1;
    uint64_t r9_7;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_64;
    uint64_t spec_select;
    uint64_t r9_6;
    struct indirect_placeholder_21_ret_type var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t rbp_8;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t local_sp_2;
    uint64_t local_sp_0;
    uint32_t var_99;
    uint64_t r8_1;
    struct indirect_placeholder_13_ret_type var_100;
    uint64_t r12_0;
    uint64_t r8_7;
    uint64_t r9_0;
    uint64_t local_sp_3;
    uint64_t var_97;
    uint64_t var_98;
    uint64_t var_96;
    bool var_93;
    uint64_t var_94;
    uint64_t *var_95;
    uint64_t local_sp_1;
    uint64_t rbp_0;
    struct indirect_placeholder_15_ret_type var_103;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t var_107;
    uint64_t rax_0;
    uint64_t rbx_0;
    uint64_t r12_1;
    unsigned char var_87;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint32_t var_92;
    struct indirect_placeholder_18_ret_type var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    struct indirect_placeholder_17_ret_type var_63;
    uint64_t r8_6;
    uint64_t local_sp_14;
    uint64_t r9_2;
    uint64_t r12_2;
    uint64_t local_sp_12;
    uint64_t r8_2;
    uint64_t local_sp_11;
    uint64_t r9_3;
    uint64_t r8_3;
    uint64_t local_sp_4;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t rdi1_0;
    uint64_t rcx_0;
    uint64_t rcx_1;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t _pre_phi;
    uint64_t rbp_1;
    uint64_t rdi1_1;
    uint64_t rcx_2;
    uint64_t rcx_3;
    unsigned char var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t local_sp_6;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t local_sp_8;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint32_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t r13_2;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t rax_2;
    uint64_t rbp_5;
    uint64_t r12_3;
    uint64_t r9_5;
    uint64_t r8_5;
    uint64_t var_65;
    uint64_t var_66;
    struct indirect_placeholder_30_ret_type var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    struct indirect_placeholder_16_ret_type var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t rbp_6;
    uint64_t var_55;
    uint64_t var_76;
    uint64_t var_77;
    struct indirect_placeholder_31_ret_type var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    struct indirect_placeholder_19_ret_type var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_state_0x82fc();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_8 = (uint32_t)rdi;
    var_9 = (uint64_t)var_8;
    var_10 = *(uint64_t *)rsi;
    var_11 = (uint64_t *)(var_0 + (-96L));
    *var_11 = 4205148UL;
    indirect_placeholder_9(var_10);
    *(uint64_t *)(var_0 + (-104L)) = 4205163UL;
    indirect_placeholder();
    *(uint64_t *)(var_0 + (-112L)) = 4205173UL;
    indirect_placeholder();
    *(unsigned char *)(var_0 + (-98L)) = (unsigned char)'\x00';
    *(unsigned char *)(var_0 + (-97L)) = (unsigned char)'\x00';
    *(unsigned char *)(var_0 + (-100L)) = (unsigned char)'\x00';
    *(unsigned char *)(var_0 + (-99L)) = (unsigned char)'\x00';
    var_12 = var_0 + (-120L);
    var_13 = (uint64_t *)var_12;
    *var_13 = 4205243UL;
    var_14 = indirect_placeholder_28(4260027UL, var_9, rsi, 4262464UL, 0UL);
    var_15 = var_14.field_0;
    var_16 = var_14.field_1;
    var_17 = var_14.field_2;
    var_18 = var_14.field_3;
    var_19 = (uint32_t)var_15;
    r9_7 = var_17;
    r9_6 = var_17;
    r8_7 = var_18;
    r8_6 = var_18;
    r12_2 = 4259998UL;
    rdi1_0 = 0UL;
    rcx_0 = 18446744073709551615UL;
    rcx_1 = 0UL;
    rcx_2 = 18446744073709551615UL;
    rcx_3 = 0UL;
    local_sp_8 = var_12;
    var_27 = 4259983UL;
    r13_2 = 1UL;
    r9_5 = var_17;
    r8_5 = var_18;
    if ((uint64_t)(var_19 + 1U) == 0UL) {
        var_23 = *(uint32_t *)4285212UL;
        var_24 = (uint64_t)var_23;
        var_25 = (uint64_t)(var_8 - var_23);
        if (var_25 > 1UL) {
            *(uint64_t *)(var_0 + (-128L)) = 4205599UL;
            indirect_placeholder_27(0UL, 4260035UL, 0UL, 0UL, var_16, var_17, var_18);
            *(uint64_t *)(var_0 + (-136L)) = 4205609UL;
            indirect_placeholder_23(1UL, 1UL, rsi);
            abort();
        }
        if (var_25 == 0UL) {
            *var_11 = 4259983UL;
        } else {
            var_26 = *(uint64_t *)((uint64_t)((long)(var_24 << 32UL) >> (long)29UL) + rsi);
            *var_11 = var_26;
            var_27 = var_26;
            r13_2 = 0UL;
        }
        *(uint64_t *)(var_0 + (-128L)) = 4206512UL;
        var_28 = indirect_placeholder_6(var_27);
        var_29 = var_0 + (-136L);
        *(uint64_t *)var_29 = 4206528UL;
        indirect_placeholder();
        rbx_0 = var_28;
        rbp_1 = var_28 + 1UL;
        local_sp_6 = var_29;
        rbx_2 = var_28;
        if (var_28 == 0UL) {
            _pre_phi = (uint64_t)var_7;
        } else {
            var_30 = (uint64_t)var_7;
            _pre_phi = var_30;
            while (rcx_0 != 0UL)
                {
                    var_31 = (uint64_t)('\x00' - *(unsigned char *)rdi1_0);
                    var_32 = rcx_0 + (-1L);
                    rcx_0 = var_32;
                    rcx_1 = var_32;
                    if (var_31 == 0UL) {
                        break;
                    }
                    rdi1_0 = rdi1_0 + var_30;
                }
            rbp_1 = (rcx_1 ^ (-1L)) + (-1L);
        }
        rdi1_1 = rbp_1;
        rbp_8 = rbp_1;
        while (rcx_2 != 0UL)
            {
                var_33 = *(unsigned char *)rdi1_1;
                var_34 = rcx_2 + (-1L);
                rcx_2 = var_34;
                rcx_3 = var_34;
                if (var_33 == '\x00') {
                    break;
                }
                rdi1_1 = rdi1_1 + _pre_phi;
            }
        var_35 = 18446744073709551614UL - (-2L);
        *var_13 = var_35;
        if (var_35 == 0UL) {
            var_43 = rbp_1 - var_28;
            var_44 = local_sp_6 + (-8L);
            *(uint64_t *)var_44 = 4206259UL;
            var_45 = indirect_placeholder_22(var_28, var_43);
            rbp_7 = var_45;
            local_sp_13 = var_44;
            rbp_8 = var_45;
            rbp_0 = var_45;
            local_sp_14 = var_44;
            local_sp_12 = var_44;
            rax_2 = var_45;
            rbp_5 = var_45;
            rbp_6 = var_45;
            if (var_45 > 2UL) {
                if (r13_2 != 0UL) {
                    if (*(unsigned char *)(local_sp_6 + 4UL) == '\x00') {
                        var_64 = local_sp_6 + (-16L);
                        *(uint64_t *)var_64 = 4206309UL;
                        indirect_placeholder();
                        spec_select = (*(unsigned char *)var_45 == '\x00') ? 4259998UL : var_45;
                        r12_1 = spec_select;
                        r12_3 = spec_select;
                        local_sp_11 = var_64;
                        if (*(unsigned char *)var_28 == '/') {
                            *(uint64_t *)(local_sp_6 + (-24L)) = 4205842UL;
                            var_67 = indirect_placeholder_30(var_28);
                            var_68 = var_67.field_0;
                            var_69 = var_67.field_1;
                            var_70 = var_67.field_2;
                            var_71 = local_sp_6 + (-32L);
                            *(uint64_t *)var_71 = 4205870UL;
                            var_72 = indirect_placeholder_16(0UL, 4261952UL, 1UL, 0UL, var_68, var_69, var_70);
                            var_73 = var_72.field_0;
                            var_74 = var_72.field_2;
                            var_75 = var_72.field_3;
                            rax_0 = var_73;
                            r9_1 = var_74;
                            r8_1 = var_75;
                            local_sp_2 = var_71;
                            var_87 = *(unsigned char *)(local_sp_2 + 14UL);
                            var_88 = (uint64_t)var_87;
                            var_89 = *(uint64_t *)(local_sp_2 + 16UL);
                            var_90 = local_sp_2 + (-8L);
                            *(uint64_t *)var_90 = 4205896UL;
                            var_91 = indirect_placeholder_5(rbp_0, rax_0, var_89, var_88);
                            var_92 = (uint32_t)var_91;
                            r12_0 = r12_1;
                            r9_0 = r9_1;
                            r8_0 = r8_1;
                            local_sp_1 = var_90;
                            if ((int)var_92 >= (int)0U) {
                                if (*(unsigned char *)(local_sp_1 + 13UL) == '\x00') {
                                    *(uint64_t *)(local_sp_1 + (-8L)) = 4205970UL;
                                    var_103 = indirect_placeholder_15(rbx_0);
                                    var_104 = var_103.field_0;
                                    var_105 = var_103.field_1;
                                    var_106 = var_103.field_2;
                                    *(uint64_t *)(local_sp_1 + (-16L)) = 4205978UL;
                                    indirect_placeholder();
                                    var_107 = (uint64_t)*(uint32_t *)var_104;
                                    *(uint64_t *)(local_sp_1 + (-24L)) = 4206003UL;
                                    indirect_placeholder_14(0UL, 4262064UL, 0UL, var_107, var_104, var_105, var_106);
                                }
                                return;
                            }
                            var_93 = (var_87 == '\x00');
                            var_94 = local_sp_2 + (-16L);
                            var_95 = (uint64_t *)var_94;
                            local_sp_1 = var_94;
                            if (!var_93) {
                                *var_95 = 4205913UL;
                                indirect_placeholder();
                                return;
                            }
                            *var_95 = 4205942UL;
                            indirect_placeholder();
                            if ((uint64_t)var_92 == 0UL) {
                                var_96 = local_sp_2 + (-24L);
                                *(uint64_t *)var_96 = 4206111UL;
                                indirect_placeholder();
                                local_sp_0 = var_96;
                                *(unsigned char *)4285360UL = (unsigned char)'\x01';
                                var_97 = *(uint64_t *)4283968UL;
                                *(uint64_t *)(local_sp_0 + (-8L)) = 4206039UL;
                                var_98 = indirect_placeholder_6(var_97);
                                *(uint64_t *)(local_sp_0 + (-16L)) = 4206050UL;
                                indirect_placeholder();
                                var_99 = *(uint32_t *)var_98;
                                *(uint64_t *)(local_sp_0 + (-24L)) = 4206060UL;
                                var_100 = indirect_placeholder_13(r12_0);
                                if ((uint64_t)(uint32_t)var_98 != 0UL & *(unsigned char *)(local_sp_0 + (-11L)) == '\x00') {
                                    var_101 = var_100.field_1;
                                    var_102 = (uint64_t)var_99;
                                    *(uint64_t *)(local_sp_0 + (-32L)) = 4206098UL;
                                    indirect_placeholder_12(0UL, 4260088UL, 0UL, var_102, var_101, r9_0, r8_0);
                                }
                                return;
                            }
                            if (*(unsigned char *)(local_sp_1 + 13UL) == '\x00') {
                                *(uint64_t *)(local_sp_1 + (-8L)) = 4205970UL;
                                var_103 = indirect_placeholder_15(rbx_0);
                                var_104 = var_103.field_0;
                                var_105 = var_103.field_1;
                                var_106 = var_103.field_2;
                                *(uint64_t *)(local_sp_1 + (-16L)) = 4205978UL;
                                indirect_placeholder();
                                var_107 = (uint64_t)*(uint32_t *)var_104;
                                *(uint64_t *)(local_sp_1 + (-24L)) = 4206003UL;
                                indirect_placeholder_14(0UL, 4262064UL, 0UL, var_107, var_104, var_105, var_106);
                            }
                            return;
                        }
                    }
                    var_55 = local_sp_12 + (-8L);
                    *(uint64_t *)var_55 = 4205719UL;
                    indirect_placeholder();
                    local_sp_3 = var_55;
                    r9_2 = r9_6;
                    r8_2 = r8_6;
                    r9_3 = r9_6;
                    r8_3 = r8_6;
                    local_sp_4 = var_55;
                    rbp_5 = rbp_6;
                    if (rax_2 != 0UL) {
                        while (1U)
                            {
                                r9_3 = r9_2;
                                r8_3 = r8_2;
                                local_sp_4 = local_sp_3;
                                var_56 = local_sp_4 + (-8L);
                                *(uint64_t *)var_56 = 4205759UL;
                                var_57 = indirect_placeholder_6(var_28);
                                r12_3 = r12_2;
                                r9_5 = r9_3;
                                r8_5 = r8_3;
                                local_sp_11 = var_56;
                                if (var_28 == var_57) {
                                    break;
                                }
                                *(uint64_t *)(local_sp_4 + (-16L)) = 4205776UL;
                                var_58 = indirect_placeholder_18(var_28);
                                var_59 = var_58.field_0;
                                var_60 = var_58.field_1;
                                var_61 = var_58.field_2;
                                var_62 = local_sp_4 + (-24L);
                                *(uint64_t *)var_62 = 4205804UL;
                                var_63 = indirect_placeholder_17(0UL, 4261896UL, 1UL, 0UL, var_59, var_60, var_61);
                                r9_2 = var_63.field_2;
                                r8_2 = var_63.field_3;
                                local_sp_3 = var_62;
                                continue;
                            }
                    }
                    r12_2 = rax_2;
                    if (*(unsigned char *)rax_2 == '\x00') {
                        while (1U)
                            {
                                r9_3 = r9_2;
                                r8_3 = r8_2;
                                local_sp_4 = local_sp_3;
                                var_56 = local_sp_4 + (-8L);
                                *(uint64_t *)var_56 = 4205759UL;
                                var_57 = indirect_placeholder_6(var_28);
                                r12_3 = r12_2;
                                r9_5 = r9_3;
                                r8_5 = r8_3;
                                local_sp_11 = var_56;
                                if (var_28 == var_57) {
                                    break;
                                }
                                *(uint64_t *)(local_sp_4 + (-16L)) = 4205776UL;
                                var_58 = indirect_placeholder_18(var_28);
                                var_59 = var_58.field_0;
                                var_60 = var_58.field_1;
                                var_61 = var_58.field_2;
                                var_62 = local_sp_4 + (-24L);
                                *(uint64_t *)var_62 = 4205804UL;
                                var_63 = indirect_placeholder_17(0UL, 4261896UL, 1UL, 0UL, var_59, var_60, var_61);
                                r9_2 = var_63.field_2;
                                r8_2 = var_63.field_3;
                                local_sp_3 = var_62;
                                continue;
                            }
                    }
                    var_56 = local_sp_4 + (-8L);
                    *(uint64_t *)var_56 = 4205759UL;
                    var_57 = indirect_placeholder_6(var_28);
                    r12_3 = r12_2;
                    r9_5 = r9_3;
                    r8_5 = r8_3;
                    local_sp_11 = var_56;
                    if (var_28 != var_57) {
                        *(uint64_t *)(local_sp_4 + (-16L)) = 4205776UL;
                        var_58 = indirect_placeholder_18(var_28);
                        var_59 = var_58.field_0;
                        var_60 = var_58.field_1;
                        var_61 = var_58.field_2;
                        var_62 = local_sp_4 + (-24L);
                        *(uint64_t *)var_62 = 4205804UL;
                        var_63 = indirect_placeholder_17(0UL, 4261896UL, 1UL, 0UL, var_59, var_60, var_61);
                        r9_2 = var_63.field_2;
                        r8_2 = var_63.field_3;
                        local_sp_3 = var_62;
                        while (1U)
                            {
                                r9_3 = r9_2;
                                r8_3 = r8_2;
                                local_sp_4 = local_sp_3;
                                var_56 = local_sp_4 + (-8L);
                                *(uint64_t *)var_56 = 4205759UL;
                                var_57 = indirect_placeholder_6(var_28);
                                r12_3 = r12_2;
                                r9_5 = r9_3;
                                r8_5 = r8_3;
                                local_sp_11 = var_56;
                                if (var_28 == var_57) {
                                    break;
                                }
                                *(uint64_t *)(local_sp_4 + (-16L)) = 4205776UL;
                                var_58 = indirect_placeholder_18(var_28);
                                var_59 = var_58.field_0;
                                var_60 = var_58.field_1;
                                var_61 = var_58.field_2;
                                var_62 = local_sp_4 + (-24L);
                                *(uint64_t *)var_62 = 4205804UL;
                                var_63 = indirect_placeholder_17(0UL, 4261896UL, 1UL, 0UL, var_59, var_60, var_61);
                                r9_2 = var_63.field_2;
                                r8_2 = var_63.field_3;
                                local_sp_3 = var_62;
                                continue;
                            }
                    }
                    *(uint64_t *)(local_sp_11 + (-8L)) = 4206358UL;
                    var_65 = indirect_placeholder_29(0UL, r12_3, var_28);
                    var_66 = local_sp_11 + (-16L);
                    *(uint64_t *)var_66 = 4206369UL;
                    indirect_placeholder();
                    rbp_7 = rbp_5;
                    rbx_2 = var_65;
                    local_sp_13 = var_66;
                    r9_7 = r9_5;
                    r8_7 = r8_5;
                }
            } else {
                *(uint64_t *)(local_sp_14 + (-8L)) = 4205681UL;
                var_46 = indirect_placeholder_31(var_28);
                var_47 = var_46.field_0;
                var_48 = var_46.field_1;
                var_49 = var_46.field_2;
                var_50 = local_sp_14 + (-16L);
                *(uint64_t *)var_50 = 4205709UL;
                var_51 = indirect_placeholder_19(0UL, 4260054UL, 1UL, 0UL, var_47, var_48, var_49);
                var_52 = var_51.field_0;
                var_53 = var_51.field_2;
                var_54 = var_51.field_3;
                r9_6 = var_53;
                r8_6 = var_54;
                local_sp_12 = var_50;
                rax_2 = var_52;
                rbp_6 = rbp_8;
                var_55 = local_sp_12 + (-8L);
                *(uint64_t *)var_55 = 4205719UL;
                indirect_placeholder();
                local_sp_3 = var_55;
                r9_2 = r9_6;
                r8_2 = r8_6;
                r9_3 = r9_6;
                r8_3 = r8_6;
                local_sp_4 = var_55;
                rbp_5 = rbp_6;
                if (rax_2 != 0UL) {
                    while (1U)
                        {
                            r9_3 = r9_2;
                            r8_3 = r8_2;
                            local_sp_4 = local_sp_3;
                            var_56 = local_sp_4 + (-8L);
                            *(uint64_t *)var_56 = 4205759UL;
                            var_57 = indirect_placeholder_6(var_28);
                            r12_3 = r12_2;
                            r9_5 = r9_3;
                            r8_5 = r8_3;
                            local_sp_11 = var_56;
                            if (var_28 == var_57) {
                                break;
                            }
                            *(uint64_t *)(local_sp_4 + (-16L)) = 4205776UL;
                            var_58 = indirect_placeholder_18(var_28);
                            var_59 = var_58.field_0;
                            var_60 = var_58.field_1;
                            var_61 = var_58.field_2;
                            var_62 = local_sp_4 + (-24L);
                            *(uint64_t *)var_62 = 4205804UL;
                            var_63 = indirect_placeholder_17(0UL, 4261896UL, 1UL, 0UL, var_59, var_60, var_61);
                            r9_2 = var_63.field_2;
                            r8_2 = var_63.field_3;
                            local_sp_3 = var_62;
                            continue;
                        }
                }
                r12_2 = rax_2;
                if (*(unsigned char *)rax_2 != '\x00') {
                    while (1U)
                        {
                            r9_3 = r9_2;
                            r8_3 = r8_2;
                            local_sp_4 = local_sp_3;
                            var_56 = local_sp_4 + (-8L);
                            *(uint64_t *)var_56 = 4205759UL;
                            var_57 = indirect_placeholder_6(var_28);
                            r12_3 = r12_2;
                            r9_5 = r9_3;
                            r8_5 = r8_3;
                            local_sp_11 = var_56;
                            if (var_28 == var_57) {
                                break;
                            }
                            *(uint64_t *)(local_sp_4 + (-16L)) = 4205776UL;
                            var_58 = indirect_placeholder_18(var_28);
                            var_59 = var_58.field_0;
                            var_60 = var_58.field_1;
                            var_61 = var_58.field_2;
                            var_62 = local_sp_4 + (-24L);
                            *(uint64_t *)var_62 = 4205804UL;
                            var_63 = indirect_placeholder_17(0UL, 4261896UL, 1UL, 0UL, var_59, var_60, var_61);
                            r9_2 = var_63.field_2;
                            r8_2 = var_63.field_3;
                            local_sp_3 = var_62;
                            continue;
                        }
                }
                var_56 = local_sp_4 + (-8L);
                *(uint64_t *)var_56 = 4205759UL;
                var_57 = indirect_placeholder_6(var_28);
                r12_3 = r12_2;
                r9_5 = r9_3;
                r8_5 = r8_3;
                local_sp_11 = var_56;
                if (var_28 != var_57) {
                    *(uint64_t *)(local_sp_4 + (-16L)) = 4205776UL;
                    var_58 = indirect_placeholder_18(var_28);
                    var_59 = var_58.field_0;
                    var_60 = var_58.field_1;
                    var_61 = var_58.field_2;
                    var_62 = local_sp_4 + (-24L);
                    *(uint64_t *)var_62 = 4205804UL;
                    var_63 = indirect_placeholder_17(0UL, 4261896UL, 1UL, 0UL, var_59, var_60, var_61);
                    r9_2 = var_63.field_2;
                    r8_2 = var_63.field_3;
                    local_sp_3 = var_62;
                    while (1U)
                        {
                            r9_3 = r9_2;
                            r8_3 = r8_2;
                            local_sp_4 = local_sp_3;
                            var_56 = local_sp_4 + (-8L);
                            *(uint64_t *)var_56 = 4205759UL;
                            var_57 = indirect_placeholder_6(var_28);
                            r12_3 = r12_2;
                            r9_5 = r9_3;
                            r8_5 = r8_3;
                            local_sp_11 = var_56;
                            if (var_28 == var_57) {
                                break;
                            }
                            *(uint64_t *)(local_sp_4 + (-16L)) = 4205776UL;
                            var_58 = indirect_placeholder_18(var_28);
                            var_59 = var_58.field_0;
                            var_60 = var_58.field_1;
                            var_61 = var_58.field_2;
                            var_62 = local_sp_4 + (-24L);
                            *(uint64_t *)var_62 = 4205804UL;
                            var_63 = indirect_placeholder_17(0UL, 4261896UL, 1UL, 0UL, var_59, var_60, var_61);
                            r9_2 = var_63.field_2;
                            r8_2 = var_63.field_3;
                            local_sp_3 = var_62;
                            continue;
                        }
                }
                *(uint64_t *)(local_sp_11 + (-8L)) = 4206358UL;
                var_65 = indirect_placeholder_29(0UL, r12_3, var_28);
                var_66 = local_sp_11 + (-16L);
                *(uint64_t *)var_66 = 4206369UL;
                indirect_placeholder();
                rbp_7 = rbp_5;
                rbx_2 = var_65;
                local_sp_13 = var_66;
                r9_7 = r9_5;
                r8_7 = r8_5;
                var_76 = local_sp_13 + (-8L);
                *(uint64_t *)var_76 = 4206380UL;
                var_77 = indirect_placeholder_6(rbx_2);
                r8_0 = r8_7;
                r9_1 = r9_7;
                local_sp_2 = var_76;
                r8_1 = r8_7;
                r12_0 = var_77;
                r9_0 = r9_7;
                rbp_0 = rbp_7;
                rax_0 = var_77;
                rbx_0 = rbx_2;
                r12_1 = var_77;
                if (*(unsigned char *)(local_sp_13 + 7UL) != '\x00') {
                    var_78 = (uint64_t)*(unsigned char *)(local_sp_13 + 6UL);
                    var_79 = *(uint64_t *)(local_sp_13 + 8UL);
                    *(uint64_t *)(local_sp_13 + (-16L)) = 4206415UL;
                    var_80 = indirect_placeholder_5(rbp_7, var_77, var_79, var_78);
                    if ((uint64_t)(uint32_t)var_80 == 0UL) {
                        var_86 = local_sp_13 + (-24L);
                        *(uint64_t *)var_86 = 4206013UL;
                        indirect_placeholder();
                        local_sp_0 = var_86;
                        if (*(unsigned char *)(local_sp_13 + (-10L)) == '\x00') {
                            return;
                        }
                    }
                    if (*(unsigned char *)(local_sp_13 + (-3L)) == '\x00') {
                        *(uint64_t *)(local_sp_13 + (-24L)) = 4206449UL;
                        var_81 = indirect_placeholder_11(rbx_2);
                        var_82 = var_81.field_0;
                        var_83 = var_81.field_1;
                        var_84 = var_81.field_2;
                        *(uint64_t *)(local_sp_13 + (-32L)) = 4206457UL;
                        indirect_placeholder();
                        var_85 = (uint64_t)*(uint32_t *)var_82;
                        *(uint64_t *)(local_sp_13 + (-40L)) = 4206482UL;
                        indirect_placeholder_10(0UL, 4262016UL, 0UL, var_85, var_82, var_83, var_84);
                    }
                    return;
                }
                var_87 = *(unsigned char *)(local_sp_2 + 14UL);
                var_88 = (uint64_t)var_87;
                var_89 = *(uint64_t *)(local_sp_2 + 16UL);
                var_90 = local_sp_2 + (-8L);
                *(uint64_t *)var_90 = 4205896UL;
                var_91 = indirect_placeholder_5(rbp_0, rax_0, var_89, var_88);
                var_92 = (uint32_t)var_91;
                r12_0 = r12_1;
                r9_0 = r9_1;
                r8_0 = r8_1;
                local_sp_1 = var_90;
                if ((int)var_92 >= (int)0U) {
                    if (*(unsigned char *)(local_sp_1 + 13UL) == '\x00') {
                        *(uint64_t *)(local_sp_1 + (-8L)) = 4205970UL;
                        var_103 = indirect_placeholder_15(rbx_0);
                        var_104 = var_103.field_0;
                        var_105 = var_103.field_1;
                        var_106 = var_103.field_2;
                        *(uint64_t *)(local_sp_1 + (-16L)) = 4205978UL;
                        indirect_placeholder();
                        var_107 = (uint64_t)*(uint32_t *)var_104;
                        *(uint64_t *)(local_sp_1 + (-24L)) = 4206003UL;
                        indirect_placeholder_14(0UL, 4262064UL, 0UL, var_107, var_104, var_105, var_106);
                    }
                    return;
                }
                var_93 = (var_87 == '\x00');
                var_94 = local_sp_2 + (-16L);
                var_95 = (uint64_t *)var_94;
                local_sp_1 = var_94;
                if (!var_93) {
                    *var_95 = 4205913UL;
                    indirect_placeholder();
                    return;
                }
                *var_95 = 4205942UL;
                indirect_placeholder();
                if ((uint64_t)var_92 != 0UL) {
                    if (*(unsigned char *)(local_sp_1 + 13UL) == '\x00') {
                        *(uint64_t *)(local_sp_1 + (-8L)) = 4205970UL;
                        var_103 = indirect_placeholder_15(rbx_0);
                        var_104 = var_103.field_0;
                        var_105 = var_103.field_1;
                        var_106 = var_103.field_2;
                        *(uint64_t *)(local_sp_1 + (-16L)) = 4205978UL;
                        indirect_placeholder();
                        var_107 = (uint64_t)*(uint32_t *)var_104;
                        *(uint64_t *)(local_sp_1 + (-24L)) = 4206003UL;
                        indirect_placeholder_14(0UL, 4262064UL, 0UL, var_107, var_104, var_105, var_106);
                    }
                    return;
                }
                var_96 = local_sp_2 + (-24L);
                *(uint64_t *)var_96 = 4206111UL;
                indirect_placeholder();
                local_sp_0 = var_96;
            }
        } else {
            var_36 = var_0 + (-144L);
            *(uint64_t *)var_36 = 4206236UL;
            var_37 = indirect_placeholder_6(rbp_1);
            local_sp_6 = var_36;
            if (rbp_1 != var_37) {
                *(uint64_t *)(var_0 + (-152L)) = 4205645UL;
                var_38 = indirect_placeholder_21(rbp_1);
                var_39 = var_38.field_0;
                var_40 = var_38.field_1;
                var_41 = var_38.field_2;
                var_42 = var_0 + (-160L);
                *(uint64_t *)var_42 = 4205673UL;
                indirect_placeholder_20(0UL, 4261848UL, 1UL, 0UL, var_39, var_40, var_41);
                local_sp_14 = var_42;
                *(uint64_t *)(local_sp_14 + (-8L)) = 4205681UL;
                var_46 = indirect_placeholder_31(var_28);
                var_47 = var_46.field_0;
                var_48 = var_46.field_1;
                var_49 = var_46.field_2;
                var_50 = local_sp_14 + (-16L);
                *(uint64_t *)var_50 = 4205709UL;
                var_51 = indirect_placeholder_19(0UL, 4260054UL, 1UL, 0UL, var_47, var_48, var_49);
                var_52 = var_51.field_0;
                var_53 = var_51.field_2;
                var_54 = var_51.field_3;
                r9_6 = var_53;
                r8_6 = var_54;
                local_sp_12 = var_50;
                rax_2 = var_52;
                rbp_6 = rbp_8;
                var_55 = local_sp_12 + (-8L);
                *(uint64_t *)var_55 = 4205719UL;
                indirect_placeholder();
                local_sp_3 = var_55;
                r9_2 = r9_6;
                r8_2 = r8_6;
                r9_3 = r9_6;
                r8_3 = r8_6;
                local_sp_4 = var_55;
                rbp_5 = rbp_6;
                if (rax_2 != 0UL) {
                    while (1U)
                        {
                            r9_3 = r9_2;
                            r8_3 = r8_2;
                            local_sp_4 = local_sp_3;
                            var_56 = local_sp_4 + (-8L);
                            *(uint64_t *)var_56 = 4205759UL;
                            var_57 = indirect_placeholder_6(var_28);
                            r12_3 = r12_2;
                            r9_5 = r9_3;
                            r8_5 = r8_3;
                            local_sp_11 = var_56;
                            if (var_28 == var_57) {
                                break;
                            }
                            *(uint64_t *)(local_sp_4 + (-16L)) = 4205776UL;
                            var_58 = indirect_placeholder_18(var_28);
                            var_59 = var_58.field_0;
                            var_60 = var_58.field_1;
                            var_61 = var_58.field_2;
                            var_62 = local_sp_4 + (-24L);
                            *(uint64_t *)var_62 = 4205804UL;
                            var_63 = indirect_placeholder_17(0UL, 4261896UL, 1UL, 0UL, var_59, var_60, var_61);
                            r9_2 = var_63.field_2;
                            r8_2 = var_63.field_3;
                            local_sp_3 = var_62;
                            continue;
                        }
                }
                r12_2 = rax_2;
                if (*(unsigned char *)rax_2 == '\x00') {
                    while (1U)
                        {
                            r9_3 = r9_2;
                            r8_3 = r8_2;
                            local_sp_4 = local_sp_3;
                            var_56 = local_sp_4 + (-8L);
                            *(uint64_t *)var_56 = 4205759UL;
                            var_57 = indirect_placeholder_6(var_28);
                            r12_3 = r12_2;
                            r9_5 = r9_3;
                            r8_5 = r8_3;
                            local_sp_11 = var_56;
                            if (var_28 == var_57) {
                                break;
                            }
                            *(uint64_t *)(local_sp_4 + (-16L)) = 4205776UL;
                            var_58 = indirect_placeholder_18(var_28);
                            var_59 = var_58.field_0;
                            var_60 = var_58.field_1;
                            var_61 = var_58.field_2;
                            var_62 = local_sp_4 + (-24L);
                            *(uint64_t *)var_62 = 4205804UL;
                            var_63 = indirect_placeholder_17(0UL, 4261896UL, 1UL, 0UL, var_59, var_60, var_61);
                            r9_2 = var_63.field_2;
                            r8_2 = var_63.field_3;
                            local_sp_3 = var_62;
                            continue;
                        }
                }
                var_56 = local_sp_4 + (-8L);
                *(uint64_t *)var_56 = 4205759UL;
                var_57 = indirect_placeholder_6(var_28);
                r12_3 = r12_2;
                r9_5 = r9_3;
                r8_5 = r8_3;
                local_sp_11 = var_56;
                if (var_28 != var_57) {
                    *(uint64_t *)(local_sp_4 + (-16L)) = 4205776UL;
                    var_58 = indirect_placeholder_18(var_28);
                    var_59 = var_58.field_0;
                    var_60 = var_58.field_1;
                    var_61 = var_58.field_2;
                    var_62 = local_sp_4 + (-24L);
                    *(uint64_t *)var_62 = 4205804UL;
                    var_63 = indirect_placeholder_17(0UL, 4261896UL, 1UL, 0UL, var_59, var_60, var_61);
                    r9_2 = var_63.field_2;
                    r8_2 = var_63.field_3;
                    local_sp_3 = var_62;
                    while (1U)
                        {
                            r9_3 = r9_2;
                            r8_3 = r8_2;
                            local_sp_4 = local_sp_3;
                            var_56 = local_sp_4 + (-8L);
                            *(uint64_t *)var_56 = 4205759UL;
                            var_57 = indirect_placeholder_6(var_28);
                            r12_3 = r12_2;
                            r9_5 = r9_3;
                            r8_5 = r8_3;
                            local_sp_11 = var_56;
                            if (var_28 == var_57) {
                                break;
                            }
                            *(uint64_t *)(local_sp_4 + (-16L)) = 4205776UL;
                            var_58 = indirect_placeholder_18(var_28);
                            var_59 = var_58.field_0;
                            var_60 = var_58.field_1;
                            var_61 = var_58.field_2;
                            var_62 = local_sp_4 + (-24L);
                            *(uint64_t *)var_62 = 4205804UL;
                            var_63 = indirect_placeholder_17(0UL, 4261896UL, 1UL, 0UL, var_59, var_60, var_61);
                            r9_2 = var_63.field_2;
                            r8_2 = var_63.field_3;
                            local_sp_3 = var_62;
                            continue;
                        }
                }
                *(uint64_t *)(local_sp_11 + (-8L)) = 4206358UL;
                var_65 = indirect_placeholder_29(0UL, r12_3, var_28);
                var_66 = local_sp_11 + (-16L);
                *(uint64_t *)var_66 = 4206369UL;
                indirect_placeholder();
                rbp_7 = rbp_5;
                rbx_2 = var_65;
                local_sp_13 = var_66;
                r9_7 = r9_5;
                r8_7 = r8_5;
                var_76 = local_sp_13 + (-8L);
                *(uint64_t *)var_76 = 4206380UL;
                var_77 = indirect_placeholder_6(rbx_2);
                r8_0 = r8_7;
                r9_1 = r9_7;
                local_sp_2 = var_76;
                r8_1 = r8_7;
                r12_0 = var_77;
                r9_0 = r9_7;
                rbp_0 = rbp_7;
                rax_0 = var_77;
                rbx_0 = rbx_2;
                r12_1 = var_77;
                if (*(unsigned char *)(local_sp_13 + 7UL) != '\x00') {
                    var_78 = (uint64_t)*(unsigned char *)(local_sp_13 + 6UL);
                    var_79 = *(uint64_t *)(local_sp_13 + 8UL);
                    *(uint64_t *)(local_sp_13 + (-16L)) = 4206415UL;
                    var_80 = indirect_placeholder_5(rbp_7, var_77, var_79, var_78);
                    if ((uint64_t)(uint32_t)var_80 == 0UL) {
                        var_86 = local_sp_13 + (-24L);
                        *(uint64_t *)var_86 = 4206013UL;
                        indirect_placeholder();
                        local_sp_0 = var_86;
                        if (*(unsigned char *)(local_sp_13 + (-10L)) == '\x00') {
                            return;
                        }
                    }
                    if (*(unsigned char *)(local_sp_13 + (-3L)) == '\x00') {
                        *(uint64_t *)(local_sp_13 + (-24L)) = 4206449UL;
                        var_81 = indirect_placeholder_11(rbx_2);
                        var_82 = var_81.field_0;
                        var_83 = var_81.field_1;
                        var_84 = var_81.field_2;
                        *(uint64_t *)(local_sp_13 + (-32L)) = 4206457UL;
                        indirect_placeholder();
                        var_85 = (uint64_t)*(uint32_t *)var_82;
                        *(uint64_t *)(local_sp_13 + (-40L)) = 4206482UL;
                        indirect_placeholder_10(0UL, 4262016UL, 0UL, var_85, var_82, var_83, var_84);
                    }
                    return;
                }
                var_87 = *(unsigned char *)(local_sp_2 + 14UL);
                var_88 = (uint64_t)var_87;
                var_89 = *(uint64_t *)(local_sp_2 + 16UL);
                var_90 = local_sp_2 + (-8L);
                *(uint64_t *)var_90 = 4205896UL;
                var_91 = indirect_placeholder_5(rbp_0, rax_0, var_89, var_88);
                var_92 = (uint32_t)var_91;
                r12_0 = r12_1;
                r9_0 = r9_1;
                r8_0 = r8_1;
                local_sp_1 = var_90;
                if ((int)var_92 >= (int)0U) {
                    if (*(unsigned char *)(local_sp_1 + 13UL) == '\x00') {
                        *(uint64_t *)(local_sp_1 + (-8L)) = 4205970UL;
                        var_103 = indirect_placeholder_15(rbx_0);
                        var_104 = var_103.field_0;
                        var_105 = var_103.field_1;
                        var_106 = var_103.field_2;
                        *(uint64_t *)(local_sp_1 + (-16L)) = 4205978UL;
                        indirect_placeholder();
                        var_107 = (uint64_t)*(uint32_t *)var_104;
                        *(uint64_t *)(local_sp_1 + (-24L)) = 4206003UL;
                        indirect_placeholder_14(0UL, 4262064UL, 0UL, var_107, var_104, var_105, var_106);
                    }
                    return;
                }
                var_93 = (var_87 == '\x00');
                var_94 = local_sp_2 + (-16L);
                var_95 = (uint64_t *)var_94;
                local_sp_1 = var_94;
                if (!var_93) {
                    *var_95 = 4205913UL;
                    indirect_placeholder();
                    return;
                }
                *var_95 = 4205942UL;
                indirect_placeholder();
                if ((uint64_t)var_92 != 0UL) {
                    if (*(unsigned char *)(local_sp_1 + 13UL) == '\x00') {
                        *(uint64_t *)(local_sp_1 + (-8L)) = 4205970UL;
                        var_103 = indirect_placeholder_15(rbx_0);
                        var_104 = var_103.field_0;
                        var_105 = var_103.field_1;
                        var_106 = var_103.field_2;
                        *(uint64_t *)(local_sp_1 + (-16L)) = 4205978UL;
                        indirect_placeholder();
                        var_107 = (uint64_t)*(uint32_t *)var_104;
                        *(uint64_t *)(local_sp_1 + (-24L)) = 4206003UL;
                        indirect_placeholder_14(0UL, 4262064UL, 0UL, var_107, var_104, var_105, var_106);
                    }
                    return;
                }
                var_96 = local_sp_2 + (-24L);
                *(uint64_t *)var_96 = 4206111UL;
                indirect_placeholder();
                local_sp_0 = var_96;
            }
        }
    } else {
        if ((int)var_19 > (int)128U) {
            *(uint64_t *)(local_sp_8 + (-8L)) = 4205430UL;
            indirect_placeholder_23(1UL, 1UL, rsi);
            abort();
        }
        if ((int)var_19 > (int)85U) {
            if ((uint64_t)(var_19 + (-86)) > 42UL) {
                function_dispatcher((unsigned char *)(0UL));
                return;
            }
        }
        if ((uint64_t)(var_19 + 131U) != 0UL) {
            if ((uint64_t)(var_19 + 130U) == 0UL) {
                *(uint64_t *)(var_0 + (-128L)) = 4205375UL;
                indirect_placeholder_23(1UL, 0UL, rsi);
                abort();
            }
        }
        *(uint64_t *)(var_0 + (-136L)) = 0UL;
        var_20 = *(uint64_t *)4285088UL;
        var_21 = *(uint64_t *)4283968UL;
        *(uint64_t *)(var_0 + (-144L)) = 4205348UL;
        indirect_placeholder_25(0UL, 4259935UL, var_21, 4259976UL, var_20, 4260003UL, 4260014UL);
        var_22 = var_0 + (-152L);
        *(uint64_t *)var_22 = 4205358UL;
        indirect_placeholder();
        local_sp_8 = var_22;
    }
}
