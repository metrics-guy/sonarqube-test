typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_16_ret_type;
struct indirect_placeholder_16_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_10(uint64_t param_0);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_6(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern void indirect_placeholder_13(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_16_ret_type indirect_placeholder_16(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
uint64_t bb_getndelim2(uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t rcx, uint64_t r8, uint64_t r9) {
    uint64_t local_sp_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t r14_2;
    uint64_t var_32;
    uint32_t var_33;
    uint64_t r15_0;
    uint64_t local_sp_7;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t r12_1;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t rax_5;
    uint64_t r13_0;
    uint64_t var_39;
    uint64_t rbp_4;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t *var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t r15_1;
    uint64_t r12_4;
    uint64_t var_45;
    uint64_t *var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t r12_0;
    uint64_t var_50;
    uint64_t rax_3;
    uint64_t local_sp_4;
    uint64_t var_51;
    uint64_t r14_4;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t rbp_0;
    uint64_t r14_3;
    uint64_t r12_2;
    uint64_t r14_0;
    uint64_t rax_0;
    uint64_t local_sp_1;
    uint64_t r13_1;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    unsigned char var_60;
    uint64_t var_61;
    uint64_t rax_1;
    uint64_t local_sp_2;
    uint64_t var_59;
    uint64_t rbp_1;
    uint64_t r12_3;
    uint64_t rax_2;
    uint64_t local_sp_3;
    uint64_t rbp_2;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    struct indirect_placeholder_16_ret_type var_65;
    uint64_t var_66;
    uint64_t r13_2;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t rax_6;
    uint64_t local_sp_6;
    uint64_t *var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    unsigned char var_18;
    uint64_t var_19;
    uint64_t var_20;
    unsigned char var_21;
    uint64_t var_22;
    uint32_t *var_23;
    uint32_t var_24;
    uint64_t var_25;
    bool var_26;
    uint32_t *var_27;
    uint32_t var_28;
    uint64_t rax_4;
    uint64_t var_29;
    uint64_t var_30;
    bool var_31;
    uint64_t local_sp_8;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_8 = var_0 + (-136L);
    *(uint64_t *)(var_0 + (-88L)) = rdi;
    *(uint64_t *)(var_0 + (-80L)) = rsi;
    *(uint64_t *)(var_0 + (-96L)) = rdx;
    *(uint64_t *)(var_0 + (-128L)) = rcx;
    var_9 = var_0 + (-120L);
    *(uint32_t *)var_9 = (uint32_t)r8;
    *(uint32_t *)(var_0 + (-104L)) = (uint32_t)r9;
    var_10 = *(uint64_t *)rdi;
    *(uint64_t *)(var_0 + (-112L)) = var_10;
    r14_2 = *(uint64_t *)rsi;
    r15_1 = var_3;
    r12_4 = var_5;
    rax_6 = 18446744073709551615UL;
    local_sp_6 = var_8;
    if (var_10 != 0UL) {
        var_11 = (rcx < 64UL) ? rcx : 64UL;
        var_12 = var_0 + (-144L);
        *(uint64_t *)var_12 = 4209689UL;
        var_13 = indirect_placeholder_10(var_11);
        *(uint64_t *)var_9 = var_13;
        r14_2 = var_11;
        local_sp_6 = var_12;
        if (var_13 == 0UL) {
            return rax_6;
        }
    }
    var_14 = (uint64_t *)(local_sp_6 + 40UL);
    var_15 = *var_14;
    var_16 = r14_2 - var_15;
    var_17 = helper_cc_compute_c_wrapper(var_16, var_15, var_7, 17U);
    local_sp_7 = local_sp_6;
    r14_4 = r14_2;
    r14_3 = r14_2;
    r13_2 = var_16;
    local_sp_8 = local_sp_6;
    if (var_17 == 0UL) {
        **(uint64_t **)(local_sp_8 + 48UL) = *(uint64_t *)(local_sp_8 + 24UL);
        **(uint64_t **)(local_sp_8 + 56UL) = r14_4;
        return rax_6;
    }
    var_18 = (unsigned char)(var_16 == 0UL);
    var_19 = *(uint64_t *)(local_sp_6 + 8UL);
    var_20 = helper_cc_compute_c_wrapper(r14_2 - var_19, var_19, var_7, 17U);
    var_21 = var_18 & ((unsigned char)var_20 ^ '\x01');
    var_22 = (uint64_t)var_21;
    *(unsigned char *)(local_sp_6 + 23UL) = var_21;
    if ((var_22 & 1UL) == 0UL) {
        return;
    }
    var_23 = (uint32_t *)(local_sp_6 + 16UL);
    var_24 = *var_23;
    var_25 = (uint64_t)var_24;
    var_26 = ((uint64_t)(var_24 + 1U) == 0UL);
    var_27 = (uint32_t *)(local_sp_6 + 32UL);
    var_28 = *var_27;
    rax_4 = var_25;
    if (var_26) {
        var_29 = (uint64_t)var_28;
        *var_23 = var_28;
        rax_4 = var_29;
    } else {
        *var_27 = ((var_28 == 4294967295U) ? var_24 : var_28);
    }
    rbp_4 = *(uint64_t *)(local_sp_6 + 24UL) + *var_14;
    rax_5 = rax_4;
    while (1U)
        {
            var_30 = local_sp_7 + (-8L);
            *(uint64_t *)var_30 = 4210111UL;
            indirect_placeholder_1();
            var_31 = (rax_5 == 0UL);
            local_sp_0 = var_30;
            r13_0 = r13_2;
            r14_4 = r14_3;
            rbp_0 = rbp_4;
            r12_2 = r12_4;
            r14_0 = r14_3;
            if (var_31) {
                var_39 = local_sp_7 + (-16L);
                *(uint64_t *)var_39 = 4209793UL;
                indirect_placeholder_1();
                *(uint32_t *)(local_sp_7 + 20UL) = 0U;
                var_40 = ((uint64_t)((uint32_t)r15_1 & (-256)) | ((uint64_t)*(uint32_t *)local_sp_7 == 0UL)) | ((uint64_t)*(uint32_t *)(local_sp_7 + 16UL) == 0UL);
                *(uint64_t *)(local_sp_7 + 56UL) = 1UL;
                r15_0 = var_40;
                local_sp_0 = var_39;
            } else {
                var_32 = (uint64_t)*(unsigned char *)(local_sp_7 + 15UL);
                var_33 = *(uint32_t *)(local_sp_7 + 8UL);
                r15_0 = var_32;
                var_34 = *(uint64_t *)(local_sp_7 + 64UL);
                var_35 = (uint64_t)*(uint32_t *)(local_sp_7 + 24UL);
                var_36 = (uint64_t)var_33;
                var_37 = local_sp_7 + (-16L);
                *(uint64_t *)var_37 = 4209742UL;
                var_38 = indirect_placeholder_6(var_35, rax_5, var_36, var_34);
                r15_0 = (uint64_t)*(unsigned char *)(local_sp_7 + 7UL);
                local_sp_0 = var_37;
                if (var_33 != 4294967295U & var_38 == 0UL) {
                    *(uint64_t *)(local_sp_7 + 56UL) = ((var_38 - rax_5) + 1UL);
                    r15_0 = 1UL;
                }
            }
            var_41 = *(uint64_t *)(local_sp_0 + 72UL) + 1UL;
            var_42 = (uint64_t *)(local_sp_0 + 8UL);
            var_43 = *var_42;
            var_44 = helper_cc_compute_c_wrapper(r14_3 - var_43, var_43, var_7, 17U);
            rax_0 = var_41;
            local_sp_1 = local_sp_0;
            r15_1 = r15_0;
            local_sp_8 = local_sp_0;
            if ((var_44 != 0UL) && (var_41 > r13_2)) {
                r14_4 = r14_0;
                r14_3 = r14_0;
                r13_1 = r13_0;
                rax_1 = rax_0;
                local_sp_2 = local_sp_1;
                rbp_1 = rbp_0;
                r12_3 = r12_2;
                rax_2 = rax_0;
                local_sp_3 = local_sp_1;
                if (r13_0 <= 1UL) {
                    var_56 = r13_0 + (-1L);
                    var_57 = *(uint64_t *)(local_sp_1 + 72UL);
                    var_58 = (var_57 > var_56) ? var_56 : var_57;
                    r12_3 = var_58;
                    if (var_31) {
                        var_60 = *(unsigned char *)(local_sp_1 + 36UL);
                        var_61 = (uint64_t)var_60;
                        *(unsigned char *)rbp_0 = var_60;
                        rax_1 = var_61;
                    } else {
                        var_59 = local_sp_1 + (-8L);
                        *(uint64_t *)var_59 = 4210047UL;
                        indirect_placeholder_1();
                        local_sp_2 = var_59;
                    }
                    r13_1 = r13_0 - var_58;
                    rbp_1 = rbp_0 + var_58;
                    rax_2 = rax_1;
                    local_sp_3 = local_sp_2;
                }
                r12_4 = r12_3;
                rax_3 = rax_2;
                local_sp_4 = local_sp_3;
                rbp_2 = rbp_1;
                r13_2 = r13_1;
                if (!var_31) {
                    var_62 = *(uint64_t *)(local_sp_3 + 72UL);
                    var_63 = *(uint64_t *)(local_sp_3 + 144UL);
                    var_64 = local_sp_3 + (-8L);
                    *(uint64_t *)var_64 = 4210076UL;
                    var_65 = indirect_placeholder_16(r13_1, rax_5, var_63, rbp_1, r12_3, var_62);
                    var_66 = var_65.field_0;
                    rbp_2 = var_65.field_1;
                    rax_3 = var_66;
                    local_sp_4 = var_64;
                    local_sp_8 = var_64;
                    if ((uint64_t)(uint32_t)var_66 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                }
                rbp_4 = rbp_2;
                rax_5 = rax_3;
                local_sp_7 = local_sp_4;
                if ((uint64_t)(unsigned char)r15_0 == 0UL) {
                    continue;
                }
                *(unsigned char *)rbp_2 = (unsigned char)'\x00';
                var_67 = *(uint64_t *)(local_sp_4 + 40UL);
                var_68 = *(uint64_t *)(local_sp_4 + 24UL);
                var_69 = var_67 + var_68;
                **(uint64_t **)(local_sp_4 + 48UL) = var_68;
                **(uint64_t **)(local_sp_4 + 56UL) = r14_0;
                var_70 = rbp_2 - var_69;
                var_71 = (var_70 == 0UL) ? 18446744073709551615UL : var_70;
                rax_6 = var_71;
                loop_state_var = 0U;
                break;
            }
            var_45 = (r14_3 > 63UL) ? (r14_3 << 1UL) : (r14_3 + 64UL);
            var_46 = (uint64_t *)(local_sp_0 + 24UL);
            var_47 = rbp_4 - *var_46;
            var_48 = (var_41 > (var_45 - var_47)) ? (var_47 + var_41) : var_45;
            var_49 = helper_cc_compute_c_wrapper(r14_3 - var_48, var_48, var_7, 17U);
            r12_0 = var_48;
            if ((var_49 == 0UL) || (var_48 > var_43)) {
                r12_0 = *var_42;
            }
            var_50 = *(uint64_t *)(local_sp_0 + 40UL);
            r12_1 = r12_0;
            rax_0 = var_50;
            if ((long)(r12_0 - var_50) <= (long)18446744073709551615UL) {
                var_51 = var_50 ^ (-9223372036854775808L);
                r12_1 = var_51;
                if (r14_3 != var_51) {
                    loop_state_var = 1U;
                    break;
                }
            }
            var_52 = *var_46;
            var_53 = local_sp_0 + (-8L);
            *(uint64_t *)var_53 = 4210008UL;
            indirect_placeholder_13(var_52, r12_1);
            r12_2 = r12_1;
            r14_0 = r12_1;
            local_sp_1 = var_53;
            local_sp_8 = var_53;
            if (var_50 != 0UL) {
                loop_state_var = 1U;
                break;
            }
            var_54 = r12_1 - var_47;
            var_55 = var_47 + var_50;
            *(uint64_t *)(local_sp_0 + 16UL) = var_50;
            r13_0 = var_54;
            rbp_0 = var_55;
        }
    switch (loop_state_var) {
      case 0U:
        {
            return rax_6;
        }
        break;
      case 1U:
        {
            break;
        }
        break;
    }
}
