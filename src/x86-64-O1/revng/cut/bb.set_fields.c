typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_18_ret_type;
struct indirect_placeholder_19_ret_type;
struct indirect_placeholder_20_ret_type;
struct indirect_placeholder_21_ret_type;
struct indirect_placeholder_22_ret_type;
struct indirect_placeholder_18_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_19_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_20_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_21_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_22_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_10(uint64_t param_0);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint32_t init_state_0x82fc(void);
extern void indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_13(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_18_ret_type indirect_placeholder_18(uint64_t param_0);
extern struct indirect_placeholder_19_ret_type indirect_placeholder_19(uint64_t param_0);
extern struct indirect_placeholder_20_ret_type indirect_placeholder_20(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_21_ret_type indirect_placeholder_21(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_22_ret_type indirect_placeholder_22(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
void bb_set_fields(uint64_t rdi, uint64_t rsi, uint64_t rcx, uint64_t r10, uint64_t r8, uint64_t r9) {
    uint64_t local_sp_1;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint32_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t rax_1;
    uint64_t local_sp_2;
    uint64_t var_74;
    uint64_t rbx_0;
    uint64_t rcx3_0;
    uint64_t local_sp_0;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t rcx3_1;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t *var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t local_sp_3;
    uint64_t r85_0;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t var_89;
    uint32_t var_28;
    unsigned char *var_29;
    uint64_t rbp_1;
    uint64_t r104_2;
    bool var_30;
    uint64_t var_31;
    unsigned char *var_32;
    uint64_t rcx3_7;
    uint64_t rax_0;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t rax_3;
    uint64_t var_35;
    uint64_t var_36;
    struct indirect_placeholder_18_ret_type var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_52;
    uint64_t *var_53;
    uint64_t var_41;
    uint64_t r14_1;
    uint64_t var_42;
    uint64_t var_43;
    struct indirect_placeholder_19_ret_type var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t *_pre_phi305;
    uint64_t *_pre_phi;
    uint64_t rcx3_2;
    uint64_t r104_0;
    uint64_t r96_0;
    uint64_t local_sp_4;
    uint64_t var_71;
    unsigned char *var_49;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_96;
    uint64_t var_11;
    uint64_t cc_src_0;
    uint64_t cc_dst_0;
    uint32_t cc_op_0;
    uint64_t rdi1_0;
    uint64_t rsi2_0;
    uint64_t rcx3_3;
    uint64_t cc_src_1;
    uint64_t cc_dst_1;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint32_t cc_op_1;
    uint64_t rcx3_4;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    bool var_19;
    uint64_t var_20;
    uint64_t spec_select;
    uint64_t r13_0;
    uint64_t rbp_0;
    uint64_t r12_1;
    uint64_t rcx3_5;
    uint64_t cc_src2_0;
    uint64_t r13_2;
    uint64_t rax_2;
    uint64_t r12_3;
    uint64_t r13_1;
    uint64_t r12_2;
    uint64_t r14_0;
    uint64_t rcx3_6;
    uint64_t r85_2;
    uint64_t r104_1;
    uint64_t r96_2;
    uint64_t r85_1;
    uint64_t local_sp_7;
    uint64_t r96_1;
    uint64_t local_sp_5;
    unsigned char var_21;
    uint64_t var_22;
    uint64_t local_sp_6;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t *var_27;
    unsigned char *_pre_phi309;
    uint64_t *var_59;
    uint64_t *_pre304;
    uint64_t var_50;
    bool var_51;
    struct indirect_placeholder_20_ret_type var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t *var_62;
    struct indirect_placeholder_21_ret_type var_63;
    uint64_t var_64;
    uint64_t *var_65;
    struct indirect_placeholder_22_ret_type var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_97;
    uint64_t var_99;
    unsigned char var_100;
    unsigned char var_101;
    unsigned char var_102;
    uint64_t var_103;
    uint64_t var_104;
    unsigned char *var_105;
    uint64_t var_107;
    uint64_t var_106;
    uint64_t var_98;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_cc_src2();
    var_8 = init_state_0x82fc();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_9 = var_0 + (-88L);
    *(uint32_t *)(var_0 + (-60L)) = (uint32_t)rsi;
    var_10 = rsi & 1UL;
    *(uint32_t *)(var_0 + (-64L)) = (uint32_t)var_10;
    rax_1 = 0UL;
    rbx_0 = 0UL;
    rcx3_0 = 4207851UL;
    rcx3_1 = 4207851UL;
    rax_0 = 1844674407370955161UL;
    cc_src_0 = 40UL;
    cc_dst_0 = var_10;
    cc_op_0 = 24U;
    rdi1_0 = 4268127UL;
    rsi2_0 = rdi;
    rcx3_3 = 2UL;
    rcx3_4 = 0UL;
    r13_0 = 0UL;
    rbp_0 = rdi;
    r12_1 = 0UL;
    rcx3_5 = rcx;
    cc_src2_0 = var_7;
    r12_3 = 0UL;
    r104_1 = r10;
    r85_1 = r8;
    r96_1 = r9;
    local_sp_5 = var_9;
    if (var_10 != 0UL) {
        var_11 = (uint64_t)var_8;
        cc_op_0 = 14U;
        cc_src_1 = cc_src_0;
        cc_dst_1 = cc_dst_0;
        cc_op_1 = cc_op_0;
        while (rcx3_3 != 0UL)
            {
                var_12 = (uint64_t)*(unsigned char *)rdi1_0;
                var_13 = (uint64_t)*(unsigned char *)rsi2_0 - var_12;
                var_14 = rcx3_3 + (-1L);
                cc_src_0 = var_12;
                cc_dst_0 = var_13;
                rcx3_3 = var_14;
                cc_src_1 = var_12;
                cc_dst_1 = var_13;
                cc_op_1 = 14U;
                rcx3_4 = var_14;
                if ((uint64_t)(unsigned char)var_13 == 0UL) {
                    break;
                }
                rdi1_0 = rdi1_0 + var_11;
                rsi2_0 = rsi2_0 + var_11;
                cc_op_0 = 14U;
                cc_src_1 = cc_src_0;
                cc_dst_1 = cc_dst_0;
                cc_op_1 = cc_op_0;
            }
        var_15 = helper_cc_compute_all_wrapper(cc_dst_1, cc_src_1, var_7, cc_op_1);
        var_16 = ((var_15 & 65UL) == 0UL);
        var_17 = helper_cc_compute_c_wrapper(cc_dst_1, var_15, var_7, 1U);
        var_18 = (uint64_t)((unsigned char)var_16 - (unsigned char)var_17);
        var_19 = (var_18 == 0UL);
        var_20 = rdi + 1UL;
        spec_select = var_19 ? 1UL : 0UL;
        rax_1 = var_18;
        r13_0 = spec_select;
        rbp_0 = var_19 ? var_20 : rdi;
        r12_1 = spec_select;
        rcx3_5 = rcx3_4;
        cc_src2_0 = var_17;
    }
    *(unsigned char *)(var_0 + (-74L)) = (unsigned char)'\x00';
    *(unsigned char *)(var_0 + (-73L)) = (unsigned char)'\x00';
    *(uint64_t *)(var_0 + (-72L)) = 1UL;
    rbp_1 = rbp_0;
    rax_2 = rax_1;
    r13_1 = r13_0;
    r12_2 = r12_1;
    r14_0 = r13_0;
    rcx3_6 = rcx3_5;
    while (1U)
        {
            *(uint64_t *)local_sp_5 = rbp_1;
            var_21 = *(unsigned char *)rbp_1;
            var_22 = (uint64_t)var_21;
            r104_2 = r104_1;
            rcx3_7 = rcx3_6;
            r14_1 = r14_0;
            r13_2 = r13_1;
            r85_2 = r85_1;
            r96_2 = r96_1;
            local_sp_7 = local_sp_5;
            local_sp_6 = local_sp_5;
            if ((uint64_t)(var_21 + '\xd3') == 0UL) {
                r14_1 = 1UL;
                if ((uint64_t)(unsigned char)r14_0 == 0UL) {
                    var_98 = ((*(unsigned char *)(local_sp_5 + 28UL) & '\x04') == '\x00') ? 4263218UL : 4262976UL;
                    *(uint64_t *)(local_sp_5 + (-8L)) = 4208307UL;
                    indirect_placeholder_8(0UL, var_98, 0UL, 0UL, rcx3_6, r85_1, r96_1);
                    *(uint64_t *)(local_sp_5 + (-16L)) = 4208317UL;
                    indirect_placeholder_9(var_22, 1UL, rbp_1);
                    abort();
                }
                var_99 = rax_2 & (-256L);
                var_100 = (unsigned char)(r12_2 == 0UL);
                var_101 = (unsigned char)r13_1;
                var_102 = var_100 & var_101;
                var_103 = (uint64_t)var_102 & 1UL;
                var_104 = var_99 | var_103;
                var_105 = (unsigned char *)(local_sp_5 + 14UL);
                *var_105 = var_102;
                rax_3 = var_104;
                if (var_103 == 0UL) {
                    var_106 = ((*(unsigned char *)(local_sp_5 + 28UL) & '\x04') == '\x00') ? 4263238UL : 4263008UL;
                    *(uint64_t *)(local_sp_5 + (-8L)) = 4208356UL;
                    indirect_placeholder_8(0UL, var_106, 0UL, 0UL, rcx3_6, r85_1, r96_1);
                    *(uint64_t *)(local_sp_5 + (-16L)) = 4208366UL;
                    indirect_placeholder_9(var_22, 1UL, rbp_1);
                    abort();
                }
                if ((uint64_t)var_101 == 0UL) {
                    *var_105 = var_101;
                    *(uint64_t *)(local_sp_5 + 16UL) = 1UL;
                } else {
                    var_107 = (uint64_t)(uint32_t)r13_1;
                    *(uint64_t *)(local_sp_5 + 16UL) = r12_2;
                    r14_1 = var_107;
                }
            } else {
                r13_2 = 0UL;
                if ((uint64_t)(var_21 + '\xd4') != 0UL) {
                    var_23 = (uint64_t)var_21;
                    var_24 = (uint64_t)(uint32_t)var_23;
                    *(uint64_t *)(local_sp_5 + (-8L)) = 4208568UL;
                    var_25 = indirect_placeholder_10(var_24);
                    var_26 = local_sp_5 + (-16L);
                    var_27 = (uint64_t *)var_26;
                    *var_27 = 4208576UL;
                    indirect_placeholder_1();
                    local_sp_6 = var_26;
                    local_sp_7 = var_26;
                    if (!((var_21 != '\x00') && ((uint64_t)(uint32_t)var_25 == 0UL))) {
                        var_28 = (uint32_t)var_21 + (-48);
                        if (var_28 > 9U) {
                            var_42 = (uint64_t)var_28;
                            var_43 = *var_27;
                            *(uint64_t *)(local_sp_5 + (-24L)) = 4208933UL;
                            var_44 = indirect_placeholder_19(var_43);
                            var_45 = var_44.field_0;
                            var_46 = var_44.field_1;
                            var_47 = var_44.field_2;
                            var_48 = ((*(unsigned char *)(local_sp_5 + 4UL) & '\x04') == '\x00') ? 4263294UL : 4263096UL;
                            *(uint64_t *)(local_sp_5 + (-32L)) = 4208975UL;
                            indirect_placeholder_8(0UL, var_48, 0UL, 0UL, var_45, var_46, var_47);
                            *(uint64_t *)(local_sp_5 + (-40L)) = 4208985UL;
                            indirect_placeholder_9(var_42, 1UL, rbp_1);
                            abort();
                        }
                        var_29 = (unsigned char *)(local_sp_5 + (-2L));
                        if (*var_29 == '\x00') {
                            *(uint64_t *)4285384UL = *var_27;
                        } else {
                            if (*(uint64_t *)4285384UL == 0UL) {
                                *(uint64_t *)4285384UL = *var_27;
                            }
                        }
                        var_30 = ((uint64_t)(unsigned char)r14_0 == 0UL);
                        var_31 = var_30 ? 1UL : (uint64_t)(uint32_t)r13_1;
                        var_32 = (unsigned char *)(local_sp_5 + (-1L));
                        *var_32 = (unsigned char)(var_30 ? (uint64_t)*var_32 : r14_0);
                        r13_2 = var_31;
                        if (r12_2 <= 1844674407370955161UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_33 = (r12_2 * 10UL) + (uint64_t)((long)((var_23 << 32UL) + (-206158430208L)) >> (long)32UL);
                        rax_0 = 18446744073709551615UL;
                        rax_3 = var_33;
                        r12_3 = var_33;
                        if (var_33 != 18446744073709551615UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_34 = helper_cc_compute_c_wrapper(var_33 - r12_2, r12_2, cc_src2_0, 17U);
                        rax_0 = var_33;
                        if (var_34 != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        *var_29 = (unsigned char)'\x01';
                        rbp_1 = rbp_1 + 1UL;
                        rax_2 = rax_3;
                        r13_1 = r13_2;
                        r12_2 = r12_3;
                        r14_0 = r14_1;
                        rcx3_6 = rcx3_7;
                        r104_1 = r104_2;
                        r85_1 = r85_2;
                        r96_1 = r96_2;
                        local_sp_5 = local_sp_7;
                        continue;
                    }
                }
                r14_1 = 0UL;
                if ((uint64_t)(unsigned char)r14_0 == 0UL) {
                    if (r12_2 != 0UL) {
                        var_97 = ((*(unsigned char *)(local_sp_6 + 28UL) & '\x04') == '\x00') ? 4263238UL : 4263008UL;
                        *(uint64_t *)(local_sp_6 + (-8L)) = 4208690UL;
                        indirect_placeholder_8(0UL, var_97, 0UL, 0UL, rcx3_6, r85_1, r96_1);
                        *(uint64_t *)(local_sp_6 + (-16L)) = 4208700UL;
                        indirect_placeholder_9(var_22, 1UL, rbp_1);
                        abort();
                    }
                    var_64 = local_sp_6 + (-8L);
                    var_65 = (uint64_t *)var_64;
                    *var_65 = 4208489UL;
                    var_66 = indirect_placeholder_22(r12_2, r12_2, rcx3_6, r104_1);
                    var_67 = var_66.field_0;
                    var_68 = var_66.field_1;
                    var_69 = var_66.field_2;
                    var_70 = var_66.field_3;
                    r85_0 = var_69;
                    _pre_phi = var_65;
                    rcx3_2 = var_67;
                    r104_0 = var_68;
                    r96_0 = var_70;
                    local_sp_4 = var_64;
                } else {
                    if ((uint64_t)(unsigned char)r13_1 == 0UL) {
                        var_49 = (unsigned char *)(local_sp_6 + 15UL);
                        _pre_phi309 = var_49;
                        if (*var_49 != '\x00') {
                            if (*(uint32_t *)(local_sp_6 + 24UL) == 0U) {
                                *(uint64_t *)(local_sp_6 + (-8L)) = 4208391UL;
                                indirect_placeholder_8(0UL, 4263184UL, 0UL, 0UL, rcx3_6, r85_1, r96_1);
                                *(uint64_t *)(local_sp_6 + (-16L)) = 4208401UL;
                                indirect_placeholder_9(var_22, 1UL, rbp_1);
                                abort();
                            }
                            var_59 = (uint64_t *)(local_sp_6 + 16UL);
                            *var_59 = 1UL;
                            _pre_phi305 = var_59;
                            var_60 = *_pre_phi305;
                            var_61 = local_sp_6 + (-8L);
                            var_62 = (uint64_t *)var_61;
                            *var_62 = 4208646UL;
                            var_63 = indirect_placeholder_21(var_60, 18446744073709551615UL, rcx3_6, r104_1);
                            r85_0 = var_63.field_2;
                            _pre_phi = var_62;
                            rcx3_2 = var_63.field_0;
                            r104_0 = var_63.field_1;
                            r96_0 = var_63.field_3;
                            local_sp_4 = var_61;
                            var_71 = *_pre_phi;
                            r104_2 = r104_0;
                            rcx3_7 = rcx3_2;
                            rax_3 = var_71;
                            r85_2 = r85_0;
                            r96_2 = r96_0;
                            local_sp_7 = local_sp_4;
                            if (*(unsigned char *)var_71 == '\x00') {
                                *(unsigned char *)(local_sp_4 + 14UL) = (unsigned char)'\x00';
                                *(unsigned char *)(local_sp_4 + 15UL) = (unsigned char)'\x00';
                                rbp_1 = rbp_1 + 1UL;
                                rax_2 = rax_3;
                                r13_1 = r13_2;
                                r12_2 = r12_3;
                                r14_0 = r14_1;
                                rcx3_6 = rcx3_7;
                                r104_1 = r104_2;
                                r85_1 = r85_2;
                                r96_1 = r96_2;
                                local_sp_5 = local_sp_7;
                                continue;
                            }
                            if (*(uint64_t *)4285832UL != 0UL) {
                                var_72 = local_sp_4 + (-8L);
                                *(uint64_t *)var_72 = 4209019UL;
                                indirect_placeholder_1();
                                var_73 = *(uint64_t *)4285832UL;
                                var_74 = var_73;
                                local_sp_0 = var_72;
                                local_sp_2 = var_72;
                                if (var_73 != 0UL) {
                                    loop_state_var = 2U;
                                    break;
                                }
                                loop_state_var = 0U;
                                break;
                            }
                            var_96 = ((*(unsigned char *)(local_sp_4 + 28UL) & '\x04') == '\x00') ? 4263317UL : 4263136UL;
                            *(uint64_t *)(local_sp_4 + (-8L)) = 4209081UL;
                            indirect_placeholder_8(0UL, var_96, 0UL, 0UL, rcx3_2, r85_0, r96_0);
                            *(uint64_t *)(local_sp_4 + (-16L)) = 4209091UL;
                            indirect_placeholder_9(var_22, 1UL, rbp_1);
                            abort();
                        }
                    }
                    _pre_phi309 = (unsigned char *)(local_sp_6 + 15UL);
                    if (*_pre_phi309 == '\x00') {
                        _pre304 = (uint64_t *)(local_sp_6 + 16UL);
                        _pre_phi305 = _pre304;
                        var_60 = *_pre_phi305;
                        var_61 = local_sp_6 + (-8L);
                        var_62 = (uint64_t *)var_61;
                        *var_62 = 4208646UL;
                        var_63 = indirect_placeholder_21(var_60, 18446744073709551615UL, rcx3_6, r104_1);
                        r85_0 = var_63.field_2;
                        _pre_phi = var_62;
                        rcx3_2 = var_63.field_0;
                        r104_0 = var_63.field_1;
                        r96_0 = var_63.field_3;
                        local_sp_4 = var_61;
                    } else {
                        var_50 = *(uint64_t *)(local_sp_6 + 16UL);
                        var_51 = (var_50 > r12_2);
                        var_52 = local_sp_6 + (-8L);
                        var_53 = (uint64_t *)var_52;
                        _pre_phi = var_53;
                        local_sp_4 = var_52;
                        if (!var_51) {
                            *var_53 = 4208459UL;
                            indirect_placeholder_8(0UL, 4263340UL, 0UL, 0UL, rcx3_6, r85_1, r96_1);
                            *(uint64_t *)(local_sp_6 + (-16L)) = 4208469UL;
                            indirect_placeholder_9(var_22, 1UL, rbp_1);
                            abort();
                        }
                        *var_53 = 4208432UL;
                        var_54 = indirect_placeholder_20(var_50, r12_2, rcx3_6, r104_1);
                        var_55 = var_54.field_0;
                        var_56 = var_54.field_1;
                        var_57 = var_54.field_2;
                        var_58 = var_54.field_3;
                        rcx3_2 = var_55;
                        r104_0 = var_56;
                        r85_0 = var_57;
                        r96_0 = var_58;
                    }
                }
            }
        }
    switch (loop_state_var) {
      case 1U:
        {
            var_35 = *(uint64_t *)4285384UL;
            *(uint64_t *)(local_sp_5 + (-24L)) = 4208842UL;
            indirect_placeholder_1();
            *(uint64_t *)(local_sp_5 + (-32L)) = 4208853UL;
            var_36 = indirect_placeholder(var_35, rax_0);
            *(uint64_t *)(local_sp_5 + (-40L)) = 4208864UL;
            var_37 = indirect_placeholder_18(var_36);
            var_38 = var_37.field_0;
            var_39 = var_37.field_1;
            var_40 = var_37.field_2;
            var_41 = ((*(unsigned char *)(local_sp_5 + (-12L)) & '\x04') == '\x00') ? 4263265UL : 4263056UL;
            *(uint64_t *)(local_sp_5 + (-48L)) = 4208906UL;
            indirect_placeholder_8(0UL, var_41, 0UL, 0UL, var_38, var_39, var_40);
            *(uint64_t *)(local_sp_5 + (-56L)) = 4208914UL;
            indirect_placeholder_1();
            *(uint64_t *)(local_sp_5 + (-64L)) = 4208924UL;
            indirect_placeholder_9(var_36, 1UL, rbp_1);
            abort();
        }
        break;
      case 2U:
      case 0U:
        {
            switch (loop_state_var) {
              case 0U:
                {
                    var_75 = rbx_0 + 1UL;
                    var_76 = helper_cc_compute_c_wrapper(var_75 - var_74, var_74, cc_src2_0, 17U);
                    rbx_0 = var_75;
                    local_sp_1 = local_sp_0;
                    rcx3_1 = rcx3_0;
                    local_sp_2 = local_sp_0;
                    while (var_76 != 0UL)
                        {
                            var_77 = var_75 << 4UL;
                            var_78 = var_77 + (-16L);
                            while (1U)
                                {
                                    var_79 = *(uint64_t *)4285824UL;
                                    var_80 = var_77 + var_79;
                                    var_81 = var_78 + var_79;
                                    var_82 = (uint64_t *)(var_81 + 8UL);
                                    var_83 = *var_82;
                                    rcx3_0 = var_81;
                                    local_sp_0 = local_sp_1;
                                    rcx3_1 = var_81;
                                    local_sp_2 = local_sp_1;
                                    if (*(uint64_t *)var_80 <= var_83) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    var_84 = *(uint64_t *)(var_80 + 8UL);
                                    var_85 = helper_cc_compute_c_wrapper(var_84 - var_83, var_83, cc_src2_0, 17U);
                                    *var_82 = ((var_85 == 0UL) ? var_84 : var_83);
                                    var_86 = local_sp_1 + (-8L);
                                    *(uint64_t *)var_86 = 4209187UL;
                                    indirect_placeholder_1();
                                    var_87 = *(uint64_t *)4285832UL + (-1L);
                                    *(uint64_t *)4285832UL = var_87;
                                    local_sp_1 = var_86;
                                    local_sp_2 = var_86;
                                    if (var_87 > var_75) {
                                        continue;
                                    }
                                    loop_state_var = 1U;
                                    break;
                                }
                            switch_state_var = 0;
                            switch (loop_state_var) {
                              case 1U:
                                {
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              case 0U:
                                {
                                    var_88 = *(uint64_t *)4285832UL;
                                    var_74 = var_88;
                                    if (var_88 > var_75) {
                                        switch_state_var = 1;
                                        break;
                                    }
                                    var_75 = rbx_0 + 1UL;
                                    var_76 = helper_cc_compute_c_wrapper(var_75 - var_74, var_74, cc_src2_0, 17U);
                                    rbx_0 = var_75;
                                    local_sp_1 = local_sp_0;
                                    rcx3_1 = rcx3_0;
                                    local_sp_2 = local_sp_0;
                                    continue;
                                }
                                break;
                            }
                            if (switch_state_var)
                                break;
                        }
                }
                break;
              case 2U:
                {
                    local_sp_3 = local_sp_2;
                    if ((*(unsigned char *)(local_sp_2 + 28UL) & '\x02') != '\x00') {
                        var_89 = local_sp_2 + (-8L);
                        *(uint64_t *)var_89 = 4209309UL;
                        indirect_placeholder_13(rcx3_1, r104_0);
                        local_sp_3 = var_89;
                    }
                    var_90 = *(uint64_t *)4285832UL + 1UL;
                    *(uint64_t *)4285832UL = var_90;
                    var_91 = var_90 << 4UL;
                    var_92 = *(uint64_t *)4285824UL;
                    *(uint64_t *)(local_sp_3 + (-8L)) = 4209251UL;
                    var_93 = indirect_placeholder(var_92, var_91);
                    *(uint64_t *)4285824UL = var_93;
                    var_94 = (*(uint64_t *)4285832UL << 4UL) + var_93;
                    var_95 = var_94 + (-16L);
                    *(uint64_t *)(var_94 + (-8L)) = 18446744073709551615UL;
                    *(uint64_t *)var_95 = 18446744073709551615UL;
                    return;
                }
                break;
            }
        }
        break;
    }
}
