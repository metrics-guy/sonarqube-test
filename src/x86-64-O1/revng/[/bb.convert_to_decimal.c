typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct helper_pxor_xmm_wrapper_65_ret_type;
struct type_4;
struct type_6;
struct helper_cvtsq2ss_wrapper_ret_type;
struct helper_mulss_wrapper_ret_type;
struct helper_addss_wrapper_ret_type;
struct helper_comiss_wrapper_ret_type;
struct helper_cvttss2sq_wrapper_ret_type;
struct helper_subss_wrapper_ret_type;
struct helper_pxor_xmm_wrapper_65_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_4 {
};
struct type_6 {
};
struct helper_cvtsq2ss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_mulss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_addss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_comiss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvttss2sq_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_subss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_state_0x8558(void);
extern unsigned char init_state_0x8549(void);
extern unsigned char init_state_0x854c(void);
extern unsigned char init_state_0x8548(void);
extern unsigned char init_state_0x854b(void);
extern unsigned char init_state_0x8547(void);
extern unsigned char init_state_0x854d(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t init_state_0x8560(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_6(uint64_t param_0);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct helper_pxor_xmm_wrapper_65_ret_type helper_pxor_xmm_wrapper_65(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_r13(void);
extern uint64_t init_state_0x8d58(void);
extern struct helper_cvtsq2ss_wrapper_ret_type helper_cvtsq2ss_wrapper(struct type_4 *param_0, struct type_6 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_mulss_wrapper_ret_type helper_mulss_wrapper(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_addss_wrapper_ret_type helper_addss_wrapper(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9);
extern struct helper_comiss_wrapper_ret_type helper_comiss_wrapper(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_cvttss2sq_wrapper_ret_type helper_cvttss2sq_wrapper(struct type_4 *param_0, struct type_6 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4);
extern struct helper_subss_wrapper_ret_type helper_subss_wrapper(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
uint64_t bb_convert_to_decimal(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    unsigned char var_9;
    unsigned char var_10;
    unsigned char var_11;
    unsigned char var_12;
    unsigned char var_13;
    struct helper_cvtsq2ss_wrapper_ret_type var_15;
    unsigned char var_14;
    uint64_t var_34;
    uint64_t rax_0;
    uint64_t var_35;
    uint64_t var_37;
    uint64_t rsi3_2;
    uint64_t rsi3_0;
    uint64_t rbp_0;
    uint64_t cc_src2_0;
    uint64_t r8_0_in;
    uint64_t r8_0;
    uint64_t var_36;
    uint64_t rdi2_0;
    uint64_t rax_1;
    uint32_t *var_38;
    uint32_t var_39;
    unsigned __int128 var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t rsi3_1;
    uint64_t rax_2;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t cc_src2_1;
    uint64_t var_48;
    uint64_t rsi3_4;
    uint64_t rsi3_3;
    uint64_t var_49;
    uint64_t rsi3_6;
    uint64_t state_0x8558_0;
    uint64_t var_16;
    struct helper_cvtsq2ss_wrapper_ret_type var_17;
    struct helper_addss_wrapper_ret_type var_18;
    uint64_t cc_dst_0;
    unsigned char storemerge;
    uint64_t var_19;
    uint64_t var_20;
    struct helper_mulss_wrapper_ret_type var_21;
    uint64_t var_22;
    struct helper_comiss_wrapper_ret_type var_23;
    uint64_t var_24;
    unsigned char var_25;
    uint64_t var_26;
    struct helper_cvttss2sq_wrapper_ret_type var_27;
    uint64_t rax_3;
    struct helper_subss_wrapper_ret_type var_28;
    struct helper_cvttss2sq_wrapper_ret_type var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_rbp();
    var_4 = init_r12();
    var_5 = init_cc_src2();
    var_6 = init_state_0x8558();
    var_7 = init_state_0x8560();
    var_8 = init_state_0x8d58();
    var_9 = init_state_0x8549();
    var_10 = init_state_0x854c();
    var_11 = init_state_0x8548();
    var_12 = init_state_0x854b();
    var_13 = init_state_0x8547();
    var_14 = init_state_0x854d();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_2;
    rbp_0 = rdi;
    cc_src2_0 = var_5;
    rax_1 = 0UL;
    cc_src2_1 = var_5;
    cc_dst_0 = rdi;
    if ((long)rdi < (long)0UL) {
        var_16 = (rdi >> 1UL) | (rdi & 1UL);
        helper_pxor_xmm_wrapper_65((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(776UL), var_6, var_7);
        var_17 = helper_cvtsq2ss_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), var_16, var_9, var_11, var_12, var_13);
        var_18 = helper_addss_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(776UL), var_17.field_0, var_17.field_1, var_10, var_11, var_12, var_13, var_14);
        state_0x8558_0 = var_18.field_0;
        cc_dst_0 = var_16;
        storemerge = var_18.field_1;
    } else {
        helper_pxor_xmm_wrapper_65((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(776UL), var_6, var_7);
        var_15 = helper_cvtsq2ss_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), rdi, var_9, var_11, var_12, var_13);
        state_0x8558_0 = var_15.field_0;
        storemerge = var_15.field_1;
    }
    var_19 = (uint64_t)*(uint32_t *)4266276UL;
    var_20 = var_8 & (-4294967296L);
    var_21 = helper_mulss_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(2824UL), state_0x8558_0, var_20 | var_19, storemerge, var_10, var_11, var_12, var_13, var_14);
    var_22 = var_21.field_0;
    var_23 = helper_comiss_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(2824UL), var_22, var_20 | (uint64_t)*(uint32_t *)4266280UL, var_21.field_1, var_10);
    var_24 = var_23.field_0;
    var_25 = var_23.field_1;
    var_26 = helper_cc_compute_c_wrapper(cc_dst_0, var_24, var_5, 1U);
    if (var_26 == 0UL) {
        var_28 = helper_subss_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(2824UL), var_22, var_20 | (uint64_t)*(uint32_t *)4266280UL, var_25, var_10, var_11, var_12, var_13, var_14);
        var_29 = helper_cvttss2sq_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), var_28.field_0, var_28.field_1, var_10);
        var_30 = var_29.field_0 ^ (-9223372036854775808L);
        helper_cc_compute_all_wrapper(cc_dst_0, var_24, var_5, 1U);
        rax_3 = var_30;
    } else {
        var_27 = helper_cvttss2sq_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), var_22, var_25, var_10);
        rax_3 = var_27.field_0;
    }
    var_31 = (rax_3 * 9UL) + 9UL;
    *(uint64_t *)(var_0 + (-48L)) = 4222897UL;
    var_32 = indirect_placeholder(var_31, rdx);
    *(uint64_t *)(var_0 + (-56L)) = 4222905UL;
    var_33 = indirect_placeholder_6(var_32);
    rax_0 = var_33;
    rsi3_0 = var_33;
    if (var_33 == 0UL) {
        return var_33;
    }
    if (rdx == 0UL) {
        if (rdi != 0UL) {
            *(unsigned char *)var_33 = (unsigned char)'0';
            rsi3_6 = var_33 + 1UL;
            *(unsigned char *)rsi3_6 = (unsigned char)'\x00';
            return var_33;
        }
    }
    var_34 = var_33 + rdx;
    rsi3_0 = var_34;
    rsi3_2 = var_34;
    var_35 = rax_0 + 1UL;
    *(unsigned char *)rax_0 = (unsigned char)'0';
    rax_0 = var_35;
    do {
        var_35 = rax_0 + 1UL;
        *(unsigned char *)rax_0 = (unsigned char)'0';
        rax_0 = var_35;
    } while (var_34 != var_35);
    if (rdi != 0UL) {
        var_48 = helper_cc_compute_c_wrapper(var_33 - rsi3_2, rsi3_2, cc_src2_1, 17U);
        rsi3_3 = rsi3_2;
        rsi3_4 = rsi3_2;
        if (var_48 == 0UL) {
            rsi3_6 = rsi3_4;
            if (var_33 == rsi3_4) {
                *(unsigned char *)var_33 = (unsigned char)'0';
                rsi3_6 = var_33 + 1UL;
            }
            *(unsigned char *)rsi3_6 = (unsigned char)'\x00';
            return var_33;
        }
        while (1U)
            {
                var_49 = rsi3_3 + (-1L);
                rsi3_3 = var_49;
                rsi3_4 = rsi3_3;
                if (*(unsigned char *)var_49 != '0') {
                    loop_state_var = 0U;
                    break;
                }
                if (var_33 == var_49) {
                    continue;
                }
                loop_state_var = 1U;
                break;
            }
        switch (loop_state_var) {
          case 0U:
            {
                break;
            }
            break;
          case 1U:
            {
                *(unsigned char *)var_33 = (unsigned char)'0';
                rsi3_6 = var_33 + 1UL;
                *(unsigned char *)rsi3_6 = (unsigned char)'\x00';
                return var_33;
            }
            break;
        }
    }
    r8_0_in = rsi3_0;
    var_46 = helper_cc_compute_c_wrapper((uint64_t)*(uint32_t *)((var_36 + rsi) + (-4L)) + (-1L), 1UL, cc_src2_0, 16U);
    var_47 = rbp_0 - var_46;
    rbp_0 = var_47;
    cc_src2_0 = var_46;
    cc_src2_1 = var_46;
    do {
        r8_0 = r8_0_in + 9UL;
        var_36 = rbp_0 << 2UL;
        r8_0_in = r8_0;
        rdi2_0 = rbp_0;
        rsi3_1 = r8_0_in;
        rsi3_2 = r8_0;
        var_37 = rax_1 << 32UL;
        var_38 = (uint32_t *)(((rdi2_0 << 2UL) + rsi) + (-4L));
        var_39 = *var_38;
        var_40 = ((unsigned __int128)((var_37 | (uint64_t)var_39) >> 9UL) * 19342813113834067ULL) >> 75ULL;
        var_41 = (uint64_t)var_40;
        *var_38 = (uint32_t)var_40;
        var_42 = (uint64_t)(((uint32_t)var_41 * 3294967296U) + var_39);
        var_43 = rdi2_0 + (-1L);
        rdi2_0 = var_43;
        rax_1 = var_42;
        rax_2 = var_42;
        do {
            var_37 = rax_1 << 32UL;
            var_38 = (uint32_t *)(((rdi2_0 << 2UL) + rsi) + (-4L));
            var_39 = *var_38;
            var_40 = ((unsigned __int128)((var_37 | (uint64_t)var_39) >> 9UL) * 19342813113834067ULL) >> 75ULL;
            var_41 = (uint64_t)var_40;
            *var_38 = (uint32_t)var_40;
            var_42 = (uint64_t)(((uint32_t)var_41 * 3294967296U) + var_39);
            var_43 = rdi2_0 + (-1L);
            rdi2_0 = var_43;
            rax_1 = var_42;
            rax_2 = var_42;
        } while (var_43 != 0UL);
        var_44 = rsi3_1 + 1UL;
        var_45 = (rax_2 * 3435973837UL) >> 35UL;
        *(unsigned char *)rsi3_1 = (((unsigned char)rax_2 + ((unsigned char)var_45 * '\xf6')) + '0');
        rsi3_1 = var_44;
        rax_2 = var_45;
        do {
            var_44 = rsi3_1 + 1UL;
            var_45 = (rax_2 * 3435973837UL) >> 35UL;
            *(unsigned char *)rsi3_1 = (((unsigned char)rax_2 + ((unsigned char)var_45 * '\xf6')) + '0');
            rsi3_1 = var_44;
            rax_2 = var_45;
        } while (var_44 != r8_0);
        var_46 = helper_cc_compute_c_wrapper((uint64_t)*(uint32_t *)((var_36 + rsi) + (-4L)) + (-1L), 1UL, cc_src2_0, 16U);
        var_47 = rbp_0 - var_46;
        rbp_0 = var_47;
        cc_src2_0 = var_46;
        cc_src2_1 = var_46;
    } while (var_47 != 0UL);
    var_48 = helper_cc_compute_c_wrapper(var_33 - rsi3_2, rsi3_2, cc_src2_1, 17U);
    rsi3_3 = rsi3_2;
    rsi3_4 = rsi3_2;
    if (var_48 != 0UL) {
        while (1U)
            {
                var_49 = rsi3_3 + (-1L);
                rsi3_3 = var_49;
                rsi3_4 = rsi3_3;
                if (*(unsigned char *)var_49 != '0') {
                    loop_state_var = 0U;
                    break;
                }
                if (var_33 == var_49) {
                    continue;
                }
                loop_state_var = 1U;
                break;
            }
        switch (loop_state_var) {
          case 0U:
            {
                break;
            }
            break;
          case 1U:
            {
                *(unsigned char *)var_33 = (unsigned char)'0';
                rsi3_6 = var_33 + 1UL;
                *(unsigned char *)rsi3_6 = (unsigned char)'\x00';
                return var_33;
            }
            break;
        }
    }
    rsi3_6 = rsi3_4;
    if (var_33 == rsi3_4) {
        *(unsigned char *)var_33 = (unsigned char)'0';
        rsi3_6 = var_33 + 1UL;
    }
    *(unsigned char *)rsi3_6 = (unsigned char)'\x00';
}
