typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_divq_EAX_wrapper_ret_type;
struct type_5;
struct helper_divq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint32_t field_3;
    uint64_t field_4;
    uint32_t field_5;
    uint32_t field_6;
    uint64_t field_7;
    uint32_t field_8;
    uint64_t field_9;
    uint32_t field_10;
    unsigned char field_11;
    uint32_t field_12;
};
struct type_5 {
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_6(uint64_t param_0);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r10(void);
extern uint64_t init_r9(void);
extern uint32_t init_state_0x8248(void);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern struct helper_divq_EAX_wrapper_ret_type helper_divq_EAX_wrapper(struct type_5 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint32_t param_13, uint64_t param_14, uint32_t param_15, uint64_t param_16, uint64_t param_17, uint64_t param_18, uint32_t param_19);
extern uint64_t helper_clz_wrapper(uint64_t param_0);
uint64_t bb_divide(uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t rcx, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint32_t var_10;
    uint64_t var_11;
    uint32_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint32_t var_16;
    uint64_t rdx1_4;
    uint64_t var_64;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t *var_20;
    uint64_t var_21;
    uint64_t r15_12;
    uint64_t rax_9;
    uint64_t rcx4_8;
    uint32_t state_0x8248_6;
    uint64_t state_0x9018_6;
    uint32_t state_0x9010_6;
    uint64_t var_154;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t rsi3_0;
    uint64_t rax_0;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t cc_src_0;
    uint64_t _pre338_pre_phi;
    uint64_t var_51;
    uint64_t rsi3_1;
    uint64_t rax_1;
    uint64_t state_0x9018_5;
    uint32_t state_0x8248_0;
    uint32_t state_0x9010_5;
    uint64_t state_0x9018_0;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t rax_2;
    uint64_t storemerge3;
    uint64_t r12_2;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t _pre_phi339;
    uint64_t *_pre_phi336;
    uint64_t r14_0;
    uint64_t local_sp_0;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t rbx_5;
    uint64_t r13_5;
    uint64_t r13_0;
    uint64_t r15_5;
    uint64_t rbx_0;
    uint64_t rdi2_4;
    uint64_t r15_0;
    uint64_t rbp_5;
    uint64_t rdi2_0;
    uint64_t r14_6;
    uint64_t rbp_0;
    uint64_t rsi3_8;
    uint64_t r14_1;
    uint64_t rcx4_6;
    uint64_t rsi3_2;
    uint64_t r10_4;
    uint64_t rcx4_0;
    uint64_t r85_5;
    uint64_t r9_0;
    uint64_t r85_0;
    uint32_t state_0x9010_2;
    uint64_t rax_3;
    uint64_t local_sp_3;
    uint64_t state_0x82d8_2;
    uint32_t state_0x9080_2;
    uint64_t local_sp_6;
    uint32_t state_0x9010_0;
    uint64_t state_0x82d8_5;
    uint64_t local_sp_1;
    uint32_t state_0x9080_5;
    uint64_t state_0x82d8_0;
    uint32_t state_0x9080_0;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_72;
    uint64_t var_73;
    uint32_t var_74;
    uint64_t var_75;
    struct helper_divq_EAX_wrapper_ret_type var_71;
    uint32_t var_76;
    uint64_t var_77;
    uint32_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t r13_3;
    uint64_t r13_2;
    uint64_t cc_dst_0;
    uint32_t cc_op_0;
    uint64_t r13_1;
    uint64_t rbx_1;
    uint64_t r15_1;
    uint64_t rdi2_1;
    uint64_t rbp_1;
    uint64_t r14_2;
    uint64_t rsi3_3;
    uint64_t rcx4_1;
    uint64_t r10_0;
    uint64_t r9_1;
    uint64_t r85_1;
    uint32_t state_0x8248_1;
    uint64_t state_0x9018_1;
    uint32_t state_0x9010_1;
    uint64_t local_sp_2;
    uint64_t state_0x82d8_1;
    uint32_t state_0x9080_1;
    uint64_t var_85;
    uint64_t r13_4;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t rbx_2;
    uint64_t r15_2;
    uint64_t rdi2_2;
    uint64_t rbp_2;
    uint64_t r14_3;
    uint64_t rsi3_4;
    uint64_t rcx4_2;
    uint64_t r10_1;
    uint64_t rdx1_0;
    uint64_t r9_2;
    uint64_t r85_2;
    uint64_t r11_0;
    uint32_t state_0x8248_2;
    uint64_t state_0x9018_2;
    uint64_t rdx1_1;
    uint64_t rax_6;
    uint64_t local_sp_8;
    uint64_t var_30;
    uint64_t rbx_3;
    uint64_t r15_3;
    uint64_t rdi2_3;
    uint64_t rbp_3;
    uint64_t r14_4;
    uint64_t rsi3_5;
    uint64_t rcx4_3;
    uint64_t r10_2;
    uint64_t r9_3;
    uint64_t r85_3;
    uint64_t r11_1;
    uint32_t state_0x8248_3;
    uint64_t state_0x9018_3;
    uint32_t state_0x9010_3;
    uint64_t local_sp_4;
    uint64_t state_0x82d8_3;
    uint32_t state_0x9080_3;
    uint64_t rbx_4;
    uint64_t r15_4;
    uint64_t rbp_4;
    uint64_t r14_5;
    uint64_t rsi3_6;
    uint64_t rcx4_4;
    uint64_t r10_3;
    uint64_t r9_4;
    uint64_t r85_4;
    uint64_t r11_2;
    uint32_t state_0x8248_4;
    uint64_t state_0x9018_4;
    uint32_t state_0x9010_4;
    uint64_t local_sp_5;
    uint64_t state_0x82d8_4;
    uint32_t state_0x9080_4;
    uint64_t var_96;
    uint32_t var_97;
    uint64_t var_98;
    uint64_t rcx4_5;
    uint64_t rax_4;
    uint64_t var_99;
    uint64_t var_100;
    uint32_t *var_101;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t storemerge;
    uint64_t rsi3_7;
    uint64_t rax_5;
    uint64_t var_107;
    uint32_t var_108;
    uint64_t var_109;
    uint32_t *var_110;
    uint32_t var_111;
    uint64_t var_112;
    uint32_t var_113;
    uint32_t var_114;
    uint32_t var_115;
    uint64_t var_116;
    bool var_117;
    uint64_t var_118;
    uint64_t var_119;
    uint64_t var_120;
    uint64_t r9_5;
    uint64_t r11_3;
    uint32_t state_0x8248_5;
    uint64_t var_121;
    uint64_t var_122;
    uint64_t var_123;
    uint64_t *var_124;
    uint64_t var_125;
    uint64_t r13_7;
    uint64_t rcx4_7;
    uint64_t var_127;
    uint64_t var_126;
    uint64_t r13_8;
    uint64_t rbx_8;
    uint64_t rbp_8;
    uint64_t rbx_7;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t rbp_6;
    uint64_t r12_0;
    uint32_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t *_pre_phi;
    uint64_t rbp_7;
    uint64_t r12_1;
    uint64_t var_31;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_59;
    uint64_t *var_60;
    uint64_t var_61;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t *var_42;
    uint64_t var_43;
    uint64_t var_128;
    uint64_t var_129;
    uint64_t rcx4_9;
    uint64_t var_130;
    uint64_t var_131;
    uint64_t rax_7;
    uint64_t state_0x82d8_6;
    uint32_t state_0x9080_6;
    uint64_t var_132;
    uint64_t var_133;
    struct helper_divq_EAX_wrapper_ret_type var_134;
    uint64_t var_135;
    bool var_136;
    uint32_t var_137;
    uint64_t var_138;
    uint64_t r14_8;
    uint64_t r15_7;
    uint64_t rcx4_10;
    uint64_t local_sp_9;
    uint64_t rax_8;
    bool var_139;
    bool var_140;
    uint64_t rdx1_2;
    uint64_t var_141;
    uint64_t rsi3_9;
    uint64_t var_142;
    uint64_t var_143;
    uint64_t var_144;
    uint64_t var_145;
    uint64_t r13_11;
    uint64_t *var_148;
    uint64_t var_149;
    uint64_t *_pre_phi334;
    uint64_t r15_8;
    uint64_t local_sp_10;
    uint64_t *var_146;
    uint64_t var_147;
    uint64_t *_pre_phi335;
    uint64_t var_150;
    uint64_t r13_9;
    uint64_t r15_9;
    uint64_t local_sp_11;
    uint64_t r13_10;
    uint64_t rdx1_3;
    uint32_t *var_151;
    uint32_t var_152;
    uint64_t var_153;
    uint64_t r15_10;
    uint64_t local_sp_12;
    uint64_t var_155;
    uint64_t r15_11;
    uint64_t local_sp_13;
    uint64_t *_pre_phi341;
    uint64_t var_156;
    uint64_t *var_157;
    uint64_t local_sp_14;
    uint64_t var_158;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t *var_34;
    uint64_t var_35;
    bool var_36;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t *var_29;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_r10();
    var_8 = init_cc_src2();
    var_9 = init_r9();
    var_10 = init_state_0x8248();
    var_11 = init_state_0x9018();
    var_12 = init_state_0x9010();
    var_13 = init_state_0x8408();
    var_14 = init_state_0x8328();
    var_15 = init_state_0x82d8();
    var_16 = init_state_0x9080();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_17 = var_0 + (-136L);
    *(uint64_t *)(var_0 + (-128L)) = rsi;
    *(uint64_t *)var_17 = rdx;
    *(uint64_t *)(var_0 + (-120L)) = rcx;
    *(uint64_t *)(var_0 + (-112L)) = r8;
    var_18 = (rdi << 2UL) + 8UL;
    var_19 = var_0 + (-144L);
    var_20 = (uint64_t *)var_19;
    *var_20 = 4224270UL;
    var_21 = indirect_placeholder_6(var_18);
    rdx1_4 = rdx;
    r15_12 = 0UL;
    rax_9 = var_21;
    state_0x8248_6 = var_10;
    state_0x9018_6 = var_11;
    state_0x9010_6 = var_12;
    rsi3_0 = 0UL;
    rax_0 = 0UL;
    rsi3_1 = 0UL;
    rax_1 = 0UL;
    state_0x8248_0 = var_10;
    state_0x9018_0 = var_11;
    rax_2 = 0UL;
    storemerge3 = var_21;
    r12_2 = rdx;
    r14_0 = rcx;
    state_0x9010_0 = var_12;
    state_0x82d8_0 = var_15;
    state_0x9080_0 = var_16;
    cc_op_0 = 25U;
    r13_1 = rsi;
    r15_1 = var_21;
    r14_2 = rcx;
    r10_0 = var_7;
    r9_1 = var_9;
    r85_1 = r8;
    state_0x8248_1 = var_10;
    state_0x9018_1 = var_11;
    state_0x9010_1 = var_12;
    state_0x82d8_1 = var_15;
    state_0x9080_1 = var_16;
    r11_0 = 4294967295UL;
    local_sp_8 = var_19;
    r11_2 = 4294967295UL;
    rcx4_5 = 0UL;
    rax_4 = 0UL;
    rsi3_7 = 0UL;
    rax_5 = 0UL;
    r11_3 = 0UL;
    rbx_8 = 0UL;
    rbp_8 = rdi;
    rbx_7 = rdi;
    r12_0 = rdx;
    _pre_phi = var_20;
    rcx4_9 = 0UL;
    rax_7 = 0UL;
    state_0x82d8_6 = var_15;
    state_0x9080_6 = var_16;
    r14_8 = rcx;
    r15_7 = var_21;
    rcx4_10 = 0UL;
    rdx1_2 = 0UL;
    rsi3_9 = 0UL;
    rdx1_3 = 0UL;
    r15_10 = var_21;
    if (var_21 == 0UL) {
        return r15_12;
    }
    if (rdi == 0UL) {
        rbx_8 = rbx_7;
        while (*(uint32_t *)(((rbx_7 << 2UL) + rsi) + (-4L)) != 0U)
            {
                var_22 = rbx_7 + (-1L);
                rbx_7 = var_22;
                rbx_8 = 0UL;
                if (var_22 == 0UL) {
                    break;
                }
                rbx_8 = rbx_7;
            }
    }
    var_23 = *var_20;
    rbx_0 = rbx_8;
    rbx_1 = rbx_8;
    rcx4_8 = rbx_8;
    if (var_23 == 0UL) {
        rbp_6 = (var_23 << 2UL) + (-4L);
        r12_2 = 0UL;
        rax_9 = 0UL;
        while (1U)
            {
                var_24 = *(uint32_t *)(rbp_6 + rcx);
                rbp_7 = rbp_6;
                r12_1 = r12_0;
                if (var_24 != 0U) {
                    rdx1_1 = r12_0 << 2UL;
                    rax_6 = (uint64_t)var_24;
                    loop_state_var = 1U;
                    break;
                }
                var_25 = rbp_6 + (-4L);
                var_26 = r12_0 + (-1L);
                rbp_6 = var_25;
                r12_0 = var_26;
                rbp_8 = var_25;
                if (var_26 == 0UL) {
                    continue;
                }
                var_27 = r12_0 << 2UL;
                rdx1_4 = var_27;
                loop_state_var = 0U;
                break;
            }
        var_28 = var_0 + (-152L);
        var_29 = (uint64_t *)var_28;
        *var_29 = 4224351UL;
        indirect_placeholder_1();
        rdx1_1 = rdx1_4;
        rax_6 = rax_9;
        local_sp_8 = var_28;
        _pre_phi = var_29;
        rbp_7 = rbp_8;
        r12_1 = r12_2;
    } else {
        var_28 = var_0 + (-152L);
        var_29 = (uint64_t *)var_28;
        *var_29 = 4224351UL;
        indirect_placeholder_1();
        rdx1_1 = rdx1_4;
        rax_6 = rax_9;
        local_sp_8 = var_28;
        _pre_phi = var_29;
        rbp_7 = rbp_8;
        r12_1 = r12_2;
        *_pre_phi = rdx1_1;
        var_30 = rbx_8 - r12_1;
        var_31 = helper_cc_compute_c_wrapper(var_30, r12_1, var_8, 17U);
        cc_src_0 = r12_1;
        rbp_1 = rbp_7;
        rsi3_3 = r12_1;
        rcx4_7 = r12_1;
        local_sp_9 = local_sp_8;
        rax_8 = r12_1;
        if (var_31 == 0UL) {
            if (r12_1 == 1UL) {
                var_128 = var_21 + 4UL;
                var_129 = rbx_8 << 2UL;
                r13_7 = var_128;
                if (rbx_8 != 0UL) {
                    var_130 = (uint64_t)**(uint32_t **)(local_sp_8 + 16UL);
                    var_131 = *(uint64_t *)(local_sp_8 + 8UL);
                    var_132 = rax_7 << 32UL;
                    var_133 = rcx4_8 << 2UL;
                    var_134 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), var_130, var_132 | (uint64_t)*(uint32_t *)((var_133 + var_131) + (-4L)), 0UL, 4224576UL, rbx_8, var_128, rbp_7, var_130, rcx4_8, var_7, var_131, var_129, state_0x8248_6, state_0x9018_6, state_0x9010_6, var_13, var_14, state_0x82d8_6, state_0x9080_6);
                    *(uint32_t *)(var_133 + var_21) = (uint32_t)var_134.field_0;
                    var_135 = rcx4_8 + (-1L);
                    var_136 = (var_135 == 0UL);
                    var_137 = (uint32_t)var_134.field_1;
                    var_138 = (uint64_t)var_137;
                    rcx4_8 = var_135;
                    rax_7 = var_138;
                    while (!var_136)
                        {
                            state_0x8248_6 = var_134.field_6;
                            state_0x9018_6 = var_134.field_7;
                            state_0x9010_6 = var_134.field_8;
                            state_0x82d8_6 = var_134.field_9;
                            state_0x9080_6 = var_134.field_10;
                            var_132 = rax_7 << 32UL;
                            var_133 = rcx4_8 << 2UL;
                            var_134 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), var_130, var_132 | (uint64_t)*(uint32_t *)((var_133 + var_131) + (-4L)), 0UL, 4224576UL, rbx_8, var_128, rbp_7, var_130, rcx4_8, var_7, var_131, var_129, state_0x8248_6, state_0x9018_6, state_0x9010_6, var_13, var_14, state_0x82d8_6, state_0x9080_6);
                            *(uint32_t *)(var_133 + var_21) = (uint32_t)var_134.field_0;
                            var_135 = rcx4_8 + (-1L);
                            var_136 = (var_135 == 0UL);
                            var_137 = (uint32_t)var_134.field_1;
                            var_138 = (uint64_t)var_137;
                            rcx4_8 = var_135;
                            rax_7 = var_138;
                        }
                    if (var_138 == 0UL) {
                        *(uint32_t *)var_21 = var_137;
                        rcx4_9 = 1UL;
                    }
                }
                rcx4_10 = rcx4_9;
                if (*(uint32_t *)(var_129 + var_21) == 0U) {
                    *_pre_phi = (rbx_8 + (-1L));
                    *(uint64_t *)(local_sp_8 + 8UL) = 0UL;
                } else {
                    *_pre_phi = rbx_8;
                    *(uint64_t *)(local_sp_8 + 8UL) = 0UL;
                }
            } else {
                var_37 = (uint64_t)(uint32_t)rax_6;
                var_38 = helper_clz_wrapper(var_37);
                var_39 = (uint64_t)((long)(((var_37 == 0UL) ? rax_6 : (var_38 ^ 63UL)) << 32UL) >> (long)32UL) ^ 31UL;
                *(uint64_t *)(local_sp_8 + 16UL) = var_39;
                if (var_39 == 0UL) {
                    var_59 = local_sp_8 + (-8L);
                    var_60 = (uint64_t *)var_59;
                    *var_60 = 4224413UL;
                    indirect_placeholder_1();
                    var_61 = (rbx_8 << 2UL) + var_21;
                    *(uint32_t *)var_61 = 0U;
                    *_pre_phi = 0UL;
                    _pre_phi339 = var_61;
                    _pre_phi336 = var_60;
                    local_sp_0 = var_59;
                    var_62 = *_pre_phi336;
                    var_63 = var_62 + var_21;
                    *(uint64_t *)(local_sp_0 + 56UL) = var_30;
                    *_pre_phi336 = (var_30 + 1UL);
                    var_64 = (uint64_t)*(uint32_t *)(rbp_7 + r14_0);
                    var_65 = (uint64_t)*(uint32_t *)((var_62 + r14_0) + (-8L));
                    *(uint64_t *)(local_sp_0 + 16UL) = ((var_64 << 32UL) | var_65);
                    var_66 = var_63 + (-4L);
                    *(uint64_t *)(local_sp_0 + 32UL) = (0UL - (r12_1 << 2UL));
                    var_67 = var_65 * 4294967295UL;
                    *(uint64_t *)(local_sp_0 + 40UL) = var_67;
                    *(uint64_t *)(local_sp_0 + 48UL) = var_67;
                    *(uint64_t *)(local_sp_0 + 64UL) = var_63;
                    *(uint64_t *)(local_sp_0 + 72UL) = var_21;
                    r13_0 = var_64;
                    r15_0 = var_65;
                    rdi2_0 = var_65;
                    rbp_0 = var_66;
                    r14_1 = r14_0;
                    rsi3_2 = var_62;
                    rcx4_0 = var_64;
                    r9_0 = var_64;
                    r85_0 = _pre_phi339;
                    local_sp_1 = local_sp_0;
                } else {
                    var_40 = *_pre_phi;
                    var_41 = local_sp_8 + (-8L);
                    var_42 = (uint64_t *)var_41;
                    *var_42 = 4224648UL;
                    var_43 = indirect_placeholder_6(var_40);
                    *_pre_phi = var_43;
                    _pre_phi336 = var_42;
                    local_sp_0 = var_41;
                    rdi2_1 = var_43;
                    if (var_43 != 0UL) {
                        *(uint64_t *)(local_sp_8 + (-16L)) = 4224909UL;
                        indirect_placeholder_1();
                        var_58 = *var_42;
                        r15_12 = var_58;
                        return r15_12;
                    }
                    if (r12_1 == 0UL) {
                        var_44 = *(uint64_t *)(local_sp_8 + 8UL);
                        var_45 = var_44 & 63UL;
                        rcx4_1 = var_44;
                        var_46 = rsi3_0 << 2UL;
                        var_47 = rax_0 + ((uint64_t)*(uint32_t *)(var_46 + rcx) << var_45);
                        *(uint32_t *)(var_46 + var_43) = (uint32_t)var_47;
                        var_48 = var_47 >> 32UL;
                        var_49 = rsi3_0 + 1UL;
                        rsi3_0 = var_49;
                        rax_0 = var_48;
                        cc_dst_0 = var_48;
                        do {
                            var_46 = rsi3_0 << 2UL;
                            var_47 = rax_0 + ((uint64_t)*(uint32_t *)(var_46 + rcx) << var_45);
                            *(uint32_t *)(var_46 + var_43) = (uint32_t)var_47;
                            var_48 = var_47 >> 32UL;
                            var_49 = rsi3_0 + 1UL;
                            rsi3_0 = var_49;
                            rax_0 = var_48;
                            cc_dst_0 = var_48;
                        } while (var_49 != r12_1);
                        if (var_48 == 0UL) {
                            if (rbx_8 == 0UL) {
                                _pre338_pre_phi = (rbx_8 << 2UL) + var_21;
                            } else {
                                var_51 = *(uint64_t *)(local_sp_8 + 8UL) & 63UL;
                                var_52 = rsi3_1 << 2UL;
                                var_53 = rax_1 + ((uint64_t)*(uint32_t *)(var_52 + rsi) << var_51);
                                *(uint32_t *)(var_52 + var_21) = (uint32_t)var_53;
                                var_54 = var_53 >> 32UL;
                                var_55 = rsi3_1 + 1UL;
                                rsi3_1 = var_55;
                                rax_1 = var_54;
                                rax_2 = var_54;
                                do {
                                    var_52 = rsi3_1 << 2UL;
                                    var_53 = rax_1 + ((uint64_t)*(uint32_t *)(var_52 + rsi) << var_51);
                                    *(uint32_t *)(var_52 + var_21) = (uint32_t)var_53;
                                    var_54 = var_53 >> 32UL;
                                    var_55 = rsi3_1 + 1UL;
                                    rsi3_1 = var_55;
                                    rax_1 = var_54;
                                    rax_2 = var_54;
                                } while (var_55 != rbx_8);
                                var_56 = (rbx_8 << 2UL) + var_21;
                                _pre338_pre_phi = var_56;
                                storemerge3 = var_56;
                            }
                            *(uint32_t *)storemerge3 = (uint32_t)rax_2;
                            var_57 = *_pre_phi;
                            _pre_phi339 = _pre338_pre_phi;
                            r14_0 = var_57;
                            var_62 = *_pre_phi336;
                            var_63 = var_62 + var_21;
                            *(uint64_t *)(local_sp_0 + 56UL) = var_30;
                            *_pre_phi336 = (var_30 + 1UL);
                            var_64 = (uint64_t)*(uint32_t *)(rbp_7 + r14_0);
                            var_65 = (uint64_t)*(uint32_t *)((var_62 + r14_0) + (-8L));
                            *(uint64_t *)(local_sp_0 + 16UL) = ((var_64 << 32UL) | var_65);
                            var_66 = var_63 + (-4L);
                            *(uint64_t *)(local_sp_0 + 32UL) = (0UL - (r12_1 << 2UL));
                            var_67 = var_65 * 4294967295UL;
                            *(uint64_t *)(local_sp_0 + 40UL) = var_67;
                            *(uint64_t *)(local_sp_0 + 48UL) = var_67;
                            *(uint64_t *)(local_sp_0 + 64UL) = var_63;
                            *(uint64_t *)(local_sp_0 + 72UL) = var_21;
                            r13_0 = var_64;
                            r15_0 = var_65;
                            rdi2_0 = var_65;
                            rbp_0 = var_66;
                            r14_1 = r14_0;
                            rsi3_2 = var_62;
                            rcx4_0 = var_64;
                            r9_0 = var_64;
                            r85_0 = _pre_phi339;
                            local_sp_1 = local_sp_0;
                        } else {
                            var_50 = local_sp_8 + (-16L);
                            *(uint64_t *)var_50 = 4224924UL;
                            indirect_placeholder_1();
                            local_sp_2 = var_50;
                            var_85 = helper_cc_compute_all_wrapper(cc_dst_0, cc_src_0, var_8, cc_op_0);
                            state_0x9010_2 = state_0x9010_1;
                            local_sp_3 = local_sp_2;
                            state_0x82d8_2 = state_0x82d8_1;
                            state_0x9080_2 = state_0x9080_1;
                            r13_2 = r13_1;
                            r13_4 = r13_1;
                            rbx_2 = rbx_1;
                            r15_2 = r15_1;
                            rdi2_2 = rdi2_1;
                            rbp_2 = rbp_1;
                            r14_3 = r14_2;
                            rsi3_4 = rsi3_3;
                            r10_1 = r10_0;
                            r9_2 = r9_1;
                            r85_2 = r85_1;
                            state_0x8248_2 = state_0x8248_1;
                            state_0x9018_2 = state_0x9018_1;
                            rbx_4 = rbx_1;
                            r15_4 = r15_1;
                            rbp_4 = rbp_1;
                            r14_5 = r14_2;
                            rsi3_6 = rsi3_3;
                            rcx4_4 = rcx4_1;
                            r10_3 = r10_0;
                            r9_4 = r9_1;
                            r85_4 = r85_1;
                            state_0x8248_4 = state_0x8248_1;
                            state_0x9018_4 = state_0x9018_1;
                            state_0x9010_4 = state_0x9010_1;
                            local_sp_5 = local_sp_2;
                            state_0x82d8_4 = state_0x82d8_1;
                            state_0x9080_4 = state_0x9080_1;
                            if ((var_85 & 65UL) == 0UL) {
                                var_86 = (uint64_t)(uint32_t)r9_1;
                                var_87 = (uint64_t)*(uint32_t *)(r85_1 + (-4L));
                                var_88 = var_86 + var_87;
                                var_89 = helper_cc_compute_c_wrapper(var_88, var_87, var_8, 8U);
                                if (var_89 == 0UL) {
                                    var_90 = var_88 << 32UL;
                                    var_91 = (uint64_t)*(uint32_t *)(r85_1 + (-8L));
                                    var_92 = var_90 | var_91;
                                    var_93 = *(uint64_t *)(local_sp_2 + 48UL);
                                    var_94 = *(uint64_t *)(local_sp_2 + 40UL);
                                    var_95 = helper_cc_compute_c_wrapper(var_92 - var_94, var_94, var_8, 17U);
                                    rcx4_2 = var_91;
                                    rdx1_0 = var_92;
                                    rax_3 = var_93;
                                    rcx4_4 = var_91;
                                    if (var_95 == 0UL) {
                                        r13_3 = r13_2;
                                        rbx_3 = rbx_2;
                                        r15_3 = r15_2;
                                        rdi2_3 = rdi2_2;
                                        rbp_3 = rbp_2;
                                        r14_4 = r14_3;
                                        rsi3_5 = rsi3_4;
                                        rcx4_3 = rcx4_2;
                                        r10_2 = r10_1;
                                        r9_3 = r9_2;
                                        r85_3 = r85_2;
                                        r11_1 = (uint64_t)(((uint32_t)r11_0 + (uint32_t)((rax_3 - rdx1_0) <= *(uint64_t *)(local_sp_3 + 16UL))) + (-2));
                                        state_0x8248_3 = state_0x8248_2;
                                        state_0x9018_3 = state_0x9018_2;
                                        state_0x9010_3 = state_0x9010_2;
                                        local_sp_4 = local_sp_3;
                                        state_0x82d8_3 = state_0x82d8_2;
                                        state_0x9080_3 = state_0x9080_2;
                                        state_0x9018_5 = state_0x9018_3;
                                        state_0x9010_5 = state_0x9010_3;
                                        rbx_5 = rbx_3;
                                        r13_5 = r13_3;
                                        r15_5 = r15_3;
                                        rdi2_4 = rdi2_3;
                                        rbp_5 = rbp_3;
                                        r14_6 = r14_4;
                                        rsi3_8 = rsi3_5;
                                        rcx4_6 = rcx4_3;
                                        r10_4 = r10_2;
                                        r85_5 = r85_3;
                                        local_sp_6 = local_sp_4;
                                        state_0x82d8_5 = state_0x82d8_3;
                                        state_0x9080_5 = state_0x9080_3;
                                        r13_4 = r13_3;
                                        rbx_4 = rbx_3;
                                        r15_4 = r15_3;
                                        rbp_4 = rbp_3;
                                        r14_5 = r14_4;
                                        rsi3_6 = rsi3_5;
                                        rcx4_4 = rcx4_3;
                                        r10_3 = r10_2;
                                        r9_4 = r9_3;
                                        r85_4 = r85_3;
                                        r11_2 = r11_1;
                                        state_0x8248_4 = state_0x8248_3;
                                        state_0x9018_4 = state_0x9018_3;
                                        state_0x9010_4 = state_0x9010_3;
                                        local_sp_5 = local_sp_4;
                                        state_0x82d8_4 = state_0x82d8_3;
                                        state_0x9080_4 = state_0x9080_3;
                                        r9_5 = r9_3;
                                        state_0x8248_5 = state_0x8248_3;
                                        var_96 = *(uint64_t *)(local_sp_5 + 32UL) + r85_4;
                                        state_0x9018_5 = state_0x9018_4;
                                        state_0x9010_5 = state_0x9010_4;
                                        rbx_5 = rbx_4;
                                        r13_5 = r13_4;
                                        r15_5 = r15_4;
                                        rdi2_4 = var_96;
                                        rbp_5 = rbp_4;
                                        r14_6 = r14_5;
                                        rsi3_8 = rsi3_6;
                                        rcx4_6 = rcx4_4;
                                        r10_4 = r10_3;
                                        r85_5 = r85_4;
                                        local_sp_6 = local_sp_5;
                                        state_0x82d8_5 = state_0x82d8_4;
                                        state_0x9080_5 = state_0x9080_4;
                                        r9_5 = r9_4;
                                        r11_3 = r11_2;
                                        state_0x8248_5 = state_0x8248_4;
                                        if (r11_1 != 0UL & r12_1 != 0UL) {
                                            var_97 = (uint32_t)r11_2;
                                            var_98 = (uint64_t)var_97;
                                            rbx_5 = var_98;
                                            rcx4_6 = r12_1;
                                            var_99 = rcx4_5 << 2UL;
                                            var_100 = var_98 * (uint64_t)(unsigned __int128)*(uint32_t *)(var_99 + r14_5);
                                            var_101 = (uint32_t *)(var_99 + var_96);
                                            var_102 = rax_4 + (var_100 + (uint64_t)(*var_101 ^ (-1)));
                                            *var_101 = ((uint32_t)var_102 ^ (-1));
                                            var_103 = var_102 >> 32UL;
                                            var_104 = rcx4_5 + 1UL;
                                            rcx4_5 = var_104;
                                            rax_4 = var_103;
                                            rsi3_8 = var_100;
                                            do {
                                                var_99 = rcx4_5 << 2UL;
                                                var_100 = var_98 * (uint64_t)(unsigned __int128)*(uint32_t *)(var_99 + r14_5);
                                                var_101 = (uint32_t *)(var_99 + var_96);
                                                var_102 = rax_4 + (var_100 + (uint64_t)(*var_101 ^ (-1)));
                                                *var_101 = ((uint32_t)var_102 ^ (-1));
                                                var_103 = var_102 >> 32UL;
                                                var_104 = rcx4_5 + 1UL;
                                                rcx4_5 = var_104;
                                                rax_4 = var_103;
                                                rsi3_8 = var_100;
                                            } while (var_104 != r12_1);
                                            var_105 = helper_cc_compute_c_wrapper((uint64_t)*(uint32_t *)r10_3 - var_103, var_103, var_8, 16U);
                                            if (var_105 != 0UL) {
                                                var_106 = (uint64_t)(var_97 + (-1));
                                                r11_3 = var_106;
                                                var_120 = rax_5 + 1UL;
                                                rsi3_7 = storemerge;
                                                rax_5 = var_120;
                                                rsi3_8 = storemerge;
                                                do {
                                                    var_107 = rax_5 << 2UL;
                                                    var_108 = *(uint32_t *)(var_107 + r14_5);
                                                    var_109 = (uint64_t)var_108;
                                                    var_110 = (uint32_t *)(var_107 + var_96);
                                                    var_111 = *var_110;
                                                    var_112 = (uint64_t)var_111;
                                                    var_113 = var_108 + var_111;
                                                    var_114 = (uint32_t)rsi3_7;
                                                    var_115 = var_113 + var_114;
                                                    var_116 = (uint64_t)var_115;
                                                    *var_110 = var_115;
                                                    var_117 = ((uint64_t)var_114 == 0UL);
                                                    var_118 = var_112 ^ 4294967295UL;
                                                    rbx_5 = var_109;
                                                    rcx4_6 = var_116;
                                                    if (var_117) {
                                                        var_119 = helper_cc_compute_c_wrapper(var_118 - var_109, var_109, var_8, 16U);
                                                        storemerge = (uint64_t)(unsigned char)var_119;
                                                    } else {
                                                        storemerge = (var_118 <= var_109);
                                                    }
                                                    var_120 = rax_5 + 1UL;
                                                    rsi3_7 = storemerge;
                                                    rax_5 = var_120;
                                                    rsi3_8 = storemerge;
                                                } while (var_120 != r12_1);
                                            }
                                        }
                                    } else {
                                        var_96 = *(uint64_t *)(local_sp_5 + 32UL) + r85_4;
                                        state_0x9018_5 = state_0x9018_4;
                                        state_0x9010_5 = state_0x9010_4;
                                        rbx_5 = rbx_4;
                                        r13_5 = r13_4;
                                        r15_5 = r15_4;
                                        rdi2_4 = var_96;
                                        rbp_5 = rbp_4;
                                        r14_6 = r14_5;
                                        rsi3_8 = rsi3_6;
                                        rcx4_6 = rcx4_4;
                                        r10_4 = r10_3;
                                        r85_5 = r85_4;
                                        local_sp_6 = local_sp_5;
                                        state_0x82d8_5 = state_0x82d8_4;
                                        state_0x9080_5 = state_0x9080_4;
                                        r9_5 = r9_4;
                                        r11_3 = r11_2;
                                        state_0x8248_5 = state_0x8248_4;
                                        if (r12_1 != 0UL) {
                                            var_97 = (uint32_t)r11_2;
                                            var_98 = (uint64_t)var_97;
                                            rbx_5 = var_98;
                                            rcx4_6 = r12_1;
                                            var_99 = rcx4_5 << 2UL;
                                            var_100 = var_98 * (uint64_t)(unsigned __int128)*(uint32_t *)(var_99 + r14_5);
                                            var_101 = (uint32_t *)(var_99 + var_96);
                                            var_102 = rax_4 + (var_100 + (uint64_t)(*var_101 ^ (-1)));
                                            *var_101 = ((uint32_t)var_102 ^ (-1));
                                            var_103 = var_102 >> 32UL;
                                            var_104 = rcx4_5 + 1UL;
                                            rcx4_5 = var_104;
                                            rax_4 = var_103;
                                            rsi3_8 = var_100;
                                            do {
                                                var_99 = rcx4_5 << 2UL;
                                                var_100 = var_98 * (uint64_t)(unsigned __int128)*(uint32_t *)(var_99 + r14_5);
                                                var_101 = (uint32_t *)(var_99 + var_96);
                                                var_102 = rax_4 + (var_100 + (uint64_t)(*var_101 ^ (-1)));
                                                *var_101 = ((uint32_t)var_102 ^ (-1));
                                                var_103 = var_102 >> 32UL;
                                                var_104 = rcx4_5 + 1UL;
                                                rcx4_5 = var_104;
                                                rax_4 = var_103;
                                                rsi3_8 = var_100;
                                            } while (var_104 != r12_1);
                                            var_105 = helper_cc_compute_c_wrapper((uint64_t)*(uint32_t *)r10_3 - var_103, var_103, var_8, 16U);
                                            if (var_105 != 0UL) {
                                                var_106 = (uint64_t)(var_97 + (-1));
                                                r11_3 = var_106;
                                                var_120 = rax_5 + 1UL;
                                                rsi3_7 = storemerge;
                                                rax_5 = var_120;
                                                rsi3_8 = storemerge;
                                                do {
                                                    var_107 = rax_5 << 2UL;
                                                    var_108 = *(uint32_t *)(var_107 + r14_5);
                                                    var_109 = (uint64_t)var_108;
                                                    var_110 = (uint32_t *)(var_107 + var_96);
                                                    var_111 = *var_110;
                                                    var_112 = (uint64_t)var_111;
                                                    var_113 = var_108 + var_111;
                                                    var_114 = (uint32_t)rsi3_7;
                                                    var_115 = var_113 + var_114;
                                                    var_116 = (uint64_t)var_115;
                                                    *var_110 = var_115;
                                                    var_117 = ((uint64_t)var_114 == 0UL);
                                                    var_118 = var_112 ^ 4294967295UL;
                                                    rbx_5 = var_109;
                                                    rcx4_6 = var_116;
                                                    if (var_117) {
                                                        var_119 = helper_cc_compute_c_wrapper(var_118 - var_109, var_109, var_8, 16U);
                                                        storemerge = (uint64_t)(unsigned char)var_119;
                                                    } else {
                                                        storemerge = (var_118 <= var_109);
                                                    }
                                                    var_120 = rax_5 + 1UL;
                                                    rsi3_7 = storemerge;
                                                    rax_5 = var_120;
                                                    rsi3_8 = storemerge;
                                                } while (var_120 != r12_1);
                                            }
                                        }
                                    }
                                } else {
                                    var_96 = *(uint64_t *)(local_sp_5 + 32UL) + r85_4;
                                    state_0x9018_5 = state_0x9018_4;
                                    state_0x9010_5 = state_0x9010_4;
                                    rbx_5 = rbx_4;
                                    r13_5 = r13_4;
                                    r15_5 = r15_4;
                                    rdi2_4 = var_96;
                                    rbp_5 = rbp_4;
                                    r14_6 = r14_5;
                                    rsi3_8 = rsi3_6;
                                    rcx4_6 = rcx4_4;
                                    r10_4 = r10_3;
                                    r85_5 = r85_4;
                                    local_sp_6 = local_sp_5;
                                    state_0x82d8_5 = state_0x82d8_4;
                                    state_0x9080_5 = state_0x9080_4;
                                    r9_5 = r9_4;
                                    r11_3 = r11_2;
                                    state_0x8248_5 = state_0x8248_4;
                                    if (r12_1 != 0UL) {
                                        var_97 = (uint32_t)r11_2;
                                        var_98 = (uint64_t)var_97;
                                        rbx_5 = var_98;
                                        rcx4_6 = r12_1;
                                        var_99 = rcx4_5 << 2UL;
                                        var_100 = var_98 * (uint64_t)(unsigned __int128)*(uint32_t *)(var_99 + r14_5);
                                        var_101 = (uint32_t *)(var_99 + var_96);
                                        var_102 = rax_4 + (var_100 + (uint64_t)(*var_101 ^ (-1)));
                                        *var_101 = ((uint32_t)var_102 ^ (-1));
                                        var_103 = var_102 >> 32UL;
                                        var_104 = rcx4_5 + 1UL;
                                        rcx4_5 = var_104;
                                        rax_4 = var_103;
                                        rsi3_8 = var_100;
                                        do {
                                            var_99 = rcx4_5 << 2UL;
                                            var_100 = var_98 * (uint64_t)(unsigned __int128)*(uint32_t *)(var_99 + r14_5);
                                            var_101 = (uint32_t *)(var_99 + var_96);
                                            var_102 = rax_4 + (var_100 + (uint64_t)(*var_101 ^ (-1)));
                                            *var_101 = ((uint32_t)var_102 ^ (-1));
                                            var_103 = var_102 >> 32UL;
                                            var_104 = rcx4_5 + 1UL;
                                            rcx4_5 = var_104;
                                            rax_4 = var_103;
                                            rsi3_8 = var_100;
                                        } while (var_104 != r12_1);
                                        var_105 = helper_cc_compute_c_wrapper((uint64_t)*(uint32_t *)r10_3 - var_103, var_103, var_8, 16U);
                                        if (var_105 != 0UL) {
                                            var_106 = (uint64_t)(var_97 + (-1));
                                            r11_3 = var_106;
                                            var_120 = rax_5 + 1UL;
                                            rsi3_7 = storemerge;
                                            rax_5 = var_120;
                                            rsi3_8 = storemerge;
                                            do {
                                                var_107 = rax_5 << 2UL;
                                                var_108 = *(uint32_t *)(var_107 + r14_5);
                                                var_109 = (uint64_t)var_108;
                                                var_110 = (uint32_t *)(var_107 + var_96);
                                                var_111 = *var_110;
                                                var_112 = (uint64_t)var_111;
                                                var_113 = var_108 + var_111;
                                                var_114 = (uint32_t)rsi3_7;
                                                var_115 = var_113 + var_114;
                                                var_116 = (uint64_t)var_115;
                                                *var_110 = var_115;
                                                var_117 = ((uint64_t)var_114 == 0UL);
                                                var_118 = var_112 ^ 4294967295UL;
                                                rbx_5 = var_109;
                                                rcx4_6 = var_116;
                                                if (var_117) {
                                                    var_119 = helper_cc_compute_c_wrapper(var_118 - var_109, var_109, var_8, 16U);
                                                    storemerge = (uint64_t)(unsigned char)var_119;
                                                } else {
                                                    storemerge = (var_118 <= var_109);
                                                }
                                                var_120 = rax_5 + 1UL;
                                                rsi3_7 = storemerge;
                                                rax_5 = var_120;
                                                rsi3_8 = storemerge;
                                            } while (var_120 != r12_1);
                                        }
                                    }
                                }
                            } else {
                                var_96 = *(uint64_t *)(local_sp_5 + 32UL) + r85_4;
                                state_0x9018_5 = state_0x9018_4;
                                state_0x9010_5 = state_0x9010_4;
                                rbx_5 = rbx_4;
                                r13_5 = r13_4;
                                r15_5 = r15_4;
                                rdi2_4 = var_96;
                                rbp_5 = rbp_4;
                                r14_6 = r14_5;
                                rsi3_8 = rsi3_6;
                                rcx4_6 = rcx4_4;
                                r10_4 = r10_3;
                                r85_5 = r85_4;
                                local_sp_6 = local_sp_5;
                                state_0x82d8_5 = state_0x82d8_4;
                                state_0x9080_5 = state_0x9080_4;
                                r9_5 = r9_4;
                                r11_3 = r11_2;
                                state_0x8248_5 = state_0x8248_4;
                                if (r12_1 != 0UL) {
                                    var_97 = (uint32_t)r11_2;
                                    var_98 = (uint64_t)var_97;
                                    rbx_5 = var_98;
                                    rcx4_6 = r12_1;
                                    var_99 = rcx4_5 << 2UL;
                                    var_100 = var_98 * (uint64_t)(unsigned __int128)*(uint32_t *)(var_99 + r14_5);
                                    var_101 = (uint32_t *)(var_99 + var_96);
                                    var_102 = rax_4 + (var_100 + (uint64_t)(*var_101 ^ (-1)));
                                    *var_101 = ((uint32_t)var_102 ^ (-1));
                                    var_103 = var_102 >> 32UL;
                                    var_104 = rcx4_5 + 1UL;
                                    rcx4_5 = var_104;
                                    rax_4 = var_103;
                                    rsi3_8 = var_100;
                                    do {
                                        var_99 = rcx4_5 << 2UL;
                                        var_100 = var_98 * (uint64_t)(unsigned __int128)*(uint32_t *)(var_99 + r14_5);
                                        var_101 = (uint32_t *)(var_99 + var_96);
                                        var_102 = rax_4 + (var_100 + (uint64_t)(*var_101 ^ (-1)));
                                        *var_101 = ((uint32_t)var_102 ^ (-1));
                                        var_103 = var_102 >> 32UL;
                                        var_104 = rcx4_5 + 1UL;
                                        rcx4_5 = var_104;
                                        rax_4 = var_103;
                                        rsi3_8 = var_100;
                                    } while (var_104 != r12_1);
                                    var_105 = helper_cc_compute_c_wrapper((uint64_t)*(uint32_t *)r10_3 - var_103, var_103, var_8, 16U);
                                    if (var_105 != 0UL) {
                                        var_106 = (uint64_t)(var_97 + (-1));
                                        r11_3 = var_106;
                                        var_120 = rax_5 + 1UL;
                                        rsi3_7 = storemerge;
                                        rax_5 = var_120;
                                        rsi3_8 = storemerge;
                                        do {
                                            var_107 = rax_5 << 2UL;
                                            var_108 = *(uint32_t *)(var_107 + r14_5);
                                            var_109 = (uint64_t)var_108;
                                            var_110 = (uint32_t *)(var_107 + var_96);
                                            var_111 = *var_110;
                                            var_112 = (uint64_t)var_111;
                                            var_113 = var_108 + var_111;
                                            var_114 = (uint32_t)rsi3_7;
                                            var_115 = var_113 + var_114;
                                            var_116 = (uint64_t)var_115;
                                            *var_110 = var_115;
                                            var_117 = ((uint64_t)var_114 == 0UL);
                                            var_118 = var_112 ^ 4294967295UL;
                                            rbx_5 = var_109;
                                            rcx4_6 = var_116;
                                            if (var_117) {
                                                var_119 = helper_cc_compute_c_wrapper(var_118 - var_109, var_109, var_8, 16U);
                                                storemerge = (uint64_t)(unsigned char)var_119;
                                            } else {
                                                storemerge = (var_118 <= var_109);
                                            }
                                            var_120 = rax_5 + 1UL;
                                            rsi3_7 = storemerge;
                                            rax_5 = var_120;
                                            rsi3_8 = storemerge;
                                        } while (var_120 != r12_1);
                                    }
                                }
                            }
                            *(uint32_t *)r10_4 = (uint32_t)r11_3;
                            var_121 = r85_5 + (-4L);
                            state_0x8248_0 = state_0x8248_5;
                            state_0x9018_0 = state_0x9018_5;
                            r13_0 = r13_5;
                            rbx_0 = rbx_5;
                            r15_0 = r15_5;
                            rdi2_0 = rdi2_4;
                            rbp_0 = rbp_5;
                            r14_1 = r14_6;
                            rsi3_2 = rsi3_8;
                            rcx4_0 = rcx4_6;
                            r9_0 = r9_5;
                            r85_0 = var_121;
                            state_0x9010_0 = state_0x9010_5;
                            local_sp_1 = local_sp_6;
                            state_0x82d8_0 = state_0x82d8_5;
                            state_0x9080_0 = state_0x9080_5;
                            r14_8 = r14_6;
                            local_sp_9 = local_sp_6;
                            local_sp_10 = local_sp_6;
                            if (var_121 != rbp_5) {
                                var_122 = *(uint64_t *)(local_sp_6 + 64UL);
                                var_123 = *(uint64_t *)(local_sp_6 + 72UL);
                                var_124 = (uint64_t *)local_sp_6;
                                var_125 = *var_124;
                                *var_124 = ((*(uint32_t *)(((var_125 << 2UL) + var_122) + (-4L)) == 0U) ? *(uint64_t *)(local_sp_6 + 56UL) : var_125);
                                r13_7 = var_122;
                                r15_7 = var_123;
                                r13_8 = var_122;
                                r15_8 = var_123;
                                if (r12_1 == 0UL) {
                                    r13_8 = r13_7;
                                    r13_11 = r13_7;
                                    r15_8 = r15_7;
                                    local_sp_10 = local_sp_9;
                                    r13_9 = r13_7;
                                    r15_9 = r15_7;
                                    local_sp_11 = local_sp_9;
                                    r15_11 = r15_7;
                                    local_sp_13 = local_sp_9;
                                    while (1U)
                                        {
                                            var_139 = (rax_8 > rcx4_10);
                                            var_140 = (rax_8 == 0UL);
                                            if (var_139 || var_140) {
                                                rdx1_2 = (uint64_t)(*(uint32_t *)(((rax_8 << 2UL) + r15_7) + (-4L)) >> 31U);
                                            }
                                            var_141 = helper_cc_compute_c_wrapper(rax_8 - rcx4_10, rcx4_10, var_8, 17U);
                                            if (var_141 == 0UL) {
                                                rsi3_9 = (uint64_t)((uint32_t)((uint64_t)*(uint32_t *)((rax_8 << 2UL) + r15_7) << 1UL) & (-2));
                                            }
                                            var_142 = rdx1_2 | rsi3_9;
                                            var_143 = helper_cc_compute_c_wrapper(rax_8 - r12_1, r12_1, var_8, 17U);
                                            if (var_143 == 0UL) {
                                                if (var_142 != 0UL) {
                                                    loop_state_var = 2U;
                                                    break;
                                                }
                                            }
                                            var_144 = (uint64_t)*(uint32_t *)((rax_8 << 2UL) + r14_8);
                                            if (var_142 <= var_144) {
                                                loop_state_var = 2U;
                                                break;
                                            }
                                            var_145 = helper_cc_compute_c_wrapper(var_142 - var_144, var_144, var_8, 16U);
                                            if (var_145 != 0UL) {
                                                loop_state_var = 1U;
                                                break;
                                            }
                                            if (var_140) {
                                                rax_8 = rax_8 + (-1L);
                                                continue;
                                            }
                                            var_148 = (uint64_t *)local_sp_9;
                                            var_149 = *var_148;
                                            _pre_phi334 = var_148;
                                            var_150 = var_149;
                                            if (var_149 != 0UL) {
                                                loop_state_var = 1U;
                                                break;
                                            }
                                            if ((*(unsigned char *)r13_7 & '\x01') != '\x00') {
                                                loop_state_var = 1U;
                                                break;
                                            }
                                            loop_state_var = 0U;
                                            break;
                                        }
                                    switch (loop_state_var) {
                                      case 1U:
                                        {
                                            local_sp_14 = local_sp_13;
                                            r15_12 = r15_11;
                                            if (*(uint64_t *)(local_sp_13 + 8UL) == 0UL) {
                                                _pre_phi341 = (uint64_t *)local_sp_13;
                                            } else {
                                                var_156 = local_sp_13 + (-8L);
                                                var_157 = (uint64_t *)var_156;
                                                *var_157 = 4225388UL;
                                                indirect_placeholder_1();
                                                _pre_phi341 = var_157;
                                                local_sp_14 = var_156;
                                            }
                                            var_158 = *(uint64_t *)(local_sp_14 + 24UL);
                                            *(uint64_t *)(var_158 + 8UL) = r13_11;
                                            *(uint64_t *)var_158 = *_pre_phi341;
                                            return r15_12;
                                        }
                                        break;
                                      case 0U:
                                        {
                                            r13_11 = r13_9;
                                            _pre_phi335 = _pre_phi334;
                                            r13_10 = r13_9;
                                            r15_10 = r15_9;
                                            local_sp_12 = local_sp_11;
                                            r15_11 = r15_9;
                                            local_sp_13 = local_sp_11;
                                            while (1U)
                                                {
                                                    var_151 = (uint32_t *)((rdx1_3 << 2UL) + r13_9);
                                                    var_152 = *var_151 + 1U;
                                                    var_153 = (uint64_t)var_152;
                                                    *var_151 = var_152;
                                                    if (var_153 != 0UL) {
                                                        loop_state_var = 0U;
                                                        break;
                                                    }
                                                    var_154 = rdx1_3 + 1UL;
                                                    rdx1_3 = var_154;
                                                    if (var_150 == var_154) {
                                                        continue;
                                                    }
                                                    loop_state_var = 1U;
                                                    break;
                                                }
                                            var_155 = *_pre_phi335;
                                            *(uint32_t *)((var_155 << 2UL) + r13_10) = 1U;
                                            *_pre_phi335 = (var_155 + 1UL);
                                            r13_11 = r13_10;
                                            r15_11 = r15_10;
                                            local_sp_13 = local_sp_12;
                                        }
                                        break;
                                      case 2U:
                                        {
                                            var_146 = (uint64_t *)local_sp_10;
                                            var_147 = *var_146;
                                            _pre_phi334 = var_146;
                                            _pre_phi335 = var_146;
                                            var_150 = var_147;
                                            r13_9 = r13_8;
                                            r15_9 = r15_8;
                                            local_sp_11 = local_sp_10;
                                            r13_10 = r13_8;
                                            r15_10 = r15_8;
                                            local_sp_12 = local_sp_10;
                                            if (var_147 == 0UL) {
                                                var_155 = *_pre_phi335;
                                                *(uint32_t *)((var_155 << 2UL) + r13_10) = 1U;
                                                *_pre_phi335 = (var_155 + 1UL);
                                                r13_11 = r13_10;
                                                r15_11 = r15_10;
                                                local_sp_13 = local_sp_12;
                                                local_sp_14 = local_sp_13;
                                                r15_12 = r15_11;
                                                if (*(uint64_t *)(local_sp_13 + 8UL) == 0UL) {
                                                    _pre_phi341 = (uint64_t *)local_sp_13;
                                                } else {
                                                    var_156 = local_sp_13 + (-8L);
                                                    var_157 = (uint64_t *)var_156;
                                                    *var_157 = 4225388UL;
                                                    indirect_placeholder_1();
                                                    _pre_phi341 = var_157;
                                                    local_sp_14 = var_156;
                                                }
                                                var_158 = *(uint64_t *)(local_sp_14 + 24UL);
                                                *(uint64_t *)(var_158 + 8UL) = r13_11;
                                                *(uint64_t *)var_158 = *_pre_phi341;
                                                return r15_12;
                                            }
                                            r13_11 = r13_9;
                                            _pre_phi335 = _pre_phi334;
                                            r13_10 = r13_9;
                                            r15_10 = r15_9;
                                            local_sp_12 = local_sp_11;
                                            r15_11 = r15_9;
                                            local_sp_13 = local_sp_11;
                                            while (1U)
                                                {
                                                    var_151 = (uint32_t *)((rdx1_3 << 2UL) + r13_9);
                                                    var_152 = *var_151 + 1U;
                                                    var_153 = (uint64_t)var_152;
                                                    *var_151 = var_152;
                                                    if (var_153 != 0UL) {
                                                        loop_state_var = 0U;
                                                        break;
                                                    }
                                                    var_154 = rdx1_3 + 1UL;
                                                    rdx1_3 = var_154;
                                                    if (var_150 == var_154) {
                                                        continue;
                                                    }
                                                    loop_state_var = 1U;
                                                    break;
                                                }
                                            var_155 = *_pre_phi335;
                                            *(uint32_t *)((var_155 << 2UL) + r13_10) = 1U;
                                            *_pre_phi335 = (var_155 + 1UL);
                                            r13_11 = r13_10;
                                            r15_11 = r15_10;
                                            local_sp_13 = local_sp_12;
                                        }
                                        break;
                                    }
                                } else {
                                    while (1U)
                                        {
                                            rcx4_10 = rcx4_7;
                                            if (*(uint32_t *)(((rcx4_7 << 2UL) + var_123) + (-4L)) == 0U) {
                                                var_127 = rcx4_7 + (-1L);
                                                rcx4_7 = var_127;
                                                rcx4_10 = 0UL;
                                                if (var_127 == 0UL) {
                                                    continue;
                                                }
                                                loop_state_var = 0U;
                                                break;
                                            }
                                            var_126 = helper_cc_compute_c_wrapper(r12_1 - rcx4_7, rcx4_7, var_8, 17U);
                                            if (var_126 != 0UL) {
                                                loop_state_var = 1U;
                                                break;
                                            }
                                            loop_state_var = 0U;
                                            break;
                                        }
                                    switch (loop_state_var) {
                                      case 0U:
                                        {
                                            break;
                                        }
                                        break;
                                      case 1U:
                                        {
                                            var_146 = (uint64_t *)local_sp_10;
                                            var_147 = *var_146;
                                            _pre_phi334 = var_146;
                                            _pre_phi335 = var_146;
                                            var_150 = var_147;
                                            r13_9 = r13_8;
                                            r15_9 = r15_8;
                                            local_sp_11 = local_sp_10;
                                            r13_10 = r13_8;
                                            r15_10 = r15_8;
                                            local_sp_12 = local_sp_10;
                                            if (var_147 != 0UL) {
                                                var_155 = *_pre_phi335;
                                                *(uint32_t *)((var_155 << 2UL) + r13_10) = 1U;
                                                *_pre_phi335 = (var_155 + 1UL);
                                                r13_11 = r13_10;
                                                r15_11 = r15_10;
                                                local_sp_13 = local_sp_12;
                                                local_sp_14 = local_sp_13;
                                                r15_12 = r15_11;
                                                if (*(uint64_t *)(local_sp_13 + 8UL) == 0UL) {
                                                    _pre_phi341 = (uint64_t *)local_sp_13;
                                                } else {
                                                    var_156 = local_sp_13 + (-8L);
                                                    var_157 = (uint64_t *)var_156;
                                                    *var_157 = 4225388UL;
                                                    indirect_placeholder_1();
                                                    _pre_phi341 = var_157;
                                                    local_sp_14 = var_156;
                                                }
                                                var_158 = *(uint64_t *)(local_sp_14 + 24UL);
                                                *(uint64_t *)(var_158 + 8UL) = r13_11;
                                                *(uint64_t *)var_158 = *_pre_phi341;
                                                return r15_12;
                                            }
                                            r13_11 = r13_9;
                                            _pre_phi335 = _pre_phi334;
                                            r13_10 = r13_9;
                                            r15_10 = r15_9;
                                            local_sp_12 = local_sp_11;
                                            r15_11 = r15_9;
                                            local_sp_13 = local_sp_11;
                                            while (1U)
                                                {
                                                    var_151 = (uint32_t *)((rdx1_3 << 2UL) + r13_9);
                                                    var_152 = *var_151 + 1U;
                                                    var_153 = (uint64_t)var_152;
                                                    *var_151 = var_152;
                                                    if (var_153 != 0UL) {
                                                        loop_state_var = 0U;
                                                        break;
                                                    }
                                                    var_154 = rdx1_3 + 1UL;
                                                    rdx1_3 = var_154;
                                                    if (var_150 == var_154) {
                                                        continue;
                                                    }
                                                    loop_state_var = 1U;
                                                    break;
                                                }
                                            switch (loop_state_var) {
                                              case 1U:
                                                {
                                                    var_155 = *_pre_phi335;
                                                    *(uint32_t *)((var_155 << 2UL) + r13_10) = 1U;
                                                    *_pre_phi335 = (var_155 + 1UL);
                                                    r13_11 = r13_10;
                                                    r15_11 = r15_10;
                                                    local_sp_13 = local_sp_12;
                                                }
                                                break;
                                              case 0U:
                                                {
                                                    local_sp_14 = local_sp_13;
                                                    r15_12 = r15_11;
                                                    if (*(uint64_t *)(local_sp_13 + 8UL) == 0UL) {
                                                        _pre_phi341 = (uint64_t *)local_sp_13;
                                                    } else {
                                                        var_156 = local_sp_13 + (-8L);
                                                        var_157 = (uint64_t *)var_156;
                                                        *var_157 = 4225388UL;
                                                        indirect_placeholder_1();
                                                        _pre_phi341 = var_157;
                                                        local_sp_14 = var_156;
                                                    }
                                                    var_158 = *(uint64_t *)(local_sp_14 + 24UL);
                                                    *(uint64_t *)(var_158 + 8UL) = r13_11;
                                                    *(uint64_t *)var_158 = *_pre_phi341;
                                                    return r15_12;
                                                }
                                                break;
                                            }
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                    } else {
                        if (rbx_8 == 0UL) {
                            _pre338_pre_phi = (rbx_8 << 2UL) + var_21;
                        } else {
                            var_51 = *(uint64_t *)(local_sp_8 + 8UL) & 63UL;
                            var_52 = rsi3_1 << 2UL;
                            var_53 = rax_1 + ((uint64_t)*(uint32_t *)(var_52 + rsi) << var_51);
                            *(uint32_t *)(var_52 + var_21) = (uint32_t)var_53;
                            var_54 = var_53 >> 32UL;
                            var_55 = rsi3_1 + 1UL;
                            rsi3_1 = var_55;
                            rax_1 = var_54;
                            rax_2 = var_54;
                            do {
                                var_52 = rsi3_1 << 2UL;
                                var_53 = rax_1 + ((uint64_t)*(uint32_t *)(var_52 + rsi) << var_51);
                                *(uint32_t *)(var_52 + var_21) = (uint32_t)var_53;
                                var_54 = var_53 >> 32UL;
                                var_55 = rsi3_1 + 1UL;
                                rsi3_1 = var_55;
                                rax_1 = var_54;
                                rax_2 = var_54;
                            } while (var_55 != rbx_8);
                            var_56 = (rbx_8 << 2UL) + var_21;
                            _pre338_pre_phi = var_56;
                            storemerge3 = var_56;
                        }
                        *(uint32_t *)storemerge3 = (uint32_t)rax_2;
                        var_57 = *_pre_phi;
                        _pre_phi339 = _pre338_pre_phi;
                        r14_0 = var_57;
                        var_62 = *_pre_phi336;
                        var_63 = var_62 + var_21;
                        *(uint64_t *)(local_sp_0 + 56UL) = var_30;
                        *_pre_phi336 = (var_30 + 1UL);
                        var_64 = (uint64_t)*(uint32_t *)(rbp_7 + r14_0);
                        var_65 = (uint64_t)*(uint32_t *)((var_62 + r14_0) + (-8L));
                        *(uint64_t *)(local_sp_0 + 16UL) = ((var_64 << 32UL) | var_65);
                        var_66 = var_63 + (-4L);
                        *(uint64_t *)(local_sp_0 + 32UL) = (0UL - (r12_1 << 2UL));
                        var_67 = var_65 * 4294967295UL;
                        *(uint64_t *)(local_sp_0 + 40UL) = var_67;
                        *(uint64_t *)(local_sp_0 + 48UL) = var_67;
                        *(uint64_t *)(local_sp_0 + 64UL) = var_63;
                        *(uint64_t *)(local_sp_0 + 72UL) = var_21;
                        r13_0 = var_64;
                        r15_0 = var_65;
                        rdi2_0 = var_65;
                        rbp_0 = var_66;
                        r14_1 = r14_0;
                        rsi3_2 = var_62;
                        rcx4_0 = var_64;
                        r9_0 = var_64;
                        r85_0 = _pre_phi339;
                        local_sp_1 = local_sp_0;
                    }
                }
            }
        } else {
            var_32 = rbx_8 << 2UL;
            var_33 = local_sp_8 + (-8L);
            var_34 = (uint64_t *)var_33;
            *var_34 = 4224459UL;
            indirect_placeholder_1();
            var_35 = var_32 + var_21;
            var_36 = (rbx_8 > r12_1);
            *var_34 = 0UL;
            *_pre_phi = 0UL;
            r13_7 = var_35;
            rcx4_10 = rbx_8;
            local_sp_9 = var_33;
            _pre_phi335 = var_34;
            r13_10 = var_35;
            local_sp_12 = var_33;
            if (!var_36) {
                var_155 = *_pre_phi335;
                *(uint32_t *)((var_155 << 2UL) + r13_10) = 1U;
                *_pre_phi335 = (var_155 + 1UL);
                r13_11 = r13_10;
                r15_11 = r15_10;
                local_sp_13 = local_sp_12;
                local_sp_14 = local_sp_13;
                r15_12 = r15_11;
                if (*(uint64_t *)(local_sp_13 + 8UL) == 0UL) {
                    _pre_phi341 = (uint64_t *)local_sp_13;
                } else {
                    var_156 = local_sp_13 + (-8L);
                    var_157 = (uint64_t *)var_156;
                    *var_157 = 4225388UL;
                    indirect_placeholder_1();
                    _pre_phi341 = var_157;
                    local_sp_14 = var_156;
                }
                var_158 = *(uint64_t *)(local_sp_14 + 24UL);
                *(uint64_t *)(var_158 + 8UL) = r13_11;
                *(uint64_t *)var_158 = *_pre_phi341;
                return r15_12;
            }
            r13_8 = r13_7;
            r13_11 = r13_7;
            r15_8 = r15_7;
            local_sp_10 = local_sp_9;
            r13_9 = r13_7;
            r15_9 = r15_7;
            local_sp_11 = local_sp_9;
            r15_11 = r15_7;
            local_sp_13 = local_sp_9;
            while (1U)
                {
                    var_139 = (rax_8 > rcx4_10);
                    var_140 = (rax_8 == 0UL);
                    if (var_139 || var_140) {
                        rdx1_2 = (uint64_t)(*(uint32_t *)(((rax_8 << 2UL) + r15_7) + (-4L)) >> 31U);
                    }
                    var_141 = helper_cc_compute_c_wrapper(rax_8 - rcx4_10, rcx4_10, var_8, 17U);
                    if (var_141 == 0UL) {
                        rsi3_9 = (uint64_t)((uint32_t)((uint64_t)*(uint32_t *)((rax_8 << 2UL) + r15_7) << 1UL) & (-2));
                    }
                    var_142 = rdx1_2 | rsi3_9;
                    var_143 = helper_cc_compute_c_wrapper(rax_8 - r12_1, r12_1, var_8, 17U);
                    if (var_143 == 0UL) {
                        if (var_142 != 0UL) {
                            loop_state_var = 2U;
                            break;
                        }
                    }
                    var_144 = (uint64_t)*(uint32_t *)((rax_8 << 2UL) + r14_8);
                    if (var_142 <= var_144) {
                        loop_state_var = 2U;
                        break;
                    }
                    var_145 = helper_cc_compute_c_wrapper(var_142 - var_144, var_144, var_8, 16U);
                    if (var_145 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    if (var_140) {
                        rax_8 = rax_8 + (-1L);
                        continue;
                    }
                    var_148 = (uint64_t *)local_sp_9;
                    var_149 = *var_148;
                    _pre_phi334 = var_148;
                    var_150 = var_149;
                    if (var_149 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    if ((*(unsigned char *)r13_7 & '\x01') != '\x00') {
                        loop_state_var = 1U;
                        break;
                    }
                    loop_state_var = 0U;
                    break;
                }
            switch (loop_state_var) {
              case 1U:
                {
                    local_sp_14 = local_sp_13;
                    r15_12 = r15_11;
                    if (*(uint64_t *)(local_sp_13 + 8UL) == 0UL) {
                        _pre_phi341 = (uint64_t *)local_sp_13;
                    } else {
                        var_156 = local_sp_13 + (-8L);
                        var_157 = (uint64_t *)var_156;
                        *var_157 = 4225388UL;
                        indirect_placeholder_1();
                        _pre_phi341 = var_157;
                        local_sp_14 = var_156;
                    }
                    var_158 = *(uint64_t *)(local_sp_14 + 24UL);
                    *(uint64_t *)(var_158 + 8UL) = r13_11;
                    *(uint64_t *)var_158 = *_pre_phi341;
                }
                break;
              case 0U:
                {
                    r13_11 = r13_9;
                    _pre_phi335 = _pre_phi334;
                    r13_10 = r13_9;
                    r15_10 = r15_9;
                    local_sp_12 = local_sp_11;
                    r15_11 = r15_9;
                    local_sp_13 = local_sp_11;
                    while (1U)
                        {
                            var_151 = (uint32_t *)((rdx1_3 << 2UL) + r13_9);
                            var_152 = *var_151 + 1U;
                            var_153 = (uint64_t)var_152;
                            *var_151 = var_152;
                            if (var_153 != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            var_154 = rdx1_3 + 1UL;
                            rdx1_3 = var_154;
                            if (var_150 == var_154) {
                                continue;
                            }
                            loop_state_var = 1U;
                            break;
                        }
                    var_155 = *_pre_phi335;
                    *(uint32_t *)((var_155 << 2UL) + r13_10) = 1U;
                    *_pre_phi335 = (var_155 + 1UL);
                    r13_11 = r13_10;
                    r15_11 = r15_10;
                    local_sp_13 = local_sp_12;
                }
                break;
              case 2U:
                {
                    var_146 = (uint64_t *)local_sp_10;
                    var_147 = *var_146;
                    _pre_phi334 = var_146;
                    _pre_phi335 = var_146;
                    var_150 = var_147;
                    r13_9 = r13_8;
                    r15_9 = r15_8;
                    local_sp_11 = local_sp_10;
                    r13_10 = r13_8;
                    r15_10 = r15_8;
                    local_sp_12 = local_sp_10;
                    if (var_147 == 0UL) {
                        var_155 = *_pre_phi335;
                        *(uint32_t *)((var_155 << 2UL) + r13_10) = 1U;
                        *_pre_phi335 = (var_155 + 1UL);
                        r13_11 = r13_10;
                        r15_11 = r15_10;
                        local_sp_13 = local_sp_12;
                        local_sp_14 = local_sp_13;
                        r15_12 = r15_11;
                        if (*(uint64_t *)(local_sp_13 + 8UL) == 0UL) {
                            _pre_phi341 = (uint64_t *)local_sp_13;
                        } else {
                            var_156 = local_sp_13 + (-8L);
                            var_157 = (uint64_t *)var_156;
                            *var_157 = 4225388UL;
                            indirect_placeholder_1();
                            _pre_phi341 = var_157;
                            local_sp_14 = var_156;
                        }
                        var_158 = *(uint64_t *)(local_sp_14 + 24UL);
                        *(uint64_t *)(var_158 + 8UL) = r13_11;
                        *(uint64_t *)var_158 = *_pre_phi341;
                        return r15_12;
                    }
                    r13_11 = r13_9;
                    _pre_phi335 = _pre_phi334;
                    r13_10 = r13_9;
                    r15_10 = r15_9;
                    local_sp_12 = local_sp_11;
                    r15_11 = r15_9;
                    local_sp_13 = local_sp_11;
                    while (1U)
                        {
                            var_151 = (uint32_t *)((rdx1_3 << 2UL) + r13_9);
                            var_152 = *var_151 + 1U;
                            var_153 = (uint64_t)var_152;
                            *var_151 = var_152;
                            if (var_153 != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            var_154 = rdx1_3 + 1UL;
                            rdx1_3 = var_154;
                            if (var_150 == var_154) {
                                continue;
                            }
                            loop_state_var = 1U;
                            break;
                        }
                    var_155 = *_pre_phi335;
                    *(uint32_t *)((var_155 << 2UL) + r13_10) = 1U;
                    *_pre_phi335 = (var_155 + 1UL);
                    r13_11 = r13_10;
                    r15_11 = r15_10;
                    local_sp_13 = local_sp_12;
                }
                break;
            }
        }
    }
}
