typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct helper_comisd_wrapper_80_ret_type;
struct type_4;
struct type_6;
struct helper_pxor_xmm_wrapper_57_ret_type;
struct helper_mulsd_wrapper_86_ret_type;
struct helper_cvttsd2si_wrapper_ret_type;
struct helper_cvtsq2sd_wrapper_ret_type;
struct helper_subsd_wrapper_ret_type;
struct helper_comisd_wrapper_70_ret_type;
struct helper_ucomisd_wrapper_93_ret_type;
struct helper_comisd_wrapper_80_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct type_4 {
};
struct type_6 {
};
struct helper_pxor_xmm_wrapper_57_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_mulsd_wrapper_86_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvttsd2si_wrapper_ret_type {
    uint32_t field_0;
    unsigned char field_1;
};
struct helper_cvtsq2sd_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_subsd_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_comisd_wrapper_70_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_ucomisd_wrapper_93_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_state_0x8558(void);
extern unsigned char init_state_0x8549(void);
extern unsigned char init_state_0x854c(void);
extern unsigned char init_state_0x8548(void);
extern unsigned char init_state_0x854b(void);
extern unsigned char init_state_0x8547(void);
extern unsigned char init_state_0x854d(void);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern struct helper_comisd_wrapper_80_ret_type helper_comisd_wrapper_80(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_pxor_xmm_wrapper_57_ret_type helper_pxor_xmm_wrapper_57(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_6(uint64_t param_0);
extern struct helper_mulsd_wrapper_86_ret_type helper_mulsd_wrapper_86(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_cvttsd2si_wrapper_ret_type helper_cvttsd2si_wrapper(struct type_4 *param_0, struct type_6 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4);
extern struct helper_cvtsq2sd_wrapper_ret_type helper_cvtsq2sd_wrapper(struct type_4 *param_0, struct type_6 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_subsd_wrapper_ret_type helper_subsd_wrapper(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_comisd_wrapper_70_ret_type helper_comisd_wrapper_70(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_ucomisd_wrapper_93_ret_type helper_ucomisd_wrapper_93(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
uint64_t bb_decode_double(uint64_t rdi, uint64_t rsi) {
    struct helper_cvttsd2si_wrapper_ret_type var_37;
    struct helper_cvttsd2si_wrapper_ret_type var_23;
    struct helper_cvtsq2sd_wrapper_ret_type var_26;
    struct helper_cvttsd2si_wrapper_ret_type var_54;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    unsigned char var_6;
    unsigned char var_7;
    unsigned char var_8;
    unsigned char var_9;
    unsigned char var_10;
    unsigned char var_11;
    uint64_t var_12;
    unsigned char var_17;
    uint64_t var_18;
    struct helper_comisd_wrapper_80_ret_type var_19;
    struct helper_mulsd_wrapper_86_ret_type var_21;
    uint64_t var_22;
    unsigned char var_24;
    uint64_t var_25;
    struct helper_subsd_wrapper_ret_type var_27;
    uint64_t var_28;
    struct helper_comisd_wrapper_70_ret_type var_29;
    uint64_t var_30;
    uint64_t local_sp_6;
    unsigned char var_31;
    uint64_t var_32;
    struct helper_comisd_wrapper_80_ret_type var_33;
    struct helper_mulsd_wrapper_86_ret_type var_35;
    uint64_t var_36;
    unsigned char var_38;
    uint64_t var_39;
    struct helper_cvtsq2sd_wrapper_ret_type var_40;
    struct helper_subsd_wrapper_ret_type var_41;
    uint64_t var_42;
    struct helper_comisd_wrapper_70_ret_type var_43;
    uint64_t var_44;
    uint64_t local_sp_5;
    unsigned char var_45;
    uint64_t var_46;
    struct helper_comisd_wrapper_80_ret_type var_47;
    unsigned char var_49;
    uint64_t var_50;
    uint32_t *var_51;
    struct helper_mulsd_wrapper_86_ret_type var_52;
    uint64_t var_53;
    unsigned char var_55;
    uint64_t var_56;
    struct helper_cvtsq2sd_wrapper_ret_type var_57;
    struct helper_subsd_wrapper_ret_type var_58;
    uint64_t var_59;
    struct helper_comisd_wrapper_70_ret_type var_60;
    uint64_t var_61;
    uint64_t local_sp_4;
    unsigned char var_62;
    uint64_t var_63;
    struct helper_comisd_wrapper_80_ret_type var_64;
    struct helper_mulsd_wrapper_86_ret_type var_66;
    uint64_t var_67;
    struct helper_cvttsd2si_wrapper_ret_type var_68;
    unsigned char var_69;
    uint64_t var_70;
    struct helper_cvtsq2sd_wrapper_ret_type var_71;
    struct helper_subsd_wrapper_ret_type var_72;
    uint64_t var_73;
    struct helper_comisd_wrapper_70_ret_type var_74;
    uint64_t var_75;
    uint64_t local_sp_3;
    struct helper_comisd_wrapper_80_ret_type var_76;
    unsigned char var_78;
    uint64_t var_79;
    struct helper_ucomisd_wrapper_93_ret_type var_80;
    uint64_t var_81;
    uint64_t local_sp_2;
    uint64_t var_82;
    uint64_t storemerge;
    uint64_t local_sp_1;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_77;
    uint64_t var_65;
    uint64_t var_48;
    uint64_t var_34;
    uint64_t var_20;
    uint64_t var_13;
    uint64_t var_14;
    struct helper_comisd_wrapper_70_ret_type var_15;
    uint64_t var_16;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r12();
    var_4 = init_cc_src2();
    var_5 = init_state_0x8558();
    var_6 = init_state_0x8549();
    var_7 = init_state_0x854c();
    var_8 = init_state_0x8548();
    var_9 = init_state_0x854b();
    var_10 = init_state_0x8547();
    var_11 = init_state_0x854d();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-48L)) = var_5;
    *(uint64_t *)(var_0 + (-64L)) = 4227407UL;
    var_12 = indirect_placeholder_6(8UL);
    storemerge = 2UL;
    if (var_12 == 0UL) {
        return var_12;
    }
    var_13 = *(uint64_t *)(var_0 + (-56L));
    var_14 = var_0 + (-72L);
    *(uint64_t *)var_14 = 4227435UL;
    indirect_placeholder_1();
    var_15 = helper_comisd_wrapper_70((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(2824UL), var_13, *(uint64_t *)4266464UL, var_6, var_7);
    var_16 = helper_cc_compute_c_wrapper(var_12, var_15.field_0, var_4, 1U);
    local_sp_6 = var_14;
    local_sp_5 = var_14;
    local_sp_4 = var_14;
    local_sp_3 = var_14;
    local_sp_2 = var_14;
    local_sp_1 = var_14;
    if (var_16 == 0UL) {
        var_17 = var_15.field_1;
        var_18 = *(uint64_t *)4266336UL;
        var_19 = helper_comisd_wrapper_80((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(776UL), var_13, var_18, var_17, var_7);
        if ((var_19.field_0 & 65UL) == 0UL) {
            var_21 = helper_mulsd_wrapper_86((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(2824UL), var_13, *(uint64_t *)4266536UL, var_19.field_1, var_7, var_8, var_9, var_10, var_11);
            var_22 = var_21.field_0;
            var_23 = helper_cvttsd2si_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), var_22, var_21.field_1, var_7);
            var_24 = var_23.field_1;
            var_25 = (uint64_t)var_23.field_0;
            helper_pxor_xmm_wrapper_57((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(840UL), var_18, 0UL);
            var_26 = helper_cvtsq2sd_wrapper((struct type_4 *)(0UL), (struct type_6 *)(840UL), var_25, var_24, var_8, var_9, var_10);
            var_27 = helper_subsd_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(840UL), var_22, var_26.field_0, var_26.field_1, var_7, var_8, var_9, var_10, var_11);
            var_28 = var_27.field_0;
            var_29 = helper_comisd_wrapper_70((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(2824UL), var_28, *(uint64_t *)4266464UL, var_27.field_1, var_7);
            var_30 = helper_cc_compute_c_wrapper(var_12, var_29.field_0, var_4, 1U);
            if (var_30 == 0UL) {
                var_31 = var_29.field_1;
                var_32 = *(uint64_t *)4266336UL;
                var_33 = helper_comisd_wrapper_80((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(776UL), var_28, var_32, var_31, var_7);
                if ((var_33.field_0 & 65UL) == 0UL) {
                    var_35 = helper_mulsd_wrapper_86((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(2824UL), var_28, *(uint64_t *)4266496UL, var_33.field_1, var_7, var_8, var_9, var_10, var_11);
                    var_36 = var_35.field_0;
                    var_37 = helper_cvttsd2si_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), var_36, var_35.field_1, var_7);
                    var_38 = var_37.field_1;
                    var_39 = (uint64_t)var_37.field_0;
                    helper_pxor_xmm_wrapper_57((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(840UL), var_32, 0UL);
                    var_40 = helper_cvtsq2sd_wrapper((struct type_4 *)(0UL), (struct type_6 *)(840UL), var_39, var_38, var_8, var_9, var_10);
                    var_41 = helper_subsd_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(840UL), var_36, var_40.field_0, var_40.field_1, var_7, var_8, var_9, var_10, var_11);
                    var_42 = var_41.field_0;
                    var_43 = helper_comisd_wrapper_70((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(2824UL), var_42, *(uint64_t *)4266464UL, var_41.field_1, var_7);
                    var_44 = helper_cc_compute_c_wrapper(var_12, var_43.field_0, var_4, 1U);
                    if (var_44 == 0UL) {
                        var_45 = var_43.field_1;
                        var_46 = *(uint64_t *)4266336UL;
                        var_47 = helper_comisd_wrapper_80((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(776UL), var_42, var_46, var_45, var_7);
                        if ((var_47.field_0 & 65UL) == 0UL) {
                            var_49 = var_47.field_1;
                            var_50 = (uint64_t)((uint32_t)(var_25 << 16UL) & (-65536)) | var_39;
                            var_51 = (uint32_t *)(var_12 + 4UL);
                            *var_51 = (uint32_t)var_50;
                            var_52 = helper_mulsd_wrapper_86((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(2824UL), var_42, *(uint64_t *)4266496UL, var_49, var_7, var_8, var_9, var_10, var_11);
                            var_53 = var_52.field_0;
                            var_54 = helper_cvttsd2si_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), var_53, var_52.field_1, var_7);
                            var_55 = var_54.field_1;
                            var_56 = (uint64_t)var_54.field_0;
                            helper_pxor_xmm_wrapper_57((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(840UL), var_46, 0UL);
                            var_57 = helper_cvtsq2sd_wrapper((struct type_4 *)(0UL), (struct type_6 *)(840UL), var_56, var_55, var_8, var_9, var_10);
                            var_58 = helper_subsd_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(840UL), var_53, var_57.field_0, var_57.field_1, var_7, var_8, var_9, var_10, var_11);
                            var_59 = var_58.field_0;
                            var_60 = helper_comisd_wrapper_70((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(2824UL), var_59, *(uint64_t *)4266464UL, var_58.field_1, var_7);
                            var_61 = helper_cc_compute_c_wrapper(var_50, var_60.field_0, var_4, 1U);
                            var_62 = var_60.field_1;
                            var_63 = *(uint64_t *)4266336UL;
                            var_64 = helper_comisd_wrapper_80((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(776UL), var_59, var_63, var_62, var_7);
                            if (var_61 != 0UL & (var_64.field_0 & 65UL) != 0UL) {
                                var_66 = helper_mulsd_wrapper_86((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(2824UL), var_59, *(uint64_t *)4266496UL, var_64.field_1, var_7, var_8, var_9, var_10, var_11);
                                var_67 = var_66.field_0;
                                var_68 = helper_cvttsd2si_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), var_67, var_66.field_1, var_7);
                                var_69 = var_68.field_1;
                                var_70 = (uint64_t)var_68.field_0;
                                helper_pxor_xmm_wrapper_57((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(840UL), var_63, 0UL);
                                var_71 = helper_cvtsq2sd_wrapper((struct type_4 *)(0UL), (struct type_6 *)(840UL), var_70, var_69, var_8, var_9, var_10);
                                var_72 = helper_subsd_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(840UL), var_67, var_71.field_0, var_71.field_1, var_7, var_8, var_9, var_10, var_11);
                                var_73 = var_72.field_0;
                                var_74 = helper_comisd_wrapper_70((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(2824UL), var_73, *(uint64_t *)4266464UL, var_72.field_1, var_7);
                                var_75 = helper_cc_compute_c_wrapper(var_50, var_74.field_0, var_4, 1U);
                                if (var_75 == 0UL) {
                                    var_76 = helper_comisd_wrapper_80((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(776UL), var_73, *(uint64_t *)4266336UL, var_74.field_1, var_7);
                                    if ((var_76.field_0 & 65UL) == 0UL) {
                                        var_78 = var_76.field_1;
                                        var_79 = (uint64_t)((uint32_t)(var_56 << 16UL) & (-65536)) | var_70;
                                        *(uint32_t *)var_12 = (uint32_t)var_79;
                                        var_80 = helper_ucomisd_wrapper_93((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(2824UL), var_73, *(uint64_t *)4266464UL, var_78, var_7);
                                        var_81 = var_80.field_0;
                                        if ((var_81 & 4UL) == 0UL) {
                                            var_84 = local_sp_2 + (-8L);
                                            *(uint64_t *)var_84 = 4227797UL;
                                            indirect_placeholder_1();
                                            local_sp_1 = var_84;
                                        } else {
                                            var_82 = helper_cc_compute_all_wrapper(var_79, var_81, var_4, 1U);
                                            if ((var_82 & 64UL) == 0UL) {
                                                var_84 = local_sp_2 + (-8L);
                                                *(uint64_t *)var_84 = 4227797UL;
                                                indirect_placeholder_1();
                                                local_sp_1 = var_84;
                                            } else {
                                                if (*var_51 == 0U) {
                                                    var_83 = (var_79 != 0UL);
                                                    storemerge = var_83;
                                                }
                                            }
                                        }
                                    } else {
                                        var_77 = local_sp_3 + (-8L);
                                        *(uint64_t *)var_77 = 4227792UL;
                                        indirect_placeholder_1();
                                        local_sp_2 = var_77;
                                        var_84 = local_sp_2 + (-8L);
                                        *(uint64_t *)var_84 = 4227797UL;
                                        indirect_placeholder_1();
                                        local_sp_1 = var_84;
                                    }
                                } else {
                                    var_77 = local_sp_3 + (-8L);
                                    *(uint64_t *)var_77 = 4227792UL;
                                    indirect_placeholder_1();
                                    local_sp_2 = var_77;
                                    var_84 = local_sp_2 + (-8L);
                                    *(uint64_t *)var_84 = 4227797UL;
                                    indirect_placeholder_1();
                                    local_sp_1 = var_84;
                                }
                                *(uint64_t *)rsi = storemerge;
                                *(uint64_t *)(rsi + 8UL) = var_12;
                                *(uint32_t *)rdi = (*(uint32_t *)(local_sp_1 + 28UL) + (-53));
                                return var_12;
                            }
                        }
                        var_48 = local_sp_5 + (-8L);
                        *(uint64_t *)var_48 = 4227782UL;
                        indirect_placeholder_1();
                        local_sp_4 = var_48;
                    } else {
                        var_48 = local_sp_5 + (-8L);
                        *(uint64_t *)var_48 = 4227782UL;
                        indirect_placeholder_1();
                        local_sp_4 = var_48;
                    }
                } else {
                    var_34 = local_sp_6 + (-8L);
                    *(uint64_t *)var_34 = 4227777UL;
                    indirect_placeholder_1();
                    local_sp_5 = var_34;
                    var_48 = local_sp_5 + (-8L);
                    *(uint64_t *)var_48 = 4227782UL;
                    indirect_placeholder_1();
                    local_sp_4 = var_48;
                }
            } else {
                var_34 = local_sp_6 + (-8L);
                *(uint64_t *)var_34 = 4227777UL;
                indirect_placeholder_1();
                local_sp_5 = var_34;
                var_48 = local_sp_5 + (-8L);
                *(uint64_t *)var_48 = 4227782UL;
                indirect_placeholder_1();
                local_sp_4 = var_48;
            }
        } else {
            var_20 = var_0 + (-80L);
            *(uint64_t *)var_20 = 4227772UL;
            indirect_placeholder_1();
            local_sp_6 = var_20;
            var_34 = local_sp_6 + (-8L);
            *(uint64_t *)var_34 = 4227777UL;
            indirect_placeholder_1();
            local_sp_5 = var_34;
            var_48 = local_sp_5 + (-8L);
            *(uint64_t *)var_48 = 4227782UL;
            indirect_placeholder_1();
            local_sp_4 = var_48;
        }
    } else {
        var_20 = var_0 + (-80L);
        *(uint64_t *)var_20 = 4227772UL;
        indirect_placeholder_1();
        local_sp_6 = var_20;
        var_34 = local_sp_6 + (-8L);
        *(uint64_t *)var_34 = 4227777UL;
        indirect_placeholder_1();
        local_sp_5 = var_34;
        var_48 = local_sp_5 + (-8L);
        *(uint64_t *)var_48 = 4227782UL;
        indirect_placeholder_1();
        local_sp_4 = var_48;
        var_65 = local_sp_4 + (-8L);
        *(uint64_t *)var_65 = 4227787UL;
        indirect_placeholder_1();
        local_sp_3 = var_65;
        var_77 = local_sp_3 + (-8L);
        *(uint64_t *)var_77 = 4227792UL;
        indirect_placeholder_1();
        local_sp_2 = var_77;
        var_84 = local_sp_2 + (-8L);
        *(uint64_t *)var_84 = 4227797UL;
        indirect_placeholder_1();
        local_sp_1 = var_84;
        *(uint64_t *)rsi = storemerge;
        *(uint64_t *)(rsi + 8UL) = var_12;
        *(uint32_t *)rdi = (*(uint32_t *)(local_sp_1 + 28UL) + (-53));
    }
}
