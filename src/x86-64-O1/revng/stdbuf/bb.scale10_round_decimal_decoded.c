typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_pxor_xmm_wrapper_51_ret_type;
struct type_5;
struct type_7;
struct helper_cvtsq2ss_wrapper_ret_type;
struct helper_addss_wrapper_ret_type;
struct helper_mulss_wrapper_ret_type;
struct helper_ucomiss_wrapper_ret_type;
struct helper_cvttss2sq_wrapper_ret_type;
struct helper_subss_wrapper_ret_type;
struct helper_cvttss2si_wrapper_ret_type;
struct helper_pxor_xmm_wrapper_51_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_5 {
};
struct type_7 {
};
struct helper_cvtsq2ss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_addss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_mulss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_ucomiss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvttss2sq_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_subss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvttss2si_wrapper_ret_type {
    uint32_t field_0;
    unsigned char field_1;
};
extern uint64_t llvm_uadd_sat_i64(uint64_t param_0, uint64_t param_1);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_state_0x8558(void);
extern unsigned char init_state_0x8549(void);
extern unsigned char init_state_0x854c(void);
extern unsigned char init_state_0x8548(void);
extern unsigned char init_state_0x854b(void);
extern unsigned char init_state_0x8547(void);
extern unsigned char init_state_0x854d(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_state_0x8560(void);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct helper_pxor_xmm_wrapper_51_ret_type helper_pxor_xmm_wrapper_51(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_r15(void);
extern uint64_t init_state_0x8d58(void);
extern struct helper_cvtsq2ss_wrapper_ret_type helper_cvtsq2ss_wrapper(struct type_5 *param_0, struct type_7 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_addss_wrapper_ret_type helper_addss_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9);
extern struct helper_mulss_wrapper_ret_type helper_mulss_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_ucomiss_wrapper_ret_type helper_ucomiss_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_cvttss2sq_wrapper_ret_type helper_cvttss2sq_wrapper(struct type_5 *param_0, struct type_7 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4);
extern struct helper_subss_wrapper_ret_type helper_subss_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_cvttss2si_wrapper_ret_type helper_cvttss2si_wrapper(struct type_5 *param_0, struct type_7 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4);
uint64_t bb_scale10_round_decimal_decoded(uint64_t rdx, uint64_t rdi, uint64_t rcx, uint64_t rsi, uint64_t r8) {
    uint64_t rdi2_3;
    uint64_t rax_2;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t rbp_0;
    uint64_t local_sp_0;
    uint64_t var_116;
    uint64_t var_117;
    uint64_t rax_3;
    struct helper_cvtsq2ss_wrapper_ret_type var_126;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    unsigned char var_11;
    unsigned char var_12;
    unsigned char var_13;
    unsigned char var_14;
    unsigned char var_15;
    unsigned char var_16;
    uint64_t rax_4;
    uint64_t var_111;
    uint64_t *var_17;
    uint64_t *var_18;
    uint64_t *var_19;
    uint64_t *var_20;
    uint64_t var_21;
    uint64_t *var_22;
    uint64_t rax_10;
    uint64_t rbp_6;
    uint32_t var_92;
    uint64_t var_93;
    uint64_t var_69;
    uint32_t *var_70;
    uint32_t var_71;
    uint64_t var_72;
    uint64_t rax_5;
    uint64_t var_143;
    uint64_t rax_0;
    uint64_t var_144;
    uint64_t rsi4_1;
    uint64_t rdi2_0;
    uint64_t rbx_0;
    uint64_t rdi2_1;
    uint64_t cc_src2_0;
    uint64_t var_145;
    uint64_t rcx3_0;
    uint64_t rsi4_0;
    uint64_t r85_0;
    uint64_t var_146;
    uint32_t *_cast;
    uint32_t var_147;
    unsigned __int128 var_148;
    uint64_t var_149;
    uint64_t var_150;
    uint64_t var_151;
    uint64_t var_152;
    uint64_t rdi2_2;
    uint64_t rcx3_2;
    uint64_t var_153;
    uint64_t var_154;
    uint64_t var_155;
    uint64_t var_156;
    uint64_t rsi4_2;
    uint64_t var_157;
    uint64_t rsi4_3;
    uint64_t var_158;
    uint64_t rsi4_5;
    uint64_t var_124;
    uint64_t var_125;
    uint64_t state_0x8558_0;
    uint64_t var_127;
    struct helper_cvtsq2ss_wrapper_ret_type var_128;
    struct helper_addss_wrapper_ret_type var_129;
    uint64_t cc_dst_0;
    unsigned char storemerge;
    struct helper_mulss_wrapper_ret_type var_130;
    uint64_t var_131;
    struct helper_ucomiss_wrapper_ret_type var_132;
    uint64_t var_133;
    unsigned char var_134;
    uint64_t var_135;
    struct helper_cvttss2sq_wrapper_ret_type var_136;
    uint64_t rax_1;
    struct helper_subss_wrapper_ret_type var_137;
    struct helper_cvttss2sq_wrapper_ret_type var_138;
    uint64_t var_139;
    uint64_t var_140;
    uint64_t var_141;
    uint64_t var_142;
    uint64_t var_106;
    bool var_107;
    bool var_108;
    uint64_t rdi2_4;
    uint64_t var_118;
    uint64_t var_119;
    uint64_t var_109;
    uint64_t var_110;
    uint64_t rsi4_4;
    uint64_t var_112;
    uint64_t var_113;
    uint64_t var_114;
    uint64_t var_115;
    uint64_t var_120;
    uint64_t var_121;
    uint64_t var_122;
    uint64_t var_123;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t rbp_3;
    uint32_t var_51;
    uint64_t rbp_2;
    uint64_t rbp_1_ph;
    uint64_t r85_1_ph;
    bool var_52;
    uint64_t r85_1;
    uint32_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t rcx3_3;
    uint64_t rax_6;
    uint32_t *var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    bool var_65;
    uint64_t r15_0_in;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_80;
    uint64_t rbp_4;
    uint32_t *_pre_phi314;
    uint64_t rdi2_5;
    uint64_t rsi4_6;
    uint64_t rax_7;
    uint32_t *var_81;
    uint64_t rbp_7;
    uint64_t var_85;
    uint32_t *var_86;
    uint32_t *_pre_phi316;
    uint32_t *var_87;
    uint32_t *var_88;
    uint64_t rbp_5;
    uint64_t var_89;
    uint64_t rax_8;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t rax_9;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t var_98;
    uint64_t *var_99;
    uint64_t var_100;
    uint64_t var_101;
    uint32_t var_102;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t var_23;
    uint32_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t r14_0;
    uint32_t var_29;
    uint32_t var_30;
    uint32_t var_31;
    uint64_t var_32;
    uint32_t var_33;
    uint32_t var_34;
    uint64_t var_35;
    uint32_t var_36;
    struct helper_pxor_xmm_wrapper_51_ret_type var_37;
    uint64_t var_38;
    struct helper_cvtsq2ss_wrapper_ret_type var_39;
    uint64_t var_40;
    unsigned char var_41;
    uint64_t var_42;
    uint64_t var_43;
    struct helper_mulss_wrapper_ret_type var_44;
    uint64_t var_45;
    struct helper_cvttss2si_wrapper_ret_type var_46;
    uint32_t var_47;
    unsigned char var_48;
    uint64_t var_49;
    uint64_t var_50;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r14();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_r12();
    var_6 = init_r15();
    var_7 = init_cc_src2();
    var_8 = init_state_0x8558();
    var_9 = init_state_0x8560();
    var_10 = init_state_0x8d58();
    var_11 = init_state_0x8549();
    var_12 = init_state_0x854c();
    var_13 = init_state_0x8548();
    var_14 = init_state_0x854b();
    var_15 = init_state_0x8547();
    var_16 = init_state_0x854d();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_3;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    var_17 = (uint64_t *)(var_0 + (-120L));
    *var_17 = rsi;
    var_18 = (uint64_t *)(var_0 + (-112L));
    *var_18 = rdx;
    var_19 = (uint64_t *)(var_0 + (-136L));
    *var_19 = rcx;
    var_20 = (uint64_t *)(var_0 + (-104L));
    *var_20 = rsi;
    var_21 = var_0 + (-96L);
    var_22 = (uint64_t *)var_21;
    *var_22 = rdx;
    rax_2 = 0UL;
    rax_3 = 0UL;
    rax_4 = 0UL;
    rax_10 = 0UL;
    rbp_6 = 0UL;
    rax_5 = 0UL;
    cc_src2_0 = var_7;
    rcx3_0 = 0UL;
    rsi4_5 = 1220703125UL;
    rsi4_4 = 0UL;
    rbp_3 = 1UL;
    rbp_1_ph = 1UL;
    r85_1_ph = 0UL;
    rax_6 = 0UL;
    r15_0_in = r8;
    rax_7 = 0UL;
    if (rcx == 0UL) {
        return rax_10;
    }
    var_23 = r8 + rdi;
    var_24 = (uint32_t)var_23;
    var_25 = (uint64_t)var_24;
    var_26 = helper_cc_compute_all_wrapper(var_25, 0UL, 0UL, 24U);
    r14_0 = var_25;
    if ((uint64_t)(((unsigned char)(var_26 >> 4UL) ^ (unsigned char)var_26) & '\xc0') == 0UL) {
        *(uint64_t *)(var_0 + (-128L)) = 0UL;
    } else {
        var_27 = helper_cc_compute_all_wrapper(r8, 0UL, 0UL, 24U);
        if ((uint64_t)(((unsigned char)(var_27 >> 4UL) ^ (unsigned char)var_27) & '\xc0') == 0UL) {
            var_28 = ((long)(var_23 << 32UL) > (long)(r8 << 32UL)) ? r8 : var_25;
            *(uint64_t *)(var_0 + (-128L)) = (uint64_t)((long)(var_28 << 32UL) >> (long)32UL);
            r14_0 = (uint64_t)(var_24 - (uint32_t)var_28);
            r15_0_in = r8 - var_28;
        } else {
            *(uint64_t *)(var_0 + (-128L)) = 0UL;
        }
    }
    var_29 = (uint32_t)r15_0_in;
    var_30 = (uint32_t)(uint64_t)((long)((uint64_t)var_29 << 32UL) >> (long)63UL);
    var_31 = (var_30 ^ var_29) - var_30;
    var_32 = (uint64_t)var_31;
    var_33 = (uint32_t)(uint64_t)((long)(r14_0 << 32UL) >> (long)63UL);
    var_34 = (uint32_t)r14_0;
    var_35 = (uint64_t)((var_33 ^ var_34) - var_33);
    var_36 = (uint32_t)(var_35 >> 5UL) & 134217727U;
    *(uint32_t *)(var_0 + (-140L)) = var_36;
    var_37 = helper_pxor_xmm_wrapper_51((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_8, var_9);
    var_38 = var_37.field_1;
    var_39 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_32, var_11, var_13, var_14, var_15);
    var_40 = var_39.field_0;
    var_41 = var_39.field_1;
    var_42 = (uint64_t)*(uint32_t *)4258600UL;
    var_43 = var_10 & (-4294967296L);
    var_44 = helper_mulss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), var_40, var_43 | var_42, var_41, var_12, var_13, var_14, var_15, var_16);
    var_45 = var_44.field_0;
    var_46 = helper_cvttss2si_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_45, var_44.field_1, var_12);
    var_47 = var_46.field_0;
    var_48 = var_46.field_1;
    var_49 = (((uint64_t)(var_36 + var_47) << 2UL) + 8UL) & 17179869180UL;
    *(uint64_t *)(var_0 + (-160L)) = 4219827UL;
    var_50 = indirect_placeholder_1(var_49);
    rcx3_3 = var_50;
    rsi4_6 = var_50;
    if (var_50 == 0UL) {
        *(uint64_t *)(var_0 + (-168L)) = 4219845UL;
        indirect_placeholder();
    } else {
        *(uint32_t *)var_50 = 1U;
        if (var_32 != 0UL) {
            var_51 = var_31 + 13U;
            rbp_3 = 0UL;
            while (1U)
                {
                    var_52 = (rbp_1_ph == 0UL);
                    r85_1 = r85_1_ph;
                    rbp_2 = rbp_1_ph;
                    while (1U)
                        {
                            var_53 = (uint32_t)r85_1 + 13U;
                            var_54 = (uint64_t)var_53;
                            var_55 = var_32 - var_54;
                            var_56 = helper_cc_compute_c_wrapper(var_55, var_54, var_7, 16U);
                            r85_1_ph = var_54;
                            r85_1 = var_54;
                            if (var_56 != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            if (var_52) {
                                continue;
                            }
                            loop_state_var = 0U;
                            break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 0U:
                        {
                            var_57 = (rbp_1_ph << 2UL) + var_50;
                            var_58 = (uint32_t *)rcx3_3;
                            var_59 = rax_6 + (rsi4_5 * (uint64_t)*var_58);
                            *var_58 = (uint32_t)var_59;
                            var_60 = var_59 >> 32UL;
                            var_61 = rcx3_3 + 4UL;
                            rcx3_3 = var_61;
                            rax_6 = var_60;
                            do {
                                var_58 = (uint32_t *)rcx3_3;
                                var_59 = rax_6 + (rsi4_5 * (uint64_t)*var_58);
                                *var_58 = (uint32_t)var_59;
                                var_60 = var_59 >> 32UL;
                                var_61 = rcx3_3 + 4UL;
                                rcx3_3 = var_61;
                                rax_6 = var_60;
                            } while (var_61 != var_57);
                            if (var_60 == 0UL) {
                                var_62 = rbp_1_ph + 1UL;
                                *(uint32_t *)var_57 = (uint32_t)var_60;
                                rbp_2 = var_62;
                            }
                            var_63 = helper_cc_compute_c_wrapper(var_55, var_54, var_7, 16U);
                            rbp_1_ph = rbp_2;
                            rbp_3 = rbp_2;
                            if (var_63 != 0UL) {
                                continue;
                            }
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 1U:
                        {
                            rsi4_5 = (uint64_t)*(uint32_t *)((((uint64_t)(var_51 - var_53) << 2UL) & 17179869180UL) + 4258272UL);
                            if (var_52) {
                                switch_state_var = 1;
                                break;
                            }
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
        }
        var_64 = var_35 & 31UL;
        var_65 = ((int)var_29 < (int)0U);
        rdi2_5 = rbp_3;
        rbp_4 = rbp_3;
        rbp_5 = rbp_3;
        rbp_7 = rbp_3;
        if (var_65) {
            var_80 = helper_cc_compute_all_wrapper(r14_0, 0UL, 0UL, 24U);
            if ((uint64_t)(((unsigned char)(var_80 >> 4UL) ^ (unsigned char)var_80) & '\xc0') == 0UL) {
                var_102 = *(uint32_t *)(var_0 + (-148L));
                var_103 = (uint64_t)var_102;
                var_104 = ((*(uint64_t *)(var_0 + (-128L)) + var_103) << 2UL) + 4UL;
                *(uint64_t *)(var_0 + (-168L)) = 4221094UL;
                var_105 = indirect_placeholder_1(var_104);
                rdi2_3 = var_105;
                if (var_105 != 0UL) {
                    *(uint64_t *)(var_0 + (-176L)) = 4220359UL;
                    indirect_placeholder();
                    *(uint64_t *)(var_0 + (-184L)) = 4220369UL;
                    indirect_placeholder();
                    return rax_10;
                }
                if (var_102 != 0U) {
                    *(uint32_t *)((rax_2 << 2UL) + var_105) = 0U;
                    var_106 = rax_2 + 1UL;
                    rax_2 = var_106;
                    do {
                        *(uint32_t *)((rax_2 << 2UL) + var_105) = 0U;
                        var_106 = rax_2 + 1UL;
                        rax_2 = var_106;
                    } while (var_106 != var_103);
                    rdi2_3 = (var_103 << 2UL) + var_105;
                }
                var_107 = (var_64 == 0UL);
                var_108 = (*var_19 == 0UL);
                rdi2_4 = rdi2_3;
                if (var_107) {
                    if (!var_108) {
                        var_116 = *var_17;
                        var_117 = *var_18;
                        var_118 = rax_3 << 2UL;
                        *(uint32_t *)(var_118 + rdi2_3) = *(uint32_t *)(var_118 + var_117);
                        var_119 = rax_3 + 1UL;
                        rax_3 = var_119;
                        do {
                            var_118 = rax_3 << 2UL;
                            *(uint32_t *)(var_118 + rdi2_3) = *(uint32_t *)(var_118 + var_117);
                            var_119 = rax_3 + 1UL;
                            rax_3 = var_119;
                        } while (var_119 != var_116);
                        rdi2_4 = (*var_19 << 2UL) + rdi2_3;
                    }
                } else {
                    if (!var_108) {
                        var_109 = *var_17;
                        var_110 = *var_18;
                        var_111 = rsi4_4 << 2UL;
                        var_112 = rax_4 + ((uint64_t)*(uint32_t *)(var_111 + var_110) << var_64);
                        *(uint32_t *)(var_111 + rdi2_3) = (uint32_t)var_112;
                        var_113 = var_112 >> 32UL;
                        var_114 = rsi4_4 + 1UL;
                        rsi4_4 = var_114;
                        rax_4 = var_113;
                        do {
                            var_111 = rsi4_4 << 2UL;
                            var_112 = rax_4 + ((uint64_t)*(uint32_t *)(var_111 + var_110) << var_64);
                            *(uint32_t *)(var_111 + rdi2_3) = (uint32_t)var_112;
                            var_113 = var_112 >> 32UL;
                            var_114 = rsi4_4 + 1UL;
                            rsi4_4 = var_114;
                            rax_4 = var_113;
                        } while (var_114 != var_109);
                        var_115 = (*var_19 << 2UL) + rdi2_3;
                        rdi2_4 = var_115;
                        if (var_113 == 0UL) {
                            *(uint32_t *)var_115 = (uint32_t)var_113;
                            rdi2_4 = var_115 + 4UL;
                        }
                    }
                }
                var_120 = (uint64_t)((long)(rdi2_4 - var_105) >> (long)2UL);
                var_121 = var_0 + (-88L);
                *(uint64_t *)(var_0 + (-176L)) = 4220559UL;
                var_122 = indirect_placeholder_2(rbp_3, var_120, var_50, var_105, var_121);
                var_123 = var_0 + (-184L);
                *(uint64_t *)var_123 = 4220570UL;
                indirect_placeholder();
                rbp_0 = var_122;
                local_sp_0 = var_123;
            } else {
                if (var_64 == 0UL) {
                    if (rbp_3 == 0UL) {
                        var_87 = (uint32_t *)(var_0 + (-148L));
                        _pre_phi314 = var_87;
                        rbp_7 = 0UL;
                        if (*var_87 != 0U) {
                            var_92 = *_pre_phi314;
                            var_93 = (uint64_t)var_92;
                            rax_9 = var_93;
                            if (var_92 == 0U) {
                                var_94 = rax_9 + (-1L);
                                *(uint32_t *)((var_94 << 2UL) + var_50) = 0U;
                                rax_9 = var_94;
                                do {
                                    var_94 = rax_9 + (-1L);
                                    *(uint32_t *)((var_94 << 2UL) + var_50) = 0U;
                                    rax_9 = var_94;
                                } while (var_94 != 0UL);
                            }
                            rbp_7 = rbp_6 + var_93;
                        }
                    } else {
                        var_81 = (uint32_t *)rsi4_6;
                        var_82 = rax_7 + ((uint64_t)*var_81 << var_64);
                        *var_81 = (uint32_t)var_82;
                        var_83 = var_82 >> 32UL;
                        var_84 = rdi2_5 + (-1L);
                        rdi2_5 = var_84;
                        rax_7 = var_83;
                        while (var_84 != 0UL)
                            {
                                rsi4_6 = rsi4_6 + 4UL;
                                var_81 = (uint32_t *)rsi4_6;
                                var_82 = rax_7 + ((uint64_t)*var_81 << var_64);
                                *var_81 = (uint32_t)var_82;
                                var_83 = var_82 >> 32UL;
                                var_84 = rdi2_5 + (-1L);
                                rdi2_5 = var_84;
                                rax_7 = var_83;
                            }
                        if (var_83 == 0UL) {
                            var_86 = (uint32_t *)(var_0 + (-148L));
                            _pre_phi316 = var_86;
                            if (*var_86 != 0U) {
                                var_89 = ((uint64_t)*_pre_phi316 << 2UL) + var_50;
                                rax_8 = rbp_5;
                                _pre_phi314 = _pre_phi316;
                                rbp_6 = rbp_5;
                                var_90 = rax_8 + (-1L);
                                var_91 = var_90 << 2UL;
                                *(uint32_t *)(var_91 + var_89) = *(uint32_t *)(var_91 + var_50);
                                rax_8 = var_90;
                                do {
                                    var_90 = rax_8 + (-1L);
                                    var_91 = var_90 << 2UL;
                                    *(uint32_t *)(var_91 + var_89) = *(uint32_t *)(var_91 + var_50);
                                    rax_8 = var_90;
                                } while (var_90 != 0UL);
                                var_92 = *_pre_phi314;
                                var_93 = (uint64_t)var_92;
                                rax_9 = var_93;
                                if (var_92 == 0U) {
                                    var_94 = rax_9 + (-1L);
                                    *(uint32_t *)((var_94 << 2UL) + var_50) = 0U;
                                    rax_9 = var_94;
                                    do {
                                        var_94 = rax_9 + (-1L);
                                        *(uint32_t *)((var_94 << 2UL) + var_50) = 0U;
                                        rax_9 = var_94;
                                    } while (var_94 != 0UL);
                                }
                                rbp_7 = rbp_6 + var_93;
                            }
                        } else {
                            *(uint32_t *)((rbp_3 << 2UL) + var_50) = (uint32_t)var_83;
                            var_85 = rbp_3 + 1UL;
                            rbp_4 = var_85;
                            var_88 = (uint32_t *)(var_0 + (-148L));
                            _pre_phi316 = var_88;
                            rbp_5 = rbp_4;
                            _pre_phi314 = var_88;
                            rbp_7 = rbp_4;
                            if (*var_88 != 0U) {
                                if (rbp_4 != 0UL) {
                                    var_89 = ((uint64_t)*_pre_phi316 << 2UL) + var_50;
                                    rax_8 = rbp_5;
                                    _pre_phi314 = _pre_phi316;
                                    rbp_6 = rbp_5;
                                    var_90 = rax_8 + (-1L);
                                    var_91 = var_90 << 2UL;
                                    *(uint32_t *)(var_91 + var_89) = *(uint32_t *)(var_91 + var_50);
                                    rax_8 = var_90;
                                    do {
                                        var_90 = rax_8 + (-1L);
                                        var_91 = var_90 << 2UL;
                                        *(uint32_t *)(var_91 + var_89) = *(uint32_t *)(var_91 + var_50);
                                        rax_8 = var_90;
                                    } while (var_90 != 0UL);
                                }
                                var_92 = *_pre_phi314;
                                var_93 = (uint64_t)var_92;
                                rax_9 = var_93;
                                if (var_92 != 0U) {
                                    var_94 = rax_9 + (-1L);
                                    *(uint32_t *)((var_94 << 2UL) + var_50) = 0U;
                                    rax_9 = var_94;
                                    do {
                                        var_94 = rax_9 + (-1L);
                                        *(uint32_t *)((var_94 << 2UL) + var_50) = 0U;
                                        rax_9 = var_94;
                                    } while (var_94 != 0UL);
                                }
                                rbp_7 = rbp_6 + var_93;
                            }
                        }
                    }
                } else {
                    var_88 = (uint32_t *)(var_0 + (-148L));
                    _pre_phi316 = var_88;
                    rbp_5 = rbp_4;
                    _pre_phi314 = var_88;
                    rbp_7 = rbp_4;
                    if (*var_88 != 0U) {
                        if (rbp_4 != 0UL) {
                            var_89 = ((uint64_t)*_pre_phi316 << 2UL) + var_50;
                            rax_8 = rbp_5;
                            _pre_phi314 = _pre_phi316;
                            rbp_6 = rbp_5;
                            var_90 = rax_8 + (-1L);
                            var_91 = var_90 << 2UL;
                            *(uint32_t *)(var_91 + var_89) = *(uint32_t *)(var_91 + var_50);
                            rax_8 = var_90;
                            do {
                                var_90 = rax_8 + (-1L);
                                var_91 = var_90 << 2UL;
                                *(uint32_t *)(var_91 + var_89) = *(uint32_t *)(var_91 + var_50);
                                rax_8 = var_90;
                            } while (var_90 != 0UL);
                        }
                        var_92 = *_pre_phi314;
                        var_93 = (uint64_t)var_92;
                        rax_9 = var_93;
                        if (var_92 == 0U) {
                            var_94 = rax_9 + (-1L);
                            *(uint32_t *)((var_94 << 2UL) + var_50) = 0U;
                            rax_9 = var_94;
                            do {
                                var_94 = rax_9 + (-1L);
                                *(uint32_t *)((var_94 << 2UL) + var_50) = 0U;
                                rax_9 = var_94;
                            } while (var_94 != 0UL);
                        }
                        rbp_7 = rbp_6 + var_93;
                    }
                }
                var_95 = var_0 + (-80L);
                var_96 = *(uint64_t *)(var_0 + (-128L));
                var_97 = *var_17;
                var_98 = var_0 + (-168L);
                var_99 = (uint64_t *)var_98;
                local_sp_0 = var_98;
                if (var_65) {
                    *var_99 = 4220205UL;
                    var_101 = indirect_placeholder_2(rbp_7, var_96, var_50, var_97, var_95);
                    rbp_0 = var_101;
                } else {
                    *var_99 = 4220171UL;
                    var_100 = indirect_placeholder_2(rbp_7, var_96, var_50, var_97, var_95);
                    rbp_0 = var_100;
                }
            }
        } else {
            if ((int)var_34 > (int)4294967295U) {
                var_66 = *(uint64_t *)(var_0 + (-128L));
                var_67 = *var_17;
                *(uint64_t *)(var_0 + (-168L)) = 4221053UL;
                var_68 = indirect_placeholder_2(rbp_3, var_66, var_50, var_67, var_21);
                if (var_68 != 0UL) {
                    *(uint64_t *)(var_0 + (-176L)) = 4220221UL;
                    indirect_placeholder();
                    *(uint64_t *)(var_0 + (-184L)) = 4220231UL;
                    indirect_placeholder();
                    return rax_10;
                }
                var_69 = (rbp_3 << 2UL) + var_50;
                var_70 = (uint32_t *)(var_0 + (-156L));
                var_71 = *var_70;
                var_72 = (uint64_t)var_71;
                if (var_71 != 0U) {
                    *(uint32_t *)((rax_5 << 2UL) + var_69) = 0U;
                    var_73 = rax_5 + 1UL;
                    rax_5 = var_73;
                    do {
                        *(uint32_t *)((rax_5 << 2UL) + var_69) = 0U;
                        var_73 = rax_5 + 1UL;
                        rax_5 = var_73;
                    } while (var_73 != var_72);
                }
                *(uint32_t *)((var_72 << 2UL) + var_69) = (uint32_t)(1UL << var_64);
                var_74 = (uint64_t)(*var_70 + 1U);
                var_75 = var_0 + (-88L);
                var_76 = *var_20;
                var_77 = *var_22;
                *(uint64_t *)(var_0 + (-176L)) = 4220320UL;
                var_78 = indirect_placeholder_2(var_74, var_76, var_69, var_77, var_75);
                var_79 = var_0 + (-184L);
                *(uint64_t *)var_79 = 4220331UL;
                indirect_placeholder();
                rbp_0 = var_78;
                local_sp_0 = var_79;
            } else {
                if (var_64 == 0UL) {
                    if (rbp_3 == 0UL) {
                        var_81 = (uint32_t *)rsi4_6;
                        var_82 = rax_7 + ((uint64_t)*var_81 << var_64);
                        *var_81 = (uint32_t)var_82;
                        var_83 = var_82 >> 32UL;
                        var_84 = rdi2_5 + (-1L);
                        rdi2_5 = var_84;
                        rax_7 = var_83;
                        while (var_84 != 0UL)
                            {
                                rsi4_6 = rsi4_6 + 4UL;
                                var_81 = (uint32_t *)rsi4_6;
                                var_82 = rax_7 + ((uint64_t)*var_81 << var_64);
                                *var_81 = (uint32_t)var_82;
                                var_83 = var_82 >> 32UL;
                                var_84 = rdi2_5 + (-1L);
                                rdi2_5 = var_84;
                                rax_7 = var_83;
                            }
                        if (var_83 == 0UL) {
                            *(uint32_t *)((rbp_3 << 2UL) + var_50) = (uint32_t)var_83;
                            var_85 = rbp_3 + 1UL;
                            rbp_4 = var_85;
                            var_88 = (uint32_t *)(var_0 + (-148L));
                            _pre_phi316 = var_88;
                            rbp_5 = rbp_4;
                            _pre_phi314 = var_88;
                            rbp_7 = rbp_4;
                            if (*var_88 != 0U) {
                                if (rbp_4 != 0UL) {
                                    var_89 = ((uint64_t)*_pre_phi316 << 2UL) + var_50;
                                    rax_8 = rbp_5;
                                    _pre_phi314 = _pre_phi316;
                                    rbp_6 = rbp_5;
                                    var_90 = rax_8 + (-1L);
                                    var_91 = var_90 << 2UL;
                                    *(uint32_t *)(var_91 + var_89) = *(uint32_t *)(var_91 + var_50);
                                    rax_8 = var_90;
                                    do {
                                        var_90 = rax_8 + (-1L);
                                        var_91 = var_90 << 2UL;
                                        *(uint32_t *)(var_91 + var_89) = *(uint32_t *)(var_91 + var_50);
                                        rax_8 = var_90;
                                    } while (var_90 != 0UL);
                                }
                                var_92 = *_pre_phi314;
                                var_93 = (uint64_t)var_92;
                                rax_9 = var_93;
                                if (var_92 == 0U) {
                                    var_94 = rax_9 + (-1L);
                                    *(uint32_t *)((var_94 << 2UL) + var_50) = 0U;
                                    rax_9 = var_94;
                                    do {
                                        var_94 = rax_9 + (-1L);
                                        *(uint32_t *)((var_94 << 2UL) + var_50) = 0U;
                                        rax_9 = var_94;
                                    } while (var_94 != 0UL);
                                }
                                rbp_7 = rbp_6 + var_93;
                            }
                        } else {
                            var_86 = (uint32_t *)(var_0 + (-148L));
                            _pre_phi316 = var_86;
                            if (*var_86 != 0U) {
                                var_89 = ((uint64_t)*_pre_phi316 << 2UL) + var_50;
                                rax_8 = rbp_5;
                                _pre_phi314 = _pre_phi316;
                                rbp_6 = rbp_5;
                                var_90 = rax_8 + (-1L);
                                var_91 = var_90 << 2UL;
                                *(uint32_t *)(var_91 + var_89) = *(uint32_t *)(var_91 + var_50);
                                rax_8 = var_90;
                                do {
                                    var_90 = rax_8 + (-1L);
                                    var_91 = var_90 << 2UL;
                                    *(uint32_t *)(var_91 + var_89) = *(uint32_t *)(var_91 + var_50);
                                    rax_8 = var_90;
                                } while (var_90 != 0UL);
                                var_92 = *_pre_phi314;
                                var_93 = (uint64_t)var_92;
                                rax_9 = var_93;
                                if (var_92 == 0U) {
                                    var_94 = rax_9 + (-1L);
                                    *(uint32_t *)((var_94 << 2UL) + var_50) = 0U;
                                    rax_9 = var_94;
                                    do {
                                        var_94 = rax_9 + (-1L);
                                        *(uint32_t *)((var_94 << 2UL) + var_50) = 0U;
                                        rax_9 = var_94;
                                    } while (var_94 != 0UL);
                                }
                                rbp_7 = rbp_6 + var_93;
                            }
                        }
                    } else {
                        var_87 = (uint32_t *)(var_0 + (-148L));
                        _pre_phi314 = var_87;
                        rbp_7 = 0UL;
                        if (*var_87 != 0U) {
                            var_92 = *_pre_phi314;
                            var_93 = (uint64_t)var_92;
                            rax_9 = var_93;
                            if (var_92 == 0U) {
                                var_94 = rax_9 + (-1L);
                                *(uint32_t *)((var_94 << 2UL) + var_50) = 0U;
                                rax_9 = var_94;
                                do {
                                    var_94 = rax_9 + (-1L);
                                    *(uint32_t *)((var_94 << 2UL) + var_50) = 0U;
                                    rax_9 = var_94;
                                } while (var_94 != 0UL);
                            }
                            rbp_7 = rbp_6 + var_93;
                        }
                    }
                } else {
                    var_88 = (uint32_t *)(var_0 + (-148L));
                    _pre_phi316 = var_88;
                    rbp_5 = rbp_4;
                    _pre_phi314 = var_88;
                    rbp_7 = rbp_4;
                    if (*var_88 != 0U) {
                        if (rbp_4 != 0UL) {
                            var_89 = ((uint64_t)*_pre_phi316 << 2UL) + var_50;
                            rax_8 = rbp_5;
                            _pre_phi314 = _pre_phi316;
                            rbp_6 = rbp_5;
                            var_90 = rax_8 + (-1L);
                            var_91 = var_90 << 2UL;
                            *(uint32_t *)(var_91 + var_89) = *(uint32_t *)(var_91 + var_50);
                            rax_8 = var_90;
                            do {
                                var_90 = rax_8 + (-1L);
                                var_91 = var_90 << 2UL;
                                *(uint32_t *)(var_91 + var_89) = *(uint32_t *)(var_91 + var_50);
                                rax_8 = var_90;
                            } while (var_90 != 0UL);
                        }
                        var_92 = *_pre_phi314;
                        var_93 = (uint64_t)var_92;
                        rax_9 = var_93;
                        if (var_92 == 0U) {
                            var_94 = rax_9 + (-1L);
                            *(uint32_t *)((var_94 << 2UL) + var_50) = 0U;
                            rax_9 = var_94;
                            do {
                                var_94 = rax_9 + (-1L);
                                *(uint32_t *)((var_94 << 2UL) + var_50) = 0U;
                                rax_9 = var_94;
                            } while (var_94 != 0UL);
                        }
                        rbp_7 = rbp_6 + var_93;
                    }
                }
                var_95 = var_0 + (-80L);
                var_96 = *(uint64_t *)(var_0 + (-128L));
                var_97 = *var_17;
                var_98 = var_0 + (-168L);
                var_99 = (uint64_t *)var_98;
                local_sp_0 = var_98;
                if (var_65) {
                    *var_99 = 4220171UL;
                    var_100 = indirect_placeholder_2(rbp_7, var_96, var_50, var_97, var_95);
                    rbp_0 = var_100;
                } else {
                    *var_99 = 4220205UL;
                    var_101 = indirect_placeholder_2(rbp_7, var_96, var_50, var_97, var_95);
                    rbp_0 = var_101;
                }
            }
        }
        *(uint64_t *)(local_sp_0 + (-8L)) = 4220578UL;
        indirect_placeholder();
        *(uint64_t *)(local_sp_0 + (-16L)) = 4220588UL;
        indirect_placeholder();
        if (rbp_0 != 0UL) {
            var_124 = *(uint64_t *)(local_sp_0 + 64UL);
            var_125 = *(uint64_t *)(local_sp_0 + 72UL);
            rbx_0 = var_124;
            cc_dst_0 = var_124;
            if ((long)var_124 < (long)0UL) {
                var_127 = (var_124 >> 1UL) | (var_124 & 1UL);
                helper_pxor_xmm_wrapper_51((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_45, var_38);
                var_128 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_127, var_48, var_13, var_14, var_15);
                var_129 = helper_addss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_128.field_0, var_128.field_1, var_12, var_13, var_14, var_15, var_16);
                state_0x8558_0 = var_129.field_0;
                cc_dst_0 = var_127;
                storemerge = var_129.field_1;
            } else {
                helper_pxor_xmm_wrapper_51((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_45, var_38);
                var_126 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_124, var_48, var_13, var_14, var_15);
                state_0x8558_0 = var_126.field_0;
                storemerge = var_126.field_1;
            }
            var_130 = helper_mulss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), state_0x8558_0, var_43 | (uint64_t)*(uint32_t *)4258604UL, storemerge, var_12, var_13, var_14, var_15, var_16);
            var_131 = var_130.field_0;
            var_132 = helper_ucomiss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), var_131, var_43 | (uint64_t)*(uint32_t *)4258608UL, var_130.field_1, var_12);
            var_133 = var_132.field_0;
            var_134 = var_132.field_1;
            var_135 = helper_cc_compute_c_wrapper(cc_dst_0, var_133, var_7, 1U);
            if (var_135 == 0UL) {
                var_137 = helper_subss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), var_131, var_43 | (uint64_t)*(uint32_t *)4258608UL, var_134, var_12, var_13, var_14, var_15, var_16);
                var_138 = helper_cvttss2sq_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_137.field_0, var_137.field_1, var_12);
                rax_1 = var_138.field_0 ^ (-9223372036854775808L);
            } else {
                var_136 = helper_cvttss2sq_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_131, var_134, var_12);
                rax_1 = var_136.field_0;
            }
            var_139 = (rax_1 * 9UL) + 9UL;
            var_140 = *(uint64_t *)(local_sp_0 + 8UL);
            var_141 = llvm_uadd_sat_i64(var_139, var_140);
            *(uint64_t *)(local_sp_0 + (-24L)) = 4220736UL;
            var_142 = indirect_placeholder_1(var_141);
            rax_0 = var_142;
            rdi2_0 = var_142;
            rax_10 = var_142;
            if (var_142 == 0UL) {
                *(uint64_t *)(local_sp_0 + (-32L)) = 4220999UL;
                indirect_placeholder();
            } else {
                if (var_140 == 0UL) {
                    if (var_124 != 0UL) {
                        var_158 = var_142 + 1UL;
                        *(unsigned char *)var_142 = (unsigned char)'0';
                        rsi4_3 = var_158;
                        *(unsigned char *)rsi4_3 = (unsigned char)'\x00';
                        *(uint64_t *)(local_sp_0 + (-32L)) = 4220999UL;
                        indirect_placeholder();
                        return rax_10;
                    }
                }
                var_143 = var_140 + var_142;
                rdi2_0 = var_143;
                rsi4_1 = var_143;
                var_144 = rax_0 + 1UL;
                *(unsigned char *)rax_0 = (unsigned char)'0';
                rax_0 = var_144;
                do {
                    var_144 = rax_0 + 1UL;
                    *(unsigned char *)rax_0 = (unsigned char)'0';
                    rax_0 = var_144;
                } while (var_144 != var_143);
                if (var_124 == 0UL) {
                    rdi2_1 = rdi2_0;
                    var_155 = helper_cc_compute_c_wrapper((uint64_t)*(uint32_t *)(var_145 + (-4L)) + (-1L), 1UL, cc_src2_0, 16U);
                    var_156 = rbx_0 - var_155;
                    rbx_0 = var_156;
                    cc_src2_0 = var_155;
                    do {
                        var_145 = (rbx_0 << 2UL) + var_125;
                        rsi4_0 = var_145;
                        r85_0 = rbx_0;
                        rdi2_2 = rdi2_1;
                        var_146 = rsi4_0 + (-4L);
                        _cast = (uint32_t *)var_146;
                        var_147 = *_cast;
                        var_148 = ((unsigned __int128)(((rcx3_0 << 32UL) | (uint64_t)var_147) >> 9UL) * 19342813113834067ULL) >> 75ULL;
                        var_149 = (uint64_t)var_148;
                        *_cast = (uint32_t)var_148;
                        var_150 = (uint64_t)(((uint32_t)var_149 * 3294967296U) + var_147);
                        var_151 = r85_0 + (-1L);
                        rcx3_0 = var_150;
                        rsi4_0 = var_146;
                        r85_0 = var_151;
                        rcx3_2 = var_150;
                        do {
                            var_146 = rsi4_0 + (-4L);
                            _cast = (uint32_t *)var_146;
                            var_147 = *_cast;
                            var_148 = ((unsigned __int128)(((rcx3_0 << 32UL) | (uint64_t)var_147) >> 9UL) * 19342813113834067ULL) >> 75ULL;
                            var_149 = (uint64_t)var_148;
                            *_cast = (uint32_t)var_148;
                            var_150 = (uint64_t)(((uint32_t)var_149 * 3294967296U) + var_147);
                            var_151 = r85_0 + (-1L);
                            rcx3_0 = var_150;
                            rsi4_0 = var_146;
                            r85_0 = var_151;
                            rcx3_2 = var_150;
                        } while (var_151 != 0UL);
                        var_152 = rdi2_1 + 9UL;
                        rdi2_1 = var_152;
                        rsi4_1 = var_152;
                        var_153 = rdi2_2 + 1UL;
                        var_154 = (rcx3_2 * 3435973837UL) >> 35UL;
                        *(unsigned char *)rdi2_2 = (((unsigned char)rcx3_2 + ((unsigned char)var_154 * '\xf6')) + '0');
                        rdi2_2 = var_153;
                        rcx3_2 = var_154;
                        do {
                            var_153 = rdi2_2 + 1UL;
                            var_154 = (rcx3_2 * 3435973837UL) >> 35UL;
                            *(unsigned char *)rdi2_2 = (((unsigned char)rcx3_2 + ((unsigned char)var_154 * '\xf6')) + '0');
                            rdi2_2 = var_153;
                            rcx3_2 = var_154;
                        } while (var_153 != var_152);
                        var_155 = helper_cc_compute_c_wrapper((uint64_t)*(uint32_t *)(var_145 + (-4L)) + (-1L), 1UL, cc_src2_0, 16U);
                        var_156 = rbx_0 - var_155;
                        rbx_0 = var_156;
                        cc_src2_0 = var_155;
                    } while (var_156 != 0UL);
                    rsi4_2 = rsi4_1;
                    rsi4_3 = rsi4_1;
                    if (rsi4_1 > var_142) {
                        if (*(unsigned char *)(rsi4_1 + (-1L)) == '0') {
                            if (rsi4_1 == var_142) {
                                var_158 = var_142 + 1UL;
                                *(unsigned char *)var_142 = (unsigned char)'0';
                                rsi4_3 = var_158;
                            }
                        } else {
                            while (1U)
                                {
                                    var_157 = rsi4_2 + (-1L);
                                    rsi4_2 = var_157;
                                    rsi4_3 = var_157;
                                    if (var_157 != var_142) {
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    if (*(unsigned char *)(rsi4_2 + (-2L)) == '0') {
                                        continue;
                                    }
                                    loop_state_var = 0U;
                                    break;
                                }
                            var_158 = var_142 + 1UL;
                            *(unsigned char *)var_142 = (unsigned char)'0';
                            rsi4_3 = var_158;
                        }
                    } else {
                        if (rsi4_1 == var_142) {
                            var_158 = var_142 + 1UL;
                            *(unsigned char *)var_142 = (unsigned char)'0';
                            rsi4_3 = var_158;
                        }
                        *(unsigned char *)rsi4_3 = (unsigned char)'\x00';
                    }
                } else {
                    rsi4_2 = rsi4_1;
                    rsi4_3 = rsi4_1;
                    if (rsi4_1 <= var_142) {
                        if (rsi4_1 == var_142) {
                            var_158 = var_142 + 1UL;
                            *(unsigned char *)var_142 = (unsigned char)'0';
                            rsi4_3 = var_158;
                        }
                        *(unsigned char *)rsi4_3 = (unsigned char)'\x00';
                        *(uint64_t *)(local_sp_0 + (-32L)) = 4220999UL;
                        indirect_placeholder();
                        return rax_10;
                    }
                    if (*(unsigned char *)(rsi4_1 + (-1L)) == '0') {
                        if (rsi4_1 == var_142) {
                            var_158 = var_142 + 1UL;
                            *(unsigned char *)var_142 = (unsigned char)'0';
                            rsi4_3 = var_158;
                        }
                    } else {
                        while (1U)
                            {
                                var_157 = rsi4_2 + (-1L);
                                rsi4_2 = var_157;
                                rsi4_3 = var_157;
                                if (var_157 != var_142) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                if (*(unsigned char *)(rsi4_2 + (-2L)) == '0') {
                                    continue;
                                }
                                loop_state_var = 0U;
                                break;
                            }
                        var_158 = var_142 + 1UL;
                        *(unsigned char *)var_142 = (unsigned char)'0';
                        rsi4_3 = var_158;
                    }
                }
            }
        }
    }
}
