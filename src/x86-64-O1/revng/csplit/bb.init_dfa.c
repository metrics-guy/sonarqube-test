typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_109_ret_type;
struct indirect_placeholder_110_ret_type;
struct indirect_placeholder_109_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_110_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint32_t init_state_0x82fc(void);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r10(void);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern struct indirect_placeholder_109_ret_type indirect_placeholder_109(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
extern struct indirect_placeholder_110_ret_type indirect_placeholder_110(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
uint64_t bb_init_dfa(uint64_t rdi, uint64_t rsi) {
    uint64_t rbx_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint32_t var_10;
    uint64_t *_cast;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t local_sp_0;
    uint64_t local_sp_1;
    uint64_t var_49;
    uint64_t *var_50;
    uint64_t rbp_0;
    uint32_t var_51;
    uint32_t var_52;
    uint64_t r13_0;
    uint32_t var_46;
    bool var_47;
    uint64_t var_48;
    uint64_t rbx_1;
    uint64_t var_19;
    uint64_t var_32;
    uint64_t cc_dst_0;
    uint64_t rcx_0;
    uint64_t cc_src_0;
    uint64_t rdi1_0;
    uint64_t rsi2_0;
    uint64_t cc_dst_1;
    uint64_t rcx_1;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t cc_src_1;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t rbx_2;
    unsigned char *var_39;
    uint64_t rcx_2;
    uint64_t cc_src2_0;
    unsigned char *var_40;
    unsigned char var_41;
    uint64_t var_42;
    struct indirect_placeholder_109_ret_type var_43;
    uint64_t var_44;
    uint64_t *var_45;
    uint64_t rax_2;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_20;
    struct indirect_placeholder_110_ret_type var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t *var_29;
    uint64_t var_30;
    uint32_t *var_31;
    uint64_t rcx_3;
    uint64_t rdi1_1;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r13();
    var_3 = init_rbp();
    var_4 = init_r12();
    var_5 = init_r14();
    var_6 = init_r10();
    var_7 = init_cc_src2();
    var_8 = init_r9();
    var_9 = init_r8();
    var_10 = init_state_0x82fc();
    *(uint64_t *)(var_0 + (-8L)) = var_5;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_3;
    *(uint64_t *)(var_0 + (-40L)) = var_1;
    _cast = (uint64_t *)rdi;
    *_cast = 0UL;
    *(uint64_t *)(rdi + 224UL) = 0UL;
    var_11 = rdi + 8UL;
    var_12 = var_11 & (-8L);
    var_13 = (uint64_t)((uint32_t)((uint64_t)(((uint32_t)rdi - (uint32_t)var_12) + 232U) >> 3UL) & 536870911U);
    var_14 = (uint64_t)var_10;
    var_15 = var_14 << 3UL;
    rbp_0 = 0UL;
    r13_0 = 0UL;
    rbx_1 = 1UL;
    rcx_0 = 2UL;
    cc_src_0 = 45UL;
    rdi1_0 = 4321137UL;
    rcx_1 = 0UL;
    rbx_2 = 1UL;
    cc_src2_0 = var_7;
    rax_2 = 12UL;
    rcx_3 = var_13;
    rdi1_1 = var_12;
    while (rcx_3 != 0UL)
        {
            *(uint64_t *)rdi1_1 = 0UL;
            rcx_3 = rcx_3 + (-1L);
            rdi1_1 = rdi1_1 + var_15;
        }
    *(uint32_t *)(rdi + 128UL) = 15U;
    if (rsi > 384307168202282324UL) {
        return rax_2;
    }
    var_16 = rsi + 1UL;
    *(uint64_t *)var_11 = var_16;
    var_17 = var_16 << 4UL;
    *(uint64_t *)(var_0 + (-48L)) = 4233563UL;
    var_18 = indirect_placeholder_1(var_17);
    *_cast = var_18;
    if (rsi == 0UL) {
        var_19 = rbx_1 << 1UL;
        var_20 = helper_cc_compute_c_wrapper(rsi - var_19, var_19, var_7, 17U);
        rbx_1 = var_19;
        rbx_2 = var_19;
        do {
            var_19 = rbx_1 << 1UL;
            var_20 = helper_cc_compute_c_wrapper(rsi - var_19, var_19, var_7, 17U);
            rbx_1 = var_19;
            rbx_2 = var_19;
        } while (var_20 != 0UL);
    }
    *(uint64_t *)(var_0 + (-56L)) = 4233602UL;
    var_21 = indirect_placeholder_110(rbx_2, 0UL, 24UL, rsi, rbx_2, var_6, var_8, var_9);
    var_22 = var_21.field_0;
    var_23 = var_21.field_1;
    var_24 = var_21.field_2;
    var_25 = var_21.field_3;
    var_26 = var_21.field_4;
    var_27 = var_21.field_5;
    var_28 = var_21.field_6;
    var_29 = (uint64_t *)(rdi + 64UL);
    *var_29 = var_22;
    var_30 = var_23 + (-1L);
    *(uint64_t *)(rdi + 136UL) = var_30;
    *(uint64_t *)(var_0 + (-64L)) = 4233624UL;
    indirect_placeholder();
    var_31 = (uint32_t *)(rdi + 180UL);
    *var_31 = (uint32_t)var_22;
    *(uint64_t *)(var_0 + (-72L)) = 4233642UL;
    indirect_placeholder();
    rcx_2 = var_24;
    if ((*(unsigned char *)var_22 & '\xdf') != 'U' & (*(unsigned char *)(var_22 + 1UL) & '\xdf') != 'T' & (*(unsigned char *)(var_22 + 2UL) & '\xdf') != 'F') {
        var_32 = (uint64_t)*(unsigned char *)(var_22 + 3UL) + (-45L);
        cc_dst_0 = var_32;
        rsi2_0 = (var_22 + ((uint64_t)(unsigned char)var_32 == 0UL)) + 3UL;
        cc_dst_1 = cc_dst_0;
        cc_src_1 = cc_src_0;
        while (rcx_0 != 0UL)
            {
                var_33 = (uint64_t)*(unsigned char *)rdi1_0;
                var_34 = (uint64_t)*(unsigned char *)rsi2_0 - var_33;
                var_35 = rcx_0 + (-1L);
                cc_dst_0 = var_34;
                rcx_0 = var_35;
                cc_src_0 = var_33;
                cc_dst_1 = var_34;
                rcx_1 = var_35;
                cc_src_1 = var_33;
                if ((uint64_t)(unsigned char)var_34 == 0UL) {
                    break;
                }
                rdi1_0 = rdi1_0 + var_14;
                rsi2_0 = rsi2_0 + var_14;
                cc_dst_1 = cc_dst_0;
                cc_src_1 = cc_src_0;
            }
        var_36 = helper_cc_compute_all_wrapper(cc_dst_1, cc_src_1, var_7, 14U);
        var_37 = ((var_36 & 65UL) == 0UL);
        var_38 = helper_cc_compute_c_wrapper(cc_dst_1, var_36, var_7, 1U);
        rcx_2 = rcx_1;
        cc_src2_0 = var_38;
        if ((uint64_t)((unsigned char)var_37 - (unsigned char)var_38) == 0UL) {
            var_39 = (unsigned char *)(rdi + 176UL);
            *var_39 = (*var_39 | '\x04');
        }
    }
    var_40 = (unsigned char *)(rdi + 176UL);
    var_41 = *var_40 & '\xf7';
    *var_40 = var_41;
    if ((int)*var_31 <= (int)1U) {
        if ((var_41 & '\x04') != '\x00') {
            var_42 = var_0 + (-80L);
            *(uint64_t *)var_42 = 4233818UL;
            var_43 = indirect_placeholder_109(var_30, rcx_2, 32UL, var_25, 1UL, var_26, var_27, var_28);
            var_44 = var_43.field_0;
            var_45 = (uint64_t *)(rdi + 120UL);
            *var_45 = var_44;
            local_sp_0 = var_42;
            if (var_44 == 0UL) {
                return rax_2;
            }
            var_46 = (uint32_t)var_44;
            var_47 = ((uint64_t)(var_46 + 1U) == 0UL);
            while (1U)
                {
                    rbx_0 = (uint64_t)((uint32_t)(r13_0 << 3UL) & (-64));
                    local_sp_1 = local_sp_0;
                    while (1U)
                        {
                            var_48 = local_sp_1 + (-8L);
                            *(uint64_t *)var_48 = 4233869UL;
                            indirect_placeholder();
                            local_sp_0 = var_48;
                            local_sp_1 = var_48;
                            if (var_47) {
                                var_49 = 1UL << (rbp_0 & 63UL);
                                var_50 = (uint64_t *)(r13_0 + *var_45);
                                *var_50 = (*var_50 | var_49);
                            }
                            var_51 = (uint32_t)rbx_0;
                            if ((uint64_t)(var_51 & (-128)) != 0UL & (uint64_t)(var_51 - var_46) == 0UL) {
                                *var_40 = (*var_40 | '\b');
                            }
                            var_52 = (uint32_t)rbp_0;
                            if ((uint64_t)(var_52 + (-63)) == 0UL) {
                                break;
                            }
                            rbx_0 = (uint64_t)(var_51 + 1U);
                            rbp_0 = (uint64_t)(var_52 + 1U);
                            continue;
                        }
                    if (r13_0 == 24UL) {
                        break;
                    }
                    r13_0 = r13_0 + 8UL;
                    continue;
                }
        }
        *(uint64_t *)(rdi + 120UL) = 4330144UL;
    }
    if (*_cast != 0UL) {
        return rax_2;
    }
    var_53 = helper_cc_compute_c_wrapper(*var_29 + (-1L), 1UL, cc_src2_0, 17U);
    var_54 = (0UL - var_53) & 12UL;
    rax_2 = var_54;
    return rax_2;
}
