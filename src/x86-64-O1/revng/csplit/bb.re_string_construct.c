typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint32_t init_state_0x82fc(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_15(uint64_t param_0, uint64_t param_1);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_4(uint64_t param_0);
extern void indirect_placeholder_26(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
uint64_t bb_re_string_construct(uint64_t rcx, uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t r9, uint64_t r8) {
    uint64_t local_sp_1;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint32_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t rax_0;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t local_sp_0;
    uint64_t *var_17;
    uint64_t *_pre_phi;
    uint32_t *var_19;
    uint64_t r13_0;
    bool var_18;
    bool var_20;
    uint64_t *var_21;
    uint64_t *var_22;
    uint64_t *var_23;
    uint64_t var_24;
    uint64_t var_29;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t rcx1_0;
    uint64_t rdi3_0;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r13();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_state_0x82fc();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    *(uint64_t *)rdi = 0UL;
    *(uint64_t *)(rdi + 144UL) = 0UL;
    var_8 = rdi + 8UL;
    var_9 = var_8 & (-8L);
    var_10 = (uint64_t)var_7 << 3UL;
    r13_0 = rsi;
    rax_0 = 0UL;
    rcx1_0 = (uint64_t)((uint32_t)((uint64_t)(((uint32_t)rdi - (uint32_t)var_9) + 152U) >> 3UL) & 536870911U);
    rdi3_0 = var_9;
    while (rcx1_0 != 0UL)
        {
            *(uint64_t *)rdi3_0 = 0UL;
            rcx1_0 = rcx1_0 + (-1L);
            rdi3_0 = rdi3_0 + var_10;
        }
    var_11 = (uint64_t)(unsigned char)r8;
    var_12 = var_0 + (-64L);
    *(uint64_t *)var_12 = 4236232UL;
    indirect_placeholder_26(rcx, rdi, rsi, rdx, r9, var_11);
    var_13 = helper_cc_compute_all_wrapper(rdx, 0UL, 0UL, 25U);
    local_sp_0 = var_12;
    if ((uint64_t)(((unsigned char)(var_13 >> 4UL) ^ (unsigned char)var_13) & '\xc0') != 0UL) {
        var_14 = rdx + 1UL;
        var_15 = var_0 + (-72L);
        *(uint64_t *)var_15 = 4236338UL;
        var_16 = indirect_placeholder_15(rdi, var_14);
        local_sp_0 = var_15;
        rax_0 = var_16;
        if ((uint64_t)(uint32_t)var_16 == 0UL) {
            return rax_0;
        }
    }
    local_sp_1 = local_sp_0;
    if (*(unsigned char *)(rdi + 139UL) == '\x00') {
        _pre_phi = (uint64_t *)var_8;
    } else {
        var_17 = (uint64_t *)var_8;
        _pre_phi = var_17;
        r13_0 = *var_17;
    }
    *_pre_phi = r13_0;
    var_18 = (var_11 == 0UL);
    var_19 = (uint32_t *)(r9 + 180UL);
    var_20 = ((int)*var_19 > (int)1U);
    if (var_18) {
        if (var_20) {
            *(uint64_t *)(local_sp_0 + (-8L)) = 4236398UL;
            indirect_placeholder_4(rdi);
        } else {
            if (rcx == 0UL) {
                var_29 = *(uint64_t *)(rdi + 64UL);
                *(uint64_t *)(rdi + 48UL) = var_29;
                *(uint64_t *)(rdi + 56UL) = var_29;
            } else {
                *(uint64_t *)(local_sp_0 + (-8L)) = 4236383UL;
                indirect_placeholder_4(rdi);
            }
        }
    } else {
        if (!var_20) {
            var_21 = (uint64_t *)(rdi + 56UL);
            var_22 = (uint64_t *)(rdi + 64UL);
            var_23 = (uint64_t *)(rdi + 48UL);
            *(uint64_t *)(local_sp_1 + (-8L)) = 4236278UL;
            var_24 = indirect_placeholder_1(rdi);
            rax_0 = var_24;
            while ((uint64_t)(uint32_t)var_24 != 0UL)
                {
                    if ((long)*var_21 < (long)rdx) {
                        break;
                    }
                    var_25 = *var_22;
                    if ((long)var_25 > (long)(*var_23 + (uint64_t)*var_19)) {
                        break;
                    }
                    var_26 = var_25 << 1UL;
                    var_27 = local_sp_1 + (-16L);
                    *(uint64_t *)var_27 = 4236320UL;
                    var_28 = indirect_placeholder_15(rdi, var_26);
                    local_sp_1 = var_27;
                    rax_0 = var_28;
                    if ((uint64_t)(uint32_t)var_28 == 0UL) {
                        break;
                    }
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4236278UL;
                    var_24 = indirect_placeholder_1(rdi);
                    rax_0 = var_24;
                }
        }
        *(uint64_t *)(local_sp_0 + (-8L)) = 4236352UL;
        indirect_placeholder_4(rdi);
    }
    return rax_0;
}
