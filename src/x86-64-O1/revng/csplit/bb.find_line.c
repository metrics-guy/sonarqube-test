typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_find_line_ret_type;
struct indirect_placeholder_69_ret_type;
struct indirect_placeholder_68_ret_type;
struct bb_find_line_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_69_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_68_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
extern struct indirect_placeholder_69_ret_type indirect_placeholder_69(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r12(void);
extern struct indirect_placeholder_68_ret_type indirect_placeholder_68(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
typedef _Bool bool;
struct bb_find_line_ret_type bb_find_line(uint64_t r13, uint64_t rdi, uint64_t r14) {
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    struct indirect_placeholder_69_ret_type var_7;
    struct indirect_placeholder_68_ret_type var_17;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_22;
    uint64_t rbx_0_ph;
    uint64_t local_sp_1_ph_be;
    uint64_t var_23;
    uint64_t local_sp_1_ph;
    uint64_t var_13;
    uint64_t local_sp_0;
    uint64_t r133_0;
    uint64_t rbp_0;
    uint64_t r145_0;
    uint64_t r133_2;
    uint64_t r133_1_ph;
    uint64_t rbp_1_ph;
    uint64_t r145_1_ph;
    uint64_t rbx_0;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t r12_0;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t rdx_0;
    uint64_t rdx_1;
    uint64_t rdi4_0;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t rdi4_1;
    uint64_t var_29;
    uint64_t var_24;
    uint64_t r145_2;
    struct bb_find_line_ret_type mrv;
    struct bb_find_line_ret_type mrv1;
    struct bb_find_line_ret_type mrv2;
    uint64_t var_6;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r12();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_4 = var_0 + (-24L);
    *(uint64_t *)var_4 = var_1;
    var_5 = *(uint64_t *)4359456UL;
    var_13 = var_5;
    local_sp_0 = var_4;
    r133_0 = r13;
    rbp_0 = rdi;
    r145_0 = r14;
    r12_0 = 0UL;
    if (var_5 != 0UL) {
        var_6 = var_0 + (-32L);
        *(uint64_t *)var_6 = 4209409UL;
        var_7 = indirect_placeholder_69(var_1, r13, rdi, 0UL, r14);
        var_8 = var_7.field_0;
        var_9 = var_7.field_2;
        var_10 = var_7.field_3;
        var_11 = var_7.field_4;
        var_12 = var_7.field_5;
        local_sp_0 = var_6;
        r133_0 = var_9;
        rbp_0 = var_10;
        r145_0 = var_12;
        r133_2 = var_9;
        r12_0 = var_11;
        r145_2 = var_12;
        if ((uint64_t)(unsigned char)var_8 != 0UL) {
            mrv.field_0 = r12_0;
            mrv1 = mrv;
            mrv1.field_1 = r133_2;
            mrv2 = mrv1;
            mrv2.field_2 = r145_2;
            return mrv2;
        }
        var_13 = *(uint64_t *)4359456UL;
    }
    rbx_0_ph = var_13;
    local_sp_1_ph = local_sp_0;
    r133_2 = r133_0;
    r133_1_ph = r133_0;
    rbp_1_ph = rbp_0;
    r145_1_ph = r145_0;
    r145_2 = r145_0;
    if (*(uint64_t *)(var_13 + 16UL) > rbp_0) {
        mrv.field_0 = r12_0;
        mrv1 = mrv;
        mrv1.field_1 = r133_2;
        mrv2 = mrv1;
        mrv2.field_2 = r145_2;
        return mrv2;
    }
    while (1U)
        {
            rbx_0 = rbx_0_ph;
            r133_2 = r133_1_ph;
            r145_2 = r145_1_ph;
            while (1U)
                {
                    var_14 = *(uint64_t *)(rbx_0 + 16UL);
                    if ((var_14 + *(uint64_t *)(rbx_0 + 32UL)) > rbp_1_ph) {
                        var_15 = *(uint64_t *)(rbx_0 + 64UL);
                        rbx_0 = var_15;
                        if (var_15 == 0UL) {
                            continue;
                        }
                        loop_state_var = 0U;
                        break;
                    }
                    var_25 = *(uint64_t *)(rbx_0 + 48UL);
                    var_26 = rbp_1_ph - var_14;
                    rdx_0 = var_25;
                    rdi4_0 = var_26;
                    rdx_1 = var_25;
                    rdi4_1 = var_26;
                    if (var_26 <= 79UL) {
                        loop_state_var = 2U;
                        break;
                    }
                    loop_state_var = 1U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    var_16 = local_sp_1_ph + (-8L);
                    *(uint64_t *)var_16 = 4209470UL;
                    var_17 = indirect_placeholder_68(rbx_0, r133_1_ph, rbp_1_ph, 0UL, r145_1_ph);
                    var_18 = var_17.field_0;
                    var_19 = var_17.field_2;
                    var_20 = var_17.field_3;
                    var_21 = var_17.field_5;
                    local_sp_1_ph_be = var_16;
                    r133_2 = var_19;
                    r133_1_ph = var_19;
                    rbp_1_ph = var_20;
                    r145_1_ph = var_21;
                    r145_2 = var_21;
                    if ((uint64_t)(unsigned char)var_18 != 0UL) {
                        var_24 = var_17.field_4;
                        r12_0 = var_24;
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    var_22 = *(uint64_t *)(var_17.field_1 + 64UL);
                    rbx_0_ph = var_22;
                    if (var_22 == 0UL) {
                        var_23 = local_sp_1_ph + (-16L);
                        *(uint64_t *)var_23 = 4209508UL;
                        indirect_placeholder();
                        local_sp_1_ph_be = var_23;
                    }
                    local_sp_1_ph = local_sp_1_ph_be;
                    continue;
                }
                break;
              case 1U:
                {
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 2U:
                {
                    loop_state_var = 2U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 1U:
        {
            var_29 = ((rdi4_1 << 4UL) + rdx_1) + 24UL;
            r12_0 = var_29;
        }
        break;
      case 2U:
        {
            var_27 = *(uint64_t *)(rdx_0 + 1304UL);
            var_28 = rdi4_0 + (-80L);
            rdx_0 = var_27;
            rdi4_0 = var_28;
            rdx_1 = var_27;
            rdi4_1 = var_28;
            do {
                var_27 = *(uint64_t *)(rdx_0 + 1304UL);
                var_28 = rdi4_0 + (-80L);
                rdx_0 = var_27;
                rdi4_0 = var_28;
                rdx_1 = var_27;
                rdi4_1 = var_28;
            } while (var_28 <= 79UL);
        }
        break;
    }
}
