typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_2(uint64_t param_0, uint64_t param_1);
uint64_t bb_re_node_set_add_intersect(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t var_8;
    uint64_t rdx1_2;
    uint64_t *_pre_phi105;
    uint64_t *var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t *var_14;
    uint64_t var_15;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t local_sp_0;
    uint64_t *var_24;
    uint64_t r9_1;
    uint64_t rax_0_ph;
    uint64_t rcx_0_ph;
    uint64_t rsi3_0_ph;
    uint64_t r9_0_ph;
    uint64_t rcx_0_ph76;
    uint64_t rsi3_0_ph77;
    uint64_t rsi3_0;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t r9_2;
    uint64_t var_30;
    uint64_t var_34;
    uint64_t var_31;
    uint64_t rax_3;
    uint64_t rax_1;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_35;
    uint64_t rax_2;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t rcx_1_ph;
    uint64_t r8_0;
    uint64_t rdx1_0_ph;
    uint64_t r8_0_ph;
    uint64_t var_44;
    uint64_t rdx1_0;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    bool var_48;
    uint64_t *var_49;
    uint64_t var_51;
    uint64_t var_50;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t *var_18;
    uint64_t var_19;
    uint64_t var_20;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r13();
    var_3 = init_rbp();
    var_4 = init_r12();
    var_5 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_1;
    var_6 = var_0 + (-40L);
    var_7 = (uint64_t *)(rsi + 8UL);
    var_8 = *var_7;
    var_22 = var_8;
    local_sp_0 = var_6;
    rdx1_2 = 0UL;
    var_9 = (uint64_t *)(rdx + 8UL);
    var_10 = *var_9;
    var_21 = var_10;
    if (~(var_8 != 0UL & var_10 != 0UL)) {
        return rdx1_2;
    }
    var_11 = var_8 + var_10;
    var_12 = (uint64_t *)rdi;
    var_13 = *var_12;
    var_14 = (uint64_t *)(rdi + 8UL);
    var_15 = *var_14;
    var_23 = var_15;
    rdx1_2 = 12UL;
    if ((long)(var_11 + var_15) > (long)var_13) {
        _pre_phi105 = (uint64_t *)(rdi + 16UL);
    } else {
        var_16 = var_13 + var_11;
        var_17 = var_16 << 3UL;
        var_18 = (uint64_t *)(rdi + 16UL);
        var_19 = *var_18;
        var_20 = var_0 + (-48L);
        *(uint64_t *)var_20 = 4237933UL;
        indirect_placeholder_2(var_19, var_17);
        _pre_phi105 = var_18;
        local_sp_0 = var_20;
        if (var_11 != 0UL) {
            return rdx1_2;
        }
        *var_18 = var_11;
        *var_12 = var_16;
        var_21 = *var_9;
        var_22 = *var_7;
        var_23 = *var_14;
    }
    var_24 = (uint64_t *)(rdx + 16UL);
    rax_0_ph = var_23 + (-1L);
    rcx_0_ph = var_22 + (-1L);
    rsi3_0_ph = var_21 + (-1L);
    r9_0_ph = (var_22 + var_23) + var_21;
    while (1U)
        {
            r9_1 = r9_0_ph;
            rcx_0_ph76 = rcx_0_ph;
            rsi3_0_ph77 = rsi3_0_ph;
            r9_2 = r9_0_ph;
            rax_1 = rax_0_ph;
            rax_2 = rax_0_ph;
            while (1U)
                {
                    rsi3_0 = rsi3_0_ph77;
                    while (1U)
                        {
                            var_25 = *(uint64_t *)((rcx_0_ph76 << 3UL) + *(uint64_t *)(rsi + 16UL));
                            var_26 = *(uint64_t *)((rsi3_0 << 3UL) + *var_24);
                            var_27 = var_25 - var_26;
                            rsi3_0_ph77 = rsi3_0;
                            if (var_27 != 0UL) {
                                loop_state_var = 2U;
                                break;
                            }
                            var_28 = helper_cc_compute_all_wrapper(var_27, var_26, var_5, 17U);
                            if ((signed char)((unsigned char)(var_28 >> 4UL) ^ (unsigned char)var_28) <= '\xff') {
                                loop_state_var = 1U;
                                break;
                            }
                            var_29 = rsi3_0 + (-1L);
                            rsi3_0 = var_29;
                            if ((long)var_29 > (long)18446744073709551615UL) {
                                continue;
                            }
                            loop_state_var = 0U;
                            break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 1U:
                        {
                            var_30 = rcx_0_ph76 + (-1L);
                            rcx_0_ph76 = var_30;
                            if ((long)var_30 < (long)0UL) {
                                continue;
                            }
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 0U:
                        {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 2U:
                        {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 1U:
                {
                    if ((long)rax_0_ph >= (long)0UL) {
                        var_31 = *_pre_phi105;
                        var_34 = var_31;
                        while (1U)
                            {
                                var_32 = *(uint64_t *)((rax_1 << 3UL) + var_31);
                                rax_2 = rax_1;
                                rax_3 = rax_1;
                                if ((long)var_25 >= (long)var_32) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                var_33 = rax_1 + (-1L);
                                rax_1 = var_33;
                                rax_2 = var_33;
                                if (rax_1 == 0UL) {
                                    continue;
                                }
                                loop_state_var = 1U;
                                break;
                            }
                        if (!(((long)rax_1 >= (long)0UL) && (var_25 == var_32))) {
                            var_36 = rcx_0_ph76 + (-1L);
                            rax_0_ph = rax_3;
                            rcx_0_ph = var_36;
                            r9_0_ph = r9_1;
                            r9_2 = r9_1;
                            if ((long)var_36 < (long)0UL) {
                                switch_state_var = 1;
                                break;
                            }
                            var_37 = rsi3_0 + (-1L);
                            rsi3_0_ph = var_37;
                            if ((long)var_37 <= (long)18446744073709551615UL) {
                                continue;
                            }
                            switch_state_var = 1;
                            break;
                        }
                    }
                    var_34 = *_pre_phi105;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    var_38 = *var_14;
    var_39 = var_38 + (-1L);
    var_40 = (var_38 + *var_7) + *var_9;
    var_41 = var_40 + (-1L);
    var_42 = var_40 - r9_2;
    *var_14 = (var_38 + var_42);
    var_43 = helper_cc_compute_all_wrapper(var_42, 0UL, 0UL, 25U);
    rcx_1_ph = var_39;
    rdx1_0_ph = var_42;
    r8_0_ph = var_41;
    if (!(((uint64_t)(((unsigned char)(var_43 >> 4UL) ^ (unsigned char)var_43) & '\xc0') == 0UL) && ((long)var_39 > (long)18446744073709551615UL))) {
        while (1U)
            {
                var_44 = rcx_1_ph << 3UL;
                rdx1_0 = rdx1_0_ph;
                r8_0 = r8_0_ph;
                while (1U)
                    {
                        var_45 = *_pre_phi105;
                        var_46 = *(uint64_t *)((r8_0 << 3UL) + var_45);
                        var_47 = *(uint64_t *)(var_44 + var_45);
                        var_48 = ((long)var_46 > (long)var_47);
                        var_49 = (uint64_t *)(((rdx1_0 + rcx_1_ph) << 3UL) + var_45);
                        rdx1_0_ph = rdx1_0;
                        r8_0_ph = r8_0;
                        if (!var_48) {
                            loop_state_var = 1U;
                            break;
                        }
                        *var_49 = var_46;
                        var_51 = rdx1_0 + (-1L);
                        rdx1_0 = var_51;
                        if (var_51 != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        r8_0 = r8_0 + (-1L);
                        continue;
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 0U:
                    {
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 1U:
                    {
                        *var_49 = var_47;
                        var_50 = rcx_1_ph + (-1L);
                        rcx_1_ph = var_50;
                        if ((long)var_50 >= (long)0UL) {
                            continue;
                        }
                        switch_state_var = 1;
                        break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
    }
    *(uint64_t *)(local_sp_0 + (-8L)) = 4238117UL;
    indirect_placeholder();
    return rdx1_2;
}
