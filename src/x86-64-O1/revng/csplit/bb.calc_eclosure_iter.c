typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_146_ret_type;
struct indirect_placeholder_146_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_15(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_30(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t indirect_placeholder_6(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_146_ret_type indirect_placeholder_146(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
typedef _Bool bool;
uint64_t bb_calc_eclosure_iter(uint64_t rcx, uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_146_ret_type var_38;
    uint64_t var_23;
    uint64_t *_pre97;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint32_t *var_13;
    uint32_t var_14;
    uint64_t local_sp_8;
    uint64_t rbx_2;
    uint64_t r12_1;
    uint64_t local_sp_1;
    uint64_t *_pre101;
    uint64_t *_pre105;
    uint64_t *_pre_phi106;
    uint64_t local_sp_7;
    uint64_t local_sp_0;
    uint64_t rbx_0;
    uint64_t var_52;
    uint64_t *var_53;
    uint64_t var_54;
    uint64_t *var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t *var_58;
    uint64_t *_pre_phi102;
    uint64_t *_pre_phi98;
    uint64_t local_sp_4;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t local_sp_6;
    uint64_t var_43;
    uint32_t _pre_phi107;
    uint64_t local_sp_2;
    uint64_t _pre_phi94;
    uint64_t *var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint32_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint32_t var_24;
    uint64_t local_sp_3;
    uint64_t _pre93;
    uint64_t var_25;
    uint64_t rax_1;
    uint64_t rbx_1;
    uint64_t r12_0;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t _pre_phi109;
    uint32_t var_42;
    uint64_t local_sp_5;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint32_t var_33;
    uint64_t _pre108;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r13();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    *(uint64_t *)(var_0 + (-136L)) = rdi;
    *(uint32_t *)(var_0 + (-124L)) = (uint32_t)rcx;
    var_7 = rdx * 24UL;
    var_8 = (uint64_t *)(rsi + 40UL);
    var_9 = *(uint64_t *)((var_7 + *var_8) + 8UL) + 1UL;
    var_10 = var_0 + (-88L);
    var_11 = var_0 + (-160L);
    *(uint64_t *)var_11 = 4243836UL;
    var_12 = indirect_placeholder_15(var_10, var_9);
    var_13 = (uint32_t *)(var_0 + (-148L));
    var_14 = (uint32_t)var_12;
    *var_13 = var_14;
    local_sp_8 = var_11;
    local_sp_7 = var_11;
    rbx_0 = rsi;
    local_sp_3 = var_11;
    rbx_1 = rsi;
    r12_0 = 0UL;
    if ((uint64_t)var_14 == 0UL) {
        return (uint64_t)*(uint32_t *)(local_sp_8 + 12UL);
    }
    *(uint64_t *)((var_7 + *(uint64_t *)(rsi + 48UL)) + 8UL) = 18446744073709551615UL;
    var_15 = (uint64_t *)rsi;
    var_16 = *var_15;
    var_17 = rdx << 4UL;
    var_18 = (var_17 + var_16) + 8UL;
    var_19 = *(uint32_t *)var_18;
    _pre_phi94 = var_18;
    if ((var_19 & 261888U) == 0U) {
        var_20 = var_7 + *var_8;
        if (*(uint64_t *)(var_20 + 8UL) == 0UL) {
            var_49 = local_sp_7 + 64UL;
            var_50 = local_sp_7 + (-8L);
            *(uint64_t *)var_50 = 4244276UL;
            var_51 = indirect_placeholder_15(var_49, rdx);
            local_sp_0 = var_50;
            local_sp_8 = var_50;
            if ((uint64_t)(unsigned char)var_51 == 0UL) {
                *(uint32_t *)(local_sp_7 + 4UL) = 12U;
                return (uint64_t)*(uint32_t *)(local_sp_8 + 12UL);
            }
            var_52 = *(uint64_t *)(rbx_0 + 48UL);
            var_53 = (uint64_t *)(local_sp_0 + 64UL);
            var_54 = *var_53;
            var_55 = (uint64_t *)(local_sp_0 + 72UL);
            var_56 = *var_55;
            var_57 = var_7 + var_52;
            *(uint64_t *)var_57 = var_54;
            *(uint64_t *)(var_57 + 8UL) = var_56;
            var_58 = (uint64_t *)(local_sp_0 + 80UL);
            *(uint64_t *)(var_57 + 16UL) = *var_58;
            _pre_phi106 = var_58;
            _pre_phi102 = var_55;
            _pre_phi98 = var_53;
            local_sp_1 = local_sp_0;
            var_59 = *(uint64_t *)(local_sp_1 + 16UL);
            var_60 = *_pre_phi102;
            *(uint64_t *)var_59 = *_pre_phi98;
            *(uint64_t *)(var_59 + 8UL) = var_60;
            *(uint64_t *)(var_59 + 16UL) = *_pre_phi106;
            local_sp_8 = local_sp_1;
        } else {
            if ((*(unsigned char *)(((**(uint64_t **)(var_20 + 16UL) << 4UL) + var_16) + 10UL) & '\x04') != '\x00') {
                var_21 = (uint64_t)(uint32_t)((uint16_t)(var_19 >> 8U) & (unsigned short)1023U);
                var_22 = var_0 + (-168L);
                *(uint64_t *)var_22 = 4244000UL;
                var_23 = indirect_placeholder_30(rdx, rdx, rsi, rdx, var_21);
                var_24 = (uint32_t)var_23;
                local_sp_3 = var_22;
                local_sp_8 = var_22;
                if ((uint64_t)var_24 != 0UL) {
                    *(uint32_t *)(var_0 + (-156L)) = var_24;
                    return (uint64_t)*(uint32_t *)(local_sp_8 + 12UL);
                }
                _pre93 = (var_17 + *var_15) + 8UL;
                _pre_phi94 = _pre93;
            }
            local_sp_4 = local_sp_3;
            local_sp_7 = local_sp_3;
            var_25 = var_7 + *var_8;
            rax_1 = var_25;
            if ((*(unsigned char *)_pre_phi94 & '\b') != '\x00' & (long)*(uint64_t *)(var_25 + 8UL) <= (long)0UL) {
                *(unsigned char *)(local_sp_3 + 27UL) = (unsigned char)'\x00';
                while (1U)
                    {
                        var_26 = *(uint64_t *)((r12_0 << 3UL) + *(uint64_t *)(rax_1 + 16UL));
                        var_27 = var_26 * 24UL;
                        var_28 = var_27 + *(uint64_t *)(rbx_1 + 48UL);
                        var_29 = *(uint64_t *)(var_28 + 8UL);
                        local_sp_5 = local_sp_4;
                        local_sp_6 = local_sp_4;
                        rbx_2 = rbx_1;
                        r12_1 = r12_0;
                        switch_state_var = 0;
                        switch (var_29) {
                          case 18446744073709551615UL:
                            {
                                *(unsigned char *)(local_sp_4 + 27UL) = (unsigned char)'\x01';
                            }
                            break;
                          case 0UL:
                            {
                                var_30 = local_sp_4 + 32UL;
                                var_31 = local_sp_4 + (-8L);
                                *(uint64_t *)var_31 = 4244033UL;
                                var_32 = indirect_placeholder_6(0UL, var_26, var_30, rbx_1);
                                var_33 = (uint32_t)var_32;
                                _pre_phi107 = var_33;
                                local_sp_2 = var_31;
                                local_sp_5 = var_31;
                                if ((uint64_t)var_33 != 0UL) {
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                _pre108 = var_31 + 32UL;
                                _pre_phi109 = _pre108;
                                var_36 = local_sp_5 + 64UL;
                                var_37 = local_sp_5 + (-8L);
                                *(uint64_t *)var_37 = 4244149UL;
                                var_38 = indirect_placeholder_146(rbx_1, var_36, var_27, r12_0, _pre_phi109);
                                var_39 = var_38.field_0;
                                var_40 = var_38.field_1;
                                var_41 = var_38.field_3;
                                var_42 = (uint32_t)var_39;
                                rbx_2 = var_40;
                                r12_1 = var_41;
                                local_sp_6 = var_37;
                                _pre_phi107 = var_42;
                                local_sp_2 = var_37;
                                if ((uint64_t)var_42 != 0UL) {
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                if (*(uint64_t *)((var_38.field_2 + *(uint64_t *)(var_40 + 48UL)) + 8UL) == 0UL) {
                                    var_43 = local_sp_5 + (-16L);
                                    *(uint64_t *)var_43 = 4244056UL;
                                    indirect_placeholder();
                                    *(unsigned char *)(local_sp_5 + 11UL) = (unsigned char)'\x01';
                                    local_sp_6 = var_43;
                                }
                            }
                            break;
                          default:
                            {
                                var_34 = *(uint64_t *)var_28;
                                var_35 = local_sp_4 + 32UL;
                                *(uint64_t *)var_35 = var_34;
                                *(uint64_t *)(local_sp_4 + 40UL) = var_29;
                                *(uint64_t *)(local_sp_4 + 48UL) = *(uint64_t *)(var_28 + 16UL);
                                _pre_phi109 = var_35;
                                var_36 = local_sp_5 + 64UL;
                                var_37 = local_sp_5 + (-8L);
                                *(uint64_t *)var_37 = 4244149UL;
                                var_38 = indirect_placeholder_146(rbx_1, var_36, var_27, r12_0, _pre_phi109);
                                var_39 = var_38.field_0;
                                var_40 = var_38.field_1;
                                var_41 = var_38.field_3;
                                var_42 = (uint32_t)var_39;
                                rbx_2 = var_40;
                                r12_1 = var_41;
                                local_sp_6 = var_37;
                                _pre_phi107 = var_42;
                                local_sp_2 = var_37;
                                if ((uint64_t)var_42 == 0UL) {
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                if (*(uint64_t *)((var_38.field_2 + *(uint64_t *)(var_40 + 48UL)) + 8UL) == 0UL) {
                                    var_43 = local_sp_5 + (-16L);
                                    *(uint64_t *)var_43 = 4244056UL;
                                    indirect_placeholder();
                                    *(unsigned char *)(local_sp_5 + 11UL) = (unsigned char)'\x01';
                                    local_sp_6 = var_43;
                                }
                            }
                            break;
                        }
                        if (switch_state_var)
                            break;
                        var_44 = r12_1 + 1UL;
                        var_45 = var_7 + *(uint64_t *)(rbx_2 + 40UL);
                        rbx_0 = rbx_2;
                        local_sp_4 = local_sp_6;
                        rax_1 = var_45;
                        rbx_1 = rbx_2;
                        r12_0 = var_44;
                        if ((long)*(uint64_t *)(var_45 + 8UL) > (long)var_44) {
                            continue;
                        }
                        var_46 = local_sp_6 + 64UL;
                        var_47 = local_sp_6 + (-8L);
                        *(uint64_t *)var_47 = 4244180UL;
                        var_48 = indirect_placeholder_15(var_46, rdx);
                        local_sp_0 = var_47;
                        local_sp_1 = var_47;
                        local_sp_8 = var_47;
                        if ((uint64_t)(unsigned char)var_48 == 0UL) {
                            *(uint32_t *)(local_sp_6 + 4UL) = 12U;
                            loop_state_var = 3U;
                            break;
                        }
                        if (*(unsigned char *)(local_sp_6 + 20UL) != '\x01') {
                            loop_state_var = 2U;
                            break;
                        }
                        if (*(unsigned char *)(local_sp_6 + 19UL) != '\x00') {
                            loop_state_var = 2U;
                            break;
                        }
                        *(uint64_t *)((var_7 + *(uint64_t *)(rbx_2 + 48UL)) + 8UL) = 0UL;
                        _pre97 = (uint64_t *)(var_47 + 64UL);
                        _pre101 = (uint64_t *)(var_47 + 72UL);
                        _pre105 = (uint64_t *)(var_47 + 80UL);
                        _pre_phi106 = _pre105;
                        _pre_phi102 = _pre101;
                        _pre_phi98 = _pre97;
                        loop_state_var = 1U;
                        break;
                    }
                switch (loop_state_var) {
                  case 3U:
                    {
                        return (uint64_t)*(uint32_t *)(local_sp_8 + 12UL);
                    }
                    break;
                  case 0U:
                    {
                        *(uint32_t *)(local_sp_2 + 12UL) = _pre_phi107;
                        local_sp_8 = local_sp_2;
                    }
                    break;
                  case 2U:
                  case 1U:
                    {
                        switch (loop_state_var) {
                          case 1U:
                            {
                                var_59 = *(uint64_t *)(local_sp_1 + 16UL);
                                var_60 = *_pre_phi102;
                                *(uint64_t *)var_59 = *_pre_phi98;
                                *(uint64_t *)(var_59 + 8UL) = var_60;
                                *(uint64_t *)(var_59 + 16UL) = *_pre_phi106;
                                local_sp_8 = local_sp_1;
                            }
                            break;
                          case 2U:
                            {
                                var_52 = *(uint64_t *)(rbx_0 + 48UL);
                                var_53 = (uint64_t *)(local_sp_0 + 64UL);
                                var_54 = *var_53;
                                var_55 = (uint64_t *)(local_sp_0 + 72UL);
                                var_56 = *var_55;
                                var_57 = var_7 + var_52;
                                *(uint64_t *)var_57 = var_54;
                                *(uint64_t *)(var_57 + 8UL) = var_56;
                                var_58 = (uint64_t *)(local_sp_0 + 80UL);
                                *(uint64_t *)(var_57 + 16UL) = *var_58;
                                _pre_phi106 = var_58;
                                _pre_phi102 = var_55;
                                _pre_phi98 = var_53;
                                local_sp_1 = local_sp_0;
                            }
                            break;
                        }
                    }
                    break;
                }
            }
        }
    } else {
        local_sp_4 = local_sp_3;
        local_sp_7 = local_sp_3;
        if ((*(unsigned char *)_pre_phi94 & '\b') == '\x00') {
            var_49 = local_sp_7 + 64UL;
            var_50 = local_sp_7 + (-8L);
            *(uint64_t *)var_50 = 4244276UL;
            var_51 = indirect_placeholder_15(var_49, rdx);
            local_sp_0 = var_50;
            local_sp_8 = var_50;
            if ((uint64_t)(unsigned char)var_51 != 0UL) {
                *(uint32_t *)(local_sp_7 + 4UL) = 12U;
                return (uint64_t)*(uint32_t *)(local_sp_8 + 12UL);
            }
            var_52 = *(uint64_t *)(rbx_0 + 48UL);
            var_53 = (uint64_t *)(local_sp_0 + 64UL);
            var_54 = *var_53;
            var_55 = (uint64_t *)(local_sp_0 + 72UL);
            var_56 = *var_55;
            var_57 = var_7 + var_52;
            *(uint64_t *)var_57 = var_54;
            *(uint64_t *)(var_57 + 8UL) = var_56;
            var_58 = (uint64_t *)(local_sp_0 + 80UL);
            *(uint64_t *)(var_57 + 16UL) = *var_58;
            _pre_phi106 = var_58;
            _pre_phi102 = var_55;
            _pre_phi98 = var_53;
            local_sp_1 = local_sp_0;
            var_59 = *(uint64_t *)(local_sp_1 + 16UL);
            var_60 = *_pre_phi102;
            *(uint64_t *)var_59 = *_pre_phi98;
            *(uint64_t *)(var_59 + 8UL) = var_60;
            *(uint64_t *)(var_59 + 16UL) = *_pre_phi106;
            local_sp_8 = local_sp_1;
            return (uint64_t)*(uint32_t *)(local_sp_8 + 12UL);
        }
        var_25 = var_7 + *var_8;
        rax_1 = var_25;
        if ((long)*(uint64_t *)(var_25 + 8UL) > (long)0UL) {
            *(unsigned char *)(local_sp_3 + 27UL) = (unsigned char)'\x00';
            while (1U)
                {
                    var_26 = *(uint64_t *)((r12_0 << 3UL) + *(uint64_t *)(rax_1 + 16UL));
                    var_27 = var_26 * 24UL;
                    var_28 = var_27 + *(uint64_t *)(rbx_1 + 48UL);
                    var_29 = *(uint64_t *)(var_28 + 8UL);
                    local_sp_5 = local_sp_4;
                    local_sp_6 = local_sp_4;
                    rbx_2 = rbx_1;
                    r12_1 = r12_0;
                    switch_state_var = 0;
                    switch (var_29) {
                      case 18446744073709551615UL:
                        {
                            *(unsigned char *)(local_sp_4 + 27UL) = (unsigned char)'\x01';
                        }
                        break;
                      case 0UL:
                        {
                            var_30 = local_sp_4 + 32UL;
                            var_31 = local_sp_4 + (-8L);
                            *(uint64_t *)var_31 = 4244033UL;
                            var_32 = indirect_placeholder_6(0UL, var_26, var_30, rbx_1);
                            var_33 = (uint32_t)var_32;
                            _pre_phi107 = var_33;
                            local_sp_2 = var_31;
                            local_sp_5 = var_31;
                            if ((uint64_t)var_33 != 0UL) {
                                loop_state_var = 0U;
                                switch_state_var = 1;
                                break;
                            }
                            _pre108 = var_31 + 32UL;
                            _pre_phi109 = _pre108;
                            var_36 = local_sp_5 + 64UL;
                            var_37 = local_sp_5 + (-8L);
                            *(uint64_t *)var_37 = 4244149UL;
                            var_38 = indirect_placeholder_146(rbx_1, var_36, var_27, r12_0, _pre_phi109);
                            var_39 = var_38.field_0;
                            var_40 = var_38.field_1;
                            var_41 = var_38.field_3;
                            var_42 = (uint32_t)var_39;
                            rbx_2 = var_40;
                            r12_1 = var_41;
                            local_sp_6 = var_37;
                            _pre_phi107 = var_42;
                            local_sp_2 = var_37;
                            if ((uint64_t)var_42 != 0UL) {
                                loop_state_var = 0U;
                                switch_state_var = 1;
                                break;
                            }
                            if (*(uint64_t *)((var_38.field_2 + *(uint64_t *)(var_40 + 48UL)) + 8UL) == 0UL) {
                                var_43 = local_sp_5 + (-16L);
                                *(uint64_t *)var_43 = 4244056UL;
                                indirect_placeholder();
                                *(unsigned char *)(local_sp_5 + 11UL) = (unsigned char)'\x01';
                                local_sp_6 = var_43;
                            }
                        }
                        break;
                      default:
                        {
                            var_34 = *(uint64_t *)var_28;
                            var_35 = local_sp_4 + 32UL;
                            *(uint64_t *)var_35 = var_34;
                            *(uint64_t *)(local_sp_4 + 40UL) = var_29;
                            *(uint64_t *)(local_sp_4 + 48UL) = *(uint64_t *)(var_28 + 16UL);
                            _pre_phi109 = var_35;
                            var_36 = local_sp_5 + 64UL;
                            var_37 = local_sp_5 + (-8L);
                            *(uint64_t *)var_37 = 4244149UL;
                            var_38 = indirect_placeholder_146(rbx_1, var_36, var_27, r12_0, _pre_phi109);
                            var_39 = var_38.field_0;
                            var_40 = var_38.field_1;
                            var_41 = var_38.field_3;
                            var_42 = (uint32_t)var_39;
                            rbx_2 = var_40;
                            r12_1 = var_41;
                            local_sp_6 = var_37;
                            _pre_phi107 = var_42;
                            local_sp_2 = var_37;
                            if ((uint64_t)var_42 == 0UL) {
                                loop_state_var = 0U;
                                switch_state_var = 1;
                                break;
                            }
                            if (*(uint64_t *)((var_38.field_2 + *(uint64_t *)(var_40 + 48UL)) + 8UL) == 0UL) {
                                var_43 = local_sp_5 + (-16L);
                                *(uint64_t *)var_43 = 4244056UL;
                                indirect_placeholder();
                                *(unsigned char *)(local_sp_5 + 11UL) = (unsigned char)'\x01';
                                local_sp_6 = var_43;
                            }
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                    var_44 = r12_1 + 1UL;
                    var_45 = var_7 + *(uint64_t *)(rbx_2 + 40UL);
                    rbx_0 = rbx_2;
                    local_sp_4 = local_sp_6;
                    rax_1 = var_45;
                    rbx_1 = rbx_2;
                    r12_0 = var_44;
                    if ((long)*(uint64_t *)(var_45 + 8UL) > (long)var_44) {
                        continue;
                    }
                    var_46 = local_sp_6 + 64UL;
                    var_47 = local_sp_6 + (-8L);
                    *(uint64_t *)var_47 = 4244180UL;
                    var_48 = indirect_placeholder_15(var_46, rdx);
                    local_sp_0 = var_47;
                    local_sp_1 = var_47;
                    local_sp_8 = var_47;
                    if ((uint64_t)(unsigned char)var_48 == 0UL) {
                        *(uint32_t *)(local_sp_6 + 4UL) = 12U;
                        loop_state_var = 3U;
                        break;
                    }
                    if (*(unsigned char *)(local_sp_6 + 20UL) != '\x01') {
                        loop_state_var = 2U;
                        break;
                    }
                    if (*(unsigned char *)(local_sp_6 + 19UL) != '\x00') {
                        loop_state_var = 2U;
                        break;
                    }
                    *(uint64_t *)((var_7 + *(uint64_t *)(rbx_2 + 48UL)) + 8UL) = 0UL;
                    _pre97 = (uint64_t *)(var_47 + 64UL);
                    _pre101 = (uint64_t *)(var_47 + 72UL);
                    _pre105 = (uint64_t *)(var_47 + 80UL);
                    _pre_phi106 = _pre105;
                    _pre_phi102 = _pre101;
                    _pre_phi98 = _pre97;
                    loop_state_var = 1U;
                    break;
                }
            switch (loop_state_var) {
              case 0U:
                {
                    *(uint32_t *)(local_sp_2 + 12UL) = _pre_phi107;
                    local_sp_8 = local_sp_2;
                }
                break;
              case 3U:
                {
                    return (uint64_t)*(uint32_t *)(local_sp_8 + 12UL);
                }
                break;
              case 2U:
              case 1U:
                {
                    switch (loop_state_var) {
                      case 2U:
                        {
                            var_52 = *(uint64_t *)(rbx_0 + 48UL);
                            var_53 = (uint64_t *)(local_sp_0 + 64UL);
                            var_54 = *var_53;
                            var_55 = (uint64_t *)(local_sp_0 + 72UL);
                            var_56 = *var_55;
                            var_57 = var_7 + var_52;
                            *(uint64_t *)var_57 = var_54;
                            *(uint64_t *)(var_57 + 8UL) = var_56;
                            var_58 = (uint64_t *)(local_sp_0 + 80UL);
                            *(uint64_t *)(var_57 + 16UL) = *var_58;
                            _pre_phi106 = var_58;
                            _pre_phi102 = var_55;
                            _pre_phi98 = var_53;
                            local_sp_1 = local_sp_0;
                        }
                        break;
                      case 1U:
                        {
                            var_59 = *(uint64_t *)(local_sp_1 + 16UL);
                            var_60 = *_pre_phi102;
                            *(uint64_t *)var_59 = *_pre_phi98;
                            *(uint64_t *)(var_59 + 8UL) = var_60;
                            *(uint64_t *)(var_59 + 16UL) = *_pre_phi106;
                            local_sp_8 = local_sp_1;
                        }
                        break;
                    }
                }
                break;
            }
        }
    }
}
