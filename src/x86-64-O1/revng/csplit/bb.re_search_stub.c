typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern uint64_t indirect_placeholder_6(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
uint64_t bb_re_search_stub(uint64_t rcx, uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t r9, uint64_t r8) {
    uint64_t var_14;
    unsigned char var_15;
    bool var_16;
    uint64_t var_17;
    uint64_t *var_18;
    uint64_t r15_0;
    uint64_t *_pre_phi88;
    uint64_t *_pre87;
    uint64_t var_19;
    uint64_t *var_20;
    uint64_t var_21;
    bool var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t rbx_1;
    uint64_t var_34;
    uint64_t local_sp_2;
    uint64_t r14_1;
    uint64_t var_35;
    uint64_t local_sp_0;
    uint64_t local_sp_3;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    bool var_39;
    uint64_t var_40;
    uint64_t local_sp_1;
    uint64_t var_42;
    uint64_t rbx_0;
    uint64_t var_41;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t *var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint32_t var_33;
    uint64_t r14_0;
    bool var_9;
    bool var_10;
    unsigned char *_pre_phi85;
    unsigned char *var_11;
    unsigned char *var_12;
    unsigned char var_13;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r13();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    var_7 = var_0 + (-88L);
    *(uint64_t *)(var_0 + (-80L)) = rsi;
    *(uint64_t *)(var_0 + (-72L)) = r9;
    *(uint32_t *)(var_0 + (-64L)) = *(uint32_t *)(var_0 | 16UL);
    var_8 = r8 + rcx;
    r15_0 = 1UL;
    rbx_1 = 18446744073709551615UL;
    r14_1 = 0UL;
    local_sp_3 = var_7;
    rbx_0 = 18446744073709551614UL;
    r14_0 = rdx;
    if (((long)rcx < (long)0UL) || ((long)rcx > (long)rdx)) {
        return rbx_1;
    }
    rbx_1 = 18446744073709551614UL;
    if ((long)var_8 > (long)rdx) {
        var_9 = ((long)r8 >= (long)0UL);
        var_10 = ((long)var_8 < (long)rcx);
        if (var_9 && var_10) {
            r14_0 = var_8;
            if ((long)var_8 < (long)0UL) {
                var_11 = (unsigned char *)(rdi + 56UL);
                *(uint32_t *)(var_0 + (-60L)) = (uint32_t)((*var_11 >> '\x05') & '\x03');
                _pre_phi85 = var_11;
            } else {
                if (((long)r8 > (long)18446744073709551615UL) || var_10) {
                    var_11 = (unsigned char *)(rdi + 56UL);
                    *(uint32_t *)(var_0 + (-60L)) = (uint32_t)((*var_11 >> '\x05') & '\x03');
                    _pre_phi85 = var_11;
                } else {
                    var_12 = (unsigned char *)(rdi + 56UL);
                    var_13 = *var_12;
                    *(uint32_t *)(var_0 + (-60L)) = (uint32_t)((var_13 >> '\x05') & '\x03');
                    _pre_phi85 = var_12;
                    r14_1 = r14_0;
                    if ((long)r14_0 <= (long)rcx & (*(uint64_t *)(rdi + 32UL) != 0UL) && ((var_13 & '\b') == '\x00')) {
                        var_14 = var_0 + (-96L);
                        *(uint64_t *)var_14 = 4278264UL;
                        indirect_placeholder_1(rdi);
                        local_sp_3 = var_14;
                    }
                }
            }
        } else {
            var_12 = (unsigned char *)(rdi + 56UL);
            var_13 = *var_12;
            *(uint32_t *)(var_0 + (-60L)) = (uint32_t)((var_13 >> '\x05') & '\x03');
            _pre_phi85 = var_12;
            r14_1 = r14_0;
            if ((long)r14_0 <= (long)rcx & (*(uint64_t *)(rdi + 32UL) != 0UL) && ((var_13 & '\b') == '\x00')) {
                var_14 = var_0 + (-96L);
                *(uint64_t *)var_14 = 4278264UL;
                indirect_placeholder_1(rdi);
                local_sp_3 = var_14;
            }
        }
    } else {
        var_12 = (unsigned char *)(rdi + 56UL);
        var_13 = *var_12;
        *(uint32_t *)(var_0 + (-60L)) = (uint32_t)((var_13 >> '\x05') & '\x03');
        _pre_phi85 = var_12;
        r14_1 = r14_0;
        if ((long)r14_0 <= (long)rcx & (*(uint64_t *)(rdi + 32UL) != 0UL) && ((var_13 & '\b') == '\x00')) {
            var_14 = var_0 + (-96L);
            *(uint64_t *)var_14 = 4278264UL;
            indirect_placeholder_1(rdi);
            local_sp_3 = var_14;
        }
    }
    var_15 = *_pre_phi85;
    var_16 = ((var_15 & '\x10') == '\x00');
    var_17 = local_sp_3 + 96UL;
    var_18 = (uint64_t *)var_17;
    if (var_16) {
        *var_18 = 0UL;
    } else {
        if (*var_18 != 0UL) {
            if ((var_15 & '\x06') == '\x04') {
                var_19 = **(uint64_t **)var_17;
                var_20 = (uint64_t *)(rdi + 48UL);
                _pre_phi88 = var_20;
                if (var_19 > *var_20) {
                    r15_0 = *_pre_phi88 + 1UL;
                } else {
                    var_21 = helper_cc_compute_all_wrapper(var_19, 0UL, 0UL, 25U);
                    var_22 = ((uint64_t)(((unsigned char)(var_21 >> 4UL) ^ (unsigned char)var_21) & '\xc0') == 0UL);
                    var_23 = var_22 ? var_19 : 1UL;
                    *var_18 = (var_22 ? *var_18 : 0UL);
                    r15_0 = var_23;
                }
            } else {
                _pre87 = (uint64_t *)(rdi + 48UL);
                _pre_phi88 = _pre87;
                r15_0 = *_pre_phi88 + 1UL;
            }
        }
    }
    var_24 = r15_0 << 4UL;
    *(uint64_t *)(local_sp_3 + (-8L)) = 4278137UL;
    var_25 = indirect_placeholder_1(var_24);
    if (var_25 == 0UL) {
        return;
    }
    var_26 = (uint64_t)*(uint32_t *)(local_sp_3 + 20UL);
    var_27 = local_sp_3 + (-24L);
    var_28 = (uint64_t *)var_27;
    *var_28 = var_26;
    *(uint64_t *)(local_sp_3 + (-32L)) = var_25;
    *(uint64_t *)(local_sp_3 + (-40L)) = r15_0;
    var_29 = *(uint64_t *)(local_sp_3 + 8UL);
    var_30 = *(uint64_t *)local_sp_3;
    *(uint64_t *)(local_sp_3 + (-48L)) = 4278189UL;
    var_31 = indirect_placeholder_7(rcx, rdx, rdi, var_30, var_29, r14_1);
    var_32 = local_sp_3 + (-16L);
    var_33 = (uint32_t)var_31;
    local_sp_0 = var_32;
    local_sp_2 = var_32;
    if ((uint64_t)var_33 == 0UL) {
        var_35 = *(uint64_t *)(local_sp_3 + 80UL);
        local_sp_2 = var_27;
        if (var_35 != 0UL) {
            var_36 = (uint64_t)((*_pre_phi85 >> '\x01') & '\x03');
            *var_28 = 4278368UL;
            var_37 = indirect_placeholder_6(var_36, r15_0, var_35, var_25);
            var_38 = (var_37 << 1UL) & 6UL;
            *_pre_phi85 = ((*_pre_phi85 & '\xf9') | (unsigned char)var_38);
            local_sp_0 = var_27;
            if (var_38 != 0UL) {
                *(uint64_t *)(local_sp_2 + (-8L)) = 4278222UL;
                indirect_placeholder();
                rbx_1 = rbx_0;
                return rbx_1;
            }
        }
        var_39 = (*(unsigned char *)(local_sp_0 + 24UL) == '\x00');
        var_40 = *(uint64_t *)var_25;
        local_sp_1 = local_sp_0;
        local_sp_2 = local_sp_0;
        rbx_0 = var_40;
        if (!var_39) {
            if (var_40 != rcx) {
                var_41 = local_sp_0 + (-8L);
                *(uint64_t *)var_41 = 4278453UL;
                indirect_placeholder();
                local_sp_1 = var_41;
            }
            var_42 = *(uint64_t *)(var_25 + 8UL) - rcx;
            local_sp_2 = local_sp_1;
            rbx_0 = var_42;
        }
    } else {
        var_34 = ((uint64_t)(var_33 + (-1)) == 0UL) ? 18446744073709551615UL : 18446744073709551614UL;
        rbx_0 = var_34;
        *(uint64_t *)(local_sp_2 + (-8L)) = 4278222UL;
        indirect_placeholder();
        rbx_1 = rbx_0;
    }
}
