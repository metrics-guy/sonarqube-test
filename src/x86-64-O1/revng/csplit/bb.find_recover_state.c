typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_find_recover_state_ret_type;
struct indirect_placeholder_192_ret_type;
struct bb_find_recover_state_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_192_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern struct indirect_placeholder_192_ret_type indirect_placeholder_192(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
struct bb_find_recover_state_ret_type bb_find_recover_state(uint64_t rdi, uint64_t rsi, uint64_t r10, uint64_t r9, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    uint32_t *var_7;
    uint64_t local_sp_0;
    uint64_t r107_0;
    uint64_t r98_0;
    uint64_t r89_0;
    uint64_t var_8;
    uint64_t var_9;
    struct bb_find_recover_state_ret_type mrv3;
    struct bb_find_recover_state_ret_type mrv4;
    uint64_t var_10;
    uint64_t rax_0;
    uint64_t var_11;
    uint64_t rax_1;
    uint64_t var_12;
    uint64_t var_13;
    struct indirect_placeholder_192_ret_type var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    bool var_18;
    uint64_t var_19;
    uint64_t r107_1;
    uint64_t r98_1;
    uint64_t r89_1;
    struct bb_find_recover_state_ret_type mrv;
    struct bb_find_recover_state_ret_type mrv1;
    struct bb_find_recover_state_ret_type mrv2;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    var_3 = var_0 + (-24L);
    var_4 = (uint64_t *)(rsi + 192UL);
    var_5 = (uint64_t *)(rsi + 72UL);
    var_6 = (uint64_t *)(rsi + 184UL);
    var_7 = (uint32_t *)rdi;
    local_sp_0 = var_3;
    r107_0 = r10;
    r98_0 = r9;
    r89_0 = r8;
    rax_1 = 0UL;
    while (1U)
        {
            var_8 = *var_4;
            var_9 = *var_5;
            var_10 = var_9;
            rax_0 = var_9;
            r107_1 = r107_0;
            r98_1 = r98_0;
            r89_1 = r89_0;
            while (1U)
                {
                    var_11 = rax_0 + 1UL;
                    rax_0 = var_11;
                    if ((long)var_8 >= (long)var_11) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_12 = var_10 + 1UL;
                    *var_5 = var_12;
                    var_10 = var_12;
                    if (*(uint64_t *)((var_11 << 3UL) + *var_6) == 0UL) {
                        continue;
                    }
                    loop_state_var = 0U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 0U:
                {
                    var_13 = local_sp_0 + (-8L);
                    *(uint64_t *)var_13 = 4268126UL;
                    var_14 = indirect_placeholder_192(var_8, 0UL, rdi, rsi, r107_0, r98_0, r89_0);
                    var_15 = var_14.field_1;
                    var_16 = var_14.field_2;
                    var_17 = var_14.field_3;
                    var_18 = (*var_7 == 0U);
                    var_19 = var_14.field_0;
                    local_sp_0 = var_13;
                    r107_0 = var_15;
                    r98_0 = var_16;
                    r89_0 = var_17;
                    rax_1 = var_19;
                    r107_1 = var_15;
                    r98_1 = var_16;
                    r89_1 = var_17;
                    if (var_18 && (var_19 == 0UL)) {
                        continue;
                    }
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    mrv.field_0 = rax_1;
    mrv1 = mrv;
    mrv1.field_1 = var_8;
    mrv2 = mrv1;
    mrv2.field_2 = r107_1;
    mrv3 = mrv2;
    mrv3.field_3 = r98_1;
    mrv4 = mrv3;
    mrv4.field_4 = r89_1;
    return mrv4;
}
