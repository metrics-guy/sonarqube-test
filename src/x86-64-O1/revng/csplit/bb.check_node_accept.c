typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_139_ret_type;
struct indirect_placeholder_139_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_15(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct indirect_placeholder_139_ret_type indirect_placeholder_139(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
uint64_t bb_check_node_accept(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    unsigned char var_6;
    uint64_t var_7;
    uint64_t var_8;
    unsigned char var_9;
    uint64_t var_10;
    uint64_t rax_0;
    uint32_t *var_15;
    uint64_t var_21;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t local_sp_0;
    uint64_t var_16;
    struct indirect_placeholder_139_ret_type var_17;
    uint64_t var_18;
    uint32_t var_19;
    uint64_t var_20;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r12();
    var_4 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_5 = var_0 + (-24L);
    *(uint64_t *)var_5 = var_1;
    var_6 = *(unsigned char *)(*(uint64_t *)(rdi + 8UL) + rdx);
    var_7 = (uint64_t)var_6;
    var_8 = rsi + 8UL;
    var_9 = *(unsigned char *)var_8;
    var_10 = (uint64_t)var_9 + (-5L);
    local_sp_0 = var_5;
    rax_0 = 0UL;
    if ((uint64_t)(unsigned char)var_10 == 0UL) {
        var_11 = helper_cc_compute_all_wrapper(var_10, 5UL, var_4, 14U);
        if ((var_11 & 65UL) == 0UL) {
            if (((uint64_t)(var_9 + '\xf9') != 0UL) || ((signed char)var_6 < '\x00')) {
                return rax_0;
            }
            if ((uint64_t)(var_6 + '\xf6') != 0UL) {
                if (var_6 != '\x00' & (signed char)*(unsigned char *)(*(uint64_t *)(rdi + 152UL) + 216UL) > '\xff') {
                    return rax_0;
                }
            }
            if ((*(unsigned char *)(*(uint64_t *)(rdi + 152UL) + 216UL) & '@') == '\x00') {
                return rax_0;
            }
        }
        if ((uint64_t)(var_9 + '\xff') == 0UL) {
            if ((uint64_t)(*(unsigned char *)rsi - var_6) == 0UL) {
                return rax_0;
            }
        }
        if ((uint64_t)(var_9 + '\xfd') == 0UL) {
            return rax_0;
        }
        var_12 = *(uint64_t *)rsi;
        var_13 = var_0 + (-32L);
        *(uint64_t *)var_13 = 4238828UL;
        var_14 = indirect_placeholder_15(var_12, var_7);
        local_sp_0 = var_13;
        rax_0 = var_14;
        if ((uint64_t)(unsigned char)var_14 == 0UL) {
            return rax_0;
        }
        var_15 = (uint32_t *)var_8;
        rax_0 = 1UL;
        if ((*var_15 & 261888U) == 0U) {
            return rax_0;
        }
        var_16 = (uint64_t)*(uint32_t *)(rdi + 160UL);
        *(uint64_t *)(local_sp_0 + (-8L)) = 4238867UL;
        var_17 = indirect_placeholder_139(1UL, var_16, rdi, rdx);
        var_18 = var_17.field_0;
        var_19 = *var_15 >> 8U;
        var_20 = (uint64_t)var_19;
        rax_0 = 0UL;
        if ((var_20 & 4UL) == 0UL) {
            if (((var_18 & 1UL) != 0UL) && ((var_20 & 8UL) == 0UL)) {
                return rax_0;
            }
        }
        if (((var_20 & 8UL) == 0UL) || ((var_18 & 1UL) == 0UL)) {
            return rax_0;
        }
        if (((var_20 & 32UL) != 0UL) && ((var_18 & 2UL) == 0UL)) {
            var_21 = ((signed char)(unsigned char)var_19 > '\xff') ? 1UL : ((var_18 >> 3UL) & 1UL);
            rax_0 = var_21;
        }
    } else {
        if ((uint64_t)(var_6 + '\xf6') == 0UL) {
            if (var_6 != '\x00') {
                if ((signed char)*(unsigned char *)(*(uint64_t *)(rdi + 152UL) + 216UL) > '\xff') {
                    return rax_0;
                }
            }
            var_15 = (uint32_t *)var_8;
            rax_0 = 1UL;
            if ((*var_15 & 261888U) == 0U) {
                return rax_0;
            }
            var_16 = (uint64_t)*(uint32_t *)(rdi + 160UL);
            *(uint64_t *)(local_sp_0 + (-8L)) = 4238867UL;
            var_17 = indirect_placeholder_139(1UL, var_16, rdi, rdx);
            var_18 = var_17.field_0;
            var_19 = *var_15 >> 8U;
            var_20 = (uint64_t)var_19;
            rax_0 = 0UL;
            if ((var_20 & 4UL) == 0UL) {
                if (((var_20 & 8UL) == 0UL) || ((var_18 & 1UL) == 0UL)) {
                    return rax_0;
                }
            }
            if (((var_18 & 1UL) != 0UL) && ((var_20 & 8UL) == 0UL)) {
                return rax_0;
            }
            if (((var_20 & 32UL) != 0UL) && ((var_18 & 2UL) == 0UL)) {
                var_21 = ((signed char)(unsigned char)var_19 > '\xff') ? 1UL : ((var_18 >> 3UL) & 1UL);
                rax_0 = var_21;
            }
        } else {
            if ((*(unsigned char *)(*(uint64_t *)(rdi + 152UL) + 216UL) & '@') == '\x00') {
                return rax_0;
            }
            var_15 = (uint32_t *)var_8;
            rax_0 = 1UL;
            if ((*var_15 & 261888U) == 0U) {
                return rax_0;
            }
            var_16 = (uint64_t)*(uint32_t *)(rdi + 160UL);
            *(uint64_t *)(local_sp_0 + (-8L)) = 4238867UL;
            var_17 = indirect_placeholder_139(1UL, var_16, rdi, rdx);
            var_18 = var_17.field_0;
            var_19 = *var_15 >> 8U;
            var_20 = (uint64_t)var_19;
            rax_0 = 0UL;
            if ((var_20 & 4UL) == 0UL) {
                if (((var_18 & 1UL) != 0UL) && ((var_20 & 8UL) == 0UL)) {
                    return rax_0;
                }
            }
            if (((var_20 & 8UL) == 0UL) || ((var_18 & 1UL) == 0UL)) {
                return rax_0;
            }
            if (((var_20 & 32UL) != 0UL) && ((var_18 & 2UL) == 0UL)) {
                var_21 = ((signed char)(unsigned char)var_19 > '\xff') ? 1UL : ((var_18 >> 3UL) & 1UL);
                rax_0 = var_21;
            }
        }
    }
}
