typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_147_ret_type;
struct indirect_placeholder_147_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_15(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_16(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_4(uint64_t param_0);
extern void indirect_placeholder_2(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_147_ret_type indirect_placeholder_147(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
uint64_t bb_create_cd_newstate(uint64_t rcx, uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t r10, uint64_t r9, uint64_t r8) {
    uint64_t local_sp_1;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    struct indirect_placeholder_147_ret_type var_7;
    uint64_t var_8;
    uint64_t rbp_0;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_34;
    uint32_t var_12;
    unsigned char *var_13;
    uint64_t *var_14;
    uint64_t *var_15;
    uint64_t local_sp_3;
    uint64_t local_sp_2;
    uint64_t rbx_0;
    uint64_t r87_2;
    uint64_t local_sp_0;
    uint64_t r87_0;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint32_t var_21;
    bool var_22;
    bool var_23;
    unsigned char var_24;
    uint64_t r87_1;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t *var_25;
    uint64_t var_26;
    uint64_t *var_27;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r13();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    *(uint32_t *)(var_0 + (-72L)) = (uint32_t)rdx;
    *(uint64_t *)(var_0 + (-64L)) = rcx;
    *(uint64_t *)(var_0 + (-96L)) = 4245105UL;
    var_7 = indirect_placeholder_147(var_1, rcx, 112UL, var_4, 1UL, r10, r9, r8);
    var_8 = var_7.field_0;
    rbx_0 = 0UL;
    r87_0 = 0UL;
    rbp_0 = 0UL;
    if (var_8 == 0UL) {
        return rbp_0;
    }
    var_9 = var_8 + 8UL;
    var_10 = var_0 + (-104L);
    *(uint64_t *)var_10 = 4245132UL;
    var_11 = indirect_placeholder_15(var_9, rsi);
    local_sp_0 = var_10;
    local_sp_3 = var_10;
    rbp_0 = var_8;
    if ((uint64_t)(uint32_t)var_11 == 0UL) {
        var_12 = *(uint32_t *)(var_0 + (-88L));
        var_13 = (unsigned char *)(var_8 + 104UL);
        *var_13 = ((*var_13 & '\xf0') | ((unsigned char)var_12 & '\x0f'));
        var_14 = (uint64_t *)(var_8 + 80UL);
        *var_14 = var_9;
        var_15 = (uint64_t *)(rsi + 8UL);
        if ((long)*var_15 <= (long)0UL) {
            *(uint32_t *)(var_0 + (-84L)) = (var_12 & 4U);
            while (1U)
                {
                    var_16 = (*(uint64_t *)((rbx_0 << 3UL) + *(uint64_t *)(rsi + 16UL)) << 4UL) + *(uint64_t *)rdi;
                    var_17 = var_16 + 8UL;
                    var_18 = (uint64_t)*(unsigned char *)var_17;
                    var_19 = (uint64_t)(*(uint32_t *)var_17 >> 8U);
                    var_20 = (uint64_t)((uint16_t)var_19 & (unsigned short)1023U);
                    var_21 = (uint32_t)var_18;
                    var_22 = ((uint64_t)(var_21 + (-1)) == 0UL);
                    var_23 = (var_20 == 0UL);
                    local_sp_1 = local_sp_0;
                    r87_1 = r87_0;
                    local_sp_2 = local_sp_0;
                    r87_2 = r87_0;
                    if (var_22 && var_23) {
                        var_35 = rbx_0 + 1UL;
                        rbx_0 = var_35;
                        local_sp_0 = local_sp_2;
                        r87_0 = r87_2;
                        local_sp_3 = local_sp_2;
                        if ((long)*var_15 > (long)var_35) {
                            continue;
                        }
                        loop_state_var = 1U;
                        break;
                    }
                    var_24 = ((*(unsigned char *)(var_16 + 10UL) << '\x01') & ' ') | *var_13;
                    *var_13 = var_24;
                    if ((uint64_t)(var_21 + (-2)) == 0UL) {
                        *var_13 = (var_24 | '\x10');
                    } else {
                        if ((uint64_t)(var_21 + (-4)) == 0UL) {
                            *var_13 = (var_24 | '@');
                        }
                    }
                    if (!var_23) {
                        if (var_9 != *var_14) {
                            var_25 = (uint64_t *)(local_sp_0 + (-8L));
                            *var_25 = 4245242UL;
                            var_26 = indirect_placeholder_1(24UL);
                            var_27 = (uint64_t *)local_sp_0;
                            *var_27 = var_26;
                            *var_14 = var_26;
                            r87_1 = 0UL;
                            if (var_26 != 0UL) {
                                *(uint64_t *)(local_sp_0 + (-16L)) = 4245300UL;
                                indirect_placeholder_4(var_8);
                                var_34 = *var_25;
                                rbp_0 = var_34;
                                loop_state_var = 0U;
                                break;
                            }
                            var_28 = *var_27;
                            var_29 = local_sp_0 + (-16L);
                            *(uint64_t *)var_29 = 4245269UL;
                            var_30 = indirect_placeholder_15(var_28, rsi);
                            local_sp_1 = var_29;
                            if ((uint64_t)(uint32_t)var_30 != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            *var_13 = (*var_13 | '\x80');
                        }
                        local_sp_2 = local_sp_1;
                        r87_2 = r87_1;
                        if ((var_19 & 1UL) == 0UL) {
                            if ((var_19 & 2UL) != 0UL & (*(unsigned char *)(local_sp_1 + 16UL) & '\x01') != '\x00') {
                                var_31 = rbx_0 - r87_1;
                                var_32 = local_sp_1 + (-8L);
                                *(uint64_t *)var_32 = 4245366UL;
                                indirect_placeholder_2(var_9, var_31);
                                var_33 = r87_1 + 1UL;
                                local_sp_2 = var_32;
                                r87_2 = var_33;
                                var_35 = rbx_0 + 1UL;
                                rbx_0 = var_35;
                                local_sp_0 = local_sp_2;
                                r87_0 = r87_2;
                                local_sp_3 = local_sp_2;
                                if ((long)*var_15 > (long)var_35) {
                                    continue;
                                }
                                loop_state_var = 1U;
                                break;
                            }
                        }
                        if (!(((*(unsigned char *)(local_sp_1 + 16UL) & '\x01') != '\x00') && ((var_19 & 2UL) == 0UL))) {
                            var_31 = rbx_0 - r87_1;
                            var_32 = local_sp_1 + (-8L);
                            *(uint64_t *)var_32 = 4245366UL;
                            indirect_placeholder_2(var_9, var_31);
                            var_33 = r87_1 + 1UL;
                            local_sp_2 = var_32;
                            r87_2 = var_33;
                            var_35 = rbx_0 + 1UL;
                            rbx_0 = var_35;
                            local_sp_0 = local_sp_2;
                            r87_0 = r87_2;
                            local_sp_3 = local_sp_2;
                            if ((long)*var_15 > (long)var_35) {
                                continue;
                            }
                            loop_state_var = 1U;
                            break;
                        }
                        if ((var_19 & 16UL) == 0UL) {
                            if ((*(unsigned char *)(local_sp_1 + 16UL) & '\x02') != '\x00') {
                                if ((var_19 & 64UL) != 0UL) {
                                    var_35 = rbx_0 + 1UL;
                                    rbx_0 = var_35;
                                    local_sp_0 = local_sp_2;
                                    r87_0 = r87_2;
                                    local_sp_3 = local_sp_2;
                                    if ((long)*var_15 > (long)var_35) {
                                        continue;
                                    }
                                    loop_state_var = 1U;
                                    break;
                                }
                                if (*(uint32_t *)(local_sp_1 + 20UL) != 0U) {
                                    var_35 = rbx_0 + 1UL;
                                    rbx_0 = var_35;
                                    local_sp_0 = local_sp_2;
                                    r87_0 = r87_2;
                                    local_sp_3 = local_sp_2;
                                    if ((long)*var_15 > (long)var_35) {
                                        continue;
                                    }
                                    loop_state_var = 1U;
                                    break;
                                }
                            }
                        }
                        if ((var_19 & 64UL) == 0UL) {
                            var_35 = rbx_0 + 1UL;
                            rbx_0 = var_35;
                            local_sp_0 = local_sp_2;
                            r87_0 = r87_2;
                            local_sp_3 = local_sp_2;
                            if ((long)*var_15 > (long)var_35) {
                                continue;
                            }
                            loop_state_var = 1U;
                            break;
                        }
                        if (*(uint32_t *)(local_sp_1 + 20UL) != 0U) {
                            var_35 = rbx_0 + 1UL;
                            rbx_0 = var_35;
                            local_sp_0 = local_sp_2;
                            r87_0 = r87_2;
                            local_sp_3 = local_sp_2;
                            if ((long)*var_15 > (long)var_35) {
                                continue;
                            }
                            loop_state_var = 1U;
                            break;
                        }
                        var_31 = rbx_0 - r87_1;
                        var_32 = local_sp_1 + (-8L);
                        *(uint64_t *)var_32 = 4245366UL;
                        indirect_placeholder_2(var_9, var_31);
                        var_33 = r87_1 + 1UL;
                        local_sp_2 = var_32;
                        r87_2 = var_33;
                    }
                }
            switch (loop_state_var) {
              case 0U:
                {
                    return rbp_0;
                }
                break;
              case 1U:
                {
                    break;
                }
                break;
            }
        }
        var_36 = *(uint64_t *)(local_sp_3 + 24UL);
        *(uint64_t *)(local_sp_3 + (-8L)) = 4245555UL;
        var_37 = indirect_placeholder_16(var_36, rdi, var_8);
        if ((uint64_t)(uint32_t)var_37 == 0UL) {
            *(uint64_t *)(local_sp_3 + (-16L)) = 4245585UL;
            indirect_placeholder_4(var_8);
            rbp_0 = 0UL;
        }
    } else {
        *(uint64_t *)(var_0 + (-112L)) = 4245204UL;
        indirect_placeholder();
    }
}
