typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_2(uint64_t param_0, uint64_t param_1);
uint64_t bb_re_copy_regs(uint64_t rcx, uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint32_t var_10;
    uint64_t var_11;
    uint64_t *var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t r13_0;
    uint64_t rax_1;
    uint64_t var_24;
    uint64_t local_sp_0;
    uint64_t var_13;
    uint64_t var_12;
    uint64_t *var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t *var_17;
    uint64_t var_18;
    uint64_t var_25;
    uint64_t rbx_0;
    uint64_t var_26;
    uint64_t *var_27;
    uint64_t *var_28;
    uint64_t rax_0;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t *var_31;
    uint64_t *var_32;
    uint64_t rbx_1;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_22;
    uint64_t var_23;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r13();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    var_8 = var_0 + (-72L);
    var_9 = rdx + 1UL;
    var_10 = (uint32_t)rcx;
    var_11 = (uint64_t)var_10;
    r13_0 = 1UL;
    rax_1 = 0UL;
    local_sp_0 = var_8;
    rbx_0 = 0UL;
    rax_0 = 0UL;
    if (var_11 == 0UL) {
        var_22 = var_9 << 3UL;
        *(uint64_t *)(var_0 + (-80L)) = 4252441UL;
        var_23 = indirect_placeholder_1(var_22);
        *(uint64_t *)(rdi + 8UL) = var_23;
        if (var_23 == 0UL) {
            return rax_1;
        }
        *(uint64_t *)(var_0 + (-88L)) = 4252462UL;
        var_24 = indirect_placeholder_1(var_22);
        *(uint64_t *)(rdi + 16UL) = var_24;
        if (var_24 != 0UL) {
            *(uint64_t *)(var_0 + (-96L)) = 4252495UL;
            indirect_placeholder();
            return rax_1;
        }
        *(uint64_t *)rdi = var_9;
    } else {
        r13_0 = var_11;
        if ((uint64_t)(var_10 + (-1)) == 0UL) {
            var_14 = (uint64_t *)rdi;
            var_15 = helper_cc_compute_c_wrapper(*var_14 - var_9, var_9, var_7, 17U);
            if (var_15 != 0UL) {
                var_16 = var_9 << 3UL;
                var_17 = (uint64_t *)(rdi + 8UL);
                var_18 = *var_17;
                *(uint64_t *)(var_0 + (-64L)) = var_16;
                *(uint64_t *)(var_0 + (-80L)) = 4252536UL;
                indirect_placeholder_2(var_18, var_16);
                if (var_16 != 0UL) {
                    return rax_1;
                }
                var_19 = (uint64_t *)(rdi + 16UL);
                var_20 = *var_19;
                var_21 = *(uint64_t *)var_8;
                *(uint64_t *)(var_0 + (-88L)) = 4252562UL;
                indirect_placeholder_2(var_20, var_21);
                *var_17 = var_16;
                *var_19 = var_16;
                *var_14 = var_9;
            }
        } else {
            r13_0 = 2UL;
            if ((uint64_t)(var_10 + (-2)) == 0UL) {
                var_12 = var_0 + (-80L);
                *(uint64_t *)var_12 = 4252627UL;
                indirect_placeholder();
                local_sp_0 = var_12;
            }
            var_13 = helper_cc_compute_c_wrapper(*(uint64_t *)rdi - rdx, rdx, var_7, 17U);
            if (var_13 == 0UL) {
                *(uint64_t *)(local_sp_0 + (-8L)) = 4252657UL;
                indirect_placeholder();
            }
        }
    }
    var_25 = helper_cc_compute_all_wrapper(rdx, 0UL, 0UL, 25U);
    rax_1 = r13_0;
    if ((uint64_t)(((unsigned char)(var_25 >> 4UL) ^ (unsigned char)var_25) & '\xc0') != 0UL) {
        var_26 = rdx << 3UL;
        var_27 = (uint64_t *)(rdi + 8UL);
        var_28 = (uint64_t *)(rdi + 16UL);
        rbx_0 = rdx;
        var_29 = (rax_0 << 1UL) + rsi;
        *(uint64_t *)(rax_0 + *var_27) = *(uint64_t *)var_29;
        *(uint64_t *)(rax_0 + *var_28) = *(uint64_t *)(var_29 + 8UL);
        var_30 = rax_0 + 8UL;
        rax_0 = var_30;
        do {
            var_29 = (rax_0 << 1UL) + rsi;
            *(uint64_t *)(rax_0 + *var_27) = *(uint64_t *)var_29;
            *(uint64_t *)(rax_0 + *var_28) = *(uint64_t *)(var_29 + 8UL);
            var_30 = rax_0 + 8UL;
            rax_0 = var_30;
        } while (var_26 != var_30);
    }
    var_31 = (uint64_t *)rdi;
    rbx_1 = rbx_0;
    if (*var_31 > rbx_0) {
        return rax_1;
    }
    var_32 = (uint64_t *)(rdi + 8UL);
    var_33 = *(uint64_t *)(rdi + 16UL);
    var_34 = rbx_1 << 3UL;
    *(uint64_t *)(var_34 + var_33) = 18446744073709551615UL;
    *(uint64_t *)(var_34 + *var_32) = 18446744073709551615UL;
    var_35 = rbx_1 + 1UL;
    rbx_1 = var_35;
    do {
        var_33 = *(uint64_t *)(rdi + 16UL);
        var_34 = rbx_1 << 3UL;
        *(uint64_t *)(var_34 + var_33) = 18446744073709551615UL;
        *(uint64_t *)(var_34 + *var_32) = 18446744073709551615UL;
        var_35 = rbx_1 + 1UL;
        rbx_1 = var_35;
    } while (*var_31 <= var_35);
    return rax_1;
}
