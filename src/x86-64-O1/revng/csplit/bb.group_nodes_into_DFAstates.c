typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_15(uint64_t param_0, uint64_t param_1);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_4(uint64_t param_0);
extern void indirect_placeholder_2(uint64_t param_0, uint64_t param_1);
uint64_t bb_group_nodes_into_DFAstates(uint64_t rcx, uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t rbp_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t r15_7;
    uint64_t rbp_1;
    uint64_t var_125;
    uint64_t r15_5;
    uint64_t var_126;
    uint64_t var_127;
    uint64_t local_sp_14;
    uint64_t var_78;
    uint64_t local_sp_13;
    uint64_t local_sp_11;
    uint64_t local_sp_7;
    uint64_t *_pre_phi;
    uint64_t local_sp_0;
    uint64_t r15_0;
    uint64_t var_120;
    uint64_t var_121;
    uint64_t var_122;
    uint64_t local_sp_1;
    uint64_t rbx_0;
    uint64_t var_123;
    uint64_t var_124;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t local_sp_2;
    uint64_t *var_32;
    uint64_t var_9;
    uint64_t *_pre_phi236;
    uint64_t local_sp_3;
    uint64_t local_sp_6;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t r15_6;
    uint64_t local_sp_4;
    uint64_t r15_1;
    uint64_t var_10;
    uint64_t *var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint32_t var_17;
    uint64_t var_18;
    uint32_t var_19;
    bool var_20;
    uint64_t var_21;
    unsigned char *var_22;
    unsigned char *_pre_phi244;
    uint64_t local_sp_5;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t local_sp_8;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_51;
    uint64_t var_56;
    uint64_t rcx1_0;
    uint64_t rdx2_0;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t *var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t rcx1_2;
    uint64_t rax_0;
    uint64_t rcx1_1;
    uint64_t var_52;
    uint64_t *var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_72;
    uint64_t rcx1_3;
    uint64_t rdx2_1;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t *var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t rcx1_5;
    uint64_t rcx1_4;
    uint64_t rdx2_2;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t *var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_79;
    uint64_t r15_3;
    uint64_t local_sp_9;
    uint64_t r15_2;
    uint64_t _pre_phi250;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t *var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t *_pre_phi246;
    uint64_t local_sp_10;
    uint64_t rax_1;
    uint64_t rcx1_6;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_113;
    uint64_t rdx2_3;
    uint64_t rdi3_0;
    uint64_t r12_0;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t *var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t var_100;
    uint64_t *var_101;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t *var_107;
    uint64_t var_108;
    uint64_t local_sp_12;
    uint64_t r15_4;
    uint64_t var_109;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t var_112;
    uint64_t var_114;
    uint64_t var_115;
    uint64_t var_116;
    uint64_t var_117;
    uint64_t var_118;
    uint64_t var_119;
    uint64_t var_23;
    uint64_t *var_128;
    uint64_t var_129;
    uint64_t var_26;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r13();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    *(uint64_t *)(var_0 + (-184L)) = rdi;
    *(uint64_t *)(var_0 + (-192L)) = rsi;
    *(uint64_t *)(var_0 + (-168L)) = rdx;
    *(uint64_t *)(var_0 + (-160L)) = rcx;
    var_7 = var_0 + (-88L);
    var_8 = var_0 + (-224L);
    *(uint64_t *)var_8 = 4248729UL;
    indirect_placeholder_4(var_7);
    rbp_0 = 0UL;
    r15_7 = 0UL;
    rbp_1 = 0UL;
    var_9 = 0UL;
    local_sp_4 = var_8;
    r15_1 = 0UL;
    rcx1_0 = 0UL;
    rdx2_0 = 0UL;
    rax_0 = 0UL;
    rcx1_1 = 0UL;
    rcx1_3 = 0UL;
    rdx2_1 = 0UL;
    rcx1_4 = 0UL;
    rdx2_2 = 0UL;
    rax_1 = 0UL;
    rcx1_6 = 0UL;
    rdx2_3 = 0UL;
    rdi3_0 = 0UL;
    r12_0 = 0UL;
    if ((long)*(uint64_t *)(rsi + 16UL) > (long)0UL) {
        return r15_7;
    }
    *(uint64_t *)(var_0 + (-216L)) = 0UL;
    r15_7 = 18446744073709551615UL;
    while (1U)
        {
            var_10 = var_9 << 3UL;
            *(uint64_t *)(local_sp_4 + 40UL) = var_10;
            var_11 = (uint64_t *)(local_sp_4 + 24UL);
            var_12 = *(uint64_t *)(var_10 + *(uint64_t *)(*var_11 + 24UL)) << 4UL;
            var_13 = local_sp_4 + 32UL;
            var_14 = var_12 + **(uint64_t **)var_13;
            var_15 = var_14 + 8UL;
            var_16 = (uint64_t)*(unsigned char *)var_15;
            var_17 = *(uint32_t *)var_15 >> 8U;
            var_18 = (uint64_t)(uint32_t)((uint16_t)var_17 & (unsigned short)1023U);
            var_19 = (uint32_t)var_16;
            var_20 = ((uint64_t)(var_19 + (-1)) == 0UL);
            r15_5 = r15_1;
            local_sp_14 = local_sp_4;
            r15_6 = r15_1;
            local_sp_5 = local_sp_4;
            r15_2 = r15_1;
            if (var_20) {
                var_40 = (uint64_t)*(unsigned char *)var_14;
                var_41 = local_sp_4 + 128UL;
                var_42 = local_sp_4 + (-8L);
                *(uint64_t *)var_42 = 4248776UL;
                indirect_placeholder_2(var_41, var_40);
                local_sp_6 = var_42;
            } else {
                if ((uint64_t)(var_19 + (-3)) == 0UL) {
                    var_37 = *(uint64_t *)var_14;
                    var_38 = local_sp_4 + 128UL;
                    var_39 = local_sp_4 + (-8L);
                    *(uint64_t *)var_39 = 4249048UL;
                    indirect_placeholder_2(var_38, var_37);
                    local_sp_6 = var_39;
                } else {
                    if ((uint64_t)(var_19 + (-5)) == 0UL) {
                        var_26 = *(uint64_t *)var_13;
                        if ((int)*(uint32_t *)(var_26 + 180UL) > (int)1U) {
                            var_29 = *(uint64_t *)(var_26 + 120UL);
                            var_30 = local_sp_4 + 128UL;
                            var_31 = local_sp_4 + (-8L);
                            *(uint64_t *)var_31 = 4249084UL;
                            indirect_placeholder_2(var_30, var_29);
                            local_sp_2 = var_31;
                        } else {
                            var_27 = local_sp_4 + 128UL;
                            var_28 = local_sp_4 + (-8L);
                            *(uint64_t *)var_28 = 4249152UL;
                            indirect_placeholder_4(var_27);
                            local_sp_2 = var_28;
                        }
                        var_32 = (uint64_t *)(local_sp_2 + 32UL);
                        _pre_phi236 = var_32;
                        local_sp_3 = local_sp_2;
                        if ((*(unsigned char *)(*var_32 + 216UL) & '@') == '\x00') {
                            var_33 = local_sp_2 + 128UL;
                            var_34 = local_sp_2 + (-8L);
                            *(uint64_t *)var_34 = 4249172UL;
                            indirect_placeholder_2(var_33, 10UL);
                            _pre_phi236 = (uint64_t *)(var_34 + 32UL);
                            local_sp_3 = var_34;
                        }
                        local_sp_6 = local_sp_3;
                        if ((signed char)*(unsigned char *)(*_pre_phi236 + 216UL) > '\xff') {
                            var_35 = local_sp_3 + 128UL;
                            var_36 = local_sp_3 + (-8L);
                            *(uint64_t *)var_36 = 4249134UL;
                            indirect_placeholder_2(var_35, 0UL);
                            local_sp_6 = var_36;
                        }
                    } else {
                        if ((uint64_t)(var_19 + (-7)) != 0UL) {
                            var_128 = (uint64_t *)(local_sp_14 + 8UL);
                            var_129 = *var_128 + 1UL;
                            *var_128 = var_129;
                            var_9 = var_129;
                            local_sp_4 = local_sp_14;
                            r15_1 = r15_6;
                            r15_7 = r15_6;
                            if ((long)*(uint64_t *)(*(uint64_t *)(local_sp_14 + 24UL) + 16UL) > (long)var_129) {
                                continue;
                            }
                            loop_state_var = 0U;
                            break;
                        }
                        var_21 = local_sp_4 + 128UL;
                        *(uint64_t *)var_21 = 18446744073709551615UL;
                        *(uint64_t *)(local_sp_4 + 136UL) = 18446744073709551615UL;
                        var_22 = (unsigned char *)(*(uint64_t *)var_13 + 216UL);
                        _pre_phi244 = var_22;
                        if ((*var_22 & '@') == '\x00') {
                            var_23 = local_sp_4 + (-8L);
                            *(uint64_t *)var_23 = 4249192UL;
                            indirect_placeholder_2(var_21, 10UL);
                            _pre_phi244 = (unsigned char *)(*var_11 + 216UL);
                            local_sp_5 = var_23;
                        }
                        local_sp_6 = local_sp_5;
                        if ((signed char)*_pre_phi244 > '\xff') {
                            var_24 = local_sp_5 + 128UL;
                            var_25 = local_sp_5 + (-8L);
                            *(uint64_t *)var_25 = 4249982UL;
                            indirect_placeholder_2(var_24, 0UL);
                            local_sp_6 = var_25;
                        }
                    }
                }
            }
            local_sp_7 = local_sp_6;
            local_sp_8 = local_sp_6;
            if ((uint64_t)((uint16_t)var_18 & (unsigned short)1023U) == 0UL) {
                var_78 = helper_cc_compute_all_wrapper(r15_1, 0UL, 0UL, 25U);
                local_sp_9 = local_sp_8;
                local_sp_13 = local_sp_8;
                if ((uint64_t)(((unsigned char)(var_78 >> 4UL) ^ (unsigned char)var_78) & '\xc0') == 0UL) {
                    r15_0 = r15_5;
                    local_sp_14 = local_sp_13;
                    r15_6 = r15_5;
                    if (r15_5 != rbp_1) {
                        var_114 = (r15_5 << 5UL) + *(uint64_t *)(local_sp_13 + 56UL);
                        var_115 = local_sp_13 + 128UL;
                        *(uint64_t *)(local_sp_13 + (-8L)) = 4250012UL;
                        indirect_placeholder_2(var_114, var_115);
                        var_116 = *(uint64_t *)(*(uint64_t *)(local_sp_13 + 32UL) + *(uint64_t *)(*(uint64_t *)(local_sp_13 + 16UL) + 24UL));
                        var_117 = (r15_5 * 24UL) + *(uint64_t *)(local_sp_13 + 40UL);
                        var_118 = local_sp_13 + (-16L);
                        *(uint64_t *)var_118 = 4250048UL;
                        var_119 = indirect_placeholder_15(var_117, var_116);
                        local_sp_0 = var_118;
                        if ((uint64_t)(uint32_t)var_119 != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_125 = r15_5 + 1UL;
                        var_126 = local_sp_13 + 112UL;
                        var_127 = local_sp_13 + (-24L);
                        *(uint64_t *)var_127 = 4250069UL;
                        indirect_placeholder_4(var_126);
                        local_sp_14 = var_127;
                        r15_6 = var_125;
                    }
                    var_128 = (uint64_t *)(local_sp_14 + 8UL);
                    var_129 = *var_128 + 1UL;
                    *var_128 = var_129;
                    var_9 = var_129;
                    local_sp_4 = local_sp_14;
                    r15_1 = r15_6;
                    r15_7 = r15_6;
                    if ((long)*(uint64_t *)(*(uint64_t *)(local_sp_14 + 24UL) + 16UL) > (long)var_129) {
                        continue;
                    }
                    loop_state_var = 0U;
                    break;
                }
                *(uint64_t *)(local_sp_8 + 16UL) = var_14;
                var_79 = *(uint64_t *)(local_sp_8 + 56UL);
                while (1U)
                    {
                        rbp_1 = rbp_0;
                        r15_0 = r15_2;
                        r15_3 = r15_2;
                        local_sp_10 = local_sp_9;
                        r15_4 = r15_2;
                        if (var_20) {
                            var_80 = (uint64_t)**(unsigned char **)(local_sp_9 + 16UL);
                            var_81 = (rbp_0 << 5UL) + var_79;
                            var_82 = local_sp_9 + (-8L);
                            var_83 = (uint64_t *)var_82;
                            *var_83 = 4249556UL;
                            var_84 = indirect_placeholder_15(var_81, var_80);
                            _pre_phi250 = var_81;
                            _pre_phi246 = var_83;
                            local_sp_10 = var_82;
                            local_sp_11 = var_82;
                            if ((uint64_t)(unsigned char)var_84 != 0UL) {
                                var_113 = rbp_0 + 1UL;
                                rbp_0 = var_113;
                                rbp_1 = var_113;
                                r15_5 = r15_3;
                                local_sp_13 = local_sp_11;
                                local_sp_9 = local_sp_11;
                                r15_2 = r15_3;
                                if ((long)r15_3 > (long)var_113) {
                                    continue;
                                }
                                loop_state_var = 0U;
                                break;
                            }
                        }
                        _pre_phi250 = (rbp_0 << 5UL) + var_79;
                        _pre_phi246 = (uint64_t *)local_sp_9;
                        *_pre_phi246 = rbp_0;
                        local_sp_11 = local_sp_10;
                        _pre_phi = _pre_phi246;
                        local_sp_12 = local_sp_10;
                        var_85 = rax_1 << 3UL;
                        var_86 = var_85 + local_sp_10;
                        var_87 = *(uint64_t *)(var_86 + 128UL) & *(uint64_t *)(var_85 + _pre_phi250);
                        *(uint64_t *)(var_86 + 64UL) = var_87;
                        var_88 = rcx1_6 | var_87;
                        rcx1_6 = var_88;
                        while (rax_1 != 3UL)
                            {
                                rax_1 = rax_1 + 1UL;
                                var_85 = rax_1 << 3UL;
                                var_86 = var_85 + local_sp_10;
                                var_87 = *(uint64_t *)(var_86 + 128UL) & *(uint64_t *)(var_85 + _pre_phi250);
                                *(uint64_t *)(var_86 + 64UL) = var_87;
                                var_88 = rcx1_6 | var_87;
                                rcx1_6 = var_88;
                            }
                        if (var_88 == 0UL) {
                            var_113 = rbp_0 + 1UL;
                            rbp_0 = var_113;
                            rbp_1 = var_113;
                            r15_5 = r15_3;
                            local_sp_13 = local_sp_11;
                            local_sp_9 = local_sp_11;
                            r15_2 = r15_3;
                            if ((long)r15_3 > (long)var_113) {
                                continue;
                            }
                            loop_state_var = 0U;
                            break;
                        }
                        var_89 = rdx2_3 << 3UL;
                        var_90 = var_89 + local_sp_10;
                        var_91 = (uint64_t *)(var_90 + 128UL);
                        var_92 = *var_91;
                        var_93 = *(uint64_t *)(var_89 + _pre_phi250);
                        var_94 = var_93 & (var_92 ^ (-1L));
                        *(uint64_t *)(var_90 + 96UL) = var_94;
                        var_95 = rdi3_0 | var_94;
                        var_96 = var_92 & (var_93 ^ (-1L));
                        *var_91 = var_96;
                        var_97 = r12_0 | var_96;
                        rdi3_0 = var_95;
                        r12_0 = var_97;
                        while (rdx2_3 != 3UL)
                            {
                                rdx2_3 = rdx2_3 + 1UL;
                                var_89 = rdx2_3 << 3UL;
                                var_90 = var_89 + local_sp_10;
                                var_91 = (uint64_t *)(var_90 + 128UL);
                                var_92 = *var_91;
                                var_93 = *(uint64_t *)(var_89 + _pre_phi250);
                                var_94 = var_93 & (var_92 ^ (-1L));
                                *(uint64_t *)(var_90 + 96UL) = var_94;
                                var_95 = rdi3_0 | var_94;
                                var_96 = var_92 & (var_93 ^ (-1L));
                                *var_91 = var_96;
                                var_97 = r12_0 | var_96;
                                rdi3_0 = var_95;
                                r12_0 = var_97;
                            }
                        if (var_95 != 0UL) {
                            var_98 = (r15_2 << 5UL) + var_79;
                            var_99 = local_sp_10 + 96UL;
                            *(uint64_t *)(local_sp_10 + (-8L)) = 4249659UL;
                            indirect_placeholder_2(var_98, var_99);
                            var_100 = local_sp_10 + 56UL;
                            var_101 = (uint64_t *)(local_sp_10 + (-16L));
                            *var_101 = 4249672UL;
                            indirect_placeholder_2(_pre_phi250, var_100);
                            var_102 = *var_101;
                            var_103 = *(uint64_t *)(local_sp_10 + 32UL);
                            var_104 = (var_102 * 24UL) + var_103;
                            var_105 = (r15_2 * 24UL) + var_103;
                            var_106 = local_sp_10 + (-24L);
                            var_107 = (uint64_t *)var_106;
                            *var_107 = 4249702UL;
                            var_108 = indirect_placeholder_15(var_105, var_104);
                            local_sp_0 = var_106;
                            _pre_phi = var_107;
                            local_sp_12 = var_106;
                            if ((uint64_t)(uint32_t)var_108 != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            r15_4 = r15_2 + 1UL;
                        }
                        var_109 = *(uint64_t *)(*(uint64_t *)(local_sp_12 + 40UL) + *(uint64_t *)(*(uint64_t *)(local_sp_12 + 24UL) + 24UL));
                        var_110 = (*_pre_phi * 24UL) + *(uint64_t *)(local_sp_12 + 48UL);
                        var_111 = local_sp_12 + (-8L);
                        *(uint64_t *)var_111 = 4249754UL;
                        var_112 = indirect_placeholder_15(var_110, var_109);
                        r15_5 = r15_4;
                        local_sp_13 = var_111;
                        local_sp_11 = var_111;
                        local_sp_0 = var_111;
                        r15_0 = r15_4;
                        r15_3 = r15_4;
                        if ((uint64_t)(unsigned char)var_112 != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        if (var_97 != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 0U:
                    {
                        break;
                    }
                    break;
                  case 1U:
                    {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
            if ((var_18 & 32UL) != 0UL) {
                var_43 = local_sp_6 + 128UL;
                *(uint64_t *)(local_sp_6 + (-8L)) = 4249215UL;
                var_44 = indirect_placeholder_15(var_43, 10UL);
                var_45 = local_sp_6 + 120UL;
                var_46 = local_sp_6 + (-16L);
                *(uint64_t *)var_46 = 4249230UL;
                indirect_placeholder_4(var_45);
                local_sp_14 = var_46;
                if ((uint64_t)(unsigned char)var_44 != 0UL) {
                    var_128 = (uint64_t *)(local_sp_14 + 8UL);
                    var_129 = *var_128 + 1UL;
                    *var_128 = var_129;
                    var_9 = var_129;
                    local_sp_4 = local_sp_14;
                    r15_1 = r15_6;
                    r15_7 = r15_6;
                    if ((long)*(uint64_t *)(*(uint64_t *)(local_sp_14 + 24UL) + 16UL) > (long)var_129) {
                        continue;
                    }
                    loop_state_var = 0U;
                    break;
                }
                var_47 = local_sp_6 + 112UL;
                var_48 = local_sp_6 + (-24L);
                *(uint64_t *)var_48 = 4249257UL;
                indirect_placeholder_2(var_47, 10UL);
                local_sp_7 = var_48;
            }
            local_sp_8 = local_sp_7;
            local_sp_14 = local_sp_7;
            if ((signed char)(unsigned char)var_17 <= '\xff') {
                var_49 = local_sp_7 + 128UL;
                var_50 = local_sp_7 + (-8L);
                *(uint64_t *)var_50 = 4249275UL;
                indirect_placeholder_4(var_49);
                local_sp_14 = var_50;
                var_128 = (uint64_t *)(local_sp_14 + 8UL);
                var_129 = *var_128 + 1UL;
                *var_128 = var_129;
                var_9 = var_129;
                local_sp_4 = local_sp_14;
                r15_1 = r15_6;
                r15_7 = r15_6;
                if ((long)*(uint64_t *)(*(uint64_t *)(local_sp_14 + 24UL) + 16UL) > (long)var_129) {
                    continue;
                }
                loop_state_var = 0U;
                break;
            }
            if ((var_18 & 4UL) != 0UL) {
                if (!var_20) {
                    if ((*(unsigned char *)(var_14 + 10UL) & '@') != '\x00') {
                        var_62 = local_sp_7 + 128UL;
                        var_63 = local_sp_7 + (-8L);
                        *(uint64_t *)var_63 = 4249304UL;
                        indirect_placeholder_4(var_62);
                        local_sp_14 = var_63;
                        var_128 = (uint64_t *)(local_sp_14 + 8UL);
                        var_129 = *var_128 + 1UL;
                        *var_128 = var_129;
                        var_9 = var_129;
                        local_sp_4 = local_sp_14;
                        r15_1 = r15_6;
                        r15_7 = r15_6;
                        if ((long)*(uint64_t *)(*(uint64_t *)(local_sp_14 + 24UL) + 16UL) > (long)var_129) {
                            continue;
                        }
                        loop_state_var = 0U;
                        break;
                    }
                }
                var_51 = *(uint64_t *)(local_sp_7 + 32UL);
                if ((int)*(uint32_t *)(var_51 + 180UL) > (int)1U) {
                    var_52 = rax_0 << 3UL;
                    var_53 = (uint64_t *)((var_52 + local_sp_7) + 128UL);
                    var_54 = *var_53 & *(uint64_t *)((var_52 + var_51) + 184UL);
                    *var_53 = var_54;
                    var_55 = rcx1_1 | var_54;
                    rcx1_1 = var_55;
                    rcx1_2 = var_55;
                    while (rax_0 != 3UL)
                        {
                            rax_0 = rax_0 + 1UL;
                            var_52 = rax_0 << 3UL;
                            var_53 = (uint64_t *)((var_52 + local_sp_7) + 128UL);
                            var_54 = *var_53 & *(uint64_t *)((var_52 + var_51) + 184UL);
                            *var_53 = var_54;
                            var_55 = rcx1_1 | var_54;
                            rcx1_1 = var_55;
                            rcx1_2 = var_55;
                        }
                }
                var_56 = *(uint64_t *)(var_51 + 120UL);
                var_57 = rdx2_0 << 3UL;
                var_58 = *(uint64_t *)((var_57 + var_51) + 184UL) | (*(uint64_t *)(var_57 + var_56) ^ (-1L));
                var_59 = (uint64_t *)((var_57 + local_sp_7) + 128UL);
                var_60 = var_58 & *var_59;
                *var_59 = var_60;
                var_61 = rcx1_0 | var_60;
                rcx1_0 = var_61;
                rcx1_2 = var_61;
                while (rdx2_0 != 3UL)
                    {
                        rdx2_0 = rdx2_0 + 1UL;
                        var_57 = rdx2_0 << 3UL;
                        var_58 = *(uint64_t *)((var_57 + var_51) + 184UL) | (*(uint64_t *)(var_57 + var_56) ^ (-1L));
                        var_59 = (uint64_t *)((var_57 + local_sp_7) + 128UL);
                        var_60 = var_58 & *var_59;
                        *var_59 = var_60;
                        var_61 = rcx1_0 | var_60;
                        rcx1_0 = var_61;
                        rcx1_2 = var_61;
                    }
                if (rcx1_2 != 0UL) {
                    var_128 = (uint64_t *)(local_sp_14 + 8UL);
                    var_129 = *var_128 + 1UL;
                    *var_128 = var_129;
                    var_9 = var_129;
                    local_sp_4 = local_sp_14;
                    r15_1 = r15_6;
                    r15_7 = r15_6;
                    if ((long)*(uint64_t *)(*(uint64_t *)(local_sp_14 + 24UL) + 16UL) > (long)var_129) {
                        continue;
                    }
                    loop_state_var = 0U;
                    break;
                }
            }
            if ((var_18 & 8UL) != 0UL) {
                if (!var_20) {
                    if ((*(unsigned char *)(var_14 + 10UL) & '@') != '\x00') {
                        var_64 = local_sp_7 + 128UL;
                        var_65 = local_sp_7 + (-8L);
                        *(uint64_t *)var_65 = 4249390UL;
                        indirect_placeholder_4(var_64);
                        local_sp_14 = var_65;
                        var_128 = (uint64_t *)(local_sp_14 + 8UL);
                        var_129 = *var_128 + 1UL;
                        *var_128 = var_129;
                        var_9 = var_129;
                        local_sp_4 = local_sp_14;
                        r15_1 = r15_6;
                        r15_7 = r15_6;
                        if ((long)*(uint64_t *)(*(uint64_t *)(local_sp_14 + 24UL) + 16UL) > (long)var_129) {
                            continue;
                        }
                        loop_state_var = 0U;
                        break;
                    }
                }
                var_66 = *(uint64_t *)(local_sp_7 + 32UL);
                if ((int)*(uint32_t *)(var_66 + 180UL) > (int)1U) {
                    var_67 = rdx2_2 << 3UL;
                    var_68 = *(uint64_t *)((var_67 + var_66) + 184UL) ^ (-1L);
                    var_69 = (uint64_t *)((var_67 + local_sp_7) + 128UL);
                    var_70 = *var_69 & var_68;
                    *var_69 = var_70;
                    var_71 = rcx1_4 | var_70;
                    rcx1_4 = var_71;
                    rcx1_5 = var_71;
                    while (rdx2_2 != 3UL)
                        {
                            rdx2_2 = rdx2_2 + 1UL;
                            var_67 = rdx2_2 << 3UL;
                            var_68 = *(uint64_t *)((var_67 + var_66) + 184UL) ^ (-1L);
                            var_69 = (uint64_t *)((var_67 + local_sp_7) + 128UL);
                            var_70 = *var_69 & var_68;
                            *var_69 = var_70;
                            var_71 = rcx1_4 | var_70;
                            rcx1_4 = var_71;
                            rcx1_5 = var_71;
                        }
                }
                var_72 = *(uint64_t *)(var_66 + 120UL);
                var_73 = rdx2_1 << 3UL;
                var_74 = (*(uint64_t *)((var_73 + var_66) + 184UL) & *(uint64_t *)(var_73 + var_72)) ^ (-1L);
                var_75 = (uint64_t *)((var_73 + local_sp_7) + 128UL);
                var_76 = *var_75 & var_74;
                *var_75 = var_76;
                var_77 = rcx1_3 | var_76;
                rcx1_3 = var_77;
                rcx1_5 = var_77;
                while (rdx2_1 != 3UL)
                    {
                        rdx2_1 = rdx2_1 + 1UL;
                        var_73 = rdx2_1 << 3UL;
                        var_74 = (*(uint64_t *)((var_73 + var_66) + 184UL) & *(uint64_t *)(var_73 + var_72)) ^ (-1L);
                        var_75 = (uint64_t *)((var_73 + local_sp_7) + 128UL);
                        var_76 = *var_75 & var_74;
                        *var_75 = var_76;
                        var_77 = rcx1_3 | var_76;
                        rcx1_3 = var_77;
                        rcx1_5 = var_77;
                    }
                if (rcx1_5 != 0UL) {
                    var_128 = (uint64_t *)(local_sp_14 + 8UL);
                    var_129 = *var_128 + 1UL;
                    *var_128 = var_129;
                    var_9 = var_129;
                    local_sp_4 = local_sp_14;
                    r15_1 = r15_6;
                    r15_7 = r15_6;
                    if ((long)*(uint64_t *)(*(uint64_t *)(local_sp_14 + 24UL) + 16UL) > (long)var_129) {
                        continue;
                    }
                    loop_state_var = 0U;
                    break;
                }
            }
        }
    var_120 = helper_cc_compute_all_wrapper(r15_0, 0UL, 0UL, 25U);
    local_sp_1 = local_sp_0;
    if ((uint64_t)(((unsigned char)(var_120 >> 4UL) ^ (unsigned char)var_120) & '\xc0') == 0UL) {
        return;
    }
    var_121 = *(uint64_t *)(local_sp_0 + 48UL) + 16UL;
    var_122 = (r15_0 * 24UL) + var_121;
    rbx_0 = var_121;
    var_123 = local_sp_1 + (-8L);
    *(uint64_t *)var_123 = 4250104UL;
    indirect_placeholder();
    var_124 = rbx_0 + 24UL;
    local_sp_1 = var_123;
    rbx_0 = var_124;
    do {
        var_123 = local_sp_1 + (-8L);
        *(uint64_t *)var_123 = 4250104UL;
        indirect_placeholder();
        var_124 = rbx_0 + 24UL;
        local_sp_1 = var_123;
        rbx_0 = var_124;
    } while (var_124 != var_122);
}
