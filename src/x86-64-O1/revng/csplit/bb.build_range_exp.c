typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint32_t init_state_0x82fc(void);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_15(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_2(uint64_t param_0, uint64_t param_1);
uint64_t bb_build_range_exp(uint64_t rcx, uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t r9, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint32_t var_8;
    uint64_t var_9;
    uint32_t var_10;
    uint64_t rax_0;
    uint32_t *_cast;
    uint32_t var_11;
    bool var_12;
    unsigned char rdi3_3_shrunk;
    uint64_t var_13;
    uint64_t rcx1_0;
    uint64_t rdi3_0;
    uint64_t rcx1_1;
    unsigned char var_14;
    uint64_t var_15;
    uint64_t _pre109;
    uint64_t _pre_phi;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t rcx1_2;
    uint64_t rdi3_1;
    uint64_t rcx1_3;
    unsigned char var_19;
    uint64_t var_20;
    uint64_t rcx1_4;
    uint64_t rdi3_2;
    uint64_t rcx1_5;
    unsigned char var_21;
    uint64_t var_22;
    uint64_t rdi3_3;
    unsigned char r15_0_shrunk;
    uint64_t r15_0;
    uint32_t var_25;
    uint64_t local_sp_0;
    uint64_t storemerge3;
    uint64_t local_sp_1;
    uint64_t storemerge;
    uint32_t var_28;
    uint32_t var_29;
    uint64_t local_sp_3;
    uint64_t *var_30;
    uint64_t var_31;
    uint64_t *_pre_phi117;
    uint64_t *_pre_phi113;
    uint64_t var_41;
    uint64_t local_sp_2;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t *var_34;
    uint64_t var_35;
    uint64_t *var_36;
    uint64_t *var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t r12_0;
    uint64_t local_sp_4;
    uint64_t local_sp_5;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_23;
    uint64_t var_24;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r13();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_cc_src2();
    var_8 = init_state_0x82fc();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    var_9 = var_0 + (-72L);
    *(uint64_t *)var_9 = rcx;
    var_10 = *(uint32_t *)r8;
    rax_0 = 11UL;
    rdi3_3_shrunk = (unsigned char)'\x00';
    rcx1_0 = 18446744073709551615UL;
    rcx1_1 = 0UL;
    rcx1_2 = 18446744073709551615UL;
    rcx1_3 = 0UL;
    rcx1_4 = 18446744073709551615UL;
    rcx1_5 = 0UL;
    r15_0_shrunk = (unsigned char)'\x00';
    local_sp_0 = var_9;
    r12_0 = 0UL;
    if ((uint64_t)((var_10 + (-2)) & (-3)) == 0UL) {
        return rax_0;
    }
    _cast = (uint32_t *)r9;
    var_11 = *_cast;
    var_25 = var_11;
    if ((uint64_t)((var_11 + (-2)) & (-3)) == 0UL) {
        return;
    }
    var_12 = ((uint64_t)(var_10 + (-3)) == 0UL);
    rax_0 = 3UL;
    if (!var_12) {
        if ((uint64_t)(var_11 + (-3)) == 0UL) {
            var_13 = (uint64_t)var_8;
            rdi3_0 = *(uint64_t *)(r9 + 8UL);
            while (rcx1_0 != 0UL)
                {
                    var_14 = *(unsigned char *)rdi3_0;
                    var_15 = rcx1_0 + (-1L);
                    rcx1_0 = var_15;
                    rcx1_1 = var_15;
                    if (var_14 == '\x00') {
                        break;
                    }
                    rdi3_0 = rdi3_0 + var_13;
                }
            if ((18446744073709551614UL - (-2L)) > 1UL) {
                return rax_0;
            }
            if (var_10 == 0U) {
                rdi3_3_shrunk = *(unsigned char *)(r8 + 8UL);
            } else {
                if (!0) {
                    _pre109 = r8 + 8UL;
                    _pre_phi = _pre109;
                    rdi3_3_shrunk = **(unsigned char **)_pre_phi;
                }
            }
        } else {
            if (var_10 == 0U) {
                rdi3_3_shrunk = *(unsigned char *)(r8 + 8UL);
            }
        }
        rdi3_3 = (uint64_t)rdi3_3_shrunk;
        if (var_11 == 0U) {
            r15_0_shrunk = *(unsigned char *)(r9 + 8UL);
        } else {
            if ((uint64_t)(var_11 + (-3)) == 0UL) {
                r15_0_shrunk = **(unsigned char **)(r9 + 8UL);
            }
        }
        r15_0 = (uint64_t)r15_0_shrunk;
        if ((var_10 == 0U) || var_12) {
            var_23 = var_0 + (-80L);
            *(uint64_t *)var_23 = 4253114UL;
            var_24 = indirect_placeholder_15(rdi3_3, rdx);
            var_25 = *_cast;
            local_sp_0 = var_23;
            storemerge3 = (uint64_t)(uint32_t)var_24;
        } else {
            storemerge3 = (uint64_t)*(uint32_t *)(r8 + 8UL);
        }
        var_26 = local_sp_0 + (-8L);
        *(uint64_t *)var_26 = 4253132UL;
        var_27 = indirect_placeholder_15(r15_0, rdx);
        local_sp_1 = var_26;
        storemerge = (uint64_t)(uint32_t)var_27;
        var_28 = (uint32_t)storemerge3;
        local_sp_2 = local_sp_1;
        local_sp_3 = local_sp_1;
        var_29 = (uint32_t)storemerge;
        rax_0 = 11UL;
        if ((uint64_t)(var_28 + 1U) != 0UL & (uint64_t)(var_29 + 1U) != 0UL & !(((uint64_t)((uint32_t)rdi & 65536U) != 0UL) && (storemerge3 > storemerge))) {
            rax_0 = 0UL;
            if (rdx != 0UL) {
                var_30 = (uint64_t *)(rdx + 64UL);
                var_31 = *var_30;
                var_41 = var_31;
                if (**(uint64_t **)local_sp_1 == var_31) {
                    var_32 = (var_31 << 1UL) | 1UL;
                    var_33 = var_32 << 2UL;
                    var_34 = (uint64_t *)(rdx + 8UL);
                    var_35 = *var_34;
                    *(uint64_t *)(local_sp_1 + 8UL) = var_33;
                    var_36 = (uint64_t *)(local_sp_1 + (-8L));
                    *var_36 = 4253171UL;
                    indirect_placeholder_2(var_35, var_33);
                    var_37 = (uint64_t *)(rdx + 16UL);
                    var_38 = *var_37;
                    var_39 = *(uint64_t *)local_sp_1;
                    var_40 = local_sp_1 + (-16L);
                    *(uint64_t *)var_40 = 4253189UL;
                    indirect_placeholder_2(var_38, var_39);
                    *var_36 = var_33;
                    *var_34 = var_33;
                    *var_37 = *var_36;
                    **(uint64_t **)var_40 = var_32;
                    _pre_phi117 = var_37;
                    _pre_phi113 = var_34;
                    var_41 = *var_30;
                    local_sp_2 = var_40;
                } else {
                    _pre_phi117 = (uint64_t *)(rdx + 16UL);
                    _pre_phi113 = (uint64_t *)(rdx + 8UL);
                }
                *(uint32_t *)((var_41 << 2UL) + *_pre_phi113) = var_28;
                var_42 = *_pre_phi117;
                var_43 = *var_30;
                *var_30 = (var_43 + 1UL);
                *(uint32_t *)((var_43 << 2UL) + var_42) = var_29;
                local_sp_3 = local_sp_2;
            }
            local_sp_4 = local_sp_3;
            while (1U)
                {
                    local_sp_5 = local_sp_4;
                    var_44 = helper_cc_compute_c_wrapper(storemerge - r12_0, r12_0, var_7, 16U);
                    if (storemerge3 <= (uint64_t)(uint32_t)r12_0 & var_44 == 0UL) {
                        var_45 = local_sp_4 + (-8L);
                        *(uint64_t *)var_45 = 4253290UL;
                        indirect_placeholder_2(rsi, r12_0);
                        local_sp_5 = var_45;
                    }
                    local_sp_4 = local_sp_5;
                    if (r12_0 == 255UL) {
                        break;
                    }
                    r12_0 = r12_0 + 1UL;
                    continue;
                }
        }
    }
    var_16 = r8 + 8UL;
    var_17 = *(uint64_t *)var_16;
    var_18 = (uint64_t)var_8;
    rdi3_1 = var_17;
    _pre_phi = var_16;
    while (rcx1_2 != 0UL)
        {
            var_19 = *(unsigned char *)rdi3_1;
            var_20 = rcx1_2 + (-1L);
            rcx1_2 = var_20;
            rcx1_3 = var_20;
            if (var_19 == '\x00') {
                break;
            }
            rdi3_1 = rdi3_1 + var_18;
        }
    if ((18446744073709551614UL - (-2L)) > 1UL) {
        return rax_0;
    }
    if ((uint64_t)(var_11 + (-3)) != 0UL) {
        rdi3_2 = *(uint64_t *)(r9 + 8UL);
        while (rcx1_4 != 0UL)
            {
                var_21 = *(unsigned char *)rdi3_2;
                var_22 = rcx1_4 + (-1L);
                rcx1_4 = var_22;
                rcx1_5 = var_22;
                if (var_21 == '\x00') {
                    break;
                }
                rdi3_2 = rdi3_2 + var_18;
            }
        if ((18446744073709551614UL - (-2L)) > 1UL) {
            return rax_0;
        }
    }
    rdi3_3_shrunk = **(unsigned char **)_pre_phi;
}
