typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_16(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern uint64_t indirect_placeholder_6(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
uint64_t bb_parse_branch(uint64_t rcx, uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t r9, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint32_t *var_9;
    uint64_t rbp_1;
    uint64_t rax_0;
    uint64_t rbp_0;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t *var_22;
    uint64_t var_23;
    uint64_t local_sp_0;
    uint64_t storemerge;
    unsigned char var_24;
    uint64_t local_sp_1;
    unsigned char *var_10;
    unsigned char var_11;
    uint64_t var_12;
    uint64_t *var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    bool var_17;
    bool var_18;
    bool or_cond;
    bool var_19;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r13();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    *(uint64_t *)(var_0 + (-88L)) = rdi;
    *(uint64_t *)(var_0 + (-80L)) = rcx;
    *(uint64_t *)(var_0 + (-64L)) = *(uint64_t *)rsi;
    var_7 = var_0 + (-96L);
    *(uint64_t *)var_7 = 4258709UL;
    var_8 = indirect_placeholder_7(rcx, rdx, rdi, rsi, r9, r8);
    var_9 = (uint32_t *)r9;
    local_sp_1 = var_7;
    rbp_0 = var_8;
    rbp_1 = 0UL;
    var_10 = (unsigned char *)(rdx + 8UL);
    var_11 = *var_10;
    rax_0 = (uint64_t)var_11;
    rbp_1 = var_8;
    if (!(!((*var_9 != 0U) && (var_8 == 0UL)) & (uint64_t)((var_11 & '\xf7') + '\xfe') != 0UL)) {
        return rbp_1;
    }
    rbp_1 = rbp_0;
    while (!((((uint64_t)((unsigned char)rax_0 + '\xf7') == 0UL) ^ 1) || (r8 == 0UL)))
        {
            var_12 = *(uint64_t *)(local_sp_1 + 8UL);
            var_13 = (uint64_t *)local_sp_1;
            var_14 = *var_13;
            var_15 = local_sp_1 + (-8L);
            *(uint64_t *)var_15 = 4258899UL;
            var_16 = indirect_placeholder_7(var_12, rdx, var_14, rsi, r9, r8);
            var_17 = (*var_9 != 0U);
            var_18 = (var_16 == 0UL);
            or_cond = var_17 && var_18;
            var_19 = (rbp_0 == 0UL);
            local_sp_0 = var_15;
            rbp_1 = 0UL;
            if (!or_cond) {
                if (var_19) {
                    break;
                }
                *(uint64_t *)(local_sp_1 + (-16L)) = 4258768UL;
                indirect_placeholder_16(0UL, rbp_0, 4242276UL);
                break;
            }
            if (!(var_19 || var_18)) {
                var_20 = *(uint64_t *)(local_sp_1 + 16UL);
                var_21 = local_sp_1 + (-16L);
                var_22 = (uint64_t *)var_21;
                *var_22 = 4258949UL;
                var_23 = indirect_placeholder_6(16UL, var_16, var_20, rbp_0);
                *var_13 = var_23;
                local_sp_0 = var_21;
                storemerge = var_23;
                if (var_23 != 0UL) {
                    *(uint64_t *)(local_sp_1 + (-24L)) = 4258793UL;
                    indirect_placeholder_16(0UL, var_16, 4242276UL);
                    *(uint64_t *)(local_sp_1 + (-32L)) = 4258811UL;
                    indirect_placeholder_16(0UL, rbp_0, 4242276UL);
                    *var_9 = 12U;
                    rbp_1 = *var_22;
                    break;
                }
            }
            storemerge = var_19 ? var_16 : rbp_0;
        }
    return rbp_1;
}
