typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_153_ret_type;
struct indirect_placeholder_153_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_15(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_16(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_4(uint64_t param_0);
extern struct indirect_placeholder_153_ret_type indirect_placeholder_153(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
uint64_t bb_create_ci_newstate(uint64_t rcx, uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t r10, uint64_t r9, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    struct indirect_placeholder_153_ret_type var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t rbx_0;
    uint64_t *var_11;
    uint64_t *var_12;
    uint64_t *var_13;
    unsigned char *var_14;
    uint64_t rsi4_0;
    uint64_t var_15;
    uint64_t var_16;
    uint32_t var_17;
    unsigned char var_18;
    unsigned char var_22;
    uint32_t *_pre_phi;
    uint32_t *var_19;
    unsigned char var_20;
    unsigned char var_21;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_9;
    uint64_t var_10;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r13();
    var_3 = init_rbp();
    var_4 = init_r12();
    var_5 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_5;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_3;
    *(uint64_t *)(var_0 + (-40L)) = var_1;
    *(uint64_t *)(var_0 + (-48L)) = 4246805UL;
    var_6 = indirect_placeholder_153(var_1, rcx, 112UL, rsi, 1UL, r10, r9, r8);
    var_7 = var_6.field_0;
    var_8 = var_6.field_3;
    rsi4_0 = 0UL;
    rbx_0 = 0UL;
    if (var_7 == 0UL) {
        return rbx_0;
    }
    var_9 = var_7 + 8UL;
    *(uint64_t *)(var_0 + (-56L)) = 4246832UL;
    var_10 = indirect_placeholder_15(var_9, var_8);
    rbx_0 = var_7;
    if ((uint64_t)(uint32_t)var_10 == 0UL) {
        *(uint64_t *)(var_0 + (-64L)) = 4246866UL;
        indirect_placeholder();
    } else {
        *(uint64_t *)(var_7 + 80UL) = var_9;
        var_11 = (uint64_t *)(var_8 + 8UL);
        if ((long)*var_11 <= (long)0UL) {
            var_12 = (uint64_t *)(var_8 + 16UL);
            var_13 = (uint64_t *)rdi;
            var_14 = (unsigned char *)(var_7 + 104UL);
            var_23 = rsi4_0 + 1UL;
            rsi4_0 = var_23;
            do {
                var_15 = (*(uint64_t *)((rsi4_0 << 3UL) + *var_12) << 4UL) + *var_13;
                var_16 = var_15 + 8UL;
                var_17 = (uint32_t)(uint64_t)*(unsigned char *)var_16;
                if ((uint64_t)(var_17 + (-1)) == 0UL) {
                    var_19 = (uint32_t *)var_16;
                    _pre_phi = var_19;
                    if ((*var_19 & 261888U) != 0U) {
                        var_23 = rsi4_0 + 1UL;
                        rsi4_0 = var_23;
                        if ((long)*var_11 <= (long)var_23) {
                            continue;
                        }
                        break;
                    }
                    var_20 = ((*(unsigned char *)(var_15 + 10UL) << '\x01') & ' ') | *var_14;
                    *var_14 = var_20;
                    var_21 = var_20;
                } else {
                    var_18 = ((*(unsigned char *)(var_15 + 10UL) << '\x01') & ' ') | *var_14;
                    *var_14 = var_18;
                    var_21 = var_18;
                    var_22 = var_18;
                    if ((uint64_t)(var_17 + (-2)) != 0UL) {
                        *var_14 = (var_18 | '\x10');
                        var_23 = rsi4_0 + 1UL;
                        rsi4_0 = var_23;
                        if ((long)*var_11 <= (long)var_23) {
                            continue;
                        }
                        break;
                    }
                    if ((uint64_t)(var_17 + (-4)) != 0UL) {
                        *var_14 = (var_18 | '@');
                        var_23 = rsi4_0 + 1UL;
                        rsi4_0 = var_23;
                        if ((long)*var_11 <= (long)var_23) {
                            continue;
                        }
                        break;
                    }
                    if ((uint64_t)(var_17 + (-12)) != 0UL) {
                        *var_14 = (var_22 | '\x80');
                        var_23 = rsi4_0 + 1UL;
                        rsi4_0 = var_23;
                        if ((long)*var_11 <= (long)var_23) {
                            continue;
                        }
                        break;
                    }
                    _pre_phi = (uint32_t *)var_16;
                }
                var_22 = var_21;
                if ((*_pre_phi & 261888U) == 0U) {
                    *var_14 = (var_22 | '\x80');
                }
                var_23 = rsi4_0 + 1UL;
                rsi4_0 = var_23;
            } while ((long)*var_11 <= (long)var_23);
        }
        *(uint64_t *)(var_0 + (-64L)) = 4247044UL;
        var_24 = indirect_placeholder_16(rdx, rdi, var_7);
        if ((uint64_t)(uint32_t)var_24 == 0UL) {
            *(uint64_t *)(var_0 + (-72L)) = 4247068UL;
            indirect_placeholder_4(var_7);
            rbx_0 = 0UL;
        }
    }
    return rbx_0;
}
