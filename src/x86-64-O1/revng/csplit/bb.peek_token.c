typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern void function_dispatcher(unsigned char *param_0);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_15(uint64_t param_0, uint64_t param_1);
uint64_t bb_peek_token(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t var_7;
    uint64_t var_24;
    uint64_t rax_1;
    uint64_t rax_2;
    uint64_t var_21;
    uint64_t var_22;
    uint32_t var_23;
    uint64_t var_17;
    unsigned char var_8;
    uint64_t var_9;
    unsigned char *var_10;
    unsigned char *var_11;
    uint32_t *var_12;
    uint64_t rax_0;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint32_t var_16;
    uint64_t var_18;
    uint64_t var_19;
    unsigned char var_20;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r13();
    var_3 = init_rbp();
    var_4 = init_r12();
    var_5 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_5;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_3;
    *(uint64_t *)(var_0 + (-40L)) = var_1;
    var_6 = (uint64_t *)(rsi + 72UL);
    var_7 = *var_6;
    rax_0 = var_7;
    rax_1 = 1UL;
    rax_2 = 1UL;
    if ((long)*(uint64_t *)(rsi + 104UL) <= (long)var_7) {
        *(unsigned char *)(rdi + 8UL) = (unsigned char)'\x02';
        rax_1 = 0UL;
        return rax_1;
    }
    var_8 = *(unsigned char *)(var_7 + *(uint64_t *)(rsi + 8UL));
    var_9 = (uint64_t)var_8;
    var_10 = (unsigned char *)rdi;
    *var_10 = var_8;
    var_11 = (unsigned char *)(rdi + 10UL);
    *var_11 = (*var_11 & '\x9f');
    var_12 = (uint32_t *)(rsi + 144UL);
    if ((int)*var_12 <= (int)1U) {
        var_13 = *var_6;
        rax_0 = var_13;
        if (var_13 != *(uint64_t *)(rsi + 48UL) & *(uint32_t *)((var_13 << 2UL) + *(uint64_t *)(rsi + 16UL)) != 4294967295U) {
            *(unsigned char *)(rdi + 8UL) = (unsigned char)'\x01';
            *var_11 = (*var_11 | ' ');
            return rax_1;
        }
    }
    rax_1 = 2UL;
    if ((uint64_t)(var_8 + '\xa4') == 0UL) {
        if ((long)(*var_6 + 1UL) >= (long)*(uint64_t *)(rsi + 88UL)) {
            *(unsigned char *)(rdi + 8UL) = (unsigned char)'$';
            return rax_1;
        }
        *(uint64_t *)(var_0 + (-64L)) = 4239484UL;
        var_18 = indirect_placeholder_15(rsi, 1UL);
        var_19 = (uint64_t)(uint32_t)var_18;
        var_20 = (unsigned char)var_18;
        *var_10 = var_20;
        *(unsigned char *)(rdi + 8UL) = (unsigned char)'\x01';
        if ((int)*var_12 > (int)1U) {
            var_21 = *var_6 + 1UL;
            *(uint64_t *)(var_0 + (-72L)) = 4239521UL;
            var_22 = indirect_placeholder_15(rsi, var_21);
            *(uint64_t *)(var_0 + (-80L)) = 4239530UL;
            indirect_placeholder();
            var_23 = (uint32_t)var_22;
            *var_11 = ((*var_11 & '\xbf') | ((((uint64_t)(var_23 + (-95)) == 0UL) || ((uint64_t)var_23 != 0UL)) ? '@' : '\x00'));
        } else {
            *(uint64_t *)(var_0 + (-72L)) = 4239595UL;
            indirect_placeholder();
            *var_11 = ((*var_11 & '\xbf') | ((((uint64_t)(var_20 + '\xa1') == 0UL) || (var_19 != 0UL)) ? '@' : '\x00'));
        }
        var_24 = (uint64_t)((unsigned char)var_19 + '\xd9');
        rax_2 = var_24;
        if (var_24 <= 86UL) {
            return rax_1;
        }
    }
    *(unsigned char *)(rdi + 8UL) = (unsigned char)'\x01';
    if ((int)*var_12 > (int)1U) {
        var_14 = *var_6;
        *(uint64_t *)(var_0 + (-64L)) = 4239319UL;
        var_15 = indirect_placeholder_15(rsi, var_14);
        *(uint64_t *)(var_0 + (-72L)) = 4239329UL;
        indirect_placeholder();
        var_16 = (uint32_t)var_15;
        *var_11 = ((*var_11 & '\xbf') | ((((uint64_t)(var_16 + (-95)) == 0UL) || ((uint64_t)var_16 != 0UL)) ? '@' : '\x00'));
    } else {
        *(uint64_t *)(var_0 + (-64L)) = 4240250UL;
        indirect_placeholder();
        *var_11 = ((*var_11 & '\xbf') | ((((uint64_t)(var_8 + '\xa1') == 0UL) || ((uint64_t)(uint32_t)rax_0 != 0UL)) ? '@' : '\x00'));
    }
    if ((uint64_t)(var_8 & '\xc0') == 0UL) {
        if (var_8 > '\t') {
            return rax_1;
        }
        if ((uint64_t)((var_8 + '\xf6') & '\xfe') > 53UL) {
            return rax_1;
        }
    }
    var_17 = (uint64_t)((uint32_t)var_9 + (-91));
    rax_2 = var_17;
    if ((uint64_t)(unsigned char)var_17 > 34UL) {
        return rax_1;
    }
    if ((uint64_t)(var_8 + '\xa5') > 34UL) {
        return rax_1;
    }
    function_dispatcher((unsigned char *)(0UL));
    return rax_2;
    function_dispatcher((unsigned char *)(0UL));
    return rax_2;
}
