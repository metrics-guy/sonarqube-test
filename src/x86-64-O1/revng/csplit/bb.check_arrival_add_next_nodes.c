typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_163_ret_type;
struct indirect_placeholder_163_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_15(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_16(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_163_ret_type indirect_placeholder_163(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t indirect_placeholder_6(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
uint64_t bb_check_arrival_add_next_nodes(uint64_t rcx, uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t local_sp_5;
    uint64_t rbp_2;
    uint64_t rbp_3;
    uint32_t rax_0_shrunk;
    uint64_t *_pre_phi86;
    uint64_t local_sp_2;
    uint64_t local_sp_0;
    uint64_t r12_3;
    uint64_t r12_2;
    uint64_t rbp_0;
    uint64_t r12_0;
    uint64_t var_50;
    uint64_t local_sp_3;
    uint64_t rbx_0;
    uint32_t var_42;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t rbp_1;
    uint32_t var_33;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t local_sp_1;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint32_t *var_31;
    uint32_t var_32;
    uint64_t r12_1;
    uint64_t *var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t _pre84;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t *var_10;
    uint64_t *var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t *var_14;
    uint64_t var_15;
    uint64_t var_44;
    uint64_t var_17;
    uint64_t *_pre85;
    uint64_t _pre_phi;
    uint64_t local_sp_4;
    uint64_t var_43;
    uint64_t var_45;
    uint64_t var_16;
    uint64_t *var_18;
    uint64_t var_19;
    uint32_t var_20;
    struct indirect_placeholder_163_ret_type var_27;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r13();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    var_7 = var_0 + (-120L);
    *(uint64_t *)var_7 = rsi;
    *(uint64_t *)(var_0 + (-112L)) = rcx;
    var_8 = *(uint64_t *)(rdi + 152UL);
    *(uint32_t *)(var_0 + (-60L)) = 0U;
    *(uint64_t *)(var_0 + (-88L)) = 0UL;
    *(uint64_t *)(var_0 + (-80L)) = 0UL;
    *(uint64_t *)(var_0 + (-72L)) = 0UL;
    var_9 = (uint64_t *)(rdx + 8UL);
    local_sp_5 = var_7;
    rbp_3 = 0UL;
    rax_0_shrunk = 12U;
    r12_3 = var_8;
    local_sp_3 = var_7;
    if ((long)*var_9 > (long)0UL) {
        *(uint64_t *)(local_sp_5 + (-8L)) = 4264447UL;
        indirect_placeholder();
        rax_0_shrunk = 0U;
        return (uint64_t)rax_0_shrunk;
    }
    var_10 = (uint64_t *)(rdx + 16UL);
    var_11 = (uint64_t *)(rdi + 184UL);
    while (1U)
        {
            var_12 = *(uint64_t *)((rbp_3 << 3UL) + *var_10);
            var_13 = var_12 << 4UL;
            var_14 = (uint64_t *)r12_3;
            var_15 = var_13 + *var_14;
            rbp_2 = rbp_3;
            r12_2 = r12_3;
            rbp_0 = rbp_3;
            r12_0 = r12_3;
            rbp_1 = rbp_3;
            r12_1 = r12_3;
            _pre_phi = var_15;
            local_sp_4 = local_sp_3;
            if ((*(unsigned char *)(var_15 + 10UL) & '\x10') == '\x00') {
                _pre85 = (uint64_t *)local_sp_3;
                _pre_phi86 = _pre85;
                var_43 = *_pre_phi86;
                var_44 = local_sp_4 + (-8L);
                *(uint64_t *)var_44 = 4264332UL;
                var_45 = indirect_placeholder_16(var_43, rdi, _pre_phi);
                local_sp_0 = var_44;
                local_sp_2 = var_44;
                if ((uint64_t)(unsigned char)var_45 != 0UL) {
                    var_50 = rbp_0 + 1UL;
                    local_sp_3 = local_sp_0;
                    rbp_3 = var_50;
                    r12_3 = r12_0;
                    local_sp_5 = local_sp_0;
                    if ((long)*var_9 > (long)var_50) {
                        continue;
                    }
                    loop_state_var = 1U;
                    break;
                }
            }
            var_16 = *(uint64_t *)local_sp_3;
            var_17 = local_sp_3 + (-8L);
            var_18 = (uint64_t *)var_17;
            *var_18 = 4264061UL;
            var_19 = indirect_placeholder_6(var_16, rdi, r12_3, var_12);
            var_20 = (uint32_t)var_19;
            local_sp_1 = var_17;
            local_sp_2 = var_17;
            _pre_phi86 = var_18;
            local_sp_4 = var_17;
            if ((int)var_20 <= (int)1U) {
                if ((uint64_t)var_20 != 0UL) {
                    _pre84 = var_13 + *var_14;
                    _pre_phi = _pre84;
                    var_43 = *_pre_phi86;
                    var_44 = local_sp_4 + (-8L);
                    *(uint64_t *)var_44 = 4264332UL;
                    var_45 = indirect_placeholder_16(var_43, rdi, _pre_phi);
                    local_sp_0 = var_44;
                    local_sp_2 = var_44;
                    if ((uint64_t)(unsigned char)var_45 == 0UL) {
                        var_50 = rbp_0 + 1UL;
                        local_sp_3 = local_sp_0;
                        rbp_3 = var_50;
                        r12_3 = r12_0;
                        local_sp_5 = local_sp_0;
                        if ((long)*var_9 > (long)var_50) {
                            continue;
                        }
                        loop_state_var = 1U;
                        break;
                    }
                }
                var_46 = *(uint64_t *)((var_12 << 3UL) + *(uint64_t *)(r12_2 + 24UL));
                var_47 = *(uint64_t *)(local_sp_2 + 8UL);
                var_48 = local_sp_2 + (-8L);
                *(uint64_t *)var_48 = 4264261UL;
                var_49 = indirect_placeholder_15(var_47, var_46);
                local_sp_0 = var_48;
                rbp_0 = rbp_2;
                r12_0 = r12_2;
                if ((uint64_t)(unsigned char)var_49 == 0UL) {
                    var_50 = rbp_0 + 1UL;
                    local_sp_3 = local_sp_0;
                    rbp_3 = var_50;
                    r12_3 = r12_0;
                    local_sp_5 = local_sp_0;
                    if ((long)*var_9 > (long)var_50) {
                        continue;
                    }
                    loop_state_var = 1U;
                    break;
                }
                *(uint64_t *)(local_sp_2 + (-16L)) = 4264417UL;
                indirect_placeholder();
                loop_state_var = 0U;
                break;
            }
            *(uint64_t *)(local_sp_3 + 8UL) = *(uint64_t *)((var_12 << 3UL) + *(uint64_t *)(r12_3 + 24UL));
            var_21 = *var_18 + (uint64_t)((long)(var_19 << 32UL) >> (long)32UL);
            var_22 = var_21 << 3UL;
            *(uint64_t *)(local_sp_3 + 16UL) = var_22;
            var_23 = *(uint64_t *)(var_22 + *var_11);
            *(uint64_t *)(local_sp_3 + 32UL) = 0UL;
            rbx_0 = var_21;
            if (var_23 != 0UL) {
                var_24 = var_23 + 8UL;
                var_25 = local_sp_3 + 24UL;
                var_26 = local_sp_3 + (-16L);
                *(uint64_t *)var_26 = 4264146UL;
                var_27 = indirect_placeholder_163(var_21, var_25, rbp_3, r12_3, var_24);
                var_28 = var_27.field_1;
                var_29 = var_27.field_2;
                var_30 = var_27.field_3;
                var_31 = (uint32_t *)(local_sp_3 + 44UL);
                var_32 = (uint32_t)var_27.field_0;
                *var_31 = var_32;
                local_sp_1 = var_26;
                rbx_0 = var_28;
                rbp_1 = var_29;
                r12_1 = var_30;
                if ((uint64_t)var_32 != 0UL) {
                    *(uint64_t *)(local_sp_3 + (-24L)) = 4264348UL;
                    indirect_placeholder();
                    var_33 = *(uint32_t *)(local_sp_3 + 36UL);
                    rax_0_shrunk = var_33;
                    loop_state_var = 0U;
                    break;
                }
            }
            var_34 = (uint64_t *)(local_sp_1 + 16UL);
            var_35 = *var_34;
            var_36 = local_sp_1 + 32UL;
            *(uint64_t *)(local_sp_1 + (-8L)) = 4264173UL;
            var_37 = indirect_placeholder_15(var_36, var_35);
            rbp_2 = rbp_1;
            r12_2 = r12_1;
            if ((uint64_t)(unsigned char)var_37 != 0UL) {
                *(uint64_t *)(local_sp_1 + (-16L)) = 4264364UL;
                indirect_placeholder();
                loop_state_var = 0U;
                break;
            }
            *(uint64_t *)(local_sp_1 + 8UL) = (*var_34 + *var_11);
            var_38 = local_sp_1 + 24UL;
            var_39 = local_sp_1 + 52UL;
            var_40 = local_sp_1 + (-16L);
            *(uint64_t *)var_40 = 4264216UL;
            var_41 = indirect_placeholder_16(var_38, var_39, r12_1);
            **(uint64_t **)local_sp_1 = var_41;
            local_sp_2 = var_40;
            if (*(uint64_t *)((rbx_0 << 3UL) + *var_11) != 0UL & *(uint32_t *)(local_sp_1 + 44UL) != 0U) {
                *(uint64_t *)(local_sp_1 + (-24L)) = 4264392UL;
                indirect_placeholder();
                var_42 = *(uint32_t *)(local_sp_1 + 36UL);
                rax_0_shrunk = var_42;
                loop_state_var = 0U;
                break;
            }
        }
    switch (loop_state_var) {
      case 0U:
        {
            return (uint64_t)rax_0_shrunk;
        }
        break;
      case 1U:
        {
            break;
        }
        break;
    }
}
