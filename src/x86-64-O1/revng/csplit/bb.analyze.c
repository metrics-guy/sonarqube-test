typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_analyze_ret_type;
struct bb_analyze_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_16(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
struct bb_analyze_ret_type bb_analyze(uint64_t rdi) {
    uint64_t local_sp_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t *var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t *var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t rax_2;
    uint64_t var_42;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_39;
    uint64_t *_pre_phi85;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t rax_0;
    uint64_t rdx_0;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_24;
    uint64_t rax_1;
    uint64_t var_25;
    uint64_t *var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t *var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t *var_23;
    struct bb_analyze_ret_type mrv;
    struct bb_analyze_ret_type mrv1;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r13();
    var_3 = init_rbp();
    var_4 = init_r12();
    var_5 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    var_6 = *(uint64_t *)rdi;
    var_7 = (uint64_t *)(var_6 + 8UL);
    var_8 = *var_7 << 3UL;
    *(uint64_t *)(var_0 + (-32L)) = 4244500UL;
    var_9 = indirect_placeholder_1(var_8);
    var_10 = (uint64_t *)(var_6 + 24UL);
    *var_10 = var_9;
    var_11 = *var_7 << 3UL;
    *(uint64_t *)(var_0 + (-40L)) = 4244521UL;
    var_12 = indirect_placeholder_1(var_11);
    var_13 = (uint64_t *)(var_6 + 32UL);
    *var_13 = var_12;
    var_14 = *var_7 * 24UL;
    *(uint64_t *)(var_0 + (-48L)) = 4244542UL;
    var_15 = indirect_placeholder_1(var_14);
    var_16 = (uint64_t *)(var_6 + 40UL);
    *var_16 = var_15;
    var_17 = *var_7 * 24UL;
    *(uint64_t *)(var_0 + (-56L)) = 4244563UL;
    var_18 = indirect_placeholder_1(var_17);
    *(uint64_t *)(var_6 + 48UL) = var_18;
    rax_0 = 0UL;
    rax_1 = 0UL;
    rax_2 = 12UL;
    if (*var_10 == 0UL) {
        mrv.field_0 = rax_2;
        mrv1 = mrv;
        mrv1.field_1 = var_4;
        return mrv1;
    }
    if (~(*var_13 != 0UL & !((*var_16 == 0UL) || (var_18 == 0UL)))) {
        return;
    }
    var_19 = (uint64_t *)(rdi + 48UL);
    var_20 = *var_19 << 3UL;
    var_21 = var_0 + (-64L);
    *(uint64_t *)var_21 = 4244626UL;
    var_22 = indirect_placeholder_1(var_20);
    var_23 = (uint64_t *)(var_6 + 224UL);
    *var_23 = var_22;
    var_24 = var_22;
    local_sp_0 = var_21;
    if (var_22 == 0UL) {
        _pre_phi85 = (uint64_t *)(var_6 + 104UL);
    } else {
        if (*var_19 == 0UL) {
            *(uint64_t *)((rax_1 << 3UL) + var_24) = rax_1;
            var_25 = rax_1 + 1UL;
            rax_1 = var_25;
            while (*var_19 <= var_25)
                {
                    var_24 = *var_23;
                    *(uint64_t *)((rax_1 << 3UL) + var_24) = rax_1;
                    var_25 = rax_1 + 1UL;
                    rax_1 = var_25;
                }
        }
        var_26 = (uint64_t *)(var_6 + 104UL);
        var_27 = *var_26;
        var_28 = var_0 + (-72L);
        *(uint64_t *)var_28 = 4244688UL;
        indirect_placeholder_16(var_6, var_27, 4227559UL);
        var_29 = *var_19;
        _pre_phi85 = var_26;
        local_sp_0 = var_28;
        if (var_29 == 0UL) {
            var_31 = var_0 + (-80L);
            *(uint64_t *)var_31 = 4244927UL;
            indirect_placeholder();
            *var_23 = 0UL;
            local_sp_0 = var_31;
        } else {
            rdx_0 = rax_0;
            while (*(uint64_t *)((rax_0 << 3UL) + *var_23) != rax_0)
                {
                    var_30 = rax_0 + 1UL;
                    rax_0 = var_30;
                    rdx_0 = var_29;
                    if (var_29 == var_30) {
                        break;
                    }
                    rdx_0 = rax_0;
                }
            if (var_29 == rdx_0) {
                var_31 = var_0 + (-80L);
                *(uint64_t *)var_31 = 4244927UL;
                indirect_placeholder();
                *var_23 = 0UL;
                local_sp_0 = var_31;
            }
        }
    }
    var_32 = *_pre_phi85;
    *(uint64_t *)(local_sp_0 + (-8L)) = 4244763UL;
    var_33 = indirect_placeholder_16(rdi, var_32, 4231734UL);
    rax_2 = var_33;
    var_34 = *_pre_phi85;
    *(uint64_t *)(local_sp_0 + (-16L)) = 4244788UL;
    var_35 = indirect_placeholder_16(var_6, var_34, 4242825UL);
    rax_2 = var_35;
    var_36 = *_pre_phi85;
    *(uint64_t *)(local_sp_0 + (-24L)) = 4244809UL;
    indirect_placeholder_16(var_6, var_36, 4227698UL);
    var_37 = *_pre_phi85;
    *(uint64_t *)(local_sp_0 + (-32L)) = 4244826UL;
    var_38 = indirect_placeholder_16(var_6, var_37, 4251643UL);
    rax_2 = var_38;
    *(uint64_t *)(local_sp_0 + (-40L)) = 4244838UL;
    var_39 = indirect_placeholder_1(var_6);
    rax_2 = var_39;
    if ((uint64_t)(uint32_t)var_33 != 0UL & (uint64_t)(uint32_t)var_35 != 0UL & (uint64_t)(uint32_t)var_38 != 0UL & (uint64_t)(uint32_t)var_39 != 0UL) {
        rax_2 = 12UL;
        if ((*(unsigned char *)(rdi + 56UL) & '\x10') == '\x00') {
            if (*var_19 != 0UL) {
                rax_2 = var_39;
                if (*(uint64_t *)(var_6 + 152UL) == 0UL) {
                    mrv.field_0 = rax_2;
                    mrv1 = mrv;
                    mrv1.field_1 = var_4;
                    return mrv1;
                }
            }
            rax_2 = var_39;
            if ((*(unsigned char *)(var_6 + 176UL) & '\x01') != '\x00' & *(uint64_t *)(var_6 + 152UL) == 0UL) {
                mrv.field_0 = rax_2;
                mrv1 = mrv;
                mrv1.field_1 = var_4;
                return mrv1;
            }
        }
        rax_2 = var_39;
        if (*(uint64_t *)(var_6 + 152UL) == 0UL) {
            mrv.field_0 = rax_2;
            mrv1 = mrv;
            mrv1.field_1 = var_4;
            return mrv1;
        }
        var_40 = *(uint64_t *)(var_6 + 16UL) * 24UL;
        *(uint64_t *)(local_sp_0 + (-48L)) = 4244891UL;
        var_41 = indirect_placeholder_1(var_40);
        *(uint64_t *)(var_6 + 56UL) = var_41;
        if (var_41 == 0UL) {
            *(uint64_t *)(local_sp_0 + (-56L)) = 4244908UL;
            var_42 = indirect_placeholder_7(var_6, var_2, var_6, rdi, var_4, var_5);
            rax_2 = var_42;
        }
    }
}
