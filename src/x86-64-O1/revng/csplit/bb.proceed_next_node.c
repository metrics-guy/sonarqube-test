typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_15(uint64_t param_0, uint64_t param_1);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_16(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern uint64_t indirect_placeholder_6(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
uint64_t bb_proceed_next_node(uint64_t rcx, uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t r9, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t *var_8;
    uint64_t *var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t *_cast2;
    uint64_t var_12;
    unsigned char var_13;
    uint64_t var_53;
    uint64_t *var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t *var_57;
    uint64_t var_58;
    uint64_t *_pre_phi116;
    uint64_t r12_3;
    uint64_t rbx_0;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t local_sp_0;
    uint64_t r12_0;
    uint64_t r12_1;
    uint64_t var_33;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t storemerge;
    uint64_t r12_2;
    uint64_t var_20;
    uint64_t *var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t *_pre_phi;
    uint64_t var_59;
    uint64_t *_pre_phi114;
    uint64_t *_pre;
    uint64_t *_pre115;
    uint64_t local_sp_1;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t *var_62;
    uint64_t var_63;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t *var_44;
    uint64_t local_sp_2;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t *var_47;
    uint64_t *_pre_phi113;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t *var_50;
    uint64_t local_sp_3;
    uint64_t r12_2_in_in;
    uint64_t local_sp_4;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t *var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t *var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t *var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r13();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    var_7 = var_0 + (-88L);
    *(uint64_t *)(var_0 + (-64L)) = rsi;
    var_8 = (uint64_t *)(var_0 + (-72L));
    *var_8 = rdx;
    var_9 = (uint64_t *)(var_0 + (-80L));
    *var_9 = r9;
    var_10 = *(uint64_t *)(rdi + 152UL);
    var_11 = r8 << 4UL;
    _cast2 = (uint64_t *)var_10;
    var_12 = var_11 + *_cast2;
    var_13 = *(unsigned char *)(var_12 + 8UL);
    r12_3 = 18446744073709551615UL;
    rbx_0 = 0UL;
    r12_1 = 18446744073709551615UL;
    local_sp_1 = var_7;
    local_sp_2 = var_7;
    if (((uint64_t)var_13 & 8UL) != 0UL) {
        var_14 = (uint64_t *)rcx;
        *(uint64_t *)var_7 = (*(uint64_t *)((*var_14 << 3UL) + *(uint64_t *)(rdi + 184UL)) + 8UL);
        var_15 = (r8 * 24UL) + *(uint64_t *)(var_10 + 40UL);
        var_16 = var_0 + (-96L);
        *(uint64_t *)var_16 = 4271400UL;
        var_17 = indirect_placeholder_15(r9, r8);
        local_sp_0 = var_16;
        r12_3 = 18446744073709551614UL;
        var_18 = *(uint64_t *)(var_15 + 8UL);
        var_19 = helper_cc_compute_all_wrapper(var_18, 0UL, 0UL, 25U);
        r12_3 = 18446744073709551615UL;
        if ((uint64_t)(unsigned char)var_17 != 0UL & (uint64_t)(((unsigned char)(var_19 >> 4UL) ^ (unsigned char)var_19) & '\xc0') != 0UL) {
            while (1U)
                {
                    var_20 = *(uint64_t *)((rbx_0 << 3UL) + *(uint64_t *)(var_15 + 16UL));
                    var_21 = (uint64_t *)local_sp_0;
                    var_22 = *var_21;
                    var_23 = local_sp_0 + (-8L);
                    *(uint64_t *)var_23 = 4271553UL;
                    var_24 = indirect_placeholder_15(var_22, var_20);
                    r12_0 = r12_1;
                    local_sp_0 = var_23;
                    r12_3 = var_20;
                    if (var_24 == 0UL) {
                        var_33 = rbx_0 + 1UL;
                        rbx_0 = var_33;
                        r12_1 = r12_0;
                        r12_3 = r12_0;
                        if (var_18 != var_33) {
                            continue;
                        }
                        break;
                    }
                    r12_0 = var_20;
                    if (r12_1 != 18446744073709551615UL) {
                        var_25 = *var_21;
                        *(uint64_t *)(local_sp_0 + (-16L)) = 4271455UL;
                        var_26 = indirect_placeholder_15(var_25, r12_1);
                        if (var_26 == 0UL) {
                            break;
                        }
                        var_27 = *(uint64_t *)(local_sp_0 + 80UL);
                        r12_3 = r12_1;
                        if (var_27 == 0UL) {
                            break;
                        }
                        var_28 = *var_21;
                        var_29 = *(uint64_t *)(local_sp_0 + 8UL);
                        var_30 = *var_14;
                        *(uint64_t *)(local_sp_0 + (-24L)) = 4271506UL;
                        var_31 = indirect_placeholder_7(var_29, var_20, var_27, var_30, var_25, var_28);
                        var_32 = ((uint64_t)(uint32_t)var_31 == 0UL) ? r12_1 : 18446744073709551614UL;
                        r12_3 = var_32;
                        break;
                    }
                }
        }
        return r12_3;
    }
    if ((*(unsigned char *)(var_12 + 10UL) & '\x10') == '\x00') {
        r12_3 = 18446744073709551614UL;
        if ((uint64_t)(var_13 + '\xfc') == 0UL) {
            var_40 = ((*(uint64_t *)var_12 << 4UL) + 16UL) + *var_8;
            var_41 = *(uint64_t *)(var_40 + 8UL);
            var_42 = *(uint64_t *)var_40;
            var_43 = var_41 - var_42;
            var_44 = (uint64_t *)var_7;
            *var_44 = var_43;
            _pre_phi113 = var_44;
            if (*(uint64_t *)(var_0 | 8UL) == 0UL) {
                if (var_43 != 0UL) {
                    var_48 = (r8 << 3UL) + *(uint64_t *)(var_10 + 24UL);
                    var_49 = *_pre_phi113;
                    var_50 = (uint64_t *)rcx;
                    _pre_phi = var_50;
                    local_sp_3 = local_sp_2;
                    r12_2_in_in = var_48;
                    storemerge = var_49 + *var_50;
                    r12_2 = *(uint64_t *)r12_2_in_in;
                    *_pre_phi = storemerge;
                    local_sp_4 = local_sp_3;
                    r12_3 = r12_2;
                    if (*(uint64_t *)(local_sp_3 + 96UL) != 0UL) {
                        r12_3 = 18446744073709551615UL;
                        if ((long)*(uint64_t *)(rdi + 168UL) < (long)storemerge) {
                            return r12_3;
                        }
                        var_66 = *(uint64_t *)((storemerge << 3UL) + *(uint64_t *)(rdi + 184UL));
                        if (var_66 == 0UL) {
                            return r12_3;
                        }
                        var_67 = var_66 + 8UL;
                        var_68 = local_sp_3 + (-8L);
                        *(uint64_t *)var_68 = 4271315UL;
                        var_69 = indirect_placeholder_15(var_67, r12_2);
                        local_sp_4 = var_68;
                        if (var_69 == 0UL) {
                            return r12_3;
                        }
                    }
                    *(uint64_t *)(*(uint64_t *)(local_sp_4 + 8UL) + 8UL) = 0UL;
                    return r12_3;
                }
            }
            if ((var_41 == 18446744073709551615UL) || (var_42 == 18446744073709551615UL)) {
                return r12_3;
            }
            if (var_43 == 0UL) {
                var_51 = *var_9;
                *(uint64_t *)(var_0 + (-96L)) = 4271742UL;
                var_52 = indirect_placeholder_15(var_51, r8);
                if ((uint64_t)(unsigned char)var_52 == 0UL) {
                    return r12_3;
                }
                var_53 = **(uint64_t **)(((r8 * 24UL) + *(uint64_t *)(var_10 + 40UL)) + 16UL);
                var_54 = (uint64_t *)rcx;
                var_55 = *(uint64_t *)((*var_54 << 3UL) + *(uint64_t *)(rdi + 184UL)) + 8UL;
                var_56 = var_0 + (-104L);
                var_57 = (uint64_t *)var_56;
                *var_57 = 4271793UL;
                var_58 = indirect_placeholder_15(var_55, var_53);
                _pre_phi116 = var_57;
                _pre_phi114 = var_54;
                local_sp_1 = var_56;
                r12_3 = var_53;
                if (var_58 == 0UL) {
                    return r12_3;
                }
            }
            var_45 = *(uint64_t *)(rdi + 8UL);
            var_46 = var_0 + (-96L);
            var_47 = (uint64_t *)var_46;
            *var_47 = 4271706UL;
            indirect_placeholder();
            _pre_phi113 = var_47;
            local_sp_2 = var_46;
            if ((uint64_t)(uint32_t)var_45 == 0UL) {
                return r12_3;
            }
            var_48 = (r8 << 3UL) + *(uint64_t *)(var_10 + 24UL);
            var_49 = *_pre_phi113;
            var_50 = (uint64_t *)rcx;
            _pre_phi = var_50;
            local_sp_3 = local_sp_2;
            r12_2_in_in = var_48;
            storemerge = var_49 + *var_50;
            r12_2 = *(uint64_t *)r12_2_in_in;
            *_pre_phi = storemerge;
            local_sp_4 = local_sp_3;
            r12_3 = r12_2;
            if (*(uint64_t *)(local_sp_3 + 96UL) != 0UL) {
                r12_3 = 18446744073709551615UL;
                if ((long)*(uint64_t *)(rdi + 168UL) < (long)storemerge) {
                    return r12_3;
                }
                var_66 = *(uint64_t *)((storemerge << 3UL) + *(uint64_t *)(rdi + 184UL));
                if (var_66 == 0UL) {
                    return r12_3;
                }
                var_67 = var_66 + 8UL;
                var_68 = local_sp_3 + (-8L);
                *(uint64_t *)var_68 = 4271315UL;
                var_69 = indirect_placeholder_15(var_67, r12_2);
                local_sp_4 = var_68;
                if (var_69 == 0UL) {
                    return r12_3;
                }
            }
            *(uint64_t *)(*(uint64_t *)(local_sp_4 + 8UL) + 8UL) = 0UL;
            return r12_3;
        }
        _pre = (uint64_t *)rcx;
        _pre115 = (uint64_t *)var_7;
        _pre_phi116 = _pre115;
        _pre_phi114 = _pre;
        var_59 = *_pre_phi114;
        *_pre_phi116 = var_59;
        var_60 = var_11 + *_cast2;
        var_61 = local_sp_1 + (-8L);
        var_62 = (uint64_t *)var_61;
        *var_62 = 4271234UL;
        var_63 = indirect_placeholder_16(var_59, rdi, var_60);
        _pre_phi = _pre_phi114;
        local_sp_3 = var_61;
        if ((uint64_t)(unsigned char)var_63 == 0UL) {
            return r12_3;
        }
        var_64 = (r8 << 3UL) + *(uint64_t *)(var_10 + 24UL);
        var_65 = *var_62 + 1UL;
        r12_2_in_in = var_64;
        storemerge = var_65;
    } else {
        var_34 = (uint64_t *)rcx;
        var_35 = *var_34;
        var_36 = var_0 + (-96L);
        var_37 = (uint64_t *)var_36;
        *var_37 = 4271586UL;
        var_38 = indirect_placeholder_6(var_35, rdi, var_10, r8);
        var_39 = var_38 << 32UL;
        *var_37 = (uint64_t)((long)var_39 >> (long)32UL);
        _pre_phi116 = var_37;
        _pre_phi114 = var_34;
        local_sp_1 = var_36;
        local_sp_2 = var_36;
        _pre_phi113 = var_37;
        if (var_39 == 0UL) {
            var_48 = (r8 << 3UL) + *(uint64_t *)(var_10 + 24UL);
            var_49 = *_pre_phi113;
            var_50 = (uint64_t *)rcx;
            _pre_phi = var_50;
            local_sp_3 = local_sp_2;
            r12_2_in_in = var_48;
            storemerge = var_49 + *var_50;
        } else {
            var_59 = *_pre_phi114;
            *_pre_phi116 = var_59;
            var_60 = var_11 + *_cast2;
            var_61 = local_sp_1 + (-8L);
            var_62 = (uint64_t *)var_61;
            *var_62 = 4271234UL;
            var_63 = indirect_placeholder_16(var_59, rdi, var_60);
            _pre_phi = _pre_phi114;
            local_sp_3 = var_61;
            if ((uint64_t)(unsigned char)var_63 != 0UL) {
                return r12_3;
            }
            var_64 = (r8 << 3UL) + *(uint64_t *)(var_10 + 24UL);
            var_65 = *var_62 + 1UL;
            r12_2_in_in = var_64;
            storemerge = var_65;
        }
        r12_2 = *(uint64_t *)r12_2_in_in;
        *_pre_phi = storemerge;
        local_sp_4 = local_sp_3;
        r12_3 = r12_2;
        if (*(uint64_t *)(local_sp_3 + 96UL) != 0UL) {
            r12_3 = 18446744073709551615UL;
            if ((long)*(uint64_t *)(rdi + 168UL) < (long)storemerge) {
                return r12_3;
            }
            var_66 = *(uint64_t *)((storemerge << 3UL) + *(uint64_t *)(rdi + 184UL));
            if (var_66 == 0UL) {
                return r12_3;
            }
            var_67 = var_66 + 8UL;
            var_68 = local_sp_3 + (-8L);
            *(uint64_t *)var_68 = 4271315UL;
            var_69 = indirect_placeholder_15(var_67, r12_2);
            local_sp_4 = var_68;
            if (var_69 != 0UL) {
                return r12_3;
            }
        }
        *(uint64_t *)(*(uint64_t *)(local_sp_4 + 8UL) + 8UL) = 0UL;
    }
}
