typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_53_ret_type;
struct indirect_placeholder_54_ret_type;
struct indirect_placeholder_55_ret_type;
struct indirect_placeholder_52_ret_type;
struct indirect_placeholder_57_ret_type;
struct indirect_placeholder_58_ret_type;
struct indirect_placeholder_59_ret_type;
struct indirect_placeholder_60_ret_type;
struct indirect_placeholder_56_ret_type;
struct indirect_placeholder_61_ret_type;
struct indirect_placeholder_62_ret_type;
struct indirect_placeholder_63_ret_type;
struct indirect_placeholder_53_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_54_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
};
struct indirect_placeholder_55_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_52_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_57_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_58_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_59_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
};
struct indirect_placeholder_60_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_56_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_61_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_62_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_63_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern void indirect_placeholder_26(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern void indirect_placeholder_2(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_6(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern void indirect_placeholder_28(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_53_ret_type indirect_placeholder_53(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_54_ret_type indirect_placeholder_54(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_55_ret_type indirect_placeholder_55(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_52_ret_type indirect_placeholder_52(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_57_ret_type indirect_placeholder_57(uint64_t param_0);
extern struct indirect_placeholder_58_ret_type indirect_placeholder_58(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_59_ret_type indirect_placeholder_59(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_60_ret_type indirect_placeholder_60(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_56_ret_type indirect_placeholder_56(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_61_ret_type indirect_placeholder_61(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_62_ret_type indirect_placeholder_62(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_63_ret_type indirect_placeholder_63(uint64_t param_0, uint64_t param_1, uint64_t param_2);
void bb_process_regexp(uint64_t rdi, uint64_t r14, uint64_t rsi) {
    uint64_t r13_2;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    unsigned char var_6;
    uint64_t var_7;
    bool var_8;
    uint64_t local_sp_5;
    uint64_t r12_0;
    uint64_t rbp_0;
    uint64_t local_sp_2;
    struct indirect_placeholder_53_ret_type var_57;
    uint64_t rbp_2;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    struct indirect_placeholder_55_ret_type var_51;
    uint64_t local_sp_8;
    uint64_t r142_1;
    uint64_t local_sp_1;
    uint64_t local_sp_0;
    uint64_t rbx_0;
    uint64_t rbp_1;
    uint64_t var_63;
    uint64_t rbx_1;
    uint64_t rbp_3;
    uint64_t var_40;
    uint64_t var_58;
    uint64_t var_59;
    struct indirect_placeholder_52_ret_type var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t r13_1;
    uint64_t var_29;
    uint64_t var_30;
    struct indirect_placeholder_57_ret_type var_31;
    uint64_t local_sp_7_be;
    uint64_t rbp_5_be;
    uint64_t r12_3_be;
    uint64_t local_sp_7;
    uint64_t r12_3;
    uint64_t var_25;
    uint64_t r142_2;
    struct indirect_placeholder_58_ret_type var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t *_pre_phi;
    uint64_t r142_0;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    struct indirect_placeholder_60_ret_type var_24;
    uint64_t local_sp_4;
    uint64_t local_sp_3;
    uint64_t rbx_2;
    uint64_t rbx_3;
    uint64_t rbp_4;
    uint64_t var_35;
    uint64_t var_36;
    struct indirect_placeholder_56_ret_type var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t r12_1;
    uint64_t local_sp_6;
    uint64_t var_11;
    struct indirect_placeholder_61_ret_type var_12;
    uint64_t r13_0;
    uint64_t r12_2;
    uint64_t var_13;
    uint64_t *_cast;
    uint64_t rbp_5;
    uint64_t var_14;
    uint64_t var_15;
    struct indirect_placeholder_62_ret_type var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t r142_3;
    uint64_t var_41;
    uint64_t var_42;
    struct indirect_placeholder_63_ret_type var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_9;
    uint64_t var_10;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r13();
    var_3 = init_rbp();
    var_4 = init_r12();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_1;
    var_5 = var_0 + (-40L);
    var_6 = *(unsigned char *)(rdi + 29UL);
    var_7 = (uint64_t)var_6;
    var_8 = (var_6 == '\x00');
    local_sp_5 = var_5;
    r142_1 = r14;
    rbx_0 = var_7;
    rbx_1 = var_7;
    rbx_2 = var_7;
    rbx_3 = var_7;
    r12_1 = rdi;
    r13_0 = rsi;
    if (var_8) {
        var_9 = var_0 + (-48L);
        *(uint64_t *)var_9 = 4210024UL;
        var_10 = indirect_placeholder_1(rdi);
        local_sp_5 = var_9;
        r12_1 = var_10;
    }
    local_sp_6 = local_sp_5;
    r12_2 = r12_1;
    if (*(unsigned char *)4359344UL != '\x00' & *(uint64_t *)4359424UL == 0UL) {
        var_11 = local_sp_5 + (-8L);
        *(uint64_t *)var_11 = 4210034UL;
        var_12 = indirect_placeholder_61(rsi, var_3, r12_1, r14);
        local_sp_6 = var_11;
        r13_0 = var_12.field_1;
        r12_2 = var_12.field_3;
        r142_1 = var_12.field_4;
    }
    var_13 = r12_2 + 32UL;
    _cast = (uint64_t *)r12_2;
    r13_2 = r13_0;
    r12_0 = r12_2;
    rbp_2 = var_13;
    local_sp_8 = local_sp_6;
    rbp_1 = var_13;
    r13_1 = r13_0;
    local_sp_7 = local_sp_6;
    r12_3 = r12_2;
    r142_2 = r142_1;
    _pre_phi = _cast;
    rbp_5 = var_13;
    r142_3 = r142_1;
    if ((long)*_cast >= (long)0UL) {
        while (1U)
            {
                var_41 = *(uint64_t *)4359424UL + 1UL;
                *(uint64_t *)4359424UL = var_41;
                var_42 = local_sp_8 + (-8L);
                *(uint64_t *)var_42 = 4210140UL;
                var_43 = indirect_placeholder_63(r13_2, var_41, r142_3);
                var_44 = var_43.field_0;
                var_45 = var_43.field_1;
                var_46 = var_43.field_2;
                r13_2 = var_45;
                local_sp_1 = var_42;
                local_sp_0 = var_42;
                r142_0 = var_46;
                r142_3 = var_46;
                if (var_44 == 0UL) {
                    var_47 = *(uint64_t *)var_44;
                    var_48 = *(uint64_t *)(var_44 + 8UL);
                    var_49 = var_47 + (*(unsigned char *)((var_47 + var_48) + (-1L)) == '\n');
                    var_50 = local_sp_8 + (-16L);
                    *(uint64_t *)var_50 = 4210187UL;
                    var_51 = indirect_placeholder_55(0UL, var_49, var_13, var_48, 0UL, var_49);
                    local_sp_2 = var_50;
                    local_sp_8 = var_50;
                    switch_state_var = 0;
                    switch (var_51.field_0) {
                      case 18446744073709551615UL:
                        {
                            continue;
                        }
                        break;
                      case 18446744073709551614UL:
                        {
                            var_52 = var_51.field_1;
                            var_53 = var_51.field_2;
                            var_54 = var_51.field_3;
                            *(uint64_t *)(local_sp_8 + (-24L)) = 4210328UL;
                            indirect_placeholder_54(0UL, var_52, 4318728UL, 0UL, 0UL, var_53, var_54);
                            *(uint64_t *)(local_sp_8 + (-32L)) = 4210333UL;
                            indirect_placeholder_2(var_7, var_13);
                            abort();
                        }
                        break;
                      default:
                        {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
                if (*(unsigned char *)(r12_2 + 28UL) == '\x00') {
                    *(uint64_t *)(local_sp_8 + (-16L)) = 4210303UL;
                    indirect_placeholder_26(var_7, var_7, r12_2, var_13, r12_2, var_45);
                    abort();
                }
                if (!var_8) {
                    loop_state_var = 1U;
                    break;
                }
                var_63 = local_sp_0 + (-8L);
                *(uint64_t *)var_63 = 4210277UL;
                indirect_placeholder();
                local_sp_1 = var_63;
                rbx_1 = rbx_0;
                rbp_2 = rbp_1;
                loop_state_var = 1U;
                break;
            }
        switch (loop_state_var) {
          case 1U:
            {
                while (1U)
                    {
                        *(uint64_t *)(local_sp_1 + (-8L)) = 4210282UL;
                        var_58 = indirect_placeholder_6(var_45, rbp_2, r12_2, var_46);
                        var_59 = local_sp_1 + (-16L);
                        *(uint64_t *)var_59 = 4210287UL;
                        var_60 = indirect_placeholder_52(rbx_1, var_58);
                        var_61 = var_60.field_0;
                        var_62 = var_60.field_1;
                        local_sp_0 = var_59;
                        rbx_0 = var_61;
                        rbp_1 = var_62;
                        var_63 = local_sp_0 + (-8L);
                        *(uint64_t *)var_63 = 4210277UL;
                        indirect_placeholder();
                        local_sp_1 = var_63;
                        rbx_1 = rbx_0;
                        rbp_2 = rbp_1;
                        continue;
                    }
            }
            break;
          case 0U:
            {
                var_55 = *(uint64_t *)4359424UL + *_pre_phi;
                var_56 = (uint64_t)*(uint32_t *)(r12_0 + 24UL);
                *(uint64_t *)(local_sp_2 + (-8L)) = 4210226UL;
                indirect_placeholder_28(var_56, var_55, r142_0, var_7);
                rbp_0 = var_55;
                if (var_8) {
                    *(uint64_t *)(local_sp_2 + (-16L)) = 4210338UL;
                    var_57 = indirect_placeholder_53(var_7, var_55);
                    rbp_0 = var_57.field_1;
                }
                if ((long)*_pre_phi > (long)0UL) {
                    *(uint64_t *)4359424UL = rbp_0;
                }
                return;
            }
            break;
        }
    }
    while (1U)
        {
            var_14 = *(uint64_t *)4359424UL + 1UL;
            *(uint64_t *)4359424UL = var_14;
            var_15 = local_sp_7 + (-8L);
            *(uint64_t *)var_15 = 4209937UL;
            var_16 = indirect_placeholder_62(r13_1, var_14, r142_2);
            var_17 = var_16.field_0;
            var_18 = var_16.field_1;
            var_19 = var_16.field_2;
            r12_0 = r12_3;
            rbp_3 = rbp_5;
            r142_0 = var_19;
            local_sp_4 = var_15;
            local_sp_3 = var_15;
            rbp_4 = rbp_5;
            if (var_17 != 0UL) {
                if (*(unsigned char *)(r12_3 + 28UL) != '\x00') {
                    if (!var_8) {
                        loop_state_var = 2U;
                        break;
                    }
                    loop_state_var = 0U;
                    break;
                }
                *(uint64_t *)(local_sp_7 + (-16L)) = 4210087UL;
                indirect_placeholder_26(var_7, var_7, r12_3, rbp_5, r12_3, var_18);
                abort();
            }
            var_20 = *(uint64_t *)var_17;
            var_21 = *(uint64_t *)(var_17 + 8UL);
            var_22 = var_20 + (*(unsigned char *)((var_20 + var_21) + (-1L)) == '\n');
            var_23 = local_sp_7 + (-16L);
            *(uint64_t *)var_23 = 4209984UL;
            var_24 = indirect_placeholder_60(0UL, var_22, rbp_5, var_21, 0UL, var_22);
            local_sp_2 = var_23;
            switch_state_var = 0;
            switch (var_24.field_0) {
              case 18446744073709551614UL:
                {
                    var_32 = var_24.field_1;
                    var_33 = var_24.field_2;
                    var_34 = var_24.field_3;
                    *(uint64_t *)(local_sp_7 + (-24L)) = 4210112UL;
                    indirect_placeholder_59(0UL, var_32, 4318728UL, 0UL, 0UL, var_33, var_34);
                    *(uint64_t *)(local_sp_7 + (-32L)) = 4210117UL;
                    indirect_placeholder_2(var_7, rbp_5);
                    abort();
                }
                break;
              case 18446744073709551615UL:
                {
                    var_25 = local_sp_7 + (-24L);
                    *(uint64_t *)var_25 = 4210005UL;
                    var_26 = indirect_placeholder_58(var_18, rbp_5, r12_3, var_19);
                    var_27 = var_26.field_1;
                    var_28 = var_26.field_4;
                    local_sp_7_be = var_25;
                    r13_1 = var_27;
                    r142_2 = var_28;
                    if (var_8) {
                        var_29 = var_26.field_0;
                        var_30 = local_sp_7 + (-32L);
                        *(uint64_t *)var_30 = 4210017UL;
                        var_31 = indirect_placeholder_57(var_29);
                        local_sp_7_be = var_30;
                        rbp_5_be = var_31.field_0;
                        r12_3_be = var_31.field_1;
                    } else {
                        rbp_5_be = var_26.field_2;
                        r12_3_be = var_26.field_3;
                    }
                    local_sp_7 = local_sp_7_be;
                    rbp_5 = rbp_5_be;
                    r12_3 = r12_3_be;
                    continue;
                }
                break;
              default:
                {
                    _pre_phi = (uint64_t *)r12_3;
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 1U:
        {
            break;
        }
        break;
      case 2U:
      case 0U:
        {
            switch (loop_state_var) {
              case 2U:
                {
                    while (1U)
                        {
                            *(uint64_t *)(local_sp_4 + (-8L)) = 4210066UL;
                            var_35 = indirect_placeholder_6(var_18, rbp_4, r12_3, var_19);
                            var_36 = local_sp_4 + (-16L);
                            *(uint64_t *)var_36 = 4210071UL;
                            var_37 = indirect_placeholder_56(rbx_3, var_35);
                            var_38 = var_37.field_0;
                            var_39 = var_37.field_1;
                            local_sp_3 = var_36;
                            rbx_2 = var_38;
                            rbp_3 = var_39;
                            var_40 = local_sp_3 + (-8L);
                            *(uint64_t *)var_40 = 4210061UL;
                            indirect_placeholder();
                            local_sp_4 = var_40;
                            rbx_3 = rbx_2;
                            rbp_4 = rbp_3;
                            continue;
                        }
                }
                break;
              case 0U:
                {
                    var_40 = local_sp_3 + (-8L);
                    *(uint64_t *)var_40 = 4210061UL;
                    indirect_placeholder();
                    local_sp_4 = var_40;
                    rbx_3 = rbx_2;
                    rbp_4 = rbp_3;
                }
                break;
            }
        }
        break;
    }
}
