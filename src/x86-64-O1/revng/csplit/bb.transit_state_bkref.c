typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_transit_state_bkref_ret_type;
struct indirect_placeholder_171_ret_type;
struct indirect_placeholder_172_ret_type;
struct indirect_placeholder_174_ret_type;
struct indirect_placeholder_173_ret_type;
struct indirect_placeholder_175_ret_type;
struct indirect_placeholder_176_ret_type;
struct bb_transit_state_bkref_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_171_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_172_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_174_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_173_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_175_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_176_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_16(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_r10(void);
extern uint64_t init_r9(void);
extern uint64_t init_rcx(void);
extern uint64_t init_r8(void);
extern struct indirect_placeholder_171_ret_type indirect_placeholder_171(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_172_ret_type indirect_placeholder_172(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_174_ret_type indirect_placeholder_174(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_173_ret_type indirect_placeholder_173(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_175_ret_type indirect_placeholder_175(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_176_ret_type indirect_placeholder_176(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
typedef _Bool bool;
struct bb_transit_state_bkref_ret_type bb_transit_state_bkref(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    struct indirect_placeholder_173_ret_type var_98;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t local_sp_6;
    uint64_t rcx_0;
    uint64_t var_101;
    struct indirect_placeholder_171_ret_type var_102;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t var_107;
    uint32_t *var_108;
    uint32_t var_109;
    uint64_t local_sp_2;
    uint64_t local_sp_7;
    uint64_t var_92;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    struct indirect_placeholder_172_ret_type var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_69;
    uint64_t r10_6;
    uint64_t r9_6;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint32_t *var_67;
    uint32_t var_68;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    struct indirect_placeholder_174_ret_type var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t local_sp_0;
    uint64_t r10_0;
    uint64_t r9_0;
    uint64_t r8_0;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t var_97;
    uint32_t *var_99;
    uint32_t var_100;
    uint64_t rcx_4;
    uint64_t local_sp_5;
    uint64_t r9_4;
    uint64_t rcx_2;
    uint64_t local_sp_1;
    uint64_t r10_2;
    uint64_t rcx_1;
    uint64_t r13_0;
    uint64_t r8_4;
    uint64_t r9_2;
    uint64_t r10_1;
    uint64_t r8_2;
    uint64_t r9_1;
    uint64_t r8_1;
    uint64_t var_42;
    uint64_t *var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t *var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    struct indirect_placeholder_175_ret_type var_52;
    uint64_t var_53;
    uint32_t *var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t *var_61;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t local_sp_4;
    uint64_t *var_15;
    uint32_t *var_16;
    uint64_t *var_17;
    uint64_t *var_18;
    uint64_t *var_19;
    uint64_t *var_20;
    uint64_t *var_21;
    uint64_t *var_22;
    uint64_t var_23;
    uint64_t rcx_5;
    uint64_t local_sp_3;
    uint64_t r14_1;
    uint64_t rcx_3;
    uint64_t r14_0;
    uint64_t r10_3;
    uint64_t r9_3;
    uint64_t r8_3;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint32_t *var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    struct indirect_placeholder_176_ret_type var_31;
    uint64_t var_32;
    uint32_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint32_t *var_40;
    uint32_t var_41;
    uint64_t r10_4;
    uint64_t var_112;
    uint64_t var_113;
    uint64_t rcx_6;
    uint64_t r10_5;
    uint64_t r9_5;
    uint64_t r8_5;
    uint64_t rcx_7;
    uint64_t r8_6;
    struct bb_transit_state_bkref_ret_type mrv;
    struct bb_transit_state_bkref_ret_type mrv1;
    struct bb_transit_state_bkref_ret_type mrv2;
    struct bb_transit_state_bkref_ret_type mrv3;
    struct bb_transit_state_bkref_ret_type mrv4;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rcx();
    var_3 = init_r13();
    var_4 = init_r15();
    var_5 = init_rbp();
    var_6 = init_r12();
    var_7 = init_r14();
    var_8 = init_r10();
    var_9 = init_r9();
    var_10 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_4;
    *(uint64_t *)(var_0 + (-16L)) = var_7;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_6;
    *(uint64_t *)(var_0 + (-40L)) = var_5;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    var_11 = var_0 + (-184L);
    var_12 = (uint64_t *)(var_0 + (-168L));
    *var_12 = rsi;
    var_13 = *(uint64_t *)(rdi + 152UL);
    var_14 = *(uint64_t *)(rdi + 72UL);
    *(uint64_t *)(var_0 + (-176L)) = var_14;
    local_sp_6 = var_11;
    local_sp_3 = var_11;
    rcx_3 = var_2;
    r14_0 = 0UL;
    r10_3 = var_8;
    r9_3 = var_9;
    r8_3 = var_10;
    rcx_6 = var_2;
    r10_5 = var_8;
    r9_5 = var_9;
    r8_5 = var_10;
    if ((long)*(uint64_t *)(rsi + 8UL) <= (long)0UL) {
        *(uint32_t *)(local_sp_6 + 124UL) = 0U;
        local_sp_7 = local_sp_6;
        r10_6 = r10_5;
        r9_6 = r9_5;
        rcx_7 = rcx_6;
        r8_6 = r8_5;
        mrv.field_0 = (uint64_t)*(uint32_t *)(local_sp_7 + 124UL);
        mrv1 = mrv;
        mrv1.field_1 = rcx_7;
        mrv2 = mrv1;
        mrv2.field_2 = r10_6;
        mrv3 = mrv2;
        mrv3.field_3 = r9_6;
        mrv4 = mrv3;
        mrv4.field_4 = r8_6;
        return mrv4;
    }
    *(uint64_t *)(var_0 + (-128L)) = (var_14 << 3UL);
    var_15 = (uint64_t *)var_13;
    var_16 = (uint32_t *)(rdi + 160UL);
    var_17 = (uint64_t *)(rdi + 200UL);
    var_18 = (uint64_t *)(rdi + 216UL);
    var_19 = (uint64_t *)(var_13 + 24UL);
    var_20 = (uint64_t *)(var_13 + 48UL);
    var_21 = (uint64_t *)(var_13 + 40UL);
    var_22 = (uint64_t *)(rdi + 184UL);
    var_23 = *var_12;
    while (1U)
        {
            var_24 = *(uint64_t *)(var_23 + 16UL);
            var_25 = *(uint64_t *)((r14_0 << 3UL) + var_24);
            var_26 = ((var_25 << 4UL) + *var_15) + 8UL;
            r10_6 = r10_3;
            r9_6 = r9_3;
            rcx_4 = rcx_3;
            local_sp_5 = local_sp_3;
            r9_4 = r9_3;
            r8_4 = r8_3;
            r10_1 = r10_3;
            r9_1 = r9_3;
            r8_1 = r8_3;
            local_sp_4 = local_sp_3;
            rcx_5 = rcx_3;
            r14_1 = r14_0;
            r10_4 = r10_3;
            r8_6 = r8_3;
            if (*(unsigned char *)var_26 == '\x04') {
                var_112 = r14_1 + 1UL;
                var_113 = *(uint64_t *)(local_sp_5 + 16UL);
                local_sp_6 = local_sp_5;
                var_23 = var_113;
                local_sp_3 = local_sp_5;
                rcx_3 = rcx_5;
                r14_0 = var_112;
                r10_3 = r10_4;
                r9_3 = r9_4;
                r8_3 = r8_4;
                rcx_6 = rcx_5;
                r10_5 = r10_4;
                r9_5 = r9_4;
                r8_5 = r8_4;
                if ((long)*(uint64_t *)(var_113 + 8UL) > (long)var_112) {
                    continue;
                }
                loop_state_var = 1U;
                break;
            }
            var_27 = (uint32_t *)var_26;
            if ((*var_27 & 261888U) != 0U) {
                var_28 = (uint64_t)*var_16;
                var_29 = *(uint64_t *)(local_sp_3 + 8UL);
                var_30 = local_sp_3 + (-8L);
                *(uint64_t *)var_30 = 4266936UL;
                var_31 = indirect_placeholder_176(var_24, var_28, rdi, var_29);
                var_32 = var_31.field_0;
                var_33 = *var_27 >> 8U;
                var_34 = (uint64_t)var_33;
                var_35 = (uint64_t)(var_33 & 16712703U);
                local_sp_4 = var_30;
                rcx_4 = var_35;
                local_sp_5 = var_30;
                rcx_5 = var_35;
                if ((var_34 & 4UL) == 0UL) {
                    if (!(((var_34 & 8UL) == 0UL) || ((var_32 & 1UL) == 0UL))) {
                        var_112 = r14_1 + 1UL;
                        var_113 = *(uint64_t *)(local_sp_5 + 16UL);
                        local_sp_6 = local_sp_5;
                        var_23 = var_113;
                        local_sp_3 = local_sp_5;
                        rcx_3 = rcx_5;
                        r14_0 = var_112;
                        r10_3 = r10_4;
                        r9_3 = r9_4;
                        r8_3 = r8_4;
                        rcx_6 = rcx_5;
                        r10_5 = r10_4;
                        r9_5 = r9_4;
                        r8_5 = r8_4;
                        if ((long)*(uint64_t *)(var_113 + 8UL) > (long)var_112) {
                            continue;
                        }
                        loop_state_var = 1U;
                        break;
                    }
                }
                if (!(((var_32 & 1UL) != 0UL) && ((var_34 & 8UL) == 0UL))) {
                    var_112 = r14_1 + 1UL;
                    var_113 = *(uint64_t *)(local_sp_5 + 16UL);
                    local_sp_6 = local_sp_5;
                    var_23 = var_113;
                    local_sp_3 = local_sp_5;
                    rcx_3 = rcx_5;
                    r14_0 = var_112;
                    r10_3 = r10_4;
                    r9_3 = r9_4;
                    r8_3 = r8_4;
                    rcx_6 = rcx_5;
                    r10_5 = r10_4;
                    r9_5 = r9_4;
                    r8_5 = r8_4;
                    if ((long)*(uint64_t *)(var_113 + 8UL) > (long)var_112) {
                        continue;
                    }
                    loop_state_var = 1U;
                    break;
                }
                if (!(((var_34 & 32UL) != 0UL) && ((var_32 & 2UL) == 0UL))) {
                    var_112 = r14_1 + 1UL;
                    var_113 = *(uint64_t *)(local_sp_5 + 16UL);
                    local_sp_6 = local_sp_5;
                    var_23 = var_113;
                    local_sp_3 = local_sp_5;
                    rcx_3 = rcx_5;
                    r14_0 = var_112;
                    r10_3 = r10_4;
                    r9_3 = r9_4;
                    r8_3 = r8_4;
                    rcx_6 = rcx_5;
                    r10_5 = r10_4;
                    r9_5 = r9_4;
                    r8_5 = r8_4;
                    if ((long)*(uint64_t *)(var_113 + 8UL) > (long)var_112) {
                        continue;
                    }
                    loop_state_var = 1U;
                    break;
                }
                if (!(((signed char)(unsigned char)var_33 <= '\xff') && ((var_32 & 8UL) == 0UL))) {
                    var_112 = r14_1 + 1UL;
                    var_113 = *(uint64_t *)(local_sp_5 + 16UL);
                    local_sp_6 = local_sp_5;
                    var_23 = var_113;
                    local_sp_3 = local_sp_5;
                    rcx_3 = rcx_5;
                    r14_0 = var_112;
                    r10_3 = r10_4;
                    r9_3 = r9_4;
                    r8_3 = r8_4;
                    rcx_6 = rcx_5;
                    r10_5 = r10_4;
                    r9_5 = r9_4;
                    r8_5 = r8_4;
                    if ((long)*(uint64_t *)(var_113 + 8UL) > (long)var_112) {
                        continue;
                    }
                    loop_state_var = 1U;
                    break;
                }
            }
            var_36 = *var_17;
            var_37 = *(uint64_t *)(local_sp_4 + 8UL);
            var_38 = local_sp_4 + (-8L);
            *(uint64_t *)var_38 = 4266989UL;
            var_39 = indirect_placeholder_16(var_37, rdi, var_25);
            var_40 = (uint32_t *)(local_sp_4 + 116UL);
            var_41 = (uint32_t)var_39;
            *var_40 = var_41;
            local_sp_7 = var_38;
            local_sp_5 = var_38;
            local_sp_1 = var_38;
            rcx_1 = rcx_4;
            r13_0 = var_36;
            rcx_5 = rcx_4;
            rcx_7 = rcx_4;
            if ((uint64_t)var_41 != 0UL) {
                loop_state_var = 0U;
                break;
            }
            if ((long)*var_17 <= (long)var_36) {
                *(uint64_t *)(local_sp_4 + 72UL) = (var_25 << 3UL);
                *(uint64_t *)(local_sp_4 + 64UL) = (var_25 * 24UL);
                *(uint64_t *)(local_sp_4 + 80UL) = r14_0;
                while (1U)
                    {
                        var_42 = (r13_0 * 40UL) + *var_18;
                        local_sp_2 = local_sp_1;
                        r10_6 = r10_1;
                        r9_6 = r9_1;
                        rcx_2 = rcx_1;
                        r10_2 = r10_1;
                        r9_2 = r9_1;
                        r8_2 = r8_1;
                        r8_6 = r8_1;
                        if (*(uint64_t *)var_42 == var_25) {
                            var_110 = r13_0 + 1UL;
                            local_sp_5 = local_sp_2;
                            r9_4 = r9_2;
                            local_sp_1 = local_sp_2;
                            rcx_1 = rcx_2;
                            r13_0 = var_110;
                            r8_4 = r8_2;
                            r10_1 = r10_2;
                            r9_1 = r9_2;
                            r8_1 = r8_2;
                            rcx_5 = rcx_2;
                            r10_4 = r10_2;
                            if ((long)*var_17 > (long)var_110) {
                                continue;
                            }
                            loop_state_var = 0U;
                            break;
                        }
                        var_43 = (uint64_t *)(local_sp_1 + 8UL);
                        if (*(uint64_t *)(var_42 + 8UL) != *var_43) {
                            var_44 = *(uint64_t *)(var_42 + 24UL);
                            var_45 = *(uint64_t *)(var_42 + 16UL);
                            var_46 = var_44 - var_45;
                            var_47 = local_sp_1 + 48UL;
                            var_48 = (uint64_t *)var_47;
                            *var_48 = var_46;
                            if (var_46 == 0UL) {
                                *(uint64_t *)(local_sp_1 + 32UL) = ((**(uint64_t **)((*(uint64_t *)(local_sp_1 + 72UL) + *var_21) + 16UL) * 24UL) + *var_20);
                            } else {
                                *(uint64_t *)(local_sp_1 + 32UL) = ((*(uint64_t *)(*(uint64_t *)(local_sp_1 + 80UL) + *var_19) * 24UL) + *var_20);
                            }
                            var_49 = (var_44 + *var_43) - var_45;
                            var_50 = (uint64_t)*var_16;
                            var_51 = var_49 + (-1L);
                            *(uint64_t *)(local_sp_1 + (-8L)) = 4267335UL;
                            var_52 = indirect_placeholder_175(var_45, var_50, rdi, var_51);
                            var_53 = var_52.field_0;
                            var_54 = (uint32_t *)(local_sp_1 + 36UL);
                            *var_54 = (uint32_t)var_53;
                            var_55 = *var_22;
                            var_56 = var_49 << 3UL;
                            var_57 = var_56 + var_55;
                            *(uint64_t *)(local_sp_1 + 56UL) = var_57;
                            var_58 = *(uint64_t *)var_57;
                            var_59 = *var_48;
                            var_60 = *(uint64_t *)(var_59 + var_55);
                            var_61 = (uint64_t *)(local_sp_1 + 16UL);
                            *var_61 = 0UL;
                            rcx_7 = var_59;
                            if (var_60 == 0UL) {
                                *var_61 = *(uint64_t *)(var_60 + 16UL);
                            }
                            if (var_58 == 0UL) {
                                var_81 = (uint64_t)*var_54;
                                var_82 = *(uint64_t *)(local_sp_1 + 24UL);
                                var_83 = local_sp_1 + 116UL;
                                var_84 = local_sp_1 + (-16L);
                                *(uint64_t *)var_84 = 4267109UL;
                                var_85 = indirect_placeholder_174(var_81, var_82, var_83, var_13);
                                var_86 = var_85.field_0;
                                var_87 = var_85.field_1;
                                var_88 = var_85.field_2;
                                var_89 = var_85.field_3;
                                var_90 = var_85.field_4;
                                **(uint64_t **)var_47 = var_86;
                                var_91 = *var_22;
                                rcx_0 = var_87;
                                local_sp_7 = var_84;
                                var_92 = var_91;
                                r10_6 = var_88;
                                r9_6 = var_89;
                                local_sp_0 = var_84;
                                r10_0 = var_88;
                                r9_0 = var_89;
                                r8_0 = var_90;
                                rcx_7 = var_87;
                                r8_6 = var_90;
                                if (*(uint64_t *)(var_56 + var_91) != 0UL & *(uint32_t *)(local_sp_1 + 108UL) != 0U) {
                                    loop_state_var = 1U;
                                    break;
                                }
                            }
                            var_62 = *(uint64_t *)(var_58 + 80UL);
                            var_63 = *(uint64_t *)(local_sp_1 + 24UL);
                            var_64 = local_sp_1 + 88UL;
                            *(uint64_t *)(local_sp_1 + (-16L)) = 4267427UL;
                            var_65 = indirect_placeholder_16(var_63, var_64, var_62);
                            var_66 = local_sp_1 + 108UL;
                            var_67 = (uint32_t *)var_66;
                            var_68 = (uint32_t)var_65;
                            *var_67 = var_68;
                            if ((uint64_t)var_68 != 0UL) {
                                var_69 = local_sp_1 + (-24L);
                                *(uint64_t *)var_69 = 4267159UL;
                                indirect_placeholder();
                                local_sp_7 = var_69;
                                loop_state_var = 1U;
                                break;
                            }
                            var_70 = var_56 + *var_22;
                            var_71 = (uint64_t)*(uint32_t *)(local_sp_1 + 28UL);
                            var_72 = local_sp_1 + 80UL;
                            *(uint64_t *)(local_sp_1 + (-24L)) = 4267469UL;
                            var_73 = indirect_placeholder_172(var_71, var_72, var_66, var_13);
                            var_74 = var_73.field_0;
                            var_75 = var_73.field_1;
                            var_76 = var_73.field_2;
                            var_77 = var_73.field_3;
                            var_78 = var_73.field_4;
                            *(uint64_t *)var_70 = var_74;
                            var_79 = local_sp_1 + (-32L);
                            *(uint64_t *)var_79 = 4267482UL;
                            indirect_placeholder();
                            var_80 = *var_22;
                            rcx_0 = var_75;
                            local_sp_7 = var_79;
                            var_92 = var_80;
                            r10_6 = var_76;
                            r9_6 = var_77;
                            local_sp_0 = var_79;
                            r10_0 = var_76;
                            r9_0 = var_77;
                            r8_0 = var_78;
                            rcx_7 = var_75;
                            r8_6 = var_78;
                            if (*(uint64_t *)(var_56 + var_80) != 0UL) {
                                if (*(uint32_t *)(local_sp_1 + 92UL) == 0U) {
                                    loop_state_var = 1U;
                                    break;
                                }
                            }
                            local_sp_2 = local_sp_0;
                            rcx_2 = rcx_0;
                            r10_2 = r10_0;
                            r9_2 = r9_0;
                            r8_2 = r8_0;
                            var_93 = *(uint64_t *)(*(uint64_t *)(local_sp_0 + 56UL) + var_92);
                            var_94 = *(uint64_t *)(local_sp_0 + 24UL);
                            rcx_2 = var_94;
                            if (*(uint64_t *)(local_sp_0 + 48UL) != 0UL & (long)*(uint64_t *)(var_93 + 16UL) <= (long)var_94) {
                                var_95 = *(uint64_t *)(local_sp_0 + 8UL);
                                var_96 = *(uint64_t *)(local_sp_0 + 32UL);
                                var_97 = local_sp_0 + (-8L);
                                *(uint64_t *)var_97 = 4267566UL;
                                var_98 = indirect_placeholder_173(var_94, var_95, rdi, var_96, r10_0, r9_0, r8_0);
                                var_99 = (uint32_t *)(local_sp_0 + 116UL);
                                var_100 = (uint32_t)var_98.field_0;
                                *var_99 = var_100;
                                local_sp_7 = var_97;
                                if ((uint64_t)var_100 != 0UL) {
                                    rcx_7 = var_98.field_1;
                                    r10_6 = var_98.field_2;
                                    r9_6 = var_98.field_3;
                                    r8_6 = var_98.field_4;
                                    loop_state_var = 1U;
                                    break;
                                }
                                var_101 = local_sp_0 + (-16L);
                                *(uint64_t *)var_101 = 4267589UL;
                                var_102 = indirect_placeholder_171(rdi, var_96);
                                var_103 = var_102.field_0;
                                var_104 = var_102.field_1;
                                var_105 = var_102.field_2;
                                var_106 = var_102.field_3;
                                var_107 = var_102.field_4;
                                var_108 = (uint32_t *)(local_sp_0 + 108UL);
                                var_109 = (uint32_t)var_103;
                                *var_108 = var_109;
                                local_sp_2 = var_101;
                                local_sp_7 = var_101;
                                r10_6 = var_105;
                                r9_6 = var_106;
                                rcx_2 = var_104;
                                r10_2 = var_105;
                                r9_2 = var_106;
                                r8_2 = var_107;
                                rcx_7 = var_104;
                                r8_6 = var_107;
                                if ((uint64_t)var_109 != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                            }
                        }
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 0U:
                    {
                        var_111 = *(uint64_t *)(local_sp_2 + 88UL);
                        r14_1 = var_111;
                    }
                    break;
                  case 1U:
                    {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
        }
    *(uint32_t *)(local_sp_6 + 124UL) = 0U;
    local_sp_7 = local_sp_6;
    r10_6 = r10_5;
    r9_6 = r9_5;
    rcx_7 = rcx_6;
    r8_6 = r8_5;
}
