typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_151_ret_type;
struct indirect_placeholder_151_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_15(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_16(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_151_ret_type indirect_placeholder_151(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
uint64_t bb_add_epsilon_src_nodes(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_151_ret_type var_21;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint32_t var_10;
    uint64_t var_11;
    uint64_t rax_0;
    uint64_t var_27;
    uint64_t local_sp_0;
    uint64_t local_sp_1;
    uint64_t rbx_0;
    uint64_t rbp_0;
    uint64_t r12_0;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint32_t *var_25;
    uint32_t var_26;
    uint64_t var_12;
    uint64_t *var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint32_t *var_17;
    uint32_t var_18;
    uint64_t rbp_1;
    uint64_t var_28;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r13();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    var_7 = var_0 + (-60L);
    *(uint32_t *)var_7 = 0U;
    var_8 = var_0 + (-80L);
    *(uint64_t *)var_8 = 4247317UL;
    var_9 = indirect_placeholder_16(rsi, var_7, rdi);
    var_10 = *(uint32_t *)(var_0 + (-68L));
    var_11 = (uint64_t)var_10;
    rax_0 = var_11;
    local_sp_1 = var_8;
    rbx_0 = 0UL;
    rbp_0 = rsi;
    r12_0 = rdi;
    rbp_1 = rsi;
    if (var_10 == 0U) {
        return rax_0;
    }
    var_12 = var_9 + 56UL;
    rax_0 = 12UL;
    if (*(uint64_t *)var_12 != 0UL) {
        var_13 = (uint64_t *)(rsi + 8UL);
        var_14 = *var_13;
        var_15 = var_0 + (-88L);
        *(uint64_t *)var_15 = 4247353UL;
        var_16 = indirect_placeholder_15(var_12, var_14);
        var_17 = (uint32_t *)(var_0 + (-76L));
        var_18 = (uint32_t)var_16;
        *var_17 = var_18;
        local_sp_0 = var_15;
        local_sp_1 = var_15;
        if ((uint64_t)var_18 == 0UL) {
            return rax_0;
        }
        if ((long)*var_13 <= (long)0UL) {
            while (1U)
                {
                    var_19 = (*(uint64_t *)((rbx_0 << 3UL) + *(uint64_t *)(rbp_0 + 16UL)) * 24UL) + *(uint64_t *)(r12_0 + 56UL);
                    var_20 = local_sp_0 + (-8L);
                    *(uint64_t *)var_20 = 4247397UL;
                    var_21 = indirect_placeholder_151(rbx_0, var_12, rbp_0, r12_0, var_19);
                    var_22 = var_21.field_0;
                    var_23 = var_21.field_2;
                    var_24 = var_21.field_3;
                    var_25 = (uint32_t *)(local_sp_0 | 4UL);
                    var_26 = (uint32_t)var_22;
                    *var_25 = var_26;
                    local_sp_0 = var_20;
                    local_sp_1 = var_20;
                    rbp_0 = var_23;
                    r12_0 = var_24;
                    rbp_1 = var_23;
                    if ((uint64_t)var_26 != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_27 = var_21.field_1 + 1UL;
                    rbx_0 = var_27;
                    if ((long)*(uint64_t *)(var_23 + 8UL) > (long)var_27) {
                        continue;
                    }
                    loop_state_var = 1U;
                    break;
                }
            switch (loop_state_var) {
              case 0U:
                {
                    return rax_0;
                }
                break;
              case 1U:
                {
                    break;
                }
                break;
            }
        }
    }
    *(uint64_t *)(local_sp_1 + (-8L)) = 4247430UL;
    var_28 = indirect_placeholder_16(var_12, rbp_1, rdx);
    rax_0 = var_28;
}
