typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_15(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_30(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t indirect_placeholder_16(uint64_t param_0, uint64_t param_1, uint64_t param_2);
typedef _Bool bool;
uint64_t bb_duplicate_node_closure(uint64_t rcx, uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t r8) {
    uint64_t var_41;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t *var_11;
    uint64_t *var_12;
    uint64_t local_sp_1_ph;
    uint64_t rbx_0_ph;
    uint64_t r15_0_ph;
    uint64_t r12_0_ph;
    uint64_t var_13;
    uint64_t local_sp_0;
    uint64_t local_sp_1;
    uint64_t rbx_0;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t rax_0;
    uint64_t var_35;
    uint64_t local_sp_1_be;
    uint64_t rbx_0_be;
    uint64_t r12_0_be;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_36;
    uint64_t *var_37;
    uint64_t var_38;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t r12_0;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_28;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r13();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    var_8 = var_0 + (-72L);
    *(uint64_t *)var_8 = rcx;
    var_9 = (uint64_t)(uint32_t)r8;
    var_10 = (uint64_t *)rdi;
    var_11 = (uint64_t *)(rdi + 40UL);
    var_12 = (uint64_t *)(rdi + 24UL);
    local_sp_1_ph = var_8;
    rbx_0_ph = rdx;
    r15_0_ph = var_9;
    r12_0_ph = rsi;
    rax_0 = 12UL;
    while (1U)
        {
            var_13 = (uint64_t)(uint32_t)r15_0_ph;
            local_sp_1 = local_sp_1_ph;
            rbx_0 = rbx_0_ph;
            r12_0 = r12_0_ph;
            while (1U)
                {
                    var_14 = r12_0 << 4UL;
                    if (*(unsigned char *)((var_14 + *var_10) + 8UL) != '\x04') {
                        var_47 = *var_12;
                        var_48 = r12_0 << 3UL;
                        var_49 = *(uint64_t *)(var_48 + var_47);
                        *(uint64_t *)(local_sp_1 + 8UL) = rbx_0;
                        var_50 = rbx_0 * 24UL;
                        *(uint64_t *)((var_50 + *var_11) + 8UL) = 0UL;
                        *(uint64_t *)(local_sp_1 + (-8L)) = 4243207UL;
                        var_51 = indirect_placeholder_16(var_13, rdi, var_49);
                        rbx_0_be = var_51;
                        r12_0_be = var_49;
                        if (var_51 != 18446744073709551615UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_52 = *var_12;
                        *(uint64_t *)((*(uint64_t *)local_sp_1 << 3UL) + var_52) = *(uint64_t *)(var_48 + var_52);
                        var_53 = var_50 + *var_11;
                        var_54 = local_sp_1 + (-16L);
                        *(uint64_t *)var_54 = 4243252UL;
                        var_55 = indirect_placeholder_15(var_53, var_51);
                        local_sp_1_be = var_54;
                        if ((uint64_t)(unsigned char)var_55 != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        local_sp_1 = local_sp_1_be;
                        rbx_0 = rbx_0_be;
                        r12_0 = r12_0_be;
                        continue;
                    }
                    var_15 = *var_11;
                    var_16 = r12_0 * 24UL;
                    var_17 = var_16 + var_15;
                    rax_0 = 0UL;
                    switch_state_var = 0;
                    switch (*(uint64_t *)(var_17 + 8UL)) {
                      case 0UL:
                        {
                            var_28 = *var_12;
                            *(uint64_t *)((rbx_0 << 3UL) + var_28) = *(uint64_t *)((r12_0 << 3UL) + var_28);
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 1UL:
                        {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      default:
                        {
                            var_29 = **(uint64_t **)(var_17 + 16UL);
                            var_30 = rbx_0 * 24UL;
                            *(uint64_t *)((var_30 + var_15) + 8UL) = 0UL;
                            *(uint64_t *)(local_sp_1 + (-8L)) = 4243681UL;
                            var_31 = indirect_placeholder_16(var_13, rdi, var_29);
                            if (var_31 == 18446744073709551615UL) {
                                *(uint64_t *)(local_sp_1 + (-16L)) = 4243454UL;
                                var_35 = indirect_placeholder_16(var_13, rdi, var_29);
                                if (var_35 != 18446744073709551615UL) {
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                                var_36 = var_30 + *var_11;
                                var_37 = (uint64_t *)(local_sp_1 + (-24L));
                                *var_37 = 4243482UL;
                                var_38 = indirect_placeholder_15(var_36, var_35);
                                if ((uint64_t)(unsigned char)var_38 != 0UL) {
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                                var_39 = *var_37;
                                var_40 = local_sp_1 + (-32L);
                                *(uint64_t *)var_40 = 4243511UL;
                                var_41 = indirect_placeholder_30(var_39, var_35, rdi, var_29, var_13);
                                local_sp_0 = var_40;
                                rax_0 = var_41;
                                if ((uint64_t)(uint32_t)var_41 != 0UL) {
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                            }
                            var_32 = var_30 + *var_11;
                            var_33 = local_sp_1 + (-16L);
                            *(uint64_t *)var_33 = 4243706UL;
                            var_34 = indirect_placeholder_15(var_32, var_31);
                            local_sp_0 = var_33;
                            if ((uint64_t)(unsigned char)var_34 != 0UL) {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            var_42 = *(uint64_t *)(*(uint64_t *)((var_16 + *var_11) + 16UL) + 8UL);
                            *(uint64_t *)(local_sp_0 + (-8L)) = 4243546UL;
                            var_43 = indirect_placeholder_16(var_13, rdi, var_42);
                            rbx_0_be = var_43;
                            r12_0_be = var_42;
                            if (var_43 != 18446744073709551615UL) {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            var_44 = var_30 + *var_11;
                            var_45 = local_sp_0 + (-16L);
                            *(uint64_t *)var_45 = 4243574UL;
                            var_46 = indirect_placeholder_15(var_44, var_43);
                            local_sp_1_be = var_45;
                            if ((uint64_t)(unsigned char)var_46 != 0UL) {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            local_sp_1 = local_sp_1_be;
                            rbx_0 = rbx_0_be;
                            r12_0 = r12_0_be;
                            continue;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 0U:
                {
                    var_18 = **(uint64_t **)(var_17 + 16UL);
                    var_19 = rbx_0 * 24UL;
                    *(uint64_t *)((var_19 + var_15) + 8UL) = 0UL;
                    r12_0_ph = var_18;
                    if ((r12_0 != *(uint64_t *)local_sp_1) || (r12_0 == rbx_0)) {
                        var_23 = var_13 | (uint64_t)(uint32_t)((uint16_t)(*(uint32_t *)((var_14 + *var_10) + 8UL) >> 8U) & (unsigned short)1023U);
                        *(uint64_t *)(local_sp_1 + (-8L)) = 4243363UL;
                        var_24 = indirect_placeholder_16(var_23, rdi, var_18);
                        rbx_0_ph = var_24;
                        r15_0_ph = var_23;
                        if (var_24 == 18446744073709551615UL) {
                            switch_state_var = 1;
                            break;
                        }
                        var_25 = var_19 + *var_11;
                        var_26 = local_sp_1 + (-16L);
                        *(uint64_t *)var_26 = 4243391UL;
                        var_27 = indirect_placeholder_15(var_25, var_24);
                        local_sp_1_ph = var_26;
                        if ((uint64_t)(unsigned char)var_27 != 0UL) {
                            continue;
                        }
                        switch_state_var = 1;
                        break;
                    }
                    var_20 = var_19 + *var_11;
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4243418UL;
                    var_21 = indirect_placeholder_15(var_20, var_18);
                    var_22 = helper_cc_compute_c_wrapper(var_21 + (-1L), 1UL, var_7, 14U);
                    rax_0 = (0UL - var_22) & 12UL;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    return rax_0;
}
