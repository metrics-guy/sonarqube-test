typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_re_acquire_state_context_ret_type;
struct indirect_placeholder_148_ret_type;
struct bb_re_acquire_state_context_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_148_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_15(uint64_t param_0, uint64_t param_1);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r10(void);
extern uint64_t init_r9(void);
extern uint64_t indirect_placeholder_129(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern uint64_t init_r8(void);
extern struct indirect_placeholder_148_ret_type indirect_placeholder_148(uint64_t param_0, uint64_t param_1);
struct bb_re_acquire_state_context_ret_type bb_re_acquire_state_context(uint64_t rcx, uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_31;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t r8_3;
    uint64_t rcx5_0;
    uint64_t local_sp_1;
    uint64_t rbx_0;
    uint64_t var_17;
    uint64_t r8_1;
    uint64_t local_sp_0;
    uint64_t r8_0;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    struct indirect_placeholder_148_ret_type var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t rbp_0;
    uint64_t var_27;
    uint64_t local_sp_2;
    uint64_t r8_2;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint32_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_18;
    uint64_t var_26;
    struct bb_re_acquire_state_context_ret_type mrv;
    struct bb_re_acquire_state_context_ret_type mrv1;
    struct bb_re_acquire_state_context_ret_type mrv2;
    struct bb_re_acquire_state_context_ret_type mrv3;
    struct bb_re_acquire_state_context_ret_type mrv4;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r13();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_r10();
    var_8 = init_r9();
    var_9 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    *(uint64_t *)(var_0 + (-64L)) = rdi;
    *(uint64_t *)(var_0 + (-72L)) = rsi;
    var_10 = (uint64_t *)(var_0 + (-80L));
    *var_10 = rdx;
    r8_3 = var_9;
    rcx5_0 = rcx;
    rbx_0 = 0UL;
    r8_0 = var_9;
    rbp_0 = 0UL;
    r8_2 = var_9;
    if (*(uint64_t *)(rdx + 8UL) == 0UL) {
        *(uint32_t *)rdi = 0U;
    } else {
        var_11 = (uint32_t)rcx;
        var_12 = (uint64_t)var_11;
        var_13 = var_0 + (-96L);
        *(uint64_t *)var_13 = 4245650UL;
        var_14 = indirect_placeholder_15(rdx, var_12);
        var_15 = *var_10;
        var_16 = ((var_14 & *(uint64_t *)(var_15 + 136UL)) * 24UL) + *(uint64_t *)(var_15 + 64UL);
        var_17 = *(uint64_t *)var_16;
        var_18 = helper_cc_compute_all_wrapper(var_17, 0UL, 0UL, 25U);
        local_sp_0 = var_13;
        local_sp_2 = var_13;
        rcx5_0 = var_14;
        if ((uint64_t)(((unsigned char)(var_18 >> 4UL) ^ (unsigned char)var_18) & '\xc0') != 0UL) {
            while (1U)
                {
                    var_19 = *(uint64_t *)((rbx_0 << 3UL) + *(uint64_t *)(var_16 + 16UL));
                    local_sp_1 = local_sp_0;
                    r8_1 = r8_0;
                    rbp_0 = var_19;
                    if (*(uint64_t *)var_19 == var_14) {
                        var_27 = rbx_0 + 1UL;
                        rbx_0 = var_27;
                        local_sp_0 = local_sp_1;
                        r8_0 = r8_1;
                        local_sp_2 = local_sp_1;
                        r8_2 = r8_1;
                        if (var_17 == var_27) {
                            continue;
                        }
                        loop_state_var = 0U;
                        break;
                    }
                    var_20 = *(uint64_t *)(var_19 + 80UL);
                    var_21 = *(uint64_t *)(local_sp_0 + 8UL);
                    var_22 = local_sp_0 + (-8L);
                    *(uint64_t *)var_22 = 4245754UL;
                    var_23 = indirect_placeholder_148(var_20, var_21);
                    var_24 = var_23.field_0;
                    var_25 = var_23.field_2;
                    local_sp_1 = var_22;
                    r8_1 = var_25;
                    r8_3 = var_25;
                    if ((uint64_t)((uint32_t)(uint64_t)(*(unsigned char *)(var_19 + 104UL) & '\x0f') - var_11) != 0UL & (uint64_t)(unsigned char)var_24 != 0UL) {
                        var_26 = var_23.field_1;
                        rcx5_0 = var_26;
                        loop_state_var = 1U;
                        break;
                    }
                }
            switch (loop_state_var) {
              case 1U:
                {
                    mrv.field_0 = rbp_0;
                    mrv1 = mrv;
                    mrv1.field_1 = rcx5_0;
                    mrv2 = mrv1;
                    mrv2.field_2 = var_7;
                    mrv3 = mrv2;
                    mrv3.field_3 = var_8;
                    mrv4 = mrv3;
                    mrv4.field_4 = r8_3;
                    return mrv4;
                }
                break;
              case 0U:
                {
                    break;
                }
                break;
            }
        }
        var_28 = *(uint64_t *)(local_sp_2 + 8UL);
        var_29 = local_sp_2 + 16UL;
        var_30 = *(uint64_t *)var_29;
        *(uint64_t *)(local_sp_2 + (-8L)) = 4245781UL;
        var_31 = indirect_placeholder_129(var_14, var_12, var_30, var_28, var_7, var_8, r8_2);
        rbp_0 = var_31;
        r8_3 = r8_2;
        if (var_31 == 0UL) {
            **(uint32_t **)var_29 = 12U;
            rbp_0 = 0UL;
        }
    }
}
