typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_143_ret_type;
struct indirect_placeholder_142_ret_type;
struct indirect_placeholder_141_ret_type;
struct indirect_placeholder_144_ret_type;
struct indirect_placeholder_145_ret_type;
struct indirect_placeholder_143_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_142_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_141_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_144_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_145_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_15(uint64_t param_0, uint64_t param_1);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct indirect_placeholder_143_ret_type indirect_placeholder_143(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_142_ret_type indirect_placeholder_142(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_141_ret_type indirect_placeholder_141(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_144_ret_type indirect_placeholder_144(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_145_ret_type indirect_placeholder_145(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
typedef _Bool bool;
uint64_t bb_create_initial_state(uint64_t rdi) {
    struct indirect_placeholder_144_ret_type var_28;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint32_t var_11;
    uint64_t var_12;
    uint64_t r12_3;
    uint64_t rbp_2;
    uint64_t local_sp_0;
    uint64_t var_53;
    uint64_t r12_2;
    uint64_t var_39;
    uint64_t var_40;
    struct indirect_placeholder_143_ret_type var_41;
    uint64_t var_42;
    uint64_t *var_43;
    uint64_t var_44;
    uint64_t var_45;
    struct indirect_placeholder_142_ret_type var_46;
    uint64_t var_47;
    uint64_t *var_48;
    uint64_t var_49;
    uint64_t var_50;
    struct indirect_placeholder_141_ret_type var_51;
    uint64_t var_52;
    uint64_t rdx_1;
    uint64_t var_54;
    uint64_t var_26;
    uint64_t rbx_0;
    uint64_t var_27;
    uint64_t rcx_0;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t local_sp_2;
    uint64_t local_sp_3;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t rbx_1;
    uint64_t local_sp_1;
    uint64_t r12_1;
    uint64_t rbp_0;
    uint64_t r12_0;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t *var_20;
    uint64_t rdx_0;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t rbp_1;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    struct indirect_placeholder_145_ret_type var_37;
    uint64_t var_38;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r13();
    var_3 = init_rbp();
    var_4 = init_r12();
    var_5 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_5;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_3;
    *(uint64_t *)(var_0 + (-40L)) = var_1;
    var_6 = var_0 + (-72L);
    var_7 = *(uint64_t *)(*(uint64_t *)(*(uint64_t *)(rdi + 104UL) + 24UL) + 56UL);
    *(uint64_t *)(rdi + 144UL) = var_7;
    var_8 = (var_7 * 24UL) + *(uint64_t *)(rdi + 48UL);
    var_9 = var_0 + (-80L);
    *(uint64_t *)var_9 = 4245874UL;
    var_10 = indirect_placeholder_15(var_6, var_8);
    var_11 = (uint32_t)var_10;
    var_12 = (uint64_t)var_11;
    *(uint32_t *)(var_0 + (-52L)) = var_11;
    r12_3 = var_12;
    rbp_2 = rdi;
    r12_2 = 0UL;
    rdx_1 = 0UL;
    rbx_0 = 0UL;
    local_sp_3 = var_9;
    local_sp_1 = var_9;
    rbp_0 = rdi;
    r12_0 = 0UL;
    rdx_0 = 0UL;
    if (var_12 == 0UL) {
        return (uint64_t)(uint32_t)r12_3;
    }
    if ((long)*(uint64_t *)(rdi + 152UL) <= (long)0UL) {
        var_13 = *(uint64_t *)var_6;
        var_14 = helper_cc_compute_all_wrapper(var_13, 0UL, 0UL, 25U);
        rcx_0 = var_13;
        if ((uint64_t)(((unsigned char)(var_14 >> 4UL) ^ (unsigned char)var_14) & '\xc0') != 0UL) {
            while (1U)
                {
                    var_15 = *(uint64_t *)(local_sp_1 + 16UL);
                    var_16 = *(uint64_t *)rbp_0;
                    var_17 = *(uint64_t *)((rbx_0 << 3UL) + var_15);
                    var_18 = (var_17 << 4UL) + var_16;
                    local_sp_2 = local_sp_1;
                    rbx_1 = rbx_0;
                    rbp_1 = rbp_0;
                    r12_1 = r12_0;
                    if (*(unsigned char *)(var_18 + 8UL) == '\x04') {
                        var_33 = rbx_1 + 1UL;
                        var_34 = *(uint64_t *)(local_sp_2 + 8UL);
                        rbp_2 = rbp_1;
                        r12_2 = r12_1;
                        rbx_0 = var_33;
                        rcx_0 = var_34;
                        local_sp_3 = local_sp_2;
                        local_sp_1 = local_sp_2;
                        rbp_0 = rbp_1;
                        r12_0 = r12_1;
                        if ((long)var_34 > (long)var_33) {
                            continue;
                        }
                        loop_state_var = 0U;
                        break;
                    }
                    var_19 = helper_cc_compute_all_wrapper(rcx_0, 0UL, 0UL, 25U);
                    if ((uint64_t)(((unsigned char)(var_19 >> 4UL) ^ (unsigned char)var_19) & '\xc0') == 0UL) {
                        var_23 = **(uint64_t **)(((var_17 * 24UL) + *(uint64_t *)(rbp_0 + 40UL)) + 16UL);
                        var_24 = local_sp_1 + (-8L);
                        *(uint64_t *)var_24 = 4245993UL;
                        var_25 = indirect_placeholder_15(local_sp_1, var_23);
                        local_sp_2 = var_24;
                        var_26 = (var_23 * 24UL) + *(uint64_t *)(rbp_0 + 48UL);
                        var_27 = local_sp_1 + (-16L);
                        *(uint64_t *)var_27 = 4246084UL;
                        var_28 = indirect_placeholder_144(rbx_0, var_24, rbp_0, r12_0, var_26);
                        var_29 = var_28.field_0;
                        var_30 = var_28.field_2;
                        var_31 = var_28.field_3;
                        var_32 = (uint64_t)(uint32_t)var_29;
                        r12_3 = var_32;
                        local_sp_2 = var_27;
                        rbx_1 = 0UL;
                        r12_1 = var_31;
                        rbp_1 = var_30;
                        if (rcx_0 != rdx_1 & var_25 != 0UL & var_32 != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                    }
                    var_20 = (uint64_t *)var_18;
                    while (1U)
                        {
                            var_21 = (*(uint64_t *)((rdx_0 << 3UL) + var_15) << 4UL) + var_16;
                            rdx_1 = rdx_0;
                            if (*(unsigned char *)(var_21 + 8UL) != '\t') {
                                if (*(uint64_t *)var_21 != *var_20) {
                                    loop_state_var = 0U;
                                    break;
                                }
                            }
                            var_22 = rdx_0 + 1UL;
                            rdx_0 = var_22;
                            if (rcx_0 == var_22) {
                                continue;
                            }
                            loop_state_var = 1U;
                            break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 0U:
                        {
                            break;
                        }
                        break;
                      case 1U:
                        {
                            var_33 = rbx_1 + 1UL;
                            var_34 = *(uint64_t *)(local_sp_2 + 8UL);
                            rbp_2 = rbp_1;
                            r12_2 = r12_1;
                            rbx_0 = var_33;
                            rcx_0 = var_34;
                            local_sp_3 = local_sp_2;
                            local_sp_1 = local_sp_2;
                            rbp_0 = rbp_1;
                            r12_0 = r12_1;
                            if ((long)var_34 > (long)var_33) {
                                continue;
                            }
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            switch (loop_state_var) {
              case 1U:
                {
                    return (uint64_t)(uint32_t)r12_3;
                }
                break;
              case 0U:
                {
                    break;
                }
                break;
            }
        }
    }
    var_35 = local_sp_3 + 28UL;
    var_36 = local_sp_3 + (-8L);
    *(uint64_t *)var_36 = 4246118UL;
    var_37 = indirect_placeholder_145(0UL, local_sp_3, var_35, rbp_2);
    var_38 = var_37.field_0;
    *(uint64_t *)(rbp_2 + 72UL) = var_38;
    local_sp_0 = var_36;
    r12_3 = r12_2;
    if (var_38 == 0UL) {
        var_54 = (uint64_t)*(uint32_t *)(local_sp_3 + 20UL);
        r12_3 = var_54;
    } else {
        if ((signed char)*(unsigned char *)(var_38 + 104UL) < '\x00') {
            var_39 = local_sp_3 + 20UL;
            var_40 = local_sp_3 + (-16L);
            *(uint64_t *)var_40 = 4246199UL;
            var_41 = indirect_placeholder_143(1UL, var_36, var_39, rbp_2);
            var_42 = var_41.field_0;
            var_43 = (uint64_t *)(rbp_2 + 80UL);
            *var_43 = var_42;
            var_44 = local_sp_3 + 12UL;
            var_45 = local_sp_3 + (-24L);
            *(uint64_t *)var_45 = 4246224UL;
            var_46 = indirect_placeholder_142(2UL, var_40, var_44, rbp_2);
            var_47 = var_46.field_0;
            var_48 = (uint64_t *)(rbp_2 + 88UL);
            *var_48 = var_47;
            var_49 = local_sp_3 + 4UL;
            var_50 = local_sp_3 + (-32L);
            *(uint64_t *)var_50 = 4246249UL;
            var_51 = indirect_placeholder_141(6UL, var_45, var_49, rbp_2);
            var_52 = var_51.field_0;
            *(uint64_t *)(rbp_2 + 96UL) = var_52;
            local_sp_0 = var_50;
            if (*var_43 != 0UL) {
                var_53 = (uint64_t)*(uint32_t *)(local_sp_3 + (-4L));
                r12_3 = var_53;
                return (uint64_t)(uint32_t)r12_3;
            }
            if (!((*var_48 == 0UL) || (var_52 == 0UL))) {
                var_53 = (uint64_t)*(uint32_t *)(local_sp_3 + (-4L));
                r12_3 = var_53;
                return (uint64_t)(uint32_t)r12_3;
            }
        }
        *(uint64_t *)(rbp_2 + 96UL) = var_38;
        *(uint64_t *)(rbp_2 + 88UL) = var_38;
        *(uint64_t *)(rbp_2 + 80UL) = var_38;
        *(uint64_t *)(local_sp_0 + (-8L)) = 4246155UL;
        indirect_placeholder();
    }
}
