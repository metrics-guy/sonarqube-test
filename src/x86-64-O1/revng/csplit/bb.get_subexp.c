typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_177_ret_type;
struct indirect_placeholder_178_ret_type;
struct indirect_placeholder_180_ret_type;
struct indirect_placeholder_179_ret_type;
struct indirect_placeholder_181_ret_type;
struct indirect_placeholder_182_ret_type;
struct indirect_placeholder_183_ret_type;
struct indirect_placeholder_177_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_178_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_180_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_179_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_181_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_182_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_183_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_15(uint64_t param_0, uint64_t param_1);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_129(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_177_ret_type indirect_placeholder_177(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_178_ret_type indirect_placeholder_178(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_180_ret_type indirect_placeholder_180(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
extern struct indirect_placeholder_179_ret_type indirect_placeholder_179(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_181_ret_type indirect_placeholder_181(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_182_ret_type indirect_placeholder_182(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_183_ret_type indirect_placeholder_183(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
uint64_t bb_get_subexp(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_181_ret_type var_41;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_85;
    uint64_t *var_7;
    uint64_t *var_8;
    uint64_t var_9;
    struct indirect_placeholder_177_ret_type var_10;
    uint64_t var_11;
    uint64_t rbx_0;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t rbp_0;
    uint64_t local_sp_11;
    uint64_t rax_1;
    uint64_t r14_4;
    uint64_t var_64;
    uint64_t r12_0;
    uint64_t var_43;
    uint64_t r13_4;
    uint64_t *_pre_phi201;
    uint64_t var_65;
    uint64_t var_66;
    struct indirect_placeholder_180_ret_type var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t local_sp_0;
    uint64_t var_72;
    uint64_t *var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t *var_76;
    struct indirect_placeholder_179_ret_type var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t var_83;
    uint32_t var_84;
    uint64_t local_sp_9;
    uint64_t local_sp_5;
    uint64_t local_sp_2;
    uint64_t local_sp_4;
    uint64_t rax_0;
    uint64_t var_12;
    uint64_t *var_13;
    uint64_t *var_14;
    uint64_t *var_15;
    uint64_t *var_16;
    uint64_t *var_17;
    uint64_t *var_18;
    uint64_t local_sp_12;
    uint64_t var_19;
    uint64_t local_sp_1;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t *var_22;
    bool var_23;
    uint64_t *var_24;
    uint64_t var_25;
    uint64_t *_pre_phi196;
    uint64_t r13_0;
    uint64_t *_pre_phi199;
    uint64_t r14_0;
    uint64_t var_26;
    uint64_t *var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t _pre_phi203;
    uint64_t var_31;
    uint64_t local_sp_3;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_44;
    uint64_t r13_1;
    uint64_t r12_1;
    uint64_t r14_1;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t *_pre195;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t *var_36;
    uint64_t local_sp_6;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_42;
    uint64_t local_sp_7;
    uint64_t r12_2;
    uint64_t r14_2;
    uint64_t rbx_2;
    uint64_t local_sp_8;
    uint64_t rbx_1;
    uint64_t r13_2;
    uint64_t r12_3;
    uint64_t r14_3;
    uint64_t *var_47;
    uint64_t var_48;
    uint64_t local_sp_10;
    uint64_t r13_3;
    uint64_t var_49;
    uint64_t var_50;
    struct indirect_placeholder_182_ret_type var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    struct indirect_placeholder_183_ret_type var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_88;
    uint64_t *var_89;
    uint64_t var_90;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r13();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    var_7 = (uint64_t *)(var_0 + (-80L));
    *var_7 = rsi;
    var_8 = (uint64_t *)(var_0 + (-88L));
    *var_8 = rdx;
    var_9 = var_0 + (-128L);
    *(uint64_t *)var_9 = 4265971UL;
    var_10 = indirect_placeholder_177(rdi, rdx);
    var_11 = var_10.field_0;
    var_19 = 0UL;
    local_sp_1 = var_9;
    r13_0 = 0UL;
    rax_1 = 0UL;
    if (var_11 != 18446744073709551615UL) {
        rax_0 = (var_11 * 40UL) + *(uint64_t *)(rdi + 216UL);
        while (1U)
            {
                if (*(uint64_t *)rax_0 != rsi) {
                    loop_state_var = 1U;
                    break;
                }
                if (*(unsigned char *)(rax_0 + 32UL) != '\x00') {
                    loop_state_var = 0U;
                    break;
                }
                rax_0 = rax_0 + 40UL;
                continue;
            }
        switch (loop_state_var) {
          case 1U:
            {
                return rax_1;
            }
            break;
          case 0U:
            {
                break;
            }
            break;
        }
    }
    var_12 = *(uint64_t *)(rdi + 152UL);
    *var_7 = var_12;
    *(uint64_t *)(var_0 + (-72L)) = *(uint64_t *)((*var_8 << 4UL) + *(uint64_t *)var_12);
    var_13 = (uint64_t *)(rdi + 232UL);
    if ((long)*var_13 > (long)0UL) {
        return rax_1;
    }
    var_14 = (uint64_t *)(rdi + 8UL);
    *(uint64_t *)(var_0 + (-120L)) = *var_14;
    *(uint64_t *)(var_0 + (-104L)) = 0UL;
    var_15 = (uint64_t *)(rdi + 248UL);
    var_16 = (uint64_t *)(rdi + 48UL);
    var_17 = (uint64_t *)(rdi + 88UL);
    var_18 = (uint64_t *)(rdi + 184UL);
    rax_1 = 12UL;
    while (1U)
        {
            var_20 = *(uint64_t *)((var_19 << 3UL) + *var_15);
            local_sp_2 = local_sp_1;
            local_sp_7 = local_sp_1;
            rbx_1 = var_20;
            local_sp_12 = local_sp_1;
            if (*(uint64_t *)((*(uint64_t *)(var_20 + 8UL) << 4UL) + **(uint64_t **)(local_sp_1 + 48UL)) == *(uint64_t *)(local_sp_1 + 56UL)) {
                var_89 = (uint64_t *)(local_sp_12 + 24UL);
                var_90 = *var_89 + 1UL;
                *var_89 = var_90;
                var_19 = var_90;
                local_sp_1 = local_sp_12;
                rax_1 = 0UL;
                if ((long)*var_13 <= (long)var_90) {
                    continue;
                }
                break;
            }
            var_21 = *(uint64_t *)var_20;
            var_22 = (uint64_t *)(var_20 + 32UL);
            var_23 = ((long)*var_22 > (long)0UL);
            var_24 = (uint64_t *)(local_sp_1 + 32UL);
            var_25 = *var_24;
            r12_0 = var_21;
            _pre_phi196 = var_24;
            r14_0 = var_25;
            r12_2 = var_21;
            r14_2 = var_25;
            if (var_23) {
                local_sp_8 = local_sp_7;
                r12_3 = r12_2;
                r14_3 = r14_2;
                local_sp_12 = local_sp_7;
                if ((long)*_pre_phi196 >= (long)r12_2) {
                    r13_2 = *(uint64_t *)(local_sp_7 + 8UL);
                    while (1U)
                        {
                            var_47 = (uint64_t *)rbx_1;
                            var_48 = helper_cc_compute_all_wrapper(r12_3 - *var_47, 0UL, 0UL, 25U);
                            rbx_0 = rbx_1;
                            r14_4 = r14_3;
                            r13_4 = r13_2;
                            _pre_phi201 = var_47;
                            local_sp_9 = local_sp_8;
                            local_sp_12 = local_sp_8;
                            rbx_2 = rbx_1;
                            local_sp_10 = local_sp_8;
                            r13_3 = r13_2;
                            if ((uint64_t)(((unsigned char)(var_48 >> 4UL) ^ (unsigned char)var_48) & '\xc0') != 0UL) {
                                if ((long)*var_16 <= (long)r14_3) {
                                    if ((long)*var_17 <= (long)r14_3) {
                                        loop_state_var = 3U;
                                        break;
                                    }
                                    var_49 = (uint64_t)((uint32_t)r14_3 + 1U);
                                    var_50 = local_sp_8 + (-8L);
                                    *(uint64_t *)var_50 = 4266420UL;
                                    var_51 = indirect_placeholder_182(rdi, var_49);
                                    var_52 = var_51.field_0;
                                    local_sp_9 = var_50;
                                    rax_1 = var_52;
                                    if ((uint64_t)(uint32_t)var_52 != 0UL) {
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    r13_3 = *var_14;
                                }
                                local_sp_10 = local_sp_9;
                                r13_4 = r13_3;
                                r14_4 = r14_3 + 1UL;
                                local_sp_12 = local_sp_9;
                                if ((uint64_t)(*(unsigned char *)(r14_3 + r13_3) - *(unsigned char *)((r12_3 + r13_3) + (-1L))) != 0UL) {
                                    loop_state_var = 0U;
                                    break;
                                }
                            }
                            var_53 = *(uint64_t *)((r12_3 << 3UL) + *var_18);
                            r13_2 = r13_4;
                            r14_3 = r14_4;
                            local_sp_11 = local_sp_10;
                            var_54 = var_53 + 8UL;
                            var_55 = *(uint64_t *)(local_sp_10 + 56UL);
                            var_56 = *(uint64_t *)(local_sp_10 + 48UL);
                            var_57 = local_sp_10 + (-8L);
                            *(uint64_t *)var_57 = 4266569UL;
                            var_58 = indirect_placeholder_183(9UL, var_55, var_56, var_54);
                            var_59 = var_58.field_0;
                            var_60 = var_58.field_1;
                            var_61 = var_58.field_2;
                            var_62 = var_58.field_3;
                            var_63 = var_58.field_4;
                            local_sp_0 = var_57;
                            rbp_0 = var_59;
                            local_sp_11 = var_57;
                            if (var_53 != 0UL & var_59 != 18446744073709551615UL) {
                                var_64 = *(uint64_t *)(rbx_1 + 16UL);
                                var_71 = var_64;
                                if (var_64 != 0UL) {
                                    var_65 = (r12_3 - *var_47) + 1UL;
                                    var_66 = local_sp_10 + (-16L);
                                    *(uint64_t *)var_66 = 4266454UL;
                                    var_67 = indirect_placeholder_180(rbx_1, var_60, 24UL, var_59, var_65, var_61, var_62, var_63);
                                    var_68 = var_67.field_0;
                                    var_69 = var_67.field_1;
                                    var_70 = var_67.field_3;
                                    *(uint64_t *)(var_69 + 16UL) = var_68;
                                    var_71 = var_68;
                                    local_sp_0 = var_66;
                                    rbx_0 = var_69;
                                    rbp_0 = var_70;
                                    if (var_68 != 0UL) {
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    _pre_phi201 = (uint64_t *)var_69;
                                }
                                var_72 = *(uint64_t *)(rbx_0 + 8UL);
                                var_73 = (uint64_t *)(local_sp_0 + (-16L));
                                *var_73 = 9UL;
                                var_74 = *_pre_phi201;
                                var_75 = local_sp_0 + (-24L);
                                var_76 = (uint64_t *)var_75;
                                *var_76 = 4266620UL;
                                var_77 = indirect_placeholder_179(var_74, var_72, rdi, var_71, r12_3, rbp_0);
                                var_78 = var_77.field_0;
                                var_79 = var_77.field_1;
                                var_80 = var_77.field_2;
                                var_81 = var_77.field_3;
                                var_82 = var_77.field_4;
                                var_83 = local_sp_0 + (-8L);
                                var_84 = (uint32_t)var_78;
                                local_sp_11 = var_83;
                                rbx_2 = rbx_0;
                                rax_1 = var_78;
                                if ((uint64_t)(var_84 + (-1)) != 0UL) {
                                    local_sp_11 = var_75;
                                    if ((uint64_t)var_84 != 0UL) {
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    *var_73 = 4266651UL;
                                    var_85 = indirect_placeholder_129(var_79, r12_3, rbx_0, rbp_0, var_80, var_81, var_82);
                                    rax_1 = 12UL;
                                    if (var_85 != 0UL) {
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    var_86 = *(uint64_t *)(local_sp_0 + 16UL);
                                    var_87 = *(uint64_t *)(local_sp_0 + 24UL);
                                    *var_76 = 4266680UL;
                                    indirect_placeholder_178(var_87, var_85, rdi, rbx_0, var_86);
                                }
                            }
                            var_88 = r12_3 + 1UL;
                            local_sp_8 = local_sp_11;
                            rbx_1 = rbx_2;
                            r12_3 = var_88;
                            local_sp_12 = local_sp_11;
                            if ((long)*(uint64_t *)(local_sp_11 + 32UL) < (long)var_88) {
                                continue;
                            }
                            loop_state_var = 2U;
                            break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 1U:
                        {
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 3U:
                      case 2U:
                      case 0U:
                        {
                            switch (loop_state_var) {
                              case 0U:
                                {
                                    *(uint64_t *)(local_sp_9 + 8UL) = r13_3;
                                }
                                break;
                              case 3U:
                                {
                                    *(uint64_t *)(local_sp_8 + 8UL) = r13_2;
                                }
                                break;
                              case 2U:
                                {
                                    *(uint64_t *)(local_sp_11 + 8UL) = r13_4;
                                }
                                break;
                            }
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            } else {
                while (1U)
                    {
                        var_26 = *(uint64_t *)((r13_0 << 3UL) + *(uint64_t *)(var_20 + 40UL));
                        var_27 = (uint64_t *)local_sp_2;
                        *var_27 = var_26;
                        *(uint64_t *)(local_sp_2 + 16UL) = r12_0;
                        var_28 = *(uint64_t *)(var_26 + 8UL);
                        var_29 = var_28 - r12_0;
                        var_30 = helper_cc_compute_all_wrapper(var_29, 0UL, 0UL, 25U);
                        r12_0 = var_28;
                        local_sp_5 = local_sp_2;
                        _pre_phi199 = var_27;
                        local_sp_3 = local_sp_2;
                        r13_1 = r13_0;
                        r12_1 = var_28;
                        r14_1 = r14_0;
                        local_sp_6 = local_sp_2;
                        if ((uint64_t)(((unsigned char)(var_30 >> 4UL) ^ (unsigned char)var_30) & '\xc0') != 0UL) {
                            _pre_phi203 = r14_0 + var_29;
                            var_37 = *(uint64_t *)(local_sp_6 + 32UL);
                            var_38 = *(uint64_t *)(local_sp_6 + 40UL);
                            var_39 = *_pre_phi199;
                            var_40 = local_sp_6 + (-8L);
                            *(uint64_t *)var_40 = 4266153UL;
                            var_41 = indirect_placeholder_181(var_38, var_39, rdi, var_20, var_37);
                            var_42 = var_41.field_0;
                            *_pre_phi199 = *var_14;
                            rax_1 = var_42;
                            local_sp_2 = var_40;
                            local_sp_4 = var_40;
                            r14_0 = _pre_phi203;
                            r14_1 = _pre_phi203;
                            if ((uint64_t)((uint32_t)var_42 & (-2)) != 0UL) {
                                loop_state_var = 2U;
                                break;
                            }
                            var_43 = r13_0 + 1UL;
                            r13_0 = var_43;
                            r13_1 = var_43;
                            if ((long)*var_22 > (long)var_43) {
                                continue;
                            }
                            loop_state_var = 1U;
                            break;
                        }
                        var_31 = r14_0 + var_29;
                        _pre_phi203 = var_31;
                        if ((long)var_31 > (long)*var_16) {
                        } else {
                            if ((long)var_31 <= (long)*var_17) {
                                loop_state_var = 0U;
                                break;
                            }
                            var_32 = local_sp_2 + (-8L);
                            *(uint64_t *)var_32 = 4266249UL;
                            var_33 = indirect_placeholder_15(rdi, var_31);
                            local_sp_5 = var_32;
                            rax_1 = var_33;
                            if ((uint64_t)(uint32_t)var_33 != 0UL) {
                                loop_state_var = 2U;
                                break;
                            }
                            *var_27 = *var_14;
                        }
                        var_34 = *(uint64_t *)(local_sp_5 + 8UL);
                        var_35 = local_sp_5 + (-8L);
                        var_36 = (uint64_t *)var_35;
                        *var_36 = 4266117UL;
                        indirect_placeholder();
                        local_sp_3 = var_35;
                        _pre_phi199 = var_36;
                        local_sp_6 = var_35;
                        if ((uint64_t)(uint32_t)var_34 != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 2U:
                    {
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 0U:
                    {
                        local_sp_4 = local_sp_3;
                        local_sp_12 = local_sp_3;
                        if ((long)*var_22 <= (long)r13_0) {
                            var_89 = (uint64_t *)(local_sp_12 + 24UL);
                            var_90 = *var_89 + 1UL;
                            *var_89 = var_90;
                            var_19 = var_90;
                            local_sp_1 = local_sp_12;
                            rax_1 = 0UL;
                            if ((long)*var_13 <= (long)var_90) {
                                continue;
                            }
                            switch_state_var = 1;
                            break;
                        }
                        var_44 = *(uint64_t *)(local_sp_3 + 16UL);
                        r12_1 = var_44;
                        var_45 = helper_cc_compute_all_wrapper(r13_1, 0UL, 0UL, 25U);
                        var_46 = r12_1 + ((uint64_t)(((unsigned char)(var_45 >> 4UL) ^ (unsigned char)var_45) & '\xc0') == 0UL);
                        _pre195 = (uint64_t *)(local_sp_4 + 32UL);
                        _pre_phi196 = _pre195;
                        local_sp_7 = local_sp_4;
                        r12_2 = var_46;
                        r14_2 = r14_1;
                        local_sp_8 = local_sp_7;
                        r12_3 = r12_2;
                        r14_3 = r14_2;
                        local_sp_12 = local_sp_7;
                        if ((long)*_pre_phi196 < (long)r12_2) {
                            var_89 = (uint64_t *)(local_sp_12 + 24UL);
                            var_90 = *var_89 + 1UL;
                            *var_89 = var_90;
                            var_19 = var_90;
                            local_sp_1 = local_sp_12;
                            rax_1 = 0UL;
                            if ((long)*var_13 <= (long)var_90) {
                                continue;
                            }
                            switch_state_var = 1;
                            break;
                        }
                        r13_2 = *(uint64_t *)(local_sp_7 + 8UL);
                        while (1U)
                            {
                                var_47 = (uint64_t *)rbx_1;
                                var_48 = helper_cc_compute_all_wrapper(r12_3 - *var_47, 0UL, 0UL, 25U);
                                rbx_0 = rbx_1;
                                r14_4 = r14_3;
                                r13_4 = r13_2;
                                _pre_phi201 = var_47;
                                local_sp_9 = local_sp_8;
                                local_sp_12 = local_sp_8;
                                rbx_2 = rbx_1;
                                local_sp_10 = local_sp_8;
                                r13_3 = r13_2;
                                if ((uint64_t)(((unsigned char)(var_48 >> 4UL) ^ (unsigned char)var_48) & '\xc0') != 0UL) {
                                    if ((long)*var_16 <= (long)r14_3) {
                                        if ((long)*var_17 <= (long)r14_3) {
                                            loop_state_var = 3U;
                                            break;
                                        }
                                        var_49 = (uint64_t)((uint32_t)r14_3 + 1U);
                                        var_50 = local_sp_8 + (-8L);
                                        *(uint64_t *)var_50 = 4266420UL;
                                        var_51 = indirect_placeholder_182(rdi, var_49);
                                        var_52 = var_51.field_0;
                                        local_sp_9 = var_50;
                                        rax_1 = var_52;
                                        if ((uint64_t)(uint32_t)var_52 != 0UL) {
                                            loop_state_var = 1U;
                                            break;
                                        }
                                        r13_3 = *var_14;
                                    }
                                    local_sp_10 = local_sp_9;
                                    r13_4 = r13_3;
                                    r14_4 = r14_3 + 1UL;
                                    local_sp_12 = local_sp_9;
                                    if ((uint64_t)(*(unsigned char *)(r14_3 + r13_3) - *(unsigned char *)((r12_3 + r13_3) + (-1L))) != 0UL) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                }
                                var_53 = *(uint64_t *)((r12_3 << 3UL) + *var_18);
                                r13_2 = r13_4;
                                r14_3 = r14_4;
                                local_sp_11 = local_sp_10;
                                var_54 = var_53 + 8UL;
                                var_55 = *(uint64_t *)(local_sp_10 + 56UL);
                                var_56 = *(uint64_t *)(local_sp_10 + 48UL);
                                var_57 = local_sp_10 + (-8L);
                                *(uint64_t *)var_57 = 4266569UL;
                                var_58 = indirect_placeholder_183(9UL, var_55, var_56, var_54);
                                var_59 = var_58.field_0;
                                var_60 = var_58.field_1;
                                var_61 = var_58.field_2;
                                var_62 = var_58.field_3;
                                var_63 = var_58.field_4;
                                local_sp_0 = var_57;
                                rbp_0 = var_59;
                                local_sp_11 = var_57;
                                if (var_53 != 0UL & var_59 != 18446744073709551615UL) {
                                    var_64 = *(uint64_t *)(rbx_1 + 16UL);
                                    var_71 = var_64;
                                    if (var_64 != 0UL) {
                                        var_65 = (r12_3 - *var_47) + 1UL;
                                        var_66 = local_sp_10 + (-16L);
                                        *(uint64_t *)var_66 = 4266454UL;
                                        var_67 = indirect_placeholder_180(rbx_1, var_60, 24UL, var_59, var_65, var_61, var_62, var_63);
                                        var_68 = var_67.field_0;
                                        var_69 = var_67.field_1;
                                        var_70 = var_67.field_3;
                                        *(uint64_t *)(var_69 + 16UL) = var_68;
                                        var_71 = var_68;
                                        local_sp_0 = var_66;
                                        rbx_0 = var_69;
                                        rbp_0 = var_70;
                                        if (var_68 != 0UL) {
                                            loop_state_var = 1U;
                                            break;
                                        }
                                        _pre_phi201 = (uint64_t *)var_69;
                                    }
                                    var_72 = *(uint64_t *)(rbx_0 + 8UL);
                                    var_73 = (uint64_t *)(local_sp_0 + (-16L));
                                    *var_73 = 9UL;
                                    var_74 = *_pre_phi201;
                                    var_75 = local_sp_0 + (-24L);
                                    var_76 = (uint64_t *)var_75;
                                    *var_76 = 4266620UL;
                                    var_77 = indirect_placeholder_179(var_74, var_72, rdi, var_71, r12_3, rbp_0);
                                    var_78 = var_77.field_0;
                                    var_79 = var_77.field_1;
                                    var_80 = var_77.field_2;
                                    var_81 = var_77.field_3;
                                    var_82 = var_77.field_4;
                                    var_83 = local_sp_0 + (-8L);
                                    var_84 = (uint32_t)var_78;
                                    local_sp_11 = var_83;
                                    rbx_2 = rbx_0;
                                    rax_1 = var_78;
                                    if ((uint64_t)(var_84 + (-1)) != 0UL) {
                                        local_sp_11 = var_75;
                                        if ((uint64_t)var_84 != 0UL) {
                                            loop_state_var = 1U;
                                            break;
                                        }
                                        *var_73 = 4266651UL;
                                        var_85 = indirect_placeholder_129(var_79, r12_3, rbx_0, rbp_0, var_80, var_81, var_82);
                                        rax_1 = 12UL;
                                        if (var_85 != 0UL) {
                                            loop_state_var = 1U;
                                            break;
                                        }
                                        var_86 = *(uint64_t *)(local_sp_0 + 16UL);
                                        var_87 = *(uint64_t *)(local_sp_0 + 24UL);
                                        *var_76 = 4266680UL;
                                        indirect_placeholder_178(var_87, var_85, rdi, rbx_0, var_86);
                                    }
                                }
                                var_88 = r12_3 + 1UL;
                                local_sp_8 = local_sp_11;
                                rbx_1 = rbx_2;
                                r12_3 = var_88;
                                local_sp_12 = local_sp_11;
                                if ((long)*(uint64_t *)(local_sp_11 + 32UL) < (long)var_88) {
                                    continue;
                                }
                                loop_state_var = 2U;
                                break;
                            }
                        switch_state_var = 0;
                        switch (loop_state_var) {
                          case 1U:
                            {
                                switch_state_var = 1;
                                break;
                            }
                            break;
                          case 3U:
                          case 2U:
                          case 0U:
                            {
                                switch (loop_state_var) {
                                  case 2U:
                                    {
                                        *(uint64_t *)(local_sp_11 + 8UL) = r13_4;
                                    }
                                    break;
                                  case 3U:
                                    {
                                        *(uint64_t *)(local_sp_8 + 8UL) = r13_2;
                                    }
                                    break;
                                  case 0U:
                                    {
                                        *(uint64_t *)(local_sp_9 + 8UL) = r13_3;
                                    }
                                    break;
                                }
                            }
                            break;
                        }
                        if (switch_state_var)
                            break;
                    }
                    break;
                  case 1U:
                    {
                        var_45 = helper_cc_compute_all_wrapper(r13_1, 0UL, 0UL, 25U);
                        var_46 = r12_1 + ((uint64_t)(((unsigned char)(var_45 >> 4UL) ^ (unsigned char)var_45) & '\xc0') == 0UL);
                        _pre195 = (uint64_t *)(local_sp_4 + 32UL);
                        _pre_phi196 = _pre195;
                        local_sp_7 = local_sp_4;
                        r12_2 = var_46;
                        r14_2 = r14_1;
                        local_sp_8 = local_sp_7;
                        r12_3 = r12_2;
                        r14_3 = r14_2;
                        local_sp_12 = local_sp_7;
                        if ((long)*_pre_phi196 < (long)r12_2) {
                            var_89 = (uint64_t *)(local_sp_12 + 24UL);
                            var_90 = *var_89 + 1UL;
                            *var_89 = var_90;
                            var_19 = var_90;
                            local_sp_1 = local_sp_12;
                            rax_1 = 0UL;
                            if ((long)*var_13 <= (long)var_90) {
                                continue;
                            }
                            switch_state_var = 1;
                            break;
                        }
                        r13_2 = *(uint64_t *)(local_sp_7 + 8UL);
                        while (1U)
                            {
                                var_47 = (uint64_t *)rbx_1;
                                var_48 = helper_cc_compute_all_wrapper(r12_3 - *var_47, 0UL, 0UL, 25U);
                                rbx_0 = rbx_1;
                                r14_4 = r14_3;
                                r13_4 = r13_2;
                                _pre_phi201 = var_47;
                                local_sp_9 = local_sp_8;
                                local_sp_12 = local_sp_8;
                                rbx_2 = rbx_1;
                                local_sp_10 = local_sp_8;
                                r13_3 = r13_2;
                                if ((uint64_t)(((unsigned char)(var_48 >> 4UL) ^ (unsigned char)var_48) & '\xc0') != 0UL) {
                                    if ((long)*var_16 <= (long)r14_3) {
                                        if ((long)*var_17 <= (long)r14_3) {
                                            loop_state_var = 3U;
                                            break;
                                        }
                                        var_49 = (uint64_t)((uint32_t)r14_3 + 1U);
                                        var_50 = local_sp_8 + (-8L);
                                        *(uint64_t *)var_50 = 4266420UL;
                                        var_51 = indirect_placeholder_182(rdi, var_49);
                                        var_52 = var_51.field_0;
                                        local_sp_9 = var_50;
                                        rax_1 = var_52;
                                        if ((uint64_t)(uint32_t)var_52 != 0UL) {
                                            loop_state_var = 1U;
                                            break;
                                        }
                                        r13_3 = *var_14;
                                    }
                                    local_sp_10 = local_sp_9;
                                    r13_4 = r13_3;
                                    r14_4 = r14_3 + 1UL;
                                    local_sp_12 = local_sp_9;
                                    if ((uint64_t)(*(unsigned char *)(r14_3 + r13_3) - *(unsigned char *)((r12_3 + r13_3) + (-1L))) != 0UL) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                }
                                var_53 = *(uint64_t *)((r12_3 << 3UL) + *var_18);
                                r13_2 = r13_4;
                                r14_3 = r14_4;
                                local_sp_11 = local_sp_10;
                                var_54 = var_53 + 8UL;
                                var_55 = *(uint64_t *)(local_sp_10 + 56UL);
                                var_56 = *(uint64_t *)(local_sp_10 + 48UL);
                                var_57 = local_sp_10 + (-8L);
                                *(uint64_t *)var_57 = 4266569UL;
                                var_58 = indirect_placeholder_183(9UL, var_55, var_56, var_54);
                                var_59 = var_58.field_0;
                                var_60 = var_58.field_1;
                                var_61 = var_58.field_2;
                                var_62 = var_58.field_3;
                                var_63 = var_58.field_4;
                                local_sp_0 = var_57;
                                rbp_0 = var_59;
                                local_sp_11 = var_57;
                                if (var_53 != 0UL & var_59 != 18446744073709551615UL) {
                                    var_64 = *(uint64_t *)(rbx_1 + 16UL);
                                    var_71 = var_64;
                                    if (var_64 != 0UL) {
                                        var_65 = (r12_3 - *var_47) + 1UL;
                                        var_66 = local_sp_10 + (-16L);
                                        *(uint64_t *)var_66 = 4266454UL;
                                        var_67 = indirect_placeholder_180(rbx_1, var_60, 24UL, var_59, var_65, var_61, var_62, var_63);
                                        var_68 = var_67.field_0;
                                        var_69 = var_67.field_1;
                                        var_70 = var_67.field_3;
                                        *(uint64_t *)(var_69 + 16UL) = var_68;
                                        var_71 = var_68;
                                        local_sp_0 = var_66;
                                        rbx_0 = var_69;
                                        rbp_0 = var_70;
                                        if (var_68 != 0UL) {
                                            loop_state_var = 1U;
                                            break;
                                        }
                                        _pre_phi201 = (uint64_t *)var_69;
                                    }
                                    var_72 = *(uint64_t *)(rbx_0 + 8UL);
                                    var_73 = (uint64_t *)(local_sp_0 + (-16L));
                                    *var_73 = 9UL;
                                    var_74 = *_pre_phi201;
                                    var_75 = local_sp_0 + (-24L);
                                    var_76 = (uint64_t *)var_75;
                                    *var_76 = 4266620UL;
                                    var_77 = indirect_placeholder_179(var_74, var_72, rdi, var_71, r12_3, rbp_0);
                                    var_78 = var_77.field_0;
                                    var_79 = var_77.field_1;
                                    var_80 = var_77.field_2;
                                    var_81 = var_77.field_3;
                                    var_82 = var_77.field_4;
                                    var_83 = local_sp_0 + (-8L);
                                    var_84 = (uint32_t)var_78;
                                    local_sp_11 = var_83;
                                    rbx_2 = rbx_0;
                                    rax_1 = var_78;
                                    if ((uint64_t)(var_84 + (-1)) != 0UL) {
                                        local_sp_11 = var_75;
                                        if ((uint64_t)var_84 != 0UL) {
                                            loop_state_var = 1U;
                                            break;
                                        }
                                        *var_73 = 4266651UL;
                                        var_85 = indirect_placeholder_129(var_79, r12_3, rbx_0, rbp_0, var_80, var_81, var_82);
                                        rax_1 = 12UL;
                                        if (var_85 != 0UL) {
                                            loop_state_var = 1U;
                                            break;
                                        }
                                        var_86 = *(uint64_t *)(local_sp_0 + 16UL);
                                        var_87 = *(uint64_t *)(local_sp_0 + 24UL);
                                        *var_76 = 4266680UL;
                                        indirect_placeholder_178(var_87, var_85, rdi, rbx_0, var_86);
                                    }
                                }
                                var_88 = r12_3 + 1UL;
                                local_sp_8 = local_sp_11;
                                rbx_1 = rbx_2;
                                r12_3 = var_88;
                                local_sp_12 = local_sp_11;
                                if ((long)*(uint64_t *)(local_sp_11 + 32UL) < (long)var_88) {
                                    continue;
                                }
                                loop_state_var = 2U;
                                break;
                            }
                        switch_state_var = 0;
                        switch (loop_state_var) {
                          case 1U:
                            {
                                switch_state_var = 1;
                                break;
                            }
                            break;
                          case 3U:
                          case 2U:
                          case 0U:
                            {
                                switch (loop_state_var) {
                                  case 2U:
                                    {
                                        *(uint64_t *)(local_sp_11 + 8UL) = r13_4;
                                    }
                                    break;
                                  case 3U:
                                    {
                                        *(uint64_t *)(local_sp_8 + 8UL) = r13_2;
                                    }
                                    break;
                                  case 0U:
                                    {
                                        *(uint64_t *)(local_sp_9 + 8UL) = r13_3;
                                    }
                                    break;
                                }
                            }
                            break;
                        }
                        if (switch_state_var)
                            break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
        }
    return rax_1;
}
