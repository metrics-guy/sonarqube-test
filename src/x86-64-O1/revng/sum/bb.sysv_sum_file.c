typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_r12(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r9(void);
extern uint32_t init_state_0x82fc(void);
extern uint64_t init_r8(void);
extern void indirect_placeholder_3(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
void bb_sysv_sum_file(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint32_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t local_sp_1;
    uint64_t local_sp_0;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t rdx_0;
    uint64_t rbp_0;
    uint64_t rbx_0;
    uint64_t rsi2_0;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t local_sp_3;
    uint64_t rbx_1;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t cc_src_0;
    uint64_t cc_dst_0;
    uint32_t cc_op_0;
    uint64_t rdi1_0;
    uint64_t rcx_0;
    uint64_t cc_src_1;
    uint64_t cc_dst_1;
    uint64_t var_14;
    uint64_t var_15;
    uint32_t cc_op_1;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    bool var_19;
    uint64_t local_sp_2;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_20;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_cc_src2();
    var_8 = init_r9();
    var_9 = init_r8();
    var_10 = init_state_0x82fc();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_11 = var_0 + (-8456L);
    var_12 = (uint64_t)(uint32_t)rsi;
    var_13 = (uint64_t)var_10;
    rbp_0 = 0UL;
    rsi2_0 = rdi;
    rbx_1 = 0UL;
    cc_src_0 = 8408UL;
    cc_dst_0 = var_11;
    cc_op_0 = 17U;
    rdi1_0 = 4261647UL;
    rcx_0 = 2UL;
    local_sp_2 = var_11;
    cc_op_0 = 14U;
    cc_src_1 = cc_src_0;
    cc_dst_1 = cc_dst_0;
    cc_op_1 = cc_op_0;
    while (rcx_0 != 0UL)
        {
            rdi1_0 = rdi1_0 + var_13;
            rsi2_0 = rsi2_0 + var_13;
            rcx_0 = rcx_0 + (-1L);
            cc_op_0 = 14U;
            cc_src_1 = cc_src_0;
            cc_dst_1 = cc_dst_0;
            cc_op_1 = cc_op_0;
        }
    var_16 = helper_cc_compute_all_wrapper(cc_dst_1, cc_src_1, var_7, cc_op_1);
    var_17 = ((var_16 & 65UL) == 0UL);
    var_18 = helper_cc_compute_c_wrapper(cc_dst_1, var_16, var_7, 1U);
    var_19 = ((((uint32_t)var_17 - (uint32_t)var_18) << 24U) == 0U);
    if (var_19) {
        *(unsigned char *)4277040UL = (unsigned char)'\x01';
    } else {
        var_20 = var_0 + (-8464L);
        *(uint64_t *)var_20 = 4204625UL;
        indirect_placeholder();
        local_sp_2 = var_20;
    }
    local_sp_3 = local_sp_2;
    while (1U)
        {
            var_21 = local_sp_3 + 208UL;
            var_22 = local_sp_3 + (-8L);
            *(uint64_t *)var_22 = 4204801UL;
            var_23 = indirect_placeholder_4(8192UL, 0UL, var_21);
            rbx_0 = rbx_1;
            local_sp_1 = var_22;
            local_sp_3 = var_22;
            switch_state_var = 0;
            switch (var_23) {
              case 18446744073709551615UL:
                {
                    *(uint64_t *)(local_sp_3 + (-16L)) = 4204712UL;
                    var_24 = indirect_placeholder_4(rdi, 0UL, 3UL);
                    *(uint64_t *)(local_sp_3 + (-24L)) = 4204720UL;
                    indirect_placeholder();
                    var_25 = (uint64_t)*(uint32_t *)var_24;
                    *(uint64_t *)(local_sp_3 + (-32L)) = 4204745UL;
                    indirect_placeholder_3(0UL, 4256725UL, 0UL, var_25, var_24, var_8, var_9);
                    if (!var_19) {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    *(uint64_t *)(local_sp_3 + (-40L)) = 4204767UL;
                    indirect_placeholder();
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 0UL:
                {
                    if (!var_19) {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    var_26 = local_sp_3 + (-16L);
                    *(uint64_t *)var_26 = 4204853UL;
                    indirect_placeholder();
                    local_sp_1 = var_26;
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
              default:
                {
                    var_29 = local_sp_3 + 200UL;
                    var_30 = var_29 + var_23;
                    rdx_0 = var_29;
                    var_31 = (uint64_t)((uint32_t)rbx_0 + (uint32_t)(uint64_t)*(unsigned char *)rdx_0);
                    var_32 = rdx_0 + 1UL;
                    rdx_0 = var_32;
                    rbx_0 = var_31;
                    rbx_1 = var_31;
                    do {
                        var_31 = (uint64_t)((uint32_t)rbx_0 + (uint32_t)(uint64_t)*(unsigned char *)rdx_0);
                        var_32 = rdx_0 + 1UL;
                        rdx_0 = var_32;
                        rbx_0 = var_31;
                        rbx_1 = var_31;
                    } while (var_32 != var_30);
                    rbp_0 = rbp_0 + var_23;
                    continue;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            return;
        }
        break;
      case 1U:
        {
            *(uint64_t *)(local_sp_1 + (-8L)) = 4204900UL;
            indirect_placeholder_7(0UL, rbp_0, local_sp_1, 1UL, 512UL);
            var_27 = local_sp_1 + (-16L);
            *(uint64_t *)var_27 = 4204920UL;
            indirect_placeholder();
            local_sp_0 = var_27;
            if (var_12 != 0UL) {
                var_28 = local_sp_1 + (-24L);
                *(uint64_t *)var_28 = 4205034UL;
                indirect_placeholder();
                local_sp_0 = var_28;
            }
            *(uint64_t *)(local_sp_0 + (-8L)) = 4204935UL;
            indirect_placeholder();
        }
        break;
    }
}
