typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_152_ret_type;
struct indirect_placeholder_150_ret_type;
struct indirect_placeholder_153_ret_type;
struct indirect_placeholder_151_ret_type;
struct indirect_placeholder_154_ret_type;
struct indirect_placeholder_149_ret_type;
struct indirect_placeholder_152_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_150_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_153_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_151_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_154_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_149_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint32_t init_state_0x82fc(void);
extern uint64_t init_cc_src(void);
extern uint64_t indirect_placeholder_124(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_113(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint64_t init_cc_dst(void);
extern uint32_t init_cc_op(void);
extern struct indirect_placeholder_152_ret_type indirect_placeholder_152(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_150_ret_type indirect_placeholder_150(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_153_ret_type indirect_placeholder_153(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_151_ret_type indirect_placeholder_151(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_154_ret_type indirect_placeholder_154(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_149_ret_type indirect_placeholder_149(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
uint64_t bb_touch(uint64_t rdi) {
    uint64_t var_36;
    uint64_t var_49;
    uint64_t var_41;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint32_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint32_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    unsigned char var_54;
    uint64_t var_55;
    uint64_t local_sp_4;
    uint64_t local_sp_2;
    uint64_t local_sp_3;
    uint64_t var_50;
    uint64_t rax_1;
    struct indirect_placeholder_152_ret_type var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_43;
    uint64_t rbp_0;
    uint64_t rax_0;
    uint64_t r14_0;
    uint64_t local_sp_0;
    uint64_t local_sp_1;
    unsigned char var_51;
    uint64_t var_52;
    uint64_t var_53;
    struct indirect_placeholder_153_ret_type var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    struct indirect_placeholder_154_ret_type var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t rbx_0;
    uint32_t var_28;
    uint64_t var_27;
    uint64_t r13_0;
    uint64_t var_26;
    uint64_t var_25;
    uint64_t var_23;
    uint32_t *var_24;
    uint64_t cc_src_0;
    uint64_t cc_dst_0;
    uint32_t cc_op_0;
    uint64_t rdi1_0;
    uint64_t rsi_0;
    uint64_t rcx_0;
    uint64_t cc_src_1;
    uint64_t cc_dst_1;
    uint64_t var_13;
    uint64_t var_14;
    uint32_t cc_op_1;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint32_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint32_t var_21;
    uint64_t var_22;
    uint32_t rbx_0_shrunk;
    uint64_t local_sp_6;
    uint64_t local_sp_5;
    uint64_t var_29;
    uint64_t var_30;
    uint32_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    bool var_38;
    uint64_t var_39;
    uint64_t *var_40;
    uint64_t var_42;
    uint64_t var_35;
    uint64_t var_37;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_cc_src2();
    var_2 = init_cc_src();
    var_3 = init_cc_dst();
    var_4 = init_cc_op();
    var_5 = init_r13();
    var_6 = init_rbx();
    var_7 = init_rbp();
    var_8 = init_r12();
    var_9 = init_r14();
    var_10 = init_state_0x82fc();
    *(uint64_t *)(var_0 + (-8L)) = var_9;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    *(uint64_t *)(var_0 + (-24L)) = var_8;
    *(uint64_t *)(var_0 + (-32L)) = var_7;
    var_11 = var_0 + (-40L);
    *(uint64_t *)var_11 = var_6;
    var_12 = (uint64_t)var_10;
    local_sp_4 = var_11;
    r13_0 = 1UL;
    cc_src_0 = var_2;
    cc_dst_0 = var_3;
    cc_op_0 = var_4;
    rdi1_0 = 4287836UL;
    rsi_0 = rdi;
    rcx_0 = 2UL;
    cc_op_0 = 14U;
    cc_src_1 = cc_src_0;
    cc_dst_1 = cc_dst_0;
    cc_op_1 = cc_op_0;
    while (rcx_0 != 0UL)
        {
            rdi1_0 = rdi1_0 + var_12;
            rsi_0 = rsi_0 + var_12;
            rcx_0 = rcx_0 + (-1L);
            cc_op_0 = 14U;
            cc_src_1 = cc_src_0;
            cc_dst_1 = cc_dst_0;
            cc_op_1 = cc_op_0;
        }
    var_15 = helper_cc_compute_all_wrapper(cc_dst_1, cc_src_1, var_1, cc_op_1);
    var_16 = ((var_15 & 65UL) == 0UL);
    var_17 = helper_cc_compute_c_wrapper(cc_dst_1, var_15, var_1, 1U);
    var_18 = ((uint32_t)var_16 - (uint32_t)var_17) << 24U;
    rbx_0_shrunk = (uint32_t)((int)var_18 >> (int)24U);
    r13_0 = 4294967295UL;
    rbx_0_shrunk = 0U;
    var_19 = var_0 + (-48L);
    *(uint64_t *)var_19 = 4204867UL;
    var_20 = indirect_placeholder_113(2369UL, 0UL, rdi, 438UL);
    var_21 = (uint32_t)var_20;
    var_22 = (uint64_t)var_21;
    r13_0 = var_22;
    local_sp_4 = var_19;
    var_23 = var_0 + (-56L);
    *(uint64_t *)var_23 = 4204880UL;
    indirect_placeholder();
    var_24 = (uint32_t *)var_20;
    local_sp_4 = var_23;
    var_25 = var_0 + (-64L);
    *(uint64_t *)var_25 = 4204890UL;
    indirect_placeholder();
    local_sp_4 = var_25;
    var_26 = var_0 + (-72L);
    *(uint64_t *)var_26 = 4204900UL;
    indirect_placeholder();
    local_sp_4 = var_26;
    if (var_18 != 0U & *(unsigned char *)4322307UL != '\x00' & *(unsigned char *)4322305UL != '\x00' & (uint64_t)(var_21 + 1U) != 0UL & *var_24 != 21U & *var_24 != 22U & *var_24 == 1U) {
        var_27 = var_0 + (-80L);
        *(uint64_t *)var_27 = 4204910UL;
        indirect_placeholder();
        rbx_0_shrunk = *var_24;
        local_sp_4 = var_27;
    }
    rbx_0 = (uint64_t)rbx_0_shrunk;
    var_28 = *(uint32_t *)4322308UL;
    local_sp_5 = local_sp_4;
    local_sp_6 = local_sp_4;
    if ((uint64_t)(var_28 + (-3)) != 0UL) {
        if ((uint64_t)(var_28 + (-2)) == 0UL) {
            *(uint64_t *)4322280UL = 1073741822UL;
        } else {
            if ((uint64_t)(var_28 + (-1)) != 0UL) {
                var_29 = local_sp_4 + (-8L);
                *(uint64_t *)var_29 = 4205208UL;
                indirect_placeholder();
                local_sp_5 = var_29;
            }
            *(uint64_t *)4322296UL = 1073741822UL;
            local_sp_6 = local_sp_5;
        }
    }
    var_30 = (*(unsigned char *)4322304UL == '\x00') ? 4322272UL : 0UL;
    var_31 = (uint32_t)r13_0;
    var_32 = var_7 & (-256L);
    var_33 = ((uint64_t)(var_31 + 1U) == 0UL) & (uint64_t)*(unsigned char *)4322305UL;
    var_34 = var_32 | var_33;
    rbp_0 = var_34;
    if (var_33 != 0UL) {
        var_35 = local_sp_6 + (-8L);
        *(uint64_t *)var_35 = 4205408UL;
        var_36 = indirect_placeholder_124(rdi, r13_0, 4294967196UL, var_30, 256UL);
        var_37 = (uint64_t)(uint32_t)var_36;
        rax_0 = var_36;
        r14_0 = var_37;
        local_sp_0 = var_35;
        rax_1 = rax_0;
        local_sp_1 = local_sp_0;
        rbp_0 = 1UL;
        if (r14_0 == 0UL) {
            return (uint64_t)(uint32_t)rbp_0;
        }
        local_sp_2 = local_sp_1;
        local_sp_3 = local_sp_1;
        if (rbx_0_shrunk == 0U) {
            *(uint64_t *)(local_sp_3 + (-8L)) = 4205351UL;
            var_56 = indirect_placeholder_154(4UL, rdi);
            var_57 = var_56.field_0;
            var_58 = var_56.field_1;
            var_59 = var_56.field_2;
            *(uint64_t *)(local_sp_3 + (-16L)) = 4205376UL;
            indirect_placeholder_149(0UL, 4286434UL, 0UL, rbx_0, var_57, var_58, var_59);
            rbp_0 = 0UL;
            return (uint64_t)(uint32_t)rbp_0;
        }
        var_51 = *(unsigned char *)4322307UL;
        if (var_51 != '\x00') {
            var_52 = (uint64_t)var_51;
            var_53 = local_sp_1 + (-8L);
            *(uint64_t *)var_53 = 4205100UL;
            indirect_placeholder();
            local_sp_2 = var_53;
            rbp_0 = var_52;
            if (*(uint32_t *)rax_1 == 2U) {
                return (uint64_t)(uint32_t)rbp_0;
            }
        }
        *(uint64_t *)(local_sp_2 + (-8L)) = 4205118UL;
        var_60 = indirect_placeholder_153(4UL, rdi);
        var_61 = var_60.field_0;
        var_62 = var_60.field_1;
        var_63 = var_60.field_2;
        *(uint64_t *)(local_sp_2 + (-16L)) = 4205126UL;
        indirect_placeholder();
        var_64 = (uint64_t)*(uint32_t *)var_61;
        *(uint64_t *)(local_sp_2 + (-24L)) = 4205151UL;
        indirect_placeholder_151(0UL, 4286450UL, 0UL, var_64, var_61, var_62, var_63);
        rbp_0 = 0UL;
        return (uint64_t)(uint32_t)rbp_0;
    }
    var_38 = ((uint64_t)(var_31 + (-1)) == 0UL);
    var_39 = local_sp_6 + (-8L);
    var_40 = (uint64_t *)var_39;
    local_sp_0 = var_39;
    if (var_38) {
        *var_40 = 4205287UL;
        var_49 = indirect_placeholder_124(0UL, 1UL, 4294967196UL, var_30, 0UL);
        rax_1 = var_49;
        rbp_0 = 1UL;
        if ((uint64_t)(uint32_t)var_49 == 0UL) {
            return (uint64_t)(uint32_t)rbp_0;
        }
        var_50 = local_sp_6 + (-16L);
        *(uint64_t *)var_50 = 4205305UL;
        indirect_placeholder();
        local_sp_1 = var_50;
        local_sp_2 = var_50;
        local_sp_3 = var_50;
        if (*(uint32_t *)var_49 == 9U) {
            var_54 = *(unsigned char *)4322307UL;
            var_55 = (uint64_t)var_54;
            rbp_0 = var_55;
            if (var_54 == '\x00') {
                return (uint64_t)(uint32_t)rbp_0;
            }
            if (rbx_0_shrunk == 0U) {
                *(uint64_t *)(local_sp_3 + (-8L)) = 4205351UL;
                var_56 = indirect_placeholder_154(4UL, rdi);
                var_57 = var_56.field_0;
                var_58 = var_56.field_1;
                var_59 = var_56.field_2;
                *(uint64_t *)(local_sp_3 + (-16L)) = 4205376UL;
                indirect_placeholder_149(0UL, 4286434UL, 0UL, rbx_0, var_57, var_58, var_59);
                rbp_0 = 0UL;
                return (uint64_t)(uint32_t)rbp_0;
            }
        }
        local_sp_2 = local_sp_1;
        local_sp_3 = local_sp_1;
        if (rbx_0_shrunk != 0U) {
            *(uint64_t *)(local_sp_3 + (-8L)) = 4205351UL;
            var_56 = indirect_placeholder_154(4UL, rdi);
            var_57 = var_56.field_0;
            var_58 = var_56.field_1;
            var_59 = var_56.field_2;
            *(uint64_t *)(local_sp_3 + (-16L)) = 4205376UL;
            indirect_placeholder_149(0UL, 4286434UL, 0UL, rbx_0, var_57, var_58, var_59);
            rbp_0 = 0UL;
            return (uint64_t)(uint32_t)rbp_0;
        }
        var_51 = *(unsigned char *)4322307UL;
        if (var_51 != '\x00') {
            var_52 = (uint64_t)var_51;
            var_53 = local_sp_1 + (-8L);
            *(uint64_t *)var_53 = 4205100UL;
            indirect_placeholder();
            local_sp_2 = var_53;
            rbp_0 = var_52;
            if (*(uint32_t *)rax_1 == 2U) {
                return (uint64_t)(uint32_t)rbp_0;
            }
        }
        *(uint64_t *)(local_sp_2 + (-8L)) = 4205118UL;
        var_60 = indirect_placeholder_153(4UL, rdi);
        var_61 = var_60.field_0;
        var_62 = var_60.field_1;
        var_63 = var_60.field_2;
        *(uint64_t *)(local_sp_2 + (-16L)) = 4205126UL;
        indirect_placeholder();
        var_64 = (uint64_t)*(uint32_t *)var_61;
        *(uint64_t *)(local_sp_2 + (-24L)) = 4205151UL;
        indirect_placeholder_151(0UL, 4286450UL, 0UL, var_64, var_61, var_62, var_63);
        rbp_0 = 0UL;
        return (uint64_t)(uint32_t)rbp_0;
    }
    *var_40 = 4205039UL;
    var_41 = indirect_placeholder_124(rdi, r13_0, 4294967196UL, var_30, 0UL);
    var_42 = (uint64_t)(uint32_t)var_41;
    rax_0 = var_41;
    r14_0 = var_42;
    if (r13_0 != 0UL) {
        var_43 = local_sp_6 + (-16L);
        *(uint64_t *)var_43 = 4205057UL;
        indirect_placeholder();
        r14_0 = 0UL;
        local_sp_0 = var_43;
        if (var_42 == 0UL) {
            *(uint64_t *)(local_sp_6 + (-24L)) = 4205226UL;
            var_44 = indirect_placeholder_152(4UL, rdi);
            var_45 = var_44.field_0;
            var_46 = var_44.field_1;
            var_47 = var_44.field_2;
            *(uint64_t *)(local_sp_6 + (-32L)) = 4205234UL;
            indirect_placeholder();
            var_48 = (uint64_t)*(uint32_t *)var_45;
            *(uint64_t *)(local_sp_6 + (-40L)) = 4205259UL;
            indirect_placeholder_150(0UL, 4286415UL, 0UL, var_48, var_45, var_46, var_47);
            return (uint64_t)(uint32_t)rbp_0;
        }
    }
    rax_1 = rax_0;
    local_sp_1 = local_sp_0;
    rbp_0 = 1UL;
    if (r14_0 == 0UL) {
        return (uint64_t)(uint32_t)rbp_0;
    }
    local_sp_2 = local_sp_1;
    local_sp_3 = local_sp_1;
    if (rbx_0_shrunk == 0U) {
        *(uint64_t *)(local_sp_3 + (-8L)) = 4205351UL;
        var_56 = indirect_placeholder_154(4UL, rdi);
        var_57 = var_56.field_0;
        var_58 = var_56.field_1;
        var_59 = var_56.field_2;
        *(uint64_t *)(local_sp_3 + (-16L)) = 4205376UL;
        indirect_placeholder_149(0UL, 4286434UL, 0UL, rbx_0, var_57, var_58, var_59);
        rbp_0 = 0UL;
        return (uint64_t)(uint32_t)rbp_0;
    }
    var_51 = *(unsigned char *)4322307UL;
    if (var_51 != '\x00') {
        var_52 = (uint64_t)var_51;
        var_53 = local_sp_1 + (-8L);
        *(uint64_t *)var_53 = 4205100UL;
        indirect_placeholder();
        local_sp_2 = var_53;
        rbp_0 = var_52;
        if (*(uint32_t *)rax_1 == 2U) {
            return (uint64_t)(uint32_t)rbp_0;
        }
    }
    *(uint64_t *)(local_sp_2 + (-8L)) = 4205118UL;
    var_60 = indirect_placeholder_153(4UL, rdi);
    var_61 = var_60.field_0;
    var_62 = var_60.field_1;
    var_63 = var_60.field_2;
    *(uint64_t *)(local_sp_2 + (-16L)) = 4205126UL;
    indirect_placeholder();
    var_64 = (uint64_t)*(uint32_t *)var_61;
    *(uint64_t *)(local_sp_2 + (-24L)) = 4205151UL;
    indirect_placeholder_151(0UL, 4286450UL, 0UL, var_64, var_61, var_62, var_63);
    rbp_0 = 0UL;
    return (uint64_t)(uint32_t)rbp_0;
}
