typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_35_ret_type;
struct helper_divq_EAX_wrapper_ret_type;
struct type_6;
struct indirect_placeholder_36_ret_type;
struct indirect_placeholder_37_ret_type;
struct indirect_placeholder_39_ret_type;
struct indirect_placeholder_40_ret_type;
struct indirect_placeholder_41_ret_type;
struct indirect_placeholder_38_ret_type;
struct indirect_placeholder_42_ret_type;
struct indirect_placeholder_35_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct helper_divq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint32_t field_4;
    uint32_t field_5;
    uint32_t field_6;
    uint64_t field_7;
    uint32_t field_8;
    uint64_t field_9;
    uint32_t field_10;
    unsigned char field_11;
    uint32_t field_12;
};
struct type_6 {
};
struct indirect_placeholder_36_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_37_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_39_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_40_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_41_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_38_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_42_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern struct indirect_placeholder_35_ret_type indirect_placeholder_35(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_r13(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r15(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_2(uint64_t param_0, uint64_t param_1);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint32_t init_state_0x8248(void);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern struct helper_divq_EAX_wrapper_ret_type helper_divq_EAX_wrapper(struct type_6 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint32_t param_13, uint64_t param_14, uint32_t param_15, uint64_t param_16, uint64_t param_17, uint64_t param_18, uint32_t param_19);
extern struct indirect_placeholder_36_ret_type indirect_placeholder_36(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_37_ret_type indirect_placeholder_37(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_39_ret_type indirect_placeholder_39(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_40_ret_type indirect_placeholder_40(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_41_ret_type indirect_placeholder_41(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_38_ret_type indirect_placeholder_38(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_42_ret_type indirect_placeholder_42(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
uint64_t bb_tac_seekable(uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t rcx, uint64_t r10, uint64_t r9, uint64_t r8) {
    uint64_t r15_3;
    uint64_t *_pre_phi;
    uint64_t rcx4_0;
    uint64_t r87_5;
    uint64_t r96_0;
    uint64_t r87_0;
    uint64_t var_124;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint32_t var_8;
    uint64_t var_9;
    uint32_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint32_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    unsigned char var_19;
    uint64_t var_20;
    uint64_t var_21;
    struct helper_divq_EAX_wrapper_ret_type var_22;
    uint64_t var_23;
    uint64_t r15_5_ph;
    uint64_t rcx4_4;
    uint64_t local_sp_9;
    uint64_t var_131;
    uint64_t var_132;
    uint64_t var_133;
    uint64_t var_134;
    uint64_t rax_5;
    uint64_t local_sp_0;
    uint64_t var_125;
    uint64_t var_126;
    uint64_t var_127;
    uint64_t var_128;
    uint64_t var_129;
    uint64_t var_130;
    uint64_t var_118;
    uint64_t var_119;
    uint64_t var_120;
    uint64_t var_121;
    uint64_t *var_122;
    struct indirect_placeholder_36_ret_type var_123;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_73;
    struct indirect_placeholder_37_ret_type var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t rcx4_3_ph;
    uint64_t local_sp_11;
    uint64_t local_sp_1;
    uint64_t r14_4;
    uint64_t var_54;
    uint64_t r15_4;
    uint64_t r15_1;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t r87_7;
    uint64_t local_sp_14;
    struct indirect_placeholder_39_ret_type var_44;
    uint64_t rcx4_7;
    uint64_t var_39;
    uint64_t rcx4_8;
    uint64_t r15_0;
    uint64_t rdx1_0;
    uint64_t r14_0;
    uint64_t rax_0;
    uint64_t local_sp_2;
    uint64_t r14_1;
    uint64_t rax_1;
    uint64_t local_sp_3;
    uint64_t var_55;
    bool var_56;
    uint64_t var_57;
    uint64_t rbx_0;
    uint64_t rax_2;
    uint64_t local_sp_8;
    uint64_t var_85;
    bool var_58;
    uint64_t r15_2_ph;
    uint64_t r14_2_ph;
    uint64_t rcx4_1_ph;
    uint64_t rbx_1_ph;
    uint64_t r96_1_ph;
    uint64_t r87_1_ph;
    uint64_t rax_3_ph;
    uint64_t local_sp_4_ph;
    uint64_t rbx_1_be;
    uint64_t var_59;
    uint64_t rcx4_6;
    uint64_t r14_2;
    uint64_t rax_6;
    uint64_t rcx4_1;
    uint64_t rax_3_be;
    uint64_t rbx_1;
    uint64_t r96_1;
    uint64_t r87_1;
    uint64_t rax_3;
    uint64_t local_sp_4;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t rsi3_0;
    uint64_t rcx4_2;
    uint64_t r96_2;
    uint64_t r87_2;
    uint64_t local_sp_5;
    uint64_t var_81;
    uint64_t r87_3;
    uint64_t local_sp_6;
    uint64_t var_66;
    uint64_t var_67;
    struct indirect_placeholder_40_ret_type var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_64;
    struct indirect_placeholder_41_ret_type var_65;
    uint64_t rbx_2_ph;
    uint64_t r96_3_ph;
    uint64_t r87_4_ph;
    uint64_t rax_4_ph;
    uint64_t local_sp_7_ph;
    bool var_78;
    uint64_t rbx_2_ph237;
    uint64_t local_sp_7_ph238;
    uint64_t rbx_2;
    uint64_t var_79;
    uint64_t rbx_3;
    uint64_t var_80;
    uint64_t r96_4;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t rcx4_5;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t var_107;
    uint64_t var_108;
    uint64_t var_109;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t var_112;
    uint64_t var_113;
    uint64_t var_114;
    uint64_t var_115;
    uint64_t var_116;
    uint64_t *var_117;
    uint64_t var_135;
    uint64_t var_86;
    unsigned char *var_87;
    unsigned char var_88;
    uint64_t var_89;
    bool var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t r14_3;
    uint64_t local_sp_10;
    uint64_t var_100;
    uint64_t var_101;
    uint64_t var_94;
    uint64_t var_95;
    unsigned char var_96;
    uint64_t var_97;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_46;
    uint64_t local_sp_12;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t r96_5;
    uint64_t r87_6;
    uint64_t local_sp_13;
    uint64_t var_45;
    uint64_t var_32;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    struct indirect_placeholder_42_ret_type var_31;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t rcx4_8_ph;
    uint64_t rbx_4_ph_in_in_in;
    uint64_t r96_6_ph;
    uint64_t r87_7_ph;
    uint64_t local_sp_14_ph;
    uint64_t rbx_4_ph;
    uint64_t r15_5;
    uint64_t r96_6;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    unsigned char var_36;
    uint64_t var_37;
    unsigned char var_38;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r15();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_cc_src2();
    var_8 = init_state_0x8248();
    var_9 = init_state_0x9018();
    var_10 = init_state_0x9010();
    var_11 = init_state_0x8408();
    var_12 = init_state_0x8328();
    var_13 = init_state_0x82d8();
    var_14 = init_state_0x9080();
    *(uint64_t *)(var_0 + (-8L)) = var_4;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_1;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_15 = var_0 + (-88L);
    var_16 = var_0 + (-64L);
    *(uint32_t *)var_16 = (uint32_t)rdi;
    var_17 = var_0 + (-72L);
    *(uint64_t *)var_17 = rsi;
    var_18 = *(uint64_t *)4359664UL;
    var_19 = *(unsigned char *)var_18;
    var_20 = (uint64_t)var_19;
    *(uint64_t *)var_15 = (var_18 + 1UL);
    var_21 = *(uint64_t *)4359640UL;
    var_22 = helper_divq_EAX_wrapper((struct type_6 *)(0UL), *(uint64_t *)4359624UL, 4206110UL, rdx, 0UL, var_20, var_3, rdi, rsi, rcx, r10, r9, r8, var_8, var_9, var_10, var_11, var_12, var_13, var_14);
    var_23 = var_22.field_2;
    r15_5_ph = rdx;
    rcx4_8_ph = rcx;
    rbx_4_ph_in_in_in = var_16;
    r96_6_ph = r9;
    r87_7_ph = r8;
    local_sp_14_ph = var_15;
    var_24 = var_22.field_1;
    var_25 = rdx - var_23;
    var_26 = var_0 + (-96L);
    *(uint64_t *)var_26 = 4206142UL;
    indirect_placeholder();
    r15_5_ph = var_25;
    rbx_4_ph_in_in_in = var_17;
    local_sp_14_ph = var_26;
    if (var_23 != 0UL & (long)var_24 > (long)18446744073709551615UL) {
        var_27 = *(uint64_t *)(var_0 + (-80L));
        *(uint64_t *)(var_0 + (-104L)) = 4206171UL;
        var_28 = indirect_placeholder_4(var_27, 0UL, 3UL);
        *(uint64_t *)(var_0 + (-112L)) = 4206179UL;
        indirect_placeholder();
        var_29 = (uint64_t)*(uint32_t *)var_28;
        var_30 = var_0 + (-120L);
        *(uint64_t *)var_30 = 4206204UL;
        var_31 = indirect_placeholder_42(0UL, 4314220UL, 0UL, var_29, var_28, r9, r8);
        rcx4_8_ph = var_31.field_1;
        rbx_4_ph_in_in_in = var_26;
        r96_6_ph = var_31.field_2;
        r87_7_ph = var_31.field_3;
        local_sp_14_ph = var_30;
    }
    rbx_4_ph = (uint64_t)*(uint32_t *)rbx_4_ph_in_in_in;
    r87_7 = r87_7_ph;
    local_sp_14 = local_sp_14_ph;
    rcx4_8 = rcx4_8_ph;
    var_32 = *(uint64_t *)4359624UL;
    r15_5 = r15_5_ph;
    r96_6 = r96_6_ph;
    var_33 = *(uint64_t *)4359632UL;
    var_34 = local_sp_14 + (-8L);
    *(uint64_t *)var_34 = 4206238UL;
    var_35 = indirect_placeholder_4(var_32, rbx_4_ph, var_33);
    var_36 = (unsigned char)(r15_5 != 0UL);
    var_37 = (rcx4_8 & (-256L)) | (var_35 == 0UL);
    var_38 = var_36 & (unsigned char)var_37;
    r14_4 = var_35;
    r15_4 = r15_5;
    rcx4_7 = var_37;
    r15_0 = r15_5;
    rdx1_0 = var_35;
    r14_0 = var_35;
    rax_0 = var_35;
    local_sp_2 = var_34;
    rcx4_1_ph = var_37;
    r96_1_ph = r96_6;
    r87_1_ph = r87_7;
    local_sp_12 = var_34;
    r96_5 = r96_6;
    r87_6 = r87_7;
    while (((uint64_t)var_38 & 1UL) != 0UL)
        {
            var_39 = local_sp_14 + (-16L);
            *(uint64_t *)var_39 = 4206276UL;
            indirect_placeholder();
            local_sp_13 = var_39;
            if ((long)var_35 > (long)18446744073709551615UL) {
                var_40 = *(uint64_t *)local_sp_14;
                *(uint64_t *)(local_sp_14 + (-24L)) = 4206301UL;
                var_41 = indirect_placeholder_4(var_40, 0UL, 3UL);
                *(uint64_t *)(local_sp_14 + (-32L)) = 4206309UL;
                indirect_placeholder();
                var_42 = (uint64_t)*(uint32_t *)var_41;
                var_43 = local_sp_14 + (-40L);
                *(uint64_t *)var_43 = 4206334UL;
                var_44 = indirect_placeholder_39(0UL, 4314220UL, 0UL, var_42, var_41, r96_6, r87_7);
                rcx4_7 = var_44.field_1;
                r96_5 = var_44.field_2;
                r87_6 = var_44.field_3;
                local_sp_13 = var_43;
            }
            var_45 = *(uint64_t *)4359624UL;
            r87_7 = r87_6;
            local_sp_14 = local_sp_13;
            rcx4_8 = rcx4_7;
            var_32 = var_45;
            r15_5 = r15_5 - var_45;
            r96_6 = r96_5;
            var_33 = *(uint64_t *)4359632UL;
            var_34 = local_sp_14 + (-8L);
            *(uint64_t *)var_34 = 4206238UL;
            var_35 = indirect_placeholder_4(var_32, rbx_4_ph, var_33);
            var_36 = (unsigned char)(r15_5 != 0UL);
            var_37 = (rcx4_8 & (-256L)) | (var_35 == 0UL);
            var_38 = var_36 & (unsigned char)var_37;
            r14_4 = var_35;
            r15_4 = r15_5;
            rcx4_7 = var_37;
            r15_0 = r15_5;
            rdx1_0 = var_35;
            r14_0 = var_35;
            rax_0 = var_35;
            local_sp_2 = var_34;
            rcx4_1_ph = var_37;
            r96_1_ph = r96_6;
            r87_1_ph = r87_7;
            local_sp_12 = var_34;
            r96_5 = r96_6;
            r87_6 = r87_7;
        }
    *(unsigned char *)(local_sp_14 + 23UL) = var_38;
    if (var_35 == *(uint64_t *)4359624UL) {
        local_sp_11 = local_sp_2;
        r15_1 = r15_0;
        r14_1 = r14_0;
        rax_1 = rax_0;
        local_sp_3 = local_sp_2;
        if (r14_0 != 18446744073709551615UL) {
            var_50 = *(uint64_t *)(local_sp_11 + 16UL);
            *(uint64_t *)(local_sp_11 + (-8L)) = 4206473UL;
            var_51 = indirect_placeholder_4(var_50, 0UL, 3UL);
            *(uint64_t *)(local_sp_11 + (-16L)) = 4206481UL;
            indirect_placeholder();
            var_52 = (uint64_t)*(uint32_t *)var_51;
            var_53 = local_sp_11 + (-24L);
            *(uint64_t *)var_53 = 4206506UL;
            indirect_placeholder_38(0UL, 4314189UL, 0UL, var_52, var_51, r96_6, r87_7);
            local_sp_1 = var_53;
            return (uint64_t)*(unsigned char *)(local_sp_1 + 31UL);
        }
        var_55 = r14_1 + *(uint64_t *)4359632UL;
        var_56 = (*(uint64_t *)4359648UL == 0UL);
        var_57 = var_55 - var_21;
        rbx_0 = var_56 ? var_55 : (var_57 + 1UL);
        rax_2 = var_56 ? rax_1 : var_57;
        *(unsigned char *)(local_sp_3 + 30UL) = (unsigned char)'\x01';
        var_58 = (var_21 == 1UL);
        r15_2_ph = r15_1;
        r14_2_ph = var_55;
        rbx_1_ph = rbx_0;
        rax_3_ph = rax_2;
        local_sp_4_ph = local_sp_3;
        while (1U)
            {
                var_59 = *(uint64_t *)4359648UL;
                r14_2 = r14_2_ph;
                rcx4_1 = rcx4_1_ph;
                rbx_1 = rbx_1_ph;
                r96_1 = r96_1_ph;
                r87_1 = r87_1_ph;
                rax_3 = rax_3_ph;
                local_sp_4 = local_sp_4_ph;
                while (1U)
                    {
                        rcx4_3_ph = rcx4_1;
                        rcx4_2 = rcx4_1;
                        r96_2 = r96_1;
                        local_sp_5 = local_sp_4;
                        local_sp_6 = local_sp_4;
                        rbx_2_ph = rbx_1;
                        r96_3_ph = r96_1;
                        r87_4_ph = r87_1;
                        rax_4_ph = rax_3;
                        local_sp_7_ph = local_sp_4;
                        if (var_59 != 0UL) {
                            var_78 = ((uint64_t)(uint32_t)rax_4_ph == 0UL);
                            r87_5 = r87_4_ph;
                            rcx4_4 = rcx4_3_ph;
                            rax_5 = rax_4_ph;
                            rbx_2_ph237 = rbx_2_ph;
                            local_sp_7_ph238 = local_sp_7_ph;
                            r96_4 = r96_3_ph;
                            while (1U)
                                {
                                    rbx_2 = rbx_2_ph237;
                                    local_sp_8 = local_sp_7_ph238;
                                    var_79 = rbx_2 + (-1L);
                                    rbx_2_ph237 = var_79;
                                    rbx_2 = var_79;
                                    rbx_3 = var_79;
                                    do {
                                        var_79 = rbx_2 + (-1L);
                                        rbx_2_ph237 = var_79;
                                        rbx_2 = var_79;
                                        rbx_3 = var_79;
                                    } while ((uint64_t)(*(unsigned char *)var_79 - var_19) != 0UL);
                                    if (var_58) {
                                        break;
                                    }
                                    var_80 = local_sp_7_ph238 + (-8L);
                                    *(uint64_t *)var_80 = 4206678UL;
                                    indirect_placeholder();
                                    local_sp_7_ph238 = var_80;
                                    local_sp_8 = var_80;
                                    if (!var_78) {
                                        continue;
                                    }
                                    break;
                                }
                            var_85 = *(uint64_t *)4359632UL;
                            r96_0 = r96_4;
                            r87_0 = r87_5;
                            local_sp_9 = local_sp_8;
                            rbx_1_be = rbx_3;
                            rcx4_6 = rcx4_4;
                            rax_6 = rax_5;
                            r96_1 = r96_4;
                            r87_1 = r87_5;
                            rcx4_5 = rcx4_4;
                            r14_3 = rbx_3;
                            local_sp_10 = local_sp_8;
                            if (var_85 > rbx_3) {
                                break;
                            }
                            if (*(unsigned char *)4359656UL == '\x00') {
                                var_98 = local_sp_8 + (-8L);
                                *(uint64_t *)var_98 = 4207294UL;
                                var_99 = indirect_placeholder_1(rbx_3, r14_2);
                                rcx4_6 = var_99;
                                local_sp_10 = var_98;
                            } else {
                                var_86 = rbx_3 + *(uint64_t *)4359640UL;
                                var_87 = (unsigned char *)(local_sp_8 + 30UL);
                                var_88 = *var_87 ^ '\x01';
                                var_89 = (uint64_t)var_88;
                                var_90 = (r14_2 != var_86);
                                var_91 = var_90;
                                var_92 = (rax_5 & (-256L)) | var_91;
                                var_93 = var_91 | var_89;
                                *var_87 = (var_88 | var_90);
                                r14_3 = var_86;
                                rax_6 = var_92;
                                if (var_93 == 0UL) {
                                    var_94 = local_sp_8 + (-8L);
                                    *(uint64_t *)var_94 = 4207269UL;
                                    var_95 = indirect_placeholder_1(var_86, r14_2);
                                    var_96 = *(unsigned char *)(local_sp_8 + 23UL);
                                    var_97 = (uint64_t)var_96;
                                    *(unsigned char *)(local_sp_8 + 22UL) = var_96;
                                    rcx4_6 = var_95;
                                    rax_6 = var_97;
                                    local_sp_10 = var_94;
                                }
                            }
                            var_100 = *(uint64_t *)4359648UL;
                            var_59 = var_100;
                            r14_2 = r14_3;
                            rcx4_1 = rcx4_6;
                            rax_3_be = rax_6;
                            local_sp_4 = local_sp_10;
                            if (var_100 == 0UL) {
                                var_101 = 1UL - *(uint64_t *)4359640UL;
                                rbx_1_be = rbx_3 + var_101;
                                rax_3_be = var_101;
                            }
                            rbx_1 = rbx_1_be;
                            rax_3 = rax_3_be;
                            continue;
                        }
                        var_60 = *(uint64_t *)4359632UL;
                        var_61 = rbx_1 - var_60;
                        var_62 = 1UL - var_61;
                        rsi3_0 = var_60;
                        r87_2 = var_62;
                        r87_3 = var_62;
                        rbx_2_ph = var_61;
                        rax_4_ph = 0UL;
                        if ((long)var_62 <= (long)1UL) {
                            var_63 = helper_cc_compute_all_wrapper(0UL - var_61, 1UL, var_7, 17U);
                            if ((var_63 & 64UL) != 0UL) {
                                var_81 = *(uint64_t *)4359632UL;
                                r87_5 = r87_2;
                                rcx4_4 = rcx4_2;
                                rax_5 = var_81;
                                local_sp_8 = local_sp_5;
                                rbx_3 = var_81 + (-1L);
                                r96_4 = r96_2;
                                var_85 = *(uint64_t *)4359632UL;
                                r96_0 = r96_4;
                                r87_0 = r87_5;
                                local_sp_9 = local_sp_8;
                                rbx_1_be = rbx_3;
                                rcx4_6 = rcx4_4;
                                rax_6 = rax_5;
                                r96_1 = r96_4;
                                r87_1 = r87_5;
                                rcx4_5 = rcx4_4;
                                r14_3 = rbx_3;
                                local_sp_10 = local_sp_8;
                                if (var_85 > rbx_3) {
                                    break;
                                }
                                if (*(unsigned char *)4359656UL == '\x00') {
                                    var_98 = local_sp_8 + (-8L);
                                    *(uint64_t *)var_98 = 4207294UL;
                                    var_99 = indirect_placeholder_1(rbx_3, r14_2);
                                    rcx4_6 = var_99;
                                    local_sp_10 = var_98;
                                } else {
                                    var_86 = rbx_3 + *(uint64_t *)4359640UL;
                                    var_87 = (unsigned char *)(local_sp_8 + 30UL);
                                    var_88 = *var_87 ^ '\x01';
                                    var_89 = (uint64_t)var_88;
                                    var_90 = (r14_2 != var_86);
                                    var_91 = var_90;
                                    var_92 = (rax_5 & (-256L)) | var_91;
                                    var_93 = var_91 | var_89;
                                    *var_87 = (var_88 | var_90);
                                    r14_3 = var_86;
                                    rax_6 = var_92;
                                    if (var_93 == 0UL) {
                                        var_94 = local_sp_8 + (-8L);
                                        *(uint64_t *)var_94 = 4207269UL;
                                        var_95 = indirect_placeholder_1(var_86, r14_2);
                                        var_96 = *(unsigned char *)(local_sp_8 + 23UL);
                                        var_97 = (uint64_t)var_96;
                                        *(unsigned char *)(local_sp_8 + 22UL) = var_96;
                                        rcx4_6 = var_95;
                                        rax_6 = var_97;
                                        local_sp_10 = var_94;
                                    }
                                }
                                var_100 = *(uint64_t *)4359648UL;
                                var_59 = var_100;
                                r14_2 = r14_3;
                                rcx4_1 = rcx4_6;
                                rax_3_be = rax_6;
                                local_sp_4 = local_sp_10;
                                if (var_100 == 0UL) {
                                    var_101 = 1UL - *(uint64_t *)4359640UL;
                                    rbx_1_be = rbx_3 + var_101;
                                    rax_3_be = var_101;
                                }
                                rbx_1 = rbx_1_be;
                                rax_3 = rax_3_be;
                                continue;
                            }
                        }
                        var_64 = local_sp_4 + (-8L);
                        *(uint64_t *)var_64 = 4206536UL;
                        var_65 = indirect_placeholder_41(0UL, 4314236UL, 1UL, 0UL, rcx4_1, r96_1, var_62);
                        rsi3_0 = var_65.field_0;
                        r87_3 = var_65.field_3;
                        local_sp_6 = var_64;
                    }
                if (r15_2_ph != 0UL) {
                    var_135 = local_sp_8 + (-8L);
                    *(uint64_t *)var_135 = 4206695UL;
                    indirect_placeholder_1(var_85, r14_2);
                    *(unsigned char *)(local_sp_8 + 23UL) = (unsigned char)'\x01';
                    local_sp_1 = var_135;
                    break;
                }
                var_102 = r14_2 - var_85;
                var_103 = *(uint64_t *)4359624UL;
                var_104 = helper_cc_compute_c_wrapper(var_103 - var_102, var_102, var_7, 17U);
                if (var_104 != 0UL) {
                    var_105 = *(uint64_t *)4359648UL;
                    var_106 = (var_105 == 0UL) ? 1UL : var_105;
                    var_107 = *(uint64_t *)4359616UL;
                    *(uint64_t *)4359624UL = (var_103 << 1UL);
                    var_108 = ((var_103 << 2UL) + var_105) + 2UL;
                    *(uint64_t *)4359616UL = var_108;
                    var_109 = helper_cc_compute_c_wrapper(var_108 - var_107, var_107, var_7, 17U);
                    rcx4_5 = var_107;
                    if (var_109 != 0UL) {
                        *(uint64_t *)(local_sp_8 + (-8L)) = 4206725UL;
                        indirect_placeholder_2(r96_4, r87_5);
                        abort();
                    }
                    var_110 = var_85 - var_106;
                    var_111 = local_sp_8 + (-8L);
                    *(uint64_t *)var_111 = 4207031UL;
                    var_112 = indirect_placeholder_1(var_110, var_108);
                    *(uint64_t *)4359632UL = (var_106 + var_112);
                    local_sp_9 = var_111;
                }
                var_113 = *(uint64_t *)4359624UL;
                var_114 = r15_2_ph - var_113;
                var_115 = helper_cc_compute_c_wrapper(var_114, var_113, var_7, 17U);
                rcx4_0 = rcx4_5;
                r15_3 = var_114;
                if (var_115 == 0UL) {
                    *(uint64_t *)4359624UL = r15_2_ph;
                    r15_3 = 0UL;
                }
                var_116 = local_sp_9 + (-8L);
                var_117 = (uint64_t *)var_116;
                *var_117 = 4206761UL;
                indirect_placeholder();
                _pre_phi = var_117;
                local_sp_0 = var_116;
                r15_2_ph = r15_3;
                if ((long)var_113 < (long)0UL) {
                    var_118 = *(uint64_t *)(local_sp_9 + 8UL);
                    *(uint64_t *)(local_sp_9 + (-16L)) = 4207084UL;
                    var_119 = indirect_placeholder_4(var_118, 0UL, 3UL);
                    *(uint64_t *)(local_sp_9 + (-24L)) = 4207092UL;
                    indirect_placeholder();
                    var_120 = (uint64_t)*(uint32_t *)var_119;
                    var_121 = local_sp_9 + (-32L);
                    var_122 = (uint64_t *)var_121;
                    *var_122 = 4207117UL;
                    var_123 = indirect_placeholder_36(0UL, 4314220UL, 0UL, var_120, var_119, r96_4, r87_5);
                    _pre_phi = var_122;
                    rcx4_0 = var_123.field_1;
                    r96_0 = var_123.field_2;
                    r87_0 = var_123.field_3;
                    local_sp_0 = var_121;
                }
                var_124 = *(uint64_t *)4359632UL;
                *(uint64_t *)(local_sp_0 + 8UL) = *(uint64_t *)4359624UL;
                *(uint64_t *)(local_sp_0 + (-8L)) = 4206808UL;
                indirect_placeholder();
                var_125 = *_pre_phi;
                var_126 = (var_102 + var_125) + var_124;
                var_127 = *(uint64_t *)4359648UL;
                var_128 = (uint64_t)*(uint32_t *)(local_sp_0 + 16UL);
                var_129 = local_sp_0 + (-16L);
                *(uint64_t *)var_129 = 4206848UL;
                var_130 = indirect_placeholder_4(var_125, var_128, var_124);
                r14_2_ph = var_126;
                rcx4_1_ph = rcx4_0;
                r96_1_ph = r96_0;
                r87_1_ph = r87_0;
                rax_3_ph = var_130;
                local_sp_4_ph = var_129;
                if (var_130 == *(uint64_t *)4359624UL) {
                    rbx_1_ph = (var_127 == 0UL) ? var_126 : (var_125 + var_124);
                    continue;
                }
                var_131 = *_pre_phi;
                *(uint64_t *)(local_sp_0 + (-24L)) = 4207142UL;
                var_132 = indirect_placeholder_4(var_131, 0UL, 3UL);
                *(uint64_t *)(local_sp_0 + (-32L)) = 4207150UL;
                indirect_placeholder();
                var_133 = (uint64_t)*(uint32_t *)var_132;
                var_134 = local_sp_0 + (-40L);
                *(uint64_t *)var_134 = 4207175UL;
                indirect_placeholder_35(0UL, 4314189UL, 0UL, var_133, var_132, r96_0, r87_0);
                local_sp_1 = var_134;
                break;
            }
        return (uint64_t)*(unsigned char *)(local_sp_1 + 31UL);
    }
    var_46 = (uint64_t)*(uint32_t *)(local_sp_14 + 16UL);
    rax_0 = 0UL;
    while (1U)
        {
            var_47 = *(uint64_t *)4359632UL;
            var_48 = local_sp_12 + (-8L);
            *(uint64_t *)var_48 = 4206422UL;
            var_49 = indirect_placeholder_4(rdx1_0, var_46, var_47);
            local_sp_11 = var_48;
            r14_4 = var_49;
            r15_0 = r15_4;
            rdx1_0 = var_49;
            r14_0 = r14_4;
            local_sp_2 = var_48;
            r14_1 = var_49;
            rax_1 = var_49;
            local_sp_3 = var_48;
            local_sp_12 = var_48;
            switch_state_var = 0;
            switch (var_49) {
              case 0UL:
                {
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 18446744073709551615UL:
                {
                    loop_state_var = 2U;
                    switch_state_var = 1;
                    break;
                }
                break;
              default:
                {
                    var_54 = r15_4 + var_49;
                    r15_1 = var_54;
                    r15_4 = var_54;
                    if (*(uint64_t *)4359624UL == var_49) {
                        continue;
                    }
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 2U:
        {
            var_50 = *(uint64_t *)(local_sp_11 + 16UL);
            *(uint64_t *)(local_sp_11 + (-8L)) = 4206473UL;
            var_51 = indirect_placeholder_4(var_50, 0UL, 3UL);
            *(uint64_t *)(local_sp_11 + (-16L)) = 4206481UL;
            indirect_placeholder();
            var_52 = (uint64_t)*(uint32_t *)var_51;
            var_53 = local_sp_11 + (-24L);
            *(uint64_t *)var_53 = 4206506UL;
            indirect_placeholder_38(0UL, 4314189UL, 0UL, var_52, var_51, r96_6, r87_7);
            local_sp_1 = var_53;
        }
        break;
      case 0U:
        {
            var_55 = r14_1 + *(uint64_t *)4359632UL;
            var_56 = (*(uint64_t *)4359648UL == 0UL);
            var_57 = var_55 - var_21;
            rbx_0 = var_56 ? var_55 : (var_57 + 1UL);
            rax_2 = var_56 ? rax_1 : var_57;
            *(unsigned char *)(local_sp_3 + 30UL) = (unsigned char)'\x01';
            var_58 = (var_21 == 1UL);
            r15_2_ph = r15_1;
            r14_2_ph = var_55;
            rbx_1_ph = rbx_0;
            rax_3_ph = rax_2;
            local_sp_4_ph = local_sp_3;
            while (1U)
                {
                    var_59 = *(uint64_t *)4359648UL;
                    r14_2 = r14_2_ph;
                    rcx4_1 = rcx4_1_ph;
                    rbx_1 = rbx_1_ph;
                    r96_1 = r96_1_ph;
                    r87_1 = r87_1_ph;
                    rax_3 = rax_3_ph;
                    local_sp_4 = local_sp_4_ph;
                    while (1U)
                        {
                            rcx4_3_ph = rcx4_1;
                            rcx4_2 = rcx4_1;
                            r96_2 = r96_1;
                            local_sp_5 = local_sp_4;
                            local_sp_6 = local_sp_4;
                            rbx_2_ph = rbx_1;
                            r96_3_ph = r96_1;
                            r87_4_ph = r87_1;
                            rax_4_ph = rax_3;
                            local_sp_7_ph = local_sp_4;
                            if (var_59 != 0UL) {
                                var_78 = ((uint64_t)(uint32_t)rax_4_ph == 0UL);
                                r87_5 = r87_4_ph;
                                rcx4_4 = rcx4_3_ph;
                                rax_5 = rax_4_ph;
                                rbx_2_ph237 = rbx_2_ph;
                                local_sp_7_ph238 = local_sp_7_ph;
                                r96_4 = r96_3_ph;
                                while (1U)
                                    {
                                        rbx_2 = rbx_2_ph237;
                                        local_sp_8 = local_sp_7_ph238;
                                        var_79 = rbx_2 + (-1L);
                                        rbx_2_ph237 = var_79;
                                        rbx_2 = var_79;
                                        rbx_3 = var_79;
                                        do {
                                            var_79 = rbx_2 + (-1L);
                                            rbx_2_ph237 = var_79;
                                            rbx_2 = var_79;
                                            rbx_3 = var_79;
                                        } while ((uint64_t)(*(unsigned char *)var_79 - var_19) != 0UL);
                                        if (var_58) {
                                            break;
                                        }
                                        var_80 = local_sp_7_ph238 + (-8L);
                                        *(uint64_t *)var_80 = 4206678UL;
                                        indirect_placeholder();
                                        local_sp_7_ph238 = var_80;
                                        local_sp_8 = var_80;
                                        if (!var_78) {
                                            continue;
                                        }
                                        break;
                                    }
                                var_85 = *(uint64_t *)4359632UL;
                                r96_0 = r96_4;
                                r87_0 = r87_5;
                                local_sp_9 = local_sp_8;
                                rbx_1_be = rbx_3;
                                rcx4_6 = rcx4_4;
                                rax_6 = rax_5;
                                r96_1 = r96_4;
                                r87_1 = r87_5;
                                rcx4_5 = rcx4_4;
                                r14_3 = rbx_3;
                                local_sp_10 = local_sp_8;
                                if (var_85 > rbx_3) {
                                    break;
                                }
                                if (*(unsigned char *)4359656UL == '\x00') {
                                    var_98 = local_sp_8 + (-8L);
                                    *(uint64_t *)var_98 = 4207294UL;
                                    var_99 = indirect_placeholder_1(rbx_3, r14_2);
                                    rcx4_6 = var_99;
                                    local_sp_10 = var_98;
                                } else {
                                    var_86 = rbx_3 + *(uint64_t *)4359640UL;
                                    var_87 = (unsigned char *)(local_sp_8 + 30UL);
                                    var_88 = *var_87 ^ '\x01';
                                    var_89 = (uint64_t)var_88;
                                    var_90 = (r14_2 != var_86);
                                    var_91 = var_90;
                                    var_92 = (rax_5 & (-256L)) | var_91;
                                    var_93 = var_91 | var_89;
                                    *var_87 = (var_88 | var_90);
                                    r14_3 = var_86;
                                    rax_6 = var_92;
                                    if (var_93 == 0UL) {
                                        var_94 = local_sp_8 + (-8L);
                                        *(uint64_t *)var_94 = 4207269UL;
                                        var_95 = indirect_placeholder_1(var_86, r14_2);
                                        var_96 = *(unsigned char *)(local_sp_8 + 23UL);
                                        var_97 = (uint64_t)var_96;
                                        *(unsigned char *)(local_sp_8 + 22UL) = var_96;
                                        rcx4_6 = var_95;
                                        rax_6 = var_97;
                                        local_sp_10 = var_94;
                                    }
                                }
                                var_100 = *(uint64_t *)4359648UL;
                                var_59 = var_100;
                                r14_2 = r14_3;
                                rcx4_1 = rcx4_6;
                                rax_3_be = rax_6;
                                local_sp_4 = local_sp_10;
                                if (var_100 == 0UL) {
                                    var_101 = 1UL - *(uint64_t *)4359640UL;
                                    rbx_1_be = rbx_3 + var_101;
                                    rax_3_be = var_101;
                                }
                                rbx_1 = rbx_1_be;
                                rax_3 = rax_3_be;
                                continue;
                            }
                            var_60 = *(uint64_t *)4359632UL;
                            var_61 = rbx_1 - var_60;
                            var_62 = 1UL - var_61;
                            rsi3_0 = var_60;
                            r87_2 = var_62;
                            r87_3 = var_62;
                            rbx_2_ph = var_61;
                            rax_4_ph = 0UL;
                            if ((long)var_62 <= (long)1UL) {
                                var_63 = helper_cc_compute_all_wrapper(0UL - var_61, 1UL, var_7, 17U);
                                if ((var_63 & 64UL) != 0UL) {
                                    var_81 = *(uint64_t *)4359632UL;
                                    r87_5 = r87_2;
                                    rcx4_4 = rcx4_2;
                                    rax_5 = var_81;
                                    local_sp_8 = local_sp_5;
                                    rbx_3 = var_81 + (-1L);
                                    r96_4 = r96_2;
                                    var_85 = *(uint64_t *)4359632UL;
                                    r96_0 = r96_4;
                                    r87_0 = r87_5;
                                    local_sp_9 = local_sp_8;
                                    rbx_1_be = rbx_3;
                                    rcx4_6 = rcx4_4;
                                    rax_6 = rax_5;
                                    r96_1 = r96_4;
                                    r87_1 = r87_5;
                                    rcx4_5 = rcx4_4;
                                    r14_3 = rbx_3;
                                    local_sp_10 = local_sp_8;
                                    if (var_85 > rbx_3) {
                                        break;
                                    }
                                    if (*(unsigned char *)4359656UL == '\x00') {
                                        var_98 = local_sp_8 + (-8L);
                                        *(uint64_t *)var_98 = 4207294UL;
                                        var_99 = indirect_placeholder_1(rbx_3, r14_2);
                                        rcx4_6 = var_99;
                                        local_sp_10 = var_98;
                                    } else {
                                        var_86 = rbx_3 + *(uint64_t *)4359640UL;
                                        var_87 = (unsigned char *)(local_sp_8 + 30UL);
                                        var_88 = *var_87 ^ '\x01';
                                        var_89 = (uint64_t)var_88;
                                        var_90 = (r14_2 != var_86);
                                        var_91 = var_90;
                                        var_92 = (rax_5 & (-256L)) | var_91;
                                        var_93 = var_91 | var_89;
                                        *var_87 = (var_88 | var_90);
                                        r14_3 = var_86;
                                        rax_6 = var_92;
                                        if (var_93 == 0UL) {
                                            var_94 = local_sp_8 + (-8L);
                                            *(uint64_t *)var_94 = 4207269UL;
                                            var_95 = indirect_placeholder_1(var_86, r14_2);
                                            var_96 = *(unsigned char *)(local_sp_8 + 23UL);
                                            var_97 = (uint64_t)var_96;
                                            *(unsigned char *)(local_sp_8 + 22UL) = var_96;
                                            rcx4_6 = var_95;
                                            rax_6 = var_97;
                                            local_sp_10 = var_94;
                                        }
                                    }
                                    var_100 = *(uint64_t *)4359648UL;
                                    var_59 = var_100;
                                    r14_2 = r14_3;
                                    rcx4_1 = rcx4_6;
                                    rax_3_be = rax_6;
                                    local_sp_4 = local_sp_10;
                                    if (var_100 == 0UL) {
                                        var_101 = 1UL - *(uint64_t *)4359640UL;
                                        rbx_1_be = rbx_3 + var_101;
                                        rax_3_be = var_101;
                                    }
                                    rbx_1 = rbx_1_be;
                                    rax_3 = rax_3_be;
                                    continue;
                                }
                            }
                            var_64 = local_sp_4 + (-8L);
                            *(uint64_t *)var_64 = 4206536UL;
                            var_65 = indirect_placeholder_41(0UL, 4314236UL, 1UL, 0UL, rcx4_1, r96_1, var_62);
                            rsi3_0 = var_65.field_0;
                            r87_3 = var_65.field_3;
                            local_sp_6 = var_64;
                        }
                    if (r15_2_ph != 0UL) {
                        var_135 = local_sp_8 + (-8L);
                        *(uint64_t *)var_135 = 4206695UL;
                        indirect_placeholder_1(var_85, r14_2);
                        *(unsigned char *)(local_sp_8 + 23UL) = (unsigned char)'\x01';
                        local_sp_1 = var_135;
                        break;
                    }
                    var_102 = r14_2 - var_85;
                    var_103 = *(uint64_t *)4359624UL;
                    var_104 = helper_cc_compute_c_wrapper(var_103 - var_102, var_102, var_7, 17U);
                    if (var_104 != 0UL) {
                        var_105 = *(uint64_t *)4359648UL;
                        var_106 = (var_105 == 0UL) ? 1UL : var_105;
                        var_107 = *(uint64_t *)4359616UL;
                        *(uint64_t *)4359624UL = (var_103 << 1UL);
                        var_108 = ((var_103 << 2UL) + var_105) + 2UL;
                        *(uint64_t *)4359616UL = var_108;
                        var_109 = helper_cc_compute_c_wrapper(var_108 - var_107, var_107, var_7, 17U);
                        rcx4_5 = var_107;
                        if (var_109 != 0UL) {
                            *(uint64_t *)(local_sp_8 + (-8L)) = 4206725UL;
                            indirect_placeholder_2(r96_4, r87_5);
                            abort();
                        }
                        var_110 = var_85 - var_106;
                        var_111 = local_sp_8 + (-8L);
                        *(uint64_t *)var_111 = 4207031UL;
                        var_112 = indirect_placeholder_1(var_110, var_108);
                        *(uint64_t *)4359632UL = (var_106 + var_112);
                        local_sp_9 = var_111;
                    }
                    var_113 = *(uint64_t *)4359624UL;
                    var_114 = r15_2_ph - var_113;
                    var_115 = helper_cc_compute_c_wrapper(var_114, var_113, var_7, 17U);
                    rcx4_0 = rcx4_5;
                    r15_3 = var_114;
                    if (var_115 == 0UL) {
                        *(uint64_t *)4359624UL = r15_2_ph;
                        r15_3 = 0UL;
                    }
                    var_116 = local_sp_9 + (-8L);
                    var_117 = (uint64_t *)var_116;
                    *var_117 = 4206761UL;
                    indirect_placeholder();
                    _pre_phi = var_117;
                    local_sp_0 = var_116;
                    r15_2_ph = r15_3;
                    if ((long)var_113 < (long)0UL) {
                        var_118 = *(uint64_t *)(local_sp_9 + 8UL);
                        *(uint64_t *)(local_sp_9 + (-16L)) = 4207084UL;
                        var_119 = indirect_placeholder_4(var_118, 0UL, 3UL);
                        *(uint64_t *)(local_sp_9 + (-24L)) = 4207092UL;
                        indirect_placeholder();
                        var_120 = (uint64_t)*(uint32_t *)var_119;
                        var_121 = local_sp_9 + (-32L);
                        var_122 = (uint64_t *)var_121;
                        *var_122 = 4207117UL;
                        var_123 = indirect_placeholder_36(0UL, 4314220UL, 0UL, var_120, var_119, r96_4, r87_5);
                        _pre_phi = var_122;
                        rcx4_0 = var_123.field_1;
                        r96_0 = var_123.field_2;
                        r87_0 = var_123.field_3;
                        local_sp_0 = var_121;
                    }
                    var_124 = *(uint64_t *)4359632UL;
                    *(uint64_t *)(local_sp_0 + 8UL) = *(uint64_t *)4359624UL;
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4206808UL;
                    indirect_placeholder();
                    var_125 = *_pre_phi;
                    var_126 = (var_102 + var_125) + var_124;
                    var_127 = *(uint64_t *)4359648UL;
                    var_128 = (uint64_t)*(uint32_t *)(local_sp_0 + 16UL);
                    var_129 = local_sp_0 + (-16L);
                    *(uint64_t *)var_129 = 4206848UL;
                    var_130 = indirect_placeholder_4(var_125, var_128, var_124);
                    r14_2_ph = var_126;
                    rcx4_1_ph = rcx4_0;
                    r96_1_ph = r96_0;
                    r87_1_ph = r87_0;
                    rax_3_ph = var_130;
                    local_sp_4_ph = var_129;
                    if (var_130 == *(uint64_t *)4359624UL) {
                        rbx_1_ph = (var_127 == 0UL) ? var_126 : (var_125 + var_124);
                        continue;
                    }
                    var_131 = *_pre_phi;
                    *(uint64_t *)(local_sp_0 + (-24L)) = 4207142UL;
                    var_132 = indirect_placeholder_4(var_131, 0UL, 3UL);
                    *(uint64_t *)(local_sp_0 + (-32L)) = 4207150UL;
                    indirect_placeholder();
                    var_133 = (uint64_t)*(uint32_t *)var_132;
                    var_134 = local_sp_0 + (-40L);
                    *(uint64_t *)var_134 = 4207175UL;
                    indirect_placeholder_35(0UL, 4314189UL, 0UL, var_133, var_132, r96_0, r87_0);
                    local_sp_1 = var_134;
                    break;
                }
        }
        break;
      case 1U:
        {
            local_sp_11 = local_sp_2;
            r15_1 = r15_0;
            r14_1 = r14_0;
            rax_1 = rax_0;
            local_sp_3 = local_sp_2;
            if (r14_0 == 18446744073709551615UL) {
                var_50 = *(uint64_t *)(local_sp_11 + 16UL);
                *(uint64_t *)(local_sp_11 + (-8L)) = 4206473UL;
                var_51 = indirect_placeholder_4(var_50, 0UL, 3UL);
                *(uint64_t *)(local_sp_11 + (-16L)) = 4206481UL;
                indirect_placeholder();
                var_52 = (uint64_t)*(uint32_t *)var_51;
                var_53 = local_sp_11 + (-24L);
                *(uint64_t *)var_53 = 4206506UL;
                indirect_placeholder_38(0UL, 4314189UL, 0UL, var_52, var_51, r96_6, r87_7);
                local_sp_1 = var_53;
                return (uint64_t)*(unsigned char *)(local_sp_1 + 31UL);
            }
            var_55 = r14_1 + *(uint64_t *)4359632UL;
            var_56 = (*(uint64_t *)4359648UL == 0UL);
            var_57 = var_55 - var_21;
            rbx_0 = var_56 ? var_55 : (var_57 + 1UL);
            rax_2 = var_56 ? rax_1 : var_57;
            *(unsigned char *)(local_sp_3 + 30UL) = (unsigned char)'\x01';
            var_58 = (var_21 == 1UL);
            r15_2_ph = r15_1;
            r14_2_ph = var_55;
            rbx_1_ph = rbx_0;
            rax_3_ph = rax_2;
            local_sp_4_ph = local_sp_3;
            while (1U)
                {
                    var_59 = *(uint64_t *)4359648UL;
                    r14_2 = r14_2_ph;
                    rcx4_1 = rcx4_1_ph;
                    rbx_1 = rbx_1_ph;
                    r96_1 = r96_1_ph;
                    r87_1 = r87_1_ph;
                    rax_3 = rax_3_ph;
                    local_sp_4 = local_sp_4_ph;
                    while (1U)
                        {
                            rcx4_3_ph = rcx4_1;
                            rcx4_2 = rcx4_1;
                            r96_2 = r96_1;
                            local_sp_5 = local_sp_4;
                            local_sp_6 = local_sp_4;
                            rbx_2_ph = rbx_1;
                            r96_3_ph = r96_1;
                            r87_4_ph = r87_1;
                            rax_4_ph = rax_3;
                            local_sp_7_ph = local_sp_4;
                            if (var_59 != 0UL) {
                                var_78 = ((uint64_t)(uint32_t)rax_4_ph == 0UL);
                                r87_5 = r87_4_ph;
                                rcx4_4 = rcx4_3_ph;
                                rax_5 = rax_4_ph;
                                rbx_2_ph237 = rbx_2_ph;
                                local_sp_7_ph238 = local_sp_7_ph;
                                r96_4 = r96_3_ph;
                                while (1U)
                                    {
                                        rbx_2 = rbx_2_ph237;
                                        local_sp_8 = local_sp_7_ph238;
                                        var_79 = rbx_2 + (-1L);
                                        rbx_2_ph237 = var_79;
                                        rbx_2 = var_79;
                                        rbx_3 = var_79;
                                        do {
                                            var_79 = rbx_2 + (-1L);
                                            rbx_2_ph237 = var_79;
                                            rbx_2 = var_79;
                                            rbx_3 = var_79;
                                        } while ((uint64_t)(*(unsigned char *)var_79 - var_19) != 0UL);
                                        if (var_58) {
                                            break;
                                        }
                                        var_80 = local_sp_7_ph238 + (-8L);
                                        *(uint64_t *)var_80 = 4206678UL;
                                        indirect_placeholder();
                                        local_sp_7_ph238 = var_80;
                                        local_sp_8 = var_80;
                                        if (!var_78) {
                                            continue;
                                        }
                                        break;
                                    }
                                var_85 = *(uint64_t *)4359632UL;
                                r96_0 = r96_4;
                                r87_0 = r87_5;
                                local_sp_9 = local_sp_8;
                                rbx_1_be = rbx_3;
                                rcx4_6 = rcx4_4;
                                rax_6 = rax_5;
                                r96_1 = r96_4;
                                r87_1 = r87_5;
                                rcx4_5 = rcx4_4;
                                r14_3 = rbx_3;
                                local_sp_10 = local_sp_8;
                                if (var_85 > rbx_3) {
                                    break;
                                }
                                if (*(unsigned char *)4359656UL == '\x00') {
                                    var_98 = local_sp_8 + (-8L);
                                    *(uint64_t *)var_98 = 4207294UL;
                                    var_99 = indirect_placeholder_1(rbx_3, r14_2);
                                    rcx4_6 = var_99;
                                    local_sp_10 = var_98;
                                } else {
                                    var_86 = rbx_3 + *(uint64_t *)4359640UL;
                                    var_87 = (unsigned char *)(local_sp_8 + 30UL);
                                    var_88 = *var_87 ^ '\x01';
                                    var_89 = (uint64_t)var_88;
                                    var_90 = (r14_2 != var_86);
                                    var_91 = var_90;
                                    var_92 = (rax_5 & (-256L)) | var_91;
                                    var_93 = var_91 | var_89;
                                    *var_87 = (var_88 | var_90);
                                    r14_3 = var_86;
                                    rax_6 = var_92;
                                    if (var_93 == 0UL) {
                                        var_94 = local_sp_8 + (-8L);
                                        *(uint64_t *)var_94 = 4207269UL;
                                        var_95 = indirect_placeholder_1(var_86, r14_2);
                                        var_96 = *(unsigned char *)(local_sp_8 + 23UL);
                                        var_97 = (uint64_t)var_96;
                                        *(unsigned char *)(local_sp_8 + 22UL) = var_96;
                                        rcx4_6 = var_95;
                                        rax_6 = var_97;
                                        local_sp_10 = var_94;
                                    }
                                }
                                var_100 = *(uint64_t *)4359648UL;
                                var_59 = var_100;
                                r14_2 = r14_3;
                                rcx4_1 = rcx4_6;
                                rax_3_be = rax_6;
                                local_sp_4 = local_sp_10;
                                if (var_100 == 0UL) {
                                    var_101 = 1UL - *(uint64_t *)4359640UL;
                                    rbx_1_be = rbx_3 + var_101;
                                    rax_3_be = var_101;
                                }
                                rbx_1 = rbx_1_be;
                                rax_3 = rax_3_be;
                                continue;
                            }
                            var_60 = *(uint64_t *)4359632UL;
                            var_61 = rbx_1 - var_60;
                            var_62 = 1UL - var_61;
                            rsi3_0 = var_60;
                            r87_2 = var_62;
                            r87_3 = var_62;
                            rbx_2_ph = var_61;
                            rax_4_ph = 0UL;
                            if ((long)var_62 <= (long)1UL) {
                                var_63 = helper_cc_compute_all_wrapper(0UL - var_61, 1UL, var_7, 17U);
                                if ((var_63 & 64UL) != 0UL) {
                                    var_81 = *(uint64_t *)4359632UL;
                                    r87_5 = r87_2;
                                    rcx4_4 = rcx4_2;
                                    rax_5 = var_81;
                                    local_sp_8 = local_sp_5;
                                    rbx_3 = var_81 + (-1L);
                                    r96_4 = r96_2;
                                    var_85 = *(uint64_t *)4359632UL;
                                    r96_0 = r96_4;
                                    r87_0 = r87_5;
                                    local_sp_9 = local_sp_8;
                                    rbx_1_be = rbx_3;
                                    rcx4_6 = rcx4_4;
                                    rax_6 = rax_5;
                                    r96_1 = r96_4;
                                    r87_1 = r87_5;
                                    rcx4_5 = rcx4_4;
                                    r14_3 = rbx_3;
                                    local_sp_10 = local_sp_8;
                                    if (var_85 > rbx_3) {
                                        break;
                                    }
                                    if (*(unsigned char *)4359656UL == '\x00') {
                                        var_98 = local_sp_8 + (-8L);
                                        *(uint64_t *)var_98 = 4207294UL;
                                        var_99 = indirect_placeholder_1(rbx_3, r14_2);
                                        rcx4_6 = var_99;
                                        local_sp_10 = var_98;
                                    } else {
                                        var_86 = rbx_3 + *(uint64_t *)4359640UL;
                                        var_87 = (unsigned char *)(local_sp_8 + 30UL);
                                        var_88 = *var_87 ^ '\x01';
                                        var_89 = (uint64_t)var_88;
                                        var_90 = (r14_2 != var_86);
                                        var_91 = var_90;
                                        var_92 = (rax_5 & (-256L)) | var_91;
                                        var_93 = var_91 | var_89;
                                        *var_87 = (var_88 | var_90);
                                        r14_3 = var_86;
                                        rax_6 = var_92;
                                        if (var_93 == 0UL) {
                                            var_94 = local_sp_8 + (-8L);
                                            *(uint64_t *)var_94 = 4207269UL;
                                            var_95 = indirect_placeholder_1(var_86, r14_2);
                                            var_96 = *(unsigned char *)(local_sp_8 + 23UL);
                                            var_97 = (uint64_t)var_96;
                                            *(unsigned char *)(local_sp_8 + 22UL) = var_96;
                                            rcx4_6 = var_95;
                                            rax_6 = var_97;
                                            local_sp_10 = var_94;
                                        }
                                    }
                                    var_100 = *(uint64_t *)4359648UL;
                                    var_59 = var_100;
                                    r14_2 = r14_3;
                                    rcx4_1 = rcx4_6;
                                    rax_3_be = rax_6;
                                    local_sp_4 = local_sp_10;
                                    if (var_100 == 0UL) {
                                        var_101 = 1UL - *(uint64_t *)4359640UL;
                                        rbx_1_be = rbx_3 + var_101;
                                        rax_3_be = var_101;
                                    }
                                    rbx_1 = rbx_1_be;
                                    rax_3 = rax_3_be;
                                    continue;
                                }
                            }
                            var_64 = local_sp_4 + (-8L);
                            *(uint64_t *)var_64 = 4206536UL;
                            var_65 = indirect_placeholder_41(0UL, 4314236UL, 1UL, 0UL, rcx4_1, r96_1, var_62);
                            rsi3_0 = var_65.field_0;
                            r87_3 = var_65.field_3;
                            local_sp_6 = var_64;
                        }
                    if (r15_2_ph != 0UL) {
                        var_135 = local_sp_8 + (-8L);
                        *(uint64_t *)var_135 = 4206695UL;
                        indirect_placeholder_1(var_85, r14_2);
                        *(unsigned char *)(local_sp_8 + 23UL) = (unsigned char)'\x01';
                        local_sp_1 = var_135;
                        break;
                    }
                    var_102 = r14_2 - var_85;
                    var_103 = *(uint64_t *)4359624UL;
                    var_104 = helper_cc_compute_c_wrapper(var_103 - var_102, var_102, var_7, 17U);
                    if (var_104 != 0UL) {
                        var_105 = *(uint64_t *)4359648UL;
                        var_106 = (var_105 == 0UL) ? 1UL : var_105;
                        var_107 = *(uint64_t *)4359616UL;
                        *(uint64_t *)4359624UL = (var_103 << 1UL);
                        var_108 = ((var_103 << 2UL) + var_105) + 2UL;
                        *(uint64_t *)4359616UL = var_108;
                        var_109 = helper_cc_compute_c_wrapper(var_108 - var_107, var_107, var_7, 17U);
                        rcx4_5 = var_107;
                        if (var_109 != 0UL) {
                            *(uint64_t *)(local_sp_8 + (-8L)) = 4206725UL;
                            indirect_placeholder_2(r96_4, r87_5);
                            abort();
                        }
                        var_110 = var_85 - var_106;
                        var_111 = local_sp_8 + (-8L);
                        *(uint64_t *)var_111 = 4207031UL;
                        var_112 = indirect_placeholder_1(var_110, var_108);
                        *(uint64_t *)4359632UL = (var_106 + var_112);
                        local_sp_9 = var_111;
                    }
                    var_113 = *(uint64_t *)4359624UL;
                    var_114 = r15_2_ph - var_113;
                    var_115 = helper_cc_compute_c_wrapper(var_114, var_113, var_7, 17U);
                    rcx4_0 = rcx4_5;
                    r15_3 = var_114;
                    if (var_115 == 0UL) {
                        *(uint64_t *)4359624UL = r15_2_ph;
                        r15_3 = 0UL;
                    }
                    var_116 = local_sp_9 + (-8L);
                    var_117 = (uint64_t *)var_116;
                    *var_117 = 4206761UL;
                    indirect_placeholder();
                    _pre_phi = var_117;
                    local_sp_0 = var_116;
                    r15_2_ph = r15_3;
                    if ((long)var_113 < (long)0UL) {
                        var_118 = *(uint64_t *)(local_sp_9 + 8UL);
                        *(uint64_t *)(local_sp_9 + (-16L)) = 4207084UL;
                        var_119 = indirect_placeholder_4(var_118, 0UL, 3UL);
                        *(uint64_t *)(local_sp_9 + (-24L)) = 4207092UL;
                        indirect_placeholder();
                        var_120 = (uint64_t)*(uint32_t *)var_119;
                        var_121 = local_sp_9 + (-32L);
                        var_122 = (uint64_t *)var_121;
                        *var_122 = 4207117UL;
                        var_123 = indirect_placeholder_36(0UL, 4314220UL, 0UL, var_120, var_119, r96_4, r87_5);
                        _pre_phi = var_122;
                        rcx4_0 = var_123.field_1;
                        r96_0 = var_123.field_2;
                        r87_0 = var_123.field_3;
                        local_sp_0 = var_121;
                    }
                    var_124 = *(uint64_t *)4359632UL;
                    *(uint64_t *)(local_sp_0 + 8UL) = *(uint64_t *)4359624UL;
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4206808UL;
                    indirect_placeholder();
                    var_125 = *_pre_phi;
                    var_126 = (var_102 + var_125) + var_124;
                    var_127 = *(uint64_t *)4359648UL;
                    var_128 = (uint64_t)*(uint32_t *)(local_sp_0 + 16UL);
                    var_129 = local_sp_0 + (-16L);
                    *(uint64_t *)var_129 = 4206848UL;
                    var_130 = indirect_placeholder_4(var_125, var_128, var_124);
                    r14_2_ph = var_126;
                    rcx4_1_ph = rcx4_0;
                    r96_1_ph = r96_0;
                    r87_1_ph = r87_0;
                    rax_3_ph = var_130;
                    local_sp_4_ph = var_129;
                    if (var_130 == *(uint64_t *)4359624UL) {
                        rbx_1_ph = (var_127 == 0UL) ? var_126 : (var_125 + var_124);
                        continue;
                    }
                    var_131 = *_pre_phi;
                    *(uint64_t *)(local_sp_0 + (-24L)) = 4207142UL;
                    var_132 = indirect_placeholder_4(var_131, 0UL, 3UL);
                    *(uint64_t *)(local_sp_0 + (-32L)) = 4207150UL;
                    indirect_placeholder();
                    var_133 = (uint64_t)*(uint32_t *)var_132;
                    var_134 = local_sp_0 + (-40L);
                    *(uint64_t *)var_134 = 4207175UL;
                    indirect_placeholder_35(0UL, 4314189UL, 0UL, var_133, var_132, r96_0, r87_0);
                    local_sp_1 = var_134;
                    break;
                }
        }
        break;
    }
    return (uint64_t)*(unsigned char *)(local_sp_1 + 31UL);
}
