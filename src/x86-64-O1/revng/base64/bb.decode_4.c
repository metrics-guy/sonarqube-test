typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_8(uint64_t param_0);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
uint64_t bb_decode_4(uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t rcx) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t rbx_0;
    uint64_t local_sp_2;
    uint64_t r15_0;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t local_sp_1;
    uint64_t local_sp_0;
    uint64_t r15_1;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    unsigned char var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_37;
    uint64_t var_15;
    uint64_t *var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    unsigned char var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t r15_2;
    unsigned char var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    *(uint64_t *)(var_0 + (-64L)) = rdx;
    rbx_0 = 0UL;
    if (rsi > 1UL) {
        return rbx_0;
    }
    var_7 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)rdi;
    *(uint64_t *)(var_0 + (-72L)) = 4207344UL;
    var_8 = indirect_placeholder_8(var_7);
    var_9 = (uint64_t)(uint32_t)var_8;
    rbx_0 = var_9;
    var_10 = *(unsigned char *)(rdi + 1UL);
    var_11 = (uint64_t)(uint32_t)(uint64_t)var_10;
    *(uint32_t *)(var_0 + (-60L)) = (uint32_t)var_10;
    var_12 = var_0 + (-80L);
    *(uint64_t *)var_12 = 4207369UL;
    var_13 = indirect_placeholder_8(var_11);
    var_14 = (uint64_t)(uint32_t)var_13;
    local_sp_1 = var_12;
    rbx_0 = var_14;
    if (~((uint64_t)(unsigned char)var_8 != 0UL & (uint64_t)(unsigned char)var_13 != 0UL)) {
        return;
    }
    var_15 = *(uint64_t *)rdx;
    var_16 = (uint64_t *)rcx;
    r15_1 = var_15;
    rbx_0 = 0UL;
    if (*var_16 == 0UL) {
        *(uint64_t *)(var_0 + (-88L)) = 4207397UL;
        var_17 = indirect_placeholder_8(var_7);
        var_18 = (uint64_t)*(uint32_t *)(var_0 + (-76L));
        var_19 = var_0 + (-96L);
        *(uint64_t *)var_19 = 4207409UL;
        var_20 = indirect_placeholder_8(var_18);
        *(unsigned char *)var_15 = ((unsigned char)(uint64_t)((long)((uint64_t)*(unsigned char *)((uint64_t)(unsigned char)var_20 + 4257376UL) << 56UL) >> (long)60UL) | (unsigned char)((uint64_t)*(unsigned char *)((uint64_t)(unsigned char)var_17 + 4257376UL) << 2UL));
        *var_16 = (*var_16 + (-1L));
        local_sp_1 = var_19;
        r15_1 = var_15 + 1UL;
    }
    r15_0 = r15_1;
    local_sp_2 = local_sp_1;
    r15_2 = r15_1;
    if (rsi == 2UL) {
        **(uint64_t **)local_sp_1 = r15_1;
    } else {
        var_21 = *(unsigned char *)(rdi + 2UL);
        rbx_0 = var_14;
        if (var_21 == '=') {
            if (rsi != 4UL) {
                **(uint64_t **)local_sp_1 = r15_1;
                return rbx_0;
            }
            if (*(unsigned char *)(rdi + 3UL) != '=') {
                **(uint64_t **)local_sp_1 = r15_1;
                return rbx_0;
            }
        }
        var_22 = (uint64_t)(uint32_t)(uint64_t)var_21;
        var_23 = local_sp_1 + (-8L);
        *(uint64_t *)var_23 = 4207494UL;
        var_24 = indirect_placeholder_8(var_22);
        local_sp_0 = var_23;
        if ((uint64_t)(unsigned char)var_24 != 0UL) {
            **(uint64_t **)var_23 = r15_1;
            var_37 = (uint64_t)(uint32_t)var_24;
            rbx_0 = var_37;
            return rbx_0;
        }
        if (*var_16 == 0UL) {
            var_25 = (uint64_t)*(uint32_t *)(local_sp_1 + 4UL);
            *(uint64_t *)(local_sp_1 + (-16L)) = 4207518UL;
            var_26 = indirect_placeholder_8(var_25);
            *(unsigned char *)(local_sp_1 + (-4L)) = (unsigned char)var_26;
            var_27 = local_sp_1 + (-24L);
            *(uint64_t *)var_27 = 4207530UL;
            var_28 = indirect_placeholder_8(var_22);
            *(unsigned char *)r15_1 = ((unsigned char)(uint64_t)((long)((uint64_t)*(unsigned char *)((uint64_t)(unsigned char)var_28 + 4257376UL) << 56UL) >> (long)58UL) | (unsigned char)((uint64_t)*(unsigned char *)((uint64_t)*(unsigned char *)(local_sp_1 + (-12L)) + 4257376UL) << 4UL));
            *var_16 = (*var_16 + (-1L));
            local_sp_0 = var_27;
            r15_0 = r15_1 + 1UL;
        }
        local_sp_2 = local_sp_0;
        r15_2 = r15_0;
        if (rsi != 3UL) {
            **(uint64_t **)local_sp_0 = r15_0;
            return rbx_0;
        }
        var_29 = *(unsigned char *)(rdi + 3UL);
        if (var_29 == '=') {
            if (rsi == 4UL) {
                **(uint64_t **)local_sp_0 = r15_0;
                return rbx_0;
            }
        }
        var_30 = (uint64_t)(uint32_t)(uint64_t)var_29;
        var_31 = local_sp_0 + (-8L);
        *(uint64_t *)var_31 = 4207611UL;
        var_32 = indirect_placeholder_8(var_30);
        local_sp_2 = var_31;
        if ((uint64_t)(unsigned char)var_32 != 0UL) {
            **(uint64_t **)var_31 = r15_0;
            var_36 = (uint64_t)(uint32_t)var_32;
            rbx_0 = var_36;
            return rbx_0;
        }
        if (*var_16 == 0UL) {
            *(uint64_t *)(local_sp_0 + (-16L)) = 4207634UL;
            var_33 = indirect_placeholder_8(var_22);
            var_34 = local_sp_0 + (-24L);
            *(uint64_t *)var_34 = 4207644UL;
            var_35 = indirect_placeholder_8(var_30);
            *(unsigned char *)r15_0 = ((*(unsigned char *)((uint64_t)(unsigned char)var_33 + 4257376UL) << '\x06') | *(unsigned char *)((uint64_t)(unsigned char)var_35 + 4257376UL));
            *var_16 = (*var_16 + (-1L));
            local_sp_2 = var_34;
            r15_2 = r15_0 + 1UL;
        }
        **(uint64_t **)local_sp_2 = r15_2;
    }
}
