typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_factor_insert_multiplicity_ret_type;
struct bb_factor_insert_multiplicity_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rcx(void);
struct bb_factor_insert_multiplicity_ret_type bb_factor_insert_multiplicity(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t rax_0;
    unsigned char *var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint32_t var_5;
    uint64_t rcx_0;
    uint64_t var_11;
    uint64_t r8_0;
    uint32_t var_6;
    uint64_t var_7;
    uint64_t rcx_1;
    uint64_t var_8;
    uint64_t var_9;
    unsigned char *var_10;
    struct bb_factor_insert_multiplicity_ret_type mrv;
    struct bb_factor_insert_multiplicity_ret_type mrv1;
    struct bb_factor_insert_multiplicity_ret_type mrv2;
    struct bb_factor_insert_multiplicity_ret_type mrv3;
    struct bb_factor_insert_multiplicity_ret_type mrv4;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t _pre_phi;
    uint64_t var_14;
    uint64_t var_15;
    unsigned char var_16;
    uint64_t var_17;
    uint64_t rcx_2;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    struct bb_factor_insert_multiplicity_ret_type mrv5;
    struct bb_factor_insert_multiplicity_ret_type mrv6;
    struct bb_factor_insert_multiplicity_ret_type mrv7;
    struct bb_factor_insert_multiplicity_ret_type mrv8;
    struct bb_factor_insert_multiplicity_ret_type mrv9;
    revng_init_local_sp(0UL);
    var_0 = (unsigned char *)(rdi + 250UL);
    var_1 = (uint64_t)*var_0;
    var_2 = rdi + 16UL;
    var_3 = rdi + 224UL;
    var_4 = var_1 + (-1L);
    var_5 = (uint32_t)var_4;
    rcx_0 = var_4;
    rax_0 = var_4;
    if ((int)var_5 < (int)0U) {
        var_18 = (uint64_t)var_5;
        var_19 = init_rcx();
        _pre_phi = var_18 << 32UL;
        rcx_2 = var_19;
    } else {
        while (1U)
            {
                rcx_1 = rcx_0;
                if (*(uint64_t *)(((rcx_0 << 3UL) + rdi) + 16UL) > rsi) {
                    var_11 = rcx_0 + (-1L);
                    rcx_0 = var_11;
                    rcx_1 = var_11;
                    if ((int)(uint32_t)var_11 > (int)4294967295U) {
                        continue;
                    }
                    r8_0 = (uint64_t)((uint32_t)rcx_0 + (-1));
                    break;
                }
                var_6 = (uint32_t)rcx_0;
                var_7 = (uint64_t)var_6;
                r8_0 = var_7;
                if ((int)var_6 < (int)0U) {
                    break;
                }
                var_8 = rcx_0 << 32UL;
                var_9 = (uint64_t)((long)var_8 >> (long)32UL);
                rcx_1 = var_9;
                if (*(uint64_t *)((uint64_t)((long)var_8 >> (long)29UL) + var_2) != rsi) {
                    break;
                }
                var_10 = (unsigned char *)(var_9 + var_3);
                *var_10 = (*var_10 + (unsigned char)rdx);
                mrv.field_0 = rsi;
                mrv1 = mrv;
                mrv1.field_1 = var_9;
                mrv2 = mrv1;
                mrv2.field_2 = var_2;
                mrv3 = mrv2;
                mrv3.field_3 = var_1;
                mrv4 = mrv3;
                mrv4.field_4 = var_7;
                return mrv4;
            }
        var_12 = var_4 << 32UL;
        var_13 = r8_0 << 32UL;
        _pre_phi = var_13;
        rcx_2 = rcx_1;
        if ((long)var_12 <= (long)var_13) {
            var_14 = (rax_0 << 3UL) + rdi;
            *(uint64_t *)(var_14 + 24UL) = *(uint64_t *)(var_14 + 16UL);
            var_15 = rax_0 + rdi;
            var_16 = *(unsigned char *)(var_15 + 224UL);
            *(unsigned char *)(var_15 + 225UL) = var_16;
            var_17 = rax_0 + (-1L);
            rax_0 = var_17;
            do {
                var_14 = (rax_0 << 3UL) + rdi;
                *(uint64_t *)(var_14 + 24UL) = *(uint64_t *)(var_14 + 16UL);
                var_15 = rax_0 + rdi;
                var_16 = *(unsigned char *)(var_15 + 224UL);
                *(unsigned char *)(var_15 + 225UL) = var_16;
                var_17 = rax_0 + (-1L);
                rax_0 = var_17;
            } while ((long)var_13 >= (long)(var_17 << 32UL));
            rcx_2 = (uint64_t)var_16;
        }
    }
    var_20 = (uint64_t)((long)_pre_phi >> (long)32UL);
    *(uint64_t *)(((uint64_t)((long)_pre_phi >> (long)29UL) + var_2) + 8UL) = rsi;
    *(unsigned char *)((var_20 + var_3) + 1UL) = (unsigned char)rdx;
    var_21 = var_1 + 1UL;
    *var_0 = (unsigned char)var_21;
    mrv5.field_0 = rsi;
    mrv6 = mrv5;
    mrv6.field_1 = rcx_2;
    mrv7 = mrv6;
    mrv7.field_2 = var_2;
    mrv8 = mrv7;
    mrv8.field_3 = var_21;
    mrv9 = mrv8;
    mrv9.field_4 = var_20;
    return mrv9;
}
