typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_divq_EAX_wrapper_ret_type;
struct type_5;
struct indirect_placeholder_1_ret_type;
struct indirect_placeholder_2_ret_type;
struct indirect_placeholder_3_ret_type;
struct indirect_placeholder_5_ret_type;
struct indirect_placeholder_6_ret_type;
struct indirect_placeholder_7_ret_type;
struct indirect_placeholder_8_ret_type;
struct helper_divq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint32_t field_4;
    uint32_t field_5;
    uint32_t field_6;
    uint64_t field_7;
    uint32_t field_8;
    uint64_t field_9;
    uint32_t field_10;
    unsigned char field_11;
    uint32_t field_12;
};
struct type_5 {
};
struct indirect_placeholder_1_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_2_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_3_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_5_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_6_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_7_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_8_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint32_t init_state_0x8248(void);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern struct helper_divq_EAX_wrapper_ret_type helper_divq_EAX_wrapper(struct type_5 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint32_t param_13, uint64_t param_14, uint32_t param_15, uint64_t param_16, uint64_t param_17, uint64_t param_18, uint32_t param_19);
extern struct indirect_placeholder_1_ret_type indirect_placeholder_1(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_2_ret_type indirect_placeholder_2(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_3_ret_type indirect_placeholder_3(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_5_ret_type indirect_placeholder_5(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_6_ret_type indirect_placeholder_6(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_7_ret_type indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_8_ret_type indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_lines_chunk_split(uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t rcx, uint64_t r10, uint64_t r9, uint64_t r8) {
    struct indirect_placeholder_8_ret_type var_101;
    struct indirect_placeholder_3_ret_type var_63;
    struct indirect_placeholder_2_ret_type var_79;
    struct indirect_placeholder_1_ret_type var_58;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint32_t var_8;
    uint64_t var_9;
    uint32_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint32_t var_14;
    uint64_t var_15;
    uint64_t rdx1_1;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t r87_1;
    uint64_t local_sp_1;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t r96_6;
    uint64_t *_pre_phi215;
    uint64_t rdx1_0;
    uint64_t r15_10;
    uint64_t r15_7;
    uint64_t r13_0;
    uint64_t r12_7;
    uint64_t rbp_5;
    uint64_t r15_0;
    uint64_t r12_5;
    uint64_t rbp_0;
    uint64_t r96_11;
    uint64_t local_sp_11;
    uint64_t r12_0;
    uint64_t r96_0;
    uint64_t r87_0;
    uint64_t local_sp_0;
    uint64_t r13_1;
    uint64_t r15_1;
    uint64_t rbp_1;
    uint64_t r12_1;
    uint64_t r96_1;
    uint64_t var_53;
    uint64_t rdx1_2;
    uint64_t r96_2;
    uint64_t r87_2;
    uint64_t local_sp_2;
    uint64_t var_64;
    uint64_t *var_65;
    bool var_66;
    uint64_t var_67;
    unsigned char *var_68;
    unsigned char var_69;
    uint64_t var_70;
    uint64_t r13_3;
    uint64_t var_71;
    uint64_t r15_3;
    uint64_t var_72;
    bool var_73;
    uint64_t var_74;
    bool var_75;
    bool _not;
    bool var_76;
    uint64_t r12_2;
    uint64_t local_sp_8;
    uint64_t r96_4;
    uint64_t r13_2;
    uint64_t r15_2;
    uint64_t r87_4;
    uint64_t r96_3;
    uint64_t local_sp_4;
    uint64_t r87_3;
    uint64_t local_sp_3;
    uint64_t var_77;
    unsigned char *_pre_phi207;
    uint64_t var_78;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t r96_5;
    uint64_t r87_5;
    uint64_t local_sp_5;
    uint64_t var_82;
    uint64_t var_83;
    bool var_84;
    uint64_t *_pre_phi211;
    uint64_t r87_6;
    uint64_t *_pre210;
    uint64_t *_pre214;
    uint64_t local_sp_6;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t r87_11;
    uint64_t local_sp_14;
    struct indirect_placeholder_5_ret_type var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    struct indirect_placeholder_6_ret_type var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t r13_9;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t rdi2_0;
    uint64_t rsi3_0;
    uint64_t rcx4_0;
    uint64_t local_sp_7;
    struct helper_divq_EAX_wrapper_ret_type var_19;
    uint64_t var_20;
    uint64_t r15_4;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t r13_4;
    uint64_t var_28;
    uint64_t var_21;
    uint64_t rbp_2;
    uint64_t rbx_0_ph;
    uint64_t r15_8;
    uint64_t r13_5;
    uint64_t r12_6;
    uint64_t r15_5;
    uint64_t rbp_3;
    uint64_t r96_9;
    uint64_t r12_3;
    uint64_t r87_9;
    uint64_t r96_7;
    uint64_t local_sp_12;
    uint64_t r87_7;
    uint64_t local_sp_9;
    uint64_t r13_6;
    uint64_t r15_6;
    uint64_t rbp_4;
    uint64_t r12_4;
    uint64_t r96_8;
    uint64_t r87_8;
    uint64_t local_sp_10;
    uint64_t r15_9;
    uint64_t *var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t *var_43;
    uint64_t var_44;
    bool var_45;
    uint64_t var_87;
    uint64_t r13_7;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    struct indirect_placeholder_7_ret_type var_93;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t r13_8;
    uint64_t r96_10;
    uint64_t r87_10;
    uint64_t local_sp_13;
    uint64_t storemerge;
    uint64_t var_97;
    uint64_t var_98;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t r15_11_ph;
    uint64_t cc_src2_0_ph;
    uint64_t r96_12_ph;
    uint64_t r87_12_ph;
    uint64_t local_sp_15_ph;
    uint64_t r15_11;
    uint64_t r96_12;
    uint64_t r87_12;
    uint64_t local_sp_15;
    uint64_t var_99;
    uint64_t var_100;
    uint64_t var_18;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_cc_src2();
    var_8 = init_state_0x8248();
    var_9 = init_state_0x9018();
    var_10 = init_state_0x9010();
    var_11 = init_state_0x8408();
    var_12 = init_state_0x8328();
    var_13 = init_state_0x82d8();
    var_14 = init_state_0x9080();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_15 = var_0 + (-136L);
    *(uint64_t *)(var_0 + (-80L)) = rdx;
    *(uint64_t *)(var_0 + (-64L)) = rcx;
    *(uint64_t *)(var_0 + (-88L)) = r9;
    rdx1_0 = 4271950UL;
    rbp_0 = 18446744073709551615UL;
    r12_2 = 0UL;
    rdi2_0 = 4271652UL;
    rsi3_0 = 4271349UL;
    rcx4_0 = 4272928UL;
    local_sp_7 = var_15;
    r15_4 = 1UL;
    rbp_2 = r8;
    rbx_0_ph = rsi;
    rbp_3 = 18446744073709551615UL;
    r96_7 = r9;
    r87_7 = r8;
    cc_src2_0_ph = var_7;
    r96_12_ph = r9;
    r87_12_ph = r8;
    if (rsi == 0UL) {
        var_18 = var_0 + (-144L);
        *(uint64_t *)var_18 = 4211066UL;
        indirect_placeholder();
        local_sp_7 = var_18;
    }
    var_19 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), rsi, 4211076UL, *(uint64_t *)(local_sp_7 + 48UL), 0UL, rsi, rdi2_0, r8, rsi3_0, rcx4_0, r10, r9, r8, var_8, var_9, var_10, var_11, var_12, var_13, var_14);
    var_20 = var_19.field_1;
    *(uint64_t *)(local_sp_7 + 16UL) = var_20;
    local_sp_8 = local_sp_7;
    if (rdi <= 1UL) {
        var_21 = var_20 + (-1L);
        r13_4 = var_21;
        *(unsigned char *)(local_sp_8 + 71UL) = (unsigned char)'\x00';
        *(unsigned char *)(local_sp_8 + 70UL) = (unsigned char)'\x01';
        r13_5 = r13_4;
        r15_5 = r15_4;
        rbp_3 = rbp_2;
        r12_3 = r12_2;
        local_sp_9 = local_sp_8;
        r15_11_ph = r15_4;
        local_sp_15_ph = local_sp_8;
        if ((long)*(uint64_t *)(local_sp_8 + 48UL) > (long)r12_2) {
            r15_11 = r15_11_ph;
            r96_12 = r96_12_ph;
            r87_12 = r87_12_ph;
            local_sp_15 = local_sp_15_ph;
            while (rdi != 0UL)
                {
                    var_99 = helper_cc_compute_c_wrapper(rbx_0_ph - r15_11, r15_11, cc_src2_0_ph, 17U);
                    if (var_99 == 0UL) {
                        break;
                    }
                    var_100 = local_sp_15 + (-8L);
                    *(uint64_t *)var_100 = 4211897UL;
                    var_101 = indirect_placeholder_8(0UL, 1UL, 0UL, r96_12, r87_12);
                    r15_11 = r15_11 + 1UL;
                    r96_12 = var_101.field_1;
                    r87_12 = var_101.field_2;
                    local_sp_15 = var_100;
                }
            return;
        }
        *(uint64_t *)(local_sp_8 + 8UL) = rsi;
        loop_state_var = 2U;
        while (1U)
            {
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 2U:
                    {
                        r15_10 = r15_5;
                        r12_7 = r12_3;
                        r96_11 = r96_7;
                        r87_11 = r87_7;
                        local_sp_14 = local_sp_9;
                        r13_9 = r13_5;
                        r13_6 = r13_5;
                        r15_6 = r15_5;
                        rbp_4 = rbp_3;
                        r12_4 = r12_3;
                        r96_8 = r96_7;
                        r87_8 = r87_7;
                        local_sp_10 = local_sp_9;
                        if (rbp_3 != 18446744073709551615UL) {
                            r15_7 = r15_6;
                            r12_5 = r12_4;
                            local_sp_11 = local_sp_10;
                            r15_8 = r15_6;
                            r12_6 = r12_4;
                            r96_9 = r96_8;
                            r87_9 = r87_8;
                            local_sp_12 = local_sp_10;
                            r15_9 = r15_6;
                            r13_7 = r13_6;
                            r13_8 = r13_6;
                            r96_10 = r96_8;
                            r87_10 = r87_8;
                            local_sp_13 = local_sp_10;
                            if (rbp_4 != 0UL) {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            var_40 = (uint64_t *)(local_sp_10 + 48UL);
                            var_41 = *var_40 - r12_4;
                            var_42 = (var_41 > rbp_4) ? rbp_4 : var_41;
                            var_43 = (uint64_t *)(local_sp_10 + 32UL);
                            *var_43 = var_42;
                            var_44 = *(uint64_t *)(local_sp_10 + 56UL);
                            *(uint64_t *)(local_sp_10 + 40UL) = (var_44 + var_42);
                            var_45 = (var_42 == 0UL);
                            *(unsigned char *)(local_sp_10 + 71UL) = (unsigned char)'\x00';
                            _pre_phi215 = var_43;
                            rbp_5 = var_44;
                            _pre_phi211 = var_40;
                            if (!var_45) {
                                r13_5 = r13_8;
                                r15_5 = r15_8;
                                r12_3 = r12_6;
                                r96_7 = r96_9;
                                r87_7 = r87_9;
                                local_sp_9 = local_sp_12;
                                r15_9 = r15_8;
                                r96_10 = r96_9;
                                r87_10 = r87_9;
                                local_sp_13 = local_sp_12;
                                if ((long)*_pre_phi211 <= (long)r12_6) {
                                    loop_state_var = 2U;
                                    continue;
                                }
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            var_85 = r13_7 - r12_5;
                            var_86 = ((long)var_85 > (long)0UL) ? var_85 : 0UL;
                            var_87 = *_pre_phi215;
                            var_88 = (var_86 > var_87) ? var_87 : var_86;
                            var_89 = var_87 - var_88;
                            var_90 = var_88 + rbp_5;
                            var_91 = (uint64_t)*(uint32_t *)4297440UL;
                            var_92 = local_sp_11 + (-8L);
                            *(uint64_t *)var_92 = 4211562UL;
                            var_93 = indirect_placeholder_7(var_89, var_90, var_91);
                            var_94 = var_93.field_0;
                            var_95 = var_93.field_1;
                            var_96 = var_93.field_2;
                            rdx1_1 = var_89;
                            r87_1 = var_96;
                            local_sp_1 = var_92;
                            rdx1_0 = var_89;
                            r13_0 = r13_7;
                            r15_0 = r15_7;
                            rbp_0 = rbp_5;
                            r12_0 = r12_5;
                            r96_0 = var_95;
                            r87_0 = var_96;
                            local_sp_0 = var_92;
                            r13_1 = r13_7;
                            r15_1 = r15_7;
                            rbp_1 = rbp_5;
                            r12_1 = r12_5;
                            r96_1 = var_95;
                            if (var_94 != 0UL) {
                                loop_state_var = 0U;
                                continue;
                            }
                            *(uint64_t *)(local_sp_11 + 16UL) = (var_94 + 1UL);
                            *(unsigned char *)(local_sp_11 + 61UL) = (unsigned char)'\x01';
                            loop_state_var = 1U;
                            continue;
                        }
                        var_36 = *(uint64_t *)(local_sp_14 + 72UL);
                        var_37 = *(uint64_t *)(local_sp_14 + 56UL);
                        var_38 = local_sp_14 + (-8L);
                        *(uint64_t *)var_38 = 4211251UL;
                        var_39 = indirect_placeholder_4(var_36, 0UL, var_37);
                        r13_0 = r13_9;
                        r15_0 = r15_10;
                        r12_0 = r12_7;
                        r13_6 = r13_9;
                        r15_6 = r15_10;
                        rbp_4 = var_39;
                        r12_4 = r12_7;
                        r96_8 = r96_11;
                        r87_8 = r87_11;
                        local_sp_10 = var_38;
                        if (var_39 != 18446744073709551615UL) {
                            r15_7 = r15_6;
                            r12_5 = r12_4;
                            local_sp_11 = local_sp_10;
                            r15_8 = r15_6;
                            r12_6 = r12_4;
                            r96_9 = r96_8;
                            r87_9 = r87_8;
                            local_sp_12 = local_sp_10;
                            r15_9 = r15_6;
                            r13_7 = r13_6;
                            r13_8 = r13_6;
                            r96_10 = r96_8;
                            r87_10 = r87_8;
                            local_sp_13 = local_sp_10;
                            if (rbp_4 != 0UL) {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            var_40 = (uint64_t *)(local_sp_10 + 48UL);
                            var_41 = *var_40 - r12_4;
                            var_42 = (var_41 > rbp_4) ? rbp_4 : var_41;
                            var_43 = (uint64_t *)(local_sp_10 + 32UL);
                            *var_43 = var_42;
                            var_44 = *(uint64_t *)(local_sp_10 + 56UL);
                            *(uint64_t *)(local_sp_10 + 40UL) = (var_44 + var_42);
                            var_45 = (var_42 == 0UL);
                            *(unsigned char *)(local_sp_10 + 71UL) = (unsigned char)'\x00';
                            _pre_phi215 = var_43;
                            rbp_5 = var_44;
                            _pre_phi211 = var_40;
                            if (!var_45) {
                                r13_5 = r13_8;
                                r15_5 = r15_8;
                                r12_3 = r12_6;
                                r96_7 = r96_9;
                                r87_7 = r87_9;
                                local_sp_9 = local_sp_12;
                                r15_9 = r15_8;
                                r96_10 = r96_9;
                                r87_10 = r87_9;
                                local_sp_13 = local_sp_12;
                                if ((long)*_pre_phi211 <= (long)r12_6) {
                                    loop_state_var = 2U;
                                    continue;
                                }
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            var_85 = r13_7 - r12_5;
                            var_86 = ((long)var_85 > (long)0UL) ? var_85 : 0UL;
                            var_87 = *_pre_phi215;
                            var_88 = (var_86 > var_87) ? var_87 : var_86;
                            var_89 = var_87 - var_88;
                            var_90 = var_88 + rbp_5;
                            var_91 = (uint64_t)*(uint32_t *)4297440UL;
                            var_92 = local_sp_11 + (-8L);
                            *(uint64_t *)var_92 = 4211562UL;
                            var_93 = indirect_placeholder_7(var_89, var_90, var_91);
                            var_94 = var_93.field_0;
                            var_95 = var_93.field_1;
                            var_96 = var_93.field_2;
                            rdx1_1 = var_89;
                            r87_1 = var_96;
                            local_sp_1 = var_92;
                            rdx1_0 = var_89;
                            r13_0 = r13_7;
                            r15_0 = r15_7;
                            rbp_0 = rbp_5;
                            r12_0 = r12_5;
                            r96_0 = var_95;
                            r87_0 = var_96;
                            local_sp_0 = var_92;
                            r13_1 = r13_7;
                            r15_1 = r15_7;
                            rbp_1 = rbp_5;
                            r12_1 = r12_5;
                            r96_1 = var_95;
                            if (var_94 != 0UL) {
                                loop_state_var = 0U;
                                continue;
                            }
                            *(uint64_t *)(local_sp_11 + 16UL) = (var_94 + 1UL);
                            *(unsigned char *)(local_sp_11 + 61UL) = (unsigned char)'\x01';
                            loop_state_var = 1U;
                            continue;
                        }
                        var_46 = *(uint64_t *)4298352UL;
                        *(uint64_t *)(local_sp_14 + (-16L)) = 4211286UL;
                        var_47 = indirect_placeholder_4(var_46, 0UL, 3UL);
                        *(uint64_t *)(local_sp_14 + (-24L)) = 4211294UL;
                        indirect_placeholder();
                        var_48 = (uint64_t)*(uint32_t *)var_47;
                        var_49 = local_sp_14 + (-32L);
                        *(uint64_t *)var_49 = 4211319UL;
                        var_50 = indirect_placeholder_5(0UL, 4271950UL, 1UL, var_48, var_47, r96_11, r87_11);
                        var_51 = var_50.field_5;
                        var_52 = var_50.field_6;
                        r96_0 = var_51;
                        r87_0 = var_52;
                        local_sp_0 = var_49;
                        *(uint64_t *)(local_sp_0 + 24UL) = *(uint64_t *)(local_sp_0 + 40UL);
                        *(unsigned char *)(local_sp_0 + 69UL) = (unsigned char)'\x00';
                        rdx1_1 = rdx1_0;
                        r87_1 = r87_0;
                        local_sp_1 = local_sp_0;
                        r13_1 = r13_0;
                        r15_1 = r15_0;
                        rbp_1 = rbp_0;
                        r12_1 = r12_0;
                        r96_1 = r96_0;
                    }
                    break;
                  case 1U:
                  case 0U:
                    {
                        *(uint64_t *)(local_sp_0 + 24UL) = *(uint64_t *)(local_sp_0 + 40UL);
                        *(unsigned char *)(local_sp_0 + 69UL) = (unsigned char)'\x00';
                        rdx1_1 = rdx1_0;
                        r87_1 = r87_0;
                        local_sp_1 = local_sp_0;
                        r13_1 = r13_0;
                        r15_1 = r15_0;
                        rbp_1 = rbp_0;
                        r12_1 = r12_0;
                        r96_1 = r96_0;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
        switch (loop_state_var) {
          case 0U:
            {
                return;
            }
            break;
          case 1U:
            {
                storemerge = *(uint64_t *)(local_sp_13 + 8UL);
                var_97 = helper_cc_compute_c_wrapper((uint64_t)*(unsigned char *)(local_sp_13 + 71UL) + (-1L), 1UL, var_7, 14U);
                var_98 = (r15_9 + 1UL) - var_97;
                rbx_0_ph = storemerge;
                r15_11_ph = var_98;
                cc_src2_0_ph = var_97;
                r96_12_ph = r96_10;
                r87_12_ph = r87_10;
                local_sp_15_ph = local_sp_13;
                r15_11 = r15_11_ph;
                r96_12 = r96_12_ph;
                r87_12 = r87_12_ph;
                local_sp_15 = local_sp_15_ph;
                while (rdi != 0UL)
                    {
                        var_99 = helper_cc_compute_c_wrapper(rbx_0_ph - r15_11, r15_11, cc_src2_0_ph, 17U);
                        if (var_99 == 0UL) {
                            break;
                        }
                        var_100 = local_sp_15 + (-8L);
                        *(uint64_t *)var_100 = 4211897UL;
                        var_101 = indirect_placeholder_8(0UL, 1UL, 0UL, r96_12, r87_12);
                        r15_11 = r15_11 + 1UL;
                        r96_12 = var_101.field_1;
                        r87_12 = var_101.field_2;
                        local_sp_15 = var_100;
                    }
                return;
            }
            break;
        }
    }
    var_22 = rdi + (-1L);
    var_23 = var_22 * var_20;
    var_24 = var_23 + (-1L);
    var_25 = helper_cc_compute_c_wrapper(var_24 - r8, r8, var_7, 17U);
    r15_10 = var_22;
    r12_7 = var_24;
    r12_2 = var_24;
    r13_9 = var_24;
    r15_4 = var_22;
    r13_4 = var_24;
    rbp_2 = 18446744073709551615UL;
    if (var_25 == 0UL) {
        var_28 = local_sp_7 + (-8L);
        *(uint64_t *)var_28 = 4211160UL;
        indirect_placeholder();
        local_sp_8 = var_28;
        if ((long)var_23 > (long)18446744073709551615UL) {
            *(unsigned char *)(local_sp_8 + 71UL) = (unsigned char)'\x00';
            *(unsigned char *)(local_sp_8 + 70UL) = (unsigned char)'\x01';
            r13_5 = r13_4;
            r15_5 = r15_4;
            rbp_3 = rbp_2;
            r12_3 = r12_2;
            local_sp_9 = local_sp_8;
            r15_11_ph = r15_4;
            local_sp_15_ph = local_sp_8;
            if ((long)*(uint64_t *)(local_sp_8 + 48UL) <= (long)r12_2) {
                r15_11 = r15_11_ph;
                r96_12 = r96_12_ph;
                r87_12 = r87_12_ph;
                local_sp_15 = local_sp_15_ph;
                while (rdi != 0UL)
                    {
                        var_99 = helper_cc_compute_c_wrapper(rbx_0_ph - r15_11, r15_11, cc_src2_0_ph, 17U);
                        if (var_99 == 0UL) {
                            break;
                        }
                        var_100 = local_sp_15 + (-8L);
                        *(uint64_t *)var_100 = 4211897UL;
                        var_101 = indirect_placeholder_8(0UL, 1UL, 0UL, r96_12, r87_12);
                        r15_11 = r15_11 + 1UL;
                        r96_12 = var_101.field_1;
                        r87_12 = var_101.field_2;
                        local_sp_15 = var_100;
                    }
                return;
            }
            *(uint64_t *)(local_sp_8 + 8UL) = rsi;
            loop_state_var = 2U;
        } else {
            var_29 = *(uint64_t *)4298352UL;
            *(uint64_t *)(local_sp_7 + (-16L)) = 4211198UL;
            var_30 = indirect_placeholder_4(var_29, 0UL, 3UL);
            *(uint64_t *)(local_sp_7 + (-24L)) = 4211206UL;
            indirect_placeholder();
            var_31 = (uint64_t)*(uint32_t *)var_30;
            var_32 = local_sp_7 + (-32L);
            *(uint64_t *)var_32 = 4211231UL;
            var_33 = indirect_placeholder_6(0UL, 4271950UL, 1UL, var_31, var_30, r9, r8);
            var_34 = var_33.field_5;
            var_35 = var_33.field_6;
            r96_11 = var_34;
            r87_11 = var_35;
            local_sp_14 = var_32;
            var_36 = *(uint64_t *)(local_sp_14 + 72UL);
            var_37 = *(uint64_t *)(local_sp_14 + 56UL);
            var_38 = local_sp_14 + (-8L);
            *(uint64_t *)var_38 = 4211251UL;
            var_39 = indirect_placeholder_4(var_36, 0UL, var_37);
            r13_0 = r13_9;
            r15_0 = r15_10;
            r12_0 = r12_7;
            r13_6 = r13_9;
            r15_6 = r15_10;
            rbp_4 = var_39;
            r12_4 = r12_7;
            r96_8 = r96_11;
            r87_8 = r87_11;
            local_sp_10 = var_38;
            if (var_39 == 18446744073709551615UL) {
                var_46 = *(uint64_t *)4298352UL;
                *(uint64_t *)(local_sp_14 + (-16L)) = 4211286UL;
                var_47 = indirect_placeholder_4(var_46, 0UL, 3UL);
                *(uint64_t *)(local_sp_14 + (-24L)) = 4211294UL;
                indirect_placeholder();
                var_48 = (uint64_t)*(uint32_t *)var_47;
                var_49 = local_sp_14 + (-32L);
                *(uint64_t *)var_49 = 4211319UL;
                var_50 = indirect_placeholder_5(0UL, 4271950UL, 1UL, var_48, var_47, r96_11, r87_11);
                var_51 = var_50.field_5;
                var_52 = var_50.field_6;
                r96_0 = var_51;
                r87_0 = var_52;
                local_sp_0 = var_49;
                *(uint64_t *)(local_sp_0 + 24UL) = *(uint64_t *)(local_sp_0 + 40UL);
                *(unsigned char *)(local_sp_0 + 69UL) = (unsigned char)'\x00';
                rdx1_1 = rdx1_0;
                r87_1 = r87_0;
                local_sp_1 = local_sp_0;
                r13_1 = r13_0;
                r15_1 = r15_0;
                rbp_1 = rbp_0;
                r12_1 = r12_0;
                r96_1 = r96_0;
                var_53 = *(uint64_t *)(local_sp_1 + 24UL) - rbp_1;
                r96_6 = r96_1;
                rdx1_2 = var_53;
                r96_2 = r96_1;
                r87_2 = r87_1;
                local_sp_2 = local_sp_1;
                r13_3 = r13_1;
                r15_3 = r15_1;
                r13_2 = r13_1;
                r87_6 = r87_1;
                local_sp_6 = local_sp_1;
                if (r15_1 == rdi) {
                    var_54 = local_sp_1 + (-8L);
                    *(uint64_t *)var_54 = 4211355UL;
                    var_55 = indirect_placeholder_4(var_53, 1UL, rbp_1);
                    local_sp_2 = var_54;
                    if (var_55 != var_53) {
                        *(uint64_t *)(local_sp_1 + (-16L)) = 4211369UL;
                        indirect_placeholder();
                        var_56 = (uint64_t)*(uint32_t *)var_55;
                        var_57 = local_sp_1 + (-24L);
                        *(uint64_t *)var_57 = 4211396UL;
                        var_58 = indirect_placeholder_1(0UL, 4271950UL, 1UL, var_56, 4271682UL, r96_1, r87_1);
                        var_59 = var_58.field_5;
                        var_60 = var_58.field_6;
                        r96_6 = var_59;
                        r87_6 = var_60;
                        local_sp_6 = var_57;
                        var_61 = (uint64_t)*(unsigned char *)(local_sp_6 + 70UL);
                        var_62 = local_sp_6 + (-8L);
                        *(uint64_t *)var_62 = 4211412UL;
                        var_63 = indirect_placeholder_3(var_53, var_61, rbp_1, r96_6, r87_6);
                        r96_2 = var_63.field_1;
                        r87_2 = var_63.field_2;
                        local_sp_2 = var_62;
                    }
                } else {
                    rdx1_2 = rdx1_1;
                    if (rdi == 0UL) {
                        var_61 = (uint64_t)*(unsigned char *)(local_sp_6 + 70UL);
                        var_62 = local_sp_6 + (-8L);
                        *(uint64_t *)var_62 = 4211412UL;
                        var_63 = indirect_placeholder_3(var_53, var_61, rbp_1, r96_6, r87_6);
                        r96_2 = var_63.field_1;
                        r87_2 = var_63.field_2;
                        local_sp_2 = var_62;
                    }
                }
                var_64 = var_53 + r12_1;
                var_65 = (uint64_t *)(local_sp_2 + 32UL);
                *var_65 = (*var_65 - var_53);
                var_66 = ((long)var_64 > (long)r13_1);
                var_67 = var_66;
                var_68 = (unsigned char *)(local_sp_2 + 69UL);
                var_69 = *var_68;
                var_70 = (uint64_t)var_69;
                r12_5 = var_64;
                r96_3 = r96_2;
                r87_3 = r87_2;
                local_sp_3 = local_sp_2;
                _pre_phi207 = var_68;
                r96_5 = r96_2;
                r87_5 = r87_2;
                local_sp_5 = local_sp_2;
                r12_6 = var_64;
                if ((var_67 | var_70) == 0UL) {
                    *var_68 = (var_69 | var_66);
                } else {
                    var_71 = (var_70 ^ 1UL) & ((rdx1_2 & (-256L)) | (*(uint64_t *)(local_sp_2 + 24UL) == *(uint64_t *)(local_sp_2 + 40UL)));
                    if (var_71 == 0UL) {
                        var_72 = r15_1 + 1UL;
                        var_73 = (rdi != 0UL);
                        var_74 = helper_cc_compute_c_wrapper(rdi - var_72, var_72, var_7, 17U);
                        var_75 = (var_74 == 0UL);
                        _not = var_73 ^ 1;
                        r15_2 = var_72;
                        if (var_75 || _not) {
                            return;
                        }
                        var_76 = (rdi == 0UL);
                        while (1U)
                            {
                                var_77 = (*(uint64_t *)(local_sp_3 + 8UL) == r15_2) ? (*(uint64_t *)(local_sp_2 + 48UL) + (-1L)) : (r13_2 + *(uint64_t *)(local_sp_3 + 16UL));
                                r13_3 = var_77;
                                r15_3 = r15_2;
                                r96_4 = r96_3;
                                r13_2 = var_77;
                                r87_4 = r87_3;
                                local_sp_4 = local_sp_3;
                                r96_5 = r96_3;
                                r87_5 = r87_3;
                                local_sp_5 = local_sp_3;
                                if ((long)var_77 >= (long)var_64) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                if (var_76) {
                                    var_78 = local_sp_3 + (-8L);
                                    *(uint64_t *)var_78 = 4211437UL;
                                    var_79 = indirect_placeholder_2(0UL, 1UL, 0UL, r96_3, r87_3);
                                    r96_4 = var_79.field_1;
                                    r87_4 = var_79.field_2;
                                    local_sp_4 = var_78;
                                }
                                var_80 = r15_2 + 1UL;
                                var_81 = helper_cc_compute_c_wrapper(rdi - var_80, var_80, var_7, 17U);
                                r15_2 = var_80;
                                r96_3 = r96_4;
                                r87_3 = r87_4;
                                local_sp_3 = local_sp_4;
                                if ((var_81 == 0UL) || _not) {
                                    continue;
                                }
                                loop_state_var = 1U;
                                break;
                            }
                        switch (loop_state_var) {
                          case 0U:
                            {
                                _pre_phi207 = (unsigned char *)(local_sp_3 + 69UL);
                            }
                            break;
                          case 1U:
                            {
                                return;
                            }
                            break;
                        }
                    } else {
                        *(unsigned char *)(local_sp_2 + 71UL) = (unsigned char)var_71;
                        *var_68 = (unsigned char)'\x00';
                    }
                }
            } else {
                r15_7 = r15_6;
                r12_5 = r12_4;
                local_sp_11 = local_sp_10;
                r15_8 = r15_6;
                r12_6 = r12_4;
                r96_9 = r96_8;
                r87_9 = r87_8;
                local_sp_12 = local_sp_10;
                r15_9 = r15_6;
                r13_7 = r13_6;
                r13_8 = r13_6;
                r96_10 = r96_8;
                r87_10 = r87_8;
                local_sp_13 = local_sp_10;
                if (rbp_4 != 0UL) {
                    storemerge = *(uint64_t *)(local_sp_13 + 8UL);
                    var_97 = helper_cc_compute_c_wrapper((uint64_t)*(unsigned char *)(local_sp_13 + 71UL) + (-1L), 1UL, var_7, 14U);
                    var_98 = (r15_9 + 1UL) - var_97;
                    rbx_0_ph = storemerge;
                    r15_11_ph = var_98;
                    cc_src2_0_ph = var_97;
                    r96_12_ph = r96_10;
                    r87_12_ph = r87_10;
                    local_sp_15_ph = local_sp_13;
                    r15_11 = r15_11_ph;
                    r96_12 = r96_12_ph;
                    r87_12 = r87_12_ph;
                    local_sp_15 = local_sp_15_ph;
                    while (rdi != 0UL)
                        {
                            var_99 = helper_cc_compute_c_wrapper(rbx_0_ph - r15_11, r15_11, cc_src2_0_ph, 17U);
                            if (var_99 == 0UL) {
                                break;
                            }
                            var_100 = local_sp_15 + (-8L);
                            *(uint64_t *)var_100 = 4211897UL;
                            var_101 = indirect_placeholder_8(0UL, 1UL, 0UL, r96_12, r87_12);
                            r15_11 = r15_11 + 1UL;
                            r96_12 = var_101.field_1;
                            r87_12 = var_101.field_2;
                            local_sp_15 = var_100;
                        }
                    return;
                }
                var_40 = (uint64_t *)(local_sp_10 + 48UL);
                var_41 = *var_40 - r12_4;
                var_42 = (var_41 > rbp_4) ? rbp_4 : var_41;
                var_43 = (uint64_t *)(local_sp_10 + 32UL);
                *var_43 = var_42;
                var_44 = *(uint64_t *)(local_sp_10 + 56UL);
                *(uint64_t *)(local_sp_10 + 40UL) = (var_44 + var_42);
                var_45 = (var_42 == 0UL);
                *(unsigned char *)(local_sp_10 + 71UL) = (unsigned char)'\x00';
                _pre_phi215 = var_43;
                rbp_5 = var_44;
                _pre_phi211 = var_40;
                if (!var_45) {
                    r13_5 = r13_8;
                    r15_5 = r15_8;
                    r12_3 = r12_6;
                    r96_7 = r96_9;
                    r87_7 = r87_9;
                    local_sp_9 = local_sp_12;
                    r15_9 = r15_8;
                    r96_10 = r96_9;
                    r87_10 = r87_9;
                    local_sp_13 = local_sp_12;
                    if ((long)*_pre_phi211 > (long)r12_6) {
                        storemerge = *(uint64_t *)(local_sp_13 + 8UL);
                        var_97 = helper_cc_compute_c_wrapper((uint64_t)*(unsigned char *)(local_sp_13 + 71UL) + (-1L), 1UL, var_7, 14U);
                        var_98 = (r15_9 + 1UL) - var_97;
                        rbx_0_ph = storemerge;
                        r15_11_ph = var_98;
                        cc_src2_0_ph = var_97;
                        r96_12_ph = r96_10;
                        r87_12_ph = r87_10;
                        local_sp_15_ph = local_sp_13;
                        r15_11 = r15_11_ph;
                        r96_12 = r96_12_ph;
                        r87_12 = r87_12_ph;
                        local_sp_15 = local_sp_15_ph;
                        while (rdi != 0UL)
                            {
                                var_99 = helper_cc_compute_c_wrapper(rbx_0_ph - r15_11, r15_11, cc_src2_0_ph, 17U);
                                if (var_99 == 0UL) {
                                    break;
                                }
                                var_100 = local_sp_15 + (-8L);
                                *(uint64_t *)var_100 = 4211897UL;
                                var_101 = indirect_placeholder_8(0UL, 1UL, 0UL, r96_12, r87_12);
                                r15_11 = r15_11 + 1UL;
                                r96_12 = var_101.field_1;
                                r87_12 = var_101.field_2;
                                local_sp_15 = var_100;
                            }
                        return;
                    }
                    loop_state_var = 2U;
                    while (1U)
                        {
                            switch_state_var = 0;
                            switch (loop_state_var) {
                              case 2U:
                                {
                                    r15_10 = r15_5;
                                    r12_7 = r12_3;
                                    r96_11 = r96_7;
                                    r87_11 = r87_7;
                                    local_sp_14 = local_sp_9;
                                    r13_9 = r13_5;
                                    r13_6 = r13_5;
                                    r15_6 = r15_5;
                                    rbp_4 = rbp_3;
                                    r12_4 = r12_3;
                                    r96_8 = r96_7;
                                    r87_8 = r87_7;
                                    local_sp_10 = local_sp_9;
                                    if (rbp_3 != 18446744073709551615UL) {
                                        r15_7 = r15_6;
                                        r12_5 = r12_4;
                                        local_sp_11 = local_sp_10;
                                        r15_8 = r15_6;
                                        r12_6 = r12_4;
                                        r96_9 = r96_8;
                                        r87_9 = r87_8;
                                        local_sp_12 = local_sp_10;
                                        r15_9 = r15_6;
                                        r13_7 = r13_6;
                                        r13_8 = r13_6;
                                        r96_10 = r96_8;
                                        r87_10 = r87_8;
                                        local_sp_13 = local_sp_10;
                                        if (rbp_4 != 0UL) {
                                            loop_state_var = 1U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                        var_40 = (uint64_t *)(local_sp_10 + 48UL);
                                        var_41 = *var_40 - r12_4;
                                        var_42 = (var_41 > rbp_4) ? rbp_4 : var_41;
                                        var_43 = (uint64_t *)(local_sp_10 + 32UL);
                                        *var_43 = var_42;
                                        var_44 = *(uint64_t *)(local_sp_10 + 56UL);
                                        *(uint64_t *)(local_sp_10 + 40UL) = (var_44 + var_42);
                                        var_45 = (var_42 == 0UL);
                                        *(unsigned char *)(local_sp_10 + 71UL) = (unsigned char)'\x00';
                                        _pre_phi215 = var_43;
                                        rbp_5 = var_44;
                                        _pre_phi211 = var_40;
                                        if (!var_45) {
                                            r13_5 = r13_8;
                                            r15_5 = r15_8;
                                            r12_3 = r12_6;
                                            r96_7 = r96_9;
                                            r87_7 = r87_9;
                                            local_sp_9 = local_sp_12;
                                            r15_9 = r15_8;
                                            r96_10 = r96_9;
                                            r87_10 = r87_9;
                                            local_sp_13 = local_sp_12;
                                            if ((long)*_pre_phi211 <= (long)r12_6) {
                                                loop_state_var = 2U;
                                                continue;
                                            }
                                            loop_state_var = 1U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                        var_85 = r13_7 - r12_5;
                                        var_86 = ((long)var_85 > (long)0UL) ? var_85 : 0UL;
                                        var_87 = *_pre_phi215;
                                        var_88 = (var_86 > var_87) ? var_87 : var_86;
                                        var_89 = var_87 - var_88;
                                        var_90 = var_88 + rbp_5;
                                        var_91 = (uint64_t)*(uint32_t *)4297440UL;
                                        var_92 = local_sp_11 + (-8L);
                                        *(uint64_t *)var_92 = 4211562UL;
                                        var_93 = indirect_placeholder_7(var_89, var_90, var_91);
                                        var_94 = var_93.field_0;
                                        var_95 = var_93.field_1;
                                        var_96 = var_93.field_2;
                                        rdx1_1 = var_89;
                                        r87_1 = var_96;
                                        local_sp_1 = var_92;
                                        rdx1_0 = var_89;
                                        r13_0 = r13_7;
                                        r15_0 = r15_7;
                                        rbp_0 = rbp_5;
                                        r12_0 = r12_5;
                                        r96_0 = var_95;
                                        r87_0 = var_96;
                                        local_sp_0 = var_92;
                                        r13_1 = r13_7;
                                        r15_1 = r15_7;
                                        rbp_1 = rbp_5;
                                        r12_1 = r12_5;
                                        r96_1 = var_95;
                                        if (var_94 != 0UL) {
                                            loop_state_var = 0U;
                                            continue;
                                        }
                                        *(uint64_t *)(local_sp_11 + 16UL) = (var_94 + 1UL);
                                        *(unsigned char *)(local_sp_11 + 61UL) = (unsigned char)'\x01';
                                        loop_state_var = 1U;
                                        continue;
                                    }
                                    var_36 = *(uint64_t *)(local_sp_14 + 72UL);
                                    var_37 = *(uint64_t *)(local_sp_14 + 56UL);
                                    var_38 = local_sp_14 + (-8L);
                                    *(uint64_t *)var_38 = 4211251UL;
                                    var_39 = indirect_placeholder_4(var_36, 0UL, var_37);
                                    r13_0 = r13_9;
                                    r15_0 = r15_10;
                                    r12_0 = r12_7;
                                    r13_6 = r13_9;
                                    r15_6 = r15_10;
                                    rbp_4 = var_39;
                                    r12_4 = r12_7;
                                    r96_8 = r96_11;
                                    r87_8 = r87_11;
                                    local_sp_10 = var_38;
                                    if (var_39 != 18446744073709551615UL) {
                                        r15_7 = r15_6;
                                        r12_5 = r12_4;
                                        local_sp_11 = local_sp_10;
                                        r15_8 = r15_6;
                                        r12_6 = r12_4;
                                        r96_9 = r96_8;
                                        r87_9 = r87_8;
                                        local_sp_12 = local_sp_10;
                                        r15_9 = r15_6;
                                        r13_7 = r13_6;
                                        r13_8 = r13_6;
                                        r96_10 = r96_8;
                                        r87_10 = r87_8;
                                        local_sp_13 = local_sp_10;
                                        if (rbp_4 != 0UL) {
                                            loop_state_var = 1U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                        var_40 = (uint64_t *)(local_sp_10 + 48UL);
                                        var_41 = *var_40 - r12_4;
                                        var_42 = (var_41 > rbp_4) ? rbp_4 : var_41;
                                        var_43 = (uint64_t *)(local_sp_10 + 32UL);
                                        *var_43 = var_42;
                                        var_44 = *(uint64_t *)(local_sp_10 + 56UL);
                                        *(uint64_t *)(local_sp_10 + 40UL) = (var_44 + var_42);
                                        var_45 = (var_42 == 0UL);
                                        *(unsigned char *)(local_sp_10 + 71UL) = (unsigned char)'\x00';
                                        _pre_phi215 = var_43;
                                        rbp_5 = var_44;
                                        _pre_phi211 = var_40;
                                        if (!var_45) {
                                            r13_5 = r13_8;
                                            r15_5 = r15_8;
                                            r12_3 = r12_6;
                                            r96_7 = r96_9;
                                            r87_7 = r87_9;
                                            local_sp_9 = local_sp_12;
                                            r15_9 = r15_8;
                                            r96_10 = r96_9;
                                            r87_10 = r87_9;
                                            local_sp_13 = local_sp_12;
                                            if ((long)*_pre_phi211 <= (long)r12_6) {
                                                loop_state_var = 2U;
                                                continue;
                                            }
                                            loop_state_var = 1U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                        var_85 = r13_7 - r12_5;
                                        var_86 = ((long)var_85 > (long)0UL) ? var_85 : 0UL;
                                        var_87 = *_pre_phi215;
                                        var_88 = (var_86 > var_87) ? var_87 : var_86;
                                        var_89 = var_87 - var_88;
                                        var_90 = var_88 + rbp_5;
                                        var_91 = (uint64_t)*(uint32_t *)4297440UL;
                                        var_92 = local_sp_11 + (-8L);
                                        *(uint64_t *)var_92 = 4211562UL;
                                        var_93 = indirect_placeholder_7(var_89, var_90, var_91);
                                        var_94 = var_93.field_0;
                                        var_95 = var_93.field_1;
                                        var_96 = var_93.field_2;
                                        rdx1_1 = var_89;
                                        r87_1 = var_96;
                                        local_sp_1 = var_92;
                                        rdx1_0 = var_89;
                                        r13_0 = r13_7;
                                        r15_0 = r15_7;
                                        rbp_0 = rbp_5;
                                        r12_0 = r12_5;
                                        r96_0 = var_95;
                                        r87_0 = var_96;
                                        local_sp_0 = var_92;
                                        r13_1 = r13_7;
                                        r15_1 = r15_7;
                                        rbp_1 = rbp_5;
                                        r12_1 = r12_5;
                                        r96_1 = var_95;
                                        if (var_94 != 0UL) {
                                            loop_state_var = 0U;
                                            continue;
                                        }
                                        *(uint64_t *)(local_sp_11 + 16UL) = (var_94 + 1UL);
                                        *(unsigned char *)(local_sp_11 + 61UL) = (unsigned char)'\x01';
                                        loop_state_var = 1U;
                                        continue;
                                    }
                                    var_46 = *(uint64_t *)4298352UL;
                                    *(uint64_t *)(local_sp_14 + (-16L)) = 4211286UL;
                                    var_47 = indirect_placeholder_4(var_46, 0UL, 3UL);
                                    *(uint64_t *)(local_sp_14 + (-24L)) = 4211294UL;
                                    indirect_placeholder();
                                    var_48 = (uint64_t)*(uint32_t *)var_47;
                                    var_49 = local_sp_14 + (-32L);
                                    *(uint64_t *)var_49 = 4211319UL;
                                    var_50 = indirect_placeholder_5(0UL, 4271950UL, 1UL, var_48, var_47, r96_11, r87_11);
                                    var_51 = var_50.field_5;
                                    var_52 = var_50.field_6;
                                    r96_0 = var_51;
                                    r87_0 = var_52;
                                    local_sp_0 = var_49;
                                    *(uint64_t *)(local_sp_0 + 24UL) = *(uint64_t *)(local_sp_0 + 40UL);
                                    *(unsigned char *)(local_sp_0 + 69UL) = (unsigned char)'\x00';
                                    rdx1_1 = rdx1_0;
                                    r87_1 = r87_0;
                                    local_sp_1 = local_sp_0;
                                    r13_1 = r13_0;
                                    r15_1 = r15_0;
                                    rbp_1 = rbp_0;
                                    r12_1 = r12_0;
                                    r96_1 = r96_0;
                                }
                                break;
                              case 1U:
                              case 0U:
                                {
                                    *(uint64_t *)(local_sp_0 + 24UL) = *(uint64_t *)(local_sp_0 + 40UL);
                                    *(unsigned char *)(local_sp_0 + 69UL) = (unsigned char)'\x00';
                                    rdx1_1 = rdx1_0;
                                    r87_1 = r87_0;
                                    local_sp_1 = local_sp_0;
                                    r13_1 = r13_0;
                                    r15_1 = r15_0;
                                    rbp_1 = rbp_0;
                                    r12_1 = r12_0;
                                    r96_1 = r96_0;
                                }
                                break;
                            }
                            if (switch_state_var)
                                break;
                        }
                    switch (loop_state_var) {
                      case 0U:
                        {
                            return;
                        }
                        break;
                      case 1U:
                        {
                            storemerge = *(uint64_t *)(local_sp_13 + 8UL);
                            var_97 = helper_cc_compute_c_wrapper((uint64_t)*(unsigned char *)(local_sp_13 + 71UL) + (-1L), 1UL, var_7, 14U);
                            var_98 = (r15_9 + 1UL) - var_97;
                            rbx_0_ph = storemerge;
                            r15_11_ph = var_98;
                            cc_src2_0_ph = var_97;
                            r96_12_ph = r96_10;
                            r87_12_ph = r87_10;
                            local_sp_15_ph = local_sp_13;
                            r15_11 = r15_11_ph;
                            r96_12 = r96_12_ph;
                            r87_12 = r87_12_ph;
                            local_sp_15 = local_sp_15_ph;
                            while (rdi != 0UL)
                                {
                                    var_99 = helper_cc_compute_c_wrapper(rbx_0_ph - r15_11, r15_11, cc_src2_0_ph, 17U);
                                    if (var_99 == 0UL) {
                                        break;
                                    }
                                    var_100 = local_sp_15 + (-8L);
                                    *(uint64_t *)var_100 = 4211897UL;
                                    var_101 = indirect_placeholder_8(0UL, 1UL, 0UL, r96_12, r87_12);
                                    r15_11 = r15_11 + 1UL;
                                    r96_12 = var_101.field_1;
                                    r87_12 = var_101.field_2;
                                    local_sp_15 = var_100;
                                }
                            return;
                        }
                        break;
                    }
                }
                var_85 = r13_7 - r12_5;
                var_86 = ((long)var_85 > (long)0UL) ? var_85 : 0UL;
                var_87 = *_pre_phi215;
                var_88 = (var_86 > var_87) ? var_87 : var_86;
                var_89 = var_87 - var_88;
                var_90 = var_88 + rbp_5;
                var_91 = (uint64_t)*(uint32_t *)4297440UL;
                var_92 = local_sp_11 + (-8L);
                *(uint64_t *)var_92 = 4211562UL;
                var_93 = indirect_placeholder_7(var_89, var_90, var_91);
                var_94 = var_93.field_0;
                var_95 = var_93.field_1;
                var_96 = var_93.field_2;
                rdx1_1 = var_89;
                r87_1 = var_96;
                local_sp_1 = var_92;
                rdx1_0 = var_89;
                r13_0 = r13_7;
                r15_0 = r15_7;
                rbp_0 = rbp_5;
                r12_0 = r12_5;
                r96_0 = var_95;
                r87_0 = var_96;
                local_sp_0 = var_92;
                r13_1 = r13_7;
                r15_1 = r15_7;
                rbp_1 = rbp_5;
                r12_1 = r12_5;
                r96_1 = var_95;
                if (var_94 == 0UL) {
                    loop_state_var = 0U;
                } else {
                    *(uint64_t *)(local_sp_11 + 16UL) = (var_94 + 1UL);
                    *(unsigned char *)(local_sp_11 + 61UL) = (unsigned char)'\x01';
                    loop_state_var = 1U;
                }
            }
        }
    } else {
        var_26 = r8 - var_24;
        var_27 = local_sp_7 + (-8L);
        *(uint64_t *)var_27 = 4211134UL;
        indirect_placeholder();
        rbp_2 = var_26;
        local_sp_8 = var_27;
        *(unsigned char *)(local_sp_8 + 71UL) = (unsigned char)'\x00';
        *(unsigned char *)(local_sp_8 + 70UL) = (unsigned char)'\x01';
        r13_5 = r13_4;
        r15_5 = r15_4;
        rbp_3 = rbp_2;
        r12_3 = r12_2;
        local_sp_9 = local_sp_8;
        r15_11_ph = r15_4;
        local_sp_15_ph = local_sp_8;
        if ((long)*(uint64_t *)(local_sp_8 + 48UL) > (long)r12_2) {
            r15_11 = r15_11_ph;
            r96_12 = r96_12_ph;
            r87_12 = r87_12_ph;
            local_sp_15 = local_sp_15_ph;
            while (rdi != 0UL)
                {
                    var_99 = helper_cc_compute_c_wrapper(rbx_0_ph - r15_11, r15_11, cc_src2_0_ph, 17U);
                    if (var_99 == 0UL) {
                        break;
                    }
                    var_100 = local_sp_15 + (-8L);
                    *(uint64_t *)var_100 = 4211897UL;
                    var_101 = indirect_placeholder_8(0UL, 1UL, 0UL, r96_12, r87_12);
                    r15_11 = r15_11 + 1UL;
                    r96_12 = var_101.field_1;
                    r87_12 = var_101.field_2;
                    local_sp_15 = var_100;
                }
            return;
        }
        *(uint64_t *)(local_sp_8 + 8UL) = rsi;
        loop_state_var = 2U;
    }
}
