typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_closeout_ret_type;
struct indirect_placeholder_45_ret_type;
struct indirect_placeholder_46_ret_type;
struct indirect_placeholder_44_ret_type;
struct indirect_placeholder_48_ret_type;
struct indirect_placeholder_49_ret_type;
struct indirect_placeholder_50_ret_type;
struct indirect_placeholder_51_ret_type;
struct bb_closeout_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_45_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_46_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_44_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_48_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_49_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_50_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_51_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_47(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_13(uint64_t param_0);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r9(void);
extern uint64_t init_rax(void);
extern uint64_t init_r8(void);
extern struct indirect_placeholder_45_ret_type indirect_placeholder_45(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_46_ret_type indirect_placeholder_46(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_44_ret_type indirect_placeholder_44(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_48_ret_type indirect_placeholder_48(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_49_ret_type indirect_placeholder_49(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_50_ret_type indirect_placeholder_50(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_51_ret_type indirect_placeholder_51(uint64_t param_0);
struct bb_closeout_ret_type bb_closeout(uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t rcx) {
    struct indirect_placeholder_46_ret_type var_59;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint32_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t rax_4;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t r9_3;
    uint64_t r8_3;
    uint64_t rcx6_2;
    uint32_t _pre116;
    uint32_t _pre_pre_phi;
    uint64_t local_sp_1;
    uint32_t _pre_phi;
    uint64_t local_sp_0;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t rcx6_3;
    uint64_t var_54;
    uint64_t local_sp_2;
    uint64_t var_46;
    uint16_t var_47;
    bool var_48;
    uint64_t var_49;
    uint32_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    struct indirect_placeholder_44_ret_type var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_63;
    struct indirect_placeholder_48_ret_type var_64;
    uint64_t local_sp_6;
    uint64_t var_42;
    uint64_t rbx_5;
    uint32_t *var_43;
    uint64_t rax_5;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t r8_5;
    struct indirect_placeholder_49_ret_type var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint32_t *_pre;
    uint32_t *_pre_phi117;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    struct indirect_placeholder_50_ret_type var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t local_sp_8;
    uint64_t local_sp_3;
    uint64_t rax_0;
    uint64_t local_sp_7;
    uint64_t local_sp_4;
    uint64_t rbx_6;
    uint64_t rax_1;
    uint64_t r9_5;
    uint64_t rbx_2;
    uint64_t r9_0;
    uint64_t r8_0;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t rax_2;
    uint64_t rdx3_0;
    uint32_t *_cast;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t local_sp_5;
    uint64_t rbx_3;
    uint64_t rdi4_0;
    uint64_t rcx6_0;
    uint64_t r9_1;
    uint64_t r8_1;
    uint64_t var_37;
    uint32_t var_38;
    uint64_t var_39;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_10;
    struct indirect_placeholder_51_ret_type var_11;
    uint64_t var_12;
    uint64_t rax_3;
    uint64_t rbx_4;
    uint64_t rsi5_1;
    uint64_t rcx6_1;
    uint64_t r9_2;
    uint64_t r8_2;
    uint64_t var_40;
    uint64_t r9_4;
    uint64_t r8_4;
    struct bb_closeout_ret_type mrv;
    struct bb_closeout_ret_type mrv1;
    struct bb_closeout_ret_type mrv2;
    uint64_t var_41;
    uint64_t var_23;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbx();
    var_3 = init_rbp();
    var_4 = init_r12();
    var_5 = init_r9();
    var_6 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_4;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    var_7 = (uint32_t)rsi;
    var_8 = (uint64_t)var_7;
    var_9 = (uint64_t)(uint32_t)rdx;
    r9_3 = var_5;
    r8_3 = var_6;
    rcx6_2 = 0UL;
    rbx_5 = var_8;
    rbx_2 = var_8;
    r9_0 = var_5;
    r8_0 = var_6;
    rdx3_0 = 0UL;
    rax_3 = var_1;
    rbx_4 = var_8;
    rsi5_1 = rsi;
    rcx6_1 = rcx;
    r9_2 = var_5;
    r8_2 = var_6;
    if (rdi == 0UL) {
        local_sp_6 = var_0 + (-72L);
    } else {
        var_10 = var_0 + (-80L);
        *(uint64_t *)var_10 = 4208535UL;
        var_11 = indirect_placeholder_51(rdi);
        var_12 = var_11.field_0;
        local_sp_3 = var_10;
        rax_0 = var_12;
        rcx6_2 = rcx;
        if ((uint64_t)(uint32_t)var_12 == 0UL) {
            *(uint64_t *)(var_0 + (-88L)) = 4208612UL;
            indirect_placeholder();
            var_13 = (uint64_t)*(uint32_t *)var_12;
            var_14 = var_0 + (-96L);
            *(uint64_t *)var_14 = 4208619UL;
            var_15 = indirect_placeholder_13(var_13);
            local_sp_3 = var_14;
            rax_0 = var_15;
            if ((uint64_t)(unsigned char)var_15 == 0UL) {
                *(uint64_t *)(var_0 + (-104L)) = 4208641UL;
                var_16 = indirect_placeholder_4(rcx, 0UL, 3UL);
                *(uint64_t *)(var_0 + (-112L)) = 4208649UL;
                indirect_placeholder();
                var_17 = (uint64_t)*(uint32_t *)var_16;
                var_18 = var_0 + (-120L);
                *(uint64_t *)var_18 = 4208674UL;
                var_19 = indirect_placeholder_50(0UL, 4271950UL, 1UL, var_17, var_16, var_5, var_6);
                var_20 = var_19.field_0;
                var_21 = var_19.field_5;
                var_22 = var_19.field_6;
                rax_5 = var_20;
                r8_5 = var_22;
                local_sp_8 = var_18;
                rbx_6 = var_16;
                r9_5 = var_21;
                var_23 = local_sp_8 + (-8L);
                *(uint64_t *)var_23 = 4208681UL;
                indirect_placeholder();
                local_sp_4 = var_23;
                rax_1 = rax_5;
                rbx_2 = rbx_6;
                r9_0 = r9_5;
                r8_0 = r8_5;
                if ((int)(uint32_t)rax_5 > (int)4294967295U) {
                    *(uint64_t *)(local_sp_8 + (-16L)) = 4208707UL;
                    var_24 = indirect_placeholder_4(rcx, 0UL, 3UL);
                    *(uint64_t *)(local_sp_8 + (-24L)) = 4208715UL;
                    indirect_placeholder();
                    var_25 = (uint64_t)*(uint32_t *)var_24;
                    var_26 = local_sp_8 + (-32L);
                    *(uint64_t *)var_26 = 4208740UL;
                    var_27 = indirect_placeholder_49(0UL, 4271950UL, 1UL, var_25, var_24, r9_5, r8_5);
                    var_28 = var_27.field_1;
                    var_29 = var_27.field_2;
                    var_30 = var_27.field_3;
                    var_31 = var_27.field_5;
                    var_32 = var_27.field_6;
                    _pre = (uint32_t *)var_29;
                    _pre_phi117 = _pre;
                    local_sp_5 = var_26;
                    rbx_3 = var_24;
                    rdi4_0 = var_28;
                    rcx6_0 = var_30;
                    r9_1 = var_31;
                    r8_1 = var_32;
                    var_37 = rcx6_0 + (-1L);
                    *(uint64_t *)4298688UL = var_37;
                    var_38 = *(uint32_t *)((var_37 << 2UL) + rdi4_0);
                    var_39 = (uint64_t)var_38;
                    *_pre_phi117 = var_38;
                    rax_4 = var_39;
                    r9_3 = r9_1;
                    r8_3 = r8_1;
                    rcx6_2 = var_37;
                    rbx_5 = rbx_3;
                    local_sp_7 = local_sp_5;
                } else {
                    var_33 = *(uint64_t *)4298688UL;
                    rax_4 = rax_1;
                    r9_3 = r9_0;
                    r8_3 = r8_0;
                    rbx_5 = rbx_2;
                    local_sp_7 = local_sp_4;
                    local_sp_5 = local_sp_4;
                    rbx_3 = rbx_2;
                    rcx6_0 = var_33;
                    r9_1 = r9_0;
                    r8_1 = r8_0;
                    if (var_33 != 0UL) {
                        var_34 = *(uint64_t *)4298704UL;
                        rax_2 = var_34;
                        rdi4_0 = var_34;
                        rcx6_2 = var_33;
                        while (1U)
                            {
                                _cast = (uint32_t *)rax_2;
                                _pre_phi117 = _cast;
                                if ((uint64_t)(*_cast - (uint32_t)rbx_2) != 0UL) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                var_35 = rdx3_0 + 1UL;
                                var_36 = rax_2 + 4UL;
                                rax_2 = var_36;
                                rdx3_0 = var_35;
                                rax_4 = var_36;
                                if (var_35 == var_33) {
                                    continue;
                                }
                                loop_state_var = 1U;
                                break;
                            }
                        var_37 = rcx6_0 + (-1L);
                        *(uint64_t *)4298688UL = var_37;
                        var_38 = *(uint32_t *)((var_37 << 2UL) + rdi4_0);
                        var_39 = (uint64_t)var_38;
                        *_pre_phi117 = var_38;
                        rax_4 = var_39;
                        r9_3 = r9_1;
                        r8_3 = r8_1;
                        rcx6_2 = var_37;
                        rbx_5 = rbx_3;
                        local_sp_7 = local_sp_5;
                    }
                }
            } else {
                local_sp_4 = local_sp_3;
                rax_1 = rax_0;
                local_sp_7 = local_sp_3;
                rax_4 = rax_0;
                var_33 = *(uint64_t *)4298688UL;
                rax_4 = rax_1;
                r9_3 = r9_0;
                r8_3 = r8_0;
                rbx_5 = rbx_2;
                local_sp_7 = local_sp_4;
                local_sp_5 = local_sp_4;
                rbx_3 = rbx_2;
                rcx6_0 = var_33;
                r9_1 = r9_0;
                r8_1 = r8_0;
                if ((int)var_7 >= (int)0U & var_33 != 0UL) {
                    var_34 = *(uint64_t *)4298704UL;
                    rax_2 = var_34;
                    rdi4_0 = var_34;
                    rcx6_2 = var_33;
                    while (1U)
                        {
                            _cast = (uint32_t *)rax_2;
                            _pre_phi117 = _cast;
                            if ((uint64_t)(*_cast - (uint32_t)rbx_2) != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            var_35 = rdx3_0 + 1UL;
                            var_36 = rax_2 + 4UL;
                            rax_2 = var_36;
                            rdx3_0 = var_35;
                            rax_4 = var_36;
                            if (var_35 == var_33) {
                                continue;
                            }
                            loop_state_var = 1U;
                            break;
                        }
                    var_37 = rcx6_0 + (-1L);
                    *(uint64_t *)4298688UL = var_37;
                    var_38 = *(uint32_t *)((var_37 << 2UL) + rdi4_0);
                    var_39 = (uint64_t)var_38;
                    *_pre_phi117 = var_38;
                    rax_4 = var_39;
                    r9_3 = r9_1;
                    r8_3 = r8_1;
                    rcx6_2 = var_37;
                    rbx_5 = rbx_3;
                    local_sp_7 = local_sp_5;
                }
            }
        } else {
            local_sp_4 = local_sp_3;
            rax_1 = rax_0;
            local_sp_7 = local_sp_3;
            rax_4 = rax_0;
            if ((int)var_7 < (int)0U) {
                var_40 = helper_cc_compute_all_wrapper(var_9, 0UL, 0UL, 24U);
                rbx_4 = rbx_5;
                rcx6_3 = rcx6_2;
                r9_4 = r9_3;
                r8_4 = r8_3;
                if ((uint64_t)(((unsigned char)(var_40 >> 4UL) ^ (unsigned char)var_40) & '\xc0') == 0UL) {
                    mrv.field_0 = rcx6_3;
                    mrv1 = mrv;
                    mrv1.field_1 = r9_4;
                    mrv2 = mrv1;
                    mrv2.field_2 = r8_4;
                    return mrv2;
                }
                *(uint32_t *)(local_sp_7 + 12UL) = 0U;
                var_41 = local_sp_7 + (-8L);
                *(uint64_t *)var_41 = 4208786UL;
                indirect_placeholder();
                local_sp_2 = var_41;
                if ((uint64_t)((uint32_t)rax_4 + 1U) != 0UL) {
                    var_42 = local_sp_7 + (-16L);
                    *(uint64_t *)var_42 = 4208914UL;
                    indirect_placeholder();
                    var_43 = (uint32_t *)rax_4;
                    local_sp_2 = var_42;
                    if (*var_43 != 10U) {
                        *(uint64_t *)(local_sp_7 + (-24L)) = 4208924UL;
                        indirect_placeholder();
                        var_44 = (uint64_t)*var_43;
                        var_45 = local_sp_7 + (-32L);
                        *(uint64_t *)var_45 = 4208946UL;
                        indirect_placeholder_45(0UL, 4271623UL, 1UL, var_44, rcx6_2, r9_3, r8_3);
                        _pre116 = (uint32_t)rbx_5;
                        _pre_pre_phi = _pre116;
                        local_sp_1 = var_45;
                        var_54 = local_sp_1 + (-8L);
                        *(uint64_t *)var_54 = 4208968UL;
                        indirect_placeholder();
                        _pre_phi = _pre_pre_phi;
                        local_sp_0 = var_54;
                        var_55 = *(uint64_t *)4298720UL;
                        *(uint64_t *)(local_sp_0 + (-8L)) = 4208867UL;
                        var_56 = indirect_placeholder_4(rcx, 0UL, 3UL);
                        var_57 = (uint64_t)(_pre_phi + 128U);
                        var_58 = local_sp_0 + 8UL;
                        *(uint64_t *)(local_sp_0 + (-16L)) = 4208904UL;
                        var_59 = indirect_placeholder_46(0UL, 4268768UL, var_57, 0UL, var_56, var_55, var_58);
                        var_60 = var_59.field_3;
                        var_61 = var_59.field_5;
                        var_62 = var_59.field_6;
                        rcx6_3 = var_60;
                        r9_4 = var_61;
                        r8_4 = var_62;
                        mrv.field_0 = rcx6_3;
                        mrv1 = mrv;
                        mrv1.field_1 = r9_4;
                        mrv2 = mrv1;
                        mrv2.field_2 = r8_4;
                        return mrv2;
                    }
                }
                var_46 = (uint64_t)*(uint32_t *)(local_sp_2 + 12UL);
                var_47 = (uint16_t)var_46;
                var_48 = ((uint64_t)((uint32_t)(uint64_t)var_47 + (-1)) > 254UL);
                var_49 = var_46 & 127UL;
                rcx6_3 = var_46;
                if (!var_48) {
                    var_50 = (uint32_t)var_49;
                    _pre_phi = var_50;
                    _pre_pre_phi = var_50;
                    if ((uint64_t)(var_50 + (-13)) == 0UL) {
                        mrv.field_0 = rcx6_3;
                        mrv1 = mrv;
                        mrv1.field_1 = r9_4;
                        mrv2 = mrv1;
                        mrv2.field_2 = r8_4;
                        return mrv2;
                    }
                    var_51 = local_sp_2 + 16UL;
                    var_52 = local_sp_2 + (-8L);
                    *(uint64_t *)var_52 = 4208838UL;
                    var_53 = indirect_placeholder_47(var_49, var_51);
                    local_sp_0 = var_52;
                    local_sp_1 = var_52;
                    if ((uint64_t)(uint32_t)var_53 == 0UL) {
                        var_54 = local_sp_1 + (-8L);
                        *(uint64_t *)var_54 = 4208968UL;
                        indirect_placeholder();
                        _pre_phi = _pre_pre_phi;
                        local_sp_0 = var_54;
                    }
                    var_55 = *(uint64_t *)4298720UL;
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4208867UL;
                    var_56 = indirect_placeholder_4(rcx, 0UL, 3UL);
                    var_57 = (uint64_t)(_pre_phi + 128U);
                    var_58 = local_sp_0 + 8UL;
                    *(uint64_t *)(local_sp_0 + (-16L)) = 4208904UL;
                    var_59 = indirect_placeholder_46(0UL, 4268768UL, var_57, 0UL, var_56, var_55, var_58);
                    var_60 = var_59.field_3;
                    var_61 = var_59.field_5;
                    var_62 = var_59.field_6;
                    rcx6_3 = var_60;
                    r9_4 = var_61;
                    r8_4 = var_62;
                    mrv.field_0 = rcx6_3;
                    mrv1 = mrv;
                    mrv1.field_1 = r9_4;
                    mrv2 = mrv1;
                    mrv2.field_2 = r8_4;
                    return mrv2;
                }
                if (var_49 != 0UL) {
                    var_65 = (uint64_t)(unsigned char)(var_46 >> 8UL);
                    if ((uint64_t)(var_47 & (unsigned short)65280U) != 0UL) {
                        var_66 = *(uint64_t *)4298720UL;
                        *(uint64_t *)(local_sp_2 + (-8L)) = 4209008UL;
                        var_67 = indirect_placeholder_4(rcx, 0UL, 3UL);
                        *(uint64_t *)(local_sp_2 + (-16L)) = 4209039UL;
                        var_68 = indirect_placeholder_44(0UL, 4268816UL, var_65, 0UL, var_67, var_66, var_65);
                        var_69 = var_68.field_3;
                        var_70 = var_68.field_5;
                        var_71 = var_68.field_6;
                        rcx6_3 = var_69;
                        r9_4 = var_70;
                        r8_4 = var_71;
                    }
                    mrv.field_0 = rcx6_3;
                    mrv1 = mrv;
                    mrv1.field_1 = r9_4;
                    mrv2 = mrv1;
                    mrv2.field_2 = r8_4;
                    return mrv2;
                }
                var_63 = local_sp_2 + (-8L);
                *(uint64_t *)var_63 = 4209066UL;
                var_64 = indirect_placeholder_48(0UL, 4268856UL, 1UL, 0UL, var_46, r9_3, r8_3);
                local_sp_6 = var_63;
                rax_3 = var_64.field_0;
                rsi5_1 = var_64.field_2;
                rcx6_1 = var_64.field_3;
                r9_2 = var_64.field_5;
                r8_2 = var_64.field_6;
            } else {
                var_33 = *(uint64_t *)4298688UL;
                rax_4 = rax_1;
                r9_3 = r9_0;
                r8_3 = r8_0;
                rbx_5 = rbx_2;
                local_sp_7 = local_sp_4;
                local_sp_5 = local_sp_4;
                rbx_3 = rbx_2;
                rcx6_0 = var_33;
                r9_1 = r9_0;
                r8_1 = r8_0;
                if (var_33 != 0UL) {
                    var_34 = *(uint64_t *)4298704UL;
                    rax_2 = var_34;
                    rdi4_0 = var_34;
                    rcx6_2 = var_33;
                    while (1U)
                        {
                            _cast = (uint32_t *)rax_2;
                            _pre_phi117 = _cast;
                            if ((uint64_t)(*_cast - (uint32_t)rbx_2) != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            var_35 = rdx3_0 + 1UL;
                            var_36 = rax_2 + 4UL;
                            rax_2 = var_36;
                            rdx3_0 = var_35;
                            rax_4 = var_36;
                            if (var_35 == var_33) {
                                continue;
                            }
                            loop_state_var = 1U;
                            break;
                        }
                    var_37 = rcx6_0 + (-1L);
                    *(uint64_t *)4298688UL = var_37;
                    var_38 = *(uint32_t *)((var_37 << 2UL) + rdi4_0);
                    var_39 = (uint64_t)var_38;
                    *_pre_phi117 = var_38;
                    rax_4 = var_39;
                    r9_3 = r9_1;
                    r8_3 = r8_1;
                    rcx6_2 = var_37;
                    rbx_5 = rbx_3;
                    local_sp_7 = local_sp_5;
                }
            }
        }
    }
}
