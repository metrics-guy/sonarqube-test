typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_13(uint64_t param_0);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_rax(void);
typedef _Bool bool;
uint64_t bb_input_file_size(uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t rcx) {
    uint64_t local_sp_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_23;
    uint64_t rbx_0;
    uint64_t rbp_1;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t rbp_0;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint32_t *var_25;
    uint64_t local_sp_1;
    uint64_t var_24;
    uint64_t var_22;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r15();
    var_5 = init_rbp();
    var_6 = init_r12();
    var_7 = init_r14();
    var_8 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_4;
    *(uint64_t *)(var_0 + (-16L)) = var_7;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_6;
    *(uint64_t *)(var_0 + (-40L)) = var_5;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    *(uint64_t *)(var_0 + (-64L)) = rsi;
    var_9 = var_0 + (-80L);
    *(uint64_t *)var_9 = 4205459UL;
    indirect_placeholder();
    rbp_0 = 0UL;
    local_sp_0 = var_9;
    rbp_1 = 18446744073709551615UL;
    if ((long)var_1 < (long)0UL) {
        *(uint64_t *)(var_0 + (-88L)) = 4205645UL;
        indirect_placeholder();
        var_25 = (uint32_t *)var_1;
        if (*var_25 == 29U) {
            *(uint64_t *)(var_0 + (-96L)) = 4205662UL;
            indirect_placeholder();
            *var_25 = 0U;
        }
    } else {
        var_10 = (uint64_t)(uint32_t)rdi;
        while (1U)
            {
                var_11 = rcx - rbp_0;
                var_12 = rbp_0 + rdx;
                var_13 = local_sp_0 + (-8L);
                *(uint64_t *)var_13 = 4205494UL;
                var_14 = indirect_placeholder_4(var_11, var_10, var_12);
                local_sp_0 = var_13;
                rbp_1 = rbp_0;
                switch_state_var = 0;
                switch (var_14) {
                  case 18446744073709551615UL:
                    {
                        rbp_1 = 18446744073709551615UL;
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 0UL:
                    {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  default:
                    {
                        var_15 = var_14 + rbp_0;
                        var_16 = helper_cc_compute_c_wrapper(var_15 - rcx, rcx, var_8, 17U);
                        rbp_0 = var_15;
                        rbp_1 = 18446744073709551615UL;
                        if (var_16 == 0UL) {
                            continue;
                        }
                        var_17 = *(uint64_t *)local_sp_0;
                        var_18 = *(uint64_t *)(var_17 + 48UL);
                        rbx_0 = var_18;
                        if (var_18 != 0UL) {
                            *(uint64_t *)(local_sp_0 + (-16L)) = 4205675UL;
                            indirect_placeholder();
                            *(uint32_t *)var_17 = 75U;
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        var_19 = var_1 + var_15;
                        var_20 = local_sp_0 + (-16L);
                        *(uint64_t *)var_20 = 4205555UL;
                        var_21 = indirect_placeholder_13(var_17);
                        local_sp_1 = var_20;
                        if (!(((long)var_18 < (long)var_19) || ((uint64_t)(unsigned char)var_21 == 0UL))) {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        var_22 = local_sp_0 + (-24L);
                        *(uint64_t *)var_22 = 4205582UL;
                        indirect_placeholder();
                        rbx_0 = var_19;
                        local_sp_1 = var_22;
                        if ((long)var_21 >= (long)0UL) {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        if (var_19 != var_21) {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        var_23 = local_sp_0 + (-32L);
                        *(uint64_t *)var_23 = 4205706UL;
                        indirect_placeholder();
                        rbx_0 = ((long)var_21 < (long)var_19) ? var_19 : var_21;
                        local_sp_1 = var_23;
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
        var_24 = var_15 + (rbx_0 - var_19);
        rbp_1 = var_24;
        if (var_24 == 9223372036854775807UL) {
            *(uint64_t *)(local_sp_1 + (-8L)) = 4205625UL;
            indirect_placeholder();
            *(uint32_t *)9223372036854775807UL = 75U;
            rbp_1 = 18446744073709551615UL;
        }
    }
}
