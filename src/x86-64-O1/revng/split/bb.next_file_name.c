typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_80_ret_type;
struct indirect_placeholder_81_ret_type;
struct indirect_placeholder_82_ret_type;
struct indirect_placeholder_83_ret_type;
struct indirect_placeholder_80_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_81_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_82_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_83_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint32_t init_state_0x82fc(void);
extern void indirect_placeholder_10(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_80_ret_type indirect_placeholder_80(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
extern struct indirect_placeholder_81_ret_type indirect_placeholder_81(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_82_ret_type indirect_placeholder_82(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_83_ret_type indirect_placeholder_83(uint64_t param_0, uint64_t param_1);
void bb_next_file_name(uint64_t rcx, uint64_t r9, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint32_t var_6;
    uint64_t var_7;
    uint64_t r92_2;
    uint64_t rdx_0;
    uint64_t r92_1;
    uint64_t rcx1_2;
    uint64_t var_62;
    uint64_t local_sp_0;
    uint64_t var_63;
    uint64_t rdi_0;
    uint64_t rcx1_0;
    uint64_t rcx1_1;
    unsigned char var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_61;
    uint64_t var_57;
    uint64_t local_sp_1;
    uint64_t var_58;
    uint64_t r92_0;
    uint64_t r83_0;
    uint64_t local_sp_2;
    struct indirect_placeholder_80_ret_type var_59;
    uint64_t var_60;
    uint64_t var_54;
    uint64_t rbx_0;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t rcx1_4;
    uint64_t var_10;
    unsigned char var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    bool var_16;
    unsigned char *var_17;
    uint64_t r83_1;
    uint64_t rcx1_3;
    uint64_t rax_0;
    uint64_t *_cast;
    uint64_t var_18;
    bool var_19;
    unsigned char var_20;
    unsigned char *var_21;
    unsigned char var_22;
    unsigned char *var_23;
    uint64_t var_24;
    uint64_t r83_2;
    uint64_t var_25;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t rdi_1;
    uint64_t rcx1_5;
    uint64_t rcx1_6;
    unsigned char var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t rax_1;
    uint64_t rdi_2;
    uint64_t rcx1_7;
    uint64_t rcx1_8;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    struct indirect_placeholder_82_ret_type var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    struct indirect_placeholder_83_ret_type var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_rbp();
    var_4 = init_r12();
    var_5 = init_cc_src2();
    var_6 = init_state_0x82fc();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_2;
    var_7 = *(uint64_t *)4298392UL;
    r92_2 = r9;
    r92_1 = r9;
    rcx1_0 = 18446744073709551615UL;
    rcx1_1 = 0UL;
    rcx1_4 = rcx;
    r83_1 = r8;
    r83_2 = r8;
    rcx1_5 = 18446744073709551615UL;
    rcx1_6 = 0UL;
    rax_1 = 0UL;
    rcx1_7 = 18446744073709551615UL;
    rcx1_8 = 0UL;
    if (var_7 != 0UL) {
        var_25 = *(uint64_t *)4298168UL;
        if (var_25 == 0UL) {
            var_38 = *(uint64_t *)4298400UL;
            var_39 = (uint64_t)var_6;
            rdi_1 = var_38;
            while (rcx1_5 != 0UL)
                {
                    var_40 = *(unsigned char *)rdi_1;
                    var_41 = rcx1_5 + (-1L);
                    rcx1_5 = var_41;
                    rcx1_6 = var_41;
                    if (var_40 == '\x00') {
                        break;
                    }
                    rdi_1 = rdi_1 + var_39;
                }
            var_42 = 18446744073709551614UL - (-2L);
            *(uint64_t *)4298160UL = var_42;
            var_43 = *(uint64_t *)4298360UL;
            rdi_2 = var_43;
            if (var_43 != 0UL) {
                while (rcx1_7 != 0UL)
                    {
                        var_44 = (uint64_t)('\x00' - *(unsigned char *)rdi_2);
                        var_45 = rcx1_7 + (-1L);
                        rcx1_7 = var_45;
                        rcx1_8 = var_45;
                        if (var_44 == 0UL) {
                            break;
                        }
                        rdi_2 = rdi_2 + var_39;
                    }
                rax_1 = 18446744073709551614UL - (-2L);
            }
            *(uint64_t *)4298152UL = rax_1;
            var_46 = var_42 + *(uint64_t *)4298376UL;
            var_47 = rax_1 + var_46;
            *(uint64_t *)4298168UL = var_47;
            var_48 = var_47 + 1UL;
            rcx1_2 = var_46;
            if (var_42 <= var_48) {
                *(uint64_t *)(var_0 + (-48L)) = 4206385UL;
                indirect_placeholder_10(r92_2, r83_2);
                abort();
            }
            *(uint64_t *)(var_0 + (-48L)) = 4205979UL;
            var_49 = indirect_placeholder_82(var_7, var_48);
            var_50 = var_49.field_0;
            var_51 = var_49.field_1;
            var_52 = var_49.field_2;
            *(uint64_t *)4298392UL = var_50;
            var_53 = var_0 + (-56L);
            *(uint64_t *)var_53 = 4206011UL;
            indirect_placeholder();
            r92_0 = var_51;
            r83_0 = var_52;
            local_sp_2 = var_53;
            var_54 = *(uint64_t *)4298160UL;
            rbx_0 = var_50;
        } else {
            *(uint64_t *)4298168UL = (var_25 + 2UL);
            *(uint64_t *)4298376UL = (*(uint64_t *)4298376UL + 1UL);
            var_26 = var_25 + 3UL;
            var_27 = *(uint64_t *)4298160UL;
            var_28 = helper_cc_compute_c_wrapper(var_26 - var_27, var_27, var_5, 17U);
            if (var_28 != 0UL) {
                *(uint64_t *)(var_0 + (-48L)) = 4206385UL;
                indirect_placeholder_10(r92_2, r83_2);
                abort();
            }
            var_29 = var_0 + (-48L);
            *(uint64_t *)var_29 = 4206057UL;
            var_30 = indirect_placeholder_83(var_7, var_26);
            var_31 = var_30.field_0;
            var_32 = var_30.field_1;
            var_33 = var_30.field_2;
            *(uint64_t *)4298392UL = var_31;
            var_34 = *(uint64_t *)4298160UL;
            var_35 = **(uint64_t **)4298144UL;
            var_36 = *(uint64_t *)4297448UL;
            *(unsigned char *)(var_34 + var_31) = *(unsigned char *)(var_35 + var_36);
            var_37 = var_34 + 1UL;
            *(uint64_t *)4298160UL = var_37;
            rcx1_2 = var_36;
            r92_0 = var_32;
            r83_0 = var_33;
            local_sp_2 = var_29;
            var_54 = var_37;
            rbx_0 = var_31;
        }
        *(uint64_t *)4298384UL = (rbx_0 + var_54);
        var_55 = *(uint64_t *)4298376UL;
        var_56 = local_sp_2 + (-8L);
        *(uint64_t *)var_56 = 4206154UL;
        indirect_placeholder();
        local_sp_1 = var_56;
        if (*(uint64_t *)4298360UL == 0UL) {
            var_57 = local_sp_2 + (-16L);
            *(uint64_t *)var_57 = 4206182UL;
            indirect_placeholder();
            local_sp_1 = var_57;
        }
        *(unsigned char *)(*(uint64_t *)4298168UL + rbx_0) = (unsigned char)'\x00';
        *(uint64_t *)(local_sp_1 + (-8L)) = 4206205UL;
        indirect_placeholder();
        var_58 = local_sp_1 + (-16L);
        *(uint64_t *)var_58 = 4206218UL;
        var_59 = indirect_placeholder_80(rbx_0, var_55, var_25, 8UL, rcx1_2, var_7, r92_0, r83_0);
        *(uint64_t *)4298144UL = var_59.field_0;
        var_60 = *(uint64_t *)4298368UL;
        var_62 = var_60;
        local_sp_0 = var_58;
        if (var_60 != 0UL) {
            if (var_59.field_1 == 0UL) {
                var_61 = local_sp_1 + (-24L);
                *(uint64_t *)var_61 = 4206410UL;
                indirect_placeholder();
                var_62 = *(uint64_t *)4298368UL;
                local_sp_0 = var_61;
            }
            var_63 = (uint64_t)var_6;
            rdi_0 = var_62;
            while (rcx1_0 != 0UL)
                {
                    var_64 = *(unsigned char *)rdi_0;
                    var_65 = rcx1_0 + (-1L);
                    rcx1_0 = var_65;
                    rcx1_1 = var_65;
                    if (var_64 == '\x00') {
                        break;
                    }
                    rdi_0 = rdi_0 + var_63;
                }
            var_66 = *(uint64_t *)4298376UL;
            *(uint64_t *)(local_sp_0 + (-8L)) = 4206313UL;
            indirect_placeholder();
            if (rcx1_1 != 18446744073709551614UL) {
                rdx_0 = 18446744073709551613UL - (-3L);
                *(uint64_t *)((rdx_0 << 3UL) + (*(uint64_t *)4298144UL + (((rcx1_1 << 3UL) + 16UL) + (var_66 << 3UL)))) = (uint64_t)((long)(((uint64_t)*(unsigned char *)(rdx_0 + var_62) << 32UL) + (-206158430208L)) >> (long)32UL);
                while (rdx_0 != 0UL)
                    {
                        rdx_0 = rdx_0 + (-1L);
                        *(uint64_t *)((rdx_0 << 3UL) + (*(uint64_t *)4298144UL + (((rcx1_1 << 3UL) + 16UL) + (var_66 << 3UL)))) = (uint64_t)((long)(((uint64_t)*(unsigned char *)(rdx_0 + var_62) << 32UL) + (-206158430208L)) >> (long)32UL);
                    }
            }
        }
        return;
    }
    var_8 = *(uint64_t *)4298376UL;
    var_9 = var_8 + (-1L);
    rax_0 = var_9;
    if (var_8 == 0UL) {
        *(uint64_t *)(var_0 + (-48L)) = 4206548UL;
        indirect_placeholder_81(0UL, 4268576UL, 1UL, 0UL, rcx1_4, r92_1, r83_1);
        abort();
    }
    var_10 = *(uint64_t *)4298144UL;
    var_11 = *(unsigned char *)4297456UL;
    var_12 = (uint64_t)var_11;
    var_13 = *(uint64_t *)4297448UL;
    var_14 = *(uint64_t *)4298384UL;
    var_15 = (var_9 << 3UL) + var_10;
    var_16 = (var_11 == '\x00');
    var_17 = (unsigned char *)var_13;
    rcx1_3 = var_15;
    r92_1 = var_12;
    r92_2 = var_12;
    while (1U)
        {
            _cast = (uint64_t *)rcx1_3;
            var_18 = *_cast + 1UL;
            *_cast = var_18;
            var_19 = (rax_0 == 0UL);
            rcx1_4 = rcx1_3;
            r83_1 = rcx1_3;
            r83_2 = rcx1_3;
            if ((var_19 ^ 1) || var_16) {
                var_22 = *(unsigned char *)(var_18 + var_13);
                var_23 = (unsigned char *)(rax_0 + var_14);
                *var_23 = var_22;
                if (var_22 != '\x00') {
                    loop_state_var = 2U;
                    break;
                }
                *_cast = 0UL;
                *var_23 = *var_17;
                var_24 = rcx1_3 + (-8L);
                rcx1_3 = var_24;
                rcx1_4 = var_24;
                if (!var_19) {
                    loop_state_var = 1U;
                    break;
                }
                rax_0 = rax_0 + (-1L);
                continue;
            }
            if (*(unsigned char *)((*(uint64_t *)var_10 + var_13) + 1UL) != '\x00') {
                loop_state_var = 0U;
                break;
            }
            var_20 = *(unsigned char *)(var_18 + var_13);
            var_21 = (unsigned char *)var_14;
            *var_21 = var_20;
            if (var_20 != '\x00') {
                loop_state_var = 2U;
                break;
            }
            *_cast = 0UL;
            *var_21 = *var_17;
            loop_state_var = 1U;
            break;
        }
    switch (loop_state_var) {
      case 1U:
        {
            *(uint64_t *)(var_0 + (-48L)) = 4206548UL;
            indirect_placeholder_81(0UL, 4268576UL, 1UL, 0UL, rcx1_4, r92_1, r83_1);
            abort();
        }
        break;
      case 2U:
        {
            return;
        }
        break;
      case 0U:
        {
            var_25 = *(uint64_t *)4298168UL;
            if (var_25 == 0UL) {
                var_38 = *(uint64_t *)4298400UL;
                var_39 = (uint64_t)var_6;
                rdi_1 = var_38;
                while (rcx1_5 != 0UL)
                    {
                        var_40 = *(unsigned char *)rdi_1;
                        var_41 = rcx1_5 + (-1L);
                        rcx1_5 = var_41;
                        rcx1_6 = var_41;
                        if (var_40 == '\x00') {
                            break;
                        }
                        rdi_1 = rdi_1 + var_39;
                    }
                var_42 = 18446744073709551614UL - (-2L);
                *(uint64_t *)4298160UL = var_42;
                var_43 = *(uint64_t *)4298360UL;
                rdi_2 = var_43;
                if (var_43 != 0UL) {
                    while (rcx1_7 != 0UL)
                        {
                            var_44 = (uint64_t)('\x00' - *(unsigned char *)rdi_2);
                            var_45 = rcx1_7 + (-1L);
                            rcx1_7 = var_45;
                            rcx1_8 = var_45;
                            if (var_44 == 0UL) {
                                break;
                            }
                            rdi_2 = rdi_2 + var_39;
                        }
                    rax_1 = 18446744073709551614UL - (-2L);
                }
                *(uint64_t *)4298152UL = rax_1;
                var_46 = var_42 + *(uint64_t *)4298376UL;
                var_47 = rax_1 + var_46;
                *(uint64_t *)4298168UL = var_47;
                var_48 = var_47 + 1UL;
                rcx1_2 = var_46;
                if (var_42 <= var_48) {
                    *(uint64_t *)(var_0 + (-48L)) = 4206385UL;
                    indirect_placeholder_10(r92_2, r83_2);
                    abort();
                }
                *(uint64_t *)(var_0 + (-48L)) = 4205979UL;
                var_49 = indirect_placeholder_82(var_7, var_48);
                var_50 = var_49.field_0;
                var_51 = var_49.field_1;
                var_52 = var_49.field_2;
                *(uint64_t *)4298392UL = var_50;
                var_53 = var_0 + (-56L);
                *(uint64_t *)var_53 = 4206011UL;
                indirect_placeholder();
                r92_0 = var_51;
                r83_0 = var_52;
                local_sp_2 = var_53;
                var_54 = *(uint64_t *)4298160UL;
                rbx_0 = var_50;
            } else {
                *(uint64_t *)4298168UL = (var_25 + 2UL);
                *(uint64_t *)4298376UL = (*(uint64_t *)4298376UL + 1UL);
                var_26 = var_25 + 3UL;
                var_27 = *(uint64_t *)4298160UL;
                var_28 = helper_cc_compute_c_wrapper(var_26 - var_27, var_27, var_5, 17U);
                if (var_28 != 0UL) {
                    *(uint64_t *)(var_0 + (-48L)) = 4206385UL;
                    indirect_placeholder_10(r92_2, r83_2);
                    abort();
                }
                var_29 = var_0 + (-48L);
                *(uint64_t *)var_29 = 4206057UL;
                var_30 = indirect_placeholder_83(var_7, var_26);
                var_31 = var_30.field_0;
                var_32 = var_30.field_1;
                var_33 = var_30.field_2;
                *(uint64_t *)4298392UL = var_31;
                var_34 = *(uint64_t *)4298160UL;
                var_35 = **(uint64_t **)4298144UL;
                var_36 = *(uint64_t *)4297448UL;
                *(unsigned char *)(var_34 + var_31) = *(unsigned char *)(var_35 + var_36);
                var_37 = var_34 + 1UL;
                *(uint64_t *)4298160UL = var_37;
                rcx1_2 = var_36;
                r92_0 = var_32;
                r83_0 = var_33;
                local_sp_2 = var_29;
                var_54 = var_37;
                rbx_0 = var_31;
            }
            *(uint64_t *)4298384UL = (rbx_0 + var_54);
            var_55 = *(uint64_t *)4298376UL;
            var_56 = local_sp_2 + (-8L);
            *(uint64_t *)var_56 = 4206154UL;
            indirect_placeholder();
            local_sp_1 = var_56;
            if (*(uint64_t *)4298360UL == 0UL) {
                var_57 = local_sp_2 + (-16L);
                *(uint64_t *)var_57 = 4206182UL;
                indirect_placeholder();
                local_sp_1 = var_57;
            }
            *(unsigned char *)(*(uint64_t *)4298168UL + rbx_0) = (unsigned char)'\x00';
            *(uint64_t *)(local_sp_1 + (-8L)) = 4206205UL;
            indirect_placeholder();
            var_58 = local_sp_1 + (-16L);
            *(uint64_t *)var_58 = 4206218UL;
            var_59 = indirect_placeholder_80(rbx_0, var_55, var_25, 8UL, rcx1_2, var_7, r92_0, r83_0);
            *(uint64_t *)4298144UL = var_59.field_0;
            var_60 = *(uint64_t *)4298368UL;
            var_62 = var_60;
            local_sp_0 = var_58;
            if (var_60 != 0UL) {
                if (var_59.field_1 == 0UL) {
                    var_61 = local_sp_1 + (-24L);
                    *(uint64_t *)var_61 = 4206410UL;
                    indirect_placeholder();
                    var_62 = *(uint64_t *)4298368UL;
                    local_sp_0 = var_61;
                }
                var_63 = (uint64_t)var_6;
                rdi_0 = var_62;
                while (rcx1_0 != 0UL)
                    {
                        var_64 = *(unsigned char *)rdi_0;
                        var_65 = rcx1_0 + (-1L);
                        rcx1_0 = var_65;
                        rcx1_1 = var_65;
                        if (var_64 == '\x00') {
                            break;
                        }
                        rdi_0 = rdi_0 + var_63;
                    }
                var_66 = *(uint64_t *)4298376UL;
                *(uint64_t *)(local_sp_0 + (-8L)) = 4206313UL;
                indirect_placeholder();
                if (rcx1_1 != 18446744073709551614UL) {
                    rdx_0 = 18446744073709551613UL - (-3L);
                    *(uint64_t *)((rdx_0 << 3UL) + (*(uint64_t *)4298144UL + (((rcx1_1 << 3UL) + 16UL) + (var_66 << 3UL)))) = (uint64_t)((long)(((uint64_t)*(unsigned char *)(rdx_0 + var_62) << 32UL) + (-206158430208L)) >> (long)32UL);
                    while (rdx_0 != 0UL)
                        {
                            rdx_0 = rdx_0 + (-1L);
                            *(uint64_t *)((rdx_0 << 3UL) + (*(uint64_t *)4298144UL + (((rcx1_1 << 3UL) + 16UL) + (var_66 << 3UL)))) = (uint64_t)((long)(((uint64_t)*(unsigned char *)(rdx_0 + var_62) << 32UL) + (-206158430208L)) >> (long)32UL);
                        }
                }
            }
            return;
        }
        break;
    }
}
