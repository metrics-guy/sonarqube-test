typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint32_t init_state_0x82fc(void);
extern uint64_t init_rax(void);
extern void indirect_placeholder_10(uint64_t param_0);
void bb_initialize_regex(void) {
    uint64_t local_sp_2;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint32_t var_3;
    uint64_t var_4;
    uint64_t local_sp_1;
    uint64_t local_sp_0;
    uint64_t rbx_0;
    unsigned char var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t rdi_0;
    uint64_t rax_0;
    uint64_t var_9;
    uint64_t rcx_0;
    uint64_t var_8;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbx();
    var_3 = init_state_0x82fc();
    var_4 = var_0 + (-8L);
    *(uint64_t *)var_4 = var_2;
    local_sp_1 = var_4;
    local_sp_0 = var_4;
    rbx_0 = 0UL;
    rdi_0 = 4363904UL;
    rax_0 = 0UL;
    rcx_0 = 32UL;
    if (*(unsigned char *)4365184UL != '\x00') {
        var_5 = (unsigned char)var_1;
        var_6 = local_sp_0 + (-8L);
        *(uint64_t *)var_6 = 4206031UL;
        indirect_placeholder();
        *(unsigned char *)(rbx_0 + 4364224UL) = var_5;
        local_sp_0 = var_6;
        local_sp_1 = var_6;
        while (rbx_0 != 255UL)
            {
                rbx_0 = rbx_0 + 1UL;
                var_6 = local_sp_0 + (-8L);
                *(uint64_t *)var_6 = 4206031UL;
                indirect_placeholder();
                *(unsigned char *)(rbx_0 + 4364224UL) = var_5;
                local_sp_0 = var_6;
                local_sp_1 = var_6;
            }
    }
    var_7 = *(uint64_t *)4364832UL;
    local_sp_2 = local_sp_1;
    if (var_7 == 0UL) {
        if (*(unsigned char *)4363040UL == '\x00') {
            *(uint64_t *)4364832UL = 4332462UL;
        } else {
            if (*(unsigned char *)4365193UL == '\x00') {
                *(uint64_t *)4364832UL = 4321656UL;
            } else {
                *(uint64_t *)4364832UL = 4332462UL;
            }
        }
        var_8 = local_sp_1 + (-8L);
        *(uint64_t *)var_8 = 4206132UL;
        indirect_placeholder_10(4364832UL);
        local_sp_2 = var_8;
    } else {
        if (*(unsigned char *)var_7 == '\x00') {
            *(uint64_t *)4364832UL = 0UL;
        } else {
            var_8 = local_sp_1 + (-8L);
            *(uint64_t *)var_8 = 4206132UL;
            indirect_placeholder_10(4364832UL);
            local_sp_2 = var_8;
        }
    }
    if (*(uint64_t *)4364480UL == 0UL) {
        *(uint64_t *)(local_sp_2 + (-8L)) = 4206152UL;
        indirect_placeholder_10(4364480UL);
    } else {
        if (*(uint64_t *)4365176UL != 0UL) {
            if (*(unsigned char *)4363040UL == '\x00') {
                *(unsigned char *)(rax_0 + 4363904UL) = ((uint64_t)((((uint32_t)rax_0 | 32U) + (-97)) & (-2)) < 26UL);
                while (rax_0 != 255UL)
                    {
                        rax_0 = rax_0 + 1UL;
                        *(unsigned char *)(rax_0 + 4363904UL) = ((uint64_t)((((uint32_t)rax_0 | 32U) + (-97)) & (-2)) < 26UL);
                    }
            }
            var_9 = (uint64_t)var_3 << 3UL;
            while (rcx_0 != 0UL)
                {
                    *(uint64_t *)rdi_0 = 72340172838076673UL;
                    rcx_0 = rcx_0 + (-1L);
                    rdi_0 = var_9 + rdi_0;
                }
            *(unsigned char *)4363936UL = (unsigned char)'\x00';
            *(unsigned char *)4363913UL = (unsigned char)'\x00';
            *(unsigned char *)4363914UL = (unsigned char)'\x00';
        }
    }
    return;
}
