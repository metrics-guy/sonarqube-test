typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_4(uint64_t param_0);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
uint64_t bb_compare_words(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t *var_11;
    uint64_t var_12;
    bool var_13;
    uint64_t var_14;
    bool var_15;
    uint64_t local_sp_1;
    uint64_t var_24;
    uint64_t *_pre_phi;
    uint64_t rbp_1;
    uint64_t local_sp_0;
    uint64_t var_33;
    uint64_t rbp_0;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t *var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t rax_0;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t *var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r15();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_4;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_1;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_8 = var_0 + (-56L);
    var_9 = *(uint64_t *)(rsi + 8UL);
    var_10 = *(uint64_t *)(rdi + 8UL);
    var_11 = (uint64_t *)var_8;
    *var_11 = var_10;
    var_12 = ((long)var_9 > (long)var_10) ? var_10 : var_9;
    var_13 = (*(unsigned char *)4365184UL == '\x00');
    var_14 = helper_cc_compute_all_wrapper(var_12, 0UL, 0UL, 25U);
    var_15 = ((uint64_t)(((unsigned char)(var_14 >> 4UL) ^ (unsigned char)var_14) & '\xc0') == 0UL);
    local_sp_1 = var_8;
    _pre_phi = var_11;
    rbp_1 = 0UL;
    local_sp_0 = var_8;
    rbp_0 = 0UL;
    rax_0 = 4294967295UL;
    if (var_13) {
        if (!var_15) {
            var_25 = *(uint64_t *)rsi;
            while (1U)
                {
                    var_26 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(rbp_0 + *(uint64_t *)rdi);
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4204488UL;
                    var_27 = indirect_placeholder_4(var_26);
                    var_28 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(rbp_0 + var_25);
                    var_29 = local_sp_0 + (-16L);
                    var_30 = (uint64_t *)var_29;
                    *var_30 = 4204500UL;
                    var_31 = indirect_placeholder_4(var_28);
                    var_32 = (uint64_t)((uint32_t)(uint64_t)(unsigned char)var_27 - (uint32_t)(uint64_t)(unsigned char)var_31);
                    local_sp_0 = var_29;
                    _pre_phi = var_30;
                    rax_0 = var_32;
                    if (var_32 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_33 = rbp_0 + 1UL;
                    rbp_0 = var_33;
                    if (var_12 == var_33) {
                        continue;
                    }
                    loop_state_var = 0U;
                    break;
                }
            switch (loop_state_var) {
              case 1U:
                {
                    return rax_0;
                }
                break;
              case 0U:
                {
                    break;
                }
                break;
            }
        }
    }
    if (var_15) {
        var_34 = *_pre_phi;
        if ((long)var_9 <= (long)var_34) {
            var_35 = helper_cc_compute_all_wrapper(var_9 - var_34, var_34, var_7, 17U);
            var_36 = ((var_35 >> 11UL) ^ (var_35 >> 7UL)) & 1UL;
            rax_0 = var_36;
        }
        return rax_0;
    }
    var_16 = *(uint64_t *)rsi;
    while (1U)
        {
            var_17 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(rbp_1 + *(uint64_t *)rdi);
            *(uint64_t *)(local_sp_1 + (-8L)) = 4204549UL;
            var_18 = indirect_placeholder_4(var_17);
            var_19 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(rbp_1 + var_16);
            var_20 = local_sp_1 + (-16L);
            var_21 = (uint64_t *)var_20;
            *var_21 = 4204561UL;
            var_22 = indirect_placeholder_4(var_19);
            var_23 = (uint64_t)((uint32_t)(uint64_t)*(unsigned char *)((uint64_t)(unsigned char)var_18 + 4364224UL) - (uint32_t)(uint64_t)*(unsigned char *)((uint64_t)(unsigned char)var_22 + 4364224UL));
            local_sp_1 = var_20;
            _pre_phi = var_21;
            rax_0 = var_23;
            if (var_23 != 0UL) {
                loop_state_var = 1U;
                break;
            }
            var_24 = rbp_1 + 1UL;
            rbp_1 = var_24;
            if (var_12 == var_24) {
                continue;
            }
            loop_state_var = 0U;
            break;
        }
    switch (loop_state_var) {
      case 1U:
        {
            return rax_0;
        }
        break;
      case 0U:
        {
            break;
        }
        break;
    }
}
