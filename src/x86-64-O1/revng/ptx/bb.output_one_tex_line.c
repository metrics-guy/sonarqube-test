typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_31(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_4(uint64_t param_0);
extern uint64_t init_r13(void);
extern void indirect_placeholder_3(uint64_t param_0, uint64_t param_1);
void bb_output_one_tex_line(void) {
    uint64_t var_14;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_18;
    uint64_t rbx_1;
    uint64_t rbx_0;
    uint64_t local_sp_0;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t local_sp_1;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t local_sp_2;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r12();
    var_5 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_3;
    *(uint64_t *)(var_0 + (-48L)) = 4212654UL;
    indirect_placeholder();
    *(uint64_t *)(var_0 + (-56L)) = 4212664UL;
    indirect_placeholder();
    var_6 = *(uint64_t *)4363424UL;
    var_7 = *(uint64_t *)4363432UL;
    *(uint64_t *)(var_0 + (-64L)) = 4212683UL;
    indirect_placeholder_3(var_6, var_7);
    *(uint64_t *)(var_0 + (-72L)) = 4212713UL;
    indirect_placeholder();
    var_8 = *(uint64_t *)4363392UL;
    var_9 = *(uint64_t *)4363400UL;
    *(uint64_t *)(var_0 + (-80L)) = 4212732UL;
    indirect_placeholder_3(var_8, var_9);
    *(uint64_t *)(var_0 + (-88L)) = 4212755UL;
    indirect_placeholder();
    var_10 = *(uint64_t *)4363360UL;
    var_11 = *(uint64_t *)4363368UL;
    rbx_0 = var_10;
    rbx_1 = var_10;
    if (*(uint64_t *)4364480UL == 0UL) {
        var_15 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)var_10;
        var_16 = var_0 + (-96L);
        *(uint64_t *)var_16 = 4213030UL;
        var_17 = indirect_placeholder_4(var_15);
        local_sp_0 = var_16;
        local_sp_2 = var_16;
        if (*(unsigned char *)((uint64_t)(unsigned char)var_17 + 4363904UL) != '\x00') {
            var_18 = helper_cc_compute_c_wrapper(var_10 - var_11, var_11, var_5, 17U);
            if (var_18 == 0UL) {
                var_19 = rbx_0 + 1UL;
                rbx_0 = var_19;
                rbx_1 = var_11;
                local_sp_2 = local_sp_0;
                while (var_11 != var_19)
                    {
                        var_20 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)var_19;
                        var_21 = local_sp_0 + (-8L);
                        *(uint64_t *)var_21 = 4213068UL;
                        var_22 = indirect_placeholder_4(var_20);
                        local_sp_0 = var_21;
                        rbx_1 = var_19;
                        local_sp_2 = var_21;
                        if (*(unsigned char *)((uint64_t)(unsigned char)var_22 + 4363904UL) == '\x00') {
                            break;
                        }
                        var_19 = rbx_0 + 1UL;
                        rbx_0 = var_19;
                        rbx_1 = var_11;
                        local_sp_2 = local_sp_0;
                    }
            }
        }
        rbx_1 = var_10 + 1UL;
    } else {
        var_12 = var_11 - var_10;
        var_13 = var_0 + (-96L);
        *(uint64_t *)var_13 = 4212816UL;
        var_14 = indirect_placeholder_31(0UL, var_12, 4364488UL, var_10, 0UL);
        local_sp_2 = var_13;
        if (var_14 != 18446744073709551614UL) {
            *(uint64_t *)(var_0 + (-104L)) = 4213022UL;
            indirect_placeholder();
            abort();
        }
        rbx_1 = var_10 + ((var_14 == 18446744073709551615UL) ? 1UL : var_14);
    }
    *(uint64_t *)(local_sp_2 + (-8L)) = 4212853UL;
    indirect_placeholder_3(var_10, rbx_1);
    *(uint64_t *)(local_sp_2 + (-16L)) = 4212876UL;
    indirect_placeholder();
    *(uint64_t *)(local_sp_2 + (-24L)) = 4212887UL;
    indirect_placeholder_3(rbx_1, var_11);
    *(uint64_t *)(local_sp_2 + (-32L)) = 4212910UL;
    indirect_placeholder();
    var_23 = *(uint64_t *)4363328UL;
    var_24 = *(uint64_t *)4363336UL;
    *(uint64_t *)(local_sp_2 + (-40L)) = 4212929UL;
    indirect_placeholder_3(var_23, var_24);
    var_25 = local_sp_2 + (-48L);
    *(uint64_t *)var_25 = 4212939UL;
    indirect_placeholder();
    local_sp_1 = var_25;
    if (*(unsigned char *)4365194UL != '\x00') {
        if (*(unsigned char *)4365193UL == '\x00') {
            *(uint64_t *)(local_sp_1 + (-8L)) = 4213006UL;
            indirect_placeholder();
            return;
        }
    }
    *(uint64_t *)(local_sp_2 + (-56L)) = 4212967UL;
    indirect_placeholder();
    var_26 = *(uint64_t *)4363296UL;
    var_27 = *(uint64_t *)4363304UL;
    *(uint64_t *)(local_sp_2 + (-64L)) = 4212986UL;
    indirect_placeholder_3(var_26, var_27);
    var_28 = local_sp_2 + (-72L);
    *(uint64_t *)var_28 = 4212996UL;
    indirect_placeholder();
    local_sp_1 = var_28;
    *(uint64_t *)(local_sp_1 + (-8L)) = 4213006UL;
    indirect_placeholder();
    return;
}
