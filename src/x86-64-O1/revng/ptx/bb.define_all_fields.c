typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_31(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_4(uint64_t param_0);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_rcx(void);
void bb_define_all_fields(uint64_t rdi) {
    uint64_t var_36;
    bool var_37;
    unsigned char *var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t rdx_3;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t r13_3;
    uint64_t var_52;
    uint64_t local_sp_16;
    uint64_t rbx_9;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_165;
    uint64_t var_166;
    uint64_t var_66;
    uint64_t var_150;
    uint64_t var_110;
    uint64_t var_43;
    uint64_t var_19;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t rcx_3;
    uint64_t rcx_5;
    uint64_t local_sp_38;
    uint64_t var_182;
    unsigned char rbp_9_in;
    uint64_t rbx_14;
    uint64_t rbx_24;
    uint64_t r13_6;
    uint64_t var_172;
    uint64_t local_sp_35;
    uint64_t rbp_7;
    uint64_t local_sp_37;
    uint64_t local_sp_36;
    uint64_t r15_0;
    uint64_t var_159;
    uint64_t local_sp_34;
    uint64_t r13_0;
    uint64_t r13_4;
    uint64_t local_sp_0;
    uint64_t rbx_22;
    uint64_t var_154;
    uint64_t var_155;
    uint64_t var_156;
    uint64_t var_157;
    uint64_t var_158;
    uint64_t rdx_0;
    uint64_t local_sp_1;
    uint64_t var_160;
    uint64_t var_161;
    uint64_t rdx_9;
    uint64_t r13_5;
    uint64_t rbp_6;
    uint64_t local_sp_33;
    uint64_t rcx_12;
    uint64_t rbp_5;
    uint64_t rdx_8;
    uint64_t local_sp_30;
    uint64_t local_sp_28;
    uint64_t rbx_19;
    uint64_t local_sp_15;
    uint64_t rcx_0;
    uint64_t rdx_1;
    uint64_t rbx_0;
    uint64_t local_sp_2;
    uint64_t var_119;
    uint64_t rcx_9;
    uint64_t rcx_11;
    uint64_t rbx_1;
    uint64_t local_sp_3;
    uint64_t var_115;
    uint64_t var_116;
    uint64_t var_117;
    uint64_t var_118;
    uint64_t var_114;
    uint64_t rax_1;
    uint64_t r14_0;
    uint64_t var_75;
    uint64_t rdx_11;
    uint64_t rbx_2;
    uint64_t rbp_10;
    uint64_t local_sp_4;
    uint64_t local_sp_39;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t rcx_1;
    uint64_t rbx_25;
    uint64_t rdx_2;
    uint64_t local_sp_5;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t rcx_13;
    uint64_t rcx_8;
    uint64_t local_sp_24;
    uint64_t var_104;
    uint64_t rbp_4;
    uint64_t rbx_16;
    uint64_t rbx_15;
    uint64_t local_sp_22;
    uint64_t var_94;
    uint64_t rbx_13;
    unsigned char storemerge;
    uint64_t var_87;
    uint64_t rbx_12;
    uint64_t rbx_11;
    uint64_t local_sp_31;
    uint64_t var_97;
    uint64_t rbp_2;
    uint64_t var_62;
    uint64_t var_61;
    uint64_t local_sp_12;
    uint64_t var_47;
    uint64_t rcx_7;
    uint64_t r13_1;
    uint64_t local_sp_6;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t r13_2;
    uint64_t local_sp_14;
    uint64_t local_sp_11;
    uint64_t rbx_7;
    uint64_t rcx_2;
    uint64_t rbx_3;
    uint64_t local_sp_7;
    uint64_t rbx_4;
    uint64_t local_sp_8;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_23;
    uint64_t rcx_4;
    uint64_t rbx_5;
    uint64_t local_sp_9;
    uint64_t var_28;
    uint64_t rcx_6;
    uint64_t rbp_0;
    uint64_t rbx_6;
    uint64_t local_sp_10;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_135;
    uint64_t var_136;
    uint64_t var_22;
    uint64_t rbp_1;
    uint64_t rax_0;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t local_sp_13;
    uint64_t rbx_8;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_57;
    uint64_t var_58;
    bool var_59;
    unsigned char *var_60;
    uint64_t var_63;
    uint64_t local_sp_17;
    uint64_t rbp_3;
    uint64_t rdx_4;
    uint64_t rbx_10;
    uint64_t r14_1;
    uint64_t local_sp_18;
    uint64_t local_sp_21;
    uint64_t var_79;
    uint64_t local_sp_19;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t local_sp_20;
    uint64_t var_86;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t local_sp_23;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t var_100;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t local_sp_25;
    uint64_t rdx_5;
    uint64_t rbx_17;
    uint64_t local_sp_26;
    uint64_t var_120;
    uint64_t var_121;
    uint64_t var_122;
    uint64_t rax_2;
    uint64_t rcx_10;
    uint64_t rdx_6;
    uint64_t rbx_18;
    uint64_t local_sp_27;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t var_107;
    uint64_t var_108;
    uint64_t var_109;
    uint64_t var_111;
    uint64_t var_112;
    uint64_t var_113;
    uint64_t rdx_7;
    uint64_t local_sp_29;
    uint64_t var_123;
    uint32_t var_124;
    uint64_t var_125;
    uint32_t var_126;
    uint64_t var_127;
    uint64_t var_128;
    uint64_t r15_1;
    uint64_t rbx_20;
    uint64_t var_129;
    uint64_t *var_130;
    uint64_t var_131;
    uint64_t var_132;
    uint64_t var_133;
    uint64_t var_134;
    uint64_t var_137;
    uint64_t var_138;
    uint64_t local_sp_32;
    uint64_t rbx_21;
    uint64_t var_139;
    uint64_t var_140;
    uint64_t var_141;
    uint64_t var_142;
    uint64_t var_143;
    uint64_t var_144;
    bool var_145;
    unsigned char *var_146;
    uint64_t var_147;
    uint64_t var_148;
    uint64_t var_149;
    uint64_t var_151;
    uint64_t var_152;
    uint64_t var_153;
    uint64_t rbx_23;
    uint64_t var_162;
    unsigned char storemerge6;
    uint64_t var_163;
    uint64_t var_164;
    uint64_t var_167;
    uint64_t var_168;
    uint64_t var_169;
    uint64_t var_170;
    uint64_t var_171;
    uint64_t var_173;
    uint64_t var_174;
    uint64_t rax_5;
    uint64_t var_175;
    uint64_t var_176;
    uint64_t var_177;
    uint64_t var_178;
    uint64_t var_179;
    uint64_t var_180;
    uint64_t var_181;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rcx();
    var_2 = init_rbp();
    var_3 = init_r13();
    var_4 = init_rbx();
    var_5 = init_r15();
    var_6 = init_r12();
    var_7 = init_r14();
    var_8 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_5;
    *(uint64_t *)(var_0 + (-16L)) = var_7;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_6;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    *(uint64_t *)(var_0 + (-48L)) = var_4;
    var_9 = var_0 + (-104L);
    *(uint64_t *)(var_0 + (-72L)) = rdi;
    var_10 = *(uint64_t *)rdi;
    *(uint64_t *)4363360UL = var_10;
    var_11 = var_10 + *(uint64_t *)(rdi + 8UL);
    *(uint64_t *)4363368UL = var_11;
    *(uint64_t *)(var_0 + (-64L)) = (var_10 + *(uint64_t *)(rdi + 16UL));
    var_12 = var_10 + *(uint64_t *)(rdi + 24UL);
    var_13 = ((uint64_t)*(uint32_t *)(rdi + 40UL) << 4UL) + *(uint64_t *)4363768UL;
    *(uint64_t *)(var_0 + (-80L)) = *(uint64_t *)var_13;
    var_14 = *(uint64_t *)(var_13 + 8UL);
    var_15 = helper_cc_compute_c_wrapper(var_11 - var_12, var_12, var_8, 17U);
    rcx_3 = var_1;
    rcx_5 = var_1;
    rbp_9_in = (unsigned char)'\x00';
    r13_6 = 0UL;
    r15_0 = 0UL;
    r14_0 = 0UL;
    rbp_4 = 0UL;
    storemerge = (unsigned char)'\x00';
    local_sp_11 = var_9;
    rbx_7 = var_11;
    rbx_5 = var_11;
    local_sp_9 = var_9;
    rax_0 = 0UL;
    r15_1 = 0UL;
    storemerge6 = (unsigned char)'\x00';
    if (var_15 == 0UL) {
        while (1U)
            {
                var_16 = *(uint64_t *)4363360UL;
                rcx_3 = rcx_5;
                local_sp_12 = local_sp_11;
                rcx_2 = rcx_5;
                rbx_4 = rbx_7;
                rcx_4 = rcx_5;
                rbx_5 = var_12;
                rcx_6 = rcx_5;
                rbx_6 = rbx_7;
                rbp_1 = var_16;
                if (rbx_7 <= (var_16 + *(uint64_t *)4363448UL)) {
                    loop_state_var = 0U;
                    break;
                }
                *(uint64_t *)4363368UL = rbx_7;
                if (*(uint64_t *)4364480UL != 0UL) {
                    var_17 = var_12 - rbx_7;
                    var_18 = local_sp_11 + (-8L);
                    *(uint64_t *)var_18 = 4209442UL;
                    var_19 = indirect_placeholder_31(0UL, var_17, 4364488UL, rbx_7, 0UL);
                    rcx_2 = 0UL;
                    local_sp_7 = var_18;
                    if (var_19 == 18446744073709551614UL) {
                        *(uint64_t *)(local_sp_11 + (-16L)) = 4209336UL;
                        indirect_placeholder();
                        abort();
                    }
                    rbx_3 = rbx_7 + ((var_19 == 18446744073709551615UL) ? 1UL : var_19);
                    rcx_3 = rcx_2;
                    rcx_5 = rcx_2;
                    local_sp_11 = local_sp_7;
                    rbx_7 = rbx_3;
                    rbx_5 = rbx_3;
                    local_sp_9 = local_sp_7;
                    if (var_12 > rbx_3) {
                        continue;
                    }
                    loop_state_var = 2U;
                    break;
                }
                var_20 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)rbx_7;
                var_21 = local_sp_11 + (-8L);
                *(uint64_t *)var_21 = 4209344UL;
                var_22 = indirect_placeholder_4(var_20);
                local_sp_7 = var_21;
                local_sp_8 = var_21;
                local_sp_10 = var_21;
                if (*(unsigned char *)((uint64_t)(unsigned char)var_22 + 4363904UL) != '\x00') {
                    if (var_12 <= rbx_7) {
                        var_23 = *(uint64_t *)4363360UL;
                        rbp_0 = var_23;
                        loop_state_var = 1U;
                        break;
                    }
                    while (1U)
                        {
                            var_24 = rbx_4 + 1UL;
                            rbx_3 = var_24;
                            rbx_4 = var_24;
                            local_sp_9 = local_sp_8;
                            if (var_12 != var_24) {
                                loop_state_var = 0U;
                                break;
                            }
                            var_25 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)var_24;
                            var_26 = local_sp_8 + (-8L);
                            *(uint64_t *)var_26 = 4209487UL;
                            var_27 = indirect_placeholder_4(var_25);
                            local_sp_7 = var_26;
                            local_sp_8 = var_26;
                            if (*(unsigned char *)((uint64_t)(unsigned char)var_27 + 4363904UL) == '\x00') {
                                continue;
                            }
                            loop_state_var = 1U;
                            break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 1U:
                        {
                            break;
                        }
                        break;
                      case 0U:
                        {
                            loop_state_var = 2U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
                rbx_3 = rbx_7 + 1UL;
            }
        switch (loop_state_var) {
          case 1U:
            {
                *(uint64_t *)4363368UL = rbx_6;
                rcx_6 = rcx_4;
                rbp_1 = rbp_0;
                local_sp_12 = local_sp_10;
            }
            break;
          case 0U:
            {
                break;
            }
            break;
          case 2U:
            {
                var_28 = *(uint64_t *)4363360UL;
                local_sp_12 = local_sp_9;
                rcx_4 = rcx_3;
                rcx_6 = rcx_3;
                rbp_0 = var_28;
                rbx_6 = rbx_5;
                local_sp_10 = local_sp_9;
                rbp_1 = var_28;
                if (rbx_5 > (var_28 + *(uint64_t *)4363448UL)) {
                    *(uint64_t *)4363368UL = rbx_6;
                    rcx_6 = rcx_4;
                    rbp_1 = rbp_0;
                    local_sp_12 = local_sp_10;
                }
            }
            break;
        }
    } else {
        var_28 = *(uint64_t *)4363360UL;
        local_sp_12 = local_sp_9;
        rcx_4 = rcx_3;
        rcx_6 = rcx_3;
        rbp_0 = var_28;
        rbx_6 = rbx_5;
        local_sp_10 = local_sp_9;
        rbp_1 = var_28;
        if (rbx_5 > (var_28 + *(uint64_t *)4363448UL)) {
            *(uint64_t *)4363368UL = rbx_6;
            rcx_6 = rcx_4;
            rbp_1 = rbp_0;
            local_sp_12 = local_sp_10;
        }
        local_sp_13 = local_sp_12;
        local_sp_14 = local_sp_12;
        rcx_7 = rcx_6;
        if (*(uint64_t *)4363016UL == 0UL) {
            var_29 = helper_cc_compute_c_wrapper(*(uint64_t *)4363368UL - var_12, var_12, var_8, 17U);
            rax_0 = (uint64_t)(unsigned char)var_29;
        }
        *(unsigned char *)4363344UL = ((unsigned char)rax_0 & '\x01');
        var_30 = *(uint64_t *)4363368UL;
        r13_2 = var_30;
        if (var_30 <= rbp_1) {
            *(unsigned char *)(local_sp_12 + 8UL) = (unsigned char)'\x00';
            while (1U)
                {
                    rbx_8 = r13_2 + (-1L);
                    *(uint64_t *)(local_sp_13 + 16UL) = r13_2;
                    var_31 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)rbx_8;
                    var_32 = local_sp_13 + (-8L);
                    *(uint64_t *)var_32 = 4209617UL;
                    var_33 = indirect_placeholder_4(var_31);
                    var_34 = (uint64_t)(unsigned char)var_33;
                    var_35 = local_sp_13 + (-16L);
                    *(uint64_t *)var_35 = 4209625UL;
                    var_36 = indirect_placeholder_4(var_34);
                    var_37 = ((uint64_t)(uint32_t)var_36 == 0UL);
                    var_38 = (unsigned char *)var_32;
                    r13_2 = rbx_8;
                    local_sp_13 = var_35;
                    local_sp_14 = var_35;
                    if (var_37) {
                        *var_38 = (unsigned char)'\x01';
                        if (rbp_1 == rbx_8) {
                            continue;
                        }
                        *(uint64_t *)4363368UL = rbp_1;
                        break;
                    }
                    if (*var_38 == '\x00') {
                        break;
                    }
                    *(uint64_t *)4363368UL = *(uint64_t *)local_sp_13;
                    break;
                }
        }
        var_39 = *(uint64_t *)(*(uint64_t *)(local_sp_14 + 32UL) + 16UL);
        var_40 = *(uint64_t *)4363872UL + *(uint64_t *)4363464UL;
        rdx_3 = var_40;
        local_sp_15 = local_sp_14;
        if ((long)var_40 < (long)(0UL - var_39)) {
            r13_3 = var_39 + rbp_1;
        } else {
            var_41 = rbp_1 - var_40;
            r13_1 = var_41;
            rcx_7 = 0UL;
            rdx_3 = 1UL;
            r13_3 = var_41;
            if (*(uint64_t *)4364480UL == 0UL) {
                var_44 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)var_41;
                var_45 = local_sp_14 + (-8L);
                *(uint64_t *)var_45 = 4209797UL;
                var_46 = indirect_placeholder_4(var_44);
                local_sp_6 = var_45;
                local_sp_15 = var_45;
                if (*(unsigned char *)((uint64_t)(unsigned char)var_46 + 4363904UL) != '\x00') {
                    var_47 = helper_cc_compute_c_wrapper(0UL - var_40, rbp_1, var_8, 17U);
                    if (var_47 == 0UL) {
                        var_48 = r13_1 + 1UL;
                        r13_1 = var_48;
                        r13_3 = rbp_1;
                        local_sp_15 = local_sp_6;
                        while (var_48 != rbp_1)
                            {
                                var_49 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)var_48;
                                var_50 = local_sp_6 + (-8L);
                                *(uint64_t *)var_50 = 4209833UL;
                                var_51 = indirect_placeholder_4(var_49);
                                local_sp_6 = var_50;
                                r13_3 = var_48;
                                local_sp_15 = var_50;
                                if (*(unsigned char *)((uint64_t)(unsigned char)var_51 + 4363904UL) == '\x00') {
                                    break;
                                }
                                var_48 = r13_1 + 1UL;
                                r13_1 = var_48;
                                r13_3 = rbp_1;
                                local_sp_15 = local_sp_6;
                            }
                    }
                }
                r13_3 = var_41 + 1UL;
            } else {
                var_42 = local_sp_14 + (-8L);
                *(uint64_t *)var_42 = 4209737UL;
                var_43 = indirect_placeholder_31(0UL, var_40, 4364488UL, var_41, 0UL);
                local_sp_15 = var_42;
                if (var_43 != 18446744073709551614UL) {
                    *(uint64_t *)(local_sp_14 + (-16L)) = 4209787UL;
                    indirect_placeholder();
                    abort();
                }
                r13_3 = var_41 + ((var_43 == 18446744073709551615UL) ? 1UL : var_43);
            }
        }
        *(uint64_t *)4363392UL = r13_3;
        var_52 = *(uint64_t *)4363360UL;
        *(uint64_t *)4363400UL = var_52;
        local_sp_16 = local_sp_15;
        r13_4 = r13_3;
        r13_5 = r13_3;
        rdx_11 = rdx_3;
        rbx_25 = r13_3;
        rcx_13 = rcx_7;
        rcx_8 = rcx_7;
        rbp_2 = var_52;
        var_62 = var_52;
        local_sp_17 = local_sp_15;
        rdx_4 = rdx_3;
        rbx_10 = r13_3;
        if (var_52 <= r13_3) {
            *(unsigned char *)(local_sp_15 + 8UL) = (unsigned char)'\x00';
            var_62 = r13_3;
            while (1U)
                {
                    rbx_9 = rbp_2 + (-1L);
                    *(uint64_t *)(local_sp_16 + 16UL) = rbp_2;
                    var_53 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)rbx_9;
                    var_54 = local_sp_16 + (-8L);
                    *(uint64_t *)var_54 = 4209910UL;
                    var_55 = indirect_placeholder_4(var_53);
                    var_56 = (uint64_t)(unsigned char)var_55;
                    var_57 = local_sp_16 + (-16L);
                    *(uint64_t *)var_57 = 4209918UL;
                    var_58 = indirect_placeholder_4(var_56);
                    var_59 = ((uint64_t)(uint32_t)var_58 == 0UL);
                    var_60 = (unsigned char *)var_54;
                    rbp_2 = rbx_9;
                    local_sp_16 = var_57;
                    local_sp_17 = var_57;
                    if (var_59) {
                        *var_60 = (unsigned char)'\x01';
                        if (r13_3 == rbx_9) {
                            continue;
                        }
                        *(uint64_t *)4363400UL = r13_3;
                        break;
                    }
                    if (*var_60 != '\x00') {
                        var_62 = *(uint64_t *)4363400UL;
                        break;
                    }
                    var_61 = *(uint64_t *)local_sp_16;
                    *(uint64_t *)4363400UL = var_61;
                    var_62 = var_61;
                    break;
                }
        }
        var_63 = *(uint64_t *)4363456UL;
        rbp_10 = var_62;
        local_sp_39 = local_sp_17;
        rbp_3 = var_62;
        r14_1 = var_63;
        local_sp_18 = local_sp_17;
        if (var_62 > (var_63 + r13_3)) {
            var_76 = *(uint64_t *)4363392UL;
            var_77 = *(uint64_t *)4363456UL;
            var_78 = *(uint64_t *)4363400UL;
            rdx_11 = rdx_2;
            rbp_10 = var_78;
            local_sp_39 = local_sp_5;
            rbx_25 = var_76;
            rcx_13 = rcx_1;
            rcx_8 = rcx_1;
            rbp_3 = var_78;
            rdx_4 = rdx_2;
            rbx_10 = var_76;
            r14_1 = var_77;
            local_sp_18 = local_sp_5;
            do {
                rcx_1 = rcx_13;
                rdx_2 = rdx_11;
                if (*(uint64_t *)4364480UL == 0UL) {
                    var_67 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)rbx_25;
                    var_68 = local_sp_39 + (-8L);
                    *(uint64_t *)var_68 = 4210256UL;
                    var_69 = indirect_placeholder_4(var_67);
                    local_sp_4 = var_68;
                    local_sp_5 = var_68;
                    if (*(unsigned char *)((uint64_t)(unsigned char)var_69 + 4363904UL) == '\x00') {
                        *(uint64_t *)4363392UL = (rbx_25 + 1UL);
                    } else {
                        var_70 = *(uint64_t *)4363392UL;
                        var_71 = helper_cc_compute_c_wrapper(var_70 - rbp_10, rbp_10, var_8, 17U);
                        rbx_2 = var_70;
                        if (var_71 != 0UL) {
                            while (1U)
                                {
                                    var_72 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)rbx_2;
                                    var_73 = local_sp_4 + (-8L);
                                    *(uint64_t *)var_73 = 4210407UL;
                                    var_74 = indirect_placeholder_4(var_72);
                                    r14_0 = 1UL;
                                    local_sp_4 = var_73;
                                    local_sp_5 = var_73;
                                    if (*(unsigned char *)((uint64_t)(unsigned char)var_74 + 4363904UL) != '\x00') {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    var_75 = rbx_2 + 1UL;
                                    rbx_2 = var_75;
                                    if (var_75 == rbp_10) {
                                        continue;
                                    }
                                    loop_state_var = 1U;
                                    break;
                                }
                            switch (loop_state_var) {
                              case 1U:
                                {
                                    *(uint64_t *)4363392UL = rbp_10;
                                }
                                break;
                              case 0U:
                                {
                                    if (r14_0 == 0UL) {
                                        *(uint64_t *)4363392UL = rbx_2;
                                    }
                                }
                                break;
                            }
                        }
                    }
                } else {
                    var_64 = rbp_10 - rbx_25;
                    var_65 = local_sp_39 + (-8L);
                    *(uint64_t *)var_65 = 4210353UL;
                    var_66 = indirect_placeholder_31(0UL, var_64, 4364488UL, rbx_25, 0UL);
                    rcx_1 = 1UL;
                    rdx_2 = var_64;
                    local_sp_5 = var_65;
                    if (var_66 != 18446744073709551614UL) {
                        *(uint64_t *)(local_sp_39 + (-16L)) = 4210248UL;
                        indirect_placeholder();
                        abort();
                    }
                    *(uint64_t *)4363392UL = (*(uint64_t *)4363392UL + ((var_66 == 18446744073709551615UL) ? 1UL : var_66));
                }
                var_76 = *(uint64_t *)4363392UL;
                var_77 = *(uint64_t *)4363456UL;
                var_78 = *(uint64_t *)4363400UL;
                rdx_11 = rdx_2;
                rbp_10 = var_78;
                local_sp_39 = local_sp_5;
                rbx_25 = var_76;
                rcx_13 = rcx_1;
                rcx_8 = rcx_1;
                rbp_3 = var_78;
                rdx_4 = rdx_2;
                rbx_10 = var_76;
                r14_1 = var_77;
                local_sp_18 = local_sp_5;
            } while (var_78 <= (var_77 + var_76));
        }
        rdx_8 = rdx_4;
        rcx_9 = rcx_8;
        rcx_11 = rcx_8;
        rbx_12 = rbx_10;
        rbx_11 = rbx_10;
        local_sp_21 = local_sp_18;
        local_sp_19 = local_sp_18;
        local_sp_20 = local_sp_18;
        rdx_5 = rdx_4;
        if (*(uint64_t *)4363016UL != 0UL) {
            var_79 = helper_cc_compute_c_wrapper(*(uint64_t *)(local_sp_18 + 24UL) - rbx_10, rbx_10, var_8, 17U);
            if (var_79 != 0UL) {
                var_80 = rbx_11 + (-1L);
                var_81 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)var_80;
                *(uint64_t *)(local_sp_19 + (-8L)) = 4210018UL;
                var_82 = indirect_placeholder_4(var_81);
                var_83 = (uint64_t)(unsigned char)var_82;
                var_84 = local_sp_19 + (-16L);
                *(uint64_t *)var_84 = 4210026UL;
                var_85 = indirect_placeholder_4(var_83);
                rbx_11 = var_80;
                local_sp_19 = var_84;
                rbx_12 = rbx_11;
                local_sp_20 = var_84;
                while ((uint64_t)(uint32_t)var_85 != 0UL)
                    {
                        rbx_12 = var_80;
                        if (*(uint64_t *)(local_sp_19 + 8UL) == var_80) {
                            break;
                        }
                        var_80 = rbx_11 + (-1L);
                        var_81 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)var_80;
                        *(uint64_t *)(local_sp_19 + (-8L)) = 4210018UL;
                        var_82 = indirect_placeholder_4(var_81);
                        var_83 = (uint64_t)(unsigned char)var_82;
                        var_84 = local_sp_19 + (-16L);
                        *(uint64_t *)var_84 = 4210026UL;
                        var_85 = indirect_placeholder_4(var_83);
                        rbx_11 = var_80;
                        local_sp_19 = var_84;
                        rbx_12 = rbx_11;
                        local_sp_20 = var_84;
                    }
            }
            var_86 = helper_cc_compute_c_wrapper(*(uint64_t *)(local_sp_20 + 40UL) - rbx_12, rbx_12, var_8, 17U);
            local_sp_21 = local_sp_20;
            storemerge = (unsigned char)var_86;
        }
        *(unsigned char *)4363376UL = storemerge;
        var_87 = *(uint64_t *)4363392UL;
        rbx_13 = var_87;
        local_sp_22 = local_sp_21;
        rbx_14 = var_87;
        local_sp_23 = local_sp_21;
        if (var_14 <= var_87) {
            *(unsigned char *)(local_sp_21 + 8UL) = (unsigned char)'\x00';
            while (1U)
                {
                    var_88 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)rbx_13;
                    var_89 = local_sp_22 + (-8L);
                    *(uint64_t *)var_89 = 4210083UL;
                    var_90 = indirect_placeholder_4(var_88);
                    var_91 = (uint64_t)(unsigned char)var_90;
                    var_92 = local_sp_22 + (-16L);
                    *(uint64_t *)var_92 = 4210091UL;
                    var_93 = indirect_placeholder_4(var_91);
                    local_sp_22 = var_92;
                    rbx_14 = rbx_13;
                    local_sp_23 = var_92;
                    if ((uint64_t)(uint32_t)var_93 == 0UL) {
                        var_94 = rbx_13 + 1UL;
                        *(unsigned char *)var_89 = (unsigned char)'\x01';
                        rbx_13 = var_94;
                        rbx_14 = var_14;
                        if (var_94 == var_14) {
                            continue;
                        }
                        *(uint64_t *)4363392UL = var_14;
                        break;
                    }
                    if (*(unsigned char *)var_89 == '\x00') {
                        break;
                    }
                    *(uint64_t *)4363392UL = rbx_13;
                    break;
                }
        }
        var_95 = (r14_1 + (rbx_14 - rbp_3)) - *(uint64_t *)4363024UL;
        var_96 = helper_cc_compute_all_wrapper(var_95, 0UL, 0UL, 25U);
        local_sp_24 = local_sp_23;
        local_sp_25 = local_sp_23;
        local_sp_31 = local_sp_23;
        if ((uint64_t)(((unsigned char)(var_96 >> 4UL) ^ (unsigned char)var_96) & '\xc0') != 0UL) {
            *(uint64_t *)4363424UL = 0UL;
            *(uint64_t *)4363432UL = 0UL;
            *(unsigned char *)4363408UL = (unsigned char)'\x00';
            var_135 = (*(uint64_t *)4363448UL + (*(uint64_t *)4363360UL - *(uint64_t *)4363368UL)) - *(uint64_t *)4363024UL;
            var_136 = helper_cc_compute_all_wrapper(var_135, 0UL, 0UL, 25U);
            local_sp_32 = local_sp_31;
            local_sp_33 = local_sp_31;
            rdx_9 = rdx_8;
            local_sp_37 = local_sp_31;
            if ((uint64_t)(((unsigned char)(var_136 >> 4UL) ^ (unsigned char)var_136) & '\xc0') != 0UL) {
                var_137 = *(uint64_t *)4363392UL;
                *(uint64_t *)4363336UL = var_137;
                var_138 = *(uint64_t *)(local_sp_31 + 24UL);
                rbp_6 = var_137;
                if (var_137 <= var_138) {
                    *(unsigned char *)(local_sp_31 + 8UL) = (unsigned char)'\x00';
                    while (1U)
                        {
                            rbx_21 = rbp_6 + (-1L);
                            *(uint64_t *)(local_sp_32 + 16UL) = rbp_6;
                            var_139 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)rbx_21;
                            var_140 = local_sp_32 + (-8L);
                            *(uint64_t *)var_140 = 4211003UL;
                            var_141 = indirect_placeholder_4(var_139);
                            var_142 = (uint64_t)(unsigned char)var_141;
                            var_143 = local_sp_32 + (-16L);
                            *(uint64_t *)var_143 = 4211011UL;
                            var_144 = indirect_placeholder_4(var_142);
                            var_145 = ((uint64_t)(uint32_t)var_144 == 0UL);
                            var_146 = (unsigned char *)var_140;
                            rbp_6 = rbx_21;
                            local_sp_32 = var_143;
                            local_sp_33 = var_143;
                            if (var_145) {
                                *var_146 = (unsigned char)'\x01';
                                if (rbx_21 == var_138) {
                                    continue;
                                }
                                *(uint64_t *)4363336UL = var_138;
                                break;
                            }
                            if (*var_146 == '\x00') {
                                break;
                            }
                            *(uint64_t *)4363336UL = *(uint64_t *)local_sp_32;
                            break;
                        }
                }
                *(uint64_t *)4363328UL = r13_3;
                var_147 = *(uint64_t *)4363336UL;
                rbx_22 = var_147;
                local_sp_34 = local_sp_33;
                rbx_23 = var_147;
                local_sp_35 = local_sp_33;
                if (var_147 > (var_135 + r13_3)) {
                    var_160 = *(uint64_t *)4363328UL;
                    var_161 = *(uint64_t *)4363336UL;
                    local_sp_35 = local_sp_1;
                    local_sp_34 = local_sp_1;
                    r13_4 = var_160;
                    rbx_22 = var_161;
                    rdx_9 = rdx_0;
                    r13_5 = var_160;
                    rbx_23 = var_161;
                    do {
                        rdx_0 = rdx_9;
                        if (*(uint64_t *)4364480UL == 0UL) {
                            var_151 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)r13_4;
                            var_152 = local_sp_34 + (-8L);
                            *(uint64_t *)var_152 = 4211114UL;
                            var_153 = indirect_placeholder_4(var_151);
                            local_sp_0 = var_152;
                            local_sp_1 = var_152;
                            if (*(unsigned char *)((uint64_t)(unsigned char)var_153 + 4363904UL) == '\x00') {
                                *(uint64_t *)4363328UL = (r13_4 + 1UL);
                            } else {
                                var_154 = *(uint64_t *)4363328UL;
                                var_155 = helper_cc_compute_c_wrapper(var_154 - rbx_22, rbx_22, var_8, 17U);
                                r13_0 = var_154;
                                if (var_155 != 0UL) {
                                    while (1U)
                                        {
                                            var_156 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)r13_0;
                                            var_157 = local_sp_0 + (-8L);
                                            *(uint64_t *)var_157 = 4211256UL;
                                            var_158 = indirect_placeholder_4(var_156);
                                            r15_0 = 1UL;
                                            local_sp_0 = var_157;
                                            local_sp_1 = var_157;
                                            if (*(unsigned char *)((uint64_t)(unsigned char)var_158 + 4363904UL) != '\x00') {
                                                loop_state_var = 1U;
                                                break;
                                            }
                                            var_159 = r13_0 + 1UL;
                                            r13_0 = var_159;
                                            if (var_159 == rbx_22) {
                                                continue;
                                            }
                                            loop_state_var = 0U;
                                            break;
                                        }
                                    switch (loop_state_var) {
                                      case 0U:
                                        {
                                            *(uint64_t *)4363328UL = rbx_22;
                                        }
                                        break;
                                      case 1U:
                                        {
                                            if (r15_0 == 0UL) {
                                                *(uint64_t *)4363328UL = r13_0;
                                            }
                                        }
                                        break;
                                    }
                                }
                            }
                        } else {
                            var_148 = rbx_22 - r13_4;
                            var_149 = local_sp_34 + (-8L);
                            *(uint64_t *)var_149 = 4211205UL;
                            var_150 = indirect_placeholder_31(0UL, var_148, 4364488UL, r13_4, 0UL);
                            rdx_0 = var_148;
                            local_sp_1 = var_149;
                            if (var_150 != 18446744073709551614UL) {
                                *(uint64_t *)(local_sp_34 + (-16L)) = 4211104UL;
                                indirect_placeholder();
                                abort();
                            }
                            *(uint64_t *)4363328UL = (*(uint64_t *)4363328UL + ((var_150 == 18446744073709551615UL) ? 1UL : var_150));
                        }
                        var_160 = *(uint64_t *)4363328UL;
                        var_161 = *(uint64_t *)4363336UL;
                        local_sp_35 = local_sp_1;
                        local_sp_34 = local_sp_1;
                        r13_4 = var_160;
                        rbx_22 = var_161;
                        rdx_9 = rdx_0;
                        r13_5 = var_160;
                        rbx_23 = var_161;
                    } while (var_161 <= (var_135 + var_160));
                }
                var_162 = helper_cc_compute_c_wrapper(r13_5 - rbx_23, rbx_23, var_8, 17U);
                local_sp_36 = local_sp_35;
                local_sp_37 = local_sp_35;
                if (var_162 == 0UL) {
                    *(unsigned char *)4363376UL = (unsigned char)'\x00';
                    var_163 = (*(uint64_t *)4363016UL != 0UL);
                    var_164 = helper_cc_compute_c_wrapper(*(uint64_t *)(local_sp_35 + 40UL) - r13_5, r13_5, var_8, 17U);
                    storemerge6 = (unsigned char)var_164 & (unsigned char)var_163;
                }
                *(unsigned char *)4363312UL = storemerge6;
                var_165 = *(uint64_t *)4363328UL;
                var_166 = helper_cc_compute_c_wrapper(var_165 - rbx_23, rbx_23, var_8, 17U);
                rbp_7 = var_165;
                if (var_166 == 0UL) {
                    while (1U)
                        {
                            var_167 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)rbp_7;
                            *(uint64_t *)(local_sp_36 + (-8L)) = 4211388UL;
                            var_168 = indirect_placeholder_4(var_167);
                            var_169 = (uint64_t)(unsigned char)var_168;
                            var_170 = local_sp_36 + (-16L);
                            *(uint64_t *)var_170 = 4211396UL;
                            var_171 = indirect_placeholder_4(var_169);
                            r13_6 = 1UL;
                            local_sp_36 = var_170;
                            local_sp_37 = var_170;
                            if ((uint64_t)(uint32_t)var_171 == 0UL) {
                                var_172 = rbp_7 + 1UL;
                                rbp_7 = var_172;
                                if (var_172 == rbx_23) {
                                    continue;
                                }
                                *(uint64_t *)4363328UL = rbx_23;
                                break;
                            }
                            if (r13_6 == 0UL) {
                                break;
                            }
                            *(uint64_t *)4363328UL = rbp_7;
                            break;
                        }
                }
            }
            *(uint64_t *)4363328UL = 0UL;
            *(uint64_t *)4363336UL = 0UL;
            *(unsigned char *)4363312UL = (unsigned char)'\x00';
            local_sp_38 = local_sp_37;
            if (*(unsigned char *)4365194UL == '\x00') {
                var_175 = *(uint64_t *)(*(uint64_t *)(local_sp_37 + 32UL) + 32UL) + *(uint64_t *)4363360UL;
                *(uint64_t *)4363296UL = var_175;
                *(uint64_t *)4363304UL = var_175;
                var_176 = helper_cc_compute_c_wrapper(var_175 - var_12, var_12, var_8, 17U);
                rbx_24 = var_175;
                if (*(unsigned char *)4365193UL != '\x00' & var_176 == 0UL) {
                    while (1U)
                        {
                            var_177 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)rbx_24;
                            *(uint64_t *)(local_sp_38 + (-8L)) = 4211674UL;
                            var_178 = indirect_placeholder_4(var_177);
                            var_179 = (uint64_t)(unsigned char)var_178;
                            var_180 = local_sp_38 + (-16L);
                            *(uint64_t *)var_180 = 4211682UL;
                            var_181 = indirect_placeholder_4(var_179);
                            rbp_9_in = (unsigned char)'\x01';
                            local_sp_38 = var_180;
                            if ((uint64_t)(uint32_t)var_181 == 0UL) {
                                var_182 = rbx_24 + 1UL;
                                rbx_24 = var_182;
                                if (var_182 == var_12) {
                                    continue;
                                }
                                *(uint64_t *)4363304UL = var_12;
                                break;
                            }
                            if (rbp_9_in == '\x00') {
                                break;
                            }
                            *(uint64_t *)4363304UL = rbx_24;
                            break;
                        }
                }
            }
            var_173 = (uint64_t)*(uint32_t *)(*(uint64_t *)(local_sp_37 + 32UL) + 40UL);
            var_174 = helper_cc_compute_all_wrapper(var_173, 0UL, 0UL, 24U);
            rax_5 = var_173;
            if ((uint64_t)(((unsigned char)(var_174 >> 4UL) ^ (unsigned char)var_174) & '\xc0') == 0UL) {
                rax_5 = *(uint64_t *)4363776UL;
            }
            *(uint64_t *)(local_sp_37 + (-8L)) = 4211567UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_37 + (-16L)) = 4211591UL;
            indirect_placeholder();
            *(uint64_t *)4363304UL = rax_5;
            return;
        }
        var_97 = *(uint64_t *)4363368UL;
        *(uint64_t *)4363424UL = var_97;
        var_98 = helper_cc_compute_c_wrapper(var_97 - var_14, var_14, var_8, 17U);
        rbx_15 = var_97;
        rbx_16 = var_97;
        if (var_98 == 0UL) {
            while (1U)
                {
                    var_99 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)rbx_15;
                    *(uint64_t *)(local_sp_24 + (-8L)) = 4210177UL;
                    var_100 = indirect_placeholder_4(var_99);
                    var_101 = (uint64_t)(unsigned char)var_100;
                    var_102 = local_sp_24 + (-16L);
                    *(uint64_t *)var_102 = 4210185UL;
                    var_103 = indirect_placeholder_4(var_101);
                    rbp_4 = 1UL;
                    local_sp_24 = var_102;
                    rbx_16 = rbx_15;
                    local_sp_25 = var_102;
                    if ((uint64_t)(uint32_t)var_103 == 0UL) {
                        var_104 = rbx_15 + 1UL;
                        rbx_15 = var_104;
                        rbx_16 = var_14;
                        if (var_104 == var_14) {
                            continue;
                        }
                        *(uint64_t *)4363424UL = var_14;
                        break;
                    }
                    if (rbp_4 == 0UL) {
                        break;
                    }
                    *(uint64_t *)4363424UL = rbx_15;
                    break;
                }
        }
        *(uint64_t *)4363432UL = rbx_16;
        rbx_17 = rbx_16;
        local_sp_26 = local_sp_25;
        rbx_19 = rbx_16;
        local_sp_28 = local_sp_25;
        if (var_12 > rbx_16) {
            while (1U)
                {
                    var_105 = *(uint64_t *)4363424UL;
                    var_106 = var_95 + var_105;
                    var_107 = helper_cc_compute_c_wrapper(rbx_19 - var_106, var_106, var_8, 17U);
                    rcx_12 = rcx_11;
                    rcx_0 = rcx_11;
                    rdx_1 = var_106;
                    rcx_9 = rcx_11;
                    rbx_1 = rbx_19;
                    rdx_5 = var_106;
                    rbx_17 = var_12;
                    rax_2 = var_105;
                    rcx_10 = rcx_11;
                    rdx_6 = var_106;
                    rbx_18 = rbx_19;
                    rdx_7 = var_106;
                    local_sp_29 = local_sp_28;
                    if (var_107 != 0UL) {
                        loop_state_var = 2U;
                        break;
                    }
                    *(uint64_t *)4363432UL = rbx_19;
                    if (*(uint64_t *)4364480UL != 0UL) {
                        var_108 = var_12 - rbx_19;
                        var_109 = local_sp_28 + (-8L);
                        *(uint64_t *)var_109 = 4210656UL;
                        var_110 = indirect_placeholder_31(0UL, var_108, 4364488UL, rbx_19, 0UL);
                        rcx_0 = 0UL;
                        rdx_1 = var_108;
                        local_sp_2 = var_109;
                        if (var_110 == 18446744073709551614UL) {
                            *(uint64_t *)(local_sp_28 + (-16L)) = 4210556UL;
                            indirect_placeholder();
                            abort();
                        }
                        rbx_0 = rbx_19 + ((var_110 == 18446744073709551615UL) ? 1UL : var_110);
                        var_119 = helper_cc_compute_c_wrapper(rbx_0 - var_12, var_12, var_8, 17U);
                        local_sp_28 = local_sp_2;
                        rbx_19 = rbx_0;
                        rcx_9 = rcx_0;
                        rcx_11 = rcx_0;
                        rdx_5 = rdx_1;
                        rbx_17 = rbx_0;
                        local_sp_26 = local_sp_2;
                        if (var_119 == 0UL) {
                            continue;
                        }
                        loop_state_var = 1U;
                        break;
                    }
                    var_111 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)rbx_19;
                    var_112 = local_sp_28 + (-8L);
                    *(uint64_t *)var_112 = 4210564UL;
                    var_113 = indirect_placeholder_4(var_111);
                    local_sp_2 = var_112;
                    local_sp_3 = var_112;
                    local_sp_27 = var_112;
                    if (*(unsigned char *)((uint64_t)(unsigned char)var_113 + 4363904UL) != '\x00') {
                        if (var_12 <= rbx_19) {
                            var_114 = *(uint64_t *)4363424UL;
                            rax_1 = var_114;
                            loop_state_var = 0U;
                            break;
                        }
                        while (1U)
                            {
                                var_115 = rbx_1 + 1UL;
                                rbx_0 = var_115;
                                rbx_1 = var_115;
                                local_sp_26 = local_sp_3;
                                if (var_12 != var_115) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                var_116 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)var_115;
                                var_117 = local_sp_3 + (-8L);
                                *(uint64_t *)var_117 = 4210701UL;
                                var_118 = indirect_placeholder_4(var_116);
                                local_sp_2 = var_117;
                                local_sp_3 = var_117;
                                if (*(unsigned char *)((uint64_t)(unsigned char)var_118 + 4363904UL) == '\x00') {
                                    continue;
                                }
                                loop_state_var = 0U;
                                break;
                            }
                        switch_state_var = 0;
                        switch (loop_state_var) {
                          case 0U:
                            {
                                break;
                            }
                            break;
                          case 1U:
                            {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            break;
                        }
                        if (switch_state_var)
                            break;
                    }
                    rbx_0 = rbx_19 + 1UL;
                }
            switch (loop_state_var) {
              case 0U:
                {
                    *(uint64_t *)4363432UL = rbx_18;
                    rax_2 = rax_1;
                    rcx_12 = rcx_10;
                    rdx_7 = rdx_6;
                    local_sp_29 = local_sp_27;
                }
                break;
              case 2U:
                {
                    break;
                }
                break;
              case 1U:
                {
                    var_120 = *(uint64_t *)4363424UL;
                    var_121 = var_95 + var_120;
                    var_122 = helper_cc_compute_c_wrapper(rbx_17 - var_121, var_121, var_8, 17U);
                    rcx_12 = rcx_9;
                    rax_1 = var_120;
                    rax_2 = var_120;
                    rcx_10 = rcx_9;
                    rdx_6 = rdx_5;
                    rbx_18 = rbx_17;
                    local_sp_27 = local_sp_26;
                    rdx_7 = rdx_5;
                    local_sp_29 = local_sp_26;
                    if (var_122 == 0UL) {
                        *(uint64_t *)4363432UL = rbx_18;
                        rax_2 = rax_1;
                        rcx_12 = rcx_10;
                        rdx_7 = rdx_6;
                        local_sp_29 = local_sp_27;
                    }
                }
                break;
            }
        } else {
            var_120 = *(uint64_t *)4363424UL;
            var_121 = var_95 + var_120;
            var_122 = helper_cc_compute_c_wrapper(rbx_17 - var_121, var_121, var_8, 17U);
            rcx_12 = rcx_9;
            rax_1 = var_120;
            rax_2 = var_120;
            rcx_10 = rcx_9;
            rdx_6 = rdx_5;
            rbx_18 = rbx_17;
            local_sp_27 = local_sp_26;
            rdx_7 = rdx_5;
            local_sp_29 = local_sp_26;
            if (var_122 == 0UL) {
                *(uint64_t *)4363432UL = rbx_18;
                rax_2 = rax_1;
                rcx_12 = rcx_10;
                rdx_7 = rdx_6;
                local_sp_29 = local_sp_27;
            }
            var_123 = *(uint64_t *)4363432UL;
            rbp_5 = var_123;
            local_sp_30 = local_sp_29;
            rdx_8 = rdx_7;
            local_sp_31 = local_sp_29;
            if (var_123 <= rax_2) {
                *(unsigned char *)4363344UL = (unsigned char)'\x00';
                var_124 = ((uint32_t)rdx_7 & (-256)) | (uint32_t)(*(uint64_t *)4363016UL != 0UL);
                var_125 = helper_cc_compute_c_wrapper(var_123 - var_12, var_12, var_8, 17U);
                var_126 = var_124 & (((uint32_t)rcx_12 & (-256)) | (uint32_t)(uint64_t)(unsigned char)var_125);
                var_127 = (uint64_t)var_126;
                var_128 = (uint64_t)(var_126 & (-255));
                *(unsigned char *)4363408UL = (unsigned char)var_127;
                rdx_8 = var_128;
                while (1U)
                    {
                        rbx_20 = rbp_5 + (-1L);
                        *(uint64_t *)(local_sp_30 + 8UL) = rbp_5;
                        var_129 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)rbx_20;
                        var_130 = (uint64_t *)(local_sp_30 + (-8L));
                        *var_130 = 4210816UL;
                        var_131 = indirect_placeholder_4(var_129);
                        var_132 = (uint64_t)(unsigned char)var_131;
                        var_133 = local_sp_30 + (-16L);
                        *(uint64_t *)var_133 = 4210824UL;
                        var_134 = indirect_placeholder_4(var_132);
                        rbp_5 = rbx_20;
                        r15_1 = 1UL;
                        local_sp_30 = var_133;
                        local_sp_31 = var_133;
                        if ((uint64_t)(uint32_t)var_134 == 0UL) {
                            if (rax_2 == rbx_20) {
                                continue;
                            }
                            *(uint64_t *)4363432UL = rax_2;
                            break;
                        }
                        if (r15_1 == 0UL) {
                            break;
                        }
                        *(uint64_t *)4363432UL = *var_130;
                        break;
                    }
            }
            *(unsigned char *)4363408UL = (unsigned char)'\x00';
        }
    }
}
