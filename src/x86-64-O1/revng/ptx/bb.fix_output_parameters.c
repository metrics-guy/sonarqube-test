typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_4(uint64_t param_0);
extern uint32_t init_state_0x82fc(void);
void bb_fix_output_parameters(void) {
    unsigned char var_24;
    uint64_t var_25;
    uint32_t rdi_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint32_t var_3;
    uint64_t var_4;
    uint64_t local_sp_3;
    uint64_t rbx_0;
    uint64_t rax_0;
    uint32_t var_34;
    uint64_t local_sp_6;
    uint64_t local_sp_0;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    unsigned char var_39;
    uint64_t rbx_2;
    uint64_t rbx_1;
    uint64_t rcx_0;
    uint64_t rdi_1;
    uint64_t rcx_1;
    unsigned char var_8;
    uint64_t var_9;
    uint64_t rax_1;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t local_sp_1;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t local_sp_2;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t local_sp_4;
    uint64_t local_sp_5_ph;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t local_sp_5;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t rcx_2;
    uint64_t rdi_2;
    uint64_t rcx_3;
    uint64_t var_26;
    uint64_t var_27;
    bool var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t _pn;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = init_state_0x82fc();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_4 = var_0 + (-56L);
    rdi_0 = 36U;
    local_sp_3 = var_4;
    rbx_0 = 4323481UL;
    rax_0 = 128UL;
    rbx_2 = 0UL;
    rbx_1 = 0UL;
    rcx_0 = 18446744073709551615UL;
    rcx_1 = 0UL;
    rax_1 = 0UL;
    local_sp_1 = var_4;
    var_13 = 0UL;
    local_sp_2 = var_4;
    rcx_2 = 18446744073709551615UL;
    rcx_3 = 0UL;
    if (*(unsigned char *)4365194UL == '\x00') {
        *(uint64_t *)4363864UL = 0UL;
        if (*(uint32_t *)4363800UL != 0U) {
            var_5 = (uint64_t)var_3;
            var_12 = rbx_1 + 1UL;
            rbx_1 = var_12;
            var_13 = var_11;
            do {
                var_6 = local_sp_1 + (-8L);
                *(uint64_t *)var_6 = 4208712UL;
                indirect_placeholder();
                var_7 = *(uint64_t *)((rbx_1 << 3UL) + *(uint64_t *)4363784UL);
                rdi_1 = var_7;
                local_sp_1 = var_6;
                local_sp_2 = var_6;
                if (var_7 != 0UL) {
                    while (rcx_0 != 0UL)
                        {
                            var_8 = *(unsigned char *)rdi_1;
                            var_9 = rcx_0 + (-1L);
                            rcx_0 = var_9;
                            rcx_1 = var_9;
                            if (var_8 == '\x00') {
                                break;
                            }
                            rdi_1 = rdi_1 + var_5;
                        }
                    rax_1 = 18446744073709551614UL - (-2L);
                }
                var_10 = *(uint64_t *)4363864UL;
                var_11 = var_10;
                if ((long)var_10 < (long)rax_1) {
                    *(uint64_t *)4363864UL = rax_1;
                    var_11 = rax_1;
                }
                var_12 = rbx_1 + 1UL;
                rbx_1 = var_12;
                var_13 = var_11;
            } while (var_12 >= (uint64_t)*(uint32_t *)4363800UL);
        }
        *(uint64_t *)4363864UL = (var_13 + 1UL);
        var_14 = var_13 + 2UL;
        var_15 = local_sp_2 + (-8L);
        *(uint64_t *)var_15 = 4208806UL;
        var_16 = indirect_placeholder_4(var_14);
        *(uint64_t *)4363296UL = var_16;
        local_sp_3 = var_15;
        local_sp_4 = var_15;
        if (*(unsigned char *)4365194UL == '\x00') {
            local_sp_4 = local_sp_3;
            local_sp_5_ph = local_sp_3;
            if (*(unsigned char *)4365193UL == '\x00') {
                var_18 = *(uint64_t *)4363032UL;
                local_sp_5 = local_sp_5_ph;
            } else {
                local_sp_5_ph = local_sp_4;
                local_sp_5 = local_sp_4;
                if (*(unsigned char *)4365192UL == '\x00') {
                    var_17 = *(uint64_t *)4363032UL - (*(uint64_t *)4363024UL + *(uint64_t *)4363864UL);
                    *(uint64_t *)4363032UL = var_17;
                    var_18 = var_17;
                } else {
                    var_18 = *(uint64_t *)4363032UL;
                    local_sp_5 = local_sp_5_ph;
                }
            }
        } else {
            local_sp_5_ph = local_sp_4;
            local_sp_5 = local_sp_4;
            if (*(unsigned char *)4365192UL == '\x00') {
                var_17 = *(uint64_t *)4363032UL - (*(uint64_t *)4363024UL + *(uint64_t *)4363864UL);
                *(uint64_t *)4363032UL = var_17;
                var_18 = var_17;
            } else {
                var_18 = *(uint64_t *)4363032UL;
                local_sp_5 = local_sp_5_ph;
            }
        }
    } else {
        local_sp_4 = local_sp_3;
        local_sp_5_ph = local_sp_3;
        if (*(unsigned char *)4365193UL == '\x00') {
            var_18 = *(uint64_t *)4363032UL;
            local_sp_5 = local_sp_5_ph;
        } else {
            local_sp_5_ph = local_sp_4;
            local_sp_5 = local_sp_4;
            if (*(unsigned char *)4365192UL == '\x00') {
                var_17 = *(uint64_t *)4363032UL - (*(uint64_t *)4363024UL + *(uint64_t *)4363864UL);
                *(uint64_t *)4363032UL = var_17;
                var_18 = var_17;
            } else {
                var_18 = *(uint64_t *)4363032UL;
                local_sp_5 = local_sp_5_ph;
            }
        }
    }
    var_19 = var_18;
    local_sp_6 = local_sp_5;
    if ((long)var_18 < (long)0UL) {
        *(uint64_t *)4363032UL = 0UL;
        var_19 = 0UL;
    }
    var_20 = (uint64_t)((long)((var_19 >> 63UL) + var_19) >> (long)1UL);
    *(uint64_t *)4363464UL = var_20;
    var_21 = var_20 - *(uint64_t *)4363024UL;
    *(uint64_t *)4363456UL = var_21;
    *(uint64_t *)4363448UL = var_20;
    var_22 = *(uint64_t *)4363016UL;
    rdi_2 = var_22;
    if (var_22 == 0UL) {
        *(uint64_t *)4363016UL = 0UL;
        var_27 = *(uint64_t *)4363440UL;
    } else {
        var_23 = (uint64_t)var_3;
        while (rcx_2 != 0UL)
            {
                var_24 = *(unsigned char *)rdi_2;
                var_25 = rcx_2 + (-1L);
                rcx_2 = var_25;
                rcx_3 = var_25;
                if (var_24 == '\x00') {
                    break;
                }
                rdi_2 = rdi_2 + var_23;
            }
        var_26 = 18446744073709551614UL - (-2L);
        *(uint64_t *)4363440UL = var_26;
        var_27 = var_26;
    }
    var_28 = (*(unsigned char *)4363040UL == '\x00');
    var_29 = var_27 << 1UL;
    _pn = var_29;
    if (var_28) {
        _pn = var_29 | 1UL;
    } else {
        var_30 = var_21 - var_29;
        *(uint64_t *)4363456UL = (((long)var_30 > (long)0UL) ? var_30 : 0UL);
    }
    *(uint64_t *)4363448UL = (var_20 - _pn);
    var_31 = (uint64_t)(uint32_t)rbx_2;
    var_32 = local_sp_6 + (-8L);
    *(uint64_t *)var_32 = 4209065UL;
    var_33 = indirect_placeholder_4(var_31);
    *(unsigned char *)(rbx_2 + 4363488UL) = ((uint64_t)(uint32_t)var_33 != 0UL);
    local_sp_0 = var_32;
    local_sp_6 = var_32;
    while (rbx_2 != 255UL)
        {
            rbx_2 = rbx_2 + 1UL;
            var_31 = (uint64_t)(uint32_t)rbx_2;
            var_32 = local_sp_6 + (-8L);
            *(uint64_t *)var_32 = 4209065UL;
            var_33 = indirect_placeholder_4(var_31);
            *(unsigned char *)(rbx_2 + 4363488UL) = ((uint64_t)(uint32_t)var_33 != 0UL);
            local_sp_0 = var_32;
            local_sp_6 = var_32;
        }
    *(unsigned char *)4363500UL = (unsigned char)'\x01';
    var_34 = *(uint32_t *)4365188UL;
    if ((uint64_t)(var_34 + (-2)) != 0UL) {
        if ((uint64_t)(var_34 + (-3)) != 0UL) {
            var_35 = (uint64_t)(uint32_t)((int)(rdi_0 << 24U) >> (int)24U);
            var_36 = local_sp_0 + (-8L);
            *(uint64_t *)var_36 = 4209171UL;
            var_37 = indirect_placeholder_4(var_35);
            *(unsigned char *)((uint64_t)(unsigned char)var_37 + 4363488UL) = (unsigned char)'\x01';
            var_38 = rbx_0 + 1UL;
            var_39 = *(unsigned char *)var_38;
            rbx_0 = var_38;
            local_sp_0 = var_36;
            while (var_39 != '\x00')
                {
                    rdi_0 = (uint32_t)var_39;
                    var_35 = (uint64_t)(uint32_t)((int)(rdi_0 << 24U) >> (int)24U);
                    var_36 = local_sp_0 + (-8L);
                    *(uint64_t *)var_36 = 4209171UL;
                    var_37 = indirect_placeholder_4(var_35);
                    *(unsigned char *)((uint64_t)(unsigned char)var_37 + 4363488UL) = (unsigned char)'\x01';
                    var_38 = rbx_0 + 1UL;
                    var_39 = *(unsigned char *)var_38;
                    rbx_0 = var_38;
                    local_sp_0 = var_36;
                }
            *(unsigned char *)(rax_0 + 4363488UL) = (*(unsigned char *)((uint64_t)(unsigned char)rax_0 + 4326304UL) != '\x00');
            while (rax_0 != 255UL)
                {
                    rax_0 = rax_0 + 1UL;
                    *(unsigned char *)(rax_0 + 4363488UL) = (*(unsigned char *)((uint64_t)(unsigned char)rax_0 + 4326304UL) != '\x00');
                }
        }
    }
    *(unsigned char *)4363522UL = (unsigned char)'\x01';
    return;
}
