typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_12_ret_type;
struct indirect_placeholder_14_ret_type;
struct indirect_placeholder_15_ret_type;
struct indirect_placeholder_16_ret_type;
struct indirect_placeholder_12_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_14_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_15_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_16_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_4(uint64_t param_0);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern uint64_t init_rcx(void);
extern struct indirect_placeholder_12_ret_type indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_14_ret_type indirect_placeholder_14(uint64_t param_0);
extern struct indirect_placeholder_15_ret_type indirect_placeholder_15(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_16_ret_type indirect_placeholder_16(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
typedef _Bool bool;
void bb_find_occurs_in_text(uint64_t rdi, uint64_t r10) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t local_sp_0;
    uint64_t var_134;
    uint64_t local_sp_8;
    uint64_t r12_0;
    uint64_t local_sp_23;
    uint64_t var_135;
    uint64_t rcx_6;
    uint64_t var_136;
    uint64_t local_sp_17;
    uint64_t var_137;
    uint64_t r12_5;
    uint64_t var_138;
    uint64_t *var_125;
    uint64_t *var_126;
    uint64_t var_127;
    uint64_t var_128;
    uint64_t var_129;
    uint64_t var_130;
    uint64_t var_131;
    uint64_t var_132;
    uint64_t var_133;
    uint64_t local_sp_33;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t r15_5;
    uint64_t local_sp_13;
    uint64_t var_114;
    uint64_t r13_7;
    uint64_t rcx_3;
    uint64_t rbp_4;
    uint64_t local_sp_4;
    uint64_t var_81;
    uint64_t r13_2;
    uint64_t r13_1;
    uint64_t local_sp_19;
    uint64_t var_67;
    uint64_t r102_5_ph;
    uint64_t rcx_1;
    uint64_t rbp_6;
    uint64_t rcx_0;
    uint64_t r15_3;
    uint64_t rbp_0;
    uint64_t local_sp_32;
    uint64_t rbx_0;
    uint64_t r15_0;
    uint64_t r102_0;
    uint64_t local_sp_1;
    uint64_t rcx_11;
    uint64_t rbp_1;
    uint64_t rbx_1;
    uint64_t r15_1;
    uint64_t r102_1;
    uint64_t local_sp_2;
    uint64_t var_68;
    uint64_t rbp_5;
    unsigned char var_69;
    uint64_t r15_3_ph;
    uint64_t var_70;
    uint64_t rcx_5;
    uint64_t var_71;
    uint64_t rdx_1;
    uint64_t rbp_3;
    uint64_t rbp_2;
    uint64_t r13_3;
    uint64_t rdx_0;
    uint64_t r12_2;
    uint64_t r13_0;
    uint64_t r14_1;
    uint64_t r12_1;
    uint64_t local_sp_6;
    uint64_t r14_0;
    uint64_t local_sp_3;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t local_sp_5;
    uint64_t var_82;
    uint64_t r13_4;
    uint64_t r14_2;
    uint64_t local_sp_7;
    uint64_t var_83;
    uint64_t rcx_10_ph_be;
    uint64_t r13_5;
    uint64_t r14_3;
    uint64_t local_sp_9;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t local_sp_10;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    unsigned char var_100;
    uint64_t var_97;
    uint64_t var_98;
    uint64_t rbx_2;
    uint64_t local_sp_11;
    uint64_t var_99;
    uint64_t var_116;
    uint64_t var_101;
    uint64_t rcx_4;
    uint64_t var_102;
    uint64_t rdx_3;
    uint64_t rcx_2;
    uint64_t r13_8;
    uint64_t rdx_2;
    uint64_t local_sp_14;
    uint64_t r13_6;
    uint64_t local_sp_12;
    uint64_t *var_103;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t var_107;
    uint64_t var_108;
    uint64_t var_109;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t var_112;
    uint64_t var_113;
    uint64_t var_115;
    uint64_t r13_9;
    uint64_t r12_3;
    uint64_t local_sp_15;
    uint64_t r13_10;
    uint64_t r12_4;
    uint64_t local_sp_16;
    uint64_t *var_117;
    uint64_t var_118;
    uint64_t var_119;
    uint64_t var_120;
    uint64_t var_121;
    uint64_t var_122;
    uint64_t var_123;
    uint64_t var_124;
    uint64_t r13_11;
    uint64_t r12_6;
    uint64_t local_sp_18;
    uint64_t var_139;
    uint64_t rbp_8_ph_be;
    uint64_t r13_17_ph_be;
    uint64_t r14_6_ph_be;
    uint64_t local_sp_31_ph_be;
    uint64_t rcx_10_ph;
    uint64_t var_92;
    uint64_t var_93;
    struct indirect_placeholder_12_ret_type var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t local_sp_34;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_33;
    struct indirect_placeholder_14_ret_type var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t rcx_8;
    uint64_t r15_2;
    uint64_t var_40;
    uint64_t var_140;
    uint64_t rcx_12;
    uint64_t r15_4;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t r14_5;
    uint64_t r14_4;
    uint64_t local_sp_20;
    uint64_t var_24;
    uint64_t r13_12;
    uint64_t rbx_3;
    uint64_t local_sp_22;
    uint64_t var_25;
    uint64_t r13_13;
    uint64_t rbx_4;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t local_sp_21;
    uint64_t r13_15;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t r13_14;
    uint64_t local_sp_24;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t rbx_5;
    uint64_t local_sp_25;
    uint64_t r102_7;
    uint64_t rcx_7;
    uint64_t rbp_7;
    uint64_t r13_16;
    uint64_t rbx_6;
    uint64_t r102_2;
    uint64_t local_sp_26;
    uint64_t var_28;
    uint64_t var_29;
    struct indirect_placeholder_15_ret_type var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t r102_3;
    uint64_t local_sp_27;
    uint64_t rcx_9;
    uint64_t r102_4;
    uint64_t local_sp_28;
    uint64_t local_sp_29;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t local_sp_30;
    uint64_t rbp_8_ph;
    uint64_t r13_17_ph;
    uint64_t r14_6_ph;
    uint64_t local_sp_31_ph;
    uint64_t rbp_9;
    uint64_t rcx_10;
    uint64_t rbp_8;
    uint64_t r102_5;
    uint64_t local_sp_31;
    uint64_t var_49;
    uint64_t var_50;
    struct indirect_placeholder_16_ret_type var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_141;
    uint64_t r102_6;
    uint64_t var_142;
    uint64_t var_143;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rcx();
    var_2 = init_rbp();
    var_3 = init_r13();
    var_4 = init_rbx();
    var_5 = init_r15();
    var_6 = init_r12();
    var_7 = init_r14();
    var_8 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_5;
    *(uint64_t *)(var_0 + (-16L)) = var_7;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_6;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    *(uint64_t *)(var_0 + (-48L)) = var_4;
    var_9 = var_0 + (-152L);
    *(uint32_t *)(var_0 + (-100L)) = (uint32_t)rdi;
    var_10 = (uint64_t)((long)(rdi << 32UL) >> (long)28UL) + *(uint64_t *)4363768UL;
    *(uint64_t *)(var_0 + (-128L)) = var_10;
    var_11 = *(uint64_t *)var_10;
    local_sp_23 = var_9;
    rdx_0 = 0UL;
    rdx_2 = 0UL;
    r15_2 = var_11;
    local_sp_22 = var_9;
    r13_13 = var_11;
    rbx_4 = var_11;
    r13_14 = var_11;
    local_sp_24 = var_9;
    rcx_7 = var_1;
    rbp_7 = var_2;
    r102_2 = r10;
    if (*(unsigned char *)4365193UL == '\x00') {
        *(uint64_t *)(var_0 + (-112L)) = 0UL;
        var_26 = *(uint64_t *)(*(uint64_t *)(local_sp_24 + 24UL) + 8UL);
        var_27 = helper_cc_compute_c_wrapper(var_11 - var_26, var_26, var_8, 17U);
        r13_15 = r13_14;
        rbx_5 = var_26;
        local_sp_25 = local_sp_24;
        if (var_27 == 0UL) {
            return;
        }
    }
    var_12 = *(uint64_t *)(var_10 + 8UL);
    var_13 = helper_cc_compute_c_wrapper(var_11 - var_12, var_12, var_8, 17U);
    if (var_13 != 0UL) {
        *(uint64_t *)(local_sp_23 + 40UL) = (r13_13 - var_11);
        r13_14 = r13_13;
        local_sp_24 = local_sp_23;
        var_26 = *(uint64_t *)(*(uint64_t *)(local_sp_24 + 24UL) + 8UL);
        var_27 = helper_cc_compute_c_wrapper(var_11 - var_26, var_26, var_8, 17U);
        r13_15 = r13_14;
        rbx_5 = var_26;
        local_sp_25 = local_sp_24;
        if (var_27 == 0UL) {
            return;
        }
        *(uint64_t *)(local_sp_25 + 8UL) = var_11;
        r13_16 = r13_15;
        rbx_6 = rbx_5;
        local_sp_26 = local_sp_25;
        while (1U)
            {
                r15_3_ph = r15_2;
                var_42 = rbx_6;
                rcx_8 = rcx_7;
                r102_3 = r102_2;
                local_sp_27 = local_sp_26;
                rbp_8_ph = rbp_7;
                r13_17_ph = r13_16;
                if (*(uint64_t *)4364832UL != 0UL) {
                    *(uint64_t *)(local_sp_27 + 72UL) = rbx_6;
                    rcx_9 = rcx_8;
                    r102_4 = r102_3;
                    local_sp_28 = local_sp_27;
                    r102_5_ph = r102_4;
                    rcx_10_ph = rcx_9;
                    r14_5 = var_42;
                    r14_4 = var_42;
                    local_sp_29 = local_sp_28;
                    local_sp_30 = local_sp_28;
                    if (var_42 > r15_2) {
                        var_43 = r14_4 + (-1L);
                        var_44 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)var_43;
                        *(uint64_t *)(local_sp_29 + (-8L)) = 4207345UL;
                        var_45 = indirect_placeholder_4(var_44);
                        var_46 = (uint64_t)(unsigned char)var_45;
                        var_47 = local_sp_29 + (-16L);
                        *(uint64_t *)var_47 = 4207353UL;
                        var_48 = indirect_placeholder_4(var_46);
                        r14_4 = var_43;
                        local_sp_29 = var_47;
                        r14_5 = r14_4;
                        local_sp_30 = var_47;
                        while ((uint64_t)(uint32_t)var_48 != 0UL)
                            {
                                r14_5 = r15_2;
                                if (var_43 == r15_2) {
                                    break;
                                }
                                var_43 = r14_4 + (-1L);
                                var_44 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)var_43;
                                *(uint64_t *)(local_sp_29 + (-8L)) = 4207345UL;
                                var_45 = indirect_placeholder_4(var_44);
                                var_46 = (uint64_t)(unsigned char)var_45;
                                var_47 = local_sp_29 + (-16L);
                                *(uint64_t *)var_47 = 4207353UL;
                                var_48 = indirect_placeholder_4(var_46);
                                r14_4 = var_43;
                                local_sp_29 = var_47;
                                r14_5 = r14_4;
                                local_sp_30 = var_47;
                            }
                    }
                    *(uint64_t *)(local_sp_30 + 16UL) = r15_2;
                    r14_6_ph = r14_5;
                    local_sp_31_ph = local_sp_30;
                    while (1U)
                        {
                            r15_3 = r15_3_ph;
                            rbp_1 = r14_6_ph;
                            r15_1 = r14_6_ph;
                            r13_0 = r13_17_ph;
                            r13_4 = r13_17_ph;
                            r14_2 = r14_6_ph;
                            r13_5 = r13_17_ph;
                            r14_3 = r14_6_ph;
                            r13_16 = r13_17_ph;
                            rcx_10 = rcx_10_ph;
                            rbp_8 = rbp_8_ph;
                            r102_5 = r102_5_ph;
                            local_sp_31 = local_sp_31_ph;
                            while (1U)
                                {
                                    local_sp_33 = local_sp_31;
                                    r15_5 = r15_3;
                                    rcx_1 = rcx_10;
                                    rcx_0 = rcx_10;
                                    rbp_0 = rbp_8;
                                    local_sp_32 = local_sp_31;
                                    r102_0 = r102_5;
                                    rcx_11 = rcx_10;
                                    r102_1 = r102_5;
                                    local_sp_34 = local_sp_31;
                                    rcx_12 = rcx_10;
                                    r15_4 = r15_3;
                                    r102_7 = r102_5;
                                    rbp_7 = rbp_8;
                                    rbp_9 = rbp_8;
                                    r102_6 = r102_5;
                                    if (*(uint64_t *)4364480UL == 0UL) {
                                        var_49 = r14_6_ph - r15_3;
                                        var_50 = local_sp_31 + (-8L);
                                        *(uint64_t *)var_50 = 4208116UL;
                                        var_51 = indirect_placeholder_16(0UL, var_49, 4364488UL, r15_3, 4364160UL, var_49);
                                        var_52 = var_51.field_0;
                                        var_53 = var_51.field_1;
                                        var_54 = var_51.field_2;
                                        rcx_0 = var_53;
                                        r102_0 = var_54;
                                        local_sp_1 = var_50;
                                        local_sp_34 = var_50;
                                        rcx_12 = var_53;
                                        r102_7 = var_54;
                                        switch_state_var = 0;
                                        switch (var_52) {
                                          case 18446744073709551614UL:
                                            {
                                                *(uint64_t *)(local_sp_31 + (-16L)) = 4207554UL;
                                                indirect_placeholder();
                                                abort();
                                            }
                                            break;
                                          case 18446744073709551615UL:
                                            {
                                                loop_state_var = 1U;
                                                switch_state_var = 1;
                                                break;
                                            }
                                            break;
                                          default:
                                            {
                                                var_55 = r15_3 + **(uint64_t **)4364168UL;
                                                var_56 = r15_3 + **(uint64_t **)4364176UL;
                                                rbx_0 = var_55;
                                                r15_0 = var_56;
                                                local_sp_33 = local_sp_1;
                                                r15_5 = rbx_0;
                                                rcx_1 = rcx_0;
                                                rcx_11 = rcx_0;
                                                rbp_1 = rbp_0;
                                                rbx_1 = rbx_0;
                                                r15_1 = r15_0;
                                                r102_1 = r102_0;
                                                local_sp_2 = local_sp_1;
                                                rbp_9 = rbp_0;
                                                r102_6 = r102_0;
                                                if (rbx_0 != r15_0) {
                                                    loop_state_var = 0U;
                                                    switch_state_var = 1;
                                                    break;
                                                }
                                                r15_3 = r15_5 + 1UL;
                                                rcx_10 = rcx_11;
                                                rbp_8 = rbp_9;
                                                r102_5 = r102_6;
                                                local_sp_31 = local_sp_33;
                                                continue;
                                            }
                                            break;
                                        }
                                        if (switch_state_var)
                                            break;
                                    }
                                    var_57 = r15_3 - r14_6_ph;
                                    var_58 = helper_cc_compute_c_wrapper(var_57, r14_6_ph, var_8, 17U);
                                    if (var_58 != 0UL) {
                                        var_141 = helper_cc_compute_all_wrapper(var_57, r14_6_ph, var_8, 17U);
                                        if ((var_141 & 64UL) != 0UL) {
                                            loop_state_var = 1U;
                                            break;
                                        }
                                        r15_3 = r15_5 + 1UL;
                                        rcx_10 = rcx_11;
                                        rbp_8 = rbp_9;
                                        r102_5 = r102_6;
                                        local_sp_31 = local_sp_33;
                                        continue;
                                    }
                                    while (1U)
                                        {
                                            var_59 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)r15_4;
                                            var_60 = local_sp_32 + (-8L);
                                            *(uint64_t *)var_60 = 4207404UL;
                                            var_61 = indirect_placeholder_4(var_59);
                                            local_sp_33 = var_60;
                                            r15_5 = r15_4;
                                            local_sp_19 = var_60;
                                            rbp_6 = r15_4;
                                            local_sp_32 = var_60;
                                            rbx_0 = r15_4;
                                            rbx_1 = r15_4;
                                            local_sp_34 = var_60;
                                            if (*(unsigned char *)((uint64_t)(unsigned char)var_61 + 4363904UL) != '\x00') {
                                                loop_state_var = 1U;
                                                break;
                                            }
                                            var_140 = r15_4 + 1UL;
                                            r15_4 = var_140;
                                            if (var_140 == r14_6_ph) {
                                                continue;
                                            }
                                            loop_state_var = 0U;
                                            break;
                                        }
                                    switch_state_var = 0;
                                    switch (loop_state_var) {
                                      case 1U:
                                        {
                                            var_62 = r15_4 - r14_6_ph;
                                            if (var_62 != 0UL) {
                                                loop_state_var = 1U;
                                                switch_state_var = 1;
                                                break;
                                            }
                                            var_63 = helper_cc_compute_c_wrapper(var_62, r14_6_ph, var_8, 17U);
                                            if (var_63 != 0UL) {
                                                r15_3 = r15_5 + 1UL;
                                                rcx_10 = rcx_11;
                                                rbp_8 = rbp_9;
                                                r102_5 = r102_6;
                                                local_sp_31 = local_sp_33;
                                                continue;
                                            }
                                            while (1U)
                                                {
                                                    var_64 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)rbp_6;
                                                    var_65 = local_sp_19 + (-8L);
                                                    *(uint64_t *)var_65 = 4207581UL;
                                                    var_66 = indirect_placeholder_4(var_64);
                                                    local_sp_19 = var_65;
                                                    rbp_0 = rbp_6;
                                                    r15_0 = rbp_6;
                                                    local_sp_1 = var_65;
                                                    local_sp_2 = var_65;
                                                    if (*(unsigned char *)((uint64_t)(unsigned char)var_66 + 4363904UL) != '\x00') {
                                                        loop_state_var = 1U;
                                                        break;
                                                    }
                                                    var_67 = rbp_6 + 1UL;
                                                    rbp_6 = var_67;
                                                    if (var_67 == r14_6_ph) {
                                                        continue;
                                                    }
                                                    loop_state_var = 0U;
                                                    break;
                                                }
                                            switch_state_var = 0;
                                            switch (loop_state_var) {
                                              case 1U:
                                                {
                                                    local_sp_33 = local_sp_1;
                                                    r15_5 = rbx_0;
                                                    rcx_1 = rcx_0;
                                                    rcx_11 = rcx_0;
                                                    rbp_1 = rbp_0;
                                                    rbx_1 = rbx_0;
                                                    r15_1 = r15_0;
                                                    r102_1 = r102_0;
                                                    local_sp_2 = local_sp_1;
                                                    rbp_9 = rbp_0;
                                                    r102_6 = r102_0;
                                                    if (rbx_0 != r15_0) {
                                                        loop_state_var = 0U;
                                                        switch_state_var = 1;
                                                        break;
                                                    }
                                                    r15_3 = r15_5 + 1UL;
                                                    rcx_10 = rcx_11;
                                                    rbp_8 = rbp_9;
                                                    r102_5 = r102_6;
                                                    local_sp_31 = local_sp_33;
                                                    continue;
                                                }
                                                break;
                                              case 0U:
                                                {
                                                    loop_state_var = 0U;
                                                    switch_state_var = 1;
                                                    break;
                                                }
                                                break;
                                            }
                                            if (switch_state_var)
                                                break;
                                        }
                                        break;
                                      case 0U:
                                        {
                                            loop_state_var = 1U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                        break;
                                    }
                                    if (switch_state_var)
                                        break;
                                }
                            switch_state_var = 0;
                            switch (loop_state_var) {
                              case 1U:
                                {
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              case 0U:
                                {
                                    *(uint64_t *)(local_sp_2 + 80UL) = rbx_1;
                                    var_68 = r15_1 - rbx_1;
                                    *(uint64_t *)(local_sp_2 + 88UL) = var_68;
                                    local_sp_8 = local_sp_2;
                                    rcx_6 = rcx_1;
                                    rbp_4 = rbp_1;
                                    r102_5_ph = r102_1;
                                    rbp_5 = rbp_1;
                                    r15_3_ph = r15_1;
                                    rcx_5 = rcx_1;
                                    rbp_2 = rbp_1;
                                    local_sp_3 = local_sp_2;
                                    local_sp_7 = local_sp_2;
                                    rcx_10_ph_be = rcx_1;
                                    rbx_2 = rbx_1;
                                    rcx_4 = rcx_1;
                                    if ((long)var_68 > (long)*(uint64_t *)4363872UL) {
                                        *(uint64_t *)4363872UL = var_68;
                                    }
                                    var_69 = *(unsigned char *)4365193UL;
                                    if (var_69 != '\x00') {
                                        var_70 = helper_cc_compute_c_wrapper(r13_17_ph - rbx_1, rbx_1, var_8, 17U);
                                        if (var_70 != 0UL) {
                                            var_71 = *(uint64_t *)4363792UL;
                                            *(unsigned char *)(local_sp_2 + 32UL) = var_69;
                                            *(uint64_t *)(local_sp_2 + 56UL) = r14_6_ph;
                                            r12_1 = var_71;
                                            r14_0 = *(uint64_t *)(local_sp_2 + 8UL);
                                            rbp_4 = rbp_3;
                                            rbp_2 = rbp_3;
                                            rdx_0 = rdx_1;
                                            r13_0 = r13_3;
                                            r12_1 = r12_2;
                                            r14_0 = r14_1;
                                            local_sp_3 = local_sp_6;
                                            r13_4 = r13_3;
                                            local_sp_7 = local_sp_6;
                                            do {
                                                local_sp_4 = local_sp_3;
                                                rdx_1 = rdx_0;
                                                rbp_3 = rbp_2;
                                                r12_2 = r12_1;
                                                r14_1 = r14_0;
                                                local_sp_6 = local_sp_3;
                                                local_sp_5 = local_sp_3;
                                                if (*(unsigned char *)r13_0 == '\n') {
                                                    r13_3 = r13_0 + 1UL;
                                                } else {
                                                    var_72 = r12_1 + 1UL;
                                                    var_73 = r13_0 + 1UL;
                                                    var_74 = *(uint64_t *)(*(uint64_t *)(local_sp_3 + 24UL) + 8UL);
                                                    var_75 = helper_cc_compute_c_wrapper(var_73 - var_74, var_74, var_8, 17U);
                                                    r13_2 = var_73;
                                                    r13_1 = var_73;
                                                    rbp_3 = var_74;
                                                    r12_2 = var_72;
                                                    r14_1 = var_73;
                                                    if (var_75 != 0UL) {
                                                        var_76 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)r13_1;
                                                        *(uint64_t *)(local_sp_4 + (-8L)) = 4207696UL;
                                                        var_77 = indirect_placeholder_4(var_76);
                                                        var_78 = (uint64_t)(unsigned char)var_77;
                                                        var_79 = local_sp_4 + (-16L);
                                                        *(uint64_t *)var_79 = 4207704UL;
                                                        var_80 = indirect_placeholder_4(var_78);
                                                        local_sp_4 = var_79;
                                                        r13_2 = r13_1;
                                                        local_sp_5 = var_79;
                                                        while ((uint64_t)(uint32_t)var_80 != 0UL)
                                                            {
                                                                var_81 = r13_1 + 1UL;
                                                                r13_1 = var_81;
                                                                r13_2 = var_74;
                                                                if (var_81 == var_74) {
                                                                    break;
                                                                }
                                                                var_76 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)r13_1;
                                                                *(uint64_t *)(local_sp_4 + (-8L)) = 4207696UL;
                                                                var_77 = indirect_placeholder_4(var_76);
                                                                var_78 = (uint64_t)(unsigned char)var_77;
                                                                var_79 = local_sp_4 + (-16L);
                                                                *(uint64_t *)var_79 = 4207704UL;
                                                                var_80 = indirect_placeholder_4(var_78);
                                                                local_sp_4 = var_79;
                                                                r13_2 = r13_1;
                                                                local_sp_5 = var_79;
                                                            }
                                                    }
                                                    *(uint64_t *)(local_sp_5 + 40UL) = (r13_2 - var_73);
                                                    rdx_1 = (uint64_t)*(unsigned char *)(local_sp_5 + 32UL);
                                                    r13_3 = r13_2;
                                                    local_sp_6 = local_sp_5;
                                                }
                                                rbp_4 = rbp_3;
                                                rbp_2 = rbp_3;
                                                rdx_0 = rdx_1;
                                                r13_0 = r13_3;
                                                r12_1 = r12_2;
                                                r14_0 = r14_1;
                                                local_sp_3 = local_sp_6;
                                                r13_4 = r13_3;
                                                local_sp_7 = local_sp_6;
                                            } while (rbx_1 <= r13_3);
                                            *(uint64_t *)(local_sp_6 + 8UL) = r14_1;
                                            var_82 = *(uint64_t *)(local_sp_6 + 56UL);
                                            r14_2 = var_82;
                                            if ((uint64_t)(unsigned char)rdx_1 == 0UL) {
                                                *(uint64_t *)4363792UL = r12_2;
                                            }
                                        }
                                        var_83 = helper_cc_compute_c_wrapper(rbx_1 - r13_4, r13_4, var_8, 17U);
                                        local_sp_8 = local_sp_7;
                                        rbp_5 = rbp_4;
                                        r13_5 = r13_4;
                                        r14_3 = r14_2;
                                        rbp_8_ph_be = rbp_4;
                                        r13_17_ph_be = r13_4;
                                        r14_6_ph_be = r14_2;
                                        local_sp_31_ph_be = local_sp_7;
                                        if (var_83 != 0UL) {
                                            rcx_10_ph = rcx_10_ph_be;
                                            rbp_8_ph = rbp_8_ph_be;
                                            r13_17_ph = r13_17_ph_be;
                                            r14_6_ph = r14_6_ph_be;
                                            local_sp_31_ph = local_sp_31_ph_be;
                                            continue;
                                        }
                                    }
                                    local_sp_9 = local_sp_8;
                                    r13_6 = r13_5;
                                    r13_9 = r13_5;
                                    r13_10 = r13_5;
                                    r13_11 = r13_5;
                                    rbp_8_ph_be = rbp_5;
                                    r13_17_ph_be = r13_5;
                                    r14_6_ph_be = r14_3;
                                    if (*(uint64_t *)4365160UL != 0UL) {
                                        var_84 = local_sp_8 + 80UL;
                                        var_85 = local_sp_8 + (-8L);
                                        *(uint64_t *)var_85 = 4208240UL;
                                        var_86 = indirect_placeholder_2(var_84, 4363840UL);
                                        local_sp_9 = var_85;
                                        local_sp_31_ph_be = var_85;
                                        if ((uint64_t)(unsigned char)var_86 != 0UL) {
                                            rcx_10_ph = rcx_10_ph_be;
                                            rbp_8_ph = rbp_8_ph_be;
                                            r13_17_ph = r13_17_ph_be;
                                            r14_6_ph = r14_6_ph_be;
                                            local_sp_31_ph = local_sp_31_ph_be;
                                            continue;
                                        }
                                    }
                                    local_sp_10 = local_sp_9;
                                    if (*(uint64_t *)4365168UL != 0UL) {
                                        var_87 = local_sp_9 + 80UL;
                                        var_88 = local_sp_9 + (-8L);
                                        *(uint64_t *)var_88 = 4208273UL;
                                        var_89 = indirect_placeholder_2(var_87, 4363808UL);
                                        local_sp_10 = var_88;
                                        local_sp_31_ph_be = var_88;
                                        if ((uint64_t)(unsigned char)var_89 != 0UL) {
                                            rcx_10_ph = rcx_10_ph_be;
                                            rbp_8_ph = rbp_8_ph_be;
                                            r13_17_ph = r13_17_ph_be;
                                            r14_6_ph = r14_6_ph_be;
                                            local_sp_31_ph = local_sp_31_ph_be;
                                            continue;
                                        }
                                    }
                                    var_90 = *(uint64_t *)4363752UL;
                                    var_91 = *(uint64_t *)4363744UL;
                                    var_98 = var_91;
                                    local_sp_11 = local_sp_10;
                                    if (var_91 == var_90) {
                                        var_92 = *(uint64_t *)4363760UL;
                                        var_93 = local_sp_10 + (-8L);
                                        *(uint64_t *)var_93 = 4207811UL;
                                        var_94 = indirect_placeholder_12(rcx_1, rbp_5, 48UL, rbx_1, var_92, 4363752UL, r102_1);
                                        var_95 = var_94.field_0;
                                        var_96 = var_94.field_2;
                                        *(uint64_t *)4363760UL = var_95;
                                        var_97 = var_95;
                                        var_98 = *(uint64_t *)4363744UL;
                                        rbx_2 = var_96;
                                        local_sp_11 = var_93;
                                    } else {
                                        var_97 = *(uint64_t *)4363760UL;
                                    }
                                    var_99 = (var_98 * 48UL) + var_97;
                                    var_100 = *(unsigned char *)4365194UL;
                                    local_sp_12 = local_sp_11;
                                    r12_3 = var_98;
                                    local_sp_15 = local_sp_11;
                                    r12_4 = var_98;
                                    local_sp_16 = local_sp_11;
                                    r12_6 = var_98;
                                    local_sp_18 = local_sp_11;
                                    rbp_8_ph_be = var_99;
                                    if (var_100 == '\x00') {
                                        if (*(unsigned char *)4365193UL != '\x00') {
                                            var_139 = *(uint64_t *)(local_sp_18 + 88UL);
                                            *(uint64_t *)var_99 = *(uint64_t *)(local_sp_18 + 80UL);
                                            *(uint64_t *)(var_99 + 8UL) = var_139;
                                            *(uint64_t *)(var_99 + 16UL) = (*(uint64_t *)(local_sp_18 + 16UL) - rbx_2);
                                            *(uint64_t *)(var_99 + 24UL) = (r14_3 - rbx_2);
                                            *(uint32_t *)(var_99 + 40UL) = *(uint32_t *)(local_sp_18 + 52UL);
                                            *(uint64_t *)4363744UL = (r12_6 + 1UL);
                                            rcx_10_ph_be = rcx_6;
                                            r13_17_ph_be = r13_11;
                                            local_sp_31_ph_be = local_sp_18;
                                            rcx_10_ph = rcx_10_ph_be;
                                            rbp_8_ph = rbp_8_ph_be;
                                            r13_17_ph = r13_17_ph_be;
                                            r14_6_ph = r14_6_ph_be;
                                            local_sp_31_ph = local_sp_31_ph_be;
                                            continue;
                                        }
                                        *(uint64_t *)(var_99 + 32UL) = (*(uint64_t *)(local_sp_11 + 8UL) - rbx_2);
                                        var_116 = *(uint64_t *)(local_sp_11 + 40UL);
                                        if ((long)*(uint64_t *)4363864UL < (long)var_116) {
                                            *(uint64_t *)4363864UL = var_116;
                                        }
                                    } else {
                                        var_101 = helper_cc_compute_c_wrapper(r13_5 - rbx_2, rbx_2, var_8, 17U);
                                        if (var_101 != 0UL) {
                                            var_102 = *(uint64_t *)4363792UL;
                                            *(uint64_t *)(local_sp_11 + 32UL) = var_102;
                                            *(unsigned char *)(local_sp_11 + 56UL) = var_100;
                                            *(uint64_t *)(local_sp_11 + 64UL) = var_98;
                                            rcx_2 = var_102;
                                            rcx_4 = rcx_3;
                                            rcx_2 = rcx_3;
                                            rdx_2 = rdx_3;
                                            r13_6 = r13_8;
                                            local_sp_12 = local_sp_14;
                                            r13_9 = r13_8;
                                            local_sp_15 = local_sp_14;
                                            do {
                                                local_sp_13 = local_sp_12;
                                                rcx_3 = rcx_2;
                                                rdx_3 = rdx_2;
                                                local_sp_14 = local_sp_12;
                                                if (*(unsigned char *)r13_6 == '\n') {
                                                    r13_8 = r13_6 + 1UL;
                                                } else {
                                                    var_103 = (uint64_t *)(local_sp_12 + 32UL);
                                                    *var_103 = (*var_103 + 1UL);
                                                    var_104 = r13_6 + 1UL;
                                                    *(uint64_t *)(local_sp_12 + 8UL) = var_104;
                                                    var_105 = *(uint64_t *)(local_sp_12 + 24UL);
                                                    var_106 = *(uint64_t *)(var_105 + 8UL);
                                                    var_107 = (uint64_t)*(unsigned char *)(local_sp_12 + 56UL);
                                                    var_108 = helper_cc_compute_c_wrapper(var_104 - var_106, var_106, var_8, 17U);
                                                    r13_7 = var_104;
                                                    rcx_3 = var_105;
                                                    rdx_3 = var_107;
                                                    r13_8 = var_104;
                                                    if (var_108 != 0UL) {
                                                        while (1U)
                                                            {
                                                                var_109 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)r13_7;
                                                                *(uint64_t *)(local_sp_13 + (-8L)) = 4207907UL;
                                                                var_110 = indirect_placeholder_4(var_109);
                                                                var_111 = (uint64_t)(unsigned char)var_110;
                                                                var_112 = local_sp_13 + (-16L);
                                                                *(uint64_t *)var_112 = 4207915UL;
                                                                var_113 = indirect_placeholder_4(var_111);
                                                                local_sp_13 = var_112;
                                                                r13_8 = var_106;
                                                                local_sp_14 = var_112;
                                                                if ((uint64_t)(uint32_t)var_113 != 0UL) {
                                                                    loop_state_var = 1U;
                                                                    break;
                                                                }
                                                                var_114 = r13_7 + 1UL;
                                                                r13_7 = var_114;
                                                                if (var_114 == var_106) {
                                                                    continue;
                                                                }
                                                                loop_state_var = 0U;
                                                                break;
                                                            }
                                                        switch (loop_state_var) {
                                                          case 0U:
                                                            {
                                                                rdx_3 = (uint64_t)*(unsigned char *)(local_sp_13 + 40UL);
                                                            }
                                                            break;
                                                          case 1U:
                                                            {
                                                                rdx_3 = (uint64_t)*(unsigned char *)(local_sp_13 + 40UL);
                                                                r13_8 = r13_7;
                                                            }
                                                            break;
                                                        }
                                                    }
                                                }
                                                rcx_4 = rcx_3;
                                                rcx_2 = rcx_3;
                                                rdx_2 = rdx_3;
                                                r13_6 = r13_8;
                                                local_sp_12 = local_sp_14;
                                                r13_9 = r13_8;
                                                local_sp_15 = local_sp_14;
                                            } while (rbx_2 <= r13_8);
                                            var_115 = *(uint64_t *)(local_sp_14 + 64UL);
                                            r12_3 = var_115;
                                            if ((uint64_t)(unsigned char)rdx_3 == 0UL) {
                                                *(uint64_t *)4363792UL = *(uint64_t *)(local_sp_14 + 32UL);
                                            }
                                        }
                                        *(uint64_t *)(var_99 + 32UL) = *(uint64_t *)4363792UL;
                                        rcx_5 = rcx_4;
                                        r13_10 = r13_9;
                                        r12_4 = r12_3;
                                        local_sp_16 = local_sp_15;
                                    }
                                    rcx_6 = rcx_5;
                                    local_sp_17 = local_sp_16;
                                    r13_11 = r13_10;
                                    r12_6 = r12_4;
                                    local_sp_18 = local_sp_16;
                                    var_117 = (uint64_t *)(local_sp_16 + 16UL);
                                    var_118 = *var_117;
                                    rcx_6 = var_118;
                                    var_119 = helper_cc_compute_c_wrapper(var_118 - r14_3, r14_3, var_8, 17U);
                                    if (*(unsigned char *)4365193UL != '\x00' & *(uint64_t *)(local_sp_16 + 8UL) != var_118 & var_119 != 0UL) {
                                        *(uint64_t *)(local_sp_16 + 32UL) = r12_4;
                                        r12_5 = *var_117;
                                        while (1U)
                                            {
                                                var_120 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)r12_5;
                                                *(uint64_t *)(local_sp_17 + (-8L)) = 4208422UL;
                                                var_121 = indirect_placeholder_4(var_120);
                                                var_122 = (uint64_t)(unsigned char)var_121;
                                                var_123 = local_sp_17 + (-16L);
                                                *(uint64_t *)var_123 = 4208430UL;
                                                var_124 = indirect_placeholder_4(var_122);
                                                local_sp_0 = var_123;
                                                local_sp_17 = var_123;
                                                local_sp_18 = var_123;
                                                if ((uint64_t)(uint32_t)var_124 != 0UL) {
                                                    loop_state_var = 1U;
                                                    break;
                                                }
                                                var_137 = r12_5 + 1UL;
                                                r12_5 = var_137;
                                                if (var_137 == r14_3) {
                                                    continue;
                                                }
                                                loop_state_var = 0U;
                                                break;
                                            }
                                        switch (loop_state_var) {
                                          case 0U:
                                            {
                                                var_138 = *(uint64_t *)(local_sp_17 + 16UL);
                                                *(uint64_t *)local_sp_17 = r14_3;
                                                r12_6 = var_138;
                                            }
                                            break;
                                          case 1U:
                                            {
                                                var_125 = (uint64_t *)local_sp_17;
                                                *var_125 = r12_5;
                                                var_126 = (uint64_t *)(local_sp_17 + 16UL);
                                                var_127 = *var_126;
                                                var_128 = helper_cc_compute_c_wrapper(r12_5 - r14_3, r14_3, var_8, 17U);
                                                r12_6 = var_127;
                                                if (var_128 != 0UL) {
                                                    *var_126 = var_127;
                                                    r12_0 = *var_125;
                                                    while (1U)
                                                        {
                                                            var_129 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)r12_0;
                                                            *(uint64_t *)(local_sp_0 + (-8L)) = 4208500UL;
                                                            var_130 = indirect_placeholder_4(var_129);
                                                            var_131 = (uint64_t)(unsigned char)var_130;
                                                            var_132 = local_sp_0 + (-16L);
                                                            *(uint64_t *)var_132 = 4208508UL;
                                                            var_133 = indirect_placeholder_4(var_131);
                                                            local_sp_0 = var_132;
                                                            local_sp_18 = var_132;
                                                            if ((uint64_t)(uint32_t)var_133 != 0UL) {
                                                                loop_state_var = 1U;
                                                                break;
                                                            }
                                                            var_134 = r12_0 + 1UL;
                                                            r12_0 = var_134;
                                                            if (var_134 == r14_3) {
                                                                continue;
                                                            }
                                                            loop_state_var = 0U;
                                                            break;
                                                        }
                                                    switch (loop_state_var) {
                                                      case 0U:
                                                        {
                                                            var_135 = *(uint64_t *)(local_sp_0 + 16UL);
                                                            *(uint64_t *)local_sp_0 = r14_3;
                                                            r12_6 = var_135;
                                                        }
                                                        break;
                                                      case 1U:
                                                        {
                                                            *(uint64_t *)local_sp_0 = r12_0;
                                                            var_136 = *(uint64_t *)(local_sp_0 + 16UL);
                                                            r12_6 = var_136;
                                                        }
                                                        break;
                                                    }
                                                }
                                            }
                                            break;
                                        }
                                    }
                                    var_139 = *(uint64_t *)(local_sp_18 + 88UL);
                                    *(uint64_t *)var_99 = *(uint64_t *)(local_sp_18 + 80UL);
                                    *(uint64_t *)(var_99 + 8UL) = var_139;
                                    *(uint64_t *)(var_99 + 16UL) = (*(uint64_t *)(local_sp_18 + 16UL) - rbx_2);
                                    *(uint64_t *)(var_99 + 24UL) = (r14_3 - rbx_2);
                                    *(uint32_t *)(var_99 + 40UL) = *(uint32_t *)(local_sp_18 + 52UL);
                                    *(uint64_t *)4363744UL = (r12_6 + 1UL);
                                    rcx_10_ph_be = rcx_6;
                                    r13_17_ph_be = r13_11;
                                    local_sp_31_ph_be = local_sp_18;
                                    rcx_10_ph = rcx_10_ph_be;
                                    rbp_8_ph = rbp_8_ph_be;
                                    r13_17_ph = r13_17_ph_be;
                                    r14_6_ph = r14_6_ph_be;
                                    local_sp_31_ph = local_sp_31_ph_be;
                                    continue;
                                }
                                break;
                            }
                            if (switch_state_var)
                                break;
                        }
                    var_142 = *(uint64_t *)(*(uint64_t *)(local_sp_34 + 24UL) + 8UL);
                    var_143 = *(uint64_t *)(local_sp_34 + 72UL);
                    r15_2 = var_143;
                    rcx_7 = rcx_12;
                    rbx_6 = var_142;
                    r102_2 = r102_7;
                    local_sp_26 = local_sp_34;
                    if (var_142 <= var_143) {
                        continue;
                    }
                    break;
                }
                *(uint64_t *)(local_sp_26 + (-16L)) = 4207277UL;
                indirect_placeholder();
                abort();
            }
        return;
    }
    while (1U)
        {
            var_14 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)rbx_4;
            *(uint64_t *)(local_sp_22 + (-8L)) = 4207127UL;
            var_15 = indirect_placeholder_4(var_14);
            var_16 = (uint64_t)(unsigned char)var_15;
            var_17 = local_sp_22 + (-16L);
            *(uint64_t *)var_17 = 4207135UL;
            var_18 = indirect_placeholder_4(var_16);
            local_sp_23 = var_17;
            local_sp_20 = var_17;
            r13_12 = rbx_4;
            rbx_3 = rbx_4;
            local_sp_22 = var_17;
            r13_13 = var_12;
            local_sp_21 = var_17;
            if ((uint64_t)(uint32_t)var_18 == 0UL) {
                var_25 = rbx_4 + 1UL;
                rbx_4 = var_25;
                if (var_25 == var_12) {
                    continue;
                }
                loop_state_var = 0U;
                break;
            }
            *(uint64_t *)(local_sp_22 + 24UL) = (rbx_4 - var_11);
            if (var_12 <= rbx_4) {
                loop_state_var = 2U;
                break;
            }
            loop_state_var = 1U;
            break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(uint64_t *)(local_sp_23 + 40UL) = (r13_13 - var_11);
            r13_14 = r13_13;
            local_sp_24 = local_sp_23;
            var_26 = *(uint64_t *)(*(uint64_t *)(local_sp_24 + 24UL) + 8UL);
            var_27 = helper_cc_compute_c_wrapper(var_11 - var_26, var_26, var_8, 17U);
            r13_15 = r13_14;
            rbx_5 = var_26;
            local_sp_25 = local_sp_24;
            if (var_27 == 0UL) {
                return;
            }
        }
        break;
      case 2U:
      case 1U:
        {
            switch (loop_state_var) {
              case 2U:
                {
                    var_19 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)rbx_3;
                    *(uint64_t *)(local_sp_20 + (-8L)) = 4207190UL;
                    var_20 = indirect_placeholder_4(var_19);
                    var_21 = (uint64_t)(unsigned char)var_20;
                    var_22 = local_sp_20 + (-16L);
                    *(uint64_t *)var_22 = 4207198UL;
                    var_23 = indirect_placeholder_4(var_21);
                    local_sp_20 = var_22;
                    r13_12 = rbx_3;
                    local_sp_21 = var_22;
                    while ((uint64_t)(uint32_t)var_23 != 0UL)
                        {
                            var_24 = rbx_3 + 1UL;
                            rbx_3 = var_24;
                            r13_12 = var_12;
                            if (var_24 == var_12) {
                                break;
                            }
                            var_19 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)rbx_3;
                            *(uint64_t *)(local_sp_20 + (-8L)) = 4207190UL;
                            var_20 = indirect_placeholder_4(var_19);
                            var_21 = (uint64_t)(unsigned char)var_20;
                            var_22 = local_sp_20 + (-16L);
                            *(uint64_t *)var_22 = 4207198UL;
                            var_23 = indirect_placeholder_4(var_21);
                            local_sp_20 = var_22;
                            r13_12 = rbx_3;
                            local_sp_21 = var_22;
                        }
                }
                break;
              case 1U:
                {
                    r13_15 = r13_12;
                    rbx_5 = *(uint64_t *)(*(uint64_t *)(local_sp_21 + 24UL) + 8UL);
                    local_sp_25 = local_sp_21;
                }
                break;
            }
        }
        break;
    }
}
