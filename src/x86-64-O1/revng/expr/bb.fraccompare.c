typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
uint64_t bb_fraccompare(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t _pre_phi37;
    uint64_t var_0;
    unsigned char var_1;
    unsigned char var_2;
    bool var_3;
    bool var_4;
    uint64_t var_5;
    unsigned char _pre35;
    uint64_t _pre36;
    uint64_t var_6;
    unsigned char _pre;
    uint64_t _pre39;
    uint64_t _pre_phi40;
    uint64_t rdi2_0;
    uint64_t rsi3_0;
    uint64_t var_7;
    unsigned char var_8;
    uint64_t var_9;
    uint64_t var_10;
    unsigned char var_11;
    uint64_t var_12;
    bool var_13;
    bool var_14;
    bool var_15;
    unsigned char var_16;
    uint64_t rdi2_1;
    uint64_t rdi2_2;
    uint64_t rax_0;
    uint64_t var_17;
    unsigned char var_18;
    unsigned char var_19;
    uint64_t rsi3_1;
    uint64_t rsi3_2;
    uint64_t rax_1;
    uint64_t var_20;
    unsigned char var_21;
    uint64_t var_22;
    unsigned int loop_state_var;
    revng_init_local_sp(0UL);
    var_0 = init_cc_src2();
    var_1 = *(unsigned char *)rdi;
    var_2 = (unsigned char)rdx;
    var_3 = ((uint64_t)(var_1 - var_2) == 0UL);
    var_4 = ((uint64_t)(*(unsigned char *)rsi - var_2) == 0UL);
    rdi2_0 = rdi;
    rsi3_0 = rsi;
    if (var_3) {
        if (var_4) {
            return 0UL;
        }
        var_5 = rsi + 1UL;
        _pre35 = *(unsigned char *)var_5;
        _pre36 = (uint64_t)_pre35;
        _pre_phi37 = _pre36;
        var_19 = _pre35;
        rsi3_1 = var_5;
        rsi3_2 = rsi3_1;
        rax_1 = _pre_phi37;
        if ((uint64_t)(var_19 + '\xd0') != 0UL) {
            var_20 = rsi3_2 + 1UL;
            var_21 = *(unsigned char *)var_20;
            rsi3_2 = var_20;
            do {
                var_20 = rsi3_2 + 1UL;
                var_21 = *(unsigned char *)var_20;
                rsi3_2 = var_20;
            } while (var_21 != '0');
            rax_1 = (uint64_t)var_21;
        }
        var_22 = helper_cc_compute_c_wrapper((uint64_t)((uint32_t)((int)((uint32_t)rax_1 << 24U) >> (int)24U) + (-48)) + (-10L), 10UL, var_0, 16U);
        return (uint64_t)(0U - (uint32_t)var_22);
    }
    if (!var_4) {
        var_6 = rdi + 1UL;
        _pre = *(unsigned char *)var_6;
        _pre39 = (uint64_t)_pre;
        _pre_phi40 = _pre39;
        var_16 = _pre;
        rdi2_1 = var_6;
        rdi2_2 = rdi2_1;
        rax_0 = _pre_phi40;
        if ((uint64_t)(var_16 + '\xd0') != 0UL) {
            var_17 = rdi2_2 + 1UL;
            var_18 = *(unsigned char *)var_17;
            rdi2_2 = var_17;
            do {
                var_17 = rdi2_2 + 1UL;
                var_18 = *(unsigned char *)var_17;
                rdi2_2 = var_17;
            } while (var_18 != '0');
            rax_0 = (uint64_t)var_18;
        }
        return (((uint32_t)((int)((uint32_t)rax_0 << 24U) >> (int)24U) + (-48)) < 10U);
    }
    while (1U)
        {
            var_7 = rdi2_0 + 1UL;
            var_8 = *(unsigned char *)var_7;
            var_9 = (uint64_t)var_8;
            var_10 = rsi3_0 + 1UL;
            var_11 = *(unsigned char *)var_10;
            var_12 = (uint64_t)var_11;
            var_13 = ((uint64_t)(var_8 - var_11) == 0UL);
            var_14 = (((uint32_t)var_8 + (-48)) > 9U);
            _pre_phi37 = var_12;
            _pre_phi40 = var_9;
            rdi2_0 = var_7;
            rsi3_0 = var_10;
            var_16 = var_8;
            rdi2_1 = var_7;
            var_19 = var_11;
            rsi3_1 = var_10;
            if (var_13) {
                if (var_14) {
                    continue;
                }
                loop_state_var = 0U;
                break;
            }
            var_15 = (((uint32_t)var_11 + (-48)) > 9U);
            if (var_14) {
                if (!var_15) {
                    loop_state_var = 2U;
                    break;
                }
                loop_state_var = 0U;
                break;
            }
            if (!var_15) {
                loop_state_var = 1U;
                break;
            }
            return (uint64_t)((uint32_t)(uint64_t)var_8 - (uint32_t)(uint64_t)var_11);
        }
    switch (loop_state_var) {
      case 0U:
        {
            return 0UL;
        }
        break;
      case 1U:
        {
            rdi2_2 = rdi2_1;
            rax_0 = _pre_phi40;
            if ((uint64_t)(var_16 + '\xd0') != 0UL) {
                var_17 = rdi2_2 + 1UL;
                var_18 = *(unsigned char *)var_17;
                rdi2_2 = var_17;
                do {
                    var_17 = rdi2_2 + 1UL;
                    var_18 = *(unsigned char *)var_17;
                    rdi2_2 = var_17;
                } while (var_18 != '0');
                rax_0 = (uint64_t)var_18;
            }
            return (((uint32_t)((int)((uint32_t)rax_0 << 24U) >> (int)24U) + (-48)) < 10U);
        }
        break;
      case 2U:
        {
            rsi3_2 = rsi3_1;
            rax_1 = _pre_phi37;
            if ((uint64_t)(var_19 + '\xd0') != 0UL) {
                var_20 = rsi3_2 + 1UL;
                var_21 = *(unsigned char *)var_20;
                rsi3_2 = var_20;
                do {
                    var_20 = rsi3_2 + 1UL;
                    var_21 = *(unsigned char *)var_20;
                    rsi3_2 = var_20;
                } while (var_21 != '0');
                rax_1 = (uint64_t)var_21;
            }
            var_22 = helper_cc_compute_c_wrapper((uint64_t)((uint32_t)((int)((uint32_t)rax_1 << 24U) >> (int)24U) + (-48)) + (-10L), 10UL, var_0, 16U);
            return (uint64_t)(0U - (uint32_t)var_22);
        }
        break;
    }
}
