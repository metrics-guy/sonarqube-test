typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2);
uint64_t bb_numcompare(uint64_t rcx, uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t r10) {
    uint64_t var_0;
    uint64_t var_1;
    unsigned char var_2;
    uint64_t var_3;
    unsigned char var_4;
    uint64_t var_5;
    uint64_t rdi3_7;
    uint64_t rsi4_3;
    unsigned char var_58;
    uint64_t var_71;
    uint64_t rdi3_0;
    uint64_t r105_4;
    uint64_t var_38;
    unsigned char var_39;
    uint32_t var_40;
    uint32_t var_41;
    uint32_t var_42;
    uint64_t rdi3_1;
    uint32_t _pre_phi200;
    uint64_t var_43;
    unsigned char var_44;
    uint64_t r9_2;
    uint64_t r9_0;
    uint64_t r8_0;
    unsigned char var_45;
    uint32_t var_46;
    uint64_t r8_1;
    unsigned char _pre_phi195;
    uint64_t var_47;
    unsigned char var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t r8_2;
    unsigned char var_6;
    uint32_t var_7;
    uint32_t var_8;
    uint64_t r8_6;
    uint64_t rdi3_2;
    uint64_t rsi4_0;
    unsigned char var_9;
    uint64_t rdi3_5;
    uint64_t rsi4_1;
    uint64_t rdi3_3_ph;
    uint64_t r8_3_ph;
    uint64_t r8_3;
    uint64_t var_10;
    unsigned char var_11;
    uint64_t var_12;
    uint32_t var_13;
    uint64_t rdi3_4;
    uint64_t var_14;
    unsigned char var_15;
    uint64_t var_16;
    uint64_t r9_3;
    uint64_t r8_4;
    uint32_t var_17;
    uint32_t var_18;
    uint32_t _pre_phi206;
    uint32_t var_19;
    uint64_t var_20;
    uint64_t r105_1;
    uint64_t r105_0;
    uint32_t var_21;
    uint64_t var_22;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t r9_4_ph;
    uint64_t r8_5_ph;
    uint64_t r8_5;
    uint64_t var_23;
    uint32_t var_24;
    uint64_t var_25;
    uint64_t r9_5;
    uint64_t rdi3_6_ph;
    uint64_t rsi4_2_ph;
    uint64_t rdi3_6;
    uint64_t var_26;
    uint32_t var_27;
    uint64_t var_28;
    uint64_t var_31;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_32;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_51;
    unsigned char var_52;
    uint32_t var_53;
    uint32_t var_54;
    uint32_t var_55;
    uint64_t r8_7;
    uint32_t _pre_phi191;
    uint64_t var_56;
    unsigned char var_57;
    uint32_t var_59;
    uint64_t rdi3_8;
    unsigned char _pre_phi193;
    uint64_t var_60;
    unsigned char var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t rdi3_9;
    uint64_t var_65;
    unsigned char var_66;
    uint64_t var_67;
    uint32_t _pre_phi;
    uint64_t rdi3_12;
    uint64_t rdi3_10_ph;
    uint64_t r8_8_ph;
    uint64_t r8_8;
    uint64_t var_68;
    unsigned char var_69;
    uint32_t var_70;
    uint64_t rdi3_11;
    unsigned char var_72;
    uint64_t var_73;
    uint64_t rsi4_5;
    uint64_t r8_9;
    uint32_t var_74;
    uint32_t _pre_phi197;
    uint32_t var_75;
    uint64_t var_76;
    uint64_t r105_3;
    uint64_t r105_2;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t r9_8_ph;
    uint64_t r8_10_ph;
    uint64_t r8_10;
    uint64_t var_79;
    uint32_t var_80;
    uint64_t var_81;
    uint64_t r9_9;
    uint64_t rdi3_13_ph;
    uint64_t rsi4_6_ph;
    uint64_t rdi3_13;
    uint64_t var_82;
    uint32_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_cc_src2();
    var_2 = *(unsigned char *)rdi;
    var_3 = (uint64_t)var_2;
    var_4 = *(unsigned char *)rsi;
    var_5 = (uint64_t)var_4;
    rdi3_7 = rsi;
    rsi4_3 = var_5;
    rdi3_0 = rsi;
    r105_4 = 1UL;
    r9_2 = var_3;
    r9_0 = var_3;
    r8_0 = rdi;
    r8_2 = rdi;
    r8_6 = rdi;
    rdi3_2 = rsi;
    rsi4_0 = var_5;
    r9_4_ph = 0UL;
    r9_5 = 0UL;
    rsi4_2_ph = 0UL;
    rdi3_9 = rsi;
    r9_8_ph = 0UL;
    r9_9 = 0UL;
    rsi4_6_ph = 0UL;
    if ((uint64_t)(var_2 + '\xd3') == 0UL) {
        if ((uint64_t)(var_4 + '\xd3') == 0UL) {
            while (1U)
                {
                    var_38 = rdi3_0 + 1UL;
                    var_39 = *(unsigned char *)var_38;
                    rdi3_0 = var_38;
                    rdi3_1 = var_38;
                    if ((uint64_t)(var_39 + '\xd0') != 0UL) {
                        continue;
                    }
                    var_40 = (uint32_t)(uint64_t)var_39;
                    var_41 = (uint32_t)rcx;
                    _pre_phi200 = var_40;
                    if ((uint64_t)(var_40 - var_41) != 0UL) {
                        continue;
                    }
                    break;
                }
            var_42 = (uint32_t)rdx;
            if ((uint64_t)(var_40 - var_42) != 0UL) {
                var_43 = rdi3_1 + 1UL;
                var_44 = *(unsigned char *)var_43;
                rdi3_1 = var_43;
                do {
                    var_43 = rdi3_1 + 1UL;
                    var_44 = *(unsigned char *)var_43;
                    rdi3_1 = var_43;
                } while (var_44 != '0');
                _pre_phi200 = (uint32_t)(uint64_t)var_44;
            }
            if ((uint64_t)((_pre_phi200 + (-48)) & (-2)) <= 9UL) {
                while (1U)
                    {
                        var_45 = (unsigned char)r9_0;
                        r8_1 = r8_0;
                        _pre_phi195 = var_45;
                        if ((uint64_t)(var_45 + '\xd0') != 0UL) {
                            var_46 = (uint32_t)r9_0;
                            if ((uint64_t)(var_46 - var_41) == 0UL) {
                                break;
                            }
                        }
                        var_50 = r8_0 + 1UL;
                        r9_0 = (uint64_t)*(unsigned char *)var_50;
                        r8_0 = var_50;
                        continue;
                    }
                if ((uint64_t)(var_46 - var_42) != 0UL) {
                    var_47 = r8_1 + 1UL;
                    var_48 = *(unsigned char *)var_47;
                    r8_1 = var_47;
                    do {
                        var_47 = r8_1 + 1UL;
                        var_48 = *(unsigned char *)var_47;
                        r8_1 = var_47;
                    } while (var_48 != '0');
                    _pre_phi195 = (unsigned char)(uint64_t)var_48;
                }
                var_49 = ((uint64_t)(((uint32_t)(uint64_t)_pre_phi195 + (-48)) & (-2)) < 10UL);
                r105_4 = var_49;
            }
        } else {
            while (1U)
                {
                    var_6 = (unsigned char)r9_2;
                    r8_3_ph = r8_2;
                    r9_3 = r9_2;
                    r8_4 = r8_2;
                    if ((uint64_t)(var_6 + '\xd0') != 0UL) {
                        var_7 = (uint32_t)r9_2;
                        var_8 = (uint32_t)rcx;
                        if ((uint64_t)(var_7 - var_8) == 0UL) {
                            break;
                        }
                    }
                    var_37 = r8_2 + 1UL;
                    r9_2 = (uint64_t)*(unsigned char *)var_37;
                    r8_2 = var_37;
                    continue;
                }
            while (1U)
                {
                    var_9 = (unsigned char)rsi4_0;
                    rdi3_3_ph = rdi3_2;
                    rdi3_5 = rdi3_2;
                    rsi4_1 = rsi4_0;
                    if ((uint64_t)(var_9 + '\xd0') != 0UL) {
                        if ((uint64_t)((uint32_t)rsi4_0 - var_8) == 0UL) {
                            break;
                        }
                    }
                    var_36 = rdi3_2 + 1UL;
                    rdi3_2 = var_36;
                    rsi4_0 = (uint64_t)*(unsigned char *)var_36;
                    continue;
                }
            if ((uint64_t)(var_6 - var_9) != 0UL & (uint64_t)((var_7 + (-48)) & (-2)) > 9UL) {
                while (1U)
                    {
                        r8_3 = r8_3_ph;
                        rdi3_4 = rdi3_3_ph;
                        var_10 = r8_3 + 1UL;
                        var_11 = *(unsigned char *)var_10;
                        var_12 = (uint64_t)var_11;
                        var_13 = (uint32_t)var_12;
                        r8_3_ph = var_10;
                        r8_3 = var_10;
                        r9_3 = var_12;
                        r8_4 = var_10;
                        do {
                            var_10 = r8_3 + 1UL;
                            var_11 = *(unsigned char *)var_10;
                            var_12 = (uint64_t)var_11;
                            var_13 = (uint32_t)var_12;
                            r8_3_ph = var_10;
                            r8_3 = var_10;
                            r9_3 = var_12;
                            r8_4 = var_10;
                        } while ((uint64_t)(var_13 - var_8) != 0UL);
                        var_14 = rdi3_4 + 1UL;
                        var_15 = *(unsigned char *)var_14;
                        var_16 = (uint64_t)var_15;
                        rdi3_3_ph = var_14;
                        rdi3_4 = var_14;
                        rdi3_5 = var_14;
                        rsi4_1 = var_16;
                        do {
                            var_14 = rdi3_4 + 1UL;
                            var_15 = *(unsigned char *)var_14;
                            var_16 = (uint64_t)var_15;
                            rdi3_3_ph = var_14;
                            rdi3_4 = var_14;
                            rdi3_5 = var_14;
                            rsi4_1 = var_16;
                        } while ((uint64_t)((uint32_t)var_16 - var_8) != 0UL);
                        if ((uint64_t)(var_11 - var_15) == 0UL) {
                            break;
                        }
                        if ((uint64_t)((var_13 + (-48)) & (-2)) <= 9UL) {
                            continue;
                        }
                        break;
                    }
            }
            var_17 = (uint32_t)(uint64_t)(unsigned char)r9_3;
            var_18 = (uint32_t)rdx;
            r8_5_ph = r8_4;
            rdi3_6_ph = rdi3_5;
            if ((uint64_t)(var_17 - var_18) == 0UL) {
                var_19 = (uint32_t)(uint64_t)(unsigned char)rsi4_1;
                _pre_phi206 = var_19;
                if ((uint64_t)((var_19 + (-48)) & (-2)) <= 9UL) {
                    var_33 = (uint64_t)(uint32_t)((int)(var_18 << 24U) >> (int)24U);
                    *(uint64_t *)(var_0 + (-8L)) = 4218649UL;
                    var_34 = indirect_placeholder_8(var_33, r8_4, rdi3_5);
                    var_35 = (uint64_t)(uint32_t)var_34;
                    r105_4 = var_35;
                    return (uint64_t)(uint32_t)r105_4;
                }
            }
            _pre_phi206 = (uint32_t)(uint64_t)(unsigned char)rsi4_1;
            if ((uint64_t)(_pre_phi206 - var_18) == 0UL) {
                var_21 = var_17 + (-48);
                var_22 = (uint64_t)(var_17 - _pre_phi206);
                r105_0 = var_22;
                if ((uint64_t)(var_21 & (-2)) > 9UL) {
                    var_33 = (uint64_t)(uint32_t)((int)(var_18 << 24U) >> (int)24U);
                    *(uint64_t *)(var_0 + (-8L)) = 4218649UL;
                    var_34 = indirect_placeholder_8(var_33, r8_4, rdi3_5);
                    var_35 = (uint64_t)(uint32_t)var_34;
                    r105_4 = var_35;
                    return (uint64_t)(uint32_t)r105_4;
                }
                r105_1 = r105_0;
                var_25 = r9_4_ph + 1UL;
                r9_4_ph = var_25;
                r9_5 = var_25;
                do {
                    r8_5 = r8_5_ph;
                    var_23 = r8_5 + 1UL;
                    var_24 = (uint32_t)(uint64_t)*(unsigned char *)var_23;
                    r8_5_ph = var_23;
                    r8_5 = var_23;
                    do {
                        var_23 = r8_5 + 1UL;
                        var_24 = (uint32_t)(uint64_t)*(unsigned char *)var_23;
                        r8_5_ph = var_23;
                        r8_5 = var_23;
                    } while ((uint64_t)(var_24 - var_8) != 0UL);
                    var_25 = r9_4_ph + 1UL;
                    r9_4_ph = var_25;
                    r9_5 = var_25;
                } while ((uint64_t)((var_24 + (-48)) & (-2)) <= 9UL);
            } else {
                var_20 = (uint64_t)(var_17 - _pre_phi206);
                r105_0 = var_20;
                r105_1 = var_20;
                if ((uint64_t)((var_17 + (-48)) & (-2)) <= 9UL) {
                    r105_1 = r105_0;
                    var_25 = r9_4_ph + 1UL;
                    r9_4_ph = var_25;
                    r9_5 = var_25;
                    do {
                        r8_5 = r8_5_ph;
                        var_23 = r8_5 + 1UL;
                        var_24 = (uint32_t)(uint64_t)*(unsigned char *)var_23;
                        r8_5_ph = var_23;
                        r8_5 = var_23;
                        do {
                            var_23 = r8_5 + 1UL;
                            var_24 = (uint32_t)(uint64_t)*(unsigned char *)var_23;
                            r8_5_ph = var_23;
                            r8_5 = var_23;
                        } while ((uint64_t)(var_24 - var_8) != 0UL);
                        var_25 = r9_4_ph + 1UL;
                        r9_4_ph = var_25;
                        r9_5 = var_25;
                    } while ((uint64_t)((var_24 + (-48)) & (-2)) <= 9UL);
                }
            }
            if ((uint64_t)((_pre_phi206 + (-48)) & (-2)) > 9UL) {
                var_32 = (r9_5 != 0UL);
                r105_4 = var_32;
            } else {
                var_28 = rsi4_2_ph + 1UL;
                rsi4_2_ph = var_28;
                do {
                    rdi3_6 = rdi3_6_ph;
                    var_26 = rdi3_6 + 1UL;
                    var_27 = (uint32_t)(uint64_t)*(unsigned char *)var_26;
                    rdi3_6_ph = var_26;
                    rdi3_6 = var_26;
                    do {
                        var_26 = rdi3_6 + 1UL;
                        var_27 = (uint32_t)(uint64_t)*(unsigned char *)var_26;
                        rdi3_6_ph = var_26;
                        rdi3_6 = var_26;
                    } while ((uint64_t)(var_27 - var_8) != 0UL);
                    var_28 = rsi4_2_ph + 1UL;
                    rsi4_2_ph = var_28;
                } while ((uint64_t)((var_27 + (-48)) & (-2)) <= 9UL);
                if (r9_5 == var_28) {
                    var_31 = (r9_5 == 0UL) ? 0UL : (uint64_t)(uint32_t)r105_1;
                    r105_4 = var_31;
                } else {
                    var_29 = helper_cc_compute_c_wrapper(r9_5 - var_28, var_28, var_1, 17U);
                    var_30 = (uint64_t)((0U - (uint32_t)var_29) & (-2)) | 1UL;
                    r105_4 = var_30;
                }
            }
        }
    } else {
        while (1U)
            {
                var_51 = r8_6 + 1UL;
                var_52 = *(unsigned char *)var_51;
                r105_4 = 4294967295UL;
                r8_6 = var_51;
                r8_7 = var_51;
                r8_8_ph = var_51;
                r8_9 = var_51;
                if ((uint64_t)(var_52 + '\xd0') != 0UL) {
                    continue;
                }
                var_53 = (uint32_t)(uint64_t)var_52;
                var_54 = (uint32_t)rcx;
                _pre_phi191 = var_53;
                _pre_phi = var_53;
                if ((uint64_t)(var_53 - var_54) != 0UL) {
                    continue;
                }
                break;
            }
        if ((uint64_t)(var_4 + '\xd3') == 0UL) {
            var_55 = (uint32_t)rdx;
            if ((uint64_t)(var_53 - var_55) != 0UL) {
                var_56 = r8_7 + 1UL;
                var_57 = *(unsigned char *)var_56;
                r8_7 = var_56;
                do {
                    var_56 = r8_7 + 1UL;
                    var_57 = *(unsigned char *)var_56;
                    r8_7 = var_56;
                } while (var_57 != '0');
                _pre_phi191 = (uint32_t)(uint64_t)var_57;
            }
            if ((uint64_t)((_pre_phi191 + (-48)) & (-2)) <= 9UL) {
                while (1U)
                    {
                        var_58 = (unsigned char)rsi4_3;
                        rdi3_8 = rdi3_7;
                        _pre_phi193 = var_58;
                        if ((uint64_t)(var_58 + '\xd0') != 0UL) {
                            var_59 = (uint32_t)rsi4_3;
                            if ((uint64_t)(var_59 - var_54) == 0UL) {
                                break;
                            }
                        }
                        var_64 = rdi3_7 + 1UL;
                        rdi3_7 = var_64;
                        rsi4_3 = (uint64_t)*(unsigned char *)var_64;
                        continue;
                    }
                if ((uint64_t)(var_59 - var_55) != 0UL) {
                    var_60 = rdi3_8 + 1UL;
                    var_61 = *(unsigned char *)var_60;
                    rdi3_8 = var_60;
                    do {
                        var_60 = rdi3_8 + 1UL;
                        var_61 = *(unsigned char *)var_60;
                        rdi3_8 = var_60;
                    } while (var_61 != '0');
                    _pre_phi193 = (unsigned char)(uint64_t)var_61;
                }
                var_62 = helper_cc_compute_c_wrapper((uint64_t)((uint32_t)(uint64_t)_pre_phi193 + (-48)) + (-10L), 10UL, var_1, 16U);
                var_63 = (uint64_t)(0U - (uint32_t)var_62);
                r105_4 = var_63;
            }
        } else {
            while (1U)
                {
                    var_65 = rdi3_9 + 1UL;
                    var_66 = *(unsigned char *)var_65;
                    var_67 = (uint64_t)var_66;
                    rdi3_9 = var_65;
                    rdi3_10_ph = var_65;
                    rdi3_12 = var_65;
                    rsi4_5 = var_67;
                    if ((uint64_t)(var_66 + '\xd0') != 0UL) {
                        continue;
                    }
                    if ((uint64_t)((uint32_t)var_67 - var_54) != 0UL) {
                        continue;
                    }
                    break;
                }
            if ((uint64_t)(var_52 - var_66) != 0UL & (uint64_t)((var_53 + (-48)) & (-2)) > 9UL) {
                while (1U)
                    {
                        r8_8 = r8_8_ph;
                        rdi3_11 = rdi3_10_ph;
                        var_68 = r8_8 + 1UL;
                        var_69 = *(unsigned char *)var_68;
                        var_70 = (uint32_t)(uint64_t)var_69;
                        r8_8_ph = var_68;
                        r8_8 = var_68;
                        _pre_phi = var_70;
                        r8_9 = var_68;
                        do {
                            var_68 = r8_8 + 1UL;
                            var_69 = *(unsigned char *)var_68;
                            var_70 = (uint32_t)(uint64_t)var_69;
                            r8_8_ph = var_68;
                            r8_8 = var_68;
                            _pre_phi = var_70;
                            r8_9 = var_68;
                        } while ((uint64_t)(var_70 - var_54) != 0UL);
                        var_71 = rdi3_11 + 1UL;
                        var_72 = *(unsigned char *)var_71;
                        var_73 = (uint64_t)var_72;
                        rdi3_10_ph = var_71;
                        rdi3_11 = var_71;
                        rdi3_12 = var_71;
                        rsi4_5 = var_73;
                        do {
                            var_71 = rdi3_11 + 1UL;
                            var_72 = *(unsigned char *)var_71;
                            var_73 = (uint64_t)var_72;
                            rdi3_10_ph = var_71;
                            rdi3_11 = var_71;
                            rdi3_12 = var_71;
                            rsi4_5 = var_73;
                        } while ((uint64_t)((uint32_t)var_73 - var_54) != 0UL);
                        if ((uint64_t)(var_69 - var_72) == 0UL) {
                            break;
                        }
                        if ((uint64_t)((var_70 + (-48)) & (-2)) <= 9UL) {
                            continue;
                        }
                        break;
                    }
            }
            var_74 = (uint32_t)rdx;
            r8_10_ph = r8_9;
            rdi3_13_ph = rdi3_12;
            if ((uint64_t)(_pre_phi - var_74) == 0UL) {
                var_75 = (uint32_t)rsi4_5;
                _pre_phi197 = var_75;
                if ((uint64_t)((var_75 + (-48)) & (-2)) <= 9UL) {
                    var_89 = (uint64_t)(uint32_t)((int)(var_74 << 24U) >> (int)24U);
                    *(uint64_t *)(var_0 + (-8L)) = 4218221UL;
                    var_90 = indirect_placeholder_8(var_89, rdi3_12, r8_9);
                    var_91 = (uint64_t)(uint32_t)var_90;
                    r105_4 = var_91;
                    return (uint64_t)(uint32_t)r105_4;
                }
            }
            _pre_phi197 = (uint32_t)rsi4_5;
            if ((uint64_t)(_pre_phi197 - var_74) == 0UL) {
                var_77 = (uint64_t)((_pre_phi + (-48)) & (-2));
                var_78 = (uint64_t)(_pre_phi197 - _pre_phi);
                r105_2 = var_78;
                if (var_77 > 9UL) {
                    var_89 = (uint64_t)(uint32_t)((int)(var_74 << 24U) >> (int)24U);
                    *(uint64_t *)(var_0 + (-8L)) = 4218221UL;
                    var_90 = indirect_placeholder_8(var_89, rdi3_12, r8_9);
                    var_91 = (uint64_t)(uint32_t)var_90;
                    r105_4 = var_91;
                    return (uint64_t)(uint32_t)r105_4;
                }
                r105_3 = r105_2;
                var_81 = r9_8_ph + 1UL;
                r9_8_ph = var_81;
                r9_9 = var_81;
                do {
                    r8_10 = r8_10_ph;
                    var_79 = r8_10 + 1UL;
                    var_80 = (uint32_t)(uint64_t)*(unsigned char *)var_79;
                    r8_10_ph = var_79;
                    r8_10 = var_79;
                    do {
                        var_79 = r8_10 + 1UL;
                        var_80 = (uint32_t)(uint64_t)*(unsigned char *)var_79;
                        r8_10_ph = var_79;
                        r8_10 = var_79;
                    } while ((uint64_t)(var_80 - var_54) != 0UL);
                    var_81 = r9_8_ph + 1UL;
                    r9_8_ph = var_81;
                    r9_9 = var_81;
                } while ((uint64_t)((var_80 + (-48)) & (-2)) <= 9UL);
            } else {
                var_76 = (uint64_t)(_pre_phi197 - _pre_phi);
                r105_2 = var_76;
                r105_3 = var_76;
                if ((uint64_t)((_pre_phi + (-48)) & (-2)) <= 9UL) {
                    r105_3 = r105_2;
                    var_81 = r9_8_ph + 1UL;
                    r9_8_ph = var_81;
                    r9_9 = var_81;
                    do {
                        r8_10 = r8_10_ph;
                        var_79 = r8_10 + 1UL;
                        var_80 = (uint32_t)(uint64_t)*(unsigned char *)var_79;
                        r8_10_ph = var_79;
                        r8_10 = var_79;
                        do {
                            var_79 = r8_10 + 1UL;
                            var_80 = (uint32_t)(uint64_t)*(unsigned char *)var_79;
                            r8_10_ph = var_79;
                            r8_10 = var_79;
                        } while ((uint64_t)(var_80 - var_54) != 0UL);
                        var_81 = r9_8_ph + 1UL;
                        r9_8_ph = var_81;
                        r9_9 = var_81;
                    } while ((uint64_t)((var_80 + (-48)) & (-2)) <= 9UL);
                }
            }
            if ((uint64_t)((_pre_phi197 + (-48)) & (-2)) > 9UL) {
                var_88 = (r9_9 == 0UL) ? 0UL : 4294967295UL;
                r105_4 = var_88;
            } else {
                var_84 = rsi4_6_ph + 1UL;
                rsi4_6_ph = var_84;
                do {
                    rdi3_13 = rdi3_13_ph;
                    var_82 = rdi3_13 + 1UL;
                    var_83 = (uint32_t)(uint64_t)*(unsigned char *)var_82;
                    rdi3_13_ph = var_82;
                    rdi3_13 = var_82;
                    do {
                        var_82 = rdi3_13 + 1UL;
                        var_83 = (uint32_t)(uint64_t)*(unsigned char *)var_82;
                        rdi3_13_ph = var_82;
                        rdi3_13 = var_82;
                    } while ((uint64_t)(var_83 - var_54) != 0UL);
                    var_84 = rsi4_6_ph + 1UL;
                    rsi4_6_ph = var_84;
                } while ((uint64_t)((var_83 + (-48)) & (-2)) <= 9UL);
                if (r9_9 == var_84) {
                    var_87 = (r9_9 == 0UL) ? 0UL : (uint64_t)(uint32_t)r105_3;
                    r105_4 = var_87;
                } else {
                    var_85 = helper_cc_compute_c_wrapper(r9_9 - var_84, var_84, var_1, 17U);
                    var_86 = (uint64_t)((uint32_t)((0UL - var_85) & 2UL) + (-1));
                    r105_4 = var_86;
                }
            }
        }
    }
}
