typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_15_ret_type;
struct indirect_placeholder_15_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_9(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern void indirect_placeholder_6(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_15_ret_type indirect_placeholder_15(uint64_t param_0);
uint64_t bb_operand2sig(uint64_t rdi, uint64_t rsi) {
    uint64_t local_sp_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint32_t var_8;
    uint64_t var_9;
    bool var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t var_35;
    uint64_t local_sp_6;
    uint64_t storemerge;
    bool var_19;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t local_sp_3;
    uint64_t local_sp_1;
    uint64_t var_30;
    uint64_t local_sp_5;
    uint64_t rbp_0;
    uint64_t var_21;
    unsigned char var_22;
    uint64_t local_sp_2;
    unsigned char rbx_0_in;
    uint64_t var_20;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t local_sp_4;
    uint64_t var_16;
    uint32_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    struct indirect_placeholder_15_ret_type var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_13;
    uint64_t *var_14;
    uint64_t var_15;
    uint64_t var_17;
    unsigned char *_cast2;
    unsigned char var_18;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_rbp();
    var_4 = init_r12();
    var_5 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_5;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_3;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    var_6 = (uint64_t)*(unsigned char *)rdi;
    var_7 = var_6 + 4294967248UL;
    var_8 = (uint32_t)var_7;
    var_9 = (uint64_t)var_8;
    var_10 = (var_9 > 9UL);
    var_11 = var_0 + (-64L);
    var_12 = (uint64_t *)var_11;
    storemerge = 4294967295UL;
    local_sp_2 = var_11;
    local_sp_3 = var_11;
    if (var_10) {
        *var_12 = 4206366UL;
        var_17 = indirect_placeholder_1(rdi);
        _cast2 = (unsigned char *)var_17;
        var_18 = *_cast2;
        rbx_0_in = var_18;
        rbp_0 = var_17;
        if (var_18 != '\x00') {
            var_19 = (var_17 == 0UL);
            var_21 = rbp_0 + 1UL;
            var_22 = *(unsigned char *)var_21;
            rbx_0_in = var_22;
            rbp_0 = var_21;
            do {
                var_20 = local_sp_2 + (-8L);
                *(uint64_t *)var_20 = 4206406UL;
                indirect_placeholder();
                local_sp_2 = var_20;
                local_sp_3 = var_20;
                if (var_19) {
                    *(unsigned char *)rbp_0 = (rbx_0_in + '\xe0');
                }
                var_21 = rbp_0 + 1UL;
                var_22 = *(unsigned char *)var_21;
                rbx_0_in = var_22;
                rbp_0 = var_21;
            } while (var_22 != '\x00');
        }
        var_23 = local_sp_3 + 12UL;
        var_24 = local_sp_3 + (-8L);
        *(uint64_t *)var_24 = 4206432UL;
        var_25 = indirect_placeholder_9(var_17, var_23);
        local_sp_0 = var_24;
        local_sp_1 = var_24;
        if ((uint64_t)(uint32_t)var_25 != 0UL) {
            if (*_cast2 == 'S') {
                *(uint32_t *)(local_sp_0 + 12UL) = 4294967295U;
                local_sp_1 = local_sp_0;
            } else {
                if (*(unsigned char *)(var_17 + 1UL) == 'I') {
                    *(uint32_t *)(local_sp_0 + 12UL) = 4294967295U;
                    local_sp_1 = local_sp_0;
                } else {
                    if (*(unsigned char *)(var_17 + 2UL) == 'G') {
                        *(uint32_t *)(local_sp_0 + 12UL) = 4294967295U;
                        local_sp_1 = local_sp_0;
                    } else {
                        var_26 = var_17 + 3UL;
                        var_27 = local_sp_3 + 4UL;
                        var_28 = local_sp_3 + (-16L);
                        *(uint64_t *)var_28 = 4206523UL;
                        var_29 = indirect_placeholder_9(var_26, var_27);
                        local_sp_0 = var_28;
                        local_sp_1 = var_28;
                        if ((uint64_t)(uint32_t)var_29 == 0UL) {
                            *(uint32_t *)(local_sp_0 + 12UL) = 4294967295U;
                            local_sp_1 = local_sp_0;
                        }
                    }
                }
            }
        }
        var_30 = local_sp_1 + (-8L);
        *(uint64_t *)var_30 = 4206458UL;
        indirect_placeholder();
        local_sp_5 = var_30;
    } else {
        *var_12 = 4206249UL;
        indirect_placeholder();
        *(uint32_t *)var_9 = 0U;
        var_13 = var_0 + (-72L);
        var_14 = (uint64_t *)var_13;
        *var_14 = 4206271UL;
        indirect_placeholder();
        var_15 = *var_14;
        local_sp_4 = var_13;
        if (var_15 == rdi) {
            *(uint32_t *)(local_sp_4 + 12UL) = 4294967295U;
            local_sp_5 = local_sp_4;
        } else {
            if (*(unsigned char *)var_15 == '\x00') {
                *(uint32_t *)(local_sp_4 + 12UL) = 4294967295U;
                local_sp_5 = local_sp_4;
            } else {
                var_16 = var_0 + (-80L);
                *(uint64_t *)var_16 = 4206306UL;
                indirect_placeholder();
                local_sp_4 = var_16;
                local_sp_5 = var_16;
                if (*(uint32_t *)var_15 == 0U) {
                    *(uint32_t *)(local_sp_4 + 12UL) = 4294967295U;
                    local_sp_5 = local_sp_4;
                } else {
                    if ((uint64_t)((long)(var_7 << 32UL) >> (long)32UL) == var_9) {
                        *(uint32_t *)(local_sp_4 + 12UL) = 4294967295U;
                        local_sp_5 = local_sp_4;
                    } else {
                        if ((uint64_t)((uint32_t)var_6 + (-47)) == 0UL) {
                            *(uint32_t *)(var_0 + (-68L)) = 4294967295U;
                        } else {
                            *(uint32_t *)(var_0 + (-68L)) = (var_8 & (uint32_t)(((int)var_8 > (int)254U) ? 255UL : 127UL));
                        }
                    }
                }
            }
        }
    }
    var_31 = *(uint32_t *)(local_sp_5 + 12UL);
    local_sp_6 = local_sp_5;
    if ((int)var_31 >= (int)0U) {
        var_32 = (uint64_t)var_31;
        var_33 = local_sp_5 + (-8L);
        *(uint64_t *)var_33 = 4206474UL;
        var_34 = indirect_placeholder_9(var_32, rsi);
        local_sp_6 = var_33;
        if ((uint64_t)(uint32_t)var_34 != 0UL) {
            var_35 = (uint64_t)*(uint32_t *)(local_sp_5 + 4UL);
            storemerge = var_35;
            return storemerge;
        }
    }
    *(uint64_t *)(local_sp_6 + (-8L)) = 4206537UL;
    var_36 = indirect_placeholder_15(rdi);
    var_37 = var_36.field_0;
    var_38 = var_36.field_1;
    var_39 = var_36.field_2;
    *(uint64_t *)(local_sp_6 + (-16L)) = 4206565UL;
    indirect_placeholder_6(0UL, 4258220UL, 0UL, 0UL, var_37, var_38, var_39);
    return storemerge;
}
