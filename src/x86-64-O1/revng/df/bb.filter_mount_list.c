typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_75_ret_type;
struct indirect_placeholder_76_ret_type;
struct indirect_placeholder_75_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_76_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint32_t init_state_0x82fc(void);
extern void indirect_placeholder_12(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_11(uint64_t param_0);
extern struct indirect_placeholder_75_ret_type indirect_placeholder_75(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_76_ret_type indirect_placeholder_76(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_filter_mount_list(uint64_t rdi) {
    uint64_t var_40;
    uint64_t rdi1_3;
    uint64_t rcx_6;
    uint64_t rcx_7;
    unsigned char var_41;
    uint64_t var_42;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t rax_0;
    struct indirect_placeholder_76_ret_type var_15;
    uint64_t var_0;
    uint32_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    unsigned char *var_9;
    unsigned char var_10;
    uint64_t var_11;
    uint64_t rdi1_5;
    uint64_t var_58;
    uint64_t r14_0;
    uint64_t var_48;
    uint64_t local_sp_3;
    uint64_t local_sp_0;
    uint64_t var_49;
    uint64_t rdi1_0;
    uint64_t rbp_2;
    uint64_t rcx_0;
    uint64_t rcx_1;
    unsigned char var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t rdi1_1;
    uint64_t rcx_2;
    uint64_t rcx_3;
    unsigned char var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_43;
    uint64_t local_sp_2;
    uint64_t var_59;
    uint64_t rbx_0;
    uint64_t var_60;
    uint64_t var_57;
    uint64_t *var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t rdi1_2;
    uint64_t rcx_4;
    uint64_t rcx_5;
    unsigned char var_38;
    uint64_t var_39;
    uint64_t var_46;
    uint64_t local_sp_7;
    uint64_t var_47;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t local_sp_5;
    uint64_t var_65;
    uint64_t rbp_0;
    uint64_t local_sp_4;
    uint64_t rbx_2;
    uint64_t local_sp_8;
    uint64_t rbx_1;
    uint64_t rbp_1;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t *var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t local_sp_6;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_17;
    uint64_t var_18;
    unsigned char *var_19;
    uint64_t var_20;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    struct indirect_placeholder_75_ret_type var_64;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t rax_1;
    uint64_t rdi1_4;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_16;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_state_0x82fc();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r15();
    var_5 = init_rbp();
    var_6 = init_r12();
    var_7 = init_r14();
    var_8 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_4;
    *(uint64_t *)(var_0 + (-16L)) = var_7;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_6;
    *(uint64_t *)(var_0 + (-40L)) = var_5;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_9 = (unsigned char *)(var_0 + (-221L));
    var_10 = (unsigned char)rdi;
    *var_9 = var_10;
    var_11 = *(uint64_t *)4323040UL;
    rcx_6 = 18446744073709551615UL;
    rcx_7 = 0UL;
    rdi1_5 = 0UL;
    rbp_2 = 0UL;
    rcx_0 = 18446744073709551615UL;
    rcx_1 = 0UL;
    rcx_2 = 18446744073709551615UL;
    rcx_3 = 0UL;
    rcx_4 = 18446744073709551615UL;
    rcx_5 = 0UL;
    rbx_1 = 0UL;
    rax_1 = var_11;
    rdi1_4 = 0UL;
    if (var_11 != 0UL) {
        var_12 = rdi1_4 + 1UL;
        var_13 = *(uint64_t *)(rax_1 + 48UL);
        rax_1 = var_13;
        while (var_13 != 0UL)
            {
                rdi1_4 = (uint64_t)(uint32_t)var_12;
                var_12 = rdi1_4 + 1UL;
                var_13 = *(uint64_t *)(rax_1 + 48UL);
                rax_1 = var_13;
            }
        rdi1_5 = (uint64_t)((long)(var_12 << 32UL) >> (long)32UL);
    }
    var_14 = var_0 + (-240L);
    *(uint64_t *)var_14 = 4207077UL;
    var_15 = indirect_placeholder_76(4204549UL, rdi1_5, 0UL, 4204564UL, 4206026UL);
    var_16 = var_15.field_0;
    *(uint64_t *)4323088UL = var_16;
    local_sp_5 = var_14;
    local_sp_8 = var_14;
    if (var_16 == 0UL) {
        var_71 = var_15.field_2;
        var_72 = var_15.field_1;
        *(uint64_t *)(var_0 + (-248L)) = 4207124UL;
        indirect_placeholder_12(var_72, var_71);
        abort();
    }
    var_17 = *(uint64_t *)4323040UL;
    rbx_2 = var_17;
    if (var_17 == 0UL) {
        if ((uint64_t)var_10 == 0UL) {
            return;
        }
    }
    var_18 = (uint64_t)var_1;
    while (1U)
        {
            var_19 = (unsigned char *)(rbx_2 + 40UL);
            var_20 = (uint64_t)*var_19;
            r14_0 = rbx_2;
            rbp_0 = rbp_2;
            local_sp_6 = local_sp_5;
            if ((var_20 & 2UL) != 0UL) {
                if (*(unsigned char *)4323085UL != '\x00') {
                    *(uint64_t *)(local_sp_6 + 32UL) = *(uint64_t *)(rbx_2 + 32UL);
                    local_sp_7 = local_sp_6;
                    *(uint64_t *)(local_sp_7 + (-8L)) = 4207171UL;
                    var_61 = indirect_placeholder_2(24UL);
                    *(uint64_t *)(var_61 + 8UL) = rbx_2;
                    *(uint64_t *)var_61 = *(uint64_t *)(local_sp_7 + 24UL);
                    *(uint64_t *)(var_61 + 16UL) = rbp_2;
                    var_62 = *(uint64_t *)4323088UL;
                    var_63 = local_sp_7 + (-16L);
                    *(uint64_t *)var_63 = 4207206UL;
                    var_64 = indirect_placeholder_75(var_62, var_61);
                    rbp_0 = var_61;
                    local_sp_4 = var_63;
                    if (var_64.field_0 == 0UL) {
                        var_68 = var_64.field_1;
                        var_69 = var_64.field_2;
                        *(uint64_t *)(local_sp_7 + (-24L)) = 4207665UL;
                        indirect_placeholder_12(var_68, var_69);
                        abort();
                    }
                    var_65 = *(uint64_t *)(rbx_2 + 48UL);
                    rbx_0 = var_65;
                    rbp_2 = rbp_0;
                    local_sp_5 = local_sp_4;
                    rbx_2 = rbx_0;
                    local_sp_8 = local_sp_4;
                    rbp_1 = rbp_0;
                    if (rbx_0 == 0UL) {
                        continue;
                    }
                    if ((uint64_t)var_10 != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    *(uint64_t *)4323040UL = 0UL;
                    if (rbp_0 != 0UL) {
                        loop_state_var = 2U;
                        break;
                    }
                    loop_state_var = 1U;
                    break;
                }
            }
            if ((var_20 & 1UL) != 0UL) {
                if (*(unsigned char *)4323086UL != '\x00' & *(unsigned char *)4323084UL != '\x00') {
                    *(uint64_t *)(local_sp_6 + 32UL) = *(uint64_t *)(rbx_2 + 32UL);
                    local_sp_7 = local_sp_6;
                    *(uint64_t *)(local_sp_7 + (-8L)) = 4207171UL;
                    var_61 = indirect_placeholder_2(24UL);
                    *(uint64_t *)(var_61 + 8UL) = rbx_2;
                    *(uint64_t *)var_61 = *(uint64_t *)(local_sp_7 + 24UL);
                    *(uint64_t *)(var_61 + 16UL) = rbp_2;
                    var_62 = *(uint64_t *)4323088UL;
                    var_63 = local_sp_7 + (-16L);
                    *(uint64_t *)var_63 = 4207206UL;
                    var_64 = indirect_placeholder_75(var_62, var_61);
                    rbp_0 = var_61;
                    local_sp_4 = var_63;
                    if (var_64.field_0 == 0UL) {
                        var_68 = var_64.field_1;
                        var_69 = var_64.field_2;
                        *(uint64_t *)(local_sp_7 + (-24L)) = 4207665UL;
                        indirect_placeholder_12(var_68, var_69);
                        abort();
                    }
                    var_65 = *(uint64_t *)(rbx_2 + 48UL);
                    rbx_0 = var_65;
                    rbp_2 = rbp_0;
                    local_sp_5 = local_sp_4;
                    rbx_2 = rbx_0;
                    local_sp_8 = local_sp_4;
                    rbp_1 = rbp_0;
                    if (rbx_0 == 0UL) {
                        continue;
                    }
                    if ((uint64_t)var_10 != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    *(uint64_t *)4323040UL = 0UL;
                    if (rbp_0 != 0UL) {
                        loop_state_var = 2U;
                        break;
                    }
                    loop_state_var = 1U;
                    break;
                }
            }
            var_21 = *(uint64_t *)(rbx_2 + 24UL);
            var_22 = local_sp_5 + (-8L);
            *(uint64_t *)var_22 = 4207136UL;
            var_23 = indirect_placeholder_2(var_21);
            local_sp_6 = var_22;
            if ((uint64_t)(unsigned char)var_23 != 0UL) {
                *(uint64_t *)(local_sp_6 + 32UL) = *(uint64_t *)(rbx_2 + 32UL);
                local_sp_7 = local_sp_6;
                *(uint64_t *)(local_sp_7 + (-8L)) = 4207171UL;
                var_61 = indirect_placeholder_2(24UL);
                *(uint64_t *)(var_61 + 8UL) = rbx_2;
                *(uint64_t *)var_61 = *(uint64_t *)(local_sp_7 + 24UL);
                *(uint64_t *)(var_61 + 16UL) = rbp_2;
                var_62 = *(uint64_t *)4323088UL;
                var_63 = local_sp_7 + (-16L);
                *(uint64_t *)var_63 = 4207206UL;
                var_64 = indirect_placeholder_75(var_62, var_61);
                rbp_0 = var_61;
                local_sp_4 = var_63;
                if (var_64.field_0 == 0UL) {
                    var_68 = var_64.field_1;
                    var_69 = var_64.field_2;
                    *(uint64_t *)(local_sp_7 + (-24L)) = 4207665UL;
                    indirect_placeholder_12(var_68, var_69);
                    abort();
                }
                var_65 = *(uint64_t *)(rbx_2 + 48UL);
                rbx_0 = var_65;
                rbp_2 = rbp_0;
                local_sp_5 = local_sp_4;
                rbx_2 = rbx_0;
                local_sp_8 = local_sp_4;
                rbp_1 = rbp_0;
                if (rbx_0 == 0UL) {
                    continue;
                }
                if ((uint64_t)var_10 != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                *(uint64_t *)4323040UL = 0UL;
                if (rbp_0 != 0UL) {
                    loop_state_var = 2U;
                    break;
                }
                loop_state_var = 1U;
                break;
            }
            var_24 = local_sp_5 + (-16L);
            *(uint64_t *)var_24 = 4207148UL;
            var_25 = indirect_placeholder_2(var_21);
            local_sp_6 = var_24;
            if ((uint64_t)(unsigned char)var_25 == 0UL) {
                var_26 = (uint64_t *)(rbx_2 + 8UL);
                var_27 = *var_26;
                var_28 = local_sp_5 + 16UL;
                var_29 = local_sp_5 + (-24L);
                *(uint64_t *)var_29 = 4207293UL;
                var_30 = indirect_placeholder_1(var_27, var_28);
                local_sp_6 = var_29;
                if ((uint64_t)((uint32_t)var_30 + 1U) != 0UL) {
                    var_31 = *(uint64_t *)(local_sp_5 + 8UL);
                    var_32 = local_sp_5 + (-32L);
                    *(uint64_t *)var_32 = 4207312UL;
                    var_33 = indirect_placeholder_2(var_31);
                    rax_0 = var_33;
                    local_sp_3 = var_32;
                    local_sp_7 = var_32;
                    if (var_33 != 0UL) {
                        var_34 = (uint64_t *)(var_33 + 8UL);
                        var_35 = *var_34;
                        var_36 = *(uint64_t *)(var_35 + 16UL);
                        rdi1_2 = var_36;
                        if (var_36 == 0UL) {
                            *(uint32_t *)(local_sp_5 + (-20L)) = 0U;
                        } else {
                            var_37 = *(uint64_t *)(rbx_2 + 16UL);
                            rdi1_3 = var_37;
                            if (var_37 == 0UL) {
                                *(uint32_t *)(local_sp_5 + (-20L)) = 0U;
                            } else {
                                while (rcx_4 != 0UL)
                                    {
                                        var_38 = *(unsigned char *)rdi1_2;
                                        var_39 = rcx_4 + (-1L);
                                        rcx_4 = var_39;
                                        rcx_5 = var_39;
                                        if (var_38 == '\x00') {
                                            break;
                                        }
                                        rdi1_2 = rdi1_2 + var_18;
                                    }
                                var_40 = 18446744073709551614UL - (-2L);
                                while (rcx_6 != 0UL)
                                    {
                                        var_41 = *(unsigned char *)rdi1_3;
                                        var_42 = rcx_6 + (-1L);
                                        rcx_6 = var_42;
                                        rcx_7 = var_42;
                                        if (var_41 == '\x00') {
                                            break;
                                        }
                                        rdi1_3 = rdi1_3 + var_18;
                                    }
                                var_43 = 18446744073709551614UL - (-2L);
                                var_44 = helper_cc_compute_c_wrapper(var_40 - var_43, var_43, var_8, 17U);
                                var_45 = (uint64_t)(unsigned char)var_44;
                                *(uint32_t *)(local_sp_5 + (-20L)) = (uint32_t)var_45;
                                rax_0 = var_45;
                            }
                        }
                        if (*(unsigned char *)4323032UL != '\x00') {
                            var_46 = local_sp_5 + (-40L);
                            *(uint64_t *)var_46 = 4207442UL;
                            indirect_placeholder();
                            local_sp_3 = var_46;
                            local_sp_7 = var_46;
                            if ((*var_19 & '\x02') != '\x00' & (*(unsigned char *)(var_35 + 40UL) & '\x02') != '\x00' & (uint64_t)(uint32_t)rax_0 != 0UL) {
                                *(uint64_t *)(local_sp_7 + (-8L)) = 4207171UL;
                                var_61 = indirect_placeholder_2(24UL);
                                *(uint64_t *)(var_61 + 8UL) = rbx_2;
                                *(uint64_t *)var_61 = *(uint64_t *)(local_sp_7 + 24UL);
                                *(uint64_t *)(var_61 + 16UL) = rbp_2;
                                var_62 = *(uint64_t *)4323088UL;
                                var_63 = local_sp_7 + (-16L);
                                *(uint64_t *)var_63 = 4207206UL;
                                var_64 = indirect_placeholder_75(var_62, var_61);
                                rbp_0 = var_61;
                                local_sp_4 = var_63;
                                if (var_64.field_0 == 0UL) {
                                    var_68 = var_64.field_1;
                                    var_69 = var_64.field_2;
                                    *(uint64_t *)(local_sp_7 + (-24L)) = 4207665UL;
                                    indirect_placeholder_12(var_68, var_69);
                                    abort();
                                }
                                var_65 = *(uint64_t *)(rbx_2 + 48UL);
                                rbx_0 = var_65;
                                rbp_2 = rbp_0;
                                local_sp_5 = local_sp_4;
                                rbx_2 = rbx_0;
                                local_sp_8 = local_sp_4;
                                rbp_1 = rbp_0;
                                if (rbx_0 == 0UL) {
                                    continue;
                                }
                                if ((uint64_t)var_10 != 0UL) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                *(uint64_t *)4323040UL = 0UL;
                                if (rbp_0 != 0UL) {
                                    loop_state_var = 2U;
                                    break;
                                }
                                loop_state_var = 1U;
                                break;
                            }
                        }
                        var_47 = local_sp_3 + (-8L);
                        *(uint64_t *)var_47 = 4207466UL;
                        indirect_placeholder();
                        local_sp_0 = var_47;
                        if (rax_0 == 0UL) {
                            var_48 = local_sp_3 + (-16L);
                            *(uint64_t *)var_48 = 4207484UL;
                            indirect_placeholder();
                            local_sp_0 = var_48;
                        }
                        var_49 = *(uint64_t *)(var_35 + 8UL);
                        *(uint64_t *)(local_sp_0 + 16UL) = var_49;
                        rdi1_0 = var_49;
                        local_sp_2 = local_sp_0;
                        while (rcx_0 != 0UL)
                            {
                                var_50 = *(unsigned char *)rdi1_0;
                                var_51 = rcx_0 + (-1L);
                                rcx_0 = var_51;
                                rcx_1 = var_51;
                                if (var_50 == '\x00') {
                                    break;
                                }
                                rdi1_0 = rdi1_0 + var_18;
                            }
                        var_52 = 18446744073709551614UL - (-2L);
                        var_53 = *var_26;
                        *(uint64_t *)(local_sp_0 + 24UL) = var_53;
                        rdi1_1 = var_53;
                        while (rcx_2 != 0UL)
                            {
                                var_54 = *(unsigned char *)rdi1_1;
                                var_55 = rcx_2 + (-1L);
                                rcx_2 = var_55;
                                rcx_3 = var_55;
                                if (var_54 == '\x00') {
                                    break;
                                }
                                rdi1_1 = rdi1_1 + var_18;
                            }
                        var_56 = 18446744073709551614UL - (-2L);
                        if ((*(uint32_t *)(local_sp_0 + 12UL) == 0U) && (var_52 > var_56)) {
                            *var_34 = rbx_2;
                            r14_0 = var_35;
                        } else {
                            var_57 = local_sp_0 + (-8L);
                            *(uint64_t *)var_57 = 4207627UL;
                            indirect_placeholder();
                            local_sp_2 = var_57;
                            if ((uint64_t)(uint32_t)var_56 == 0UL) {
                                var_58 = local_sp_0 + (-16L);
                                *(uint64_t *)var_58 = 4207646UL;
                                indirect_placeholder();
                                local_sp_2 = var_58;
                            }
                        }
                        var_59 = *(uint64_t *)(rbx_2 + 48UL);
                        rbx_0 = var_59;
                        local_sp_4 = local_sp_2;
                        if (*(unsigned char *)(local_sp_2 + 11UL) == '\x00') {
                            var_60 = local_sp_2 + (-8L);
                            *(uint64_t *)var_60 = 4207585UL;
                            indirect_placeholder_11(r14_0);
                            local_sp_4 = var_60;
                        }
                        rbp_2 = rbp_0;
                        local_sp_5 = local_sp_4;
                        rbx_2 = rbx_0;
                        local_sp_8 = local_sp_4;
                        rbp_1 = rbp_0;
                        if (rbx_0 == 0UL) {
                            continue;
                        }
                        if ((uint64_t)var_10 != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        *(uint64_t *)4323040UL = 0UL;
                        if (rbp_0 != 0UL) {
                            loop_state_var = 2U;
                            break;
                        }
                        loop_state_var = 1U;
                        break;
                    }
                }
                *(uint64_t *)(local_sp_6 + 32UL) = *(uint64_t *)(rbx_2 + 32UL);
                local_sp_7 = local_sp_6;
            } else {
                *(uint64_t *)(local_sp_6 + 32UL) = *(uint64_t *)(rbx_2 + 32UL);
                local_sp_7 = local_sp_6;
            }
        }
    switch (loop_state_var) {
      case 0U:
        {
            return;
        }
        break;
      case 2U:
      case 1U:
        {
            switch (loop_state_var) {
              case 1U:
                {
                    var_70 = *(uint64_t *)4323088UL;
                    *(uint64_t *)(local_sp_8 + (-8L)) = 4207725UL;
                    indirect_placeholder_11(var_70);
                    *(uint64_t *)4323088UL = 0UL;
                    return;
                }
                break;
              case 2U:
                {
                    var_66 = *(uint64_t *)(rbp_1 + 8UL);
                    *(uint64_t *)(var_66 + 48UL) = rbx_1;
                    var_67 = *(uint64_t *)(rbp_1 + 16UL);
                    rbx_1 = var_66;
                    rbp_1 = var_67;
                    do {
                        var_66 = *(uint64_t *)(rbp_1 + 8UL);
                        *(uint64_t *)(var_66 + 48UL) = rbx_1;
                        var_67 = *(uint64_t *)(rbp_1 + 16UL);
                        rbx_1 = var_66;
                        rbp_1 = var_67;
                    } while (var_67 != 0UL);
                    *(uint64_t *)4323040UL = var_66;
                }
                break;
            }
        }
        break;
    }
}
