typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_102_ret_type;
struct indirect_placeholder_103_ret_type;
struct indirect_placeholder_104_ret_type;
struct indirect_placeholder_102_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_103_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_104_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern struct indirect_placeholder_102_ret_type indirect_placeholder_102(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_103_ret_type indirect_placeholder_103(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_104_ret_type indirect_placeholder_104(uint64_t param_0, uint64_t param_1, uint64_t param_2);
void bb_two_way_long_needle(uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t rcx) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t *var_11;
    struct indirect_placeholder_102_ret_type var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t rax_4;
    uint64_t var_62;
    uint64_t local_sp_1;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t *_pre_phi;
    uint64_t var_66;
    uint64_t rax_0;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t storemerge;
    uint64_t var_67;
    uint64_t r12_0;
    uint64_t var_70;
    uint64_t rax_1_in;
    uint64_t rax_1;
    uint64_t r12_2;
    uint64_t var_71;
    uint64_t var_63;
    uint64_t var_35;
    uint64_t rcx4_0;
    uint64_t r14_1;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t local_sp_2;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t rax_2;
    uint64_t var_43;
    uint64_t var_42;
    uint64_t r12_1;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t rdx1_0;
    uint64_t rax_3;
    uint64_t var_27;
    uint64_t r14_0;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_36;
    uint64_t var_52;
    uint64_t _pre_phi176;
    uint64_t local_sp_0;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t *var_60;
    struct indirect_placeholder_103_ret_type var_61;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t rdi2_0;
    uint64_t r12_3;
    uint64_t *var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t *var_33;
    struct indirect_placeholder_104_ret_type var_34;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t rax_5;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t *var_22;
    uint64_t var_23;
    uint64_t *var_24;
    uint64_t var_50;
    uint64_t *var_51;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_8 = (uint64_t *)(var_0 + (-2144L));
    *var_8 = rsi;
    var_9 = var_0 + (-64L);
    var_10 = var_0 + (-2192L);
    var_11 = (uint64_t *)var_10;
    *var_11 = 4243565UL;
    var_12 = indirect_placeholder_102(var_9, rcx, rdx, rdi, var_5, rcx);
    var_13 = var_12.field_0;
    var_14 = var_12.field_1;
    *var_11 = var_13;
    var_15 = var_0 + (-2128L);
    var_16 = var_0 + (-80L);
    rax_4 = var_15;
    r12_2 = 0UL;
    r14_1 = 0UL;
    r12_1 = 0UL;
    var_27 = var_14;
    _pre_phi176 = 18446744073709551615UL;
    r12_3 = 0UL;
    rax_5 = rdx;
    *(uint64_t *)rax_4 = var_14;
    var_17 = rax_4 + 8UL;
    rax_4 = var_17;
    do {
        *(uint64_t *)rax_4 = var_14;
        var_17 = rax_4 + 8UL;
        rax_4 = var_17;
    } while (var_17 != var_16);
    if (var_14 != 0UL) {
        var_18 = var_14 + rdx;
        var_19 = var_14 + (-1L);
        _pre_phi176 = var_19;
        *(uint64_t *)((((uint64_t)*(unsigned char *)rax_5 << 3UL) + var_10) + 64UL) = ((var_19 + rdx) - rax_5);
        var_20 = rax_5 + 1UL;
        rax_5 = var_20;
        do {
            *(uint64_t *)((((uint64_t)*(unsigned char *)rax_5 << 3UL) + var_10) + 64UL) = ((var_19 + rdx) - rax_5);
            var_20 = rax_5 + 1UL;
            rax_5 = var_20;
        } while (var_20 != var_18);
        var_21 = *(uint64_t *)(var_0 + (-72L));
        var_22 = (uint64_t *)(var_0 + (-2176L));
        *var_22 = var_21;
        var_23 = var_0 + (-2200L);
        var_24 = (uint64_t *)var_23;
        *var_24 = 4243670UL;
        indirect_placeholder();
        _pre_phi = var_24;
        local_sp_0 = var_23;
        local_sp_2 = var_23;
        if ((uint64_t)(uint32_t)var_21 != 0UL) {
            var_25 = *var_24;
            *(uint64_t *)(var_0 + (-2168L)) = (1UL - var_25);
            *(uint64_t *)(var_0 + (-2152L)) = (var_25 + (-1L));
            *var_22 = (var_14 - *(uint64_t *)(var_0 + (-2184L)));
            *var_8 = var_25;
            var_26 = *(uint64_t *)(var_0 + (-2160L));
            *var_11 = var_14;
            rdi2_0 = var_26;
            var_28 = (uint64_t *)(local_sp_2 + 8UL);
            var_29 = var_27 + r14_1;
            var_30 = var_29 - rdi2_0;
            var_31 = rdi2_0 + rdi;
            var_32 = local_sp_2 + (-8L);
            var_33 = (uint64_t *)var_32;
            *var_33 = 4243785UL;
            var_34 = indirect_placeholder_104(var_30, var_31, 0UL);
            rdi2_0 = var_29;
            local_sp_2 = var_32;
            while (!((var_34.field_0 != 0UL) || (var_29 == 0UL)))
                {
                    var_35 = *(uint64_t *)((((uint64_t)*(unsigned char *)((var_29 + rdi) + (-1L)) << 3UL) + var_32) + 64UL);
                    rcx4_0 = var_35;
                    if (var_35 != 0UL) {
                        if (*var_28 > var_35) {
                            rcx4_0 = (r12_3 == 0UL) ? var_35 : *(uint64_t *)(local_sp_2 + 16UL);
                        }
                        var_36 = r14_1 + rcx4_0;
                        r14_0 = var_36;
                        var_27 = *(uint64_t *)local_sp_2;
                        r12_3 = r12_1;
                        r14_1 = r14_0;
                        var_28 = (uint64_t *)(local_sp_2 + 8UL);
                        var_29 = var_27 + r14_1;
                        var_30 = var_29 - rdi2_0;
                        var_31 = rdi2_0 + rdi;
                        var_32 = local_sp_2 + (-8L);
                        var_33 = (uint64_t *)var_32;
                        *var_33 = 4243785UL;
                        var_34 = indirect_placeholder_104(var_30, var_31, 0UL);
                        rdi2_0 = var_29;
                        local_sp_2 = var_32;
                        continue;
                    }
                    var_37 = *var_33;
                    var_38 = helper_cc_compute_c_wrapper(r12_3 - var_37, var_37, var_7, 17U);
                    var_39 = (var_38 == 0UL) ? r12_3 : var_37;
                    var_40 = r14_1 + rdi;
                    var_41 = helper_cc_compute_c_wrapper(var_39 - var_19, var_19, var_7, 17U);
                    rax_2 = var_39;
                    if (var_41 != 0UL) {
                        while (1U)
                            {
                                if ((uint64_t)(*(unsigned char *)(rax_2 + rdx) - *(unsigned char *)(rax_2 + var_40)) != 0UL) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                var_43 = rax_2 + 1UL;
                                rax_2 = var_43;
                                if (var_43 == var_19) {
                                    continue;
                                }
                                loop_state_var = 1U;
                                break;
                            }
                        if (var_19 <= rax_2) {
                            var_42 = (r14_1 + *(uint64_t *)(local_sp_2 + 24UL)) + rax_2;
                            r14_0 = var_42;
                            var_27 = *(uint64_t *)local_sp_2;
                            r12_3 = r12_1;
                            r14_1 = r14_0;
                            var_28 = (uint64_t *)(local_sp_2 + 8UL);
                            var_29 = var_27 + r14_1;
                            var_30 = var_29 - rdi2_0;
                            var_31 = rdi2_0 + rdi;
                            var_32 = local_sp_2 + (-8L);
                            var_33 = (uint64_t *)var_32;
                            *var_33 = 4243785UL;
                            var_34 = indirect_placeholder_104(var_30, var_31, 0UL);
                            rdi2_0 = var_29;
                            local_sp_2 = var_32;
                            continue;
                        }
                    }
                    var_44 = *(uint64_t *)(local_sp_2 + 40UL);
                    var_45 = *(uint64_t *)(local_sp_2 + 48UL);
                    var_46 = *var_33;
                    var_47 = helper_cc_compute_c_wrapper(r12_3 - var_46, var_46, var_7, 17U);
                    rax_3 = var_44;
                    rdx1_0 = var_45;
                    if (var_47 == 0UL) {
                        if ((r12_3 + 1UL) <= rdx1_0) {
                            break;
                        }
                        var_48 = r14_1 + *var_28;
                        var_49 = *(uint64_t *)(local_sp_2 + 16UL);
                        r12_1 = var_49;
                        r14_0 = var_48;
                    } else {
                        while (1U)
                            {
                                rdx1_0 = r12_3;
                                if ((uint64_t)(*(unsigned char *)(rax_3 + rdx) - *(unsigned char *)(rax_3 + var_40)) != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                if (r12_3 != rax_3) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                rax_3 = rax_3 + (-1L);
                                continue;
                            }
                        rdx1_0 = rax_3 + 1UL;
                    }
                }
            return;
        }
    }
    var_50 = var_0 + (-2200L);
    var_51 = (uint64_t *)var_50;
    *var_51 = 4244291UL;
    indirect_placeholder();
    _pre_phi = var_51;
    local_sp_0 = var_50;
    if ((uint64_t)((uint32_t)var_16 & (-8)) != 0UL) {
        return;
    }
}
