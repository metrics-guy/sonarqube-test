typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint32_t init_state_0x82fc(void);
uint64_t bb_mbsalign(uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t rcx, uint64_t r8, uint64_t r9) {
    uint64_t local_sp_2;
    uint64_t *var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t *_pre_phi111;
    uint64_t _pre_phi;
    uint64_t rbp_1;
    uint64_t r14_1;
    uint64_t rax_0;
    uint64_t local_sp_3;
    uint64_t var_44;
    uint64_t rbx_1;
    uint64_t rbp_2;
    uint64_t rax_1;
    uint64_t r13_0;
    uint64_t rbx_2;
    uint64_t var_45;
    uint64_t *var_28;
    uint64_t var_0;
    uint32_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t rbp_0;
    uint64_t rbx_3;
    uint64_t var_17;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t *_pre_pre_phi;
    uint64_t rbx_0_in;
    uint64_t local_sp_0;
    uint64_t rbx_0;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t *var_34;
    uint64_t var_27;
    uint64_t *var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    bool var_39;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t local_sp_4;
    uint64_t rdi2_0;
    uint64_t rcx4_0;
    uint64_t rcx4_1;
    unsigned char var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t local_sp_1;
    uint64_t r14_0;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t local_sp_5;
    uint64_t var_53;
    uint64_t local_sp_6;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_15;
    uint64_t *var_16;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_state_0x82fc();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r15();
    var_5 = init_rbp();
    var_6 = init_r12();
    var_7 = init_r14();
    var_8 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_4;
    *(uint64_t *)(var_0 + (-16L)) = var_7;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_6;
    *(uint64_t *)(var_0 + (-40L)) = var_5;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_9 = var_0 + (-104L);
    *(uint64_t *)(var_0 + (-72L)) = rdi;
    *(uint64_t *)(var_0 + (-64L)) = rsi;
    *(uint64_t *)(var_0 + (-96L)) = rdx;
    *(uint32_t *)(var_0 + (-76L)) = (uint32_t)r8;
    var_10 = (uint64_t)var_1;
    rbx_1 = 0UL;
    rbx_2 = 0UL;
    rbx_3 = 18446744073709551615UL;
    rdi2_0 = rdi;
    rcx4_0 = 18446744073709551615UL;
    rcx4_1 = 0UL;
    local_sp_1 = var_9;
    while (rcx4_0 != 0UL)
        {
            rdi2_0 = rdi2_0 + var_10;
        }
    var_13 = rcx4_1 ^ (-1L);
    var_14 = 18446744073709551614UL - (-2L);
    rbp_0 = var_14;
    r14_0 = var_14;
    rbp_1 = var_14;
    if ((r9 & 2UL) == 0UL) {
        var_15 = var_0 + (-112L);
        var_16 = (uint64_t *)var_15;
        *var_16 = 4223679UL;
        indirect_placeholder();
        local_sp_1 = var_15;
        if (rcx4_1 < 18446744073709551614UL) {
            var_17 = var_0 + (-120L);
            *(uint64_t *)var_17 = 4223705UL;
            indirect_placeholder();
            local_sp_2 = var_17;
            local_sp_6 = var_17;
            if (rcx4_1 == 0UL) {
                var_39 = ((r9 & 1UL) == 0UL);
                *(uint64_t *)var_9 = 0UL;
                rbx_3 = var_13;
                if (var_39) {
                    *(uint64_t *)(local_sp_6 + (-8L)) = 4224136UL;
                    indirect_placeholder();
                    *(uint64_t *)(local_sp_6 + (-16L)) = 4224146UL;
                    indirect_placeholder();
                    return rbx_3;
                }
                var_40 = (uint64_t *)rcx;
                var_41 = *var_40;
                var_42 = var_41 - r14_0;
                var_43 = helper_cc_compute_c_wrapper(var_42, r14_0, var_8, 17U);
                _pre_phi111 = var_40;
                _pre_phi = var_42;
                rbp_1 = rbp_0;
                r14_1 = r14_0;
                rax_0 = var_41;
                local_sp_3 = local_sp_2;
                rbp_2 = var_41;
                rax_1 = var_41;
                _pre_pre_phi = var_40;
                local_sp_4 = local_sp_2;
                if (var_43 == 0UL) {
                    var_44 = helper_cc_compute_c_wrapper(r14_1 - rax_0, rax_0, var_8, 17U);
                    _pre_phi111 = _pre_pre_phi;
                    rbx_1 = (var_44 == 0UL) ? 0UL : _pre_phi;
                    rbp_2 = rbp_1;
                    rax_1 = r14_1;
                    local_sp_4 = local_sp_3;
                }
            } else {
                var_18 = 0UL - rcx4_1;
                var_19 = var_18 << 2UL;
                var_20 = var_0 + (-128L);
                *(uint64_t *)var_20 = 4223774UL;
                var_21 = indirect_placeholder_2(var_19);
                local_sp_2 = var_20;
                local_sp_6 = var_20;
                if (var_21 == 0UL) {
                    *var_16 = 0UL;
                    if ((r9 & 1UL) == 0UL) {
                        *(uint64_t *)(local_sp_6 + (-8L)) = 4224136UL;
                        indirect_placeholder();
                        *(uint64_t *)(local_sp_6 + (-16L)) = 4224146UL;
                        indirect_placeholder();
                        return rbx_3;
                    }
                    var_40 = (uint64_t *)rcx;
                    var_41 = *var_40;
                    var_42 = var_41 - r14_0;
                    var_43 = helper_cc_compute_c_wrapper(var_42, r14_0, var_8, 17U);
                    _pre_phi111 = var_40;
                    _pre_phi = var_42;
                    rbp_1 = rbp_0;
                    r14_1 = r14_0;
                    rax_0 = var_41;
                    local_sp_3 = local_sp_2;
                    rbp_2 = var_41;
                    rax_1 = var_41;
                    _pre_pre_phi = var_40;
                    local_sp_4 = local_sp_2;
                    if (var_43 == 0UL) {
                        var_44 = helper_cc_compute_c_wrapper(r14_1 - rax_0, rax_0, var_8, 17U);
                        _pre_phi111 = _pre_pre_phi;
                        rbx_1 = (var_44 == 0UL) ? 0UL : _pre_phi;
                        rbp_2 = rbp_1;
                        rax_1 = r14_1;
                        local_sp_4 = local_sp_3;
                    }
                } else {
                    var_22 = (uint64_t *)(var_0 + (-136L));
                    *var_22 = 4223802UL;
                    indirect_placeholder();
                    *(uint32_t *)((var_19 + var_21) + (-4L)) = 0U;
                    *(uint64_t *)(var_0 + (-144L)) = 4223828UL;
                    var_23 = indirect_placeholder_2(var_21);
                    var_24 = var_0 + (-152L);
                    *(uint64_t *)var_24 = 4223841UL;
                    var_25 = indirect_placeholder_1(var_21, var_18);
                    var_26 = (uint64_t)((long)(var_25 << 32UL) >> (long)32UL);
                    r14_1 = var_26;
                    local_sp_3 = var_24;
                    rbx_0_in = var_25;
                    local_sp_0 = var_24;
                    r14_0 = var_26;
                    if ((uint64_t)(unsigned char)var_23 == 0UL) {
                        var_28 = (uint64_t *)rcx;
                        var_29 = *var_28;
                        var_30 = var_29 - var_26;
                        var_31 = helper_cc_compute_c_wrapper(var_30, var_26, var_8, 17U);
                        rbx_0_in = var_14;
                        _pre_pre_phi = var_28;
                        _pre_phi = var_30;
                        rax_0 = var_29;
                        if (var_31 == 0UL) {
                            *var_22 = 0UL;
                            var_44 = helper_cc_compute_c_wrapper(r14_1 - rax_0, rax_0, var_8, 17U);
                            _pre_phi111 = _pre_pre_phi;
                            rbx_1 = (var_44 == 0UL) ? 0UL : _pre_phi;
                            rbp_2 = rbp_1;
                            rax_1 = r14_1;
                            local_sp_4 = local_sp_3;
                        } else {
                            rbx_0 = rbx_0_in + 1UL;
                            var_32 = local_sp_0 + (-8L);
                            *(uint64_t *)var_32 = 4223996UL;
                            var_33 = indirect_placeholder_2(rbx_0);
                            var_34 = (uint64_t *)(local_sp_0 + 8UL);
                            *var_34 = var_33;
                            local_sp_2 = var_32;
                            local_sp_6 = var_32;
                            if (var_33 == 0UL) {
                                if ((r9 & 1UL) == 0UL) {
                                    *(uint64_t *)(local_sp_6 + (-8L)) = 4224136UL;
                                    indirect_placeholder();
                                    *(uint64_t *)(local_sp_6 + (-16L)) = 4224146UL;
                                    indirect_placeholder();
                                    return rbx_3;
                                }
                            }
                            var_35 = *(uint64_t *)rcx;
                            *(uint64_t *)(local_sp_0 + (-16L)) = 4224018UL;
                            var_36 = indirect_placeholder_1(var_21, var_35);
                            var_37 = *(uint64_t *)local_sp_0;
                            var_38 = local_sp_0 + (-24L);
                            *(uint64_t *)var_38 = 4224040UL;
                            indirect_placeholder();
                            *var_34 = var_37;
                            rbp_0 = var_36;
                            r14_0 = var_36;
                            local_sp_2 = var_38;
                            var_40 = (uint64_t *)rcx;
                            var_41 = *var_40;
                            var_42 = var_41 - r14_0;
                            var_43 = helper_cc_compute_c_wrapper(var_42, r14_0, var_8, 17U);
                            _pre_phi111 = var_40;
                            _pre_phi = var_42;
                            rbp_1 = rbp_0;
                            r14_1 = r14_0;
                            rax_0 = var_41;
                            local_sp_3 = local_sp_2;
                            rbp_2 = var_41;
                            rax_1 = var_41;
                            _pre_pre_phi = var_40;
                            local_sp_4 = local_sp_2;
                            if (var_43 == 0UL) {
                                var_44 = helper_cc_compute_c_wrapper(r14_1 - rax_0, rax_0, var_8, 17U);
                                _pre_phi111 = _pre_pre_phi;
                                rbx_1 = (var_44 == 0UL) ? 0UL : _pre_phi;
                                rbp_2 = rbp_1;
                                rax_1 = r14_1;
                                local_sp_4 = local_sp_3;
                            }
                        }
                    } else {
                        var_27 = var_0 + (-160L);
                        *(uint64_t *)var_27 = 4223984UL;
                        indirect_placeholder();
                        local_sp_0 = var_27;
                        rbx_0 = rbx_0_in + 1UL;
                        var_32 = local_sp_0 + (-8L);
                        *(uint64_t *)var_32 = 4223996UL;
                        var_33 = indirect_placeholder_2(rbx_0);
                        var_34 = (uint64_t *)(local_sp_0 + 8UL);
                        *var_34 = var_33;
                        local_sp_2 = var_32;
                        local_sp_6 = var_32;
                        if (var_33 != 0UL) {
                            if ((r9 & 1UL) == 0UL) {
                                *(uint64_t *)(local_sp_6 + (-8L)) = 4224136UL;
                                indirect_placeholder();
                                *(uint64_t *)(local_sp_6 + (-16L)) = 4224146UL;
                                indirect_placeholder();
                                return rbx_3;
                            }
                        }
                        var_35 = *(uint64_t *)rcx;
                        *(uint64_t *)(local_sp_0 + (-16L)) = 4224018UL;
                        var_36 = indirect_placeholder_1(var_21, var_35);
                        var_37 = *(uint64_t *)local_sp_0;
                        var_38 = local_sp_0 + (-24L);
                        *(uint64_t *)var_38 = 4224040UL;
                        indirect_placeholder();
                        *var_34 = var_37;
                        rbp_0 = var_36;
                        r14_0 = var_36;
                        local_sp_2 = var_38;
                    }
                }
            }
        } else {
            *(uint64_t *)(local_sp_1 + 16UL) = 0UL;
            local_sp_2 = local_sp_1;
            var_40 = (uint64_t *)rcx;
            var_41 = *var_40;
            var_42 = var_41 - r14_0;
            var_43 = helper_cc_compute_c_wrapper(var_42, r14_0, var_8, 17U);
            _pre_phi111 = var_40;
            _pre_phi = var_42;
            rbp_1 = rbp_0;
            r14_1 = r14_0;
            rax_0 = var_41;
            local_sp_3 = local_sp_2;
            rbp_2 = var_41;
            rax_1 = var_41;
            _pre_pre_phi = var_40;
            local_sp_4 = local_sp_2;
            if (var_43 == 0UL) {
                var_44 = helper_cc_compute_c_wrapper(r14_1 - rax_0, rax_0, var_8, 17U);
                _pre_phi111 = _pre_pre_phi;
                rbx_1 = (var_44 == 0UL) ? 0UL : _pre_phi;
                rbp_2 = rbp_1;
                rax_1 = r14_1;
                local_sp_4 = local_sp_3;
            }
        }
    } else {
        *(uint64_t *)(local_sp_1 + 16UL) = 0UL;
        local_sp_2 = local_sp_1;
        var_40 = (uint64_t *)rcx;
        var_41 = *var_40;
        var_42 = var_41 - r14_0;
        var_43 = helper_cc_compute_c_wrapper(var_42, r14_0, var_8, 17U);
        _pre_phi111 = var_40;
        _pre_phi = var_42;
        rbp_1 = rbp_0;
        r14_1 = r14_0;
        rax_0 = var_41;
        local_sp_3 = local_sp_2;
        rbp_2 = var_41;
        rax_1 = var_41;
        _pre_pre_phi = var_40;
        local_sp_4 = local_sp_2;
        if (var_43 == 0UL) {
            var_44 = helper_cc_compute_c_wrapper(r14_1 - rax_0, rax_0, var_8, 17U);
            _pre_phi111 = _pre_pre_phi;
            rbx_1 = (var_44 == 0UL) ? 0UL : _pre_phi;
            rbp_2 = rbp_1;
            rax_1 = r14_1;
            local_sp_4 = local_sp_3;
        }
        *_pre_phi111 = rax_1;
        r13_0 = rbx_1;
        local_sp_5 = local_sp_4;
        switch (*(uint32_t *)(local_sp_4 + 28UL)) {
          case 1U:
            {
                r13_0 = 0UL;
                rbx_2 = rbx_1;
            }
            break;
          case 0U:
            {
                var_46 = ((r9 & 4UL) == 0UL) ? rbx_2 : 0UL;
                var_47 = ((r9 & 8UL) == 0UL) ? r13_0 : 0UL;
                var_48 = *(uint64_t *)(local_sp_4 + 8UL);
                if (var_48 == 0UL) {
                    var_49 = *(uint64_t *)(local_sp_4 + 40UL);
                    var_50 = (var_48 + var_49) + (-1L);
                    *(uint64_t *)(local_sp_4 + (-8L)) = 4224190UL;
                    var_51 = indirect_placeholder_4(var_46, var_49, var_50);
                    *(uint64_t *)(local_sp_4 + (-16L)) = 4224216UL;
                    indirect_placeholder();
                    var_52 = local_sp_4 + (-24L);
                    *(uint64_t *)var_52 = 4224230UL;
                    indirect_placeholder_4(var_47, var_51, var_50);
                    local_sp_5 = var_52;
                }
                var_53 = (var_46 + rbp_2) + var_47;
                rbx_3 = var_53;
                local_sp_6 = local_sp_5;
                *(uint64_t *)(local_sp_6 + (-8L)) = 4224136UL;
                indirect_placeholder();
                *(uint64_t *)(local_sp_6 + (-16L)) = 4224146UL;
                indirect_placeholder();
                return rbx_3;
            }
            break;
          default:
            {
                var_45 = rbx_1 >> 1UL;
                r13_0 = var_45;
                rbx_2 = (rbx_1 & 1UL) + var_45;
            }
            break;
        }
    }
}
