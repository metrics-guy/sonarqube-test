typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_get_header_ret_type;
struct indirect_placeholder_81_ret_type;
struct indirect_placeholder_80_ret_type;
struct indirect_placeholder_82_ret_type;
struct indirect_placeholder_79_ret_type;
struct bb_get_header_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_81_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_80_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_82_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_79_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern void indirect_placeholder_12(uint64_t param_0, uint64_t param_1);
extern uint64_t init_rcx(void);
extern struct indirect_placeholder_81_ret_type indirect_placeholder_81(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_80_ret_type indirect_placeholder_80(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_82_ret_type indirect_placeholder_82(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_79_ret_type indirect_placeholder_79(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
struct bb_get_header_ret_type bb_get_header(uint64_t r8, uint64_t r9) {
    struct indirect_placeholder_80_ret_type var_20;
    struct indirect_placeholder_82_ret_type var_41;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t rcx_2;
    uint64_t rbx_0;
    uint64_t local_sp_2;
    uint64_t *var_60;
    uint64_t r94_1;
    uint64_t *_pre_phi84;
    uint64_t local_sp_0;
    uint64_t storemerge;
    uint64_t rcx_1;
    uint64_t var_61;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t rax_0;
    uint64_t rdx_0;
    uint64_t rcx_0;
    uint64_t local_sp_1;
    uint64_t r83_0;
    uint64_t r94_0;
    uint64_t var_50;
    bool var_51;
    uint64_t *var_52;
    uint64_t r83_2;
    uint64_t r94_2;
    struct bb_get_header_ret_type mrv;
    struct bb_get_header_ret_type mrv1;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t r83_1;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint32_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t *var_15;
    struct indirect_placeholder_81_ret_type var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint16_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t rdi_0;
    uint64_t rsi_0;
    uint64_t var_28;
    uint64_t var_29;
    uint16_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t *var_40;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    struct indirect_placeholder_79_ret_type var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_24;
    struct bb_get_header_ret_type mrv2;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_rbp();
    var_4 = init_r12();
    var_5 = init_rcx();
    var_6 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_2;
    var_7 = var_0 + (-272L);
    *(uint64_t *)var_7 = 4206258UL;
    indirect_placeholder_12(r8, r9);
    rcx_2 = var_5;
    rbx_0 = 0UL;
    local_sp_2 = var_7;
    r94_1 = r9;
    rcx_1 = var_5;
    r83_2 = r8;
    r94_2 = r9;
    r83_1 = r8;
    if (*(uint64_t *)4322928UL == 0UL) {
        mrv.field_0 = rcx_2;
        mrv1 = mrv;
        mrv1.field_1 = r83_2;
        mrv2 = mrv1;
        mrv2.field_2 = r94_2;
        return mrv2;
    }
    while (1U)
        {
            *(uint64_t *)(local_sp_2 + 8UL) = 0UL;
            var_8 = rbx_0 << 3UL;
            var_9 = *(uint64_t *)(var_8 + *(uint64_t *)4322936UL);
            var_10 = *(uint64_t *)(var_9 + 24UL);
            rcx_0 = rcx_1;
            r83_0 = r83_1;
            r94_0 = r94_1;
            rax_0 = var_9;
            if (*(uint32_t *)var_9 != 2U) {
                var_24 = local_sp_2 + (-8L);
                *(uint64_t *)var_24 = 4206313UL;
                indirect_placeholder();
                *(uint64_t *)local_sp_2 = rax_0;
                local_sp_1 = var_24;
                var_50 = *(uint64_t *)(local_sp_1 + 8UL);
                var_51 = (var_50 == 0UL);
                var_52 = (uint64_t *)(local_sp_1 + (-8L));
                rcx_2 = rcx_0;
                r94_1 = r94_0;
                rcx_1 = rcx_0;
                r83_2 = r83_0;
                r94_2 = r94_0;
                r83_1 = r83_0;
                if (var_51) {
                    *var_52 = 4206810UL;
                    indirect_placeholder_12(r83_0, r94_0);
                    abort();
                }
                *var_52 = 4206337UL;
                indirect_placeholder_2(var_50);
                var_53 = *(uint64_t *)local_sp_1;
                *(uint64_t *)(var_8 + *(uint64_t *)(((*(uint64_t *)4322912UL << 3UL) + *(uint64_t *)4322920UL) + (-8L))) = var_53;
                var_54 = *(uint64_t *)(*(uint64_t *)(var_8 + *(uint64_t *)4322936UL) + 32UL);
                var_55 = local_sp_1 + (-16L);
                *(uint64_t *)var_55 = 4206390UL;
                var_56 = indirect_placeholder_1(var_53, 0UL);
                local_sp_0 = var_55;
                if (var_54 > (uint64_t)((long)(var_56 << 32UL) >> (long)32UL)) {
                    var_60 = (uint64_t *)(var_8 + *(uint64_t *)4322936UL);
                    _pre_phi84 = var_60;
                    storemerge = *(uint64_t *)(*var_60 + 32UL);
                } else {
                    var_57 = *var_52;
                    var_58 = local_sp_1 + (-24L);
                    *(uint64_t *)var_58 = 4206825UL;
                    var_59 = indirect_placeholder_1(var_57, 0UL);
                    _pre_phi84 = (uint64_t *)(var_8 + *(uint64_t *)4322936UL);
                    local_sp_0 = var_58;
                    storemerge = (uint64_t)((long)(var_59 << 32UL) >> (long)32UL);
                }
                *(uint64_t *)(*_pre_phi84 + 32UL) = storemerge;
                var_61 = rbx_0 + 1UL;
                rbx_0 = var_61;
                local_sp_2 = local_sp_0;
                if (*(uint64_t *)4322928UL <= var_61) {
                    continue;
                }
                break;
            }
            var_11 = *(uint32_t *)4322944UL;
            var_12 = (uint64_t)var_11;
            rcx_0 = 4287417UL;
            rax_0 = var_12;
            if (var_11 != 0U) {
                rcx_0 = var_10;
                if ((uint64_t)(var_11 + (-4)) != 0UL) {
                    if ((uint64_t)(var_11 + (-3)) == 0UL) {
                        var_24 = local_sp_2 + (-8L);
                        *(uint64_t *)var_24 = 4206313UL;
                        indirect_placeholder();
                        *(uint64_t *)local_sp_2 = rax_0;
                        local_sp_1 = var_24;
                    } else {
                        var_13 = local_sp_2 + 16UL;
                        var_14 = *(uint64_t *)4323072UL;
                        var_15 = (uint64_t *)(local_sp_2 + (-8L));
                        *var_15 = 4206534UL;
                        var_16 = indirect_placeholder_81(var_14, var_13);
                        var_17 = var_16.field_0;
                        var_18 = var_16.field_1;
                        var_19 = local_sp_2 + (-16L);
                        *(uint64_t *)var_19 = 4206560UL;
                        var_20 = indirect_placeholder_80(0UL, var_17, local_sp_2, 4287424UL, var_10, var_18, r94_1);
                        var_21 = var_20.field_0;
                        var_22 = var_20.field_1;
                        var_23 = var_20.field_2;
                        local_sp_1 = var_19;
                        r83_0 = var_22;
                        r94_0 = var_23;
                        if ((uint64_t)((uint32_t)var_21 + 1U) == 0UL) {
                            *var_15 = 0UL;
                        }
                    }
                    var_50 = *(uint64_t *)(local_sp_1 + 8UL);
                    var_51 = (var_50 == 0UL);
                    var_52 = (uint64_t *)(local_sp_1 + (-8L));
                    rcx_2 = rcx_0;
                    r94_1 = r94_0;
                    rcx_1 = rcx_0;
                    r83_2 = r83_0;
                    r94_2 = r94_0;
                    r83_1 = r83_0;
                    if (var_51) {
                        *var_52 = 4206810UL;
                        indirect_placeholder_12(r83_0, r94_0);
                        abort();
                    }
                    *var_52 = 4206337UL;
                    indirect_placeholder_2(var_50);
                    var_53 = *(uint64_t *)local_sp_1;
                    *(uint64_t *)(var_8 + *(uint64_t *)(((*(uint64_t *)4322912UL << 3UL) + *(uint64_t *)4322920UL) + (-8L))) = var_53;
                    var_54 = *(uint64_t *)(*(uint64_t *)(var_8 + *(uint64_t *)4322936UL) + 32UL);
                    var_55 = local_sp_1 + (-16L);
                    *(uint64_t *)var_55 = 4206390UL;
                    var_56 = indirect_placeholder_1(var_53, 0UL);
                    local_sp_0 = var_55;
                    if (var_54 > (uint64_t)((long)(var_56 << 32UL) >> (long)32UL)) {
                        var_60 = (uint64_t *)(var_8 + *(uint64_t *)4322936UL);
                        _pre_phi84 = var_60;
                        storemerge = *(uint64_t *)(*var_60 + 32UL);
                    } else {
                        var_57 = *var_52;
                        var_58 = local_sp_1 + (-24L);
                        *(uint64_t *)var_58 = 4206825UL;
                        var_59 = indirect_placeholder_1(var_57, 0UL);
                        _pre_phi84 = (uint64_t *)(var_8 + *(uint64_t *)4322936UL);
                        local_sp_0 = var_58;
                        storemerge = (uint64_t)((long)(var_59 << 32UL) >> (long)32UL);
                    }
                    *(uint64_t *)(*_pre_phi84 + 32UL) = storemerge;
                    var_61 = rbx_0 + 1UL;
                    rbx_0 = var_61;
                    local_sp_2 = local_sp_0;
                    if (*(uint64_t *)4322928UL <= var_61) {
                        continue;
                    }
                    break;
                }
                if ((*(unsigned char *)4323080UL & '\x10') != '\x00') {
                    var_24 = local_sp_2 + (-8L);
                    *(uint64_t *)var_24 = 4206313UL;
                    indirect_placeholder();
                    *(uint64_t *)local_sp_2 = rax_0;
                    local_sp_1 = var_24;
                    var_50 = *(uint64_t *)(local_sp_1 + 8UL);
                    var_51 = (var_50 == 0UL);
                    var_52 = (uint64_t *)(local_sp_1 + (-8L));
                    rcx_2 = rcx_0;
                    r94_1 = r94_0;
                    rcx_1 = rcx_0;
                    r83_2 = r83_0;
                    r94_2 = r94_0;
                    r83_1 = r83_0;
                    if (var_51) {
                        *var_52 = 4206810UL;
                        indirect_placeholder_12(r83_0, r94_0);
                        abort();
                    }
                    *var_52 = 4206337UL;
                    indirect_placeholder_2(var_50);
                    var_53 = *(uint64_t *)local_sp_1;
                    *(uint64_t *)(var_8 + *(uint64_t *)(((*(uint64_t *)4322912UL << 3UL) + *(uint64_t *)4322920UL) + (-8L))) = var_53;
                    var_54 = *(uint64_t *)(*(uint64_t *)(var_8 + *(uint64_t *)4322936UL) + 32UL);
                    var_55 = local_sp_1 + (-16L);
                    *(uint64_t *)var_55 = 4206390UL;
                    var_56 = indirect_placeholder_1(var_53, 0UL);
                    local_sp_0 = var_55;
                    if (var_54 > (uint64_t)((long)(var_56 << 32UL) >> (long)32UL)) {
                        var_60 = (uint64_t *)(var_8 + *(uint64_t *)4322936UL);
                        _pre_phi84 = var_60;
                        storemerge = *(uint64_t *)(*var_60 + 32UL);
                    } else {
                        var_57 = *var_52;
                        var_58 = local_sp_1 + (-24L);
                        *(uint64_t *)var_58 = 4206825UL;
                        var_59 = indirect_placeholder_1(var_57, 0UL);
                        _pre_phi84 = (uint64_t *)(var_8 + *(uint64_t *)4322936UL);
                        local_sp_0 = var_58;
                        storemerge = (uint64_t)((long)(var_59 << 32UL) >> (long)32UL);
                    }
                    *(uint64_t *)(*_pre_phi84 + 32UL) = storemerge;
                    var_61 = rbx_0 + 1UL;
                    rbx_0 = var_61;
                    local_sp_2 = local_sp_0;
                    if (*(uint64_t *)4322928UL <= var_61) {
                        continue;
                    }
                    break;
                }
            }
            var_25 = (uint16_t)(uint64_t)*(uint32_t *)4323080UL;
            var_26 = (uint64_t)(var_25 & (unsigned short)292U);
            var_27 = *(uint64_t *)4323072UL;
            rdi_0 = var_27;
            rsi_0 = var_27;
            var_28 = (uint64_t)(((unsigned __int128)(rsi_0 >> 3UL) * 2361183241434822607ULL) >> 68ULL);
            var_29 = rsi_0 + (var_28 * 18446744073709550616UL);
            var_30 = (uint16_t)rdi_0;
            var_31 = (uint64_t)(var_30 & (unsigned short)1023U);
            var_32 = var_29 | var_31;
            rsi_0 = var_28;
            while (var_32 != 0UL)
                {
                    rdi_0 = rdi_0 >> 10UL;
                    var_28 = (uint64_t)(((unsigned __int128)(rsi_0 >> 3UL) * 2361183241434822607ULL) >> 68ULL);
                    var_29 = rsi_0 + (var_28 * 18446744073709550616UL);
                    var_30 = (uint16_t)rdi_0;
                    var_31 = (uint64_t)(var_30 & (unsigned short)1023U);
                    var_32 = var_29 | var_31;
                    rsi_0 = var_28;
                }
            var_33 = (var_32 & (-256L)) | (var_29 == 0UL);
            var_34 = (uint64_t)(var_30 & (unsigned short)768U) | (var_31 == 0UL);
            var_35 = var_33 - var_34;
            var_36 = helper_cc_compute_c_wrapper(var_35, var_34, var_6, 14U);
            if (var_36 == 0UL) {
                rdx_0 = (uint64_t)(var_25 & (unsigned short)260U) | 184UL;
            } else {
                var_37 = helper_cc_compute_all_wrapper(var_35, var_34, var_6, 14U);
                if ((var_37 & 65UL) == 0UL) {
                    rdx_0 = (uint64_t)(var_25 & (unsigned short)260U) | 152UL;
                } else {
                    rdx_0 = var_26 | 152UL;
                }
            }
            var_38 = (uint64_t)(uint32_t)(((rdx_0 & 32UL) == 0UL) ? ((uint64_t)((uint32_t)rdx_0 & (-65281)) | 256UL) : rdx_0);
            var_39 = local_sp_2 + 16UL;
            var_40 = (uint64_t *)(local_sp_2 + (-8L));
            *var_40 = 4206730UL;
            var_41 = indirect_placeholder_82(var_38, var_27, var_39, 1UL, 1UL);
            var_42 = var_41.field_0;
            var_43 = var_41.field_1;
            var_44 = var_41.field_2;
            var_45 = local_sp_2 + (-16L);
            *(uint64_t *)var_45 = 4206758UL;
            var_46 = indirect_placeholder_79(0UL, var_42, local_sp_2, 4287424UL, 4287417UL, var_43, var_44);
            var_47 = var_46.field_0;
            var_48 = var_46.field_1;
            var_49 = var_46.field_2;
            local_sp_1 = var_45;
            r83_0 = var_48;
            r94_0 = var_49;
            if ((uint64_t)((uint32_t)var_47 + 1U) == 0UL) {
                *var_40 = 0UL;
            }
        }
    mrv.field_0 = rcx_2;
    mrv1 = mrv;
    mrv1.field_1 = r83_2;
    mrv2 = mrv1;
    mrv2.field_2 = r94_2;
    return mrv2;
}
