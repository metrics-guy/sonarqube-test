typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_105_ret_type;
struct indirect_placeholder_106_ret_type;
struct indirect_placeholder_107_ret_type;
struct indirect_placeholder_105_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_106_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_107_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern struct indirect_placeholder_105_ret_type indirect_placeholder_105(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_106_ret_type indirect_placeholder_106(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_107_ret_type indirect_placeholder_107(uint64_t param_0, uint64_t param_1, uint64_t param_2);
uint64_t bb_two_way_short_needle(uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t rcx) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t *var_8;
    uint64_t *var_9;
    uint64_t var_10;
    uint64_t *var_11;
    struct indirect_placeholder_105_ret_type var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t *var_16;
    uint64_t var_17;
    uint64_t *var_18;
    uint64_t rdi2_0;
    uint64_t var_32;
    uint64_t rax_0;
    uint64_t local_sp_1;
    uint64_t var_43;
    uint64_t var_34;
    uint64_t var_33;
    uint64_t storemerge3;
    uint64_t rax_1_in;
    uint64_t rax_1;
    uint64_t var_35;
    uint64_t r13_0;
    uint64_t rax_4;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t r14_0;
    uint64_t var_46;
    uint64_t storemerge2;
    uint64_t rax_2;
    uint64_t var_48;
    uint64_t var_47;
    uint64_t r15_0;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t rdx1_0;
    uint64_t rax_3;
    uint64_t storemerge;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t r15_1;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    bool var_24;
    bool var_25;
    uint64_t local_sp_0;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t *var_30;
    struct indirect_placeholder_106_ret_type var_31;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t *var_41;
    struct indirect_placeholder_107_ret_type var_42;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_8 = (uint64_t *)(var_0 + (-112L));
    *var_8 = rdi;
    var_9 = (uint64_t *)(var_0 + (-96L));
    *var_9 = rsi;
    var_10 = var_0 + (-64L);
    var_11 = (uint64_t *)(var_0 + (-144L));
    *var_11 = 4242990UL;
    var_12 = indirect_placeholder_105(var_10, rdx, rdx, rcx, var_5, rcx);
    var_13 = var_12.field_0;
    var_14 = var_12.field_1;
    var_15 = *(uint64_t *)(var_0 + (-72L));
    var_16 = (uint64_t *)(var_0 + (-128L));
    *var_16 = var_15;
    var_17 = var_0 + (-152L);
    var_18 = (uint64_t *)var_17;
    *var_18 = 4243018UL;
    indirect_placeholder();
    rax_0 = var_13;
    local_sp_1 = var_17;
    rax_1_in = var_13;
    r13_0 = 0UL;
    rax_4 = 0UL;
    r14_0 = 0UL;
    storemerge = 0UL;
    r15_1 = 0UL;
    local_sp_0 = var_17;
    if ((uint64_t)(uint32_t)var_15 == 0UL) {
        *var_11 = (1UL - var_13);
        *(uint64_t *)(var_0 + (-120L)) = (var_13 + (-1L));
        *(uint64_t *)(var_0 + (-104L)) = (rcx - *(uint64_t *)(var_0 + (-136L)));
        *var_9 = var_13;
        *var_18 = var_13;
        var_36 = *var_16;
        storemerge2 = *var_8;
        var_37 = r15_1 + rcx;
        var_38 = var_37 - storemerge2;
        var_39 = storemerge2 + var_36;
        var_40 = local_sp_1 + (-8L);
        var_41 = (uint64_t *)var_40;
        *var_41 = 4243132UL;
        var_42 = indirect_placeholder_107(var_38, var_39, 0UL);
        local_sp_1 = var_40;
        storemerge2 = var_37;
        while (!((var_42.field_0 != 0UL) || (var_37 == 0UL)))
            {
                var_43 = *var_41;
                var_44 = helper_cc_compute_c_wrapper(r14_0 - var_43, var_43, var_7, 17U);
                var_45 = (var_44 == 0UL) ? r14_0 : var_43;
                var_46 = r15_1 + var_36;
                rax_2 = var_45;
                if (var_45 >= rcx) {
                    while (1U)
                        {
                            if ((uint64_t)(*(unsigned char *)(rax_2 + var_14) - *(unsigned char *)(rax_2 + var_46)) != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            var_48 = rax_2 + 1UL;
                            rax_2 = var_48;
                            if (var_48 == rcx) {
                                continue;
                            }
                            loop_state_var = 0U;
                            break;
                        }
                    if (rax_2 >= rcx) {
                        var_47 = (r15_1 + *(uint64_t *)local_sp_1) + rax_2;
                        r15_0 = var_47;
                        r15_1 = r15_0;
                        r14_0 = storemerge;
                        var_37 = r15_1 + rcx;
                        var_38 = var_37 - storemerge2;
                        var_39 = storemerge2 + var_36;
                        var_40 = local_sp_1 + (-8L);
                        var_41 = (uint64_t *)var_40;
                        *var_41 = 4243132UL;
                        var_42 = indirect_placeholder_107(var_38, var_39, 0UL);
                        local_sp_1 = var_40;
                        storemerge2 = var_37;
                        continue;
                    }
                }
                var_49 = *(uint64_t *)(local_sp_1 + 24UL);
                var_50 = *(uint64_t *)(local_sp_1 + 48UL);
                var_51 = *var_41;
                var_52 = helper_cc_compute_c_wrapper(r14_0 - var_51, var_51, var_7, 17U);
                rax_3 = var_49;
                rdx1_0 = var_50;
                if (var_52 == 0UL) {
                    if ((r14_0 + 1UL) <= rdx1_0) {
                        rax_4 = *(uint64_t *)(local_sp_1 + 16UL) + r15_1;
                        break;
                    }
                    var_53 = r15_1 + *(uint64_t *)(local_sp_1 + 8UL);
                    var_54 = *(uint64_t *)(local_sp_1 + 40UL);
                    r15_0 = var_53;
                    storemerge = var_54;
                    r15_1 = r15_0;
                    r14_0 = storemerge;
                    var_37 = r15_1 + rcx;
                    var_38 = var_37 - storemerge2;
                    var_39 = storemerge2 + var_36;
                    var_40 = local_sp_1 + (-8L);
                    var_41 = (uint64_t *)var_40;
                    *var_41 = 4243132UL;
                    var_42 = indirect_placeholder_107(var_38, var_39, 0UL);
                    local_sp_1 = var_40;
                    storemerge2 = var_37;
                    continue;
                }
                while (1U)
                    {
                        rdx1_0 = r14_0;
                        if ((uint64_t)(*(unsigned char *)(rax_3 + var_14) - *(unsigned char *)(rax_3 + var_46)) != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        if (r14_0 != rax_3) {
                            loop_state_var = 0U;
                            break;
                        }
                        rax_3 = rax_3 + (-1L);
                        continue;
                    }
                rdx1_0 = rax_3 + 1UL;
            }
    } else {
        var_19 = rcx - var_13;
        var_20 = helper_cc_compute_c_wrapper(var_19 - var_13, var_13, var_7, 17U);
        var_21 = ((var_20 == 0UL) ? var_19 : var_13) + 1UL;
        *var_11 = var_21;
        *(uint64_t *)(var_0 + (-80L)) = var_21;
        var_22 = *var_8;
        *var_18 = (1UL - var_13);
        var_23 = *var_16;
        var_24 = (var_13 < rcx);
        var_25 = (var_13 == 0UL);
        rdi2_0 = var_22;
        while (1U)
            {
                var_26 = r13_0 + rcx;
                var_27 = var_26 - rdi2_0;
                var_28 = rdi2_0 + var_23;
                var_29 = local_sp_0 + (-8L);
                var_30 = (uint64_t *)var_29;
                *var_30 = 4243405UL;
                var_31 = indirect_placeholder_106(var_27, var_28, 0UL);
                rdi2_0 = var_26;
                local_sp_0 = var_29;
                if (!((var_26 != 0UL) && (var_31.field_0 == 0UL))) {
                    loop_state_var = 1U;
                    break;
                }
                var_32 = r13_0 + var_23;
                if (!var_24) {
                    while (1U)
                        {
                            if ((uint64_t)(*(unsigned char *)(rax_0 + var_14) - *(unsigned char *)(rax_0 + var_32)) != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            var_34 = rax_0 + 1UL;
                            rax_0 = var_34;
                            if (var_34 == rcx) {
                                continue;
                            }
                            loop_state_var = 0U;
                            break;
                        }
                    if (rax_0 >= rcx) {
                        var_33 = (r13_0 + *var_30) + rax_0;
                        storemerge3 = var_33;
                        r13_0 = storemerge3;
                        continue;
                    }
                }
                if (!var_25) {
                    loop_state_var = 0U;
                    break;
                }
                while (1U)
                    {
                        rax_1 = rax_1_in + (-1L);
                        rax_1_in = rax_1;
                        if ((uint64_t)(*(unsigned char *)(rax_1 + var_14) - *(unsigned char *)(rax_1 + var_32)) != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        if (rax_1 == 0UL) {
                            continue;
                        }
                        loop_state_var = 0U;
                        break;
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 1U:
                    {
                        var_35 = r13_0 + *(uint64_t *)local_sp_0;
                        storemerge3 = var_35;
                        r13_0 = storemerge3;
                        continue;
                    }
                    break;
                  case 0U:
                    {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
        rax_4 = *(uint64_t *)(local_sp_0 + 16UL) + r13_0;
    }
}
