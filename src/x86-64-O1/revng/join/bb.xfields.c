typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_63_ret_type;
struct indirect_placeholder_64_ret_type;
struct indirect_placeholder_65_ret_type;
struct indirect_placeholder_66_ret_type;
struct indirect_placeholder_67_ret_type;
struct indirect_placeholder_63_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_64_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_65_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_66_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_67_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_14(uint64_t param_0);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern struct indirect_placeholder_63_ret_type indirect_placeholder_63(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_64_ret_type indirect_placeholder_64(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_65_ret_type indirect_placeholder_65(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_66_ret_type indirect_placeholder_66(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_67_ret_type indirect_placeholder_67(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
typedef _Bool bool;
void bb_xfields(uint64_t rdi, uint64_t rcx, uint64_t r10) {
    struct indirect_placeholder_63_ret_type var_30;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t local_sp_0;
    uint64_t var_37;
    uint64_t rbx_4;
    uint64_t rbx_0;
    uint64_t rbx_1;
    uint64_t local_sp_2;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t rcx2_0;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t r103_0;
    uint64_t local_sp_1;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t rbp_0;
    uint64_t rbp_1;
    uint64_t local_sp_5;
    uint64_t var_23;
    uint64_t rbx_3;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t local_sp_3;
    uint64_t var_40;
    uint32_t var_9;
    uint32_t var_10;
    uint64_t rbx_2;
    uint64_t local_sp_4;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    struct indirect_placeholder_65_ret_type var_14;
    uint64_t var_15;
    uint64_t rcx2_1;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t r103_1;
    uint64_t local_sp_6;
    uint64_t var_41;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_rbp();
    var_4 = init_r12();
    var_5 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_2;
    var_6 = var_0 + (-40L);
    var_7 = *(uint64_t *)(rdi + 16UL);
    var_8 = (*(uint64_t *)(rdi + 8UL) + var_7) + (-1L);
    rbx_4 = var_7;
    rcx2_0 = rcx;
    r103_0 = r10;
    rbp_1 = var_8;
    local_sp_5 = var_6;
    rbx_3 = var_7;
    rbx_2 = var_7;
    local_sp_4 = var_6;
    rcx2_1 = rcx;
    r103_1 = r10;
    local_sp_6 = var_6;
    if (var_7 == var_8) {
        return;
    }
    var_9 = *(uint32_t *)4285060UL;
    var_10 = var_9;
    if (((int)var_9 < (int)0U) || (var_9 == 10U)) {
        if ((int)var_9 <= (int)4294967295U) {
            while (1U)
                {
                    var_20 = (uint64_t)*(unsigned char *)rbx_3;
                    var_21 = local_sp_5 + (-8L);
                    *(uint64_t *)var_21 = 4206497UL;
                    var_22 = indirect_placeholder_14(var_20);
                    rbx_1 = rbx_3;
                    local_sp_1 = var_21;
                    local_sp_5 = var_21;
                    rbx_4 = var_8;
                    if ((uint64_t)(unsigned char)var_22 == 0UL) {
                        var_23 = rbx_3 + 1UL;
                        rbx_3 = var_23;
                        if (var_8 == var_23) {
                            continue;
                        }
                        loop_state_var = 0U;
                        break;
                    }
                    while (1U)
                        {
                            var_24 = rbx_1 + 1UL;
                            rbp_0 = var_24;
                            local_sp_2 = local_sp_1;
                            local_sp_3 = local_sp_1;
                            if (var_8 != var_24) {
                                loop_state_var = 0U;
                                break;
                            }
                            while (1U)
                                {
                                    var_25 = (uint64_t)*(unsigned char *)rbp_0;
                                    var_26 = local_sp_2 + (-8L);
                                    *(uint64_t *)var_26 = 4206640UL;
                                    var_27 = indirect_placeholder_14(var_25);
                                    local_sp_2 = var_26;
                                    local_sp_3 = var_26;
                                    if ((uint64_t)(unsigned char)var_27 != 0UL) {
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    var_38 = rbp_0 + 1UL;
                                    var_39 = helper_cc_compute_all_wrapper(var_8 - var_38, var_38, var_5, 17U);
                                    rbp_0 = var_38;
                                    rbp_1 = var_38;
                                    if ((var_39 & 64UL) == 0UL) {
                                        continue;
                                    }
                                    loop_state_var = 0U;
                                    break;
                                }
                            switch_state_var = 0;
                            switch (loop_state_var) {
                              case 1U:
                                {
                                    var_28 = rbp_0 - rbx_1;
                                    var_29 = local_sp_2 + (-16L);
                                    *(uint64_t *)var_29 = 4206661UL;
                                    var_30 = indirect_placeholder_63(var_28, rdi, rbx_1, rcx2_0, r103_0);
                                    var_31 = var_30.field_0;
                                    var_32 = var_30.field_1;
                                    local_sp_0 = var_29;
                                    rcx2_0 = var_31;
                                    r103_0 = var_32;
                                    rcx2_1 = var_31;
                                    r103_1 = var_32;
                                    local_sp_6 = var_29;
                                    if (var_8 != rbp_0) {
                                        loop_state_var = 1U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    var_33 = rbp_0 + 1UL;
                                    rbx_0 = var_33;
                                    if (var_8 != var_33) {
                                        loop_state_var = 2U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    while (1U)
                                        {
                                            var_34 = (uint64_t)*(unsigned char *)rbx_0;
                                            var_35 = local_sp_0 + (-8L);
                                            *(uint64_t *)var_35 = 4206683UL;
                                            var_36 = indirect_placeholder_14(var_34);
                                            local_sp_0 = var_35;
                                            rbx_1 = rbx_0;
                                            local_sp_1 = var_35;
                                            local_sp_6 = var_35;
                                            if ((uint64_t)(unsigned char)var_36 != 0UL) {
                                                loop_state_var = 1U;
                                                break;
                                            }
                                            var_37 = rbx_0 + 1UL;
                                            rbx_0 = var_37;
                                            if (var_8 == var_37) {
                                                continue;
                                            }
                                            loop_state_var = 0U;
                                            break;
                                        }
                                    switch_state_var = 0;
                                    switch (loop_state_var) {
                                      case 1U:
                                        {
                                            if (var_8 == rbx_0) {
                                                continue;
                                            }
                                            loop_state_var = 2U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                        break;
                                      case 0U:
                                        {
                                            loop_state_var = 2U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                        break;
                                    }
                                    if (switch_state_var)
                                        break;
                                }
                                break;
                              case 0U:
                                {
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                            }
                            if (switch_state_var)
                                break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 0U:
                        {
                            var_40 = rbp_1 - rbx_1;
                            *(uint64_t *)(local_sp_3 + (-8L)) = 4206615UL;
                            indirect_placeholder_64(var_40, rdi, rbx_1, rcx2_0, r103_0);
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 1U:
                        {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 2U:
                        {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            switch (loop_state_var) {
              case 0U:
                {
                    return;
                }
                break;
              case 1U:
                {
                    break;
                }
                break;
            }
        }
    }
    var_11 = var_8 - rbx_2;
    var_12 = (uint64_t)var_10;
    var_13 = local_sp_4 + (-8L);
    *(uint64_t *)var_13 = 4206553UL;
    var_14 = indirect_placeholder_65(var_11, rbx_2, var_12);
    var_15 = var_14.field_0;
    rbx_4 = rbx_2;
    local_sp_6 = var_13;
    while (var_15 != 0UL)
        {
            var_16 = var_14.field_2;
            var_17 = var_14.field_1;
            var_18 = var_15 - rbx_2;
            var_19 = local_sp_4 + (-16L);
            *(uint64_t *)var_19 = 4206529UL;
            indirect_placeholder_66(var_18, rdi, rbx_2, var_17, var_16);
            var_10 = *(uint32_t *)4285060UL;
            rbx_2 = var_15 + 1UL;
            local_sp_4 = var_19;
            var_11 = var_8 - rbx_2;
            var_12 = (uint64_t)var_10;
            var_13 = local_sp_4 + (-8L);
            *(uint64_t *)var_13 = 4206553UL;
            var_14 = indirect_placeholder_65(var_11, rbx_2, var_12);
            var_15 = var_14.field_0;
            rbx_4 = rbx_2;
            local_sp_6 = var_13;
        }
    rcx2_1 = var_14.field_1;
    r103_1 = var_14.field_2;
    var_41 = var_8 - rbx_4;
    *(uint64_t *)(local_sp_6 + (-8L)) = 4206578UL;
    indirect_placeholder_67(var_41, rdi, rbx_4, rcx2_1, r103_1);
}
