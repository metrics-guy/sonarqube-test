typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_78_ret_type;
struct indirect_placeholder_79_ret_type;
struct indirect_placeholder_80_ret_type;
struct indirect_placeholder_81_ret_type;
struct indirect_placeholder_82_ret_type;
struct indirect_placeholder_78_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_79_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_80_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_81_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_82_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_8(uint64_t param_0);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_14(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint64_t indirect_placeholder_10(void);
extern struct indirect_placeholder_78_ret_type indirect_placeholder_78(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_79_ret_type indirect_placeholder_79(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_80_ret_type indirect_placeholder_80(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_81_ret_type indirect_placeholder_81(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_82_ret_type indirect_placeholder_82(uint64_t param_0, uint64_t param_1);
uint64_t bb_prompt(uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t rcx, uint64_t r8, uint64_t r9) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    unsigned char *var_9;
    unsigned char var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t r12_0;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_68;
    uint64_t r85_0;
    uint64_t r96_0;
    uint64_t rbp_6;
    uint64_t rax_0;
    uint64_t local_sp_0;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t rbp_5;
    uint64_t var_65;
    uint64_t *_pre_phi128;
    uint64_t local_sp_9;
    uint64_t local_sp_1;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t r13_1;
    uint32_t var_44;
    uint64_t cc_src_0;
    uint64_t var_45;
    uint64_t local_sp_7;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t local_sp_8;
    uint64_t local_sp_2;
    uint64_t rbp_0;
    uint64_t r12_1;
    uint64_t r85_1;
    uint64_t r96_1;
    uint64_t var_58;
    uint64_t var_33;
    bool var_34;
    uint64_t cc_src2_0;
    uint32_t var_35;
    uint64_t local_sp_10;
    uint32_t _pre_phi;
    uint64_t local_sp_3;
    uint64_t r13_0;
    uint32_t *var_25;
    uint32_t var_26;
    uint32_t var_36;
    uint64_t var_27;
    uint64_t var_28;
    uint32_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t local_sp_4;
    uint64_t rbp_1;
    uint32_t var_37;
    uint64_t local_sp_5;
    uint64_t rbx_1;
    uint64_t rbp_2;
    uint64_t *_pre127;
    uint64_t local_sp_6;
    uint64_t rbx_2;
    uint64_t rbp_3;
    uint64_t var_46;
    uint64_t var_47;
    struct indirect_placeholder_80_ret_type var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t rbp_4;
    uint64_t var_52;
    uint64_t var_53;
    struct indirect_placeholder_81_ret_type var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_59;
    uint64_t var_60;
    struct indirect_placeholder_82_ret_type var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t *var_40;
    uint64_t var_41;
    uint32_t *_cast;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    unsigned char *var_17;
    unsigned char var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    unsigned char *var_24;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_8 = var_0 + (-216L);
    *(uint32_t *)(var_0 + (-204L)) = (uint32_t)r8;
    var_9 = (unsigned char *)(var_0 + (-205L));
    var_10 = (unsigned char)rdx;
    *var_9 = var_10;
    var_11 = (uint64_t)*(uint32_t *)(rdi + 44UL);
    *(uint64_t *)var_8 = *(uint64_t *)(rsi + 56UL);
    var_12 = *(uint64_t *)(rsi + 48UL);
    rax_0 = 3UL;
    rbp_5 = 1UL;
    cc_src_0 = 3UL;
    cc_src2_0 = var_7;
    rbp_1 = 0UL;
    rbx_1 = 0UL;
    rbp_4 = 21UL;
    if (r9 == 0UL) {
        var_19 = (uint64_t)(uint32_t)rdx;
        var_20 = var_0 + (-200L);
        var_21 = var_0 + (-224L);
        *(uint64_t *)var_21 = 4213196UL;
        indirect_placeholder_8(var_20);
        var_22 = helper_cc_compute_c_wrapper(var_19 + (-1L), 1UL, var_7, 14U);
        var_23 = ((0UL - var_22) & 4UL) ^ 4UL;
        var_24 = (unsigned char *)(var_0 + (-213L));
        *var_24 = (((uint64_t)var_10 == 0UL) ? *var_24 : '\x00');
        local_sp_3 = var_21;
        r13_0 = var_23;
        cc_src2_0 = var_22;
    } else {
        _cast = (uint32_t *)r9;
        *_cast = 2U;
        var_13 = var_0 + (-200L);
        *(uint64_t *)(var_0 + (-224L)) = 4212505UL;
        indirect_placeholder_8(var_13);
        var_14 = ((uint64_t)var_10 == 0UL) ? 0UL : 4UL;
        var_15 = var_0 + (-232L);
        *(uint64_t *)var_15 = 4212531UL;
        var_16 = indirect_placeholder_9(var_11, var_12, rcx);
        var_17 = (unsigned char *)(var_0 + (-221L));
        var_18 = (unsigned char)var_16;
        *var_17 = var_18;
        *_cast = (((uint64_t)var_18 == 0UL) ? 3U : 4U);
        local_sp_3 = var_15;
        r13_0 = var_14;
    }
    local_sp_4 = local_sp_3;
    r13_1 = r13_0;
    if (*(uint64_t *)(rsi + 32UL) == 0UL) {
        return rax_0;
    }
    var_25 = (uint32_t *)(rcx + 4UL);
    var_26 = *var_25;
    var_36 = var_26;
    rax_0 = 2UL;
    if ((uint64_t)(var_26 + (-5)) == 0UL) {
        return;
    }
    rax_0 = 4UL;
    if (*(unsigned char *)rcx == '\x00') {
        local_sp_5 = local_sp_4;
        rbp_2 = rbp_1;
        local_sp_10 = local_sp_4;
        rbp_6 = rbp_1;
        if (var_36 == 3U) {
            return rax_0;
        }
        var_37 = (uint32_t)r13_0;
        _pre_phi = var_37;
        if ((uint64_t)var_37 != 0UL) {
            local_sp_6 = local_sp_5;
            rbx_2 = rbx_1;
            rbp_3 = rbp_2;
            local_sp_7 = local_sp_5;
            if ((uint64_t)(_pre_phi + (-4)) == 0UL) {
                _pre127 = (uint64_t *)local_sp_5;
                _pre_phi128 = _pre127;
                var_46 = *_pre_phi128;
                var_47 = local_sp_6 + (-8L);
                *(uint64_t *)var_47 = 4213069UL;
                var_48 = indirect_placeholder_80(4UL, var_46);
                var_49 = var_48.field_0;
                var_50 = var_48.field_1;
                var_51 = var_48.field_2;
                r12_0 = var_49;
                r85_0 = var_50;
                r96_0 = var_51;
                local_sp_1 = var_47;
                local_sp_2 = var_47;
                rbp_0 = rbp_3;
                r12_1 = var_49;
                r85_1 = var_50;
                r96_1 = var_51;
                if ((int)(uint32_t)rbx_2 >= (int)0U) {
                    var_58 = (uint64_t)(uint32_t)rbp_0;
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4212690UL;
                    indirect_placeholder_79(0UL, 4325683UL, 0UL, var_58, r12_1, r85_1, r96_1);
                    return rax_0;
                }
                var_66 = local_sp_1 + 16UL;
                *(uint64_t *)(local_sp_1 + (-8L)) = 4213101UL;
                var_67 = indirect_placeholder_14(var_66, var_11, var_12, 256UL);
                if ((uint64_t)(uint32_t)var_67 != 0UL) {
                    *(uint64_t *)(local_sp_1 + (-16L)) = 4212976UL;
                    indirect_placeholder_1();
                    var_68 = (uint64_t)*(uint32_t *)var_67;
                    *(uint64_t *)(local_sp_1 + (-24L)) = 4213001UL;
                    indirect_placeholder_78(0UL, 4325683UL, 0UL, var_68, r12_0, r85_0, r96_0);
                    return rax_0;
                }
                var_69 = local_sp_1 + 8UL;
                *(uint64_t *)(local_sp_1 + (-16L)) = 4213119UL;
                indirect_placeholder_8(var_69);
                var_70 = local_sp_1 + (-24L);
                *(uint64_t *)var_70 = 4213165UL;
                indirect_placeholder_1();
                local_sp_0 = var_70;
                *(uint64_t *)(local_sp_0 + (-8L)) = 4213170UL;
                var_71 = indirect_placeholder_10();
                var_72 = ((uint64_t)(unsigned char)var_71 == 0UL) ? 3UL : 2UL;
                rax_0 = var_72;
                return rax_0;
            }
            local_sp_8 = local_sp_7;
            local_sp_9 = local_sp_7;
            if (*(unsigned char *)(rcx + 9UL) != '\x00') {
                if (*(unsigned char *)(rcx + 10UL) == '\x00') {
                    var_52 = *(uint64_t *)local_sp_8;
                    var_53 = local_sp_8 + (-8L);
                    *(uint64_t *)var_53 = 4212662UL;
                    var_54 = indirect_placeholder_81(4UL, var_52);
                    var_55 = var_54.field_0;
                    var_56 = var_54.field_1;
                    var_57 = var_54.field_2;
                    local_sp_2 = var_53;
                    rbp_0 = rbp_4;
                    r12_1 = var_55;
                    r85_1 = var_56;
                    r96_1 = var_57;
                    var_58 = (uint64_t)(uint32_t)rbp_0;
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4212690UL;
                    indirect_placeholder_79(0UL, 4325683UL, 0UL, var_58, r12_1, r85_1, r96_1);
                    return rax_0;
                }
                if (*(unsigned char *)(local_sp_7 + 11UL) == '\x00') {
                    var_52 = *(uint64_t *)local_sp_8;
                    var_53 = local_sp_8 + (-8L);
                    *(uint64_t *)var_53 = 4212662UL;
                    var_54 = indirect_placeholder_81(4UL, var_52);
                    var_55 = var_54.field_0;
                    var_56 = var_54.field_1;
                    var_57 = var_54.field_2;
                    local_sp_2 = var_53;
                    rbp_0 = rbp_4;
                    r12_1 = var_55;
                    r85_1 = var_56;
                    r96_1 = var_57;
                    var_58 = (uint64_t)(uint32_t)rbp_0;
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4212690UL;
                    indirect_placeholder_79(0UL, 4325683UL, 0UL, var_58, r12_1, r85_1, r96_1);
                    return rax_0;
                }
            }
            var_59 = *(uint64_t *)local_sp_9;
            var_60 = local_sp_9 + (-8L);
            *(uint64_t *)var_60 = 4212887UL;
            var_61 = indirect_placeholder_82(4UL, var_59);
            var_62 = var_61.field_0;
            var_63 = var_61.field_1;
            var_64 = var_61.field_2;
            local_sp_1 = var_60;
            r12_0 = var_62;
            r85_0 = var_63;
            r96_0 = var_64;
            if ((rbp_5 & (*(uint32_t *)(local_sp_9 + 4UL) == 2U)) != 0UL) {
                if (*(unsigned char *)(local_sp_9 + 3UL) != '\x00') {
                    var_65 = local_sp_9 + (-16L);
                    *(uint64_t *)var_65 = 4212966UL;
                    indirect_placeholder_1();
                    local_sp_0 = var_65;
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4213170UL;
                    var_71 = indirect_placeholder_10();
                    var_72 = ((uint64_t)(unsigned char)var_71 == 0UL) ? 3UL : 2UL;
                    rax_0 = var_72;
                    return rax_0;
                }
            }
            var_66 = local_sp_1 + 16UL;
            *(uint64_t *)(local_sp_1 + (-8L)) = 4213101UL;
            var_67 = indirect_placeholder_14(var_66, var_11, var_12, 256UL);
            if ((uint64_t)(uint32_t)var_67 != 0UL) {
                *(uint64_t *)(local_sp_1 + (-16L)) = 4212976UL;
                indirect_placeholder_1();
                var_68 = (uint64_t)*(uint32_t *)var_67;
                *(uint64_t *)(local_sp_1 + (-24L)) = 4213001UL;
                indirect_placeholder_78(0UL, 4325683UL, 0UL, var_68, r12_0, r85_0, r96_0);
                return rax_0;
            }
            var_69 = local_sp_1 + 8UL;
            *(uint64_t *)(local_sp_1 + (-16L)) = 4213119UL;
            indirect_placeholder_8(var_69);
            var_70 = local_sp_1 + (-24L);
            *(uint64_t *)var_70 = 4213165UL;
            indirect_placeholder_1();
            local_sp_0 = var_70;
            *(uint64_t *)(local_sp_0 + (-8L)) = 4213170UL;
            var_71 = indirect_placeholder_10();
            var_72 = ((uint64_t)(unsigned char)var_71 == 0UL) ? 3UL : 2UL;
            rax_0 = var_72;
            return rax_0;
        }
        var_38 = local_sp_10 + 16UL;
        var_39 = local_sp_10 + (-8L);
        var_40 = (uint64_t *)var_39;
        *var_40 = 4212761UL;
        var_41 = indirect_placeholder_14(var_38, var_11, var_12, 256UL);
        rbp_5 = 0UL;
        _pre_phi128 = var_40;
        local_sp_9 = var_39;
        local_sp_7 = var_39;
        local_sp_6 = var_39;
        rbp_3 = rbp_6;
        if ((uint64_t)(uint32_t)var_41 != 0UL) {
            var_42 = local_sp_10 + (-16L);
            *(uint64_t *)var_42 = 4212841UL;
            indirect_placeholder_1();
            var_43 = (uint64_t)*(uint32_t *)var_41;
            local_sp_8 = var_42;
            rbp_4 = var_43;
            var_52 = *(uint64_t *)local_sp_8;
            var_53 = local_sp_8 + (-8L);
            *(uint64_t *)var_53 = 4212662UL;
            var_54 = indirect_placeholder_81(4UL, var_52);
            var_55 = var_54.field_0;
            var_56 = var_54.field_1;
            var_57 = var_54.field_2;
            local_sp_2 = var_53;
            rbp_0 = rbp_4;
            r12_1 = var_55;
            r85_1 = var_56;
            r96_1 = var_57;
            var_58 = (uint64_t)(uint32_t)rbp_0;
            *(uint64_t *)(local_sp_2 + (-8L)) = 4212690UL;
            indirect_placeholder_79(0UL, 4325683UL, 0UL, var_58, r12_1, r85_1, r96_1);
            return rax_0;
        }
        var_44 = (uint32_t)((uint16_t)*(uint32_t *)(local_sp_10 + 32UL) & (unsigned short)61440U);
        if ((uint64_t)((var_44 + (-40960)) & (-4096)) == 0UL) {
            if (*var_25 == 3U) {
                return rax_0;
            }
        }
        var_45 = (uint64_t)(uint32_t)r13_1;
        rbx_2 = var_45;
        if ((uint64_t)((var_44 + (-16384)) & (-4096)) != 0UL) {
            var_46 = *_pre_phi128;
            var_47 = local_sp_6 + (-8L);
            *(uint64_t *)var_47 = 4213069UL;
            var_48 = indirect_placeholder_80(4UL, var_46);
            var_49 = var_48.field_0;
            var_50 = var_48.field_1;
            var_51 = var_48.field_2;
            r12_0 = var_49;
            r85_0 = var_50;
            r96_0 = var_51;
            local_sp_1 = var_47;
            local_sp_2 = var_47;
            rbp_0 = rbp_3;
            r12_1 = var_49;
            r85_1 = var_50;
            r96_1 = var_51;
            if ((int)(uint32_t)rbx_2 >= (int)0U) {
                var_58 = (uint64_t)(uint32_t)rbp_0;
                *(uint64_t *)(local_sp_2 + (-8L)) = 4212690UL;
                indirect_placeholder_79(0UL, 4325683UL, 0UL, var_58, r12_1, r85_1, r96_1);
                return rax_0;
            }
            var_66 = local_sp_1 + 16UL;
            *(uint64_t *)(local_sp_1 + (-8L)) = 4213101UL;
            var_67 = indirect_placeholder_14(var_66, var_11, var_12, 256UL);
            if ((uint64_t)(uint32_t)var_67 != 0UL) {
                *(uint64_t *)(local_sp_1 + (-16L)) = 4212976UL;
                indirect_placeholder_1();
                var_68 = (uint64_t)*(uint32_t *)var_67;
                *(uint64_t *)(local_sp_1 + (-24L)) = 4213001UL;
                indirect_placeholder_78(0UL, 4325683UL, 0UL, var_68, r12_0, r85_0, r96_0);
                return rax_0;
            }
            var_69 = local_sp_1 + 8UL;
            *(uint64_t *)(local_sp_1 + (-16L)) = 4213119UL;
            indirect_placeholder_8(var_69);
            var_70 = local_sp_1 + (-24L);
            *(uint64_t *)var_70 = 4213165UL;
            indirect_placeholder_1();
            local_sp_0 = var_70;
            *(uint64_t *)(local_sp_0 + (-8L)) = 4213170UL;
            var_71 = indirect_placeholder_10();
            var_72 = ((uint64_t)(unsigned char)var_71 == 0UL) ? 3UL : 2UL;
            rax_0 = var_72;
            return rax_0;
        }
        local_sp_8 = local_sp_7;
        local_sp_9 = local_sp_7;
        if (*(unsigned char *)(rcx + 9UL) != '\x00') {
            if (*(unsigned char *)(rcx + 10UL) != '\x00') {
                var_52 = *(uint64_t *)local_sp_8;
                var_53 = local_sp_8 + (-8L);
                *(uint64_t *)var_53 = 4212662UL;
                var_54 = indirect_placeholder_81(4UL, var_52);
                var_55 = var_54.field_0;
                var_56 = var_54.field_1;
                var_57 = var_54.field_2;
                local_sp_2 = var_53;
                rbp_0 = rbp_4;
                r12_1 = var_55;
                r85_1 = var_56;
                r96_1 = var_57;
                var_58 = (uint64_t)(uint32_t)rbp_0;
                *(uint64_t *)(local_sp_2 + (-8L)) = 4212690UL;
                indirect_placeholder_79(0UL, 4325683UL, 0UL, var_58, r12_1, r85_1, r96_1);
                return rax_0;
            }
            if (*(unsigned char *)(local_sp_7 + 11UL) == '\x00') {
                var_52 = *(uint64_t *)local_sp_8;
                var_53 = local_sp_8 + (-8L);
                *(uint64_t *)var_53 = 4212662UL;
                var_54 = indirect_placeholder_81(4UL, var_52);
                var_55 = var_54.field_0;
                var_56 = var_54.field_1;
                var_57 = var_54.field_2;
                local_sp_2 = var_53;
                rbp_0 = rbp_4;
                r12_1 = var_55;
                r85_1 = var_56;
                r96_1 = var_57;
                var_58 = (uint64_t)(uint32_t)rbp_0;
                *(uint64_t *)(local_sp_2 + (-8L)) = 4212690UL;
                indirect_placeholder_79(0UL, 4325683UL, 0UL, var_58, r12_1, r85_1, r96_1);
                return rax_0;
            }
        }
        var_59 = *(uint64_t *)local_sp_9;
        var_60 = local_sp_9 + (-8L);
        *(uint64_t *)var_60 = 4212887UL;
        var_61 = indirect_placeholder_82(4UL, var_59);
        var_62 = var_61.field_0;
        var_63 = var_61.field_1;
        var_64 = var_61.field_2;
        local_sp_1 = var_60;
        r12_0 = var_62;
        r85_0 = var_63;
        r96_0 = var_64;
        if ((rbp_5 & (*(uint32_t *)(local_sp_9 + 4UL) == 2U)) != 0UL) {
            if (*(unsigned char *)(local_sp_9 + 3UL) == '\x00') {
                var_65 = local_sp_9 + (-16L);
                *(uint64_t *)var_65 = 4212966UL;
                indirect_placeholder_1();
                local_sp_0 = var_65;
                *(uint64_t *)(local_sp_0 + (-8L)) = 4213170UL;
                var_71 = indirect_placeholder_10();
                var_72 = ((uint64_t)(unsigned char)var_71 == 0UL) ? 3UL : 2UL;
                rax_0 = var_72;
                return rax_0;
            }
        }
        var_66 = local_sp_1 + 16UL;
        *(uint64_t *)(local_sp_1 + (-8L)) = 4213101UL;
        var_67 = indirect_placeholder_14(var_66, var_11, var_12, 256UL);
        if ((uint64_t)(uint32_t)var_67 == 0UL) {
            *(uint64_t *)(local_sp_1 + (-16L)) = 4212976UL;
            indirect_placeholder_1();
            var_68 = (uint64_t)*(uint32_t *)var_67;
            *(uint64_t *)(local_sp_1 + (-24L)) = 4213001UL;
            indirect_placeholder_78(0UL, 4325683UL, 0UL, var_68, r12_0, r85_0, r96_0);
            return rax_0;
        }
        var_69 = local_sp_1 + 8UL;
        *(uint64_t *)(local_sp_1 + (-16L)) = 4213119UL;
        indirect_placeholder_8(var_69);
        var_70 = local_sp_1 + (-24L);
        *(uint64_t *)var_70 = 4213165UL;
        indirect_placeholder_1();
        local_sp_0 = var_70;
        *(uint64_t *)(local_sp_0 + (-8L)) = 4213170UL;
        var_71 = indirect_placeholder_10();
        var_72 = ((uint64_t)(unsigned char)var_71 == 0UL) ? 3UL : 2UL;
        rax_0 = var_72;
    } else {
        if ((uint64_t)(var_26 + (-3)) != 0UL) {
            cc_src_0 = 0UL;
            if (*(unsigned char *)(rcx + 24UL) == '\x00') {
                return rax_0;
            }
        }
        var_27 = local_sp_3 + 16UL;
        *(uint64_t *)(local_sp_3 + (-8L)) = 4212620UL;
        var_28 = indirect_placeholder_14(2UL, var_27, var_11, var_12);
        var_29 = (uint32_t)var_28;
        var_30 = (uint64_t)var_29;
        var_31 = local_sp_3 + (-16L);
        *(uint64_t *)var_31 = 4212627UL;
        indirect_placeholder_1();
        var_32 = (uint64_t)*(uint32_t *)var_28;
        rbp_6 = var_32;
        r13_1 = var_30;
        local_sp_8 = var_31;
        local_sp_10 = var_31;
        local_sp_4 = var_31;
        rbp_1 = var_32;
        local_sp_5 = var_31;
        rbx_1 = var_30;
        rbp_2 = var_32;
        rbp_4 = var_32;
        if (var_30 != 0UL) {
            var_33 = helper_cc_compute_all_wrapper(var_30, cc_src_0, cc_src2_0, 24U);
            var_34 = ((signed char)(unsigned char)var_33 >= '\x00');
            var_35 = (uint32_t)r13_0;
            _pre_phi = var_35;
            if (!(var_34 && ((uint64_t)var_35 == 0UL))) {
                if ((int)var_29 <= (int)4294967295U) {
                    var_52 = *(uint64_t *)local_sp_8;
                    var_53 = local_sp_8 + (-8L);
                    *(uint64_t *)var_53 = 4212662UL;
                    var_54 = indirect_placeholder_81(4UL, var_52);
                    var_55 = var_54.field_0;
                    var_56 = var_54.field_1;
                    var_57 = var_54.field_2;
                    local_sp_2 = var_53;
                    rbp_0 = rbp_4;
                    r12_1 = var_55;
                    r85_1 = var_56;
                    r96_1 = var_57;
                    var_58 = (uint64_t)(uint32_t)rbp_0;
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4212690UL;
                    indirect_placeholder_79(0UL, 4325683UL, 0UL, var_58, r12_1, r85_1, r96_1);
                    return rax_0;
                }
                local_sp_6 = local_sp_5;
                rbx_2 = rbx_1;
                rbp_3 = rbp_2;
                local_sp_7 = local_sp_5;
                if ((uint64_t)(_pre_phi + (-4)) == 0UL) {
                    _pre127 = (uint64_t *)local_sp_5;
                    _pre_phi128 = _pre127;
                    var_46 = *_pre_phi128;
                    var_47 = local_sp_6 + (-8L);
                    *(uint64_t *)var_47 = 4213069UL;
                    var_48 = indirect_placeholder_80(4UL, var_46);
                    var_49 = var_48.field_0;
                    var_50 = var_48.field_1;
                    var_51 = var_48.field_2;
                    r12_0 = var_49;
                    r85_0 = var_50;
                    r96_0 = var_51;
                    local_sp_1 = var_47;
                    local_sp_2 = var_47;
                    rbp_0 = rbp_3;
                    r12_1 = var_49;
                    r85_1 = var_50;
                    r96_1 = var_51;
                    if ((int)(uint32_t)rbx_2 >= (int)0U) {
                        var_58 = (uint64_t)(uint32_t)rbp_0;
                        *(uint64_t *)(local_sp_2 + (-8L)) = 4212690UL;
                        indirect_placeholder_79(0UL, 4325683UL, 0UL, var_58, r12_1, r85_1, r96_1);
                        return rax_0;
                    }
                    var_66 = local_sp_1 + 16UL;
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4213101UL;
                    var_67 = indirect_placeholder_14(var_66, var_11, var_12, 256UL);
                    if ((uint64_t)(uint32_t)var_67 != 0UL) {
                        *(uint64_t *)(local_sp_1 + (-16L)) = 4212976UL;
                        indirect_placeholder_1();
                        var_68 = (uint64_t)*(uint32_t *)var_67;
                        *(uint64_t *)(local_sp_1 + (-24L)) = 4213001UL;
                        indirect_placeholder_78(0UL, 4325683UL, 0UL, var_68, r12_0, r85_0, r96_0);
                        return rax_0;
                    }
                    var_69 = local_sp_1 + 8UL;
                    *(uint64_t *)(local_sp_1 + (-16L)) = 4213119UL;
                    indirect_placeholder_8(var_69);
                    var_70 = local_sp_1 + (-24L);
                    *(uint64_t *)var_70 = 4213165UL;
                    indirect_placeholder_1();
                    local_sp_0 = var_70;
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4213170UL;
                    var_71 = indirect_placeholder_10();
                    var_72 = ((uint64_t)(unsigned char)var_71 == 0UL) ? 3UL : 2UL;
                    rax_0 = var_72;
                    return rax_0;
                }
                local_sp_8 = local_sp_7;
                local_sp_9 = local_sp_7;
                if (*(unsigned char *)(rcx + 9UL) != '\x00') {
                    if (*(unsigned char *)(rcx + 10UL) == '\x00') {
                        var_52 = *(uint64_t *)local_sp_8;
                        var_53 = local_sp_8 + (-8L);
                        *(uint64_t *)var_53 = 4212662UL;
                        var_54 = indirect_placeholder_81(4UL, var_52);
                        var_55 = var_54.field_0;
                        var_56 = var_54.field_1;
                        var_57 = var_54.field_2;
                        local_sp_2 = var_53;
                        rbp_0 = rbp_4;
                        r12_1 = var_55;
                        r85_1 = var_56;
                        r96_1 = var_57;
                        var_58 = (uint64_t)(uint32_t)rbp_0;
                        *(uint64_t *)(local_sp_2 + (-8L)) = 4212690UL;
                        indirect_placeholder_79(0UL, 4325683UL, 0UL, var_58, r12_1, r85_1, r96_1);
                        return rax_0;
                    }
                    if (*(unsigned char *)(local_sp_7 + 11UL) == '\x00') {
                        var_52 = *(uint64_t *)local_sp_8;
                        var_53 = local_sp_8 + (-8L);
                        *(uint64_t *)var_53 = 4212662UL;
                        var_54 = indirect_placeholder_81(4UL, var_52);
                        var_55 = var_54.field_0;
                        var_56 = var_54.field_1;
                        var_57 = var_54.field_2;
                        local_sp_2 = var_53;
                        rbp_0 = rbp_4;
                        r12_1 = var_55;
                        r85_1 = var_56;
                        r96_1 = var_57;
                        var_58 = (uint64_t)(uint32_t)rbp_0;
                        *(uint64_t *)(local_sp_2 + (-8L)) = 4212690UL;
                        indirect_placeholder_79(0UL, 4325683UL, 0UL, var_58, r12_1, r85_1, r96_1);
                        return rax_0;
                    }
                }
                var_59 = *(uint64_t *)local_sp_9;
                var_60 = local_sp_9 + (-8L);
                *(uint64_t *)var_60 = 4212887UL;
                var_61 = indirect_placeholder_82(4UL, var_59);
                var_62 = var_61.field_0;
                var_63 = var_61.field_1;
                var_64 = var_61.field_2;
                local_sp_1 = var_60;
                r12_0 = var_62;
                r85_0 = var_63;
                r96_0 = var_64;
                if ((rbp_5 & (*(uint32_t *)(local_sp_9 + 4UL) == 2U)) != 0UL) {
                    if (*(unsigned char *)(local_sp_9 + 3UL) != '\x00') {
                        var_65 = local_sp_9 + (-16L);
                        *(uint64_t *)var_65 = 4212966UL;
                        indirect_placeholder_1();
                        local_sp_0 = var_65;
                        *(uint64_t *)(local_sp_0 + (-8L)) = 4213170UL;
                        var_71 = indirect_placeholder_10();
                        var_72 = ((uint64_t)(unsigned char)var_71 == 0UL) ? 3UL : 2UL;
                        rax_0 = var_72;
                        return rax_0;
                    }
                }
                var_66 = local_sp_1 + 16UL;
                *(uint64_t *)(local_sp_1 + (-8L)) = 4213101UL;
                var_67 = indirect_placeholder_14(var_66, var_11, var_12, 256UL);
                if ((uint64_t)(uint32_t)var_67 != 0UL) {
                    *(uint64_t *)(local_sp_1 + (-16L)) = 4212976UL;
                    indirect_placeholder_1();
                    var_68 = (uint64_t)*(uint32_t *)var_67;
                    *(uint64_t *)(local_sp_1 + (-24L)) = 4213001UL;
                    indirect_placeholder_78(0UL, 4325683UL, 0UL, var_68, r12_0, r85_0, r96_0);
                    return rax_0;
                }
                var_69 = local_sp_1 + 8UL;
                *(uint64_t *)(local_sp_1 + (-16L)) = 4213119UL;
                indirect_placeholder_8(var_69);
                var_70 = local_sp_1 + (-24L);
                *(uint64_t *)var_70 = 4213165UL;
                indirect_placeholder_1();
                local_sp_0 = var_70;
                *(uint64_t *)(local_sp_0 + (-8L)) = 4213170UL;
                var_71 = indirect_placeholder_10();
                var_72 = ((uint64_t)(unsigned char)var_71 == 0UL) ? 3UL : 2UL;
                rax_0 = var_72;
                return rax_0;
            }
            var_38 = local_sp_10 + 16UL;
            var_39 = local_sp_10 + (-8L);
            var_40 = (uint64_t *)var_39;
            *var_40 = 4212761UL;
            var_41 = indirect_placeholder_14(var_38, var_11, var_12, 256UL);
            rbp_5 = 0UL;
            _pre_phi128 = var_40;
            local_sp_9 = var_39;
            local_sp_7 = var_39;
            local_sp_6 = var_39;
            rbp_3 = rbp_6;
            if ((uint64_t)(uint32_t)var_41 != 0UL) {
                var_42 = local_sp_10 + (-16L);
                *(uint64_t *)var_42 = 4212841UL;
                indirect_placeholder_1();
                var_43 = (uint64_t)*(uint32_t *)var_41;
                local_sp_8 = var_42;
                rbp_4 = var_43;
                var_52 = *(uint64_t *)local_sp_8;
                var_53 = local_sp_8 + (-8L);
                *(uint64_t *)var_53 = 4212662UL;
                var_54 = indirect_placeholder_81(4UL, var_52);
                var_55 = var_54.field_0;
                var_56 = var_54.field_1;
                var_57 = var_54.field_2;
                local_sp_2 = var_53;
                rbp_0 = rbp_4;
                r12_1 = var_55;
                r85_1 = var_56;
                r96_1 = var_57;
                var_58 = (uint64_t)(uint32_t)rbp_0;
                *(uint64_t *)(local_sp_2 + (-8L)) = 4212690UL;
                indirect_placeholder_79(0UL, 4325683UL, 0UL, var_58, r12_1, r85_1, r96_1);
                return rax_0;
            }
            var_44 = (uint32_t)((uint16_t)*(uint32_t *)(local_sp_10 + 32UL) & (unsigned short)61440U);
            if ((uint64_t)((var_44 + (-40960)) & (-4096)) == 0UL) {
                if (*var_25 == 3U) {
                    return rax_0;
                }
            }
            var_45 = (uint64_t)(uint32_t)r13_1;
            rbx_2 = var_45;
            if ((uint64_t)((var_44 + (-16384)) & (-4096)) != 0UL) {
                var_46 = *_pre_phi128;
                var_47 = local_sp_6 + (-8L);
                *(uint64_t *)var_47 = 4213069UL;
                var_48 = indirect_placeholder_80(4UL, var_46);
                var_49 = var_48.field_0;
                var_50 = var_48.field_1;
                var_51 = var_48.field_2;
                r12_0 = var_49;
                r85_0 = var_50;
                r96_0 = var_51;
                local_sp_1 = var_47;
                local_sp_2 = var_47;
                rbp_0 = rbp_3;
                r12_1 = var_49;
                r85_1 = var_50;
                r96_1 = var_51;
                if ((int)(uint32_t)rbx_2 >= (int)0U) {
                    var_58 = (uint64_t)(uint32_t)rbp_0;
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4212690UL;
                    indirect_placeholder_79(0UL, 4325683UL, 0UL, var_58, r12_1, r85_1, r96_1);
                    return rax_0;
                }
                var_66 = local_sp_1 + 16UL;
                *(uint64_t *)(local_sp_1 + (-8L)) = 4213101UL;
                var_67 = indirect_placeholder_14(var_66, var_11, var_12, 256UL);
                if ((uint64_t)(uint32_t)var_67 != 0UL) {
                    *(uint64_t *)(local_sp_1 + (-16L)) = 4212976UL;
                    indirect_placeholder_1();
                    var_68 = (uint64_t)*(uint32_t *)var_67;
                    *(uint64_t *)(local_sp_1 + (-24L)) = 4213001UL;
                    indirect_placeholder_78(0UL, 4325683UL, 0UL, var_68, r12_0, r85_0, r96_0);
                    return rax_0;
                }
                var_69 = local_sp_1 + 8UL;
                *(uint64_t *)(local_sp_1 + (-16L)) = 4213119UL;
                indirect_placeholder_8(var_69);
                var_70 = local_sp_1 + (-24L);
                *(uint64_t *)var_70 = 4213165UL;
                indirect_placeholder_1();
                local_sp_0 = var_70;
                *(uint64_t *)(local_sp_0 + (-8L)) = 4213170UL;
                var_71 = indirect_placeholder_10();
                var_72 = ((uint64_t)(unsigned char)var_71 == 0UL) ? 3UL : 2UL;
                rax_0 = var_72;
                return rax_0;
            }
            local_sp_8 = local_sp_7;
            local_sp_9 = local_sp_7;
            if (*(unsigned char *)(rcx + 9UL) != '\x00') {
                if (*(unsigned char *)(rcx + 10UL) != '\x00') {
                    var_52 = *(uint64_t *)local_sp_8;
                    var_53 = local_sp_8 + (-8L);
                    *(uint64_t *)var_53 = 4212662UL;
                    var_54 = indirect_placeholder_81(4UL, var_52);
                    var_55 = var_54.field_0;
                    var_56 = var_54.field_1;
                    var_57 = var_54.field_2;
                    local_sp_2 = var_53;
                    rbp_0 = rbp_4;
                    r12_1 = var_55;
                    r85_1 = var_56;
                    r96_1 = var_57;
                    var_58 = (uint64_t)(uint32_t)rbp_0;
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4212690UL;
                    indirect_placeholder_79(0UL, 4325683UL, 0UL, var_58, r12_1, r85_1, r96_1);
                    return rax_0;
                }
                if (*(unsigned char *)(local_sp_7 + 11UL) == '\x00') {
                    var_52 = *(uint64_t *)local_sp_8;
                    var_53 = local_sp_8 + (-8L);
                    *(uint64_t *)var_53 = 4212662UL;
                    var_54 = indirect_placeholder_81(4UL, var_52);
                    var_55 = var_54.field_0;
                    var_56 = var_54.field_1;
                    var_57 = var_54.field_2;
                    local_sp_2 = var_53;
                    rbp_0 = rbp_4;
                    r12_1 = var_55;
                    r85_1 = var_56;
                    r96_1 = var_57;
                    var_58 = (uint64_t)(uint32_t)rbp_0;
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4212690UL;
                    indirect_placeholder_79(0UL, 4325683UL, 0UL, var_58, r12_1, r85_1, r96_1);
                    return rax_0;
                }
            }
            var_59 = *(uint64_t *)local_sp_9;
            var_60 = local_sp_9 + (-8L);
            *(uint64_t *)var_60 = 4212887UL;
            var_61 = indirect_placeholder_82(4UL, var_59);
            var_62 = var_61.field_0;
            var_63 = var_61.field_1;
            var_64 = var_61.field_2;
            local_sp_1 = var_60;
            r12_0 = var_62;
            r85_0 = var_63;
            r96_0 = var_64;
            if ((rbp_5 & (*(uint32_t *)(local_sp_9 + 4UL) == 2U)) != 0UL) {
                if (*(unsigned char *)(local_sp_9 + 3UL) == '\x00') {
                    var_65 = local_sp_9 + (-16L);
                    *(uint64_t *)var_65 = 4212966UL;
                    indirect_placeholder_1();
                    local_sp_0 = var_65;
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4213170UL;
                    var_71 = indirect_placeholder_10();
                    var_72 = ((uint64_t)(unsigned char)var_71 == 0UL) ? 3UL : 2UL;
                    rax_0 = var_72;
                    return rax_0;
                }
            }
            var_66 = local_sp_1 + 16UL;
            *(uint64_t *)(local_sp_1 + (-8L)) = 4213101UL;
            var_67 = indirect_placeholder_14(var_66, var_11, var_12, 256UL);
            if ((uint64_t)(uint32_t)var_67 == 0UL) {
                *(uint64_t *)(local_sp_1 + (-16L)) = 4212976UL;
                indirect_placeholder_1();
                var_68 = (uint64_t)*(uint32_t *)var_67;
                *(uint64_t *)(local_sp_1 + (-24L)) = 4213001UL;
                indirect_placeholder_78(0UL, 4325683UL, 0UL, var_68, r12_0, r85_0, r96_0);
                return rax_0;
            }
            var_69 = local_sp_1 + 8UL;
            *(uint64_t *)(local_sp_1 + (-16L)) = 4213119UL;
            indirect_placeholder_8(var_69);
            var_70 = local_sp_1 + (-24L);
            *(uint64_t *)var_70 = 4213165UL;
            indirect_placeholder_1();
            local_sp_0 = var_70;
            *(uint64_t *)(local_sp_0 + (-8L)) = 4213170UL;
            var_71 = indirect_placeholder_10();
            var_72 = ((uint64_t)(unsigned char)var_71 == 0UL) ? 3UL : 2UL;
            rax_0 = var_72;
            return rax_0;
        }
        var_36 = *var_25;
    }
}
