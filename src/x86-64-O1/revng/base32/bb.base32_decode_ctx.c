typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
uint64_t bb_base32_decode_ctx(uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t rcx, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t *_cast;
    bool var_8;
    bool var_9;
    bool r13_0;
    uint64_t rdi2_0;
    uint64_t local_sp_4;
    uint64_t local_sp_0_be;
    uint64_t var_10;
    uint64_t local_sp_0;
    uint64_t *var_18;
    uint64_t local_sp_1;
    bool storemerge;
    uint64_t rbx_0;
    uint64_t var_37;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t *var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t local_sp_2;
    uint64_t var_19;
    uint64_t *var_20;
    uint64_t var_21;
    uint64_t *_pre_phi87;
    uint64_t var_22;
    uint64_t *var_23;
    uint64_t var_24;
    uint64_t *var_25;
    uint64_t var_26;
    uint64_t *var_27;
    uint64_t var_28;
    uint64_t _pre_phi80;
    uint64_t *_pre_phi83;
    uint64_t rbx_1;
    uint64_t local_sp_3;
    uint64_t var_36;
    uint64_t *_pre_phi79;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t rbx_2;
    uint64_t local_sp_5;
    uint64_t *var_29;
    uint64_t var_30;
    uint64_t *var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_41;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_7 = var_0 + (-104L);
    *(uint64_t *)(var_0 + (-80L)) = rsi;
    *(uint64_t *)(var_0 + (-88L)) = rdx;
    *(uint64_t *)(var_0 + (-96L)) = rcx;
    _cast = (uint64_t *)r8;
    *(uint64_t *)(var_0 + (-64L)) = *_cast;
    var_8 = (rdi != 0UL);
    var_9 = (rdi == 0UL);
    r13_0 = 1;
    storemerge = 0;
    local_sp_0 = var_7;
    if (var_9) {
        r13_0 = (*(uint32_t *)rdi == 0U);
        storemerge = (rdx == 0UL);
    }
    while (1U)
        {
            local_sp_1 = local_sp_0;
            local_sp_2 = local_sp_0;
            if (!((r13_0 ^ 1) || storemerge)) {
                var_10 = local_sp_1 + 40UL;
                var_11 = *(uint64_t *)var_10;
                var_12 = local_sp_1 + 8UL;
                var_13 = (uint64_t *)(local_sp_1 + 16UL);
                var_14 = *var_13;
                var_15 = *(uint64_t *)(local_sp_1 + 24UL);
                var_16 = local_sp_1 + (-8L);
                *(uint64_t *)var_16 = 4208401UL;
                var_17 = indirect_placeholder_4(var_12, var_15, var_14, var_10);
                local_sp_1 = var_16;
                rbx_0 = var_11;
                local_sp_2 = var_16;
                while ((uint64_t)(unsigned char)var_17 != 0UL)
                    {
                        *var_13 = (*var_13 + 8UL);
                        var_18 = (uint64_t *)var_12;
                        *var_18 = (*var_18 + (-8L));
                        var_10 = local_sp_1 + 40UL;
                        var_11 = *(uint64_t *)var_10;
                        var_12 = local_sp_1 + 8UL;
                        var_13 = (uint64_t *)(local_sp_1 + 16UL);
                        var_14 = *var_13;
                        var_15 = *(uint64_t *)(local_sp_1 + 24UL);
                        var_16 = local_sp_1 + (-8L);
                        *(uint64_t *)var_16 = 4208401UL;
                        var_17 = indirect_placeholder_4(var_12, var_15, var_14, var_10);
                        local_sp_1 = var_16;
                        rbx_0 = var_11;
                        local_sp_2 = var_16;
                    }
            }
            rbx_0 = *(uint64_t *)(local_sp_0 + 40UL);
            var_19 = local_sp_2 + 16UL;
            var_20 = (uint64_t *)var_19;
            var_21 = *var_20;
            local_sp_4 = local_sp_2;
            local_sp_0_be = local_sp_2;
            _pre_phi87 = var_20;
            _pre_phi83 = var_20;
            local_sp_3 = local_sp_2;
            _pre_phi79 = var_20;
            local_sp_5 = local_sp_2;
            if (var_21 == 0UL) {
                if (!storemerge) {
                    loop_state_var = 0U;
                    break;
                }
                var_29 = (uint64_t *)(local_sp_2 + 40UL);
                var_30 = *var_29 - rbx_0;
                var_31 = (uint64_t *)(local_sp_2 + 8UL);
                *var_31 = (*var_31 + var_30);
                *var_29 = rbx_0;
                var_32 = local_sp_2 + 24UL;
                var_33 = *(uint64_t *)var_32;
                _pre_phi80 = var_32;
                rbx_2 = var_33;
                if (!var_9) {
                    loop_state_var = 1U;
                    break;
                }
                var_34 = local_sp_2 + (-8L);
                *(uint64_t *)var_34 = 4208536UL;
                var_35 = indirect_placeholder_4(rbx_2, rdi, _pre_phi80, var_19);
                _pre_phi83 = (uint64_t *)(var_34 + 16UL);
                rbx_1 = rbx_2;
                rdi2_0 = var_35;
                local_sp_3 = var_34;
            } else {
                var_22 = local_sp_2 + 24UL;
                var_23 = (uint64_t *)var_22;
                var_24 = *var_23;
                rdi2_0 = var_24;
                _pre_phi80 = var_22;
                if (!(((*(unsigned char *)var_24 == '\n') ^ 1) || (var_8 ^ 1))) {
                    *var_23 = (var_24 + 1UL);
                    *var_20 = (var_21 + (-1L));
                    local_sp_0 = local_sp_0_be;
                    continue;
                }
                var_25 = (uint64_t *)(local_sp_2 + 40UL);
                var_26 = *var_25 - rbx_0;
                var_27 = (uint64_t *)(local_sp_2 + 8UL);
                *var_27 = (*var_27 + var_26);
                *var_25 = rbx_0;
                var_28 = var_21 + var_24;
                rbx_1 = var_28;
                rbx_2 = var_28;
                if (var_9) {
                    var_34 = local_sp_2 + (-8L);
                    *(uint64_t *)var_34 = 4208536UL;
                    var_35 = indirect_placeholder_4(rbx_2, rdi, _pre_phi80, var_19);
                    _pre_phi83 = (uint64_t *)(var_34 + 16UL);
                    rbx_1 = rbx_2;
                    rdi2_0 = var_35;
                    local_sp_3 = var_34;
                }
            }
            var_36 = *_pre_phi83;
            _pre_phi79 = _pre_phi83;
            local_sp_4 = local_sp_3;
            if (!(var_36 == 0UL && ((var_36 > 7UL) || storemerge) || (var_8 ^ 1))) {
                loop_state_var = 1U;
                break;
            }
            var_37 = local_sp_3 + 40UL;
            var_38 = local_sp_3 + 8UL;
            var_39 = local_sp_3 + (-8L);
            *(uint64_t *)var_39 = 4208580UL;
            var_40 = indirect_placeholder_4(var_38, rdi2_0, var_36, var_37);
            local_sp_0_be = var_39;
            local_sp_5 = var_39;
            if ((uint64_t)(unsigned char)var_40 != 0UL) {
                _pre_phi87 = (uint64_t *)(var_39 + 16UL);
                loop_state_var = 0U;
                break;
            }
            *(uint64_t *)var_38 = (rbx_1 - *_pre_phi83);
            local_sp_0 = local_sp_0_be;
            continue;
        }
    switch (loop_state_var) {
      case 0U:
        {
            var_41 = *(uint64_t *)(local_sp_5 + 40UL);
            *_cast = (*_cast - var_41);
            return (var_41 & (-256L)) | (*_pre_phi87 == 0UL);
        }
        break;
      case 1U:
        {
            *_pre_phi79 = 0UL;
            _pre_phi87 = _pre_phi79;
            local_sp_5 = local_sp_4;
        }
        break;
    }
}
