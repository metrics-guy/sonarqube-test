typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_16_ret_type;
struct indirect_placeholder_17_ret_type;
struct indirect_placeholder_18_ret_type;
struct indirect_placeholder_16_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_17_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_18_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r9(void);
extern uint64_t init_rax(void);
extern struct indirect_placeholder_16_ret_type indirect_placeholder_16(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_17_ret_type indirect_placeholder_17(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_18_ret_type indirect_placeholder_18(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
typedef _Bool bool;
void bb_wrap_write(uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t rcx, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t r12_2;
    uint64_t r14_3;
    uint64_t *var_30;
    uint64_t rbp_3;
    uint64_t r12_3;
    uint64_t var_31;
    uint64_t rbp_0;
    uint64_t r14_2;
    uint64_t r12_0;
    uint64_t rbx_0;
    uint64_t rbp_1;
    uint64_t local_sp_3;
    uint64_t rcx4_2;
    uint64_t r14_0;
    uint64_t local_sp_2;
    uint64_t rcx4_0;
    uint64_t local_sp_0;
    uint64_t r85_3;
    uint64_t r85_2;
    uint64_t r9_0;
    uint64_t r85_0;
    uint64_t var_32;
    uint64_t rbp_2;
    uint64_t r9_3;
    uint64_t var_26;
    uint64_t rbx_1;
    uint64_t var_29;
    uint64_t rax_0;
    uint64_t var_27;
    uint64_t r12_1;
    uint64_t r14_1;
    uint64_t rcx4_1;
    uint64_t local_sp_1;
    uint64_t r9_1;
    uint64_t r85_1;
    uint64_t var_20;
    uint64_t var_21;
    struct indirect_placeholder_17_ret_type var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t rax_1;
    uint64_t var_13;
    uint64_t var_14;
    struct indirect_placeholder_18_ret_type var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_10;
    uint64_t r9_2;
    uint64_t *var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_28;
    uint64_t var_11;
    uint64_t var_12;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r15();
    var_5 = init_rbp();
    var_6 = init_r12();
    var_7 = init_r14();
    var_8 = init_cc_src2();
    var_9 = init_r9();
    *(uint64_t *)(var_0 + (-8L)) = var_4;
    *(uint64_t *)(var_0 + (-16L)) = var_7;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_6;
    *(uint64_t *)(var_0 + (-40L)) = var_5;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    *(uint64_t *)(var_0 + (-64L)) = r8;
    r12_2 = rcx;
    r14_2 = rdx;
    rbx_0 = var_3;
    rbp_1 = var_5;
    rcx4_2 = rcx;
    r85_2 = r8;
    rbp_2 = 0UL;
    r12_1 = var_6;
    r14_1 = var_7;
    r9_2 = var_9;
    if (rdx != 0UL) {
        var_10 = var_0 + (-72L);
        local_sp_2 = var_10;
        if (rsi == 0UL) {
            return;
        }
        var_33 = (uint64_t *)r12_2;
        var_34 = r14_2 - *var_33;
        var_35 = rsi - rbp_2;
        var_36 = (var_34 > var_35) ? var_35 : var_34;
        r14_3 = r14_2;
        rbp_3 = rbp_2;
        r12_3 = r12_2;
        rbp_0 = rbp_2;
        r12_0 = r12_2;
        rbx_0 = 0UL;
        rbp_1 = rbp_2;
        local_sp_3 = local_sp_2;
        r14_0 = r14_2;
        rcx4_0 = rcx4_2;
        r85_3 = r85_2;
        r9_0 = r9_2;
        r85_0 = r85_2;
        r9_3 = r9_2;
        rbx_1 = var_36;
        rax_0 = var_35;
        r12_1 = r12_2;
        r14_1 = r14_2;
        rcx4_1 = rcx4_2;
        r9_1 = r9_2;
        r85_1 = r85_2;
        rax_1 = var_35;
        if (var_36 != 0UL) {
            while (1U)
                {
                    var_26 = *(uint64_t *)4279872UL;
                    var_27 = local_sp_3 + (-8L);
                    *(uint64_t *)var_27 = 4205260UL;
                    indirect_placeholder();
                    var_28 = helper_cc_compute_c_wrapper(rax_1 - rbx_1, rbx_1, var_8, 17U);
                    r12_0 = r12_3;
                    r14_0 = r14_3;
                    rcx4_0 = var_26;
                    local_sp_0 = var_27;
                    r9_0 = r9_3;
                    r85_0 = r85_3;
                    if (var_28 == 0UL) {
                        *(uint64_t *)(local_sp_3 + (-16L)) = 4205337UL;
                        indirect_placeholder();
                        var_29 = (uint64_t)*(uint32_t *)rax_1;
                        *(uint64_t *)(local_sp_3 + (-24L)) = 4205359UL;
                        indirect_placeholder_16(0UL, 4256991UL, 1UL, var_29, var_26, r9_3, r85_3);
                        abort();
                    }
                    var_30 = (uint64_t *)r12_3;
                    *var_30 = (*var_30 + rbx_1);
                    var_31 = rbp_3 + rbx_1;
                    rbp_0 = var_31;
                    while (1U)
                        {
                            var_32 = helper_cc_compute_c_wrapper(rbp_0 - rsi, rsi, var_8, 17U);
                            r12_2 = r12_0;
                            r14_2 = r14_0;
                            rcx4_2 = rcx4_0;
                            local_sp_2 = local_sp_0;
                            r85_2 = r85_0;
                            rbp_2 = rbp_0;
                            r9_2 = r9_0;
                            if (var_32 != 0UL) {
                                loop_state_var = 2U;
                                break;
                            }
                            var_33 = (uint64_t *)r12_2;
                            var_34 = r14_2 - *var_33;
                            var_35 = rsi - rbp_2;
                            var_36 = (var_34 > var_35) ? var_35 : var_34;
                            r14_3 = r14_2;
                            rbp_3 = rbp_2;
                            r12_3 = r12_2;
                            rbp_0 = rbp_2;
                            r12_0 = r12_2;
                            rbx_0 = 0UL;
                            rbp_1 = rbp_2;
                            local_sp_3 = local_sp_2;
                            r14_0 = r14_2;
                            rcx4_0 = rcx4_2;
                            r85_3 = r85_2;
                            r9_0 = r9_2;
                            r85_0 = r85_2;
                            r9_3 = r9_2;
                            rbx_1 = var_36;
                            rax_0 = var_35;
                            r12_1 = r12_2;
                            r14_1 = r14_2;
                            rcx4_1 = rcx4_2;
                            r9_1 = r9_2;
                            r85_1 = r85_2;
                            rax_1 = var_35;
                            if (var_36 != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            var_37 = local_sp_2 + (-8L);
                            *(uint64_t *)var_37 = 4205317UL;
                            indirect_placeholder();
                            local_sp_0 = var_37;
                            local_sp_1 = var_37;
                            if ((uint64_t)((uint32_t)var_35 + 1U) != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            *var_33 = 0UL;
                            continue;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 0U:
                        {
                            continue;
                        }
                        break;
                      case 1U:
                        {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 2U:
                        {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            switch (loop_state_var) {
              case 1U:
                {
                    return;
                }
                break;
              case 0U:
                {
                    break;
                }
                break;
            }
        }
        var_37 = local_sp_2 + (-8L);
        *(uint64_t *)var_37 = 4205317UL;
        indirect_placeholder();
        local_sp_0 = var_37;
        local_sp_1 = var_37;
        if ((uint64_t)((uint32_t)var_35 + 1U) != 0UL) {
            while (1U)
                {
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4205214UL;
                    indirect_placeholder();
                    var_20 = (uint64_t)*(uint32_t *)rax_0;
                    var_21 = local_sp_1 + (-16L);
                    *(uint64_t *)var_21 = 4205236UL;
                    var_22 = indirect_placeholder_17(0UL, 4256991UL, 1UL, var_20, rcx4_1, r9_1, r85_1);
                    var_23 = var_22.field_0;
                    var_24 = var_22.field_2;
                    var_25 = var_22.field_3;
                    r14_3 = r14_1;
                    rbp_3 = rbp_1;
                    r12_3 = r12_1;
                    local_sp_3 = var_21;
                    r85_3 = var_25;
                    r9_3 = var_24;
                    rbx_1 = rbx_0;
                    rax_1 = var_23;
                    while (1U)
                        {
                            var_26 = *(uint64_t *)4279872UL;
                            var_27 = local_sp_3 + (-8L);
                            *(uint64_t *)var_27 = 4205260UL;
                            indirect_placeholder();
                            var_28 = helper_cc_compute_c_wrapper(rax_1 - rbx_1, rbx_1, var_8, 17U);
                            r12_0 = r12_3;
                            r14_0 = r14_3;
                            rcx4_0 = var_26;
                            local_sp_0 = var_27;
                            r9_0 = r9_3;
                            r85_0 = r85_3;
                            if (var_28 == 0UL) {
                                *(uint64_t *)(local_sp_3 + (-16L)) = 4205337UL;
                                indirect_placeholder();
                                var_29 = (uint64_t)*(uint32_t *)rax_1;
                                *(uint64_t *)(local_sp_3 + (-24L)) = 4205359UL;
                                indirect_placeholder_16(0UL, 4256991UL, 1UL, var_29, var_26, r9_3, r85_3);
                                abort();
                            }
                            var_30 = (uint64_t *)r12_3;
                            *var_30 = (*var_30 + rbx_1);
                            var_31 = rbp_3 + rbx_1;
                            rbp_0 = var_31;
                            while (1U)
                                {
                                    var_32 = helper_cc_compute_c_wrapper(rbp_0 - rsi, rsi, var_8, 17U);
                                    r12_2 = r12_0;
                                    r14_2 = r14_0;
                                    rcx4_2 = rcx4_0;
                                    local_sp_2 = local_sp_0;
                                    r85_2 = r85_0;
                                    rbp_2 = rbp_0;
                                    r9_2 = r9_0;
                                    if (var_32 != 0UL) {
                                        loop_state_var = 2U;
                                        break;
                                    }
                                    var_33 = (uint64_t *)r12_2;
                                    var_34 = r14_2 - *var_33;
                                    var_35 = rsi - rbp_2;
                                    var_36 = (var_34 > var_35) ? var_35 : var_34;
                                    r14_3 = r14_2;
                                    rbp_3 = rbp_2;
                                    r12_3 = r12_2;
                                    rbp_0 = rbp_2;
                                    r12_0 = r12_2;
                                    rbx_0 = 0UL;
                                    rbp_1 = rbp_2;
                                    local_sp_3 = local_sp_2;
                                    r14_0 = r14_2;
                                    rcx4_0 = rcx4_2;
                                    r85_3 = r85_2;
                                    r9_0 = r9_2;
                                    r85_0 = r85_2;
                                    r9_3 = r9_2;
                                    rbx_1 = var_36;
                                    rax_0 = var_35;
                                    r12_1 = r12_2;
                                    r14_1 = r14_2;
                                    rcx4_1 = rcx4_2;
                                    r9_1 = r9_2;
                                    r85_1 = r85_2;
                                    rax_1 = var_35;
                                    if (var_36 != 0UL) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    var_37 = local_sp_2 + (-8L);
                                    *(uint64_t *)var_37 = 4205317UL;
                                    indirect_placeholder();
                                    local_sp_0 = var_37;
                                    local_sp_1 = var_37;
                                    if ((uint64_t)((uint32_t)var_35 + 1U) != 0UL) {
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    *var_33 = 0UL;
                                    continue;
                                }
                            switch_state_var = 0;
                            switch (loop_state_var) {
                              case 0U:
                                {
                                    continue;
                                }
                                break;
                              case 1U:
                                {
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              case 2U:
                                {
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                            }
                            if (switch_state_var)
                                break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 1U:
                        {
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 0U:
                        {
                            continue;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            return;
        }
        *var_33 = 0UL;
        while (1U)
            {
                var_32 = helper_cc_compute_c_wrapper(rbp_0 - rsi, rsi, var_8, 17U);
                r12_2 = r12_0;
                r14_2 = r14_0;
                rcx4_2 = rcx4_0;
                local_sp_2 = local_sp_0;
                r85_2 = r85_0;
                rbp_2 = rbp_0;
                r9_2 = r9_0;
                if (var_32 != 0UL) {
                    loop_state_var = 2U;
                    break;
                }
                var_33 = (uint64_t *)r12_2;
                var_34 = r14_2 - *var_33;
                var_35 = rsi - rbp_2;
                var_36 = (var_34 > var_35) ? var_35 : var_34;
                r14_3 = r14_2;
                rbp_3 = rbp_2;
                r12_3 = r12_2;
                rbp_0 = rbp_2;
                r12_0 = r12_2;
                rbx_0 = 0UL;
                rbp_1 = rbp_2;
                local_sp_3 = local_sp_2;
                r14_0 = r14_2;
                rcx4_0 = rcx4_2;
                r85_3 = r85_2;
                r9_0 = r9_2;
                r85_0 = r85_2;
                r9_3 = r9_2;
                rbx_1 = var_36;
                rax_0 = var_35;
                r12_1 = r12_2;
                r14_1 = r14_2;
                rcx4_1 = rcx4_2;
                r9_1 = r9_2;
                r85_1 = r85_2;
                rax_1 = var_35;
                if (var_36 != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                var_37 = local_sp_2 + (-8L);
                *(uint64_t *)var_37 = 4205317UL;
                indirect_placeholder();
                local_sp_0 = var_37;
                local_sp_1 = var_37;
                if ((uint64_t)((uint32_t)var_35 + 1U) != 0UL) {
                    loop_state_var = 1U;
                    break;
                }
                *var_33 = 0UL;
                continue;
            }
        switch (loop_state_var) {
          case 1U:
            {
                while (1U)
                    {
                        *(uint64_t *)(local_sp_1 + (-8L)) = 4205214UL;
                        indirect_placeholder();
                        var_20 = (uint64_t)*(uint32_t *)rax_0;
                        var_21 = local_sp_1 + (-16L);
                        *(uint64_t *)var_21 = 4205236UL;
                        var_22 = indirect_placeholder_17(0UL, 4256991UL, 1UL, var_20, rcx4_1, r9_1, r85_1);
                        var_23 = var_22.field_0;
                        var_24 = var_22.field_2;
                        var_25 = var_22.field_3;
                        r14_3 = r14_1;
                        rbp_3 = rbp_1;
                        r12_3 = r12_1;
                        local_sp_3 = var_21;
                        r85_3 = var_25;
                        r9_3 = var_24;
                        rbx_1 = rbx_0;
                        rax_1 = var_23;
                        while (1U)
                            {
                                var_26 = *(uint64_t *)4279872UL;
                                var_27 = local_sp_3 + (-8L);
                                *(uint64_t *)var_27 = 4205260UL;
                                indirect_placeholder();
                                var_28 = helper_cc_compute_c_wrapper(rax_1 - rbx_1, rbx_1, var_8, 17U);
                                r12_0 = r12_3;
                                r14_0 = r14_3;
                                rcx4_0 = var_26;
                                local_sp_0 = var_27;
                                r9_0 = r9_3;
                                r85_0 = r85_3;
                                if (var_28 == 0UL) {
                                    *(uint64_t *)(local_sp_3 + (-16L)) = 4205337UL;
                                    indirect_placeholder();
                                    var_29 = (uint64_t)*(uint32_t *)rax_1;
                                    *(uint64_t *)(local_sp_3 + (-24L)) = 4205359UL;
                                    indirect_placeholder_16(0UL, 4256991UL, 1UL, var_29, var_26, r9_3, r85_3);
                                    abort();
                                }
                                var_30 = (uint64_t *)r12_3;
                                *var_30 = (*var_30 + rbx_1);
                                var_31 = rbp_3 + rbx_1;
                                rbp_0 = var_31;
                                while (1U)
                                    {
                                        var_32 = helper_cc_compute_c_wrapper(rbp_0 - rsi, rsi, var_8, 17U);
                                        r12_2 = r12_0;
                                        r14_2 = r14_0;
                                        rcx4_2 = rcx4_0;
                                        local_sp_2 = local_sp_0;
                                        r85_2 = r85_0;
                                        rbp_2 = rbp_0;
                                        r9_2 = r9_0;
                                        if (var_32 != 0UL) {
                                            loop_state_var = 2U;
                                            break;
                                        }
                                        var_33 = (uint64_t *)r12_2;
                                        var_34 = r14_2 - *var_33;
                                        var_35 = rsi - rbp_2;
                                        var_36 = (var_34 > var_35) ? var_35 : var_34;
                                        r14_3 = r14_2;
                                        rbp_3 = rbp_2;
                                        r12_3 = r12_2;
                                        rbp_0 = rbp_2;
                                        r12_0 = r12_2;
                                        rbx_0 = 0UL;
                                        rbp_1 = rbp_2;
                                        local_sp_3 = local_sp_2;
                                        r14_0 = r14_2;
                                        rcx4_0 = rcx4_2;
                                        r85_3 = r85_2;
                                        r9_0 = r9_2;
                                        r85_0 = r85_2;
                                        r9_3 = r9_2;
                                        rbx_1 = var_36;
                                        rax_0 = var_35;
                                        r12_1 = r12_2;
                                        r14_1 = r14_2;
                                        rcx4_1 = rcx4_2;
                                        r9_1 = r9_2;
                                        r85_1 = r85_2;
                                        rax_1 = var_35;
                                        if (var_36 != 0UL) {
                                            loop_state_var = 0U;
                                            break;
                                        }
                                        var_37 = local_sp_2 + (-8L);
                                        *(uint64_t *)var_37 = 4205317UL;
                                        indirect_placeholder();
                                        local_sp_0 = var_37;
                                        local_sp_1 = var_37;
                                        if ((uint64_t)((uint32_t)var_35 + 1U) != 0UL) {
                                            loop_state_var = 1U;
                                            break;
                                        }
                                        *var_33 = 0UL;
                                        continue;
                                    }
                                switch_state_var = 0;
                                switch (loop_state_var) {
                                  case 0U:
                                    {
                                        continue;
                                    }
                                    break;
                                  case 1U:
                                    {
                                        loop_state_var = 0U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    break;
                                  case 2U:
                                    {
                                        loop_state_var = 1U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    break;
                                }
                                if (switch_state_var)
                                    break;
                            }
                        switch_state_var = 0;
                        switch (loop_state_var) {
                          case 1U:
                            {
                                switch_state_var = 1;
                                break;
                            }
                            break;
                          case 0U:
                            {
                                continue;
                            }
                            break;
                        }
                        if (switch_state_var)
                            break;
                    }
            }
            break;
          case 2U:
            {
                return;
            }
            break;
          case 0U:
            {
                while (1U)
                    {
                        var_26 = *(uint64_t *)4279872UL;
                        var_27 = local_sp_3 + (-8L);
                        *(uint64_t *)var_27 = 4205260UL;
                        indirect_placeholder();
                        var_28 = helper_cc_compute_c_wrapper(rax_1 - rbx_1, rbx_1, var_8, 17U);
                        r12_0 = r12_3;
                        r14_0 = r14_3;
                        rcx4_0 = var_26;
                        local_sp_0 = var_27;
                        r9_0 = r9_3;
                        r85_0 = r85_3;
                        if (var_28 == 0UL) {
                            *(uint64_t *)(local_sp_3 + (-16L)) = 4205337UL;
                            indirect_placeholder();
                            var_29 = (uint64_t)*(uint32_t *)rax_1;
                            *(uint64_t *)(local_sp_3 + (-24L)) = 4205359UL;
                            indirect_placeholder_16(0UL, 4256991UL, 1UL, var_29, var_26, r9_3, r85_3);
                            abort();
                        }
                        var_30 = (uint64_t *)r12_3;
                        *var_30 = (*var_30 + rbx_1);
                        var_31 = rbp_3 + rbx_1;
                        rbp_0 = var_31;
                        while (1U)
                            {
                                var_32 = helper_cc_compute_c_wrapper(rbp_0 - rsi, rsi, var_8, 17U);
                                r12_2 = r12_0;
                                r14_2 = r14_0;
                                rcx4_2 = rcx4_0;
                                local_sp_2 = local_sp_0;
                                r85_2 = r85_0;
                                rbp_2 = rbp_0;
                                r9_2 = r9_0;
                                if (var_32 != 0UL) {
                                    loop_state_var = 2U;
                                    break;
                                }
                                var_33 = (uint64_t *)r12_2;
                                var_34 = r14_2 - *var_33;
                                var_35 = rsi - rbp_2;
                                var_36 = (var_34 > var_35) ? var_35 : var_34;
                                r14_3 = r14_2;
                                rbp_3 = rbp_2;
                                r12_3 = r12_2;
                                rbp_0 = rbp_2;
                                r12_0 = r12_2;
                                rbx_0 = 0UL;
                                rbp_1 = rbp_2;
                                local_sp_3 = local_sp_2;
                                r14_0 = r14_2;
                                rcx4_0 = rcx4_2;
                                r85_3 = r85_2;
                                r9_0 = r9_2;
                                r85_0 = r85_2;
                                r9_3 = r9_2;
                                rbx_1 = var_36;
                                rax_0 = var_35;
                                r12_1 = r12_2;
                                r14_1 = r14_2;
                                rcx4_1 = rcx4_2;
                                r9_1 = r9_2;
                                r85_1 = r85_2;
                                rax_1 = var_35;
                                if (var_36 != 0UL) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                var_37 = local_sp_2 + (-8L);
                                *(uint64_t *)var_37 = 4205317UL;
                                indirect_placeholder();
                                local_sp_0 = var_37;
                                local_sp_1 = var_37;
                                if ((uint64_t)((uint32_t)var_35 + 1U) != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                *var_33 = 0UL;
                                continue;
                            }
                        switch_state_var = 0;
                        switch (loop_state_var) {
                          case 0U:
                            {
                                continue;
                            }
                            break;
                          case 1U:
                            {
                                loop_state_var = 0U;
                                switch_state_var = 1;
                                break;
                            }
                            break;
                          case 2U:
                            {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            break;
                        }
                        if (switch_state_var)
                            break;
                    }
                switch (loop_state_var) {
                  case 1U:
                    {
                        return;
                    }
                    break;
                  case 0U:
                    {
                        while (1U)
                            {
                                *(uint64_t *)(local_sp_1 + (-8L)) = 4205214UL;
                                indirect_placeholder();
                                var_20 = (uint64_t)*(uint32_t *)rax_0;
                                var_21 = local_sp_1 + (-16L);
                                *(uint64_t *)var_21 = 4205236UL;
                                var_22 = indirect_placeholder_17(0UL, 4256991UL, 1UL, var_20, rcx4_1, r9_1, r85_1);
                                var_23 = var_22.field_0;
                                var_24 = var_22.field_2;
                                var_25 = var_22.field_3;
                                r14_3 = r14_1;
                                rbp_3 = rbp_1;
                                r12_3 = r12_1;
                                local_sp_3 = var_21;
                                r85_3 = var_25;
                                r9_3 = var_24;
                                rbx_1 = rbx_0;
                                rax_1 = var_23;
                                while (1U)
                                    {
                                        var_26 = *(uint64_t *)4279872UL;
                                        var_27 = local_sp_3 + (-8L);
                                        *(uint64_t *)var_27 = 4205260UL;
                                        indirect_placeholder();
                                        var_28 = helper_cc_compute_c_wrapper(rax_1 - rbx_1, rbx_1, var_8, 17U);
                                        r12_0 = r12_3;
                                        r14_0 = r14_3;
                                        rcx4_0 = var_26;
                                        local_sp_0 = var_27;
                                        r9_0 = r9_3;
                                        r85_0 = r85_3;
                                        if (var_28 == 0UL) {
                                            *(uint64_t *)(local_sp_3 + (-16L)) = 4205337UL;
                                            indirect_placeholder();
                                            var_29 = (uint64_t)*(uint32_t *)rax_1;
                                            *(uint64_t *)(local_sp_3 + (-24L)) = 4205359UL;
                                            indirect_placeholder_16(0UL, 4256991UL, 1UL, var_29, var_26, r9_3, r85_3);
                                            abort();
                                        }
                                        var_30 = (uint64_t *)r12_3;
                                        *var_30 = (*var_30 + rbx_1);
                                        var_31 = rbp_3 + rbx_1;
                                        rbp_0 = var_31;
                                        while (1U)
                                            {
                                                var_32 = helper_cc_compute_c_wrapper(rbp_0 - rsi, rsi, var_8, 17U);
                                                r12_2 = r12_0;
                                                r14_2 = r14_0;
                                                rcx4_2 = rcx4_0;
                                                local_sp_2 = local_sp_0;
                                                r85_2 = r85_0;
                                                rbp_2 = rbp_0;
                                                r9_2 = r9_0;
                                                if (var_32 != 0UL) {
                                                    loop_state_var = 2U;
                                                    break;
                                                }
                                                var_33 = (uint64_t *)r12_2;
                                                var_34 = r14_2 - *var_33;
                                                var_35 = rsi - rbp_2;
                                                var_36 = (var_34 > var_35) ? var_35 : var_34;
                                                r14_3 = r14_2;
                                                rbp_3 = rbp_2;
                                                r12_3 = r12_2;
                                                rbp_0 = rbp_2;
                                                r12_0 = r12_2;
                                                rbx_0 = 0UL;
                                                rbp_1 = rbp_2;
                                                local_sp_3 = local_sp_2;
                                                r14_0 = r14_2;
                                                rcx4_0 = rcx4_2;
                                                r85_3 = r85_2;
                                                r9_0 = r9_2;
                                                r85_0 = r85_2;
                                                r9_3 = r9_2;
                                                rbx_1 = var_36;
                                                rax_0 = var_35;
                                                r12_1 = r12_2;
                                                r14_1 = r14_2;
                                                rcx4_1 = rcx4_2;
                                                r9_1 = r9_2;
                                                r85_1 = r85_2;
                                                rax_1 = var_35;
                                                if (var_36 != 0UL) {
                                                    loop_state_var = 0U;
                                                    break;
                                                }
                                                var_37 = local_sp_2 + (-8L);
                                                *(uint64_t *)var_37 = 4205317UL;
                                                indirect_placeholder();
                                                local_sp_0 = var_37;
                                                local_sp_1 = var_37;
                                                if ((uint64_t)((uint32_t)var_35 + 1U) != 0UL) {
                                                    loop_state_var = 1U;
                                                    break;
                                                }
                                                *var_33 = 0UL;
                                                continue;
                                            }
                                        switch_state_var = 0;
                                        switch (loop_state_var) {
                                          case 0U:
                                            {
                                                continue;
                                            }
                                            break;
                                          case 1U:
                                            {
                                                loop_state_var = 0U;
                                                switch_state_var = 1;
                                                break;
                                            }
                                            break;
                                          case 2U:
                                            {
                                                loop_state_var = 1U;
                                                switch_state_var = 1;
                                                break;
                                            }
                                            break;
                                        }
                                        if (switch_state_var)
                                            break;
                                    }
                                switch_state_var = 0;
                                switch (loop_state_var) {
                                  case 1U:
                                    {
                                        switch_state_var = 1;
                                        break;
                                    }
                                    break;
                                  case 0U:
                                    {
                                        continue;
                                    }
                                    break;
                                }
                                if (switch_state_var)
                                    break;
                            }
                    }
                    break;
                }
            }
            break;
        }
    }
    var_11 = *(uint64_t *)4279872UL;
    *(uint64_t *)(var_0 + (-80L)) = 4205162UL;
    indirect_placeholder();
    var_12 = helper_cc_compute_c_wrapper(var_1 - rsi, rsi, var_8, 17U);
    if (var_12 != 0UL) {
        return;
    }
    *(uint64_t *)(var_0 + (-88L)) = 4205187UL;
    indirect_placeholder();
    var_13 = (uint64_t)*(uint32_t *)var_1;
    var_14 = var_0 + (-96L);
    *(uint64_t *)var_14 = 4205209UL;
    var_15 = indirect_placeholder_18(0UL, 4256991UL, 1UL, var_13, var_11, var_9, r8);
    var_16 = var_15.field_0;
    var_17 = var_15.field_1;
    var_18 = var_15.field_2;
    var_19 = var_15.field_3;
    rax_0 = var_16;
    rcx4_1 = var_17;
    local_sp_1 = var_14;
    r9_1 = var_18;
    r85_1 = var_19;
}
