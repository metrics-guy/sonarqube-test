typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_9(uint64_t param_0);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
uint64_t bb_decode_8(uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t rcx) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t rbx_0;
    uint64_t local_sp_6;
    uint64_t r14_0;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t r14_2;
    uint64_t local_sp_0;
    uint64_t r14_1;
    uint64_t var_49;
    uint64_t var_50;
    unsigned char var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    unsigned char var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    unsigned char var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t local_sp_1;
    uint64_t local_sp_2;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    unsigned char var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_64;
    uint64_t local_sp_3;
    uint64_t r14_3;
    uint64_t var_27;
    uint64_t var_28;
    unsigned char var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    unsigned char var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    unsigned char var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t local_sp_4;
    uint64_t var_14;
    uint64_t *var_15;
    uint64_t local_sp_5;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    unsigned char var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t r14_4;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    *(uint64_t *)(var_0 + (-64L)) = rdx;
    rbx_0 = 0UL;
    if (rsi > 7UL) {
        return rbx_0;
    }
    var_7 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)rdi;
    *(uint64_t *)(var_0 + (-72L)) = 4207476UL;
    var_8 = indirect_placeholder_9(var_7);
    var_9 = (uint64_t)(uint32_t)var_8;
    rbx_0 = var_9;
    var_10 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(rdi + 1UL);
    var_11 = var_0 + (-80L);
    *(uint64_t *)var_11 = 4207499UL;
    var_12 = indirect_placeholder_9(var_10);
    var_13 = (uint64_t)(uint32_t)var_12;
    local_sp_5 = var_11;
    rbx_0 = var_13;
    if (~((uint64_t)(unsigned char)var_8 != 0UL & (uint64_t)(unsigned char)var_12 != 0UL)) {
        return;
    }
    var_14 = *(uint64_t *)rdx;
    var_15 = (uint64_t *)rcx;
    r14_3 = var_14;
    rbx_0 = 0UL;
    if (*var_15 == 0UL) {
        *(uint64_t *)(var_0 + (-88L)) = 4207527UL;
        var_16 = indirect_placeholder_9(var_7);
        var_17 = var_0 + (-96L);
        *(uint64_t *)var_17 = 4207538UL;
        var_18 = indirect_placeholder_9(var_10);
        *(unsigned char *)var_14 = ((unsigned char)(uint64_t)((long)((uint64_t)*(unsigned char *)((uint64_t)(unsigned char)var_18 + 4257408UL) << 56UL) >> (long)58UL) | (unsigned char)((uint64_t)*(unsigned char *)((uint64_t)(unsigned char)var_16 + 4257408UL) << 3UL));
        *var_15 = (*var_15 + (-1L));
        local_sp_5 = var_17;
        r14_3 = var_14 + 1UL;
    }
    var_19 = *(unsigned char *)(rdi + 2UL);
    r14_2 = r14_3;
    local_sp_6 = local_sp_5;
    r14_4 = r14_3;
    if (var_19 == '=') {
        if (*(unsigned char *)(rdi + 3UL) != '=') {
            **(uint64_t **)local_sp_5 = r14_3;
            return rbx_0;
        }
        if (*(unsigned char *)(rdi + 4UL) != '=') {
            **(uint64_t **)local_sp_5 = r14_3;
            return rbx_0;
        }
        if (*(unsigned char *)(rdi + 5UL) != '=') {
            **(uint64_t **)local_sp_5 = r14_3;
            return rbx_0;
        }
        if (*(unsigned char *)(rdi + 6UL) != '=') {
            **(uint64_t **)local_sp_5 = r14_3;
            return rbx_0;
        }
        if (*(unsigned char *)(rdi + 7UL) != '=') {
            **(uint64_t **)local_sp_5 = r14_3;
            return rbx_0;
        }
    }
    var_20 = (uint64_t)(uint32_t)(uint64_t)var_19;
    var_21 = local_sp_5 + (-8L);
    *(uint64_t *)var_21 = 4207613UL;
    var_22 = indirect_placeholder_9(var_20);
    local_sp_4 = var_21;
    if ((uint64_t)(unsigned char)var_22 != 0UL) {
        **(uint64_t **)local_sp_4 = r14_3;
        return rbx_0;
    }
    var_23 = *(unsigned char *)(rdi + 3UL);
    var_24 = (uint64_t)(uint32_t)(uint64_t)var_23;
    *(uint32_t *)(local_sp_5 + 4UL) = (uint32_t)var_23;
    var_25 = local_sp_5 + (-16L);
    *(uint64_t *)var_25 = 4207636UL;
    var_26 = indirect_placeholder_9(var_24);
    local_sp_3 = var_25;
    local_sp_4 = var_25;
    if ((uint64_t)(unsigned char)var_26 != 0UL) {
        **(uint64_t **)local_sp_4 = r14_3;
        return rbx_0;
    }
    if (*var_15 == 0UL) {
        *(uint64_t *)(local_sp_5 + (-24L)) = 4207659UL;
        var_27 = indirect_placeholder_9(var_10);
        *(uint64_t *)(local_sp_5 + (-32L)) = 4207670UL;
        var_28 = indirect_placeholder_9(var_20);
        var_29 = (unsigned char)((uint64_t)*(unsigned char *)((uint64_t)(unsigned char)var_28 + 4257408UL) << 1UL) | (unsigned char)((uint64_t)*(unsigned char *)((uint64_t)(unsigned char)var_27 + 4257408UL) << 6UL);
        var_30 = (uint64_t)*(uint32_t *)(local_sp_5 + (-20L));
        var_31 = local_sp_5 + (-40L);
        *(uint64_t *)var_31 = 4207714UL;
        var_32 = indirect_placeholder_9(var_30);
        *(unsigned char *)r14_3 = (var_29 | (unsigned char)(uint64_t)((long)((uint64_t)*(unsigned char *)((uint64_t)(unsigned char)var_32 + 4257408UL) << 56UL) >> (long)60UL));
        *var_15 = (*var_15 + (-1L));
        local_sp_3 = var_31;
        r14_2 = r14_3 + 1UL;
    }
    var_33 = *(unsigned char *)(rdi + 4UL);
    r14_1 = r14_2;
    local_sp_6 = local_sp_3;
    r14_4 = r14_2;
    if (var_33 == '=') {
        if (*(unsigned char *)(rdi + 5UL) != '=') {
            **(uint64_t **)local_sp_3 = r14_2;
            return rbx_0;
        }
        if (*(unsigned char *)(rdi + 6UL) != '=') {
            **(uint64_t **)local_sp_3 = r14_2;
            return rbx_0;
        }
        if (*(unsigned char *)(rdi + 7UL) == '=') {
            **(uint64_t **)local_sp_3 = r14_2;
            return rbx_0;
        }
    }
    var_34 = (uint64_t)(uint32_t)(uint64_t)var_33;
    var_35 = local_sp_3 + (-8L);
    *(uint64_t *)var_35 = 4207769UL;
    var_36 = indirect_placeholder_9(var_34);
    local_sp_2 = var_35;
    if ((uint64_t)(unsigned char)var_36 != 0UL) {
        **(uint64_t **)var_35 = r14_2;
        var_64 = (uint64_t)(uint32_t)var_36;
        rbx_0 = var_64;
        return rbx_0;
    }
    if (*var_15 == 0UL) {
        var_37 = (uint64_t)*(uint32_t *)(local_sp_3 + 4UL);
        *(uint64_t *)(local_sp_3 + (-16L)) = 4207793UL;
        var_38 = indirect_placeholder_9(var_37);
        var_39 = local_sp_3 + (-24L);
        *(uint64_t *)var_39 = 4207804UL;
        var_40 = indirect_placeholder_9(var_34);
        *(unsigned char *)r14_2 = ((unsigned char)(uint64_t)((long)((uint64_t)*(unsigned char *)((uint64_t)(unsigned char)var_40 + 4257408UL) << 56UL) >> (long)57UL) | (unsigned char)((uint64_t)*(unsigned char *)((uint64_t)(unsigned char)var_38 + 4257408UL) << 4UL));
        *var_15 = (*var_15 + (-1L));
        local_sp_2 = var_39;
        r14_1 = r14_2 + 1UL;
    }
    var_41 = *(unsigned char *)(rdi + 5UL);
    r14_0 = r14_1;
    local_sp_6 = local_sp_2;
    r14_4 = r14_1;
    if (var_41 == '=') {
        if (*(unsigned char *)(rdi + 6UL) != '=') {
            **(uint64_t **)local_sp_2 = r14_1;
            return rbx_0;
        }
        if (*(unsigned char *)(rdi + 7UL) == '=') {
            **(uint64_t **)local_sp_2 = r14_1;
            return rbx_0;
        }
    }
    var_42 = (uint64_t)(uint32_t)(uint64_t)var_41;
    var_43 = local_sp_2 + (-8L);
    *(uint64_t *)var_43 = 4207876UL;
    var_44 = indirect_placeholder_9(var_42);
    local_sp_1 = var_43;
    if ((uint64_t)(unsigned char)var_44 != 0UL) {
        **(uint64_t **)local_sp_1 = r14_1;
        return rbx_0;
    }
    var_45 = *(unsigned char *)(rdi + 6UL);
    var_46 = (uint64_t)(uint32_t)(uint64_t)var_45;
    *(uint32_t *)(local_sp_2 + 4UL) = (uint32_t)var_45;
    var_47 = local_sp_2 + (-16L);
    *(uint64_t *)var_47 = 4207899UL;
    var_48 = indirect_placeholder_9(var_46);
    local_sp_0 = var_47;
    local_sp_1 = var_47;
    if ((uint64_t)(unsigned char)var_48 != 0UL) {
        **(uint64_t **)local_sp_1 = r14_1;
        return rbx_0;
    }
    if (*var_15 == 0UL) {
        *(uint64_t *)(local_sp_2 + (-24L)) = 4207922UL;
        var_49 = indirect_placeholder_9(var_34);
        *(uint64_t *)(local_sp_2 + (-32L)) = 4207933UL;
        var_50 = indirect_placeholder_9(var_42);
        var_51 = (unsigned char)((uint64_t)*(unsigned char *)((uint64_t)(unsigned char)var_50 + 4257408UL) << 2UL) | (unsigned char)((uint64_t)*(unsigned char *)((uint64_t)(unsigned char)var_49 + 4257408UL) << 7UL);
        var_52 = (uint64_t)*(uint32_t *)(local_sp_2 + (-20L));
        var_53 = local_sp_2 + (-40L);
        *(uint64_t *)var_53 = 4207980UL;
        var_54 = indirect_placeholder_9(var_52);
        *(unsigned char *)r14_1 = (var_51 | (unsigned char)(uint64_t)((long)((uint64_t)*(unsigned char *)((uint64_t)(unsigned char)var_54 + 4257408UL) << 56UL) >> (long)59UL));
        *var_15 = (*var_15 + (-1L));
        local_sp_0 = var_53;
        r14_0 = r14_1 + 1UL;
    }
    var_55 = *(unsigned char *)(rdi + 7UL);
    local_sp_6 = local_sp_0;
    r14_4 = r14_0;
    if (var_55 != '=') {
        var_56 = (uint64_t)(uint32_t)(uint64_t)var_55;
        var_57 = local_sp_0 + (-8L);
        *(uint64_t *)var_57 = 4208029UL;
        var_58 = indirect_placeholder_9(var_56);
        local_sp_6 = var_57;
        if ((uint64_t)(unsigned char)var_58 != 0UL) {
            **(uint64_t **)var_57 = r14_0;
            var_63 = (uint64_t)(uint32_t)var_58;
            rbx_0 = var_63;
            return rbx_0;
        }
        if (*var_15 == 0UL) {
            var_59 = (uint64_t)*(uint32_t *)(local_sp_0 + 4UL);
            *(uint64_t *)(local_sp_0 + (-16L)) = 4208053UL;
            var_60 = indirect_placeholder_9(var_59);
            var_61 = local_sp_0 + (-24L);
            *(uint64_t *)var_61 = 4208063UL;
            var_62 = indirect_placeholder_9(var_56);
            *(unsigned char *)r14_0 = ((*(unsigned char *)((uint64_t)(unsigned char)var_60 + 4257408UL) << '\x05') | *(unsigned char *)((uint64_t)(unsigned char)var_62 + 4257408UL));
            *var_15 = (*var_15 + (-1L));
            local_sp_6 = var_61;
            r14_4 = r14_0 + 1UL;
        }
    }
    **(uint64_t **)local_sp_6 = r14_4;
    rbx_0 = var_13;
}
