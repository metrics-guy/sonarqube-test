typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_9(uint64_t param_0);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
void bb_base32_encode(uint64_t rax, uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t rcx) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    bool var_8;
    bool var_9;
    uint64_t local_sp_6;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t rax1_0;
    uint64_t local_sp_0;
    uint64_t r13_1;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t rax1_1;
    uint64_t local_sp_1;
    uint64_t r13_0;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t rbp_0;
    uint64_t var_30;
    unsigned char *var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t r12_0;
    uint64_t r14_0;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_13;
    unsigned char *var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t local_sp_3;
    uint64_t var_50;
    uint64_t rbp_1;
    uint64_t r15_0;
    unsigned char rax1_2;
    uint64_t local_sp_2;
    unsigned char *var_34;
    uint64_t var_35;
    uint64_t var_36;
    unsigned char storemerge;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_51;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    var_7 = var_0 + (-48L);
    *(uint64_t *)var_7 = var_2;
    var_8 = (rsi == 0UL);
    var_9 = (rcx == 0UL);
    local_sp_6 = var_7;
    rax1_0 = 0UL;
    r13_1 = 0UL;
    rax1_1 = 0UL;
    r13_0 = 0UL;
    rbp_0 = rdx;
    r12_0 = rcx;
    r14_0 = rdi;
    rbp_1 = rdx;
    r15_0 = rsi;
    rax1_2 = (unsigned char)'=';
    storemerge = (unsigned char)'=';
    if (var_8) {
        if (var_9) {
            return;
        }
    }
    if (var_9) {
        return;
    }
    while (1U)
        {
            var_10 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)r14_0;
            var_11 = local_sp_6 + (-8L);
            *(uint64_t *)var_11 = 4206967UL;
            var_12 = indirect_placeholder_9(var_10);
            *(unsigned char *)rbp_0 = *(unsigned char *)(((var_12 >> 3UL) & 31UL) | 4257376UL);
            local_sp_2 = var_11;
            if (r12_0 != 1UL) {
                loop_state_var = 0U;
                break;
            }
            var_13 = var_12 << 2UL;
            if ((r15_0 + (-1L)) == 0UL) {
                *(unsigned char *)(rbp_0 + 1UL) = *(unsigned char *)((var_13 & 28UL) | 4257376UL);
                if (r12_0 != 2UL) {
                    loop_state_var = 0U;
                    break;
                }
                *(unsigned char *)(rbp_0 + 2UL) = (unsigned char)'=';
                if (r12_0 != 3UL) {
                    loop_state_var = 0U;
                    break;
                }
                *(unsigned char *)(rbp_0 + 3UL) = (unsigned char)'=';
                if (r12_0 != 4UL) {
                    loop_state_var = 0U;
                    break;
                }
            }
            var_14 = (unsigned char *)(r14_0 + 1UL);
            var_15 = (uint64_t)(uint32_t)(uint64_t)*var_14;
            *(uint64_t *)(local_sp_6 + (-16L)) = 4207028UL;
            var_16 = indirect_placeholder_9(var_15);
            *(unsigned char *)(rbp_0 + 1UL) = *(unsigned char *)((((var_16 >> 6UL) & 3UL) | (var_13 & 28UL)) | 4257376UL);
            if (r12_0 != 2UL) {
                loop_state_var = 0U;
                break;
            }
            var_17 = (uint64_t)(uint32_t)(uint64_t)*var_14;
            *(uint64_t *)(local_sp_6 + (-24L)) = 4207067UL;
            var_18 = indirect_placeholder_9(var_17);
            *(unsigned char *)(rbp_0 + 2UL) = *(unsigned char *)(((var_18 >> 1UL) & 31UL) | 4257376UL);
            if (r12_0 != 3UL) {
                loop_state_var = 0U;
                break;
            }
            var_19 = (uint64_t)(uint32_t)(uint64_t)*var_14;
            var_20 = local_sp_6 + (-32L);
            *(uint64_t *)var_20 = 4207102UL;
            var_21 = indirect_placeholder_9(var_19);
            var_22 = var_21 << 4UL;
            local_sp_2 = var_20;
            if (r15_0 == 2UL) {
                *(unsigned char *)(rbp_0 + 3UL) = *(unsigned char *)((var_22 & 16UL) | 4257376UL);
                if (r12_0 == 4UL) {
                    loop_state_var = 0U;
                    break;
                }
            }
            var_23 = (unsigned char *)(r14_0 + 2UL);
            var_24 = (uint64_t)(uint32_t)(uint64_t)*var_23;
            *(uint64_t *)(local_sp_6 + (-40L)) = 4207128UL;
            var_25 = indirect_placeholder_9(var_24);
            *(unsigned char *)(rbp_0 + 3UL) = *(unsigned char *)((((var_25 >> 4UL) & 15UL) | (var_22 & 16UL)) | 4257376UL);
            if (r12_0 != 4UL) {
                loop_state_var = 0U;
                break;
            }
            var_26 = (uint64_t)(uint32_t)(uint64_t)*var_23;
            var_27 = local_sp_6 + (-48L);
            *(uint64_t *)var_27 = 4207167UL;
            var_28 = indirect_placeholder_9(var_26);
            var_29 = var_28 << 1UL;
            var_30 = r15_0 + (-3L);
            local_sp_1 = var_27;
            r13_0 = var_30;
            if (var_30 == 0UL) {
                var_31 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(r14_0 + 3UL);
                var_32 = local_sp_6 + (-56L);
                *(uint64_t *)var_32 = 4207196UL;
                var_33 = indirect_placeholder_9(var_31);
                rax1_1 = (var_33 >> 7UL) & 1UL;
                local_sp_1 = var_32;
            }
            rax1_2 = *(unsigned char *)(((var_29 & 30UL) | rax1_1) | 4257376UL);
            local_sp_2 = local_sp_1;
            *(unsigned char *)(rbp_0 + 4UL) = rax1_2;
            local_sp_3 = local_sp_2;
            if (r12_0 != 5UL) {
                loop_state_var = 0U;
                break;
            }
            if (r13_0 == 0UL) {
                *(unsigned char *)(rbp_0 + 5UL) = (unsigned char)'=';
                if (r12_0 != 6UL) {
                    loop_state_var = 0U;
                    break;
                }
            }
            var_34 = (unsigned char *)(r14_0 + 3UL);
            var_35 = (uint64_t)(uint32_t)(uint64_t)*var_34;
            *(uint64_t *)(local_sp_2 + (-8L)) = 4207246UL;
            var_36 = indirect_placeholder_9(var_35);
            *(unsigned char *)(rbp_0 + 5UL) = *(unsigned char *)(((var_36 >> 2UL) & 31UL) | 4257376UL);
            if (r12_0 != 6UL) {
                loop_state_var = 0U;
                break;
            }
            var_37 = (uint64_t)(uint32_t)(uint64_t)*var_34;
            var_38 = local_sp_2 + (-16L);
            *(uint64_t *)var_38 = 4207278UL;
            var_39 = indirect_placeholder_9(var_37);
            var_40 = var_39 << 3UL;
            var_41 = r13_0 + (-1L);
            local_sp_0 = var_38;
            r13_1 = var_41;
            if (var_41 == 0UL) {
                var_42 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(r14_0 + 4UL);
                var_43 = local_sp_2 + (-24L);
                *(uint64_t *)var_43 = 4207305UL;
                var_44 = indirect_placeholder_9(var_42);
                rax1_0 = (var_44 >> 5UL) & 7UL;
                local_sp_0 = var_43;
            }
            local_sp_3 = local_sp_0;
            storemerge = *(unsigned char *)(((var_40 & 24UL) | rax1_0) | 4257376UL);
            *(unsigned char *)(rbp_0 + 6UL) = storemerge;
            if (r12_0 != 7UL) {
                loop_state_var = 0U;
                break;
            }
            if (r13_1 != 0UL) {
                var_51 = rbp_0 + 8UL;
                *(unsigned char *)(rbp_0 + 7UL) = (unsigned char)'=';
                rbp_1 = var_51;
                if (r12_0 != 8UL) {
                    loop_state_var = 1U;
                    break;
                }
                loop_state_var = 0U;
                break;
            }
            var_45 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(r14_0 + 4UL);
            var_46 = local_sp_3 + (-8L);
            *(uint64_t *)var_46 = 4206914UL;
            var_47 = indirect_placeholder_9(var_45);
            var_48 = rbp_0 + 8UL;
            *(unsigned char *)(rbp_0 + 7UL) = *(unsigned char *)((var_47 & 31UL) | 4257376UL);
            var_49 = r12_0 + (-8L);
            rbp_0 = var_48;
            r12_0 = var_49;
            local_sp_6 = var_46;
            rbp_1 = var_48;
            if (var_49 != 0UL) {
                loop_state_var = 0U;
                break;
            }
            var_50 = r13_1 + (-1L);
            r15_0 = var_50;
            if (var_50 != 0UL) {
                loop_state_var = 1U;
                break;
            }
            r14_0 = r14_0 + 5UL;
            continue;
        }
    switch (loop_state_var) {
      case 0U:
        {
            return;
        }
        break;
      case 1U:
        {
            *(unsigned char *)rbp_1 = (unsigned char)'\x00';
            return;
        }
        break;
    }
}
