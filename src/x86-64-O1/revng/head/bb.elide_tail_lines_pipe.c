typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_13_ret_type;
struct indirect_placeholder_14_ret_type;
struct indirect_placeholder_15_ret_type;
struct indirect_placeholder_12_ret_type;
struct indirect_placeholder_16_ret_type;
struct indirect_placeholder_17_ret_type;
struct indirect_placeholder_13_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_14_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_15_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_12_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_16_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_17_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_6(uint64_t param_0);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern struct indirect_placeholder_13_ret_type indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_14_ret_type indirect_placeholder_14(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_15_ret_type indirect_placeholder_15(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_12_ret_type indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_16_ret_type indirect_placeholder_16(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_17_ret_type indirect_placeholder_17(uint64_t param_0, uint64_t param_1, uint64_t param_2);
typedef _Bool bool;
uint64_t bb_elide_tail_lines_pipe(uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t rcx) {
    uint64_t rbx_1;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t rbx_2_ph_ph;
    uint64_t local_sp_2;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t rdi2_0;
    uint64_t rdi2_1;
    uint64_t r15_2;
    uint64_t var_71;
    uint64_t *var_72;
    uint64_t var_73;
    uint64_t local_sp_1;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t r15_0;
    uint64_t local_sp_7;
    uint64_t *var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t r8_0_ph_ph;
    uint64_t rbx_0;
    uint64_t r15_1;
    uint64_t rbp_0;
    uint64_t local_sp_0;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    struct indirect_placeholder_14_ret_type var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t r12_1_ph_ph;
    uint64_t local_sp_7_ph_ph;
    uint64_t r9_0_ph_ph;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t *var_65;
    uint64_t rbp_1;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_43;
    uint64_t var_68;
    uint64_t rbp_2;
    uint64_t local_sp_3;
    uint64_t local_sp_5;
    uint64_t r12_0;
    uint64_t local_sp_4;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t rbp_3;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t var_82;
    struct indirect_placeholder_15_ret_type var_83;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t local_sp_7_ph_ph159;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t *var_29;
    uint64_t r15_3_ph;
    uint64_t var_35;
    uint64_t *var_36;
    uint64_t var_37;
    struct indirect_placeholder_16_ret_type var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t r15_3_ph_ph;
    uint64_t *var_13;
    uint64_t rbx_2_ph_ph156;
    uint64_t r15_3_ph_ph157;
    uint64_t r8_0_ph_ph158;
    uint64_t r9_0_ph_ph160;
    uint64_t rbx_2_ph;
    uint64_t local_sp_7_ph;
    uint64_t *var_14;
    uint64_t *var_15;
    uint64_t *var_16;
    uint64_t r15_3;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t *var_32;
    uint64_t var_26;
    uint64_t rdi2_2;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t local_sp_6;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t *var_42;
    uint64_t var_44;
    struct indirect_placeholder_17_ret_type var_45;
    uint64_t var_46;
    uint64_t *var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_r8();
    var_8 = init_cc_src2();
    var_9 = init_r9();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    *(uint64_t *)(var_0 + (-64L)) = rdi;
    *(uint32_t *)(var_0 + (-84L)) = (uint32_t)rsi;
    *(uint64_t *)(var_0 + (-96L)) = rdx;
    *(uint64_t *)(var_0 + (-72L)) = rcx;
    *(uint64_t *)(var_0 + (-80L)) = rcx;
    *(uint64_t *)(var_0 + (-128L)) = 4204952UL;
    var_10 = indirect_placeholder_6(1048UL);
    *(uint64_t *)(var_10 + 1032UL) = 0UL;
    *(uint64_t *)(var_10 + 1024UL) = 0UL;
    *(uint64_t *)(var_10 + 1040UL) = 0UL;
    var_11 = var_0 + (-136L);
    *(uint64_t *)var_11 = 4204998UL;
    var_12 = indirect_placeholder_6(1048UL);
    *(uint64_t *)(var_0 + (-120L)) = var_10;
    rbx_2_ph_ph = var_12;
    rdi2_0 = 0UL;
    r8_0_ph_ph = var_7;
    r12_1_ph_ph = var_10;
    local_sp_7_ph_ph = var_11;
    r9_0_ph_ph = var_9;
    rbp_2 = 1UL;
    r15_3_ph_ph = 0UL;
    while (1U)
        {
            var_13 = (uint64_t *)(r12_1_ph_ph + 1032UL);
            rbx_1 = r12_1_ph_ph;
            rbx_2_ph_ph = r12_1_ph_ph;
            rbx_0 = r12_1_ph_ph;
            r12_0 = r12_1_ph_ph;
            local_sp_7_ph_ph159 = local_sp_7_ph_ph;
            rbx_2_ph_ph156 = rbx_2_ph_ph;
            r15_3_ph_ph157 = r15_3_ph_ph;
            r8_0_ph_ph158 = r8_0_ph_ph;
            r9_0_ph_ph160 = r9_0_ph_ph;
            while (1U)
                {
                    rbx_2_ph = rbx_2_ph_ph156;
                    r15_3_ph = r15_3_ph_ph157;
                    local_sp_7_ph = local_sp_7_ph_ph159;
                    while (1U)
                        {
                            var_14 = (uint64_t *)(rbx_2_ph + 1024UL);
                            var_15 = (uint64_t *)(rbx_2_ph + 1032UL);
                            var_16 = (uint64_t *)(rbx_2_ph + 1040UL);
                            r15_3 = r15_3_ph;
                            local_sp_7 = local_sp_7_ph;
                            while (1U)
                                {
                                    var_17 = (uint64_t *)(local_sp_7 + 8UL);
                                    *var_17 = rbx_2_ph;
                                    var_18 = (uint64_t)*(uint32_t *)(local_sp_7 + 36UL);
                                    var_19 = local_sp_7 + (-8L);
                                    *(uint64_t *)var_19 = 4205054UL;
                                    var_20 = indirect_placeholder_5(1024UL, var_18, rbx_2_ph);
                                    r15_0 = r15_3;
                                    r15_3_ph_ph157 = r15_3;
                                    local_sp_6 = var_19;
                                    if ((var_20 + (-1L)) <= 18446744073709551613UL) {
                                        var_46 = local_sp_7 + (-16L);
                                        *(uint64_t *)var_46 = 4205354UL;
                                        indirect_placeholder_1();
                                        local_sp_0 = var_46;
                                        local_sp_1 = var_46;
                                        if (var_20 != 18446744073709551615UL) {
                                            var_47 = *(uint64_t *)local_sp_7;
                                            var_48 = *(uint64_t *)(var_47 + 1024UL);
                                            if (var_48 != 0UL) {
                                                loop_state_var = 1U;
                                                break;
                                            }
                                            if ((uint64_t)(*(unsigned char *)((var_48 + var_47) + (-1L)) - *(unsigned char *)4285233UL) != 0UL) {
                                                loop_state_var = 1U;
                                                break;
                                            }
                                            var_49 = (uint64_t *)(var_47 + 1032UL);
                                            *var_49 = (*var_49 + 1UL);
                                            r15_0 = r15_3 + 1UL;
                                            loop_state_var = 1U;
                                            break;
                                        }
                                        var_74 = *(uint64_t *)(local_sp_7 + 40UL);
                                        *(uint64_t *)(local_sp_7 + (-24L)) = 4205667UL;
                                        var_75 = indirect_placeholder(4UL, var_74);
                                        *(uint64_t *)(local_sp_7 + (-32L)) = 4205675UL;
                                        indirect_placeholder_1();
                                        var_76 = (uint64_t)*(uint32_t *)var_75;
                                        var_77 = local_sp_7 + (-40L);
                                        *(uint64_t *)var_77 = 4205700UL;
                                        indirect_placeholder_12(0UL, 4260113UL, 0UL, var_76, var_75, r8_0_ph_ph158, r9_0_ph_ph160);
                                        rbp_2 = 0UL;
                                        local_sp_3 = var_77;
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    if (*(uint64_t *)(local_sp_7 + 16UL) != 0UL) {
                                        loop_state_var = 3U;
                                        break;
                                    }
                                    *var_14 = var_20;
                                    *var_15 = 0UL;
                                    *var_16 = 0UL;
                                    var_21 = *(uint64_t *)local_sp_7;
                                    var_22 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)4285233UL;
                                    rdi2_2 = var_21;
                                    var_23 = (var_20 + var_21) - rdi2_2;
                                    var_24 = local_sp_6 + (-8L);
                                    *(uint64_t *)var_24 = 4205153UL;
                                    var_25 = indirect_placeholder_5(var_23, rdi2_2, var_22);
                                    local_sp_6 = var_24;
                                    while (var_25 != 0UL)
                                        {
                                            var_26 = var_25 + 1UL;
                                            *var_15 = (*var_15 + 1UL);
                                            rdi2_2 = var_26;
                                            var_23 = (var_20 + var_21) - rdi2_2;
                                            var_24 = local_sp_6 + (-8L);
                                            *(uint64_t *)var_24 = 4205153UL;
                                            var_25 = indirect_placeholder_5(var_23, rdi2_2, var_22);
                                            local_sp_6 = var_24;
                                        }
                                    var_27 = r15_3 + *var_15;
                                    var_28 = *(uint64_t *)(local_sp_6 + 8UL);
                                    var_29 = (uint64_t *)(var_28 + 1024UL);
                                    r15_3_ph = var_27;
                                    r15_3 = var_27;
                                    if ((var_20 + *var_29) <= 1023UL) {
                                        loop_state_var = 2U;
                                        break;
                                    }
                                    var_30 = local_sp_6 + (-16L);
                                    *(uint64_t *)var_30 = 4205290UL;
                                    indirect_placeholder_1();
                                    *var_29 = (*var_29 + *var_14);
                                    var_31 = *var_15;
                                    var_32 = (uint64_t *)(var_28 + 1032UL);
                                    *var_32 = (*var_32 + var_31);
                                    local_sp_7 = var_30;
                                    continue;
                                }
                            switch_state_var = 0;
                            switch (loop_state_var) {
                              case 2U:
                                {
                                    *(uint64_t *)(var_28 + 1040UL) = rbx_2_ph;
                                    if ((var_27 - *var_13) <= *(uint64_t *)(local_sp_6 + 16UL)) {
                                        loop_state_var = 1U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    var_33 = local_sp_6 + (-16L);
                                    *(uint64_t *)var_33 = 4205333UL;
                                    var_34 = indirect_placeholder_6(1048UL);
                                    *(uint64_t *)local_sp_6 = rbx_2_ph;
                                    rbx_2_ph = var_34;
                                    local_sp_7_ph = var_33;
                                    continue;
                                }
                                break;
                              case 1U:
                                {
                                    var_50 = r15_0 - *var_13;
                                    var_51 = *var_17;
                                    var_52 = helper_cc_compute_c_wrapper(var_51 - var_50, var_50, var_8, 17U);
                                    r15_1 = r15_0;
                                    r15_2 = r15_0;
                                    if (var_52 != 0UL) {
                                        loop_state_var = 2U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    rbp_0 = *(uint64_t *)(local_sp_7 + 24UL);
                                    loop_state_var = 3U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              case 0U:
                                {
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              case 3U:
                                {
                                    loop_state_var = 4U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                            }
                            if (switch_state_var)
                                break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 4U:
                        {
                            var_42 = (uint64_t *)(local_sp_7 + 32UL);
                            *var_42 = (*var_42 + var_20);
                            var_43 = *(uint64_t *)local_sp_7;
                            var_44 = local_sp_7 + (-16L);
                            *(uint64_t *)var_44 = 4205032UL;
                            var_45 = indirect_placeholder_17(rbx_2_ph, var_43, var_20);
                            rbx_2_ph_ph156 = var_45.field_0;
                            r8_0_ph_ph158 = var_45.field_5;
                            local_sp_7_ph_ph159 = var_44;
                            r9_0_ph_ph160 = var_45.field_6;
                            continue;
                        }
                        break;
                      case 0U:
                        {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 1U:
                        {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 2U:
                        {
                            loop_state_var = 2U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 3U:
                        {
                            loop_state_var = 3U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    var_35 = *(uint64_t *)(r12_1_ph_ph + 1024UL);
                    var_36 = (uint64_t *)(local_sp_6 + 32UL);
                    *var_36 = (*var_36 + var_35);
                    var_37 = local_sp_6 + (-16L);
                    *(uint64_t *)var_37 = 4205240UL;
                    var_38 = indirect_placeholder_16(rbx_2_ph, r12_1_ph_ph, var_35);
                    var_39 = var_38.field_5;
                    var_40 = var_38.field_6;
                    var_41 = var_27 - *var_13;
                    *(uint64_t *)local_sp_6 = var_38.field_0;
                    r8_0_ph_ph = var_39;
                    r12_1_ph_ph = *(uint64_t *)(r12_1_ph_ph + 1040UL);
                    local_sp_7_ph_ph = var_37;
                    r9_0_ph_ph = var_40;
                    r15_3_ph_ph = var_41;
                    continue;
                }
                break;
              case 0U:
                {
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 2U:
                {
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 3U:
                {
                    loop_state_var = 2U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            local_sp_4 = local_sp_3;
            local_sp_5 = local_sp_3;
            rbp_3 = rbp_2;
            if (r12_1_ph_ph == 0UL) {
                var_78 = *(uint64_t *)(r12_0 + 1040UL);
                var_79 = local_sp_4 + (-8L);
                *(uint64_t *)var_79 = 4205622UL;
                indirect_placeholder_1();
                r12_0 = var_78;
                local_sp_4 = var_79;
                local_sp_5 = var_79;
                do {
                    var_78 = *(uint64_t *)(r12_0 + 1040UL);
                    var_79 = local_sp_4 + (-8L);
                    *(uint64_t *)var_79 = 4205622UL;
                    indirect_placeholder_1();
                    r12_0 = var_78;
                    local_sp_4 = var_79;
                    local_sp_5 = var_79;
                } while (var_78 != 0UL);
            }
            if ((long)*(uint64_t *)(local_sp_5 + 48UL) > (long)18446744073709551615UL) {
                var_80 = *(uint64_t *)(local_sp_5 + 56UL);
                var_81 = *(uint64_t *)(local_sp_5 + 40UL);
                var_82 = (uint64_t)*(uint32_t *)(local_sp_5 + 36UL);
                *(uint64_t *)(local_sp_5 + (-8L)) = 4205739UL;
                var_83 = indirect_placeholder_15(0UL, var_82, var_81, var_80);
                rbp_3 = ((long)var_83.field_0 < (long)0UL) ? 0UL : rbp_2;
            }
            return (uint64_t)(uint32_t)rbp_3;
        }
        break;
      case 2U:
      case 1U:
        {
            switch (loop_state_var) {
              case 1U:
                {
                    var_60 = *(uint64_t *)(local_sp_1 + 24UL);
                    var_61 = helper_cc_compute_c_wrapper(var_60 - r15_2, r15_2, var_8, 17U);
                    rdi2_1 = rbx_1;
                    local_sp_2 = local_sp_1;
                    local_sp_3 = local_sp_1;
                    if (var_61 != 0UL) {
                        var_62 = r15_2 - var_60;
                        var_63 = rbx_1 + *(uint64_t *)(rbx_1 + 1024UL);
                        var_64 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)4285233UL;
                        var_65 = (uint64_t *)(rbx_1 + 1032UL);
                        rbp_1 = var_62;
                        var_66 = var_63 - rdi2_1;
                        var_67 = local_sp_2 + (-8L);
                        *(uint64_t *)var_67 = 4205551UL;
                        var_68 = indirect_placeholder_5(var_66, rdi2_1, var_64);
                        local_sp_2 = var_67;
                        while (var_68 != 0UL)
                            {
                                var_69 = var_68 + 1UL;
                                *var_65 = (*var_65 + 1UL);
                                var_70 = rbp_1 + (-1L);
                                rdi2_0 = var_69;
                                rdi2_1 = var_69;
                                rbp_1 = var_70;
                                if (var_70 == 0UL) {
                                    break;
                                }
                                var_66 = var_63 - rdi2_1;
                                var_67 = local_sp_2 + (-8L);
                                *(uint64_t *)var_67 = 4205551UL;
                                var_68 = indirect_placeholder_5(var_66, rdi2_1, var_64);
                                local_sp_2 = var_67;
                            }
                        var_71 = rdi2_0 - rbx_1;
                        var_72 = (uint64_t *)(local_sp_2 + 32UL);
                        *var_72 = (*var_72 + var_71);
                        var_73 = local_sp_2 + (-16L);
                        *(uint64_t *)var_73 = 4205596UL;
                        indirect_placeholder_13(rbx_1, rbx_1, var_71);
                        local_sp_3 = var_73;
                    }
                }
                break;
              case 2U:
                {
                    var_53 = *(uint64_t *)(rbx_0 + 1024UL);
                    var_54 = rbp_0 + var_53;
                    var_55 = local_sp_0 + (-8L);
                    *(uint64_t *)var_55 = 4205458UL;
                    var_56 = indirect_placeholder_14(rbx_0, rbx_0, var_53);
                    var_57 = var_56.field_0;
                    var_58 = r15_1 - *(uint64_t *)(var_57 + 1032UL);
                    var_59 = *(uint64_t *)(var_57 + 1040UL);
                    rbx_1 = var_59;
                    r15_2 = var_58;
                    local_sp_1 = var_55;
                    rbx_0 = var_59;
                    r15_1 = var_58;
                    rbp_0 = var_54;
                    local_sp_0 = var_55;
                    do {
                        var_53 = *(uint64_t *)(rbx_0 + 1024UL);
                        var_54 = rbp_0 + var_53;
                        var_55 = local_sp_0 + (-8L);
                        *(uint64_t *)var_55 = 4205458UL;
                        var_56 = indirect_placeholder_14(rbx_0, rbx_0, var_53);
                        var_57 = var_56.field_0;
                        var_58 = r15_1 - *(uint64_t *)(var_57 + 1032UL);
                        var_59 = *(uint64_t *)(var_57 + 1040UL);
                        rbx_1 = var_59;
                        r15_2 = var_58;
                        local_sp_1 = var_55;
                        rbx_0 = var_59;
                        r15_1 = var_58;
                        rbp_0 = var_54;
                        local_sp_0 = var_55;
                    } while ((var_58 - *(uint64_t *)(var_59 + 1032UL)) <= var_51);
                    *(uint64_t *)(local_sp_0 + 32UL) = var_54;
                }
                break;
            }
        }
        break;
    }
}
