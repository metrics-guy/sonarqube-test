typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_44_ret_type;
struct indirect_placeholder_47_ret_type;
struct indirect_placeholder_45_ret_type;
struct indirect_placeholder_46_ret_type;
struct indirect_placeholder_44_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_47_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_45_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_46_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r9(void);
extern struct indirect_placeholder_44_ret_type indirect_placeholder_44(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_r8(void);
extern struct indirect_placeholder_47_ret_type indirect_placeholder_47(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_45_ret_type indirect_placeholder_45(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_46_ret_type indirect_placeholder_46(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
typedef _Bool bool;
uint64_t bb_head_lines(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_16;
    uint64_t var_17;
    struct indirect_placeholder_44_ret_type var_18;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t rax_0;
    unsigned char var_13;
    uint64_t rbx_0;
    uint64_t rbp_2;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t r9_0;
    uint64_t local_sp_1;
    uint64_t var_21;
    uint64_t var_8;
    uint64_t rbp_0;
    uint64_t r8_0;
    uint64_t rbp_1;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t local_sp_0;
    uint64_t var_23;
    uint64_t var_22;
    uint64_t var_9;
    uint64_t var_10;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_rbp();
    var_4 = init_r12();
    var_5 = init_r8();
    var_6 = init_r9();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_2;
    var_7 = (uint64_t)(uint32_t)rsi;
    rax_0 = 1UL;
    rbx_0 = 0UL;
    rbp_2 = rdx;
    r9_0 = var_6;
    r8_0 = var_5;
    if (rdx == 0UL) {
        return rax_0;
    }
    local_sp_1 = var_0 + (-1208L);
    while (1U)
        {
            var_8 = local_sp_1 + 144UL;
            var_9 = local_sp_1 + (-8L);
            *(uint64_t *)var_9 = 4206151UL;
            var_10 = indirect_placeholder_5(1024UL, var_7, var_8);
            rbp_1 = rbp_2;
            switch_state_var = 0;
            switch (var_10) {
              case 18446744073709551615UL:
                {
                    *(uint64_t *)(local_sp_1 + (-16L)) = 4205928UL;
                    var_11 = indirect_placeholder(4UL, rdi);
                    *(uint64_t *)(local_sp_1 + (-24L)) = 4205936UL;
                    indirect_placeholder_1();
                    var_12 = (uint64_t)*(uint32_t *)var_11;
                    *(uint64_t *)(local_sp_1 + (-32L)) = 4205961UL;
                    indirect_placeholder_47(0UL, 4260113UL, 0UL, var_12, var_11, r8_0, r9_0);
                    rax_0 = 0UL;
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 0UL:
                {
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
              default:
                {
                    var_13 = *(unsigned char *)4285233UL;
                    while (1U)
                        {
                            var_14 = rbx_0 + 1UL;
                            rbp_0 = rbp_1;
                            rbx_0 = var_14;
                            if ((uint64_t)(*(unsigned char *)((var_14 + var_9) + 143UL) - var_13) == 0UL) {
                                rbp_1 = rbp_0;
                                rbp_2 = rbp_0;
                                if (var_14 == var_10) {
                                    continue;
                                }
                                loop_state_var = 2U;
                                break;
                            }
                            var_15 = rbp_1 + (-1L);
                            rbp_0 = var_15;
                            if (var_15 != 0UL) {
                                var_19 = var_14 - var_10;
                                var_20 = local_sp_1 + (-16L);
                                *(uint64_t *)var_20 = 4206078UL;
                                indirect_placeholder_1();
                                local_sp_0 = var_20;
                                if ((long)var_10 >= (long)0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                var_21 = local_sp_1 + (-24L);
                                *(uint64_t *)var_21 = 4205991UL;
                                indirect_placeholder_1();
                                local_sp_0 = var_21;
                                if ((uint64_t)(uint32_t)var_10 != 0UL) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                if ((uint32_t)((uint16_t)*(uint32_t *)local_sp_1 & (unsigned short)61440U) != 32768U) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                loop_state_var = 0U;
                                break;
                            }
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 2U:
                        {
                            var_16 = local_sp_1 + 136UL;
                            var_17 = local_sp_1 + (-16L);
                            *(uint64_t *)var_17 = 4206125UL;
                            var_18 = indirect_placeholder_44(var_10, var_16, var_10);
                            local_sp_1 = var_17;
                            if (rbp_0 != 0UL) {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            r8_0 = var_18.field_5;
                            r9_0 = var_18.field_6;
                            continue;
                        }
                        break;
                      case 0U:
                        {
                            var_22 = local_sp_1 + (-32L);
                            *(uint64_t *)var_22 = 4206030UL;
                            indirect_placeholder_46(1UL, var_7, var_19, rdi);
                            local_sp_0 = var_22;
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 1U:
                        {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    var_23 = local_sp_0 + 144UL;
    *(uint64_t *)(local_sp_0 + (-8L)) = 4206099UL;
    indirect_placeholder_45(var_14, var_23, var_14);
}
