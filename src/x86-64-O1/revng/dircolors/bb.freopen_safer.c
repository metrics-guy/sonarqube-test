typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_rax(void);
uint64_t bb_freopen_safer(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t rbx_3;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint32_t var_9;
    uint64_t local_sp_10;
    uint64_t rax_5;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t storemerge2;
    uint64_t local_sp_3;
    uint64_t local_sp_0;
    uint64_t r15_55465;
    uint64_t rax_0;
    uint64_t r14_15663;
    uint64_t r13_0;
    uint64_t r15_3;
    uint64_t rbx_0;
    uint64_t rbp_3;
    uint64_t local_sp_8;
    uint64_t r15_0;
    uint64_t rbp_0;
    uint64_t local_sp_1;
    uint64_t rbx_2;
    uint64_t rax_1;
    uint64_t r15_2;
    uint64_t rbx_1;
    uint64_t rbp_2;
    uint64_t r15_1;
    uint64_t rbp_1;
    uint64_t local_sp_2;
    uint64_t local_sp_6;
    uint64_t var_30;
    uint64_t rax_2;
    uint64_t var_29;
    uint64_t rax_6;
    uint64_t local_sp_4;
    uint64_t rax_3;
    uint64_t r13_1;
    uint64_t var_28;
    uint64_t r13_35267;
    uint64_t r14_2;
    uint64_t local_sp_5;
    uint64_t rax_4;
    uint64_t r13_2;
    uint64_t r15_4;
    uint64_t r14_0;
    uint64_t storemerge;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t local_sp_9;
    uint64_t var_10;
    uint64_t local_sp_7;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t rax_7;
    uint64_t var_25;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t storemerge1;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r15();
    var_5 = init_rbp();
    var_6 = init_r12();
    var_7 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_4;
    *(uint64_t *)(var_0 + (-16L)) = var_7;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_6;
    *(uint64_t *)(var_0 + (-40L)) = var_5;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_8 = var_0 + (-64L);
    *(uint64_t *)var_8 = 4208299UL;
    indirect_placeholder();
    var_9 = (uint32_t)var_1;
    rbx_3 = 0UL;
    local_sp_10 = var_8;
    rax_5 = var_1;
    storemerge2 = 0UL;
    r15_55465 = 0UL;
    r14_15663 = 0UL;
    local_sp_8 = var_8;
    rbx_2 = 0UL;
    r13_35267 = 0UL;
    r14_2 = 0UL;
    storemerge = 0UL;
    local_sp_9 = var_8;
    rax_7 = var_1;
    storemerge1 = 0UL;
    if ((uint64_t)(var_9 + (-1)) != 0UL) {
        if ((uint64_t)(var_9 + (-2)) != 0UL) {
            if ((uint64_t)var_9 != 0UL) {
                var_25 = local_sp_8 + (-8L);
                *(uint64_t *)var_25 = 4208442UL;
                indirect_placeholder();
                local_sp_5 = var_25;
                rax_4 = rax_7;
                r13_2 = r13_35267;
                r15_4 = r15_55465;
                r14_0 = r14_15663;
                storemerge = rax_7;
                var_26 = local_sp_5 + (-8L);
                *(uint64_t *)var_26 = 4208450UL;
                indirect_placeholder();
                var_27 = (uint64_t)*(uint32_t *)rax_4;
                rbx_3 = storemerge;
                local_sp_0 = var_26;
                rax_0 = rax_4;
                r13_0 = r13_2;
                r15_3 = r15_4;
                rbx_0 = storemerge;
                rbp_3 = var_27;
                r15_0 = r15_4;
                rbp_0 = var_27;
                local_sp_4 = var_26;
                rax_3 = rax_4;
                r13_1 = r13_2;
                if ((uint64_t)(unsigned char)r14_0 == 0UL) {
                    var_28 = local_sp_4 + (-8L);
                    *(uint64_t *)var_28 = 4208677UL;
                    indirect_placeholder();
                    local_sp_0 = var_28;
                    rax_0 = rax_3;
                    r13_0 = r13_1;
                    rbx_0 = rbx_3;
                    r15_0 = r15_3;
                    rbp_0 = rbp_3;
                }
                local_sp_3 = local_sp_0;
                local_sp_1 = local_sp_0;
                rbx_2 = rbx_0;
                rax_1 = rax_0;
                r15_2 = r15_0;
                rbx_1 = rbx_0;
                rbp_2 = rbp_0;
                r15_1 = r15_0;
                rbp_1 = rbp_0;
                rax_2 = rax_0;
                if ((uint64_t)(unsigned char)r13_0 == 0UL) {
                    var_29 = local_sp_3 + (-8L);
                    *(uint64_t *)var_29 = 4208616UL;
                    indirect_placeholder();
                    local_sp_1 = var_29;
                    rax_1 = rax_2;
                    rbx_1 = rbx_2;
                    r15_1 = r15_2;
                    rbp_1 = rbp_2;
                }
                local_sp_2 = local_sp_1;
                if ((uint64_t)(unsigned char)r15_1 == 0UL) {
                    var_30 = local_sp_1 + (-8L);
                    *(uint64_t *)var_30 = 4208638UL;
                    indirect_placeholder();
                    local_sp_2 = var_30;
                }
                if (rbx_1 == 0UL) {
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4208648UL;
                    indirect_placeholder();
                    *(uint32_t *)rax_1 = (uint32_t)rbp_1;
                }
                return rbx_1;
            }
            var_10 = var_0 + (-72L);
            *(uint64_t *)var_10 = 4208336UL;
            indirect_placeholder();
            local_sp_9 = var_10;
            storemerge1 = (var_7 & (-256L)) | 1UL;
        }
        var_11 = local_sp_9 + (-8L);
        *(uint64_t *)var_11 = 4208366UL;
        indirect_placeholder();
        local_sp_10 = var_11;
        r14_2 = storemerge1;
        storemerge2 = (var_2 & (-256L)) | 1UL;
    }
    var_12 = local_sp_10 + (-8L);
    *(uint64_t *)var_12 = 4208388UL;
    indirect_placeholder();
    var_13 = (uint64_t)var_9;
    var_14 = (var_4 & (-256L)) | (var_13 != 0UL);
    r15_55465 = var_14;
    r14_15663 = r14_2;
    r15_3 = var_14;
    r15_2 = var_14;
    local_sp_6 = var_12;
    r13_1 = storemerge2;
    r13_35267 = storemerge2;
    r13_2 = storemerge2;
    r15_4 = var_14;
    r14_0 = r14_2;
    if (var_13 != 0UL) {
        var_15 = local_sp_10 + (-16L);
        *(uint64_t *)var_15 = 4208533UL;
        var_16 = indirect_placeholder_5(0UL);
        local_sp_5 = var_15;
        rax_4 = var_16;
        local_sp_6 = var_15;
        rax_5 = var_16;
        if ((uint64_t)(unsigned char)var_16 != 0UL) {
            var_26 = local_sp_5 + (-8L);
            *(uint64_t *)var_26 = 4208450UL;
            indirect_placeholder();
            var_27 = (uint64_t)*(uint32_t *)rax_4;
            rbx_3 = storemerge;
            local_sp_0 = var_26;
            rax_0 = rax_4;
            r13_0 = r13_2;
            r15_3 = r15_4;
            rbx_0 = storemerge;
            rbp_3 = var_27;
            r15_0 = r15_4;
            rbp_0 = var_27;
            local_sp_4 = var_26;
            rax_3 = rax_4;
            r13_1 = r13_2;
            if ((uint64_t)(unsigned char)r14_0 == 0UL) {
                var_28 = local_sp_4 + (-8L);
                *(uint64_t *)var_28 = 4208677UL;
                indirect_placeholder();
                local_sp_0 = var_28;
                rax_0 = rax_3;
                r13_0 = r13_1;
                rbx_0 = rbx_3;
                r15_0 = r15_3;
                rbp_0 = rbp_3;
            }
            local_sp_3 = local_sp_0;
            local_sp_1 = local_sp_0;
            rbx_2 = rbx_0;
            rax_1 = rax_0;
            r15_2 = r15_0;
            rbx_1 = rbx_0;
            rbp_2 = rbp_0;
            r15_1 = r15_0;
            rbp_1 = rbp_0;
            rax_2 = rax_0;
            if ((uint64_t)(unsigned char)r13_0 == 0UL) {
                var_29 = local_sp_3 + (-8L);
                *(uint64_t *)var_29 = 4208616UL;
                indirect_placeholder();
                local_sp_1 = var_29;
                rax_1 = rax_2;
                rbx_1 = rbx_2;
                r15_1 = r15_2;
                rbp_1 = rbp_2;
            }
            local_sp_2 = local_sp_1;
            if ((uint64_t)(unsigned char)r15_1 == 0UL) {
                var_30 = local_sp_1 + (-8L);
                *(uint64_t *)var_30 = 4208638UL;
                indirect_placeholder();
                local_sp_2 = var_30;
            }
            if (rbx_1 == 0UL) {
                *(uint64_t *)(local_sp_2 + (-8L)) = 4208648UL;
                indirect_placeholder();
                *(uint32_t *)rax_1 = (uint32_t)rbp_1;
            }
            return rbx_1;
        }
    }
    local_sp_7 = local_sp_6;
    rax_6 = rax_5;
    if ((uint64_t)(unsigned char)storemerge2 != 0UL) {
        var_17 = local_sp_6 + (-8L);
        *(uint64_t *)var_17 = 4208581UL;
        var_18 = indirect_placeholder_5(1UL);
        rax_2 = var_18;
        rax_3 = var_18;
        local_sp_7 = var_17;
        rax_6 = var_18;
        if ((uint64_t)(unsigned char)var_18 != 0UL) {
            var_19 = local_sp_6 + (-16L);
            *(uint64_t *)var_19 = 4208594UL;
            indirect_placeholder();
            var_20 = (uint64_t)*(uint32_t *)var_18;
            local_sp_3 = var_19;
            rbp_2 = var_20;
            local_sp_4 = var_19;
            rbp_3 = var_20;
            if ((uint64_t)(unsigned char)r14_2 != 0UL) {
                var_29 = local_sp_3 + (-8L);
                *(uint64_t *)var_29 = 4208616UL;
                indirect_placeholder();
                local_sp_1 = var_29;
                rax_1 = rax_2;
                rbx_1 = rbx_2;
                r15_1 = r15_2;
                rbp_1 = rbp_2;
                local_sp_2 = local_sp_1;
                if ((uint64_t)(unsigned char)r15_1 == 0UL) {
                    var_30 = local_sp_1 + (-8L);
                    *(uint64_t *)var_30 = 4208638UL;
                    indirect_placeholder();
                    local_sp_2 = var_30;
                }
                if (rbx_1 == 0UL) {
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4208648UL;
                    indirect_placeholder();
                    *(uint32_t *)rax_1 = (uint32_t)rbp_1;
                }
                return rbx_1;
            }
            var_28 = local_sp_4 + (-8L);
            *(uint64_t *)var_28 = 4208677UL;
            indirect_placeholder();
            local_sp_0 = var_28;
            rax_0 = rax_3;
            r13_0 = r13_1;
            rbx_0 = rbx_3;
            r15_0 = r15_3;
            rbp_0 = rbp_3;
            local_sp_3 = local_sp_0;
            local_sp_1 = local_sp_0;
            rbx_2 = rbx_0;
            rax_1 = rax_0;
            r15_2 = r15_0;
            rbx_1 = rbx_0;
            rbp_2 = rbp_0;
            r15_1 = r15_0;
            rbp_1 = rbp_0;
            rax_2 = rax_0;
            if ((uint64_t)(unsigned char)r13_0 == 0UL) {
                var_29 = local_sp_3 + (-8L);
                *(uint64_t *)var_29 = 4208616UL;
                indirect_placeholder();
                local_sp_1 = var_29;
                rax_1 = rax_2;
                rbx_1 = rbx_2;
                r15_1 = r15_2;
                rbp_1 = rbp_2;
            }
            local_sp_2 = local_sp_1;
            if ((uint64_t)(unsigned char)r15_1 == 0UL) {
                var_30 = local_sp_1 + (-8L);
                *(uint64_t *)var_30 = 4208638UL;
                indirect_placeholder();
                local_sp_2 = var_30;
            }
            if (rbx_1 == 0UL) {
                *(uint64_t *)(local_sp_2 + (-8L)) = 4208648UL;
                indirect_placeholder();
                *(uint32_t *)rax_1 = (uint32_t)rbp_1;
            }
            return rbx_1;
        }
    }
    local_sp_8 = local_sp_7;
    rax_7 = rax_6;
    if ((uint64_t)(unsigned char)r14_2 == 0UL) {
        var_21 = local_sp_7 + (-8L);
        *(uint64_t *)var_21 = 4208420UL;
        var_22 = indirect_placeholder_5(2UL);
        rax_3 = var_22;
        local_sp_8 = var_21;
        rax_7 = var_22;
        if ((uint64_t)(unsigned char)var_22 == 0UL) {
            var_23 = local_sp_7 + (-16L);
            *(uint64_t *)var_23 = 4208660UL;
            indirect_placeholder();
            var_24 = (uint64_t)*(uint32_t *)var_22;
            local_sp_4 = var_23;
            rbp_3 = var_24;
            var_28 = local_sp_4 + (-8L);
            *(uint64_t *)var_28 = 4208677UL;
            indirect_placeholder();
            local_sp_0 = var_28;
            rax_0 = rax_3;
            r13_0 = r13_1;
            rbx_0 = rbx_3;
            r15_0 = r15_3;
            rbp_0 = rbp_3;
        } else {
            var_25 = local_sp_8 + (-8L);
            *(uint64_t *)var_25 = 4208442UL;
            indirect_placeholder();
            local_sp_5 = var_25;
            rax_4 = rax_7;
            r13_2 = r13_35267;
            r15_4 = r15_55465;
            r14_0 = r14_15663;
            storemerge = rax_7;
            var_26 = local_sp_5 + (-8L);
            *(uint64_t *)var_26 = 4208450UL;
            indirect_placeholder();
            var_27 = (uint64_t)*(uint32_t *)rax_4;
            rbx_3 = storemerge;
            local_sp_0 = var_26;
            rax_0 = rax_4;
            r13_0 = r13_2;
            r15_3 = r15_4;
            rbx_0 = storemerge;
            rbp_3 = var_27;
            r15_0 = r15_4;
            rbp_0 = var_27;
            local_sp_4 = var_26;
            rax_3 = rax_4;
            r13_1 = r13_2;
            if ((uint64_t)(unsigned char)r14_0 == 0UL) {
                var_28 = local_sp_4 + (-8L);
                *(uint64_t *)var_28 = 4208677UL;
                indirect_placeholder();
                local_sp_0 = var_28;
                rax_0 = rax_3;
                r13_0 = r13_1;
                rbx_0 = rbx_3;
                r15_0 = r15_3;
                rbp_0 = rbp_3;
            }
        }
    } else {
        var_25 = local_sp_8 + (-8L);
        *(uint64_t *)var_25 = 4208442UL;
        indirect_placeholder();
        local_sp_5 = var_25;
        rax_4 = rax_7;
        r13_2 = r13_35267;
        r15_4 = r15_55465;
        r14_0 = r14_15663;
        storemerge = rax_7;
        var_26 = local_sp_5 + (-8L);
        *(uint64_t *)var_26 = 4208450UL;
        indirect_placeholder();
        var_27 = (uint64_t)*(uint32_t *)rax_4;
        rbx_3 = storemerge;
        local_sp_0 = var_26;
        rax_0 = rax_4;
        r13_0 = r13_2;
        r15_3 = r15_4;
        rbx_0 = storemerge;
        rbp_3 = var_27;
        r15_0 = r15_4;
        rbp_0 = var_27;
        local_sp_4 = var_26;
        rax_3 = rax_4;
        r13_1 = r13_2;
        if ((uint64_t)(unsigned char)r14_0 == 0UL) {
            var_28 = local_sp_4 + (-8L);
            *(uint64_t *)var_28 = 4208677UL;
            indirect_placeholder();
            local_sp_0 = var_28;
            rax_0 = rax_3;
            r13_0 = r13_1;
            rbx_0 = rbx_3;
            r15_0 = r15_3;
            rbp_0 = rbp_3;
        }
    }
    local_sp_3 = local_sp_0;
    local_sp_1 = local_sp_0;
    rbx_2 = rbx_0;
    rax_1 = rax_0;
    r15_2 = r15_0;
    rbx_1 = rbx_0;
    rbp_2 = rbp_0;
    r15_1 = r15_0;
    rbp_1 = rbp_0;
    rax_2 = rax_0;
    if ((uint64_t)(unsigned char)r13_0 == 0UL) {
        var_29 = local_sp_3 + (-8L);
        *(uint64_t *)var_29 = 4208616UL;
        indirect_placeholder();
        local_sp_1 = var_29;
        rax_1 = rax_2;
        rbx_1 = rbx_2;
        r15_1 = r15_2;
        rbp_1 = rbp_2;
    }
    local_sp_2 = local_sp_1;
    if ((uint64_t)(unsigned char)r15_1 == 0UL) {
        var_30 = local_sp_1 + (-8L);
        *(uint64_t *)var_30 = 4208638UL;
        indirect_placeholder();
        local_sp_2 = var_30;
    }
    if (rbx_1 != 0UL) {
        return rbx_1;
    }
    *(uint64_t *)(local_sp_2 + (-8L)) = 4208648UL;
    indirect_placeholder();
    *(uint32_t *)rax_1 = (uint32_t)rbp_1;
    return rbx_1;
}
