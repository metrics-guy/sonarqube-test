typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct helper_pxor_xmm_wrapper_85_ret_type;
struct type_4;
struct type_6;
struct helper_ucomisd_wrapper_ret_type;
struct helper_cvtsi2sd_wrapper_ret_type;
struct helper_comisd_wrapper_130_ret_type;
struct helper_pxor_xmm_wrapper_106_ret_type;
struct helper_cvtsi2sd_wrapper_107_ret_type;
struct helper_divsd_wrapper_127_ret_type;
struct helper_comisd_wrapper_128_ret_type;
struct helper_divsd_wrapper_129_ret_type;
struct helper_mulsd_wrapper_131_ret_type;
struct helper_divsd_wrapper_132_ret_type;
struct helper_ucomisd_wrapper_133_ret_type;
struct helper_ucomisd_wrapper_134_ret_type;
struct helper_pxor_xmm_wrapper_85_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_4 {
};
struct type_6 {
};
struct helper_ucomisd_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvtsi2sd_wrapper_ret_type {
    uint64_t field_0;
};
struct helper_comisd_wrapper_130_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_pxor_xmm_wrapper_106_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_cvtsi2sd_wrapper_107_ret_type {
    uint64_t field_0;
};
struct helper_divsd_wrapper_127_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_comisd_wrapper_128_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_divsd_wrapper_129_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_mulsd_wrapper_131_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_divsd_wrapper_132_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_ucomisd_wrapper_133_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_ucomisd_wrapper_134_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rax(void);
extern void indirect_placeholder(void);
extern uint64_t init_cc_src2(void);
extern uint64_t init_state_0x8558(void);
extern unsigned char init_state_0x8549(void);
extern unsigned char init_state_0x854c(void);
extern unsigned char init_state_0x8548(void);
extern unsigned char init_state_0x854b(void);
extern unsigned char init_state_0x8547(void);
extern unsigned char init_state_0x854d(void);
extern uint64_t init_state_0x8560(void);
extern uint64_t init_state_0x8598(void);
extern uint64_t init_state_0x85a0(void);
extern struct helper_pxor_xmm_wrapper_85_ret_type helper_pxor_xmm_wrapper_85(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4);
extern struct helper_ucomisd_wrapper_ret_type helper_ucomisd_wrapper(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct helper_cvtsi2sd_wrapper_ret_type helper_cvtsi2sd_wrapper(struct type_4 *param_0, struct type_6 *param_1, uint32_t param_2);
extern struct helper_comisd_wrapper_130_ret_type helper_comisd_wrapper_130(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_pxor_xmm_wrapper_106_ret_type helper_pxor_xmm_wrapper_106(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4);
extern struct helper_cvtsi2sd_wrapper_107_ret_type helper_cvtsi2sd_wrapper_107(struct type_4 *param_0, struct type_6 *param_1, uint32_t param_2);
extern struct helper_divsd_wrapper_127_ret_type helper_divsd_wrapper_127(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_comisd_wrapper_128_ret_type helper_comisd_wrapper_128(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_divsd_wrapper_129_ret_type helper_divsd_wrapper_129(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_mulsd_wrapper_131_ret_type helper_mulsd_wrapper_131(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_divsd_wrapper_132_ret_type helper_divsd_wrapper_132(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_ucomisd_wrapper_133_ret_type helper_ucomisd_wrapper_133(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_ucomisd_wrapper_134_ret_type helper_ucomisd_wrapper_134(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
void bb_scale_radix_exp(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    unsigned char var_7;
    unsigned char var_8;
    unsigned char var_9;
    unsigned char var_10;
    unsigned char var_11;
    uint64_t var_12;
    uint64_t var_13;
    struct helper_comisd_wrapper_128_ret_type var_24;
    unsigned char var_14;
    struct helper_ucomisd_wrapper_ret_type var_15;
    uint64_t var_16;
    unsigned char var_17;
    uint64_t cc_src_0;
    uint64_t var_18;
    uint64_t var_19;
    struct helper_cvtsi2sd_wrapper_ret_type var_20;
    uint64_t var_21;
    struct helper_divsd_wrapper_127_ret_type var_22;
    uint64_t var_23;
    uint64_t state_0x8558_0;
    uint64_t rsi2_0;
    unsigned char state_0x8549_0;
    struct helper_divsd_wrapper_129_ret_type var_25;
    struct helper_comisd_wrapper_130_ret_type var_26;
    struct helper_mulsd_wrapper_131_ret_type var_27;
    uint64_t var_28;
    uint64_t *var_29;
    struct helper_cvtsi2sd_wrapper_107_ret_type var_30;
    uint64_t var_31;
    struct helper_pxor_xmm_wrapper_85_ret_type var_32;
    uint64_t var_33;
    unsigned char state_0x8549_2;
    uint64_t rsi2_1_in;
    unsigned char state_0x8549_1;
    uint64_t rsi2_1;
    struct helper_divsd_wrapper_132_ret_type var_34;
    uint64_t var_35;
    unsigned char var_36;
    struct helper_ucomisd_wrapper_133_ret_type var_37;
    uint64_t var_38;
    unsigned char var_39;
    struct helper_ucomisd_wrapper_134_ret_type var_40;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_state_0x8558();
    var_3 = init_state_0x8560();
    var_4 = init_cc_src2();
    var_5 = init_state_0x8598();
    var_6 = init_state_0x85a0();
    var_7 = init_state_0x854c();
    var_8 = init_state_0x8548();
    var_9 = init_state_0x854b();
    var_10 = init_state_0x8547();
    var_11 = init_state_0x854d();
    var_12 = var_0 + (-24L);
    var_13 = rdi + (-2L);
    state_0x8558_0 = var_2;
    rsi2_0 = rsi;
    rsi2_1_in = rsi;
    if ((uint64_t)(uint32_t)var_13 == 0UL) {
        *(uint64_t *)(var_0 + (-32L)) = 4230793UL;
        indirect_placeholder();
        *(uint64_t *)var_12 = var_2;
    } else {
        var_14 = init_state_0x8549();
        var_15 = helper_ucomisd_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(2824UL), *(uint64_t *)4281152UL, var_2, var_14, var_7);
        var_16 = var_15.field_0;
        var_17 = var_15.field_1;
        cc_src_0 = var_16;
        state_0x8549_1 = var_17;
        if ((var_16 & 4UL) != 0UL) {
            var_18 = helper_cc_compute_all_wrapper(var_13, var_16, var_4, 1U);
            cc_src_0 = var_18;
            if ((var_18 & 64UL) != 0UL) {
                *(uint64_t *)(var_0 + (-16L)) = var_2;
                return;
            }
        }
        if ((long)rsi < (long)0UL) {
            var_29 = (uint64_t *)(var_0 + (-16L));
            *var_29 = var_2;
            helper_pxor_xmm_wrapper_106((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(776UL), var_2, var_3);
            var_30 = helper_cvtsi2sd_wrapper_107((struct type_4 *)(0UL), (struct type_6 *)(776UL), (uint32_t)rdi);
            var_31 = var_30.field_0;
            var_32 = helper_pxor_xmm_wrapper_85((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(840UL), var_5, var_6);
            var_33 = var_32.field_0;
            while (1U)
                {
                    rsi2_1 = rsi2_1_in + 1UL;
                    var_34 = helper_divsd_wrapper_132((struct type_4 *)(0UL), (struct type_6 *)(1096UL), (struct type_6 *)(776UL), var_31, *var_29, state_0x8549_1, var_7, var_8, var_9, var_10, var_11);
                    var_35 = var_34.field_0;
                    var_36 = var_34.field_1;
                    *var_29 = var_35;
                    var_37 = helper_ucomisd_wrapper_133((struct type_4 *)(0UL), (struct type_6 *)(1096UL), (struct type_6 *)(840UL), var_33, var_35, var_36, var_7);
                    var_38 = var_37.field_0;
                    var_39 = var_37.field_1;
                    rsi2_1_in = rsi2_1;
                    state_0x8549_2 = var_39;
                    if ((var_38 & 4UL) == 0UL) {
                        state_0x8549_1 = state_0x8549_2;
                        if (rsi2_1 != 0UL) {
                            continue;
                        }
                        break;
                    }
                    var_40 = helper_ucomisd_wrapper_134((struct type_4 *)(0UL), (struct type_6 *)(1096UL), (struct type_6 *)(2824UL), *(uint64_t *)4281152UL, var_35, var_39, var_7);
                    state_0x8549_2 = var_40.field_1;
                    if ((var_40.field_0 & 64UL) != 0UL) {
                        *(uint64_t *)(var_0 + (-32L)) = 4230828UL;
                        indirect_placeholder();
                        *(uint32_t *)var_1 = 34U;
                        break;
                    }
                }
        }
        var_19 = helper_cc_compute_all_wrapper(rsi, cc_src_0, var_4, 25U);
        if ((var_19 & 64UL) != 0UL) {
            helper_pxor_xmm_wrapper_85((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(840UL), var_5, var_6);
            var_20 = helper_cvtsi2sd_wrapper((struct type_4 *)(0UL), (struct type_6 *)(840UL), (uint32_t)rdi);
            var_21 = var_20.field_0;
            var_22 = helper_divsd_wrapper_127((struct type_4 *)(0UL), (struct type_6 *)(968UL), (struct type_6 *)(840UL), *(uint64_t *)4281160UL, var_21, var_17, var_7, var_8, var_9, var_10, var_11);
            var_23 = *(uint64_t *)4281168UL;
            state_0x8549_0 = var_22.field_1;
            while (1U)
                {
                    var_24 = helper_comisd_wrapper_128((struct type_4 *)(0UL), (struct type_6 *)(968UL), (struct type_6 *)(776UL), state_0x8558_0, var_22.field_0, state_0x8549_0, var_7);
                    if ((var_24.field_0 & 65UL) != 0UL) {
                        *(uint64_t *)(var_0 + (-32L)) = 4230841UL;
                        indirect_placeholder();
                        *(uint32_t *)var_1 = 34U;
                        *(uint64_t *)var_12 = *(uint64_t *)4281136UL;
                        break;
                    }
                    var_25 = helper_divsd_wrapper_129((struct type_4 *)(0UL), (struct type_6 *)(904UL), (struct type_6 *)(840UL), var_21, var_23, var_24.field_1, var_7, var_8, var_9, var_10, var_11);
                    var_26 = helper_comisd_wrapper_130((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(904UL), state_0x8558_0, var_25.field_0, var_25.field_1, var_7);
                    if ((var_26.field_0 & 65UL) == 0UL) {
                        var_27 = helper_mulsd_wrapper_131((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(840UL), state_0x8558_0, var_21, var_26.field_1, var_7, var_8, var_9, var_10, var_11);
                        var_28 = rsi2_0 + (-1L);
                        rsi2_0 = var_28;
                        if (var_28 == 0UL) {
                            state_0x8558_0 = var_27.field_0;
                            state_0x8549_0 = var_27.field_1;
                            continue;
                        }
                        *(uint64_t *)(var_0 + (-16L)) = var_27.field_0;
                        break;
                    }
                    *(uint64_t *)(var_0 + (-32L)) = 4230868UL;
                    indirect_placeholder();
                    *(uint32_t *)var_1 = 34U;
                    *(uint64_t *)var_12 = *(uint64_t *)4281144UL;
                    break;
                }
        }
        *(uint64_t *)(var_0 + (-16L)) = var_2;
    }
}
