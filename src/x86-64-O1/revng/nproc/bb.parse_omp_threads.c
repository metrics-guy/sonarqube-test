typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
uint64_t bb_parse_omp_threads(uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t r12_0;
    uint64_t rbx_0_in;
    unsigned char var_23;
    uint64_t local_sp_0;
    uint64_t rbp_1;
    unsigned char var_19;
    unsigned char rbp_0_in;
    uint64_t rbx_0;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_10;
    unsigned char var_11;
    bool var_12;
    uint64_t var_13;
    uint64_t local_sp_2;
    uint64_t local_sp_1;
    unsigned char var_5;
    uint64_t var_6;
    uint64_t rbx_1;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t rbp_2;
    uint64_t var_14;
    uint64_t *var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r12();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    var_4 = var_0 + (-40L);
    local_sp_1 = var_4;
    rbx_1 = rdi;
    local_sp_2 = var_4;
    r12_0 = 0UL;
    if (rdi == 0UL) {
        return r12_0;
    }
    var_5 = *(unsigned char *)rdi;
    var_6 = (uint64_t)var_5;
    rbp_1 = var_6;
    rbp_2 = var_6;
    if (var_5 == '\x00') {
        var_7 = (uint64_t)(uint32_t)((int)((uint32_t)rbp_1 << 24U) >> (int)24U);
        var_8 = local_sp_1 + (-8L);
        *(uint64_t *)var_8 = 4205245UL;
        var_9 = indirect_placeholder_2(var_7);
        local_sp_1 = var_8;
        local_sp_2 = var_8;
        rbp_2 = rbp_1;
        while ((uint64_t)(unsigned char)var_9 != 0UL)
            {
                var_10 = rbx_1 + 1UL;
                var_11 = *(unsigned char *)var_10;
                var_12 = (var_11 == '\x00');
                var_13 = (uint64_t)var_11;
                rbx_1 = var_10;
                rbp_1 = var_13;
                rbp_2 = var_13;
                if (var_12) {
                    break;
                }
                var_7 = (uint64_t)(uint32_t)((int)((uint32_t)rbp_1 << 24U) >> (int)24U);
                var_8 = local_sp_1 + (-8L);
                *(uint64_t *)var_8 = 4205245UL;
                var_9 = indirect_placeholder_2(var_7);
                local_sp_1 = var_8;
                local_sp_2 = var_8;
                rbp_2 = rbp_1;
            }
    }
    var_14 = (uint64_t)(uint32_t)((int)((uint32_t)rbp_2 << 24U) >> (int)24U);
    var_15 = (uint64_t *)(local_sp_2 + (-8L));
    *var_15 = 4205270UL;
    var_16 = indirect_placeholder_2(var_14);
    if ((uint64_t)(unsigned char)var_16 == 0UL) {
        return;
    }
    *(uint64_t *)local_sp_2 = 0UL;
    var_17 = local_sp_2 + (-16L);
    *(uint64_t *)var_17 = 4205319UL;
    indirect_placeholder();
    var_18 = *var_15;
    local_sp_0 = var_17;
    rbx_0_in = var_18;
    r12_0 = var_16;
    if (var_18 == 0UL) {
        var_19 = *(unsigned char *)var_18;
        rbp_0_in = var_19;
        if (var_19 != '\x00') {
            while (1U)
                {
                    rbx_0 = rbx_0_in + 1UL;
                    var_20 = (uint64_t)(uint32_t)(uint64_t)rbp_0_in;
                    var_21 = local_sp_0 + (-8L);
                    *(uint64_t *)var_21 = 4205353UL;
                    var_22 = indirect_placeholder_2(var_20);
                    local_sp_0 = var_21;
                    rbx_0_in = rbx_0;
                    if ((uint64_t)(unsigned char)var_22 == 0UL) {
                        *(uint64_t *)local_sp_0 = rbx_0;
                        var_23 = *(unsigned char *)rbx_0;
                        rbp_0_in = var_23;
                        if (var_23 == '\x00') {
                            continue;
                        }
                        loop_state_var = 0U;
                        break;
                    }
                    if (rbp_0_in != ',') {
                        loop_state_var = 1U;
                        break;
                    }
                    loop_state_var = 0U;
                    break;
                }
            r12_0 = 0UL;
        }
    } else {
        r12_0 = 0UL;
    }
}
