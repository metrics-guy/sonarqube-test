typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r15(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_4(uint64_t param_0);
typedef _Bool bool;
uint64_t bb_verrevcmp(uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t rcx) {
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t rbp_0;
    uint64_t r14_1;
    uint64_t r15_0;
    uint64_t local_sp_6;
    uint64_t r15_1;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t local_sp_1;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint32_t *var_23;
    uint32_t var_24;
    uint64_t var_25;
    uint32_t var_53;
    uint64_t local_sp_3;
    unsigned char *_pre_phi96;
    unsigned char *_pre95;
    uint64_t rax_0;
    uint64_t local_sp_0;
    uint32_t var_54;
    uint64_t rbx_0;
    uint64_t rbp_1;
    uint64_t rbp_2;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t r14_0;
    uint64_t local_sp_2;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t local_sp_4;
    unsigned char *var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t local_sp_5;
    unsigned char *var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t rbx_1;
    uint64_t rbx_2;
    uint64_t var_30;
    uint64_t *var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_8 = var_0 + (-104L);
    *(uint64_t *)(var_0 + (-80L)) = rdx;
    *(uint64_t *)(var_0 + (-88L)) = rcx;
    *(uint32_t *)(var_0 + (-68L)) = 0U;
    rbp_0 = 0UL;
    r15_0 = 0UL;
    r15_1 = 0UL;
    r14_0 = 0UL;
    local_sp_2 = var_8;
    while (1U)
        {
            var_9 = helper_cc_compute_c_wrapper(r14_0 - rsi, rsi, var_7, 17U);
            rbx_0 = r14_0;
            local_sp_3 = local_sp_2;
            if (var_9 != 0UL) {
                var_10 = *(uint64_t *)(local_sp_2 + 16UL);
                var_11 = helper_cc_compute_c_wrapper(rbp_0 - var_10, var_10, var_7, 17U);
                if (var_11 == 0UL) {
                    break;
                }
            }
            var_12 = rbp_0 - r14_0;
            var_13 = var_12 + *(uint64_t *)(local_sp_2 + 24UL);
            r15_0 = 1UL;
            while (1U)
                {
                    var_14 = var_12 + rbx_0;
                    var_15 = helper_cc_compute_c_wrapper(rbx_0 - rsi, rsi, var_7, 17U);
                    rbp_1 = var_14;
                    rbp_2 = var_14;
                    local_sp_4 = local_sp_3;
                    rbx_1 = rbx_0;
                    rbx_2 = rbx_0;
                    if (var_15 != 0UL) {
                        var_16 = (unsigned char *)(rbx_0 + rdi);
                        var_17 = (uint64_t)(uint32_t)(uint64_t)*var_16;
                        var_18 = local_sp_3 + (-8L);
                        *(uint64_t *)var_18 = 4241115UL;
                        var_19 = indirect_placeholder_4(var_17);
                        local_sp_4 = var_18;
                        if ((uint64_t)(unsigned char)var_19 != 0UL) {
                            var_20 = (uint64_t)*var_16;
                            var_21 = local_sp_3 + (-16L);
                            *(uint64_t *)var_21 = 4241129UL;
                            var_22 = indirect_placeholder_4(var_20);
                            var_23 = (uint32_t *)var_18;
                            var_24 = (uint32_t)var_22;
                            *var_23 = var_24;
                            var_25 = (uint64_t)*(uint32_t *)(local_sp_3 + 20UL);
                            var_53 = var_24;
                            rax_0 = var_25;
                            local_sp_0 = var_21;
                            local_sp_1 = var_21;
                            if (*(uint64_t *)local_sp_3 != var_14) {
                                _pre95 = (unsigned char *)(rbx_0 + var_13);
                                _pre_phi96 = _pre95;
                                var_50 = (uint64_t)*_pre_phi96;
                                var_51 = local_sp_1 + (-8L);
                                *(uint64_t *)var_51 = 4241086UL;
                                var_52 = indirect_placeholder_4(var_50);
                                var_53 = *(uint32_t *)local_sp_1;
                                rax_0 = var_52;
                                local_sp_0 = var_51;
                            }
                            var_54 = (uint32_t)rax_0;
                            local_sp_3 = local_sp_0;
                            if ((uint64_t)(var_54 - var_53) == 0UL) {
                                rbx_0 = rbx_0 + 1UL;
                                continue;
                            }
                            r15_0 = (uint64_t)(var_53 - var_54);
                            loop_state_var = 1U;
                            break;
                        }
                    }
                    local_sp_5 = local_sp_4;
                    if (*(uint64_t *)(local_sp_4 + 16UL) <= var_14) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_26 = (unsigned char *)(rbx_0 + var_13);
                    var_27 = (uint64_t)(uint32_t)(uint64_t)*var_26;
                    var_28 = local_sp_4 + (-8L);
                    *(uint64_t *)var_28 = 4241053UL;
                    var_29 = indirect_placeholder_4(var_27);
                    _pre_phi96 = var_26;
                    local_sp_1 = var_28;
                    local_sp_5 = var_28;
                    if ((uint64_t)(unsigned char)var_29 != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    if (rbx_0 == rsi) {
                        *(uint32_t *)local_sp_4 = *(uint32_t *)(local_sp_4 + 28UL);
                    } else {
                        var_47 = (uint64_t)*(unsigned char *)(rbx_0 + rdi);
                        var_48 = local_sp_4 + (-16L);
                        *(uint64_t *)var_48 = 4241072UL;
                        var_49 = indirect_placeholder_4(var_47);
                        *(uint32_t *)var_28 = (uint32_t)var_49;
                        local_sp_1 = var_48;
                    }
                    var_50 = (uint64_t)*_pre_phi96;
                    var_51 = local_sp_1 + (-8L);
                    *(uint64_t *)var_51 = 4241086UL;
                    var_52 = indirect_placeholder_4(var_50);
                    var_53 = *(uint32_t *)local_sp_1;
                    rax_0 = var_52;
                    local_sp_0 = var_51;
                    var_54 = (uint32_t)rax_0;
                    local_sp_3 = local_sp_0;
                    if ((uint64_t)(var_54 - var_53) == 0UL) {
                        rbx_0 = rbx_0 + 1UL;
                        continue;
                    }
                    r15_0 = (uint64_t)(var_53 - var_54);
                    loop_state_var = 1U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 0U:
                {
                    local_sp_6 = local_sp_5;
                    if (*(unsigned char *)(rbx_0 + rdi) == '0') {
                        var_30 = rbx_1 + 1UL;
                        rbx_1 = var_30;
                        rbx_2 = var_30;
                        do {
                            var_30 = rbx_1 + 1UL;
                            rbx_1 = var_30;
                            rbx_2 = var_30;
                        } while (*(unsigned char *)(var_30 + rdi) != '0');
                    }
                    var_31 = (uint64_t *)(local_sp_5 + 24UL);
                    var_32 = *var_31;
                    r14_1 = rbx_2;
                    if (*(unsigned char *)(var_14 + var_32) == '0') {
                        var_33 = rbp_1 + 1UL;
                        rbp_1 = var_33;
                        rbp_2 = var_33;
                        do {
                            var_33 = rbp_1 + 1UL;
                            rbp_1 = var_33;
                            rbp_2 = var_33;
                        } while (*(unsigned char *)(var_33 + var_32) != '0');
                    }
                    var_34 = rbp_2 - rbx_2;
                    *(uint64_t *)(local_sp_5 + 8UL) = var_34;
                    *(uint64_t *)(local_sp_5 + 40UL) = (*var_31 + var_34);
                    while (1U)
                        {
                            var_35 = *(uint64_t *)(local_sp_6 + 8UL);
                            var_36 = (uint64_t)*(unsigned char *)(r14_1 + rdi);
                            var_37 = (uint64_t)(uint32_t)var_36;
                            *(uint64_t *)(local_sp_6 + (-8L)) = 4241256UL;
                            var_38 = indirect_placeholder_4(var_37);
                            r14_0 = r14_1;
                            if ((uint64_t)(unsigned char)var_38 != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            var_39 = (uint64_t)*(unsigned char *)(r14_1 + *(uint64_t *)(local_sp_6 + 32UL));
                            var_40 = (uint64_t)(uint32_t)var_39;
                            var_41 = local_sp_6 + (-16L);
                            *(uint64_t *)var_41 = 4241277UL;
                            var_42 = indirect_placeholder_4(var_40);
                            local_sp_6 = var_41;
                            if ((uint64_t)(unsigned char)var_42 != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            r15_1 = (uint64_t)(uint32_t)((r15_1 == 0UL) ? (var_36 - var_39) : r15_1);
                            r14_1 = r14_1 + 1UL;
                            continue;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 1U:
                        {
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 0U:
                        {
                            var_43 = var_35 + r14_1;
                            var_44 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(var_43 + *(uint64_t *)(local_sp_6 + 16UL));
                            var_45 = local_sp_6 + (-16L);
                            *(uint64_t *)var_45 = 4241306UL;
                            var_46 = indirect_placeholder_4(var_44);
                            rbp_0 = var_43;
                            local_sp_2 = var_45;
                            r15_0 = 4294967295UL;
                            if ((uint64_t)(unsigned char)var_46 == 0UL) {
                                switch_state_var = 1;
                                break;
                            }
                            r15_0 = r15_1;
                            if (r15_1 != 0UL) {
                                continue;
                            }
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    return (uint64_t)(uint32_t)r15_0;
}
