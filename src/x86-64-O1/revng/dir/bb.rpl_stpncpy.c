typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
uint64_t bb_rpl_stpncpy(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t rdi2_0;
    uint64_t var_0;
    uint64_t rsi3_1;
    uint64_t rax_0;
    uint64_t rsi3_0;
    unsigned char var_1;
    uint64_t rdx1_1;
    unsigned char var_2;
    uint64_t rdi2_1;
    unsigned char var_3;
    uint64_t var_4;
    unsigned char var_5;
    uint64_t var_6;
    uint64_t rax_1;
    uint64_t var_12;
    uint64_t var_7;
    uint64_t rax_3;
    uint64_t rdx1_0;
    uint64_t rsi3_2;
    uint64_t var_8;
    unsigned char var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t rdi2_2;
    uint64_t rax_2_in;
    uint64_t rax_2;
    unsigned int loop_state_var;
    revng_init_local_sp(0UL);
    rax_0 = rdi;
    rsi3_0 = rsi;
    rdi2_0 = rdi;
    rsi3_1 = rsi;
    if (rdx > 3UL) {
        var_7 = rdx & 3UL;
        rdx1_0 = var_7;
        rdi2_1 = rdi2_0;
        rsi3_2 = rsi3_1;
        rax_3 = rdi2_0;
        if (var_7 == 0UL) {
            return rax_3;
        }
        while (1U)
            {
                var_8 = rsi3_2 + 1UL;
                var_9 = *(unsigned char *)rsi3_2;
                var_10 = rdx1_0 + (-1L);
                var_11 = rdi2_1 + 1UL;
                *(unsigned char *)rdi2_1 = var_9;
                rdx1_1 = var_10;
                rdi2_1 = var_11;
                rax_3 = var_11;
                rdx1_0 = var_10;
                rsi3_2 = var_8;
                rdi2_2 = var_11;
                if (var_9 != '\x00') {
                    loop_state_var = 1U;
                    break;
                }
                if (var_10 == 0UL) {
                    continue;
                }
                loop_state_var = 0U;
                break;
            }
        switch (loop_state_var) {
          case 0U:
            {
                return rax_3;
            }
            break;
          case 1U:
            {
                rax_2_in = rdx1_1;
                if (rdx1_1 != 0UL) {
                    rax_2 = rax_2_in + (-1L);
                    *(unsigned char *)(rax_2 + rdi2_2) = (unsigned char)'\x00';
                    rax_2_in = rax_2;
                    do {
                        rax_2 = rax_2_in + (-1L);
                        *(unsigned char *)(rax_2 + rdi2_2) = (unsigned char)'\x00';
                        rax_2_in = rax_2;
                    } while (rax_2 != 0UL);
                }
                return rdi2_2 + (-1L);
            }
            break;
        }
    }
    var_0 = (rdx & (-4L)) + rdi;
    rdi2_0 = var_0;
    while (1U)
        {
            var_1 = *(unsigned char *)rsi3_0;
            *(unsigned char *)rax_0 = var_1;
            if (var_1 != '\x00') {
                rax_1 = rax_0 + 1UL;
                loop_state_var = 0U;
                break;
            }
            var_2 = *(unsigned char *)(rsi3_0 + 1UL);
            *(unsigned char *)(rax_0 + 1UL) = var_2;
            if (var_2 != '\x00') {
                rax_1 = rax_0 + 2UL;
                loop_state_var = 0U;
                break;
            }
            var_3 = *(unsigned char *)(rsi3_0 + 2UL);
            *(unsigned char *)(rax_0 + 2UL) = var_3;
            if (var_3 != '\x00') {
                rax_1 = rax_0 + 3UL;
                loop_state_var = 0U;
                break;
            }
            var_4 = rsi3_0 + 4UL;
            var_5 = *(unsigned char *)(rsi3_0 + 3UL);
            var_6 = rax_0 + 4UL;
            *(unsigned char *)(rax_0 + 3UL) = var_5;
            rax_0 = var_6;
            rsi3_0 = var_4;
            rax_1 = var_6;
            rsi3_1 = var_4;
            if (var_5 != '\x00') {
                loop_state_var = 0U;
                break;
            }
            if (var_6 == var_0) {
                continue;
            }
            loop_state_var = 1U;
            break;
        }
    switch (loop_state_var) {
      case 1U:
        {
            break;
        }
        break;
      case 0U:
        {
            var_12 = (rdi - rax_1) + rdx;
            rdx1_1 = var_12;
            rdi2_2 = rax_1;
            rax_2_in = rdx1_1;
            if (rdx1_1 != 0UL) {
                rax_2 = rax_2_in + (-1L);
                *(unsigned char *)(rax_2 + rdi2_2) = (unsigned char)'\x00';
                rax_2_in = rax_2;
                do {
                    rax_2 = rax_2_in + (-1L);
                    *(unsigned char *)(rax_2 + rdi2_2) = (unsigned char)'\x00';
                    rax_2_in = rax_2;
                } while (rax_2 != 0UL);
            }
            return rdi2_2 + (-1L);
        }
        break;
    }
}
