typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_print_file_name_and_frills_ret_type;
struct bb_print_file_name_and_frills_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder_2(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint64_t init_r8(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_r9(void);
extern uint64_t init_r10(void);
struct bb_print_file_name_and_frills_ret_type bb_print_file_name_and_frills(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t local_sp_0;
    uint64_t local_sp_3;
    uint64_t rdi7_0;
    uint64_t rbp_0;
    uint64_t rsi8_0;
    struct bb_print_file_name_and_frills_ret_type mrv;
    struct bb_print_file_name_and_frills_ret_type mrv1;
    uint64_t var_12;
    struct bb_print_file_name_and_frills_ret_type mrv2;
    struct bb_print_file_name_and_frills_ret_type mrv3;
    struct bb_print_file_name_and_frills_ret_type mrv4;
    struct bb_print_file_name_and_frills_ret_type mrv5;
    struct bb_print_file_name_and_frills_ret_type mrv6;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t local_sp_2;
    uint64_t local_sp_1;
    uint64_t r8_0;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t r8_1;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_7;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r9();
    var_2 = init_rbx();
    var_3 = init_rbp();
    var_4 = init_r10();
    var_5 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_6 = var_0 + (-240L);
    *(uint64_t *)var_6 = 4226034UL;
    indirect_placeholder_2();
    local_sp_0 = var_6;
    rdi7_0 = rdi;
    rsi8_0 = 0UL;
    r8_0 = var_5;
    r8_1 = var_5;
    if (*(unsigned char *)4396796UL == '\x00') {
        *(uint64_t *)(var_0 + (-248L)) = 4226220UL;
        indirect_placeholder_3(rdi, var_6, 196UL);
        var_7 = var_0 + (-256L);
        *(uint64_t *)var_7 = 4226257UL;
        indirect_placeholder_2();
        local_sp_0 = var_7;
    }
    local_sp_1 = local_sp_0;
    local_sp_2 = local_sp_0;
    if (*(unsigned char *)4396844UL != '\x00') {
        if (*(unsigned char *)(rdi + 184UL) != '\x00') {
            var_8 = *(uint64_t *)(rdi + 88UL);
            var_9 = *(uint64_t *)4396832UL;
            var_10 = (uint64_t)*(uint32_t *)4396840UL;
            var_11 = local_sp_0 + (-8L);
            *(uint64_t *)var_11 = 4226292UL;
            indirect_placeholder_5(var_10, var_8, local_sp_0, 512UL, var_9);
            local_sp_1 = var_11;
            r8_0 = var_9;
        }
        var_12 = local_sp_1 + (-8L);
        *(uint64_t *)var_12 = 4226108UL;
        indirect_placeholder_2();
        local_sp_2 = var_12;
        r8_1 = r8_0;
    }
    local_sp_3 = local_sp_2;
    if (*(unsigned char *)4396901UL == '\x00') {
        var_13 = local_sp_2 + (-8L);
        *(uint64_t *)var_13 = 4226158UL;
        indirect_placeholder_2();
        local_sp_3 = var_13;
    }
    *(uint64_t *)(local_sp_3 + (-8L)) = 4226179UL;
    var_14 = indirect_placeholder_1(0UL, rdi, 0UL, rsi);
    rbp_0 = var_14;
    if (*(uint32_t *)4396820UL == 0U) {
        mrv.field_0 = var_1;
        mrv1 = mrv;
        mrv1.field_1 = rbp_0;
        mrv2 = mrv1;
        mrv2.field_2 = rdi7_0;
        mrv3 = mrv2;
        mrv3.field_3 = rsi8_0;
        mrv4 = mrv3;
        mrv4.field_4 = rsi;
        mrv5 = mrv4;
        mrv5.field_5 = var_4;
        mrv6 = mrv5;
        mrv6.field_6 = r8_1;
        return mrv6;
    }
    var_15 = (uint64_t)*(uint32_t *)(rdi + 168UL);
    var_16 = (uint64_t)*(uint32_t *)(rdi + 48UL);
    var_17 = (uint64_t)*(unsigned char *)(rdi + 184UL);
    *(uint64_t *)(local_sp_3 + (-16L)) = 4226321UL;
    var_18 = indirect_placeholder_3(var_15, var_17, var_16);
    rdi7_0 = var_17;
    rbp_0 = var_14 + (uint64_t)(unsigned char)var_18;
    rsi8_0 = var_16;
    mrv.field_0 = var_1;
    mrv1 = mrv;
    mrv1.field_1 = rbp_0;
    mrv2 = mrv1;
    mrv2.field_2 = rdi7_0;
    mrv3 = mrv2;
    mrv3.field_3 = rsi8_0;
    mrv4 = mrv3;
    mrv4.field_4 = rsi;
    mrv5 = mrv4;
    mrv5.field_5 = var_4;
    mrv6 = mrv5;
    mrv6.field_6 = r8_1;
    return mrv6;
}
