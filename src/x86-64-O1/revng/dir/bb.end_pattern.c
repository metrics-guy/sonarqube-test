typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder_2(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_4(uint64_t param_0);
typedef _Bool bool;
uint64_t bb_end_pattern(uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    unsigned char var_6;
    uint64_t rbp_0;
    uint64_t rbx_2;
    uint64_t local_sp_2;
    uint64_t local_sp_0;
    unsigned char rax_0_in;
    uint64_t cc_src2_2;
    uint64_t rbx_0;
    uint64_t cc_src2_0;
    uint64_t rcx_0;
    uint64_t rax_0;
    unsigned char var_23;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint32_t var_15;
    uint32_t var_19;
    uint64_t local_sp_1;
    uint64_t cc_src2_1;
    uint64_t var_20;
    unsigned char var_21;
    uint64_t storemerge2;
    uint64_t var_22;
    uint64_t rbx_1;
    unsigned char var_24;
    uint64_t var_16;
    uint64_t var_17;
    uint32_t var_18;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r12();
    var_4 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_5 = var_0 + (-24L);
    *(uint64_t *)var_5 = var_1;
    var_6 = *(unsigned char *)(rdi + 1UL);
    rbp_0 = rdi;
    local_sp_0 = var_5;
    rax_0_in = var_6;
    rbx_0 = rdi;
    cc_src2_0 = var_4;
    if (var_6 == '\x00') {
        return rbp_0;
    }
    while (1U)
        {
            rcx_0 = rbx_0 + 1UL;
            rax_0 = (uint64_t)rax_0_in;
            rbx_2 = rcx_0;
            local_sp_2 = local_sp_0;
            cc_src2_2 = cc_src2_0;
            local_sp_1 = local_sp_0;
            cc_src2_1 = cc_src2_0;
            if ((uint64_t)(rax_0_in + '\xa5') == 0UL) {
                var_15 = *(uint32_t *)4397400UL;
                var_19 = var_15;
                if (var_15 == 0U) {
                    var_16 = local_sp_0 + (-8L);
                    *(uint64_t *)var_16 = 4267873UL;
                    indirect_placeholder_2();
                    var_17 = helper_cc_compute_c_wrapper(rax_0 + (-1L), 1UL, cc_src2_0, 17U);
                    var_18 = (0U - (uint32_t)var_17) | 1U;
                    *(uint32_t *)4397400UL = var_18;
                    var_19 = var_18;
                    local_sp_1 = var_16;
                    cc_src2_1 = var_17;
                }
                var_20 = rbx_0 + 2UL;
                var_21 = *(unsigned char *)var_20;
                var_23 = var_21;
                storemerge2 = var_20;
                local_sp_2 = local_sp_1;
                cc_src2_2 = cc_src2_1;
                if ((uint64_t)(var_21 + '\xdf') == 0UL) {
                    var_22 = rbx_0 + 3UL;
                    var_23 = *(unsigned char *)var_22;
                    storemerge2 = var_22;
                } else {
                    if ((int)var_19 <= (int)4294967295U & (uint64_t)(var_21 + '\xa2') == 0UL) {
                        var_22 = rbx_0 + 3UL;
                        var_23 = *(unsigned char *)var_22;
                        storemerge2 = var_22;
                    }
                }
                rbx_1 = storemerge2 + (var_23 == ']');
                while (1U)
                    {
                        rbx_2 = rbx_1;
                        switch_state_var = 0;
                        switch (*(unsigned char *)rbx_1) {
                          case '\x00':
                            {
                                loop_state_var = 0U;
                                switch_state_var = 1;
                                break;
                            }
                            break;
                          case ']':
                            {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            break;
                          default:
                            {
                                rbx_1 = rbx_1 + 1UL;
                                continue;
                            }
                            break;
                        }
                        if (switch_state_var)
                            break;
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 0U:
                    {
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 1U:
                    {
                        break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
            var_7 = (uint64_t)((uint32_t)rax_0 + (-33));
            var_8 = var_7 + (-31L);
            if ((uint64_t)((unsigned char)var_7 & '\xe0') != 0UL) {
                if ((uint64_t)(rax_0_in + '\xd7') == 0UL) {
                    var_24 = *(unsigned char *)(rbx_2 + 1UL);
                    local_sp_0 = local_sp_2;
                    rax_0_in = var_24;
                    rbx_0 = rbx_2;
                    cc_src2_0 = cc_src2_2;
                    if (var_24 != '\x00') {
                        continue;
                    }
                    break;
                }
                rbp_0 = rbx_0 + 2UL;
                break;
            }
            var_9 = 3221227009UL >> (var_7 & 63UL);
            var_10 = helper_cc_compute_all_wrapper(var_8, 31UL, cc_src2_0, 14U);
            var_11 = helper_cc_compute_c_wrapper(var_8, (var_10 & (-2L)) | (var_9 & 1UL), cc_src2_0, 1U);
            if (var_11 != 0UL) {
                if ((uint64_t)(rax_0_in + '\xd7') != 0UL) {
                    rbp_0 = rbx_0 + 2UL;
                    break;
                }
            }
            var_12 = rbx_0 + 2UL;
            if (*(unsigned char *)var_12 != '(') {
                if ((uint64_t)(rax_0_in + '\xd7') != 0UL) {
                    rbp_0 = rbx_0 + 2UL;
                    break;
                }
            }
            var_13 = local_sp_0 + (-8L);
            *(uint64_t *)var_13 = 4267954UL;
            var_14 = indirect_placeholder_4(var_12);
            local_sp_2 = var_13;
            rbx_2 = var_14;
        }
    return rbp_0;
}
