typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_divq_EAX_wrapper_ret_type;
struct type_5;
struct indirect_placeholder_105_ret_type;
struct indirect_placeholder_106_ret_type;
struct indirect_placeholder_107_ret_type;
struct indirect_placeholder_108_ret_type;
struct indirect_placeholder_109_ret_type;
struct indirect_placeholder_114_ret_type;
struct indirect_placeholder_123_ret_type;
struct indirect_placeholder_113_ret_type;
struct indirect_placeholder_115_ret_type;
struct indirect_placeholder_116_ret_type;
struct indirect_placeholder_117_ret_type;
struct indirect_placeholder_118_ret_type;
struct indirect_placeholder_119_ret_type;
struct indirect_placeholder_121_ret_type;
struct indirect_placeholder_120_ret_type;
struct indirect_placeholder_122_ret_type;
struct indirect_placeholder_124_ret_type;
struct indirect_placeholder_125_ret_type;
struct indirect_placeholder_139_ret_type;
struct indirect_placeholder_126_ret_type;
struct indirect_placeholder_127_ret_type;
struct indirect_placeholder_128_ret_type;
struct indirect_placeholder_129_ret_type;
struct indirect_placeholder_130_ret_type;
struct indirect_placeholder_131_ret_type;
struct indirect_placeholder_134_ret_type;
struct indirect_placeholder_132_ret_type;
struct indirect_placeholder_136_ret_type;
struct indirect_placeholder_135_ret_type;
struct indirect_placeholder_133_ret_type;
struct indirect_placeholder_137_ret_type;
struct indirect_placeholder_110_ret_type;
struct indirect_placeholder_138_ret_type;
struct indirect_placeholder_111_ret_type;
struct indirect_placeholder_112_ret_type;
struct helper_divq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint32_t field_4;
    uint32_t field_5;
    uint32_t field_6;
    uint64_t field_7;
    uint32_t field_8;
    uint64_t field_9;
    uint32_t field_10;
    unsigned char field_11;
    uint32_t field_12;
};
struct type_5 {
};
struct indirect_placeholder_105_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_106_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_107_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_108_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_109_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_114_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_123_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_113_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_115_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_116_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_117_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_118_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_119_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_121_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_120_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_122_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_124_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_125_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_139_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_126_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_127_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_128_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_129_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_130_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_131_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_134_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_132_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_136_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_135_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_133_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_137_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_110_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_138_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_111_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_112_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_r13(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r15(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern void indirect_placeholder_1(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint32_t init_state_0x8248(void);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern struct helper_divq_EAX_wrapper_ret_type helper_divq_EAX_wrapper(struct type_5 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint32_t param_13, uint64_t param_14, uint32_t param_15, uint64_t param_16, uint64_t param_17, uint64_t param_18, uint32_t param_19);
extern void indirect_placeholder_81(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_105_ret_type indirect_placeholder_105(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_106_ret_type indirect_placeholder_106(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_107_ret_type indirect_placeholder_107(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_108_ret_type indirect_placeholder_108(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_109_ret_type indirect_placeholder_109(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_114_ret_type indirect_placeholder_114(uint64_t param_0);
extern struct indirect_placeholder_123_ret_type indirect_placeholder_123(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_113_ret_type indirect_placeholder_113(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_115_ret_type indirect_placeholder_115(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_116_ret_type indirect_placeholder_116(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_117_ret_type indirect_placeholder_117(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_118_ret_type indirect_placeholder_118(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_119_ret_type indirect_placeholder_119(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_121_ret_type indirect_placeholder_121(uint64_t param_0);
extern struct indirect_placeholder_120_ret_type indirect_placeholder_120(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_122_ret_type indirect_placeholder_122(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_124_ret_type indirect_placeholder_124(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_125_ret_type indirect_placeholder_125(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_139_ret_type indirect_placeholder_139(uint64_t param_0);
extern struct indirect_placeholder_126_ret_type indirect_placeholder_126(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_127_ret_type indirect_placeholder_127(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_128_ret_type indirect_placeholder_128(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_129_ret_type indirect_placeholder_129(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_130_ret_type indirect_placeholder_130(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_131_ret_type indirect_placeholder_131(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_134_ret_type indirect_placeholder_134(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_132_ret_type indirect_placeholder_132(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_136_ret_type indirect_placeholder_136(uint64_t param_0);
extern struct indirect_placeholder_135_ret_type indirect_placeholder_135(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_133_ret_type indirect_placeholder_133(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_137_ret_type indirect_placeholder_137(uint64_t param_0);
extern struct indirect_placeholder_110_ret_type indirect_placeholder_110(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_138_ret_type indirect_placeholder_138(uint64_t param_0);
extern struct indirect_placeholder_111_ret_type indirect_placeholder_111(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_112_ret_type indirect_placeholder_112(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
uint64_t bb_scanargs(uint64_t rdi, uint64_t rsi, uint64_t rcx, uint64_t r10, uint64_t r9, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint32_t var_8;
    uint64_t var_9;
    uint32_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint32_t var_14;
    struct indirect_placeholder_105_ret_type var_218;
    uint64_t var_15;
    uint32_t var_16;
    uint64_t var_17;
    uint64_t r95_12;
    uint64_t rcx3_1;
    uint64_t var_214;
    uint64_t var_215;
    uint64_t var_216;
    uint64_t rcx3_0;
    uint64_t r86_12;
    uint64_t r95_0;
    uint64_t rax_4;
    uint64_t r86_0;
    uint64_t local_sp_0;
    uint64_t var_217;
    uint64_t var_219;
    uint64_t var_220;
    uint64_t var_221;
    uint64_t rcx3_9;
    uint64_t var_211;
    uint64_t var_212;
    uint64_t var_213;
    uint64_t var_112;
    uint64_t var_113;
    uint64_t var_114;
    uint64_t r95_1;
    uint64_t r86_1;
    uint64_t local_sp_1;
    uint64_t var_209;
    struct indirect_placeholder_106_ret_type var_210;
    uint64_t var_206;
    uint64_t var_207;
    uint64_t var_208;
    uint64_t rcx3_2;
    uint64_t r95_2;
    uint64_t r86_2;
    uint64_t local_sp_2;
    uint64_t var_201;
    struct indirect_placeholder_107_ret_type var_202;
    uint64_t var_203;
    uint64_t var_204;
    uint64_t var_205;
    uint64_t var_198;
    uint64_t var_199;
    uint64_t var_200;
    uint64_t rcx3_3;
    uint64_t r95_3;
    uint64_t r86_3;
    uint64_t local_sp_3;
    uint64_t var_196;
    struct indirect_placeholder_108_ret_type var_197;
    uint64_t var_193;
    uint64_t var_194;
    uint64_t var_195;
    uint64_t var_188;
    struct indirect_placeholder_109_ret_type var_189;
    uint64_t var_190;
    uint64_t var_191;
    uint64_t var_192;
    uint64_t var_131;
    uint64_t var_132;
    uint64_t var_133;
    uint64_t rdi1_1;
    struct indirect_placeholder_114_ret_type var_222;
    uint64_t var_223;
    uint64_t var_224;
    uint64_t var_225;
    uint64_t var_87;
    uint64_t var_128;
    struct indirect_placeholder_115_ret_type var_129;
    uint64_t var_130;
    uint64_t var_125;
    uint64_t var_126;
    uint64_t var_127;
    uint64_t var_122;
    struct indirect_placeholder_116_ret_type var_123;
    uint64_t var_124;
    uint64_t var_119;
    uint64_t var_120;
    uint64_t var_121;
    uint64_t rcx3_6;
    uint64_t r95_8;
    uint64_t r104_0;
    uint64_t r86_8;
    uint64_t r95_7;
    uint64_t rax_3;
    uint64_t local_sp_5;
    uint64_t var_115;
    uint64_t *var_116;
    struct indirect_placeholder_117_ret_type var_117;
    uint64_t var_118;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t var_107;
    uint64_t rdi1_0;
    uint64_t var_109;
    struct indirect_placeholder_118_ret_type var_110;
    uint64_t var_111;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t var_100;
    uint64_t var_101;
    struct indirect_placeholder_119_ret_type var_102;
    uint64_t rsi2_0;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t rcx3_4;
    uint64_t r86_6;
    uint64_t r95_4;
    uint64_t local_sp_8;
    uint64_t r86_4;
    uint64_t rax_0;
    uint64_t local_sp_4;
    uint64_t local_sp_7;
    uint64_t var_108;
    uint64_t rsi2_1;
    uint64_t rcx3_5;
    uint64_t r95_5;
    uint64_t r86_5;
    uint64_t rax_1;
    uint64_t local_sp_6;
    uint64_t rdi1_2;
    struct indirect_placeholder_121_ret_type var_134;
    uint64_t var_135;
    uint64_t var_136;
    uint64_t var_137;
    uint64_t var_138;
    struct indirect_placeholder_120_ret_type var_139;
    uint64_t r95_6;
    uint64_t var_94;
    struct indirect_placeholder_122_ret_type var_95;
    uint64_t var_72;
    struct indirect_placeholder_124_ret_type var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_81;
    struct indirect_placeholder_125_ret_type var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    struct indirect_placeholder_123_ret_type var_88;
    struct indirect_placeholder_126_ret_type var_71;
    struct indirect_placeholder_127_ret_type var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    struct indirect_placeholder_128_ret_type var_59;
    bool var_60;
    uint64_t var_61;
    uint64_t *var_62;
    struct indirect_placeholder_129_ret_type var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    struct indirect_placeholder_130_ret_type var_47;
    bool var_48;
    uint64_t var_49;
    uint64_t *var_50;
    struct indirect_placeholder_131_ret_type var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    struct indirect_placeholder_134_ret_type var_30;
    uint64_t var_31;
    uint64_t rsi2_2;
    uint64_t local_sp_10;
    uint64_t var_22;
    uint64_t var_23;
    struct indirect_placeholder_132_ret_type var_24;
    uint64_t var_25;
    struct indirect_placeholder_136_ret_type var_226;
    uint64_t var_227;
    uint64_t var_228;
    uint64_t var_229;
    uint64_t r12_0;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t r86_7;
    uint64_t rax_2;
    uint64_t local_sp_9;
    uint64_t r104_1;
    uint64_t rbx_0;
    uint64_t var_140;
    uint64_t rbx_1;
    uint64_t rbx_2;
    struct indirect_placeholder_133_ret_type var_35;
    bool var_36;
    uint64_t var_37;
    uint64_t *var_38;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t rdi1_3;
    uint64_t rbp_0;
    uint64_t rsi2_3;
    uint64_t rcx3_7;
    uint64_t r104_2;
    uint64_t r95_9;
    uint64_t r86_9;
    uint64_t local_sp_11;
    uint64_t rdi1_4;
    uint64_t rbp_1;
    uint64_t rsi2_4;
    uint64_t rcx3_8;
    uint64_t r104_3;
    uint64_t r95_10;
    uint64_t r86_10;
    uint64_t local_sp_12;
    uint32_t var_143;
    uint32_t var_141;
    uint32_t var_142;
    uint32_t var_144;
    uint64_t var_145;
    uint64_t var_149;
    uint64_t *_pre_phi;
    uint64_t *_pre;
    uint64_t *var_157;
    uint64_t var_158;
    uint64_t var_160;
    uint32_t var_161;
    uint64_t var_162;
    struct helper_divq_EAX_wrapper_ret_type var_159;
    uint32_t var_163;
    uint64_t var_164;
    uint32_t var_165;
    uint32_t state_0x8248_0;
    uint64_t var_166;
    uint64_t state_0x9018_0;
    uint32_t state_0x9010_0;
    uint64_t state_0x82d8_0;
    uint32_t state_0x9080_0;
    uint64_t *_pre367;
    uint64_t *_pre_phi368;
    uint64_t *var_167;
    uint64_t var_168;
    uint64_t var_170;
    uint32_t var_171;
    uint64_t var_172;
    struct helper_divq_EAX_wrapper_ret_type var_169;
    uint32_t var_173;
    uint64_t var_174;
    uint32_t var_175;
    uint32_t state_0x8248_1;
    uint64_t var_176;
    uint64_t state_0x9018_1;
    uint32_t state_0x9010_1;
    uint64_t state_0x82d8_1;
    uint32_t state_0x9080_1;
    uint64_t *_pre371;
    uint64_t *_pre_phi372;
    uint64_t *var_177;
    uint64_t var_178;
    uint64_t var_180;
    struct helper_divq_EAX_wrapper_ret_type var_179;
    uint64_t var_181;
    uint64_t var_182;
    uint64_t var_183;
    uint16_t var_184;
    uint64_t local_sp_14;
    uint64_t rcx3_10;
    uint64_t r95_11;
    uint64_t r86_11;
    uint64_t local_sp_13;
    uint64_t var_185;
    uint64_t var_186;
    uint64_t var_187;
    uint64_t var_153;
    struct indirect_placeholder_137_ret_type var_154;
    uint64_t var_155;
    uint64_t var_156;
    struct indirect_placeholder_138_ret_type var_150;
    uint64_t var_151;
    uint64_t var_152;
    struct indirect_placeholder_139_ret_type var_146;
    uint64_t var_147;
    uint64_t var_148;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_cc_src2();
    var_8 = init_state_0x8248();
    var_9 = init_state_0x9018();
    var_10 = init_state_0x9010();
    var_11 = init_state_0x8408();
    var_12 = init_state_0x8328();
    var_13 = init_state_0x82d8();
    var_14 = init_state_0x9080();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_15 = var_0 + (-104L);
    var_16 = *(uint32_t *)4301692UL;
    var_17 = (uint64_t)var_16 << 32UL;
    rax_4 = 4211754UL;
    r95_8 = r9;
    r86_8 = r8;
    local_sp_10 = var_15;
    r104_1 = r10;
    rbx_0 = var_2;
    rbx_1 = var_2;
    rdi1_3 = rdi;
    rbp_0 = var_4;
    rsi2_3 = rsi;
    rcx3_7 = rcx;
    r104_2 = r10;
    r95_9 = r9;
    r86_9 = r8;
    local_sp_11 = var_15;
    state_0x8248_0 = var_8;
    state_0x9018_0 = var_9;
    state_0x9010_0 = var_10;
    state_0x82d8_0 = var_13;
    state_0x9080_0 = var_14;
    if ((long)var_17 < (long)(rdi << 32UL)) {
        var_18 = (uint64_t)((long)var_17 >> (long)29UL) + rsi;
        var_19 = (uint64_t)((var_16 ^ (-1)) + (uint32_t)rdi) + (uint64_t)var_16;
        var_20 = (var_19 << 3UL) + rsi;
        *(uint64_t *)(var_0 + (-96L)) = 0UL;
        *(uint64_t *)var_15 = 0UL;
        *(uint64_t *)(var_0 + (-88L)) = 18446744073709551615UL;
        *(uint64_t *)(var_0 + (-80L)) = 0UL;
        r12_0 = var_18;
        rax_3 = var_19;
        loop_state_var = 1U;
        while (1U)
            {
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 0U:
                    {
                        var_21 = *(uint64_t *)r12_0;
                        *(uint64_t *)(local_sp_10 + (-8L)) = 4207444UL;
                        indirect_placeholder_1();
                        r104_0 = r104_1;
                        r95_7 = r95_8;
                        r86_7 = r86_8;
                        rbp_0 = var_21;
                        rbp_1 = var_21;
                        if (rax_3 == 0UL) {
                            *(uint64_t *)(local_sp_10 + (-16L)) = 4207352UL;
                            var_226 = indirect_placeholder_136(var_21);
                            var_227 = var_226.field_0;
                            var_228 = var_226.field_1;
                            var_229 = var_226.field_2;
                            *(uint64_t *)(local_sp_10 + (-24L)) = 4207380UL;
                            indirect_placeholder_135(0UL, 4272381UL, 0UL, 0UL, var_227, var_228, var_229);
                            *(uint64_t *)(local_sp_10 + (-32L)) = 4207390UL;
                            indirect_placeholder_81(rbx_0, 1UL, var_21);
                            abort();
                        }
                        var_22 = rax_3 + 1UL;
                        var_23 = local_sp_10 + (-16L);
                        *(uint64_t *)var_23 = 4207466UL;
                        var_24 = indirect_placeholder_132(var_21, 4272405UL);
                        var_25 = var_24.field_0;
                        rax_2 = var_25;
                        local_sp_9 = var_23;
                        rbx_0 = var_22;
                        rbx_1 = var_22;
                        rbx_2 = var_22;
                        if ((uint64_t)(unsigned char)var_25 != 0UL) {
                            var_26 = var_24.field_3;
                            var_27 = var_24.field_2;
                            var_28 = var_24.field_1;
                            *(uint64_t *)4302568UL = var_22;
                            rdi1_2 = var_28;
                            rsi2_2 = var_27;
                            rcx3_6 = var_26;
                            r95_8 = r95_7;
                            r86_8 = r86_7;
                            rax_3 = rax_2;
                            local_sp_10 = local_sp_9;
                            r104_1 = r104_0;
                            rdi1_3 = rdi1_2;
                            rsi2_3 = rsi2_2;
                            rcx3_7 = rcx3_6;
                            r104_2 = r104_0;
                            r95_9 = r95_7;
                            r86_9 = r86_7;
                            local_sp_11 = local_sp_9;
                            rdi1_4 = rdi1_2;
                            rsi2_4 = rsi2_2;
                            rcx3_8 = rcx3_6;
                            r104_3 = r104_0;
                            r95_10 = r95_7;
                            r86_10 = r86_7;
                            local_sp_12 = local_sp_9;
                            if (r12_0 == var_20) {
                                r12_0 = r12_0 + 8UL;
                                loop_state_var = 0U;
                                continue;
                            }
                            var_140 = *(uint64_t *)(local_sp_9 + 24UL);
                            if (var_140 != 0UL) {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            *(uint64_t *)4302536UL = var_140;
                            *(uint64_t *)4302544UL = var_140;
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        var_29 = local_sp_10 + (-24L);
                        *(uint64_t *)var_29 = 4207403UL;
                        var_30 = indirect_placeholder_134(var_21, 4272408UL);
                        var_31 = var_30.field_0;
                        rax_2 = var_31;
                        local_sp_9 = var_29;
                        if ((uint64_t)(unsigned char)var_31 != 0UL) {
                            var_32 = var_30.field_3;
                            var_33 = var_30.field_2;
                            var_34 = var_30.field_1;
                            *(uint64_t *)4302560UL = var_22;
                            rdi1_2 = var_34;
                            rsi2_2 = var_33;
                            rcx3_6 = var_32;
                            r95_8 = r95_7;
                            r86_8 = r86_7;
                            rax_3 = rax_2;
                            local_sp_10 = local_sp_9;
                            r104_1 = r104_0;
                            rdi1_3 = rdi1_2;
                            rsi2_3 = rsi2_2;
                            rcx3_7 = rcx3_6;
                            r104_2 = r104_0;
                            r95_9 = r95_7;
                            r86_9 = r86_7;
                            local_sp_11 = local_sp_9;
                            rdi1_4 = rdi1_2;
                            rsi2_4 = rsi2_2;
                            rcx3_8 = rcx3_6;
                            r104_3 = r104_0;
                            r95_10 = r95_7;
                            r86_10 = r86_7;
                            local_sp_12 = local_sp_9;
                            if (r12_0 == var_20) {
                                r12_0 = r12_0 + 8UL;
                                loop_state_var = 0U;
                                continue;
                            }
                            var_140 = *(uint64_t *)(local_sp_9 + 24UL);
                            if (var_140 != 0UL) {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            *(uint64_t *)4302536UL = var_140;
                            *(uint64_t *)4302544UL = var_140;
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        *(uint64_t *)(local_sp_10 + (-32L)) = 4207492UL;
                        var_35 = indirect_placeholder_133(var_21, 4272411UL);
                        var_36 = ((uint64_t)(unsigned char)var_35.field_0 == 0UL);
                        var_37 = local_sp_10 + (-40L);
                        var_38 = (uint64_t *)var_37;
                        local_sp_9 = var_37;
                        if (!var_36) {
                            *var_38 = 4207608UL;
                            var_39 = indirect_placeholder_131(0UL, var_22, 4278784UL, 4272416UL);
                            var_40 = var_39.field_0;
                            var_41 = var_39.field_1;
                            var_42 = var_39.field_2;
                            var_43 = var_39.field_3;
                            var_44 = var_39.field_4;
                            var_45 = var_39.field_5;
                            var_46 = var_39.field_6;
                            *(uint32_t *)4302476UL = (*(uint32_t *)4302476UL | (uint32_t)var_40);
                            rcx3_6 = var_43;
                            r104_0 = var_44;
                            r95_7 = var_45;
                            rdi1_2 = var_41;
                            rsi2_2 = var_42;
                            r86_7 = var_46;
                            rax_2 = var_40;
                            r95_8 = r95_7;
                            r86_8 = r86_7;
                            rax_3 = rax_2;
                            local_sp_10 = local_sp_9;
                            r104_1 = r104_0;
                            rdi1_3 = rdi1_2;
                            rsi2_3 = rsi2_2;
                            rcx3_7 = rcx3_6;
                            r104_2 = r104_0;
                            r95_9 = r95_7;
                            r86_9 = r86_7;
                            local_sp_11 = local_sp_9;
                            rdi1_4 = rdi1_2;
                            rsi2_4 = rsi2_2;
                            rcx3_8 = rcx3_6;
                            r104_3 = r104_0;
                            r95_10 = r95_7;
                            r86_10 = r86_7;
                            local_sp_12 = local_sp_9;
                            if (r12_0 == var_20) {
                                r12_0 = r12_0 + 8UL;
                                loop_state_var = 0U;
                                continue;
                            }
                            var_140 = *(uint64_t *)(local_sp_9 + 24UL);
                            if (var_140 != 0UL) {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            *(uint64_t *)4302536UL = var_140;
                            *(uint64_t *)4302544UL = var_140;
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        *var_38 = 4207509UL;
                        var_47 = indirect_placeholder_130(var_21, 4272435UL);
                        var_48 = ((uint64_t)(unsigned char)var_47.field_0 == 0UL);
                        var_49 = local_sp_10 + (-48L);
                        var_50 = (uint64_t *)var_49;
                        local_sp_9 = var_49;
                        if (!var_48) {
                            *var_50 = 4207642UL;
                            var_51 = indirect_placeholder_129(0UL, var_22, 4278464UL, 4272441UL);
                            var_52 = var_51.field_0;
                            var_53 = var_51.field_1;
                            var_54 = var_51.field_2;
                            var_55 = var_51.field_3;
                            var_56 = var_51.field_4;
                            var_57 = var_51.field_5;
                            var_58 = var_51.field_6;
                            *(uint32_t *)4302472UL = (*(uint32_t *)4302472UL | (uint32_t)var_52);
                            rcx3_6 = var_55;
                            r104_0 = var_56;
                            r95_7 = var_57;
                            rdi1_2 = var_53;
                            rsi2_2 = var_54;
                            r86_7 = var_58;
                            rax_2 = var_52;
                            r95_8 = r95_7;
                            r86_8 = r86_7;
                            rax_3 = rax_2;
                            local_sp_10 = local_sp_9;
                            r104_1 = r104_0;
                            rdi1_3 = rdi1_2;
                            rsi2_3 = rsi2_2;
                            rcx3_7 = rcx3_6;
                            r104_2 = r104_0;
                            r95_9 = r95_7;
                            r86_9 = r86_7;
                            local_sp_11 = local_sp_9;
                            rdi1_4 = rdi1_2;
                            rsi2_4 = rsi2_2;
                            rcx3_8 = rcx3_6;
                            r104_3 = r104_0;
                            r95_10 = r95_7;
                            r86_10 = r86_7;
                            local_sp_12 = local_sp_9;
                            if (r12_0 == var_20) {
                                r12_0 = r12_0 + 8UL;
                                loop_state_var = 0U;
                                continue;
                            }
                            var_140 = *(uint64_t *)(local_sp_9 + 24UL);
                            if (var_140 != 0UL) {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            *(uint64_t *)4302536UL = var_140;
                            *(uint64_t *)4302544UL = var_140;
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        *var_50 = 4207526UL;
                        var_59 = indirect_placeholder_128(var_21, 4272460UL);
                        var_60 = ((uint64_t)(unsigned char)var_59.field_0 == 0UL);
                        var_61 = local_sp_10 + (-56L);
                        var_62 = (uint64_t *)var_61;
                        local_sp_9 = var_61;
                        if (!var_60) {
                            *var_62 = 4207676UL;
                            var_63 = indirect_placeholder_127(0UL, var_22, 4278464UL, 4272466UL);
                            var_64 = var_63.field_0;
                            var_65 = var_63.field_1;
                            var_66 = var_63.field_2;
                            var_67 = var_63.field_3;
                            var_68 = var_63.field_4;
                            var_69 = var_63.field_5;
                            var_70 = var_63.field_6;
                            *(uint32_t *)4302468UL = (*(uint32_t *)4302468UL | (uint32_t)var_64);
                            rcx3_6 = var_67;
                            r104_0 = var_68;
                            r95_7 = var_69;
                            rdi1_2 = var_65;
                            rsi2_2 = var_66;
                            r86_7 = var_70;
                            rax_2 = var_64;
                            r95_8 = r95_7;
                            r86_8 = r86_7;
                            rax_3 = rax_2;
                            local_sp_10 = local_sp_9;
                            r104_1 = r104_0;
                            rdi1_3 = rdi1_2;
                            rsi2_3 = rsi2_2;
                            rcx3_7 = rcx3_6;
                            r104_2 = r104_0;
                            r95_9 = r95_7;
                            r86_9 = r86_7;
                            local_sp_11 = local_sp_9;
                            rdi1_4 = rdi1_2;
                            rsi2_4 = rsi2_2;
                            rcx3_8 = rcx3_6;
                            r104_3 = r104_0;
                            r95_10 = r95_7;
                            r86_10 = r86_7;
                            local_sp_12 = local_sp_9;
                            if (r12_0 == var_20) {
                                r12_0 = r12_0 + 8UL;
                                loop_state_var = 0U;
                                continue;
                            }
                            var_140 = *(uint64_t *)(local_sp_9 + 24UL);
                            if (var_140 != 0UL) {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            *(uint64_t *)4302536UL = var_140;
                            *(uint64_t *)4302544UL = var_140;
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        *var_62 = 4207543UL;
                        var_71 = indirect_placeholder_126(var_21, 4272486UL);
                        if ((uint64_t)(unsigned char)var_71.field_0 != 0UL) {
                            var_72 = local_sp_10 + (-64L);
                            *(uint64_t *)var_72 = 4207574UL;
                            var_73 = indirect_placeholder_124(1UL, var_22, 4278400UL, 4272493UL);
                            var_74 = var_73.field_0;
                            var_75 = var_73.field_1;
                            var_76 = var_73.field_2;
                            var_77 = var_73.field_3;
                            var_78 = var_73.field_4;
                            var_79 = var_73.field_5;
                            var_80 = var_73.field_6;
                            *(uint32_t *)4301548UL = (uint32_t)var_74;
                            rcx3_6 = var_77;
                            r104_0 = var_78;
                            r95_7 = var_79;
                            rdi1_2 = var_75;
                            rsi2_2 = var_76;
                            r86_7 = var_80;
                            rax_2 = var_74;
                            local_sp_9 = var_72;
                            r95_8 = r95_7;
                            r86_8 = r86_7;
                            rax_3 = rax_2;
                            local_sp_10 = local_sp_9;
                            r104_1 = r104_0;
                            rdi1_3 = rdi1_2;
                            rsi2_3 = rsi2_2;
                            rcx3_7 = rcx3_6;
                            r104_2 = r104_0;
                            r95_9 = r95_7;
                            r86_9 = r86_7;
                            local_sp_11 = local_sp_9;
                            rdi1_4 = rdi1_2;
                            rsi2_4 = rsi2_2;
                            rcx3_8 = rcx3_6;
                            r104_3 = r104_0;
                            r95_10 = r95_7;
                            r86_10 = r86_7;
                            local_sp_12 = local_sp_9;
                            if (r12_0 == var_20) {
                                r12_0 = r12_0 + 8UL;
                                loop_state_var = 0U;
                                continue;
                            }
                            var_140 = *(uint64_t *)(local_sp_9 + 24UL);
                            if (var_140 != 0UL) {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            *(uint64_t *)4302536UL = var_140;
                            *(uint64_t *)4302544UL = var_140;
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        var_81 = local_sp_10 + (-12L);
                        *(uint32_t *)var_81 = 0U;
                        *(uint64_t *)(local_sp_10 + (-64L)) = 4207708UL;
                        var_82 = indirect_placeholder_125(var_22, var_81);
                        var_83 = var_82.field_0;
                        var_84 = var_82.field_4;
                        var_85 = var_82.field_5;
                        var_86 = var_82.field_6;
                        var_87 = local_sp_10 + (-72L);
                        *(uint64_t *)var_87 = 4207724UL;
                        var_88 = indirect_placeholder_123(var_21, 4272514UL);
                        r104_0 = var_84;
                        r86_6 = var_86;
                        r95_4 = var_85;
                        local_sp_8 = var_87;
                        r86_4 = var_86;
                        local_sp_4 = var_87;
                        r95_6 = var_85;
                        if ((uint64_t)(unsigned char)var_88.field_0 == 0UL) {
                            var_89 = var_88.field_3;
                            var_90 = var_88.field_2;
                            var_91 = var_88.field_1;
                            var_92 = 18446744073709551612UL - (-4L);
                            var_93 = (var_92 < 9223372036854775807UL) ? var_92 : 9223372036854775807UL;
                            *(uint64_t *)4302544UL = var_83;
                            rdi1_0 = var_91;
                            rsi2_0 = var_90;
                            rcx3_4 = var_89;
                            rax_0 = var_93;
                        } else {
                            var_94 = local_sp_8 + (-8L);
                            *(uint64_t *)var_94 = 4207838UL;
                            var_95 = indirect_placeholder_122(var_21, 4272518UL);
                            r95_4 = r95_6;
                            r86_4 = r86_6;
                            local_sp_4 = var_94;
                            r95_5 = r95_6;
                            r86_5 = r86_6;
                            if ((uint64_t)(unsigned char)var_95.field_0 == 0UL) {
                                var_96 = var_95.field_3;
                                var_97 = var_95.field_2;
                                var_98 = var_95.field_1;
                                var_99 = 0UL - *(uint64_t *)4302552UL;
                                var_100 = (var_99 < 9223372036854775807UL) ? var_99 : 9223372036854775807UL;
                                *(uint64_t *)4302536UL = var_83;
                                rdi1_0 = var_98;
                                rsi2_0 = var_97;
                                rcx3_4 = var_96;
                                rax_0 = var_100;
                            } else {
                                var_101 = local_sp_8 + (-16L);
                                *(uint64_t *)var_101 = 4207881UL;
                                var_102 = indirect_placeholder_119(var_21, 4272523UL);
                                local_sp_4 = var_101;
                                if ((uint64_t)(unsigned char)var_102.field_0 != 0UL) {
                                    var_109 = local_sp_8 + (-24L);
                                    *(uint64_t *)var_109 = 4207932UL;
                                    var_110 = indirect_placeholder_118(var_21, 4272522UL);
                                    var_111 = var_110.field_0;
                                    local_sp_5 = var_109;
                                    rax_1 = var_111;
                                    local_sp_6 = var_109;
                                    if ((uint64_t)(unsigned char)var_111 == 0UL) {
                                        var_115 = local_sp_8 + (-32L);
                                        var_116 = (uint64_t *)var_115;
                                        *var_116 = 4207970UL;
                                        var_117 = indirect_placeholder_117(var_21, 4272802UL);
                                        var_118 = var_117.field_0;
                                        rax_1 = var_118;
                                        local_sp_6 = var_115;
                                        if ((uint64_t)(unsigned char)var_118 == 0UL) {
                                            var_119 = var_117.field_3;
                                            var_120 = var_117.field_2;
                                            var_121 = var_117.field_1;
                                            *var_116 = var_83;
                                            rdi1_1 = var_121;
                                            rsi2_1 = var_120;
                                            rcx3_5 = var_119;
                                        } else {
                                            var_122 = local_sp_8 + (-40L);
                                            *(uint64_t *)var_122 = 4207991UL;
                                            var_123 = indirect_placeholder_116(var_21, 4272600UL);
                                            var_124 = var_123.field_0;
                                            rax_1 = var_124;
                                            local_sp_6 = var_122;
                                            if ((uint64_t)(unsigned char)var_124 == 0UL) {
                                                var_125 = var_123.field_3;
                                                var_126 = var_123.field_2;
                                                var_127 = var_123.field_1;
                                                *var_116 = var_83;
                                                rdi1_1 = var_127;
                                                rsi2_1 = var_126;
                                                rcx3_5 = var_125;
                                            } else {
                                                var_128 = local_sp_8 + (-48L);
                                                *(uint64_t *)var_128 = 4208012UL;
                                                var_129 = indirect_placeholder_115(var_21, 4272526UL);
                                                var_130 = var_129.field_0;
                                                rax_1 = var_130;
                                                local_sp_6 = var_128;
                                                if ((uint64_t)(unsigned char)var_130 != 0UL) {
                                                    *(uint64_t *)(local_sp_8 + (-56L)) = 4208034UL;
                                                    var_222 = indirect_placeholder_114(var_21);
                                                    var_223 = var_222.field_0;
                                                    var_224 = var_222.field_1;
                                                    var_225 = var_222.field_2;
                                                    *(uint64_t *)(local_sp_8 + (-64L)) = 4208062UL;
                                                    indirect_placeholder_113(0UL, 4272381UL, 0UL, 0UL, var_223, var_224, var_225);
                                                    *(uint64_t *)(local_sp_8 + (-72L)) = 4208072UL;
                                                    indirect_placeholder_81(var_22, 1UL, var_21);
                                                    abort();
                                                }
                                                var_131 = var_129.field_3;
                                                var_132 = var_129.field_2;
                                                var_133 = var_129.field_1;
                                                *var_116 = var_83;
                                                rdi1_1 = var_133;
                                                rsi2_1 = var_132;
                                                rcx3_5 = var_131;
                                            }
                                        }
                                        rcx3_6 = rcx3_5;
                                        r95_7 = r95_5;
                                        local_sp_7 = local_sp_6;
                                        rdi1_2 = rdi1_1;
                                        rsi2_2 = rsi2_1;
                                        r86_7 = r86_5;
                                        rax_2 = rax_1;
                                        local_sp_9 = local_sp_6;
                                        if (*(uint32_t *)(local_sp_6 + 44UL) != 0U) {
                                            r95_8 = r95_7;
                                            r86_8 = r86_7;
                                            rax_3 = rax_2;
                                            local_sp_10 = local_sp_9;
                                            r104_1 = r104_0;
                                            rdi1_3 = rdi1_2;
                                            rsi2_3 = rsi2_2;
                                            rcx3_7 = rcx3_6;
                                            r104_2 = r104_0;
                                            r95_9 = r95_7;
                                            r86_9 = r86_7;
                                            local_sp_11 = local_sp_9;
                                            rdi1_4 = rdi1_2;
                                            rsi2_4 = rsi2_2;
                                            rcx3_8 = rcx3_6;
                                            r104_3 = r104_0;
                                            r95_10 = r95_7;
                                            r86_10 = r86_7;
                                            local_sp_12 = local_sp_9;
                                            if (r12_0 != var_20) {
                                                r12_0 = r12_0 + 8UL;
                                                loop_state_var = 0U;
                                                continue;
                                            }
                                            var_140 = *(uint64_t *)(local_sp_9 + 24UL);
                                            if (var_140 == 0UL) {
                                                loop_state_var = 1U;
                                                switch_state_var = 1;
                                                break;
                                            }
                                            *(uint64_t *)4302536UL = var_140;
                                            *(uint64_t *)4302544UL = var_140;
                                            loop_state_var = 0U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                    }
                                    var_112 = var_110.field_3;
                                    var_113 = var_110.field_2;
                                    var_114 = var_110.field_1;
                                    *(uint64_t *)4302528UL = var_83;
                                    rdi1_1 = var_114;
                                    rsi2_1 = var_113;
                                    rcx3_5 = var_112;
                                    if (var_83 != 0UL) {
                                        rcx3_6 = rcx3_5;
                                        r95_7 = r95_5;
                                        local_sp_7 = local_sp_6;
                                        rdi1_2 = rdi1_1;
                                        rsi2_2 = rsi2_1;
                                        r86_7 = r86_5;
                                        rax_2 = rax_1;
                                        local_sp_9 = local_sp_6;
                                        if (*(uint32_t *)(local_sp_6 + 44UL) != 0U) {
                                            r95_8 = r95_7;
                                            r86_8 = r86_7;
                                            rax_3 = rax_2;
                                            local_sp_10 = local_sp_9;
                                            r104_1 = r104_0;
                                            rdi1_3 = rdi1_2;
                                            rsi2_3 = rsi2_2;
                                            rcx3_7 = rcx3_6;
                                            r104_2 = r104_0;
                                            r95_9 = r95_7;
                                            r86_9 = r86_7;
                                            local_sp_11 = local_sp_9;
                                            rdi1_4 = rdi1_2;
                                            rsi2_4 = rsi2_2;
                                            rcx3_8 = rcx3_6;
                                            r104_3 = r104_0;
                                            r95_10 = r95_7;
                                            r86_10 = r86_7;
                                            local_sp_12 = local_sp_9;
                                            if (r12_0 == var_20) {
                                                r12_0 = r12_0 + 8UL;
                                                loop_state_var = 0U;
                                                continue;
                                            }
                                            var_140 = *(uint64_t *)(local_sp_9 + 24UL);
                                            if (var_140 != 0UL) {
                                                loop_state_var = 1U;
                                                switch_state_var = 1;
                                                break;
                                            }
                                            *(uint64_t *)4302536UL = var_140;
                                            *(uint64_t *)4302544UL = var_140;
                                            loop_state_var = 0U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                    }
                                    *(uint32_t *)(local_sp_5 + 44UL) = 4U;
                                    local_sp_7 = local_sp_5;
                                }
                                var_103 = var_102.field_3;
                                var_104 = var_102.field_2;
                                var_105 = var_102.field_1;
                                var_106 = 18446744073709551612UL - (-4L);
                                var_107 = (var_106 < 9223372036854775807UL) ? var_106 : 9223372036854775807UL;
                                *(uint64_t *)(local_sp_8 + 8UL) = var_83;
                                rdi1_0 = var_105;
                                rsi2_0 = var_104;
                                rcx3_4 = var_103;
                                rax_0 = var_107;
                            }
                        }
                    }
                    break;
                  case 1U:
                    {
                        var_94 = local_sp_8 + (-8L);
                        *(uint64_t *)var_94 = 4207838UL;
                        var_95 = indirect_placeholder_122(var_21, 4272518UL);
                        r95_4 = r95_6;
                        r86_4 = r86_6;
                        local_sp_4 = var_94;
                        r95_5 = r95_6;
                        r86_5 = r86_6;
                        if ((uint64_t)(unsigned char)var_95.field_0 == 0UL) {
                            var_96 = var_95.field_3;
                            var_97 = var_95.field_2;
                            var_98 = var_95.field_1;
                            var_99 = 0UL - *(uint64_t *)4302552UL;
                            var_100 = (var_99 < 9223372036854775807UL) ? var_99 : 9223372036854775807UL;
                            *(uint64_t *)4302536UL = var_83;
                            rdi1_0 = var_98;
                            rsi2_0 = var_97;
                            rcx3_4 = var_96;
                            rax_0 = var_100;
                        } else {
                            var_101 = local_sp_8 + (-16L);
                            *(uint64_t *)var_101 = 4207881UL;
                            var_102 = indirect_placeholder_119(var_21, 4272523UL);
                            local_sp_4 = var_101;
                            if ((uint64_t)(unsigned char)var_102.field_0 != 0UL) {
                                var_109 = local_sp_8 + (-24L);
                                *(uint64_t *)var_109 = 4207932UL;
                                var_110 = indirect_placeholder_118(var_21, 4272522UL);
                                var_111 = var_110.field_0;
                                local_sp_5 = var_109;
                                rax_1 = var_111;
                                local_sp_6 = var_109;
                                if ((uint64_t)(unsigned char)var_111 != 0UL) {
                                    var_115 = local_sp_8 + (-32L);
                                    var_116 = (uint64_t *)var_115;
                                    *var_116 = 4207970UL;
                                    var_117 = indirect_placeholder_117(var_21, 4272802UL);
                                    var_118 = var_117.field_0;
                                    rax_1 = var_118;
                                    local_sp_6 = var_115;
                                    if ((uint64_t)(unsigned char)var_118 == 0UL) {
                                        var_119 = var_117.field_3;
                                        var_120 = var_117.field_2;
                                        var_121 = var_117.field_1;
                                        *var_116 = var_83;
                                        rdi1_1 = var_121;
                                        rsi2_1 = var_120;
                                        rcx3_5 = var_119;
                                    } else {
                                        var_122 = local_sp_8 + (-40L);
                                        *(uint64_t *)var_122 = 4207991UL;
                                        var_123 = indirect_placeholder_116(var_21, 4272600UL);
                                        var_124 = var_123.field_0;
                                        rax_1 = var_124;
                                        local_sp_6 = var_122;
                                        if ((uint64_t)(unsigned char)var_124 == 0UL) {
                                            var_125 = var_123.field_3;
                                            var_126 = var_123.field_2;
                                            var_127 = var_123.field_1;
                                            *var_116 = var_83;
                                            rdi1_1 = var_127;
                                            rsi2_1 = var_126;
                                            rcx3_5 = var_125;
                                        } else {
                                            var_128 = local_sp_8 + (-48L);
                                            *(uint64_t *)var_128 = 4208012UL;
                                            var_129 = indirect_placeholder_115(var_21, 4272526UL);
                                            var_130 = var_129.field_0;
                                            rax_1 = var_130;
                                            local_sp_6 = var_128;
                                            if ((uint64_t)(unsigned char)var_130 == 0UL) {
                                                *(uint64_t *)(local_sp_8 + (-56L)) = 4208034UL;
                                                var_222 = indirect_placeholder_114(var_21);
                                                var_223 = var_222.field_0;
                                                var_224 = var_222.field_1;
                                                var_225 = var_222.field_2;
                                                *(uint64_t *)(local_sp_8 + (-64L)) = 4208062UL;
                                                indirect_placeholder_113(0UL, 4272381UL, 0UL, 0UL, var_223, var_224, var_225);
                                                *(uint64_t *)(local_sp_8 + (-72L)) = 4208072UL;
                                                indirect_placeholder_81(var_22, 1UL, var_21);
                                                abort();
                                            }
                                            var_131 = var_129.field_3;
                                            var_132 = var_129.field_2;
                                            var_133 = var_129.field_1;
                                            *var_116 = var_83;
                                            rdi1_1 = var_133;
                                            rsi2_1 = var_132;
                                            rcx3_5 = var_131;
                                        }
                                    }
                                    rcx3_6 = rcx3_5;
                                    r95_7 = r95_5;
                                    local_sp_7 = local_sp_6;
                                    rdi1_2 = rdi1_1;
                                    rsi2_2 = rsi2_1;
                                    r86_7 = r86_5;
                                    rax_2 = rax_1;
                                    local_sp_9 = local_sp_6;
                                    if (*(uint32_t *)(local_sp_6 + 44UL) == 0U) {
                                        r95_8 = r95_7;
                                        r86_8 = r86_7;
                                        rax_3 = rax_2;
                                        local_sp_10 = local_sp_9;
                                        r104_1 = r104_0;
                                        rdi1_3 = rdi1_2;
                                        rsi2_3 = rsi2_2;
                                        rcx3_7 = rcx3_6;
                                        r104_2 = r104_0;
                                        r95_9 = r95_7;
                                        r86_9 = r86_7;
                                        local_sp_11 = local_sp_9;
                                        rdi1_4 = rdi1_2;
                                        rsi2_4 = rsi2_2;
                                        rcx3_8 = rcx3_6;
                                        r104_3 = r104_0;
                                        r95_10 = r95_7;
                                        r86_10 = r86_7;
                                        local_sp_12 = local_sp_9;
                                        if (r12_0 != var_20) {
                                            r12_0 = r12_0 + 8UL;
                                            loop_state_var = 0U;
                                            continue;
                                        }
                                        var_140 = *(uint64_t *)(local_sp_9 + 24UL);
                                        if (var_140 == 0UL) {
                                            loop_state_var = 1U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                        *(uint64_t *)4302536UL = var_140;
                                        *(uint64_t *)4302544UL = var_140;
                                        loop_state_var = 0U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    *(uint64_t *)(local_sp_7 + (-8L)) = 4207784UL;
                                    var_134 = indirect_placeholder_121(var_22);
                                    var_135 = var_134.field_0;
                                    var_136 = var_134.field_1;
                                    var_137 = (*(uint32_t *)(local_sp_7 + 36UL) == 1U) ? 75UL : 0UL;
                                    var_138 = local_sp_7 + (-16L);
                                    *(uint64_t *)var_138 = 4207825UL;
                                    var_139 = indirect_placeholder_120(0UL, 4272333UL, 1UL, var_137, 4272532UL, var_136, var_135);
                                    r95_6 = var_139.field_1;
                                    r86_6 = var_139.field_2;
                                    local_sp_8 = var_138;
                                    loop_state_var = 1U;
                                    continue;
                                }
                                var_112 = var_110.field_3;
                                var_113 = var_110.field_2;
                                var_114 = var_110.field_1;
                                *(uint64_t *)4302528UL = var_83;
                                rdi1_1 = var_114;
                                rsi2_1 = var_113;
                                rcx3_5 = var_112;
                                if (var_83 != 0UL) {
                                    rcx3_6 = rcx3_5;
                                    r95_7 = r95_5;
                                    local_sp_7 = local_sp_6;
                                    rdi1_2 = rdi1_1;
                                    rsi2_2 = rsi2_1;
                                    r86_7 = r86_5;
                                    rax_2 = rax_1;
                                    local_sp_9 = local_sp_6;
                                    if (*(uint32_t *)(local_sp_6 + 44UL) != 0U) {
                                        r95_8 = r95_7;
                                        r86_8 = r86_7;
                                        rax_3 = rax_2;
                                        local_sp_10 = local_sp_9;
                                        r104_1 = r104_0;
                                        rdi1_3 = rdi1_2;
                                        rsi2_3 = rsi2_2;
                                        rcx3_7 = rcx3_6;
                                        r104_2 = r104_0;
                                        r95_9 = r95_7;
                                        r86_9 = r86_7;
                                        local_sp_11 = local_sp_9;
                                        rdi1_4 = rdi1_2;
                                        rsi2_4 = rsi2_2;
                                        rcx3_8 = rcx3_6;
                                        r104_3 = r104_0;
                                        r95_10 = r95_7;
                                        r86_10 = r86_7;
                                        local_sp_12 = local_sp_9;
                                        if (r12_0 != var_20) {
                                            r12_0 = r12_0 + 8UL;
                                            loop_state_var = 0U;
                                            continue;
                                        }
                                        var_140 = *(uint64_t *)(local_sp_9 + 24UL);
                                        if (var_140 == 0UL) {
                                            loop_state_var = 1U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                        *(uint64_t *)4302536UL = var_140;
                                        *(uint64_t *)4302544UL = var_140;
                                        loop_state_var = 0U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                }
                                *(uint32_t *)(local_sp_5 + 44UL) = 4U;
                                local_sp_7 = local_sp_5;
                            }
                            var_103 = var_102.field_3;
                            var_104 = var_102.field_2;
                            var_105 = var_102.field_1;
                            var_106 = 18446744073709551612UL - (-4L);
                            var_107 = (var_106 < 9223372036854775807UL) ? var_106 : 9223372036854775807UL;
                            *(uint64_t *)(local_sp_8 + 8UL) = var_83;
                            rdi1_0 = var_105;
                            rsi2_0 = var_104;
                            rcx3_4 = var_103;
                            rax_0 = var_107;
                        }
                    }
                    break;
                }
                if (switch_state_var)
                    break;
                rdi1_1 = rdi1_0;
                local_sp_5 = local_sp_4;
                local_sp_7 = local_sp_4;
                rsi2_1 = rsi2_0;
                rcx3_5 = rcx3_4;
                r95_5 = r95_4;
                r86_5 = r86_4;
                rax_1 = rax_0;
                local_sp_6 = local_sp_4;
                if (var_83 == 0UL) {
                    *(uint32_t *)(local_sp_5 + 44UL) = 4U;
                    local_sp_7 = local_sp_5;
                } else {
                    var_108 = helper_cc_compute_c_wrapper(rax_0 - var_83, var_83, var_7, 17U);
                    if (var_108 == 0UL) {
                        rcx3_6 = rcx3_5;
                        r95_7 = r95_5;
                        local_sp_7 = local_sp_6;
                        rdi1_2 = rdi1_1;
                        rsi2_2 = rsi2_1;
                        r86_7 = r86_5;
                        rax_2 = rax_1;
                        local_sp_9 = local_sp_6;
                        if (*(uint32_t *)(local_sp_6 + 44UL) != 0U) {
                            r95_8 = r95_7;
                            r86_8 = r86_7;
                            rax_3 = rax_2;
                            local_sp_10 = local_sp_9;
                            r104_1 = r104_0;
                            rdi1_3 = rdi1_2;
                            rsi2_3 = rsi2_2;
                            rcx3_7 = rcx3_6;
                            r104_2 = r104_0;
                            r95_9 = r95_7;
                            r86_9 = r86_7;
                            local_sp_11 = local_sp_9;
                            rdi1_4 = rdi1_2;
                            rsi2_4 = rsi2_2;
                            rcx3_8 = rcx3_6;
                            r104_3 = r104_0;
                            r95_10 = r95_7;
                            r86_10 = r86_7;
                            local_sp_12 = local_sp_9;
                            if (r12_0 == var_20) {
                                r12_0 = r12_0 + 8UL;
                                loop_state_var = 0U;
                                continue;
                            }
                            var_140 = *(uint64_t *)(local_sp_9 + 24UL);
                            if (var_140 != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            *(uint64_t *)4302536UL = var_140;
                            *(uint64_t *)4302544UL = var_140;
                            loop_state_var = 0U;
                            break;
                        }
                    }
                    *(uint32_t *)(local_sp_4 + 44UL) = 1U;
                }
            }
        *(uint32_t *)4302476UL = (*(uint32_t *)4302476UL | 2048U);
        rbx_2 = rbx_1;
        rdi1_4 = rdi1_3;
        rbp_1 = rbp_0;
        rsi2_4 = rsi2_3;
        rcx3_8 = rcx3_7;
        r104_3 = r104_2;
        r95_10 = r95_9;
        r86_10 = r86_9;
        local_sp_12 = local_sp_11;
        if (*(uint64_t *)4302544UL == 0UL) {
            *(uint64_t *)4302544UL = 512UL;
        }
        if (*(uint64_t *)4302536UL == 0UL) {
            *(uint64_t *)4302536UL = 512UL;
        }
    } else {
        *(uint64_t *)(var_0 + (-96L)) = 0UL;
        *(uint64_t *)var_15 = 0UL;
        *(uint64_t *)(var_0 + (-88L)) = 18446744073709551615UL;
        *(uint32_t *)4302476UL = (*(uint32_t *)4302476UL | 2048U);
        rbx_2 = rbx_1;
        rdi1_4 = rdi1_3;
        rbp_1 = rbp_0;
        rsi2_4 = rsi2_3;
        rcx3_8 = rcx3_7;
        r104_3 = r104_2;
        r95_10 = r95_9;
        r86_10 = r86_9;
        local_sp_12 = local_sp_11;
        if (*(uint64_t *)4302544UL == 0UL) {
            *(uint64_t *)4302544UL = 512UL;
        }
        if (*(uint64_t *)4302536UL == 0UL) {
            *(uint64_t *)4302536UL = 512UL;
        }
        r95_12 = r95_10;
        r86_12 = r86_10;
        rcx3_9 = rcx3_8;
        local_sp_14 = local_sp_12;
        rcx3_10 = rcx3_8;
        r95_11 = r95_10;
        r86_11 = r86_10;
        local_sp_13 = local_sp_12;
        if (*(uint64_t *)4302528UL == 0UL) {
            *(uint32_t *)4302476UL = (*(uint32_t *)4302476UL & (-25));
        }
        var_141 = *(uint32_t *)4302472UL;
        var_143 = var_141;
        if ((var_141 & 1052672U) == 0U) {
            var_142 = var_141 | 1052672U;
            *(uint32_t *)4302472UL = var_142;
            var_143 = var_142;
        }
        var_144 = *(uint32_t *)4302468UL;
        var_145 = (uint64_t)var_144;
        if ((var_145 & 1UL) == 0UL) {
            *(uint64_t *)(local_sp_12 + (-8L)) = 4208504UL;
            var_146 = indirect_placeholder_139(4272547UL);
            var_147 = var_146.field_0;
            var_148 = var_146.field_1;
            *(uint64_t *)(local_sp_12 + (-16L)) = 4208537UL;
            indirect_placeholder_112(0UL, 4272333UL, 0UL, 0UL, 4272466UL, var_148, var_147);
            *(uint64_t *)(local_sp_12 + (-24L)) = 4208547UL;
            indirect_placeholder_81(rbx_2, 1UL, rbp_1);
            abort();
        }
        var_149 = (uint64_t)var_143;
        if ((var_149 & 16UL) == 0UL) {
            *(uint64_t *)(local_sp_12 + (-8L)) = 4208557UL;
            var_150 = indirect_placeholder_138(4272557UL);
            var_151 = var_150.field_0;
            var_152 = var_150.field_1;
            *(uint64_t *)(local_sp_12 + (-16L)) = 4208590UL;
            indirect_placeholder_111(0UL, 4272333UL, 0UL, 0UL, 4272441UL, var_152, var_151);
            *(uint64_t *)(local_sp_12 + (-24L)) = 4208600UL;
            indirect_placeholder_81(var_149, 1UL, rbp_1);
            abort();
        }
        if ((var_145 & 12UL) == 0UL) {
            var_153 = ((var_145 & 4UL) == 0UL) ? 4272370UL : 4272358UL;
            *(uint64_t *)(local_sp_12 + (-8L)) = 4208623UL;
            var_154 = indirect_placeholder_137(var_153);
            var_155 = var_154.field_0;
            var_156 = var_154.field_1;
            *(uint64_t *)(local_sp_12 + (-16L)) = 4208656UL;
            indirect_placeholder_110(0UL, 4272333UL, 0UL, 0UL, 4272466UL, var_156, var_155);
            *(uint64_t *)(local_sp_12 + (-24L)) = 4208666UL;
            indirect_placeholder_81(var_149, 1UL, rbp_1);
            abort();
        }
        if ((var_149 & 8UL) == 0UL) {
            _pre = (uint64_t *)local_sp_12;
            _pre_phi = _pre;
            var_166 = *_pre_phi;
            if (var_166 == 0UL) {
                *(uint64_t *)4302520UL = var_166;
            }
        } else {
            var_157 = (uint64_t *)local_sp_12;
            var_158 = *var_157;
            _pre_phi = var_157;
            if (var_158 == 0UL) {
                var_159 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), *(uint64_t *)4302544UL, 4208671UL, var_158, 0UL, var_149, rdi1_4, rbp_1, rsi2_4, rcx3_8, r104_3, r95_10, r86_10, var_8, var_9, var_10, var_11, var_12, var_13, var_14);
                var_160 = var_159.field_2;
                var_161 = var_159.field_6;
                var_162 = var_159.field_7;
                var_163 = var_159.field_8;
                var_164 = var_159.field_9;
                var_165 = var_159.field_10;
                *(uint64_t *)4302520UL = var_159.field_1;
                *(uint64_t *)4302512UL = var_160;
                state_0x8248_0 = var_161;
                state_0x9018_0 = var_162;
                state_0x9010_0 = var_163;
                state_0x82d8_0 = var_164;
                state_0x9080_0 = var_165;
            } else {
                var_166 = *_pre_phi;
                if (var_166 == 0UL) {
                    *(uint64_t *)4302520UL = var_166;
                }
            }
        }
        state_0x8248_1 = state_0x8248_0;
        state_0x9018_1 = state_0x9018_0;
        state_0x9010_1 = state_0x9010_0;
        state_0x82d8_1 = state_0x82d8_0;
        state_0x9080_1 = state_0x9080_0;
        if ((var_149 & 4UL) == 0UL) {
            _pre367 = (uint64_t *)(local_sp_12 + 16UL);
            _pre_phi368 = _pre367;
            var_176 = *_pre_phi368;
            if (var_176 == 18446744073709551615UL) {
                *(uint64_t *)4301552UL = var_176;
            }
        } else {
            var_167 = (uint64_t *)(local_sp_12 + 16UL);
            var_168 = *var_167;
            _pre_phi368 = var_167;
            if (var_168 == 18446744073709551615UL) {
                var_169 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), *(uint64_t *)4302544UL, 4208702UL, var_168, 0UL, var_149, rdi1_4, rbp_1, rsi2_4, rcx3_8, r104_3, r95_10, r86_10, state_0x8248_0, state_0x9018_0, state_0x9010_0, var_11, var_12, state_0x82d8_0, state_0x9080_0);
                var_170 = var_169.field_2;
                var_171 = var_169.field_6;
                var_172 = var_169.field_7;
                var_173 = var_169.field_8;
                var_174 = var_169.field_9;
                var_175 = var_169.field_10;
                *(uint64_t *)4301552UL = var_169.field_1;
                *(uint64_t *)4302480UL = var_170;
                state_0x8248_1 = var_171;
                state_0x9018_1 = var_172;
                state_0x9010_1 = var_173;
                state_0x82d8_1 = var_174;
                state_0x9080_1 = var_175;
            } else {
                var_176 = *_pre_phi368;
                if (var_176 == 18446744073709551615UL) {
                    *(uint64_t *)4301552UL = var_176;
                }
            }
        }
        if ((var_145 & 16UL) == 0UL) {
            _pre371 = (uint64_t *)(local_sp_12 + 8UL);
            _pre_phi372 = _pre371;
            var_181 = *_pre_phi372;
            if (var_181 == 0UL) {
                *(uint64_t *)4302504UL = var_181;
            }
        } else {
            var_177 = (uint64_t *)(local_sp_12 + 8UL);
            var_178 = *var_177;
            _pre_phi372 = var_177;
            if (var_178 == 0UL) {
                var_179 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), *(uint64_t *)4302536UL, 4208733UL, var_178, 0UL, var_149, rdi1_4, rbp_1, rsi2_4, rcx3_8, r104_3, r95_10, r86_10, state_0x8248_1, state_0x9018_1, state_0x9010_1, var_11, var_12, state_0x82d8_1, state_0x9080_1);
                var_180 = var_179.field_2;
                *(uint64_t *)4302504UL = var_179.field_1;
                *(uint64_t *)4302496UL = var_180;
            } else {
                var_181 = *_pre_phi372;
                if (var_181 == 0UL) {
                    *(uint64_t *)4302504UL = var_181;
                }
            }
        }
        var_182 = (uint64_t)*(uint32_t *)4302476UL;
        var_183 = var_149 & 1UL;
        var_184 = (uint16_t)var_182;
        if (((uint64_t)(var_184 & (unsigned short)2048U) | var_183) != 0UL) {
            *(unsigned char *)4302376UL = (unsigned char)'\x00';
            var_185 = (var_183 == 0UL) ? 4211754UL : 4212055UL;
            rax_4 = var_185;
            while (1U)
                {
                    *(uint64_t *)4302176UL = rax_4;
                    *(uint32_t *)4302472UL = (var_143 & (-2));
                    var_186 = var_182 & 7UL;
                    *(uint64_t *)(local_sp_14 + (-8L)) = 4208810UL;
                    var_187 = indirect_placeholder_2(var_186);
                    rcx3_1 = rcx3_10;
                    rcx3_0 = rcx3_10;
                    r95_0 = r95_12;
                    r86_0 = r86_12;
                    r95_1 = r95_12;
                    r86_1 = r86_12;
                    rcx3_2 = rcx3_10;
                    r95_2 = r95_12;
                    r86_2 = r86_12;
                    rcx3_3 = rcx3_10;
                    r95_3 = r95_12;
                    r86_3 = r86_12;
                    if ((uint64_t)(unsigned char)var_187 != 0UL) {
                        var_188 = local_sp_14 + (-16L);
                        *(uint64_t *)var_188 = 4209045UL;
                        var_189 = indirect_placeholder_109(0UL, 4276536UL, 1UL, 0UL, rcx3_10, r95_12, r86_12);
                        var_190 = var_189.field_0;
                        var_191 = var_189.field_1;
                        var_192 = var_189.field_2;
                        rcx3_3 = var_190;
                        r95_3 = var_191;
                        r86_3 = var_192;
                        local_sp_3 = var_188;
                        var_196 = local_sp_3 + (-8L);
                        *(uint64_t *)var_196 = 4209070UL;
                        var_197 = indirect_placeholder_108(0UL, 4276584UL, 1UL, 0UL, rcx3_3, r95_3, r86_3);
                        rcx3_2 = var_197.field_0;
                        r95_2 = var_197.field_1;
                        r86_2 = var_197.field_2;
                        local_sp_2 = var_196;
                        var_201 = local_sp_2 + (-8L);
                        *(uint64_t *)var_201 = 4209095UL;
                        var_202 = indirect_placeholder_107(0UL, 4276624UL, 1UL, 0UL, rcx3_2, r95_2, r86_2);
                        var_203 = var_202.field_0;
                        var_204 = var_202.field_1;
                        var_205 = var_202.field_2;
                        rcx3_1 = var_203;
                        r95_1 = var_204;
                        r86_1 = var_205;
                        local_sp_1 = var_201;
                        var_209 = local_sp_1 + (-8L);
                        *(uint64_t *)var_209 = 4209120UL;
                        var_210 = indirect_placeholder_106(0UL, 4276656UL, 1UL, 0UL, rcx3_1, r95_1, r86_1);
                        rcx3_0 = var_210.field_0;
                        r95_0 = var_210.field_1;
                        r86_0 = var_210.field_2;
                        local_sp_0 = var_209;
                        var_217 = local_sp_0 + (-8L);
                        *(uint64_t *)var_217 = 4209145UL;
                        var_218 = indirect_placeholder_105(0UL, 4276688UL, 1UL, 0UL, rcx3_0, r95_0, r86_0);
                        var_219 = var_218.field_0;
                        var_220 = var_218.field_1;
                        var_221 = var_218.field_2;
                        rcx3_9 = var_219;
                        r95_11 = var_220;
                        r86_11 = var_221;
                        local_sp_13 = var_217;
                        *(unsigned char *)4302376UL = (unsigned char)'\x01';
                        rcx3_10 = rcx3_9;
                        r95_12 = r95_11;
                        r86_12 = r86_11;
                        local_sp_14 = local_sp_13;
                        continue;
                    }
                    var_193 = var_182 & 24UL;
                    var_194 = local_sp_14 + (-16L);
                    *(uint64_t *)var_194 = 4208828UL;
                    var_195 = indirect_placeholder_2(var_193);
                    local_sp_3 = var_194;
                    if ((uint64_t)(unsigned char)var_195 == 0UL) {
                        var_198 = var_182 & 96UL;
                        var_199 = local_sp_14 + (-24L);
                        *(uint64_t *)var_199 = 4208846UL;
                        var_200 = indirect_placeholder_2(var_198);
                        local_sp_2 = var_199;
                        if ((uint64_t)(unsigned char)var_200 != 0UL) {
                            var_206 = (uint64_t)(var_184 & (unsigned short)12288U);
                            var_207 = local_sp_14 + (-32L);
                            *(uint64_t *)var_207 = 4208867UL;
                            var_208 = indirect_placeholder_2(var_206);
                            local_sp_1 = var_207;
                            if ((uint64_t)(unsigned char)var_208 == 0UL) {
                                var_211 = (uint64_t)((uint16_t)var_149 & (unsigned short)16386U);
                                var_212 = local_sp_14 + (-40L);
                                *(uint64_t *)var_212 = 4208888UL;
                                var_213 = indirect_placeholder_2(var_211);
                                local_sp_0 = var_212;
                                var_214 = (uint64_t)((uint16_t)var_145 & (unsigned short)16386U);
                                var_215 = local_sp_14 + (-48L);
                                *(uint64_t *)var_215 = 4208910UL;
                                var_216 = indirect_placeholder_2(var_214);
                                local_sp_0 = var_215;
                                if ((uint64_t)(unsigned char)var_213 != 0UL & (uint64_t)(unsigned char)var_216 == 0UL) {
                                    break;
                                }
                            }
                            var_209 = local_sp_1 + (-8L);
                            *(uint64_t *)var_209 = 4209120UL;
                            var_210 = indirect_placeholder_106(0UL, 4276656UL, 1UL, 0UL, rcx3_1, r95_1, r86_1);
                            rcx3_0 = var_210.field_0;
                            r95_0 = var_210.field_1;
                            r86_0 = var_210.field_2;
                            local_sp_0 = var_209;
                            var_217 = local_sp_0 + (-8L);
                            *(uint64_t *)var_217 = 4209145UL;
                            var_218 = indirect_placeholder_105(0UL, 4276688UL, 1UL, 0UL, rcx3_0, r95_0, r86_0);
                            var_219 = var_218.field_0;
                            var_220 = var_218.field_1;
                            var_221 = var_218.field_2;
                            rcx3_9 = var_219;
                            r95_11 = var_220;
                            r86_11 = var_221;
                            local_sp_13 = var_217;
                            *(unsigned char *)4302376UL = (unsigned char)'\x01';
                            rcx3_10 = rcx3_9;
                            r95_12 = r95_11;
                            r86_12 = r86_11;
                            local_sp_14 = local_sp_13;
                            continue;
                        }
                    }
                    var_196 = local_sp_3 + (-8L);
                    *(uint64_t *)var_196 = 4209070UL;
                    var_197 = indirect_placeholder_108(0UL, 4276584UL, 1UL, 0UL, rcx3_3, r95_3, r86_3);
                    rcx3_2 = var_197.field_0;
                    r95_2 = var_197.field_1;
                    r86_2 = var_197.field_2;
                    local_sp_2 = var_196;
                }
            if ((var_149 & 2UL) == 0UL) {
                *(unsigned char *)4302187UL = (unsigned char)'\x01';
                *(unsigned char *)4302185UL = ((*(uint64_t *)4301552UL | *(uint64_t *)4302480UL) == 0UL);
                *(uint32_t *)4302472UL = (var_143 & (-4));
            }
            if ((var_145 & 2UL) == 0UL) {
                *(unsigned char *)4302186UL = (unsigned char)'\x01';
                *(unsigned char *)4302184UL = ((*(uint64_t *)4301552UL | *(uint64_t *)4302480UL) == 0UL);
                *(uint32_t *)4302468UL = (var_144 & (-3));
            }
            return rcx3_10;
        }
        if (*(uint64_t *)4302520UL == 0UL) {
            if ((*(uint64_t *)4301552UL + (-1L)) <= 18446744073709551613UL) {
                if ((uint32_t)(((uint16_t)var_144 | (uint16_t)var_143) & (unsigned short)16384U) == 0U) {
                    *(unsigned char *)4302376UL = (unsigned char)'\x00';
                } else {
                    *(unsigned char *)4302376UL = (unsigned char)'\x01';
                    rcx3_10 = rcx3_9;
                    r95_12 = r95_11;
                    r86_12 = r86_11;
                    local_sp_14 = local_sp_13;
                }
                while (1U)
                    {
                        *(uint64_t *)4302176UL = rax_4;
                        *(uint32_t *)4302472UL = (var_143 & (-2));
                        var_186 = var_182 & 7UL;
                        *(uint64_t *)(local_sp_14 + (-8L)) = 4208810UL;
                        var_187 = indirect_placeholder_2(var_186);
                        rcx3_1 = rcx3_10;
                        rcx3_0 = rcx3_10;
                        r95_0 = r95_12;
                        r86_0 = r86_12;
                        r95_1 = r95_12;
                        r86_1 = r86_12;
                        rcx3_2 = rcx3_10;
                        r95_2 = r95_12;
                        r86_2 = r86_12;
                        rcx3_3 = rcx3_10;
                        r95_3 = r95_12;
                        r86_3 = r86_12;
                        if ((uint64_t)(unsigned char)var_187 != 0UL) {
                            var_188 = local_sp_14 + (-16L);
                            *(uint64_t *)var_188 = 4209045UL;
                            var_189 = indirect_placeholder_109(0UL, 4276536UL, 1UL, 0UL, rcx3_10, r95_12, r86_12);
                            var_190 = var_189.field_0;
                            var_191 = var_189.field_1;
                            var_192 = var_189.field_2;
                            rcx3_3 = var_190;
                            r95_3 = var_191;
                            r86_3 = var_192;
                            local_sp_3 = var_188;
                            var_196 = local_sp_3 + (-8L);
                            *(uint64_t *)var_196 = 4209070UL;
                            var_197 = indirect_placeholder_108(0UL, 4276584UL, 1UL, 0UL, rcx3_3, r95_3, r86_3);
                            rcx3_2 = var_197.field_0;
                            r95_2 = var_197.field_1;
                            r86_2 = var_197.field_2;
                            local_sp_2 = var_196;
                            var_201 = local_sp_2 + (-8L);
                            *(uint64_t *)var_201 = 4209095UL;
                            var_202 = indirect_placeholder_107(0UL, 4276624UL, 1UL, 0UL, rcx3_2, r95_2, r86_2);
                            var_203 = var_202.field_0;
                            var_204 = var_202.field_1;
                            var_205 = var_202.field_2;
                            rcx3_1 = var_203;
                            r95_1 = var_204;
                            r86_1 = var_205;
                            local_sp_1 = var_201;
                            var_209 = local_sp_1 + (-8L);
                            *(uint64_t *)var_209 = 4209120UL;
                            var_210 = indirect_placeholder_106(0UL, 4276656UL, 1UL, 0UL, rcx3_1, r95_1, r86_1);
                            rcx3_0 = var_210.field_0;
                            r95_0 = var_210.field_1;
                            r86_0 = var_210.field_2;
                            local_sp_0 = var_209;
                            var_217 = local_sp_0 + (-8L);
                            *(uint64_t *)var_217 = 4209145UL;
                            var_218 = indirect_placeholder_105(0UL, 4276688UL, 1UL, 0UL, rcx3_0, r95_0, r86_0);
                            var_219 = var_218.field_0;
                            var_220 = var_218.field_1;
                            var_221 = var_218.field_2;
                            rcx3_9 = var_219;
                            r95_11 = var_220;
                            r86_11 = var_221;
                            local_sp_13 = var_217;
                            *(unsigned char *)4302376UL = (unsigned char)'\x01';
                            rcx3_10 = rcx3_9;
                            r95_12 = r95_11;
                            r86_12 = r86_11;
                            local_sp_14 = local_sp_13;
                            continue;
                        }
                        var_193 = var_182 & 24UL;
                        var_194 = local_sp_14 + (-16L);
                        *(uint64_t *)var_194 = 4208828UL;
                        var_195 = indirect_placeholder_2(var_193);
                        local_sp_3 = var_194;
                        if ((uint64_t)(unsigned char)var_195 == 0UL) {
                            var_198 = var_182 & 96UL;
                            var_199 = local_sp_14 + (-24L);
                            *(uint64_t *)var_199 = 4208846UL;
                            var_200 = indirect_placeholder_2(var_198);
                            local_sp_2 = var_199;
                            if ((uint64_t)(unsigned char)var_200 != 0UL) {
                                var_206 = (uint64_t)(var_184 & (unsigned short)12288U);
                                var_207 = local_sp_14 + (-32L);
                                *(uint64_t *)var_207 = 4208867UL;
                                var_208 = indirect_placeholder_2(var_206);
                                local_sp_1 = var_207;
                                if ((uint64_t)(unsigned char)var_208 == 0UL) {
                                    var_211 = (uint64_t)((uint16_t)var_149 & (unsigned short)16386U);
                                    var_212 = local_sp_14 + (-40L);
                                    *(uint64_t *)var_212 = 4208888UL;
                                    var_213 = indirect_placeholder_2(var_211);
                                    local_sp_0 = var_212;
                                    var_214 = (uint64_t)((uint16_t)var_145 & (unsigned short)16386U);
                                    var_215 = local_sp_14 + (-48L);
                                    *(uint64_t *)var_215 = 4208910UL;
                                    var_216 = indirect_placeholder_2(var_214);
                                    local_sp_0 = var_215;
                                    if ((uint64_t)(unsigned char)var_213 != 0UL & (uint64_t)(unsigned char)var_216 == 0UL) {
                                        break;
                                    }
                                }
                                var_209 = local_sp_1 + (-8L);
                                *(uint64_t *)var_209 = 4209120UL;
                                var_210 = indirect_placeholder_106(0UL, 4276656UL, 1UL, 0UL, rcx3_1, r95_1, r86_1);
                                rcx3_0 = var_210.field_0;
                                r95_0 = var_210.field_1;
                                r86_0 = var_210.field_2;
                                local_sp_0 = var_209;
                                var_217 = local_sp_0 + (-8L);
                                *(uint64_t *)var_217 = 4209145UL;
                                var_218 = indirect_placeholder_105(0UL, 4276688UL, 1UL, 0UL, rcx3_0, r95_0, r86_0);
                                var_219 = var_218.field_0;
                                var_220 = var_218.field_1;
                                var_221 = var_218.field_2;
                                rcx3_9 = var_219;
                                r95_11 = var_220;
                                r86_11 = var_221;
                                local_sp_13 = var_217;
                                *(unsigned char *)4302376UL = (unsigned char)'\x01';
                                rcx3_10 = rcx3_9;
                                r95_12 = r95_11;
                                r86_12 = r86_11;
                                local_sp_14 = local_sp_13;
                                continue;
                            }
                        }
                        var_196 = local_sp_3 + (-8L);
                        *(uint64_t *)var_196 = 4209070UL;
                        var_197 = indirect_placeholder_108(0UL, 4276584UL, 1UL, 0UL, rcx3_3, r95_3, r86_3);
                        rcx3_2 = var_197.field_0;
                        r95_2 = var_197.field_1;
                        r86_2 = var_197.field_2;
                        local_sp_2 = var_196;
                    }
                if ((var_149 & 2UL) == 0UL) {
                    *(unsigned char *)4302187UL = (unsigned char)'\x01';
                    *(unsigned char *)4302185UL = ((*(uint64_t *)4301552UL | *(uint64_t *)4302480UL) == 0UL);
                    *(uint32_t *)4302472UL = (var_143 & (-4));
                }
                if ((var_145 & 2UL) != 0UL) {
                    *(unsigned char *)4302186UL = (unsigned char)'\x01';
                    *(unsigned char *)4302184UL = ((*(uint64_t *)4301552UL | *(uint64_t *)4302480UL) == 0UL);
                    *(uint32_t *)4302468UL = (var_144 & (-3));
                }
                return rcx3_10;
            }
            *(unsigned char *)4302376UL = (unsigned char)'\x01';
            rcx3_10 = rcx3_9;
            r95_12 = r95_11;
            r86_12 = r86_11;
            local_sp_14 = local_sp_13;
        } else {
            *(unsigned char *)4302376UL = (unsigned char)'\x01';
            rcx3_10 = rcx3_9;
            r95_12 = r95_11;
            r86_12 = r86_11;
            local_sp_14 = local_sp_13;
        }
    }
}
