typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_invalidate_cache_ret_type;
struct helper_divq_EAX_wrapper_ret_type;
struct type_6;
struct indirect_placeholder_92_ret_type;
struct indirect_placeholder_91_ret_type;
struct bb_invalidate_cache_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_divq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint32_t field_4;
    uint32_t field_5;
    uint32_t field_6;
    uint64_t field_7;
    uint32_t field_8;
    uint64_t field_9;
    uint32_t field_10;
    unsigned char field_11;
    uint32_t field_12;
};
struct type_6 {
};
struct indirect_placeholder_92_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_91_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_r13(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern void indirect_placeholder_1(void);
extern uint64_t init_r10(void);
extern uint64_t init_r9(void);
extern uint32_t init_state_0x8248(void);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern struct helper_divq_EAX_wrapper_ret_type helper_divq_EAX_wrapper(struct type_6 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint32_t param_13, uint64_t param_14, uint32_t param_15, uint64_t param_16, uint64_t param_17, uint64_t param_18, uint32_t param_19);
extern uint64_t init_r8(void);
extern struct indirect_placeholder_92_ret_type indirect_placeholder_92(uint64_t param_0, uint64_t param_1);
extern uint64_t init_rcx(void);
extern uint64_t init_rdx(void);
extern struct indirect_placeholder_91_ret_type indirect_placeholder_91(uint64_t param_0, uint64_t param_1);
struct bb_invalidate_cache_ret_type bb_invalidate_cache(uint64_t rdi, uint64_t rsi) {
    uint64_t rax_3;
    uint64_t var_46;
    uint64_t var_35;
    struct indirect_placeholder_92_ret_type var_36;
    uint64_t var_37;
    uint64_t rax_4;
    struct bb_invalidate_cache_ret_type mrv;
    struct bb_invalidate_cache_ret_type mrv1;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint32_t var_11;
    uint64_t var_12;
    uint32_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint32_t var_17;
    uint64_t var_18;
    unsigned char var_19;
    unsigned char var_20;
    bool var_21;
    unsigned char _v;
    uint64_t var_22;
    uint64_t var_23;
    struct indirect_placeholder_91_ret_type var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    bool var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t rcx_1;
    uint64_t var_33;
    uint64_t rax_0;
    bool var_34;
    uint64_t rdi2_0;
    uint64_t r14_0;
    uint64_t rax_1;
    uint64_t local_sp_0;
    uint64_t rsi3_0;
    uint64_t rcx_0;
    uint64_t var_38;
    uint64_t rdi2_2;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t local_sp_2;
    uint64_t var_42;
    uint64_t spec_select;
    uint64_t rsi3_1;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t rax_2;
    struct helper_divq_EAX_wrapper_ret_type var_45;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rdx();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_rcx();
    var_8 = init_r10();
    var_9 = init_r9();
    var_10 = init_r8();
    var_11 = init_state_0x8248();
    var_12 = init_state_0x9018();
    var_13 = init_state_0x9010();
    var_14 = init_state_0x8408();
    var_15 = init_state_0x8328();
    var_16 = init_state_0x82d8();
    var_17 = init_state_0x9080();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_3;
    var_18 = (uint64_t)(uint32_t)rdi;
    var_19 = *(unsigned char *)4302185UL;
    var_20 = *(unsigned char *)4302184UL;
    var_21 = (var_18 == 0UL);
    _v = var_21 ? var_19 : var_20;
    var_22 = (uint64_t)_v;
    var_23 = var_0 + (-48L);
    *(uint64_t *)var_23 = 4210338UL;
    var_24 = indirect_placeholder_91(rdi, rsi);
    var_25 = var_24.field_0;
    var_26 = var_24.field_1;
    var_27 = (rsi != 0UL);
    var_28 = var_25 & (-256L);
    var_29 = var_28 | var_27;
    var_30 = (var_25 == 0UL);
    var_31 = (var_29 & ((var_1 & (-256L)) | var_30)) & 1UL;
    var_32 = var_28 | var_31;
    rax_3 = 4294967295UL;
    rax_4 = var_32;
    rcx_1 = var_7;
    rdi2_0 = var_26;
    r14_0 = 0UL;
    local_sp_0 = var_23;
    rsi3_0 = 18446744073709551615UL;
    rcx_0 = var_7;
    if (var_31 == 0UL) {
        mrv.field_0 = rax_4;
        mrv1 = mrv;
        mrv1.field_1 = rcx_1;
        return mrv1;
    }
    var_33 = var_25 | rsi;
    rax_0 = var_33;
    rax_4 = 1UL;
    if (var_33 != 0UL) {
        rax_0 = 1UL;
        if (_v == '\x00') {
            mrv.field_0 = rax_4;
            mrv1 = mrv;
            mrv1.field_1 = rcx_1;
            return mrv1;
        }
    }
    var_34 = (rsi == 0UL);
    rax_1 = rax_0;
    if (var_34) {
        var_35 = var_0 + (-56L);
        *(uint64_t *)var_35 = 4210441UL;
        var_36 = indirect_placeholder_92(var_18, 0UL);
        var_37 = var_36.field_0;
        rdi2_0 = var_36.field_1;
        r14_0 = var_37;
        rax_1 = var_37;
        local_sp_0 = var_35;
    }
    rdi2_2 = rdi2_0;
    local_sp_2 = local_sp_0;
    if (var_21) {
        if (*(unsigned char *)4302396UL != '\x00') {
            *(uint64_t *)(local_sp_0 + (-8L)) = 4210416UL;
            indirect_placeholder_1();
            *(uint32_t *)rax_1 = 29U;
            var_46 = (rax_3 & (-256L)) | ((uint64_t)((uint32_t)rax_3 + 1U) != 0UL);
            rcx_1 = rcx_0;
            rax_4 = var_46;
            mrv.field_0 = rax_4;
            mrv1 = mrv;
            mrv1.field_1 = rcx_1;
            return mrv1;
        }
        rsi3_0 = *(uint64_t *)4302384UL;
    } else {
        var_38 = *(uint64_t *)4301536UL;
        if (var_38 != 18446744073709551615UL) {
            rsi3_0 = var_38;
            rdi2_2 = var_18;
            if ((long)var_38 < (long)0UL) {
                var_40 = local_sp_0 + (-8L);
                *(uint64_t *)var_40 = 4210574UL;
                indirect_placeholder_1();
                *(uint64_t *)4301536UL = var_38;
                local_sp_2 = var_40;
            } else {
                if (var_34) {
                    var_39 = var_38 + (var_25 + r14_0);
                    *(uint64_t *)4301536UL = var_39;
                    rsi3_0 = var_39;
                }
            }
        }
    }
    if ((long)rsi3_0 >= (long)0UL) {
        var_41 = var_34;
        rcx_0 = 4UL;
        if ((var_41 & var_22) == 0UL) {
            var_43 = var_41 | 4294967040UL;
            var_44 = (rsi3_0 - var_25) - r14_0;
            rsi3_1 = var_44;
            rax_2 = var_43;
            if (var_30) {
                var_45 = helper_divq_EAX_wrapper((struct type_6 *)(0UL), *(uint64_t *)4302552UL, 4210594UL, rsi3_1, 0UL, rsi, rdi2_2, var_18, rsi3_1, var_7, var_8, var_9, var_10, var_11, var_12, var_13, var_14, var_15, var_16, var_17);
                rax_2 = var_45.field_1;
            }
        } else {
            var_42 = rsi3_0 - var_25;
            spec_select = var_30 ? (var_42 - r14_0) : var_42;
            rsi3_1 = spec_select;
            var_45 = helper_divq_EAX_wrapper((struct type_6 *)(0UL), *(uint64_t *)4302552UL, 4210594UL, rsi3_1, 0UL, rsi, rdi2_2, var_18, rsi3_1, var_7, var_8, var_9, var_10, var_11, var_12, var_13, var_14, var_15, var_16, var_17);
            rax_2 = var_45.field_1;
        }
        *(uint64_t *)(local_sp_2 + (-8L)) = 4210542UL;
        indirect_placeholder_1();
        rax_3 = rax_2;
    }
    var_46 = (rax_3 & (-256L)) | ((uint64_t)((uint32_t)rax_3 + 1U) != 0UL);
    rcx_1 = rcx_0;
    rax_4 = var_46;
}
