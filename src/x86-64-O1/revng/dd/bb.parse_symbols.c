typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_parse_symbols_ret_type;
struct indirect_placeholder_150_ret_type;
struct indirect_placeholder_149_ret_type;
struct bb_parse_symbols_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_150_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_149_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rax(void);
extern uint64_t init_r13(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r15(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint32_t init_state_0x82fc(void);
extern void indirect_placeholder_1(void);
extern uint64_t indirect_placeholder_71(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_r10(void);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern void indirect_placeholder_81(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_150_ret_type indirect_placeholder_150(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_149_ret_type indirect_placeholder_149(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
struct bb_parse_symbols_ret_type bb_parse_symbols(uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t rcx) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint32_t var_11;
    uint64_t var_12;
    bool var_13;
    uint64_t rbp_0;
    uint64_t local_sp_0;
    uint32_t var_18;
    uint64_t var_19;
    uint64_t local_sp_1;
    uint64_t rax_0;
    struct bb_parse_symbols_ret_type mrv;
    struct bb_parse_symbols_ret_type mrv1;
    struct bb_parse_symbols_ret_type mrv2;
    struct bb_parse_symbols_ret_type mrv3;
    struct bb_parse_symbols_ret_type mrv4;
    struct bb_parse_symbols_ret_type mrv5;
    struct bb_parse_symbols_ret_type mrv6;
    uint64_t rbx_0;
    uint64_t var_15;
    uint64_t *var_16;
    uint64_t var_17;
    uint64_t rcx10_2;
    uint64_t var_20;
    uint64_t rdi8_0;
    uint64_t rcx10_0;
    uint64_t rcx10_1;
    unsigned char var_21;
    uint64_t var_22;
    struct indirect_placeholder_150_ret_type var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t storemerge;
    uint64_t var_14;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r15();
    var_5 = init_rbp();
    var_6 = init_r12();
    var_7 = init_r14();
    var_8 = init_r10();
    var_9 = init_r9();
    var_10 = init_r8();
    var_11 = init_state_0x82fc();
    *(uint64_t *)(var_0 + (-8L)) = var_4;
    *(uint64_t *)(var_0 + (-16L)) = var_7;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_6;
    *(uint64_t *)(var_0 + (-40L)) = var_5;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_12 = var_0 + (-72L);
    *(uint64_t *)(var_0 + (-64L)) = rcx;
    var_13 = ((uint64_t)(unsigned char)rdx == 0UL);
    rbp_0 = rdi;
    local_sp_1 = var_12;
    rax_0 = var_1;
    rbx_0 = rsi;
    rcx10_0 = 18446744073709551615UL;
    rcx10_1 = 0UL;
    storemerge = 0UL;
    while (1U)
        {
            var_14 = local_sp_1 + (-8L);
            *(uint64_t *)var_14 = 4206872UL;
            indirect_placeholder_1();
            local_sp_0 = var_14;
            rdi8_0 = rbp_0;
            while (1U)
                {
                    var_15 = local_sp_0 + (-8L);
                    var_16 = (uint64_t *)var_15;
                    *var_16 = 4206821UL;
                    var_17 = indirect_placeholder_71(44UL, rbp_0, rbx_0);
                    local_sp_0 = var_15;
                    local_sp_1 = var_15;
                    if ((uint64_t)(unsigned char)var_17 != 0UL) {
                        var_18 = *(uint32_t *)(rbx_0 + 12UL);
                        if (var_18 != 0U) {
                            loop_state_var = 1U;
                            break;
                        }
                    }
                    if (*(unsigned char *)rbx_0 == '\x00') {
                        rbx_0 = rbx_0 + 16UL;
                        continue;
                    }
                    if (rax_0 != 0UL) {
                        rcx10_2 = rax_0 - rbp_0;
                        loop_state_var = 2U;
                        break;
                    }
                    var_20 = (uint64_t)var_11;
                    loop_state_var = 0U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    var_19 = (var_13 ? storemerge : 0UL) | (uint64_t)var_18;
                    rax_0 = var_19;
                    storemerge = var_19;
                    if (rax_0 != 0UL) {
                        rbp_0 = rax_0 + 1UL;
                        continue;
                    }
                    mrv.field_0 = var_19;
                    mrv1 = mrv;
                    mrv1.field_1 = rbp_0;
                    mrv2 = mrv1;
                    mrv2.field_2 = rbx_0;
                    mrv3 = mrv2;
                    mrv3.field_3 = rcx;
                    mrv4 = mrv3;
                    mrv4.field_4 = var_8;
                    mrv5 = mrv4;
                    mrv5.field_5 = var_9;
                    mrv6 = mrv5;
                    mrv6.field_6 = var_10;
                    return mrv6;
                }
                break;
              case 0U:
                {
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 2U:
                {
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 1U:
        {
            *(uint64_t *)(local_sp_0 + (-16L)) = 4206909UL;
            var_23 = indirect_placeholder_150(rbp_0, 0UL, 8UL, rcx10_2);
            var_24 = var_23.field_0;
            var_25 = var_23.field_1;
            var_26 = *var_16;
            *(uint64_t *)(local_sp_0 + (-24L)) = 4206942UL;
            indirect_placeholder_149(0UL, 4272333UL, 0UL, 0UL, var_26, var_25, var_24);
            *(uint64_t *)(local_sp_0 + (-32L)) = 4206952UL;
            indirect_placeholder_81(rbx_0, 1UL, rbp_0);
            abort();
        }
        break;
      case 0U:
        {
            while (rcx10_0 != 0UL)
                {
                    rdi8_0 = rdi8_0 + var_20;
                }
            rcx10_2 = 18446744073709551614UL - (-2L);
        }
        break;
    }
}
