typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_iwrite_ret_type;
struct indirect_placeholder_159_ret_type;
struct indirect_placeholder_158_ret_type;
struct indirect_placeholder_160_ret_type;
struct indirect_placeholder_162_ret_type;
struct indirect_placeholder_161_ret_type;
struct indirect_placeholder_157_ret_type;
struct bb_iwrite_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_159_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_158_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_160_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_162_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_161_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_157_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rax(void);
extern uint64_t init_r13(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern void indirect_placeholder_1(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
extern uint64_t indirect_placeholder_24(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct indirect_placeholder_159_ret_type indirect_placeholder_159(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_158_ret_type indirect_placeholder_158(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_160_ret_type indirect_placeholder_160(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_162_ret_type indirect_placeholder_162(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_161_ret_type indirect_placeholder_161(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_157_ret_type indirect_placeholder_157(uint64_t param_0, uint64_t param_1);
struct bb_iwrite_ret_type bb_iwrite(uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t rcx) {
    struct indirect_placeholder_161_ret_type var_14;
    struct indirect_placeholder_162_ret_type var_10;
    struct indirect_placeholder_159_ret_type var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t rbx_3;
    uint64_t local_sp_4;
    uint64_t var_22;
    struct indirect_placeholder_160_ret_type var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_30;
    uint64_t rax_3;
    uint64_t var_31;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t rbx_4;
    uint64_t rcx5_0;
    uint64_t local_sp_6;
    bool var_26;
    uint64_t rcx5_1;
    struct bb_iwrite_ret_type mrv;
    struct bb_iwrite_ret_type mrv1;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t rax_4;
    uint64_t rax_0;
    uint64_t rax_2;
    uint64_t rbx_5;
    uint64_t rax_5;
    uint64_t rbp_1;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t local_sp_5;
    uint64_t local_sp_7;
    uint64_t var_32;
    uint64_t rbx_0;
    uint64_t local_sp_0;
    uint64_t var_34;
    uint64_t rbx_2;
    uint64_t rax_1;
    uint64_t local_sp_3;
    uint64_t rbx_1;
    uint64_t rbp_0;
    uint64_t local_sp_1;
    uint64_t rbp_2;
    uint64_t var_27;
    uint64_t local_sp_2;
    uint64_t rbp_3;
    struct indirect_placeholder_157_ret_type var_36;
    uint64_t var_33;
    uint64_t var_35;
    uint64_t var_15;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    var_8 = var_0 + (-40L);
    *(uint64_t *)var_8 = var_3;
    var_9 = (uint64_t)(uint32_t)rdi;
    rbx_3 = var_3;
    rbx_4 = var_3;
    rcx5_0 = rcx;
    local_sp_6 = var_8;
    rax_4 = var_1;
    rbx_0 = rdx;
    rbx_2 = rdx;
    rbp_2 = 0UL;
    rbp_3 = 0UL;
    if ((*(unsigned char *)4302469UL & '@') != '\x00' & *(uint64_t *)4302536UL <= rdx) {
        *(uint64_t *)(var_0 + (-48L)) = 4212216UL;
        var_10 = indirect_placeholder_162(0UL, rdx, 1UL, 3UL, rcx);
        var_11 = var_10.field_1;
        var_12 = (uint64_t)((uint32_t)var_10.field_0 & (-16385));
        var_13 = var_0 + (-56L);
        *(uint64_t *)var_13 = 4212241UL;
        var_14 = indirect_placeholder_161(0UL, var_12, 1UL, 4UL, var_11);
        local_sp_4 = var_13;
        if ((uint64_t)(uint32_t)var_14.field_0 != 0UL & *(uint32_t *)4301548UL == 1U) {
            var_15 = *(uint64_t *)4302560UL;
            *(uint64_t *)(var_0 + (-64L)) = 4212310UL;
            var_16 = indirect_placeholder_159(var_15, 0UL, 3UL);
            var_17 = var_16.field_0;
            var_18 = var_16.field_1;
            var_19 = var_16.field_2;
            *(uint64_t *)(var_0 + (-72L)) = 4212318UL;
            indirect_placeholder_1();
            var_20 = (uint64_t)*(uint32_t *)var_17;
            var_21 = var_0 + (-80L);
            *(uint64_t *)var_21 = 4212343UL;
            indirect_placeholder_158(0UL, 4277304UL, 0UL, var_20, var_17, var_18, var_19);
            rbx_3 = var_17;
            local_sp_4 = var_21;
        }
        *(unsigned char *)4302184UL = (unsigned char)'\x01';
        var_22 = local_sp_4 + (-8L);
        *(uint64_t *)var_22 = 4212276UL;
        var_23 = indirect_placeholder_160(1UL, 0UL);
        var_24 = var_23.field_0;
        var_25 = var_23.field_1;
        *(uint32_t *)4302476UL = (*(uint32_t *)4302476UL | 32768U);
        rax_4 = var_24;
        rbx_4 = rbx_3;
        rcx5_0 = var_25;
        local_sp_6 = var_22;
    }
    rax_5 = rax_4;
    rbx_5 = rbx_4;
    local_sp_7 = local_sp_6;
    rcx5_1 = rcx5_0;
    if (rdx == 0UL) {
        mrv.field_0 = rbp_3;
        mrv1 = mrv;
        mrv1.field_1 = rcx5_1;
        return mrv1;
    }
    var_26 = ((long)rdx > (long)18446744073709551615UL);
    while (1U)
        {
            var_27 = local_sp_7 + (-8L);
            *(uint64_t *)var_27 = 4212397UL;
            indirect_placeholder_2(rbx_5);
            *(unsigned char *)4302488UL = (unsigned char)'\x00';
            rbp_0 = rbp_2;
            rbp_1 = rbp_2;
            rax_3 = rax_5;
            local_sp_5 = var_27;
            if ((*(unsigned char *)4302478UL & '\x01') != '\x00') {
                var_31 = local_sp_5 + (-8L);
                *(uint64_t *)var_31 = 4212374UL;
                indirect_placeholder_1();
                rax_0 = rax_3;
                rax_2 = rax_3;
                rbx_0 = rax_3;
                local_sp_0 = var_31;
                rbx_2 = rax_3;
                local_sp_3 = var_31;
                if ((long)rax_3 < (long)0UL) {
                    var_35 = local_sp_3 + (-8L);
                    *(uint64_t *)var_35 = 4212469UL;
                    indirect_placeholder_1();
                    rax_1 = rax_2;
                    rbx_1 = rbx_2;
                    local_sp_1 = var_35;
                    local_sp_2 = var_35;
                    if (*(uint32_t *)rax_2 == 4U) {
                        break;
                    }
                    rbx_5 = rbx_1;
                    rax_5 = rax_1;
                    rbp_1 = rbp_0;
                    local_sp_7 = local_sp_1;
                    rbp_2 = rbp_0;
                    local_sp_2 = local_sp_1;
                    if (rbp_0 >= rdx) {
                        continue;
                    }
                    break;
                }
                var_32 = helper_cc_compute_all_wrapper(rax_3, rbp_2, var_7, 25U);
                if ((var_32 & 64UL) != 0UL) {
                    var_34 = rbp_2 + rbx_0;
                    rax_1 = rax_0;
                    rbx_1 = rbx_0;
                    rbp_0 = var_34;
                    local_sp_1 = local_sp_0;
                    rbx_5 = rbx_1;
                    rax_5 = rax_1;
                    rbp_1 = rbp_0;
                    local_sp_7 = local_sp_1;
                    rbp_2 = rbp_0;
                    local_sp_2 = local_sp_1;
                    if (rbp_0 >= rdx) {
                        continue;
                    }
                    break;
                }
                var_33 = local_sp_5 + (-16L);
                *(uint64_t *)var_33 = 4212481UL;
                indirect_placeholder_1();
                *(uint32_t *)rax_3 = 28U;
                local_sp_2 = var_33;
                break;
            }
            var_28 = local_sp_7 + (-16L);
            *(uint64_t *)var_28 = 4212424UL;
            var_29 = indirect_placeholder_24(rsi, rdx);
            rax_0 = var_29;
            rax_2 = var_29;
            rax_3 = var_29;
            local_sp_5 = var_28;
            if ((uint64_t)(unsigned char)var_29 != 0UL) {
                var_31 = local_sp_5 + (-8L);
                *(uint64_t *)var_31 = 4212374UL;
                indirect_placeholder_1();
                rax_0 = rax_3;
                rax_2 = rax_3;
                rbx_0 = rax_3;
                local_sp_0 = var_31;
                rbx_2 = rax_3;
                local_sp_3 = var_31;
                if ((long)rax_3 >= (long)0UL) {
                    var_32 = helper_cc_compute_all_wrapper(rax_3, rbp_2, var_7, 25U);
                    if ((var_32 & 64UL) != 0UL) {
                        var_34 = rbp_2 + rbx_0;
                        rax_1 = rax_0;
                        rbx_1 = rbx_0;
                        rbp_0 = var_34;
                        local_sp_1 = local_sp_0;
                        rbx_5 = rbx_1;
                        rax_5 = rax_1;
                        rbp_1 = rbp_0;
                        local_sp_7 = local_sp_1;
                        rbp_2 = rbp_0;
                        local_sp_2 = local_sp_1;
                        if (rbp_0 >= rdx) {
                            continue;
                        }
                        break;
                    }
                    var_33 = local_sp_5 + (-16L);
                    *(uint64_t *)var_33 = 4212481UL;
                    indirect_placeholder_1();
                    *(uint32_t *)rax_3 = 28U;
                    local_sp_2 = var_33;
                    break;
                }
            }
            var_30 = local_sp_7 + (-24L);
            *(uint64_t *)var_30 = 4212447UL;
            indirect_placeholder_1();
            local_sp_0 = var_30;
            local_sp_3 = var_30;
            local_sp_5 = var_30;
            if ((long)var_29 >= (long)0UL) {
                *(unsigned char *)4302488UL = (unsigned char)'\x01';
                if (!var_26) {
                    var_34 = rbp_2 + rbx_0;
                    rax_1 = rax_0;
                    rbx_1 = rbx_0;
                    rbp_0 = var_34;
                    local_sp_1 = local_sp_0;
                    rbx_5 = rbx_1;
                    rax_5 = rax_1;
                    rbp_1 = rbp_0;
                    local_sp_7 = local_sp_1;
                    rbp_2 = rbp_0;
                    local_sp_2 = local_sp_1;
                    if (rbp_0 >= rdx) {
                        continue;
                    }
                    break;
                }
            }
            *(uint32_t *)4302476UL = (*(uint32_t *)4302476UL & (-65537));
            var_31 = local_sp_5 + (-8L);
            *(uint64_t *)var_31 = 4212374UL;
            indirect_placeholder_1();
            rax_0 = rax_3;
            rax_2 = rax_3;
            rbx_0 = rax_3;
            local_sp_0 = var_31;
            rbx_2 = rax_3;
            local_sp_3 = var_31;
            if ((long)rax_3 >= (long)0UL) {
                var_32 = helper_cc_compute_all_wrapper(rax_3, rbp_2, var_7, 25U);
                if ((var_32 & 64UL) == 0UL) {
                    var_34 = rbp_2 + rbx_0;
                    rax_1 = rax_0;
                    rbx_1 = rbx_0;
                    rbp_0 = var_34;
                    local_sp_1 = local_sp_0;
                    rbx_5 = rbx_1;
                    rax_5 = rax_1;
                    rbp_1 = rbp_0;
                    local_sp_7 = local_sp_1;
                    rbp_2 = rbp_0;
                    local_sp_2 = local_sp_1;
                    if (rbp_0 >= rdx) {
                        continue;
                    }
                    break;
                }
                var_33 = local_sp_5 + (-16L);
                *(uint64_t *)var_33 = 4212481UL;
                indirect_placeholder_1();
                *(uint32_t *)rax_3 = 28U;
                local_sp_2 = var_33;
                break;
            }
        }
    rbp_3 = rbp_1;
    if (!((*(unsigned char *)4302186UL == '\x00') || (rbp_1 == 0UL))) {
        mrv.field_0 = rbp_3;
        mrv1 = mrv;
        mrv1.field_1 = rcx5_1;
        return mrv1;
    }
    *(uint64_t *)(local_sp_2 + (-8L)) = 4212524UL;
    var_36 = indirect_placeholder_157(var_9, rbp_1);
    rcx5_1 = var_36.field_1;
    mrv.field_0 = rbp_3;
    mrv1 = mrv;
    mrv1.field_1 = rcx5_1;
    return mrv1;
}
