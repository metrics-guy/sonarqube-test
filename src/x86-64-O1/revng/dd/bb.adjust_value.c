typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct type_5;
struct helper_fldcw_wrapper_ret_type;
struct helper_fmov_FT0_STN_wrapper_ret_type;
struct helper_fadd_ST0_FT0_wrapper_ret_type;
struct helper_fpop_wrapper_ret_type;
struct helper_fldt_ST0_wrapper_ret_type;
struct helper_fmov_STN_ST0_wrapper_ret_type;
struct helper_fpush_wrapper_ret_type;
struct helper_fcomi_ST0_FT0_wrapper_ret_type;
struct helper_fmov_ST0_STN_wrapper_ret_type;
struct helper_fxchg_ST0_STN_wrapper_ret_type;
struct helper_flds_FT0_wrapper_ret_type;
struct helper_fildll_ST0_wrapper_ret_type;
struct helper_fucomi_ST0_FT0_wrapper_ret_type;
struct helper_flds_ST0_wrapper_ret_type;
struct helper_fistll_ST0_wrapper_ret_type;
struct helper_fsub_ST0_FT0_wrapper_ret_type;
struct type_5 {
};
struct helper_fldcw_wrapper_ret_type {
    unsigned char field_0;
    unsigned char field_1;
    uint16_t field_2;
};
struct helper_fmov_FT0_STN_wrapper_ret_type {
    uint64_t field_0;
    uint16_t field_1;
};
struct helper_fadd_ST0_FT0_wrapper_ret_type {
    unsigned char field_0;
    uint16_t field_1;
    uint16_t field_2;
    uint16_t field_3;
    uint16_t field_4;
    uint16_t field_5;
    uint16_t field_6;
    uint16_t field_7;
    uint16_t field_8;
    uint64_t field_9;
    uint64_t field_10;
    uint64_t field_11;
    uint64_t field_12;
    uint64_t field_13;
    uint64_t field_14;
    uint64_t field_15;
    uint64_t field_16;
};
struct helper_fpop_wrapper_ret_type {
    uint32_t field_0;
    unsigned char field_1;
    unsigned char field_2;
    unsigned char field_3;
    unsigned char field_4;
    unsigned char field_5;
    unsigned char field_6;
    unsigned char field_7;
    unsigned char field_8;
};
struct helper_fldt_ST0_wrapper_ret_type {
    uint16_t field_0;
    uint16_t field_1;
    uint16_t field_2;
    uint16_t field_3;
    uint16_t field_4;
    uint16_t field_5;
    uint16_t field_6;
    uint16_t field_7;
    uint64_t field_8;
    uint64_t field_9;
    uint64_t field_10;
    uint64_t field_11;
    uint64_t field_12;
    uint64_t field_13;
    uint64_t field_14;
    uint64_t field_15;
    uint32_t field_16;
    unsigned char field_17;
    unsigned char field_18;
    unsigned char field_19;
    unsigned char field_20;
    unsigned char field_21;
    unsigned char field_22;
    unsigned char field_23;
    unsigned char field_24;
};
struct helper_fmov_STN_ST0_wrapper_ret_type {
    uint16_t field_0;
    uint16_t field_1;
    uint16_t field_2;
    uint16_t field_3;
    uint16_t field_4;
    uint16_t field_5;
    uint16_t field_6;
    uint16_t field_7;
    uint64_t field_8;
    uint64_t field_9;
    uint64_t field_10;
    uint64_t field_11;
    uint64_t field_12;
    uint64_t field_13;
    uint64_t field_14;
    uint64_t field_15;
};
struct helper_fpush_wrapper_ret_type {
    uint32_t field_0;
    unsigned char field_1;
    unsigned char field_2;
    unsigned char field_3;
    unsigned char field_4;
    unsigned char field_5;
    unsigned char field_6;
    unsigned char field_7;
    unsigned char field_8;
};
struct helper_fcomi_ST0_FT0_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_fmov_ST0_STN_wrapper_ret_type {
    uint16_t field_0;
    uint16_t field_1;
    uint16_t field_2;
    uint16_t field_3;
    uint16_t field_4;
    uint16_t field_5;
    uint16_t field_6;
    uint16_t field_7;
    uint64_t field_8;
    uint64_t field_9;
    uint64_t field_10;
    uint64_t field_11;
    uint64_t field_12;
    uint64_t field_13;
    uint64_t field_14;
    uint64_t field_15;
};
struct helper_fxchg_ST0_STN_wrapper_ret_type {
    uint16_t field_0;
    uint16_t field_1;
    uint16_t field_2;
    uint16_t field_3;
    uint16_t field_4;
    uint16_t field_5;
    uint16_t field_6;
    uint16_t field_7;
    uint64_t field_8;
    uint64_t field_9;
    uint64_t field_10;
    uint64_t field_11;
    uint64_t field_12;
    uint64_t field_13;
    uint64_t field_14;
    uint64_t field_15;
};
struct helper_flds_FT0_wrapper_ret_type {
    unsigned char field_0;
    uint64_t field_1;
    uint16_t field_2;
};
struct helper_fildll_ST0_wrapper_ret_type {
    uint16_t field_0;
    uint16_t field_1;
    uint16_t field_2;
    uint16_t field_3;
    uint16_t field_4;
    uint16_t field_5;
    uint16_t field_6;
    uint16_t field_7;
    uint64_t field_8;
    uint64_t field_9;
    uint64_t field_10;
    uint64_t field_11;
    uint64_t field_12;
    uint64_t field_13;
    uint64_t field_14;
    uint64_t field_15;
    uint32_t field_16;
    unsigned char field_17;
    unsigned char field_18;
    unsigned char field_19;
    unsigned char field_20;
    unsigned char field_21;
    unsigned char field_22;
    unsigned char field_23;
    unsigned char field_24;
};
struct helper_fucomi_ST0_FT0_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_flds_ST0_wrapper_ret_type {
    unsigned char field_0;
    uint16_t field_1;
    uint16_t field_2;
    uint16_t field_3;
    uint16_t field_4;
    uint16_t field_5;
    uint16_t field_6;
    uint16_t field_7;
    uint16_t field_8;
    uint64_t field_9;
    uint64_t field_10;
    uint64_t field_11;
    uint64_t field_12;
    uint64_t field_13;
    uint64_t field_14;
    uint64_t field_15;
    uint64_t field_16;
    uint32_t field_17;
    unsigned char field_18;
    unsigned char field_19;
    unsigned char field_20;
    unsigned char field_21;
    unsigned char field_22;
    unsigned char field_23;
    unsigned char field_24;
    unsigned char field_25;
};
struct helper_fistll_ST0_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_fsub_ST0_FT0_wrapper_ret_type {
    unsigned char field_0;
    uint16_t field_1;
    uint16_t field_2;
    uint16_t field_3;
    uint16_t field_4;
    uint16_t field_5;
    uint16_t field_6;
    uint16_t field_7;
    uint16_t field_8;
    uint64_t field_9;
    uint64_t field_10;
    uint64_t field_11;
    uint64_t field_12;
    uint64_t field_13;
    uint64_t field_14;
    uint64_t field_15;
    uint64_t field_16;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern unsigned char init_state_0x852a(void);
extern unsigned char init_state_0x852c(void);
extern unsigned char init_state_0x8528(void);
extern unsigned char init_state_0x852e(void);
extern uint32_t init_state_0x8480(void);
extern uint16_t init_state_0x8486(void);
extern uint32_t helper_fnstcw_wrapper(struct type_5 *param_0, uint16_t param_1);
extern struct helper_fldcw_wrapper_ret_type helper_fldcw_wrapper(struct type_5 *param_0, uint32_t param_1, uint16_t param_2);
extern struct helper_fmov_FT0_STN_wrapper_ret_type helper_fmov_FT0_STN_wrapper(struct type_5 *param_0, uint32_t param_1, uint16_t param_2, uint16_t param_3, uint16_t param_4, uint16_t param_5, uint16_t param_6, uint16_t param_7, uint16_t param_8, uint16_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint64_t param_13, uint64_t param_14, uint64_t param_15, uint64_t param_16, uint64_t param_17, uint32_t param_18);
extern struct helper_fadd_ST0_FT0_wrapper_ret_type helper_fadd_ST0_FT0_wrapper(struct type_5 *param_0, unsigned char param_1, unsigned char param_2, unsigned char param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6, uint16_t param_7, uint16_t param_8, uint16_t param_9, uint16_t param_10, uint16_t param_11, uint16_t param_12, uint16_t param_13, uint16_t param_14, uint64_t param_15, uint64_t param_16, uint64_t param_17, uint64_t param_18, uint64_t param_19, uint64_t param_20, uint64_t param_21, uint64_t param_22, uint32_t param_23, uint64_t param_24, uint16_t param_25);
extern struct helper_fpop_wrapper_ret_type helper_fpop_wrapper(struct type_5 *param_0, uint32_t param_1);
extern struct helper_fldt_ST0_wrapper_ret_type helper_fldt_ST0_wrapper(struct type_5 *param_0, uint64_t param_1, uint32_t param_2);
extern struct helper_fmov_STN_ST0_wrapper_ret_type helper_fmov_STN_ST0_wrapper(struct type_5 *param_0, uint32_t param_1, uint16_t param_2, uint16_t param_3, uint16_t param_4, uint16_t param_5, uint16_t param_6, uint16_t param_7, uint16_t param_8, uint16_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint64_t param_13, uint64_t param_14, uint64_t param_15, uint64_t param_16, uint64_t param_17, uint32_t param_18);
extern struct helper_fpush_wrapper_ret_type helper_fpush_wrapper(struct type_5 *param_0, uint32_t param_1);
extern struct helper_fcomi_ST0_FT0_wrapper_ret_type helper_fcomi_ST0_FT0_wrapper(struct type_5 *param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3, uint64_t param_4, unsigned char param_5, uint16_t param_6, uint16_t param_7, uint16_t param_8, uint16_t param_9, uint16_t param_10, uint16_t param_11, uint16_t param_12, uint16_t param_13, uint64_t param_14, uint64_t param_15, uint64_t param_16, uint64_t param_17, uint64_t param_18, uint64_t param_19, uint64_t param_20, uint64_t param_21, uint32_t param_22, uint64_t param_23, uint16_t param_24);
extern unsigned char init_state_0x852d(void);
extern unsigned char init_state_0x8529(void);
extern unsigned char init_state_0x852b(void);
extern struct helper_fmov_ST0_STN_wrapper_ret_type helper_fmov_ST0_STN_wrapper(struct type_5 *param_0, uint32_t param_1, uint16_t param_2, uint16_t param_3, uint16_t param_4, uint16_t param_5, uint16_t param_6, uint16_t param_7, uint16_t param_8, uint16_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint64_t param_13, uint64_t param_14, uint64_t param_15, uint64_t param_16, uint64_t param_17, uint32_t param_18);
extern struct helper_fxchg_ST0_STN_wrapper_ret_type helper_fxchg_ST0_STN_wrapper(struct type_5 *param_0, uint32_t param_1, uint16_t param_2, uint16_t param_3, uint16_t param_4, uint16_t param_5, uint16_t param_6, uint16_t param_7, uint16_t param_8, uint16_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint64_t param_13, uint64_t param_14, uint64_t param_15, uint64_t param_16, uint64_t param_17, uint32_t param_18);
extern struct helper_flds_FT0_wrapper_ret_type helper_flds_FT0_wrapper(struct type_5 *param_0, uint32_t param_1, unsigned char param_2, unsigned char param_3, unsigned char param_4);
extern struct helper_fildll_ST0_wrapper_ret_type helper_fildll_ST0_wrapper(struct type_5 *param_0, uint64_t param_1, uint32_t param_2);
extern struct helper_fucomi_ST0_FT0_wrapper_ret_type helper_fucomi_ST0_FT0_wrapper(struct type_5 *param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3, uint64_t param_4, unsigned char param_5, uint16_t param_6, uint16_t param_7, uint16_t param_8, uint16_t param_9, uint16_t param_10, uint16_t param_11, uint16_t param_12, uint16_t param_13, uint64_t param_14, uint64_t param_15, uint64_t param_16, uint64_t param_17, uint64_t param_18, uint64_t param_19, uint64_t param_20, uint64_t param_21, uint32_t param_22, uint64_t param_23, uint16_t param_24);
extern struct helper_flds_ST0_wrapper_ret_type helper_flds_ST0_wrapper(struct type_5 *param_0, uint32_t param_1, unsigned char param_2, unsigned char param_3, unsigned char param_4, uint32_t param_5);
extern struct helper_fistll_ST0_wrapper_ret_type helper_fistll_ST0_wrapper(struct type_5 *param_0, unsigned char param_1, unsigned char param_2, uint16_t param_3, uint16_t param_4, uint16_t param_5, uint16_t param_6, uint16_t param_7, uint16_t param_8, uint16_t param_9, uint16_t param_10, uint64_t param_11, uint64_t param_12, uint64_t param_13, uint64_t param_14, uint64_t param_15, uint64_t param_16, uint64_t param_17, uint64_t param_18, uint32_t param_19);
extern struct helper_fsub_ST0_FT0_wrapper_ret_type helper_fsub_ST0_FT0_wrapper(struct type_5 *param_0, unsigned char param_1, unsigned char param_2, unsigned char param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6, uint16_t param_7, uint16_t param_8, uint16_t param_9, uint16_t param_10, uint16_t param_11, uint16_t param_12, uint16_t param_13, uint16_t param_14, uint64_t param_15, uint64_t param_16, uint64_t param_17, uint64_t param_18, uint64_t param_19, uint64_t param_20, uint64_t param_21, uint64_t param_22, uint32_t param_23, uint64_t param_24, uint16_t param_25);
void bb_adjust_value(uint64_t rdi) {
    struct helper_flds_FT0_wrapper_ret_type var_211;
    struct helper_flds_FT0_wrapper_ret_type var_131;
    struct helper_flds_FT0_wrapper_ret_type var_183;
    uint64_t var_0;
    uint64_t var_1;
    unsigned char var_2;
    unsigned char var_3;
    unsigned char var_4;
    unsigned char var_5;
    unsigned char var_6;
    unsigned char var_7;
    unsigned char var_8;
    uint32_t var_9;
    uint16_t var_10;
    struct helper_fldt_ST0_wrapper_ret_type var_11;
    uint64_t var_12;
    struct helper_fldt_ST0_wrapper_ret_type var_13;
    uint16_t var_14;
    uint16_t var_15;
    uint16_t var_16;
    uint16_t var_17;
    uint16_t var_18;
    uint16_t var_19;
    uint16_t var_20;
    uint16_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint32_t var_30;
    struct helper_fmov_FT0_STN_wrapper_ret_type var_31;
    uint64_t var_33;
    struct helper_fpop_wrapper_ret_type var_34;
    struct helper_fcomi_ST0_FT0_wrapper_ret_type var_32;
    struct helper_flds_ST0_wrapper_ret_type var_35;
    unsigned char var_36;
    uint16_t var_37;
    uint16_t var_38;
    uint16_t var_39;
    uint16_t var_40;
    uint16_t var_41;
    uint16_t var_42;
    uint16_t var_43;
    uint16_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint32_t var_53;
    uint16_t var_55;
    uint16_t var_56;
    uint16_t var_57;
    uint16_t var_58;
    struct helper_fxchg_ST0_STN_wrapper_ret_type var_54;
    uint16_t var_59;
    uint16_t var_60;
    uint16_t var_61;
    uint16_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    struct helper_fmov_FT0_STN_wrapper_ret_type var_71;
    uint64_t var_73;
    unsigned char var_74;
    struct helper_fcomi_ST0_FT0_wrapper_ret_type var_72;
    uint16_t var_76;
    uint16_t var_77;
    uint16_t var_78;
    uint16_t var_79;
    struct helper_fmov_STN_ST0_wrapper_ret_type var_75;
    uint16_t var_80;
    uint16_t var_81;
    uint16_t var_82;
    uint16_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    struct helper_fpop_wrapper_ret_type var_92;
    uint32_t var_93;
    uint64_t var_94;
    bool var_95;
    uint64_t var_96;
    uint32_t var_97;
    uint16_t *var_98;
    uint16_t var_99;
    uint16_t var_100;
    uint16_t *var_101;
    struct helper_fpush_wrapper_ret_type var_102;
    uint32_t var_103;
    uint16_t var_105;
    uint16_t var_106;
    uint16_t var_107;
    uint16_t var_108;
    struct helper_fmov_ST0_STN_wrapper_ret_type var_104;
    uint16_t var_109;
    uint16_t var_110;
    uint16_t var_111;
    uint16_t var_112;
    uint64_t var_113;
    uint64_t var_114;
    uint64_t var_115;
    uint64_t var_116;
    uint64_t var_117;
    uint64_t var_118;
    uint64_t var_119;
    uint64_t var_120;
    struct helper_fldcw_wrapper_ret_type var_121;
    uint16_t var_122;
    uint64_t var_123;
    uint64_t var_125;
    unsigned char var_126;
    uint64_t *var_127;
    struct helper_fistll_ST0_wrapper_ret_type var_124;
    struct helper_fpop_wrapper_ret_type var_128;
    uint32_t var_129;
    struct helper_fldcw_wrapper_ret_type var_130;
    unsigned char state_0x852a_0;
    unsigned char var_133;
    uint16_t var_134;
    uint16_t var_135;
    uint16_t var_136;
    struct helper_fsub_ST0_FT0_wrapper_ret_type var_132;
    uint16_t var_137;
    uint16_t var_138;
    uint16_t var_139;
    uint16_t var_140;
    uint16_t var_141;
    uint64_t var_142;
    uint64_t var_143;
    uint64_t var_144;
    uint64_t var_145;
    uint64_t var_146;
    uint64_t var_147;
    uint64_t var_148;
    uint64_t var_149;
    struct helper_fldcw_wrapper_ret_type var_150;
    uint16_t var_151;
    uint64_t var_152;
    uint64_t var_154;
    unsigned char var_155;
    uint64_t *var_156;
    struct helper_fistll_ST0_wrapper_ret_type var_153;
    struct helper_fpop_wrapper_ret_type var_157;
    uint32_t var_158;
    struct helper_fldcw_wrapper_ret_type var_159;
    unsigned char var_160;
    unsigned char var_161;
    uint64_t var_162;
    uint64_t cc_src_0;
    uint64_t rdx_0;
    unsigned char state_0x8529_0;
    unsigned char state_0x852b_0;
    uint64_t state_0x84d0_0;
    uint64_t state_0x84e0_0;
    uint16_t state_0x8498_0;
    uint16_t state_0x84a8_0;
    uint16_t state_0x84b8_0;
    uint16_t state_0x84c8_0;
    uint16_t state_0x84d8_0;
    uint16_t state_0x84e8_0;
    uint16_t state_0x84f8_0;
    uint16_t state_0x8508_0;
    uint64_t state_0x8490_0;
    uint64_t state_0x84a0_0;
    uint64_t state_0x84b0_0;
    uint64_t state_0x84c0_0;
    uint64_t state_0x84f0_1;
    uint64_t state_0x8500_1;
    uint64_t state_0x84f0_0;
    uint64_t state_0x8500_0;
    uint32_t state_0x8480_0;
    uint64_t *var_164;
    struct helper_fildll_ST0_wrapper_ret_type var_165;
    uint16_t var_166;
    uint16_t var_167;
    uint16_t var_168;
    uint16_t var_169;
    uint16_t var_170;
    uint16_t var_171;
    uint16_t var_172;
    uint16_t var_173;
    uint64_t var_174;
    uint64_t var_175;
    uint64_t var_176;
    uint64_t var_177;
    uint64_t var_178;
    uint64_t var_179;
    uint64_t var_180;
    uint64_t var_181;
    uint32_t var_182;
    unsigned char state_0x852a_1;
    struct helper_fadd_ST0_FT0_wrapper_ret_type var_184;
    uint16_t state_0x8498_1;
    uint16_t state_0x84a8_1;
    uint16_t state_0x84b8_1;
    uint16_t state_0x84c8_1;
    uint16_t state_0x84d8_1;
    uint16_t state_0x84e8_1;
    uint16_t state_0x84f8_1;
    uint16_t state_0x8508_1;
    uint64_t state_0x8490_1;
    uint64_t state_0x84a0_1;
    uint64_t state_0x84b0_1;
    uint64_t state_0x84c0_1;
    uint64_t state_0x84d0_1;
    uint64_t state_0x84e0_1;
    struct helper_fmov_FT0_STN_wrapper_ret_type var_185;
    uint64_t var_187;
    unsigned char var_188;
    struct helper_fpop_wrapper_ret_type var_189;
    struct helper_fucomi_ST0_FT0_wrapper_ret_type var_186;
    uint32_t var_190;
    struct helper_fpop_wrapper_ret_type var_191;
    uint64_t *_pre_phi365;
    struct helper_fpop_wrapper_ret_type var_163;
    unsigned char state_0x852a_2;
    uint64_t rax_0;
    uint32_t state_0x8480_1;
    uint64_t var_192;
    struct helper_fildll_ST0_wrapper_ret_type var_193;
    uint32_t var_194;
    uint64_t var_195;
    uint64_t var_196;
    uint64_t var_197;
    uint64_t var_198;
    uint64_t var_199;
    uint64_t var_200;
    uint64_t var_201;
    uint64_t var_202;
    uint16_t var_203;
    uint16_t var_204;
    uint16_t var_205;
    uint16_t var_206;
    uint16_t var_207;
    uint16_t var_208;
    uint16_t var_209;
    uint16_t var_210;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_cc_src2();
    var_2 = init_state_0x852a();
    var_3 = init_state_0x852d();
    var_4 = init_state_0x852c();
    var_5 = init_state_0x8528();
    var_6 = init_state_0x8529();
    var_7 = init_state_0x852e();
    var_8 = init_state_0x852b();
    var_9 = init_state_0x8480();
    var_10 = init_state_0x8486();
    var_11 = helper_fldt_ST0_wrapper((struct type_5 *)(0UL), var_0 | 8UL, var_9);
    var_12 = rdi + (-1L);
    rax_0 = 0UL;
    var_13 = helper_fldt_ST0_wrapper((struct type_5 *)(0UL), 4279264UL, var_11.field_16);
    var_14 = var_13.field_0;
    var_15 = var_13.field_1;
    var_16 = var_13.field_2;
    var_17 = var_13.field_3;
    var_18 = var_13.field_4;
    var_19 = var_13.field_5;
    var_20 = var_13.field_6;
    var_21 = var_13.field_7;
    var_22 = var_13.field_8;
    var_23 = var_13.field_9;
    var_24 = var_13.field_10;
    var_25 = var_13.field_11;
    var_26 = var_13.field_12;
    var_27 = var_13.field_13;
    var_28 = var_13.field_14;
    var_29 = var_13.field_15;
    var_30 = var_13.field_16;
    var_31 = helper_fmov_FT0_STN_wrapper((struct type_5 *)(0UL), 1U, var_14, var_15, var_16, var_17, var_18, var_19, var_20, var_21, var_22, var_23, var_24, var_25, var_26, var_27, var_28, var_29, var_30);
    var_32 = helper_fcomi_ST0_FT0_wrapper((struct type_5 *)(0UL), 1UL, var_12, 16U, var_1, var_2, var_14, var_15, var_16, var_17, var_18, var_19, var_20, var_21, var_22, var_23, var_24, var_25, var_26, var_27, var_28, var_29, var_30, var_31.field_0, var_31.field_1);
    var_33 = var_32.field_0;
    var_34 = helper_fpop_wrapper((struct type_5 *)(0UL), var_30);
    if (~((uint64_t)(uint32_t)var_12 != 0UL & (var_33 & 65UL) != 0UL)) {
        return;
    }
    var_35 = helper_flds_ST0_wrapper((struct type_5 *)(0UL), *(uint32_t *)4279296UL, var_32.field_1, var_3, var_7, var_34.field_0);
    var_36 = var_35.field_0;
    var_37 = var_35.field_1;
    var_38 = var_35.field_2;
    var_39 = var_35.field_3;
    var_40 = var_35.field_4;
    var_41 = var_35.field_5;
    var_42 = var_35.field_6;
    var_43 = var_35.field_7;
    var_44 = var_35.field_8;
    var_45 = var_35.field_9;
    var_46 = var_35.field_10;
    var_47 = var_35.field_11;
    var_48 = var_35.field_12;
    var_49 = var_35.field_13;
    var_50 = var_35.field_14;
    var_51 = var_35.field_15;
    var_52 = var_35.field_16;
    var_53 = var_35.field_17;
    var_54 = helper_fxchg_ST0_STN_wrapper((struct type_5 *)(0UL), 1U, var_37, var_38, var_39, var_40, var_41, var_42, var_43, var_44, var_45, var_46, var_47, var_48, var_49, var_50, var_51, var_52, var_53);
    var_55 = var_54.field_0;
    var_56 = var_54.field_1;
    var_57 = var_54.field_2;
    var_58 = var_54.field_3;
    var_59 = var_54.field_4;
    var_60 = var_54.field_5;
    var_61 = var_54.field_6;
    var_62 = var_54.field_7;
    var_63 = var_54.field_8;
    var_64 = var_54.field_9;
    var_65 = var_54.field_10;
    var_66 = var_54.field_11;
    var_67 = var_54.field_12;
    var_68 = var_54.field_13;
    var_69 = var_54.field_14;
    var_70 = var_54.field_15;
    var_71 = helper_fmov_FT0_STN_wrapper((struct type_5 *)(0UL), 1U, var_55, var_56, var_57, var_58, var_59, var_60, var_61, var_62, var_63, var_64, var_65, var_66, var_67, var_68, var_69, var_70, var_53);
    var_72 = helper_fcomi_ST0_FT0_wrapper((struct type_5 *)(0UL), var_33, var_12, 1U, var_1, var_36, var_55, var_56, var_57, var_58, var_59, var_60, var_61, var_62, var_63, var_64, var_65, var_66, var_67, var_68, var_69, var_70, var_53, var_71.field_0, var_71.field_1);
    var_73 = var_72.field_0;
    var_74 = var_72.field_1;
    var_75 = helper_fmov_STN_ST0_wrapper((struct type_5 *)(0UL), 1U, var_55, var_56, var_57, var_58, var_59, var_60, var_61, var_62, var_63, var_64, var_65, var_66, var_67, var_68, var_69, var_70, var_53);
    var_76 = var_75.field_0;
    var_77 = var_75.field_1;
    var_78 = var_75.field_2;
    var_79 = var_75.field_3;
    var_80 = var_75.field_4;
    var_81 = var_75.field_5;
    var_82 = var_75.field_6;
    var_83 = var_75.field_7;
    var_84 = var_75.field_8;
    var_85 = var_75.field_9;
    var_86 = var_75.field_10;
    var_87 = var_75.field_11;
    var_88 = var_75.field_12;
    var_89 = var_75.field_13;
    var_90 = var_75.field_14;
    var_91 = var_75.field_15;
    var_92 = helper_fpop_wrapper((struct type_5 *)(0UL), var_53);
    var_93 = var_92.field_0;
    var_94 = helper_cc_compute_c_wrapper(var_12, var_73, var_1, 1U);
    var_95 = (var_94 == 0UL);
    var_96 = var_0 + (-10L);
    var_97 = helper_fnstcw_wrapper((struct type_5 *)(0UL), var_10);
    var_98 = (uint16_t *)var_96;
    var_99 = (uint16_t)var_97;
    *var_98 = var_99;
    var_100 = (uint16_t)(unsigned char)var_99 | ((var_99 & (unsigned short)62208U) | (unsigned short)3072U);
    var_101 = (uint16_t *)(var_0 + (-12L));
    *var_101 = var_100;
    var_102 = helper_fpush_wrapper((struct type_5 *)(0UL), var_93);
    var_103 = var_102.field_0;
    var_104 = helper_fmov_ST0_STN_wrapper((struct type_5 *)(0UL), 1U, var_76, var_77, var_78, var_79, var_80, var_81, var_82, var_83, var_84, var_85, var_86, var_87, var_88, var_89, var_90, var_91, var_103);
    var_105 = var_104.field_0;
    var_106 = var_104.field_1;
    var_107 = var_104.field_2;
    var_108 = var_104.field_3;
    var_109 = var_104.field_4;
    var_110 = var_104.field_5;
    var_111 = var_104.field_6;
    var_112 = var_104.field_7;
    var_113 = var_104.field_8;
    var_114 = var_104.field_9;
    var_115 = var_104.field_10;
    var_116 = var_104.field_11;
    var_117 = var_104.field_12;
    var_118 = var_104.field_13;
    var_119 = var_104.field_14;
    var_120 = var_104.field_15;
    cc_src_0 = var_73;
    state_0x84d0_0 = var_117;
    state_0x84e0_0 = var_118;
    state_0x8498_0 = var_105;
    state_0x84a8_0 = var_106;
    state_0x84b8_0 = var_107;
    state_0x84c8_0 = var_108;
    state_0x84d8_0 = var_109;
    state_0x84e8_0 = var_110;
    state_0x84f8_0 = var_111;
    state_0x8508_0 = var_112;
    state_0x8490_0 = var_113;
    state_0x84a0_0 = var_114;
    state_0x84b0_0 = var_115;
    state_0x84c0_0 = var_116;
    state_0x84f0_0 = var_119;
    state_0x8500_0 = var_120;
    if (var_95) {
        var_131 = helper_flds_FT0_wrapper((struct type_5 *)(0UL), *(uint32_t *)4279296UL, var_74, var_3, var_7);
        var_132 = helper_fsub_ST0_FT0_wrapper((struct type_5 *)(0UL), var_131.field_0, var_4, var_5, var_6, var_7, var_8, var_105, var_106, var_107, var_108, var_109, var_110, var_111, var_112, var_113, var_114, var_115, var_116, var_117, var_118, var_119, var_120, var_103, var_131.field_1, var_131.field_2);
        var_133 = var_132.field_0;
        var_134 = var_132.field_1;
        var_135 = var_132.field_2;
        var_136 = var_132.field_3;
        var_137 = var_132.field_4;
        var_138 = var_132.field_5;
        var_139 = var_132.field_6;
        var_140 = var_132.field_7;
        var_141 = var_132.field_8;
        var_142 = var_132.field_9;
        var_143 = var_132.field_10;
        var_144 = var_132.field_11;
        var_145 = var_132.field_12;
        var_146 = var_132.field_13;
        var_147 = var_132.field_14;
        var_148 = var_132.field_15;
        var_149 = var_132.field_16;
        var_150 = helper_fldcw_wrapper((struct type_5 *)(0UL), (uint32_t)*var_101, var_10);
        var_151 = var_150.field_2;
        var_152 = var_0 + (-24L);
        var_153 = helper_fistll_ST0_wrapper((struct type_5 *)(0UL), var_133, var_150.field_0, var_134, var_135, var_136, var_137, var_138, var_139, var_140, var_141, var_142, var_143, var_144, var_145, var_146, var_147, var_148, var_149, var_103);
        var_154 = var_153.field_0;
        var_155 = var_153.field_1;
        var_156 = (uint64_t *)var_152;
        *var_156 = var_154;
        var_157 = helper_fpop_wrapper((struct type_5 *)(0UL), var_103);
        var_158 = var_157.field_0;
        var_159 = helper_fldcw_wrapper((struct type_5 *)(0UL), (uint32_t)*var_98, var_151);
        var_160 = var_159.field_0;
        var_161 = var_159.field_1;
        var_162 = *var_156;
        state_0x852a_0 = var_155;
        cc_src_0 = var_162 >> 63UL;
        rdx_0 = var_162 ^ (-9223372036854775808L);
        state_0x8529_0 = var_160;
        state_0x852b_0 = var_161;
        state_0x84d0_0 = var_146;
        state_0x84e0_0 = var_147;
        state_0x8498_0 = var_134;
        state_0x84a8_0 = var_135;
        state_0x84b8_0 = var_136;
        state_0x84c8_0 = var_137;
        state_0x84d8_0 = var_138;
        state_0x84e8_0 = var_139;
        state_0x84f8_0 = var_140;
        state_0x8508_0 = var_141;
        state_0x8490_0 = var_142;
        state_0x84a0_0 = var_143;
        state_0x84b0_0 = var_144;
        state_0x84c0_0 = var_145;
        state_0x84f0_0 = var_148;
        state_0x8500_0 = var_149;
        state_0x8480_0 = var_158;
    } else {
        var_121 = helper_fldcw_wrapper((struct type_5 *)(0UL), (uint32_t)*var_101, var_10);
        var_122 = var_121.field_2;
        var_123 = var_0 + (-24L);
        var_124 = helper_fistll_ST0_wrapper((struct type_5 *)(0UL), var_74, var_121.field_0, var_105, var_106, var_107, var_108, var_109, var_110, var_111, var_112, var_113, var_114, var_115, var_116, var_117, var_118, var_119, var_120, var_103);
        var_125 = var_124.field_0;
        var_126 = var_124.field_1;
        var_127 = (uint64_t *)var_123;
        *var_127 = var_125;
        var_128 = helper_fpop_wrapper((struct type_5 *)(0UL), var_103);
        var_129 = var_128.field_0;
        var_130 = helper_fldcw_wrapper((struct type_5 *)(0UL), (uint32_t)*var_98, var_122);
        state_0x852a_0 = var_126;
        rdx_0 = *var_127;
        state_0x8529_0 = var_130.field_0;
        state_0x852b_0 = var_130.field_1;
        state_0x8480_0 = var_129;
    }
    state_0x852a_1 = state_0x852a_0;
    state_0x852a_2 = state_0x852a_0;
    if ((uint64_t)(uint32_t)rdi == 0UL) {
        helper_fmov_STN_ST0_wrapper((struct type_5 *)(0UL), 0U, state_0x8498_0, state_0x84a8_0, state_0x84b8_0, state_0x84c8_0, state_0x84d8_0, state_0x84e8_0, state_0x84f8_0, state_0x8508_0, state_0x8490_0, state_0x84a0_0, state_0x84b0_0, state_0x84c0_0, state_0x84d0_0, state_0x84e0_0, state_0x84f0_0, state_0x8500_0, state_0x8480_0);
        var_163 = helper_fpop_wrapper((struct type_5 *)(0UL), state_0x8480_0);
        _pre_phi365 = (uint64_t *)(var_0 + (-24L));
        state_0x8480_1 = var_163.field_0;
    } else {
        var_164 = (uint64_t *)(var_0 + (-24L));
        *var_164 = rdx_0;
        var_165 = helper_fildll_ST0_wrapper((struct type_5 *)(0UL), rdx_0, state_0x8480_0);
        var_166 = var_165.field_0;
        var_167 = var_165.field_1;
        var_168 = var_165.field_2;
        var_169 = var_165.field_3;
        var_170 = var_165.field_4;
        var_171 = var_165.field_5;
        var_172 = var_165.field_6;
        var_173 = var_165.field_7;
        var_174 = var_165.field_8;
        var_175 = var_165.field_9;
        var_176 = var_165.field_10;
        var_177 = var_165.field_11;
        var_178 = var_165.field_12;
        var_179 = var_165.field_13;
        var_180 = var_165.field_14;
        var_181 = var_165.field_15;
        var_182 = var_165.field_16;
        state_0x84f0_1 = var_180;
        state_0x8500_1 = var_181;
        state_0x8498_1 = var_166;
        state_0x84a8_1 = var_167;
        state_0x84b8_1 = var_168;
        state_0x84c8_1 = var_169;
        state_0x84d8_1 = var_170;
        state_0x84e8_1 = var_171;
        state_0x84f8_1 = var_172;
        state_0x8508_1 = var_173;
        state_0x8490_1 = var_174;
        state_0x84a0_1 = var_175;
        state_0x84b0_1 = var_176;
        state_0x84c0_1 = var_177;
        state_0x84d0_1 = var_178;
        state_0x84e0_1 = var_179;
        _pre_phi365 = var_164;
        if ((long)rdx_0 < (long)0UL) {
            var_183 = helper_flds_FT0_wrapper((struct type_5 *)(0UL), *(uint32_t *)4279300UL, state_0x852a_0, var_3, var_7);
            var_184 = helper_fadd_ST0_FT0_wrapper((struct type_5 *)(0UL), var_183.field_0, var_4, var_5, state_0x8529_0, var_7, state_0x852b_0, var_166, var_167, var_168, var_169, var_170, var_171, var_172, var_173, var_174, var_175, var_176, var_177, var_178, var_179, var_180, var_181, var_182, var_183.field_1, var_183.field_2);
            state_0x84f0_1 = var_184.field_15;
            state_0x8500_1 = var_184.field_16;
            state_0x852a_1 = var_184.field_0;
            state_0x8498_1 = var_184.field_1;
            state_0x84a8_1 = var_184.field_2;
            state_0x84b8_1 = var_184.field_3;
            state_0x84c8_1 = var_184.field_4;
            state_0x84d8_1 = var_184.field_5;
            state_0x84e8_1 = var_184.field_6;
            state_0x84f8_1 = var_184.field_7;
            state_0x8508_1 = var_184.field_8;
            state_0x8490_1 = var_184.field_9;
            state_0x84a0_1 = var_184.field_10;
            state_0x84b0_1 = var_184.field_11;
            state_0x84c0_1 = var_184.field_12;
            state_0x84d0_1 = var_184.field_13;
            state_0x84e0_1 = var_184.field_14;
        }
        var_185 = helper_fmov_FT0_STN_wrapper((struct type_5 *)(0UL), 1U, state_0x8498_1, state_0x84a8_1, state_0x84b8_1, state_0x84c8_1, state_0x84d8_1, state_0x84e8_1, state_0x84f8_1, state_0x8508_1, state_0x8490_1, state_0x84a0_1, state_0x84b0_1, state_0x84c0_1, state_0x84d0_1, state_0x84e0_1, state_0x84f0_1, state_0x8500_1, var_182);
        var_186 = helper_fucomi_ST0_FT0_wrapper((struct type_5 *)(0UL), cc_src_0, rdx_0, 25U, var_1, state_0x852a_1, state_0x8498_1, state_0x84a8_1, state_0x84b8_1, state_0x84c8_1, state_0x84d8_1, state_0x84e8_1, state_0x84f8_1, state_0x8508_1, state_0x8490_1, state_0x84a0_1, state_0x84b0_1, state_0x84c0_1, state_0x84d0_1, state_0x84e0_1, state_0x84f0_1, state_0x8500_1, var_182, var_185.field_0, var_185.field_1);
        var_187 = var_186.field_0;
        var_188 = var_186.field_1;
        var_189 = helper_fpop_wrapper((struct type_5 *)(0UL), var_182);
        var_190 = var_189.field_0;
        helper_fmov_STN_ST0_wrapper((struct type_5 *)(0UL), 0U, state_0x8498_1, state_0x84a8_1, state_0x84b8_1, state_0x84c8_1, state_0x84d8_1, state_0x84e8_1, state_0x84f8_1, state_0x8508_1, state_0x8490_1, state_0x84a0_1, state_0x84b0_1, state_0x84c0_1, state_0x84d0_1, state_0x84e0_1, state_0x84f0_1, state_0x8500_1, var_190);
        var_191 = helper_fpop_wrapper((struct type_5 *)(0UL), var_190);
        state_0x852a_2 = var_188;
        rax_0 = ((var_187 & 64UL) == 0UL) ? 1UL : ((var_187 >> 2UL) & 1UL);
        state_0x8480_1 = var_191.field_0;
    }
    var_192 = rax_0 + rdx_0;
    *_pre_phi365 = var_192;
    var_193 = helper_fildll_ST0_wrapper((struct type_5 *)(0UL), var_192, state_0x8480_1);
    if ((long)var_192 >= (long)0UL) {
        return;
    }
    var_194 = var_193.field_16;
    var_195 = var_193.field_15;
    var_196 = var_193.field_14;
    var_197 = var_193.field_13;
    var_198 = var_193.field_12;
    var_199 = var_193.field_11;
    var_200 = var_193.field_10;
    var_201 = var_193.field_9;
    var_202 = var_193.field_8;
    var_203 = var_193.field_7;
    var_204 = var_193.field_6;
    var_205 = var_193.field_5;
    var_206 = var_193.field_4;
    var_207 = var_193.field_3;
    var_208 = var_193.field_2;
    var_209 = var_193.field_1;
    var_210 = var_193.field_0;
    var_211 = helper_flds_FT0_wrapper((struct type_5 *)(0UL), *(uint32_t *)4279300UL, state_0x852a_2, var_3, var_7);
    helper_fadd_ST0_FT0_wrapper((struct type_5 *)(0UL), var_211.field_0, var_4, var_5, state_0x8529_0, var_7, state_0x852b_0, var_210, var_209, var_208, var_207, var_206, var_205, var_204, var_203, var_202, var_201, var_200, var_199, var_198, var_197, var_196, var_195, var_194, var_211.field_1, var_211.field_2);
    return;
}
