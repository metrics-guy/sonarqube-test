typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_68_ret_type;
struct indirect_placeholder_67_ret_type;
struct indirect_placeholder_68_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_67_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_21(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_4(uint64_t param_0);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern struct indirect_placeholder_68_ret_type indirect_placeholder_68(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_67_ret_type indirect_placeholder_67(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
uint64_t bb_pipe_lines(uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t rcx) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t r13_1_ph;
    uint64_t rdi2_1;
    uint64_t rbx_0;
    uint64_t local_sp_0;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t rbx_2_ph;
    uint64_t var_55;
    uint64_t r13_0;
    uint64_t rdi2_0;
    uint64_t local_sp_2;
    uint64_t var_56;
    uint64_t local_sp_5;
    uint64_t var_57;
    uint64_t var_35;
    uint64_t *var_36;
    uint64_t var_37;
    bool var_38;
    uint64_t local_sp_8_ph127;
    uint64_t rbx_1;
    uint64_t local_sp_3;
    uint64_t rbx_6_ph;
    uint64_t rbx_3;
    uint64_t local_sp_4_ph;
    uint64_t rdi2_2;
    uint64_t var_34;
    uint64_t local_sp_4;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_39;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_47;
    uint64_t rax_0;
    uint64_t rbx_4;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_48;
    uint64_t rbx_5;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t r14_0;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t local_sp_6;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_66;
    struct indirect_placeholder_68_ret_type var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t r14_1_ph126;
    uint64_t var_23;
    uint64_t *var_28;
    uint64_t var_29;
    uint64_t var_31;
    uint64_t r14_1_ph;
    uint64_t local_sp_8_ph;
    uint64_t *var_13;
    uint64_t rbx_6_ph125;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t *var_14;
    uint64_t *var_15;
    uint64_t *var_16;
    uint64_t *var_17;
    uint64_t *var_18;
    uint64_t local_sp_8;
    uint64_t var_30;
    uint64_t var_27;
    uint64_t local_sp_7;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t *var_21;
    uint64_t var_22;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    *(uint64_t *)(var_0 + (-64L)) = rdi;
    *(uint32_t *)(var_0 + (-68L)) = (uint32_t)rsi;
    *(uint64_t *)(var_0 + (-80L)) = rdx;
    var_8 = (uint64_t *)(var_0 + (-112L));
    *var_8 = 4211406UL;
    var_9 = indirect_placeholder_4(1048UL);
    *(uint64_t *)(var_9 + 1032UL) = 0UL;
    *(uint64_t *)(var_9 + 1024UL) = 0UL;
    *(uint64_t *)(var_9 + 1040UL) = 0UL;
    var_10 = var_0 + (-120L);
    *(uint64_t *)var_10 = 4211452UL;
    var_11 = indirect_placeholder_4(1048UL);
    *var_8 = 0UL;
    var_12 = (uint64_t *)rcx;
    r13_1_ph = var_9;
    rbx_2_ph = 1UL;
    rbx_1 = 0UL;
    rbx_6_ph = var_11;
    r14_1_ph = var_9;
    local_sp_8_ph = var_10;
    while (1U)
        {
            var_13 = (uint64_t *)(r13_1_ph + 1032UL);
            r13_0 = r13_1_ph;
            local_sp_8_ph127 = local_sp_8_ph;
            rbx_6_ph = r13_1_ph;
            rbx_4 = r13_1_ph;
            rbx_5 = r13_1_ph;
            r14_1_ph126 = r14_1_ph;
            rbx_6_ph125 = rbx_6_ph;
            while (1U)
                {
                    var_14 = (uint64_t *)(rbx_6_ph125 + 1024UL);
                    var_15 = (uint64_t *)(rbx_6_ph125 + 1032UL);
                    var_16 = (uint64_t *)(rbx_6_ph125 + 1040UL);
                    var_17 = (uint64_t *)(r14_1_ph126 + 1024UL);
                    var_18 = (uint64_t *)(r14_1_ph126 + 1032UL);
                    r14_1_ph = rbx_6_ph125;
                    r14_1_ph126 = rbx_6_ph125;
                    rdi2_2 = rbx_6_ph125;
                    local_sp_8 = local_sp_8_ph127;
                    while (1U)
                        {
                            *(uint64_t *)(local_sp_8 + 16UL) = rbx_6_ph125;
                            var_19 = (uint64_t)*(uint32_t *)(local_sp_8 + 36UL);
                            var_20 = local_sp_8 + (-8L);
                            var_21 = (uint64_t *)var_20;
                            *var_21 = 4211613UL;
                            var_22 = indirect_placeholder_21(1024UL, var_19, rbx_6_ph125);
                            local_sp_7 = var_20;
                            if ((var_22 + (-1L)) <= 18446744073709551613UL) {
                                var_34 = local_sp_8 + (-16L);
                                *(uint64_t *)var_34 = 4211752UL;
                                indirect_placeholder();
                                local_sp_3 = var_34;
                                local_sp_5 = var_34;
                                local_sp_6 = var_34;
                                if (var_22 != 18446744073709551615UL) {
                                    var_35 = *var_17;
                                    var_36 = (uint64_t *)(local_sp_8 + 8UL);
                                    var_37 = *var_36;
                                    var_38 = ((var_37 == 0UL) || (var_35 == 0UL));
                                    rbx_1 = var_38 | 1024UL;
                                    var_40 = var_37;
                                    if (!var_38) {
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    if ((uint64_t)(*(unsigned char *)((var_35 + r14_1_ph126) + (-1L)) - *(unsigned char *)4309976UL) == 0UL) {
                                        var_41 = *var_21;
                                        loop_state_var = 2U;
                                        break;
                                    }
                                    *var_18 = (*var_18 + 1UL);
                                    var_39 = *var_21 + 1UL;
                                    *var_21 = var_39;
                                    var_40 = *var_36;
                                    var_41 = var_39;
                                    loop_state_var = 2U;
                                    break;
                                }
                                var_66 = *(uint64_t *)(local_sp_8 + 24UL);
                                *(uint64_t *)(local_sp_8 + (-24L)) = 4211843UL;
                                var_67 = indirect_placeholder_68(4UL, var_66);
                                var_68 = var_67.field_0;
                                var_69 = var_67.field_1;
                                var_70 = var_67.field_2;
                                *(uint64_t *)(local_sp_8 + (-32L)) = 4211851UL;
                                indirect_placeholder();
                                var_71 = (uint64_t)*(uint32_t *)var_68;
                                var_72 = local_sp_8 + (-40L);
                                *(uint64_t *)var_72 = 4211876UL;
                                indirect_placeholder_67(0UL, 4276468UL, 0UL, var_71, var_68, var_69, var_70);
                                local_sp_3 = var_72;
                                loop_state_var = 1U;
                                break;
                            }
                            *var_14 = var_22;
                            *var_12 = (*var_12 + var_22);
                            *var_15 = 0UL;
                            *var_16 = 0UL;
                            var_23 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)4309976UL;
                            var_24 = (var_22 + rbx_6_ph125) - rdi2_2;
                            var_25 = local_sp_7 + (-8L);
                            *(uint64_t *)var_25 = 4211495UL;
                            var_26 = indirect_placeholder_21(var_24, rdi2_2, var_23);
                            local_sp_8_ph = var_25;
                            local_sp_7 = var_25;
                            while (var_26 != 0UL)
                                {
                                    var_27 = var_26 + 1UL;
                                    *var_15 = (*var_15 + 1UL);
                                    rdi2_2 = var_27;
                                    var_24 = (var_22 + rbx_6_ph125) - rdi2_2;
                                    var_25 = local_sp_7 + (-8L);
                                    *(uint64_t *)var_25 = 4211495UL;
                                    var_26 = indirect_placeholder_21(var_24, rdi2_2, var_23);
                                    local_sp_8_ph = var_25;
                                    local_sp_7 = var_25;
                                }
                            var_28 = (uint64_t *)local_sp_7;
                            var_29 = *var_28 + *var_15;
                            *var_28 = var_29;
                            if ((*var_17 + *var_14) <= 1023UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            var_30 = local_sp_7 + (-16L);
                            *(uint64_t *)var_30 = 4211690UL;
                            indirect_placeholder();
                            *var_17 = (*var_17 + *var_14);
                            *var_18 = (*var_18 + *var_15);
                            local_sp_8 = var_30;
                            continue;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 0U:
                        {
                            *(uint64_t *)(r14_1_ph126 + 1040UL) = rbx_6_ph125;
                            var_31 = var_29 - *var_13;
                            if (var_31 <= *(uint64_t *)(local_sp_7 + 16UL)) {
                                loop_state_var = 4U;
                                switch_state_var = 1;
                                break;
                            }
                            var_32 = local_sp_7 + (-16L);
                            *(uint64_t *)var_32 = 4211733UL;
                            var_33 = indirect_placeholder_4(1048UL);
                            rbx_6_ph125 = var_33;
                            local_sp_8_ph127 = var_32;
                            continue;
                        }
                        break;
                      case 1U:
                        {
                            rbx_2_ph = rbx_1;
                            local_sp_4_ph = local_sp_3;
                            rbx_3 = rbx_1;
                            if (r13_1_ph != 0UL) {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 2U:
                        {
                            var_42 = var_41 - *var_13;
                            var_43 = helper_cc_compute_c_wrapper(var_40 - var_42, var_42, var_7, 17U);
                            rax_0 = var_42;
                            if (var_43 != 0UL) {
                                loop_state_var = 3U;
                                switch_state_var = 1;
                                break;
                            }
                            var_47 = *var_21;
                            var_48 = *var_36;
                            loop_state_var = 2U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 4U:
                {
                    *var_28 = var_31;
                    r13_1_ph = *(uint64_t *)(r13_1_ph + 1040UL);
                    continue;
                }
                break;
              case 0U:
                {
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 1U:
                {
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 2U:
                {
                    loop_state_var = 2U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 3U:
                {
                    loop_state_var = 3U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            return (uint64_t)(uint32_t)rbx_3;
        }
        break;
      case 3U:
      case 2U:
      case 1U:
        {
            switch (loop_state_var) {
              case 1U:
                {
                    local_sp_4 = local_sp_4_ph;
                    rbx_3 = rbx_2_ph;
                    var_73 = *(uint64_t *)(r13_0 + 1040UL);
                    var_74 = local_sp_4 + (-8L);
                    *(uint64_t *)var_74 = 4211806UL;
                    indirect_placeholder();
                    r13_0 = var_73;
                    local_sp_4 = var_74;
                    do {
                        var_73 = *(uint64_t *)(r13_0 + 1040UL);
                        var_74 = local_sp_4 + (-8L);
                        *(uint64_t *)var_74 = 4211806UL;
                        indirect_placeholder();
                        r13_0 = var_73;
                        local_sp_4 = var_74;
                    } while (var_73 != 0UL);
                }
                break;
              case 3U:
              case 2U:
                {
                    switch (loop_state_var) {
                      case 3U:
                        {
                            var_44 = *(uint64_t *)(rbx_4 + 1040UL);
                            *var_21 = rax_0;
                            var_45 = rax_0 - *(uint64_t *)(var_44 + 1032UL);
                            var_46 = *var_36;
                            var_47 = rax_0;
                            rax_0 = var_45;
                            rbx_4 = var_44;
                            var_48 = var_46;
                            rbx_5 = var_44;
                            do {
                                var_44 = *(uint64_t *)(rbx_4 + 1040UL);
                                *var_21 = rax_0;
                                var_45 = rax_0 - *(uint64_t *)(var_44 + 1032UL);
                                var_46 = *var_36;
                                var_47 = rax_0;
                                rax_0 = var_45;
                                rbx_4 = var_44;
                                var_48 = var_46;
                                rbx_5 = var_44;
                            } while (var_45 <= var_46);
                        }
                        break;
                      case 2U:
                        {
                            var_49 = rbx_5 + *(uint64_t *)(rbx_5 + 1024UL);
                            var_50 = helper_cc_compute_c_wrapper(var_48 - var_47, var_47, var_7, 17U);
                            rdi2_0 = rbx_5;
                            rdi2_1 = rbx_5;
                            if (var_50 != 0UL) {
                                r14_0 = var_47 - var_48;
                                var_56 = var_54 + 1UL;
                                var_57 = r14_0 + (-1L);
                                rdi2_1 = var_56;
                                rdi2_0 = var_56;
                                local_sp_5 = local_sp_2;
                                r14_0 = var_57;
                                local_sp_6 = local_sp_2;
                                do {
                                    var_51 = var_49 - rdi2_0;
                                    var_52 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)4309976UL;
                                    var_53 = local_sp_5 + (-8L);
                                    *(uint64_t *)var_53 = 4212026UL;
                                    var_54 = indirect_placeholder_21(var_51, rdi2_0, var_52);
                                    local_sp_2 = var_53;
                                    if (var_54 == 0UL) {
                                        var_55 = local_sp_5 + (-16L);
                                        *(uint64_t *)var_55 = 4212059UL;
                                        indirect_placeholder();
                                        local_sp_2 = var_55;
                                    }
                                    var_56 = var_54 + 1UL;
                                    var_57 = r14_0 + (-1L);
                                    rdi2_1 = var_56;
                                    rdi2_0 = var_56;
                                    local_sp_5 = local_sp_2;
                                    r14_0 = var_57;
                                    local_sp_6 = local_sp_2;
                                } while (var_57 != 0UL);
                            }
                            var_58 = var_49 - rdi2_1;
                            var_59 = local_sp_6 + (-8L);
                            *(uint64_t *)var_59 = 4212072UL;
                            var_60 = indirect_placeholder_2(rbx_5, var_58);
                            var_61 = *(uint64_t *)(var_60 + 1040UL);
                            rbx_0 = var_61;
                            local_sp_0 = var_59;
                            local_sp_4_ph = var_59;
                            if (var_61 == 0UL) {
                                var_62 = *(uint64_t *)(rbx_0 + 1024UL);
                                var_63 = local_sp_0 + (-8L);
                                *(uint64_t *)var_63 = 4212099UL;
                                var_64 = indirect_placeholder_21(rbx_0, rbx_0, var_62);
                                var_65 = *(uint64_t *)(var_64 + 1040UL);
                                rbx_0 = var_65;
                                local_sp_0 = var_63;
                                local_sp_4_ph = var_63;
                                do {
                                    var_62 = *(uint64_t *)(rbx_0 + 1024UL);
                                    var_63 = local_sp_0 + (-8L);
                                    *(uint64_t *)var_63 = 4212099UL;
                                    var_64 = indirect_placeholder_21(rbx_0, rbx_0, var_62);
                                    var_65 = *(uint64_t *)(var_64 + 1040UL);
                                    rbx_0 = var_65;
                                    local_sp_0 = var_63;
                                    local_sp_4_ph = var_63;
                                } while (var_65 != 0UL);
                            }
                        }
                        break;
                    }
                }
                break;
            }
        }
        break;
    }
}
