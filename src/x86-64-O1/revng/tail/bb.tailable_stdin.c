typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_tailable_stdin_ret_type;
struct bb_tailable_stdin_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint32_t init_state_0x82fc(void);
extern uint64_t init_rdx(void);
struct bb_tailable_stdin_ret_type bb_tailable_stdin(uint64_t rdi, uint64_t rsi) {
    uint32_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t cc_src2_0;
    uint64_t rdx_0;
    uint64_t rax_0;
    uint64_t cc_src2_1;
    uint64_t var_8;
    uint64_t cc_src_0;
    uint64_t cc_dst_0;
    uint64_t rdi6_0;
    uint64_t rsi7_0;
    uint64_t rcx_0;
    uint64_t cc_src_1;
    uint64_t cc_dst_1;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    struct bb_tailable_stdin_ret_type mrv1 = {1UL, /*implicit*/(int)0};
    struct bb_tailable_stdin_ret_type mrv3 = {0UL, /*implicit*/(int)0};
    uint64_t var_9;
    struct bb_tailable_stdin_ret_type mrv5 = {0UL, /*implicit*/(int)0};
    revng_init_local_sp(0UL);
    rdx_0 = 0UL;
    rax_0 = rdi;
    cc_src_0 = 0UL;
    cc_dst_0 = 0UL;
    rdi6_0 = 4276409UL;
    rcx_0 = 2UL;
    if (rsi == 0UL) {
        var_9 = init_rdx();
        mrv5.field_1 = var_9;
        return mrv5;
    }
    var_0 = init_state_0x82fc();
    var_1 = init_cc_src2();
    var_2 = (uint64_t)var_0;
    cc_src2_0 = var_1;
    while (1U)
        {
            cc_src2_1 = cc_src2_0;
            if (*(unsigned char *)(rax_0 + 52UL) == '\x00') {
                var_8 = rdx_0 + 1UL;
                cc_src2_0 = cc_src2_1;
                rdx_0 = var_8;
                if (var_8 != rsi) {
                    rax_0 = rax_0 + 96UL;
                    continue;
                }
                mrv3.field_1 = rsi;
                return mrv3;
            }
            rsi7_0 = *(uint64_t *)rax_0;
            cc_src_1 = cc_src_0;
            cc_dst_1 = cc_dst_0;
            while (rcx_0 != 0UL)
                {
                    var_3 = (uint64_t)*(unsigned char *)rdi6_0;
                    var_4 = (uint64_t)*(unsigned char *)rsi7_0 - var_3;
                    cc_src_0 = var_3;
                    cc_dst_0 = var_4;
                    cc_src_1 = var_3;
                    cc_dst_1 = var_4;
                    if ((uint64_t)(unsigned char)var_4 == 0UL) {
                        break;
                    }
                    rdi6_0 = rdi6_0 + var_2;
                    rsi7_0 = rsi7_0 + var_2;
                    rcx_0 = rcx_0 + (-1L);
                    cc_src_1 = cc_src_0;
                    cc_dst_1 = cc_dst_0;
                }
            var_5 = helper_cc_compute_all_wrapper(cc_dst_1, cc_src_1, cc_src2_0, 14U);
            var_6 = ((var_5 & 65UL) == 0UL);
            var_7 = helper_cc_compute_c_wrapper(cc_dst_1, var_5, cc_src2_0, 1U);
            cc_src2_1 = var_7;
            if ((uint64_t)((unsigned char)var_6 - (unsigned char)var_7) == 0UL) {
                mrv1.field_1 = rdx_0;
                return mrv1;
            }
        }
}
