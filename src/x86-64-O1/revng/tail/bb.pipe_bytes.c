typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_143_ret_type;
struct indirect_placeholder_142_ret_type;
struct indirect_placeholder_143_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_142_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_21(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_4(uint64_t param_0);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern struct indirect_placeholder_143_ret_type indirect_placeholder_143(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_142_ret_type indirect_placeholder_142(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
typedef _Bool bool;
uint64_t bb_pipe_bytes(uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t rcx) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t r13_1_ph;
    uint64_t var_35;
    uint64_t rbx_0;
    uint64_t local_sp_0;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t rbx_1;
    uint64_t local_sp_1;
    uint64_t r12_0_ph;
    uint64_t rbp_0;
    uint64_t local_sp_2;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t local_sp_3;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t rbx_2;
    uint64_t rax_0;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t r13_0;
    uint64_t rbx_3;
    uint64_t rsi3_0;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_48;
    struct indirect_placeholder_143_ret_type var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_22;
    uint64_t local_sp_3_ph87;
    uint64_t r14_0_ph86;
    uint64_t var_24;
    uint64_t rbp_1_ph;
    uint64_t r14_0_ph;
    uint64_t local_sp_3_ph;
    uint64_t *var_13;
    uint64_t r13_1_ph84;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t r12_0_ph85;
    uint64_t *var_14;
    uint64_t *var_15;
    uint64_t *var_16;
    uint64_t r13_1;
    uint64_t var_23;
    uint64_t *var_27;
    uint64_t *var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t *var_20;
    uint64_t var_21;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_8 = var_0 + (-88L);
    *(uint64_t *)(var_0 + (-64L)) = rdi;
    *(uint32_t *)(var_0 + (-68L)) = (uint32_t)rsi;
    *(uint64_t *)var_8 = rdx;
    *(uint64_t *)(var_0 + (-96L)) = 4210338UL;
    var_9 = indirect_placeholder_4(1040UL);
    *(uint64_t *)(var_9 + 1024UL) = 0UL;
    *(uint64_t *)(var_9 + 1032UL) = 0UL;
    var_10 = var_0 + (-104L);
    *(uint64_t *)var_10 = 4210373UL;
    var_11 = indirect_placeholder_4(1040UL);
    var_12 = (uint64_t *)rcx;
    r13_1_ph = 0UL;
    rbx_1 = 1UL;
    r12_0_ph = var_11;
    rbp_1_ph = var_9;
    r14_0_ph = var_9;
    local_sp_3_ph = var_10;
    while (1U)
        {
            var_13 = (uint64_t *)(rbp_1_ph + 1024UL);
            r12_0_ph = rbp_1_ph;
            rbp_0 = rbp_1_ph;
            rbx_2 = rbp_1_ph;
            rbx_3 = rbp_1_ph;
            local_sp_3_ph87 = local_sp_3_ph;
            r14_0_ph86 = r14_0_ph;
            r13_1_ph84 = r13_1_ph;
            r12_0_ph85 = r12_0_ph;
            while (1U)
                {
                    var_14 = (uint64_t *)(r12_0_ph85 + 1024UL);
                    var_15 = (uint64_t *)(r12_0_ph85 + 1032UL);
                    var_16 = (uint64_t *)(r14_0_ph86 + 1024UL);
                    r14_0_ph = r12_0_ph85;
                    r14_0_ph86 = r12_0_ph85;
                    r13_1 = r13_1_ph84;
                    local_sp_3 = local_sp_3_ph87;
                    while (1U)
                        {
                            var_17 = (uint64_t *)(local_sp_3 + 8UL);
                            *var_17 = r12_0_ph85;
                            var_18 = (uint64_t)*(uint32_t *)(local_sp_3 + 20UL);
                            var_19 = local_sp_3 + (-8L);
                            var_20 = (uint64_t *)var_19;
                            *var_20 = 4210440UL;
                            var_21 = indirect_placeholder_21(1024UL, var_18, r12_0_ph85);
                            r13_0 = r13_1;
                            local_sp_3_ph = var_19;
                            if ((var_21 + (-1L)) > 18446744073709551613UL) {
                                *var_12 = (*var_12 + var_21);
                                *var_14 = var_21;
                                *var_15 = 0UL;
                                var_22 = r13_1 + var_21;
                                r13_1_ph84 = var_22;
                                r13_1 = var_22;
                                if ((var_21 + *var_16) <= 1023UL) {
                                    loop_state_var = 3U;
                                    break;
                                }
                                var_23 = local_sp_3 + (-16L);
                                *(uint64_t *)var_23 = 4210403UL;
                                indirect_placeholder();
                                *var_16 = (*var_16 + *var_14);
                                local_sp_3 = var_23;
                                continue;
                            }
                            var_27 = (uint64_t *)(local_sp_3 + (-16L));
                            *var_27 = 4210568UL;
                            indirect_placeholder();
                            if (var_21 == 18446744073709551615UL) {
                                var_28 = *var_13;
                                var_29 = r13_1 - var_28;
                                var_30 = helper_cc_compute_c_wrapper(*var_27 - var_29, var_29, var_7, 17U);
                                rax_0 = var_29;
                                rsi3_0 = var_28;
                                if (var_30 != 0UL) {
                                    var_31 = *var_27;
                                    var_35 = var_31;
                                    loop_state_var = 0U;
                                    break;
                                }
                                var_35 = *var_27;
                                loop_state_var = 1U;
                                break;
                            }
                            var_48 = *var_17;
                            *(uint64_t *)(local_sp_3 + (-24L)) = 4210764UL;
                            var_49 = indirect_placeholder_143(4UL, var_48);
                            var_50 = var_49.field_0;
                            var_51 = var_49.field_1;
                            var_52 = var_49.field_2;
                            *(uint64_t *)(local_sp_3 + (-32L)) = 4210772UL;
                            indirect_placeholder();
                            var_53 = (uint64_t)*(uint32_t *)var_50;
                            var_54 = local_sp_3 + (-40L);
                            *(uint64_t *)var_54 = 4210797UL;
                            indirect_placeholder_142(0UL, 4276468UL, 0UL, var_53, var_50, var_51, var_52);
                            rbx_1 = 0UL;
                            local_sp_1 = var_54;
                            loop_state_var = 2U;
                            break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 3U:
                        {
                            *(uint64_t *)(r14_0_ph86 + 1032UL) = r12_0_ph85;
                            var_24 = var_22 - *var_13;
                            r13_1_ph = var_24;
                            if (var_24 <= *var_20) {
                                loop_state_var = 3U;
                                switch_state_var = 1;
                                break;
                            }
                            var_25 = local_sp_3 + (-16L);
                            *(uint64_t *)var_25 = 4210549UL;
                            var_26 = indirect_placeholder_4(1040UL);
                            r12_0_ph85 = var_26;
                            local_sp_3_ph87 = var_25;
                            continue;
                        }
                        break;
                      case 0U:
                        {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 1U:
                        {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 2U:
                        {
                            loop_state_var = 2U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 3U:
                {
                    rbp_1_ph = *(uint64_t *)(rbp_1_ph + 1032UL);
                    continue;
                }
                break;
              case 0U:
                {
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 1U:
                {
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 2U:
                {
                    loop_state_var = 2U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 2U:
        {
            local_sp_2 = local_sp_1;
            if (rbp_1_ph != 0UL) {
                var_55 = *(uint64_t *)(rbp_0 + 1032UL);
                var_56 = local_sp_2 + (-8L);
                *(uint64_t *)var_56 = 4210727UL;
                indirect_placeholder();
                rbp_0 = var_55;
                local_sp_2 = var_56;
                do {
                    var_55 = *(uint64_t *)(rbp_0 + 1032UL);
                    var_56 = local_sp_2 + (-8L);
                    *(uint64_t *)var_56 = 4210727UL;
                    indirect_placeholder();
                    rbp_0 = var_55;
                    local_sp_2 = var_56;
                } while (var_55 != 0UL);
            }
            return rbx_1;
        }
        break;
      case 1U:
      case 0U:
        {
            switch (loop_state_var) {
              case 0U:
                {
                    var_32 = *(uint64_t *)(rbx_2 + 1032UL);
                    var_33 = *(uint64_t *)(var_32 + 1024UL);
                    var_34 = rax_0 - var_33;
                    rbx_2 = var_32;
                    rax_0 = var_34;
                    r13_0 = rax_0;
                    rbx_3 = var_32;
                    rsi3_0 = var_33;
                    do {
                        var_32 = *(uint64_t *)(rbx_2 + 1032UL);
                        var_33 = *(uint64_t *)(var_32 + 1024UL);
                        var_34 = rax_0 - var_33;
                        rbx_2 = var_32;
                        rax_0 = var_34;
                        r13_0 = rax_0;
                        rbx_3 = var_32;
                        rsi3_0 = var_33;
                    } while (var_34 <= var_31);
                }
                break;
              case 1U:
                {
                    var_36 = r13_0 - var_35;
                    var_37 = helper_cc_compute_c_wrapper(var_35 - r13_0, r13_0, var_7, 17U);
                    var_38 = (var_37 == 0UL) ? 0UL : var_36;
                    var_39 = rsi3_0 - var_38;
                    var_40 = var_38 + rbx_3;
                    var_41 = local_sp_3 + (-24L);
                    *(uint64_t *)var_41 = 4210659UL;
                    var_42 = indirect_placeholder_21(rbx_3, var_40, var_39);
                    var_43 = *(uint64_t *)(var_42 + 1032UL);
                    rbx_0 = var_43;
                    local_sp_0 = var_41;
                    local_sp_1 = var_41;
                    if (var_43 == 0UL) {
                        var_44 = *(uint64_t *)(rbx_0 + 1024UL);
                        var_45 = local_sp_0 + (-8L);
                        *(uint64_t *)var_45 = 4210690UL;
                        var_46 = indirect_placeholder_21(rbx_0, rbx_0, var_44);
                        var_47 = *(uint64_t *)(var_46 + 1032UL);
                        rbx_0 = var_47;
                        local_sp_0 = var_45;
                        local_sp_1 = var_45;
                        do {
                            var_44 = *(uint64_t *)(rbx_0 + 1024UL);
                            var_45 = local_sp_0 + (-8L);
                            *(uint64_t *)var_45 = 4210690UL;
                            var_46 = indirect_placeholder_21(rbx_0, rbx_0, var_44);
                            var_47 = *(uint64_t *)(var_46 + 1032UL);
                            rbx_0 = var_47;
                            local_sp_0 = var_45;
                            local_sp_1 = var_45;
                        } while (var_47 != 0UL);
                    }
                }
                break;
            }
        }
        break;
    }
}
