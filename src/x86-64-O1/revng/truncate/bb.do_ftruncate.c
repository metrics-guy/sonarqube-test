typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct helper_divq_EAX_wrapper_ret_type;
struct type_5;
struct helper_idivq_EAX_wrapper_ret_type;
struct indirect_placeholder_28_ret_type;
struct indirect_placeholder_29_ret_type;
struct indirect_placeholder_30_ret_type;
struct indirect_placeholder_31_ret_type;
struct indirect_placeholder_32_ret_type;
struct indirect_placeholder_33_ret_type;
struct indirect_placeholder_34_ret_type;
struct indirect_placeholder_35_ret_type;
struct helper_divq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint32_t field_3;
    uint64_t field_4;
    uint32_t field_5;
    uint64_t field_6;
    uint32_t field_7;
    uint64_t field_8;
    uint32_t field_9;
    uint32_t field_10;
    unsigned char field_11;
    uint32_t field_12;
};
struct type_5 {
};
struct helper_idivq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint32_t field_3;
    uint64_t field_4;
    uint32_t field_5;
    uint64_t field_6;
    uint32_t field_7;
    uint64_t field_8;
    uint32_t field_9;
    uint32_t field_10;
    unsigned char field_11;
    uint32_t field_12;
};
struct indirect_placeholder_28_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_29_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_30_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_31_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_32_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_33_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_34_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_35_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern uint32_t init_state_0x8248(void);
extern struct helper_divq_EAX_wrapper_ret_type helper_divq_EAX_wrapper(struct type_5 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint64_t param_13, uint32_t param_14, uint64_t param_15, uint64_t param_16, uint64_t param_17, uint32_t param_18, uint32_t param_19);
extern uint64_t init_rax(void);
extern struct helper_idivq_EAX_wrapper_ret_type helper_idivq_EAX_wrapper(struct type_5 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint64_t param_13, uint32_t param_14, uint64_t param_15, uint64_t param_16, uint64_t param_17, uint32_t param_18, uint32_t param_19);
extern void indirect_placeholder_11(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_28_ret_type indirect_placeholder_28(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_29_ret_type indirect_placeholder_29(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_30_ret_type indirect_placeholder_30(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_31_ret_type indirect_placeholder_31(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_32_ret_type indirect_placeholder_32(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_33_ret_type indirect_placeholder_33(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_34_ret_type indirect_placeholder_34(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_35_ret_type indirect_placeholder_35(uint64_t param_0);
uint64_t bb_do_ftruncate(uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t rcx, uint64_t r10, uint64_t r9, uint64_t r8) {
    uint64_t state_0x82d8_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint32_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint32_t var_14;
    uint32_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint32_t var_18;
    uint64_t var_19;
    uint64_t rdi2_0;
    uint64_t rax_2;
    uint64_t var_65;
    struct indirect_placeholder_28_ret_type var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t rbx_0;
    struct indirect_placeholder_29_ret_type var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t rdi2_1;
    struct indirect_placeholder_30_ret_type var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t rbx_1;
    uint64_t r13_0;
    uint64_t var_26;
    uint64_t var_27;
    struct helper_idivq_EAX_wrapper_ret_type var_28;
    struct helper_idivq_EAX_wrapper_ret_type var_29;
    uint64_t var_30;
    struct indirect_placeholder_31_ret_type var_31;
    uint64_t var_32;
    struct indirect_placeholder_32_ret_type var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t rsi3_0;
    uint64_t state_0x9018_0;
    uint32_t state_0x9010_0;
    uint64_t rax_0;
    uint64_t local_sp_0;
    uint32_t state_0x9080_0;
    uint32_t state_0x8248_0;
    uint64_t rbp_0;
    uint64_t rsi3_1;
    uint64_t rax_1;
    uint64_t local_sp_1;
    uint64_t var_49;
    struct indirect_placeholder_33_ret_type var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_55;
    uint64_t var_56;
    struct helper_divq_EAX_wrapper_ret_type var_54;
    uint64_t var_57;
    struct indirect_placeholder_34_ret_type var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_63;
    struct helper_divq_EAX_wrapper_ret_type var_62;
    uint64_t var_64;
    uint64_t var_33;
    struct indirect_placeholder_35_ret_type var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t local_sp_2;
    uint64_t var_20;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r15();
    var_5 = init_rbp();
    var_6 = init_r12();
    var_7 = init_r14();
    var_8 = init_cc_src2();
    var_9 = init_state_0x9018();
    var_10 = init_state_0x9010();
    var_11 = init_state_0x8408();
    var_12 = init_state_0x8328();
    var_13 = init_state_0x82d8();
    var_14 = init_state_0x9080();
    var_15 = init_state_0x8248();
    *(uint64_t *)(var_0 + (-8L)) = var_4;
    *(uint64_t *)(var_0 + (-16L)) = var_7;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_6;
    *(uint64_t *)(var_0 + (-40L)) = var_5;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_16 = var_0 + (-200L);
    var_17 = (uint64_t)(uint32_t)rdi;
    var_18 = (uint32_t)r8;
    var_19 = (uint64_t)var_18;
    state_0x82d8_0 = var_13;
    rdi2_0 = var_17;
    rbx_0 = rdx;
    r13_0 = 0UL;
    rsi3_0 = var_16;
    state_0x9018_0 = var_9;
    state_0x9010_0 = var_10;
    rax_0 = var_1;
    local_sp_0 = var_16;
    state_0x9080_0 = var_14;
    state_0x8248_0 = var_15;
    rbp_0 = rcx;
    if (*(unsigned char *)4281144UL == '\x00') {
        rdi2_0 = rdi;
        rsi3_0 = rsi;
        if (!((var_19 == 0UL) || ((long)rcx > (long)18446744073709551615UL))) {
            var_20 = var_0 + (-208L);
            *(uint64_t *)var_20 = 4204634UL;
            indirect_placeholder_1();
            local_sp_0 = var_20;
            if ((uint64_t)(uint32_t)var_1 != 0UL) {
                *(uint64_t *)(var_0 + (-216L)) = 4204838UL;
                var_21 = indirect_placeholder_32(4UL, rsi);
                var_22 = var_21.field_0;
                var_23 = var_21.field_1;
                var_24 = var_21.field_2;
                *(uint64_t *)(var_0 + (-224L)) = 4204846UL;
                indirect_placeholder_1();
                var_25 = (uint64_t)*(uint32_t *)var_22;
                *(uint64_t *)(var_0 + (-232L)) = 4204871UL;
                indirect_placeholder_11(0UL, 4257544UL, 0UL, var_25, var_22, var_23, var_24);
                return r13_0;
            }
            if (*(unsigned char *)4281144UL != '\x00') {
                var_26 = *(uint64_t *)(var_0 + (-152L));
                var_27 = ((var_26 + (-1L)) > 2305843009213693951UL) ? 512UL : var_26;
                var_28 = helper_idivq_EAX_wrapper((struct type_5 *)(0UL), var_27, 9223372036854775808UL, 18446744073709551615UL, 4204697UL, rdx, var_17, rcx, var_16, rcx, r10, r9, r8, var_9, var_10, var_11, var_12, var_13, var_14, var_15);
                if ((long)var_28.field_0 <= (long)rdx) {
                    *(uint64_t *)(var_0 + (-216L)) = 4204895UL;
                    var_31 = indirect_placeholder_31(4UL, rsi);
                    var_32 = var_31.field_0;
                    *(uint64_t *)(var_0 + (-224L)) = 4204929UL;
                    indirect_placeholder_11(0UL, 4256184UL, 0UL, 0UL, rdx, var_32, var_27);
                    return r13_0;
                }
                var_29 = helper_idivq_EAX_wrapper((struct type_5 *)(0UL), var_27, 9223372036854775807UL, 0UL, 4204721UL, rdx, var_17, rcx, var_16, rcx, r10, r9, r8, var_28.field_6, var_28.field_7, var_11, var_12, var_28.field_8, var_28.field_9, var_28.field_10);
                var_30 = var_29.field_0;
                rax_0 = var_30;
                if ((long)var_30 >= (long)rdx) {
                    *(uint64_t *)(var_0 + (-216L)) = 4204895UL;
                    var_31 = indirect_placeholder_31(4UL, rsi);
                    var_32 = var_31.field_0;
                    *(uint64_t *)(var_0 + (-224L)) = 4204929UL;
                    indirect_placeholder_11(0UL, 4256184UL, 0UL, 0UL, rdx, var_32, var_27);
                    return r13_0;
                }
                state_0x82d8_0 = var_29.field_8;
                rbx_0 = var_27 * rdx;
                state_0x9018_0 = var_29.field_6;
                state_0x9010_0 = var_29.field_7;
                state_0x9080_0 = var_29.field_9;
                state_0x8248_0 = var_29.field_10;
            }
        }
    } else {
        var_20 = var_0 + (-208L);
        *(uint64_t *)var_20 = 4204634UL;
        indirect_placeholder_1();
        local_sp_0 = var_20;
        if ((uint64_t)(uint32_t)var_1 == 0UL) {
            *(uint64_t *)(var_0 + (-216L)) = 4204838UL;
            var_21 = indirect_placeholder_32(4UL, rsi);
            var_22 = var_21.field_0;
            var_23 = var_21.field_1;
            var_24 = var_21.field_2;
            *(uint64_t *)(var_0 + (-224L)) = 4204846UL;
            indirect_placeholder_1();
            var_25 = (uint64_t)*(uint32_t *)var_22;
            *(uint64_t *)(var_0 + (-232L)) = 4204871UL;
            indirect_placeholder_11(0UL, 4257544UL, 0UL, var_25, var_22, var_23, var_24);
            return r13_0;
        }
        if (*(unsigned char *)4281144UL != '\x00') {
            var_26 = *(uint64_t *)(var_0 + (-152L));
            var_27 = ((var_26 + (-1L)) > 2305843009213693951UL) ? 512UL : var_26;
            var_28 = helper_idivq_EAX_wrapper((struct type_5 *)(0UL), var_27, 9223372036854775808UL, 18446744073709551615UL, 4204697UL, rdx, var_17, rcx, var_16, rcx, r10, r9, r8, var_9, var_10, var_11, var_12, var_13, var_14, var_15);
            if ((long)var_28.field_0 > (long)rdx) {
                *(uint64_t *)(var_0 + (-216L)) = 4204895UL;
                var_31 = indirect_placeholder_31(4UL, rsi);
                var_32 = var_31.field_0;
                *(uint64_t *)(var_0 + (-224L)) = 4204929UL;
                indirect_placeholder_11(0UL, 4256184UL, 0UL, 0UL, rdx, var_32, var_27);
                return r13_0;
            }
            var_29 = helper_idivq_EAX_wrapper((struct type_5 *)(0UL), var_27, 9223372036854775807UL, 0UL, 4204721UL, rdx, var_17, rcx, var_16, rcx, r10, r9, r8, var_28.field_6, var_28.field_7, var_11, var_12, var_28.field_8, var_28.field_9, var_28.field_10);
            var_30 = var_29.field_0;
            rax_0 = var_30;
            if ((long)var_30 >= (long)rdx) {
                *(uint64_t *)(var_0 + (-216L)) = 4204895UL;
                var_31 = indirect_placeholder_31(4UL, rsi);
                var_32 = var_31.field_0;
                *(uint64_t *)(var_0 + (-224L)) = 4204929UL;
                indirect_placeholder_11(0UL, 4256184UL, 0UL, 0UL, rdx, var_32, var_27);
                return r13_0;
            }
            state_0x82d8_0 = var_29.field_8;
            rbx_0 = var_27 * rdx;
            state_0x9018_0 = var_29.field_6;
            state_0x9010_0 = var_29.field_7;
            state_0x9080_0 = var_29.field_9;
            state_0x8248_0 = var_29.field_10;
        }
    }
    rax_2 = rax_0;
    rdi2_1 = rdi2_0;
    rbx_1 = rbx_0;
    r13_0 = 1UL;
    rsi3_1 = rsi3_0;
    rax_1 = rax_0;
    local_sp_1 = local_sp_0;
    local_sp_2 = local_sp_0;
    if (var_19 == 0UL) {
        *(uint64_t *)(local_sp_2 + (-8L)) = 4205126UL;
        indirect_placeholder_1();
        if ((uint64_t)((uint32_t)rax_2 + 1U) == 0UL) {
            var_65 = ((long)rbx_1 > (long)0UL) ? rbx_1 : 0UL;
            *(uint64_t *)(local_sp_2 + (-16L)) = 4205324UL;
            var_66 = indirect_placeholder_28(4UL, rsi);
            var_67 = var_66.field_0;
            var_68 = var_66.field_1;
            *(uint64_t *)(local_sp_2 + (-24L)) = 4205332UL;
            indirect_placeholder_1();
            var_69 = (uint64_t)*(uint32_t *)var_67;
            *(uint64_t *)(local_sp_2 + (-32L)) = 4205360UL;
            indirect_placeholder_11(0UL, 4256360UL, 0UL, var_69, var_67, var_68, var_65);
            r13_0 = 0UL;
        }
        return r13_0;
    }
    if ((long)rcx >= (long)0UL) {
        var_33 = local_sp_0 + (-8L);
        *(uint64_t *)var_33 = 4204948UL;
        var_34 = indirect_placeholder_35(local_sp_0);
        var_35 = var_34.field_0;
        var_36 = (uint64_t)(uint32_t)var_35;
        rdi2_1 = var_17;
        r13_0 = var_36;
        rbp_0 = var_35;
        rsi3_1 = 0UL;
        rax_1 = var_35;
        local_sp_1 = var_33;
        if ((uint64_t)(unsigned char)var_35 != 0UL) {
            var_37 = var_34.field_1;
            var_38 = *(uint64_t *)(local_sp_0 + 40UL);
            r13_0 = 0UL;
            rdi2_1 = var_37;
            rbp_0 = var_38;
            rsi3_1 = rsi3_0;
            if ((long)var_38 <= (long)18446744073709551615UL) {
                *(uint64_t *)(local_sp_0 + (-16L)) = 4204982UL;
                var_39 = indirect_placeholder_30(4UL, rsi);
                var_40 = var_39.field_0;
                var_41 = var_39.field_1;
                var_42 = var_39.field_2;
                *(uint64_t *)(local_sp_0 + (-24L)) = 4205010UL;
                indirect_placeholder_11(0UL, 4256232UL, 0UL, 0UL, var_40, var_41, var_42);
                return r13_0;
            }
        }
        var_43 = local_sp_0 + (-16L);
        *(uint64_t *)var_43 = 4205036UL;
        indirect_placeholder_1();
        local_sp_1 = var_43;
        if ((long)var_35 <= (long)18446744073709551615UL) {
            *(uint64_t *)(local_sp_0 + (-24L)) = 4205061UL;
            var_44 = indirect_placeholder_29(4UL, rsi);
            var_45 = var_44.field_0;
            var_46 = var_44.field_1;
            var_47 = var_44.field_2;
            *(uint64_t *)(local_sp_0 + (-32L)) = 4205069UL;
            indirect_placeholder_1();
            var_48 = (uint64_t)*(uint32_t *)var_45;
            *(uint64_t *)(local_sp_0 + (-40L)) = 4205094UL;
            indirect_placeholder_11(0UL, 4257560UL, 0UL, var_48, var_45, var_46, var_47);
            return r13_0;
        }
    }
    rax_2 = rax_1;
    local_sp_2 = local_sp_1;
    if ((uint64_t)(var_18 + (-2)) == 0UL) {
        var_64 = helper_cc_compute_c_wrapper(rbx_0 - rbp_0, rbp_0, var_8, 17U);
        rbx_1 = (var_64 == 0UL) ? rbx_0 : rbp_0;
    } else {
        if ((uint64_t)(var_18 + (-3)) == 0UL) {
            rbx_1 = (rbx_0 > rbp_0) ? rbp_0 : rbx_0;
        } else {
            if ((uint64_t)(var_18 + (-4)) == 0UL) {
                var_62 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), rbx_0, rbp_0, 0UL, 4205179UL, rbx_0, rdi2_1, rbp_0, rsi3_1, rcx, r10, r9, r8, state_0x9018_0, state_0x9010_0, var_11, var_12, state_0x82d8_0, state_0x9080_0, state_0x8248_0);
                var_63 = var_62.field_0;
                rbx_1 = var_63 * rbx_0;
                rax_2 = var_63;
            } else {
                if ((uint64_t)(var_18 + (-5)) == 0UL) {
                    var_54 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), rbx_0, (rbx_0 + rbp_0) + (-1L), 0UL, 4205198UL, rbx_0, rdi2_1, rbp_0, rsi3_1, rcx, r10, r9, r8, state_0x9018_0, state_0x9010_0, var_11, var_12, state_0x82d8_0, state_0x9080_0, state_0x8248_0);
                    var_55 = var_54.field_0;
                    var_56 = var_55 * rbx_0;
                    rbx_1 = var_56;
                    rax_2 = var_55;
                    if ((long)var_56 <= (long)18446744073709551615UL) {
                        var_57 = var_54.field_1;
                        *(uint64_t *)(local_sp_1 + (-8L)) = 4205223UL;
                        var_58 = indirect_placeholder_34(var_57, 4UL, rsi);
                        var_59 = var_58.field_0;
                        var_60 = var_58.field_1;
                        var_61 = var_58.field_2;
                        *(uint64_t *)(local_sp_1 + (-16L)) = 4205251UL;
                        indirect_placeholder_11(0UL, 4256280UL, 0UL, 0UL, var_59, var_60, var_61);
                        return r13_0;
                    }
                }
                var_49 = 9223372036854775807UL - rbp_0;
                rax_2 = var_49;
                if ((long)var_49 >= (long)rbx_0) {
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4205272UL;
                    var_50 = indirect_placeholder_33(4UL, rsi);
                    var_51 = var_50.field_0;
                    var_52 = var_50.field_1;
                    var_53 = var_50.field_2;
                    *(uint64_t *)(local_sp_1 + (-16L)) = 4205300UL;
                    indirect_placeholder_11(0UL, 4256320UL, 0UL, 0UL, var_51, var_52, var_53);
                    return r13_0;
                }
                rbx_1 = rbx_0 + rbp_0;
            }
        }
    }
}
