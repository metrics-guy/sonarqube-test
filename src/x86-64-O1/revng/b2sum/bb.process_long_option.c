typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint32_t init_state_0x82fc(void);
uint64_t bb_process_long_option(uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t rcx, uint64_t r9, uint64_t r8) {
    uint64_t rbp_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint32_t var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t var_10;
    unsigned char var_11;
    uint64_t var_12;
    uint64_t r12_3;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_68;
    uint64_t local_sp_0;
    uint64_t local_sp_1;
    uint64_t var_50;
    uint64_t local_sp_2;
    uint64_t var_51;
    uint64_t local_sp_5;
    uint64_t rax_3;
    uint64_t rbp_4;
    uint64_t rbx_1;
    uint64_t local_sp_13;
    uint32_t var_29;
    uint64_t var_30;
    uint64_t rax_0;
    uint64_t var_37;
    uint64_t local_sp_3;
    uint32_t var_31;
    uint64_t var_32;
    uint32_t *var_33;
    uint64_t r13_0;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_70;
    uint64_t var_38;
    uint64_t rbp_1;
    uint64_t rax_1;
    uint64_t local_sp_4;
    uint64_t var_39;
    uint32_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint32_t var_73;
    uint64_t var_74;
    uint64_t local_sp_14;
    uint64_t var_60;
    uint64_t rbp_3;
    bool var_43;
    uint64_t var_44;
    uint64_t *var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t local_sp_6;
    uint64_t var_53;
    uint64_t *var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t rdi2_0;
    uint64_t rcx4_0;
    uint64_t rcx4_1;
    unsigned char var_57;
    uint64_t var_58;
    uint32_t *var_59;
    uint64_t rax_4;
    uint64_t var_52;
    uint64_t r15_1;
    uint64_t rbx_0;
    uint64_t rdi2_1;
    uint64_t rcx4_2;
    uint64_t rcx4_3;
    uint64_t rax_2;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t local_sp_10;
    uint64_t _pre204;
    uint64_t _pre_phi205;
    uint64_t r15_0;
    uint64_t rbp_2;
    uint64_t r12_1;
    uint64_t local_sp_7;
    uint64_t var_61;
    uint32_t var_62;
    uint64_t var_63;
    uint64_t *var_64;
    uint64_t var_65;
    bool var_66;
    uint32_t var_67;
    uint64_t *_pre_phi201;
    uint64_t var_69;
    uint64_t *_pre_phi197;
    uint64_t local_sp_9;
    uint64_t var_72;
    uint64_t var_71;
    uint64_t var_75;
    uint64_t var_76;
    bool var_77;
    uint32_t var_78;
    uint64_t r15_2;
    uint64_t var_79;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t r12_2;
    uint64_t var_13;
    unsigned char var_14;
    uint64_t var_15;
    uint64_t var_16;
    bool var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t *var_20;
    uint64_t local_sp_11;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t *_pre202;
    uint64_t *_pre_phi203;
    uint64_t r15_3;
    uint64_t local_sp_12;
    uint64_t var_28;
    uint64_t local_sp_15;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t local_sp_16;
    uint64_t var_84;
    uint32_t *var_85;
    uint64_t var_83;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_state_0x82fc();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_8 = var_0 + (-136L);
    *(uint32_t *)(var_0 + (-76L)) = (uint32_t)rdi;
    *(uint64_t *)(var_0 + (-104L)) = rsi;
    *(uint64_t *)(var_0 + (-88L)) = rdx;
    var_9 = (uint64_t *)var_8;
    *var_9 = rcx;
    *(uint64_t *)(var_0 + (-112L)) = r8;
    *(uint32_t *)(var_0 + (-124L)) = (uint32_t)r9;
    var_10 = *(uint64_t *)(*(uint64_t *)(var_0 | 8UL) + 32UL);
    var_11 = *(unsigned char *)var_10;
    var_12 = (uint64_t)var_11;
    r12_3 = var_10;
    rbp_4 = 0UL;
    rbx_1 = 0UL;
    rcx4_0 = 18446744073709551615UL;
    rcx4_1 = 0UL;
    rax_4 = 63UL;
    r15_1 = 0UL;
    rcx4_2 = 18446744073709551615UL;
    rcx4_3 = 0UL;
    rax_2 = var_12;
    local_sp_10 = var_8;
    r15_2 = 0UL;
    r12_2 = var_10;
    local_sp_11 = var_8;
    if (var_11 != '\x00' & (uint64_t)(var_11 + '\xc3') != 0UL) {
        while (1U)
            {
                switch_state_var = 1;
                break;
            }
        rax_2 = (uint64_t)var_14;
    }
    var_15 = *var_9;
    var_16 = *(uint64_t *)var_15;
    *(uint64_t *)(var_0 + (-120L)) = var_16;
    r12_1 = r12_3;
    rbx_0 = var_16;
    rbp_3 = var_15;
    rax_3 = rax_2;
    if (var_16 != 0UL) {
        var_26 = local_sp_11 + 16UL;
        _pre_phi205 = var_26;
        r15_3 = r15_2;
        local_sp_12 = local_sp_11;
        local_sp_14 = local_sp_11;
        if (*(uint64_t *)var_26 != 0UL) {
            local_sp_15 = local_sp_14;
            rax_4 = 4294967295UL;
            if (*(uint32_t *)(local_sp_14 + 12UL) != 0U) {
                var_80 = local_sp_14 + 144UL;
                var_81 = *(uint64_t *)(*(uint64_t *)var_80 + 32UL);
                var_82 = local_sp_14 + (-8L);
                *(uint64_t *)var_82 = 4238981UL;
                indirect_placeholder();
                local_sp_15 = var_82;
                if (*(unsigned char *)(*(uint64_t *)(((uint64_t)**(uint32_t **)var_80 << 3UL) + *(uint64_t *)(local_sp_14 + 32UL)) + 1UL) != '-' & var_81 == 0UL) {
                    return rax_4;
                }
            }
            local_sp_16 = local_sp_15;
            if (*(uint32_t *)(local_sp_15 + 152UL) == 0U) {
                var_83 = local_sp_15 + (-8L);
                *(uint64_t *)var_83 = 4239425UL;
                indirect_placeholder();
                local_sp_16 = var_83;
            }
            var_84 = *(uint64_t *)(local_sp_16 + 144UL);
            *(uint64_t *)(var_84 + 32UL) = 0UL;
            var_85 = (uint32_t *)var_84;
            *var_85 = (*var_85 + 1U);
            *(uint32_t *)(var_84 + 8UL) = 0U;
            return rax_4;
        }
        _pre202 = (uint64_t *)local_sp_11;
        _pre_phi203 = _pre202;
        var_27 = *_pre_phi203;
        *(uint32_t *)(local_sp_12 + 72UL) = 4294967295U;
        *(uint32_t *)(local_sp_12 + 56UL) = 0U;
        *(uint32_t *)(local_sp_12 + 76UL) = 0U;
        *(uint64_t *)(local_sp_12 + 40UL) = 0UL;
        *(uint32_t *)_pre_phi205 = (uint32_t)r15_3;
        *(uint64_t *)(local_sp_12 + 64UL) = r12_3;
        r13_0 = var_27;
        local_sp_13 = local_sp_12;
        while (1U)
            {
                var_28 = local_sp_13 + (-8L);
                *(uint64_t *)var_28 = 4238806UL;
                indirect_placeholder();
                rax_0 = rax_3;
                local_sp_3 = var_28;
                rbp_1 = rbp_4;
                rax_1 = rax_3;
                local_sp_4 = var_28;
                if ((uint64_t)(uint32_t)rax_3 == 0UL) {
                    var_39 = r13_0 + 32UL;
                    local_sp_5 = local_sp_4;
                    rax_3 = rax_1;
                    rbp_4 = rbp_1;
                    local_sp_13 = local_sp_4;
                    r13_0 = var_39;
                    local_sp_14 = local_sp_4;
                    rbp_2 = rbp_1;
                    local_sp_7 = local_sp_4;
                    if (*(uint64_t *)var_39 == 0UL) {
                        break;
                    }
                    rbx_1 = rbx_1 + 1UL;
                    continue;
                }
                rbp_1 = r13_0;
                if (rbp_4 == 0UL) {
                    *(uint32_t *)(local_sp_13 + 64UL) = (uint32_t)rbx_1;
                } else {
                    if (*(uint32_t *)(local_sp_13 + 4UL) != 0U) {
                        var_29 = *(uint32_t *)(r13_0 + 8UL);
                        rax_0 = (uint64_t)var_29;
                        var_30 = *(uint64_t *)(r13_0 + 16UL);
                        rax_0 = var_30;
                        var_31 = *(uint32_t *)(r13_0 + 24UL);
                        var_32 = (uint64_t)var_31;
                        rax_0 = var_32;
                        rax_1 = var_32;
                        if ((uint64_t)(*(uint32_t *)(rbp_4 + 8UL) - var_29) != 0UL & *(uint64_t *)(rbp_4 + 16UL) != var_30 & (uint64_t)(*(uint32_t *)(rbp_4 + 24UL) - var_31) != 0UL) {
                            var_39 = r13_0 + 32UL;
                            local_sp_5 = local_sp_4;
                            rax_3 = rax_1;
                            rbp_4 = rbp_1;
                            local_sp_13 = local_sp_4;
                            r13_0 = var_39;
                            local_sp_14 = local_sp_4;
                            rbp_2 = rbp_1;
                            local_sp_7 = local_sp_4;
                            if (*(uint64_t *)var_39 == 0UL) {
                                break;
                            }
                            rbx_1 = rbx_1 + 1UL;
                            continue;
                        }
                    }
                    var_33 = (uint32_t *)(local_sp_13 + 48UL);
                    rax_1 = rax_0;
                    if (*var_33 != 0U) {
                        rax_1 = 0UL;
                        if (*(uint32_t *)(local_sp_13 + 144UL) == 0U) {
                            *var_33 = 1U;
                            rax_1 = rax_0;
                            if (*(uint64_t *)(local_sp_13 + 32UL) == 0UL) {
                                var_38 = *(uint64_t *)(local_sp_3 + 40UL);
                                *(unsigned char *)(rbx_1 + var_38) = (unsigned char)'\x01';
                                rax_1 = var_38;
                                local_sp_4 = local_sp_3;
                            }
                        } else {
                            if (*(uint64_t *)(local_sp_13 + 32UL) == 0UL) {
                                var_38 = *(uint64_t *)(local_sp_3 + 40UL);
                                *(unsigned char *)(rbx_1 + var_38) = (unsigned char)'\x01';
                                rax_1 = var_38;
                                local_sp_4 = local_sp_3;
                            } else {
                                var_34 = (uint64_t)*(uint32_t *)(local_sp_13 + 8UL);
                                var_35 = local_sp_13 + (-16L);
                                *(uint64_t *)var_35 = 4238697UL;
                                var_36 = indirect_placeholder_5(var_34);
                                *(uint64_t *)(local_sp_13 + 24UL) = var_36;
                                local_sp_4 = var_35;
                                if (var_36 == 0UL) {
                                    *(uint32_t *)(local_sp_13 + 40UL) = 1U;
                                } else {
                                    var_37 = local_sp_13 + (-24L);
                                    *(uint64_t *)var_37 = 4238732UL;
                                    indirect_placeholder();
                                    *(unsigned char *)(var_36 + (uint64_t)*var_33) = (unsigned char)'\x01';
                                    *(uint32_t *)(local_sp_13 + 52UL) = 1U;
                                    local_sp_3 = var_37;
                                    var_38 = *(uint64_t *)(local_sp_3 + 40UL);
                                    *(unsigned char *)(rbx_1 + var_38) = (unsigned char)'\x01';
                                    rax_1 = var_38;
                                    local_sp_4 = local_sp_3;
                                }
                            }
                        }
                    }
                }
            }
        var_40 = *(uint32_t *)(local_sp_4 + 16UL);
        var_41 = (uint64_t)var_40;
        var_42 = *(uint64_t *)(local_sp_4 + 64UL);
        r12_1 = var_42;
        if (*(uint64_t *)(local_sp_4 + 40UL) == 0UL) {
            if (*(uint32_t *)(local_sp_4 + 152UL) != 0U) {
                var_43 = (*(uint32_t *)(local_sp_4 + 56UL) == 0U);
                var_44 = local_sp_4 + (-8L);
                var_45 = (uint64_t *)var_44;
                local_sp_5 = var_44;
                if (var_43) {
                    *var_45 = 4239111UL;
                    indirect_placeholder();
                } else {
                    *var_45 = 4239209UL;
                    indirect_placeholder();
                    var_46 = local_sp_4 + (-16L);
                    *(uint64_t *)var_46 = 4239255UL;
                    indirect_placeholder();
                    var_47 = helper_cc_compute_all_wrapper(var_41, 0UL, 0UL, 24U);
                    local_sp_0 = var_46;
                    local_sp_2 = var_46;
                    if ((uint64_t)(((unsigned char)(var_47 >> 4UL) ^ (unsigned char)var_47) & '\xc0') != 0UL) {
                        var_48 = *(uint64_t *)(local_sp_4 + 24UL);
                        var_49 = (uint64_t)(var_40 + (-1)) + var_48;
                        rbp_0 = var_48;
                        while (1U)
                            {
                                local_sp_1 = local_sp_0;
                                if (*(unsigned char *)rbp_0 == '\x00') {
                                    var_50 = local_sp_0 + (-8L);
                                    *(uint64_t *)var_50 = 4239332UL;
                                    indirect_placeholder();
                                    local_sp_1 = var_50;
                                }
                                local_sp_0 = local_sp_1;
                                local_sp_2 = local_sp_1;
                                if (rbp_0 == var_49) {
                                    break;
                                }
                                rbp_0 = rbp_0 + 1UL;
                                continue;
                            }
                    }
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4239347UL;
                    indirect_placeholder();
                    var_51 = local_sp_2 + (-16L);
                    *(uint64_t *)var_51 = 4239355UL;
                    indirect_placeholder();
                    local_sp_5 = var_51;
                }
            }
            local_sp_6 = local_sp_5;
            if (*(uint32_t *)(local_sp_5 + 76UL) == 0U) {
                var_52 = local_sp_5 + (-8L);
                *(uint64_t *)var_52 = 4239370UL;
                indirect_placeholder();
                local_sp_6 = var_52;
            }
            var_53 = *(uint64_t *)(local_sp_6 + 144UL);
            var_54 = (uint64_t *)(var_53 + 32UL);
            var_55 = *var_54;
            var_56 = (uint64_t)var_7;
            rdi2_0 = var_55;
            while (rcx4_0 != 0UL)
                {
                    var_57 = *(unsigned char *)rdi2_0;
                    var_58 = rcx4_0 + (-1L);
                    rcx4_0 = var_58;
                    rcx4_1 = var_58;
                    if (var_57 == '\x00') {
                        break;
                    }
                    rdi2_0 = rdi2_0 + var_56;
                }
            *var_54 = ((var_55 + (rcx4_1 ^ (-1L))) + (-1L));
            var_59 = (uint32_t *)var_53;
            *var_59 = (*var_59 + 1U);
            *(uint32_t *)(var_53 + 8UL) = 0U;
            return rax_4;
        }
        if (*(uint32_t *)(local_sp_4 + 56UL) != 0U) {
            if (rbp_1 != 0UL) {
                local_sp_15 = local_sp_14;
                rax_4 = 4294967295UL;
                if (*(uint32_t *)(local_sp_14 + 12UL) != 0U) {
                    var_80 = local_sp_14 + 144UL;
                    var_81 = *(uint64_t *)(*(uint64_t *)var_80 + 32UL);
                    var_82 = local_sp_14 + (-8L);
                    *(uint64_t *)var_82 = 4238981UL;
                    indirect_placeholder();
                    local_sp_15 = var_82;
                    if (*(unsigned char *)(*(uint64_t *)(((uint64_t)**(uint32_t **)var_80 << 3UL) + *(uint64_t *)(local_sp_14 + 32UL)) + 1UL) != '-' & var_81 == 0UL) {
                        return rax_4;
                    }
                }
                local_sp_16 = local_sp_15;
                if (*(uint32_t *)(local_sp_15 + 152UL) == 0U) {
                    var_83 = local_sp_15 + (-8L);
                    *(uint64_t *)var_83 = 4239425UL;
                    indirect_placeholder();
                    local_sp_16 = var_83;
                }
                var_84 = *(uint64_t *)(local_sp_16 + 144UL);
                *(uint64_t *)(var_84 + 32UL) = 0UL;
                var_85 = (uint32_t *)var_84;
                *var_85 = (*var_85 + 1U);
                *(uint32_t *)(var_84 + 8UL) = 0U;
                return rax_4;
            }
            var_60 = (uint64_t)*(uint32_t *)(local_sp_4 + 72UL);
            r15_0 = var_60;
            var_61 = local_sp_7 + 144UL;
            var_62 = **(uint32_t **)var_61;
            var_63 = (uint64_t)var_62 + 1UL;
            var_64 = (uint64_t *)var_61;
            var_65 = *var_64;
            *(uint32_t *)var_65 = (uint32_t)var_63;
            *(uint64_t *)(var_65 + 32UL) = 0UL;
            var_66 = (*(unsigned char *)r12_1 == '\x00');
            var_67 = *(uint32_t *)(rbp_2 + 8UL);
            _pre_phi201 = var_64;
            _pre_phi197 = var_64;
            local_sp_9 = local_sp_7;
            rax_4 = 0UL;
            if (var_66) {
                if (var_67 != 1U) {
                    var_69 = (uint64_t)*(uint32_t *)(local_sp_7 + 60UL);
                    var_70 = var_63 << 32UL;
                    if ((long)var_70 >= (long)(var_69 << 32UL)) {
                        if (*(uint32_t *)(local_sp_7 + 152UL) == 0U) {
                            var_71 = local_sp_7 + (-8L);
                            *(uint64_t *)var_71 = 4239656UL;
                            indirect_placeholder();
                            _pre_phi197 = (uint64_t *)(var_71 + 144UL);
                            local_sp_9 = var_71;
                        }
                        *(uint32_t *)(*_pre_phi197 + 8UL) = *(uint32_t *)(rbp_2 + 24UL);
                        var_72 = (**(unsigned char **)(local_sp_9 + 48UL) == ':') ? 58UL : 63UL;
                        rax_4 = var_72;
                        return rax_4;
                    }
                    var_73 = var_62 + 2U;
                    var_74 = *var_64;
                    *(uint32_t *)var_74 = var_73;
                    *(uint64_t *)(var_74 + 16UL) = *(uint64_t *)((uint64_t)((long)var_70 >> (long)29UL) + *(uint64_t *)(local_sp_7 + 32UL));
                }
            } else {
                if (var_67 != 0U) {
                    if (*(uint32_t *)(local_sp_7 + 152UL) == 0U) {
                        var_68 = local_sp_7 + (-8L);
                        *(uint64_t *)var_68 = 4239516UL;
                        indirect_placeholder();
                        _pre_phi201 = (uint64_t *)(var_68 + 144UL);
                    }
                    *(uint32_t *)(*_pre_phi201 + 8UL) = *(uint32_t *)(rbp_2 + 24UL);
                    return rax_4;
                }
                *(uint64_t *)(var_65 + 16UL) = (r12_1 + 1UL);
            }
            var_75 = *(uint64_t *)(local_sp_7 + 24UL);
            if (var_75 == 0UL) {
                *(uint32_t *)var_75 = (uint32_t)r15_0;
            }
            var_76 = *(uint64_t *)(rbp_2 + 16UL);
            var_77 = (var_76 == 0UL);
            var_78 = *(uint32_t *)(rbp_2 + 24UL);
            if (var_77) {
                var_79 = (uint64_t)var_78;
                rax_4 = var_79;
            } else {
                *(uint32_t *)var_76 = var_78;
            }
            return rax_4;
        }
    }
    var_17 = (rax_2 == 0UL);
    var_18 = (uint64_t)var_7;
    while (1U)
        {
            var_19 = local_sp_10 + (-8L);
            var_20 = (uint64_t *)var_19;
            *var_20 = 4238504UL;
            indirect_placeholder();
            rdi2_1 = rbx_0;
            local_sp_10 = var_19;
            r15_0 = r15_1;
            rbp_2 = rbp_3;
            local_sp_7 = var_19;
            local_sp_11 = var_19;
            _pre_phi203 = var_20;
            r15_3 = r15_1;
            local_sp_12 = var_19;
            if (var_17) {
                var_23 = rbp_3 + 32UL;
                var_24 = (uint64_t)((uint32_t)r15_1 + 1U);
                var_25 = *(uint64_t *)var_23;
                rbx_0 = var_25;
                r15_1 = var_24;
                rbp_3 = var_23;
                r15_2 = var_24;
                if (var_25 == 0UL) {
                    continue;
                }
                loop_state_var = 1U;
                break;
            }
            while (rcx4_2 != 0UL)
                {
                    var_21 = (uint64_t)('\x00' - *(unsigned char *)rdi2_1);
                    var_22 = rcx4_2 + (-1L);
                    rcx4_2 = var_22;
                    rcx4_3 = var_22;
                    if (var_21 == 0UL) {
                        break;
                    }
                    rdi2_1 = rdi2_1 + var_18;
                }
            if ((18446744073709551614UL - (-2L)) != (r12_3 - var_10)) {
                if (rbp_3 != 0UL) {
                    loop_state_var = 2U;
                    break;
                }
                _pre204 = var_19 + 16UL;
                _pre_phi205 = _pre204;
                loop_state_var = 0U;
                break;
            }
        }
    switch (loop_state_var) {
      case 0U:
        {
            var_27 = *_pre_phi203;
            *(uint32_t *)(local_sp_12 + 72UL) = 4294967295U;
            *(uint32_t *)(local_sp_12 + 56UL) = 0U;
            *(uint32_t *)(local_sp_12 + 76UL) = 0U;
            *(uint64_t *)(local_sp_12 + 40UL) = 0UL;
            *(uint32_t *)_pre_phi205 = (uint32_t)r15_3;
            *(uint64_t *)(local_sp_12 + 64UL) = r12_3;
            r13_0 = var_27;
            local_sp_13 = local_sp_12;
            while (1U)
                {
                    var_28 = local_sp_13 + (-8L);
                    *(uint64_t *)var_28 = 4238806UL;
                    indirect_placeholder();
                    rax_0 = rax_3;
                    local_sp_3 = var_28;
                    rbp_1 = rbp_4;
                    rax_1 = rax_3;
                    local_sp_4 = var_28;
                    if ((uint64_t)(uint32_t)rax_3 == 0UL) {
                        var_39 = r13_0 + 32UL;
                        local_sp_5 = local_sp_4;
                        rax_3 = rax_1;
                        rbp_4 = rbp_1;
                        local_sp_13 = local_sp_4;
                        r13_0 = var_39;
                        local_sp_14 = local_sp_4;
                        rbp_2 = rbp_1;
                        local_sp_7 = local_sp_4;
                        if (*(uint64_t *)var_39 == 0UL) {
                            break;
                        }
                        rbx_1 = rbx_1 + 1UL;
                        continue;
                    }
                    rbp_1 = r13_0;
                    if (rbp_4 == 0UL) {
                        *(uint32_t *)(local_sp_13 + 64UL) = (uint32_t)rbx_1;
                    } else {
                        if (*(uint32_t *)(local_sp_13 + 4UL) != 0U) {
                            var_29 = *(uint32_t *)(r13_0 + 8UL);
                            rax_0 = (uint64_t)var_29;
                            var_30 = *(uint64_t *)(r13_0 + 16UL);
                            rax_0 = var_30;
                            var_31 = *(uint32_t *)(r13_0 + 24UL);
                            var_32 = (uint64_t)var_31;
                            rax_0 = var_32;
                            rax_1 = var_32;
                            if ((uint64_t)(*(uint32_t *)(rbp_4 + 8UL) - var_29) != 0UL & *(uint64_t *)(rbp_4 + 16UL) != var_30 & (uint64_t)(*(uint32_t *)(rbp_4 + 24UL) - var_31) != 0UL) {
                                var_39 = r13_0 + 32UL;
                                local_sp_5 = local_sp_4;
                                rax_3 = rax_1;
                                rbp_4 = rbp_1;
                                local_sp_13 = local_sp_4;
                                r13_0 = var_39;
                                local_sp_14 = local_sp_4;
                                rbp_2 = rbp_1;
                                local_sp_7 = local_sp_4;
                                if (*(uint64_t *)var_39 == 0UL) {
                                    break;
                                }
                                rbx_1 = rbx_1 + 1UL;
                                continue;
                            }
                        }
                        var_33 = (uint32_t *)(local_sp_13 + 48UL);
                        rax_1 = rax_0;
                        if (*var_33 != 0U) {
                            rax_1 = 0UL;
                            if (*(uint32_t *)(local_sp_13 + 144UL) == 0U) {
                                *var_33 = 1U;
                                rax_1 = rax_0;
                                if (*(uint64_t *)(local_sp_13 + 32UL) == 0UL) {
                                    var_38 = *(uint64_t *)(local_sp_3 + 40UL);
                                    *(unsigned char *)(rbx_1 + var_38) = (unsigned char)'\x01';
                                    rax_1 = var_38;
                                    local_sp_4 = local_sp_3;
                                }
                            } else {
                                if (*(uint64_t *)(local_sp_13 + 32UL) == 0UL) {
                                    var_38 = *(uint64_t *)(local_sp_3 + 40UL);
                                    *(unsigned char *)(rbx_1 + var_38) = (unsigned char)'\x01';
                                    rax_1 = var_38;
                                    local_sp_4 = local_sp_3;
                                } else {
                                    var_34 = (uint64_t)*(uint32_t *)(local_sp_13 + 8UL);
                                    var_35 = local_sp_13 + (-16L);
                                    *(uint64_t *)var_35 = 4238697UL;
                                    var_36 = indirect_placeholder_5(var_34);
                                    *(uint64_t *)(local_sp_13 + 24UL) = var_36;
                                    local_sp_4 = var_35;
                                    if (var_36 == 0UL) {
                                        *(uint32_t *)(local_sp_13 + 40UL) = 1U;
                                    } else {
                                        var_37 = local_sp_13 + (-24L);
                                        *(uint64_t *)var_37 = 4238732UL;
                                        indirect_placeholder();
                                        *(unsigned char *)(var_36 + (uint64_t)*var_33) = (unsigned char)'\x01';
                                        *(uint32_t *)(local_sp_13 + 52UL) = 1U;
                                        local_sp_3 = var_37;
                                        var_38 = *(uint64_t *)(local_sp_3 + 40UL);
                                        *(unsigned char *)(rbx_1 + var_38) = (unsigned char)'\x01';
                                        rax_1 = var_38;
                                        local_sp_4 = local_sp_3;
                                    }
                                }
                            }
                        }
                    }
                }
            var_40 = *(uint32_t *)(local_sp_4 + 16UL);
            var_41 = (uint64_t)var_40;
            var_42 = *(uint64_t *)(local_sp_4 + 64UL);
            r12_1 = var_42;
            if (*(uint64_t *)(local_sp_4 + 40UL) != 0UL) {
                if (*(uint32_t *)(local_sp_4 + 56UL) != 0U) {
                    if (rbp_1 != 0UL) {
                        local_sp_15 = local_sp_14;
                        rax_4 = 4294967295UL;
                        if (*(uint32_t *)(local_sp_14 + 12UL) != 0U) {
                            var_80 = local_sp_14 + 144UL;
                            var_81 = *(uint64_t *)(*(uint64_t *)var_80 + 32UL);
                            var_82 = local_sp_14 + (-8L);
                            *(uint64_t *)var_82 = 4238981UL;
                            indirect_placeholder();
                            local_sp_15 = var_82;
                            if (*(unsigned char *)(*(uint64_t *)(((uint64_t)**(uint32_t **)var_80 << 3UL) + *(uint64_t *)(local_sp_14 + 32UL)) + 1UL) != '-' & var_81 == 0UL) {
                                return rax_4;
                            }
                        }
                        local_sp_16 = local_sp_15;
                        if (*(uint32_t *)(local_sp_15 + 152UL) == 0U) {
                            var_83 = local_sp_15 + (-8L);
                            *(uint64_t *)var_83 = 4239425UL;
                            indirect_placeholder();
                            local_sp_16 = var_83;
                        }
                        var_84 = *(uint64_t *)(local_sp_16 + 144UL);
                        *(uint64_t *)(var_84 + 32UL) = 0UL;
                        var_85 = (uint32_t *)var_84;
                        *var_85 = (*var_85 + 1U);
                        *(uint32_t *)(var_84 + 8UL) = 0U;
                        return rax_4;
                    }
                    var_60 = (uint64_t)*(uint32_t *)(local_sp_4 + 72UL);
                    r15_0 = var_60;
                    var_61 = local_sp_7 + 144UL;
                    var_62 = **(uint32_t **)var_61;
                    var_63 = (uint64_t)var_62 + 1UL;
                    var_64 = (uint64_t *)var_61;
                    var_65 = *var_64;
                    *(uint32_t *)var_65 = (uint32_t)var_63;
                    *(uint64_t *)(var_65 + 32UL) = 0UL;
                    var_66 = (*(unsigned char *)r12_1 == '\x00');
                    var_67 = *(uint32_t *)(rbp_2 + 8UL);
                    _pre_phi201 = var_64;
                    _pre_phi197 = var_64;
                    local_sp_9 = local_sp_7;
                    rax_4 = 0UL;
                    if (var_66) {
                        if (var_67 != 1U) {
                            var_69 = (uint64_t)*(uint32_t *)(local_sp_7 + 60UL);
                            var_70 = var_63 << 32UL;
                            if ((long)var_70 >= (long)(var_69 << 32UL)) {
                                if (*(uint32_t *)(local_sp_7 + 152UL) == 0U) {
                                    var_71 = local_sp_7 + (-8L);
                                    *(uint64_t *)var_71 = 4239656UL;
                                    indirect_placeholder();
                                    _pre_phi197 = (uint64_t *)(var_71 + 144UL);
                                    local_sp_9 = var_71;
                                }
                                *(uint32_t *)(*_pre_phi197 + 8UL) = *(uint32_t *)(rbp_2 + 24UL);
                                var_72 = (**(unsigned char **)(local_sp_9 + 48UL) == ':') ? 58UL : 63UL;
                                rax_4 = var_72;
                                return rax_4;
                            }
                            var_73 = var_62 + 2U;
                            var_74 = *var_64;
                            *(uint32_t *)var_74 = var_73;
                            *(uint64_t *)(var_74 + 16UL) = *(uint64_t *)((uint64_t)((long)var_70 >> (long)29UL) + *(uint64_t *)(local_sp_7 + 32UL));
                        }
                    } else {
                        if (var_67 != 0U) {
                            if (*(uint32_t *)(local_sp_7 + 152UL) == 0U) {
                                var_68 = local_sp_7 + (-8L);
                                *(uint64_t *)var_68 = 4239516UL;
                                indirect_placeholder();
                                _pre_phi201 = (uint64_t *)(var_68 + 144UL);
                            }
                            *(uint32_t *)(*_pre_phi201 + 8UL) = *(uint32_t *)(rbp_2 + 24UL);
                            return rax_4;
                        }
                        *(uint64_t *)(var_65 + 16UL) = (r12_1 + 1UL);
                    }
                    var_75 = *(uint64_t *)(local_sp_7 + 24UL);
                    if (var_75 == 0UL) {
                        *(uint32_t *)var_75 = (uint32_t)r15_0;
                    }
                    var_76 = *(uint64_t *)(rbp_2 + 16UL);
                    var_77 = (var_76 == 0UL);
                    var_78 = *(uint32_t *)(rbp_2 + 24UL);
                    if (var_77) {
                        var_79 = (uint64_t)var_78;
                        rax_4 = var_79;
                    } else {
                        *(uint32_t *)var_76 = var_78;
                    }
                    return rax_4;
                }
            }
            if (*(uint32_t *)(local_sp_4 + 152UL) != 0U) {
                var_43 = (*(uint32_t *)(local_sp_4 + 56UL) == 0U);
                var_44 = local_sp_4 + (-8L);
                var_45 = (uint64_t *)var_44;
                local_sp_5 = var_44;
                if (var_43) {
                    *var_45 = 4239111UL;
                    indirect_placeholder();
                } else {
                    *var_45 = 4239209UL;
                    indirect_placeholder();
                    var_46 = local_sp_4 + (-16L);
                    *(uint64_t *)var_46 = 4239255UL;
                    indirect_placeholder();
                    var_47 = helper_cc_compute_all_wrapper(var_41, 0UL, 0UL, 24U);
                    local_sp_0 = var_46;
                    local_sp_2 = var_46;
                    if ((uint64_t)(((unsigned char)(var_47 >> 4UL) ^ (unsigned char)var_47) & '\xc0') != 0UL) {
                        var_48 = *(uint64_t *)(local_sp_4 + 24UL);
                        var_49 = (uint64_t)(var_40 + (-1)) + var_48;
                        rbp_0 = var_48;
                        while (1U)
                            {
                                local_sp_1 = local_sp_0;
                                if (*(unsigned char *)rbp_0 == '\x00') {
                                    var_50 = local_sp_0 + (-8L);
                                    *(uint64_t *)var_50 = 4239332UL;
                                    indirect_placeholder();
                                    local_sp_1 = var_50;
                                }
                                local_sp_0 = local_sp_1;
                                local_sp_2 = local_sp_1;
                                if (rbp_0 == var_49) {
                                    break;
                                }
                                rbp_0 = rbp_0 + 1UL;
                                continue;
                            }
                    }
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4239347UL;
                    indirect_placeholder();
                    var_51 = local_sp_2 + (-16L);
                    *(uint64_t *)var_51 = 4239355UL;
                    indirect_placeholder();
                    local_sp_5 = var_51;
                }
            }
            local_sp_6 = local_sp_5;
            if (*(uint32_t *)(local_sp_5 + 76UL) == 0U) {
                var_52 = local_sp_5 + (-8L);
                *(uint64_t *)var_52 = 4239370UL;
                indirect_placeholder();
                local_sp_6 = var_52;
            }
            var_53 = *(uint64_t *)(local_sp_6 + 144UL);
            var_54 = (uint64_t *)(var_53 + 32UL);
            var_55 = *var_54;
            var_56 = (uint64_t)var_7;
            rdi2_0 = var_55;
            while (rcx4_0 != 0UL)
                {
                    var_57 = *(unsigned char *)rdi2_0;
                    var_58 = rcx4_0 + (-1L);
                    rcx4_0 = var_58;
                    rcx4_1 = var_58;
                    if (var_57 == '\x00') {
                        break;
                    }
                    rdi2_0 = rdi2_0 + var_56;
                }
            *var_54 = ((var_55 + (rcx4_1 ^ (-1L))) + (-1L));
            var_59 = (uint32_t *)var_53;
            *var_59 = (*var_59 + 1U);
            *(uint32_t *)(var_53 + 8UL) = 0U;
        }
        break;
      case 1U:
        {
            var_26 = local_sp_11 + 16UL;
            _pre_phi205 = var_26;
            r15_3 = r15_2;
            local_sp_12 = local_sp_11;
            local_sp_14 = local_sp_11;
            if (*(uint64_t *)var_26 != 0UL) {
                local_sp_15 = local_sp_14;
                rax_4 = 4294967295UL;
                if (*(uint32_t *)(local_sp_14 + 12UL) != 0U) {
                    var_80 = local_sp_14 + 144UL;
                    var_81 = *(uint64_t *)(*(uint64_t *)var_80 + 32UL);
                    var_82 = local_sp_14 + (-8L);
                    *(uint64_t *)var_82 = 4238981UL;
                    indirect_placeholder();
                    local_sp_15 = var_82;
                    if (*(unsigned char *)(*(uint64_t *)(((uint64_t)**(uint32_t **)var_80 << 3UL) + *(uint64_t *)(local_sp_14 + 32UL)) + 1UL) != '-' & var_81 == 0UL) {
                        return rax_4;
                    }
                }
                local_sp_16 = local_sp_15;
                if (*(uint32_t *)(local_sp_15 + 152UL) == 0U) {
                    var_83 = local_sp_15 + (-8L);
                    *(uint64_t *)var_83 = 4239425UL;
                    indirect_placeholder();
                    local_sp_16 = var_83;
                }
                var_84 = *(uint64_t *)(local_sp_16 + 144UL);
                *(uint64_t *)(var_84 + 32UL) = 0UL;
                var_85 = (uint32_t *)var_84;
                *var_85 = (*var_85 + 1U);
                *(uint32_t *)(var_84 + 8UL) = 0U;
                return rax_4;
            }
            _pre202 = (uint64_t *)local_sp_11;
            _pre_phi203 = _pre202;
            var_27 = *_pre_phi203;
            *(uint32_t *)(local_sp_12 + 72UL) = 4294967295U;
            *(uint32_t *)(local_sp_12 + 56UL) = 0U;
            *(uint32_t *)(local_sp_12 + 76UL) = 0U;
            *(uint64_t *)(local_sp_12 + 40UL) = 0UL;
            *(uint32_t *)_pre_phi205 = (uint32_t)r15_3;
            *(uint64_t *)(local_sp_12 + 64UL) = r12_3;
            r13_0 = var_27;
            local_sp_13 = local_sp_12;
            while (1U)
                {
                    var_28 = local_sp_13 + (-8L);
                    *(uint64_t *)var_28 = 4238806UL;
                    indirect_placeholder();
                    rax_0 = rax_3;
                    local_sp_3 = var_28;
                    rbp_1 = rbp_4;
                    rax_1 = rax_3;
                    local_sp_4 = var_28;
                    if ((uint64_t)(uint32_t)rax_3 == 0UL) {
                        var_39 = r13_0 + 32UL;
                        local_sp_5 = local_sp_4;
                        rax_3 = rax_1;
                        rbp_4 = rbp_1;
                        local_sp_13 = local_sp_4;
                        r13_0 = var_39;
                        local_sp_14 = local_sp_4;
                        rbp_2 = rbp_1;
                        local_sp_7 = local_sp_4;
                        if (*(uint64_t *)var_39 == 0UL) {
                            break;
                        }
                        rbx_1 = rbx_1 + 1UL;
                        continue;
                    }
                    rbp_1 = r13_0;
                    if (rbp_4 == 0UL) {
                        *(uint32_t *)(local_sp_13 + 64UL) = (uint32_t)rbx_1;
                    } else {
                        if (*(uint32_t *)(local_sp_13 + 4UL) != 0U) {
                            var_29 = *(uint32_t *)(r13_0 + 8UL);
                            rax_0 = (uint64_t)var_29;
                            var_30 = *(uint64_t *)(r13_0 + 16UL);
                            rax_0 = var_30;
                            var_31 = *(uint32_t *)(r13_0 + 24UL);
                            var_32 = (uint64_t)var_31;
                            rax_0 = var_32;
                            rax_1 = var_32;
                            if ((uint64_t)(*(uint32_t *)(rbp_4 + 8UL) - var_29) != 0UL & *(uint64_t *)(rbp_4 + 16UL) != var_30 & (uint64_t)(*(uint32_t *)(rbp_4 + 24UL) - var_31) != 0UL) {
                                var_39 = r13_0 + 32UL;
                                local_sp_5 = local_sp_4;
                                rax_3 = rax_1;
                                rbp_4 = rbp_1;
                                local_sp_13 = local_sp_4;
                                r13_0 = var_39;
                                local_sp_14 = local_sp_4;
                                rbp_2 = rbp_1;
                                local_sp_7 = local_sp_4;
                                if (*(uint64_t *)var_39 == 0UL) {
                                    break;
                                }
                                rbx_1 = rbx_1 + 1UL;
                                continue;
                            }
                        }
                        var_33 = (uint32_t *)(local_sp_13 + 48UL);
                        rax_1 = rax_0;
                        if (*var_33 != 0U) {
                            rax_1 = 0UL;
                            if (*(uint32_t *)(local_sp_13 + 144UL) == 0U) {
                                *var_33 = 1U;
                                rax_1 = rax_0;
                                if (*(uint64_t *)(local_sp_13 + 32UL) == 0UL) {
                                    var_38 = *(uint64_t *)(local_sp_3 + 40UL);
                                    *(unsigned char *)(rbx_1 + var_38) = (unsigned char)'\x01';
                                    rax_1 = var_38;
                                    local_sp_4 = local_sp_3;
                                }
                            } else {
                                if (*(uint64_t *)(local_sp_13 + 32UL) == 0UL) {
                                    var_38 = *(uint64_t *)(local_sp_3 + 40UL);
                                    *(unsigned char *)(rbx_1 + var_38) = (unsigned char)'\x01';
                                    rax_1 = var_38;
                                    local_sp_4 = local_sp_3;
                                } else {
                                    var_34 = (uint64_t)*(uint32_t *)(local_sp_13 + 8UL);
                                    var_35 = local_sp_13 + (-16L);
                                    *(uint64_t *)var_35 = 4238697UL;
                                    var_36 = indirect_placeholder_5(var_34);
                                    *(uint64_t *)(local_sp_13 + 24UL) = var_36;
                                    local_sp_4 = var_35;
                                    if (var_36 == 0UL) {
                                        *(uint32_t *)(local_sp_13 + 40UL) = 1U;
                                    } else {
                                        var_37 = local_sp_13 + (-24L);
                                        *(uint64_t *)var_37 = 4238732UL;
                                        indirect_placeholder();
                                        *(unsigned char *)(var_36 + (uint64_t)*var_33) = (unsigned char)'\x01';
                                        *(uint32_t *)(local_sp_13 + 52UL) = 1U;
                                        local_sp_3 = var_37;
                                        var_38 = *(uint64_t *)(local_sp_3 + 40UL);
                                        *(unsigned char *)(rbx_1 + var_38) = (unsigned char)'\x01';
                                        rax_1 = var_38;
                                        local_sp_4 = local_sp_3;
                                    }
                                }
                            }
                        }
                    }
                }
            var_40 = *(uint32_t *)(local_sp_4 + 16UL);
            var_41 = (uint64_t)var_40;
            var_42 = *(uint64_t *)(local_sp_4 + 64UL);
            r12_1 = var_42;
            if (*(uint64_t *)(local_sp_4 + 40UL) != 0UL) {
                if (*(uint32_t *)(local_sp_4 + 56UL) == 0U) {
                    if (rbp_1 == 0UL) {
                        local_sp_15 = local_sp_14;
                        rax_4 = 4294967295UL;
                        if (*(uint32_t *)(local_sp_14 + 12UL) != 0U) {
                            local_sp_16 = local_sp_15;
                            if (*(uint32_t *)(local_sp_15 + 152UL) == 0U) {
                                var_83 = local_sp_15 + (-8L);
                                *(uint64_t *)var_83 = 4239425UL;
                                indirect_placeholder();
                                local_sp_16 = var_83;
                            }
                            var_84 = *(uint64_t *)(local_sp_16 + 144UL);
                            *(uint64_t *)(var_84 + 32UL) = 0UL;
                            var_85 = (uint32_t *)var_84;
                            *var_85 = (*var_85 + 1U);
                            *(uint32_t *)(var_84 + 8UL) = 0U;
                            return rax_4;
                        }
                        var_80 = local_sp_14 + 144UL;
                        if (*(unsigned char *)(*(uint64_t *)(((uint64_t)**(uint32_t **)var_80 << 3UL) + *(uint64_t *)(local_sp_14 + 32UL)) + 1UL) != '-') {
                            var_81 = *(uint64_t *)(*(uint64_t *)var_80 + 32UL);
                            var_82 = local_sp_14 + (-8L);
                            *(uint64_t *)var_82 = 4238981UL;
                            indirect_placeholder();
                            local_sp_15 = var_82;
                            if (var_81 == 0UL) {
                                return rax_4;
                            }
                        }
                        local_sp_16 = local_sp_15;
                        if (*(uint32_t *)(local_sp_15 + 152UL) == 0U) {
                            var_83 = local_sp_15 + (-8L);
                            *(uint64_t *)var_83 = 4239425UL;
                            indirect_placeholder();
                            local_sp_16 = var_83;
                        }
                        var_84 = *(uint64_t *)(local_sp_16 + 144UL);
                        *(uint64_t *)(var_84 + 32UL) = 0UL;
                        var_85 = (uint32_t *)var_84;
                        *var_85 = (*var_85 + 1U);
                        *(uint32_t *)(var_84 + 8UL) = 0U;
                        return rax_4;
                    }
                    var_60 = (uint64_t)*(uint32_t *)(local_sp_4 + 72UL);
                    r15_0 = var_60;
                    var_61 = local_sp_7 + 144UL;
                    var_62 = **(uint32_t **)var_61;
                    var_63 = (uint64_t)var_62 + 1UL;
                    var_64 = (uint64_t *)var_61;
                    var_65 = *var_64;
                    *(uint32_t *)var_65 = (uint32_t)var_63;
                    *(uint64_t *)(var_65 + 32UL) = 0UL;
                    var_66 = (*(unsigned char *)r12_1 == '\x00');
                    var_67 = *(uint32_t *)(rbp_2 + 8UL);
                    _pre_phi201 = var_64;
                    _pre_phi197 = var_64;
                    local_sp_9 = local_sp_7;
                    rax_4 = 0UL;
                    if (var_66) {
                        if (var_67 != 0U) {
                            if (*(uint32_t *)(local_sp_7 + 152UL) == 0U) {
                                var_68 = local_sp_7 + (-8L);
                                *(uint64_t *)var_68 = 4239516UL;
                                indirect_placeholder();
                                _pre_phi201 = (uint64_t *)(var_68 + 144UL);
                            }
                            *(uint32_t *)(*_pre_phi201 + 8UL) = *(uint32_t *)(rbp_2 + 24UL);
                            return rax_4;
                        }
                        *(uint64_t *)(var_65 + 16UL) = (r12_1 + 1UL);
                    } else {
                        if (var_67 != 1U) {
                            var_69 = (uint64_t)*(uint32_t *)(local_sp_7 + 60UL);
                            var_70 = var_63 << 32UL;
                            if ((long)var_70 >= (long)(var_69 << 32UL)) {
                                if (*(uint32_t *)(local_sp_7 + 152UL) == 0U) {
                                    var_71 = local_sp_7 + (-8L);
                                    *(uint64_t *)var_71 = 4239656UL;
                                    indirect_placeholder();
                                    _pre_phi197 = (uint64_t *)(var_71 + 144UL);
                                    local_sp_9 = var_71;
                                }
                                *(uint32_t *)(*_pre_phi197 + 8UL) = *(uint32_t *)(rbp_2 + 24UL);
                                var_72 = (**(unsigned char **)(local_sp_9 + 48UL) == ':') ? 58UL : 63UL;
                                rax_4 = var_72;
                                return rax_4;
                            }
                            var_73 = var_62 + 2U;
                            var_74 = *var_64;
                            *(uint32_t *)var_74 = var_73;
                            *(uint64_t *)(var_74 + 16UL) = *(uint64_t *)((uint64_t)((long)var_70 >> (long)29UL) + *(uint64_t *)(local_sp_7 + 32UL));
                        }
                    }
                    var_75 = *(uint64_t *)(local_sp_7 + 24UL);
                    if (var_75 == 0UL) {
                        *(uint32_t *)var_75 = (uint32_t)r15_0;
                    }
                    var_76 = *(uint64_t *)(rbp_2 + 16UL);
                    var_77 = (var_76 == 0UL);
                    var_78 = *(uint32_t *)(rbp_2 + 24UL);
                    if (var_77) {
                        *(uint32_t *)var_76 = var_78;
                    } else {
                        var_79 = (uint64_t)var_78;
                        rax_4 = var_79;
                    }
                    return rax_4;
                }
            }
            if (*(uint32_t *)(local_sp_4 + 152UL) != 0U) {
                var_43 = (*(uint32_t *)(local_sp_4 + 56UL) == 0U);
                var_44 = local_sp_4 + (-8L);
                var_45 = (uint64_t *)var_44;
                local_sp_5 = var_44;
                if (var_43) {
                    *var_45 = 4239111UL;
                    indirect_placeholder();
                } else {
                    *var_45 = 4239209UL;
                    indirect_placeholder();
                    var_46 = local_sp_4 + (-16L);
                    *(uint64_t *)var_46 = 4239255UL;
                    indirect_placeholder();
                    var_47 = helper_cc_compute_all_wrapper(var_41, 0UL, 0UL, 24U);
                    local_sp_0 = var_46;
                    local_sp_2 = var_46;
                    if ((uint64_t)(((unsigned char)(var_47 >> 4UL) ^ (unsigned char)var_47) & '\xc0') != 0UL) {
                        var_48 = *(uint64_t *)(local_sp_4 + 24UL);
                        var_49 = (uint64_t)(var_40 + (-1)) + var_48;
                        rbp_0 = var_48;
                        while (1U)
                            {
                                local_sp_1 = local_sp_0;
                                if (*(unsigned char *)rbp_0 == '\x00') {
                                    var_50 = local_sp_0 + (-8L);
                                    *(uint64_t *)var_50 = 4239332UL;
                                    indirect_placeholder();
                                    local_sp_1 = var_50;
                                }
                                local_sp_0 = local_sp_1;
                                local_sp_2 = local_sp_1;
                                if (rbp_0 == var_49) {
                                    break;
                                }
                                rbp_0 = rbp_0 + 1UL;
                                continue;
                            }
                    }
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4239347UL;
                    indirect_placeholder();
                    var_51 = local_sp_2 + (-16L);
                    *(uint64_t *)var_51 = 4239355UL;
                    indirect_placeholder();
                    local_sp_5 = var_51;
                }
            }
            local_sp_6 = local_sp_5;
            if (*(uint32_t *)(local_sp_5 + 76UL) == 0U) {
                var_52 = local_sp_5 + (-8L);
                *(uint64_t *)var_52 = 4239370UL;
                indirect_placeholder();
                local_sp_6 = var_52;
            }
            var_53 = *(uint64_t *)(local_sp_6 + 144UL);
            var_54 = (uint64_t *)(var_53 + 32UL);
            var_55 = *var_54;
            var_56 = (uint64_t)var_7;
            rdi2_0 = var_55;
            while (rcx4_0 != 0UL)
                {
                    var_57 = *(unsigned char *)rdi2_0;
                    var_58 = rcx4_0 + (-1L);
                    rcx4_0 = var_58;
                    rcx4_1 = var_58;
                    if (var_57 == '\x00') {
                        break;
                    }
                    rdi2_0 = rdi2_0 + var_56;
                }
            *var_54 = ((var_55 + (rcx4_1 ^ (-1L))) + (-1L));
            var_59 = (uint32_t *)var_53;
            *var_59 = (*var_59 + 1U);
            *(uint32_t *)(var_53 + 8UL) = 0U;
        }
        break;
      case 2U:
        {
            var_61 = local_sp_7 + 144UL;
            var_62 = **(uint32_t **)var_61;
            var_63 = (uint64_t)var_62 + 1UL;
            var_64 = (uint64_t *)var_61;
            var_65 = *var_64;
            *(uint32_t *)var_65 = (uint32_t)var_63;
            *(uint64_t *)(var_65 + 32UL) = 0UL;
            var_66 = (*(unsigned char *)r12_1 == '\x00');
            var_67 = *(uint32_t *)(rbp_2 + 8UL);
            _pre_phi201 = var_64;
            _pre_phi197 = var_64;
            local_sp_9 = local_sp_7;
            rax_4 = 0UL;
            if (var_66) {
                if (var_67 != 1U) {
                    var_69 = (uint64_t)*(uint32_t *)(local_sp_7 + 60UL);
                    var_70 = var_63 << 32UL;
                    if ((long)var_70 >= (long)(var_69 << 32UL)) {
                        if (*(uint32_t *)(local_sp_7 + 152UL) == 0U) {
                            var_71 = local_sp_7 + (-8L);
                            *(uint64_t *)var_71 = 4239656UL;
                            indirect_placeholder();
                            _pre_phi197 = (uint64_t *)(var_71 + 144UL);
                            local_sp_9 = var_71;
                        }
                        *(uint32_t *)(*_pre_phi197 + 8UL) = *(uint32_t *)(rbp_2 + 24UL);
                        var_72 = (**(unsigned char **)(local_sp_9 + 48UL) == ':') ? 58UL : 63UL;
                        rax_4 = var_72;
                        return rax_4;
                    }
                    var_73 = var_62 + 2U;
                    var_74 = *var_64;
                    *(uint32_t *)var_74 = var_73;
                    *(uint64_t *)(var_74 + 16UL) = *(uint64_t *)((uint64_t)((long)var_70 >> (long)29UL) + *(uint64_t *)(local_sp_7 + 32UL));
                }
            } else {
                if (var_67 != 0U) {
                    if (*(uint32_t *)(local_sp_7 + 152UL) == 0U) {
                        var_68 = local_sp_7 + (-8L);
                        *(uint64_t *)var_68 = 4239516UL;
                        indirect_placeholder();
                        _pre_phi201 = (uint64_t *)(var_68 + 144UL);
                    }
                    *(uint32_t *)(*_pre_phi201 + 8UL) = *(uint32_t *)(rbp_2 + 24UL);
                    return rax_4;
                }
                *(uint64_t *)(var_65 + 16UL) = (r12_1 + 1UL);
            }
            var_75 = *(uint64_t *)(local_sp_7 + 24UL);
            if (var_75 == 0UL) {
                *(uint32_t *)var_75 = (uint32_t)r15_0;
            }
            var_76 = *(uint64_t *)(rbp_2 + 16UL);
            var_77 = (var_76 == 0UL);
            var_78 = *(uint32_t *)(rbp_2 + 24UL);
            if (var_77) {
                var_79 = (uint64_t)var_78;
                rax_4 = var_79;
            } else {
                *(uint32_t *)var_76 = var_78;
            }
            return rax_4;
        }
        break;
    }
}
