typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint32_t init_state_0x82fc(void);
uint64_t bb_argmatch(uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t rcx) {
    uint64_t rbx_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint32_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t rbp_0;
    uint64_t rax_1;
    uint64_t rdi2_0;
    uint64_t rcx4_0;
    uint64_t rcx4_1;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t local_sp_1;
    uint64_t var_17;
    uint64_t var_26;
    uint64_t var_18;
    unsigned char *var_19;
    uint64_t var_20;
    uint64_t rax_0;
    uint64_t local_sp_0;
    uint64_t var_21;
    uint64_t var_22;
    unsigned char var_23;
    uint64_t *var_24;
    uint64_t var_25;
    uint64_t rdi2_1;
    uint64_t rcx4_2;
    uint64_t rcx4_3;
    unsigned char var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t *var_14;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_state_0x82fc();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_8 = var_0 + (-88L);
    *(uint64_t *)(var_0 + (-80L)) = rdi;
    *(uint64_t *)(var_0 + (-64L)) = rdx;
    var_9 = (uint64_t)var_7;
    rbp_0 = 0UL;
    rax_1 = 0UL;
    rcx4_0 = 18446744073709551615UL;
    rcx4_1 = 0UL;
    local_sp_1 = var_8;
    var_26 = 18446744073709551615UL;
    rdi2_1 = rdi;
    rcx4_2 = 18446744073709551615UL;
    rcx4_3 = 0UL;
    while (rcx4_2 != 0UL)
        {
            rdi2_1 = rdi2_1 + var_9;
        }
    var_12 = *(uint64_t *)rsi;
    rbx_0 = var_12;
    if (var_12 != 0UL) {
        *(unsigned char *)(var_0 + (-65L)) = (unsigned char)'\x00';
        *(uint64_t *)var_8 = 18446744073709551615UL;
        while (1U)
            {
                var_13 = local_sp_1 + (-8L);
                var_14 = (uint64_t *)var_13;
                *var_14 = 4226782UL;
                indirect_placeholder();
                rdi2_0 = rbx_0;
                rax_0 = rax_1;
                local_sp_0 = var_13;
                var_26 = rbp_0;
                if ((uint64_t)(uint32_t)rax_1 != 0UL) {
                    rax_0 = 0UL;
                    while (rcx4_0 != 0UL)
                        {
                            var_15 = (uint64_t)((unsigned char)rax_1 - *(unsigned char *)rdi2_0);
                            var_16 = rcx4_0 + (-1L);
                            rcx4_0 = var_16;
                            rcx4_1 = var_16;
                            if (var_15 == 0UL) {
                                break;
                            }
                            rdi2_0 = rdi2_0 + var_9;
                            rax_0 = 0UL;
                        }
                    if (rcx4_3 != rcx4_1) {
                        *var_14 = rbp_0;
                        break;
                    }
                    if (*var_14 == 18446744073709551615UL) {
                        *var_14 = rbp_0;
                    } else {
                        var_17 = *(uint64_t *)(local_sp_1 + 16UL);
                        if (var_17 == 0UL) {
                            *(unsigned char *)(local_sp_1 + 15UL) = (unsigned char)'\x01';
                        } else {
                            var_18 = local_sp_1 + (-16L);
                            *(uint64_t *)var_18 = 4226852UL;
                            indirect_placeholder();
                            var_19 = (unsigned char *)(local_sp_1 + 7UL);
                            var_20 = ((uint64_t)(uint32_t)var_17 == 0UL) ? (uint64_t)*var_19 : 1UL;
                            *var_19 = (unsigned char)var_20;
                            rax_0 = var_20;
                            local_sp_0 = var_18;
                        }
                    }
                }
                var_21 = rbp_0 + 1UL;
                var_22 = *(uint64_t *)((var_21 << 3UL) + rsi);
                rax_1 = rax_0;
                rbx_0 = var_22;
                rbp_0 = var_21;
                local_sp_1 = local_sp_0;
                if (var_22 == 0UL) {
                    continue;
                }
                var_23 = *(unsigned char *)(local_sp_0 + 23UL);
                var_24 = (uint64_t *)local_sp_0;
                var_25 = (var_23 == '\x00') ? *var_24 : 18446744073709551614UL;
                *var_24 = var_25;
                var_26 = var_25;
                break;
            }
    }
    *(uint64_t *)var_8 = 18446744073709551615UL;
    return var_26;
}
