typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_18_ret_type;
struct indirect_placeholder_20_ret_type;
struct indirect_placeholder_24_ret_type;
struct indirect_placeholder_22_ret_type;
struct indirect_placeholder_23_ret_type;
struct indirect_placeholder_18_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_20_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_24_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_22_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_23_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_19(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r9(void);
extern uint32_t init_state_0x82fc(void);
extern void indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_9(uint64_t param_0);
extern uint64_t init_r8(void);
extern void indirect_placeholder_21(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_18_ret_type indirect_placeholder_18(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_20_ret_type indirect_placeholder_20(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_24_ret_type indirect_placeholder_24(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_22_ret_type indirect_placeholder_22(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_23_ret_type indirect_placeholder_23(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
typedef _Bool bool;
void bb_compare_files(uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint32_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t r13_0;
    uint64_t local_sp_1;
    uint64_t *_pre_phi;
    uint64_t local_sp_0;
    uint64_t var_120;
    uint64_t var_121;
    uint64_t var_122;
    uint64_t var_123;
    uint64_t var_124;
    uint64_t var_125;
    uint64_t var_126;
    uint64_t var_127;
    uint64_t var_111;
    uint64_t var_112;
    uint64_t *var_113;
    uint64_t var_114;
    uint64_t var_115;
    uint64_t var_116;
    uint64_t var_117;
    uint64_t var_118;
    uint64_t *var_119;
    uint64_t r8_6;
    uint64_t local_sp_13;
    uint64_t local_sp_12;
    uint64_t var_97;
    uint64_t rbp_0;
    uint64_t rax_5;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t r8_2;
    uint64_t local_sp_5;
    struct indirect_placeholder_20_ret_type var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t rax_9;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t rax_0;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t rbx_0;
    uint64_t local_sp_6;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t local_sp_2;
    uint64_t var_79;
    uint64_t r12_2;
    uint64_t *_pre_phi281;
    uint64_t rbx_2;
    uint64_t rax_1;
    uint64_t r9_7;
    uint64_t r9_4;
    uint64_t r12_0;
    uint64_t r8_7;
    uint64_t r8_4;
    uint64_t r9_0;
    uint64_t local_sp_14;
    uint64_t local_sp_9;
    uint64_t r8_0;
    uint64_t local_sp_3;
    uint64_t var_106;
    uint64_t rax_3;
    uint64_t r12_3;
    uint64_t r9_5;
    uint64_t rax_2;
    uint64_t r8_3;
    uint64_t r8_5;
    uint64_t r9_1;
    uint64_t local_sp_8;
    uint64_t local_sp_10;
    uint64_t r8_1;
    uint64_t local_sp_4;
    uint64_t r9_2;
    uint64_t local_sp_7;
    uint64_t rax_4;
    uint64_t *var_102;
    uint64_t rax_8;
    uint32_t *var_55;
    uint32_t var_56;
    uint32_t *var_57;
    uint32_t var_58;
    uint32_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t rax_6;
    uint64_t rbx_1;
    uint64_t local_sp_11;
    uint64_t rbp_1;
    uint64_t r9_3;
    uint64_t storemerge;
    uint64_t *var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t rax_7;
    uint64_t r12_1;
    uint64_t var_93;
    uint64_t r15_0;
    uint64_t *var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t r14_0;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t rbx_3;
    uint64_t rbp_2;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    struct indirect_placeholder_24_ret_type var_50;
    uint64_t var_51;
    uint64_t *var_98;
    uint64_t var_99;
    uint64_t var_100;
    uint64_t r9_6;
    uint64_t *var_101;
    uint64_t rax_10;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t *var_107;
    uint64_t var_108;
    uint64_t var_109;
    uint64_t var_110;
    uint64_t cc_src2_0;
    uint64_t r9_8;
    uint64_t r8_8;
    uint64_t local_sp_15;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t *_cast1;
    uint64_t var_17;
    uint64_t cc_src_0;
    uint64_t cc_dst_0;
    uint32_t cc_op_0;
    uint64_t rdi1_0;
    uint64_t rsi_0;
    uint64_t rcx_0;
    uint64_t cc_src_1;
    uint64_t cc_dst_1;
    uint64_t var_18;
    uint64_t var_19;
    uint32_t cc_op_1;
    uint64_t local_sp_17;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t rbx_4;
    uint64_t local_sp_16;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t *var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t *var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint32_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    struct indirect_placeholder_23_ret_type var_40;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t local_sp_18;
    uint64_t rbx_5;
    uint64_t rbp_3;
    uint64_t var_15;
    uint64_t var_16;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_cc_src2();
    var_8 = init_r9();
    var_9 = init_r8();
    var_10 = init_state_0x82fc();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_11 = var_0 + (-520L);
    *(uint64_t *)(var_0 + (-488L)) = rdi;
    var_12 = var_0 + (-152L);
    var_13 = var_0 + (-360L);
    var_14 = (uint64_t)var_10;
    r13_0 = 0UL;
    rax_9 = 0UL;
    rbx_0 = 0UL;
    r12_2 = 1UL;
    r12_3 = var_12;
    r9_5 = var_8;
    r8_5 = var_9;
    r15_0 = rdi;
    r14_0 = var_13;
    cc_src2_0 = var_7;
    r9_8 = var_8;
    r8_8 = var_9;
    cc_dst_0 = 0UL;
    cc_op_0 = 17U;
    rdi1_0 = 4262868UL;
    rcx_0 = 2UL;
    local_sp_17 = var_11;
    while (1U)
        {
            *(uint32_t *)(local_sp_17 + 40UL) = (uint32_t)r13_0;
            rbp_2 = r15_0;
            cc_src_0 = r12_3;
            local_sp_18 = local_sp_17;
            rbx_5 = r12_3 + (-96L);
            rbp_3 = (r13_0 << 5UL) + (local_sp_17 + 192UL);
            var_15 = local_sp_18 + (-8L);
            *(uint64_t *)var_15 = 4204909UL;
            indirect_placeholder_9(rbx_5);
            *(uint64_t *)rbp_3 = rbx_5;
            var_16 = rbx_5 + 24UL;
            local_sp_16 = var_15;
            rbx_5 = var_16;
            local_sp_18 = var_15;
            while ((var_16 - r12_3) != 0UL)
                {
                    rbp_3 = rbp_3 + 8UL;
                    var_15 = local_sp_18 + (-8L);
                    *(uint64_t *)var_15 = 4204909UL;
                    indirect_placeholder_9(rbx_5);
                    *(uint64_t *)rbp_3 = rbx_5;
                    var_16 = rbx_5 + 24UL;
                    local_sp_16 = var_15;
                    rbx_5 = var_16;
                    local_sp_18 = var_15;
                }
            *(uint32_t *)r14_0 = 0U;
            *(uint32_t *)(r14_0 + 4UL) = 0U;
            *(uint32_t *)(r14_0 + 8UL) = 0U;
            _cast1 = (uint64_t *)r15_0;
            var_17 = *_cast1;
            rsi_0 = var_17;
            cc_op_0 = 14U;
            cc_src_1 = cc_src_0;
            cc_dst_1 = cc_dst_0;
            cc_op_1 = cc_op_0;
            while (rcx_0 != 0UL)
                {
                    rdi1_0 = rdi1_0 + var_14;
                    rsi_0 = rsi_0 + var_14;
                    rcx_0 = rcx_0 + (-1L);
                    cc_op_0 = 14U;
                    cc_src_1 = cc_src_0;
                    cc_dst_1 = cc_dst_0;
                    cc_op_1 = cc_op_0;
                }
            var_20 = helper_cc_compute_all_wrapper(cc_dst_1, cc_src_1, cc_src2_0, cc_op_1);
            var_21 = ((var_20 & 65UL) == 0UL);
            var_22 = helper_cc_compute_c_wrapper(cc_dst_1, var_20, cc_src2_0, 1U);
            rbx_4 = *(uint64_t *)4275784UL;
            cc_src2_0 = var_22;
            if ((uint64_t)((unsigned char)var_21 - (unsigned char)var_22) == 0UL) {
                var_23 = local_sp_18 + (-16L);
                *(uint64_t *)var_23 = 4205151UL;
                var_24 = indirect_placeholder(var_17, 4256019UL);
                rbx_4 = var_24;
                local_sp_16 = var_23;
            }
            var_25 = r13_0 << 3UL;
            *(uint64_t *)((var_25 + local_sp_16) + 144UL) = rbx_4;
            if (rbx_4 != 0UL) {
                var_36 = *_cast1;
                *(uint64_t *)(local_sp_16 + (-8L)) = 4205178UL;
                var_37 = indirect_placeholder_19(var_36, 0UL, 3UL);
                *(uint64_t *)(local_sp_16 + (-16L)) = 4205186UL;
                indirect_placeholder_1();
                var_38 = (uint64_t)*(uint32_t *)var_37;
                var_39 = local_sp_16 + (-24L);
                *(uint64_t *)var_39 = 4205211UL;
                var_40 = indirect_placeholder_23(0UL, 4258005UL, 1UL, var_38, var_37, var_8, var_9);
                r9_8 = var_40.field_1;
                r8_8 = var_40.field_2;
                local_sp_15 = var_39;
                loop_state_var = 4U;
                break;
            }
            var_26 = local_sp_16 + (-8L);
            var_27 = (uint64_t *)var_26;
            *var_27 = 4205020UL;
            var_28 = indirect_placeholder_19(rbx_4, rbx_4, 2UL);
            var_29 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)4276808UL;
            var_30 = *(uint64_t *)((((uint64_t)*(uint32_t *)(local_sp_16 + 32UL) << 5UL) + var_26) + 192UL);
            var_31 = local_sp_16 + (-16L);
            var_32 = (uint64_t *)var_31;
            *var_32 = 4205052UL;
            var_33 = indirect_placeholder_19(var_29, var_30, var_28);
            *(uint64_t *)((var_25 + var_31) + 256UL) = var_33;
            var_34 = local_sp_16 + (-24L);
            *(uint64_t *)var_34 = 4205068UL;
            indirect_placeholder_1();
            var_35 = (uint32_t)var_33;
            rax_8 = var_33;
            local_sp_10 = var_34;
            local_sp_15 = var_34;
            local_sp_17 = var_34;
            if ((uint64_t)var_35 != 0UL) {
                loop_state_var = 4U;
                break;
            }
            if (r13_0 == 1UL) {
                r13_0 = r13_0 + 1UL;
                r15_0 = r15_0 + 8UL;
                r12_3 = r12_3 + 96UL;
                r14_0 = r14_0 + 12UL;
                continue;
            }
            *(uint32_t *)(local_sp_16 + 20UL) = var_35;
            *(uint64_t *)local_sp_16 = 0UL;
            *var_32 = 0UL;
            *var_27 = 0UL;
            var_87 = *(uint64_t *)(local_sp_10 + 256UL);
            r8_6 = r8_5;
            local_sp_13 = local_sp_10;
            local_sp_12 = local_sp_10;
            rax_9 = rax_8;
            rbx_2 = var_87;
            r9_4 = r9_5;
            r8_4 = r8_5;
            rax_2 = rax_8;
            r8_3 = r8_5;
            r9_1 = r9_5;
            r8_1 = r8_5;
            rax_6 = rax_8;
            rbx_1 = var_87;
            local_sp_11 = local_sp_10;
            r9_3 = r9_5;
            rax_7 = rax_8;
            rbx_3 = var_87;
            r9_6 = r9_5;
            if (var_87 != 0UL) {
                var_101 = (uint64_t *)(local_sp_13 + 264UL);
                _pre_phi281 = var_101;
                r9_7 = r9_6;
                r8_7 = r8_6;
                local_sp_14 = local_sp_13;
                rax_10 = rax_9;
                if (*var_101 != 0UL) {
                    loop_state_var = 5U;
                    break;
                }
                *(unsigned char *)(local_sp_13 + 112UL) = (unsigned char)'\x00';
                *(unsigned char *)4277055UL = (unsigned char)'\x01';
                loop_state_var = 2U;
                break;
            }
            *(unsigned char *)(local_sp_10 + 112UL) = (unsigned char)'\x00';
            *(unsigned char *)(local_sp_10 + 113UL) = (unsigned char)'\x00';
            var_88 = *(uint64_t *)(local_sp_10 + 264UL);
            rbp_1 = var_88;
            rbp_2 = var_88;
            if (var_88 != 0UL) {
                loop_state_var = 1U;
                break;
            }
            if (*(unsigned char *)4277059UL != '\x00') {
                loop_state_var = 3U;
                loop_state_var = 0U;
                break;
            }
            var_89 = *(uint64_t *)(var_88 + 8UL);
            var_90 = *(uint64_t *)(var_87 + 8UL);
            var_91 = local_sp_10 + (-8L);
            *(uint64_t *)var_91 = 4205860UL;
            indirect_placeholder_1();
            var_92 = (uint64_t)(uint32_t)rax_8;
            local_sp_8 = var_91;
            r12_1 = var_92;
            local_sp_9 = var_91;
            local_sp_12 = var_91;
            if (var_92 != 0UL) {
                if (var_89 <= var_90) {
                    loop_state_var = 1U;
                    break;
                }
                var_97 = helper_cc_compute_all_wrapper(var_89 - var_90, var_90, var_22, 17U);
                storemerge = ((var_97 >> 6UL) & 1UL) ^ 1UL;
                loop_state_var = 0U;
                loop_state_var = 0U;
                break;
            }
            *(unsigned char *)4277055UL = (unsigned char)'\x01';
            var_93 = helper_cc_compute_all_wrapper(r12_1, 0UL, 0UL, 24U);
            r12_2 = r12_1;
            rax_1 = rax_7;
            r9_7 = r9_4;
            r12_0 = r12_1;
            r8_7 = r8_4;
            r9_0 = r9_4;
            local_sp_14 = local_sp_9;
            r8_0 = r8_4;
            rax_2 = rax_7;
            r9_1 = r9_4;
            r8_1 = r8_4;
            rax_10 = rax_7;
            if ((uint64_t)(((unsigned char)(var_93 >> 4UL) ^ (unsigned char)var_93) & '\xc0') != 0UL) {
                _pre_phi281 = (uint64_t *)(local_sp_9 + 264UL);
                loop_state_var = 2U;
                break;
            }
            var_94 = (uint64_t *)(local_sp_9 + 16UL);
            *var_94 = (*var_94 + 1UL);
            var_95 = *(uint64_t *)4275776UL;
            var_96 = local_sp_9 + (-8L);
            *(uint64_t *)var_96 = 4205378UL;
            indirect_placeholder_10(1UL, rbx_2, var_95);
            local_sp_3 = var_96;
            local_sp_4 = var_96;
            if ((int)(uint32_t)r12_1 < (int)0U) {
                loop_state_var = 3U;
                break;
            }
            loop_state_var = 1U;
            loop_state_var = 0U;
            break;
        }
    switch (loop_state_var) {
      case 5U:
        {
            var_107 = (uint64_t *)(local_sp_13 + 144UL);
            var_108 = *var_107;
            var_109 = local_sp_13 + (-8L);
            *(uint64_t *)var_109 = 4205906UL;
            var_110 = indirect_placeholder_2(var_108);
            local_sp_1 = var_109;
            if ((uint64_t)(uint32_t)var_110 != 0UL) {
                var_115 = *(uint64_t *)(((uint64_t)*(uint32_t *)(local_sp_1 + 44UL) << 3UL) + *(uint64_t *)(local_sp_1 + 32UL));
                *(uint64_t *)(local_sp_1 + (-8L)) = 4205991UL;
                var_116 = indirect_placeholder_19(var_115, 0UL, 3UL);
                *(uint64_t *)(local_sp_1 + (-16L)) = 4205999UL;
                indirect_placeholder_1();
                var_117 = (uint64_t)*(uint32_t *)var_116;
                var_118 = local_sp_1 + (-24L);
                var_119 = (uint64_t *)var_118;
                *var_119 = 4206024UL;
                indirect_placeholder_18(0UL, 4258005UL, 1UL, var_117, var_116, r9_6, r8_6);
                _pre_phi = var_119;
                local_sp_0 = var_118;
                var_120 = (uint64_t)*(unsigned char *)4276808UL;
                var_121 = *(uint64_t *)4276800UL;
                var_122 = local_sp_0 + 112UL;
                var_123 = *(uint64_t *)(local_sp_0 + 24UL);
                *(uint64_t *)(local_sp_0 + (-8L)) = 4206054UL;
                indirect_placeholder(var_123, var_122);
                var_124 = local_sp_0 + 72UL;
                var_125 = *_pre_phi;
                *(uint64_t *)(local_sp_0 + (-16L)) = 4206079UL;
                indirect_placeholder(var_125, var_124);
                var_126 = local_sp_0 + 32UL;
                var_127 = *_pre_phi;
                *(uint64_t *)(local_sp_0 + (-24L)) = 4206104UL;
                indirect_placeholder(var_127, var_126);
                *(uint64_t *)(local_sp_0 + (-40L)) = var_120;
                *(uint64_t *)(local_sp_0 + (-48L)) = 4255897UL;
                *(uint64_t *)(local_sp_0 + (-56L)) = var_121;
                *(uint64_t *)(local_sp_0 + (-64L)) = 4206146UL;
                indirect_placeholder_1();
                return;
            }
            var_111 = *var_107;
            var_112 = local_sp_13 + (-16L);
            var_113 = (uint64_t *)var_112;
            *var_113 = 4205923UL;
            var_114 = indirect_placeholder_2(var_111);
            _pre_phi = var_113;
            local_sp_0 = var_112;
            local_sp_1 = var_112;
            if ((uint64_t)(uint32_t)var_114 == 0UL) {
                if (*(unsigned char *)4277052UL == '\x00') {
                    return;
                }
            }
            *(uint32_t *)(local_sp_13 + 28UL) = 1U;
            var_115 = *(uint64_t *)(((uint64_t)*(uint32_t *)(local_sp_1 + 44UL) << 3UL) + *(uint64_t *)(local_sp_1 + 32UL));
            *(uint64_t *)(local_sp_1 + (-8L)) = 4205991UL;
            var_116 = indirect_placeholder_19(var_115, 0UL, 3UL);
            *(uint64_t *)(local_sp_1 + (-16L)) = 4205999UL;
            indirect_placeholder_1();
            var_117 = (uint64_t)*(uint32_t *)var_116;
            var_118 = local_sp_1 + (-24L);
            var_119 = (uint64_t *)var_118;
            *var_119 = 4206024UL;
            indirect_placeholder_18(0UL, 4258005UL, 1UL, var_117, var_116, r9_6, r8_6);
            _pre_phi = var_119;
            local_sp_0 = var_118;
        }
        break;
      case 4U:
      case 3U:
      case 2U:
      case 1U:
      case 0U:
        {
            switch (loop_state_var) {
              case 0U:
                {
                    while (1U)
                        {
                            switch_state_var = 0;
                            switch (loop_state_var) {
                              case 3U:
                              case 0U:
                                {
                                    switch (loop_state_var) {
                                      case 3U:
                                        {
                                            var_45 = *(uint64_t *)(rbp_2 + 8UL) + (-1L);
                                            var_46 = *(uint64_t *)(rbp_2 + 16UL);
                                            var_47 = *(uint64_t *)(rbx_3 + 8UL) + (-1L);
                                            var_48 = *(uint64_t *)(rbx_3 + 16UL);
                                            var_49 = local_sp_11 + (-8L);
                                            *(uint64_t *)var_49 = 4205292UL;
                                            var_50 = indirect_placeholder_24(var_46, var_48, var_47, var_45);
                                            var_51 = var_50.field_0;
                                            r8_3 = var_50.field_2;
                                            local_sp_8 = var_49;
                                            rax_6 = var_51;
                                            rbx_1 = rbx_3;
                                            rbp_1 = rbp_2;
                                            r9_3 = var_50.field_1;
                                            storemerge = (uint64_t)(uint32_t)var_51;
                                        }
                                        break;
                                      case 0U:
                                        {
                                            rbx_2 = rbx_1;
                                            r9_4 = r9_3;
                                            r8_4 = r8_3;
                                            local_sp_9 = local_sp_8;
                                            rax_2 = rax_6;
                                            r9_1 = r9_3;
                                            r8_1 = r8_3;
                                            rax_7 = rax_6;
                                            r12_1 = storemerge;
                                            if ((uint64_t)(uint32_t)storemerge != 0UL) {
                                                *(unsigned char *)4277055UL = (unsigned char)'\x01';
                                                var_93 = helper_cc_compute_all_wrapper(r12_1, 0UL, 0UL, 24U);
                                                r12_2 = r12_1;
                                                rax_1 = rax_7;
                                                r9_7 = r9_4;
                                                r12_0 = r12_1;
                                                r8_7 = r8_4;
                                                r9_0 = r9_4;
                                                local_sp_14 = local_sp_9;
                                                r8_0 = r8_4;
                                                rax_2 = rax_7;
                                                r9_1 = r9_4;
                                                r8_1 = r8_4;
                                                rax_10 = rax_7;
                                                if ((uint64_t)(((unsigned char)(var_93 >> 4UL) ^ (unsigned char)var_93) & '\xc0') != 0UL) {
                                                    var_94 = (uint64_t *)(local_sp_9 + 16UL);
                                                    *var_94 = (*var_94 + 1UL);
                                                    var_95 = *(uint64_t *)4275776UL;
                                                    var_96 = local_sp_9 + (-8L);
                                                    *(uint64_t *)var_96 = 4205378UL;
                                                    indirect_placeholder_10(1UL, rbx_2, var_95);
                                                    local_sp_3 = var_96;
                                                    local_sp_4 = var_96;
                                                    if ((int)(uint32_t)r12_1 >= (int)0U) {
                                                        loop_state_var = 1U;
                                                        continue;
                                                    }
                                                    *(unsigned char *)(local_sp_3 + 113UL) = (unsigned char)'\x01';
                                                    var_106 = helper_cc_compute_all_wrapper(r12_0, 0UL, 0UL, 24U);
                                                    r8_2 = r8_0;
                                                    local_sp_5 = local_sp_3;
                                                    rax_3 = rax_1;
                                                    rax_2 = rax_1;
                                                    r9_1 = r9_0;
                                                    r8_1 = r8_0;
                                                    local_sp_4 = local_sp_3;
                                                    r9_2 = r9_0;
                                                    if ((uint64_t)(((unsigned char)(var_106 >> 4UL) ^ (unsigned char)var_106) & '\xc0') != 0UL) {
                                                        loop_state_var = 2U;
                                                        continue;
                                                    }
                                                    loop_state_var = 1U;
                                                    continue;
                                                }
                                                _pre_phi281 = (uint64_t *)(local_sp_9 + 264UL);
                                                var_102 = (uint64_t *)(local_sp_14 + 8UL);
                                                *var_102 = (*var_102 + 1UL);
                                                var_103 = *(uint64_t *)4275776UL;
                                                var_104 = *_pre_phi281;
                                                var_105 = local_sp_14 + (-8L);
                                                *(uint64_t *)var_105 = 4205709UL;
                                                indirect_placeholder_10(2UL, var_104, var_103);
                                                rax_1 = rax_10;
                                                r12_0 = r12_2;
                                                r9_0 = r9_7;
                                                r8_0 = r8_7;
                                                local_sp_3 = var_105;
                                                *(unsigned char *)(local_sp_3 + 113UL) = (unsigned char)'\x01';
                                                var_106 = helper_cc_compute_all_wrapper(r12_0, 0UL, 0UL, 24U);
                                                r8_2 = r8_0;
                                                local_sp_5 = local_sp_3;
                                                rax_3 = rax_1;
                                                rax_2 = rax_1;
                                                r9_1 = r9_0;
                                                r8_1 = r8_0;
                                                local_sp_4 = local_sp_3;
                                                r9_2 = r9_0;
                                                if ((uint64_t)(((unsigned char)(var_106 >> 4UL) ^ (unsigned char)var_106) & '\xc0') != 0UL) {
                                                    loop_state_var = 2U;
                                                    continue;
                                                }
                                                loop_state_var = 1U;
                                                continue;
                                            }
                                            var_52 = (uint64_t *)(local_sp_8 + 24UL);
                                            *var_52 = (*var_52 + 1UL);
                                            var_53 = *(uint64_t *)4275776UL;
                                            var_54 = local_sp_8 + (-8L);
                                            *(uint64_t *)var_54 = 4205326UL;
                                            indirect_placeholder_10(3UL, rbp_1, var_53);
                                            *(unsigned char *)(local_sp_8 + 105UL) = (unsigned char)'\x01';
                                            local_sp_4 = var_54;
                                            *(unsigned char *)(local_sp_4 + 112UL) = (unsigned char)'\x01';
                                            rax_3 = rax_2;
                                            r9_2 = r9_1;
                                            r8_2 = r8_1;
                                            local_sp_5 = local_sp_4;
                                        }
                                        break;
                                    }
                                }
                                break;
                              case 2U:
                              case 1U:
                                {
                                    switch_state_var = 0;
                                    switch (loop_state_var) {
                                      case 1U:
                                        {
                                            *(unsigned char *)(local_sp_4 + 112UL) = (unsigned char)'\x01';
                                            rax_3 = rax_2;
                                            r9_2 = r9_1;
                                            r8_2 = r8_1;
                                            local_sp_5 = local_sp_4;
                                        }
                                        break;
                                      case 2U:
                                        {
                                            rbp_0 = local_sp_5 + 160UL;
                                            local_sp_6 = local_sp_5;
                                            r9_5 = r9_2;
                                            r8_5 = r8_2;
                                            rax_4 = rax_3;
                                            while (1U)
                                                {
                                                    *(uint32_t *)(local_sp_6 + 40UL) = (uint32_t)rbx_0;
                                                    rax_5 = rax_4;
                                                    local_sp_7 = local_sp_6;
                                                    if (*(unsigned char *)((rbx_0 + local_sp_6) + 112UL) != '\x00') {
                                                        var_55 = (uint32_t *)(rbp_0 + 4UL);
                                                        var_56 = *var_55;
                                                        *(uint32_t *)(rbp_0 + 8UL) = var_56;
                                                        var_57 = (uint32_t *)rbp_0;
                                                        var_58 = *var_57;
                                                        *var_55 = var_58;
                                                        var_59 = (var_58 + 1U) & 3U;
                                                        var_60 = (uint64_t)var_59;
                                                        *var_57 = var_59;
                                                        var_61 = rbx_0 << 3UL;
                                                        var_62 = *(uint64_t *)((var_61 + local_sp_6) + 144UL);
                                                        var_63 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)4276808UL;
                                                        var_64 = rbx_0 << 32UL;
                                                        var_65 = (uint64_t)((long)var_64 >> (long)30UL);
                                                        var_66 = *(uint64_t *)((((var_65 | var_60) << 3UL) + local_sp_6) + 192UL);
                                                        var_67 = local_sp_6 + (-8L);
                                                        *(uint64_t *)var_67 = 4205458UL;
                                                        var_68 = indirect_placeholder_19(var_63, var_66, var_62);
                                                        *(uint64_t *)((var_61 + var_67) + 256UL) = var_68;
                                                        local_sp_2 = var_67;
                                                        if (var_68 == 0UL) {
                                                            var_69 = (uint64_t)(*(uint32_t *)(local_sp_6 + 32UL) + 1U);
                                                            var_70 = var_65 + (uint64_t)var_58;
                                                            var_71 = *(uint64_t *)(((var_70 << 3UL) + var_67) + 192UL);
                                                            var_72 = local_sp_6 + (-16L);
                                                            *(uint64_t *)var_72 = 4205505UL;
                                                            indirect_placeholder_21(var_69, rbx_0, var_71, rbp_0, var_68);
                                                            rax_0 = var_70;
                                                            local_sp_2 = var_72;
                                                        } else {
                                                            var_73 = var_65 + (uint64_t)var_56;
                                                            var_74 = *(uint64_t *)(((var_73 << 3UL) + var_67) + 192UL);
                                                            rax_0 = var_73;
                                                            if (*(uint64_t *)(var_74 + 16UL) == 0UL) {
                                                                var_75 = (uint64_t)(*(uint32_t *)(local_sp_6 + 32UL) + 1U);
                                                                var_76 = var_65 + (uint64_t)var_58;
                                                                var_77 = *(uint64_t *)(((var_76 << 3UL) + var_67) + 192UL);
                                                                var_78 = local_sp_6 + (-16L);
                                                                *(uint64_t *)var_78 = 4205583UL;
                                                                indirect_placeholder_21(var_75, rbx_0, var_74, rbp_0, var_77);
                                                                rax_0 = var_76;
                                                                local_sp_2 = var_78;
                                                            }
                                                        }
                                                        var_79 = local_sp_2 + (-8L);
                                                        *(uint64_t *)var_79 = 4205513UL;
                                                        indirect_placeholder_1();
                                                        rax_5 = rax_0;
                                                        local_sp_7 = var_79;
                                                        if ((uint64_t)(uint32_t)rax_0 != 0UL) {
                                                            loop_state_var = 0U;
                                                            break;
                                                        }
                                                        *(unsigned char *)((rbx_0 + var_79) + 112UL) = (unsigned char)'\x00';
                                                    }
                                                    rax_4 = rax_5;
                                                    local_sp_6 = local_sp_7;
                                                    rax_8 = rax_5;
                                                    local_sp_10 = local_sp_7;
                                                    if (rbx_0 != 1UL) {
                                                        loop_state_var = 1U;
                                                        break;
                                                    }
                                                    rbx_0 = rbx_0 + 1UL;
                                                    rbp_0 = rbp_0 + 12UL;
                                                    continue;
                                                }
                                            switch_state_var = 0;
                                            switch (loop_state_var) {
                                              case 0U:
                                                {
                                                    var_80 = *(uint64_t *)((uint64_t)((long)var_64 >> (long)29UL) + *(uint64_t *)(local_sp_2 + 24UL));
                                                    *(uint64_t *)(local_sp_2 + (-16L)) = 4205612UL;
                                                    var_81 = indirect_placeholder_19(var_80, 0UL, 3UL);
                                                    *(uint64_t *)(local_sp_2 + (-24L)) = 4205620UL;
                                                    indirect_placeholder_1();
                                                    var_82 = (uint64_t)*(uint32_t *)var_81;
                                                    var_83 = local_sp_2 + (-32L);
                                                    *(uint64_t *)var_83 = 4205645UL;
                                                    var_84 = indirect_placeholder_20(0UL, 4258005UL, 1UL, var_82, var_81, r9_2, r8_2);
                                                    var_85 = var_84.field_1;
                                                    var_86 = var_84.field_2;
                                                    r9_6 = var_85;
                                                    r8_6 = var_86;
                                                    local_sp_13 = var_83;
                                                    var_101 = (uint64_t *)(local_sp_13 + 264UL);
                                                    _pre_phi281 = var_101;
                                                    r9_7 = r9_6;
                                                    r8_7 = r8_6;
                                                    local_sp_14 = local_sp_13;
                                                    rax_10 = rax_9;
                                                    if (*var_101 == 0UL) {
                                                        switch_state_var = 1;
                                                        break;
                                                    }
                                                    *(unsigned char *)(local_sp_13 + 112UL) = (unsigned char)'\x00';
                                                    *(unsigned char *)4277055UL = (unsigned char)'\x01';
                                                }
                                                break;
                                              case 1U:
                                                {
                                                    var_87 = *(uint64_t *)(local_sp_10 + 256UL);
                                                    r8_6 = r8_5;
                                                    local_sp_13 = local_sp_10;
                                                    local_sp_12 = local_sp_10;
                                                    rax_9 = rax_8;
                                                    rbx_2 = var_87;
                                                    r9_4 = r9_5;
                                                    r8_4 = r8_5;
                                                    rax_2 = rax_8;
                                                    r8_3 = r8_5;
                                                    r9_1 = r9_5;
                                                    r8_1 = r8_5;
                                                    rax_6 = rax_8;
                                                    rbx_1 = var_87;
                                                    local_sp_11 = local_sp_10;
                                                    r9_3 = r9_5;
                                                    rax_7 = rax_8;
                                                    rbx_3 = var_87;
                                                    r9_6 = r9_5;
                                                    if (var_87 == 0UL) {
                                                        var_101 = (uint64_t *)(local_sp_13 + 264UL);
                                                        _pre_phi281 = var_101;
                                                        r9_7 = r9_6;
                                                        r8_7 = r8_6;
                                                        local_sp_14 = local_sp_13;
                                                        rax_10 = rax_9;
                                                        if (*var_101 != 0UL) {
                                                            switch_state_var = 1;
                                                            break;
                                                        }
                                                        *(unsigned char *)(local_sp_13 + 112UL) = (unsigned char)'\x00';
                                                        *(unsigned char *)4277055UL = (unsigned char)'\x01';
                                                    } else {
                                                        *(unsigned char *)(local_sp_10 + 112UL) = (unsigned char)'\x00';
                                                        *(unsigned char *)(local_sp_10 + 113UL) = (unsigned char)'\x00';
                                                        var_88 = *(uint64_t *)(local_sp_10 + 264UL);
                                                        rbp_1 = var_88;
                                                        rbp_2 = var_88;
                                                        if (var_88 != 0UL) {
                                                            *(unsigned char *)4277055UL = (unsigned char)'\x01';
                                                            var_98 = (uint64_t *)(local_sp_12 + 16UL);
                                                            *var_98 = (*var_98 + 1UL);
                                                            var_99 = *(uint64_t *)4275776UL;
                                                            var_100 = local_sp_12 + (-8L);
                                                            *(uint64_t *)var_100 = 4206188UL;
                                                            indirect_placeholder_10(1UL, var_87, var_99);
                                                            local_sp_4 = var_100;
                                                            loop_state_var = 1U;
                                                            continue;
                                                        }
                                                        if (*(unsigned char *)4277059UL != '\x00') {
                                                            loop_state_var = 3U;
                                                            continue;
                                                        }
                                                        var_89 = *(uint64_t *)(var_88 + 8UL);
                                                        var_90 = *(uint64_t *)(var_87 + 8UL);
                                                        var_91 = local_sp_10 + (-8L);
                                                        *(uint64_t *)var_91 = 4205860UL;
                                                        indirect_placeholder_1();
                                                        var_92 = (uint64_t)(uint32_t)rax_8;
                                                        local_sp_8 = var_91;
                                                        r12_1 = var_92;
                                                        local_sp_9 = var_91;
                                                        local_sp_12 = var_91;
                                                        if (var_92 != 0UL) {
                                                            if (var_89 <= var_90) {
                                                                var_97 = helper_cc_compute_all_wrapper(var_89 - var_90, var_90, var_22, 17U);
                                                                storemerge = ((var_97 >> 6UL) & 1UL) ^ 1UL;
                                                                loop_state_var = 0U;
                                                                continue;
                                                            }
                                                            *(unsigned char *)4277055UL = (unsigned char)'\x01';
                                                            var_98 = (uint64_t *)(local_sp_12 + 16UL);
                                                            *var_98 = (*var_98 + 1UL);
                                                            var_99 = *(uint64_t *)4275776UL;
                                                            var_100 = local_sp_12 + (-8L);
                                                            *(uint64_t *)var_100 = 4206188UL;
                                                            indirect_placeholder_10(1UL, var_87, var_99);
                                                            local_sp_4 = var_100;
                                                            loop_state_var = 1U;
                                                            continue;
                                                        }
                                                        *(unsigned char *)4277055UL = (unsigned char)'\x01';
                                                        var_93 = helper_cc_compute_all_wrapper(r12_1, 0UL, 0UL, 24U);
                                                        r12_2 = r12_1;
                                                        rax_1 = rax_7;
                                                        r9_7 = r9_4;
                                                        r12_0 = r12_1;
                                                        r8_7 = r8_4;
                                                        r9_0 = r9_4;
                                                        local_sp_14 = local_sp_9;
                                                        r8_0 = r8_4;
                                                        rax_2 = rax_7;
                                                        r9_1 = r9_4;
                                                        r8_1 = r8_4;
                                                        rax_10 = rax_7;
                                                        if ((uint64_t)(((unsigned char)(var_93 >> 4UL) ^ (unsigned char)var_93) & '\xc0') != 0UL) {
                                                            var_94 = (uint64_t *)(local_sp_9 + 16UL);
                                                            *var_94 = (*var_94 + 1UL);
                                                            var_95 = *(uint64_t *)4275776UL;
                                                            var_96 = local_sp_9 + (-8L);
                                                            *(uint64_t *)var_96 = 4205378UL;
                                                            indirect_placeholder_10(1UL, rbx_2, var_95);
                                                            local_sp_3 = var_96;
                                                            local_sp_4 = var_96;
                                                            if ((int)(uint32_t)r12_1 >= (int)0U) {
                                                                loop_state_var = 1U;
                                                                continue;
                                                            }
                                                            *(unsigned char *)(local_sp_3 + 113UL) = (unsigned char)'\x01';
                                                            var_106 = helper_cc_compute_all_wrapper(r12_0, 0UL, 0UL, 24U);
                                                            r8_2 = r8_0;
                                                            local_sp_5 = local_sp_3;
                                                            rax_3 = rax_1;
                                                            rax_2 = rax_1;
                                                            r9_1 = r9_0;
                                                            r8_1 = r8_0;
                                                            local_sp_4 = local_sp_3;
                                                            r9_2 = r9_0;
                                                            if ((uint64_t)(((unsigned char)(var_106 >> 4UL) ^ (unsigned char)var_106) & '\xc0') != 0UL) {
                                                                loop_state_var = 2U;
                                                                continue;
                                                            }
                                                            loop_state_var = 1U;
                                                            continue;
                                                        }
                                                        _pre_phi281 = (uint64_t *)(local_sp_9 + 264UL);
                                                    }
                                                }
                                                break;
                                            }
                                            if (switch_state_var)
                                                break;
                                            var_102 = (uint64_t *)(local_sp_14 + 8UL);
                                            *var_102 = (*var_102 + 1UL);
                                            var_103 = *(uint64_t *)4275776UL;
                                            var_104 = *_pre_phi281;
                                            var_105 = local_sp_14 + (-8L);
                                            *(uint64_t *)var_105 = 4205709UL;
                                            indirect_placeholder_10(2UL, var_104, var_103);
                                            rax_1 = rax_10;
                                            r12_0 = r12_2;
                                            r9_0 = r9_7;
                                            r8_0 = r8_7;
                                            local_sp_3 = var_105;
                                            *(unsigned char *)(local_sp_3 + 113UL) = (unsigned char)'\x01';
                                            var_106 = helper_cc_compute_all_wrapper(r12_0, 0UL, 0UL, 24U);
                                            r8_2 = r8_0;
                                            local_sp_5 = local_sp_3;
                                            rax_3 = rax_1;
                                            rax_2 = rax_1;
                                            r9_1 = r9_0;
                                            r8_1 = r8_0;
                                            local_sp_4 = local_sp_3;
                                            r9_2 = r9_0;
                                            if ((uint64_t)(((unsigned char)(var_106 >> 4UL) ^ (unsigned char)var_106) & '\xc0') != 0UL) {
                                                loop_state_var = 1U;
                                                continue;
                                            }
                                            loop_state_var = 2U;
                                            continue;
                                        }
                                        break;
                                    }
                                    if (switch_state_var)
                                        break;
                                }
                                break;
                            }
                            if (switch_state_var)
                                break;
                        }
                }
                break;
              case 3U:
              case 2U:
                {
                    switch (loop_state_var) {
                      case 2U:
                        {
                            var_102 = (uint64_t *)(local_sp_14 + 8UL);
                            *var_102 = (*var_102 + 1UL);
                            var_103 = *(uint64_t *)4275776UL;
                            var_104 = *_pre_phi281;
                            var_105 = local_sp_14 + (-8L);
                            *(uint64_t *)var_105 = 4205709UL;
                            indirect_placeholder_10(2UL, var_104, var_103);
                            rax_1 = rax_10;
                            r12_0 = r12_2;
                            r9_0 = r9_7;
                            r8_0 = r8_7;
                            local_sp_3 = var_105;
                        }
                        break;
                      case 3U:
                        {
                            *(unsigned char *)(local_sp_3 + 113UL) = (unsigned char)'\x01';
                            var_106 = helper_cc_compute_all_wrapper(r12_0, 0UL, 0UL, 24U);
                            r8_2 = r8_0;
                            local_sp_5 = local_sp_3;
                            rax_3 = rax_1;
                            rax_2 = rax_1;
                            r9_1 = r9_0;
                            r8_1 = r8_0;
                            local_sp_4 = local_sp_3;
                            r9_2 = r9_0;
                            if ((uint64_t)(((unsigned char)(var_106 >> 4UL) ^ (unsigned char)var_106) & '\xc0') == 0UL) {
                                loop_state_var = 2U;
                            } else {
                                loop_state_var = 1U;
                            }
                        }
                        break;
                    }
                }
                break;
              case 1U:
                {
                    *(unsigned char *)4277055UL = (unsigned char)'\x01';
                    var_98 = (uint64_t *)(local_sp_12 + 16UL);
                    *var_98 = (*var_98 + 1UL);
                    var_99 = *(uint64_t *)4275776UL;
                    var_100 = local_sp_12 + (-8L);
                    *(uint64_t *)var_100 = 4206188UL;
                    indirect_placeholder_10(1UL, var_87, var_99);
                    local_sp_4 = var_100;
                    loop_state_var = 1U;
                }
                break;
              case 4U:
                {
                    var_41 = *_cast1;
                    *(uint64_t *)(local_sp_15 + (-8L)) = 4205230UL;
                    var_42 = indirect_placeholder_19(var_41, 0UL, 3UL);
                    *(uint64_t *)(local_sp_15 + (-16L)) = 4205238UL;
                    indirect_placeholder_1();
                    var_43 = (uint64_t)*(uint32_t *)var_42;
                    var_44 = local_sp_15 + (-24L);
                    *(uint64_t *)var_44 = 4205263UL;
                    indirect_placeholder_22(0UL, 4258005UL, 1UL, var_43, var_42, r9_8, r8_8);
                    rbx_3 = var_42;
                    local_sp_11 = var_44;
                    loop_state_var = 3U;
                }
                break;
            }
        }
        break;
    }
}
