typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0);
uint64_t bb_next_field(uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t *_cast;
    uint64_t var_5;
    uint32_t var_6;
    bool var_7;
    unsigned char var_8;
    unsigned char rdi1_0_in;
    uint64_t var_18;
    unsigned char var_19;
    uint64_t rbx_3;
    uint64_t local_sp_0;
    unsigned char rdi1_1_in;
    uint64_t var_13;
    unsigned char var_14;
    uint64_t local_sp_1;
    uint64_t rdi1_1;
    uint64_t var_11;
    unsigned char var_15;
    uint64_t rbx_0;
    uint64_t rdi1_0;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t rbx_2;
    uint64_t rbx_1;
    uint64_t var_9;
    unsigned char var_10;
    uint64_t var_12;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r12();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_4 = var_0 + (-24L);
    *(uint64_t *)var_4 = var_1;
    _cast = (uint64_t *)rdi;
    var_5 = *_cast;
    var_6 = *(uint32_t *)4305572UL;
    var_7 = ((uint64_t)(var_6 + (-128)) == 0UL);
    var_8 = *(unsigned char *)var_5;
    rbx_3 = var_5;
    rdi1_1_in = var_8;
    local_sp_1 = var_4;
    rbx_2 = var_5;
    rbx_1 = var_5;
    if (!var_7) {
        if (((uint64_t)(var_6 - (uint32_t)(uint64_t)var_8) == 0UL) || (var_8 == '\x00')) {
            var_9 = rbx_1 + 1UL;
            var_10 = *(unsigned char *)var_9;
            rbx_1 = var_9;
            rbx_3 = var_9;
            while (var_10 != '\x00')
                {
                    if ((uint64_t)(var_6 - (uint32_t)(uint64_t)var_10) == 0UL) {
                        break;
                    }
                    var_9 = rbx_1 + 1UL;
                    var_10 = *(unsigned char *)var_9;
                    rbx_1 = var_9;
                    rbx_3 = var_9;
                }
        }
    }
    if (var_8 != '\x00') {
        *_cast = rbx_3;
        return var_5;
    }
    while (1U)
        {
            rdi1_1 = (uint64_t)rdi1_1_in;
            var_11 = local_sp_1 + (-8L);
            *(uint64_t *)var_11 = 4206675UL;
            var_12 = indirect_placeholder_3(rdi1_1);
            local_sp_0 = var_11;
            rbx_0 = rbx_2;
            local_sp_1 = var_11;
            rbx_3 = rbx_2;
            if ((uint64_t)(unsigned char)var_12 == 0UL) {
                var_13 = rbx_2 + 1UL;
                var_14 = *(unsigned char *)var_13;
                rbx_2 = var_13;
                rdi1_1_in = var_14;
                rbx_3 = var_13;
                if (var_14 != '\x00') {
                    continue;
                }
                break;
            }
            var_15 = *(unsigned char *)rbx_2;
            rdi1_0_in = var_15;
            if (var_15 == '\x00') {
                break;
            }
            rdi1_0 = (uint64_t)rdi1_0_in;
            var_16 = local_sp_0 + (-8L);
            *(uint64_t *)var_16 = 4206710UL;
            var_17 = indirect_placeholder_3(rdi1_0);
            local_sp_0 = var_16;
            rbx_3 = rbx_0;
            while ((uint64_t)(unsigned char)var_17 != 0UL)
                {
                    var_18 = rbx_0 + 1UL;
                    var_19 = *(unsigned char *)var_18;
                    rbx_0 = var_18;
                    rdi1_0_in = var_19;
                    rbx_3 = var_18;
                    if (var_19 == '\x00') {
                        break;
                    }
                    rdi1_0 = (uint64_t)rdi1_0_in;
                    var_16 = local_sp_0 + (-8L);
                    *(uint64_t *)var_16 = 4206710UL;
                    var_17 = indirect_placeholder_3(rdi1_0);
                    local_sp_0 = var_16;
                    rbx_3 = rbx_0;
                }
            break;
        }
    *_cast = rbx_3;
    return var_5;
}
