typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_fpop_wrapper_ret_type;
struct type_6;
struct helper_fmov_STN_ST0_wrapper_ret_type;
struct indirect_placeholder_73_ret_type;
struct indirect_placeholder_74_ret_type;
struct indirect_placeholder_75_ret_type;
struct indirect_placeholder_76_ret_type;
struct indirect_placeholder_77_ret_type;
struct indirect_placeholder_78_ret_type;
struct helper_fpop_wrapper_ret_type {
    uint32_t field_0;
    unsigned char field_1;
    unsigned char field_2;
    unsigned char field_3;
    unsigned char field_4;
    unsigned char field_5;
    unsigned char field_6;
    unsigned char field_7;
    unsigned char field_8;
};
struct type_6 {
};
struct helper_fmov_STN_ST0_wrapper_ret_type {
    uint16_t field_0;
    uint16_t field_1;
    uint16_t field_2;
    uint16_t field_3;
    uint16_t field_4;
    uint16_t field_5;
    uint16_t field_6;
    uint16_t field_7;
    uint64_t field_8;
    uint64_t field_9;
    uint64_t field_10;
    uint64_t field_11;
    uint64_t field_12;
    uint64_t field_13;
    uint64_t field_14;
    uint64_t field_15;
};
struct indirect_placeholder_73_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_74_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_75_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_76_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_77_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_78_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbx(void);
extern uint32_t init_state_0x82fc(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_32(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern void indirect_placeholder_37(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1);
extern uint16_t init_state_0x8498(void);
extern uint16_t init_state_0x84a8(void);
extern uint64_t init_state_0x84b0(void);
extern uint16_t init_state_0x84b8(void);
extern uint16_t init_state_0x84c8(void);
extern uint16_t init_state_0x84d8(void);
extern uint16_t init_state_0x84e8(void);
extern uint16_t init_state_0x84f8(void);
extern uint16_t init_state_0x8508(void);
extern uint64_t init_state_0x8490(void);
extern uint64_t init_state_0x84a0(void);
extern uint64_t init_state_0x84c0(void);
extern uint64_t init_state_0x84d0(void);
extern uint64_t init_state_0x84e0(void);
extern uint64_t init_state_0x84f0(void);
extern uint64_t init_state_0x8500(void);
extern uint32_t init_state_0x8480(void);
extern struct helper_fpop_wrapper_ret_type helper_fpop_wrapper(struct type_6 *param_0, uint32_t param_1);
extern struct helper_fmov_STN_ST0_wrapper_ret_type helper_fmov_STN_ST0_wrapper(struct type_6 *param_0, uint32_t param_1, uint16_t param_2, uint16_t param_3, uint16_t param_4, uint16_t param_5, uint16_t param_6, uint16_t param_7, uint16_t param_8, uint16_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint64_t param_13, uint64_t param_14, uint64_t param_15, uint64_t param_16, uint64_t param_17, uint32_t param_18);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern struct indirect_placeholder_73_ret_type indirect_placeholder_73(uint64_t param_0);
extern struct indirect_placeholder_74_ret_type indirect_placeholder_74(uint64_t param_0);
extern struct indirect_placeholder_75_ret_type indirect_placeholder_75(uint64_t param_0);
extern struct indirect_placeholder_76_ret_type indirect_placeholder_76(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_77_ret_type indirect_placeholder_77(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_78_ret_type indirect_placeholder_78(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
uint64_t bb_prepare_padded_number(uint64_t rax, uint64_t rdi) {
    uint64_t rcx_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint32_t var_5;
    uint16_t var_6;
    uint16_t var_7;
    uint16_t var_8;
    uint16_t var_9;
    uint16_t var_10;
    uint16_t var_11;
    uint16_t var_12;
    uint16_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint32_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t *var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t *var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t *var_31;
    uint64_t var_32;
    uint32_t var_33;
    uint64_t var_34;
    uint32_t *_pre_phi96;
    uint64_t var_53;
    uint64_t rax1_1;
    uint64_t rdi2_0;
    uint64_t local_sp_0;
    uint64_t local_sp_1;
    uint64_t var_44;
    uint64_t var_46;
    uint64_t _pre_phi100;
    uint64_t var_45;
    uint64_t rcx_2;
    uint64_t rdi2_1;
    uint64_t rcx_3;
    unsigned char var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t _pre_phi98;
    uint64_t rcx_4;
    uint64_t rdi2_2;
    uint64_t rcx_5;
    unsigned char var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint32_t *var_35;
    uint64_t var_36;
    bool var_58;
    uint64_t var_59;
    uint64_t *var_60;
    uint64_t rax1_0;
    uint64_t var_57;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_cc_src2();
    var_3 = init_r9();
    var_4 = init_r8();
    var_5 = init_state_0x82fc();
    var_6 = init_state_0x8498();
    var_7 = init_state_0x84a8();
    var_8 = init_state_0x84b8();
    var_9 = init_state_0x84c8();
    var_10 = init_state_0x84d8();
    var_11 = init_state_0x84e8();
    var_12 = init_state_0x84f8();
    var_13 = init_state_0x8508();
    var_14 = init_state_0x8490();
    var_15 = init_state_0x84a0();
    var_16 = init_state_0x84b0();
    var_17 = init_state_0x84c0();
    var_18 = init_state_0x84d0();
    var_19 = init_state_0x84e0();
    var_20 = init_state_0x84f0();
    var_21 = init_state_0x8500();
    var_22 = init_state_0x8480();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_23 = *(uint64_t *)4305584UL;
    var_24 = (var_23 == 18446744073709551615UL) ? rdi : var_23;
    *(uint64_t *)(var_0 + (-160L)) = *(uint64_t *)(var_0 | 16UL);
    var_25 = (uint64_t *)(var_0 | 8UL);
    var_26 = *var_25;
    var_27 = var_0 + (-168L);
    var_28 = (uint64_t *)var_27;
    *var_28 = var_26;
    var_29 = var_0 + (-140L);
    var_30 = var_0 + (-176L);
    var_31 = (uint64_t *)var_30;
    *var_31 = 4210586UL;
    var_32 = indirect_placeholder_2(10UL, var_29);
    helper_fmov_STN_ST0_wrapper((struct type_6 *)(0UL), 0U, var_6, var_7, var_8, var_9, var_10, var_11, var_12, var_13, var_14, var_15, var_16, var_17, var_18, var_19, var_20, var_21, var_22);
    helper_fpop_wrapper((struct type_6 *)(0UL), var_22);
    var_33 = *(uint32_t *)4305988UL;
    var_34 = (uint64_t)var_33;
    rcx_0 = 18446744073709551615UL;
    rax1_1 = 0UL;
    local_sp_0 = var_27;
    rcx_2 = 18446744073709551615UL;
    rcx_3 = 0UL;
    rcx_4 = 18446744073709551615UL;
    rcx_5 = 0UL;
    rax1_0 = var_32;
    if (var_33 == 0U) {
        var_35 = (uint32_t *)(var_0 + (-148L));
        var_36 = var_24 + (uint64_t)*var_35;
        _pre_phi96 = var_35;
        rax1_0 = var_36;
        if (var_36 <= 18UL) {
            if (*(uint32_t *)4305984UL != 3U) {
                var_58 = (var_24 == 0UL);
                *var_28 = *var_25;
                *var_31 = *(uint64_t *)var_0;
                var_59 = (uint64_t)*(uint32_t *)4305580UL;
                var_60 = (uint64_t *)(var_0 + (-184L));
                if (var_58) {
                    *var_60 = 4210808UL;
                    indirect_placeholder_77(0UL, 4271936UL, var_59, 0UL, var_34, var_3, var_4);
                } else {
                    *var_60 = 4210754UL;
                    indirect_placeholder_76(0UL, 4271856UL, var_59, 0UL, var_24, var_3, var_4);
                }
            }
            return rax1_1;
        }
    }
    _pre_phi96 = (uint32_t *)(var_0 + (-148L));
    rax1_1 = 1UL;
    if (*_pre_phi96 > 26U) {
        if (*(uint32_t *)4305984UL == 3U) {
            *var_28 = *var_25;
            *var_31 = *(uint64_t *)var_0;
            var_57 = (uint64_t)*(uint32_t *)4305580UL;
            *(uint64_t *)(var_0 + (-184L)) = 4210679UL;
            indirect_placeholder_78(0UL, 4272000UL, var_57, 0UL, var_34, var_3, var_4);
        }
    } else {
        *var_28 = *var_25;
        *var_31 = *(uint64_t *)var_0;
        var_37 = (uint64_t)*(uint32_t *)4305608UL;
        var_38 = (uint64_t)*(uint32_t *)4305968UL;
        var_39 = var_0 + (-144L);
        var_40 = (uint64_t)(uint32_t)var_24;
        *(uint64_t *)(var_0 + (-184L)) = 4210867UL;
        indirect_placeholder_37(rax1_0, 128UL, var_40, var_39, var_34, var_37, var_38);
        if (*(uint64_t *)4305976UL != 0UL) {
            rdi2_0 = var_0 + (-152L);
            local_sp_0 = var_30;
            while (rcx_0 != 0UL)
                {
                    if (*(unsigned char *)rdi2_0 == '\x00') {
                        break;
                    }
                    rcx_0 = rcx_0 + (-1L);
                    rdi2_0 = rdi2_0 + (uint64_t)var_5;
                }
            *var_31 = 4210920UL;
            indirect_placeholder_1();
        }
        local_sp_1 = local_sp_0;
        if (*(unsigned char *)4305872UL == '\x00') {
            var_41 = local_sp_0 + 16UL;
            *(uint64_t *)(local_sp_0 + (-8L)) = 4211047UL;
            indirect_placeholder_75(var_41);
            var_42 = *(uint64_t *)(local_sp_0 + 160UL);
            var_43 = local_sp_0 + (-16L);
            *(uint64_t *)var_43 = var_42;
            *(uint64_t *)(local_sp_0 + (-24L)) = *(uint64_t *)(local_sp_0 + 152UL);
            *(uint64_t *)(local_sp_0 + (-32L)) = 4211086UL;
            indirect_placeholder_1();
            local_sp_1 = var_43;
        }
        var_44 = *(uint64_t *)4305944UL;
        if (var_44 != 0UL) {
            var_45 = local_sp_1 + 16UL;
            var_46 = (uint64_t)var_5;
            rdi2_1 = var_45;
            _pre_phi100 = var_46;
            _pre_phi98 = var_45;
            while (rcx_2 != 0UL)
                {
                    var_47 = *(unsigned char *)rdi2_1;
                    var_48 = rcx_2 + (-1L);
                    rcx_2 = var_48;
                    rcx_3 = var_48;
                    if (var_47 == '\x00') {
                        break;
                    }
                    rdi2_1 = rdi2_1 + var_46;
                }
            var_49 = helper_cc_compute_c_wrapper((18446744073709551614UL - (-2L)) - var_44, var_44, var_2, 17U);
            if (var_49 != 0UL) {
                *(uint64_t *)local_sp_1 = var_44;
                var_50 = (uint64_t)*(uint32_t *)4305576UL;
                var_51 = *(uint64_t *)4305952UL;
                var_52 = *(uint64_t *)4305960UL;
                *(uint64_t *)(local_sp_1 + (-8L)) = 4211139UL;
                indirect_placeholder_32(var_51, var_45, var_52, local_sp_1, 2UL, var_50);
                if (*(unsigned char *)4305872UL != '\x00') {
                    var_53 = *(uint64_t *)4305960UL;
                    *(uint64_t *)(local_sp_1 + (-16L)) = 4211167UL;
                    indirect_placeholder_73(var_53);
                    *(uint64_t *)(local_sp_1 + (-24L)) = 4211192UL;
                    indirect_placeholder_1();
                }
                return rax1_1;
            }
        }
        _pre_phi100 = (uint64_t)var_5;
        _pre_phi98 = local_sp_1 + 16UL;
    }
}
