typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_40_ret_type;
struct indirect_placeholder_41_ret_type;
struct indirect_placeholder_40_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_41_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern struct indirect_placeholder_40_ret_type indirect_placeholder_40(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_41_ret_type indirect_placeholder_41(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
uint64_t bb_restricted_chown(uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t rcx, uint64_t r9, uint64_t r8) {
    struct indirect_placeholder_41_ret_type var_14;
    struct indirect_placeholder_40_ret_type var_23;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint32_t var_8;
    uint32_t var_9;
    uint64_t r15_1;
    uint64_t var_35;
    uint64_t var_36;
    uint32_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_24;
    uint32_t var_25;
    uint64_t rax_0;
    uint64_t local_sp_0;
    uint64_t var_26;
    uint64_t var_28;
    uint64_t r15_0;
    uint64_t var_29;
    uint64_t rax_1;
    uint32_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t local_sp_2;
    uint64_t rax_2;
    uint64_t local_sp_1;
    uint32_t *var_33;
    uint32_t var_34;
    uint64_t storemerge;
    uint32_t _pre_phi;
    uint64_t rax_3;
    uint64_t var_27;
    uint64_t var_18;
    uint64_t var_10;
    uint32_t *var_11;
    uint32_t var_12;
    uint64_t var_13;
    uint64_t var_15;
    uint64_t var_16;
    uint32_t var_17;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    *(uint32_t *)(var_0 + (-208L)) = (uint32_t)rcx;
    *(uint32_t *)(var_0 + (-204L)) = (uint32_t)r8;
    var_8 = *(uint32_t *)(var_0 | 8UL);
    var_9 = (uint32_t)r9;
    r15_0 = 6UL;
    storemerge = 2304UL;
    r15_1 = 5UL;
    if ((uint64_t)((var_8 & var_9) + 1U) == 0UL) {
        return (uint64_t)(uint32_t)r15_1;
    }
    var_10 = (uint64_t)(uint32_t)rdi;
    var_11 = (uint32_t *)(rdx + 24UL);
    var_12 = (uint32_t)((uint16_t)*var_11 & (unsigned short)61440U);
    if ((uint64_t)((var_12 + (-32768)) & (-4096)) != 0UL) {
        storemerge = 67840UL;
        if ((uint64_t)((var_12 + (-16384)) & (-4096)) == 0UL) {
            return (uint64_t)(uint32_t)r15_1;
        }
    }
    var_13 = var_0 + (-224L);
    *(uint64_t *)var_13 = 4206394UL;
    var_14 = indirect_placeholder_41(0UL, storemerge, var_10, rsi, rcx);
    var_15 = var_14.field_0;
    var_16 = var_14.field_1;
    var_17 = (uint32_t)var_15;
    rax_0 = var_15;
    _pre_phi = var_17;
    rax_3 = var_15;
    local_sp_2 = var_13;
    if ((int)var_17 >= (int)0U) {
        var_18 = var_0 + (-232L);
        *(uint64_t *)var_18 = 4206499UL;
        indirect_placeholder();
        local_sp_0 = var_18;
        if (*(uint32_t *)var_15 != 13U) {
            *(uint64_t *)(local_sp_0 + (-8L)) = 4206524UL;
            indirect_placeholder();
            var_26 = (*(uint32_t *)rax_0 == 13U) ? 5UL : 6UL;
            r15_1 = var_26;
            return (uint64_t)(uint32_t)r15_1;
        }
        var_19 = (uint32_t)((uint16_t)*var_11 & (unsigned short)61440U);
        var_20 = (uint64_t)var_19;
        rax_0 = var_20;
        if ((uint64_t)((var_19 + (-32768)) & (-4096)) != 0UL) {
            *(uint64_t *)(local_sp_0 + (-8L)) = 4206524UL;
            indirect_placeholder();
            var_26 = (*(uint32_t *)rax_0 == 13U) ? 5UL : 6UL;
            r15_1 = var_26;
            return (uint64_t)(uint32_t)r15_1;
        }
        var_21 = storemerge | 1UL;
        var_22 = var_0 + (-240L);
        *(uint64_t *)var_22 = 4206563UL;
        var_23 = indirect_placeholder_40(0UL, var_21, var_10, rsi, var_16);
        var_24 = var_23.field_0;
        var_25 = (uint32_t)var_24;
        rax_0 = var_24;
        local_sp_0 = var_22;
        local_sp_2 = var_22;
        _pre_phi = var_25;
        rax_3 = var_24;
        if ((int)var_25 <= (int)4294967295U) {
            *(uint64_t *)(local_sp_0 + (-8L)) = 4206524UL;
            indirect_placeholder();
            var_26 = (*(uint32_t *)rax_0 == 13U) ? 5UL : 6UL;
            r15_1 = var_26;
            return (uint64_t)(uint32_t)r15_1;
        }
    }
    var_27 = local_sp_2 + (-8L);
    *(uint64_t *)var_27 = 4206414UL;
    indirect_placeholder();
    rax_2 = rax_3;
    local_sp_1 = var_27;
    if ((uint64_t)_pre_phi != 0UL) {
        var_28 = *(uint64_t *)(local_sp_2 + 16UL);
        r15_0 = 4UL;
        rax_2 = var_28;
        var_29 = *(uint64_t *)(local_sp_2 + 8UL);
        rax_1 = var_29;
        rax_2 = var_29;
        if (*(uint64_t *)(rdx + 8UL) != var_28 & *(uint64_t *)rdx != var_29) {
            r15_0 = 2UL;
            if ((uint64_t)(var_9 + 1U) == 0UL) {
                if ((uint64_t)(*(uint32_t *)(local_sp_2 + 36UL) - var_9) != 0UL) {
                    var_30 = *(uint32_t *)(local_sp_2 + 216UL);
                    r15_0 = 6UL;
                    if (var_30 != 4294967295U) {
                        var_32 = local_sp_2 + (-16L);
                        *(uint64_t *)var_32 = 4206660UL;
                        indirect_placeholder();
                        rax_2 = rax_1;
                        local_sp_1 = var_32;
                        if ((uint64_t)(uint32_t)rax_1 != 0UL) {
                            *(uint64_t *)(local_sp_2 + (-24L)) = 4206682UL;
                            indirect_placeholder();
                            var_35 = helper_cc_compute_c_wrapper(rax_1 + (-1L), 1UL, var_7, 16U);
                            var_36 = (uint64_t)((((0U - (uint32_t)var_35) & (-4)) + 6U) & (-2));
                            r15_1 = var_36;
                            return (uint64_t)(uint32_t)r15_1;
                        }
                    }
                    var_31 = (uint64_t)var_30;
                    rax_1 = var_31;
                    rax_2 = var_31;
                    var_32 = local_sp_2 + (-16L);
                    *(uint64_t *)var_32 = 4206660UL;
                    indirect_placeholder();
                    rax_2 = rax_1;
                    local_sp_1 = var_32;
                    if ((uint64_t)(*(uint32_t *)(local_sp_2 + 40UL) - var_30) != 0UL & (uint64_t)(uint32_t)rax_1 != 0UL) {
                        *(uint64_t *)(local_sp_2 + (-24L)) = 4206682UL;
                        indirect_placeholder();
                        var_35 = helper_cc_compute_c_wrapper(rax_1 + (-1L), 1UL, var_7, 16U);
                        var_36 = (uint64_t)((((0U - (uint32_t)var_35) & (-4)) + 6U) & (-2));
                        r15_1 = var_36;
                        return (uint64_t)(uint32_t)r15_1;
                    }
                }
            }
            var_30 = *(uint32_t *)(local_sp_2 + 216UL);
            r15_0 = 6UL;
            if (var_30 != 4294967295U) {
                var_32 = local_sp_2 + (-16L);
                *(uint64_t *)var_32 = 4206660UL;
                indirect_placeholder();
                rax_2 = rax_1;
                local_sp_1 = var_32;
                if ((uint64_t)(uint32_t)rax_1 == 0UL) {
                    *(uint64_t *)(local_sp_2 + (-24L)) = 4206682UL;
                    indirect_placeholder();
                    var_35 = helper_cc_compute_c_wrapper(rax_1 + (-1L), 1UL, var_7, 16U);
                    var_36 = (uint64_t)((((0U - (uint32_t)var_35) & (-4)) + 6U) & (-2));
                    r15_1 = var_36;
                    return (uint64_t)(uint32_t)r15_1;
                }
                *(uint64_t *)(local_sp_1 + (-8L)) = 4206448UL;
                indirect_placeholder();
                var_33 = (uint32_t *)rax_2;
                var_34 = *var_33;
                *(uint64_t *)(local_sp_1 + (-16L)) = 4206458UL;
                indirect_placeholder();
                *(uint64_t *)(local_sp_1 + (-24L)) = 4206463UL;
                indirect_placeholder();
                *var_33 = var_34;
                r15_1 = r15_0;
                return (uint64_t)(uint32_t)r15_1;
            }
            var_31 = (uint64_t)var_30;
            rax_1 = var_31;
            rax_2 = var_31;
            if ((uint64_t)(*(uint32_t *)(local_sp_2 + 40UL) - var_30) != 0UL) {
                var_32 = local_sp_2 + (-16L);
                *(uint64_t *)var_32 = 4206660UL;
                indirect_placeholder();
                rax_2 = rax_1;
                local_sp_1 = var_32;
                if ((uint64_t)(uint32_t)rax_1 == 0UL) {
                    *(uint64_t *)(local_sp_2 + (-24L)) = 4206682UL;
                    indirect_placeholder();
                    var_35 = helper_cc_compute_c_wrapper(rax_1 + (-1L), 1UL, var_7, 16U);
                    var_36 = (uint64_t)((((0U - (uint32_t)var_35) & (-4)) + 6U) & (-2));
                    r15_1 = var_36;
                    return (uint64_t)(uint32_t)r15_1;
                }
            }
            *(uint64_t *)(local_sp_1 + (-8L)) = 4206448UL;
            indirect_placeholder();
            var_33 = (uint32_t *)rax_2;
            var_34 = *var_33;
            *(uint64_t *)(local_sp_1 + (-16L)) = 4206458UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_1 + (-24L)) = 4206463UL;
            indirect_placeholder();
            *var_33 = var_34;
            r15_1 = r15_0;
            return (uint64_t)(uint32_t)r15_1;
        }
    }
    *(uint64_t *)(local_sp_1 + (-8L)) = 4206448UL;
    indirect_placeholder();
    var_33 = (uint32_t *)rax_2;
    var_34 = *var_33;
    *(uint64_t *)(local_sp_1 + (-16L)) = 4206458UL;
    indirect_placeholder();
    *(uint64_t *)(local_sp_1 + (-24L)) = 4206463UL;
    indirect_placeholder();
    *var_33 = var_34;
    r15_1 = r15_0;
}
