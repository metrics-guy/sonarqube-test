typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint32_t init_state_0x82fc(void);
uint64_t bb_rpl_chown(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint32_t var_6;
    uint64_t var_7;
    uint32_t var_8;
    uint64_t var_9;
    uint32_t var_10;
    uint64_t var_11;
    uint32_t var_28;
    uint64_t rbp_2;
    uint32_t var_29;
    uint64_t local_sp_1;
    uint64_t local_sp_3;
    uint32_t var_30;
    uint32_t var_31;
    bool var_14;
    uint64_t var_15;
    bool var_16;
    uint64_t var_17;
    uint64_t var_18;
    bool var_19;
    uint64_t var_20;
    uint64_t rax_1;
    uint64_t rax_2;
    uint64_t rax_3;
    uint64_t rbx_1;
    uint64_t rax_0;
    uint64_t r13_0;
    uint64_t r14_0;
    uint64_t local_sp_0;
    uint64_t rbp_0;
    uint64_t var_27;
    uint64_t rbx_0;
    uint64_t rbp_1;
    uint64_t local_sp_2;
    bool var_21;
    uint64_t var_22;
    bool var_23;
    uint64_t var_24;
    uint64_t var_32;
    uint64_t rdi2_0;
    uint64_t rcx_0;
    uint64_t rcx_1;
    unsigned char var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_12;
    uint64_t var_13;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_rbp();
    var_4 = init_r12();
    var_5 = init_r14();
    var_6 = init_state_0x82fc();
    *(uint64_t *)(var_0 + (-8L)) = var_5;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_3;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    var_7 = var_0 + (-184L);
    var_8 = (uint32_t)rsi;
    var_9 = (uint64_t)var_8;
    var_10 = (uint32_t)rdx;
    var_11 = (uint64_t)var_10;
    rbp_2 = var_11;
    rax_3 = 0UL;
    rbx_1 = var_9;
    rax_0 = 4294967295UL;
    rbp_0 = var_11;
    rbx_0 = var_9;
    local_sp_2 = var_7;
    rdi2_0 = rdi;
    rcx_0 = 18446744073709551615UL;
    rcx_1 = 0UL;
    if ((uint64_t)((var_10 & var_8) + 1U) == 0UL) {
        var_21 = ((uint64_t)(var_10 + 1U) == 0UL);
        var_22 = (var_1 & (-256L)) | var_21;
        var_23 = ((uint64_t)(var_8 + 1U) == 0UL);
        var_24 = (var_5 & (-256L)) | var_23;
        r13_0 = var_22;
        r14_0 = var_24;
        if (!(var_21 || var_23)) {
            var_32 = (uint64_t)var_6;
            local_sp_3 = local_sp_2;
            while (rcx_0 != 0UL)
                {
                    var_33 = *(unsigned char *)rdi2_0;
                    var_34 = rcx_0 + (-1L);
                    rcx_0 = var_34;
                    rcx_1 = var_34;
                    if (var_33 == '\x00') {
                        break;
                    }
                    rdi2_0 = rdi2_0 + var_32;
                }
            var_35 = 18446744073709551614UL - (-2L);
            if (var_35 != 0UL) {
                var_36 = local_sp_2 + (-8L);
                *(uint64_t *)var_36 = 4238170UL;
                var_37 = indirect_placeholder_1(rdi, local_sp_2);
                rax_3 = var_37;
                local_sp_3 = var_36;
                if (*(unsigned char *)((var_35 + rdi) + (-1L)) != '/' & (uint64_t)(uint32_t)var_37 == 0UL) {
                    return rax_0;
                }
            }
            *(uint64_t *)(local_sp_3 + (-8L)) = 4238143UL;
            indirect_placeholder();
            rax_0 = rax_3;
            return rax_0;
        }
        var_25 = var_0 + (-192L);
        *(uint64_t *)var_25 = 4238024UL;
        var_26 = indirect_placeholder_1(rdi, var_7);
        rax_1 = var_26;
        local_sp_0 = var_25;
        if ((uint64_t)(uint32_t)var_26 == 0UL) {
            return rax_0;
        }
        rax_2 = rax_1;
        local_sp_1 = local_sp_0;
        local_sp_2 = local_sp_0;
        if ((uint64_t)(unsigned char)r13_0 == 0UL) {
            rbx_0 = (uint64_t)*(uint32_t *)(local_sp_0 + 28UL);
            rbp_1 = rbp_0;
        } else {
            var_27 = (uint64_t)*(uint32_t *)(local_sp_0 + 32UL);
            rbp_0 = var_27;
            rbp_1 = var_27;
            if ((uint64_t)(unsigned char)r14_0 == 0UL) {
                rbx_0 = (uint64_t)*(uint32_t *)(local_sp_0 + 28UL);
                rbp_1 = rbp_0;
            }
        }
        rbx_1 = rbx_0;
        rbp_2 = rbp_1;
        if ((uint64_t)(unsigned char)rax_1 != 0UL) {
            var_32 = (uint64_t)var_6;
            local_sp_3 = local_sp_2;
            while (rcx_0 != 0UL)
                {
                    var_33 = *(unsigned char *)rdi2_0;
                    var_34 = rcx_0 + (-1L);
                    rcx_0 = var_34;
                    rcx_1 = var_34;
                    if (var_33 == '\x00') {
                        break;
                    }
                    rdi2_0 = rdi2_0 + var_32;
                }
            var_35 = 18446744073709551614UL - (-2L);
            if (var_35 != 0UL) {
                *(uint64_t *)(local_sp_3 + (-8L)) = 4238143UL;
                indirect_placeholder();
                rax_0 = rax_3;
                return rax_0;
            }
            if (*(unsigned char *)((var_35 + rdi) + (-1L)) != '/') {
                var_36 = local_sp_2 + (-8L);
                *(uint64_t *)var_36 = 4238170UL;
                var_37 = indirect_placeholder_1(rdi, local_sp_2);
                rax_3 = var_37;
                local_sp_3 = var_36;
                if ((uint64_t)(uint32_t)var_37 == 0UL) {
                    return rax_0;
                }
            }
            *(uint64_t *)(local_sp_3 + (-8L)) = 4238143UL;
            indirect_placeholder();
            rax_0 = rax_3;
            return rax_0;
        }
    }
    var_12 = var_0 + (-192L);
    *(uint64_t *)var_12 = 4238043UL;
    var_13 = indirect_placeholder_1(rdi, var_7);
    local_sp_0 = var_12;
    local_sp_1 = var_12;
    if ((uint64_t)(uint32_t)var_13 == 0UL) {
        return rax_0;
    }
    var_14 = ((uint64_t)(var_10 + 1U) == 0UL);
    var_15 = (var_1 & (-256L)) | var_14;
    var_16 = ((uint64_t)(var_8 + 1U) == 0UL);
    var_17 = var_16;
    var_18 = (var_5 & (-256L)) | var_17;
    var_19 = var_14 || var_16;
    var_20 = (uint64_t)((uint32_t)var_15 & (-255)) | var_17;
    rax_1 = var_20;
    r13_0 = var_15;
    r14_0 = var_18;
    rax_2 = var_20;
    if (!var_19) {
        rax_2 = rax_1;
        local_sp_1 = local_sp_0;
        local_sp_2 = local_sp_0;
        if ((uint64_t)(unsigned char)r13_0 == 0UL) {
            rbx_0 = (uint64_t)*(uint32_t *)(local_sp_0 + 28UL);
            rbp_1 = rbp_0;
        } else {
            var_27 = (uint64_t)*(uint32_t *)(local_sp_0 + 32UL);
            rbp_0 = var_27;
            rbp_1 = var_27;
            if ((uint64_t)(unsigned char)r14_0 == 0UL) {
                rbx_0 = (uint64_t)*(uint32_t *)(local_sp_0 + 28UL);
                rbp_1 = rbp_0;
            }
        }
        rbx_1 = rbx_0;
        rbp_2 = rbp_1;
        if ((uint64_t)(unsigned char)rax_1 == 0UL) {
            var_32 = (uint64_t)var_6;
            local_sp_3 = local_sp_2;
            while (rcx_0 != 0UL)
                {
                    var_33 = *(unsigned char *)rdi2_0;
                    var_34 = rcx_0 + (-1L);
                    rcx_0 = var_34;
                    rcx_1 = var_34;
                    if (var_33 == '\x00') {
                        break;
                    }
                    rdi2_0 = rdi2_0 + var_32;
                }
            var_35 = 18446744073709551614UL - (-2L);
            if (var_35 != 0UL) {
                var_36 = local_sp_2 + (-8L);
                *(uint64_t *)var_36 = 4238170UL;
                var_37 = indirect_placeholder_1(rdi, local_sp_2);
                rax_3 = var_37;
                local_sp_3 = var_36;
                if (*(unsigned char *)((var_35 + rdi) + (-1L)) != '/' & (uint64_t)(uint32_t)var_37 == 0UL) {
                    return rax_0;
                }
            }
            *(uint64_t *)(local_sp_3 + (-8L)) = 4238143UL;
            indirect_placeholder();
            rax_0 = rax_3;
            return rax_0;
        }
    }
    *(uint64_t *)(local_sp_1 + (-8L)) = 4238193UL;
    indirect_placeholder();
    rax_0 = rax_2;
    if ((uint64_t)(uint32_t)rax_2 == 0UL) {
        return rax_0;
    }
    var_28 = *(uint32_t *)(local_sp_1 + 20UL);
    var_29 = (uint32_t)rbx_1;
    if ((uint64_t)(var_28 - var_29) != 0UL) {
        if ((uint64_t)(var_29 + 1U) == 0UL) {
            return rax_0;
        }
    }
    var_30 = *(uint32_t *)(local_sp_1 + 24UL);
    var_31 = (uint32_t)rbp_2;
    if ((uint64_t)(var_30 - var_31) != 0UL) {
        if ((uint64_t)(var_31 + 1U) != 0UL) {
            return rax_0;
        }
    }
    *(uint64_t *)(local_sp_1 + (-16L)) = 4238237UL;
    indirect_placeholder();
}
