typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_9_ret_type;
struct indirect_placeholder_10_ret_type;
struct indirect_placeholder_11_ret_type;
struct indirect_placeholder_12_ret_type;
struct indirect_placeholder_9_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_10_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_11_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_12_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r10(void);
extern uint32_t init_state_0x82fc(void);
extern void indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_1(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_9_ret_type indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_10_ret_type indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_11_ret_type indirect_placeholder_11(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_12_ret_type indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2);
uint64_t bb_fold_file(uint64_t rdi, uint64_t rsi) {
    uint64_t r13_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint32_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t rbx_0;
    uint64_t r13_0_be;
    uint64_t r12_1_be;
    uint64_t rcx_2_be;
    uint64_t local_sp_5_be;
    uint64_t rsi2_0;
    uint64_t r12_0;
    uint64_t local_sp_0;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_85;
    struct indirect_placeholder_9_ret_type var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t cc_src_0;
    uint64_t local_sp_7;
    uint64_t cc_dst_0;
    uint32_t cc_op_0;
    uint64_t rdi1_0;
    uint64_t local_sp_1;
    uint64_t rcx_0;
    uint64_t cc_src_1;
    uint64_t cc_dst_1;
    uint64_t var_72;
    uint64_t var_73;
    uint32_t cc_op_1;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t cc_src_2;
    uint64_t cc_dst_2;
    uint32_t cc_op_2;
    uint64_t rdi1_1;
    uint64_t rsi2_1;
    uint64_t rcx_1;
    uint64_t cc_src_3;
    uint64_t cc_dst_3;
    uint64_t var_78;
    uint64_t var_79;
    uint32_t cc_op_3;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t *var_67;
    struct indirect_placeholder_10_ret_type var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_15;
    bool var_65;
    uint64_t var_66;
    uint64_t var_64;
    uint64_t local_sp_2;
    uint64_t rbx_1;
    uint64_t var_46;
    uint64_t rcx_5;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t r12_2;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t rax_2;
    uint64_t rbp_0;
    uint64_t var_32;
    uint64_t var_33;
    struct indirect_placeholder_11_ret_type var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_55;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t local_sp_3;
    unsigned char var_58;
    uint64_t var_59;
    uint64_t r13_1_be;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t rax_0;
    uint64_t local_sp_4;
    uint32_t var_37;
    uint64_t var_38;
    uint64_t r12_1;
    uint64_t rcx_2;
    uint64_t local_sp_5;
    uint64_t var_39;
    uint64_t var_40;
    bool var_41;
    uint64_t var_42;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t r12_2_be;
    uint64_t rcx_5_be;
    uint64_t rax_2_be;
    uint64_t local_sp_7_be;
    uint64_t r13_1;
    uint64_t rdx_0;
    uint64_t cc_src_4;
    uint64_t cc_dst_4;
    uint32_t cc_op_4;
    uint64_t rdi1_2;
    uint64_t rsi2_2;
    uint64_t rcx_3;
    uint64_t cc_src_5;
    uint64_t cc_dst_5;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint32_t cc_op_5;
    uint64_t rcx_4;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_22;
    uint64_t *var_23;
    uint64_t var_24;
    uint64_t rax_1;
    uint64_t local_sp_6;
    uint64_t var_25;
    uint64_t var_26;
    uint32_t var_27;
    uint64_t var_28;
    uint64_t var_91;
    struct indirect_placeholder_12_ret_type var_92;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t var_21;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_r10();
    var_8 = init_cc_src2();
    var_9 = init_state_0x82fc();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_10 = var_0 + (-88L);
    *(uint64_t *)(var_0 + (-64L)) = rdi;
    *(uint64_t *)(var_0 + (-72L)) = rsi;
    var_11 = (uint64_t)var_9;
    r13_0_be = 0UL;
    r12_1_be = 0UL;
    r12_0 = 0UL;
    cc_src_0 = 18446744073709551615UL;
    cc_op_0 = 24U;
    rdi1_0 = 4262578UL;
    rcx_0 = 2UL;
    cc_src_2 = 18446744073709551615UL;
    cc_op_2 = 24U;
    rdi1_1 = 4262578UL;
    rcx_1 = 2UL;
    r12_2 = 0UL;
    r13_1_be = 0UL;
    r12_2_be = 0UL;
    r13_1 = 0UL;
    rdx_0 = 0UL;
    cc_src_4 = 40UL;
    cc_dst_4 = var_10;
    cc_op_4 = 17U;
    rdi1_2 = 4262578UL;
    rsi2_2 = rdi;
    rcx_3 = 2UL;
    rcx_4 = 0UL;
    local_sp_6 = var_10;
    cc_op_4 = 14U;
    cc_src_5 = cc_src_4;
    cc_dst_5 = cc_dst_4;
    cc_op_5 = cc_op_4;
    while (rcx_3 != 0UL)
        {
            rdi1_2 = rdi1_2 + var_11;
            rsi2_2 = rsi2_2 + var_11;
            cc_op_4 = 14U;
            cc_src_5 = cc_src_4;
            cc_dst_5 = cc_dst_4;
            cc_op_5 = cc_op_4;
        }
    var_15 = helper_cc_compute_all_wrapper(cc_dst_5, cc_src_5, var_8, cc_op_5);
    var_16 = ((var_15 & 65UL) == 0UL);
    var_17 = rdi & (-256L);
    var_18 = helper_cc_compute_c_wrapper(cc_dst_5, var_15, var_8, 1U);
    var_19 = (uint64_t)((unsigned char)var_16 - (unsigned char)var_18);
    var_20 = var_17 | var_19;
    var_24 = var_20;
    rax_1 = var_20;
    rcx_5 = rcx_4;
    if (var_19 == 0UL) {
        var_22 = *(uint64_t *)4275784UL;
        var_23 = (uint64_t *)(var_0 + (-80L));
        *var_23 = var_22;
        *(unsigned char *)4277056UL = (unsigned char)'\x01';
        var_24 = *var_23;
        rax_1 = var_22;
    } else {
        var_21 = var_0 + (-96L);
        *(uint64_t *)var_21 = 4204757UL;
        indirect_placeholder();
        *(uint64_t *)var_10 = var_20;
        local_sp_6 = var_21;
    }
    rax_2 = rax_1;
    if (var_24 == 0UL) {
        var_91 = *(uint64_t *)(local_sp_6 + 24UL);
        *(uint64_t *)(local_sp_6 + (-8L)) = 4204784UL;
        var_92 = indirect_placeholder_12(var_91, 0UL, 3UL);
        var_93 = var_92.field_0;
        var_94 = var_92.field_1;
        var_95 = var_92.field_2;
        *(uint64_t *)(local_sp_6 + (-16L)) = 4204792UL;
        indirect_placeholder();
        var_96 = (uint64_t)*(uint32_t *)var_93;
        *(uint64_t *)(local_sp_6 + (-24L)) = 4204817UL;
        indirect_placeholder_1(0UL, 4256952UL, 0UL, var_96, var_93, var_94, var_95);
    } else {
        var_25 = local_sp_6 + (-8L);
        *(uint64_t *)var_25 = 4204725UL;
        indirect_placeholder_4(var_2, var_24, 2UL);
        rdx_0 = 1UL;
        local_sp_7 = var_25;
        var_26 = local_sp_7 + (-8L);
        *(uint64_t *)var_26 = 4204929UL;
        indirect_placeholder();
        var_27 = (uint32_t)rax_2;
        var_28 = (uint64_t)var_27;
        r13_0 = r13_1;
        cc_dst_0 = rax_2;
        cc_dst_2 = rax_2;
        rbp_0 = var_28;
        rax_0 = rax_2;
        local_sp_4 = var_26;
        r12_1 = r12_2;
        rcx_2 = rcx_5;
        while ((uint64_t)(var_27 + 1U) != 0UL)
            {
                var_29 = r13_1 + 1UL;
                var_30 = *(uint64_t *)4277048UL;
                var_31 = helper_cc_compute_c_wrapper(var_29 - var_30, var_30, var_18, 17U);
                if (var_31 == 0UL) {
                    var_32 = *(uint64_t *)4277040UL;
                    var_33 = local_sp_7 + (-16L);
                    *(uint64_t *)var_33 = 4204970UL;
                    var_34 = indirect_placeholder_11(var_29, var_32, var_28, 4277048UL, rcx_5, var_7);
                    var_35 = var_34.field_0;
                    var_36 = var_34.field_2;
                    *(uint64_t *)4277040UL = var_35;
                    rbp_0 = var_36;
                    rax_0 = var_35;
                    local_sp_4 = var_33;
                }
                *(unsigned char *)(local_sp_4 + 7UL) = (unsigned char)rbp_0;
                var_37 = (uint32_t)rbp_0;
                local_sp_5 = local_sp_4;
                rax_2_be = rax_0;
                if ((uint64_t)(var_37 + (-10)) == 0UL) {
                    *(unsigned char *)(r13_1 + *(uint64_t *)4277040UL) = (unsigned char)'\n';
                    var_60 = *(uint64_t *)4275776UL;
                    var_61 = local_sp_4 + (-8L);
                    *(uint64_t *)var_61 = 4205014UL;
                    indirect_placeholder();
                    rcx_5_be = var_60;
                    local_sp_7_be = var_61;
                } else {
                    var_38 = (uint64_t)(uint32_t)((int)(var_37 << 24U) >> (int)24U);
                    r13_1_be = 1UL;
                    while (1U)
                        {
                            var_39 = local_sp_5 + (-8L);
                            *(uint64_t *)var_39 = 4204853UL;
                            var_40 = indirect_placeholder_5(r12_1, var_38);
                            var_41 = (var_40 > *(uint64_t *)(local_sp_5 + 8UL));
                            var_42 = *(uint64_t *)4277040UL;
                            local_sp_2 = var_39;
                            rbx_1 = r13_0;
                            var_55 = var_42;
                            local_sp_3 = var_39;
                            r12_2_be = var_40;
                            rcx_5_be = rcx_2;
                            rax_2_be = var_42;
                            local_sp_7_be = var_39;
                            if (!var_41) {
                                loop_state_var = 0U;
                                break;
                            }
                            if (*(unsigned char *)4277058UL == '\x00') {
                                rax_2_be = var_55;
                                local_sp_7_be = local_sp_3;
                                if (r13_0 != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                *(unsigned char *)(r13_0 + var_55) = (unsigned char)'\n';
                                var_56 = *(uint64_t *)4275776UL;
                                var_57 = local_sp_3 + (-8L);
                                *(uint64_t *)var_57 = 4205106UL;
                                indirect_placeholder();
                                rcx_2_be = var_56;
                                local_sp_5_be = var_57;
                                r13_0 = r13_0_be;
                                r12_1 = r12_1_be;
                                rcx_2 = rcx_2_be;
                                local_sp_5 = local_sp_5_be;
                                continue;
                            }
                            while (1U)
                                {
                                    local_sp_3 = local_sp_2;
                                    if (rbx_1 != 0UL) {
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    var_43 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)((rbx_1 + var_42) + (-1L));
                                    *(uint64_t *)(local_sp_2 + (-8L)) = 4205052UL;
                                    var_44 = indirect_placeholder_2(var_43);
                                    var_45 = local_sp_2 + (-16L);
                                    *(uint64_t *)var_45 = 4205060UL;
                                    indirect_placeholder();
                                    local_sp_2 = var_45;
                                    if ((uint64_t)(uint32_t)var_44 != 0UL) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    rbx_1 = rbx_1 + (-1L);
                                    continue;
                                }
                            switch (loop_state_var) {
                              case 1U:
                                {
                                    var_55 = *(uint64_t *)4277040UL;
                                }
                                break;
                              case 0U:
                                {
                                    var_46 = *(uint64_t *)4275776UL;
                                    *(uint64_t *)(local_sp_2 + (-24L)) = 4205462UL;
                                    indirect_placeholder();
                                    *(uint64_t *)(local_sp_2 + (-32L)) = 4205472UL;
                                    indirect_placeholder();
                                    var_47 = r13_0 - rbx_1;
                                    var_48 = *(uint64_t *)4277040UL;
                                    var_49 = local_sp_2 + (-40L);
                                    *(uint64_t *)var_49 = 4205497UL;
                                    indirect_placeholder();
                                    var_50 = var_48 + var_47;
                                    rcx_2_be = var_46;
                                    local_sp_5_be = var_49;
                                    rbx_0 = var_48;
                                    local_sp_0 = var_49;
                                    if (var_47 == 0UL) {
                                        var_51 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)rbx_0;
                                        var_52 = local_sp_0 + (-8L);
                                        *(uint64_t *)var_52 = 4205525UL;
                                        var_53 = indirect_placeholder_5(r12_0, var_51);
                                        var_54 = rbx_0 + 1UL;
                                        rbx_0 = var_54;
                                        r13_0_be = var_47;
                                        r12_1_be = var_53;
                                        local_sp_5_be = var_52;
                                        r12_0 = var_53;
                                        local_sp_0 = var_52;
                                        do {
                                            var_51 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)rbx_0;
                                            var_52 = local_sp_0 + (-8L);
                                            *(uint64_t *)var_52 = 4205525UL;
                                            var_53 = indirect_placeholder_5(r12_0, var_51);
                                            var_54 = rbx_0 + 1UL;
                                            rbx_0 = var_54;
                                            r13_0_be = var_47;
                                            r12_1_be = var_53;
                                            local_sp_5_be = var_52;
                                            r12_0 = var_53;
                                            local_sp_0 = var_52;
                                        } while (var_54 != var_50);
                                    }
                                    r13_0 = r13_0_be;
                                    r12_1 = r12_1_be;
                                    rcx_2 = rcx_2_be;
                                    local_sp_5 = local_sp_5_be;
                                    continue;
                                }
                                break;
                            }
                        }
                    switch (loop_state_var) {
                      case 0U:
                        {
                            *(unsigned char *)(r13_0 + var_42) = *(unsigned char *)(local_sp_5 + (-1L));
                            r13_1_be = r13_0 + 1UL;
                        }
                        break;
                      case 1U:
                        {
                            var_58 = *(unsigned char *)(local_sp_3 + 7UL);
                            var_59 = (uint64_t)var_58;
                            *(unsigned char *)var_55 = var_58;
                            rcx_5_be = var_59;
                        }
                        break;
                    }
                }
                local_sp_7 = local_sp_7_be;
                rcx_5 = rcx_5_be;
                r12_2 = r12_2_be;
                rax_2 = rax_2_be;
                r13_1 = r13_1_be;
                var_26 = local_sp_7 + (-8L);
                *(uint64_t *)var_26 = 4204929UL;
                indirect_placeholder();
                var_27 = (uint32_t)rax_2;
                var_28 = (uint64_t)var_27;
                r13_0 = r13_1;
                cc_dst_0 = rax_2;
                cc_dst_2 = rax_2;
                rbp_0 = var_28;
                rax_0 = rax_2;
                local_sp_4 = var_26;
                r12_1 = r12_2;
                rcx_2 = rcx_5;
            }
        var_62 = local_sp_7 + (-16L);
        *(uint64_t *)var_62 = 4205153UL;
        indirect_placeholder();
        var_63 = (uint64_t)*(uint32_t *)rax_2;
        local_sp_1 = var_62;
        if (r13_1 == 0UL) {
            var_64 = local_sp_7 + (-24L);
            *(uint64_t *)var_64 = 4205253UL;
            indirect_placeholder();
            local_sp_1 = var_64;
        }
        *(uint64_t *)(local_sp_1 + (-8L)) = 4205170UL;
        indirect_placeholder();
        var_65 = (var_28 == 0UL);
        var_66 = *(uint64_t *)(local_sp_1 + 16UL);
        rsi2_0 = var_66;
        rsi2_1 = var_66;
        if (var_65) {
            var_67 = (uint64_t *)(local_sp_1 + (-16L));
            *var_67 = 4205278UL;
            var_68 = indirect_placeholder_10(var_66, 0UL, 3UL);
            var_69 = var_68.field_0;
            var_70 = var_68.field_1;
            var_71 = var_68.field_2;
            *(uint64_t *)(local_sp_1 + (-24L)) = 4205303UL;
            indirect_placeholder_1(0UL, 4256952UL, 0UL, var_63, var_69, var_70, var_71);
            cc_op_0 = 14U;
            cc_src_1 = cc_src_0;
            cc_dst_1 = cc_dst_0;
            cc_op_1 = cc_op_0;
            while (rcx_0 != 0UL)
                {
                    var_72 = (uint64_t)*(unsigned char *)rdi1_0;
                    var_73 = (uint64_t)*(unsigned char *)rsi2_0 - var_72;
                    cc_src_0 = var_72;
                    cc_dst_0 = var_73;
                    cc_src_1 = var_72;
                    cc_dst_1 = var_73;
                    cc_op_1 = 14U;
                    if ((uint64_t)(unsigned char)var_73 == 0UL) {
                        break;
                    }
                    rdi1_0 = rdi1_0 + var_11;
                    rsi2_0 = rsi2_0 + var_11;
                    rcx_0 = rcx_0 + (-1L);
                    cc_op_0 = 14U;
                    cc_src_1 = cc_src_0;
                    cc_dst_1 = cc_dst_0;
                    cc_op_1 = cc_op_0;
                }
            var_74 = helper_cc_compute_all_wrapper(cc_dst_1, cc_src_1, var_18, cc_op_1);
            var_75 = ((var_74 & 65UL) == 0UL);
            var_76 = helper_cc_compute_c_wrapper(cc_dst_1, var_74, var_18, 1U);
            if ((uint64_t)((unsigned char)var_75 - (unsigned char)var_76) == 0UL) {
                var_77 = *var_67;
                *(uint64_t *)(local_sp_1 + (-32L)) = 4205342UL;
                indirect_placeholder_5(0UL, var_77);
            }
        } else {
            cc_op_2 = 14U;
            cc_src_3 = cc_src_2;
            cc_dst_3 = cc_dst_2;
            cc_op_3 = cc_op_2;
            while (rcx_1 != 0UL)
                {
                    var_78 = (uint64_t)*(unsigned char *)rdi1_1;
                    var_79 = (uint64_t)*(unsigned char *)rsi2_1 - var_78;
                    cc_src_2 = var_78;
                    cc_dst_2 = var_79;
                    cc_src_3 = var_78;
                    cc_dst_3 = var_79;
                    cc_op_3 = 14U;
                    if ((uint64_t)(unsigned char)var_79 == 0UL) {
                        break;
                    }
                    rdi1_1 = rdi1_1 + var_11;
                    rsi2_1 = rsi2_1 + var_11;
                    rcx_1 = rcx_1 + (-1L);
                    cc_op_2 = 14U;
                    cc_src_3 = cc_src_2;
                    cc_dst_3 = cc_dst_2;
                    cc_op_3 = cc_op_2;
                }
            var_80 = helper_cc_compute_all_wrapper(cc_dst_3, cc_src_3, var_18, cc_op_3);
            var_81 = ((var_80 & 65UL) == 0UL);
            var_82 = helper_cc_compute_c_wrapper(cc_dst_3, var_80, var_18, 1U);
            var_83 = *(uint64_t *)local_sp_1;
            *(uint64_t *)(local_sp_1 + (-16L)) = 4205362UL;
            var_84 = indirect_placeholder_5(1UL, var_83);
            if ((uint64_t)((unsigned char)var_81 - (unsigned char)var_82) != 0UL & (uint64_t)((uint32_t)var_84 + 1U) == 0UL) {
                var_85 = *(uint64_t *)(local_sp_1 + 8UL);
                *(uint64_t *)(local_sp_1 + (-24L)) = 4205396UL;
                var_86 = indirect_placeholder_9(var_85, 0UL, 3UL);
                var_87 = var_86.field_0;
                var_88 = var_86.field_1;
                var_89 = var_86.field_2;
                *(uint64_t *)(local_sp_1 + (-32L)) = 4205404UL;
                indirect_placeholder();
                var_90 = (uint64_t)*(uint32_t *)var_87;
                *(uint64_t *)(local_sp_1 + (-40L)) = 4205429UL;
                indirect_placeholder_1(0UL, 4256952UL, 0UL, var_90, var_87, var_88, var_89);
                rdx_0 = 0UL;
            }
        }
    }
    return rdx_0;
}
