typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern void abort(void);
extern void function_dispatcher(unsigned char *param_0);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_14(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern void indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_12(uint64_t param_0);
extern void indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
void bb_main(uint64_t rdi, uint64_t rsi) {
    uint64_t var_12;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t local_sp_7;
    uint64_t r14_1;
    uint64_t *var_23;
    uint64_t local_sp_1;
    uint64_t rax_0;
    uint64_t rdx_0;
    uint64_t rcx_0;
    uint64_t r13_0;
    unsigned char var_27;
    uint64_t rax_2;
    uint64_t var_28;
    uint64_t var_29;
    unsigned char var_30;
    uint64_t var_31;
    uint64_t local_sp_0;
    uint64_t rax_1;
    uint64_t r8_0;
    uint64_t r8_1;
    uint64_t rbx_0;
    uint64_t var_24;
    unsigned char var_25;
    uint64_t var_26;
    uint64_t local_sp_6;
    uint64_t local_sp_2;
    uint64_t r12_0;
    uint64_t rax_3;
    uint64_t r14_0;
    uint64_t var_33;
    uint64_t var_32;
    uint64_t r13_2;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t local_sp_3;
    uint64_t rax_4;
    uint64_t r13_1;
    uint32_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t local_sp_4;
    uint64_t var_21;
    uint64_t rax_5;
    uint64_t var_22;
    uint64_t var_11;
    uint32_t var_13;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_8 = (uint64_t)(uint32_t)rdi;
    var_9 = *(uint64_t *)rsi;
    *(uint64_t *)(var_0 + (-64L)) = 4204599UL;
    indirect_placeholder_12(var_9);
    *(uint64_t *)(var_0 + (-72L)) = 4204614UL;
    indirect_placeholder();
    *(uint64_t *)(var_0 + (-80L)) = 4204624UL;
    indirect_placeholder_12(2UL);
    var_10 = var_0 + (-88L);
    *(uint64_t *)var_10 = 4204634UL;
    indirect_placeholder();
    local_sp_7 = var_10;
    r14_1 = 0UL;
    rax_2 = 0UL;
    r8_1 = 0UL;
    r13_2 = 0UL;
    var_11 = local_sp_7 + (-8L);
    *(uint64_t *)var_11 = 4204672UL;
    var_12 = indirect_placeholder_14(4251826UL, var_8, rsi, 4252640UL, 0UL);
    var_13 = (uint32_t)var_12;
    local_sp_7 = var_11;
    r13_2 = 1UL;
    local_sp_3 = var_11;
    rax_4 = var_12;
    r13_1 = r13_2;
    while ((uint64_t)(var_13 + 1U) != 0UL)
        {
            rax_4 = 0UL;
            r13_1 = 1UL;
            if ((uint64_t)(var_13 + 130U) == 0UL) {
                *(uint64_t *)(local_sp_7 + (-16L)) = 4204719UL;
                indirect_placeholder_4(rsi, 0UL, var_8);
                abort();
            }
            if ((uint64_t)(var_13 + (-48)) == 0UL) {
                var_11 = local_sp_7 + (-8L);
                *(uint64_t *)var_11 = 4204672UL;
                var_12 = indirect_placeholder_14(4251826UL, var_8, rsi, 4252640UL, 0UL);
                var_13 = (uint32_t)var_12;
                local_sp_7 = var_11;
                r13_2 = 1UL;
                local_sp_3 = var_11;
                rax_4 = var_12;
                r13_1 = r13_2;
                continue;
            }
            if ((uint64_t)(var_13 + 131U) == 0UL) {
                *(uint64_t *)(local_sp_7 + (-16L)) = 4204709UL;
                indirect_placeholder_4(rsi, 2UL, var_8);
                abort();
            }
            *(uint64_t *)(local_sp_7 + (-24L)) = 0UL;
            var_14 = *(uint64_t *)4272672UL;
            var_15 = *(uint64_t *)4271680UL;
            *(uint64_t *)(local_sp_7 + (-32L)) = 4204771UL;
            indirect_placeholder_13(0UL, 4251743UL, var_15, 4251784UL, var_14, 4251793UL, 4251810UL);
            var_16 = local_sp_7 + (-40L);
            *(uint64_t *)var_16 = 4204781UL;
            indirect_placeholder();
            local_sp_3 = var_16;
            break;
        }
    var_17 = *(uint32_t *)4272796UL;
    var_18 = (uint64_t)var_17 << 32UL;
    var_19 = rdi << 32UL;
    local_sp_4 = local_sp_3;
    local_sp_6 = local_sp_3;
    rax_5 = rax_4;
    if ((long)var_18 >= (long)var_19) {
        var_20 = *(uint64_t *)4272800UL;
        helper_cc_compute_c_wrapper(r13_1 + (-1L), 1UL, var_7, 14U);
        rbx_0 = var_20;
        while (*(uint64_t *)rbx_0 != 0UL)
            {
                var_21 = local_sp_4 + (-8L);
                *(uint64_t *)var_21 = 4204828UL;
                indirect_placeholder();
                local_sp_4 = var_21;
                rbx_0 = rbx_0 + 8UL;
            }
    }
    helper_cc_compute_c_wrapper(r13_1 + (-1L), 1UL, var_7, 14U);
    r12_0 = (uint64_t)var_17;
    var_33 = r12_0 + 1UL;
    local_sp_6 = local_sp_2;
    rax_5 = rax_3;
    r12_0 = var_33;
    r14_1 = r14_0;
    do {
        var_22 = local_sp_6 + (-8L);
        *(uint64_t *)var_22 = 4204999UL;
        indirect_placeholder();
        local_sp_1 = var_22;
        local_sp_2 = var_22;
        rax_3 = rax_5;
        r14_0 = r14_1;
        if (rax_5 != 0UL) {
            var_23 = (uint64_t *)((r12_0 << 3UL) + rsi);
            r13_0 = *(uint64_t *)4272800UL;
            var_24 = *(uint64_t *)r13_0;
            rcx_0 = var_24;
            local_sp_0 = local_sp_1;
            r8_0 = r8_1;
            local_sp_2 = local_sp_1;
            rax_3 = rax_2;
            while (var_24 != 0UL)
                {
                    var_25 = *(unsigned char *)var_24;
                    var_26 = (uint64_t)var_25;
                    rax_0 = var_26;
                    rax_1 = var_26;
                    if (var_25 == '\x00') {
                        local_sp_1 = local_sp_0;
                        rax_2 = rax_1;
                        r13_0 = r13_0 + 8UL;
                        r8_1 = r8_0;
                        var_24 = *(uint64_t *)r13_0;
                        rcx_0 = var_24;
                        local_sp_0 = local_sp_1;
                        r8_0 = r8_1;
                        local_sp_2 = local_sp_1;
                        rax_3 = rax_2;
                        continue;
                    }
                    rdx_0 = *var_23;
                    while (1U)
                        {
                            var_27 = *(unsigned char *)rdx_0;
                            rax_1 = rax_0;
                            if (var_27 != '\x00') {
                                loop_state_var = 0U;
                                break;
                            }
                            var_28 = rcx_0 + 1UL;
                            var_29 = rdx_0 + 1UL;
                            rdx_0 = var_29;
                            rcx_0 = var_28;
                            if ((uint64_t)(var_27 - (unsigned char)rax_0) != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            var_30 = *(unsigned char *)var_28;
                            var_31 = (uint64_t)var_30;
                            rax_0 = var_31;
                            rax_1 = 0UL;
                            r8_0 = 1UL;
                            if ((uint64_t)(var_30 + '\xc3') != 0UL) {
                                rax_1 = var_31;
                                r8_0 = r8_1;
                                if (var_30 != '\x00') {
                                    continue;
                                }
                                loop_state_var = 0U;
                                break;
                            }
                            if (*(unsigned char *)var_29 == '\x00') {
                                continue;
                            }
                            loop_state_var = 1U;
                            break;
                        }
                    var_32 = local_sp_1 + (-8L);
                    *(uint64_t *)var_32 = 4204961UL;
                    indirect_placeholder();
                    local_sp_0 = var_32;
                }
            r14_0 = (uint64_t)((uint32_t)r14_1 + (uint32_t)(uint64_t)(unsigned char)r8_1);
        }
        var_33 = r12_0 + 1UL;
        local_sp_6 = local_sp_2;
        rax_5 = rax_3;
        r12_0 = var_33;
        r14_1 = r14_0;
    } while ((long)var_19 <= (long)(var_33 << 32UL));
    function_dispatcher((unsigned char *)(0UL));
    return;
}
