typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
uint64_t bb_mkancesdirs(uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t rcx) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    unsigned char var_8;
    uint64_t r14_4;
    uint64_t rdx1_1;
    uint64_t var_29;
    uint64_t rbp_3;
    uint64_t r13_0;
    uint64_t _pre_phi;
    unsigned char _pre;
    uint64_t var_31;
    uint64_t storemerge;
    uint32_t *var_30;
    uint64_t r13_2;
    uint64_t *_pre_phi62;
    uint64_t var_23;
    uint64_t *var_24;
    uint64_t var_9;
    uint64_t rdx1_2;
    uint64_t local_sp_3;
    uint64_t local_sp_0;
    uint64_t rdx1_0;
    uint64_t rbx_0_in;
    uint64_t r14_3;
    uint64_t rbp_0;
    uint64_t r14_0;
    uint64_t rbx_0;
    unsigned char *_cast1;
    unsigned char var_10;
    uint64_t var_11;
    bool var_12;
    bool var_13;
    uint32_t var_14;
    unsigned char var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t local_sp_1;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint32_t var_28;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t *var_22;
    unsigned char var_32;
    uint64_t local_sp_2;
    uint64_t rbp_1;
    uint64_t r14_1;
    uint64_t var_33;
    uint64_t var_34;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_7 = var_0 + (-88L);
    *(uint64_t *)var_7 = rsi;
    *(uint64_t *)(var_0 + (-80L)) = rdx;
    *(uint64_t *)(var_0 + (-72L)) = rcx;
    var_8 = *(unsigned char *)rdi;
    r14_4 = rdi;
    rdx1_1 = 1UL;
    r13_0 = 0UL;
    local_sp_0 = var_7;
    rbx_0_in = rdi;
    rbp_0 = var_4;
    r14_0 = rdi;
    if (var_8 == '\x00') {
        var_34 = r14_4 - rdi;
        storemerge = var_34;
        return storemerge;
    }
    var_9 = (uint64_t)var_8;
    *(unsigned char *)(var_0 + (-61L)) = (unsigned char)'\x00';
    rdx1_0 = var_9;
    while (1U)
        {
            rbx_0 = rbx_0_in + 1UL;
            _cast1 = (unsigned char *)rbx_0;
            var_10 = *_cast1;
            var_11 = (uint64_t)var_10;
            var_12 = ((uint64_t)(var_10 + '\xd1') == 0UL);
            var_13 = ((uint64_t)((unsigned char)rdx1_0 + '\xd1') == 0UL);
            rbp_3 = rbp_0;
            _pre_phi = var_11;
            r13_2 = r13_0;
            rdx1_2 = var_11;
            local_sp_3 = local_sp_0;
            rbx_0_in = rbx_0;
            r14_3 = r14_0;
            local_sp_1 = local_sp_0;
            var_32 = var_10;
            local_sp_2 = local_sp_0;
            r14_1 = r14_0;
            if (!var_12) {
                var_14 = (((uint32_t)rbp_0 & (-256)) | (uint32_t)var_13) & (((uint32_t)rdx1_0 & (-256)) | (uint32_t)(r13_0 != 0UL));
                var_15 = (unsigned char)(uint64_t)var_14 & (unsigned char)(var_10 != '\x00');
                var_16 = (uint64_t)(var_14 & (-256));
                var_17 = (uint64_t)var_15 & 1UL;
                var_18 = var_16 | var_17;
                rbp_1 = var_18;
                rbp_3 = var_18;
                r14_3 = rbx_0;
                if (var_17 != 0UL) {
                    var_19 = r13_0 - r14_0;
                    rbp_1 = 0UL;
                    r14_1 = rbx_0;
                    if (var_19 == 1UL) {
                        if (*(unsigned char *)r14_0 != '.') {
                            r13_0 = r13_2;
                            local_sp_0 = local_sp_3;
                            rdx1_0 = rdx1_2;
                            rbp_0 = rbp_3;
                            r14_0 = r14_3;
                            continue;
                        }
                        *(unsigned char *)r13_0 = (unsigned char)'\x00';
                        var_20 = *(uint64_t *)(local_sp_0 + 8UL);
                        var_21 = local_sp_0 + (-8L);
                        var_22 = (uint64_t *)var_21;
                        *var_22 = 4223088UL;
                        indirect_placeholder();
                        _pre_phi62 = var_22;
                        local_sp_1 = var_21;
                        if ((int)(uint32_t)var_20 < (int)0U) {
                            *(unsigned char *)(local_sp_0 + 19UL) = var_15;
                            *(uint32_t *)(local_sp_0 + 20UL) = 0U;
                        } else {
                            var_23 = local_sp_0 + (-16L);
                            var_24 = (uint64_t *)var_23;
                            *var_24 = 4223302UL;
                            indirect_placeholder();
                            *(uint32_t *)(local_sp_0 + 12UL) = *(uint32_t *)var_20;
                            _pre_phi62 = var_24;
                            local_sp_1 = var_23;
                            rdx1_1 = (uint64_t)*(unsigned char *)(local_sp_0 + 11UL);
                        }
                    } else {
                        *(unsigned char *)r13_0 = (unsigned char)'\x00';
                        rdx1_1 = 0UL;
                        if (var_19 == 2UL) {
                            if (*(unsigned char *)r14_0 == '.') {
                                if (*(unsigned char *)(r14_0 + 1UL) == '.') {
                                    *(uint32_t *)(local_sp_0 + 28UL) = 0U;
                                    *(unsigned char *)(local_sp_0 + 27UL) = (unsigned char)'\x00';
                                    _pre_phi62 = (uint64_t *)local_sp_0;
                                } else {
                                    var_20 = *(uint64_t *)(local_sp_0 + 8UL);
                                    var_21 = local_sp_0 + (-8L);
                                    var_22 = (uint64_t *)var_21;
                                    *var_22 = 4223088UL;
                                    indirect_placeholder();
                                    _pre_phi62 = var_22;
                                    local_sp_1 = var_21;
                                    if ((int)(uint32_t)var_20 < (int)0U) {
                                        var_23 = local_sp_0 + (-16L);
                                        var_24 = (uint64_t *)var_23;
                                        *var_24 = 4223302UL;
                                        indirect_placeholder();
                                        *(uint32_t *)(local_sp_0 + 12UL) = *(uint32_t *)var_20;
                                        _pre_phi62 = var_24;
                                        local_sp_1 = var_23;
                                        rdx1_1 = (uint64_t)*(unsigned char *)(local_sp_0 + 11UL);
                                    } else {
                                        *(unsigned char *)(local_sp_0 + 19UL) = var_15;
                                        *(uint32_t *)(local_sp_0 + 20UL) = 0U;
                                    }
                                }
                            } else {
                                var_20 = *(uint64_t *)(local_sp_0 + 8UL);
                                var_21 = local_sp_0 + (-8L);
                                var_22 = (uint64_t *)var_21;
                                *var_22 = 4223088UL;
                                indirect_placeholder();
                                _pre_phi62 = var_22;
                                local_sp_1 = var_21;
                                if ((int)(uint32_t)var_20 < (int)0U) {
                                    *(unsigned char *)(local_sp_0 + 19UL) = var_15;
                                    *(uint32_t *)(local_sp_0 + 20UL) = 0U;
                                } else {
                                    var_23 = local_sp_0 + (-16L);
                                    var_24 = (uint64_t *)var_23;
                                    *var_24 = 4223302UL;
                                    indirect_placeholder();
                                    *(uint32_t *)(local_sp_0 + 12UL) = *(uint32_t *)var_20;
                                    _pre_phi62 = var_24;
                                    local_sp_1 = var_23;
                                    rdx1_1 = (uint64_t)*(unsigned char *)(local_sp_0 + 11UL);
                                }
                            }
                        } else {
                            var_20 = *(uint64_t *)(local_sp_0 + 8UL);
                            var_21 = local_sp_0 + (-8L);
                            var_22 = (uint64_t *)var_21;
                            *var_22 = 4223088UL;
                            indirect_placeholder();
                            _pre_phi62 = var_22;
                            local_sp_1 = var_21;
                            if ((int)(uint32_t)var_20 < (int)0U) {
                                *(unsigned char *)(local_sp_0 + 19UL) = var_15;
                                *(uint32_t *)(local_sp_0 + 20UL) = 0U;
                            } else {
                                var_23 = local_sp_0 + (-16L);
                                var_24 = (uint64_t *)var_23;
                                *var_24 = 4223302UL;
                                indirect_placeholder();
                                *(uint32_t *)(local_sp_0 + 12UL) = *(uint32_t *)var_20;
                                _pre_phi62 = var_24;
                                local_sp_1 = var_23;
                                rdx1_1 = (uint64_t)*(unsigned char *)(local_sp_0 + 11UL);
                            }
                        }
                    }
                    var_25 = *_pre_phi62;
                    var_26 = local_sp_1 + (-8L);
                    *(uint64_t *)var_26 = 4223131UL;
                    var_27 = indirect_placeholder_3(rdx1_1, var_25, r14_0, 0UL);
                    var_28 = (uint32_t)var_27;
                    local_sp_2 = var_26;
                    if ((uint64_t)(var_28 + 1U) != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_29 = (uint64_t)var_28;
                    *(unsigned char *)r13_0 = (unsigned char)'/';
                    if (var_29 != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    _pre = *_cast1;
                    _pre_phi = (uint64_t)_pre;
                    var_32 = _pre;
                }
                r14_4 = r14_1;
                rbp_3 = rbp_1;
                rdx1_2 = _pre_phi;
                local_sp_3 = local_sp_2;
                r14_3 = r14_1;
                if (var_32 != '\x00') {
                    loop_state_var = 1U;
                    break;
                }
            }
            var_33 = var_13 ? r13_0 : rbx_0;
            r13_2 = var_33;
        }
    switch (loop_state_var) {
      case 1U:
        {
            break;
        }
        break;
      case 0U:
        {
            *(uint64_t *)(local_sp_1 + (-16L)) = 4223335UL;
            indirect_placeholder();
            var_30 = (uint32_t *)var_27;
            if (*(uint32_t *)(local_sp_1 + 20UL) != 0U & *var_30 == 2U) {
                *(uint64_t *)(local_sp_1 + (-24L)) = 4223345UL;
                indirect_placeholder();
                *var_30 = *(uint32_t *)(local_sp_1 + 4UL);
            }
            var_31 = (uint64_t)((long)(var_27 << 32UL) >> (long)32UL);
            storemerge = var_31;
            return storemerge;
        }
        break;
    }
}
