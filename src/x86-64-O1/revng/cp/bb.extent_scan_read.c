typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_extent_scan_read_ret_type;
struct bb_extent_scan_read_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_2(void);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r10(void);
extern uint64_t init_r9(void);
extern uint32_t init_state_0x82fc(void);
extern uint64_t init_r8(void);
typedef _Bool bool;
struct bb_extent_scan_read_ret_type bb_extent_scan_read(uint64_t rdi) {
    uint64_t var_32;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint32_t var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t *var_15;
    uint32_t *var_16;
    uint64_t *var_17;
    unsigned char *var_18;
    uint64_t rbp_3;
    uint64_t r14_1;
    uint64_t rbp_1;
    uint64_t rbp_0;
    uint64_t local_sp_4;
    uint64_t rcx_2;
    uint64_t r14_0;
    uint64_t local_sp_3;
    uint64_t r13_1;
    uint64_t rcx_0;
    uint64_t r13_0;
    uint64_t local_sp_0;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t *var_36;
    uint64_t var_37;
    uint64_t *var_38;
    uint64_t rcx_1;
    uint64_t local_sp_1;
    uint32_t var_40;
    uint64_t var_41;
    uint32_t *var_42;
    uint32_t var_43;
    uint32_t *var_44;
    uint64_t *var_45;
    uint64_t *_cast1_pre_phi;
    uint64_t var_46;
    uint64_t *var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t *_pre_phi126;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_57;
    uint32_t *_pre_phi131;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t rax_0;
    uint64_t var_58;
    uint64_t rbp_2;
    uint64_t rcx_6;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_39;
    uint64_t r14_2;
    uint64_t rcx_3;
    uint64_t local_sp_2;
    uint32_t var_61;
    uint64_t var_62;
    uint64_t _pre_phi120;
    uint64_t _pre_phi122;
    uint32_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint32_t _pre_phi121;
    uint64_t var_19;
    uint32_t var_20;
    uint64_t var_21;
    uint32_t var_22;
    uint64_t rax_1;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_27;
    uint64_t rcx_5;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t r14_3;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_33;
    uint64_t var_26;
    uint64_t var_69;
    struct bb_extent_scan_read_ret_type mrv;
    struct bb_extent_scan_read_ret_type mrv1;
    struct bb_extent_scan_read_ret_type mrv2;
    struct bb_extent_scan_read_ret_type mrv3;
    struct bb_extent_scan_read_ret_type mrv4;
    uint64_t rdi5_0;
    uint64_t rcx_7;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_r10();
    var_8 = init_r9();
    var_9 = init_r8();
    var_10 = init_state_0x82fc();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_11 = var_0 + (-4152L);
    var_12 = (uint64_t *)(rdi + 40UL);
    var_13 = *var_12;
    var_14 = (uint64_t)var_10 << 3UL;
    var_15 = (uint64_t *)(rdi + 8UL);
    var_16 = (uint32_t *)(rdi + 16UL);
    var_17 = (uint64_t *)(rdi + 24UL);
    var_18 = (unsigned char *)(rdi + 33UL);
    rbp_3 = var_13;
    local_sp_4 = var_11;
    r13_0 = 0UL;
    rax_1 = 1UL;
    r14_3 = 0UL;
    rcx_7 = 512UL;
    while (1U)
        {
            r14_0 = r14_3;
            r14_2 = r14_3;
            rdi5_0 = local_sp_4;
            while (rcx_7 != 0UL)
                {
                    *(uint64_t *)rdi5_0 = 0UL;
                    rdi5_0 = var_14 + rdi5_0;
                    rcx_7 = rcx_7 + (-1L);
                }
            var_19 = *var_15;
            *(uint64_t *)local_sp_4 = var_19;
            var_20 = *var_16;
            *(uint32_t *)(local_sp_4 + 16UL) = var_20;
            *(uint32_t *)(local_sp_4 + 24UL) = 72U;
            *(uint64_t *)(local_sp_4 + 8UL) = (var_19 ^ (-1L));
            var_21 = local_sp_4 + (-8L);
            *(uint64_t *)var_21 = 4235907UL;
            indirect_placeholder_2();
            var_22 = *(uint32_t *)(local_sp_4 + 12UL);
            local_sp_3 = var_21;
            if (var_22 != 0U) {
                var_69 = (uint64_t)var_20;
                *var_18 = (unsigned char)'\x01';
                rcx_6 = var_69;
                rax_1 = (*var_15 != 0UL);
                loop_state_var = 1U;
                break;
            }
            var_23 = (uint64_t)var_20;
            var_24 = (uint64_t)var_22 ^ (-1L);
            var_25 = *var_17;
            var_27 = var_25;
            rcx_5 = var_23;
            if (var_25 > var_24) {
                var_26 = local_sp_4 + (-16L);
                *(uint64_t *)var_26 = 4236066UL;
                indirect_placeholder_2();
                var_27 = *var_17;
                rcx_5 = 4323952UL;
                local_sp_3 = var_26;
            }
            var_28 = var_27 + (uint64_t)*(uint32_t *)(local_sp_3 + 20UL);
            *var_17 = var_28;
            var_29 = *var_12;
            var_30 = rbp_3 - var_29;
            var_31 = local_sp_3 + (-8L);
            *(uint64_t *)var_31 = 4235963UL;
            var_32 = indirect_placeholder_5(24UL, var_29, var_28, var_8, var_9);
            *var_12 = var_32;
            var_33 = var_30 + var_32;
            rbp_0 = var_33;
            rcx_0 = rcx_5;
            local_sp_0 = var_31;
            rbp_2 = var_33;
            rcx_3 = rcx_5;
            local_sp_2 = var_31;
            if (*(uint32_t *)(local_sp_3 + 12UL) == 0U) {
                rcx_6 = rcx_3;
                rbp_3 = rbp_2;
                r14_3 = r14_2;
                local_sp_4 = local_sp_2;
                if ((*(unsigned char *)(rbp_2 + 16UL) & '\x01') != '\x00') {
                    *var_18 = (unsigned char)'\x01';
                    var_61 = (uint32_t)r14_2;
                    var_62 = (uint64_t)var_61;
                    _pre_phi120 = var_62;
                    _pre_phi122 = var_62;
                    _pre_phi121 = var_61;
                    if (var_62 <= 72UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    *var_17 = _pre_phi122;
                    if (*var_18 != '\x00') {
                        loop_state_var = 1U;
                        break;
                    }
                    *var_15 = (*(uint64_t *)(rbp_2 + 8UL) + *(uint64_t *)rbp_2);
                    if ((uint64_t)(_pre_phi121 & (-8)) > 71UL) {
                        continue;
                    }
                    loop_state_var = 1U;
                    break;
                }
                var_63 = (uint32_t)r14_2;
                var_64 = (uint64_t)var_63;
                _pre_phi120 = var_64;
                _pre_phi122 = var_64;
                _pre_phi121 = var_63;
                if (var_64 <= 72UL) {
                    if (*var_18 != '\x00') {
                        loop_state_var = 0U;
                        break;
                    }
                    var_65 = (uint64_t)(var_63 + (-1));
                    var_66 = var_65 * 3UL;
                    var_67 = *var_12;
                    *var_17 = var_65;
                    var_68 = (var_65 * 24UL) + var_67;
                    *var_15 = (*(uint64_t *)(var_68 + (-24L)) + *(uint64_t *)(var_68 + (-16L)));
                    rcx_6 = var_66;
                    loop_state_var = 1U;
                    break;
                }
            }
            while (1U)
                {
                    var_34 = (r13_0 * 56UL) + local_sp_0;
                    var_35 = var_34 + 32UL;
                    var_36 = (uint64_t *)(var_34 + 48UL);
                    var_37 = 9223372036854775807UL - *var_36;
                    var_38 = (uint64_t *)var_35;
                    r14_1 = r14_0;
                    rbp_1 = rbp_0;
                    r13_1 = r13_0;
                    rcx_1 = rcx_0;
                    local_sp_1 = local_sp_0;
                    rax_1 = 0UL;
                    if (*var_38 > var_37) {
                        var_39 = local_sp_0 + (-8L);
                        *(uint64_t *)var_39 = 4236096UL;
                        indirect_placeholder_2();
                        rcx_1 = 4323952UL;
                        local_sp_1 = var_39;
                    }
                    var_40 = (uint32_t)r14_0;
                    var_41 = (uint64_t)var_40;
                    local_sp_0 = local_sp_1;
                    rcx_2 = rcx_1;
                    local_sp_2 = local_sp_1;
                    if (var_41 != 0UL) {
                        var_42 = (uint32_t *)(var_34 + 72UL);
                        var_43 = *var_42 & (-2);
                        var_44 = (uint32_t *)(rbp_0 + 16UL);
                        _pre_phi131 = var_42;
                        if ((uint64_t)(*var_44 - var_43) == 0UL) {
                            var_45 = (uint64_t *)(rbp_0 + 8UL);
                            var_46 = *var_45;
                            var_47 = (uint64_t *)rbp_0;
                            var_48 = var_46 + *var_47;
                            var_49 = *var_38;
                            _cast1_pre_phi = var_47;
                            _pre_phi126 = var_45;
                            var_50 = var_49;
                            if (var_48 != var_49) {
                                *var_45 = (var_46 + *var_36);
                                *var_44 = *var_42;
                                var_60 = (uint64_t)((uint32_t)r13_1 + 1U);
                                rbp_0 = rbp_1;
                                r14_0 = r14_1;
                                rcx_0 = rcx_2;
                                r13_0 = var_60;
                                rbp_2 = rbp_1;
                                r14_2 = r14_1;
                                rcx_3 = rcx_2;
                                if (var_60 < (uint64_t)*(uint32_t *)(local_sp_1 + 20UL)) {
                                    continue;
                                }
                                loop_state_var = 0U;
                                break;
                            }
                        }
                        _cast1_pre_phi = (uint64_t *)rbp_0;
                        _pre_phi126 = (uint64_t *)(rbp_0 + 8UL);
                        var_50 = *var_38;
                        var_51 = *_pre_phi126 + *_cast1_pre_phi;
                        var_54 = var_50;
                        var_57 = var_50;
                        rax_0 = var_51;
                        if (var_51 <= var_50) {
                            var_58 = *var_36;
                            rcx_2 = var_58;
                            rcx_6 = var_58;
                            if (var_58 <= (rax_0 - var_57)) {
                                if (*var_15 != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                *(unsigned char *)(rdi + 32UL) = (unsigned char)'\x01';
                                loop_state_var = 1U;
                                break;
                            }
                            *var_38 = rax_0;
                            *var_36 = ((var_57 + var_58) - rax_0);
                            var_59 = (uint64_t)((uint32_t)r13_0 + (-1));
                            r13_1 = var_59;
                            var_60 = (uint64_t)((uint32_t)r13_1 + 1U);
                            rbp_0 = rbp_1;
                            r14_0 = r14_1;
                            rcx_0 = rcx_2;
                            r13_0 = var_60;
                            rbp_2 = rbp_1;
                            r14_2 = r14_1;
                            rcx_3 = rcx_2;
                            if (var_60 < (uint64_t)*(uint32_t *)(local_sp_1 + 20UL)) {
                                continue;
                            }
                            loop_state_var = 0U;
                            break;
                        }
                    }
                    var_52 = *var_15;
                    var_53 = *var_38;
                    var_54 = var_53;
                    var_57 = var_53;
                    rax_0 = var_52;
                    if (var_52 <= var_53) {
                        var_58 = *var_36;
                        rcx_2 = var_58;
                        rcx_6 = var_58;
                        if (var_58 <= (rax_0 - var_57)) {
                            if (*var_15 != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            *(unsigned char *)(rdi + 32UL) = (unsigned char)'\x01';
                            loop_state_var = 1U;
                            break;
                        }
                        *var_38 = rax_0;
                        *var_36 = ((var_57 + var_58) - rax_0);
                        var_59 = (uint64_t)((uint32_t)r13_0 + (-1));
                        r13_1 = var_59;
                        var_60 = (uint64_t)((uint32_t)r13_1 + 1U);
                        rbp_0 = rbp_1;
                        r14_0 = r14_1;
                        rcx_0 = rcx_2;
                        r13_0 = var_60;
                        rbp_2 = rbp_1;
                        r14_2 = r14_1;
                        rcx_3 = rcx_2;
                        if (var_60 < (uint64_t)*(uint32_t *)(local_sp_1 + 20UL)) {
                            continue;
                        }
                        loop_state_var = 0U;
                        break;
                    }
                    _pre_phi131 = (uint32_t *)(var_34 + 72UL);
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    break;
                }
                break;
              case 1U:
                {
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *var_17 = _pre_phi120;
        }
        break;
      case 1U:
        {
            mrv.field_0 = rax_1;
            mrv1 = mrv;
            mrv1.field_1 = rcx_6;
            mrv2 = mrv1;
            mrv2.field_2 = var_7;
            mrv3 = mrv2;
            mrv3.field_3 = var_8;
            mrv4 = mrv3;
            mrv4.field_4 = var_9;
            return mrv4;
        }
        break;
    }
}
