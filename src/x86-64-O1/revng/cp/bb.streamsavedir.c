typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_302_ret_type;
struct indirect_placeholder_301_ret_type;
struct indirect_placeholder_303_ret_type;
struct indirect_placeholder_304_ret_type;
struct indirect_placeholder_302_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_301_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_303_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_304_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_2(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_4(uint64_t param_0);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r10(void);
extern uint64_t init_r9(void);
extern uint32_t init_state_0x82fc(void);
extern uint64_t init_r8(void);
extern void indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_302_ret_type indirect_placeholder_302(uint64_t param_0);
extern struct indirect_placeholder_301_ret_type indirect_placeholder_301(uint64_t param_0);
extern struct indirect_placeholder_303_ret_type indirect_placeholder_303(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_304_ret_type indirect_placeholder_304(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
uint64_t bb_streamsavedir(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint32_t var_11;
    struct indirect_placeholder_303_ret_type var_27;
    struct indirect_placeholder_304_ret_type var_39;
    uint64_t var_12;
    uint64_t r14_5;
    uint64_t rbx_0;
    uint64_t rbp_0;
    uint64_t rax_0;
    uint64_t local_sp_0;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t rbp_5;
    uint64_t rbx_3;
    uint64_t r14_4_ph;
    uint64_t rbp_1;
    uint64_t r14_0;
    uint64_t local_sp_1;
    uint64_t rbp_2;
    uint64_t rbp_6_ph;
    uint64_t var_57;
    uint64_t var_58;
    struct indirect_placeholder_302_ret_type var_59;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    struct indirect_placeholder_301_ret_type var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t rax_4_ph;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t r14_1;
    uint64_t rbp_4;
    uint64_t rbx_2;
    uint64_t rbx_1;
    uint64_t r15_0;
    uint64_t r14_3;
    uint64_t local_sp_3;
    uint64_t rbp_3;
    uint64_t rax_3;
    uint64_t r14_2;
    uint64_t local_sp_4;
    uint64_t rax_1;
    uint64_t local_sp_2;
    uint64_t r15_1_ph;
    uint64_t local_sp_5;
    uint64_t var_19;
    uint64_t rax_2;
    uint32_t var_45;
    uint64_t rax_4;
    uint64_t rdi1_0;
    uint64_t rcx_0;
    uint64_t rcx_1;
    unsigned char var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_23;
    uint64_t *var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    bool var_36;
    uint64_t var_37;
    uint64_t *var_38;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t local_sp_5_ph;
    uint64_t *var_15;
    uint32_t *var_16;
    uint64_t var_17;
    uint64_t *var_18;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_r10();
    var_8 = init_cc_src2();
    var_9 = init_r9();
    var_10 = init_r8();
    var_11 = init_state_0x82fc();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_12 = *(uint64_t *)(((rsi << 3UL) & 34359738360UL) + 4328096UL);
    *(uint64_t *)(var_0 + (-112L)) = var_12;
    r14_5 = 0UL;
    rbp_0 = 0UL;
    r14_4_ph = 0UL;
    rbp_1 = 0UL;
    rbp_6_ph = 0UL;
    rax_4_ph = var_12;
    r15_1_ph = 0UL;
    rax_2 = 0UL;
    rcx_0 = 18446744073709551615UL;
    rcx_1 = 0UL;
    if (rdi == 0UL) {
        return r14_5;
    }
    var_13 = var_0 + (-120L);
    *(uint64_t *)(var_0 + (-96L)) = 0UL;
    *(uint64_t *)(var_0 + (-104L)) = 0UL;
    *(uint64_t *)(var_0 + (-88L)) = 0UL;
    var_14 = (uint64_t)var_11;
    local_sp_5_ph = var_13;
    while (1U)
        {
            rbp_5 = rbp_6_ph;
            rbp_2 = rbp_6_ph;
            r14_1 = r14_4_ph;
            rbp_4 = rbp_6_ph;
            r15_0 = r15_1_ph;
            r14_3 = r14_4_ph;
            r14_2 = r14_4_ph;
            local_sp_5 = local_sp_5_ph;
            rax_4 = rax_4_ph;
            while (1U)
                {
                    var_15 = (uint64_t *)(local_sp_5 + (-8L));
                    *var_15 = 4254685UL;
                    indirect_placeholder_2();
                    var_16 = (uint32_t *)rax_4;
                    *var_16 = 0U;
                    var_17 = local_sp_5 + (-16L);
                    var_18 = (uint64_t *)var_17;
                    *var_18 = 4254699UL;
                    indirect_placeholder_2();
                    local_sp_3 = var_17;
                    local_sp_4 = var_17;
                    local_sp_5 = var_17;
                    if (rax_4 == 0UL) {
                        var_19 = rax_4 + 19UL;
                        rdi1_0 = var_19;
                        if (*(unsigned char *)var_19 == '.') {
                            rax_2 = (*(unsigned char *)(rax_4 + 20UL) == '.') ? 2UL : 1UL;
                        }
                        rax_4 = rax_2;
                        if (*(unsigned char *)((rax_2 + rax_4) + 19UL) == '\x00') {
                            continue;
                        }
                        loop_state_var = 0U;
                        break;
                    }
                    *(uint64_t *)(local_sp_5 + (-24L)) = 4254958UL;
                    indirect_placeholder_2();
                    var_45 = *var_16;
                    if (var_45 == 0U) {
                        *(uint64_t *)(local_sp_5 + (-32L)) = 4255010UL;
                        indirect_placeholder_2();
                        *(uint64_t *)(local_sp_5 + (-40L)) = 4255018UL;
                        indirect_placeholder_2();
                        *(uint64_t *)(local_sp_5 + (-48L)) = 4255023UL;
                        indirect_placeholder_2();
                        *var_16 = var_45;
                        loop_state_var = 1U;
                        break;
                    }
                    if (*var_18 == 0UL) {
                        if (rbp_6_ph != *(uint64_t *)(local_sp_5 + 8UL)) {
                            loop_state_var = 3U;
                            break;
                        }
                        var_60 = rbp_6_ph + 1UL;
                        *(uint64_t *)(local_sp_5 + (-32L)) = 4255170UL;
                        var_61 = indirect_placeholder(r14_4_ph, var_60);
                        r14_1 = var_61;
                        loop_state_var = 3U;
                        break;
                    }
                    if (r15_1_ph != 0UL) {
                        var_57 = rbp_6_ph + 1UL;
                        var_58 = local_sp_5 + (-32L);
                        *(uint64_t *)var_58 = 4254992UL;
                        var_59 = indirect_placeholder_302(var_57);
                        r14_0 = var_59.field_0;
                        local_sp_1 = var_58;
                        loop_state_var = 4U;
                        break;
                    }
                    var_46 = *var_15;
                    *(uint64_t *)(local_sp_5 + (-32L)) = 4255057UL;
                    indirect_placeholder_2();
                    var_47 = rbp_6_ph + 1UL;
                    var_48 = local_sp_5 + (-40L);
                    *(uint64_t *)var_48 = 4255066UL;
                    var_49 = indirect_placeholder_301(var_47);
                    var_50 = var_49.field_0;
                    var_51 = (r15_1_ph << 3UL) + var_46;
                    rbx_0 = var_46;
                    rax_0 = var_50;
                    local_sp_0 = var_48;
                    r14_0 = var_50;
                    loop_state_var = 2U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    while (rcx_0 != 0UL)
                        {
                            var_20 = *(unsigned char *)rdi1_0;
                            var_21 = rcx_0 + (-1L);
                            rcx_0 = var_21;
                            rcx_1 = var_21;
                            if (var_20 == '\x00') {
                                break;
                            }
                            rdi1_0 = rdi1_0 + var_14;
                        }
                    var_22 = rcx_1 ^ (-1L);
                    rbx_2 = var_22;
                    rbx_3 = var_22;
                    if (*var_15 == 0UL) {
                        var_32 = *(uint64_t *)(local_sp_5 + 16UL) - rbp_6_ph;
                        rax_3 = var_32;
                        if (var_32 <= var_22) {
                            var_33 = rbp_6_ph + var_22;
                            var_34 = local_sp_5 + 40UL;
                            *(uint64_t *)var_34 = var_33;
                            var_35 = helper_cc_compute_c_wrapper(var_22, rbp_6_ph, var_8, 17U);
                            var_36 = (var_35 == 0UL);
                            var_37 = local_sp_5 + (-24L);
                            var_38 = (uint64_t *)var_37;
                            local_sp_4 = var_37;
                            if (!var_36) {
                                *var_38 = 4254953UL;
                                indirect_placeholder_1(var_9, var_10);
                                abort();
                            }
                            *var_38 = 4254915UL;
                            var_39 = indirect_placeholder_304(1UL, var_22, r14_4_ph, rbp_6_ph, var_34, var_22, var_7);
                            var_40 = var_39.field_0;
                            var_41 = var_39.field_1;
                            var_42 = var_39.field_2;
                            var_43 = *(uint64_t *)(local_sp_5 + 32UL);
                            *(uint64_t *)(local_sp_5 + 8UL) = var_43;
                            rbx_3 = var_41;
                            rbp_5 = var_42;
                            r14_3 = var_40;
                            rax_3 = var_43;
                        }
                        var_44 = local_sp_4 + (-8L);
                        *(uint64_t *)var_44 = 4254943UL;
                        indirect_placeholder_2();
                        rbx_1 = rbx_3;
                        rbp_3 = rbp_5;
                        r14_2 = r14_3;
                        rax_1 = rax_3;
                        local_sp_2 = var_44;
                    } else {
                        if (*(uint64_t *)(local_sp_5 + 8UL) == r15_1_ph) {
                            var_23 = local_sp_5 + 40UL;
                            *(uint64_t *)var_23 = r15_1_ph;
                            var_24 = (uint64_t *)local_sp_5;
                            var_25 = *var_24;
                            var_26 = local_sp_5 + (-24L);
                            *(uint64_t *)var_26 = 4254852UL;
                            var_27 = indirect_placeholder_303(8UL, var_22, var_25, rbp_6_ph, var_23, var_22, var_7);
                            var_28 = var_27.field_1;
                            var_29 = var_27.field_2;
                            *var_15 = var_27.field_0;
                            *var_24 = *(uint64_t *)(local_sp_5 + 32UL);
                            rbx_2 = var_28;
                            rbp_4 = var_29;
                            local_sp_3 = var_26;
                        }
                        *(uint64_t *)(local_sp_3 + 40UL) = ((r15_1_ph << 3UL) + *(uint64_t *)(local_sp_3 + 16UL));
                        var_30 = local_sp_3 + (-8L);
                        *(uint64_t *)var_30 = 4254802UL;
                        var_31 = indirect_placeholder_4(var_19);
                        **(uint64_t **)(local_sp_3 + 32UL) = var_31;
                        rbx_1 = rbx_2;
                        r15_0 = r15_1_ph + 1UL;
                        rbp_3 = rbp_4;
                        rax_1 = var_31;
                        local_sp_2 = var_30;
                    }
                    r14_4_ph = r14_2;
                    rbp_6_ph = rbp_3 + rbx_1;
                    rax_4_ph = rax_1;
                    r15_1_ph = r15_0;
                    local_sp_5_ph = local_sp_2;
                    continue;
                }
                break;
              case 1U:
                {
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 2U:
                {
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 3U:
                {
                    loop_state_var = 2U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 4U:
                {
                    loop_state_var = 3U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 2U:
        {
            *(unsigned char *)(rbp_2 + r14_1) = (unsigned char)'\x00';
            r14_5 = r14_1;
        }
        break;
      case 3U:
      case 1U:
        {
            switch (loop_state_var) {
              case 1U:
                {
                    var_52 = rbp_0 + var_50;
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4255093UL;
                    indirect_placeholder_2();
                    var_53 = rax_0 - var_52;
                    var_54 = (var_53 + rbp_0) + 1UL;
                    var_55 = local_sp_0 + (-16L);
                    *(uint64_t *)var_55 = 4255109UL;
                    indirect_placeholder_2();
                    var_56 = rbx_0 + 8UL;
                    rbx_0 = var_56;
                    rbp_0 = var_54;
                    rax_0 = var_53;
                    local_sp_0 = var_55;
                    rbp_1 = var_54;
                    local_sp_1 = var_55;
                    do {
                        var_52 = rbp_0 + var_50;
                        *(uint64_t *)(local_sp_0 + (-8L)) = 4255093UL;
                        indirect_placeholder_2();
                        var_53 = rax_0 - var_52;
                        var_54 = (var_53 + rbp_0) + 1UL;
                        var_55 = local_sp_0 + (-16L);
                        *(uint64_t *)var_55 = 4255109UL;
                        indirect_placeholder_2();
                        var_56 = rbx_0 + 8UL;
                        rbx_0 = var_56;
                        rbp_0 = var_54;
                        rax_0 = var_53;
                        local_sp_0 = var_55;
                        rbp_1 = var_54;
                        local_sp_1 = var_55;
                    } while (var_56 != var_51);
                }
                break;
              case 3U:
                {
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4255128UL;
                    indirect_placeholder_2();
                    rbp_2 = rbp_1;
                    r14_1 = r14_0;
                }
                break;
            }
        }
        break;
    }
}
