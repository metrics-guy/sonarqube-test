typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_115_ret_type;
struct indirect_placeholder_114_ret_type;
struct indirect_placeholder_117_ret_type;
struct indirect_placeholder_116_ret_type;
struct indirect_placeholder_118_ret_type;
struct indirect_placeholder_113_ret_type;
struct indirect_placeholder_115_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_114_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_117_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_116_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_118_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_113_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_2(void);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_6(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_115_ret_type indirect_placeholder_115(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_114_ret_type indirect_placeholder_114(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_117_ret_type indirect_placeholder_117(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_116_ret_type indirect_placeholder_116(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_118_ret_type indirect_placeholder_118(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_113_ret_type indirect_placeholder_113(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
uint64_t bb_sparse_copy(uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t rcx, uint64_t r9, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t rbp_5;
    uint64_t rdx1_0;
    uint64_t local_sp_9;
    uint64_t rcx4_0;
    uint64_t local_sp_0;
    uint64_t rcx4_8;
    bool var_60;
    bool var_61;
    uint64_t r12_0;
    uint64_t r14_0;
    uint64_t local_sp_7;
    unsigned char var_62;
    uint64_t rdx1_2;
    unsigned char *_pre_phi189;
    unsigned char var_63;
    unsigned char var_64;
    uint64_t var_54;
    struct indirect_placeholder_115_ret_type var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_81;
    struct indirect_placeholder_117_ret_type var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t r15_4;
    uint64_t rcx4_6;
    uint64_t *var_15;
    uint64_t var_16;
    uint64_t rdx1_3;
    uint64_t var_80;
    uint64_t r15_5;
    uint64_t *_pre_phi185;
    uint64_t var_8;
    uint64_t rdx1_1;
    uint64_t rbp_0;
    uint64_t rcx4_1;
    uint64_t local_sp_1;
    uint64_t rbp_2;
    uint64_t var_65;
    unsigned char r13_0_in;
    uint64_t rcx4_7;
    uint64_t local_sp_8;
    uint64_t var_71;
    unsigned char var_72;
    uint64_t rbx_0;
    uint64_t r15_0;
    uint64_t rbp_1;
    uint64_t r12_1;
    uint64_t rcx4_2;
    uint64_t local_sp_2;
    uint64_t var_73;
    uint64_t r13_1;
    uint64_t rbx_1;
    uint64_t rbp_6;
    uint64_t r15_1;
    uint64_t r12_2;
    uint64_t rcx4_3;
    uint64_t local_sp_3;
    uint64_t var_17;
    bool var_18;
    bool var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    unsigned char *var_23;
    unsigned char var_24;
    unsigned char var_25;
    uint64_t var_26;
    unsigned char var_27;
    uint64_t var_28;
    uint64_t var_29;
    unsigned char var_30;
    uint64_t var_31;
    uint64_t var_32;
    unsigned char *var_33;
    unsigned char var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    bool var_40;
    unsigned char *var_41;
    unsigned char var_42;
    unsigned char _pre_pre_phi;
    unsigned char *var_74;
    uint64_t r15_2;
    uint64_t rcx4_4;
    uint64_t local_sp_4;
    uint64_t var_75;
    uint64_t *var_76;
    unsigned char *var_77;
    unsigned char var_78;
    uint64_t var_79;
    uint64_t r15_3;
    uint64_t rbp_3;
    uint64_t rcx4_5;
    uint64_t local_sp_5;
    uint64_t rbp_4;
    uint64_t local_sp_6;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t *var_9;
    uint64_t var_10;
    uint64_t *var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_66;
    struct indirect_placeholder_118_ret_type var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    unsigned char _pre_phi;
    uint64_t rbp_7;
    uint64_t rbp_8;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    *(uint32_t *)(var_0 + (-64L)) = (uint32_t)rdi;
    *(uint32_t *)(var_0 + (-104L)) = (uint32_t)rsi;
    *(uint64_t *)(var_0 + (-96L)) = rdx;
    *(uint64_t *)(var_0 + (-80L)) = rcx;
    *(uint64_t *)(var_0 + (-120L)) = r8;
    *(uint32_t *)(var_0 + (-60L)) = (uint32_t)r9;
    *(unsigned char *)(var_0 + (-97L)) = (unsigned char)r9;
    **(unsigned char **)(var_0 | 40UL) = (unsigned char)'\x00';
    **(uint64_t **)(var_0 | 32UL) = 0UL;
    rbp_5 = 1UL;
    r15_5 = 0UL;
    rbp_6 = 0UL;
    rcx4_6 = rcx;
    if (*(uint64_t *)(var_0 | 24UL) == 0UL) {
        return rbp_5;
    }
    var_8 = var_0 + (-136L);
    *(uint64_t *)(var_0 + (-72L)) = ((r8 == 0UL) ? rcx : r8);
    rbp_5 = 0UL;
    local_sp_7 = var_8;
    while (1U)
        {
            var_9 = (uint64_t *)(local_sp_7 + 56UL);
            var_10 = *var_9;
            var_11 = (uint64_t *)(local_sp_7 + 160UL);
            var_12 = *var_11;
            var_13 = (var_10 > var_12) ? var_12 : var_10;
            var_14 = local_sp_7 + (-8L);
            *(uint64_t *)var_14 = 4219450UL;
            indirect_placeholder_2();
            *(uint64_t *)(local_sp_7 + 40UL) = var_10;
            r15_4 = r15_5;
            rdx1_3 = var_13;
            rbp_2 = rbp_6;
            r15_1 = r15_5;
            r12_2 = var_10;
            rcx4_3 = rcx4_6;
            local_sp_3 = var_14;
            r15_3 = r15_5;
            rbp_3 = rbp_6;
            rcx4_5 = rcx4_6;
            rbp_4 = rbp_6;
            local_sp_6 = var_14;
            if ((long)var_10 < (long)0UL) {
                var_80 = local_sp_7 + (-16L);
                *(uint64_t *)var_80 = 4219513UL;
                indirect_placeholder_2();
                local_sp_5 = var_80;
                if (*(uint32_t *)var_10 != 4U) {
                    var_81 = *(uint64_t *)(local_sp_7 + 128UL);
                    *(uint64_t *)(local_sp_7 + (-24L)) = 4219540UL;
                    var_82 = indirect_placeholder_117(4UL, var_81);
                    var_83 = var_82.field_0;
                    var_84 = var_82.field_1;
                    var_85 = var_82.field_2;
                    *(uint64_t *)(local_sp_7 + (-32L)) = 4219548UL;
                    indirect_placeholder_2();
                    var_86 = (uint64_t)*(uint32_t *)var_83;
                    *(uint64_t *)(local_sp_7 + (-40L)) = 4219573UL;
                    indirect_placeholder_116(0UL, 4321471UL, 0UL, var_86, var_83, var_84, var_85);
                    loop_state_var = 1U;
                    break;
                }
                _pre_phi185 = (uint64_t *)(var_80 + 160UL);
            } else {
                if (var_10 != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                var_15 = (uint64_t *)*var_11;
                *var_15 = (*var_15 + var_10);
                var_16 = *(uint64_t *)(local_sp_7 + 32UL);
                *(uint64_t *)(local_sp_7 + 16UL) = var_16;
                r13_1 = (uint64_t)(uint32_t)rbp_6;
                rbx_1 = *var_9;
                r14_0 = var_16;
                while (1U)
                    {
                        var_17 = (rbx_1 > r12_2) ? r12_2 : rbx_1;
                        var_18 = (*(uint64_t *)(local_sp_3 + 16UL) == 0UL);
                        var_19 = (var_17 == 0UL);
                        rbp_5 = r13_1;
                        r12_0 = r12_2;
                        local_sp_1 = local_sp_3;
                        local_sp_8 = local_sp_3;
                        rbx_0 = var_17;
                        r15_0 = var_17;
                        r12_1 = r12_2;
                        r15_2 = var_17;
                        if (var_18 || var_19) {
                            var_33 = (unsigned char *)(local_sp_3 + 36UL);
                            var_34 = var_19;
                            *var_33 = var_34;
                            var_35 = (uint64_t)var_34;
                            var_36 = r13_1 ^ 1UL;
                            var_37 = (rdx1_3 & (-256L)) | (r12_2 <= rbx_1);
                            var_38 = var_36 & var_37;
                            var_39 = var_38 | var_35;
                            var_40 = (((var_38 & 1UL) | var_35) == 0UL);
                            var_41 = (unsigned char *)(local_sp_3 + 37UL);
                            var_42 = (unsigned char)r13_1;
                            *var_41 = var_42;
                            rdx1_1 = var_37;
                            rbp_0 = var_39;
                            rcx4_1 = var_35;
                            rcx4_7 = var_35;
                            _pre_pre_phi = var_42;
                            rbp_7 = var_39;
                            if (!var_40) {
                                var_65 = helper_cc_compute_c_wrapper((9223372036854775807UL - var_17) - r15_1, r15_1, var_7, 17U);
                                rbp_5 = 0UL;
                                rdx1_2 = rdx1_1;
                                rbp_1 = rbp_0;
                                rcx4_2 = rcx4_1;
                                local_sp_2 = local_sp_1;
                                if (var_65 == 0UL) {
                                    var_71 = r15_1 + var_17;
                                    var_72 = *(unsigned char *)(local_sp_1 + 37UL);
                                    r13_0_in = var_72;
                                    r15_0 = var_71;
                                    var_73 = r12_1 - rbx_0;
                                    rdx1_3 = rdx1_2;
                                    rbp_2 = rbp_1;
                                    rbx_1 = rbx_0;
                                    r15_1 = r15_0;
                                    r12_2 = var_73;
                                    rcx4_3 = rcx4_2;
                                    local_sp_3 = local_sp_2;
                                    r15_2 = r15_0;
                                    rcx4_4 = rcx4_2;
                                    local_sp_4 = local_sp_2;
                                    if (var_73 != 0UL) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    r13_1 = (uint64_t)r13_0_in;
                                    r14_0 = r14_0 + rbx_0;
                                    continue;
                                }
                                var_66 = *(uint64_t *)(local_sp_1 + 144UL);
                                *(uint64_t *)(local_sp_1 + (-8L)) = 4219835UL;
                                var_67 = indirect_placeholder_118(4UL, var_66);
                                var_68 = var_67.field_0;
                                var_69 = var_67.field_1;
                                var_70 = var_67.field_2;
                                *(uint64_t *)(local_sp_1 + (-16L)) = 4219863UL;
                                indirect_placeholder_113(0UL, 4321505UL, 0UL, 0UL, var_68, var_69, var_70);
                                loop_state_var = 2U;
                                break;
                            }
                            *(uint64_t *)(local_sp_8 + 8UL) = (var_17 + r15_1);
                            *(unsigned char *)(local_sp_8 + 38UL) = (unsigned char)'\x00';
                            _pre_phi = _pre_pre_phi;
                            rbp_8 = rbp_7;
                            rcx4_8 = rcx4_7;
                            local_sp_9 = local_sp_8;
                        } else {
                            var_20 = local_sp_3 + (-8L);
                            *(uint64_t *)var_20 = 4220155UL;
                            var_21 = indirect_placeholder(r14_0, var_17);
                            var_22 = (uint64_t)(uint32_t)var_21;
                            var_23 = (unsigned char *)(local_sp_3 + 29UL);
                            var_24 = (unsigned char)var_21;
                            *var_23 = var_24;
                            var_25 = (unsigned char)r13_1;
                            var_26 = rcx4_3 & (-256L);
                            var_27 = (unsigned char)((uint64_t)(var_24 - var_25) != 0UL);
                            var_28 = (rdx1_3 & (-256L)) | (r15_1 != 0UL);
                            var_29 = ((uint64_t)((uint32_t)rbp_2 & (-256)) | (r12_2 <= rbx_1)) & (var_22 ^ 1UL);
                            var_30 = var_27 & (unsigned char)var_28;
                            var_31 = (uint64_t)var_30 & 1UL;
                            var_32 = var_26 | var_31;
                            *(unsigned char *)(local_sp_3 + 30UL) = var_30;
                            local_sp_9 = var_20;
                            rcx4_8 = var_32;
                            rdx1_1 = var_28;
                            rbp_0 = var_29;
                            rcx4_1 = var_32;
                            local_sp_1 = var_20;
                            rcx4_7 = var_32;
                            local_sp_8 = var_20;
                            _pre_pre_phi = var_25;
                            _pre_phi = var_25;
                            rbp_7 = var_29;
                            rbp_8 = var_29;
                            if (!((var_31 == 0UL) && ((var_29 & 1UL) == 0UL))) {
                                var_65 = helper_cc_compute_c_wrapper((9223372036854775807UL - var_17) - r15_1, r15_1, var_7, 17U);
                                rbp_5 = 0UL;
                                rdx1_2 = rdx1_1;
                                rbp_1 = rbp_0;
                                rcx4_2 = rcx4_1;
                                local_sp_2 = local_sp_1;
                                if (var_65 == 0UL) {
                                    var_71 = r15_1 + var_17;
                                    var_72 = *(unsigned char *)(local_sp_1 + 37UL);
                                    r13_0_in = var_72;
                                    r15_0 = var_71;
                                    var_73 = r12_1 - rbx_0;
                                    rdx1_3 = rdx1_2;
                                    rbp_2 = rbp_1;
                                    rbx_1 = rbx_0;
                                    r15_1 = r15_0;
                                    r12_2 = var_73;
                                    rcx4_3 = rcx4_2;
                                    local_sp_3 = local_sp_2;
                                    r15_2 = r15_0;
                                    rcx4_4 = rcx4_2;
                                    local_sp_4 = local_sp_2;
                                    if (var_73 != 0UL) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    r13_1 = (uint64_t)r13_0_in;
                                    r14_0 = r14_0 + rbx_0;
                                    continue;
                                }
                                var_66 = *(uint64_t *)(local_sp_1 + 144UL);
                                *(uint64_t *)(local_sp_1 + (-8L)) = 4219835UL;
                                var_67 = indirect_placeholder_118(4UL, var_66);
                                var_68 = var_67.field_0;
                                var_69 = var_67.field_1;
                                var_70 = var_67.field_2;
                                *(uint64_t *)(local_sp_1 + (-16L)) = 4219863UL;
                                indirect_placeholder_113(0UL, 4321505UL, 0UL, 0UL, var_68, var_69, var_70);
                                loop_state_var = 2U;
                                break;
                            }
                            if (var_30 == '\x00') {
                                *(uint64_t *)local_sp_3 = r15_1;
                                *(unsigned char *)(local_sp_3 + 28UL) = (unsigned char)'\x00';
                            } else {
                                *(unsigned char *)(local_sp_3 + 28UL) = (unsigned char)'\x00';
                                *(uint64_t *)(local_sp_8 + 8UL) = (var_17 + r15_1);
                                *(unsigned char *)(local_sp_8 + 38UL) = (unsigned char)'\x00';
                                _pre_phi = _pre_pre_phi;
                                rbp_8 = rbp_7;
                                rcx4_8 = rcx4_7;
                                local_sp_9 = local_sp_8;
                            }
                        }
                        rcx4_0 = rcx4_8;
                        rbx_0 = 0UL;
                        rbp_1 = rbp_8;
                        if ((uint64_t)_pre_phi == 0UL) {
                            var_49 = *(uint64_t *)(local_sp_9 + 8UL);
                            var_50 = *(uint64_t *)(local_sp_9 + 24UL);
                            var_51 = (uint64_t)*(uint32_t *)(local_sp_9 + 32UL);
                            var_52 = local_sp_9 + (-8L);
                            *(uint64_t *)var_52 = 4219657UL;
                            var_53 = indirect_placeholder_6(var_49, var_51, var_50);
                            rdx1_0 = var_49;
                            local_sp_0 = var_52;
                            if (*(uint64_t *)local_sp_9 != var_53) {
                                var_54 = *(uint64_t *)(local_sp_9 + 144UL);
                                *(uint64_t *)(local_sp_9 + (-16L)) = 4219737UL;
                                var_55 = indirect_placeholder_115(4UL, var_54);
                                var_56 = var_55.field_0;
                                var_57 = var_55.field_1;
                                var_58 = var_55.field_2;
                                *(uint64_t *)(local_sp_9 + (-24L)) = 4219745UL;
                                indirect_placeholder_2();
                                var_59 = (uint64_t)*(uint32_t *)var_56;
                                *(uint64_t *)(local_sp_9 + (-32L)) = 4219770UL;
                                indirect_placeholder_114(0UL, 4321488UL, 0UL, var_59, var_56, var_57, var_58);
                                loop_state_var = 2U;
                                break;
                            }
                        }
                        var_43 = (uint64_t)*(unsigned char *)(local_sp_9 + 39UL);
                        var_44 = *(uint64_t *)(local_sp_9 + 8UL);
                        var_45 = *(uint64_t *)(local_sp_9 + 152UL);
                        var_46 = (uint64_t)*(uint32_t *)(local_sp_9 + 32UL);
                        var_47 = local_sp_9 + (-8L);
                        *(uint64_t *)var_47 = 4219802UL;
                        var_48 = indirect_placeholder_3(var_43, var_46, var_45, var_44);
                        rdx1_0 = var_43;
                        rcx4_0 = var_44;
                        local_sp_0 = var_47;
                        if ((uint64_t)(unsigned char)var_48 != 0UL) {
                            rbp_5 = (uint64_t)(uint32_t)var_48;
                            loop_state_var = 2U;
                            break;
                        }
                        rdx1_2 = rdx1_0;
                        rcx4_2 = rcx4_0;
                        local_sp_2 = local_sp_0;
                        rcx4_4 = rcx4_0;
                        local_sp_4 = local_sp_0;
                        if ((uint64_t)(unsigned char)rbp_8 == 0UL) {
                            var_64 = *(unsigned char *)(local_sp_0 + 37UL);
                            *(uint64_t *)(local_sp_0 + 24UL) = r14_0;
                            r13_0_in = var_64;
                        } else {
                            var_60 = (*(unsigned char *)(local_sp_0 + 36UL) == '\x00');
                            var_61 = (*(unsigned char *)(local_sp_0 + 38UL) == '\x00');
                            r15_0 = 0UL;
                            if (var_60) {
                                if (var_61) {
                                    var_63 = *(unsigned char *)(local_sp_0 + 37UL);
                                    *(uint64_t *)(local_sp_0 + 24UL) = r14_0;
                                    r13_0_in = var_63;
                                    r12_1 = r12_0;
                                } else {
                                    var_62 = *(unsigned char *)(local_sp_0 + 37UL);
                                    *(uint64_t *)(local_sp_0 + 24UL) = r14_0;
                                    r13_0_in = var_62;
                                }
                            } else {
                                r12_0 = 0UL;
                                if (!var_61) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                var_63 = *(unsigned char *)(local_sp_0 + 37UL);
                                *(uint64_t *)(local_sp_0 + 24UL) = r14_0;
                                r13_0_in = var_63;
                                r12_1 = r12_0;
                            }
                        }
                        var_73 = r12_1 - rbx_0;
                        rdx1_3 = rdx1_2;
                        rbp_2 = rbp_1;
                        rbx_1 = rbx_0;
                        r15_1 = r15_0;
                        r12_2 = var_73;
                        rcx4_3 = rcx4_2;
                        local_sp_3 = local_sp_2;
                        r15_2 = r15_0;
                        rcx4_4 = rcx4_2;
                        local_sp_4 = local_sp_2;
                        if (var_73 != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        r13_1 = (uint64_t)r13_0_in;
                        r14_0 = r14_0 + rbx_0;
                        continue;
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 1U:
                  case 0U:
                    {
                        switch (loop_state_var) {
                          case 1U:
                            {
                                _pre_phi189 = (unsigned char *)(local_sp_0 + 37UL);
                            }
                            break;
                          case 0U:
                            {
                                var_74 = (unsigned char *)(local_sp_2 + 37UL);
                                *var_74 = r13_0_in;
                                _pre_phi189 = var_74;
                            }
                            break;
                        }
                        var_75 = *(uint64_t *)(local_sp_4 + 48UL);
                        var_76 = (uint64_t *)(local_sp_4 + 160UL);
                        *var_76 = (*var_76 - var_75);
                        var_77 = *(unsigned char **)(local_sp_4 + 176UL);
                        var_78 = *_pre_phi189;
                        var_79 = (uint64_t)var_78;
                        *var_77 = var_78;
                        _pre_phi185 = var_76;
                        r15_3 = r15_2;
                        rbp_3 = var_79;
                        rcx4_5 = rcx4_4;
                        local_sp_5 = local_sp_4;
                    }
                    break;
                  case 2U:
                    {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
        }
    rbp_5 = 1UL;
    if ((uint64_t)(unsigned char)rbp_4 != 0UL) {
        return;
    }
    var_87 = (uint64_t)*(unsigned char *)(local_sp_6 + 76UL);
    var_88 = *(uint64_t *)(local_sp_6 + 152UL);
    var_89 = (uint64_t)*(uint32_t *)(local_sp_6 + 32UL);
    *(uint64_t *)(local_sp_6 + (-8L)) = 4219979UL;
    var_90 = indirect_placeholder_3(var_87, var_89, var_88, r15_4);
    rbp_5 = (uint64_t)(uint32_t)var_90;
}
