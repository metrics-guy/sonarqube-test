typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_66_ret_type;
struct indirect_placeholder_69_ret_type;
struct indirect_placeholder_68_ret_type;
struct indirect_placeholder_70_ret_type;
struct indirect_placeholder_67_ret_type;
struct indirect_placeholder_66_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_69_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_68_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_70_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_67_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_2(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_4(uint64_t param_0);
extern uint64_t init_r13(void);
extern uint64_t init_r9(void);
extern uint64_t indirect_placeholder_13(void);
extern struct indirect_placeholder_66_ret_type indirect_placeholder_66(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_69_ret_type indirect_placeholder_69(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_68_ret_type indirect_placeholder_68(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_70_ret_type indirect_placeholder_70(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_67_ret_type indirect_placeholder_67(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
uint64_t bb_set_process_security_ctx(uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t rcx, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    unsigned char var_6;
    uint64_t var_7;
    unsigned char *_pre;
    uint64_t local_sp_0;
    uint64_t r13_0;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t rbp_0;
    uint64_t local_sp_1;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_40;
    uint64_t var_41;
    unsigned char *_pre74_pre_phi;
    uint64_t local_sp_2;
    struct indirect_placeholder_69_ret_type var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    unsigned char *_pre_phi75;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_42;
    struct indirect_placeholder_70_ret_type var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    unsigned char *var_8;
    unsigned char var_9;
    uint64_t var_10;
    bool var_11;
    uint64_t var_12;
    uint64_t *var_13;
    uint64_t var_15;
    bool var_16;
    uint64_t *var_17;
    uint64_t var_14;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_rbp();
    var_4 = init_r12();
    var_5 = init_r9();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_2;
    var_6 = *(unsigned char *)(r8 + 37UL);
    var_7 = (uint64_t)var_6;
    r13_0 = 1UL;
    rbp_0 = var_7;
    if (var_6 == '\x00') {
        var_37 = (uint64_t)*(unsigned char *)(r8 + 33UL) & rcx;
        var_38 = (uint64_t)((uint32_t)rcx & (-256)) | var_37;
        rbp_0 = 1UL;
        *(uint64_t *)(var_0 + (-64L)) = 4221557UL;
        var_39 = indirect_placeholder_13();
        rbp_0 = var_38;
        if (var_37 != 0UL & (int)(uint32_t)var_39 <= (int)4294967295U) {
            *(uint64_t *)(var_0 + (-72L)) = 4221566UL;
            indirect_placeholder_2();
            var_40 = (uint64_t)*(uint32_t *)var_39;
            *(uint64_t *)(var_0 + (-80L)) = 4221573UL;
            var_41 = indirect_placeholder_4(var_40);
            if ((uint64_t)(unsigned char)var_41 == 0UL) {
                *(uint64_t *)(var_0 + (-88L)) = 4221594UL;
                var_43 = indirect_placeholder_70(4UL, rsi);
                var_44 = var_43.field_0;
                var_45 = var_43.field_1;
                var_46 = var_43.field_2;
                *(uint64_t *)(var_0 + (-96L)) = 4221602UL;
                indirect_placeholder_2();
                var_47 = (uint64_t)*(uint32_t *)var_44;
                *(uint64_t *)(var_0 + (-104L)) = 4221627UL;
                indirect_placeholder_67(0UL, 4322656UL, 0UL, var_47, var_44, var_45, var_46);
            } else {
                var_42 = (uint64_t)(uint32_t)var_41;
                rbp_0 = var_42;
            }
        }
    } else {
        if (*(unsigned char *)(r8 + 35UL) == '\x00') {
            var_8 = (unsigned char *)(r8 + 38UL);
            var_9 = *var_8;
            var_10 = (uint64_t)var_9;
            r13_0 = var_10;
            _pre74_pre_phi = var_8;
            _pre_phi75 = var_8;
            if (var_9 != '\x00') {
                var_25 = var_0 + (-64L);
                *(uint64_t *)var_25 = 4221717UL;
                var_26 = indirect_placeholder_13();
                local_sp_0 = var_25;
                local_sp_2 = var_25;
                if ((int)(uint32_t)var_26 < (int)0U) {
                    _pre = (unsigned char *)(r8 + 38UL);
                    _pre74_pre_phi = _pre;
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4221472UL;
                    var_32 = indirect_placeholder_69(4UL, rdi);
                    var_33 = var_32.field_0;
                    var_34 = var_32.field_1;
                    var_35 = var_32.field_2;
                    *(uint64_t *)(local_sp_2 + (-16L)) = 4221480UL;
                    indirect_placeholder_2();
                    var_36 = (uint64_t)*(uint32_t *)var_33;
                    *(uint64_t *)(local_sp_2 + (-24L)) = 4221505UL;
                    indirect_placeholder_68(0UL, 4322616UL, 0UL, var_36, var_33, var_34, var_35);
                    _pre_phi75 = _pre74_pre_phi;
                    if (*_pre_phi75 != '\x00') {
                        rbp_0 = 0UL;
                    }
                    return (uint64_t)(uint32_t)rbp_0;
                }
                var_27 = local_sp_0 + (-8L);
                *(uint64_t *)var_27 = 4221738UL;
                var_28 = indirect_placeholder_13();
                local_sp_1 = var_27;
                if ((int)(uint32_t)var_28 > (int)4294967295U) {
                    return (uint64_t)(uint32_t)rbp_0;
                }
                if (r13_0 != 0UL) {
                    if (*(unsigned char *)(r8 + 38UL) == '\x00') {
                        rbp_0 = 0UL;
                    }
                    return (uint64_t)(uint32_t)rbp_0;
                }
            }
            var_11 = (*(unsigned char *)(r8 + 41UL) == '\x00');
            var_12 = var_0 + (-64L);
            var_13 = (uint64_t *)var_12;
            local_sp_0 = var_12;
            if (!var_11) {
                *var_13 = 4221776UL;
                var_14 = indirect_placeholder_13();
                if ((int)(uint32_t)var_14 >= (int)0U) {
                    if (*_pre_phi75 != '\x00') {
                        rbp_0 = 0UL;
                    }
                    return (uint64_t)(uint32_t)rbp_0;
                }
                var_27 = local_sp_0 + (-8L);
                *(uint64_t *)var_27 = 4221738UL;
                var_28 = indirect_placeholder_13();
                local_sp_1 = var_27;
                if ((int)(uint32_t)var_28 > (int)4294967295U) {
                    return (uint64_t)(uint32_t)rbp_0;
                }
                if (r13_0 != 0UL) {
                    if (*(unsigned char *)(r8 + 38UL) != '\x00') {
                        rbp_0 = 0UL;
                    }
                    return (uint64_t)(uint32_t)rbp_0;
                }
            }
            *var_13 = 4221435UL;
            var_15 = indirect_placeholder_13();
            var_16 = ((int)(uint32_t)var_15 > (int)4294967295U);
            var_17 = (uint64_t *)(var_0 + (-72L));
            if (!var_16) {
                *var_17 = 4221448UL;
                indirect_placeholder_2();
                var_18 = (uint64_t)*(uint32_t *)var_15;
                var_19 = var_0 + (-80L);
                *(uint64_t *)var_19 = 4221455UL;
                var_20 = indirect_placeholder_4(var_18);
                local_sp_2 = var_19;
                if ((uint64_t)(unsigned char)var_20 == 0UL) {
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4221472UL;
                    var_32 = indirect_placeholder_69(4UL, rdi);
                    var_33 = var_32.field_0;
                    var_34 = var_32.field_1;
                    var_35 = var_32.field_2;
                    *(uint64_t *)(local_sp_2 + (-16L)) = 4221480UL;
                    indirect_placeholder_2();
                    var_36 = (uint64_t)*(uint32_t *)var_33;
                    *(uint64_t *)(local_sp_2 + (-24L)) = 4221505UL;
                    indirect_placeholder_68(0UL, 4322616UL, 0UL, var_36, var_33, var_34, var_35);
                    _pre_phi75 = _pre74_pre_phi;
                }
                if (*_pre_phi75 != '\x00') {
                    rbp_0 = 0UL;
                }
                return (uint64_t)(uint32_t)rbp_0;
            }
            *var_17 = 4221639UL;
            var_21 = indirect_placeholder_13();
            if ((int)(uint32_t)var_21 > (int)4294967295U) {
                return (uint64_t)(uint32_t)rbp_0;
            }
            *(uint64_t *)(var_0 + (-80L)) = 4221648UL;
            indirect_placeholder_2();
            var_22 = (uint64_t)*(uint32_t *)var_21;
            var_23 = var_0 + (-88L);
            *(uint64_t *)var_23 = 4221655UL;
            var_24 = indirect_placeholder_4(var_22);
            local_sp_1 = var_23;
            if ((uint64_t)(unsigned char)var_24 != 0UL) {
                if (*(unsigned char *)(r8 + 38UL) != '\x00') {
                    rbp_0 = 0UL;
                }
                return (uint64_t)(uint32_t)rbp_0;
            }
        }
        var_25 = var_0 + (-64L);
        *(uint64_t *)var_25 = 4221717UL;
        var_26 = indirect_placeholder_13();
        local_sp_0 = var_25;
        local_sp_2 = var_25;
        if ((int)(uint32_t)var_26 < (int)0U) {
            _pre = (unsigned char *)(r8 + 38UL);
            _pre74_pre_phi = _pre;
            *(uint64_t *)(local_sp_2 + (-8L)) = 4221472UL;
            var_32 = indirect_placeholder_69(4UL, rdi);
            var_33 = var_32.field_0;
            var_34 = var_32.field_1;
            var_35 = var_32.field_2;
            *(uint64_t *)(local_sp_2 + (-16L)) = 4221480UL;
            indirect_placeholder_2();
            var_36 = (uint64_t)*(uint32_t *)var_33;
            *(uint64_t *)(local_sp_2 + (-24L)) = 4221505UL;
            indirect_placeholder_68(0UL, 4322616UL, 0UL, var_36, var_33, var_34, var_35);
            _pre_phi75 = _pre74_pre_phi;
            if (*_pre_phi75 == '\x00') {
                rbp_0 = 0UL;
            }
            return (uint64_t)(uint32_t)rbp_0;
        }
        var_27 = local_sp_0 + (-8L);
        *(uint64_t *)var_27 = 4221738UL;
        var_28 = indirect_placeholder_13();
        local_sp_1 = var_27;
        if ((int)(uint32_t)var_28 > (int)4294967295U) {
            return (uint64_t)(uint32_t)rbp_0;
        }
        if (r13_0 != 0UL) {
            if (*(unsigned char *)(r8 + 38UL) == '\x00') {
                rbp_0 = 0UL;
            }
            return (uint64_t)(uint32_t)rbp_0;
        }
        var_29 = *(uint64_t *)(local_sp_1 + 8UL);
        *(uint64_t *)(local_sp_1 + (-8L)) = 4221669UL;
        var_30 = indirect_placeholder_4(var_29);
        *(uint64_t *)(local_sp_1 + (-16L)) = 4221677UL;
        indirect_placeholder_2();
        var_31 = (uint64_t)*(uint32_t *)var_30;
        *(uint64_t *)(local_sp_1 + (-24L)) = 4221702UL;
        indirect_placeholder_66(0UL, 4318744UL, 0UL, var_31, var_30, var_5, r8);
        if (*(unsigned char *)(r8 + 38UL) == '\x00') {
            rbp_0 = 0UL;
        }
    }
}
