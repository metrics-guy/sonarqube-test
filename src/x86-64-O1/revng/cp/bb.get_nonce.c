typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_2(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
void bb_get_nonce(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t rax_0;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t rax_1;
    uint32_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint32_t rax_2;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t rax_3;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t rax_4;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t rax_5;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_10;
    uint64_t *var_11;
    uint64_t var_12;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    *(uint64_t *)(var_0 + (-80L)) = 4273279UL;
    indirect_placeholder_2();
    *(uint64_t *)(var_0 + (-88L)) = 4273335UL;
    indirect_placeholder_2();
    var_8 = var_0 + (-96L);
    *(uint64_t *)var_8 = 4273350UL;
    indirect_placeholder_2();
    var_9 = helper_cc_compute_c_wrapper(0UL - rsi, rsi, var_7, 17U);
    rax_0 = 0UL;
    rax_1 = 0UL;
    rax_2 = 4U;
    rax_3 = 0UL;
    rax_4 = 0UL;
    rax_5 = 0UL;
    if (var_9 == 0UL) {
        return;
    }
    var_10 = (rsi < 16UL) ? rsi : 16UL;
    var_11 = (uint64_t *)(var_0 + (-104L));
    *var_11 = 4273387UL;
    indirect_placeholder_2();
    var_12 = helper_cc_compute_c_wrapper(var_10 + (-8L), 8UL, var_7, 16U);
    if (var_12 == 0UL) {
        *(uint64_t *)rdi = *var_11;
        *(uint64_t *)((var_10 + rdi) + (-8L)) = *(uint64_t *)((var_10 | var_8) + (-8L));
        var_13 = (rdi + 8UL) & (-8L);
        var_14 = rdi - var_13;
        var_15 = var_8 - var_14;
        var_16 = (uint64_t)(((uint32_t)var_14 + (uint32_t)var_10) & (-8));
        var_17 = helper_cc_compute_c_wrapper(var_16 + (-8L), 8UL, var_7, 16U);
        if (var_17 == 0UL) {
            *(uint64_t *)(rax_5 + var_13) = *(uint64_t *)(rax_5 + var_15);
            var_18 = (uint64_t)((uint32_t)rax_5 + 8U);
            var_19 = helper_cc_compute_c_wrapper(var_18 - var_16, var_16, var_7, 16U);
            rax_5 = var_18;
            do {
                *(uint64_t *)(rax_5 + var_13) = *(uint64_t *)(rax_5 + var_15);
                var_18 = (uint64_t)((uint32_t)rax_5 + 8U);
                var_19 = helper_cc_compute_c_wrapper(var_18 - var_16, var_16, var_7, 16U);
                rax_5 = var_18;
            } while (var_19 != 0UL);
        }
    } else {
        if ((var_10 & 4UL) == 0UL) {
            *(uint32_t *)rdi = *(uint32_t *)var_8;
            *(uint32_t *)((var_10 + rdi) + (-4L)) = *(uint32_t *)((var_10 | var_8) + (-4L));
        } else {
            *(unsigned char *)rdi = *(unsigned char *)var_8;
            if (var_10 != 0UL & (var_10 & 2UL) == 0UL) {
                *(uint16_t *)((var_10 + rdi) + (-2L)) = *(uint16_t *)((var_10 | var_8) + (-2L));
            }
        }
    }
    var_20 = helper_cc_compute_c_wrapper(var_10 - rsi, rsi, var_7, 17U);
    if (var_20 == 0UL) {
        return;
    }
    var_21 = rsi - var_10;
    var_22 = (var_21 < 4UL) ? var_21 : 4UL;
    var_23 = var_0 + (-112L);
    *(uint64_t *)var_23 = 4273567UL;
    indirect_placeholder_2();
    *(uint32_t *)var_23 = 4U;
    if (var_22 == 0UL) {
        *(unsigned char *)(rax_4 + (var_10 + rdi)) = *(unsigned char *)(rax_4 + var_23);
        var_24 = (uint64_t)((uint32_t)rax_4 + 1U);
        var_25 = helper_cc_compute_c_wrapper(var_24 - var_22, var_22, var_7, 16U);
        rax_4 = var_24;
        do {
            *(unsigned char *)(rax_4 + (var_10 + rdi)) = *(unsigned char *)(rax_4 + var_23);
            var_24 = (uint64_t)((uint32_t)rax_4 + 1U);
            var_25 = helper_cc_compute_c_wrapper(var_24 - var_22, var_22, var_7, 16U);
            rax_4 = var_24;
        } while (var_25 != 0UL);
    }
    var_26 = var_10 + var_22;
    var_27 = helper_cc_compute_c_wrapper(var_26 - rsi, rsi, var_7, 17U);
    if (var_27 != 0UL) {
        var_28 = rsi - var_26;
        var_29 = (var_28 < 4UL) ? var_28 : 4UL;
        var_30 = var_0 + (-120L);
        *(uint64_t *)var_30 = 4273639UL;
        indirect_placeholder_2();
        *(uint32_t *)var_30 = 4U;
        if (var_29 == 0UL) {
            *(unsigned char *)(rax_3 + (var_26 + rdi)) = *(unsigned char *)(rax_3 + var_30);
            var_31 = (uint64_t)((uint32_t)rax_3 + 1U);
            var_32 = helper_cc_compute_c_wrapper(var_31 - var_29, var_29, var_7, 16U);
            rax_3 = var_31;
            do {
                *(unsigned char *)(rax_3 + (var_26 + rdi)) = *(unsigned char *)(rax_3 + var_30);
                var_31 = (uint64_t)((uint32_t)rax_3 + 1U);
                var_32 = helper_cc_compute_c_wrapper(var_31 - var_29, var_29, var_7, 16U);
                rax_3 = var_31;
            } while (var_32 != 0UL);
        }
        var_33 = var_26 + var_29;
        var_34 = helper_cc_compute_c_wrapper(var_33 - rsi, rsi, var_7, 17U);
        if (var_34 != 0UL) {
            var_35 = rsi - var_33;
            var_36 = (var_35 < 4UL) ? var_35 : 4UL;
            var_37 = var_0 + (-128L);
            *(uint64_t *)var_37 = 4273711UL;
            indirect_placeholder_2();
            *(uint32_t *)var_37 = 4U;
            if (var_36 != 0UL) {
                *(unsigned char *)(rax_1 + (var_33 + rdi)) = *(unsigned char *)(rax_1 + var_37);
                var_38 = (uint32_t)rax_1 + 1U;
                var_39 = (uint64_t)var_38;
                var_40 = helper_cc_compute_c_wrapper(var_39 - var_36, var_36, var_7, 16U);
                rax_1 = var_39;
                rax_2 = var_38;
                do {
                    *(unsigned char *)(rax_1 + (var_33 + rdi)) = *(unsigned char *)(rax_1 + var_37);
                    var_38 = (uint32_t)rax_1 + 1U;
                    var_39 = (uint64_t)var_38;
                    var_40 = helper_cc_compute_c_wrapper(var_39 - var_36, var_36, var_7, 16U);
                    rax_1 = var_39;
                    rax_2 = var_38;
                } while (var_40 != 0UL);
            }
            var_41 = var_33 + var_36;
            var_42 = helper_cc_compute_c_wrapper(var_41 - rsi, rsi, var_7, 17U);
            var_43 = var_0 + (-136L);
            *(uint64_t *)var_43 = 4273764UL;
            indirect_placeholder_2();
            *(uint32_t *)var_43 = rax_2;
            var_44 = var_41 + rdi;
            var_45 = rsi - var_41;
            var_46 = (var_45 < 4UL) ? var_45 : 4UL;
            if (var_42 != 0UL & var_46 == 0UL) {
                *(unsigned char *)(rax_0 + var_44) = *(unsigned char *)(rax_0 + var_43);
                var_47 = (uint64_t)((uint32_t)rax_0 + 1U);
                var_48 = helper_cc_compute_c_wrapper(var_47 - var_46, var_46, var_7, 16U);
                rax_0 = var_47;
                do {
                    *(unsigned char *)(rax_0 + var_44) = *(unsigned char *)(rax_0 + var_43);
                    var_47 = (uint64_t)((uint32_t)rax_0 + 1U);
                    var_48 = helper_cc_compute_c_wrapper(var_47 - var_46, var_46, var_7, 16U);
                    rax_0 = var_47;
                } while (var_48 != 0UL);
            }
        }
    }
    return;
}
