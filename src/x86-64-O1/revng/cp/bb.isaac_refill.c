typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_4(uint64_t param_0);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
void bb_isaac_refill(uint64_t rdi, uint64_t rsi) {
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t r13_0;
    uint64_t r15_0;
    uint64_t rbp_0;
    uint64_t var_58;
    uint64_t *var_59;
    uint64_t var_60;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t local_sp_1;
    uint64_t var_100;
    uint64_t rbx_0;
    uint64_t local_sp_0;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t *var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t *var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t *var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t *var_93;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t *var_97;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t rbx_1;
    uint64_t r15_1;
    uint64_t rbp_1;
    uint64_t r14_0;
    uint64_t var_12;
    uint64_t *var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t *var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t *var_32;
    uint64_t var_33;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t *var_42;
    uint64_t var_43;
    uint64_t *var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t *var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t *var_52;
    uint64_t var_53;
    uint64_t var_54;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_7 = var_0 + (-72L);
    *(uint64_t *)(var_0 + (-64L)) = rdi;
    *(uint64_t *)(var_0 + (-56L)) = rsi;
    var_8 = *(uint64_t *)(rdi + 2048UL);
    var_9 = (uint64_t *)(rdi + 2064UL);
    var_10 = *var_9 + 1UL;
    *var_9 = var_10;
    var_11 = var_10 + *(uint64_t *)(rdi + 2056UL);
    *(uint64_t *)var_7 = (rdi + 1024UL);
    local_sp_1 = var_7;
    rbx_1 = rdi;
    r15_1 = var_11;
    rbp_1 = var_8;
    r14_0 = rsi;
    var_12 = *(uint64_t *)(rbx_1 + 1024UL) + ((rbp_1 ^ (rbp_1 << 21UL)) ^ (-1L));
    var_13 = (uint64_t *)rbx_1;
    var_14 = *var_13;
    *(uint64_t *)(local_sp_1 + (-8L)) = 4274552UL;
    var_15 = indirect_placeholder(rdi, var_14);
    var_16 = (var_12 + var_15) + r15_1;
    *var_13 = var_16;
    var_17 = var_16 >> 8UL;
    *(uint64_t *)(local_sp_1 + (-16L)) = 4274574UL;
    var_18 = indirect_placeholder(rdi, var_17);
    var_19 = var_14 + var_18;
    *(uint64_t *)(local_sp_1 + (-24L)) = 4274583UL;
    var_20 = indirect_placeholder_4(var_19);
    *(uint64_t *)r14_0 = var_20;
    *(uint64_t *)(local_sp_1 + (-32L)) = 4274597UL;
    var_21 = indirect_placeholder_4(var_12);
    var_22 = (var_12 ^ (var_21 >> 5UL)) + *(uint64_t *)(rbx_1 + 1032UL);
    var_23 = (uint64_t *)(rbx_1 + 8UL);
    var_24 = *var_23;
    *(uint64_t *)(local_sp_1 + (-40L)) = 4274629UL;
    var_25 = indirect_placeholder(rdi, var_24);
    var_26 = (var_20 + var_25) + var_22;
    *var_23 = var_26;
    var_27 = var_26 >> 8UL;
    *(uint64_t *)(local_sp_1 + (-48L)) = 4274652UL;
    var_28 = indirect_placeholder(rdi, var_27);
    var_29 = var_24 + var_28;
    *(uint64_t *)(local_sp_1 + (-56L)) = 4274661UL;
    var_30 = indirect_placeholder_4(var_29);
    *(uint64_t *)(r14_0 + 8UL) = var_30;
    var_31 = (var_22 ^ (var_22 << 12UL)) + *(uint64_t *)(rbx_1 + 1040UL);
    var_32 = (uint64_t *)(rbx_1 + 16UL);
    var_33 = *var_32;
    *(uint64_t *)(local_sp_1 + (-64L)) = 4274703UL;
    var_34 = indirect_placeholder(rdi, var_33);
    var_35 = (var_30 + var_34) + var_31;
    *var_32 = var_35;
    var_36 = var_35 >> 8UL;
    *(uint64_t *)(local_sp_1 + (-72L)) = 4274726UL;
    var_37 = indirect_placeholder(rdi, var_36);
    var_38 = var_33 + var_37;
    *(uint64_t *)(local_sp_1 + (-80L)) = 4274735UL;
    var_39 = indirect_placeholder_4(var_38);
    *(uint64_t *)(r14_0 + 16UL) = var_39;
    *(uint64_t *)(local_sp_1 + (-88L)) = 4274750UL;
    var_40 = indirect_placeholder_4(var_31);
    var_41 = (var_31 ^ (var_40 >> 33UL)) + *(uint64_t *)(rbx_1 + 1048UL);
    var_42 = (uint64_t *)(rbx_1 + 24UL);
    var_43 = *var_42;
    var_44 = (uint64_t *)(local_sp_1 + (-96L));
    *var_44 = 4274782UL;
    var_45 = indirect_placeholder(rdi, var_43);
    var_46 = (var_39 + var_45) + var_41;
    *var_42 = var_46;
    var_47 = var_46 >> 8UL;
    var_48 = (uint64_t *)(local_sp_1 + (-104L));
    *var_48 = 4274805UL;
    var_49 = indirect_placeholder(rdi, var_47);
    var_50 = var_43 + var_49;
    var_51 = local_sp_1 + (-112L);
    var_52 = (uint64_t *)var_51;
    *var_52 = 4274814UL;
    var_53 = indirect_placeholder_4(var_50);
    *(uint64_t *)(r14_0 + 24UL) = var_53;
    var_54 = rbx_1 + 32UL;
    r15_0 = var_53;
    rbp_0 = var_41;
    local_sp_1 = var_51;
    local_sp_0 = var_51;
    rbx_1 = var_54;
    r15_1 = var_53;
    rbp_1 = var_41;
    while (var_54 != *var_52)
        {
            r14_0 = r14_0 + 32UL;
            var_12 = *(uint64_t *)(rbx_1 + 1024UL) + ((rbp_1 ^ (rbp_1 << 21UL)) ^ (-1L));
            var_13 = (uint64_t *)rbx_1;
            var_14 = *var_13;
            *(uint64_t *)(local_sp_1 + (-8L)) = 4274552UL;
            var_15 = indirect_placeholder(rdi, var_14);
            var_16 = (var_12 + var_15) + r15_1;
            *var_13 = var_16;
            var_17 = var_16 >> 8UL;
            *(uint64_t *)(local_sp_1 + (-16L)) = 4274574UL;
            var_18 = indirect_placeholder(rdi, var_17);
            var_19 = var_14 + var_18;
            *(uint64_t *)(local_sp_1 + (-24L)) = 4274583UL;
            var_20 = indirect_placeholder_4(var_19);
            *(uint64_t *)r14_0 = var_20;
            *(uint64_t *)(local_sp_1 + (-32L)) = 4274597UL;
            var_21 = indirect_placeholder_4(var_12);
            var_22 = (var_12 ^ (var_21 >> 5UL)) + *(uint64_t *)(rbx_1 + 1032UL);
            var_23 = (uint64_t *)(rbx_1 + 8UL);
            var_24 = *var_23;
            *(uint64_t *)(local_sp_1 + (-40L)) = 4274629UL;
            var_25 = indirect_placeholder(rdi, var_24);
            var_26 = (var_20 + var_25) + var_22;
            *var_23 = var_26;
            var_27 = var_26 >> 8UL;
            *(uint64_t *)(local_sp_1 + (-48L)) = 4274652UL;
            var_28 = indirect_placeholder(rdi, var_27);
            var_29 = var_24 + var_28;
            *(uint64_t *)(local_sp_1 + (-56L)) = 4274661UL;
            var_30 = indirect_placeholder_4(var_29);
            *(uint64_t *)(r14_0 + 8UL) = var_30;
            var_31 = (var_22 ^ (var_22 << 12UL)) + *(uint64_t *)(rbx_1 + 1040UL);
            var_32 = (uint64_t *)(rbx_1 + 16UL);
            var_33 = *var_32;
            *(uint64_t *)(local_sp_1 + (-64L)) = 4274703UL;
            var_34 = indirect_placeholder(rdi, var_33);
            var_35 = (var_30 + var_34) + var_31;
            *var_32 = var_35;
            var_36 = var_35 >> 8UL;
            *(uint64_t *)(local_sp_1 + (-72L)) = 4274726UL;
            var_37 = indirect_placeholder(rdi, var_36);
            var_38 = var_33 + var_37;
            *(uint64_t *)(local_sp_1 + (-80L)) = 4274735UL;
            var_39 = indirect_placeholder_4(var_38);
            *(uint64_t *)(r14_0 + 16UL) = var_39;
            *(uint64_t *)(local_sp_1 + (-88L)) = 4274750UL;
            var_40 = indirect_placeholder_4(var_31);
            var_41 = (var_31 ^ (var_40 >> 33UL)) + *(uint64_t *)(rbx_1 + 1048UL);
            var_42 = (uint64_t *)(rbx_1 + 24UL);
            var_43 = *var_42;
            var_44 = (uint64_t *)(local_sp_1 + (-96L));
            *var_44 = 4274782UL;
            var_45 = indirect_placeholder(rdi, var_43);
            var_46 = (var_39 + var_45) + var_41;
            *var_42 = var_46;
            var_47 = var_46 >> 8UL;
            var_48 = (uint64_t *)(local_sp_1 + (-104L));
            *var_48 = 4274805UL;
            var_49 = indirect_placeholder(rdi, var_47);
            var_50 = var_43 + var_49;
            var_51 = local_sp_1 + (-112L);
            var_52 = (uint64_t *)var_51;
            *var_52 = 4274814UL;
            var_53 = indirect_placeholder_4(var_50);
            *(uint64_t *)(r14_0 + 24UL) = var_53;
            var_54 = rbx_1 + 32UL;
            r15_0 = var_53;
            rbp_0 = var_41;
            local_sp_1 = var_51;
            local_sp_0 = var_51;
            rbx_1 = var_54;
            r15_1 = var_53;
            rbp_1 = var_41;
        }
    var_55 = *var_44 + 1024UL;
    var_56 = *var_48;
    var_57 = var_56 + 1024UL;
    *var_52 = (var_56 + 2048UL);
    r13_0 = var_55;
    rbx_0 = var_57;
    var_58 = *(uint64_t *)(rbx_0 + (-1024L)) + ((rbp_0 ^ (rbp_0 << 21UL)) ^ (-1L));
    var_59 = (uint64_t *)rbx_0;
    var_60 = *var_59;
    *(uint64_t *)(local_sp_0 + (-8L)) = 4274910UL;
    var_61 = indirect_placeholder(rdi, var_60);
    var_62 = (var_58 + var_61) + r15_0;
    *var_59 = var_62;
    var_63 = var_62 >> 8UL;
    *(uint64_t *)(local_sp_0 + (-16L)) = 4274932UL;
    var_64 = indirect_placeholder(rdi, var_63);
    var_65 = var_60 + var_64;
    *(uint64_t *)(local_sp_0 + (-24L)) = 4274941UL;
    var_66 = indirect_placeholder_4(var_65);
    *(uint64_t *)r13_0 = var_66;
    *(uint64_t *)(local_sp_0 + (-32L)) = 4274956UL;
    var_67 = indirect_placeholder_4(var_58);
    var_68 = (var_58 ^ (var_67 >> 5UL)) + *(uint64_t *)(rbx_0 + (-1016L));
    var_69 = (uint64_t *)(rbx_0 + 8UL);
    var_70 = *var_69;
    *(uint64_t *)(local_sp_0 + (-40L)) = 4274988UL;
    var_71 = indirect_placeholder(rdi, var_70);
    var_72 = (var_66 + var_71) + var_68;
    *var_69 = var_72;
    var_73 = var_72 >> 8UL;
    *(uint64_t *)(local_sp_0 + (-48L)) = 4275011UL;
    var_74 = indirect_placeholder(rdi, var_73);
    var_75 = var_70 + var_74;
    *(uint64_t *)(local_sp_0 + (-56L)) = 4275020UL;
    var_76 = indirect_placeholder_4(var_75);
    *(uint64_t *)(r13_0 + 8UL) = var_76;
    var_77 = (var_68 ^ (var_68 << 12UL)) + *(uint64_t *)(rbx_0 + (-1008L));
    var_78 = (uint64_t *)(rbx_0 + 16UL);
    var_79 = *var_78;
    *(uint64_t *)(local_sp_0 + (-64L)) = 4275062UL;
    var_80 = indirect_placeholder(rdi, var_79);
    var_81 = (var_76 + var_80) + var_77;
    *var_78 = var_81;
    var_82 = var_81 >> 8UL;
    *(uint64_t *)(local_sp_0 + (-72L)) = 4275085UL;
    var_83 = indirect_placeholder(rdi, var_82);
    var_84 = var_79 + var_83;
    *(uint64_t *)(local_sp_0 + (-80L)) = 4275094UL;
    var_85 = indirect_placeholder_4(var_84);
    *(uint64_t *)(r13_0 + 16UL) = var_85;
    *(uint64_t *)(local_sp_0 + (-88L)) = 4275109UL;
    var_86 = indirect_placeholder_4(var_77);
    var_87 = (var_77 ^ (var_86 >> 33UL)) + *(uint64_t *)(rbx_0 + (-1000L));
    var_88 = (uint64_t *)(rbx_0 + 24UL);
    var_89 = *var_88;
    *(uint64_t *)(local_sp_0 + (-96L)) = 4275141UL;
    var_90 = indirect_placeholder(rdi, var_89);
    var_91 = (var_85 + var_90) + var_87;
    *var_88 = var_91;
    var_92 = var_91 >> 8UL;
    var_93 = (uint64_t *)(local_sp_0 + (-104L));
    *var_93 = 4275164UL;
    var_94 = indirect_placeholder(rdi, var_92);
    var_95 = var_89 + var_94;
    var_96 = local_sp_0 + (-112L);
    var_97 = (uint64_t *)var_96;
    *var_97 = 4275173UL;
    var_98 = indirect_placeholder_4(var_95);
    *(uint64_t *)(r13_0 + 24UL) = var_98;
    var_99 = rbx_0 + 32UL;
    local_sp_0 = var_96;
    rbx_0 = var_99;
    r15_0 = var_98;
    rbp_0 = var_87;
    while (var_99 != *var_97)
        {
            r13_0 = r13_0 + 32UL;
            var_58 = *(uint64_t *)(rbx_0 + (-1024L)) + ((rbp_0 ^ (rbp_0 << 21UL)) ^ (-1L));
            var_59 = (uint64_t *)rbx_0;
            var_60 = *var_59;
            *(uint64_t *)(local_sp_0 + (-8L)) = 4274910UL;
            var_61 = indirect_placeholder(rdi, var_60);
            var_62 = (var_58 + var_61) + r15_0;
            *var_59 = var_62;
            var_63 = var_62 >> 8UL;
            *(uint64_t *)(local_sp_0 + (-16L)) = 4274932UL;
            var_64 = indirect_placeholder(rdi, var_63);
            var_65 = var_60 + var_64;
            *(uint64_t *)(local_sp_0 + (-24L)) = 4274941UL;
            var_66 = indirect_placeholder_4(var_65);
            *(uint64_t *)r13_0 = var_66;
            *(uint64_t *)(local_sp_0 + (-32L)) = 4274956UL;
            var_67 = indirect_placeholder_4(var_58);
            var_68 = (var_58 ^ (var_67 >> 5UL)) + *(uint64_t *)(rbx_0 + (-1016L));
            var_69 = (uint64_t *)(rbx_0 + 8UL);
            var_70 = *var_69;
            *(uint64_t *)(local_sp_0 + (-40L)) = 4274988UL;
            var_71 = indirect_placeholder(rdi, var_70);
            var_72 = (var_66 + var_71) + var_68;
            *var_69 = var_72;
            var_73 = var_72 >> 8UL;
            *(uint64_t *)(local_sp_0 + (-48L)) = 4275011UL;
            var_74 = indirect_placeholder(rdi, var_73);
            var_75 = var_70 + var_74;
            *(uint64_t *)(local_sp_0 + (-56L)) = 4275020UL;
            var_76 = indirect_placeholder_4(var_75);
            *(uint64_t *)(r13_0 + 8UL) = var_76;
            var_77 = (var_68 ^ (var_68 << 12UL)) + *(uint64_t *)(rbx_0 + (-1008L));
            var_78 = (uint64_t *)(rbx_0 + 16UL);
            var_79 = *var_78;
            *(uint64_t *)(local_sp_0 + (-64L)) = 4275062UL;
            var_80 = indirect_placeholder(rdi, var_79);
            var_81 = (var_76 + var_80) + var_77;
            *var_78 = var_81;
            var_82 = var_81 >> 8UL;
            *(uint64_t *)(local_sp_0 + (-72L)) = 4275085UL;
            var_83 = indirect_placeholder(rdi, var_82);
            var_84 = var_79 + var_83;
            *(uint64_t *)(local_sp_0 + (-80L)) = 4275094UL;
            var_85 = indirect_placeholder_4(var_84);
            *(uint64_t *)(r13_0 + 16UL) = var_85;
            *(uint64_t *)(local_sp_0 + (-88L)) = 4275109UL;
            var_86 = indirect_placeholder_4(var_77);
            var_87 = (var_77 ^ (var_86 >> 33UL)) + *(uint64_t *)(rbx_0 + (-1000L));
            var_88 = (uint64_t *)(rbx_0 + 24UL);
            var_89 = *var_88;
            *(uint64_t *)(local_sp_0 + (-96L)) = 4275141UL;
            var_90 = indirect_placeholder(rdi, var_89);
            var_91 = (var_85 + var_90) + var_87;
            *var_88 = var_91;
            var_92 = var_91 >> 8UL;
            var_93 = (uint64_t *)(local_sp_0 + (-104L));
            *var_93 = 4275164UL;
            var_94 = indirect_placeholder(rdi, var_92);
            var_95 = var_89 + var_94;
            var_96 = local_sp_0 + (-112L);
            var_97 = (uint64_t *)var_96;
            *var_97 = 4275173UL;
            var_98 = indirect_placeholder_4(var_95);
            *(uint64_t *)(r13_0 + 24UL) = var_98;
            var_99 = rbx_0 + 32UL;
            local_sp_0 = var_96;
            rbx_0 = var_99;
            r15_0 = var_98;
            rbp_0 = var_87;
        }
    var_100 = *var_93;
    *(uint64_t *)(var_100 + 2048UL) = var_87;
    *(uint64_t *)(var_100 + 2056UL) = var_98;
    return;
}
