typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_2(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_4(uint64_t param_0);
extern uint32_t init_state_0x82fc(void);
uint64_t bb_rpl_unlink(uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint32_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint32_t var_20;
    uint64_t var_21;
    uint64_t rax_1;
    uint64_t rax_0;
    uint64_t local_sp_1;
    uint64_t rbx_0;
    uint64_t var_16;
    unsigned char *var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t local_sp_0;
    uint64_t var_22;
    uint64_t rax_3;
    uint64_t var_12;
    bool var_13;
    uint64_t var_14;
    uint64_t *var_15;
    uint64_t rax_2;
    uint64_t rdi1_0;
    uint64_t rcx_0;
    uint64_t rcx_1;
    unsigned char var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r12();
    var_4 = init_state_0x82fc();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    var_5 = var_0 + (-168L);
    var_6 = (uint64_t)var_4;
    local_sp_1 = var_5;
    rax_3 = 0UL;
    rdi1_0 = rdi;
    rcx_0 = 18446744073709551615UL;
    rcx_1 = 0UL;
    while (rcx_0 != 0UL)
        {
            rdi1_0 = rdi1_0 + var_6;
        }
    var_9 = 18446744073709551614UL - (-2L);
    rbx_0 = var_9;
    if (var_9 == 0UL) {
        *(uint64_t *)(local_sp_1 + (-8L)) = 4269994UL;
        indirect_placeholder_2();
        rax_2 = rax_3;
        return rax_2;
    }
    if (*(unsigned char *)((var_9 + rdi) + (-1L)) == '/') {
        return;
    }
    var_10 = var_0 + (-176L);
    *(uint64_t *)var_10 = 4270017UL;
    var_11 = indirect_placeholder(rdi, var_5);
    rax_2 = var_11;
    if ((uint64_t)(uint32_t)var_11 == 0UL) {
        return rax_2;
    }
    *(uint64_t *)(var_0 + (-184L)) = 4270029UL;
    var_12 = indirect_placeholder_4(var_9);
    var_13 = (var_12 == 0UL);
    var_14 = var_0 + (-192L);
    var_15 = (uint64_t *)var_14;
    local_sp_0 = var_14;
    rax_2 = 4294967295UL;
    if (!var_13) {
        *var_15 = 4270082UL;
        indirect_placeholder_2();
        *(volatile uint32_t *)(uint32_t *)0UL = 1U;
        return rax_2;
    }
    *var_15 = 4270051UL;
    indirect_placeholder_2();
    while (1U)
        {
            var_16 = rbx_0 + (-1L);
            var_17 = (unsigned char *)(var_16 + var_12);
            rbx_0 = var_16;
            rax_1 = rbx_0;
            if (*var_17 == '/') {
                *var_17 = (unsigned char)'\x00';
                if (var_16 == 0UL) {
                    continue;
                }
                loop_state_var = 0U;
                break;
            }
            var_18 = var_0 + (-200L);
            *(uint64_t *)var_18 = 4270106UL;
            var_19 = indirect_placeholder(var_12, var_14);
            rax_0 = var_19;
            local_sp_0 = var_18;
            if ((uint64_t)(uint32_t)var_19 != 0UL) {
                loop_state_var = 1U;
                break;
            }
            var_20 = (uint32_t)((uint16_t)*(uint32_t *)var_10 & (unsigned short)61440U);
            var_21 = (uint64_t)var_20;
            rax_0 = var_21;
            rax_1 = var_21;
            if ((uint64_t)((var_20 + (-40960)) & (-4096)) != 0UL) {
                loop_state_var = 1U;
                break;
            }
            loop_state_var = 0U;
            break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            var_22 = local_sp_0 + (-8L);
            *(uint64_t *)var_22 = 4270134UL;
            indirect_placeholder_2();
            rax_3 = rax_1;
            local_sp_1 = var_22;
        }
        break;
      case 1U:
        {
            *(uint64_t *)(var_0 + (-208L)) = 4270147UL;
            indirect_placeholder_2();
            *(uint64_t *)(var_0 + (-216L)) = 4270152UL;
            indirect_placeholder_2();
            *(uint32_t *)rax_0 = 1U;
            return rax_2;
        }
        break;
    }
}
