typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_2(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
uint64_t bb_careadlinkat(uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t rcx, uint64_t r9, uint64_t r8) {
    uint64_t storemerge;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    bool var_9;
    uint64_t spec_select;
    uint64_t spec_select65;
    uint64_t rbx_1;
    uint64_t var_12;
    uint64_t rbx_0;
    uint64_t var_15;
    uint64_t local_sp_1;
    uint64_t var_16;
    uint64_t rbp_2;
    uint64_t rax_0;
    uint64_t local_sp_4;
    uint64_t var_10;
    uint64_t local_sp_3;
    uint64_t var_11;
    uint64_t var_13;
    uint64_t var_14;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_7 = var_0 + (-1096L);
    *(uint32_t *)(var_0 + (-1088L)) = (uint32_t)rdi;
    *(uint64_t *)var_7 = rsi;
    var_8 = (r8 == 0UL) ? 4330784UL : r8;
    var_9 = (rcx == 0UL);
    spec_select = var_9 ? 1024UL : rcx;
    spec_select65 = var_9 ? (var_0 + (-1080L)) : rdx;
    storemerge = 9223372036854775808UL;
    rbx_1 = spec_select65;
    rbx_0 = 0UL;
    rbp_2 = spec_select;
    rax_0 = 4330784UL;
    local_sp_4 = var_7;
    while (1U)
        {
            var_10 = local_sp_4 + (-8L);
            *(uint64_t *)var_10 = 4271149UL;
            indirect_placeholder_2();
            rbx_1 = 4611686018427387904UL;
            rax_0 = 4611686018427387904UL;
            local_sp_3 = var_10;
            if (rbp_2 > rax_0) {
                if (spec_select65 == rbx_1) {
                    var_11 = local_sp_4 + (-16L);
                    *(uint64_t *)var_11 = 4271185UL;
                    indirect_placeholder_2();
                    local_sp_3 = var_11;
                }
                if (rbp_2 <= 4611686018427387904UL) {
                    storemerge = rbp_2 << 1UL;
                    var_12 = local_sp_3 + (-8L);
                    *(uint64_t *)var_12 = 4271124UL;
                    indirect_placeholder_2();
                    rbp_2 = storemerge;
                    local_sp_4 = var_12;
                    continue;
                }
                if ((long)rbp_2 >= (long)0UL) {
                    *(uint64_t *)(local_sp_3 + (-8L)) = 4271210UL;
                    indirect_placeholder_2();
                    *(uint32_t *)4611686018427387904UL = 36U;
                    loop_state_var = 1U;
                    break;
                }
            }
            *(unsigned char *)(rax_0 + rbx_1) = (unsigned char)'\x00';
            rbx_0 = rbx_1;
            if (rbx_1 == (local_sp_4 + 8UL)) {
                if (!((rbp_2 <= (rax_0 | 1UL)) || (spec_select65 == rbx_1))) {
                    loop_state_var = 1U;
                    break;
                }
                var_13 = *(uint64_t *)(var_8 + 8UL);
                if (var_13 != 0UL) {
                    loop_state_var = 1U;
                    break;
                }
                *(uint64_t *)(local_sp_4 + (-16L)) = 4271049UL;
                indirect_placeholder_2();
                rbx_0 = var_13;
                loop_state_var = 1U;
                break;
            }
            var_14 = local_sp_4 + (-16L);
            *(uint64_t *)var_14 = 4271068UL;
            indirect_placeholder_2();
            local_sp_1 = var_14;
            rbx_0 = 0UL;
            if (rbx_1 == 0UL) {
                *(uint64_t *)(local_sp_4 + (-24L)) = 4271094UL;
                indirect_placeholder_2();
                loop_state_var = 1U;
                break;
            }
            var_15 = *(uint64_t *)(var_8 + 24UL);
            if (var_15 != 0UL) {
                loop_state_var = 0U;
                break;
            }
            var_16 = local_sp_4 + (-24L);
            *(uint64_t *)var_16 = 4271240UL;
            indirect_placeholder_2();
            local_sp_1 = var_16;
            loop_state_var = 0U;
            break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(uint64_t *)(local_sp_1 + (-8L)) = 4271245UL;
            indirect_placeholder_2();
            *(uint32_t *)var_15 = 12U;
        }
        break;
      case 1U:
        {
            return rbx_0;
        }
        break;
    }
}
