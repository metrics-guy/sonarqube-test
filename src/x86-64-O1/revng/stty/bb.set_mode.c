typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src(void);
extern uint64_t indirect_placeholder_29(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_r12(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint32_t init_state_0x82fc(void);
extern void indirect_placeholder_42(uint64_t param_0);
uint64_t bb_set_mode(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint32_t *var_105;
    uint32_t var_106;
    uint32_t *var_107;
    uint32_t *var_108;
    uint64_t cc_src_11;
    uint64_t cc_dst_11;
    uint32_t cc_op_11;
    uint64_t rdi2_5;
    uint64_t rsi3_5;
    uint64_t rcx_5;
    uint64_t cc_src_12;
    uint64_t cc_dst_12;
    uint64_t var_55;
    uint64_t var_56;
    uint32_t cc_op_12;
    uint64_t var_57;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint32_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    bool var_9;
    uint32_t var_27;
    uint32_t *var_28;
    uint64_t rax_1;
    uint64_t cc_src_13;
    uint64_t cc_dst_13;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t cc_src_0;
    uint64_t cc_dst_0;
    uint32_t cc_op_0;
    uint64_t rdi2_0;
    uint64_t rsi3_0;
    uint64_t rcx_0;
    uint64_t cc_src_1;
    uint64_t cc_dst_1;
    uint64_t var_31;
    uint64_t var_32;
    uint32_t cc_op_1;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t _pre_phi;
    uint64_t rbx_0;
    uint64_t local_sp_0;
    uint64_t cc_src2_0;
    unsigned char var_86;
    uint64_t var_87;
    uint32_t *var_88;
    uint32_t *var_89;
    uint32_t *var_90;
    uint32_t *var_91;
    uint32_t *var_92;
    uint64_t var_13;
    uint32_t var_14;
    uint32_t *var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t cc_src_2;
    uint64_t cc_dst_2;
    uint32_t cc_op_2;
    uint64_t rdi2_1;
    uint64_t rsi3_1;
    uint64_t rcx_1;
    uint64_t cc_src_3;
    uint64_t cc_dst_3;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_58;
    uint64_t var_59;
    uint32_t cc_op_3;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t rax_0;
    uint64_t var_51;
    uint64_t cc_src_5;
    uint64_t cc_dst_5;
    uint32_t cc_op_5;
    uint64_t rdi2_2;
    uint64_t rsi3_2;
    uint64_t rcx_2;
    uint64_t cc_src_6;
    uint64_t cc_dst_6;
    uint64_t var_37;
    uint64_t var_38;
    uint32_t cc_op_6;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint32_t *var_112;
    uint64_t cc_src_7;
    uint64_t cc_dst_7;
    uint32_t cc_op_7;
    uint64_t rdi2_3;
    uint64_t rsi3_3;
    uint64_t rcx_3;
    uint64_t cc_src_8;
    uint64_t cc_dst_8;
    uint64_t var_43;
    uint64_t var_44;
    uint32_t cc_op_8;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint32_t *var_109;
    uint32_t var_110;
    uint64_t cc_src_9;
    uint64_t cc_dst_9;
    uint32_t cc_op_9;
    uint64_t rdi2_4;
    uint64_t rsi3_4;
    uint64_t rcx_4;
    uint64_t cc_src_10;
    uint64_t cc_dst_10;
    uint64_t var_49;
    uint64_t var_50;
    uint32_t cc_op_10;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_60;
    uint32_t cc_op_13;
    uint64_t rdi2_6;
    uint64_t rsi3_6;
    uint64_t rcx_6;
    uint64_t cc_src_14;
    uint64_t cc_dst_14;
    uint64_t var_61;
    uint64_t var_62;
    uint32_t cc_op_14;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t cc_src_15;
    uint64_t cc_dst_15;
    uint32_t cc_op_15;
    uint64_t rdi2_7;
    uint64_t rsi3_7;
    uint64_t rcx_7;
    uint64_t cc_src_16;
    uint64_t cc_dst_16;
    uint64_t var_67;
    uint64_t var_68;
    uint32_t cc_op_16;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint32_t *var_103;
    uint32_t var_104;
    uint64_t cc_src_17;
    uint64_t cc_dst_17;
    uint32_t cc_op_17;
    uint64_t rdi2_8;
    uint64_t rsi3_8;
    uint64_t rcx_8;
    uint64_t cc_src_18;
    uint64_t cc_dst_18;
    uint64_t var_73;
    uint64_t var_74;
    uint32_t cc_op_18;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint32_t *var_99;
    uint32_t var_100;
    uint32_t *var_101;
    uint32_t *var_102;
    uint64_t cc_src_19;
    uint64_t cc_dst_19;
    uint32_t cc_op_19;
    uint64_t rdi2_9;
    uint64_t rsi3_9;
    uint64_t rcx_9;
    uint64_t cc_src_20;
    uint64_t cc_dst_20;
    uint64_t var_79;
    uint64_t var_80;
    uint32_t cc_op_20;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint32_t *var_93;
    uint32_t var_94;
    uint32_t *var_95;
    uint32_t *var_96;
    uint32_t *var_97;
    uint32_t *var_98;
    uint32_t *var_111;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_cc_src();
    var_2 = init_rbx();
    var_3 = init_rbp();
    var_4 = init_r12();
    var_5 = init_cc_src2();
    var_6 = init_state_0x82fc();
    *(uint64_t *)(var_0 + (-8L)) = var_4;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    var_7 = (uint64_t)(uint32_t)rsi;
    var_8 = (uint64_t)(unsigned char)rsi;
    var_9 = (var_8 == 0UL);
    cc_src_11 = 0UL;
    cc_op_11 = 22U;
    rdi2_5 = 4264133UL;
    rcx_5 = 3UL;
    rax_1 = 1UL;
    cc_src_13 = 0UL;
    cc_src_0 = var_1;
    cc_dst_0 = 0UL;
    cc_op_0 = 25U;
    rdi2_0 = 4264193UL;
    rcx_0 = 6UL;
    cc_src_2 = var_1;
    cc_dst_2 = 0UL;
    cc_op_2 = 25U;
    rdi2_1 = 4264193UL;
    rcx_1 = 6UL;
    cc_src_5 = 0UL;
    cc_op_5 = 22U;
    rdi2_2 = 4264121UL;
    rcx_2 = 7UL;
    cc_src_7 = 0UL;
    cc_op_7 = 22U;
    rdi2_3 = 4264128UL;
    rcx_3 = 5UL;
    cc_src_9 = 0UL;
    cc_op_9 = 22U;
    rdi2_4 = 4265321UL;
    rcx_4 = 3UL;
    cc_op_13 = 22U;
    rdi2_6 = 4264136UL;
    rcx_6 = 5UL;
    cc_src_15 = 0UL;
    cc_op_15 = 22U;
    rdi2_7 = 4264141UL;
    rcx_7 = 7UL;
    cc_src_17 = 0UL;
    cc_op_17 = 22U;
    rdi2_8 = 4264148UL;
    rcx_8 = 6UL;
    cc_src_19 = 0UL;
    cc_op_19 = 22U;
    rdi2_9 = 4264154UL;
    rcx_9 = 7UL;
    if (var_9) {
        var_24 = (uint64_t)*(uint32_t *)(rdi + 8UL);
        var_25 = var_0 + (-32L);
        *(uint64_t *)var_25 = 4206420UL;
        var_26 = indirect_placeholder_1(var_24, rdx);
        local_sp_0 = var_25;
        if (var_26 != 0UL) {
            var_27 = *(uint32_t *)(rdi + 24UL) ^ (-1);
            var_28 = (uint32_t *)var_26;
            *var_28 = ((*var_28 & var_27) | *(uint32_t *)(rdi + 16UL));
            return rax_1;
        }
        var_29 = *(uint64_t *)rdi;
        var_30 = (uint64_t)var_6;
        rsi3_0 = var_29;
        _pre_phi = var_30;
        rbx_0 = var_29;
        cc_op_0 = 14U;
        cc_src_1 = cc_src_0;
        cc_dst_1 = cc_dst_0;
        cc_op_1 = cc_op_0;
        while (rcx_0 != 0UL)
            {
                var_31 = (uint64_t)*(unsigned char *)rdi2_0;
                var_32 = (uint64_t)*(unsigned char *)rsi3_0 - var_31;
                cc_src_0 = var_31;
                cc_dst_0 = var_32;
                cc_src_1 = var_31;
                cc_dst_1 = var_32;
                cc_op_1 = 14U;
                if ((uint64_t)(unsigned char)var_32 == 0UL) {
                    break;
                }
                rdi2_0 = rdi2_0 + var_30;
                rsi3_0 = rsi3_0 + var_30;
                rcx_0 = rcx_0 + (-1L);
                cc_op_0 = 14U;
                cc_src_1 = cc_src_0;
                cc_dst_1 = cc_dst_0;
                cc_op_1 = cc_op_0;
            }
        var_33 = helper_cc_compute_all_wrapper(cc_dst_1, cc_src_1, var_5, cc_op_1);
        var_34 = ((var_33 & 65UL) == 0UL);
        var_35 = helper_cc_compute_c_wrapper(cc_dst_1, var_33, var_5, 1U);
        var_36 = (uint64_t)((unsigned char)var_34 - (unsigned char)var_35);
        rax_0 = var_36;
        cc_src2_0 = var_35;
        if (var_36 != 0UL) {
            var_112 = (uint32_t *)(rdx + 8UL);
            *var_112 = ((*var_112 & (-817)) | 288U);
            return rax_1;
        }
    }
    rax_1 = 0UL;
    if ((*(unsigned char *)(rdi + 12UL) & '\x04') == '\x00') {
        return rax_1;
    }
    var_10 = (uint64_t)*(uint32_t *)(rdi + 8UL);
    var_11 = var_0 + (-32L);
    *(uint64_t *)var_11 = 4205500UL;
    var_12 = indirect_placeholder_29(0UL, var_10, rdx);
    local_sp_0 = var_11;
    rax_1 = var_7;
    if (var_12 != 0UL) {
        var_13 = *(uint64_t *)(rdi + 16UL);
        var_14 = *(uint32_t *)(rdi + 24UL);
        var_15 = (uint32_t *)var_12;
        *var_15 = (*var_15 & ((var_14 | (uint32_t)var_13) ^ (-1)));
        return rax_1;
    }
    var_16 = *(uint64_t *)rdi;
    var_17 = (uint64_t)var_6;
    rsi3_1 = var_16;
    _pre_phi = var_17;
    rbx_0 = var_16;
    cc_op_2 = 14U;
    cc_src_3 = cc_src_2;
    cc_dst_3 = cc_dst_2;
    cc_op_3 = cc_op_2;
    while (rcx_1 != 0UL)
        {
            var_18 = (uint64_t)*(unsigned char *)rdi2_1;
            var_19 = (uint64_t)*(unsigned char *)rsi3_1 - var_18;
            cc_src_2 = var_18;
            cc_dst_2 = var_19;
            cc_src_3 = var_18;
            cc_dst_3 = var_19;
            cc_op_3 = 14U;
            if ((uint64_t)(unsigned char)var_19 == 0UL) {
                break;
            }
            rdi2_1 = rdi2_1 + var_17;
            rsi3_1 = rsi3_1 + var_17;
            rcx_1 = rcx_1 + (-1L);
            cc_op_2 = 14U;
            cc_src_3 = cc_src_2;
            cc_dst_3 = cc_dst_2;
            cc_op_3 = cc_op_2;
        }
    var_20 = helper_cc_compute_all_wrapper(cc_dst_3, cc_src_3, var_5, cc_op_3);
    var_21 = ((var_20 & 65UL) == 0UL);
    var_22 = helper_cc_compute_c_wrapper(cc_dst_3, var_20, var_5, 1U);
    var_23 = (uint64_t)((unsigned char)var_21 - (unsigned char)var_22);
    rax_0 = var_23;
    cc_src2_0 = var_22;
    if (var_23 != 0UL) {
        var_111 = (uint32_t *)(rdx + 8UL);
        *var_111 = ((*var_111 & (-305)) | 48U);
        return rax_1;
    }
    rsi3_5 = rbx_0;
    rax_1 = var_7;
    cc_dst_5 = rax_0;
    rsi3_2 = rbx_0;
    rsi3_3 = rbx_0;
    rsi3_4 = rbx_0;
    rsi3_6 = rbx_0;
    rsi3_7 = rbx_0;
    rsi3_8 = rbx_0;
    rsi3_9 = rbx_0;
    cc_op_5 = 14U;
    cc_src_6 = cc_src_5;
    cc_dst_6 = cc_dst_5;
    cc_op_6 = cc_op_5;
    while (rcx_2 != 0UL)
        {
            var_37 = (uint64_t)*(unsigned char *)rdi2_2;
            var_38 = (uint64_t)*(unsigned char *)rsi3_2 - var_37;
            cc_src_5 = var_37;
            cc_dst_5 = var_38;
            cc_src_6 = var_37;
            cc_dst_6 = var_38;
            cc_op_6 = 14U;
            if ((uint64_t)(unsigned char)var_38 == 0UL) {
                break;
            }
            rdi2_2 = rdi2_2 + _pre_phi;
            rsi3_2 = rsi3_2 + _pre_phi;
            rcx_2 = rcx_2 + (-1L);
            cc_op_5 = 14U;
            cc_src_6 = cc_src_5;
            cc_dst_6 = cc_dst_5;
            cc_op_6 = cc_op_5;
        }
    var_39 = helper_cc_compute_all_wrapper(cc_dst_6, cc_src_6, cc_src2_0, cc_op_6);
    var_40 = ((var_39 & 65UL) == 0UL);
    var_41 = helper_cc_compute_c_wrapper(cc_dst_6, var_39, cc_src2_0, 1U);
    var_42 = (uint64_t)((unsigned char)var_40 - (unsigned char)var_41);
    cc_dst_7 = var_42;
    if (var_42 == 0UL) {
        if (!var_9) {
            var_112 = (uint32_t *)(rdx + 8UL);
            *var_112 = ((*var_112 & (-817)) | 288U);
            return rax_1;
        }
        var_111 = (uint32_t *)(rdx + 8UL);
        *var_111 = ((*var_111 & (-305)) | 48U);
        return rax_1;
    }
    cc_op_7 = 14U;
    cc_src_8 = cc_src_7;
    cc_dst_8 = cc_dst_7;
    cc_op_8 = cc_op_7;
    while (rcx_3 != 0UL)
        {
            var_43 = (uint64_t)*(unsigned char *)rdi2_3;
            var_44 = (uint64_t)*(unsigned char *)rsi3_3 - var_43;
            cc_src_7 = var_43;
            cc_dst_7 = var_44;
            cc_src_8 = var_43;
            cc_dst_8 = var_44;
            cc_op_8 = 14U;
            if ((uint64_t)(unsigned char)var_44 == 0UL) {
                break;
            }
            rdi2_3 = rdi2_3 + _pre_phi;
            rsi3_3 = rsi3_3 + _pre_phi;
            rcx_3 = rcx_3 + (-1L);
            cc_op_7 = 14U;
            cc_src_8 = cc_src_7;
            cc_dst_8 = cc_dst_7;
            cc_op_8 = cc_op_7;
        }
    var_45 = helper_cc_compute_all_wrapper(cc_dst_8, cc_src_8, var_41, cc_op_8);
    var_46 = ((var_45 & 65UL) == 0UL);
    var_47 = helper_cc_compute_c_wrapper(cc_dst_8, var_45, var_41, 1U);
    var_48 = (uint64_t)((unsigned char)var_46 - (unsigned char)var_47);
    cc_dst_9 = var_48;
    if (var_48 == 0UL) {
        var_109 = (uint32_t *)(rdx + 8UL);
        var_110 = *var_109;
        if (var_9) {
            *var_109 = ((var_110 & (-817)) | 800U);
        } else {
            *var_109 = ((var_110 & (-305)) | 48U);
        }
    } else {
        rax_1 = 1UL;
        cc_op_9 = 14U;
        cc_src_10 = cc_src_9;
        cc_dst_10 = cc_dst_9;
        cc_op_10 = cc_op_9;
        while (rcx_4 != 0UL)
            {
                var_49 = (uint64_t)*(unsigned char *)rdi2_4;
                var_50 = (uint64_t)*(unsigned char *)rsi3_4 - var_49;
                cc_src_9 = var_49;
                cc_dst_9 = var_50;
                cc_src_10 = var_49;
                cc_dst_10 = var_50;
                cc_op_10 = 14U;
                if ((uint64_t)(unsigned char)var_50 == 0UL) {
                    break;
                }
                rdi2_4 = rdi2_4 + _pre_phi;
                rsi3_4 = rsi3_4 + _pre_phi;
                rcx_4 = rcx_4 + (-1L);
                cc_op_9 = 14U;
                cc_src_10 = cc_src_9;
                cc_dst_10 = cc_dst_9;
                cc_op_10 = cc_op_9;
            }
        var_51 = helper_cc_compute_all_wrapper(cc_dst_10, cc_src_10, var_47, cc_op_10);
        var_52 = ((var_51 & 65UL) == 0UL);
        var_53 = helper_cc_compute_c_wrapper(cc_dst_10, var_51, var_47, 1U);
        var_54 = (uint64_t)((unsigned char)var_52 - (unsigned char)var_53);
        cc_dst_11 = var_54;
        if (var_54 == 0UL) {
            var_105 = (uint32_t *)rdx;
            var_106 = *var_105;
            rax_1 = var_7;
            if (var_9) {
                *var_105 = (var_106 & (-257));
                var_108 = (uint32_t *)(rdx + 4UL);
                *var_108 = (*var_108 & (-5));
            } else {
                *var_105 = ((var_106 & (-65473)) | ((uint32_t)((uint16_t)var_106 & (unsigned short)65024U) | 256U));
                var_107 = (uint32_t *)(rdx + 4UL);
                *var_107 = ((*var_107 & (-45)) | 4U);
            }
        } else {
            cc_op_11 = 14U;
            cc_src_12 = cc_src_11;
            cc_dst_12 = cc_dst_11;
            cc_op_12 = cc_op_11;
            while (rcx_5 != 0UL)
                {
                    var_55 = (uint64_t)*(unsigned char *)rdi2_5;
                    var_56 = (uint64_t)*(unsigned char *)rsi3_5 - var_55;
                    cc_src_11 = var_55;
                    cc_dst_11 = var_56;
                    cc_src_12 = var_55;
                    cc_dst_12 = var_56;
                    cc_op_12 = 14U;
                    if ((uint64_t)(unsigned char)var_56 == 0UL) {
                        break;
                    }
                    rdi2_5 = rdi2_5 + _pre_phi;
                    rsi3_5 = rsi3_5 + _pre_phi;
                    rcx_5 = rcx_5 + (-1L);
                    cc_op_11 = 14U;
                    cc_src_12 = cc_src_11;
                    cc_dst_12 = cc_dst_11;
                    cc_op_12 = cc_op_11;
                }
            var_57 = helper_cc_compute_all_wrapper(cc_dst_12, cc_src_12, var_53, cc_op_12);
            var_58 = ((var_57 & 65UL) == 0UL);
            var_59 = helper_cc_compute_c_wrapper(cc_dst_12, var_57, var_53, 1U);
            var_60 = (uint64_t)((unsigned char)var_58 - (unsigned char)var_59);
            cc_dst_13 = var_60;
            if (var_60 == 0UL) {
                *(unsigned char *)(rdx + 19UL) = (unsigned char)'\x7f';
                *(unsigned char *)(rdx + 20UL) = (unsigned char)'\x15';
            } else {
                cc_op_13 = 14U;
                cc_src_14 = cc_src_13;
                cc_dst_14 = cc_dst_13;
                cc_op_14 = cc_op_13;
                while (rcx_6 != 0UL)
                    {
                        var_61 = (uint64_t)*(unsigned char *)rdi2_6;
                        var_62 = (uint64_t)*(unsigned char *)rsi3_6 - var_61;
                        cc_src_13 = var_61;
                        cc_dst_13 = var_62;
                        cc_src_14 = var_61;
                        cc_dst_14 = var_62;
                        cc_op_14 = 14U;
                        if ((uint64_t)(unsigned char)var_62 == 0UL) {
                            break;
                        }
                        rdi2_6 = rdi2_6 + _pre_phi;
                        rsi3_6 = rsi3_6 + _pre_phi;
                        rcx_6 = rcx_6 + (-1L);
                        cc_op_13 = 14U;
                        cc_src_14 = cc_src_13;
                        cc_dst_14 = cc_dst_13;
                        cc_op_14 = cc_op_13;
                    }
                var_63 = helper_cc_compute_all_wrapper(cc_dst_14, cc_src_14, var_59, cc_op_14);
                var_64 = ((var_63 & 65UL) == 0UL);
                var_65 = helper_cc_compute_c_wrapper(cc_dst_14, var_63, var_59, 1U);
                var_66 = (uint64_t)((unsigned char)var_64 - (unsigned char)var_65);
                cc_dst_15 = var_66;
                if (var_66 == 0UL) {
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4205844UL;
                    indirect_placeholder_42(rdx);
                } else {
                    cc_op_15 = 14U;
                    cc_src_16 = cc_src_15;
                    cc_dst_16 = cc_dst_15;
                    cc_op_16 = cc_op_15;
                    while (rcx_7 != 0UL)
                        {
                            var_67 = (uint64_t)*(unsigned char *)rdi2_7;
                            var_68 = (uint64_t)*(unsigned char *)rsi3_7 - var_67;
                            cc_src_15 = var_67;
                            cc_dst_15 = var_68;
                            cc_src_16 = var_67;
                            cc_dst_16 = var_68;
                            cc_op_16 = 14U;
                            if ((uint64_t)(unsigned char)var_68 == 0UL) {
                                break;
                            }
                            rdi2_7 = rdi2_7 + _pre_phi;
                            rsi3_7 = rsi3_7 + _pre_phi;
                            rcx_7 = rcx_7 + (-1L);
                            cc_op_15 = 14U;
                            cc_src_16 = cc_src_15;
                            cc_dst_16 = cc_dst_15;
                            cc_op_16 = cc_op_15;
                        }
                    var_69 = helper_cc_compute_all_wrapper(cc_dst_16, cc_src_16, var_65, cc_op_16);
                    var_70 = ((var_69 & 65UL) == 0UL);
                    var_71 = helper_cc_compute_c_wrapper(cc_dst_16, var_69, var_65, 1U);
                    var_72 = (uint64_t)((unsigned char)var_70 - (unsigned char)var_71);
                    cc_dst_17 = var_72;
                    if (var_72 == 0UL) {
                        var_103 = (uint32_t *)(rdx + 12UL);
                        var_104 = *var_103;
                        if (var_9) {
                            *var_103 = (var_104 & (-3));
                        } else {
                            *var_103 = (var_104 | 2U);
                            rax_1 = var_7;
                        }
                    } else {
                        rax_1 = var_7;
                        cc_op_17 = 14U;
                        cc_src_18 = cc_src_17;
                        cc_dst_18 = cc_dst_17;
                        cc_op_18 = cc_op_17;
                        while (rcx_8 != 0UL)
                            {
                                var_73 = (uint64_t)*(unsigned char *)rdi2_8;
                                var_74 = (uint64_t)*(unsigned char *)rsi3_8 - var_73;
                                cc_src_17 = var_73;
                                cc_dst_17 = var_74;
                                cc_src_18 = var_73;
                                cc_dst_18 = var_74;
                                cc_op_18 = 14U;
                                if ((uint64_t)(unsigned char)var_74 == 0UL) {
                                    break;
                                }
                                rdi2_8 = rdi2_8 + _pre_phi;
                                rsi3_8 = rsi3_8 + _pre_phi;
                                rcx_8 = rcx_8 + (-1L);
                                cc_op_17 = 14U;
                                cc_src_18 = cc_src_17;
                                cc_dst_18 = cc_dst_17;
                                cc_op_18 = cc_op_17;
                            }
                        var_75 = helper_cc_compute_all_wrapper(cc_dst_18, cc_src_18, var_71, cc_op_18);
                        var_76 = ((var_75 & 65UL) == 0UL);
                        var_77 = helper_cc_compute_c_wrapper(cc_dst_18, var_75, var_71, 1U);
                        var_78 = (uint64_t)((unsigned char)var_76 - (unsigned char)var_77);
                        cc_dst_19 = var_78;
                        if (var_78 == 0UL) {
                            var_99 = (uint32_t *)(rdx + 8UL);
                            var_100 = *var_99 & (-305);
                            if (var_9) {
                                *var_99 = (var_100 | 48U);
                                var_102 = (uint32_t *)rdx;
                                *var_102 = (*var_102 & (-33));
                            } else {
                                *var_99 = (var_100 | 288U);
                                var_101 = (uint32_t *)rdx;
                                *var_101 = (*var_101 | 32U);
                            }
                        } else {
                            cc_op_19 = 14U;
                            cc_src_20 = cc_src_19;
                            cc_dst_20 = cc_dst_19;
                            cc_op_20 = cc_op_19;
                            while (rcx_9 != 0UL)
                                {
                                    var_79 = (uint64_t)*(unsigned char *)rdi2_9;
                                    var_80 = (uint64_t)*(unsigned char *)rsi3_9 - var_79;
                                    cc_src_19 = var_79;
                                    cc_dst_19 = var_80;
                                    cc_src_20 = var_79;
                                    cc_dst_20 = var_80;
                                    cc_op_20 = 14U;
                                    if ((uint64_t)(unsigned char)var_80 == 0UL) {
                                        break;
                                    }
                                    rdi2_9 = rdi2_9 + _pre_phi;
                                    rsi3_9 = rsi3_9 + _pre_phi;
                                    rcx_9 = rcx_9 + (-1L);
                                    cc_op_19 = 14U;
                                    cc_src_20 = cc_src_19;
                                    cc_dst_20 = cc_dst_19;
                                    cc_op_20 = cc_op_19;
                                }
                            var_81 = helper_cc_compute_all_wrapper(cc_dst_20, cc_src_20, var_77, cc_op_20);
                            var_82 = ((var_81 & 65UL) == 0UL);
                            var_83 = helper_cc_compute_c_wrapper(cc_dst_20, var_81, var_77, 1U);
                            var_84 = (uint64_t)((unsigned char)var_82 - (unsigned char)var_83);
                            var_85 = (uint64_t)(uint32_t)var_84;
                            if (var_84 == 0UL) {
                                var_93 = (uint32_t *)(rdx + 8UL);
                                var_94 = *var_93 & (-305);
                                if (var_9) {
                                    *var_93 = (var_94 | 48U);
                                    var_97 = (uint32_t *)rdx;
                                    *var_97 = (*var_97 & (-33));
                                    var_98 = (uint32_t *)(rdx + 4UL);
                                    *var_98 = (*var_98 & (-2));
                                } else {
                                    *var_93 = (var_94 | 288U);
                                    var_95 = (uint32_t *)rdx;
                                    *var_95 = (*var_95 | 32U);
                                    var_96 = (uint32_t *)(rdx + 4UL);
                                    *var_96 = (*var_96 | 1U);
                                }
                            } else {
                                *(uint64_t *)(local_sp_0 + (-8L)) = 4206083UL;
                                indirect_placeholder();
                                if (var_85 == 0UL) {
                                    var_86 = *(unsigned char *)rbx_0;
                                    if (!((((uint64_t)(var_86 + '\x8e') == 0UL) ^ 1) || var_9)) {
                                        var_87 = var_8 | ((uint64_t)(var_86 + '\x9d') != 0UL);
                                        rax_1 = var_87;
                                        if (var_87 != 0UL) {
                                            *(uint32_t *)rdx = 0U;
                                            var_88 = (uint32_t *)(rdx + 4UL);
                                            *var_88 = (*var_88 & (-2));
                                            var_89 = (uint32_t *)(rdx + 12UL);
                                            *var_89 = (*var_89 & (-4));
                                            *(unsigned char *)(rdx + 23UL) = (unsigned char)'\x01';
                                            *(unsigned char *)(rdx + 22UL) = (unsigned char)'\x00';
                                            return rax_1;
                                        }
                                    }
                                    var_90 = (uint32_t *)rdx;
                                    *var_90 = (*var_90 | 1318U);
                                    var_91 = (uint32_t *)(rdx + 4UL);
                                    *var_91 = (*var_91 | 1U);
                                    var_92 = (uint32_t *)(rdx + 12UL);
                                    *var_92 = (*var_92 | 3U);
                                } else {
                                    *(uint64_t *)(local_sp_0 + (-16L)) = 4206100UL;
                                    indirect_placeholder();
                                    *(uint64_t *)(local_sp_0 + (-24L)) = 4206206UL;
                                    indirect_placeholder();
                                    *(uint64_t *)(local_sp_0 + (-32L)) = 4206261UL;
                                    indirect_placeholder();
                                    *(uint64_t *)(local_sp_0 + (-40L)) = 4206318UL;
                                    indirect_placeholder();
                                    *(uint64_t *)(local_sp_0 + (-48L)) = 4206354UL;
                                    indirect_placeholder();
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
