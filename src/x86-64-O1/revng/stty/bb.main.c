typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_65_ret_type;
struct indirect_placeholder_66_ret_type;
struct indirect_placeholder_72_ret_type;
struct indirect_placeholder_68_ret_type;
struct indirect_placeholder_71_ret_type;
struct indirect_placeholder_67_ret_type;
struct indirect_placeholder_74_ret_type;
struct indirect_placeholder_73_ret_type;
struct indirect_placeholder_76_ret_type;
struct indirect_placeholder_75_ret_type;
struct indirect_placeholder_78_ret_type;
struct indirect_placeholder_77_ret_type;
struct indirect_placeholder_79_ret_type;
struct indirect_placeholder_80_ret_type;
struct indirect_placeholder_81_ret_type;
struct indirect_placeholder_82_ret_type;
struct indirect_placeholder_84_ret_type;
struct indirect_placeholder_65_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_66_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_72_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_68_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_71_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_67_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_74_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_73_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_76_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_75_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_78_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_77_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_79_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_80_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_81_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_82_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_84_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_40(void);
extern uint32_t init_state_0x82fc(void);
extern void indirect_placeholder_3(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_42(uint64_t param_0);
extern void indirect_placeholder_83(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_64(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_65_ret_type indirect_placeholder_65(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_66_ret_type indirect_placeholder_66(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_72_ret_type indirect_placeholder_72(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_68_ret_type indirect_placeholder_68(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_71_ret_type indirect_placeholder_71(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_67_ret_type indirect_placeholder_67(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_74_ret_type indirect_placeholder_74(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_73_ret_type indirect_placeholder_73(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_76_ret_type indirect_placeholder_76(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_75_ret_type indirect_placeholder_75(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_78_ret_type indirect_placeholder_78(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_77_ret_type indirect_placeholder_77(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_79_ret_type indirect_placeholder_79(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_80_ret_type indirect_placeholder_80(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_81_ret_type indirect_placeholder_81(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_82_ret_type indirect_placeholder_82(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_84_ret_type indirect_placeholder_84(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rdi, uint64_t rsi) {
    uint64_t r15_5_ph_ph;
    uint64_t local_sp_0;
    uint64_t var_88;
    struct indirect_placeholder_84_ret_type var_20;
    struct indirect_placeholder_65_ret_type var_60;
    struct indirect_placeholder_66_ret_type var_55;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint32_t var_8;
    uint32_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t rax_1;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint32_t var_61;
    uint64_t local_sp_4;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint32_t var_56;
    uint32_t _pre_phi;
    uint64_t local_sp_16;
    uint64_t r15_0;
    uint64_t var_79;
    uint64_t r15_3;
    uint64_t r15_1;
    uint64_t local_sp_1;
    uint64_t var_86;
    uint64_t var_87;
    struct indirect_placeholder_72_ret_type var_92;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t var_97;
    struct indirect_placeholder_71_ret_type var_98;
    uint64_t var_99;
    uint64_t var_100;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t local_sp_11;
    uint64_t local_sp_6;
    uint64_t r15_2;
    uint64_t var_91;
    struct indirect_placeholder_74_ret_type var_80;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t local_sp_7;
    struct indirect_placeholder_76_ret_type var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t rcx_0;
    uint64_t local_sp_10;
    uint64_t local_sp_8;
    struct indirect_placeholder_78_ret_type var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_23;
    uint64_t local_sp_16_ph_ph;
    uint64_t var_24;
    uint64_t r15_4;
    uint64_t local_sp_13;
    uint64_t cc_src2_0_ph_ph;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t local_sp_15;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    bool var_45;
    uint64_t var_46;
    struct indirect_placeholder_80_ret_type var_47;
    uint64_t r8_0;
    uint64_t var_49;
    uint64_t r9_0;
    uint64_t local_sp_9;
    uint64_t var_48;
    uint64_t var_50;
    struct indirect_placeholder_82_ret_type var_51;
    bool var_74;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint32_t _pre;
    uint64_t var_75;
    uint64_t r15_5;
    uint64_t rax_2;
    uint64_t rbp_0_ph_ph;
    uint64_t rbx_0;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t rbx_1_ph;
    uint64_t r15_5_ph;
    uint64_t local_sp_16_ph;
    uint64_t var_18;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t cc_src_0;
    uint64_t cc_dst_0;
    uint32_t cc_op_0;
    uint64_t rdi1_0;
    uint64_t rsi2_0;
    uint64_t rcx_1;
    uint64_t cc_src_1;
    uint64_t cc_dst_1;
    uint64_t var_30;
    uint64_t var_31;
    uint32_t cc_op_1;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t local_sp_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_19;
    uint64_t var_21;
    uint32_t var_22;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_cc_src2();
    var_8 = init_state_0x82fc();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_9 = (uint32_t)rdi;
    var_10 = (uint64_t)var_9;
    var_11 = *(uint64_t *)rsi;
    *(uint64_t *)(var_0 + (-96L)) = 4212788UL;
    indirect_placeholder_42(var_11);
    var_12 = var_0 + (-104L);
    *(uint64_t *)var_12 = 4212803UL;
    indirect_placeholder();
    var_13 = var_0 + (-112L);
    *(uint64_t *)var_13 = 4212813UL;
    indirect_placeholder();
    *(uint32_t *)4301560UL = 0U;
    *(unsigned char *)(var_0 + (-97L)) = (unsigned char)'\x01';
    *(unsigned char *)(var_0 + (-99L)) = (unsigned char)'\x00';
    *(unsigned char *)(var_0 + (-98L)) = (unsigned char)'\x00';
    *(uint32_t *)var_12 = 0U;
    var_14 = (uint64_t)var_8;
    r15_5_ph_ph = 0UL;
    rax_1 = 0UL;
    r15_0 = 4264712UL;
    r15_3 = 4264712UL;
    r15_1 = 4264712UL;
    local_sp_10 = (uint64_t)0L;
    local_sp_16_ph_ph = var_13;
    cc_src2_0_ph_ph = var_7;
    rbp_0_ph_ph = 0UL;
    rbx_1_ph = 1UL;
    cc_op_0 = 8U;
    rdi1_0 = 4264743UL;
    rcx_1 = 7UL;
    while (1U)
        {
            var_15 = rbp_0_ph_ph << 32UL;
            var_16 = (uint64_t)((long)var_15 >> (long)29UL) + rsi;
            var_17 = (uint64_t)(var_9 - (uint32_t)rbp_0_ph_ph);
            r15_5_ph = r15_5_ph_ph;
            local_sp_16_ph = local_sp_16_ph_ph;
            while (1U)
                {
                    var_18 = rbx_1_ph << 32UL;
                    rbx_0 = rbx_1_ph;
                    cc_src_0 = rbx_1_ph;
                    r15_5 = r15_5_ph;
                    local_sp_16 = local_sp_16_ph;
                    while (1U)
                        {
                            var_19 = local_sp_16 + (-8L);
                            *(uint64_t *)var_19 = 4213114UL;
                            var_20 = indirect_placeholder_84(4264750UL, var_17, var_16, 4274720UL, 0UL);
                            var_21 = var_20.field_0;
                            var_22 = (uint32_t)var_21;
                            r15_5_ph_ph = r15_5;
                            local_sp_16 = var_19;
                            local_sp_11 = var_19;
                            r15_4 = r15_5;
                            local_sp_13 = var_19;
                            local_sp_15 = var_19;
                            local_sp_9 = var_19;
                            local_sp_16_ph = var_19;
                            if ((uint64_t)(var_22 + 1U) != 0UL) {
                                var_42 = var_20.field_1;
                                var_43 = var_20.field_2;
                                var_44 = var_20.field_3;
                                var_45 = (*(unsigned char *)(local_sp_16 + 6UL) == '\x00');
                                rcx_0 = var_42;
                                r8_0 = var_43;
                                r9_0 = var_44;
                                if (!var_45) {
                                    loop_state_var = 4U;
                                    break;
                                }
                                if (*(unsigned char *)(local_sp_16 + 5UL) != '\x00') {
                                    loop_state_var = 4U;
                                    break;
                                }
                                var_46 = local_sp_16 + (-16L);
                                *(uint64_t *)var_46 = 4213347UL;
                                var_47 = indirect_placeholder_80(0UL, 4273520UL, 1UL, 0UL, var_42, var_43, var_44);
                                rcx_0 = var_47.field_2;
                                r8_0 = var_47.field_3;
                                r9_0 = var_47.field_4;
                                local_sp_9 = var_46;
                                loop_state_var = 3U;
                                break;
                            }
                            var_23 = var_21 + (-70L);
                            if ((uint64_t)(uint32_t)var_23 == 0UL) {
                                if (r15_5 != 0UL) {
                                    var_36 = var_20.field_1;
                                    var_37 = var_20.field_2;
                                    var_38 = var_20.field_3;
                                    var_39 = local_sp_16 + (-16L);
                                    *(uint64_t *)var_39 = 4213189UL;
                                    indirect_placeholder_79(0UL, 4273480UL, 1UL, 0UL, var_36, var_37, var_38);
                                    local_sp_15 = var_39;
                                    loop_state_var = 0U;
                                    break;
                                }
                                r15_4 = *(uint64_t *)4302360UL;
                            } else {
                                var_24 = helper_cc_compute_all_wrapper(var_23, 70UL, cc_src2_0_ph_ph, 16U);
                                if ((uint64_t)(((unsigned char)(var_24 >> 4UL) ^ (unsigned char)var_24) & '\xc0') != 0UL) {
                                    loop_state_var = 5U;
                                    break;
                                }
                                if ((uint64_t)(var_22 + (-97)) == 0UL) {
                                    *(unsigned char *)(local_sp_16 + 6UL) = (unsigned char)'\x01';
                                    *(uint32_t *)local_sp_16 = 1U;
                                } else {
                                    if ((uint64_t)(var_22 + (-103)) != 0UL) {
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    *(unsigned char *)(local_sp_16 + 5UL) = (unsigned char)'\x01';
                                    *(uint32_t *)local_sp_16 = 2U;
                                }
                            }
                            r15_5_ph = r15_4;
                            r15_5 = r15_4;
                            if ((long)var_18 < (long)((uint64_t)*(uint32_t *)4301564UL << 32UL)) {
                                continue;
                            }
                            loop_state_var = 2U;
                            break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 2U:
                        {
                            rax_2 = (((uint64_t)((long)var_15 >> (long)32UL) + (uint64_t)((long)var_18 >> (long)32UL)) << 3UL) + rsi;
                            var_40 = rbx_0 + 1UL;
                            var_41 = (uint64_t)(uint32_t)var_40;
                            *(uint64_t *)rax_2 = 0UL;
                            rbx_0 = var_41;
                            rbx_1_ph = var_41;
                            while ((long)(uint64_t)((long)(var_40 << 32UL) >> (long)32UL) >= (long)(uint64_t)*(uint32_t *)4301564UL)
                                {
                                    rax_2 = rax_2 + 8UL;
                                    var_40 = rbx_0 + 1UL;
                                    var_41 = (uint64_t)(uint32_t)var_40;
                                    *(uint64_t *)rax_2 = 0UL;
                                    rbx_0 = var_41;
                                    rbx_1_ph = var_41;
                                }
                            continue;
                        }
                        break;
                      case 4U:
                        {
                            r15_0 = r15_5;
                            if (*(unsigned char *)(local_sp_16 + 7UL) == '\x00') {
                                local_sp_10 = var_19;
                                if (r15_5 != 0UL) {
                                    loop_state_var = 3U;
                                    switch_state_var = 1;
                                    break;
                                }
                                loop_state_var = 2U;
                                switch_state_var = 1;
                                break;
                            }
                            if (!var_45) {
                                loop_state_var = 4U;
                                switch_state_var = 1;
                                break;
                            }
                            if (*(unsigned char *)(local_sp_16 + 5UL) != '\x00') {
                                loop_state_var = 4U;
                                switch_state_var = 1;
                                break;
                            }
                            if (r15_5 != 0UL) {
                                loop_state_var = 2U;
                                switch_state_var = 1;
                                break;
                            }
                            if (1) {
                                loop_state_var = 3U;
                                switch_state_var = 1;
                                break;
                            }
                            *(uint64_t *)(local_sp_16 + (-24L)) = (local_sp_16 + 23UL);
                            var_49 = local_sp_16 + 22UL;
                            *(uint64_t *)(local_sp_16 + (-32L)) = 4213313UL;
                            indirect_placeholder_64(rsi, 1UL, r15_5, var_10, 4301856UL, var_49);
                            local_sp_10 = local_sp_16 + (-16L);
                            loop_state_var = 3U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 0U:
                        {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 1U:
                        {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 3U:
                        {
                            loop_state_var = 4U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 5U:
                        {
                            loop_state_var = 5U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    var_28 = rbp_0_ph_ph + rbx_1_ph;
                    var_29 = (uint64_t)(uint32_t)var_28;
                    cc_dst_0 = var_28;
                    rsi2_0 = *(uint64_t *)((uint64_t)((long)(var_28 << 32UL) >> (long)29UL) + rsi);
                    local_sp_14 = local_sp_13;
                    rbp_0_ph_ph = var_29;
                    cc_op_0 = 14U;
                    cc_src_1 = cc_src_0;
                    cc_dst_1 = cc_dst_0;
                    cc_op_1 = cc_op_0;
                    while (rcx_1 != 0UL)
                        {
                            rdi1_0 = rdi1_0 + var_14;
                            rsi2_0 = rsi2_0 + var_14;
                            rcx_1 = rcx_1 + (-1L);
                            cc_op_0 = 14U;
                            cc_src_1 = cc_src_0;
                            cc_dst_1 = cc_dst_0;
                            cc_op_1 = cc_op_0;
                        }
                    var_32 = helper_cc_compute_all_wrapper(cc_dst_1, cc_src_1, cc_src2_0_ph_ph, cc_op_1);
                    var_33 = ((var_32 & 65UL) == 0UL);
                    var_34 = helper_cc_compute_c_wrapper(cc_dst_1, var_32, cc_src2_0_ph_ph, 1U);
                    cc_src2_0_ph_ph = var_34;
                    if ((uint64_t)((unsigned char)var_33 - (unsigned char)var_34) == 0UL) {
                        var_35 = local_sp_13 + (-8L);
                        *(uint64_t *)var_35 = 4212987UL;
                        indirect_placeholder();
                        *(unsigned char *)(local_sp_13 + 7UL) = (unsigned char)'\x00';
                        local_sp_14 = var_35;
                    }
                    *(uint32_t *)4301564UL = 0U;
                    local_sp_16_ph_ph = local_sp_14;
                    continue;
                }
                break;
              case 5U:
                {
                    if ((uint64_t)(var_22 + 131U) != 0UL) {
                        if ((uint64_t)(var_22 + 130U) != 0UL) {
                            loop_state_var = 2U;
                            switch_state_var = 1;
                            break;
                        }
                    }
                    var_25 = *(uint64_t *)4301448UL;
                    var_26 = *(uint64_t *)4300352UL;
                    *(uint64_t *)(local_sp_16 + (-16L)) = 4212920UL;
                    indirect_placeholder_83(0UL, 4264039UL, var_26, 4264590UL, var_25, 4264727UL, 0UL);
                    var_27 = local_sp_16 + (-24L);
                    *(uint64_t *)var_27 = 4212930UL;
                    indirect_placeholder();
                    local_sp_13 = var_27;
                }
                break;
              case 3U:
                {
                    var_50 = local_sp_10 + (-8L);
                    *(uint64_t *)var_50 = 4213927UL;
                    var_51 = indirect_placeholder_82(2048UL, 0UL, r15_5, 0UL);
                    local_sp_8 = var_50;
                    if ((int)(uint32_t)var_51.field_0 >= (int)0U) {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    var_52 = var_51.field_2;
                    var_53 = var_51.field_1;
                    var_54 = local_sp_10 + (-16L);
                    *(uint64_t *)var_54 = 4213955UL;
                    var_55 = indirect_placeholder_66(0UL, var_53, 0UL, 3UL, var_52);
                    var_56 = (uint32_t)var_55.field_0;
                    local_sp_7 = var_54;
                    if ((uint64_t)(var_56 + 1U) != 0UL) {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    var_57 = var_55.field_1;
                    var_58 = (uint64_t)(var_56 & (-2049));
                    var_59 = local_sp_10 + (-24L);
                    *(uint64_t *)var_59 = 4213989UL;
                    var_60 = indirect_placeholder_65(0UL, var_58, 0UL, 4UL, var_57);
                    var_61 = (uint32_t)var_60.field_0;
                    _pre_phi = var_61;
                    local_sp_0 = var_59;
                    local_sp_7 = var_59;
                    if ((int)var_61 >= (int)0U) {
                        loop_state_var = 3U;
                        switch_state_var = 1;
                        break;
                    }
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 4U:
                {
                    var_48 = local_sp_9 + (-8L);
                    *(uint64_t *)var_48 = 4213372UL;
                    indirect_placeholder_81(0UL, 4273600UL, 1UL, 0UL, rcx_0, r8_0, r9_0);
                    local_sp_8 = var_48;
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 0U:
                {
                    loop_state_var = 2U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 2U:
                {
                    loop_state_var = 4U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 2U:
        {
            *(uint64_t *)(local_sp_15 + (-8L)) = 4213199UL;
            indirect_placeholder_3(rbx_1_ph, 0UL, rbp_0_ph_ph);
            abort();
        }
        break;
      case 4U:
      case 3U:
      case 1U:
      case 0U:
        {
            switch (loop_state_var) {
              case 4U:
                {
                    while (1U)
                        {
                            var_74 = ((*(unsigned char *)(local_sp_11 + 14UL) | *(unsigned char *)(local_sp_11 + 15UL)) == '\x00');
                            if (!var_74) {
                                var_75 = local_sp_11 + (-8L);
                                *(uint64_t *)var_75 = 4213784UL;
                                indirect_placeholder();
                                local_sp_1 = var_75;
                                local_sp_6 = var_75;
                                if (var_74) {
                                    loop_state_var = 2U;
                                    break;
                                }
                                *(uint64_t *)(local_sp_6 + (-8L)) = 4213498UL;
                                var_80 = indirect_placeholder_74(r15_3, 0UL, 3UL);
                                var_81 = var_80.field_0;
                                var_82 = var_80.field_1;
                                var_83 = var_80.field_2;
                                *(uint64_t *)(local_sp_6 + (-16L)) = 4213506UL;
                                indirect_placeholder();
                                var_84 = (uint64_t)*(uint32_t *)var_81;
                                var_85 = local_sp_6 + (-24L);
                                *(uint64_t *)var_85 = 4213531UL;
                                indirect_placeholder_73(0UL, 4278165UL, 1UL, var_84, var_81, var_82, var_83);
                                r15_2 = r15_3;
                                local_sp_4 = var_85;
                                var_91 = local_sp_4 + (-8L);
                                *(uint64_t *)var_91 = 4213552UL;
                                indirect_placeholder();
                                if ((uint64_t)(uint32_t)rax_1 != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                *(uint64_t *)(var_91 + (-8L)) = 4213667UL;
                                var_92 = indirect_placeholder_72(r15_2, 0UL, 3UL);
                                var_93 = var_92.field_0;
                                var_94 = var_92.field_1;
                                var_95 = var_92.field_2;
                                *(uint64_t *)(var_91 + (-16L)) = 4213675UL;
                                indirect_placeholder();
                                var_96 = (uint64_t)*(uint32_t *)var_93;
                                var_97 = var_91 + (-24L);
                                *(uint64_t *)var_97 = 4213700UL;
                                indirect_placeholder_68(0UL, 4278165UL, 1UL, var_96, var_93, var_94, var_95);
                                *(uint64_t *)(var_97 + (-8L)) = 4213718UL;
                                var_98 = indirect_placeholder_71(r15_2, 0UL, 3UL);
                                var_99 = var_98.field_0;
                                var_100 = var_98.field_1;
                                var_101 = var_98.field_2;
                                *(uint64_t *)(var_97 + (-16L)) = 4213726UL;
                                indirect_placeholder();
                                var_102 = (uint64_t)*(uint32_t *)var_99;
                                var_103 = var_97 + (-24L);
                                *(uint64_t *)var_103 = 4213751UL;
                                indirect_placeholder_67(0UL, 4278165UL, 1UL, var_102, var_99, var_100, var_101);
                                local_sp_11 = var_103;
                                continue;
                            }
                            if (*(unsigned char *)(local_sp_11 + 13UL) == '\x00') {
                                var_76 = local_sp_11 + 31UL;
                                *(uint64_t *)(local_sp_11 + (-16L)) = var_76;
                                var_77 = local_sp_11 + 30UL;
                                *(uint64_t *)(local_sp_11 + (-24L)) = 4213883UL;
                                indirect_placeholder_64(rsi, 1UL, 4264712UL, var_10, 4301856UL, var_77);
                                var_78 = local_sp_11 + (-8L);
                                _pre = (uint32_t)var_76;
                                _pre_phi = _pre;
                                local_sp_0 = var_78;
                                var_79 = local_sp_0 + (-8L);
                                *(uint64_t *)var_79 = 4214012UL;
                                indirect_placeholder();
                                r15_3 = r15_0;
                                r15_1 = r15_0;
                                local_sp_1 = var_79;
                                local_sp_6 = var_79;
                                r15_2 = r15_0;
                                if ((uint64_t)_pre_phi == 0UL) {
                                    if ((*(unsigned char *)(local_sp_0 + 6UL) | *(unsigned char *)(local_sp_0 + 7UL)) != '\x00') {
                                        loop_state_var = 2U;
                                        break;
                                    }
                                    if (*(unsigned char *)(local_sp_0 + 5UL) != '\x00') {
                                        loop_state_var = 2U;
                                        break;
                                    }
                                    var_88 = local_sp_0 + 22UL;
                                    *(unsigned char *)var_88 = (unsigned char)'\x00';
                                    var_89 = local_sp_0 + 23UL;
                                    *(unsigned char *)var_89 = (unsigned char)'\x00';
                                    *(uint64_t *)(local_sp_0 + (-24L)) = var_89;
                                    *(uint64_t *)(local_sp_0 + (-32L)) = 4214096UL;
                                    indirect_placeholder_64(rsi, 0UL, r15_0, var_10, 4301792UL, var_88);
                                    var_90 = local_sp_0 + (-16L);
                                    rax_1 = var_89;
                                    local_sp_4 = var_90;
                                    if (*(unsigned char *)(local_sp_0 + 15UL) != '\x00') {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                }
                                *(uint64_t *)(local_sp_6 + (-8L)) = 4213498UL;
                                var_80 = indirect_placeholder_74(r15_3, 0UL, 3UL);
                                var_81 = var_80.field_0;
                                var_82 = var_80.field_1;
                                var_83 = var_80.field_2;
                                *(uint64_t *)(local_sp_6 + (-16L)) = 4213506UL;
                                indirect_placeholder();
                                var_84 = (uint64_t)*(uint32_t *)var_81;
                                var_85 = local_sp_6 + (-24L);
                                *(uint64_t *)var_85 = 4213531UL;
                                indirect_placeholder_73(0UL, 4278165UL, 1UL, var_84, var_81, var_82, var_83);
                                r15_2 = r15_3;
                                local_sp_4 = var_85;
                            } else {
                                var_75 = local_sp_11 + (-8L);
                                *(uint64_t *)var_75 = 4213784UL;
                                indirect_placeholder();
                                local_sp_1 = var_75;
                                local_sp_6 = var_75;
                                if (var_74) {
                                    loop_state_var = 2U;
                                    break;
                                }
                                *(uint64_t *)(local_sp_6 + (-8L)) = 4213498UL;
                                var_80 = indirect_placeholder_74(r15_3, 0UL, 3UL);
                                var_81 = var_80.field_0;
                                var_82 = var_80.field_1;
                                var_83 = var_80.field_2;
                                *(uint64_t *)(local_sp_6 + (-16L)) = 4213506UL;
                                indirect_placeholder();
                                var_84 = (uint64_t)*(uint32_t *)var_81;
                                var_85 = local_sp_6 + (-24L);
                                *(uint64_t *)var_85 = 4213531UL;
                                indirect_placeholder_73(0UL, 4278165UL, 1UL, var_84, var_81, var_82, var_83);
                                r15_2 = r15_3;
                                local_sp_4 = var_85;
                            }
                        }
                    switch (loop_state_var) {
                      case 0U:
                        {
                            return;
                        }
                        break;
                      case 1U:
                        {
                            *(uint64_t *)(local_sp_4 + (-16L)) = 4213571UL;
                            indirect_placeholder();
                            *(uint64_t *)(local_sp_4 + (-24L)) = 4213595UL;
                            indirect_placeholder();
                        }
                        break;
                      case 2U:
                        {
                            *(uint64_t *)(local_sp_1 + (-8L)) = 4213803UL;
                            var_86 = indirect_placeholder_40();
                            *(uint32_t *)4301920UL = (uint32_t)var_86;
                            *(uint32_t *)4301916UL = 0U;
                            var_87 = (uint64_t)*(uint32_t *)local_sp_1;
                            *(uint64_t *)(local_sp_1 + (-16L)) = 4213836UL;
                            indirect_placeholder_3(r15_1, var_87, 4301792UL);
                        }
                        break;
                    }
                }
                break;
              case 3U:
                {
                    var_79 = local_sp_0 + (-8L);
                    *(uint64_t *)var_79 = 4214012UL;
                    indirect_placeholder();
                    r15_3 = r15_0;
                    r15_1 = r15_0;
                    local_sp_1 = var_79;
                    local_sp_6 = var_79;
                    r15_2 = r15_0;
                    if ((uint64_t)_pre_phi == 0UL) {
                        if ((*(unsigned char *)(local_sp_0 + 6UL) | *(unsigned char *)(local_sp_0 + 7UL)) != '\x00') {
                            *(uint64_t *)(local_sp_1 + (-8L)) = 4213803UL;
                            var_86 = indirect_placeholder_40();
                            *(uint32_t *)4301920UL = (uint32_t)var_86;
                            *(uint32_t *)4301916UL = 0U;
                            var_87 = (uint64_t)*(uint32_t *)local_sp_1;
                            *(uint64_t *)(local_sp_1 + (-16L)) = 4213836UL;
                            indirect_placeholder_3(r15_1, var_87, 4301792UL);
                            return;
                        }
                        if (*(unsigned char *)(local_sp_0 + 5UL) != '\x00') {
                            *(uint64_t *)(local_sp_1 + (-8L)) = 4213803UL;
                            var_86 = indirect_placeholder_40();
                            *(uint32_t *)4301920UL = (uint32_t)var_86;
                            *(uint32_t *)4301916UL = 0U;
                            var_87 = (uint64_t)*(uint32_t *)local_sp_1;
                            *(uint64_t *)(local_sp_1 + (-16L)) = 4213836UL;
                            indirect_placeholder_3(r15_1, var_87, 4301792UL);
                            return;
                        }
                        var_88 = local_sp_0 + 22UL;
                        *(unsigned char *)var_88 = (unsigned char)'\x00';
                        var_89 = local_sp_0 + 23UL;
                        *(unsigned char *)var_89 = (unsigned char)'\x00';
                        *(uint64_t *)(local_sp_0 + (-24L)) = var_89;
                        *(uint64_t *)(local_sp_0 + (-32L)) = 4214096UL;
                        indirect_placeholder_64(rsi, 0UL, r15_0, var_10, 4301792UL, var_88);
                        var_90 = local_sp_0 + (-16L);
                        rax_1 = var_89;
                        local_sp_4 = var_90;
                        if (*(unsigned char *)(local_sp_0 + 15UL) == '\x00') {
                            return;
                        }
                    }
                    *(uint64_t *)(local_sp_6 + (-8L)) = 4213498UL;
                    var_80 = indirect_placeholder_74(r15_3, 0UL, 3UL);
                    var_81 = var_80.field_0;
                    var_82 = var_80.field_1;
                    var_83 = var_80.field_2;
                    *(uint64_t *)(local_sp_6 + (-16L)) = 4213506UL;
                    indirect_placeholder();
                    var_84 = (uint64_t)*(uint32_t *)var_81;
                    var_85 = local_sp_6 + (-24L);
                    *(uint64_t *)var_85 = 4213531UL;
                    indirect_placeholder_73(0UL, 4278165UL, 1UL, var_84, var_81, var_82, var_83);
                    r15_2 = r15_3;
                    local_sp_4 = var_85;
                    var_91 = local_sp_4 + (-8L);
                    *(uint64_t *)var_91 = 4213552UL;
                    indirect_placeholder();
                    if ((uint64_t)(uint32_t)rax_1 != 0UL) {
                        *(uint64_t *)(local_sp_4 + (-16L)) = 4213571UL;
                        indirect_placeholder();
                        *(uint64_t *)(local_sp_4 + (-24L)) = 4213595UL;
                        indirect_placeholder();
                        return;
                    }
                    *(uint64_t *)(var_91 + (-8L)) = 4213667UL;
                    var_92 = indirect_placeholder_72(r15_2, 0UL, 3UL);
                    var_93 = var_92.field_0;
                    var_94 = var_92.field_1;
                    var_95 = var_92.field_2;
                    *(uint64_t *)(var_91 + (-16L)) = 4213675UL;
                    indirect_placeholder();
                    var_96 = (uint64_t)*(uint32_t *)var_93;
                    var_97 = var_91 + (-24L);
                    *(uint64_t *)var_97 = 4213700UL;
                    indirect_placeholder_68(0UL, 4278165UL, 1UL, var_96, var_93, var_94, var_95);
                    *(uint64_t *)(var_97 + (-8L)) = 4213718UL;
                    var_98 = indirect_placeholder_71(r15_2, 0UL, 3UL);
                    var_99 = var_98.field_0;
                    var_100 = var_98.field_1;
                    var_101 = var_98.field_2;
                    *(uint64_t *)(var_97 + (-16L)) = 4213726UL;
                    indirect_placeholder();
                    var_102 = (uint64_t)*(uint32_t *)var_99;
                    var_103 = var_97 + (-24L);
                    *(uint64_t *)var_103 = 4213751UL;
                    indirect_placeholder_67(0UL, 4278165UL, 1UL, var_102, var_99, var_100, var_101);
                    local_sp_11 = var_103;
                }
                break;
              case 1U:
              case 0U:
                {
                    switch (loop_state_var) {
                      case 0U:
                        {
                            *(uint64_t *)(local_sp_8 + (-8L)) = 4213390UL;
                            var_62 = indirect_placeholder_78(r15_5, 0UL, 3UL);
                            var_63 = var_62.field_0;
                            var_64 = var_62.field_1;
                            var_65 = var_62.field_2;
                            *(uint64_t *)(local_sp_8 + (-16L)) = 4213398UL;
                            indirect_placeholder();
                            var_66 = (uint64_t)*(uint32_t *)var_63;
                            var_67 = local_sp_8 + (-24L);
                            *(uint64_t *)var_67 = 4213423UL;
                            indirect_placeholder_77(0UL, 4278165UL, 1UL, var_66, var_63, var_64, var_65);
                            local_sp_7 = var_67;
                        }
                        break;
                      case 1U:
                        {
                            *(uint64_t *)(local_sp_7 + (-8L)) = 4213441UL;
                            var_68 = indirect_placeholder_76(r15_5, 0UL, 3UL);
                            var_69 = var_68.field_0;
                            var_70 = var_68.field_1;
                            var_71 = var_68.field_2;
                            *(uint64_t *)(local_sp_7 + (-16L)) = 4213449UL;
                            indirect_placeholder();
                            var_72 = (uint64_t)*(uint32_t *)var_69;
                            var_73 = local_sp_7 + (-24L);
                            *(uint64_t *)var_73 = 4213474UL;
                            indirect_placeholder_75(0UL, 4273656UL, 1UL, var_72, var_69, var_70, var_71);
                            local_sp_6 = var_73;
                            *(uint64_t *)(local_sp_6 + (-8L)) = 4213498UL;
                            var_80 = indirect_placeholder_74(r15_3, 0UL, 3UL);
                            var_81 = var_80.field_0;
                            var_82 = var_80.field_1;
                            var_83 = var_80.field_2;
                            *(uint64_t *)(local_sp_6 + (-16L)) = 4213506UL;
                            indirect_placeholder();
                            var_84 = (uint64_t)*(uint32_t *)var_81;
                            var_85 = local_sp_6 + (-24L);
                            *(uint64_t *)var_85 = 4213531UL;
                            indirect_placeholder_73(0UL, 4278165UL, 1UL, var_84, var_81, var_82, var_83);
                            r15_2 = r15_3;
                            local_sp_4 = var_85;
                            var_91 = local_sp_4 + (-8L);
                            *(uint64_t *)var_91 = 4213552UL;
                            indirect_placeholder();
                            if ((uint64_t)(uint32_t)rax_1 != 0UL) {
                                *(uint64_t *)(local_sp_4 + (-16L)) = 4213571UL;
                                indirect_placeholder();
                                *(uint64_t *)(local_sp_4 + (-24L)) = 4213595UL;
                                indirect_placeholder();
                                return;
                            }
                            *(uint64_t *)(var_91 + (-8L)) = 4213667UL;
                            var_92 = indirect_placeholder_72(r15_2, 0UL, 3UL);
                            var_93 = var_92.field_0;
                            var_94 = var_92.field_1;
                            var_95 = var_92.field_2;
                            *(uint64_t *)(var_91 + (-16L)) = 4213675UL;
                            indirect_placeholder();
                            var_96 = (uint64_t)*(uint32_t *)var_93;
                            var_97 = var_91 + (-24L);
                            *(uint64_t *)var_97 = 4213700UL;
                            indirect_placeholder_68(0UL, 4278165UL, 1UL, var_96, var_93, var_94, var_95);
                            *(uint64_t *)(var_97 + (-8L)) = 4213718UL;
                            var_98 = indirect_placeholder_71(r15_2, 0UL, 3UL);
                            var_99 = var_98.field_0;
                            var_100 = var_98.field_1;
                            var_101 = var_98.field_2;
                            *(uint64_t *)(var_97 + (-16L)) = 4213726UL;
                            indirect_placeholder();
                            var_102 = (uint64_t)*(uint32_t *)var_99;
                            var_103 = var_97 + (-24L);
                            *(uint64_t *)var_103 = 4213751UL;
                            indirect_placeholder_67(0UL, 4278165UL, 1UL, var_102, var_99, var_100, var_101);
                            local_sp_11 = var_103;
                        }
                        break;
                    }
                }
                break;
            }
        }
        break;
    }
}
