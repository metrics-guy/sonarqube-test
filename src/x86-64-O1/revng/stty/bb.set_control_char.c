typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_29(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder_4(uint64_t param_0);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_rax(void);
extern uint32_t init_state_0x82fc(void);
void bb_set_control_char(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t cc_src_1;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint32_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t cc_src_0;
    uint64_t cc_dst_0;
    uint32_t cc_op_0;
    uint64_t rdi2_0;
    uint64_t rsi3_0;
    uint64_t rcx_0;
    uint64_t rsi3_1;
    uint64_t cc_dst_1;
    uint64_t var_9;
    uint64_t var_10;
    uint32_t cc_op_1;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t cc_src_2;
    uint64_t cc_dst_2;
    uint32_t cc_op_2;
    uint64_t rdi2_1;
    uint64_t var_25;
    uint64_t rcx_1;
    uint64_t cc_src_3;
    uint64_t cc_dst_3;
    uint64_t var_15;
    uint64_t var_16;
    uint32_t cc_op_3;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    unsigned char var_20;
    unsigned char var_21;
    uint64_t cc_src_4;
    uint64_t cc_dst_4;
    uint32_t cc_op_4;
    uint64_t rdi2_2;
    uint64_t rsi3_2;
    uint64_t rcx_2;
    uint64_t cc_src_5;
    uint64_t cc_dst_5;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint32_t cc_op_5;
    uint64_t rcx_3;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t rax_0;
    uint64_t cc_src_6;
    uint64_t cc_dst_6;
    uint32_t cc_op_6;
    uint64_t rdi2_3;
    uint64_t rsi3_3;
    uint64_t rcx_4;
    uint64_t cc_src_7;
    uint64_t cc_dst_7;
    uint64_t var_29;
    uint64_t var_30;
    uint32_t cc_op_7;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_34;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbx();
    var_3 = init_rbp();
    var_4 = init_cc_src2();
    var_5 = init_state_0x82fc();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_6 = var_0 + (-24L);
    var_7 = *(uint64_t *)rdi;
    var_8 = (uint64_t)var_5;
    cc_src_0 = 8UL;
    cc_dst_0 = var_6;
    cc_op_0 = 17U;
    rdi2_0 = 4264257UL;
    rsi3_0 = var_7;
    rcx_0 = 4UL;
    rsi3_1 = var_7;
    cc_src_2 = 0UL;
    cc_op_2 = 22U;
    rdi2_1 = 4264368UL;
    rcx_1 = 5UL;
    cc_src_4 = 0UL;
    cc_op_4 = 22U;
    rdi2_2 = 4264373UL;
    rsi3_2 = rsi;
    rcx_2 = 3UL;
    rcx_3 = 0UL;
    rax_0 = 0UL;
    cc_src_6 = 0UL;
    cc_op_6 = 22U;
    rdi2_3 = 4264376UL;
    rsi3_3 = rsi;
    rcx_4 = 6UL;
    cc_op_0 = 14U;
    cc_src_1 = cc_src_0;
    cc_dst_1 = cc_dst_0;
    cc_op_1 = cc_op_0;
    while (rcx_0 != 0UL)
        {
            rdi2_0 = rdi2_0 + var_8;
            rsi3_0 = rsi3_0 + var_8;
            rcx_0 = rcx_0 + (-1L);
            cc_op_0 = 14U;
            cc_src_1 = cc_src_0;
            cc_dst_1 = cc_dst_0;
            cc_op_1 = cc_op_0;
        }
    var_11 = helper_cc_compute_all_wrapper(cc_dst_1, cc_src_1, var_4, cc_op_1);
    var_12 = ((var_11 & 65UL) == 0UL);
    var_13 = helper_cc_compute_c_wrapper(cc_dst_1, var_11, var_4, 1U);
    var_14 = (uint64_t)((unsigned char)var_12 - (unsigned char)var_13);
    if (var_14 == 0UL) {
        *(uint64_t *)(var_0 + (-32L)) = 4208657UL;
        var_39 = indirect_placeholder_1(rsi, 255UL);
        rax_0 = var_39;
    } else {
        cc_dst_2 = (var_1 & (-256L)) | var_14;
        cc_op_2 = 14U;
        cc_src_3 = cc_src_2;
        cc_dst_3 = cc_dst_2;
        cc_op_3 = cc_op_2;
        while (rcx_1 != 0UL)
            {
                var_15 = (uint64_t)*(unsigned char *)rdi2_1;
                var_16 = (uint64_t)*(unsigned char *)rsi3_1 - var_15;
                cc_src_2 = var_15;
                cc_dst_2 = var_16;
                cc_src_3 = var_15;
                cc_dst_3 = var_16;
                cc_op_3 = 14U;
                if ((uint64_t)(unsigned char)var_16 == 0UL) {
                    break;
                }
                rdi2_1 = rdi2_1 + var_8;
                rsi3_1 = rsi3_1 + var_8;
                rcx_1 = rcx_1 + (-1L);
                cc_op_2 = 14U;
                cc_src_3 = cc_src_2;
                cc_dst_3 = cc_dst_2;
                cc_op_3 = cc_op_2;
            }
        var_17 = helper_cc_compute_all_wrapper(cc_dst_3, cc_src_3, var_13, cc_op_3);
        var_18 = ((var_17 & 65UL) == 0UL);
        var_19 = helper_cc_compute_c_wrapper(cc_dst_3, var_17, var_13, 1U);
        if ((uint64_t)((unsigned char)var_18 - (unsigned char)var_19) == 0UL) {
            *(uint64_t *)(var_0 + (-32L)) = 4208657UL;
            var_39 = indirect_placeholder_1(rsi, 255UL);
            rax_0 = var_39;
        } else {
            var_20 = *(unsigned char *)rsi;
            if (var_20 == '\x00') {
                var_37 = (uint64_t)(uint32_t)(uint64_t)var_20;
                *(uint64_t *)(var_0 + (-32L)) = 4208698UL;
                var_38 = indirect_placeholder_4(var_37);
                rax_0 = (uint64_t)(unsigned char)var_38;
            } else {
                var_21 = *(unsigned char *)(rsi + 1UL);
                cc_dst_4 = (uint64_t)var_21;
                if (var_21 == '\x00') {
                    var_37 = (uint64_t)(uint32_t)(uint64_t)var_20;
                    *(uint64_t *)(var_0 + (-32L)) = 4208698UL;
                    var_38 = indirect_placeholder_4(var_37);
                    rax_0 = (uint64_t)(unsigned char)var_38;
                } else {
                    cc_op_4 = 14U;
                    cc_src_5 = cc_src_4;
                    cc_dst_5 = cc_dst_4;
                    cc_op_5 = cc_op_4;
                    while (rcx_2 != 0UL)
                        {
                            var_22 = (uint64_t)*(unsigned char *)rdi2_2;
                            var_23 = (uint64_t)*(unsigned char *)rsi3_2 - var_22;
                            var_24 = rcx_2 + (-1L);
                            cc_src_4 = var_22;
                            cc_dst_4 = var_23;
                            rcx_2 = var_24;
                            cc_src_5 = var_22;
                            cc_dst_5 = var_23;
                            cc_op_5 = 14U;
                            rcx_3 = var_24;
                            if ((uint64_t)(unsigned char)var_23 == 0UL) {
                                break;
                            }
                            rdi2_2 = rdi2_2 + var_8;
                            rsi3_2 = rsi3_2 + var_8;
                            cc_op_4 = 14U;
                            cc_src_5 = cc_src_4;
                            cc_dst_5 = cc_dst_4;
                            cc_op_5 = cc_op_4;
                        }
                    var_25 = helper_cc_compute_all_wrapper(cc_dst_5, cc_src_5, var_19, cc_op_5);
                    var_26 = ((var_25 & 65UL) == 0UL);
                    var_27 = helper_cc_compute_c_wrapper(cc_dst_5, var_25, var_19, 1U);
                    var_28 = (uint64_t)((unsigned char)var_26 - (unsigned char)var_27);
                    if (var_28 != 0UL) {
                        cc_dst_6 = (rcx_3 & (-256L)) | var_28;
                        cc_op_6 = 14U;
                        cc_src_7 = cc_src_6;
                        cc_dst_7 = cc_dst_6;
                        cc_op_7 = cc_op_6;
                        while (rcx_4 != 0UL)
                            {
                                var_29 = (uint64_t)*(unsigned char *)rdi2_3;
                                var_30 = (uint64_t)*(unsigned char *)rsi3_3 - var_29;
                                cc_src_6 = var_29;
                                cc_dst_6 = var_30;
                                cc_src_7 = var_29;
                                cc_dst_7 = var_30;
                                cc_op_7 = 14U;
                                if ((uint64_t)(unsigned char)var_30 == 0UL) {
                                    break;
                                }
                                rdi2_3 = rdi2_3 + var_8;
                                rsi3_3 = rsi3_3 + var_8;
                                rcx_4 = rcx_4 + (-1L);
                                cc_op_6 = 14U;
                                cc_src_7 = cc_src_6;
                                cc_dst_7 = cc_dst_6;
                                cc_op_7 = cc_op_6;
                            }
                        var_31 = helper_cc_compute_all_wrapper(cc_dst_7, cc_src_7, var_27, cc_op_7);
                        var_32 = ((var_31 & 65UL) == 0UL);
                        var_33 = helper_cc_compute_c_wrapper(cc_dst_7, var_31, var_27, 1U);
                        if ((uint64_t)((unsigned char)var_32 - (unsigned char)var_33) != 0UL) {
                            rax_0 = 127UL;
                            if (var_20 == '^') {
                                *(uint64_t *)(var_0 + (-32L)) = 4208806UL;
                                var_34 = indirect_placeholder_29(0UL, rsi, 255UL);
                                rax_0 = var_34;
                            } else {
                                if ((uint64_t)(var_21 + '\xc1') == 0UL) {
                                    var_35 = (uint64_t)(uint32_t)(uint64_t)var_21;
                                    *(uint64_t *)(var_0 + (-32L)) = 4208783UL;
                                    var_36 = indirect_placeholder_1(127UL, var_35);
                                    rax_0 = (uint64_t)((unsigned char)var_36 & '\x9f');
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    *(unsigned char *)((*(uint64_t *)(rdi + 16UL) + rdx) + 17UL) = (unsigned char)rax_0;
    return;
}
