typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_16_ret_type;
struct indirect_placeholder_21_ret_type;
struct indirect_placeholder_17_ret_type;
struct indirect_placeholder_18_ret_type;
struct indirect_placeholder_19_ret_type;
struct indirect_placeholder_20_ret_type;
struct indirect_placeholder_16_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_21_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_17_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_18_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_19_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_20_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_4(uint64_t param_0);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint32_t init_state_0x82fc(void);
extern struct indirect_placeholder_16_ret_type indirect_placeholder_16(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_21_ret_type indirect_placeholder_21(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_17_ret_type indirect_placeholder_17(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_18_ret_type indirect_placeholder_18(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_19_ret_type indirect_placeholder_19(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_20_ret_type indirect_placeholder_20(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
void bb_display_changed(uint64_t rax, uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint32_t var_8;
    struct indirect_placeholder_21_ret_type var_16;
    uint64_t var_9;
    struct indirect_placeholder_16_ret_type var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t rdx_2;
    uint64_t r15_0;
    uint64_t local_sp_5;
    uint64_t rbp_0;
    uint64_t rbx_1;
    uint64_t rdx_0;
    uint64_t r9_4;
    uint64_t rcx_0;
    uint64_t local_sp_0;
    uint64_t cc_src2_1;
    uint64_t r9_0;
    uint64_t r8_4;
    uint64_t rdx_5;
    uint64_t var_58;
    uint64_t local_sp_6;
    struct indirect_placeholder_17_ret_type var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    struct indirect_placeholder_18_ret_type var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_57;
    uint64_t rax1_1;
    uint64_t rax1_0;
    uint64_t r8_1;
    uint64_t rdx_1;
    uint64_t cc_dst_0;
    uint32_t cc_op_0;
    uint64_t rbx_0;
    uint64_t r9_1;
    uint64_t r8_0;
    uint64_t rsi_1;
    uint64_t rcx_3;
    uint64_t local_sp_2;
    uint64_t cc_src2_0;
    uint64_t local_sp_1;
    uint64_t var_20;
    uint64_t cc_src_1;
    uint64_t cc_dst_1;
    uint32_t cc_op_1;
    uint64_t rdi2_0;
    uint64_t rsi_0;
    uint64_t rcx_1;
    uint64_t cc_src_2;
    uint64_t cc_dst_2;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint32_t cc_op_2;
    uint64_t rcx_2;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t cc_src_3;
    uint64_t cc_dst_3;
    uint64_t rdi2_1;
    uint64_t cc_src_4;
    uint64_t cc_dst_4;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t rcx_4;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    struct indirect_placeholder_19_ret_type var_40;
    uint64_t var_41;
    uint64_t rdx_3;
    uint64_t r8_2;
    uint64_t r9_2;
    uint64_t local_sp_3;
    uint64_t rcx_6;
    uint64_t rdx_4;
    uint64_t rcx_5;
    uint64_t r8_3;
    uint64_t r9_3;
    uint64_t local_sp_4;
    uint64_t var_48;
    uint32_t var_49;
    uint64_t var_50;
    uint32_t *var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t *var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_66;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    struct indirect_placeholder_20_ret_type var_47;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_cc_src2();
    var_8 = init_state_0x82fc();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_9 = var_0 + (-72L);
    *(uint64_t *)(var_0 + (-80L)) = 4207033UL;
    var_10 = indirect_placeholder_16(rdi, 1UL);
    var_11 = var_10.field_0;
    var_12 = var_10.field_1;
    var_13 = var_10.field_2;
    var_14 = var_10.field_3;
    var_15 = (uint64_t)*(unsigned char *)(rdi + 16UL);
    *(uint64_t *)(var_0 + (-88L)) = 4207053UL;
    var_16 = indirect_placeholder_21(0UL, var_11, 4264231UL, var_15, var_12, var_13, var_14);
    var_17 = var_16.field_1;
    var_18 = var_0 + (-96L);
    *(uint64_t *)var_18 = 4207063UL;
    indirect_placeholder();
    *(uint32_t *)4301916UL = 0U;
    var_19 = (uint64_t)var_8;
    r15_0 = 4264224UL;
    rbx_1 = 4275400UL;
    r9_0 = var_17;
    rax1_0 = 0UL;
    rdx_1 = var_11;
    cc_dst_0 = var_9;
    cc_op_0 = 17U;
    rbx_0 = 4274912UL;
    r8_0 = 1UL;
    rcx_3 = 6UL;
    cc_src2_0 = var_7;
    local_sp_1 = var_18;
    cc_src_1 = 24UL;
    rdi2_0 = 4264257UL;
    rcx_1 = 4UL;
    rcx_2 = 0UL;
    rdi2_1 = 4264242UL;
    rcx_4 = 0UL;
    while (1U)
        {
            var_20 = *(uint64_t *)rbx_0;
            rdx_2 = rdx_1;
            r8_1 = r8_0;
            cc_op_0 = 9U;
            r9_1 = r9_0;
            rsi_1 = var_20;
            local_sp_2 = local_sp_1;
            cc_dst_1 = cc_dst_0;
            cc_op_1 = cc_op_0;
            rsi_0 = var_20;
            rdx_3 = rdx_1;
            r8_2 = r8_0;
            r9_2 = r9_0;
            local_sp_3 = local_sp_1;
            cc_op_1 = 14U;
            cc_src_2 = cc_src_1;
            cc_dst_2 = cc_dst_1;
            cc_op_2 = cc_op_1;
            while (rcx_1 != 0UL)
                {
                    rdi2_0 = rdi2_0 + var_19;
                    rsi_0 = rsi_0 + var_19;
                    cc_op_1 = 14U;
                    cc_src_2 = cc_src_1;
                    cc_dst_2 = cc_dst_1;
                    cc_op_2 = cc_op_1;
                }
            var_24 = helper_cc_compute_all_wrapper(cc_dst_2, cc_src_2, cc_src2_0, cc_op_2);
            var_25 = ((var_24 & 65UL) == 0UL);
            var_26 = helper_cc_compute_c_wrapper(cc_dst_2, var_24, cc_src2_0, 1U);
            cc_src2_1 = var_26;
            rcx_5 = rcx_2;
            if ((uint64_t)((unsigned char)var_25 - (unsigned char)var_26) == 0UL) {
                break;
            }
            var_27 = (uint64_t)*(unsigned char *)((*(uint64_t *)(rbx_0 + 16UL) + rdi) + 17UL);
            var_28 = (uint64_t)*(unsigned char *)(rbx_0 + 8UL);
            var_29 = var_27 - var_28;
            cc_src_3 = var_28;
            cc_dst_3 = var_29;
            rax1_1 = var_27;
            if ((uint64_t)(unsigned char)var_29 != 0UL) {
                cc_src_4 = cc_src_3;
                cc_dst_4 = cc_dst_3;
                while (rcx_3 != 0UL)
                    {
                        var_30 = (uint64_t)*(unsigned char *)rdi2_1;
                        var_31 = (uint64_t)*(unsigned char *)rsi_1 - var_30;
                        var_32 = rcx_3 + (-1L);
                        rcx_3 = var_32;
                        cc_src_3 = var_30;
                        cc_dst_3 = var_31;
                        cc_src_4 = var_30;
                        cc_dst_4 = var_31;
                        rcx_4 = var_32;
                        if ((uint64_t)(unsigned char)var_31 == 0UL) {
                            break;
                        }
                        rdi2_1 = rdi2_1 + var_19;
                        rsi_1 = rsi_1 + var_19;
                        cc_src_4 = cc_src_3;
                        cc_dst_4 = cc_dst_3;
                    }
                var_33 = helper_cc_compute_all_wrapper(cc_dst_4, cc_src_4, var_26, 14U);
                var_34 = ((var_33 & 65UL) == 0UL);
                var_35 = rdx_1 & (-256L);
                var_36 = helper_cc_compute_c_wrapper(cc_dst_4, var_33, var_26, 1U);
                var_37 = (uint64_t)((unsigned char)var_34 - (unsigned char)var_36);
                rdx_2 = var_35 | var_37;
                cc_src2_1 = var_36;
                if (var_37 == 0UL) {
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4207165UL;
                    var_38 = indirect_placeholder_4(var_27);
                    var_39 = local_sp_1 + (-16L);
                    *(uint64_t *)var_39 = 4207186UL;
                    var_40 = indirect_placeholder_19(0UL, var_38, 4264248UL, var_20, rcx_4, r8_0, r9_0);
                    rdx_2 = var_38;
                    rax1_1 = 0UL;
                    r8_1 = 0UL;
                    r9_1 = var_40.field_1;
                    local_sp_2 = var_39;
                }
            }
            var_41 = rbx_0 + 24UL;
            r9_0 = r9_1;
            rax1_0 = rax1_1;
            rdx_1 = rdx_2;
            cc_dst_0 = var_41;
            rbx_0 = var_41;
            r8_0 = r8_1;
            cc_src2_0 = cc_src2_1;
            local_sp_1 = local_sp_2;
            continue;
        }
    if ((*(unsigned char *)(rdi + 12UL) & '\x02') == '\x00') {
        var_43 = rax1_0 & (-256L);
        var_44 = (uint64_t)*(unsigned char *)(rdi + 22UL);
        var_45 = (uint64_t)*(unsigned char *)(rdi + 23UL);
        var_46 = local_sp_1 + (-8L);
        *(uint64_t *)var_46 = 4207265UL;
        var_47 = indirect_placeholder_20(var_43, var_44, 4264261UL, var_45, rcx_2, r8_0, r9_0);
        rdx_3 = var_44;
        r8_2 = var_47.field_0;
        r9_2 = var_47.field_1;
        local_sp_3 = var_46;
    } else {
        if ((uint64_t)(unsigned char)r8_0 == 0UL) {
            var_42 = local_sp_1 + (-8L);
            *(uint64_t *)var_42 = 4207277UL;
            indirect_placeholder();
            local_sp_3 = var_42;
        }
    }
    *(uint32_t *)4301916UL = 0U;
    *(uint32_t *)(local_sp_3 + 8UL) = 0U;
    *(unsigned char *)(local_sp_3 + 15UL) = (unsigned char)'\x01';
    rdx_4 = rdx_3;
    r8_3 = r8_2;
    r9_3 = r9_2;
    local_sp_4 = local_sp_3;
    while (1U)
        {
            var_48 = (uint64_t)*(unsigned char *)(rbx_1 | 4UL);
            local_sp_5 = local_sp_4;
            rdx_0 = rdx_4;
            r9_4 = r9_3;
            rcx_0 = rcx_5;
            r8_4 = r8_3;
            rdx_5 = rdx_4;
            local_sp_6 = local_sp_4;
            rcx_6 = rcx_5;
            if ((var_48 & 8UL) != 0UL) {
                var_49 = *(uint32_t *)rbx_1;
                var_50 = (uint64_t)var_49;
                var_51 = (uint32_t *)(local_sp_4 + 8UL);
                *var_51 = var_49;
                if ((uint64_t)(var_49 - *var_51) != 0UL & *(unsigned char *)(local_sp_4 + 15UL) == '\x00') {
                    var_52 = local_sp_4 + (-8L);
                    *(uint64_t *)var_52 = 4207289UL;
                    indirect_placeholder();
                    *(uint32_t *)4301916UL = 0U;
                    *(uint32_t *)local_sp_4 = var_49;
                    *(unsigned char *)(local_sp_4 + 7UL) = (unsigned char)'\x01';
                    local_sp_5 = var_52;
                }
                var_53 = local_sp_5 + (-8L);
                var_54 = (uint64_t *)var_53;
                *var_54 = 4207318UL;
                var_55 = indirect_placeholder_1(var_50, rdi);
                *var_54 = var_55;
                var_56 = *(uint64_t *)(rbx_1 + 16UL);
                rbp_0 = var_56;
                local_sp_0 = var_53;
                if (var_56 == 0UL) {
                    rbp_0 = *(uint64_t *)(rbx_1 + 8UL);
                }
                if (var_55 == 0UL) {
                    var_57 = local_sp_5 + (-16L);
                    *(uint64_t *)var_57 = 4207440UL;
                    indirect_placeholder();
                    rdx_0 = 1954UL;
                    rcx_0 = 4273840UL;
                    local_sp_0 = var_57;
                }
                rdx_5 = rdx_0;
                rcx_6 = rcx_0;
                local_sp_6 = local_sp_0;
                if ((rbp_0 & (uint64_t)**(uint32_t **)local_sp_0) == *(uint64_t *)(rbx_1 + 8UL)) {
                    if ((var_48 & 2UL) == 0UL) {
                        var_62 = local_sp_0 + (-8L);
                        *(uint64_t *)var_62 = 4207466UL;
                        var_63 = indirect_placeholder_18(0UL, rdx_0, 4278165UL, r15_0, rcx_0, r8_3, r9_3);
                        var_64 = var_63.field_0;
                        var_65 = var_63.field_1;
                        *(unsigned char *)(local_sp_0 + 7UL) = (unsigned char)'\x00';
                        r8_4 = var_64;
                        r9_4 = var_65;
                        local_sp_6 = var_62;
                    }
                } else {
                    if ((uint64_t)((unsigned char)(var_48 & 5UL) + '\xfb') == 0UL) {
                        var_58 = local_sp_0 + (-8L);
                        *(uint64_t *)var_58 = 4207491UL;
                        var_59 = indirect_placeholder_17(0UL, rdx_0, 4264285UL, r15_0, rcx_0, r8_3, r9_3);
                        var_60 = var_59.field_0;
                        var_61 = var_59.field_1;
                        *(unsigned char *)(local_sp_0 + 7UL) = (unsigned char)'\x00';
                        r8_4 = var_60;
                        r9_4 = var_61;
                        local_sp_6 = var_58;
                    }
                }
            }
            var_66 = *(uint64_t *)(rbx_1 + 24UL);
            r15_0 = var_66;
            rdx_4 = rdx_5;
            rcx_5 = rcx_6;
            r8_3 = r8_4;
            r9_3 = r9_4;
            local_sp_4 = local_sp_6;
            if (var_66 == 0UL) {
                break;
            }
            rbx_1 = rbx_1 + 32UL;
            continue;
        }
    if (*(unsigned char *)(local_sp_6 + 15UL) != '\x00') {
        *(uint32_t *)4301916UL = 0U;
        return;
    }
    *(uint64_t *)(local_sp_6 + (-8L)) = 4207543UL;
    indirect_placeholder();
    *(uint32_t *)4301916UL = 0U;
    return;
}
