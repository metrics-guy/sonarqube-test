typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_4_ret_type;
struct indirect_placeholder_3_ret_type;
struct indirect_placeholder_7_ret_type;
struct indirect_placeholder_4_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_3_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_7_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_6(uint64_t param_0);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r9(void);
extern void indirect_placeholder_2(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_5(uint64_t param_0);
extern uint64_t init_r8(void);
extern uint64_t init_rcx(void);
extern struct indirect_placeholder_4_ret_type indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_3_ret_type indirect_placeholder_3(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_7_ret_type indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
uint64_t bb_print_esc(uint64_t rdi, uint64_t rsi) {
    uint64_t r13_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    unsigned char *var_12;
    unsigned char var_13;
    uint64_t r12_3;
    uint64_t var_39;
    uint64_t r14_0;
    uint64_t var_28;
    uint64_t local_sp_0;
    uint64_t var_29;
    uint64_t local_sp_1;
    uint64_t r13_2;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t r13_1;
    uint64_t storemerge_in;
    uint32_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t local_sp_2;
    uint64_t r12_2;
    uint64_t rax_0;
    uint64_t rdi1_0;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t local_sp_5;
    uint64_t var_17;
    uint64_t var_18;
    uint32_t *var_19;
    uint32_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    unsigned char var_23;
    uint32_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t r12_4;
    uint64_t var_49;
    uint64_t storemerge1_in;
    uint64_t rbp_1;
    uint64_t r12_0;
    uint64_t var_52;
    struct indirect_placeholder_7_ret_type var_53;
    uint64_t var_54;
    uint64_t local_sp_4;
    uint64_t r14_1;
    uint64_t var_50;
    uint32_t var_51;
    uint64_t local_sp_6;
    uint64_t r12_1;
    uint64_t var_14;
    uint64_t var_40;
    uint64_t var_41;
    unsigned char var_42;
    uint64_t var_43;
    uint32_t var_15;
    uint64_t var_16;
    unsigned char var_44;
    uint32_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_rcx();
    var_8 = init_r9();
    var_9 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_10 = var_0 + (-72L);
    var_11 = rdi + 1UL;
    var_12 = (unsigned char *)var_11;
    var_13 = *var_12;
    r12_3 = var_11;
    r13_2 = 0UL;
    rdi1_0 = 0UL;
    local_sp_5 = var_10;
    rbp_1 = 0UL;
    local_sp_4 = var_10;
    r14_1 = 0UL;
    local_sp_6 = var_10;
    if ((uint64_t)(var_13 + '\x88') == 0UL) {
        r12_4 = rdi + 2UL;
        while (1U)
            {
                var_44 = *(unsigned char *)r12_4;
                var_45 = (uint32_t)(uint64_t)var_44;
                var_46 = (uint64_t)var_45;
                *(uint64_t *)(local_sp_6 + (-8L)) = 4206203UL;
                var_47 = indirect_placeholder_6(var_46);
                var_48 = local_sp_6 + (-16L);
                *(uint64_t *)var_48 = 4206211UL;
                indirect_placeholder();
                r12_0 = r12_4;
                local_sp_6 = var_48;
                if ((uint64_t)(uint32_t)var_47 != 0UL) {
                    if (r14_1 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_52 = local_sp_6 + (-24L);
                    *(uint64_t *)var_52 = 4206286UL;
                    var_53 = indirect_placeholder_7(var_47, 4256368UL, 1UL, 0UL, var_7, var_8, var_9);
                    var_54 = var_53.field_0;
                    local_sp_4 = var_52;
                    r12_2 = var_54;
                    loop_state_var = 0U;
                    break;
                }
                var_49 = rbp_1 << 4UL;
                if ((uint64_t)((var_44 + '\x9f') & '\xfe') > 5UL) {
                    storemerge1_in = ((uint64_t)((var_44 + '\xbf') & '\xfe') > 5UL) ? (var_46 + 4294967248UL) : (uint64_t)(var_45 + (-55));
                } else {
                    storemerge1_in = var_46 + 4294967209UL;
                }
                var_50 = r12_4 + 1UL;
                var_51 = (uint32_t)r14_1;
                r12_0 = var_50;
                r12_4 = var_50;
                if ((uint64_t)(var_51 + (-1)) != 0UL) {
                    loop_state_var = 1U;
                    break;
                }
                rbp_1 = (uint64_t)((uint32_t)var_49 + (uint32_t)storemerge1_in);
                r14_1 = (uint64_t)(var_51 + 1U);
                continue;
            }
        switch (loop_state_var) {
          case 0U:
            {
                *(uint64_t *)(local_sp_4 + (-8L)) = 4206135UL;
                indirect_placeholder();
                r12_1 = r12_2;
            }
            break;
          case 1U:
            {
                *(uint64_t *)(local_sp_6 + (-24L)) = 4206243UL;
                indirect_placeholder();
                r12_1 = r12_0;
            }
            break;
        }
    } else {
        var_14 = (uint64_t)((uint32_t)(uint64_t)var_13 + (-48));
        if ((uint64_t)((unsigned char)var_14 & '\xf8') == 0UL) {
            var_40 = (((uint64_t)(var_13 + '\xd0') == 0UL) & rsi) + var_11;
            var_41 = var_40 + 3UL;
            rax_0 = var_40;
            var_42 = *(unsigned char *)rax_0;
            r12_2 = rax_0;
            while ((var_42 & '\xf8') != '0')
                {
                    var_43 = rax_0 + 1UL;
                    rax_0 = var_43;
                    r12_2 = var_41;
                    if (var_43 == var_41) {
                        break;
                    }
                    rdi1_0 = (uint64_t)(((uint32_t)(rdi1_0 << 3UL) + (uint32_t)(uint64_t)var_42) + (-48));
                    var_42 = *(unsigned char *)rax_0;
                    r12_2 = rax_0;
                }
            *(uint64_t *)(local_sp_4 + (-8L)) = 4206135UL;
            indirect_placeholder();
            r12_1 = r12_2;
        } else {
            if (var_13 == '\x00') {
                *(uint64_t *)(local_sp_5 + (-8L)) = 4206599UL;
                indirect_placeholder();
                r12_1 = r12_3;
                if (*var_12 == '\x00') {
                    *(uint64_t *)(local_sp_5 + (-16L)) = 4206620UL;
                    indirect_placeholder();
                    var_39 = rdi + 2UL;
                    r12_1 = var_39;
                }
            } else {
                var_15 = (uint32_t)var_13;
                *(uint32_t *)(var_0 + (-64L)) = var_15;
                var_16 = var_0 + (-80L);
                *(uint64_t *)var_16 = 4206324UL;
                indirect_placeholder();
                local_sp_2 = var_16;
                local_sp_5 = var_16;
                if (var_14 != 0UL) {
                    var_17 = (uint64_t)var_15;
                    var_18 = rdi + 2UL;
                    *(uint64_t *)(var_0 + (-88L)) = 4206341UL;
                    indirect_placeholder_5(var_17);
                    r12_1 = var_18;
                    return (uint64_t)(((uint32_t)r12_1 - (uint32_t)rdi) + (-1));
                }
                if ((uint64_t)((var_13 & '\xdf') + '\xab') != 0UL) {
                    var_19 = (uint32_t *)(var_0 + (-68L));
                    var_20 = (uint32_t)(((uint64_t)(var_13 + '\x8b') == 0UL) ? 4UL : 8UL);
                    *var_19 = var_20;
                    var_21 = rdi + 2UL;
                    var_22 = ((uint64_t)(var_20 + (-1)) + rdi) + 3UL;
                    r14_0 = var_21;
                    r12_1 = var_22;
                    r12_3 = var_22;
                    while (1U)
                        {
                            var_23 = *(unsigned char *)r14_0;
                            var_24 = (uint32_t)(uint64_t)var_23;
                            var_25 = (uint64_t)var_24;
                            *(uint64_t *)(local_sp_2 + (-8L)) = 4206459UL;
                            var_26 = indirect_placeholder_6(var_25);
                            var_27 = local_sp_2 + (-16L);
                            *(uint64_t *)var_27 = 4206467UL;
                            indirect_placeholder();
                            local_sp_0 = var_27;
                            r13_0 = r13_2;
                            local_sp_1 = var_27;
                            if ((uint64_t)(uint32_t)var_26 != 0UL) {
                                var_28 = (uint64_t)((uint32_t)(r13_2 << 4UL) & (-16));
                                r13_0 = var_28;
                                r13_1 = var_28;
                                if ((uint64_t)((var_23 + '\x9f') & '\xfe') <= 5UL) {
                                    var_29 = var_25 + 4294967209UL;
                                    storemerge_in = var_29;
                                    var_32 = (uint32_t)r13_1 + (uint32_t)storemerge_in;
                                    var_33 = (uint64_t)var_32;
                                    var_34 = r14_0 + 1UL;
                                    local_sp_2 = local_sp_1;
                                    r13_2 = var_33;
                                    r14_0 = var_34;
                                    if (var_34 != var_22) {
                                        continue;
                                    }
                                    break;
                                }
                            }
                            var_30 = local_sp_2 + (-24L);
                            *(uint64_t *)var_30 = 4206416UL;
                            indirect_placeholder_4(var_26, 4256368UL, 1UL, 0UL, var_7, var_8, var_9);
                            local_sp_0 = var_30;
                        }
                    if ((uint64_t)(var_32 & (-32)) > 159UL) {
                        if ((uint64_t)(var_32 + (-36)) != 0UL) {
                            if ((uint64_t)((var_32 + (-55296)) & (-2048)) != 0UL) {
                                var_35 = *(uint64_t *)4279872UL;
                                *(uint64_t *)(local_sp_1 + (-8L)) = 4206547UL;
                                indirect_placeholder_2(0UL, var_35, var_33);
                                return (uint64_t)(((uint32_t)r12_1 - (uint32_t)rdi) + (-1));
                            }
                        }
                        if ((uint64_t)(((var_32 & (-33)) + (-64)) & (-33)) != 0UL & (uint64_t)((var_32 + (-55296)) & (-2048)) != 0UL) {
                            var_35 = *(uint64_t *)4279872UL;
                            *(uint64_t *)(local_sp_1 + (-8L)) = 4206547UL;
                            indirect_placeholder_2(0UL, var_35, var_33);
                            return (uint64_t)(((uint32_t)r12_1 - (uint32_t)rdi) + (-1));
                        }
                    }
                    if ((uint64_t)((var_32 + (-55296)) & (-2048)) != 0UL) {
                        var_35 = *(uint64_t *)4279872UL;
                        *(uint64_t *)(local_sp_1 + (-8L)) = 4206547UL;
                        indirect_placeholder_2(0UL, var_35, var_33);
                        return (uint64_t)(((uint32_t)r12_1 - (uint32_t)rdi) + (-1));
                    }
                    var_36 = (uint64_t)*(uint32_t *)(local_sp_1 + 12UL);
                    var_37 = (uint64_t)*(uint32_t *)(local_sp_1 + 8UL);
                    var_38 = local_sp_1 + (-8L);
                    *(uint64_t *)var_38 = 4206589UL;
                    indirect_placeholder_3(0UL, 4256408UL, 1UL, 0UL, var_37, var_33, var_36);
                    local_sp_5 = var_38;
                }
            }
        }
    }
}
