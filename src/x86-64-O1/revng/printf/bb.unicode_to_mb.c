typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_r12(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t indirect_placeholder_8(void);
extern uint32_t init_state_0x82fc(void);
void bb_unicode_to_mb(uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t rcx) {
    uint64_t var_7;
    uint64_t local_sp_1;
    uint64_t var_18;
    uint64_t local_sp_0;
    uint64_t var_13;
    uint64_t cc_src_0;
    uint64_t cc_dst_0;
    uint32_t cc_op_0;
    uint64_t rdi2_0;
    uint64_t rsi3_0;
    uint64_t rcx4_0;
    uint64_t cc_src_1;
    uint64_t cc_dst_1;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint32_t var_6;
    uint64_t var_11;
    uint64_t var_12;
    uint32_t cc_op_1;
    uint64_t var_14;
    uint64_t var_15;
    bool var_16;
    uint64_t var_17;
    uint64_t var_19;
    uint64_t *var_20;
    uint64_t var_21;
    bool var_22;
    uint64_t *var_23;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_rbp();
    var_4 = init_r12();
    var_5 = init_cc_src2();
    var_6 = init_state_0x82fc();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_2;
    var_7 = (uint64_t)(uint32_t)rdi;
    local_sp_1 = var_0 + (-104L);
    cc_src_0 = 0UL;
    cc_dst_0 = 0UL;
    cc_op_0 = 16U;
    rdi2_0 = 4259391UL;
    rcx4_0 = 6UL;
    if (*(uint32_t *)4281540UL != 0U) {
        var_8 = var_0 + (-112L);
        *(uint64_t *)var_8 = 4215820UL;
        var_9 = indirect_placeholder_8();
        var_10 = (uint64_t)var_6;
        rsi3_0 = var_9;
        local_sp_0 = var_8;
        cc_op_0 = 14U;
        cc_src_1 = cc_src_0;
        cc_dst_1 = cc_dst_0;
        cc_op_1 = cc_op_0;
        while (rcx4_0 != 0UL)
            {
                var_11 = (uint64_t)*(unsigned char *)rdi2_0;
                var_12 = (uint64_t)*(unsigned char *)rsi3_0 - var_11;
                cc_src_0 = var_11;
                cc_dst_0 = var_12;
                cc_src_1 = var_11;
                cc_dst_1 = var_12;
                cc_op_1 = 14U;
                if ((uint64_t)(unsigned char)var_12 == 0UL) {
                    break;
                }
                rdi2_0 = rdi2_0 + var_10;
                rsi3_0 = rsi3_0 + var_10;
                rcx4_0 = rcx4_0 + (-1L);
                cc_op_0 = 14U;
                cc_src_1 = cc_src_0;
                cc_dst_1 = cc_dst_0;
                cc_op_1 = cc_op_0;
            }
        var_13 = helper_cc_compute_all_wrapper(cc_dst_1, cc_src_1, var_5, cc_op_1);
        var_14 = ((var_13 & 65UL) == 0UL);
        var_15 = helper_cc_compute_c_wrapper(cc_dst_1, var_13, var_5, 1U);
        var_16 = ((((uint32_t)var_14 - (uint32_t)var_15) << 24U) == 0U);
        *(uint32_t *)4281536UL = var_16;
        var_17 = var_0 + (-120L);
        *(uint64_t *)var_17 = 4215888UL;
        indirect_placeholder();
        *(uint64_t *)4281528UL = var_9;
        local_sp_0 = var_17;
        if (!var_16 && var_9 == 18446744073709551615UL) {
            var_18 = var_0 + (-128L);
            *(uint64_t *)var_18 = 4215916UL;
            indirect_placeholder();
            *(uint64_t *)4281528UL = 18446744073709551615UL;
            local_sp_0 = var_18;
        }
        *(uint32_t *)4281540UL = 1U;
        local_sp_1 = local_sp_0;
    }
    if (*(uint32_t *)4281536UL != 0U) {
        if (*(uint64_t *)4281528UL != 18446744073709551615UL) {
            *(uint64_t *)(local_sp_1 + (-8L)) = 4215938UL;
            indirect_placeholder();
            return;
        }
    }
    var_19 = local_sp_1 + 58UL;
    var_20 = (uint64_t *)(local_sp_1 + (-8L));
    *var_20 = 4215769UL;
    var_21 = indirect_placeholder_9(6UL, var_19, var_7);
    if ((int)(uint32_t)var_21 < (int)0U) {
        *(uint64_t *)(local_sp_1 + (-16L)) = 4215956UL;
        indirect_placeholder();
    } else {
        if (*(uint32_t *)4281536UL == 0U) {
            *(uint64_t *)(local_sp_1 + (-16L)) = 4215804UL;
            indirect_placeholder();
        } else {
            *var_20 = (local_sp_1 + 50UL);
            *(uint64_t *)local_sp_1 = (uint64_t)((long)(var_21 << 32UL) >> (long)32UL);
            *(uint64_t *)(local_sp_1 + 8UL) = (local_sp_1 + 24UL);
            *(uint64_t *)(local_sp_1 + 16UL) = 25UL;
            *(uint64_t *)(local_sp_1 + (-16L)) = 4216026UL;
            indirect_placeholder();
            var_22 = (*var_20 == 0UL);
            var_23 = (uint64_t *)(local_sp_1 + (-24L));
            if (var_22) {
                *var_23 = 4216072UL;
                indirect_placeholder();
                *(uint64_t *)(local_sp_1 + (-32L)) = 4216097UL;
                indirect_placeholder();
            } else {
                *var_23 = 4216115UL;
                indirect_placeholder();
            }
        }
    }
    return;
}
