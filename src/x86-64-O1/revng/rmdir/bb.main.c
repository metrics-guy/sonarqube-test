typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_6_ret_type;
struct indirect_placeholder_5_ret_type;
struct indirect_placeholder_8_ret_type;
struct indirect_placeholder_9_ret_type;
struct indirect_placeholder_6_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_5_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_8_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_9_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern void indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_2(uint64_t param_0);
extern void indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_6_ret_type indirect_placeholder_6(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_5_ret_type indirect_placeholder_5(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_8_ret_type indirect_placeholder_8(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_9_ret_type indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_9_ret_type var_12;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t local_sp_5;
    uint64_t rbx_0;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t local_sp_3;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t rbp_1;
    uint64_t r12_1;
    struct indirect_placeholder_6_ret_type var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    struct indirect_placeholder_5_ret_type var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t rbp_0;
    uint64_t r12_0;
    uint64_t local_sp_2;
    uint64_t local_sp_0;
    uint64_t var_48;
    uint32_t var_49;
    uint64_t rax_1;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t rbx_1;
    uint64_t var_24;
    uint64_t local_sp_4;
    uint64_t var_31;
    uint64_t var_32;
    struct indirect_placeholder_8_ret_type var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_11;
    uint64_t var_13;
    uint32_t var_14;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_8 = (uint64_t)(uint32_t)rdi;
    var_9 = *(uint64_t *)rsi;
    *(uint64_t *)(var_0 + (-64L)) = 4205269UL;
    indirect_placeholder_2(var_9);
    *(uint64_t *)(var_0 + (-72L)) = 4205284UL;
    indirect_placeholder();
    var_10 = var_0 + (-80L);
    *(uint64_t *)var_10 = 4205294UL;
    indirect_placeholder();
    *(unsigned char *)4277074UL = (unsigned char)'\x00';
    rbx_1 = rsi;
    rbp_1 = var_8;
    r12_1 = 1UL;
    local_sp_5 = var_10;
    while (1U)
        {
            var_11 = local_sp_5 + (-8L);
            *(uint64_t *)var_11 = 4205327UL;
            var_12 = indirect_placeholder_9(4255955UL, var_8, rsi, 4256832UL, 0UL);
            var_13 = var_12.field_0;
            var_14 = (uint32_t)var_13;
            local_sp_2 = var_11;
            local_sp_3 = var_11;
            local_sp_5 = var_11;
            if ((uint64_t)(var_14 + 1U) == 0UL) {
                var_15 = var_13 + (-112L);
                if ((uint64_t)(uint32_t)var_15 == 0UL) {
                    *(unsigned char *)4277074UL = (unsigned char)'\x01';
                    continue;
                }
                var_16 = helper_cc_compute_all_wrapper(var_15, 112UL, var_7, 16U);
                if ((uint64_t)(((unsigned char)(var_16 >> 4UL) ^ (unsigned char)var_16) & '\xc0') == 0UL) {
                    if ((uint64_t)(var_14 + (-118)) == 0UL) {
                        *(unsigned char *)4277072UL = (unsigned char)'\x01';
                        continue;
                    }
                    if ((uint64_t)(var_14 + (-128)) != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    *(unsigned char *)4277073UL = (unsigned char)'\x01';
                    continue;
                }
                if ((uint64_t)(var_14 + 131U) != 0UL) {
                    if ((uint64_t)(var_14 + 130U) != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    *(uint64_t *)(local_sp_5 + (-16L)) = 4205444UL;
                    indirect_placeholder_7(rsi, 0UL, var_8);
                    abort();
                }
                var_17 = *(uint64_t *)4276832UL;
                var_18 = *(uint64_t *)4275776UL;
                *(uint64_t *)(local_sp_5 + (-16L)) = 4205417UL;
                indirect_placeholder_4(0UL, 4255839UL, var_18, 4255933UL, var_17, 0UL, 4255939UL);
                var_19 = local_sp_5 + (-24L);
                *(uint64_t *)var_19 = 4205427UL;
                indirect_placeholder();
                local_sp_2 = var_19;
                loop_state_var = 1U;
                break;
            }
            var_20 = (uint64_t)*(uint32_t *)4276956UL;
            var_21 = var_20 - var_8;
            rax_1 = var_20;
            if ((uint64_t)(uint32_t)var_21 == 0UL) {
                var_50 = var_12.field_3;
                var_51 = var_12.field_2;
                var_52 = var_12.field_1;
                *(uint64_t *)(local_sp_5 + (-16L)) = 4205555UL;
                indirect_placeholder_4(0UL, 4255958UL, 0UL, 0UL, var_52, var_51, var_50);
                *(uint64_t *)(local_sp_5 + (-24L)) = 4205565UL;
                indirect_placeholder_7(rsi, 1UL, var_8);
                abort();
            }
            var_22 = *(uint64_t *)4275776UL;
            var_23 = helper_cc_compute_all_wrapper(var_21, var_8, var_7, 16U);
            if ((signed char)((unsigned char)(var_23 >> 4UL) ^ (unsigned char)var_23) >= '\x00') {
                loop_state_var = 0U;
                break;
            }
            while (1U)
                {
                    var_24 = *(uint64_t *)((uint64_t)((long)(rax_1 << 32UL) >> (long)29UL) + rbx_1);
                    rbx_0 = rbx_1;
                    rbp_0 = rbp_1;
                    r12_0 = r12_1;
                    local_sp_4 = local_sp_3;
                    if (*(unsigned char *)4277072UL == '\x00') {
                        *(uint64_t *)(local_sp_3 + (-8L)) = 4205578UL;
                        var_25 = indirect_placeholder_8(4UL, var_24);
                        var_26 = var_25.field_0;
                        var_27 = var_25.field_1;
                        var_28 = var_25.field_2;
                        var_29 = var_25.field_3;
                        var_30 = local_sp_3 + (-16L);
                        *(uint64_t *)var_30 = 4205599UL;
                        indirect_placeholder_4(0UL, var_26, var_22, 4255880UL, var_27, var_28, var_29);
                        local_sp_4 = var_30;
                    }
                    var_31 = local_sp_4 + (-8L);
                    *(uint64_t *)var_31 = 4205722UL;
                    var_32 = indirect_placeholder_3(var_24);
                    local_sp_0 = var_31;
                    if ((uint64_t)(uint32_t)var_32 == 0UL) {
                        if (*(unsigned char *)4277074UL == '\x00') {
                            var_46 = local_sp_4 + (-16L);
                            *(uint64_t *)var_46 = 4205743UL;
                            var_47 = indirect_placeholder_3(var_24);
                            r12_0 = (uint64_t)((uint32_t)r12_1 & (uint32_t)var_47);
                            local_sp_0 = var_46;
                        }
                    } else {
                        *(uint64_t *)(local_sp_4 + (-16L)) = 4205606UL;
                        indirect_placeholder();
                        var_33 = (uint64_t)*(uint32_t *)var_32;
                        var_34 = local_sp_4 + (-24L);
                        *(uint64_t *)var_34 = 4205616UL;
                        var_35 = indirect_placeholder_6(rbx_1, var_33, rbp_1, var_24);
                        var_36 = var_35.field_0;
                        var_37 = var_35.field_1;
                        var_38 = var_35.field_2;
                        rbx_0 = var_37;
                        rbp_0 = var_38;
                        local_sp_0 = var_34;
                        if ((uint64_t)(unsigned char)var_36 == 0UL) {
                            var_39 = (uint64_t)(uint32_t)var_36;
                            *(uint64_t *)(local_sp_4 + (-32L)) = 4205636UL;
                            var_40 = indirect_placeholder_5(4UL, var_24);
                            var_41 = var_40.field_0;
                            var_42 = var_40.field_2;
                            var_43 = var_40.field_3;
                            *(uint64_t *)(local_sp_4 + (-40L)) = 4205644UL;
                            indirect_placeholder();
                            var_44 = (uint64_t)*(uint32_t *)var_41;
                            var_45 = local_sp_4 + (-48L);
                            *(uint64_t *)var_45 = 4205669UL;
                            indirect_placeholder_4(0UL, 4255974UL, 0UL, var_44, var_41, var_42, var_43);
                            r12_0 = var_39;
                            local_sp_0 = var_45;
                        }
                    }
                    var_48 = (uint64_t)*(uint32_t *)4276956UL + 1UL;
                    var_49 = (uint32_t)var_48;
                    *(uint32_t *)4276956UL = var_49;
                    rbx_1 = rbx_0;
                    rbp_1 = rbp_0;
                    r12_1 = r12_0;
                    local_sp_3 = local_sp_0;
                    if ((long)(var_48 << 32UL) < (long)(rbp_0 << 32UL)) {
                        break;
                    }
                    rax_1 = (uint64_t)var_49;
                    continue;
                }
            loop_state_var = 0U;
            break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            return;
        }
        break;
      case 1U:
        {
            *(uint64_t *)(local_sp_2 + (-8L)) = 4205478UL;
            indirect_placeholder_7(rsi, 1UL, var_8);
            abort();
        }
        break;
    }
}
