typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
extern uint64_t init_rax(void);
extern uint32_t init_state_0x82fc(void);
void bb_print_long_entry(uint64_t rdi) {
    uint64_t var_19;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint32_t var_5;
    bool var_6;
    uint64_t *var_7;
    uint64_t local_sp_0;
    uint64_t var_28;
    uint64_t local_sp_6;
    uint64_t var_18;
    uint64_t rcx_3;
    uint64_t local_sp_3;
    uint64_t rcx_0;
    uint64_t local_sp_5;
    uint64_t local_sp_4;
    uint64_t var_20;
    uint64_t rdi1_0;
    uint64_t rcx_1;
    unsigned char var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    bool var_25;
    uint64_t var_26;
    uint64_t *var_27;
    uint64_t var_12;
    uint64_t rdi1_1;
    uint64_t rcx_2;
    unsigned char var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_11;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbx();
    var_3 = init_rbp();
    var_4 = init_r12();
    var_5 = init_state_0x82fc();
    *(uint64_t *)(var_0 + (-8L)) = var_4;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-1056L)) = 4206490UL;
    indirect_placeholder_1();
    *(uint64_t *)(var_0 + (-1064L)) = 4206508UL;
    indirect_placeholder_1();
    *(uint64_t *)(var_0 + (-1072L)) = 4206526UL;
    indirect_placeholder_1();
    *(uint64_t *)(var_0 + (-1080L)) = 4206541UL;
    indirect_placeholder_1();
    var_6 = (var_1 == 0UL);
    var_7 = (uint64_t *)(var_0 + (-1088L));
    rcx_0 = 18446744073709551615UL;
    rcx_1 = 0UL;
    rcx_2 = 18446744073709551615UL;
    rcx_3 = 0UL;
    if (var_6) {
        *var_7 = 4206700UL;
        indirect_placeholder_1();
    } else {
        *var_7 = 4206564UL;
        indirect_placeholder_1();
        var_8 = *(uint64_t *)(var_1 + 24UL);
        var_9 = *(uint64_t *)var_1;
        *(uint64_t *)(var_0 + (-1096L)) = 4206584UL;
        indirect_placeholder(var_8, var_9);
        *(uint64_t *)(var_0 + (-1104L)) = 4206605UL;
        indirect_placeholder_1();
        *(uint64_t *)(var_0 + (-1112L)) = 4206613UL;
        indirect_placeholder_1();
        var_10 = var_0 + (-1120L);
        *(uint64_t *)var_10 = 4206623UL;
        indirect_placeholder_1();
        local_sp_4 = var_10;
        if (*(unsigned char *)4276866UL == '\x00') {
            *(uint64_t *)(var_0 + (-1128L)) = 4206717UL;
            indirect_placeholder_1();
            *(uint64_t *)(var_0 + (-1136L)) = 4206736UL;
            indirect_placeholder_1();
            *(uint64_t *)(var_0 + (-1144L)) = 4206751UL;
            indirect_placeholder_1();
            *(uint64_t *)(var_0 + (-1152L)) = 4206770UL;
            indirect_placeholder_1();
            var_11 = var_0 + (-1160L);
            *(uint64_t *)var_11 = 4206780UL;
            indirect_placeholder_1();
            local_sp_4 = var_11;
        }
        local_sp_5 = local_sp_4;
        if (*(unsigned char *)4276868UL != '\x00') {
            var_12 = (uint64_t)var_5;
            rdi1_1 = *(uint64_t *)(var_1 + 32UL);
            while (rcx_2 != 0UL)
                {
                    var_13 = *(unsigned char *)rdi1_1;
                    var_14 = rcx_2 + (-1L);
                    rcx_2 = var_14;
                    rcx_3 = var_14;
                    if (var_13 == '\x00') {
                        break;
                    }
                    rdi1_1 = rdi1_1 + var_12;
                }
            var_15 = 8UL - rcx_3;
            *(uint64_t *)(local_sp_4 + (-8L)) = 4206815UL;
            var_16 = indirect_placeholder_2(var_15);
            *(uint64_t *)(local_sp_4 + (-16L)) = 4206830UL;
            indirect_placeholder_1();
            *(uint64_t *)var_16 = 7162247809296510511UL;
            *(uint16_t *)(var_16 + 8UL) = (uint16_t)(unsigned short)116U;
            var_17 = local_sp_4 + (-24L);
            *(uint64_t *)var_17 = 4206862UL;
            indirect_placeholder_1();
            local_sp_3 = var_17;
            if (var_16 != 0UL) {
                *(uint64_t *)(local_sp_4 + (-32L)) = 4206885UL;
                indirect_placeholder_1();
                *(uint64_t *)(local_sp_4 + (-40L)) = 4206906UL;
                indirect_placeholder_1();
                var_18 = local_sp_4 + (-48L);
                *(uint64_t *)var_18 = 4206944UL;
                indirect_placeholder_2(var_16);
                local_sp_3 = var_18;
            }
            var_19 = local_sp_3 + (-8L);
            *(uint64_t *)var_19 = 4206952UL;
            indirect_placeholder_1();
            local_sp_5 = var_19;
        }
        local_sp_6 = local_sp_5;
        if (*(unsigned char *)4276867UL != '\x00') {
            var_20 = (uint64_t)var_5;
            rdi1_0 = *(uint64_t *)(var_1 + 32UL);
            while (rcx_0 != 0UL)
                {
                    var_21 = *(unsigned char *)rdi1_0;
                    var_22 = rcx_0 + (-1L);
                    rcx_0 = var_22;
                    rcx_1 = var_22;
                    if (var_21 == '\x00') {
                        break;
                    }
                    rdi1_0 = rdi1_0 + var_20;
                }
            var_23 = 5UL - rcx_1;
            *(uint64_t *)(local_sp_5 + (-8L)) = 4206987UL;
            var_24 = indirect_placeholder_2(var_23);
            *(uint64_t *)(local_sp_5 + (-16L)) = 4207002UL;
            indirect_placeholder_1();
            *(uint32_t *)var_24 = 1819291183U;
            *(uint16_t *)(var_24 + 4UL) = (uint16_t)(unsigned short)28257U;
            *(unsigned char *)(var_24 + 6UL) = (unsigned char)'\x00';
            *(uint64_t *)(local_sp_5 + (-24L)) = 4207031UL;
            indirect_placeholder_1();
            var_25 = (var_24 == 0UL);
            var_26 = local_sp_5 + (-32L);
            var_27 = (uint64_t *)var_26;
            local_sp_0 = var_26;
            local_sp_6 = var_26;
            if (!var_25) {
                *var_27 = 4207049UL;
                indirect_placeholder_1();
                while (1U)
                    {
                        *(uint64_t *)(local_sp_0 + (-8L)) = 4207070UL;
                        indirect_placeholder_1();
                        var_28 = local_sp_0 + (-16L);
                        *(uint64_t *)var_28 = 4207098UL;
                        indirect_placeholder_1();
                        local_sp_0 = var_28;
                        continue;
                    }
            }
            *var_27 = 4207116UL;
            indirect_placeholder_1();
        }
        *(uint64_t *)(local_sp_6 + (-8L)) = 4206668UL;
        indirect_placeholder_1();
    }
    return;
}
