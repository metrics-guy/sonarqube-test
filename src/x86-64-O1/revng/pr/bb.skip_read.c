typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t init_rcx(void);
extern uint64_t init_rax(void);
extern void indirect_placeholder_2(uint64_t param_0, uint64_t param_1);
void bb_skip_read(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint32_t var_10;
    bool var_11;
    unsigned char *_pre49;
    unsigned char *_pre_phi50;
    uint64_t rax_1;
    uint64_t local_sp_0;
    uint64_t r14_4;
    uint64_t var_27;
    uint64_t var_16;
    unsigned char *_pre_phi47;
    uint64_t var_17;
    uint64_t r14_2;
    uint64_t local_sp_5;
    uint64_t local_sp_1;
    uint64_t var_18;
    uint64_t r14_1;
    unsigned char *var_12;
    unsigned char var_13;
    uint64_t var_14;
    uint64_t r14_0;
    unsigned char var_19;
    uint64_t var_20;
    uint64_t local_sp_2;
    uint64_t local_sp_3;
    uint64_t var_21;
    uint64_t local_sp_7;
    uint64_t r14_3;
    uint64_t var_22;
    uint32_t var_23;
    uint64_t var_24;
    uint64_t rax_0;
    uint64_t var_25;
    uint64_t local_sp_8;
    uint64_t var_26;
    uint64_t var_15;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_rcx();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_3;
    var_8 = *(uint64_t *)rdi;
    var_9 = var_0 + (-48L);
    *(uint64_t *)var_9 = 4208504UL;
    indirect_placeholder();
    var_10 = (uint32_t)var_1;
    var_11 = ((uint64_t)(var_10 + (-12)) == 0UL);
    rax_1 = var_1;
    local_sp_1 = var_9;
    local_sp_2 = var_9;
    r14_3 = 1UL;
    local_sp_8 = var_9;
    if (var_11) {
        var_12 = (unsigned char *)(rdi + 57UL);
        var_13 = *var_12;
        var_14 = (uint64_t)var_13;
        _pre_phi50 = var_12;
        _pre_phi47 = var_12;
        if (var_13 == '\x00') {
            var_19 = *(unsigned char *)4322336UL;
            var_20 = (uint64_t)var_19;
            r14_0 = var_20;
            if (var_19 != '\x00') {
                var_26 = local_sp_8 + (-8L);
                *(uint64_t *)var_26 = 4208778UL;
                indirect_placeholder();
                local_sp_0 = var_26;
                r14_4 = r14_3;
                if ((uint64_t)((uint32_t)rax_1 + (-10)) == 0UL) {
                    var_27 = local_sp_8 + (-16L);
                    *(uint64_t *)var_27 = 4208846UL;
                    indirect_placeholder();
                    local_sp_0 = var_27;
                }
                *(uint64_t *)(local_sp_0 + (-8L)) = 4208791UL;
                indirect_placeholder_2(rdi, var_7);
                if (*(unsigned char *)4322000UL == '\x00') {
                    return;
                }
                if (*(unsigned char *)4322527UL != '\x01') {
                    if ((uint64_t)((uint32_t)rsi + (-1)) == 0UL) {
                        return;
                    }
                }
                if ((uint64_t)(unsigned char)r14_4 == 0UL) {
                    *(uint32_t *)4322004UL = (*(uint32_t *)4322004UL + 1U);
                }
            }
            *_pre_phi47 = (unsigned char)'\x01';
            r14_1 = r14_0;
            local_sp_3 = local_sp_2;
        } else {
            var_15 = var_0 + (-56L);
            *(uint64_t *)var_15 = 4208644UL;
            indirect_placeholder();
            local_sp_2 = var_15;
            r14_3 = var_14;
            local_sp_8 = var_15;
            if ((uint64_t)(var_10 + (-10)) == 0UL) {
                var_17 = var_0 + (-64L);
                *(uint64_t *)var_17 = 4208691UL;
                indirect_placeholder();
                local_sp_1 = var_17;
                *_pre_phi50 = (unsigned char)'\x00';
                var_18 = (var_6 & (-256L)) | var_11;
                _pre_phi47 = _pre_phi50;
                r14_1 = var_18;
                r14_0 = var_18;
                local_sp_2 = local_sp_1;
                local_sp_3 = local_sp_1;
                if (*(unsigned char *)4322336UL == '\x00') {
                    *_pre_phi47 = (unsigned char)'\x01';
                    r14_1 = r14_0;
                    local_sp_3 = local_sp_2;
                }
            } else {
                *var_12 = (unsigned char)'\x00';
                var_16 = (var_6 & (-256L)) | 1UL;
                r14_0 = var_16;
                if (*(unsigned char *)4322336UL != '\x00') {
                    var_26 = local_sp_8 + (-8L);
                    *(uint64_t *)var_26 = 4208778UL;
                    indirect_placeholder();
                    local_sp_0 = var_26;
                    r14_4 = r14_3;
                    if ((uint64_t)((uint32_t)rax_1 + (-10)) == 0UL) {
                        var_27 = local_sp_8 + (-16L);
                        *(uint64_t *)var_27 = 4208846UL;
                        indirect_placeholder();
                        local_sp_0 = var_27;
                    }
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4208791UL;
                    indirect_placeholder_2(rdi, var_7);
                    if (*(unsigned char *)4322000UL == '\x00') {
                        return;
                    }
                    if (*(unsigned char *)4322527UL != '\x01') {
                        if ((uint64_t)((uint32_t)rsi + (-1)) == 0UL) {
                            return;
                        }
                    }
                    if ((uint64_t)(unsigned char)r14_4 == 0UL) {
                        *(uint32_t *)4322004UL = (*(uint32_t *)4322004UL + 1U);
                    }
                }
                *_pre_phi47 = (unsigned char)'\x01';
                r14_1 = r14_0;
                local_sp_3 = local_sp_2;
            }
        }
    } else {
        _pre49 = (unsigned char *)(rdi + 57UL);
        _pre_phi50 = _pre49;
        *_pre_phi50 = (unsigned char)'\x00';
        var_18 = (var_6 & (-256L)) | var_11;
        _pre_phi47 = _pre_phi50;
        r14_1 = var_18;
        r14_0 = var_18;
        local_sp_2 = local_sp_1;
        local_sp_3 = local_sp_1;
        if (*(unsigned char *)4322336UL == '\x00') {
            *_pre_phi47 = (unsigned char)'\x01';
            r14_1 = r14_0;
            local_sp_3 = local_sp_2;
        }
    }
    local_sp_5 = local_sp_3;
    r14_2 = r14_1;
    local_sp_7 = local_sp_3;
    r14_4 = r14_1;
    if ((uint64_t)(var_10 + (-10)) == 0UL) {
        r14_4 = 0UL;
        if ((uint64_t)(unsigned char)r14_1 == 0UL) {
            r14_3 = r14_2;
            local_sp_8 = local_sp_7;
            if (*(unsigned char *)4322336UL != '\x00') {
                if (*(unsigned char *)4322527UL == '\x00') {
                    *(unsigned char *)(rdi + 57UL) = (unsigned char)'\x00';
                } else {
                    var_22 = *(uint64_t *)4322568UL;
                    var_23 = *(uint32_t *)4322024UL;
                    rax_0 = var_22;
                    rax_1 = var_22;
                    if (var_23 != 0U) {
                        var_24 = (((((uint64_t)var_23 << 6UL) + 274877906880UL) & 274877906880UL) + 64UL) + var_22;
                        rax_1 = var_24;
                        *(unsigned char *)(rax_0 + 57UL) = (unsigned char)'\x00';
                        var_25 = rax_0 + 64UL;
                        rax_0 = var_25;
                        do {
                            *(unsigned char *)(rax_0 + 57UL) = (unsigned char)'\x00';
                            var_25 = rax_0 + 64UL;
                            rax_0 = var_25;
                        } while (var_25 != var_24);
                    }
                }
            }
            var_26 = local_sp_8 + (-8L);
            *(uint64_t *)var_26 = 4208778UL;
            indirect_placeholder();
            local_sp_0 = var_26;
            r14_4 = r14_3;
            if ((uint64_t)((uint32_t)rax_1 + (-10)) == 0UL) {
                var_27 = local_sp_8 + (-16L);
                *(uint64_t *)var_27 = 4208846UL;
                indirect_placeholder();
                local_sp_0 = var_27;
            }
            *(uint64_t *)(local_sp_0 + (-8L)) = 4208791UL;
            indirect_placeholder_2(rdi, var_7);
            if (*(unsigned char *)4322000UL == '\x00') {
                return;
            }
            if (*(unsigned char *)4322527UL != '\x01') {
                if ((uint64_t)((uint32_t)rsi + (-1)) == 0UL) {
                    return;
                }
            }
            if ((uint64_t)(unsigned char)r14_4 == 0UL) {
                *(uint32_t *)4322004UL = (*(uint32_t *)4322004UL + 1U);
            }
        } else {
            r14_2 = 0UL;
            if ((uint64_t)(var_10 + 1U) == 0UL) {
                r14_4 = (uint64_t)0L;
                while (1U)
                    {
                        var_21 = local_sp_5 + (-8L);
                        *(uint64_t *)var_21 = 4208564UL;
                        indirect_placeholder();
                        local_sp_5 = var_21;
                        local_sp_7 = var_21;
                        if (!0) {
                            loop_state_var = 0U;
                            break;
                        }
                        if (var_11) {
                            continue;
                        }
                        loop_state_var = 1U;
                        break;
                    }
                switch (loop_state_var) {
                  case 1U:
                    {
                        break;
                    }
                    break;
                  case 0U:
                    {
                        if (*(unsigned char *)4322000UL == '\x00') {
                            return;
                        }
                        if (*(unsigned char *)4322527UL != '\x01') {
                            if ((uint64_t)((uint32_t)rsi + (-1)) == 0UL) {
                                return;
                            }
                        }
                        if ((uint64_t)(unsigned char)r14_4 == 0UL) {
                            *(uint32_t *)4322004UL = (*(uint32_t *)4322004UL + 1U);
                        }
                    }
                    break;
                }
            } else {
                *(uint64_t *)(local_sp_3 + (-8L)) = 4208591UL;
                indirect_placeholder_2(var_8, rdi);
                if (*(unsigned char *)4322000UL == '\x00') {
                    return;
                }
                if (*(unsigned char *)4322527UL != '\x01') {
                    if ((uint64_t)((uint32_t)rsi + (-1)) == 0UL) {
                        return;
                    }
                }
                if ((uint64_t)(unsigned char)r14_4 == 0UL) {
                    *(uint32_t *)4322004UL = (*(uint32_t *)4322004UL + 1U);
                }
            }
        }
    } else {
        if (*(unsigned char *)4322000UL == '\x00') {
            return;
        }
        if (*(unsigned char *)4322527UL != '\x01') {
            if ((uint64_t)((uint32_t)rsi + (-1)) == 0UL) {
                return;
            }
        }
        if ((uint64_t)(unsigned char)r14_4 == 0UL) {
            *(uint32_t *)4322004UL = (*(uint32_t *)4322004UL + 1U);
        }
    }
}
