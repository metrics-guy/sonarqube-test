typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_4_ret_type;
struct indirect_placeholder_4_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t init_r9(void);
extern uint64_t init_rax(void);
extern void indirect_placeholder_2(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_4_ret_type indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
uint64_t bb_skip_to_page(uint64_t rdi) {
    uint32_t var_16;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t local_sp_0;
    uint64_t var_31;
    uint64_t rax_2;
    struct indirect_placeholder_4_ret_type var_30;
    uint64_t rax_0;
    uint64_t rax_1;
    uint64_t r12_0;
    uint64_t local_sp_5;
    uint32_t var_9;
    uint32_t _pre;
    uint32_t var_13;
    uint32_t var_10;
    uint64_t local_sp_1;
    uint64_t r14_0;
    uint32_t var_15;
    uint32_t var_11;
    uint64_t local_sp_2;
    uint64_t rbx_0;
    uint64_t rbp_0;
    uint64_t var_29;
    uint64_t var_12;
    uint64_t local_sp_3;
    uint64_t var_14;
    uint64_t local_sp_4;
    uint64_t var_17;
    uint32_t var_18;
    uint64_t local_sp_8;
    uint32_t var_19;
    uint64_t local_sp_6;
    uint64_t rbx_1;
    uint64_t rbp_1;
    uint32_t var_21;
    uint64_t var_20;
    uint64_t local_sp_7;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t rdx_0;
    uint32_t *var_27;
    uint64_t var_28;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_r9();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    var_8 = var_0 + (-40L);
    *(uint64_t *)var_8 = var_3;
    local_sp_0 = var_8;
    rax_0 = var_1;
    rax_1 = var_1;
    r12_0 = 1UL;
    r14_0 = 1UL;
    rbp_0 = 1UL;
    rbp_1 = 1UL;
    if (rdi > 1UL) {
        return (rax_0 & (-256L)) | ((int)*(uint32_t *)4322464UL > (int)0U);
    }
    while (1U)
        {
            local_sp_1 = local_sp_0;
            local_sp_5 = local_sp_0;
            rax_2 = rax_1;
            if ((int)*(uint32_t *)4322512UL <= (int)1U) {
                _pre = *(uint32_t *)4322024UL;
                var_9 = _pre;
                var_10 = _pre;
                while (1U)
                    {
                        var_16 = var_10;
                        var_15 = var_9;
                        var_11 = var_9;
                        local_sp_2 = local_sp_1;
                        local_sp_4 = local_sp_1;
                        if ((int)var_10 <= (int)0U) {
                            rbx_0 = *(uint64_t *)4322568UL;
                            while (1U)
                                {
                                    var_13 = var_11;
                                    local_sp_3 = local_sp_2;
                                    if (*(uint32_t *)(rbx_0 + 16UL) == 0U) {
                                        var_12 = local_sp_2 + (-8L);
                                        *(uint64_t *)var_12 = 4208898UL;
                                        indirect_placeholder_2(rbx_0, rbp_0);
                                        var_13 = *(uint32_t *)4322024UL;
                                        local_sp_3 = var_12;
                                    }
                                    var_14 = rbp_0 + 1UL;
                                    var_16 = var_13;
                                    var_15 = var_13;
                                    var_11 = var_13;
                                    local_sp_2 = local_sp_3;
                                    local_sp_4 = local_sp_3;
                                    if ((long)(uint64_t)((long)(var_14 << 32UL) >> (long)32UL) > (long)(uint64_t)var_13) {
                                        break;
                                    }
                                    rbx_0 = rbx_0 + 64UL;
                                    rbp_0 = (uint64_t)(uint32_t)var_14;
                                    continue;
                                }
                        }
                        var_17 = r14_0 + 1UL;
                        var_9 = var_15;
                        var_10 = var_16;
                        local_sp_1 = local_sp_4;
                        local_sp_5 = local_sp_4;
                        if ((long)(uint64_t)((long)(var_17 << 32UL) >> (long)32UL) < (long)(uint64_t)*(uint32_t *)4322512UL) {
                            break;
                        }
                        r14_0 = (uint64_t)(uint32_t)var_17;
                        continue;
                    }
            }
            *(unsigned char *)4322336UL = (unsigned char)'\x01';
            var_18 = *(uint32_t *)4322024UL;
            var_19 = var_18;
            local_sp_6 = local_sp_5;
            local_sp_8 = local_sp_5;
            if ((int)var_18 <= (int)0U) {
                rbx_1 = *(uint64_t *)4322568UL;
                while (1U)
                    {
                        var_21 = var_19;
                        local_sp_7 = local_sp_6;
                        if (*(uint32_t *)(rbx_1 + 16UL) == 0U) {
                            var_20 = local_sp_6 + (-8L);
                            *(uint64_t *)var_20 = 4208990UL;
                            indirect_placeholder_2(rbx_1, rbp_1);
                            var_21 = *(uint32_t *)4322024UL;
                            local_sp_7 = var_20;
                        }
                        var_22 = rbp_1 + 1UL;
                        var_23 = (uint64_t)var_21;
                        var_19 = var_21;
                        local_sp_6 = local_sp_7;
                        local_sp_8 = local_sp_7;
                        rax_2 = var_23;
                        if ((long)(var_23 << 32UL) < (long)(var_22 << 32UL)) {
                            break;
                        }
                        rbx_1 = rbx_1 + 64UL;
                        rbp_1 = (uint64_t)(uint32_t)var_22;
                        continue;
                    }
                var_24 = *(uint64_t *)4322568UL;
                var_25 = helper_cc_compute_all_wrapper(var_23, 0UL, 0UL, 24U);
                rdx_0 = var_24;
                if (*(unsigned char *)4322052UL != '\x00' & (uint64_t)(((unsigned char)(var_25 >> 4UL) ^ (unsigned char)var_25) & '\xc0') != 0UL) {
                    var_26 = ((((var_23 << 6UL) + 274877906880UL) & 274877906880UL) + 64UL) + var_24;
                    rax_2 = var_26;
                    var_28 = rdx_0 + 64UL;
                    rdx_0 = var_28;
                    do {
                        var_27 = (uint32_t *)(rdx_0 + 16UL);
                        if (*var_27 == 3U) {
                            *var_27 = 2U;
                        }
                        var_28 = rdx_0 + 64UL;
                        rdx_0 = var_28;
                    } while (var_26 != var_28);
                }
            }
            var_29 = local_sp_8 + (-8L);
            *(uint64_t *)var_29 = 4209006UL;
            indirect_placeholder();
            *(unsigned char *)4322336UL = (unsigned char)'\x00';
            rax_0 = rax_2;
            local_sp_0 = var_29;
            rax_1 = rax_2;
            if ((int)*(uint32_t *)4322464UL > (int)0U) {
                var_31 = r12_0 + 1UL;
                r12_0 = var_31;
                if (var_31 != rdi) {
                    continue;
                }
                break;
            }
            *(uint64_t *)(local_sp_8 + (-16L)) = 4209158UL;
            var_30 = indirect_placeholder_4(0UL, 4280968UL, 0UL, 0UL, rdi, var_7, r12_0);
            rax_0 = var_30.field_0;
            break;
        }
    return (rax_0 & (-256L)) | ((int)*(uint32_t *)4322464UL > (int)0U);
}
