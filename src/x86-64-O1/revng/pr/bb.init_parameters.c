typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_init_parameters_ret_type;
struct helper_idivl_EAX_wrapper_ret_type;
struct type_6;
struct indirect_placeholder_70_ret_type;
struct bb_init_parameters_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_idivl_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint32_t field_4;
    uint32_t field_5;
    uint32_t field_6;
    uint64_t field_7;
    uint32_t field_8;
    uint64_t field_9;
    uint32_t field_10;
    unsigned char field_11;
    uint32_t field_12;
};
struct type_6 {
};
struct indirect_placeholder_70_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint32_t init_state_0x8248(void);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern struct helper_idivl_EAX_wrapper_ret_type helper_idivl_EAX_wrapper(struct type_6 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint32_t param_13, uint64_t param_14, uint32_t param_15, uint64_t param_16, uint64_t param_17, uint64_t param_18, uint32_t param_19);
extern struct indirect_placeholder_70_ret_type indirect_placeholder_70(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
struct bb_init_parameters_ret_type bb_init_parameters(uint64_t rbx, uint64_t rdi, uint64_t rbp, uint64_t r10, uint64_t r9, uint64_t r8) {
    uint64_t var_0;
    uint32_t var_1;
    uint64_t var_2;
    uint32_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint32_t var_7;
    uint64_t var_8;
    uint32_t var_9;
    uint32_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint32_t var_13;
    uint32_t var_14;
    uint64_t rdx_0;
    unsigned char var_15;
    uint32_t var_16;
    uint64_t var_17;
    bool var_18;
    bool var_19;
    uint64_t r87_1;
    unsigned char var_20;
    uint64_t var_21;
    bool var_22;
    uint32_t var_23;
    uint64_t r87_0;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t rax_0;
    uint64_t rax_1;
    uint64_t var_28;
    uint32_t var_29;
    uint32_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_38;
    struct helper_idivl_EAX_wrapper_ret_type var_37;
    uint64_t var_39;
    uint64_t local_sp_0;
    uint32_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    struct bb_init_parameters_ret_type mrv;
    struct bb_init_parameters_ret_type mrv1;
    uint32_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_state_0x8248();
    var_2 = init_state_0x9018();
    var_3 = init_state_0x9010();
    var_4 = init_state_0x8408();
    var_5 = init_state_0x8328();
    var_6 = init_state_0x82d8();
    var_7 = init_state_0x9080();
    var_8 = var_0 + (-8L);
    var_9 = *(uint32_t *)4322048UL;
    var_10 = var_9 + (-10);
    var_11 = (uint64_t)var_10;
    *(uint32_t *)4322512UL = var_10;
    var_12 = helper_cc_compute_all_wrapper(var_11, 0UL, 0UL, 24U);
    rdx_0 = 0UL;
    r87_1 = r8;
    r87_0 = r8;
    rax_1 = 0UL;
    local_sp_0 = var_8;
    if ((uint64_t)(((unsigned char)(var_12 >> 4UL) ^ (unsigned char)var_12) & '\xc0') == 0UL) {
        if (*(unsigned char *)4322053UL == '\x00') {
            *(uint32_t *)4322512UL = var_9;
        }
    } else {
        *(unsigned char *)4322053UL = (unsigned char)'\x00';
        *(unsigned char *)4322522UL = (unsigned char)'\x01';
        *(uint32_t *)4322512UL = var_9;
    }
    if (*(unsigned char *)4322424UL == '\x00') {
        var_13 = *(uint32_t *)4322512UL;
        *(uint32_t *)4322512UL = (uint32_t)(uint64_t)((long)((uint64_t)((uint32_t)((uint64_t)var_13 >> 31UL) + var_13) << 32UL) >> (long)33UL);
    }
    var_14 = (uint32_t)rdi;
    if ((uint64_t)var_14 == 0UL) {
        *(unsigned char *)4322527UL = (unsigned char)'\x00';
    } else {
        var_15 = *(unsigned char *)4322527UL;
        rdx_0 = (uint64_t)var_15;
        if (var_15 == '\x00') {
            *(uint32_t *)4322024UL = var_14;
            rdx_0 = 1UL;
        }
    }
    if (*(unsigned char *)4322052UL == '\x00') {
        *(unsigned char *)4322516UL = (unsigned char)'\x01';
    }
    var_16 = *(uint32_t *)4322024UL;
    var_17 = (uint64_t)var_16;
    if ((int)var_16 > (int)1U) {
        *(unsigned char *)4322052UL = (unsigned char)'\x00';
    } else {
        var_18 = (*(unsigned char *)4322416UL == '\x00');
        var_19 = (*(unsigned char *)4322508UL == '\x00');
        if (var_18) {
            *(uint64_t *)4321984UL = (var_19 ? 4297123UL : 4280337UL);
            *(uint32_t *)4322412UL = 1U;
            *(unsigned char *)4322416UL = (unsigned char)'\x01';
        } else {
            if (!var_19 & *(uint32_t *)4322412UL != 1U && **(unsigned char **)4321984UL == '\t') {
                *(uint64_t *)4321984UL = 4297123UL;
            }
        }
        *(unsigned char *)4322509UL = (unsigned char)'\x01';
        *(unsigned char *)4322500UL = (unsigned char)'\x01';
    }
    if (*(unsigned char *)4322508UL == '\x00') {
        *(unsigned char *)4322509UL = (unsigned char)'\x00';
    }
    var_20 = *(unsigned char *)4322444UL;
    var_21 = (uint64_t)var_20;
    var_22 = (var_20 == '\x00');
    if (!var_22) {
        *(uint32_t *)4322004UL = *(uint32_t *)4321996UL;
        var_23 = *(uint32_t *)4321992UL;
        rax_0 = (uint64_t)(var_23 + 1U);
        if (*(unsigned char *)4322008UL == '\t') {
            var_24 = (uint64_t)var_23;
            var_25 = (uint64_t)((long)(var_24 << 32UL) >> (long)63UL) >> 29UL;
            var_26 = var_25 & 7UL;
            var_27 = (var_25 + var_24) & 7UL;
            r87_0 = var_27;
            rax_0 = (uint64_t)((((uint32_t)var_26 - (uint32_t)var_27) + var_23) + 8U);
        }
        *(uint32_t *)4322440UL = (uint32_t)rax_0;
        r87_1 = r87_0;
        rax_1 = (rdx_0 == 0UL) ? 0UL : rax_0;
    }
    var_28 = (uint64_t)((long)((var_17 << 32UL) + (-4294967296L)) >> (long)32UL) * (uint64_t)*(uint32_t *)4322412UL;
    var_29 = (uint32_t)var_28;
    var_30 = (uint32_t)(var_28 >> 32UL);
    var_31 = (uint64_t)var_29;
    var_32 = helper_cc_compute_all_wrapper(var_31, (uint64_t)((uint32_t)((int)var_29 >> (int)31U) - var_30), 0UL, 4U);
    var_33 = ((uint64_t)((uint16_t)var_32 & (unsigned short)2048U) == 0UL) ? var_31 : 2147483647UL;
    var_34 = (uint64_t)(*(uint32_t *)4322044UL - (uint32_t)rax_1);
    var_35 = var_34 - var_33;
    var_36 = helper_cc_compute_all_wrapper(var_35, var_33, 0UL, 16U);
    if ((uint64_t)((uint16_t)var_36 & (unsigned short)2048U) != 0UL) {
        *(uint32_t *)4322504UL = 0U;
        *(uint64_t *)(var_0 + (-16L)) = 4206077UL;
        indirect_placeholder_70(0UL, 4280339UL, 1UL, 0UL, var_17, r9, r87_1);
        abort();
    }
    var_37 = helper_idivl_EAX_wrapper((struct type_6 *)(0UL), var_17, 4205757UL, (uint64_t)(uint32_t)var_35, (uint64_t)(uint32_t)(uint64_t)((long)(var_35 << 32UL) >> (long)63UL), rbx, var_34, rbp, var_21, var_17, r10, r9, r87_1, var_1, var_2, var_3, var_4, var_5, var_6, var_7);
    var_38 = var_37.field_1;
    *(uint32_t *)4322504UL = (uint32_t)var_38;
    var_39 = helper_cc_compute_all_wrapper(var_38, 0UL, 0UL, 24U);
    if ((uint64_t)(((unsigned char)(var_39 >> 4UL) ^ (unsigned char)var_39) & '\xc0') == 0UL) {
        return;
    }
    if (!var_22) {
        *(uint64_t *)(var_0 + (-16L)) = 4206000UL;
        indirect_placeholder();
        var_40 = *(uint32_t *)4321992UL;
        var_41 = (var_40 > 11U) ? ((uint64_t)var_40 + 1UL) : 12UL;
        var_42 = var_0 + (-24L);
        *(uint64_t *)var_42 = 4206030UL;
        var_43 = indirect_placeholder_1(var_41);
        *(uint64_t *)4322432UL = var_43;
        local_sp_0 = var_42;
    }
    *(uint64_t *)(local_sp_0 + (-8L)) = 4205794UL;
    indirect_placeholder();
    var_44 = *(uint32_t *)4322036UL;
    var_45 = (uint64_t)(((int)var_44 > (int)8U) ? var_44 : 8U);
    *(uint64_t *)(local_sp_0 + (-16L)) = 4205821UL;
    var_46 = indirect_placeholder_1(var_45);
    *(uint64_t *)4322344UL = var_46;
    mrv.field_0 = rbx;
    mrv1 = mrv;
    mrv1.field_1 = rbp;
    return mrv1;
}
