typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder_6(uint64_t param_0, uint64_t param_1, uint64_t param_2);
void bb_init_page(uint64_t rsi, uint64_t r10, uint64_t r8) {
    uint64_t rax_1;
    uint32_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint32_t var_1;
    uint64_t var_2;
    uint64_t var_0;
    uint64_t var_4;
    uint64_t rax_0;
    uint64_t var_5;
    uint64_t rsi1_0;
    uint64_t var_3;
    var_0 = revng_init_local_sp(0UL);
    if (*(unsigned char *)4322052UL == '\x00') {
        var_6 = *(uint32_t *)4322024UL;
        var_7 = *(uint64_t *)4322568UL;
        rax_1 = var_7;
        if (var_6 != 0U) {
            var_8 = (((((uint64_t)var_6 << 6UL) + 274877906880UL) & 274877906880UL) + 64UL) + var_7;
            *(uint32_t *)(rax_1 + 48UL) = ((*(uint32_t *)(rax_1 + 16UL) == 0U) ? *(uint32_t *)4322512UL : 0U);
            var_9 = rax_1 + 64UL;
            rax_1 = var_9;
            do {
                *(uint32_t *)(rax_1 + 48UL) = ((*(uint32_t *)(rax_1 + 16UL) == 0U) ? *(uint32_t *)4322512UL : 0U);
                var_9 = rax_1 + 64UL;
                rax_1 = var_9;
            } while (var_9 != var_8);
        }
    } else {
        *(uint64_t *)(var_0 + (-16L)) = 4211192UL;
        indirect_placeholder_6(rsi, r10, r8);
        var_1 = *(uint32_t *)4322024UL;
        var_2 = (uint64_t)var_1;
        var_3 = *(uint64_t *)4322568UL;
        rax_0 = var_3;
        rsi1_0 = var_3;
        if ((uint64_t)(var_1 + (-1)) != 0UL) {
            var_4 = ((((var_2 << 6UL) + 274877906816UL) & 274877906880UL) + 64UL) + var_3;
            rsi1_0 = var_4;
            *(uint32_t *)(rax_0 + 48UL) = *(uint32_t *)(rax_0 + 44UL);
            var_5 = rax_0 + 64UL;
            rax_0 = var_5;
            do {
                *(uint32_t *)(rax_0 + 48UL) = *(uint32_t *)(rax_0 + 44UL);
                var_5 = rax_0 + 64UL;
                rax_0 = var_5;
            } while (var_4 != var_5);
        }
        if (*(unsigned char *)4322516UL == '\x00') {
            *(uint32_t *)(rsi1_0 + 48UL) = *(uint32_t *)(rsi1_0 + 44UL);
        } else {
            if (*(uint32_t *)(rsi1_0 + 16UL) == 0U) {
                *(uint32_t *)(rsi1_0 + 48UL) = *(uint32_t *)4322512UL;
            } else {
                *(uint32_t *)(rsi1_0 + 48UL) = 0U;
            }
        }
    }
    return;
}
