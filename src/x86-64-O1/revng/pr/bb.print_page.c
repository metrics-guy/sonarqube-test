typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_26_ret_type;
struct indirect_placeholder_28_ret_type;
struct indirect_placeholder_29_ret_type;
struct indirect_placeholder_26_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_28_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_29_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t init_r9(void);
extern uint64_t indirect_placeholder_27(void);
extern void indirect_placeholder_22(uint64_t param_0);
extern void indirect_placeholder_6(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_26_ret_type indirect_placeholder_26(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_28_ret_type indirect_placeholder_28(uint64_t param_0);
extern struct indirect_placeholder_29_ret_type indirect_placeholder_29(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
typedef _Bool bool;
uint64_t bb_print_page(uint64_t rsi, uint64_t r10, uint64_t r8) {
    uint64_t r12_7;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t rdx_1;
    uint64_t r102_6;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t rbx_0;
    uint64_t r14_4;
    uint64_t r9_1;
    uint64_t r83_1;
    uint64_t r12_0;
    uint64_t local_sp_9;
    uint64_t r102_7;
    uint64_t r14_0;
    uint64_t local_sp_8;
    uint64_t local_sp_4;
    uint64_t r102_0;
    uint64_t local_sp_0;
    uint64_t r9_0;
    uint64_t local_sp_14;
    uint64_t r83_0;
    uint64_t r14_6;
    uint64_t r12_1;
    uint64_t r14_1;
    uint64_t r102_1;
    uint64_t local_sp_1;
    uint64_t var_42;
    uint64_t r12_2;
    uint64_t r14_2;
    uint64_t local_sp_2;
    uint64_t var_45;
    uint64_t r12_10;
    uint64_t r12_3;
    uint64_t r14_3;
    uint64_t local_sp_3;
    uint64_t r12_9;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t rax_0;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t r83_7;
    uint64_t local_sp_7;
    uint64_t r102_2;
    uint64_t r9_2;
    uint64_t r83_2;
    unsigned char var_28;
    uint64_t var_29;
    uint32_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t local_sp_5;
    uint64_t r12_8;
    uint64_t local_sp_10;
    uint64_t var_49;
    uint64_t r12_4;
    uint64_t rbp_0;
    uint32_t var_35;
    uint64_t var_36;
    struct indirect_placeholder_26_ret_type var_37;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_26;
    struct indirect_placeholder_28_ret_type var_27;
    uint64_t var_19;
    struct indirect_placeholder_29_ret_type var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t r102_3;
    uint64_t local_sp_6;
    uint64_t r9_3;
    uint64_t r83_3;
    uint64_t r12_5;
    uint64_t var_38;
    uint64_t r12_6;
    uint64_t r102_4;
    uint64_t r9_4;
    uint64_t r83_4;
    uint32_t *var_18;
    uint64_t r102_5;
    uint64_t r83_6;
    uint64_t r9_5;
    uint64_t r83_5;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t r9_6;
    uint64_t var_39;
    uint32_t var_24;
    uint64_t var_25;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t local_sp_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t rdx_0;
    uint64_t r14_5;
    uint64_t rax_1;
    uint64_t local_sp_11;
    uint64_t var_50;
    unsigned char var_51;
    uint64_t local_sp_13;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t r9_7;
    uint64_t var_16;
    uint64_t var_17;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_rbp();
    var_4 = init_r12();
    var_5 = init_r14();
    var_6 = init_cc_src2();
    var_7 = init_r9();
    *(uint64_t *)(var_0 + (-8L)) = var_5;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_3;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    *(uint64_t *)(var_0 + (-48L)) = 4211801UL;
    indirect_placeholder_6(rsi, r10, r8);
    var_8 = var_0 + (-56L);
    *(uint64_t *)var_8 = 4211806UL;
    var_9 = indirect_placeholder_27();
    rdx_1 = 0UL;
    r102_7 = r10;
    local_sp_14 = var_8;
    r12_10 = 0UL;
    r83_7 = r8;
    r12_8 = 0UL;
    local_sp_10 = var_8;
    rbp_0 = 1UL;
    local_sp_12 = var_8;
    r14_5 = 0UL;
    r9_7 = var_7;
    if ((uint64_t)(uint32_t)var_9 == 0UL) {
        return rdx_1;
    }
    if (*(unsigned char *)4322053UL == '\x00') {
        *(unsigned char *)4322520UL = (unsigned char)'\x01';
    }
    *(unsigned char *)4322400UL = (unsigned char)'\x00';
    var_10 = (uint64_t)(uint32_t)((uint64_t)*(uint32_t *)4322512UL << (*(unsigned char *)4322424UL != '\x00'));
    var_11 = helper_cc_compute_all_wrapper(var_10, 0UL, 0UL, 24U);
    r14_6 = var_10;
    if ((uint64_t)(((unsigned char)(var_11 >> 4UL) ^ (unsigned char)var_11) & '\xc0') == 0UL) {
        var_12 = helper_cc_compute_all_wrapper(var_10, var_11, var_6, 1U);
        if ((var_12 & 64UL) != 0UL) {
            local_sp_13 = local_sp_12;
            if (*(unsigned char *)4322522UL != '\x00' & *(unsigned char *)4322521UL == '\x00') {
                var_54 = local_sp_12 + (-8L);
                *(uint64_t *)var_54 = 4212564UL;
                indirect_placeholder();
                *(unsigned char *)4322521UL = (unsigned char)'\x00';
                local_sp_13 = var_54;
            }
            var_55 = *(uint64_t *)4322456UL + 1UL;
            *(uint64_t *)4322456UL = var_55;
            if (var_55 <= *(uint64_t *)4322016UL) {
                *(uint64_t *)(local_sp_13 + (-8L)) = 4212581UL;
                indirect_placeholder_22(0UL);
                rdx_1 = 1UL;
            }
            return rdx_1;
        }
        var_13 = *(uint64_t *)4322568UL;
        var_14 = (uint64_t)*(uint32_t *)4322024UL;
        var_15 = helper_cc_compute_all_wrapper(var_14, 0UL, 0UL, 24U);
        rax_0 = var_13;
        rdx_0 = var_14;
        if ((uint64_t)(((unsigned char)(var_15 >> 4UL) ^ (unsigned char)var_15) & '\xc0') == 0UL) {
            *(unsigned char *)4322400UL = (unsigned char)'\x00';
            local_sp_13 = local_sp_12;
            if (*(unsigned char *)4322522UL != '\x00' & *(unsigned char *)4322521UL == '\x00') {
                var_54 = local_sp_12 + (-8L);
                *(uint64_t *)var_54 = 4212564UL;
                indirect_placeholder();
                *(unsigned char *)4322521UL = (unsigned char)'\x00';
                local_sp_13 = var_54;
            }
            var_55 = *(uint64_t *)4322456UL + 1UL;
            *(uint64_t *)4322456UL = var_55;
            if (var_55 <= *(uint64_t *)4322016UL) {
                *(uint64_t *)(local_sp_13 + (-8L)) = 4212581UL;
                indirect_placeholder_22(0UL);
                rdx_1 = 1UL;
            }
            return rdx_1;
        }
        var_49 = ((((rdx_0 << 6UL) + 274877906880UL) & 274877906880UL) + 64UL) + rax_0;
        rax_1 = rax_0;
        r12_9 = r12_8;
        local_sp_11 = local_sp_10;
        var_50 = rax_1 + 64UL;
        rax_1 = var_50;
        do {
            if (*(uint32_t *)(rax_1 + 16UL) == 0U) {
                *(unsigned char *)(rax_1 + 57UL) = (unsigned char)'\x01';
            }
            var_50 = rax_1 + 64UL;
            rax_1 = var_50;
        } while (var_49 != var_50);
    } else {
        var_16 = local_sp_14 + (-8L);
        *(uint64_t *)var_16 = 4212345UL;
        var_17 = indirect_placeholder_1(0UL);
        r14_4 = r14_6;
        r14_0 = r14_6;
        local_sp_8 = var_16;
        r12_3 = r12_10;
        r14_3 = r14_6;
        local_sp_3 = var_16;
        local_sp_7 = var_16;
        r12_5 = r12_10;
        r12_6 = r12_10;
        r102_4 = r102_7;
        r9_4 = r9_7;
        r83_4 = r83_7;
        r102_5 = r102_7;
        r9_5 = r9_7;
        r83_5 = r83_7;
        while ((uint64_t)(uint32_t)var_17 != 0UL)
            {
                *(uint32_t *)4322488UL = 0U;
                *(uint32_t *)4322496UL = 0U;
                *(uint32_t *)4322408UL = 0U;
                *(unsigned char *)4322400UL = (unsigned char)'\x00';
                *(unsigned char *)4322526UL = (unsigned char)'\x00';
                *(unsigned char *)4322525UL = (unsigned char)'\x01';
                if ((int)*(uint32_t *)4322024UL <= (int)0U) {
                    rbx_0 = *(uint64_t *)4322568UL;
                    while (1U)
                        {
                            *(uint32_t *)4322484UL = 0U;
                            var_18 = (uint32_t *)(rbx_0 + 48UL);
                            r102_2 = r102_4;
                            r9_2 = r9_4;
                            r83_2 = r83_4;
                            r12_4 = r12_5;
                            r102_3 = r102_4;
                            local_sp_6 = local_sp_7;
                            r9_3 = r9_4;
                            r83_3 = r83_4;
                            if ((int)*var_18 <= (int)0U) {
                                if (*(uint32_t *)(rbx_0 + 16UL) != 1U) {
                                    if (*(unsigned char *)4322527UL != '\x00') {
                                        if (*(unsigned char *)4322525UL == '\x00') {
                                            var_19 = local_sp_7 + (-8L);
                                            *(uint64_t *)var_19 = 4212293UL;
                                            var_20 = indirect_placeholder_29(rbx_0, r102_4, r9_4, r83_4);
                                            var_21 = var_20.field_0;
                                            var_22 = var_20.field_1;
                                            var_23 = var_20.field_2;
                                            r102_3 = var_21;
                                            local_sp_6 = var_19;
                                            r9_3 = var_22;
                                            r83_3 = var_23;
                                        } else {
                                            *(unsigned char *)4322526UL = (unsigned char)'\x01';
                                        }
                                    }
                                    r12_7 = r12_4;
                                    r102_6 = r102_3;
                                    local_sp_9 = local_sp_6;
                                    local_sp_8 = local_sp_6;
                                    local_sp_7 = local_sp_6;
                                    r12_5 = r12_4;
                                    r12_6 = r12_4;
                                    r102_4 = r102_3;
                                    r9_4 = r9_3;
                                    r83_4 = r83_3;
                                    r102_5 = r102_3;
                                    r83_6 = r83_3;
                                    r9_5 = r9_3;
                                    r83_5 = r83_3;
                                    r9_6 = r9_3;
                                    if (*(unsigned char *)4322416UL == '\x00') {
                                        *(uint32_t *)4322408UL = (*(uint32_t *)4322408UL + 1U);
                                    }
                                    var_38 = rbp_0 + 1UL;
                                    if ((long)(uint64_t)((long)(var_38 << 32UL) >> (long)32UL) <= (long)(uint64_t)*(uint32_t *)4322024UL) {
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    rbx_0 = rbx_0 + 64UL;
                                    rbp_0 = (uint64_t)(uint32_t)var_38;
                                    continue;
                                }
                            }
                            *(unsigned char *)4322524UL = (unsigned char)'\x00';
                            var_24 = *(uint32_t *)(rbx_0 + 52UL);
                            *(uint32_t *)4322404UL = var_24;
                            var_25 = local_sp_7 + (-8L);
                            *(uint64_t *)var_25 = 4212032UL;
                            indirect_placeholder();
                            local_sp_4 = var_25;
                            if ((uint32_t)(unsigned char)var_24 == 0U) {
                                var_26 = local_sp_7 + (-16L);
                                *(uint64_t *)var_26 = 4212109UL;
                                var_27 = indirect_placeholder_28(rbx_0);
                                r102_2 = var_27.field_1;
                                local_sp_4 = var_26;
                                r9_2 = var_27.field_2;
                                r83_2 = var_27.field_3;
                            }
                            var_28 = *(unsigned char *)4322400UL;
                            var_29 = (uint64_t)(uint32_t)r12_5 | (uint64_t)var_28;
                            var_30 = *var_18 + (-1);
                            var_31 = (uint64_t)var_30;
                            *var_18 = var_30;
                            var_32 = helper_cc_compute_all_wrapper(var_31, 0UL, 0UL, 24U);
                            r12_7 = var_29;
                            r102_6 = r102_2;
                            r12_0 = var_29;
                            r102_0 = r102_2;
                            r9_0 = r9_2;
                            r83_0 = r83_2;
                            local_sp_5 = local_sp_4;
                            r12_4 = var_29;
                            r102_3 = r102_2;
                            r9_3 = r9_2;
                            r83_3 = r83_2;
                            r83_6 = r83_2;
                            r9_6 = r9_2;
                            if ((uint64_t)(((unsigned char)(var_32 >> 4UL) ^ (unsigned char)var_32) & '\xc0') != 0UL) {
                                var_33 = local_sp_4 + (-8L);
                                *(uint64_t *)var_33 = 4212116UL;
                                var_34 = indirect_placeholder_27();
                                local_sp_0 = var_33;
                                local_sp_5 = var_33;
                                local_sp_9 = var_33;
                                if ((uint64_t)(uint32_t)var_34 != 0UL) {
                                    loop_state_var = 0U;
                                    break;
                                }
                            }
                            local_sp_6 = local_sp_5;
                            var_35 = *(uint32_t *)(rbx_0 + 16UL);
                            if (*(unsigned char *)4322527UL != '\x00' & var_35 != 0U) {
                                if (*(unsigned char *)4322525UL == '\x00') {
                                    *(unsigned char *)4322526UL = (unsigned char)'\x01';
                                } else {
                                    if ((uint64_t)(var_35 + (-3)) == 0UL) {
                                        var_36 = local_sp_5 + (-8L);
                                        *(uint64_t *)var_36 = 4212280UL;
                                        var_37 = indirect_placeholder_26(rbx_0, r102_2, r9_2, r83_2);
                                        r102_3 = var_37.field_0;
                                        local_sp_6 = var_36;
                                        r9_3 = var_37.field_1;
                                        r83_3 = var_37.field_2;
                                    } else {
                                        if ((uint64_t)(var_35 + (-2)) != 0UL & *(unsigned char *)4322524UL == '\x00') {
                                            var_36 = local_sp_5 + (-8L);
                                            *(uint64_t *)var_36 = 4212280UL;
                                            var_37 = indirect_placeholder_26(rbx_0, r102_2, r9_2, r83_2);
                                            r102_3 = var_37.field_0;
                                            local_sp_6 = var_36;
                                            r9_3 = var_37.field_1;
                                            r83_3 = var_37.field_2;
                                        }
                                    }
                                }
                            }
                            r12_7 = r12_4;
                            r102_6 = r102_3;
                            local_sp_9 = local_sp_6;
                            local_sp_8 = local_sp_6;
                            local_sp_7 = local_sp_6;
                            r12_5 = r12_4;
                            r12_6 = r12_4;
                            r102_4 = r102_3;
                            r9_4 = r9_3;
                            r83_4 = r83_3;
                            r102_5 = r102_3;
                            r83_6 = r83_3;
                            r9_5 = r9_3;
                            r83_5 = r83_3;
                            r9_6 = r9_3;
                            if (*(unsigned char *)4322416UL == '\x00') {
                                *(uint32_t *)4322408UL = (*(uint32_t *)4322408UL + 1U);
                            }
                            var_38 = rbp_0 + 1UL;
                            if ((long)(uint64_t)((long)(var_38 << 32UL) >> (long)32UL) <= (long)(uint64_t)*(uint32_t *)4322024UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            rbx_0 = rbx_0 + 64UL;
                            rbp_0 = (uint64_t)(uint32_t)var_38;
                            continue;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 0U:
                        {
                            if (var_28 != '\x00') {
                                r9_1 = r9_0;
                                r83_1 = r83_0;
                                r12_1 = r12_0;
                                r14_1 = r14_0;
                                r102_1 = r102_0;
                                local_sp_1 = local_sp_0;
                                r12_3 = r12_0;
                                r14_3 = r14_0;
                                local_sp_3 = local_sp_0;
                                if (*(unsigned char *)4322053UL == '\x00') {
                                    switch_state_var = 1;
                                    break;
                                }
                                var_42 = r12_1 & (uint64_t)*(unsigned char *)4322424UL;
                                r102_7 = r102_1;
                                r12_2 = r12_1;
                                r14_2 = r14_1;
                                local_sp_2 = local_sp_1;
                                r83_7 = r83_1;
                                r9_7 = r9_1;
                                if (var_42 == 0UL) {
                                    var_43 = (uint64_t)((uint32_t)r12_1 & (-256)) | var_42;
                                    var_44 = local_sp_1 + (-8L);
                                    *(uint64_t *)var_44 = 4212459UL;
                                    indirect_placeholder();
                                    r12_2 = var_43;
                                    r14_2 = (uint64_t)((uint32_t)r14_1 + (-1));
                                    local_sp_2 = var_44;
                                }
                                var_45 = helper_cc_compute_all_wrapper(r14_2, 0UL, 0UL, 24U);
                                local_sp_14 = local_sp_2;
                                r14_6 = r14_2;
                                r12_10 = r12_2;
                                r12_3 = r12_2;
                                r14_3 = r14_2;
                                local_sp_3 = local_sp_2;
                                if ((uint64_t)(((unsigned char)(var_45 >> 4UL) ^ (unsigned char)var_45) & '\xc0') == 0UL) {
                                    switch_state_var = 1;
                                    break;
                                }
                                var_16 = local_sp_14 + (-8L);
                                *(uint64_t *)var_16 = 4212345UL;
                                var_17 = indirect_placeholder_1(0UL);
                                r14_4 = r14_6;
                                r14_0 = r14_6;
                                local_sp_8 = var_16;
                                r12_3 = r12_10;
                                r14_3 = r14_6;
                                local_sp_3 = var_16;
                                local_sp_7 = var_16;
                                r12_5 = r12_10;
                                r12_6 = r12_10;
                                r102_4 = r102_7;
                                r9_4 = r9_7;
                                r83_4 = r83_7;
                                r102_5 = r102_7;
                                r9_5 = r9_7;
                                r83_5 = r83_7;
                                continue;
                            }
                            var_39 = local_sp_9 + (-8L);
                            *(uint64_t *)var_39 = 4212440UL;
                            indirect_placeholder();
                            r14_4 = (uint64_t)((uint32_t)r14_6 + (-1));
                            local_sp_8 = var_39;
                            r12_6 = r12_7;
                            r102_5 = r102_6;
                            r9_5 = r9_6;
                            r83_5 = r83_6;
                        }
                        break;
                      case 1U:
                        {
                            if (*(unsigned char *)4322400UL == '\x00') {
                                var_39 = local_sp_9 + (-8L);
                                *(uint64_t *)var_39 = 4212440UL;
                                indirect_placeholder();
                                r14_4 = (uint64_t)((uint32_t)r14_6 + (-1));
                                local_sp_8 = var_39;
                                r12_6 = r12_7;
                                r102_5 = r102_6;
                                r9_5 = r9_6;
                                r83_5 = r83_6;
                            }
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
                var_40 = local_sp_8 + (-8L);
                *(uint64_t *)var_40 = 4212312UL;
                var_41 = indirect_placeholder_27();
                r9_1 = r9_5;
                r83_1 = r83_5;
                r12_0 = r12_6;
                r14_0 = r14_4;
                r102_0 = r102_5;
                local_sp_0 = var_40;
                r9_0 = r9_5;
                r83_0 = r83_5;
                r12_1 = r12_6;
                r14_1 = r14_4;
                r102_1 = r102_5;
                local_sp_1 = var_40;
                if ((uint64_t)(uint32_t)var_41 != 0UL) {
                    r9_1 = r9_0;
                    r83_1 = r83_0;
                    r12_1 = r12_0;
                    r14_1 = r14_0;
                    r102_1 = r102_0;
                    local_sp_1 = local_sp_0;
                    r12_3 = r12_0;
                    r14_3 = r14_0;
                    local_sp_3 = local_sp_0;
                    if (*(unsigned char *)4322053UL == '\x00') {
                        break;
                    }
                }
                var_42 = r12_1 & (uint64_t)*(unsigned char *)4322424UL;
                r102_7 = r102_1;
                r12_2 = r12_1;
                r14_2 = r14_1;
                local_sp_2 = local_sp_1;
                r83_7 = r83_1;
                r9_7 = r9_1;
                if (var_42 == 0UL) {
                    var_43 = (uint64_t)((uint32_t)r12_1 & (-256)) | var_42;
                    var_44 = local_sp_1 + (-8L);
                    *(uint64_t *)var_44 = 4212459UL;
                    indirect_placeholder();
                    r12_2 = var_43;
                    r14_2 = (uint64_t)((uint32_t)r14_1 + (-1));
                    local_sp_2 = var_44;
                }
                var_45 = helper_cc_compute_all_wrapper(r14_2, 0UL, 0UL, 24U);
                local_sp_14 = local_sp_2;
                r14_6 = r14_2;
                r12_10 = r12_2;
                r12_3 = r12_2;
                r14_3 = r14_2;
                local_sp_3 = local_sp_2;
                if ((uint64_t)(((unsigned char)(var_45 >> 4UL) ^ (unsigned char)var_45) & '\xc0') == 0UL) {
                    break;
                }
                var_16 = local_sp_14 + (-8L);
                *(uint64_t *)var_16 = 4212345UL;
                var_17 = indirect_placeholder_1(0UL);
                r14_4 = r14_6;
                r14_0 = r14_6;
                local_sp_8 = var_16;
                r12_3 = r12_10;
                r14_3 = r14_6;
                local_sp_3 = var_16;
                local_sp_7 = var_16;
                r12_5 = r12_10;
                r12_6 = r12_10;
                r102_4 = r102_7;
                r9_4 = r9_7;
                r83_4 = r83_7;
                r102_5 = r102_7;
                r9_5 = r9_7;
                r83_5 = r83_7;
            }
        r12_9 = r12_3;
        r12_8 = r12_3;
        local_sp_10 = local_sp_3;
        r14_5 = r14_3;
        local_sp_11 = local_sp_3;
        var_46 = *(uint64_t *)4322568UL;
        var_47 = (uint64_t)*(uint32_t *)4322024UL;
        var_48 = helper_cc_compute_all_wrapper(var_47, 0UL, 0UL, 24U);
        rax_0 = var_46;
        rdx_0 = var_47;
        if ((uint64_t)(uint32_t)r14_3 != 0UL & (uint64_t)(((unsigned char)(var_48 >> 4UL) ^ (unsigned char)var_48) & '\xc0') != 0UL) {
            var_49 = ((((rdx_0 << 6UL) + 274877906880UL) & 274877906880UL) + 64UL) + rax_0;
            rax_1 = rax_0;
            r12_9 = r12_8;
            local_sp_11 = local_sp_10;
            var_50 = rax_1 + 64UL;
            rax_1 = var_50;
            do {
                if (*(uint32_t *)(rax_1 + 16UL) == 0U) {
                    *(unsigned char *)(rax_1 + 57UL) = (unsigned char)'\x01';
                }
                var_50 = rax_1 + 64UL;
                rax_1 = var_50;
            } while (var_49 != var_50);
        }
    }
    var_51 = (unsigned char)r12_9;
    *(unsigned char *)4322400UL = var_51;
    local_sp_12 = local_sp_11;
    if ((uint64_t)var_51 == 0UL) {
        if (*(unsigned char *)4322053UL == '\x00') {
            var_52 = (uint64_t)((uint32_t)r14_5 + 5U);
            var_53 = local_sp_11 + (-8L);
            *(uint64_t *)var_53 = 4212549UL;
            indirect_placeholder_22(var_52);
            local_sp_13 = var_53;
        } else {
            local_sp_13 = local_sp_12;
            if (*(unsigned char *)4322522UL != '\x00' & *(unsigned char *)4322521UL == '\x00') {
                var_54 = local_sp_12 + (-8L);
                *(uint64_t *)var_54 = 4212564UL;
                indirect_placeholder();
                *(unsigned char *)4322521UL = (unsigned char)'\x00';
                local_sp_13 = var_54;
            }
        }
    } else {
        local_sp_13 = local_sp_12;
        if (*(unsigned char *)4322522UL != '\x00' & *(unsigned char *)4322521UL == '\x00') {
            var_54 = local_sp_12 + (-8L);
            *(uint64_t *)var_54 = 4212564UL;
            indirect_placeholder();
            *(unsigned char *)4322521UL = (unsigned char)'\x00';
            local_sp_13 = var_54;
        }
    }
    var_55 = *(uint64_t *)4322456UL + 1UL;
    *(uint64_t *)4322456UL = var_55;
    if (var_55 <= *(uint64_t *)4322016UL) {
        return rdx_1;
    }
    *(uint64_t *)(local_sp_13 + (-8L)) = 4212581UL;
    indirect_placeholder_22(0UL);
    rdx_1 = 1UL;
    return rdx_1;
}
