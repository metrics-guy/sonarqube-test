typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_read_rest_of_line_ret_type;
struct bb_read_rest_of_line_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r10(void);
extern uint64_t init_r9(void);
extern uint64_t init_rcx(void);
extern uint64_t init_r8(void);
extern uint64_t init_rax(void);
extern void indirect_placeholder_2(uint64_t param_0, uint64_t param_1);
extern uint64_t init_rsi(void);
struct bb_read_rest_of_line_ret_type bb_read_rest_of_line(uint64_t rdi) {
    struct bb_read_rest_of_line_ret_type mrv1;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint32_t var_11;
    bool var_12;
    bool var_13;
    bool var_14;
    uint64_t local_sp_1;
    uint64_t rsi_1;
    uint64_t var_16;
    struct bb_read_rest_of_line_ret_type mrv;
    struct bb_read_rest_of_line_ret_type mrv2;
    struct bb_read_rest_of_line_ret_type mrv3;
    uint64_t var_15;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbx();
    var_3 = init_rbp();
    var_4 = init_rsi();
    var_5 = init_rcx();
    var_6 = init_r10();
    var_7 = init_r9();
    var_8 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_9 = var_0 + (-24L);
    var_10 = *(uint64_t *)rdi;
    var_11 = (uint32_t)var_1;
    var_12 = ((uint64_t)(var_11 + (-10)) == 0UL);
    var_13 = ((uint64_t)(var_11 + (-12)) == 0UL);
    var_14 = ((uint64_t)(var_11 + 1U) == 0UL);
    rsi_1 = var_4;
    local_sp_1 = var_9;
    while (1U)
        {
            var_15 = local_sp_1 + (-8L);
            *(uint64_t *)var_15 = 4209197UL;
            indirect_placeholder();
            local_sp_1 = var_15;
            if (!var_12) {
                loop_state_var = 0U;
                break;
            }
            rsi_1 = var_10;
            if (!var_13) {
                *(uint64_t *)(local_sp_1 + (-16L)) = 4209230UL;
                indirect_placeholder();
                var_16 = local_sp_1 + (-24L);
                *(uint64_t *)var_16 = 4209276UL;
                indirect_placeholder();
                if (*(unsigned char *)4322522UL != '\x00') {
                    loop_state_var = 1U;
                    break;
                }
                *(unsigned char *)4322521UL = (unsigned char)'\x01';
                loop_state_var = 1U;
                break;
            }
            rsi_1 = var_4;
            if (var_14) {
                continue;
            }
            *(uint64_t *)(local_sp_1 + (-16L)) = 4209220UL;
            indirect_placeholder_2(var_10, rdi);
            loop_state_var = 0U;
            break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            mrv.field_0 = rsi_1;
            mrv1 = mrv;
            mrv1.field_1 = var_6;
            mrv2 = mrv1;
            mrv2.field_2 = var_7;
            mrv3 = mrv2;
            mrv3.field_3 = var_8;
            return mrv3;
        }
        break;
      case 1U:
        {
            *(uint64_t *)(var_16 + (-8L)) = 4209259UL;
            indirect_placeholder_2(rdi, var_5);
        }
        break;
    }
}
