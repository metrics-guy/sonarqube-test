typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_73_ret_type;
struct indirect_placeholder_73_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r9(void);
extern uint64_t init_rax(void);
extern uint64_t init_r8(void);
extern uint64_t init_rsi(void);
extern struct indirect_placeholder_73_ret_type indirect_placeholder_73(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
typedef _Bool bool;
void bb_squeeze_filter(uint64_t rdx, uint64_t rdi) {
    uint64_t r9_5;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t rbp_8;
    uint64_t r12_6;
    uint64_t local_sp_7;
    uint64_t r9_6;
    uint64_t r8_6;
    uint64_t r12_7;
    uint64_t var_43;
    uint64_t var_44;
    struct indirect_placeholder_73_ret_type var_31;
    uint64_t local_sp_8;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t rbx_0;
    uint64_t rbp_5;
    uint64_t rax_3;
    uint64_t r13_5;
    uint64_t rax_2_be;
    uint64_t rbp_9;
    uint64_t rbp_4;
    uint64_t r12_4;
    uint64_t rbp_3_be;
    uint64_t r12_8;
    uint64_t r12_5;
    uint64_t rbp_1;
    uint64_t local_sp_5;
    uint64_t r12_3_be;
    uint64_t r9_4;
    uint64_t r9_7;
    uint64_t local_sp_3_be;
    uint64_t r8_7;
    uint64_t r8_4;
    uint64_t r9_3_be;
    uint64_t r8_3_be;
    uint64_t r9_8;
    uint64_t r13_1;
    uint64_t rsi_0;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t rbp_7;
    uint64_t rbp_10;
    uint64_t r9_3;
    uint64_t r8_3;
    uint64_t local_sp_9;
    uint64_t local_sp_6;
    uint64_t r12_1;
    uint64_t local_sp_1;
    uint64_t r8_8;
    uint64_t r8_5;
    uint64_t r9_1;
    uint64_t r8_1;
    uint64_t var_19;
    uint64_t var_20;
    unsigned char var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t rax_1;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t r13_3;
    uint64_t r13_2;
    uint64_t rbp_2;
    uint64_t r12_2;
    uint64_t local_sp_2;
    uint64_t r9_2;
    uint64_t r8_2;
    uint64_t var_34;
    uint64_t r13_4;
    uint64_t rax_2;
    uint64_t rbp_3;
    uint64_t r12_3;
    uint64_t local_sp_3;
    uint64_t var_13;
    uint64_t rbx_1;
    uint64_t var_14;
    uint64_t local_sp_4;
    uint64_t rbp_6;
    uint32_t var_38;
    uint32_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r15();
    var_5 = init_rbp();
    var_6 = init_r12();
    var_7 = init_r14();
    var_8 = init_rsi();
    var_9 = init_cc_src2();
    var_10 = init_r9();
    var_11 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_4;
    *(uint64_t *)(var_0 + (-16L)) = var_7;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_6;
    *(uint64_t *)(var_0 + (-40L)) = var_5;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_12 = var_0 + (-72L);
    *(uint64_t *)var_12 = var_8;
    rbx_0 = 0UL;
    rax_2_be = 0UL;
    r9_3 = var_10;
    r8_3 = var_11;
    rax_2 = var_1;
    rbp_3 = 0UL;
    r12_3 = 2147483647UL;
    local_sp_3 = var_12;
    loop_state_var = 3U;
    while (1U)
        {
            switch_state_var = 0;
            switch (loop_state_var) {
              case 2U:
                {
                    var_19 = helper_cc_compute_c_wrapper(r13_1 - rbx_1, rbx_1, var_9, 17U);
                    r12_7 = r12_1;
                    local_sp_8 = local_sp_1;
                    r13_5 = r13_1;
                    rbp_9 = rbp_1;
                    local_sp_5 = local_sp_1;
                    r9_4 = r9_1;
                    r9_7 = r9_1;
                    r8_7 = r8_1;
                    r8_4 = r8_1;
                    if (var_19 != 0UL) {
                        var_43 = rbx_1 - rbp_9;
                        rbp_3_be = r13_5;
                        r12_3_be = r12_7;
                        local_sp_3_be = local_sp_8;
                        r9_3_be = r9_7;
                        r8_3_be = r8_7;
                        if (var_43 == 0UL) {
                            var_44 = local_sp_8 + (-8L);
                            *(uint64_t *)var_44 = 4205285UL;
                            indirect_placeholder();
                            rax_2_be = var_43;
                            local_sp_3_be = var_44;
                        }
                        r9_3 = r9_3_be;
                        r8_3 = r8_3_be;
                        rax_2 = rax_2_be;
                        rbp_3 = rbp_3_be;
                        r12_3 = r12_3_be;
                        local_sp_3 = local_sp_3_be;
                        loop_state_var = 1U;
                        continue;
                    }
                    var_20 = r13_1 + rdi;
                    var_21 = *(unsigned char *)var_20;
                    var_22 = (uint64_t)(uint32_t)(uint64_t)var_21;
                    var_23 = r13_1 - rbp_1;
                    var_24 = var_23 + 1UL;
                    rax_1 = var_23;
                    rsi_0 = var_24;
                    r12_2 = var_22;
                    r12_4 = var_22;
                    if (r13_1 == 0UL) {
                        var_25 = ((uint64_t)(var_21 - *(unsigned char *)(var_20 + (-1L))) == 0UL) ? var_23 : var_24;
                        rax_1 = var_25;
                        rsi_0 = var_25;
                    }
                    var_26 = r13_1 + 1UL;
                    r13_2 = var_26;
                    rbp_2 = rsi_0;
                    rax_3 = rax_1;
                    rbp_5 = var_26;
                    if (rsi_0 != 0UL) {
                        rax_2_be = rax_3;
                        rbp_3_be = rbp_5;
                        r12_3_be = r12_4;
                        local_sp_3_be = local_sp_5;
                        r9_3_be = r9_4;
                        r8_3_be = r8_4;
                        rbp_6 = rbp_5;
                        if (rbx_1 <= rbp_5) {
                            while (1U)
                                {
                                    var_38 = (uint32_t)(uint64_t)*(unsigned char *)(rbp_6 + rdi);
                                    var_39 = (uint32_t)r12_4;
                                    rax_2_be = 2147483647UL;
                                    rbp_3_be = rbx_1;
                                    if ((uint64_t)(var_38 - var_39) != 0UL) {
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    var_41 = rbp_6 + 1UL;
                                    rbp_6 = var_41;
                                    if (rbx_1 == var_41) {
                                        continue;
                                    }
                                    loop_state_var = 0U;
                                    break;
                                }
                            switch (loop_state_var) {
                              case 0U:
                                {
                                    var_42 = (uint64_t)var_38;
                                    rax_2_be = var_42;
                                }
                                break;
                              case 1U:
                                {
                                    var_40 = (rbx_1 > rbp_6) ? 2147483647UL : (uint64_t)var_39;
                                    rbp_3_be = rbp_6;
                                    r12_3_be = var_40;
                                }
                                break;
                            }
                        }
                        r9_3 = r9_3_be;
                        r8_3 = r8_3_be;
                        rax_2 = rax_2_be;
                        rbp_3 = rbp_3_be;
                        r12_3 = r12_3_be;
                        local_sp_3 = local_sp_3_be;
                        loop_state_var = 1U;
                        continue;
                    }
                    var_27 = *(uint64_t *)4288064UL;
                    var_28 = local_sp_1 + (-8L);
                    *(uint64_t *)var_28 = 4205049UL;
                    indirect_placeholder();
                    local_sp_5 = var_28;
                    if (rsi_0 != rax_1) {
                        rax_2_be = rax_3;
                        rbp_3_be = rbp_5;
                        r12_3_be = r12_4;
                        local_sp_3_be = local_sp_5;
                        r9_3_be = r9_4;
                        r8_3_be = r8_4;
                        rbp_6 = rbp_5;
                        if (rbx_1 <= rbp_5) {
                            while (1U)
                                {
                                    var_38 = (uint32_t)(uint64_t)*(unsigned char *)(rbp_6 + rdi);
                                    var_39 = (uint32_t)r12_4;
                                    rax_2_be = 2147483647UL;
                                    rbp_3_be = rbx_1;
                                    if ((uint64_t)(var_38 - var_39) != 0UL) {
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    var_41 = rbp_6 + 1UL;
                                    rbp_6 = var_41;
                                    if (rbx_1 == var_41) {
                                        continue;
                                    }
                                    loop_state_var = 0U;
                                    break;
                                }
                            switch (loop_state_var) {
                              case 0U:
                                {
                                    var_42 = (uint64_t)var_38;
                                    rax_2_be = var_42;
                                }
                                break;
                              case 1U:
                                {
                                    var_40 = (rbx_1 > rbp_6) ? 2147483647UL : (uint64_t)var_39;
                                    rbp_3_be = rbp_6;
                                    r12_3_be = var_40;
                                }
                                break;
                            }
                        }
                        r9_3 = r9_3_be;
                        r8_3 = r8_3_be;
                        rax_2 = rax_2_be;
                        rbp_3 = rbp_3_be;
                        r12_3 = r12_3_be;
                        local_sp_3 = local_sp_3_be;
                        loop_state_var = 1U;
                        continue;
                    }
                    *(uint64_t *)(var_28 + (-8L)) = 4205063UL;
                    indirect_placeholder();
                    var_29 = (uint64_t)*(uint32_t *)rax_1;
                    var_30 = var_28 + (-16L);
                    *(uint64_t *)var_30 = 4205085UL;
                    var_31 = indirect_placeholder_73(0UL, 4259976UL, 1UL, var_29, var_27, r9_1, r8_1);
                    var_32 = var_31.field_2;
                    var_33 = var_31.field_3;
                    local_sp_2 = var_30;
                    r9_2 = var_32;
                    r8_2 = var_33;
                    while (1U)
                        {
                            var_34 = r13_2 + 2UL;
                            r9_5 = r9_2;
                            rbp_8 = rbp_2;
                            r12_6 = r12_2;
                            local_sp_7 = local_sp_2;
                            r9_6 = r9_2;
                            r8_6 = r8_2;
                            r12_5 = r12_2;
                            rbp_7 = rbp_2;
                            local_sp_6 = local_sp_2;
                            r8_5 = r8_2;
                            r13_3 = var_34;
                            r13_4 = var_34;
                            if (rbx_1 <= var_34) {
                                loop_state_var = 1U;
                                break;
                            }
                            var_35 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(r13_3 + rdi);
                            var_36 = local_sp_6 + (-8L);
                            *(uint64_t *)var_36 = 4204936UL;
                            var_37 = indirect_placeholder_1(var_35);
                            r12_8 = r12_5;
                            rbp_1 = rbp_7;
                            r9_8 = r9_5;
                            r13_1 = r13_3;
                            rbp_10 = rbp_7;
                            local_sp_9 = var_36;
                            r12_1 = r12_5;
                            local_sp_1 = var_36;
                            r8_8 = r8_5;
                            r9_1 = r9_5;
                            r8_1 = r8_5;
                            r13_2 = r13_3;
                            rbp_2 = rbp_7;
                            r12_2 = r12_5;
                            local_sp_2 = var_36;
                            r9_2 = r9_5;
                            r8_2 = r8_5;
                            if (*(unsigned char *)((uint64_t)(unsigned char)var_37 + 4289920UL) == '\x00') {
                                continue;
                            }
                            loop_state_var = 0U;
                            break;
                        }
                    switch (loop_state_var) {
                      case 0U:
                        {
                            if (rbx_1 != r13_3) {
                                loop_state_var = 2U;
                                continue;
                            }
                            loop_state_var = 3U;
                            continue;
                        }
                        break;
                      case 1U:
                        {
                            loop_state_var = 0U;
                            continue;
                        }
                        break;
                    }
                }
                break;
              case 0U:
                {
                    r12_7 = r12_6;
                    local_sp_8 = local_sp_7;
                    r13_5 = r13_4;
                    rbp_9 = rbp_8;
                    r12_8 = r12_6;
                    r9_7 = r9_6;
                    r8_7 = r8_6;
                    r9_8 = r9_6;
                    rbp_10 = rbp_8;
                    local_sp_9 = local_sp_7;
                    r8_8 = r8_6;
                    if (rbx_1 != r13_4) {
                        var_43 = rbx_1 - rbp_9;
                        rbp_3_be = r13_5;
                        r12_3_be = r12_7;
                        local_sp_3_be = local_sp_8;
                        r9_3_be = r9_7;
                        r8_3_be = r8_7;
                        if (var_43 == 0UL) {
                            var_44 = local_sp_8 + (-8L);
                            *(uint64_t *)var_44 = 4205285UL;
                            indirect_placeholder();
                            rax_2_be = var_43;
                            local_sp_3_be = var_44;
                        }
                        r9_3 = r9_3_be;
                        r8_3 = r8_3_be;
                        rax_2 = rax_2_be;
                        rbp_3 = rbp_3_be;
                        r12_3 = r12_3_be;
                        local_sp_3 = local_sp_3_be;
                        loop_state_var = 1U;
                        continue;
                    }
                    *(uint64_t *)(local_sp_9 + 8UL) = (rbx_1 + (-1L));
                    var_16 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)((rbx_1 + rdi) + (-1L));
                    var_17 = local_sp_9 + (-8L);
                    *(uint64_t *)var_17 = 4205212UL;
                    var_18 = indirect_placeholder_1(var_16);
                    r12_7 = r12_8;
                    local_sp_8 = var_17;
                    rbp_9 = rbp_10;
                    rbp_1 = rbp_10;
                    r9_7 = r9_8;
                    r8_7 = r8_8;
                    r12_1 = r12_8;
                    local_sp_1 = var_17;
                    r9_1 = r9_8;
                    r8_1 = r8_8;
                    if (*(unsigned char *)((uint64_t)(unsigned char)var_18 + 4289920UL) != '\x00') {
                        var_43 = rbx_1 - rbp_9;
                        rbp_3_be = r13_5;
                        r12_3_be = r12_7;
                        local_sp_3_be = local_sp_8;
                        r9_3_be = r9_7;
                        r8_3_be = r8_7;
                        if (var_43 == 0UL) {
                            var_44 = local_sp_8 + (-8L);
                            *(uint64_t *)var_44 = 4205285UL;
                            indirect_placeholder();
                            rax_2_be = var_43;
                            local_sp_3_be = var_44;
                        }
                        r9_3 = r9_3_be;
                        r8_3 = r8_3_be;
                        rax_2 = rax_2_be;
                        rbp_3 = rbp_3_be;
                        r12_3 = r12_3_be;
                        local_sp_3 = local_sp_3_be;
                        loop_state_var = 1U;
                        continue;
                    }
                    r13_1 = *(uint64_t *)local_sp_9;
                }
                break;
              case 1U:
                {
                    var_13 = helper_cc_compute_c_wrapper(rbp_3 - rbx_0, rbx_0, var_9, 17U);
                    r9_5 = r9_3;
                    r12_6 = r12_3;
                    r9_6 = r9_3;
                    r8_6 = r8_3;
                    rax_3 = rax_2;
                    rbp_4 = rbp_3;
                    r12_4 = r12_3;
                    r12_5 = r12_3;
                    r9_4 = r9_3;
                    r8_4 = r8_3;
                    r8_5 = r8_3;
                    rbx_1 = rbx_0;
                    local_sp_4 = local_sp_3;
                    if (var_13 != 0UL) {
                        var_14 = local_sp_3 + (-8L);
                        *(uint64_t *)var_14 = 4205133UL;
                        indirect_placeholder();
                        rbx_1 = rax_2;
                        rbp_4 = 0UL;
                        local_sp_4 = var_14;
                        if (rax_2 == 0UL) {
                            switch_state_var = 1;
                            break;
                        }
                    }
                    rbp_8 = rbp_4;
                    local_sp_7 = local_sp_4;
                    rbx_0 = rbx_1;
                    rbp_5 = rbp_4;
                    r13_5 = rbx_1;
                    local_sp_5 = local_sp_4;
                    rbp_7 = rbp_4;
                    local_sp_6 = local_sp_4;
                    r13_3 = rbp_4;
                    r13_4 = rbp_4;
                    if ((uint64_t)((uint32_t)r12_3 + (-2147483647)) != 0UL) {
                        rax_2_be = rax_3;
                        rbp_3_be = rbp_5;
                        r12_3_be = r12_4;
                        local_sp_3_be = local_sp_5;
                        r9_3_be = r9_4;
                        r8_3_be = r8_4;
                        rbp_6 = rbp_5;
                        if (rbx_1 <= rbp_5) {
                            while (1U)
                                {
                                    var_38 = (uint32_t)(uint64_t)*(unsigned char *)(rbp_6 + rdi);
                                    var_39 = (uint32_t)r12_4;
                                    rax_2_be = 2147483647UL;
                                    rbp_3_be = rbx_1;
                                    if ((uint64_t)(var_38 - var_39) != 0UL) {
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    var_41 = rbp_6 + 1UL;
                                    rbp_6 = var_41;
                                    if (rbx_1 == var_41) {
                                        continue;
                                    }
                                    loop_state_var = 0U;
                                    break;
                                }
                            switch (loop_state_var) {
                              case 0U:
                                {
                                    var_42 = (uint64_t)var_38;
                                    rax_2_be = var_42;
                                }
                                break;
                              case 1U:
                                {
                                    var_40 = (rbx_1 > rbp_6) ? 2147483647UL : (uint64_t)var_39;
                                    rbp_3_be = rbp_6;
                                    r12_3_be = var_40;
                                }
                                break;
                            }
                        }
                        r9_3 = r9_3_be;
                        r8_3 = r8_3_be;
                        rax_2 = rax_2_be;
                        rbp_3 = rbp_3_be;
                        r12_3 = r12_3_be;
                        local_sp_3 = local_sp_3_be;
                        loop_state_var = 1U;
                        continue;
                    }
                    var_15 = helper_cc_compute_c_wrapper(rbp_4 - rbx_1, rbx_1, var_9, 17U);
                    if (var_15 == 0UL) {
                        var_35 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(r13_3 + rdi);
                        var_36 = local_sp_6 + (-8L);
                        *(uint64_t *)var_36 = 4204936UL;
                        var_37 = indirect_placeholder_1(var_35);
                        r12_8 = r12_5;
                        rbp_1 = rbp_7;
                        r9_8 = r9_5;
                        r13_1 = r13_3;
                        rbp_10 = rbp_7;
                        local_sp_9 = var_36;
                        r12_1 = r12_5;
                        local_sp_1 = var_36;
                        r8_8 = r8_5;
                        r9_1 = r9_5;
                        r8_1 = r8_5;
                        r13_2 = r13_3;
                        rbp_2 = rbp_7;
                        r12_2 = r12_5;
                        local_sp_2 = var_36;
                        r9_2 = r9_5;
                        r8_2 = r8_5;
                        if (*(unsigned char *)((uint64_t)(unsigned char)var_37 + 4289920UL) == '\x00') {
                            if (rbx_1 != r13_3) {
                                loop_state_var = 3U;
                                continue;
                            }
                            loop_state_var = 2U;
                            continue;
                        }
                        while (1U)
                            {
                                var_34 = r13_2 + 2UL;
                                r9_5 = r9_2;
                                rbp_8 = rbp_2;
                                r12_6 = r12_2;
                                local_sp_7 = local_sp_2;
                                r9_6 = r9_2;
                                r8_6 = r8_2;
                                r12_5 = r12_2;
                                rbp_7 = rbp_2;
                                local_sp_6 = local_sp_2;
                                r8_5 = r8_2;
                                r13_3 = var_34;
                                r13_4 = var_34;
                                if (rbx_1 <= var_34) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                var_35 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(r13_3 + rdi);
                                var_36 = local_sp_6 + (-8L);
                                *(uint64_t *)var_36 = 4204936UL;
                                var_37 = indirect_placeholder_1(var_35);
                                r12_8 = r12_5;
                                rbp_1 = rbp_7;
                                r9_8 = r9_5;
                                r13_1 = r13_3;
                                rbp_10 = rbp_7;
                                local_sp_9 = var_36;
                                r12_1 = r12_5;
                                local_sp_1 = var_36;
                                r8_8 = r8_5;
                                r9_1 = r9_5;
                                r8_1 = r8_5;
                                r13_2 = r13_3;
                                rbp_2 = rbp_7;
                                r12_2 = r12_5;
                                local_sp_2 = var_36;
                                r9_2 = r9_5;
                                r8_2 = r8_5;
                                if (*(unsigned char *)((uint64_t)(unsigned char)var_37 + 4289920UL) == '\x00') {
                                    continue;
                                }
                                loop_state_var = 0U;
                                break;
                            }
                        switch (loop_state_var) {
                          case 0U:
                            {
                                if (rbx_1 != r13_3) {
                                    loop_state_var = 3U;
                                    continue;
                                }
                                loop_state_var = 2U;
                                continue;
                            }
                            break;
                          case 1U:
                            {
                                loop_state_var = 0U;
                                continue;
                            }
                            break;
                        }
                    }
                    r12_7 = r12_6;
                    local_sp_8 = local_sp_7;
                    r13_5 = r13_4;
                    rbp_9 = rbp_8;
                    r12_8 = r12_6;
                    r9_7 = r9_6;
                    r8_7 = r8_6;
                    r9_8 = r9_6;
                    rbp_10 = rbp_8;
                    local_sp_9 = local_sp_7;
                    r8_8 = r8_6;
                    if (rbx_1 == r13_4) {
                        var_43 = rbx_1 - rbp_9;
                        rbp_3_be = r13_5;
                        r12_3_be = r12_7;
                        local_sp_3_be = local_sp_8;
                        r9_3_be = r9_7;
                        r8_3_be = r8_7;
                        if (var_43 == 0UL) {
                            var_44 = local_sp_8 + (-8L);
                            *(uint64_t *)var_44 = 4205285UL;
                            indirect_placeholder();
                            rax_2_be = var_43;
                            local_sp_3_be = var_44;
                        }
                        r9_3 = r9_3_be;
                        r8_3 = r8_3_be;
                        rax_2 = rax_2_be;
                        rbp_3 = rbp_3_be;
                        r12_3 = r12_3_be;
                        local_sp_3 = local_sp_3_be;
                        loop_state_var = 1U;
                        continue;
                    }
                    *(uint64_t *)(local_sp_9 + 8UL) = (rbx_1 + (-1L));
                    var_16 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)((rbx_1 + rdi) + (-1L));
                    var_17 = local_sp_9 + (-8L);
                    *(uint64_t *)var_17 = 4205212UL;
                    var_18 = indirect_placeholder_1(var_16);
                    r12_7 = r12_8;
                    local_sp_8 = var_17;
                    rbp_9 = rbp_10;
                    rbp_1 = rbp_10;
                    r9_7 = r9_8;
                    r8_7 = r8_8;
                    r12_1 = r12_8;
                    local_sp_1 = var_17;
                    r9_1 = r9_8;
                    r8_1 = r8_8;
                    if (*(unsigned char *)((uint64_t)(unsigned char)var_18 + 4289920UL) != '\x00') {
                        var_43 = rbx_1 - rbp_9;
                        rbp_3_be = r13_5;
                        r12_3_be = r12_7;
                        local_sp_3_be = local_sp_8;
                        r9_3_be = r9_7;
                        r8_3_be = r8_7;
                        if (var_43 == 0UL) {
                            var_44 = local_sp_8 + (-8L);
                            *(uint64_t *)var_44 = 4205285UL;
                            indirect_placeholder();
                            rax_2_be = var_43;
                            local_sp_3_be = var_44;
                        }
                        r9_3 = r9_3_be;
                        r8_3 = r8_3_be;
                        rax_2 = rax_2_be;
                        rbp_3 = rbp_3_be;
                        r12_3 = r12_3_be;
                        local_sp_3 = local_sp_3_be;
                        loop_state_var = 1U;
                        continue;
                    }
                    r13_1 = *(uint64_t *)local_sp_9;
                }
                break;
              case 3U:
                {
                    *(uint64_t *)(local_sp_9 + 8UL) = (rbx_1 + (-1L));
                    var_16 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)((rbx_1 + rdi) + (-1L));
                    var_17 = local_sp_9 + (-8L);
                    *(uint64_t *)var_17 = 4205212UL;
                    var_18 = indirect_placeholder_1(var_16);
                    r12_7 = r12_8;
                    local_sp_8 = var_17;
                    rbp_9 = rbp_10;
                    rbp_1 = rbp_10;
                    r9_7 = r9_8;
                    r8_7 = r8_8;
                    r12_1 = r12_8;
                    local_sp_1 = var_17;
                    r9_1 = r9_8;
                    r8_1 = r8_8;
                    if (*(unsigned char *)((uint64_t)(unsigned char)var_18 + 4289920UL) != '\x00') {
                        var_43 = rbx_1 - rbp_9;
                        rbp_3_be = r13_5;
                        r12_3_be = r12_7;
                        local_sp_3_be = local_sp_8;
                        r9_3_be = r9_7;
                        r8_3_be = r8_7;
                        if (var_43 == 0UL) {
                            var_44 = local_sp_8 + (-8L);
                            *(uint64_t *)var_44 = 4205285UL;
                            indirect_placeholder();
                            rax_2_be = var_43;
                            local_sp_3_be = var_44;
                        }
                        r9_3 = r9_3_be;
                        r8_3 = r8_3_be;
                        rax_2 = rax_2_be;
                        rbp_3 = rbp_3_be;
                        r12_3 = r12_3_be;
                        local_sp_3 = local_sp_3_be;
                        loop_state_var = 1U;
                        continue;
                    }
                    r13_1 = *(uint64_t *)local_sp_9;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    return;
}
