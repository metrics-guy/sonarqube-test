typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_validate_case_classes_ret_type;
struct indirect_placeholder_29_ret_type;
struct bb_validate_case_classes_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_29_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_15(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r9(void);
extern void indirect_placeholder_11(uint64_t param_0);
extern uint64_t init_r8(void);
extern uint64_t init_rcx(void);
extern struct indirect_placeholder_29_ret_type indirect_placeholder_29(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
struct bb_validate_case_classes_ret_type bb_validate_case_classes(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_29_ret_type var_30;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    unsigned char var_11;
    uint64_t var_12;
    uint64_t rcx_7;
    uint64_t var_37;
    uint64_t r13_0;
    uint64_t local_sp_2;
    uint64_t rcx_6;
    uint64_t r8_3;
    uint64_t r9_0;
    uint64_t local_sp_1;
    uint64_t rdx_0_in;
    uint64_t r9_1;
    uint64_t rcx_0;
    uint64_t var_38;
    uint64_t rcx_3;
    uint64_t rdx_1_in;
    uint64_t var_39;
    uint64_t rcx_1;
    uint64_t local_sp_4;
    uint64_t local_sp_0;
    uint64_t r9_3;
    uint64_t rcx_2;
    uint64_t r9_4;
    uint64_t r8_0;
    uint64_t r8_1;
    uint64_t *var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t rcx_4;
    uint64_t r9_2;
    uint64_t r8_2;
    uint64_t r14_0;
    uint64_t local_sp_3;
    uint64_t rcx_5;
    uint64_t var_40;
    uint64_t var_29;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t *var_13;
    uint64_t var_17;
    uint64_t var_14;
    uint64_t rax_0;
    uint64_t r15_0;
    uint32_t var_15;
    uint64_t r15_1;
    uint64_t *var_18;
    uint64_t *var_19;
    uint64_t *var_20;
    uint64_t *var_21;
    uint64_t *var_22;
    uint64_t *var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_16;
    uint64_t r8_4;
    struct bb_validate_case_classes_ret_type mrv;
    struct bb_validate_case_classes_ret_type mrv1;
    struct bb_validate_case_classes_ret_type mrv2;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r15();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_rcx();
    var_8 = init_r9();
    var_9 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_10 = var_0 + (-120L);
    var_11 = *(unsigned char *)(rsi + 49UL);
    var_12 = (uint64_t)var_11;
    rcx_7 = var_7;
    r13_0 = var_12;
    rcx_6 = var_7;
    r8_3 = var_9;
    local_sp_4 = var_10;
    r9_3 = var_8;
    r9_4 = var_8;
    r14_0 = var_12;
    rcx_5 = 4265392UL;
    var_14 = 0UL;
    rax_0 = 1UL;
    r15_0 = 0UL;
    r8_4 = var_9;
    if (var_11 == '\x00') {
        mrv.field_0 = rcx_7;
        mrv1 = mrv;
        mrv1.field_1 = r9_4;
        mrv2 = mrv1;
        mrv2.field_2 = r8_4;
        return mrv2;
    }
    var_13 = (uint64_t *)(var_0 + (-112L));
    *var_13 = 0UL;
    while (1U)
        {
            var_15 = (uint32_t)rax_0;
            rdx_0_in = r15_0;
            rdx_1_in = r15_0;
            var_17 = var_14;
            r15_1 = r15_0;
            if ((uint64_t)((var_15 + (-66)) & (-2)) <= 25UL) {
                var_16 = var_14 + 1UL;
                *var_13 = var_16;
                var_17 = var_16;
                var_14 = var_17;
                rax_0 = (uint64_t)(var_15 + 1U);
                r15_0 = r15_1;
                continue;
            }
            if ((uint64_t)((var_15 + (-98)) & (-2)) > 25UL) {
                if ((int)var_15 > (int)255U) {
                    break;
                }
            }
            r15_1 = r15_0 + 1UL;
        }
    var_18 = (uint64_t *)(rdi + 24UL);
    *(uint64_t *)(var_0 + (-104L)) = *var_18;
    var_19 = (uint64_t *)(rsi + 24UL);
    *(uint64_t *)(var_0 + (-80L)) = *var_19;
    var_20 = (uint64_t *)(rdi + 8UL);
    *(uint64_t *)(var_0 + (-96L)) = *var_20;
    var_21 = (uint64_t *)(rsi + 8UL);
    *(uint64_t *)(var_0 + (-88L)) = *var_21;
    var_22 = (uint64_t *)(rdi + 16UL);
    *var_22 = 18446744073709551614UL;
    var_23 = (uint64_t *)(rsi + 16UL);
    *var_23 = 18446744073709551614UL;
    while (1U)
        {
            var_24 = local_sp_4 + 56UL;
            *(uint64_t *)(local_sp_4 + (-8L)) = 4210318UL;
            var_25 = indirect_placeholder_15(rdi, var_24);
            var_26 = local_sp_4 + 52UL;
            var_27 = local_sp_4 + (-16L);
            *(uint64_t *)var_27 = 4210333UL;
            var_28 = indirect_placeholder_15(rsi, var_26);
            *(uint32_t *)(local_sp_4 + (-12L)) = (uint32_t)var_28;
            local_sp_2 = var_27;
            r9_0 = r9_3;
            local_sp_1 = var_27;
            r9_1 = r9_3;
            rcx_3 = rcx_6;
            local_sp_0 = var_27;
            rcx_2 = rcx_6;
            r8_0 = r8_3;
            r8_1 = r8_3;
            rcx_4 = rcx_6;
            r9_2 = r9_3;
            r8_2 = r8_3;
            if ((uint64_t)(unsigned char)r13_0 != 0UL) {
                local_sp_2 = local_sp_0;
                local_sp_1 = local_sp_0;
                r9_1 = r9_0;
                rcx_3 = rcx_2;
                r8_1 = r8_0;
                rcx_4 = rcx_2;
                r9_2 = r9_0;
                r8_2 = r8_0;
                if (*(uint32_t *)(local_sp_0 + 60UL) != 2U) {
                    r13_0 = (r13_0 & (-256L)) | (*var_23 == 18446744073709551615UL);
                    rcx_6 = rcx_4;
                    r8_3 = r8_2;
                    local_sp_4 = local_sp_2;
                    r9_3 = r9_2;
                    r9_4 = r9_2;
                    r14_0 = (r14_0 & (-256L)) | (*var_22 == 18446744073709551615UL);
                    local_sp_3 = local_sp_2;
                    r8_4 = r8_2;
                    if ((uint64_t)((uint32_t)var_25 + 1U) == 0UL) {
                        break;
                    }
                    if (*(uint32_t *)(local_sp_2 + 4UL) != 4294967295U) {
                        continue;
                    }
                    break;
                }
                var_34 = (uint64_t *)(local_sp_1 + (-8L));
                *var_34 = 4210377UL;
                indirect_placeholder_11(rdi);
                var_35 = local_sp_1 + (-16L);
                *(uint64_t *)var_35 = 4210385UL;
                indirect_placeholder_11(rsi);
                var_36 = *var_18;
                rcx_0 = rcx_3;
                local_sp_2 = var_35;
                r9_2 = r9_1;
                r8_2 = r8_1;
                if (*(uint32_t *)(local_sp_1 + 40UL) == 1U) {
                    var_37 = *var_34;
                    rdx_0_in = var_37;
                    rcx_0 = var_37;
                }
                *var_18 = (var_36 - (rdx_0_in + (-1L)));
                var_38 = *var_19;
                rcx_1 = rcx_0;
                if (*(uint32_t *)(local_sp_1 + 44UL) == 1U) {
                    var_39 = *var_34;
                    rdx_1_in = var_39;
                    rcx_1 = var_39;
                }
                *var_19 = (var_38 - (rdx_1_in + (-1L)));
                rcx_4 = rcx_1;
                r13_0 = (r13_0 & (-256L)) | (*var_23 == 18446744073709551615UL);
                rcx_6 = rcx_4;
                r8_3 = r8_2;
                local_sp_4 = local_sp_2;
                r9_3 = r9_2;
                r9_4 = r9_2;
                r14_0 = (r14_0 & (-256L)) | (*var_22 == 18446744073709551615UL);
                local_sp_3 = local_sp_2;
                r8_4 = r8_2;
                if ((uint64_t)((uint32_t)var_25 + 1U) == 0UL) {
                    break;
                }
                if (*(uint32_t *)(local_sp_2 + 4UL) != 4294967295U) {
                    continue;
                }
                break;
            }
            if (*(uint32_t *)(local_sp_4 + 44UL) != 2U) {
                r13_0 = (r13_0 & (-256L)) | (*var_23 == 18446744073709551615UL);
                rcx_6 = rcx_4;
                r8_3 = r8_2;
                local_sp_4 = local_sp_2;
                r9_3 = r9_2;
                r9_4 = r9_2;
                r14_0 = (r14_0 & (-256L)) | (*var_22 == 18446744073709551615UL);
                local_sp_3 = local_sp_2;
                r8_4 = r8_2;
                if ((uint64_t)((uint32_t)var_25 + 1U) == 0UL) {
                    break;
                }
                if (*(uint32_t *)(local_sp_2 + 4UL) != 4294967295U) {
                    continue;
                }
                break;
            }
            if ((uint64_t)(unsigned char)r14_0 != 0UL) {
                var_29 = local_sp_4 + (-24L);
                *(uint64_t *)var_29 = 4210236UL;
                var_30 = indirect_placeholder_29(0UL, 4261336UL, 1UL, 0UL, rcx_6, r9_3, r8_3);
                var_31 = var_30.field_1;
                var_32 = var_30.field_2;
                var_33 = var_30.field_3;
                local_sp_0 = var_29;
                rcx_2 = var_31;
                r9_0 = var_32;
                r8_0 = var_33;
                local_sp_2 = local_sp_0;
                local_sp_1 = local_sp_0;
                r9_1 = r9_0;
                rcx_3 = rcx_2;
                r8_1 = r8_0;
                rcx_4 = rcx_2;
                r9_2 = r9_0;
                r8_2 = r8_0;
                if (*(uint32_t *)(local_sp_0 + 60UL) != 2U) {
                    r13_0 = (r13_0 & (-256L)) | (*var_23 == 18446744073709551615UL);
                    rcx_6 = rcx_4;
                    r8_3 = r8_2;
                    local_sp_4 = local_sp_2;
                    r9_3 = r9_2;
                    r9_4 = r9_2;
                    r14_0 = (r14_0 & (-256L)) | (*var_22 == 18446744073709551615UL);
                    local_sp_3 = local_sp_2;
                    r8_4 = r8_2;
                    if ((uint64_t)((uint32_t)var_25 + 1U) == 0UL) {
                        break;
                    }
                    if (*(uint32_t *)(local_sp_2 + 4UL) != 4294967295U) {
                        continue;
                    }
                    break;
                }
            }
            if (*(uint32_t *)(local_sp_4 + 40UL) != 2U) {
                var_29 = local_sp_4 + (-24L);
                *(uint64_t *)var_29 = 4210236UL;
                var_30 = indirect_placeholder_29(0UL, 4261336UL, 1UL, 0UL, rcx_6, r9_3, r8_3);
                var_31 = var_30.field_1;
                var_32 = var_30.field_2;
                var_33 = var_30.field_3;
                local_sp_0 = var_29;
                rcx_2 = var_31;
                r9_0 = var_32;
                r8_0 = var_33;
                local_sp_2 = local_sp_0;
                local_sp_1 = local_sp_0;
                r9_1 = r9_0;
                rcx_3 = rcx_2;
                r8_1 = r8_0;
                rcx_4 = rcx_2;
                r9_2 = r9_0;
                r8_2 = r8_0;
                if (*(uint32_t *)(local_sp_0 + 60UL) != 2U) {
                    r13_0 = (r13_0 & (-256L)) | (*var_23 == 18446744073709551615UL);
                    rcx_6 = rcx_4;
                    r8_3 = r8_2;
                    local_sp_4 = local_sp_2;
                    r9_3 = r9_2;
                    r9_4 = r9_2;
                    r14_0 = (r14_0 & (-256L)) | (*var_22 == 18446744073709551615UL);
                    local_sp_3 = local_sp_2;
                    r8_4 = r8_2;
                    if ((uint64_t)((uint32_t)var_25 + 1U) == 0UL) {
                        break;
                    }
                    if (*(uint32_t *)(local_sp_2 + 4UL) != 4294967295U) {
                        continue;
                    }
                    break;
                }
            }
        }
    if (*var_18 > *(uint64_t *)(local_sp_2 + 16UL)) {
        var_40 = local_sp_2 + (-8L);
        *(uint64_t *)var_40 = 4210494UL;
        indirect_placeholder();
        local_sp_3 = var_40;
    } else {
        rcx_5 = rcx_4;
        if (*var_19 > *(uint64_t *)(local_sp_2 + 40UL)) {
            var_40 = local_sp_2 + (-8L);
            *(uint64_t *)var_40 = 4210494UL;
            indirect_placeholder();
            local_sp_3 = var_40;
        }
    }
    *var_20 = *(uint64_t *)(local_sp_3 + 24UL);
    *var_21 = *(uint64_t *)(local_sp_3 + 32UL);
    rcx_7 = rcx_5;
    mrv.field_0 = rcx_7;
    mrv1 = mrv;
    mrv1.field_1 = r9_4;
    mrv2 = mrv1;
    mrv2.field_2 = r8_4;
    return mrv2;
}
