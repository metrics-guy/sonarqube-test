typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined3;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined7;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void entry(void);
void FUN_004025d0(void);
void FUN_00402640(void);
void FUN_004026d0(void);
void FUN_00402705(void);
void FUN_00402729(void);
void FUN_0040274d(long lParm1);
ulong FUN_00402923(void);
ulong FUN_00402987(char **ppcParm1,undefined8 *puParm2,undefined8 uParm3);
ulong FUN_00402a4f(void);
void FUN_00402b1b(void);
void FUN_00402b5e(void);
void FUN_00402ba1(void);
void FUN_00402be4(void);
void FUN_00402d3f(undefined8 uParm1);
undefined8 FUN_00402d88(byte *pbParm1);
void FUN_00402ee9(uint uParm1);
ulong FUN_00403007(uint uParm1,undefined8 *puParm2);
void FUN_0040361b(void);
void FUN_00403629(long lParm1,ulong uParm2);
void FUN_0040364c(undefined8 *puParm1);
long * FUN_00403664(long *plParm1,undefined8 uParm2,char cParm3);
void FUN_0040375c(undefined8 uParm1,undefined8 uParm2);
int * FUN_0040376f(byte *pbParm1);
int * FUN_00403808(int *piParm1,long lParm2);
ulong FUN_0040386b(long lParm1,long lParm2);
undefined8 FUN_00403889(long lParm1,undefined8 uParm2,byte bParm3);
undefined8 FUN_004038d7(long lParm1,undefined8 uParm2,byte bParm3,char cParm4);
undefined8 FUN_00403938(long lParm1,undefined8 uParm2,byte bParm3,char cParm4,char cParm5);
undefined8 FUN_0040399c(long lParm1,undefined8 uParm2,byte bParm3,char cParm4,char cParm5,char cParm6);
undefined8 FUN_00403a13(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7);
undefined8 FUN_00403a97(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8);
undefined8 FUN_00403b31(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined8 FUN_00403bd4(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10);
undefined8 FUN_00403c8a(byte *param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10,char param_11);
undefined * FUN_00403d4f(char *pcParm1,int iParm2);
ulong FUN_00403e12(undefined *param_1,ulong param_2,char *param_3,ulong param_4,uint param_5,ulong param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_00404d84(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
ulong FUN_00404f15(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
void FUN_00404f48(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_00404fb0(undefined8 uParm1,char cParm2);
void FUN_00404fc9(undefined8 uParm1);
void FUN_00404fdc(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00405050(void);
void FUN_00405063(undefined8 uParm1,undefined8 uParm2);
void FUN_00405078(undefined8 uParm1);
void FUN_0040508e(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_004052d2(void);
void FUN_00405324(void);
void FUN_004053a9(long lParm1);
void FUN_004053c3(void);
long FUN_004053d1(long lParm1,ulong *puParm2);
void FUN_00405404(long lParm1,ulong *puParm2,ulong uParm3);
void FUN_00405492(undefined8 uParm1,undefined8 uParm2);
long FUN_004054a5(void);
long FUN_004054cd(undefined8 param_1,ulong param_2,long param_3,long param_4,undefined8 param_5,undefined8 param_6,uint param_7);
void FUN_004055be(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
undefined8 FUN_004055df(long *plParm1,int iParm2);
ulong FUN_00405640(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_0040567d(char *pcParm1,char **ppcParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_0040595d(ulong uParm1,ulong uParm2);
ulong FUN_004059a7(undefined8 uParm1);
void FUN_004059c4(void);
void FUN_004059e9(undefined8 uParm1);
void FUN_00405a29(undefined auParm1 [16],undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong uParm9,ulong uParm10,undefined8 uParm11,undefined8 uParm12,undefined8 uParm13,undefined8 uParm14);
void FUN_00405a7f(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_00405b52(undefined8 uParm1);
void FUN_00405bd5(undefined8 uParm1);
ulong FUN_00405bed(long lParm1);
undefined8 FUN_00405c6f(void);
void FUN_00405c82(void);
void FUN_00405c90(long lParm1,int *piParm2);
char * FUN_00405d79(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_00405dfe(int param_1,undefined8 *param_2,char *param_3,char **param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
ulong FUN_00406392(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
void FUN_00406835(void);
void FUN_0040688b(void);
void FUN_004068a1(long lParm1);
ulong FUN_004068bb(uint *puParm1,byte *pbParm2,long lParm3);
void FUN_0040691a(long lParm1,long lParm2);
ulong FUN_00406946(int iParm1);
void FUN_0040695b(long lParm1,long lParm2);
void FUN_0040698c(long lParm1,long lParm2);
ulong FUN_004069bf(long lParm1,long lParm2);
void FUN_004069ee(ulong *puParm1);
void FUN_004069ff(long lParm1,long lParm2);
void FUN_00406a17(long lParm1,long lParm2);
ulong FUN_00406a2f(long lParm1,long lParm2);
ulong FUN_00406a69(long lParm1,long lParm2);
undefined8 FUN_00406a83(void);
void FUN_00406a89(undefined8 uParm1,undefined8 uParm2,undefined8 *puParm3,long lParm4,byte bParm5,long lParm6);
void FUN_00406aed(long *plParm1);
ulong FUN_00406b36(long lParm1,long lParm2);
long FUN_00406b76(long lParm1,long lParm2);
void FUN_00406bc4(long lParm1,long lParm2);
long FUN_00406c02(long lParm1,uint uParm2);
long FUN_00406d0f(long *plParm1,long lParm2,uint uParm3);
undefined8 FUN_00406d7e(char *pcParm1,long lParm2,ulong uParm3);
ulong FUN_00406ea1(long *plParm1,long lParm2,ulong uParm3);
ulong FUN_00406f14(long lParm1,uint uParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_004070cf(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long lParm5,undefined8 uParm6);
long FUN_00407134(long *plParm1,long lParm2,long lParm3,uint uParm4);
long FUN_00407187(long lParm1,long lParm2);
undefined8 FUN_004071fd(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_004072f3(long *plParm1,long lParm2);
ulong FUN_0040737c(long *plParm1);
undefined8 FUN_00407435(undefined4 *puParm1,long lParm2,char *pcParm3);
undefined8 FUN_0040752a(undefined4 *param_1,long param_2,undefined *param_3,int param_4,undefined8 param_5,undefined8 param_6,char param_7);
void FUN_0040760a(long *plParm1,code *pcParm2,undefined8 uParm3);
void FUN_0040765b(long *plParm1,code *pcParm2,undefined8 uParm3);
undefined8 FUN_004076a9(long lParm1,ulong uParm2);
void FUN_00407752(long param_1,undefined8 param_2,long param_3,long param_4,undefined8 param_5,byte param_6,long param_7);
undefined8 FUN_0040780a(long *plParm1,undefined8 uParm2);
undefined8 FUN_00407869(long lParm1);
void FUN_00407936(undefined8 *puParm1);
void FUN_00407956(undefined8 *puParm1);
void FUN_00407976(undefined8 *puParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
void FUN_0040799e(long lParm1,long *plParm2,long *plParm3,undefined *puParm4);
long ** FUN_00407a61(long **pplParm1,undefined8 uParm2);
void FUN_00407af2(void);
long FUN_00407b07(undefined4 *puParm1,long *plParm2,long lParm3);
undefined8 FUN_00407cea(undefined8 *puParm1,long lParm2,long lParm3);
undefined8 FUN_00407d60(undefined8 *puParm1,undefined8 uParm2);
ulong FUN_00407db3(long *plParm1,long lParm2);
ulong FUN_00407e9e(long *plParm1,undefined8 uParm2,long lParm3,long lParm4,uint uParm5);
undefined8 FUN_00407f8b(long *plParm1,long lParm2);
undefined8 FUN_00407fd0(long *plParm1,ulong *puParm2,ulong uParm3);
undefined8 FUN_004080c4(long lParm1,undefined4 uParm2,ulong uParm3);
undefined8 FUN_0040815e(long lParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_0040822b(long lParm1,long lParm2,undefined8 uParm3);
void FUN_004082b1(long lParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040832a(long *plParm1,ulong uParm2);
ulong FUN_0040852c(byte bParm1,long lParm2);
void FUN_00408555(long lParm1);
void FUN_004085be(long *plParm1);
void FUN_0040875d(long *plParm1,long lParm2,uint *puParm3);
undefined8 FUN_00408813(long *plParm1);
undefined8 FUN_00408da1(undefined8 *puParm1,undefined8 uParm2,long lParm3,long lParm4,byte bParm5,long lParm6);
ulong FUN_00408ed6(long lParm1,int iParm2);
undefined8 FUN_00408fa3(long lParm1,long lParm2);
undefined8 FUN_0040901e(long *plParm1,long lParm2);
undefined8 FUN_004091e2(long *plParm1,long lParm2);
undefined8 FUN_00409268(long *plParm1,long lParm2,long lParm3);
void FUN_00409423(undefined4 *puParm1,undefined *puParm2);
undefined8 FUN_00409434(long *plParm1,long lParm2,long lParm3);
void FUN_004095b3(long *plParm1,long lParm2,long lParm3,long lParm4,long lParm5,long lParm6);
undefined8 FUN_0040964d(long *plParm1,undefined8 uParm2,undefined8 uParm3,long lParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_00409721(long lParm1,long lParm2,ulong uParm3);
ulong FUN_004097f2(long lParm1,undefined8 *puParm2,long lParm3);
undefined8 FUN_00409923(long lParm1,long lParm2,undefined8 uParm3);
undefined8 FUN_0040999e(long *plParm1,long lParm2,ulong uParm3);
void FUN_00409ff9(undefined8 uParm1,long lParm2);
long FUN_0040a00a(undefined8 uParm1,byte *pbParm2,undefined8 uParm3);
long FUN_0040a0ae(long lParm1,long lParm2,undefined8 uParm3,undefined4 *puParm4,ulong uParm5,undefined4 *puParm6);
void FUN_0040a46d(long lParm1);
void FUN_0040a557(undefined8 *puParm1);
void FUN_0040a576(undefined8 *puParm1);
long FUN_0040a5bc(long *plParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040a856(long *plParm1,long lParm2,undefined8 uParm3);
ulong FUN_0040a8ed(long *plParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
ulong FUN_0040ab81(undefined4 *puParm1,long *plParm2,long lParm3,char cParm4);
undefined8 FUN_0040ad9e(long lParm1);
undefined8 FUN_0040ae38(long *plParm1);
void FUN_0040b02c(long lParm1);
long FUN_0040b086(long *plParm1,long lParm2,uint uParm3,undefined8 uParm4);
ulong * FUN_0040b2a0(undefined4 *puParm1,long lParm2,long lParm3,ulong uParm4);
ulong FUN_0040b37d(long *plParm1);
void FUN_0040b550(long *plParm1);
void FUN_0040b5a4(long lParm1);
void FUN_0040b5d0(long *plParm1);
long FUN_0040b736(long *plParm1,long lParm2,undefined8 uParm3);
ulong * FUN_0040b864(undefined4 *puParm1,long lParm2,long lParm3);
ulong FUN_0040b927(long lParm1,long lParm2,undefined8 uParm3);
ulong FUN_0040b9e4(long lParm1,undefined4 *puParm2,undefined8 uParm3,ulong uParm4);
ulong FUN_0040bae3(long lParm1,undefined8 uParm2,long lParm3,undefined8 uParm4,uint uParm5);
undefined8 FUN_0040bd0a(long lParm1,undefined8 uParm2,long lParm3,long lParm4,long lParm5);
long FUN_0040bea5(long *plParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_0040c46f(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_0040c519(long *plParm1,long lParm2,undefined8 uParm3,undefined8 uParm4);
undefined8 FUN_0040c6ea(long *plParm1,long lParm2,undefined8 uParm3,long lParm4,long lParm5,long lParm6);
void FUN_0040c8fd(long lParm1);
undefined8 FUN_0040c9b8(long *plParm1);
void FUN_0040ca19(long lParm1);
undefined8 FUN_0040cbd1(long *plParm1,undefined8 *puParm2,long lParm3,undefined8 uParm4,undefined4 *puParm5);
ulong FUN_0040cc90(ulong *puParm1,long lParm2,ulong uParm3,uint uParm4);
undefined8 FUN_0040ce55(ulong uParm1,undefined8 uParm2,long lParm3,long *plParm4,int *piParm5,int *piParm6);
undefined8 FUN_0040d118(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,byte *pbParm4);
undefined8 FUN_0040d14c(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,byte *pbParm4);
ulong FUN_0040d180(byte *pbParm1,undefined8 uParm2,long lParm3,long *plParm4,byte *pbParm5,uint uParm6);
long FUN_0040d854(long lParm1,long lParm2,long lParm3,ulong uParm4,int *piParm5);
long FUN_0040de21(long lParm1,undefined8 uParm2,undefined8 uParm3,char *pcParm4,byte bParm5,int *piParm6);
long FUN_0040e025(long lParm1,long *plParm2,undefined8 *puParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_0040e5a6(undefined8 uParm1,undefined8 *puParm2,long lParm3,undefined8 uParm4,long lParm5,int *piParm6);
long FUN_0040e6e5(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_0040e831(undefined8 uParm1,long *plParm2,ulong uParm3,int *piParm4);
long FUN_0040e8eb(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,undefined8 uParm5,int *piParm6);
void FUN_0040e9e3(long **pplParm1);
ulong FUN_0040eb17(long *plParm1,undefined8 uParm2,undefined8 uParm3,ulong uParm4);
void FUN_0040ed17(long **pplParm1,long lParm2,undefined8 *puParm3);
undefined8 FUN_0040f15f(byte **ppbParm1,byte *pbParm2,ulong uParm3);
ulong FUN_0040f770(long *plParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_0040fa50(long lParm1,long lParm2,long lParm3,undefined8 uParm4);
ulong FUN_0040fc47(long param_1,long *param_2,long param_3,long param_4,undefined8 param_5,long param_6,uint param_7);
void FUN_0041019d(undefined8 uParm1,long *plParm2,undefined8 *puParm3,undefined8 uParm4,long lParm5);
undefined8 FUN_0041020e(long lParm1,long lParm2,long lParm3);
ulong FUN_00410545(long lParm1,long lParm2);
long FUN_004108ae(int *piParm1,long lParm2,long lParm3);
long FUN_00410a5c(int *piParm1,long lParm2);
ulong FUN_00410ab8(long lParm1,long lParm2);
ulong FUN_00410cd5(long lParm1,long *plParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_00410d55(long lParm1,long *plParm2,long lParm3,undefined8 uParm4);
ulong FUN_00410eab(long lParm1,long *plParm2);
ulong FUN_00410fab(long lParm1);
ulong FUN_004111d1(long lParm1,long *plParm2,long lParm3,long lParm4);
ulong FUN_00411515(long lParm1,long *plParm2,long lParm3,long lParm4);
long FUN_00411653(long param_1,undefined8 param_2,long param_3,long *param_4,long param_5,long param_6,long param_7);
ulong FUN_00411968(long lParm1,long lParm2);
undefined8 FUN_00411ea2(int *piParm1,long lParm2,long lParm3);
long FUN_00411f65(long lParm1,char cParm2,long *plParm3);
undefined8 FUN_004122e5(long *plParm1,long lParm2,long lParm3,long *plParm4,char cParm5);
ulong FUN_004125cf(long *param_1,long param_2,long param_3,long param_4,long param_5,undefined8 param_6,ulong param_7,ulong *param_8,uint param_9);
undefined1 * FUN_00412ff2(undefined8 uParm1,undefined8 uParm2,long lParm3);
undefined8 FUN_0041304c(long *plParm1);
long FUN_004130ec(long param_1,undefined8 param_2,long param_3,long param_4,long param_5,undefined8 param_6,ulong *param_7,char param_8);
void FUN_00413335(void);
ulong FUN_0041334a(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined FUN_00413419(int iParm1);;
ulong FUN_00413423(uint uParm1);
ulong FUN_00413432(byte *pbParm1,byte *pbParm2);
ulong FUN_0041347a(undefined8 uParm1);
undefined8 FUN_004134dd(void);
ulong FUN_004134e5(ulong uParm1);
undefined * FUN_00413580(void);
char * FUN_00413858(void);
ulong FUN_0041392b(char *pcParm1,long lParm2);
long FUN_00413961(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
char * FUN_00413a6c(ulong uParm1,long lParm2,long lParm3);
ulong FUN_00413c0f(void);
int * FUN_00413c43(undefined8 param_1,long *param_2);
int * FUN_00413e7d(int *param_1,long *param_2);
int * FUN_00413fdb(long **pplParm1,ulong uParm2,long **pplParm3,uint *puParm4,long **pplParm5,ulong uParm6);
long FUN_00414532(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_004149ce(uint param_1);
uint * FUN_00414a0b(undefined8 uParm1,ulong *puParm2);
uint * FUN_00414c3b(undefined8 uParm1,int *piParm2,ulong *puParm3);
void FUN_00414de6(uint uParm1);
void FUN_00414e18(char *param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
long * FUN_00414f99(long *plParm1,long **pplParm2,long lParm3,undefined8 uParm4);
double FUN_0041a750(int *piParm1);
void FUN_0041a799(int *param_1);
long FUN_0041a81a(ulong uParm1,ulong uParm2);
void FUN_0041a82c(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0041a842(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_0041a868(ulong uParm1,ulong uParm2);
undefined8 FUN_0041a873(uint *puParm1,ulong *puParm2);
undefined8 FUN_0041ac9d(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_0041b749(undefined auParm1 [16]);
ulong FUN_0041b768(void);
void FUN_0041b790(void);
undefined8 _DT_FINI(void);

