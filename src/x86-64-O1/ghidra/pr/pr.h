typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_004021b0(void);
void FUN_004023d0(void);
void entry(void);
void FUN_004025a0(void);
void FUN_00402610(void);
void FUN_004026a0(void);
ulong FUN_004026d8(void);
void FUN_0040272d(void);
void FUN_004028b8(long lParm1);
void FUN_00402912(void);
void FUN_00402989(int iParm1);
void FUN_004029d6(long lParm1,int iParm2,char *pcParm3);
void FUN_00402a4b(void);
void FUN_00402a68(void);
ulong FUN_00402ab9(void);
ulong FUN_00402b84(int iParm1);
ulong FUN_00402dfd(uint uParm1,char cParm2,char *pcParm3);
void FUN_00402f1f(char *pcParm1);
void FUN_00402f57(undefined8 uParm1,int iParm2,undefined4 *puParm3,undefined8 uParm4);
void FUN_00402f7a(undefined8 uParm1);
ulong FUN_00402f9e(byte *pbParm1,long *plParm2);
void FUN_0040306c(long lParm1);
ulong FUN_00403162(byte bParm1);
void FUN_00403328(void);
void FUN_00403336(byte *pbParm1,uint uParm2);
undefined8 FUN_004034e5(int iParm1,undefined8 *puParm2);
void FUN_0040365c(undefined8 *puParm1);
void FUN_0040375f(undefined8 *puParm1,int iParm2);
ulong FUN_004038d8(ulong uParm1);
void FUN_00403a19(undefined8 *puParm1);
void FUN_00403a7e(void);
void FUN_00403b0e(int iParm1);
void FUN_00403b55(void);
void FUN_00403c21(void);
void FUN_00403cf3(long lParm1);
undefined8 FUN_00403d3f(undefined8 *puParm1);
void FUN_00404056(void);
void FUN_004041a2(void);
void FUN_0040441f(int iParm1);
ulong FUN_0040444c(void);
void FUN_0040476f(ulong uParm1,undefined8 uParm2);
void FUN_004047ef(void);
void FUN_00404813(void);
void FUN_00404837(long lParm1);
void FUN_00404a0d(uint uParm1);
void FUN_00404be3(char *pcParm1,char cParm2,char *pcParm3,undefined4 *puParm4);
ulong FUN_00404c99(uint uParm1,undefined8 *puParm2);
void FUN_004054ca(void);
void FUN_004054d8(long lParm1,ulong uParm2);
long FUN_004054fb(undefined8 uParm1,undefined8 uParm2);
void FUN_0040558b(undefined8 *puParm1);
ulong FUN_004055cd(ulong uParm1);
char * FUN_00405668(long lParm1,long lParm2);
ulong FUN_004056f5(byte *pbParm1,long lParm2,ulong uParm3);
void FUN_0040589c(char *pcParm1,ulong uParm2);
ulong FUN_004058c7(int iParm1,int iParm2);
long FUN_004058fb(long lParm1,long lParm2,long lParm3);
long FUN_0040592e(long lParm1,long lParm2,long lParm3);
char * FUN_00405961(char *pcParm1,long lParm2,char *pcParm3,uint *puParm4,undefined uParm5,undefined8 uParm6,undefined8 uParm7,uint uParm8);
void FUN_004071c8(void);
int * FUN_004071ea(byte *pbParm1);
int * FUN_00407283(int *piParm1,long lParm2);
ulong FUN_004072e6(long lParm1,long lParm2);
undefined8 FUN_00407304(long lParm1,undefined8 uParm2,byte bParm3);
undefined8 FUN_00407352(long lParm1,undefined8 uParm2,byte bParm3,char cParm4);
undefined8 FUN_004073b3(long lParm1,undefined8 uParm2,byte bParm3,char cParm4,char cParm5);
undefined8 FUN_00407417(long lParm1,undefined8 uParm2,byte bParm3,char cParm4,char cParm5,char cParm6);
undefined8 FUN_0040748e(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7);
undefined8 FUN_00407512(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8);
undefined8 FUN_004075ac(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined8 FUN_0040764f(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10);
undefined8 FUN_00407705(byte *param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10,char param_11);
undefined * FUN_004077ca(char *pcParm1,int iParm2);
ulong FUN_0040788d(undefined *param_1,ulong param_2,char *param_3,ulong param_4,uint param_5,ulong param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_004087ff(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
ulong FUN_00408990(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
void FUN_004089c3(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_00408a2b(undefined8 uParm1,char cParm2);
void FUN_00408a44(undefined8 uParm1);
void FUN_00408a57(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00408acb(void);
void FUN_00408ade(undefined8 uParm1,undefined8 uParm2);
void FUN_00408af3(undefined8 uParm1);
void FUN_00408b09(long lParm1);
void FUN_00408b12(undefined8 uParm1);
void FUN_00408b2f(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_00408d73(void);
void FUN_00408dc5(void);
void FUN_00408e4a(ulong uParm1,ulong uParm2);
void FUN_00408e64(ulong uParm1,ulong uParm2);
void FUN_00408e97(void);
long FUN_00408ea5(long lParm1,ulong *puParm2);
void FUN_00408ed8(long lParm1,ulong *puParm2,ulong uParm3);
void FUN_00408f66(undefined8 uParm1,undefined8 uParm2);
long FUN_00408f79(void);
long FUN_00408fa1(undefined8 param_1,ulong param_2,long param_3,long param_4,undefined8 param_5,undefined8 param_6,uint param_7);
void FUN_00409092(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
undefined8 FUN_004090b3(long *plParm1,int iParm2);
ulong FUN_00409114(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_00409151(char *pcParm1,char **ppcParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_00409431(long *plParm1,int iParm2);
ulong FUN_00409492(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_004094cf(char *pcParm1,char **ppcParm2,uint uParm3,undefined8 *puParm4,long lParm5);
void FUN_004097af(uint uParm1,int iParm2,undefined uParm3,long lParm4,undefined8 uParm5,uint uParm6);
ulong FUN_00409830(int iParm1,undefined8 uParm2,char cParm3,undefined8 uParm4,undefined8 uParm5);
ulong FUN_00409848(int iParm1);
ulong FUN_0040985d(ulong *puParm1,int iParm2);
ulong FUN_00409887(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_004098c4(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
ulong FUN_00409bfb(undefined8 uParm1);
void FUN_00409c18(void);
void FUN_00409c3d(undefined8 uParm1);
void FUN_00409c7d(undefined auParm1 [16],undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong uParm9,ulong uParm10,undefined8 uParm11,undefined8 uParm12,undefined8 uParm13,undefined8 uParm14);
void FUN_00409cd3(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_00409da6(undefined8 uParm1);
ulong FUN_00409e29(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4);
void FUN_00409fa8(undefined8 uParm1);
ulong FUN_00409fc0(long lParm1);
undefined8 FUN_0040a042(void);
void FUN_0040a055(void);
void FUN_0040a063(long lParm1,int *piParm2);
char * FUN_0040a14c(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_0040a1d1(int param_1,undefined8 *param_2,char *param_3,char **param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
ulong FUN_0040a765(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
void FUN_0040ac08(void);
void FUN_0040ac5e(void);
void FUN_0040ac74(void);
void FUN_0040ad20(long lParm1);
ulong FUN_0040ad3a(uint *puParm1,byte *pbParm2,long lParm3);
void FUN_0040ad99(long lParm1,long lParm2);
ulong FUN_0040adc5(uint uParm1,uint uParm2);
ulong FUN_0040ade5(uint *puParm1,uint *puParm2);
void FUN_0040ae2a(long lParm1,undefined8 uParm2,long lParm3);
void FUN_0040ae47(void);
void FUN_0040ae5a(long lParm1);
ulong FUN_0040ae86(long lParm1);
undefined8 * FUN_0040aebd(char *pcParm1);
undefined8 FUN_0040af48(long *plParm1,char *pcParm2);
void FUN_0040b07b(long *plParm1);
long FUN_0040b09d(long lParm1);
ulong FUN_0040b12b(long lParm1);
long FUN_0040b174(long lParm1,undefined8 uParm2,long lParm3);
long FUN_0040b201(long lParm1,undefined8 uParm2);
void FUN_0040b2a9(long lParm1);
void FUN_0040b2c8(void);
ulong FUN_0040b370(char *pcParm1);
ulong FUN_0040b3c9(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040b498(long lParm1,long lParm2);
undefined8 FUN_0040b4b6(long lParm1,undefined8 uParm2,char cParm3);
undefined8 FUN_0040b4d8(long lParm1,undefined8 uParm2,char cParm3,char cParm4);
undefined8 FUN_0040b4fd(long lParm1,undefined8 uParm2,char cParm3,char cParm4,char cParm5);
undefined8 FUN_0040b529(long lParm1,undefined8 uParm2,char cParm3,char cParm4,char cParm5,char cParm6);
undefined8 FUN_0040b558(long param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7);
undefined8 FUN_0040b592(long param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8);
undefined8 FUN_0040b5df(long param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined8 FUN_0040b62a(long param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10);
undefined8 FUN_0040b68a(char *param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10,char param_11);
void FUN_0040b6fc(ulong uParm1);
undefined FUN_0040b75f(int iParm1);;
ulong FUN_0040b769(uint uParm1);
ulong FUN_0040b778(byte *pbParm1,byte *pbParm2);
ulong FUN_0040b7c0(undefined8 uParm1);
undefined8 FUN_0040b823(void);
undefined * FUN_0040b82b(void);
char * FUN_0040bb03(void);
ulong FUN_0040bbd6(long lParm1,long lParm2);
undefined8 FUN_0040bbf4(long lParm1,undefined8 uParm2,char cParm3);
undefined8 FUN_0040bc16(long lParm1,undefined8 uParm2,char cParm3,char cParm4);
undefined8 FUN_0040bc3b(long lParm1,undefined8 uParm2,char cParm3,char cParm4,char cParm5);
undefined8 FUN_0040bc67(long lParm1,undefined8 uParm2,char cParm3,char cParm4,char cParm5,char cParm6);
undefined8 FUN_0040bc96(long param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7);
undefined8 FUN_0040bcd0(long param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8);
undefined8 FUN_0040bd1d(long param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined8 FUN_0040bd68(long param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10);
undefined8 FUN_0040bdc8(char *param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10,char param_11);
ulong FUN_0040be3a(undefined8 uParm1);
ulong FUN_0040c04e(uint uParm1,undefined8 uParm2);
long FUN_0040c1f2(long lParm1,undefined4 uParm2);
ulong FUN_0040c1fb(ulong uParm1);
ulong FUN_0040c26c(uint uParm1,uint uParm2);
long FUN_0040c28c(ulong param_1,long param_2,int param_3,int param_4,int param_5,uint param_6,int param_7,int param_8,int param_9,int param_10);
long FUN_0040c3fa(undefined8 uParm1,undefined8 uParm2);
long FUN_0040c42f(void);
void FUN_0040c4c6(code *pcParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_0040c4e4(undefined8 uParm1,long *plParm2,undefined8 uParm3);
long FUN_0040c584(uint *puParm1,undefined8 uParm2,long *plParm3);
void FUN_0040c97a(undefined8 uParm1);
ulong FUN_0040c997(char *pcParm1,char *pcParm2,ulong uParm3);
ulong FUN_0040cab2(char *pcParm1,long lParm2);
long FUN_0040cae8(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
char * FUN_0040cbf3(ulong uParm1,long lParm2,long lParm3);
ulong FUN_0040cd96(void);
int * FUN_0040cdca(undefined8 param_1,long *param_2);
int * FUN_0040d004(int *param_1,long *param_2);
int * FUN_0040d162(long **pplParm1,ulong uParm2,long **pplParm3,uint *puParm4,long **pplParm5,ulong uParm6);
long FUN_0040d6b9(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_0040db55(uint param_1);
uint * FUN_0040db92(undefined8 uParm1,ulong *puParm2);
uint * FUN_0040ddc2(undefined8 uParm1,int *piParm2,ulong *puParm3);
void FUN_0040df6d(uint uParm1);
void FUN_0040df9f(char *param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
long * FUN_0040e120(long *plParm1,long **pplParm2,long lParm3,undefined8 uParm4);
undefined8 * FUN_004138d7(ulong uParm1);
void FUN_0041394b(ulong uParm1);
double FUN_004139c7(int *piParm1);
void FUN_00413a10(int *param_1);
long FUN_00413a91(ulong uParm1,ulong uParm2);
void FUN_00413aa3(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00413ab9(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00413adf(ulong uParm1,ulong uParm2);
undefined8 FUN_00413aea(uint *puParm1,ulong *puParm2);
undefined8 FUN_00413f14(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_004149c0(undefined auParm1 [16]);
ulong FUN_004149df(void);
void FUN_00414a10(void);
undefined8 _DT_FINI(void);

