typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_004021b0(void);
void FUN_00402380(void);
void entry(void);
void FUN_00402560(void);
void FUN_004025d0(void);
void FUN_00402660(void);
undefined8 FUN_00402695(undefined8 uParm1);
void FUN_00402699(void);
void FUN_004026bd(long lParm1);
void FUN_00402893(void);
undefined8 FUN_004028a1(undefined1 *puParm1,undefined8 uParm2,ulong uParm3,undefined8 uParm4);
ulong FUN_00402991(byte *pbParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_00402b35(uint uParm1);
ulong FUN_00402dd8(uint uParm1,undefined8 *puParm2);
long FUN_00403370(char *pcParm1,char **ppcParm2,long lParm3,long lParm4);
void FUN_0040348a(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_004034e7(long *plParm1,long lParm2,long lParm3);
long FUN_004035ba(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
ulong FUN_004036cd(int iParm1,int iParm2);
void FUN_00403701(undefined8 uParm1,byte *pbParm2,long lParm3);
void FUN_00403735(undefined8 uParm1,byte *pbParm2,long lParm3);
char * FUN_00403769(long lParm1,char *pcParm2,uint *puParm3,undefined uParm4,undefined8 uParm5,undefined8 uParm6,uint uParm7);
void FUN_00405270(void);
void FUN_00405292(undefined8 *puParm1);
char * FUN_004052d4(long lParm1,long lParm2);
undefined8 FUN_00405361(void);
ulong FUN_00405367(uint uParm1);
long FUN_0040536a(long param_1);
ulong FUN_004054d2(long param_1,int param_2);
void FUN_00405699(long lParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,int iParm5);
ulong FUN_004056ad(long param_1,long param_2);
ulong FUN_00405764(ulong uParm1,int iParm2);
long * FUN_004057b3(long lParm1,undefined8 uParm2);
byte ** FUN_00405845(undefined8 uParm1,byte *pbParm2);
ulong FUN_00405a70(ulong uParm1,long lParm2,undefined8 uParm3);
undefined * FUN_00405ab0(long lParm1,undefined *puParm2,int iParm3);
undefined8 FUN_00405b65(uint *puParm1,undefined8 uParm2,int iParm3);
long FUN_00405b8d(int iParm1,long lParm2);
undefined8 FUN_00405c81(int iParm1,undefined8 uParm2);
undefined8 FUN_00405cdd(long lParm1,undefined8 uParm2,int iParm3);
void FUN_00405d30(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_00405def(long *plParm1,byte **ppbParm2);
void FUN_0040617f(undefined8 uParm1,long lParm2);
void FUN_0040646f(undefined8 uParm1,long lParm2);
ulong FUN_00406582(ulong param_1,int *param_2);
ulong FUN_0040666e(undefined8 uParm1,uint *puParm2,uint *puParm3,long lParm4);
long FUN_004066d4(undefined8 uParm1,long lParm2,long lParm3,int iParm4);
void FUN_00406754(int *piParm1,int *piParm2,long lParm3,char cParm4);
undefined8 FUN_00406acb(long lParm1);
ulong FUN_00408155(long *plParm1,byte *pbParm2,long *plParm3,undefined4 uParm4,ulong uParm5,byte *pbParm6);
undefined8 FUN_004097cd(int *piParm1,int *piParm2,long lParm3,ulong uParm4);
undefined8 FUN_0040985a(int *piParm1,char *pcParm2,ulong uParm3);
ulong FUN_00409a0d(long *plParm1,undefined8 uParm2,ulong uParm3);
int * FUN_00409b42(byte *pbParm1);
int * FUN_00409bdb(int *piParm1,long lParm2);
ulong FUN_00409c3e(long lParm1,long lParm2);
undefined8 FUN_00409c5c(long lParm1,undefined8 uParm2,byte bParm3);
undefined8 FUN_00409caa(long lParm1,undefined8 uParm2,byte bParm3,char cParm4);
undefined8 FUN_00409d0b(long lParm1,undefined8 uParm2,byte bParm3,char cParm4,char cParm5);
undefined8 FUN_00409d6f(long lParm1,undefined8 uParm2,byte bParm3,char cParm4,char cParm5,char cParm6);
undefined8 FUN_00409de6(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7);
undefined8 FUN_00409e6a(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8);
undefined8 FUN_00409f04(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined8 FUN_00409fa7(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10);
undefined8 FUN_0040a05d(byte *param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10,char param_11);
undefined * FUN_0040a122(char *pcParm1,int iParm2);
ulong FUN_0040a1e5(undefined *param_1,ulong param_2,char *param_3,ulong param_4,uint param_5,ulong param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_0040b157(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
ulong FUN_0040b2e8(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
void FUN_0040b31b(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040b3dd(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040b451(void);
void FUN_0040b464(undefined8 uParm1,undefined8 uParm2);
void FUN_0040b479(undefined8 uParm1);
ulong FUN_0040b48f(undefined8 *puParm1);
void FUN_0040b4fb(long lParm1);
ulong FUN_0040b504(uint uParm1);
void FUN_0040b507(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_0040b74b(void);
void FUN_0040b79d(void);
void FUN_0040b822(long lParm1);
void FUN_0040b83c(void);
ulong FUN_0040b84a(long lParm1,long lParm2);
ulong FUN_0040b87d(void);
ulong FUN_0040b8a5(undefined8 uParm1);
void FUN_0040b8c2(void);
void FUN_0040b8e7(undefined8 uParm1);
void FUN_0040b927(undefined auParm1 [16],undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong uParm9,ulong uParm10,undefined8 uParm11,undefined8 uParm12,undefined8 uParm13,undefined8 uParm14);
void FUN_0040b97d(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0040ba50(undefined8 uParm1);
void FUN_0040bad3(undefined8 uParm1);
ulong FUN_0040baeb(long lParm1);
undefined8 FUN_0040bb6d(void);
void FUN_0040bb80(void);
void FUN_0040bb8e(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040bba4(long lParm1,int *piParm2);
char * FUN_0040bc8d(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_0040bd12(int param_1,undefined8 *param_2,char *param_3,char **param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
ulong FUN_0040c2a6(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
void FUN_0040c749(void);
void FUN_0040c79f(void);
void FUN_0040c7b5(void);
void FUN_0040c861(void);
void FUN_0040c885(long lParm1);
ulong FUN_0040c89f(uint *puParm1,byte *pbParm2,long lParm3);
long FUN_0040c8fe(long lParm1,undefined4 uParm2);
ulong FUN_0040c907(ulong uParm1);
ulong FUN_0040c978(uint uParm1,uint uParm2);
long FUN_0040c998(ulong param_1,long param_2,int param_3,int param_4,int param_5,uint param_6,int param_7,int param_8,int param_9,int param_10);
long FUN_0040cb06(undefined8 uParm1,undefined8 uParm2);
long FUN_0040cb3b(void);
void FUN_0040cbd2(code *pcParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_0040cbf0(undefined8 uParm1,long *plParm2,undefined8 uParm3);
long FUN_0040cc90(uint *puParm1,undefined8 uParm2,long *plParm3);
void FUN_0040d086(undefined8 uParm1);
undefined8 FUN_0040d0a3(char *pcParm1);
undefined8 FUN_0040d155(long lParm1);
void FUN_0040d21c(long lParm1,long lParm2);
ulong FUN_0040d248(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,long param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_0040d354(void);
undefined8 FUN_0040d362(char *pcParm1,long lParm2);
ulong FUN_0040d3c7(uint uParm1,uint uParm2);
ulong FUN_0040d3e7(uint *puParm1,uint *puParm2);
void FUN_0040d42c(long lParm1,undefined8 uParm2,long lParm3);
void FUN_0040d449(void);
void FUN_0040d45c(long lParm1);
ulong FUN_0040d488(long lParm1);
undefined8 * FUN_0040d4bf(char *pcParm1);
undefined8 FUN_0040d54a(long *plParm1,char *pcParm2);
void FUN_0040d67d(long *plParm1);
long FUN_0040d69f(long lParm1);
ulong FUN_0040d72d(long lParm1);
long FUN_0040d776(long lParm1,undefined8 uParm2,long lParm3);
long FUN_0040d803(long lParm1,undefined8 uParm2);
void FUN_0040d8ab(long lParm1);
void FUN_0040d8ca(void);
ulong FUN_0040d972(char *pcParm1);
ulong FUN_0040d9cb(char *pcParm1,long lParm2);
long FUN_0040da01(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
char * FUN_0040db0c(ulong uParm1,long lParm2,long lParm3);
ulong FUN_0040dcaf(void);
int * FUN_0040dce3(undefined8 param_1,long *param_2);
int * FUN_0040df1d(int *param_1,long *param_2);
int * FUN_0040e07b(long **pplParm1,ulong uParm2,long **pplParm3,uint *puParm4,long **pplParm5,ulong uParm6);
long FUN_0040e5d2(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_0040ea6e(uint param_1);
uint * FUN_0040eaab(undefined8 uParm1,ulong *puParm2);
uint * FUN_0040ecdb(undefined8 uParm1,int *piParm2,ulong *puParm3);
void FUN_0040ee86(uint uParm1);
void FUN_0040eeb8(char *param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
ulong FUN_004147f0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined FUN_004148bf(int iParm1);;
undefined FUN_004148d5(int iParm1);;
undefined FUN_004148df(int iParm1);;
undefined FUN_004148f2(int iParm1);;
ulong FUN_004148fc(uint uParm1);
ulong FUN_0041490b(uint uParm1);
ulong FUN_0041491a(byte *pbParm1,byte *pbParm2);
undefined8 FUN_004149c5(void);
ulong FUN_004149cd(ulong uParm1);
undefined * FUN_00414a68(void);
char * FUN_00414d40(void);
ulong FUN_00414e13(int iParm1,int iParm2);
long FUN_00414e47(long lParm1,long lParm2,long lParm3);
long FUN_00414e7a(long lParm1,long lParm2,long lParm3);
char * FUN_00414ead(char *pcParm1,long lParm2,char *pcParm3,uint *puParm4,undefined uParm5,undefined8 uParm6,undefined8 uParm7,uint uParm8);
void FUN_00416714(void);
double FUN_00416736(int *piParm1);
void FUN_0041677f(int *param_1);
long FUN_00416800(ulong uParm1,ulong uParm2);
void FUN_00416812(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00416828(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_0041684e(ulong uParm1,ulong uParm2);
ulong FUN_00416859(long *plParm1,ulong *puParm2,int iParm3,long lParm4);
undefined8 FUN_00416980(uint *puParm1,ulong *puParm2);
undefined8 FUN_00416daa(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_00417856(char *pcParm1,char *pcParm2,ulong uParm3);
ulong FUN_00417971(undefined auParm1 [16]);
ulong FUN_00417990(void);
undefined8 * FUN_004179b4(ulong uParm1);
void FUN_00417a28(ulong uParm1);
void FUN_00417ab0(void);
undefined8 _DT_FINI(void);

