typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_004025f0(void);
void entry(void);
void FUN_00402660(void);
void FUN_004026d0(void);
void FUN_00402760(void);
undefined8 FUN_00402795(undefined8 uParm1);
void FUN_00402799(long lParm1);
void FUN_0040296f(uint uParm1);
ulong FUN_00402afe(uint uParm1,undefined8 *puParm2);
ulong FUN_00402fc6(uint param_1,undefined8 param_2,long *param_3,uint param_4,uint param_5,uint param_6,uint param_7);
char * FUN_00403181(char *pcParm1,char *pcParm2);
void FUN_00403209(undefined8 uParm1,int iParm2,undefined8 uParm3,undefined *puParm4,long lParm5,undefined *puParm6);
void FUN_004033a0(undefined4 *puParm1);
void FUN_004033cb(ulong uParm1);
void FUN_004033f9(ulong uParm1);
ulong FUN_00403427(long param_1,long param_2,uint param_3,uint param_4,uint param_5,ulong param_6,int *param_7);
ulong FUN_00403b86(undefined8 param_1,uint param_2,uint param_3,ulong param_4,uint param_5,uint param_6,int *param_7);
void FUN_00403d32(void);
void FUN_00403d46(void);
char * FUN_00403d5a(ulong uParm1,long lParm2);
int * FUN_00403d9f(byte *pbParm1);
int * FUN_00403e38(int *piParm1,long lParm2);
ulong FUN_00403e9b(long lParm1,long lParm2);
undefined8 FUN_00403eb9(long lParm1,undefined8 uParm2,byte bParm3);
undefined8 FUN_00403f07(long lParm1,undefined8 uParm2,byte bParm3,char cParm4);
undefined8 FUN_00403f68(long lParm1,undefined8 uParm2,byte bParm3,char cParm4,char cParm5);
undefined8 FUN_00403fcc(long lParm1,undefined8 uParm2,byte bParm3,char cParm4,char cParm5,char cParm6);
undefined8 FUN_00404043(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7);
undefined8 FUN_004040c7(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8);
undefined8 FUN_00404161(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined8 FUN_00404204(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10);
undefined8 FUN_004042ba(byte *param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10,char param_11);
undefined * FUN_0040437f(char *pcParm1,int iParm2);
ulong FUN_00404442(undefined *param_1,ulong param_2,char *param_3,ulong param_4,uint param_5,ulong param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_004053b4(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
ulong FUN_00405545(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
void FUN_00405578(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004055a6(ulong uParm1,undefined8 uParm2);
void FUN_00405652(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004056c6(void);
void FUN_004056d9(undefined8 uParm1,undefined8 uParm2);
void FUN_004056ee(undefined8 uParm1);
undefined8 * FUN_00405704(undefined8 *puParm1);
char ** FUN_00405743(char *pcParm1,char *pcParm2,int *piParm3,uint *puParm4,char **ppcParm5,char **ppcParm6);
long FUN_004059df(undefined8 uParm1,undefined8 uParm2,long lParm3,undefined8 uParm4,undefined8 uParm5);
void FUN_00405a9f(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_00405ce3(void);
void FUN_00405d35(void);
void FUN_00405dba(long lParm1);
void FUN_00405dd4(void);
long FUN_00405de2(long lParm1,long lParm2);
void FUN_00405e15(undefined8 uParm1,undefined8 uParm2);
void FUN_00405e3e(char *pcParm1);
void FUN_00405e66(void);
void FUN_00405e8e(undefined8 uParm1,uint uParm2);
ulong FUN_00405ed1(long lParm1,long lParm2);
ulong FUN_00405efc(int iParm1);
ulong FUN_00405f11(ulong *puParm1,int iParm2);
ulong FUN_00405f3b(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_00405f78(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_004062af(ulong uParm1,ulong uParm2);
ulong FUN_004062f9(undefined8 uParm1);
void FUN_00406316(void);
void FUN_0040633b(undefined8 uParm1);
void FUN_0040637b(undefined auParm1 [16],undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong uParm9,ulong uParm10,undefined8 uParm11,undefined8 uParm12,undefined8 uParm13,undefined8 uParm14);
void FUN_004063d1(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_004064a4(uint uParm1,char *pcParm2,uint uParm3,ulong uParm4,uint uParm5);
void FUN_004066ff(void);
void FUN_0040670d(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00406722(ulong uParm1,char *pcParm2,long lParm3,ulong uParm4);
void FUN_0040685f(long lParm1,undefined4 uParm2);
void FUN_004068a7(long lParm1,long lParm2);
long FUN_00406920(char **ppcParm1);
undefined8 FUN_00406967(long lParm1,long lParm2);
ulong FUN_004069d4(long lParm1,long lParm2);
ulong FUN_004069f9(long lParm1,long lParm2,char cParm3);
long FUN_00406b84(long lParm1,long lParm2,ulong uParm3);
long FUN_00406c60(long lParm1,undefined8 uParm2,long lParm3);
ulong FUN_00406cef(long lParm1);
void FUN_00406d4e(long lParm1,undefined8 uParm2);
void FUN_00406da1(long lParm1);
void FUN_00406dda(long lParm1);
void FUN_00406e05(undefined8 uParm1);
long FUN_00406e2c(undefined8 uParm1,undefined8 uParm2,uint uParm3,uint *puParm4);
undefined8 FUN_00406e82(long lParm1);
ulong FUN_00406f8a(void);
ulong FUN_00406fb0(void);
void FUN_00407016(long lParm1,long lParm2);
undefined8 FUN_004070a6(long lParm1,undefined8 *puParm2);
void FUN_00407150(long lParm1,uint uParm2,char cParm3);
ulong FUN_004071a3(long lParm1);
ulong FUN_004071f7(long lParm1,long lParm2,uint uParm3,byte *pbParm4);
long FUN_00407388(long *plParm1,long lParm2);
long FUN_0040741a(long *plParm1,int iParm2);
long * FUN_00407c28(char **ppcParm1,ulong uParm2,long lParm3);
ulong FUN_00407f9b(long *plParm1);
long FUN_004080c3(long *plParm1);
undefined8 FUN_00408694(undefined8 uParm1,long lParm2,uint uParm3);
void FUN_004086bc(long lParm1,int *piParm2);
char * FUN_004087a5(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_0040882a(int param_1,undefined8 *param_2,char *param_3,char **param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
ulong FUN_00408dbe(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
void FUN_00409261(void);
void FUN_004092b7(void);
undefined8 FUN_004092cd(char *pcParm1,uint uParm2,uint uParm3);
void FUN_00409397(void);
undefined8 FUN_004093a5(char *pcParm1,long lParm2);
void FUN_0040941c(long lParm1);
ulong FUN_00409436(uint *puParm1,byte *pbParm2,long lParm3);
void FUN_00409495(void);
ulong FUN_004094a8(uint param_1,undefined8 param_2,uint param_3,ulong param_4);
undefined * FUN_0040958f(undefined *puParm1,uint uParm2,char *pcParm3);
void FUN_00409699(long lParm1,long lParm2);
void FUN_004096c5(void);
undefined8 FUN_004096d3(char *pcParm1,long lParm2);
ulong FUN_00409738(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined FUN_00409807(int iParm1);;
ulong FUN_00409811(uint uParm1);
ulong FUN_00409820(byte *pbParm1,byte *pbParm2);
ulong FUN_00409868(ulong uParm1,char cParm2);
ulong FUN_00409932(ulong uParm1);
void FUN_0040993d(long lParm1);
undefined8 FUN_0040994d(long *plParm1,long *plParm2);
void FUN_004099e8(undefined8 param_1,byte param_2,ulong param_3);
undefined8 FUN_00409a34(void);
ulong FUN_00409a3c(ulong uParm1);
ulong FUN_00409ad7(ulong uParm1);
ulong FUN_00409b4f(ulong uParm1);
undefined8 FUN_00409ba4(long lParm1);
ulong FUN_00409c25(ulong uParm1,long lParm2);
void FUN_00409cbb(long lParm1,undefined8 *puParm2);
long * FUN_00409ccf(long *plParm1,long lParm2,undefined8 uParm3,char cParm4);
long * FUN_00409cf1(long lParm1,long *plParm2,long **pplParm3,char cParm4);
void FUN_00409dad(long lParm1);
undefined8 FUN_00409dd2(long lParm1,long **pplParm2,char cParm3);
long * FUN_00409ed7(long lParm1,long *plParm2);
long * FUN_00409f20(undefined8 uParm1,undefined1 *puParm2,undefined *puParm3,undefined *puParm4,long lParm5);
void FUN_0040a000(ulong **ppuParm1);
ulong FUN_0040a0ab(long *plParm1,undefined8 uParm2);
undefined8 FUN_0040a1e8(long lParm1,long lParm2,long *plParm3);
undefined8 FUN_0040a418(undefined8 uParm1,undefined8 uParm2);
long FUN_0040a447(long lParm1,undefined8 uParm2);
void FUN_0040a627(undefined4 *puParm1,undefined4 uParm2);
ulong FUN_0040a648(long lParm1);
ulong FUN_0040a64d(long lParm1,uint uParm2);
ulong FUN_0040a688(long lParm1,undefined8 uParm2,undefined8 uParm3,ulong uParm4);
undefined * FUN_0040a6c2(void);
char * FUN_0040a99a(void);
void FUN_0040aa6d(ulong uParm1);
void FUN_0040aa8d(ulong uParm1);
void FUN_0040aaad(void);
ulong FUN_0040aaf8(int *piParm1);
void FUN_0040ab47(uint *puParm1);
void FUN_0040ab68(int *piParm1);
ulong FUN_0040ab84(uint uParm1);
ulong FUN_0040ab87(uint uParm1);
void FUN_0040abbb(undefined4 *puParm1);
long FUN_0040abc2(long lParm1);
void FUN_0040abd5(uint *puParm1);
void FUN_0040abe5(int *piParm1);
undefined8 FUN_0040ac19(uint *puParm1,undefined8 uParm2);
ulong FUN_0040ac56(char *pcParm1);
undefined8 FUN_0040aee0(char *pcParm1,uint uParm2,uint uParm3);
undefined8 FUN_0040b000(undefined8 uParm1);
ulong FUN_0040b083(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4);
undefined8 FUN_0040b202(ulong uParm1);
void FUN_0040b251(undefined8 uParm1);
ulong FUN_0040b269(long lParm1);
undefined8 FUN_0040b2eb(void);
void FUN_0040b2fe(void);
long FUN_0040b30c(long lParm1,ulong uParm2);
ulong * FUN_0040b79f(ulong *puParm1,undefined8 uParm2,ulong uParm3);
ulong FUN_0040b876(char *pcParm1,long lParm2);
long FUN_0040b8ac(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
char * FUN_0040b9b7(ulong uParm1,long lParm2,long lParm3);
ulong FUN_0040bb5a(void);
int * FUN_0040bb8e(undefined8 param_1,long *param_2);
int * FUN_0040bdc8(int *param_1,long *param_2);
int * FUN_0040bf26(long **pplParm1,ulong uParm2,long **pplParm3,uint *puParm4,long **pplParm5,ulong uParm6);
long FUN_0040c47d(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_0040c919(uint param_1);
uint * FUN_0040c956(undefined8 uParm1,ulong *puParm2);
uint * FUN_0040cb86(undefined8 uParm1,int *piParm2,ulong *puParm3);
void FUN_0040cd31(uint uParm1);
void FUN_0040cd63(char *param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
double FUN_004126a4(int *piParm1);
void FUN_004126ed(int *param_1);
void FUN_0041276e(undefined8 uParm1);
long FUN_0041278b(ulong uParm1,ulong uParm2);
void FUN_0041279d(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004127b3(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_004127d9(ulong uParm1,ulong uParm2);
undefined8 FUN_004127e4(uint *puParm1,ulong *puParm2);
undefined8 FUN_00412c0e(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_004136ba(undefined auParm1 [16]);
ulong FUN_004136d9(void);
void FUN_00413700(void);
undefined8 _DT_FINI(void);

