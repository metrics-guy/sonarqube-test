typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void entry(void);
void FUN_004025c0(void);
void FUN_00402630(void);
void FUN_004026c0(void);
ulong FUN_004026f5(long lParm1);
ulong FUN_00402706(undefined *puParm1);
undefined FUN_00402fa4(long lParm1);;
void FUN_00402fb8(long lParm1,undefined4 uParm2,undefined8 uParm3,undefined8 *puParm4,undefined4 uParm5);
ulong FUN_00403007(long lParm1,long lParm2);
ulong FUN_00403055(long lParm1,long lParm2);
undefined8 FUN_00403090(long lParm1,long lParm2);
ulong FUN_004030ca(long lParm1,long lParm2);
void FUN_00403133(undefined8 uParm1);
void FUN_0040316a(void);
void FUN_0040318e(void);
byte * FUN_004031b2(byte **ppbParm1);
undefined8 FUN_004031d6(byte **ppbParm1,long lParm2);
void FUN_00403226(long lParm1);
long FUN_004033fc(byte **ppbParm1,long lParm2);
void FUN_0040348e(uint uParm1,undefined8 uParm2);
undefined8 FUN_004034e5(int iParm1,long lParm2,undefined8 *puParm3);
ulong FUN_004036dc(undefined8 uParm1,ulong uParm2);
undefined8 FUN_0040373d(undefined8 uParm1,uint uParm2,long lParm3,long *plParm4);
long FUN_0040381d(char cParm1,undefined8 uParm2,uint uParm3,ulong uParm4);
undefined8 FUN_00403910(undefined8 uParm1,uint uParm2,ulong uParm3,long *plParm4);
long FUN_004039d0(undefined8 uParm1,undefined8 uParm2,ulong uParm3,long lParm4,long lParm5,long *plParm6);
undefined8 FUN_00403ac8(undefined8 uParm1,uint uParm2,long lParm3,long lParm4,long lParm5,long *plParm6);
long FUN_00403d19(long lParm1,long *plParm2);
undefined8 FUN_00403e7a(undefined8 uParm1,uint uParm2,ulong uParm3,long *plParm4);
ulong FUN_0040407b(undefined8 uParm1,uint uParm2,ulong uParm3,long *plParm4);
ulong FUN_004042a5(undefined8 uParm1,uint uParm2,ulong uParm3,long *plParm4);
ulong FUN_00404599(undefined8 uParm1,uint uParm2,undefined8 uParm3,long *plParm4);
void FUN_004046f8(void);
undefined8 FUN_0040471d(undefined8 uParm1,undefined8 uParm2);
ulong FUN_004047be(byte **ppbParm1,undefined8 uParm2);
undefined8 FUN_00404ac6(undefined8 *puParm1,long lParm2);
void FUN_00404b2d(byte **ppbParm1,byte bParm2);
void FUN_00404fca(void);
ulong FUN_0040504c(undefined auParm1 [16],uint uParm2,char **ppcParm3,ulong uParm4);
void FUN_004059dc(undefined8 uParm1,long lParm2,long lParm3);
void FUN_00405ee6(uint uParm1);
void FUN_00406048(uint uParm1,undefined8 uParm2,undefined8 *puParm3,undefined4 *puParm4,double *pdParm5);
ulong FUN_00406407(uint uParm1,undefined8 *puParm2);
long FUN_004068cb(char *pcParm1,char **ppcParm2,long lParm3,long lParm4);
void FUN_004069e5(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_00406a42(long *plParm1,long lParm2,long lParm3);
long FUN_00406b15(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
void FUN_00406ca0(char *pcParm1);
char * FUN_00406cd8(char *pcParm1);
void FUN_00406d25(undefined8 param_1,byte param_2,ulong param_3);
ulong FUN_00406d71(ulong uParm1);
ulong FUN_00406de9(ulong uParm1);
undefined8 FUN_00406e3e(long lParm1);
ulong FUN_00406ebf(ulong uParm1,long lParm2);
void FUN_00406f55(long lParm1,undefined8 *puParm2);
long * FUN_00406f69(long *plParm1,long lParm2,undefined8 uParm3,char cParm4);
long * FUN_00406f8b(long lParm1,long *plParm2,long **pplParm3,char cParm4);
void FUN_00407047(long lParm1);
undefined8 FUN_0040706c(long lParm1,long **pplParm2,char cParm3);
undefined8 FUN_00407171(long lParm1);
long * FUN_00407176(long lParm1,long *plParm2);
long * FUN_004071bf(undefined8 uParm1,undefined1 *puParm2,undefined *puParm3,undefined *puParm4,long lParm5);
void FUN_0040729f(ulong **ppuParm1);
ulong FUN_0040734a(long *plParm1,undefined8 uParm2);
undefined8 FUN_00407487(long lParm1,long lParm2,long *plParm3);
undefined8 FUN_004076b7(undefined8 uParm1,undefined8 uParm2);
long FUN_004076e6(long lParm1,undefined8 uParm2);
char * FUN_004078c6(long lParm1,long lParm2);
long FUN_00407953(void);
int * FUN_004079bd(byte *pbParm1);
int * FUN_00407a56(int *piParm1,long lParm2);
ulong FUN_00407ab9(long lParm1,long lParm2);
undefined8 FUN_00407ad7(long lParm1,undefined8 uParm2,byte bParm3);
undefined8 FUN_00407b25(long lParm1,undefined8 uParm2,byte bParm3,char cParm4);
undefined8 FUN_00407b86(long lParm1,undefined8 uParm2,byte bParm3,char cParm4,char cParm5);
undefined8 FUN_00407bea(long lParm1,undefined8 uParm2,byte bParm3,char cParm4,char cParm5,char cParm6);
undefined8 FUN_00407c61(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7);
undefined8 FUN_00407ce5(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8);
undefined8 FUN_00407d7f(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined8 FUN_00407e22(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10);
undefined8 FUN_00407ed8(byte *param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10,char param_11);
undefined * FUN_00407f9d(char *pcParm1,int iParm2);
ulong FUN_00408060(undefined *param_1,ulong param_2,char *param_3,ulong param_4,uint param_5,ulong param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_00408fd2(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
ulong FUN_00409163(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
void FUN_00409196(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004091c4(ulong uParm1,undefined8 uParm2);
void FUN_00409270(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004092e4(void);
void FUN_004092f7(undefined8 uParm1,undefined8 uParm2);
void FUN_0040930c(undefined8 uParm1);
long FUN_00409322(uint uParm1,undefined8 uParm2,ulong uParm3);
void FUN_00409381(long lParm1);
ulong FUN_0040938a(uint uParm1);
void FUN_0040938d(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00409394(long lParm1,int iParm2,long lParm3,int iParm4);
ulong FUN_004093ad(uint uParm1);
void FUN_004093e1(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_00409625(void);
void FUN_00409677(void);
void FUN_004096fc(ulong uParm1,ulong uParm2);
void FUN_00409716(ulong uParm1,ulong uParm2);
void FUN_00409749(void);
undefined * FUN_00409757(long lParm1,long lParm2);
undefined * FUN_0040978a(void);
ulong FUN_004097b2(undefined8 param_1,ulong param_2,ulong param_3,ulong param_4,undefined8 param_5,undefined8 param_6,uint param_7);
void FUN_0040988e(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
undefined8 FUN_004098af(void);
ulong FUN_00409999(int iParm1);
ulong FUN_004099ae(ulong *puParm1,int iParm2);
ulong FUN_004099d8(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_00409a15(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_00409d4c(ulong uParm1,ulong uParm2);
ulong FUN_00409d96(undefined8 uParm1);
void FUN_00409db3(void);
void FUN_00409dd8(undefined8 uParm1);
void FUN_00409e18(undefined auParm1 [16],undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong uParm9,ulong uParm10,undefined8 uParm11,undefined8 uParm12,undefined8 uParm13,undefined8 uParm14);
void FUN_00409e6e(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_00409f41(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4);
void FUN_0040a0c0(long lParm1,int *piParm2);
char * FUN_0040a1a9(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_0040a22e(int param_1,undefined8 *param_2,char *param_3,char **param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
ulong FUN_0040a7c2(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
void FUN_0040ac65(void);
void FUN_0040acbb(void);
ulong FUN_0040acd1(undefined8 uParm1);
void FUN_0040adbe(void);
undefined8 FUN_0040adcc(char *pcParm1,long lParm2);
void FUN_0040ae43(long lParm1);
ulong FUN_0040ae5d(uint *puParm1,byte *pbParm2,long lParm3);
ulong * FUN_0040aebc(ulong *puParm1,undefined8 uParm2,ulong uParm3);
undefined8 FUN_0040af93(long *plParm1,long *plParm2);
void FUN_0040b01c(long lParm1,long lParm2);
undefined8 FUN_0040b048(uint uParm1,long lParm2,long lParm3,long lParm4,undefined8 uParm5);
void FUN_0040b135(void);
undefined8 FUN_0040b143(char *pcParm1,long lParm2);
ulong FUN_0040b1a8(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined FUN_0040b280(int iParm1);;
ulong FUN_0040b28a(uint uParm1);
ulong FUN_0040b299(byte *pbParm1,byte *pbParm2);
void FUN_0040b344(double dParm1);
undefined8 FUN_0040b40f(void);
ulong FUN_0040b417(ulong uParm1);
undefined * FUN_0040b4b2(void);
char * FUN_0040b78a(void);
void FUN_0040b85d(undefined8 uParm1);
undefined8 FUN_0040b87a(undefined8 uParm1);
void FUN_0040b8fd(undefined8 uParm1);
ulong FUN_0040b915(long lParm1);
undefined8 FUN_0040b997(void);
void FUN_0040b9aa(void);
ulong FUN_0040b9b8(char *pcParm1,long lParm2);
long FUN_0040b9ee(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
char * FUN_0040baf9(ulong uParm1,long lParm2,long lParm3);
ulong FUN_0040bc9c(void);
int * FUN_0040bcd0(undefined8 param_1,long *param_2);
int * FUN_0040bf0a(int *param_1,long *param_2);
int * FUN_0040c068(long **pplParm1,ulong uParm2,long **pplParm3,uint *puParm4,long **pplParm5,ulong uParm6);
long FUN_0040c5bf(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_0040ca5b(uint param_1);
uint * FUN_0040ca98(undefined8 uParm1,ulong *puParm2);
uint * FUN_0040ccc8(undefined8 uParm1,int *piParm2,ulong *puParm3);
void FUN_0040ce73(uint uParm1);
void FUN_0040cea5(char *param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
long * FUN_0040d026(long *plParm1,long **pplParm2,long lParm3,undefined8 uParm4);
double FUN_004127dd(int *piParm1);
void FUN_00412826(int *param_1);
long FUN_004128a7(ulong uParm1,ulong uParm2);
void FUN_004128b9(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004128cf(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_004128f5(ulong uParm1,ulong uParm2);
undefined8 FUN_00412900(uint *puParm1,ulong *puParm2);
undefined8 FUN_00412d2a(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_004137d6(undefined auParm1 [16]);
ulong FUN_004137f5(void);
void FUN_00413820(void);
undefined8 _DT_FINI(void);

