typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined3;
typedef unsigned int    undefined4;
typedef unsigned long    undefined5;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00403290(void);
void FUN_00403510(void);
void FUN_00403820(void);
void entry(void);
void FUN_00403890(void);
void FUN_00403900(void);
void FUN_00403990(void);
void FUN_004039c5(void);
void FUN_004039e9(void);
void FUN_00403a21(long lParm1);
ulong FUN_00403bf7(undefined8 uParm1);
void FUN_00403c68(long lParm1);
ulong FUN_00403d1e(undefined *puParm1);
ulong FUN_00403d89(long lParm1,undefined8 uParm2,long lParm3);
ulong FUN_00403e33(undefined8 uParm1,undefined8 uParm2,char cParm3,undefined8 uParm4);
void FUN_00403eb8(uint uParm1);
ulong FUN_00403fae(ulong uParm1,undefined8 *puParm2);
ulong FUN_004044c4(char *pcParm1);
long FUN_004044ed(long lParm1);
ulong FUN_004044f9(int iParm1);
ulong FUN_0040451e(char *pcParm1,uint uParm2);
void FUN_00404536(long lParm1);
long FUN_0040455c(undefined8 uParm1);
ulong FUN_0040458c(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00404604(undefined8 uParm1,undefined8 uParm2,long lParm3);
ulong FUN_0040464e(ulong uParm1,undefined8 uParm2,long lParm3);
ulong FUN_004046c9(long lParm1,long lParm2,char cParm3,char *pcParm4,int iParm5,int *piParm6);
undefined8 FUN_004049f5(long lParm1,long lParm2,long lParm3,char cParm4);
void FUN_00404b62(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00404b7a(long lParm1,long lParm2,long lParm3);
ulong FUN_00404f9c(long *plParm1,long lParm2);
ulong FUN_00405095(char *pcParm1);
long FUN_004050be(long lParm1,ulong uParm2);
undefined FUN_004050d5(int iParm1);;
long FUN_004050dc(void);
ulong FUN_00405100(int iParm1,undefined8 uParm2,uint uParm3);
undefined8 FUN_0040510f(long *plParm1,long *plParm2);
ulong FUN_00405143(int iParm1,undefined8 uParm2,uint uParm3);
undefined8 FUN_00405152(void);
ulong FUN_00405158(long lParm1);
ulong FUN_00405194(long lParm1,uint uParm2);
undefined8 FUN_004051b0(void);
undefined8 FUN_004051c9(void);
ulong FUN_004051e2(long lParm1);
undefined8 FUN_00405213(uint *puParm1);
void FUN_00405308(long lParm1);
ulong FUN_00405327(undefined8 uParm1,long *plParm2,undefined8 uParm3,long *plParm4,int *piParm5,undefined *puParm6);
ulong FUN_00405864(undefined8 uParm1,undefined8 uParm2);
void FUN_004058ab(long lParm1,undefined8 uParm2,long lParm3);
ulong FUN_00405972(long lParm1,undefined8 uParm2,long lParm3);
ulong FUN_004059f6(char *pcParm1,long *plParm2,char *pcParm3);
void FUN_00405b39(undefined8 uParm1,undefined8 uParm2,long lParm3);
ulong FUN_00405bb2(undefined8 uParm1,undefined8 uParm2,byte bParm3,byte bParm4,char cParm5);
void FUN_00405c7d(undefined8 uParm1,ulong uParm2);
undefined8 FUN_00405c97(uint uParm1,ulong uParm2);
ulong FUN_00405d2d(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00405d71(ulong uParm1,undefined8 uParm2,uint uParm3,long lParm4);
ulong FUN_00405e33(char *pcParm1,ulong uParm2);
ulong FUN_00405e77(uint param_1,uint param_2,long param_3,ulong param_4,ulong param_5,byte param_6,undefined8 param_7,undefined8 param_8,ulong param_9,long *param_10,byte *param_11);
ulong FUN_0040621f(ulong param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,long param_6,int param_7,undefined8 param_8,undefined8 param_9,undefined *param_10);
void FUN_0040660d(undefined8 uParm1,undefined8 uParm2,uint uParm3);
ulong FUN_0040662e(void);
ulong FUN_00406650(void);
ulong FUN_00406682(undefined8 uParm1,undefined8 uParm2,ulong uParm3,ulong uParm4,long lParm5);
ulong FUN_0040682d(undefined8 uParm1,ulong uParm2,ulong uParm3,long lParm4);
void FUN_004068d8(long lParm1);
void FUN_00406901(undefined8 *puParm1);
ulong FUN_0040695e(long lParm1);
ulong FUN_0040698f(long lParm1,undefined8 uParm2,uint uParm3,long lParm4,char cParm5,long lParm6);
ulong FUN_00406b48(void);
ulong FUN_00406b7e(undefined8 param_1,char *param_2,long param_3,uint param_4,uint param_5,byte *param_6,long *param_7);
ulong FUN_0040786f(byte *param_1,undefined8 param_2,byte param_3,long *param_4,long *param_5,uint *param_6,uint param_7,char *param_8,undefined *param_9,undefined *param_10);
void FUN_00409acb(undefined8 uParm1,undefined8 uParm2,ulong uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_00409b5b(undefined8 param_1,undefined8 param_2,byte param_3,undefined8 param_4,undefined8 param_5,undefined *param_6,byte *param_7,byte *param_8);
void FUN_00409d63(long lParm1);
void FUN_00409d7a(undefined8 uParm1,undefined8 uParm2);
long FUN_00409db1(undefined8 uParm1,undefined8 uParm2);
long FUN_00409ddb(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
long FUN_00409e4b(void);
undefined8 FUN_00409e84(void);
void FUN_00409e8a(undefined4 uParm1,undefined4 *puParm2);
ulong FUN_00409ebd(uint *puParm1);
long FUN_0040a185(long lParm1,long lParm2);
ulong FUN_0040a1f8(undefined4 uParm1,undefined8 uParm2,ulong uParm3,undefined8 uParm4,undefined4 uParm5,char cParm6);
ulong FUN_0040a311(undefined8 uParm1,ulong uParm2,undefined8 uParm3,char cParm4);
ulong FUN_0040a41b(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040a493(undefined8 uParm1);
long FUN_0040a4db(undefined8 uParm1,ulong uParm2);
ulong FUN_0040a5b2(long *plParm1,ulong uParm2,long lParm3,long lParm4,long *plParm5);
void FUN_0040a84e(long lParm1,long lParm2,undefined uParm3);
void FUN_0040a8fe(char *pcParm1);
long FUN_0040a93b(long lParm1,int iParm2,char cParm3);
void FUN_0040ab8f(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040aba2(undefined8 uParm1,char *pcParm2);
void FUN_0040abe4(undefined8 uParm1,char *pcParm2);
ulong FUN_0040ac15(ulong uParm1,ulong uParm2,ulong uParm3);
ulong FUN_0040ac83(long *plParm1,undefined8 uParm2,undefined8 uParm3);
char * FUN_0040acf9(char *pcParm1,ulong uParm2);
void FUN_0040b295(undefined8 uParm1);
long FUN_0040b41a(void);
void FUN_0040b4ad(char *pcParm1);
void FUN_0040b4c5(char *pcParm1);
undefined * FUN_0040b4fd(undefined8 uParm1);
char * FUN_0040b550(char *pcParm1);
void FUN_0040b59d(char *pcParm1);
undefined FUN_0040b5cf(char *pcParm1);;
void FUN_0040b602(void);
void FUN_0040b610(undefined8 param_1,byte param_2,ulong param_3);
void FUN_0040b65c(long lParm1,undefined8 uParm2,undefined8 *puParm3);
ulong FUN_0040b6cb(long lParm1,undefined8 uParm2,undefined8 *puParm3);
undefined * FUN_0040b702(long lParm1);
undefined8 FUN_0040b779(undefined8 uParm1);
void FUN_0040b7e1(uint uParm1,undefined *puParm2);
void FUN_0040b8f2(char *pcParm1);
void FUN_0040b90a(char *pcParm1);
long FUN_0040b91e(long lParm1,char *pcParm2,undefined8 *puParm3);
long FUN_0040b9fa(uint uParm1,long lParm2,long lParm3);
ulong FUN_0040ba5c(ulong uParm1);
ulong FUN_0040bad4(ulong uParm1);
undefined8 FUN_0040bb29(long lParm1);
ulong FUN_0040bbaa(ulong uParm1,long lParm2);
void FUN_0040bc40(long lParm1,undefined8 *puParm2);
long * FUN_0040bc54(long *plParm1,long lParm2,undefined8 uParm3,char cParm4);
long * FUN_0040bc76(long lParm1,long *plParm2,long **pplParm3,char cParm4);
void FUN_0040bd32(long lParm1);
undefined8 FUN_0040bd57(long lParm1,long **pplParm2,char cParm3);
long * FUN_0040be5c(long lParm1,long *plParm2);
long * FUN_0040bea5(undefined8 uParm1,undefined1 *puParm2,undefined *puParm3,undefined *puParm4,long lParm5);
void FUN_0040bf85(ulong **ppuParm1);
ulong FUN_0040c030(long *plParm1,undefined8 uParm2);
undefined8 FUN_0040c16d(long lParm1,long lParm2,long *plParm3);
undefined8 FUN_0040c39d(undefined8 uParm1,undefined8 uParm2);
long FUN_0040c3cc(long lParm1,undefined8 uParm2);
void FUN_0040c649(undefined8 *puParm1);
long FUN_0040c65f(byte *pbParm1);
undefined8 FUN_0040c6f8(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4,uint uParm5);
void FUN_0040c731(undefined8 uParm1,ulong uParm2,undefined4 uParm3);
int * FUN_0040c74d(int *piParm1,long lParm2);
ulong FUN_0040c7b0(long lParm1,long lParm2);
undefined8 FUN_0040c7ce(long lParm1,undefined8 uParm2,byte bParm3);
undefined8 FUN_0040c81c(long lParm1,undefined8 uParm2,byte bParm3,char cParm4);
undefined8 FUN_0040c87d(long lParm1,undefined8 uParm2,byte bParm3,char cParm4,char cParm5);
undefined8 FUN_0040c8e1(long lParm1,undefined8 uParm2,byte bParm3,char cParm4,char cParm5,char cParm6);
undefined8 FUN_0040c958(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7);
undefined8 FUN_0040c9dc(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8);
undefined8 FUN_0040ca76(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined8 FUN_0040cb19(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10);
undefined8 FUN_0040cbcf(byte *param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10,char param_11);
undefined * FUN_0040cc94(char *pcParm1,int iParm2);
ulong FUN_0040cd57(undefined *param_1,ulong param_2,char *param_3,ulong param_4,uint param_5,ulong param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_0040dcc9(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
ulong FUN_0040de5a(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
void FUN_0040de8d(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040debb(ulong uParm1,undefined8 uParm2);
void FUN_0040df67(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040dfdb(void);
void FUN_0040dfee(undefined8 uParm1,undefined8 uParm2);
void FUN_0040e003(undefined8 uParm1);
undefined8 FUN_0040e019(undefined4 uParm1);
ulong FUN_0040e02a(uint uParm1,char *pcParm2,uint uParm3,char *pcParm4,uint uParm5);
undefined8 * FUN_0040e400(undefined8 *puParm1);
long FUN_0040e43f(uint uParm1,undefined8 uParm2,ulong uParm3);
ulong FUN_0040e49e(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040e5e3(ulong uParm1,uint uParm2);
long FUN_0040e835(undefined8 uParm1,ulong uParm2);
undefined8 FUN_0040e88f(void);
undefined8 FUN_0040e8a8(void);
undefined8 FUN_0040e8c1(long lParm1);
undefined8 FUN_0040e8c6(long lParm1);
undefined8 FUN_0040e8cb(long lParm1);
void FUN_0040e8d0(long lParm1);
void FUN_0040e8d9(long lParm1);
ulong FUN_0040e8e2(uint uParm1);
void FUN_0040e8e5(void);
undefined FUN_0040e8f8(undefined8 uParm1,ulong uParm2);;
ulong FUN_0040e90e(char *pcParm1,int iParm2,undefined8 uParm3,code *pcParm4,long lParm5);
ulong FUN_0040ea61(uint uParm1);
ulong FUN_0040eaae(undefined8 uParm1,undefined8 *puParm2,long lParm3,ulong uParm4);
ulong FUN_0040f206(undefined8 *puParm1);
undefined8 FUN_0040f2ae(undefined8 uParm1,long *plParm2);
ulong FUN_0040f356(uint uParm1,long lParm2,undefined *puParm3);
void FUN_0040f6b9(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040f6d2(undefined8 uParm1,undefined *puParm2);
void FUN_0040f8bf(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_0040fb03(void);
void FUN_0040fb55(void);
ulong FUN_0040fbda(void);
void FUN_0040fc10(long lParm1);
void FUN_0040fc2a(void);
long FUN_0040fc38(long lParm1,ulong *puParm2);
void FUN_0040fc6b(long lParm1,ulong *puParm2,ulong uParm3);
void FUN_0040fca1(long lParm1,ulong *puParm2,ulong uParm3);
void FUN_0040fd2f(undefined8 uParm1,undefined8 uParm2);
void FUN_0040fd58(char *pcParm1);
void FUN_0040fd80(void);
void FUN_0040fda8(undefined8 uParm1,uint uParm2);
ulong FUN_0040fdeb(void);
ulong FUN_0040fe17(void);
undefined8 FUN_0040fe45(ulong uParm1,ulong uParm2);
ulong FUN_0040fe8f(undefined8 uParm1);
void FUN_0040feac(void);
void FUN_0040fed1(undefined8 uParm1);
void FUN_0040ff11(undefined auParm1 [16],undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong uParm9,ulong uParm10,undefined8 uParm11,undefined8 uParm12,undefined8 uParm13,undefined8 uParm14);
void FUN_0040ff67(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_0041003a(void);
ulong FUN_00410048(ulong uParm1,char *pcParm2);
ulong FUN_004100c2(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4);
undefined8 FUN_00410241(ulong uParm1);
void FUN_00410290(undefined8 uParm1);
ulong FUN_004102a8(long lParm1);
undefined8 FUN_0041032a(void);
void FUN_0041033d(void);
void FUN_0041034b(void);
void FUN_00410359(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0041036e(ulong uParm1,char *pcParm2,long lParm3,ulong uParm4);
void FUN_004104ab(long lParm1,undefined4 uParm2);
void FUN_004104f3(long lParm1,long lParm2);
long FUN_0041056c(char **ppcParm1);
undefined8 FUN_004105b3(long lParm1,long lParm2);
ulong FUN_00410620(long lParm1,long lParm2);
ulong FUN_00410645(long lParm1,long lParm2,char cParm3);
long FUN_004107d0(long lParm1,long lParm2,ulong uParm3);
long FUN_004108ac(long lParm1,undefined8 uParm2,long lParm3);
ulong FUN_0041093b(long lParm1);
void FUN_0041099a(long lParm1,undefined8 uParm2);
void FUN_004109ed(long lParm1);
void FUN_00410a26(long lParm1);
void FUN_00410a51(undefined8 uParm1);
long FUN_00410a78(undefined8 uParm1,undefined8 uParm2,uint uParm3,uint *puParm4);
undefined8 FUN_00410ace(long lParm1);
ulong FUN_00410bd6(void);
ulong FUN_00410bfc(void);
void FUN_00410c62(long lParm1,long lParm2);
undefined8 FUN_00410cf2(long lParm1,undefined8 *puParm2);
void FUN_00410d9c(long lParm1,uint uParm2,char cParm3);
ulong FUN_00410def(long lParm1);
ulong FUN_00410e43(long lParm1,long lParm2,uint uParm3,byte *pbParm4);
long FUN_00410fd4(long *plParm1,long lParm2);
long FUN_00411066(long *plParm1,int iParm2);
long * FUN_00411874(char **ppcParm1,ulong uParm2,long lParm3);
ulong FUN_00411be7(long *plParm1);
long FUN_00411d0f(long *plParm1);
undefined8 FUN_004122e0(undefined8 uParm1,long lParm2,uint uParm3);
long FUN_00412308(long lParm1,ulong uParm2);
void FUN_0041279b(long lParm1,int *piParm2);
char * FUN_00412884(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_00412909(int param_1,undefined8 *param_2,char *param_3,char **param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
ulong FUN_00412e9d(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
void FUN_00413340(void);
void FUN_00413396(void);
undefined8 FUN_004133ac(char *pcParm1,uint uParm2,uint uParm3);
ulong FUN_00413476(uint uParm1,char *pcParm2,uint uParm3,undefined8 uParm4);
ulong FUN_004135da(ulong uParm1,char *pcParm2,ulong uParm3,char *pcParm4,uint uParm5);
void FUN_00413727(void);
undefined8 FUN_00413735(char *pcParm1,long lParm2);
void FUN_004137ac(long lParm1);
ulong FUN_004137c6(uint *puParm1,byte *pbParm2,long lParm3);
ulong FUN_00413825(char *pcParm1,ulong uParm2);
undefined8 FUN_0041390b(char *pcParm1);
undefined8 FUN_00413966(char *pcParm1,uint uParm2,long lParm3);
void FUN_004139f3(void);
ulong FUN_00413a06(uint param_1,undefined8 param_2,uint param_3,ulong param_4);
undefined8 FUN_00413aed(char *pcParm1);
void FUN_00413b4b(long lParm1,long lParm2);
ulong FUN_00413b77(char *pcParm1,char *pcParm2);
void FUN_00413fff(void);
ulong FUN_00414013(char *pcParm1);
void FUN_00414091(void);
undefined8 FUN_0041409f(char *pcParm1,long lParm2);
undefined8 FUN_00414104(undefined8 uParm1,uint uParm2,char *pcParm3);
undefined8 FUN_00414173(char *pcParm1);
undefined8 FUN_00414258(ulong uParm1,char *pcParm2,ulong uParm3);
ulong FUN_00414363(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_00414432(undefined8 uParm1,undefined8 uParm2,undefined4 uParm3,undefined4 *puParm4);
void FUN_0041443a(undefined8 uParm1,int iParm2,uint uParm3);
ulong FUN_0041445e(uint *puParm1,undefined8 uParm2,uint uParm3);
void FUN_00414480(undefined8 uParm1,undefined8 uParm2);
long FUN_004144b7(char *pcParm1,char **ppcParm2,long lParm3,long lParm4);
void FUN_004145d1(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_0041462e(long *plParm1,long lParm2,long lParm3);
long FUN_00414701(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
undefined FUN_00414777(int iParm1);;
ulong FUN_00414781(uint uParm1);
ulong FUN_00414790(byte *pbParm1,byte *pbParm2);
undefined *FUN_004147d8(uint uParm1,undefined8 uParm2,undefined *puParm3,ulong uParm4,undefined1 *puParm5,code *pcParm6);
ulong FUN_0041497e(ulong uParm1,char cParm2);
ulong FUN_00414a48(ulong uParm1);
void FUN_00414a53(long lParm1);
undefined8 FUN_00414a63(long *plParm1,long *plParm2);
undefined8 FUN_00414afe(void);
void FUN_00414b06(undefined8 *puParm1);
ulong FUN_00414b48(ulong uParm1);
void FUN_00414c18(undefined4 *puParm1,undefined4 uParm2);
ulong FUN_00414c39(long lParm1);
ulong FUN_00414c3e(long lParm1,uint uParm2);
ulong FUN_00414c79(long lParm1,undefined8 uParm2,undefined8 uParm3,ulong uParm4);
undefined * FUN_00414cb3(void);
char * FUN_00414f8b(void);
void FUN_0041505e(void);
long FUN_004150a9(long lParm1);
void FUN_004150b1(undefined8 uParm1);
void FUN_004150d4(void);
ulong FUN_004150ef(undefined8 *puParm1,ulong uParm2);
void FUN_004151fb(undefined8 uParm1);
ulong FUN_00415213(undefined8 *puParm1);
void FUN_00415243(undefined8 uParm1,undefined8 uParm2);
void FUN_004152ba(long lParm1,ulong uParm2,ulong uParm3);
void FUN_00415505(undefined8 *puParm1,long lParm2,long lParm3);
void FUN_00415569(ulong *puParm1,ulong uParm2,ulong uParm3);
long FUN_0041565a(long lParm1,ulong uParm2);
void FUN_00415708(long *plParm1);
undefined8 FUN_00415727(long *plParm1);
undefined8 FUN_0041575e(undefined8 uParm1);
undefined8 FUN_00415762(long lParm1,undefined8 uParm2);
void FUN_0041576d(ulong *puParm1,long *plParm2);
void FUN_00415a86(ulong *puParm1);
void FUN_00415dfc(undefined8 uParm1);
undefined8 FUN_00415e19(char *pcParm1,uint uParm2,uint uParm3);
void FUN_00415f39(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00415f4f(undefined8 uParm1);
void FUN_00415fd2(void);
ulong FUN_004160eb(char *pcParm1,long lParm2);
long FUN_00416121(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
char * FUN_0041622c(ulong uParm1,long lParm2,long lParm3);
ulong FUN_004163cf(void);
int * FUN_00416403(undefined8 param_1,long *param_2);
int * FUN_0041663d(int *param_1,long *param_2);
int * FUN_0041679b(long **pplParm1,ulong uParm2,long **pplParm3,uint *puParm4,long **pplParm5,ulong uParm6);
long FUN_00416cf2(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_0041718e(uint param_1);
uint * FUN_004171cb(undefined8 uParm1,ulong *puParm2);
uint * FUN_004173fb(undefined8 uParm1,int *piParm2,ulong *puParm3);
void FUN_004175a6(uint uParm1);
void FUN_004175d8(char *param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
long * FUN_00417759(long *plParm1,long **pplParm2,long lParm3,undefined8 uParm4);
long FUN_0041cf10(undefined8 uParm1,undefined8 uParm2);
double FUN_0041cfa0(int *piParm1);
void FUN_0041cfe9(int *param_1);
long FUN_0041d06a(ulong uParm1,ulong uParm2);
void FUN_0041d07c(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0041d092(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_0041d0b8(ulong uParm1,ulong uParm2);
undefined8 FUN_0041d0c3(uint *puParm1,ulong *puParm2);
undefined8 FUN_0041d4ed(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_0041df99(undefined auParm1 [16]);
ulong FUN_0041dfb8(void);
void FUN_0041dfe0(void);
undefined8 _DT_FINI(void);

