typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00402220(void);
void FUN_004024a0(void);
void entry(void);
void FUN_004026d0(void);
void FUN_00402740(void);
void FUN_004027d0(void);
ulong FUN_00402805(uint uParm1);
void FUN_00402808(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00402819(undefined8 uParm1,long lParm2);
void FUN_0040282f(void);
void FUN_00402853(long lParm1);
void FUN_00402a29(void);
undefined8 FUN_00402ab2(char cParm1,char cParm2,char cParm3);
void FUN_00402b96(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_00402c38(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00402c6a(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00402c9c(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00402cce(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00402d00(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00402d32(undefined8 uParm1,undefined8 uParm2);
undefined * FUN_00402d5c(ulong *puParm1);
void FUN_0040376c(char cParm1);
ulong FUN_004037e7(char *pcParm1,uint uParm2,undefined8 uParm3,code *pcParm4,undefined8 uParm5);
ulong FUN_00403ac9(byte *pbParm1,undefined8 uParm2);
ulong FUN_00403b89(byte *pbParm1,undefined8 uParm2,undefined8 uParm3);
undefined1 * FUN_00403c89(undefined8 uParm1);
char ** FUN_00403ca8(undefined8 uParm1);
undefined8 FUN_00403d96(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,long lParm4);
void FUN_00403e9b(void);
undefined1 * FUN_00403ea9(undefined8 uParm1,ulong uParm2);
void FUN_00403f51(char *pcParm1,char *pcParm2,undefined8 uParm3,long lParm4,long lParm5);
ulong FUN_00404232(long lParm1,long lParm2,undefined8 uParm3);
void FUN_00404781(uint uParm1);
ulong FUN_0040491b(uint uParm1,undefined8 *puParm2);
long FUN_00404b81(undefined8 uParm1,undefined *puParm2);
long FUN_00404ee9(undefined8 uParm1,ulong uParm2);
long FUN_00404fc0(char *pcParm1,char **ppcParm2,long lParm3,long lParm4);
ulong FUN_004050da(long *plParm1,undefined8 uParm2,undefined8 uParm3);
char * FUN_00405150(char *pcParm1,ulong uParm2);
void FUN_004056ec(undefined8 uParm1);
void FUN_004057a5(char *pcParm1);
void FUN_004057bd(char *pcParm1);
undefined * FUN_004057f5(undefined8 uParm1);
char * FUN_00405848(char *pcParm1);
void FUN_00405895(long lParm1,undefined8 uParm2,undefined8 *puParm3);
ulong FUN_00405904(long lParm1,undefined8 uParm2,undefined8 *puParm3);
undefined * FUN_0040593b(long lParm1);
undefined8 FUN_004059b2(undefined8 uParm1);
void FUN_00405a1a(uint uParm1,undefined *puParm2);
void FUN_00405b2b(long lParm1);
ulong FUN_00405b34(ulong uParm1);
ulong FUN_00405bac(ulong uParm1);
undefined8 FUN_00405c01(long lParm1);
ulong FUN_00405c82(ulong uParm1,long lParm2);
void FUN_00405d18(long lParm1,undefined8 *puParm2);
long * FUN_00405d2c(long *plParm1,long lParm2,undefined8 uParm3,char cParm4);
long * FUN_00405d4e(long lParm1,long *plParm2,long **pplParm3,char cParm4);
void FUN_00405e0a(long lParm1);
undefined8 FUN_00405e2f(long lParm1,long **pplParm2,char cParm3);
long * FUN_00405f34(long lParm1,long *plParm2);
long * FUN_00405f7d(undefined8 uParm1,undefined1 *puParm2,undefined *puParm3,undefined *puParm4,long lParm5);
void FUN_0040605d(ulong **ppuParm1);
ulong FUN_00406108(long *plParm1,undefined8 uParm2);
undefined8 FUN_00406245(long lParm1,long lParm2,long *plParm3);
undefined8 FUN_00406475(undefined8 uParm1,undefined8 uParm2);
void FUN_0040650a(undefined8 *puParm1);
char * FUN_00406520(long lParm1,long lParm2);
ulong FUN_004065ad(int iParm1,int iParm2);
long FUN_004065e1(long lParm1,long lParm2,long lParm3);
long FUN_00406614(long lParm1,long lParm2,long lParm3);
char * FUN_00406647(char *pcParm1,long lParm2,char *pcParm3,uint *puParm4,undefined uParm5,undefined8 uParm6,undefined8 uParm7,uint uParm8);
void FUN_00407eae(void);
int * FUN_00407ed0(byte *pbParm1);
int * FUN_00407f69(int *piParm1,long lParm2);
ulong FUN_00407fcc(long lParm1,long lParm2);
undefined8 FUN_00407fea(long lParm1,undefined8 uParm2,byte bParm3);
undefined8 FUN_00408038(long lParm1,undefined8 uParm2,byte bParm3,char cParm4);
undefined8 FUN_00408099(long lParm1,undefined8 uParm2,byte bParm3,char cParm4,char cParm5);
undefined8 FUN_004080fd(long lParm1,undefined8 uParm2,byte bParm3,char cParm4,char cParm5,char cParm6);
undefined8 FUN_00408174(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7);
undefined8 FUN_004081f8(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8);
undefined8 FUN_00408292(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined8 FUN_00408335(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10);
undefined8 FUN_004083eb(byte *param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10,char param_11);
undefined * FUN_004084b0(char *pcParm1,int iParm2);
ulong FUN_00408573(undefined *param_1,ulong param_2,char *param_3,ulong param_4,uint param_5,ulong param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_004094e5(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
ulong FUN_00409676(undefined1 *puParm1);
void FUN_00409685(undefined1 *puParm1,undefined4 uParm2);
void FUN_004096c7(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004096f5(ulong uParm1,undefined8 uParm2);
void FUN_004097a1(void);
void FUN_004097b4(undefined8 uParm1,undefined8 uParm2);
void FUN_004097c9(undefined8 uParm1);
ulong FUN_004097df(int *piParm1);
void FUN_0040982e(uint *puParm1);
void FUN_0040984f(int *piParm1);
undefined8 FUN_0040986b(void);
undefined8 FUN_00409884(void);
void FUN_0040989d(long lParm1);
void FUN_004098a6(long lParm1);
void FUN_004098af(long lParm1);
void FUN_004098b8(void);
ulong FUN_004098c7(uint uParm1);
void FUN_004098ca(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_00409b0e(void);
void FUN_00409b60(void);
void FUN_00409be5(long lParm1);
void FUN_00409bff(void);
long FUN_00409c0d(long lParm1,long lParm2);
void FUN_00409c40(undefined8 uParm1,undefined8 uParm2);
void FUN_00409c69(char *pcParm1);
long FUN_00409c91(void);
long FUN_00409cb9(void);
void FUN_00409ce5(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_00409d79(ulong uParm1,ulong uParm2);
void FUN_00409dc3(undefined4 *puParm1);
long FUN_00409dca(long lParm1);
void FUN_00409ddd(uint *puParm1);
void FUN_00409ded(int *piParm1);
undefined8 FUN_00409e21(uint *puParm1,undefined8 uParm2);
ulong FUN_00409e5e(char *pcParm1);
ulong FUN_0040a0e8(undefined8 uParm1);
void FUN_0040a105(void);
void FUN_0040a12a(undefined8 uParm1);
void FUN_0040a16a(undefined auParm1 [16],undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong uParm9,ulong uParm10,undefined8 uParm11,undefined8 uParm12,undefined8 uParm13,undefined8 uParm14);
void FUN_0040a1c0(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
long FUN_0040a293(long lParm1,ulong uParm2);
void FUN_0040a726(long lParm1,int *piParm2);
char * FUN_0040a80f(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_0040a894(int param_1,undefined8 *param_2,char *param_3,char **param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
ulong FUN_0040ae28(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
void FUN_0040b2cb(void);
void FUN_0040b321(void);
void FUN_0040b337(void);
undefined8 FUN_0040b345(char *pcParm1,long lParm2);
void FUN_0040b3bc(long lParm1);
ulong FUN_0040b3d6(uint *puParm1,byte *pbParm2,long lParm3);
ulong * FUN_0040b435(ulong *puParm1,undefined8 uParm2,ulong uParm3);
undefined8 FUN_0040b50c(void);
void FUN_0040b514(char *pcParm1);
void FUN_0040b5a2(undefined8 *puParm1);
byte * FUN_0040b5db(void);
void FUN_0040bd98(void);
ulong FUN_0040bdab(uint param_1,undefined8 param_2,uint param_3,ulong param_4);
undefined8 FUN_0040be92(char *pcParm1);
void FUN_0040bef0(long lParm1,long lParm2);
void FUN_0040bf1c(void);
undefined8 FUN_0040bf2a(char *pcParm1,long lParm2);
ulong FUN_0040bf8f(long lParm1,ulong uParm2,long *plParm3);
long FUN_0040c0b3(long lParm1,long lParm2,long lParm3,ulong uParm4);
byte * FUN_0040c2ee(long lParm1,byte *pbParm2,byte *pbParm3,byte *pbParm4);
char * FUN_0040c607(char *pcParm1,char *pcParm2);
ulong FUN_0040c6aa(uint uParm1,uint uParm2);
ulong FUN_0040c6ca(uint *puParm1,uint *puParm2);
void FUN_0040c70f(long lParm1,undefined8 uParm2,long lParm3);
void FUN_0040c72c(void);
void FUN_0040c73f(long lParm1);
ulong FUN_0040c76b(long lParm1);
undefined8 * FUN_0040c7a2(char *pcParm1);
undefined8 FUN_0040c82d(long *plParm1,char *pcParm2);
void FUN_0040c960(long *plParm1);
long FUN_0040c982(long lParm1);
ulong FUN_0040ca10(long lParm1);
long FUN_0040ca59(long lParm1,undefined8 uParm2,long lParm3);
long FUN_0040cae6(long lParm1,undefined8 uParm2);
void FUN_0040cb8e(long lParm1);
void FUN_0040cbad(void);
ulong FUN_0040cc55(char *pcParm1);
ulong FUN_0040ccae(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined FUN_0040cd86(int iParm1);;
ulong FUN_0040cd90(uint uParm1);
ulong FUN_0040cd9f(byte *pbParm1,byte *pbParm2);
ulong FUN_0040cde7(ulong uParm1,char cParm2);
void FUN_0040ceb1(undefined8 param_1,byte param_2,ulong param_3);
undefined8 FUN_0040cefd(void);
ulong FUN_0040cf05(ulong uParm1);
undefined * FUN_0040cfd5(void);
char * FUN_0040d2ad(void);
ulong FUN_0040d380(uint uParm1);
undefined * FUN_0040d3b4(long lParm1,uint *puParm2);
undefined8 FUN_0040d4f7(char *pcParm1,undefined8 uParm2);
undefined8 FUN_0040d55e(undefined8 uParm1);
ulong FUN_0040d5e1(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4);
undefined8 FUN_0040d760(ulong uParm1);
void FUN_0040d7af(undefined8 uParm1);
ulong FUN_0040d7c7(long lParm1);
undefined8 FUN_0040d849(void);
void FUN_0040d85c(void);
void FUN_0040d86a(void);
void FUN_0040d878(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040d88d(ulong uParm1,char *pcParm2,long lParm3,ulong uParm4);
void FUN_0040d95b(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_0040d971(long lParm1,undefined4 uParm2);
ulong FUN_0040d97a(ulong uParm1);
ulong FUN_0040d9eb(uint uParm1,uint uParm2);
long FUN_0040da0b(ulong param_1,long param_2,int param_3,int param_4,int param_5,uint param_6,int param_7,int param_8,int param_9,int param_10);
long FUN_0040db79(undefined8 uParm1,undefined8 uParm2);
long FUN_0040dbae(void);
void FUN_0040dc45(code *pcParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_0040dc63(undefined8 uParm1,long *plParm2,undefined8 uParm3);
long FUN_0040dd03(uint *puParm1,undefined8 uParm2,long *plParm3);
void FUN_0040e0f9(undefined8 uParm1);
ulong FUN_0040e116(char *pcParm1,char *pcParm2,ulong uParm3);
ulong FUN_0040e231(char *pcParm1,long lParm2);
long FUN_0040e267(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
char * FUN_0040e372(ulong uParm1,long lParm2,long lParm3);
ulong FUN_0040e515(void);
int * FUN_0040e549(undefined8 param_1,long *param_2);
int * FUN_0040e783(int *param_1,long *param_2);
int * FUN_0040e8e1(long **pplParm1,ulong uParm2,long **pplParm3,uint *puParm4,long **pplParm5,ulong uParm6);
long FUN_0040ee38(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_0040f2d4(uint param_1);
uint * FUN_0040f311(undefined8 uParm1,ulong *puParm2);
uint * FUN_0040f541(undefined8 uParm1,int *piParm2,ulong *puParm3);
void FUN_0040f6ec(uint uParm1);
void FUN_0040f71e(char *param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
long * FUN_0040f89f(long *plParm1,long **pplParm2,long lParm3,undefined8 uParm4);
ulong FUN_00415056(long *plParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 * FUN_004150b2(ulong uParm1);
void FUN_00415126(ulong uParm1);
double FUN_004151a2(int *piParm1);
void FUN_004151eb(int *param_1);
void FUN_0041526c(undefined8 uParm1);
long FUN_00415289(ulong uParm1,ulong uParm2);
void FUN_0041529b(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004152b1(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_004152d7(ulong uParm1,ulong uParm2);
ulong FUN_004152e2(long *plParm1,ulong *puParm2,int iParm3,long lParm4);
undefined8 FUN_00415409(uint *puParm1,ulong *puParm2);
undefined8 FUN_00415833(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_004162df(undefined auParm1 [16]);
ulong FUN_004162fe(void);
void FUN_00416330(void);
undefined8 _DT_FINI(void);

