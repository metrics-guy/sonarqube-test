typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined5;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00403270(void);
void FUN_004034d0(void);
void FUN_004037e0(void);
void entry(void);
void FUN_00403850(void);
void FUN_004038c0(void);
void FUN_00403950(void);
undefined8 FUN_00403985(undefined8 uParm1);
void FUN_00403989(void);
void FUN_004039ad(void);
void FUN_004039e5(long lParm1);
ulong FUN_00403bbb(undefined8 uParm1,long lParm2,undefined *puParm3);
void FUN_00403c37(long lParm1);
void FUN_00403cee(undefined8 uParm1,undefined8 uParm2,char cParm3);
undefined8 FUN_00403e19(char *pcParm1,long lParm2,long lParm3,long *plParm4,char *pcParm5,long lParm6);
ulong FUN_004043d1(char *pcParm1,long lParm2,long lParm3,long lParm4);
void FUN_004045e6(uint uParm1);
ulong FUN_004047c2(ulong uParm1,char **ppcParm2,char *pcParm3,char cParm4,undefined1 *puParm5);
ulong FUN_00404c61(uint uParm1,undefined8 *puParm2);
ulong FUN_004053c2(char *pcParm1);
long FUN_004053eb(long lParm1,ulong uParm2);
undefined FUN_00405402(int iParm1);;
long FUN_00405409(void);
ulong FUN_0040542d(int iParm1,undefined8 uParm2,uint uParm3);
undefined8 FUN_0040543c(long *plParm1,long *plParm2);
ulong FUN_00405470(int iParm1,undefined8 uParm2,uint uParm3);
undefined8 FUN_0040547f(void);
ulong FUN_00405485(long lParm1);
ulong FUN_004054c1(long lParm1,uint uParm2);
undefined8 FUN_004054dd(void);
undefined8 FUN_004054f6(void);
ulong FUN_0040550f(long lParm1);
undefined8 FUN_00405540(uint *puParm1);
void FUN_00405635(long lParm1);
ulong FUN_00405654(undefined8 uParm1,long *plParm2,undefined8 uParm3,long *plParm4,int *piParm5,undefined *puParm6);
ulong FUN_00405b91(undefined8 uParm1,undefined8 uParm2);
void FUN_00405bd8(long lParm1,undefined8 uParm2,long lParm3);
ulong FUN_00405c9f(long lParm1,undefined8 uParm2,long lParm3);
ulong FUN_00405d23(char *pcParm1,long *plParm2,char *pcParm3);
void FUN_00405e66(undefined8 uParm1,undefined8 uParm2,long lParm3);
ulong FUN_00405edf(undefined8 uParm1,undefined8 uParm2,byte bParm3,byte bParm4,char cParm5);
void FUN_00405faa(undefined8 uParm1,ulong uParm2);
undefined8 FUN_00405fc4(uint uParm1,ulong uParm2);
ulong FUN_0040605a(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040609e(ulong uParm1,undefined8 uParm2,uint uParm3,long lParm4);
ulong FUN_00406160(char *pcParm1,ulong uParm2);
ulong FUN_004061a4(uint param_1,uint param_2,long param_3,ulong param_4,ulong param_5,byte param_6,undefined8 param_7,undefined8 param_8,ulong param_9,long *param_10,byte *param_11);
ulong FUN_0040654c(ulong param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,long param_6,int param_7,undefined8 param_8,undefined8 param_9,undefined *param_10);
void FUN_0040693a(undefined8 uParm1,undefined8 uParm2,uint uParm3);
ulong FUN_0040695b(void);
ulong FUN_0040697d(void);
ulong FUN_004069af(undefined8 uParm1,undefined8 uParm2,ulong uParm3,ulong uParm4,long lParm5);
ulong FUN_00406b5a(undefined8 uParm1,ulong uParm2,ulong uParm3,long lParm4);
void FUN_00406c05(long lParm1);
void FUN_00406c2e(long lParm1);
void FUN_00406c57(undefined8 *puParm1);
ulong FUN_00406cb4(long lParm1);
ulong FUN_00406ce5(long lParm1,undefined8 uParm2,uint uParm3,long lParm4,char cParm5,long lParm6);
ulong FUN_00406e9e(void);
ulong FUN_00406ed4(undefined8 param_1,char *param_2,long param_3,uint param_4,uint param_5,byte *param_6,long *param_7);
ulong FUN_00407bc5(byte *param_1,undefined8 param_2,byte param_3,long *param_4,long *param_5,uint *param_6,uint param_7,char *param_8,undefined *param_9,undefined *param_10);
void FUN_00409e21(undefined8 uParm1,undefined8 uParm2,ulong uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_00409eb1(undefined8 param_1,undefined8 param_2,byte param_3,undefined8 param_4,undefined8 param_5,undefined *param_6,byte *param_7,byte *param_8);
void FUN_0040a0b9(long lParm1);
void FUN_0040a0d0(undefined8 uParm1,undefined8 uParm2);
long FUN_0040a107(undefined8 uParm1,undefined8 uParm2);
long FUN_0040a131(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
long FUN_0040a1a1(void);
undefined8 FUN_0040a1da(void);
void FUN_0040a1e0(undefined4 uParm1,undefined4 *puParm2);
ulong FUN_0040a213(uint *puParm1);
long FUN_0040a4db(long lParm1,long lParm2);
ulong FUN_0040a54e(undefined4 uParm1,undefined8 uParm2,ulong uParm3,undefined8 uParm4,undefined4 uParm5,char cParm6);
ulong FUN_0040a667(undefined8 uParm1,ulong uParm2,undefined8 uParm3,char cParm4);
ulong FUN_0040a771(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040a7e9(undefined8 uParm1);
long FUN_0040a831(undefined8 uParm1,ulong uParm2);
long FUN_0040a91b(char *pcParm1,char **ppcParm2,long lParm3,long lParm4);
void FUN_0040aa35(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_0040aa92(long *plParm1,long lParm2,long lParm3);
long FUN_0040ab65(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
ulong FUN_0040abd2(long *plParm1,ulong uParm2,long lParm3,long lParm4,long *plParm5);
void FUN_0040ae6e(long lParm1,long lParm2,undefined uParm3);
void FUN_0040af1e(char *pcParm1);
long FUN_0040af5b(long lParm1,int iParm2,char cParm3);
void FUN_0040b1af(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040b1c2(undefined8 uParm1,char *pcParm2);
ulong FUN_0040b1df(undefined8 uParm1,char *pcParm2);
void FUN_0040b221(undefined8 uParm1,char *pcParm2);
ulong FUN_0040b252(ulong uParm1,ulong uParm2,ulong uParm3);
ulong FUN_0040b2c0(long *plParm1,undefined8 uParm2,undefined8 uParm3);
char * FUN_0040b336(char *pcParm1,ulong uParm2);
void FUN_0040b8d2(undefined8 uParm1);
long FUN_0040ba57(void);
void FUN_0040baea(char *pcParm1);
void FUN_0040bb02(char *pcParm1);
undefined * FUN_0040bb3a(undefined8 uParm1);
char * FUN_0040bb8d(char *pcParm1);
void FUN_0040bbda(char *pcParm1);
undefined FUN_0040bc0c(char *pcParm1);;
void FUN_0040bc3f(void);
void FUN_0040bc4d(undefined8 param_1,byte param_2,ulong param_3);
void FUN_0040bc99(long lParm1,undefined8 uParm2,undefined8 *puParm3);
ulong FUN_0040bd08(long lParm1,undefined8 uParm2,undefined8 *puParm3);
undefined8 FUN_0040bd3f(undefined8 uParm1);
void FUN_0040bda7(uint uParm1,undefined *puParm2);
void FUN_0040beb8(char *pcParm1);
void FUN_0040bed0(char *pcParm1);
long FUN_0040bee4(long lParm1,char *pcParm2,undefined8 *puParm3);
long FUN_0040bfc0(uint uParm1,long lParm2,long lParm3);
ulong FUN_0040c022(ulong uParm1);
ulong FUN_0040c09a(ulong uParm1);
undefined8 FUN_0040c0ef(long lParm1);
ulong FUN_0040c170(ulong uParm1,long lParm2);
void FUN_0040c206(long lParm1,undefined8 *puParm2);
long * FUN_0040c21a(long *plParm1,long lParm2,undefined8 uParm3,char cParm4);
long * FUN_0040c23c(long lParm1,long *plParm2,long **pplParm3,char cParm4);
void FUN_0040c2f8(long lParm1);
undefined8 FUN_0040c31d(long lParm1,long **pplParm2,char cParm3);
long * FUN_0040c422(long lParm1,long *plParm2);
long * FUN_0040c46b(undefined8 uParm1,undefined1 *puParm2,undefined *puParm3,undefined *puParm4,long lParm5);
void FUN_0040c54b(ulong **ppuParm1);
ulong FUN_0040c5f6(long *plParm1,undefined8 uParm2);
undefined8 FUN_0040c733(long lParm1,long lParm2,long *plParm3);
undefined8 FUN_0040c963(undefined8 uParm1,undefined8 uParm2);
long FUN_0040c992(long lParm1,undefined8 uParm2);
void FUN_0040cc1f(undefined8 *puParm1);
long FUN_0040cc35(byte *pbParm1);
undefined8 FUN_0040ccce(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4,uint uParm5);
void FUN_0040cd07(undefined8 uParm1,ulong uParm2,undefined4 uParm3);
int * FUN_0040cd23(int *piParm1,long lParm2);
ulong FUN_0040cd86(long lParm1,long lParm2);
undefined8 FUN_0040cda4(long lParm1,undefined8 uParm2,byte bParm3);
undefined8 FUN_0040cdf2(long lParm1,undefined8 uParm2,byte bParm3,char cParm4);
undefined8 FUN_0040ce53(long lParm1,undefined8 uParm2,byte bParm3,char cParm4,char cParm5);
undefined8 FUN_0040ceb7(long lParm1,undefined8 uParm2,byte bParm3,char cParm4,char cParm5,char cParm6);
undefined8 FUN_0040cf2e(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7);
undefined8 FUN_0040cfb2(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8);
undefined8 FUN_0040d04c(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined8 FUN_0040d0ef(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10);
undefined8 FUN_0040d1a5(byte *param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10,char param_11);
undefined * FUN_0040d26a(char *pcParm1,int iParm2);
ulong FUN_0040d32d(undefined *param_1,ulong param_2,char *param_3,ulong param_4,uint param_5,ulong param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_0040e29f(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
ulong FUN_0040e430(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
void FUN_0040e463(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040e491(ulong uParm1,undefined8 uParm2);
void FUN_0040e53d(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040e5b1(void);
void FUN_0040e5c4(undefined8 uParm1,undefined8 uParm2);
void FUN_0040e5d9(undefined8 uParm1);
undefined8 FUN_0040e5ef(undefined4 uParm1);
ulong FUN_0040e600(uint uParm1,char *pcParm2,uint uParm3,char *pcParm4,uint uParm5);
long FUN_0040e9d6(uint uParm1,undefined8 uParm2,ulong uParm3);
ulong FUN_0040ea35(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040eb7a(ulong uParm1,uint uParm2);
long FUN_0040edcc(undefined8 uParm1,ulong uParm2);
undefined8 FUN_0040ee26(void);
undefined8 FUN_0040ee3f(void);
undefined8 FUN_0040ee58(long lParm1);
undefined8 FUN_0040ee5d(long lParm1);
undefined8 FUN_0040ee62(long lParm1);
void FUN_0040ee67(long lParm1);
void FUN_0040ee70(long lParm1);
ulong FUN_0040ee79(uint uParm1);
void FUN_0040ee7c(void);
undefined FUN_0040ee8f(undefined8 uParm1,ulong uParm2);;
ulong FUN_0040eea5(char *pcParm1,int iParm2,undefined8 uParm3,code *pcParm4,long lParm5);
ulong FUN_0040eff8(uint uParm1);
ulong FUN_0040f045(undefined8 uParm1,undefined8 *puParm2,long lParm3,ulong uParm4);
ulong FUN_0040f79d(undefined8 *puParm1);
undefined8 FUN_0040f845(undefined8 uParm1,long *plParm2);
ulong FUN_0040f8ed(uint uParm1,long lParm2,undefined *puParm3);
void FUN_0040fc50(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040fc69(undefined8 uParm1,undefined *puParm2);
void FUN_0040fe56(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_0041009a(void);
void FUN_004100ec(void);
ulong FUN_00410171(void);
void FUN_004101a7(long lParm1);
void FUN_004101c1(void);
long FUN_004101cf(long lParm1,ulong *puParm2);
void FUN_00410202(long lParm1,ulong *puParm2,ulong uParm3);
void FUN_00410238(long lParm1,ulong *puParm2,ulong uParm3);
void FUN_004102c6(undefined8 uParm1,undefined8 uParm2);
void FUN_004102ef(char *pcParm1);
ulong FUN_00410317(void);
ulong FUN_0041033f(void);
ulong FUN_0041036b(void);
undefined8 FUN_00410399(ulong uParm1,ulong uParm2);
ulong FUN_004103e3(undefined8 uParm1);
void FUN_00410400(void);
void FUN_00410425(undefined8 uParm1);
void FUN_00410465(undefined auParm1 [16],undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong uParm9,ulong uParm10,undefined8 uParm11,undefined8 uParm12,undefined8 uParm13,undefined8 uParm14);
void FUN_004104bb(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_0041058e(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4);
undefined8 FUN_0041070d(ulong uParm1);
void FUN_0041075c(undefined8 uParm1);
ulong FUN_00410774(long lParm1);
undefined8 FUN_004107f6(void);
void FUN_00410809(void);
void FUN_00410817(void);
void FUN_00410825(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0041083a(ulong uParm1,char *pcParm2,long lParm3,ulong uParm4);
long FUN_00410908(long lParm1,ulong uParm2);
void FUN_00410d9b(long lParm1,int *piParm2);
char * FUN_00410e84(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_00410f09(int param_1,undefined8 *param_2,char *param_3,char **param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
ulong FUN_0041149d(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
void FUN_00411940(void);
void FUN_00411996(void);
undefined8 FUN_004119ac(char *pcParm1,uint uParm2,uint uParm3);
ulong FUN_00411a76(uint uParm1,char *pcParm2,uint uParm3,undefined8 uParm4);
ulong FUN_00411bda(ulong uParm1,char *pcParm2,ulong uParm3,char *pcParm4,uint uParm5);
void FUN_00411d27(void);
undefined8 FUN_00411d35(char *pcParm1,long lParm2);
void FUN_00411dac(long lParm1);
ulong FUN_00411dc6(uint *puParm1,byte *pbParm2,long lParm3);
ulong FUN_00411e25(char *pcParm1,ulong uParm2);
undefined8 FUN_00411f0b(char *pcParm1);
undefined8 FUN_00411f66(char *pcParm1,uint uParm2,long lParm3);
void FUN_00411ff3(void);
ulong FUN_00412006(uint param_1,undefined8 param_2,uint param_3,ulong param_4);
undefined8 FUN_004120ed(char *pcParm1);
void FUN_0041214b(long lParm1,long lParm2);
ulong FUN_00412177(char *pcParm1,char *pcParm2);
void FUN_004125ff(void);
ulong FUN_00412613(char *pcParm1);
void FUN_00412691(void);
undefined8 FUN_0041269f(char *pcParm1,long lParm2);
undefined8 FUN_00412704(undefined8 uParm1,uint uParm2,char *pcParm3);
undefined8 FUN_00412773(char *pcParm1);
undefined8 FUN_00412858(ulong uParm1,char *pcParm2,ulong uParm3);
ulong FUN_00412963(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_00412a32(undefined8 uParm1,undefined8 uParm2,undefined4 uParm3,undefined4 *puParm4);
void FUN_00412a3a(undefined8 uParm1,int iParm2,uint uParm3);
ulong FUN_00412a5e(uint *puParm1,undefined8 uParm2,uint uParm3);
void FUN_00412a80(undefined8 uParm1,undefined8 uParm2);
undefined FUN_00412aad(int iParm1);;
ulong FUN_00412ab7(uint uParm1);
ulong FUN_00412ac6(byte *pbParm1,byte *pbParm2);
undefined *FUN_00412b0e(uint uParm1,undefined8 uParm2,undefined *puParm3,ulong uParm4,undefined1 *puParm5,code *pcParm6);
ulong FUN_00412cb4(ulong uParm1,char cParm2);
undefined8 FUN_00412d7e(void);
void FUN_00412d86(undefined8 *puParm1);
ulong FUN_00412dc8(ulong uParm1);
undefined * FUN_00412e98(void);
char * FUN_00413170(void);
long FUN_00413243(long lParm1);
void FUN_0041324b(undefined8 uParm1);
void FUN_0041326e(void);
ulong FUN_00413289(undefined8 *puParm1,ulong uParm2);
void FUN_00413395(undefined8 uParm1);
ulong FUN_004133ad(undefined8 *puParm1);
void FUN_004133dd(undefined8 uParm1,undefined8 uParm2);
void FUN_00413454(long lParm1,ulong uParm2,ulong uParm3);
void FUN_0041369f(undefined8 *puParm1,long lParm2,long lParm3);
void FUN_00413703(ulong *puParm1,ulong uParm2,ulong uParm3);
long FUN_004137f4(long lParm1,ulong uParm2);
void FUN_004138a2(long *plParm1);
undefined8 FUN_004138c1(long *plParm1);
undefined8 FUN_004138f8(undefined8 uParm1);
undefined8 FUN_004138fc(long lParm1,undefined8 uParm2);
void FUN_00413907(ulong *puParm1,long *plParm2);
void FUN_00413c20(ulong *puParm1);
void FUN_00413f96(undefined8 uParm1);
undefined8 FUN_00413fb3(char *pcParm1,uint uParm2,uint uParm3);
void FUN_004140d3(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_004140e9(undefined8 uParm1);
void FUN_0041416c(void);
ulong FUN_00414285(char *pcParm1,long lParm2);
long FUN_004142bb(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
char * FUN_004143c6(ulong uParm1,long lParm2,long lParm3);
ulong FUN_00414569(void);
int * FUN_0041459d(undefined8 param_1,long *param_2);
int * FUN_004147d7(int *param_1,long *param_2);
int * FUN_00414935(long **pplParm1,ulong uParm2,long **pplParm3,uint *puParm4,long **pplParm5,ulong uParm6);
long FUN_00414e8c(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_00415328(uint param_1);
uint * FUN_00415365(undefined8 uParm1,ulong *puParm2);
uint * FUN_00415595(undefined8 uParm1,int *piParm2,ulong *puParm3);
void FUN_00415740(uint uParm1);
void FUN_00415772(char *param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
long * FUN_004158f3(long *plParm1,long **pplParm2,long lParm3,undefined8 uParm4);
long FUN_0041b0aa(undefined8 uParm1,undefined8 uParm2);
double FUN_0041b13a(int *piParm1);
void FUN_0041b183(int *param_1);
long FUN_0041b204(ulong uParm1,ulong uParm2);
void FUN_0041b216(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0041b22c(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_0041b252(ulong uParm1,ulong uParm2);
undefined8 FUN_0041b25d(uint *puParm1,ulong *puParm2);
undefined8 FUN_0041b687(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_0041c133(undefined auParm1 [16]);
ulong FUN_0041c152(void);
void FUN_0041c180(void);
undefined8 _DT_FINI(void);

