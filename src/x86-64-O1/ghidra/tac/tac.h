typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void entry(void);
void FUN_00402670(void);
void FUN_004026e0(void);
void FUN_00402770(void);
void FUN_004029c3(long lParm1,long lParm2);
void FUN_00402a9a(void);
undefined8 FUN_00402aa8(long *plParm1,long *plParm2);
long FUN_00402c72(undefined8 *puParm1,undefined8 *puParm2,ulong uParm3,undefined8 uParm4);
ulong FUN_00402ddf(ulong uParm1,undefined8 uParm2,char *pcParm3);
void FUN_004038dc(char *pcParm1);
long FUN_004038f0(long lParm1,char *pcParm2,undefined8 *puParm3);
int * FUN_00403a65(int *piParm1,long lParm2);
ulong FUN_00403ac8(long lParm1,long lParm2);
undefined8 FUN_00403ae6(long lParm1,undefined8 uParm2,byte bParm3);
undefined8 FUN_00403b34(long lParm1,undefined8 uParm2,byte bParm3,char cParm4);
undefined8 FUN_00403b95(long lParm1,undefined8 uParm2,byte bParm3,char cParm4,char cParm5);
undefined8 FUN_00403bf9(long lParm1,undefined8 uParm2,byte bParm3,char cParm4,char cParm5,char cParm6);
undefined8 FUN_00403c70(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7);
undefined8 FUN_00403cf4(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8);
undefined8 FUN_00403d8e(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined8 FUN_00403e31(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10);
undefined8 FUN_00403ee7(byte *param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10,char param_11);
undefined * FUN_00403fac(char *pcParm1,int iParm2);
ulong FUN_0040406f(undefined *param_1,ulong param_2,char *param_3,ulong param_4,uint param_5,ulong param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_00404fe1(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
ulong FUN_00405172(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
void FUN_004051a5(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004051d3(ulong uParm1,undefined8 uParm2);
void FUN_0040527f(uint uParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_00405331(uint uParm1,undefined8 uParm2,ulong uParm3);
void FUN_00405390(void);
ulong FUN_004053a5(uint uParm1);
void FUN_004056f4(long lParm1);
void FUN_0040570e(void);
long FUN_0040571c(long lParm1,long lParm2);
undefined8 FUN_0040574f(void);
undefined8 FUN_00405777(ulong uParm1,ulong uParm2);
ulong FUN_004057c1(undefined8 uParm1);
void FUN_004057de(void);
void FUN_00405803(undefined8 uParm1);
void FUN_00405843(undefined auParm1 [16],undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong uParm9,ulong uParm10,undefined8 uParm11,undefined8 uParm12,undefined8 uParm13,undefined8 uParm14);
void FUN_00405899(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_0040596c(void);
void FUN_0040597a(long lParm1,int *piParm2);
char * FUN_00405a63(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_00405ae8(int param_1,undefined8 *param_2,char *param_3,char **param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
ulong FUN_0040607c(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
void FUN_0040658b(long lParm1);
ulong FUN_004065a5(uint *puParm1,byte *pbParm2,long lParm3);
void FUN_00406604(undefined8 uParm1);
void FUN_00406621(long lParm1,long lParm2);
ulong FUN_0040664d(int iParm1);
void FUN_00406662(long lParm1,long lParm2);
void FUN_00406693(long lParm1,long lParm2);
ulong FUN_004066c6(long lParm1,long lParm2);
void FUN_004066f5(ulong *puParm1);
void FUN_00406706(long lParm1,long lParm2);
void FUN_0040671e(long lParm1,long lParm2);
ulong FUN_00406736(long lParm1,long lParm2);
ulong FUN_00406770(long lParm1,long lParm2);
undefined8 FUN_0040678a(void);
void FUN_00406790(undefined8 uParm1,undefined8 uParm2,undefined8 *puParm3,long lParm4,byte bParm5,long lParm6);
void FUN_004067f4(long *plParm1);
ulong FUN_0040683d(long lParm1,long lParm2);
long FUN_0040687d(long lParm1,long lParm2);
void FUN_004068cb(long lParm1,long lParm2);
long FUN_00406909(long lParm1,uint uParm2);
long FUN_00406a16(long *plParm1,long lParm2,uint uParm3);
undefined8 FUN_00406a85(char *pcParm1,long lParm2,ulong uParm3);
ulong FUN_00406ba8(long *plParm1,long lParm2,ulong uParm3);
ulong FUN_00406c1b(long lParm1,uint uParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_00406dd6(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long lParm5,undefined8 uParm6);
long FUN_00406e3b(long *plParm1,long lParm2,long lParm3,uint uParm4);
long FUN_00406e8e(long lParm1,long lParm2);
undefined8 FUN_00406f04(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_00406ffa(long *plParm1,long lParm2);
ulong FUN_00407083(long *plParm1);
undefined8 FUN_0040713c(undefined4 *puParm1,long lParm2,char *pcParm3);
undefined8 FUN_00407231(undefined4 *param_1,long param_2,undefined *param_3,int param_4,undefined8 param_5,undefined8 param_6,char param_7);
void FUN_00407311(long *plParm1,code *pcParm2,undefined8 uParm3);
void FUN_00407362(long *plParm1,code *pcParm2,undefined8 uParm3);
undefined8 FUN_004073b0(long lParm1,ulong uParm2);
void FUN_00407459(long param_1,undefined8 param_2,long param_3,long param_4,undefined8 param_5,byte param_6,long param_7);
undefined8 FUN_00407511(long *plParm1,undefined8 uParm2);
undefined8 FUN_00407570(long lParm1);
void FUN_0040763d(undefined8 *puParm1);
void FUN_0040765d(undefined8 *puParm1);
void FUN_0040767d(undefined8 *puParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
void FUN_004076a5(long lParm1,long *plParm2,long *plParm3,undefined *puParm4);
long ** FUN_00407768(long **pplParm1,undefined8 uParm2);
void FUN_004077f9(void);
long FUN_0040780e(undefined4 *puParm1,long *plParm2,long lParm3);
undefined8 FUN_004079f1(undefined8 *puParm1,long lParm2,long lParm3);
undefined8 FUN_00407a67(undefined8 *puParm1,undefined8 uParm2);
ulong FUN_00407aba(long *plParm1,long lParm2);
ulong FUN_00407ba5(long *plParm1,undefined8 uParm2,long lParm3,long lParm4,uint uParm5);
undefined8 FUN_00407c92(long *plParm1,long lParm2);
undefined8 FUN_00407cd7(long *plParm1,ulong *puParm2,ulong uParm3);
undefined8 FUN_00407dcb(long lParm1,undefined4 uParm2,ulong uParm3);
undefined8 FUN_00407e65(long lParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_00407f32(long lParm1,long lParm2,undefined8 uParm3);
void FUN_00407fb8(long lParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00408031(long *plParm1,ulong uParm2);
ulong FUN_00408233(byte bParm1,long lParm2);
void FUN_0040825c(long lParm1);
void FUN_004082c5(long *plParm1);
void FUN_00408464(long *plParm1,long lParm2,uint *puParm3);
undefined8 FUN_0040851a(long *plParm1);
undefined8 FUN_00408aa8(undefined8 *puParm1,undefined8 uParm2,long lParm3,long lParm4,byte bParm5,long lParm6);
ulong FUN_00408bdd(long lParm1,int iParm2);
undefined8 FUN_00408caa(long lParm1,long lParm2);
undefined8 FUN_00408d25(long *plParm1,long lParm2);
undefined8 FUN_00408ee9(long *plParm1,long lParm2);
undefined8 FUN_00408f6f(long *plParm1,long lParm2,long lParm3);
void FUN_0040912a(undefined4 *puParm1,undefined *puParm2);
undefined8 FUN_0040913b(long *plParm1,long lParm2,long lParm3);
void FUN_004092ba(long *plParm1,long lParm2,long lParm3,long lParm4,long lParm5,long lParm6);
undefined8 FUN_00409354(long *plParm1,undefined8 uParm2,undefined8 uParm3,long lParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_00409428(long lParm1,long lParm2,ulong uParm3);
ulong FUN_004094f9(long lParm1,undefined8 *puParm2,long lParm3);
undefined8 FUN_0040962a(long lParm1,long lParm2,undefined8 uParm3);
undefined8 FUN_004096a5(long *plParm1,long lParm2,ulong uParm3);
void FUN_00409d00(undefined8 uParm1,long lParm2);
long FUN_00409d11(undefined8 uParm1,byte *pbParm2,undefined8 uParm3);
long FUN_00409db5(long lParm1,long lParm2,undefined8 uParm3,undefined4 *puParm4,ulong uParm5,undefined4 *puParm6);
void FUN_0040a174(long lParm1);
void FUN_0040a25e(undefined8 *puParm1);
void FUN_0040a27d(undefined8 *puParm1);
long FUN_0040a2c3(long *plParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040a55d(long *plParm1,long lParm2,undefined8 uParm3);
ulong FUN_0040a5f4(long *plParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
ulong FUN_0040a888(undefined4 *puParm1,long *plParm2,long lParm3,char cParm4);
undefined8 FUN_0040aaa5(long lParm1);
undefined8 FUN_0040ab3f(long *plParm1);
void FUN_0040ad33(long lParm1);
long FUN_0040ad8d(long *plParm1,long lParm2,uint uParm3,undefined8 uParm4);
ulong * FUN_0040afa7(undefined4 *puParm1,long lParm2,long lParm3,ulong uParm4);
ulong FUN_0040b084(long *plParm1);
void FUN_0040b257(long *plParm1);
void FUN_0040b2ab(long lParm1);
void FUN_0040b2d7(long *plParm1);
long FUN_0040b43d(long *plParm1,long lParm2,undefined8 uParm3);
ulong * FUN_0040b56b(undefined4 *puParm1,long lParm2,long lParm3);
ulong FUN_0040b62e(long lParm1,long lParm2,undefined8 uParm3);
ulong FUN_0040b6eb(long lParm1,undefined4 *puParm2,undefined8 uParm3,ulong uParm4);
ulong FUN_0040b7ea(long lParm1,undefined8 uParm2,long lParm3,undefined8 uParm4,uint uParm5);
undefined8 FUN_0040ba11(long lParm1,undefined8 uParm2,long lParm3,long lParm4,long lParm5);
long FUN_0040bbac(long *plParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_0040c176(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_0040c220(long *plParm1,long lParm2,undefined8 uParm3,undefined8 uParm4);
undefined8 FUN_0040c3f1(long *plParm1,long lParm2,undefined8 uParm3,long lParm4,long lParm5,long lParm6);
void FUN_0040c604(long lParm1);
undefined8 FUN_0040c6bf(long *plParm1);
void FUN_0040c720(long lParm1);
undefined8 FUN_0040c8d8(long *plParm1,undefined8 *puParm2,long lParm3,undefined8 uParm4,undefined4 *puParm5);
ulong FUN_0040c997(ulong *puParm1,long lParm2,ulong uParm3,uint uParm4);
undefined8 FUN_0040cb5c(ulong uParm1,undefined8 uParm2,long lParm3,long *plParm4,int *piParm5,int *piParm6);
undefined8 FUN_0040ce1f(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,byte *pbParm4);
undefined8 FUN_0040ce53(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,byte *pbParm4);
ulong FUN_0040ce87(byte *pbParm1,undefined8 uParm2,long lParm3,long *plParm4,byte *pbParm5,uint uParm6);
long FUN_0040d55b(long lParm1,long lParm2,long lParm3,ulong uParm4,int *piParm5);
long FUN_0040db28(long lParm1,undefined8 uParm2,undefined8 uParm3,char *pcParm4,byte bParm5,int *piParm6);
long FUN_0040dd2c(long lParm1,long *plParm2,undefined8 *puParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_0040e2ad(undefined8 uParm1,undefined8 *puParm2,long lParm3,undefined8 uParm4,long lParm5,int *piParm6);
long FUN_0040e3ec(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_0040e538(undefined8 uParm1,long *plParm2,ulong uParm3,int *piParm4);
long FUN_0040e5f2(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,undefined8 uParm5,int *piParm6);
void FUN_0040e6ea(long **pplParm1);
ulong FUN_0040e81e(long *plParm1,undefined8 uParm2,undefined8 uParm3,ulong uParm4);
void FUN_0040ea1e(long **pplParm1,long lParm2,undefined8 *puParm3);
undefined8 FUN_0040ee66(byte **ppbParm1,byte *pbParm2,ulong uParm3);
ulong FUN_0040f477(long *plParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_0040f757(long lParm1,long lParm2,long lParm3,undefined8 uParm4);
ulong FUN_0040f94e(long param_1,long *param_2,long param_3,long param_4,undefined8 param_5,long param_6,uint param_7);
void FUN_0040fea4(undefined8 uParm1,long *plParm2,undefined8 *puParm3,undefined8 uParm4,long lParm5);
undefined8 FUN_0040ff15(long lParm1,long lParm2,long lParm3);
ulong FUN_0041024c(long lParm1,long lParm2);
long FUN_004105b5(int *piParm1,long lParm2,long lParm3);
long FUN_00410763(int *piParm1,long lParm2);
ulong FUN_004107bf(long lParm1,long lParm2);
ulong FUN_004109dc(long lParm1,long *plParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_00410a5c(long lParm1,long *plParm2,long lParm3,undefined8 uParm4);
ulong FUN_00410bb2(long lParm1,long *plParm2);
ulong FUN_00410cb2(long lParm1);
ulong FUN_00410ed8(long lParm1,long *plParm2,long lParm3,long lParm4);
ulong FUN_0041121c(long lParm1,long *plParm2,long lParm3,long lParm4);
long FUN_0041135a(long param_1,undefined8 param_2,long param_3,long *param_4,long param_5,long param_6,long param_7);
ulong FUN_0041166f(long lParm1,long lParm2);
undefined8 FUN_00411ba9(int *piParm1,long lParm2,long lParm3);
long FUN_00411c6c(long lParm1,char cParm2,long *plParm3);
undefined8 FUN_00411fec(long *plParm1,long lParm2,long lParm3,long *plParm4,char cParm5);
ulong FUN_004122d6(long *param_1,long param_2,long param_3,long param_4,long param_5,undefined8 param_6,ulong param_7,ulong *param_8,uint param_9);
undefined8 FUN_00412d53(long *plParm1);
long FUN_00412df3(long param_1,undefined8 param_2,long param_3,long param_4,long param_5,undefined8 param_6,ulong *param_7,char param_8);
void FUN_0041303c(void);
undefined8 FUN_00413051(char *pcParm1);
ulong FUN_00413136(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined FUN_00413205(int iParm1);;
ulong FUN_0041320f(uint uParm1);
ulong FUN_0041321e(byte *pbParm1,byte *pbParm2);
char * FUN_004132c9(char *pcParm1);
void FUN_00413316(char *pcParm1);
undefined8 FUN_00413348(void);
ulong FUN_00413350(ulong uParm1);
undefined * FUN_004133eb(void);
char * FUN_004136c3(void);
undefined FUN_00413796(undefined8 uParm1,ulong uParm2);;
ulong FUN_00413823(char *pcParm1,int iParm2,undefined8 uParm3,code *pcParm4,long lParm5);
void FUN_00413976(void);
void FUN_004139cc(void);
void FUN_004139e0(undefined8 uParm1);
undefined8 FUN_004139fd(undefined8 uParm1);
ulong FUN_00413a80(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4);
void FUN_00413bff(undefined8 uParm1);
ulong FUN_00413c17(long lParm1);
undefined8 FUN_00413c99(void);
void FUN_00413cac(void);
undefined8 FUN_00413cba(char *pcParm1,long lParm2);
void FUN_00413e17(void);
undefined8 FUN_00413e25(char *pcParm1,long lParm2);
ulong FUN_00413e8a(char *pcParm1,long lParm2);
long FUN_00413ec0(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
char * FUN_00413fcb(ulong uParm1,long lParm2,long lParm3);
ulong FUN_0041416e(void);
int * FUN_004141a2(undefined8 param_1,long *param_2);
int * FUN_004143dc(int *param_1,long *param_2);
int * FUN_0041453a(long **pplParm1,ulong uParm2,long **pplParm3,uint *puParm4,long **pplParm5,ulong uParm6);
long FUN_00414a91(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_00414f2d(uint param_1);
uint * FUN_00414f6a(undefined8 uParm1,ulong *puParm2);
uint * FUN_0041519a(undefined8 uParm1,int *piParm2,ulong *puParm3);
void FUN_00415345(uint uParm1);
void FUN_00415377(char *param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
long * FUN_004154f8(long *plParm1,long **pplParm2,long lParm3,undefined8 uParm4);
double FUN_0041ace2(int *piParm1);
void FUN_0041ad2b(int *param_1);
long FUN_0041adac(long lParm1);
void FUN_0041adb4(undefined8 uParm1);
void FUN_0041add7(void);
ulong FUN_0041adf2(undefined8 *puParm1,ulong uParm2);
void FUN_0041aefe(undefined8 uParm1);
ulong FUN_0041af16(undefined8 *puParm1);
void FUN_0041af46(undefined8 uParm1,undefined8 uParm2);
void FUN_0041afbd(long lParm1,ulong uParm2,ulong uParm3);
void FUN_0041b208(undefined8 *puParm1,long lParm2,long lParm3);
void FUN_0041b26c(ulong *puParm1,ulong uParm2,ulong uParm3);
long FUN_0041b35d(long lParm1,ulong uParm2);
void FUN_0041b40b(long *plParm1);
undefined8 FUN_0041b42a(long *plParm1);
undefined8 FUN_0041b461(undefined8 uParm1);
undefined8 FUN_0041b465(long lParm1,undefined8 uParm2);
void FUN_0041b470(ulong *puParm1,long *plParm2);
void FUN_0041b789(ulong *puParm1);
ulong FUN_0041baff(uint uParm1);
long FUN_0041bb02(ulong uParm1,ulong uParm2);
void FUN_0041bb14(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0041bb2a(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_0041bb50(ulong uParm1,ulong uParm2);
void FUN_0041bb5b(undefined8 uParm1,undefined8 uParm2);
void FUN_0041bb71(void);
undefined8 FUN_0041bc1d(uint *puParm1,ulong *puParm2);
undefined8 FUN_0041c047(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_0041caf3(undefined auParm1 [16]);
ulong FUN_0041cb12(void);
long FUN_0041cb36(undefined8 uParm1,undefined8 uParm2);
void FUN_0041cbd0(void);
undefined8 _DT_FINI(void);

