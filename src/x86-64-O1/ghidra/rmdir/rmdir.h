typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void entry(void);
void FUN_004024d0(void);
void FUN_00402540(void);
void FUN_004025d0(void);
ulong FUN_00402605(char *pcParm1);
ulong FUN_0040262e(int iParm1,undefined8 uParm2,uint uParm3);
ulong FUN_0040263d(uint uParm1);
void FUN_0040265b(long lParm1);
long FUN_00402831(undefined8 uParm1);
ulong FUN_00402861(undefined8 uParm1,undefined8 uParm2);
ulong FUN_004028d9(ulong uParm1,undefined8 uParm2);
ulong FUN_00402934(char *pcParm1);
void FUN_00402a02(uint uParm1);
ulong FUN_00402aba(uint uParm1,undefined8 *puParm2);
void FUN_00402cb4(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined FUN_00402e2c(char *pcParm1);;
int * FUN_00402e5f(byte *pbParm1);
int * FUN_00402ef8(int *piParm1,long lParm2);
ulong FUN_00402f5b(long lParm1,long lParm2);
undefined8 FUN_00402f79(long lParm1,undefined8 uParm2,byte bParm3);
undefined8 FUN_00402fc7(long lParm1,undefined8 uParm2,byte bParm3,char cParm4);
undefined8 FUN_00403028(long lParm1,undefined8 uParm2,byte bParm3,char cParm4,char cParm5);
undefined8 FUN_0040308c(long lParm1,undefined8 uParm2,byte bParm3,char cParm4,char cParm5,char cParm6);
undefined8 FUN_00403103(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7);
undefined8 FUN_00403187(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8);
undefined8 FUN_00403221(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined8 FUN_004032c4(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10);
undefined8 FUN_0040337a(byte *param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10,char param_11);
undefined * FUN_0040343f(char *pcParm1,int iParm2);
ulong FUN_00403502(undefined *param_1,ulong param_2,char *param_3,ulong param_4,uint param_5,ulong param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_00404474(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_00404638(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00404666(ulong uParm1,undefined8 uParm2);
void FUN_00404712(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_00404956(void);
void FUN_004049a8(void);
void FUN_00404a2d(long lParm1);
void FUN_00404a47(void);
ulong FUN_00404a55(long lParm1,long lParm2);
ulong FUN_00404a88(void);
ulong FUN_00404ab0(undefined8 uParm1);
void FUN_00404acd(void);
void FUN_00404af2(undefined8 uParm1);
void FUN_00404b32(undefined auParm1 [16],undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong uParm9,ulong uParm10,undefined8 uParm11,undefined8 uParm12,undefined8 uParm13,undefined8 uParm14);
void FUN_00404b88(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_00404c5b(ulong uParm1);
void FUN_00404caa(long lParm1,int *piParm2);
char * FUN_00404d93(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_00404e18(int param_1,undefined8 *param_2,char *param_3,char **param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
ulong FUN_004053ac(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
void FUN_0040584f(void);
void FUN_004058a5(void);
void FUN_004058bb(long lParm1);
ulong FUN_004058d5(uint *puParm1,byte *pbParm2,long lParm3);
void FUN_00405934(void);
ulong FUN_00405947(uint param_1,undefined8 param_2,uint param_3,ulong param_4);
void FUN_00405a2e(long lParm1,long lParm2);
ulong FUN_00405a5a(char *pcParm1);
ulong FUN_00405ad8(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined FUN_00405ba7(int iParm1);;
ulong FUN_00405bb1(uint uParm1);
ulong FUN_00405bc0(byte *pbParm1,byte *pbParm2);
ulong FUN_00405c08(ulong uParm1,char cParm2);
char * FUN_00405cd2(char *pcParm1);
void FUN_00405d1f(char *pcParm1);
undefined8 FUN_00405d51(void);
ulong FUN_00405d59(ulong uParm1);
undefined * FUN_00405df4(void);
char * FUN_004060cc(void);
undefined8 FUN_0040619f(undefined8 uParm1);
ulong FUN_00406222(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4);
void FUN_004063a1(undefined8 uParm1);
ulong FUN_004063b9(long lParm1);
undefined8 FUN_0040643b(void);
void FUN_0040644e(void);
ulong FUN_0040645c(char *pcParm1,long lParm2);
long FUN_00406492(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
char * FUN_0040659d(ulong uParm1,long lParm2,long lParm3);
ulong FUN_00406740(void);
int * FUN_00406774(undefined8 param_1,long *param_2);
int * FUN_004069ae(int *param_1,long *param_2);
int * FUN_00406b0c(long **pplParm1,ulong uParm2,long **pplParm3,uint *puParm4,long **pplParm5,ulong uParm6);
long FUN_00407063(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_004074ff(uint param_1);
uint * FUN_0040753c(undefined8 uParm1,ulong *puParm2);
uint * FUN_0040776c(undefined8 uParm1,int *piParm2,ulong *puParm3);
void FUN_00407917(uint uParm1);
void FUN_00407949(char *param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
long * FUN_00407aca(long *plParm1,long **pplParm2,long lParm3,undefined8 uParm4);
double FUN_0040d281(int *piParm1);
void FUN_0040d2ca(int *param_1);
long FUN_0040d34b(ulong uParm1,ulong uParm2);
void FUN_0040d35d(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040d373(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_0040d399(ulong uParm1,ulong uParm2);
undefined8 FUN_0040d3a4(uint *puParm1,ulong *puParm2);
undefined8 FUN_0040d7ce(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_0040e27a(undefined auParm1 [16]);
ulong FUN_0040e299(void);
void FUN_0040e2c0(void);
undefined8 _DT_FINI(void);

