typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void entry(void);
void FUN_00402640(void);
void FUN_004026b0(void);
void FUN_00402740(void);
void FUN_00402775(undefined8 *puParm1);
void FUN_0040278d(long lParm1);
void FUN_004027c2(void);
long FUN_0040283a(long lParm1,uint *puParm2);
long FUN_00402870(uint uParm1);
void FUN_00402915(char cParm1);
void FUN_004029a9(void);
void FUN_004029eb(void);
void FUN_004029fe(void);
long * FUN_00402b9c(long lParm1);
void FUN_00402bdc(ulong uParm1);
long FUN_00402c3b(void);
void FUN_00402c61(long lParm1,long lParm2,long lParm3);
void FUN_00402d05(byte *pbParm1,ulong uParm2,undefined8 uParm3,uint uParm4);
char * FUN_00402dcc(byte *pbParm1);
char * FUN_00402e33(int iParm1,long lParm2,char *pcParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_00402f07(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00402f50(undefined4 uParm1,uint uParm2,char *pcParm3);
void FUN_00403057(int iParm1,int iParm2,long lParm3);
void FUN_004031f0(long lParm1);
void FUN_00403236(undefined8 uParm1,undefined8 uParm2);
long FUN_00403263(undefined8 uParm1,long lParm2);
long FUN_004032b7(long lParm1);
void FUN_0040338a(long *plParm1);
void FUN_004033fc(long lParm1,long lParm2);
void FUN_00403475(void);
void FUN_0040354f(void);
void FUN_00403573(long lParm1);
ulong FUN_00403749(void);
ulong FUN_00403770(char *pcParm1);
ulong FUN_0040386a(void);
long * FUN_00403977(void);
void FUN_00403a29(void);
void FUN_00403a46(long lParm1,long lParm2,char cParm3);
long FUN_00403ad4(ulong uParm1);
ulong FUN_00403b83(void);
ulong FUN_00403ba2(void);
void FUN_00403bf0(ulong uParm1,char cParm2,ulong uParm3);
void FUN_00403cb2(long *plParm1,undefined8 uParm2);
void FUN_00403ea4(long lParm1,long lParm2);
void FUN_00403f55(void);
void FUN_00403ff5(uint uParm1);
ulong FUN_00404552(uint uParm1,undefined8 uParm2,uint uParm3,uint uParm4);
long FUN_004045a0(undefined8 uParm1,undefined8 uParm2);
char * FUN_00404630(ulong uParm1,long lParm2);
int * FUN_0040470e(int *piParm1,long lParm2);
ulong FUN_00404771(long lParm1,long lParm2);
undefined8 FUN_0040478f(long lParm1,undefined8 uParm2,byte bParm3);
undefined8 FUN_004047dd(long lParm1,undefined8 uParm2,byte bParm3,char cParm4);
undefined8 FUN_0040483e(long lParm1,undefined8 uParm2,byte bParm3,char cParm4,char cParm5);
undefined8 FUN_004048a2(long lParm1,undefined8 uParm2,byte bParm3,char cParm4,char cParm5,char cParm6);
undefined8 FUN_00404919(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7);
undefined8 FUN_0040499d(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8);
undefined8 FUN_00404a37(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined8 FUN_00404ada(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10);
undefined8 FUN_00404b90(byte *param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10,char param_11);
undefined * FUN_00404c55(char *pcParm1,int iParm2);
ulong FUN_00404d18(undefined *param_1,ulong param_2,char *param_3,ulong param_4,uint param_5,ulong param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_00405c8a(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
ulong FUN_00405e1b(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
void FUN_00405e4e(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00405e7c(ulong uParm1,undefined8 uParm2);
void FUN_00405e94(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_00405efc(undefined8 uParm1,char cParm2);
void FUN_00405f15(undefined8 uParm1);
void FUN_00405f28(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00405f9c(void);
void FUN_00405faf(undefined8 uParm1,undefined8 uParm2);
void FUN_00405fc4(undefined8 uParm1);
long FUN_00405fda(uint uParm1,undefined8 uParm2,ulong uParm3);
void FUN_00406039(undefined8 uParm1);
void FUN_00406056(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_0040629a(void);
void FUN_004062ec(void);
void FUN_00406371(long lParm1);
void FUN_0040638b(void);
long FUN_00406399(long lParm1,ulong *puParm2);
void FUN_004063cc(long lParm1,ulong *puParm2,ulong uParm3);
void FUN_0040645a(undefined8 uParm1,undefined8 uParm2);
long FUN_00406483(undefined8 param_1,ulong param_2,long param_3,long param_4,undefined8 param_5,undefined8 param_6,uint param_7);
void FUN_00406574(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
undefined8 FUN_00406595(long *plParm1,int iParm2);
ulong FUN_004065f6(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_00406633(char *pcParm1,char **ppcParm2,uint uParm3,undefined8 *puParm4,long lParm5);
ulong FUN_00406913(int iParm1);
ulong FUN_00406928(ulong *puParm1,int iParm2);
ulong FUN_00406952(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_0040698f(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_00406cc6(ulong uParm1,ulong uParm2);
ulong FUN_00406d10(undefined8 uParm1);
void FUN_00406d2d(void);
void FUN_00406d52(undefined8 uParm1);
void FUN_00406d92(undefined auParm1 [16],undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong uParm9,ulong uParm10,undefined8 uParm11,undefined8 uParm12,undefined8 uParm13,undefined8 uParm14);
void FUN_00406de8(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_00406ebb(undefined8 uParm1);
ulong FUN_00406f3e(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4);
void FUN_004070bd(undefined8 uParm1);
ulong FUN_004070d5(long lParm1);
undefined8 FUN_00407157(void);
void FUN_0040716a(void);
void FUN_00407178(long lParm1,int *piParm2);
char * FUN_00407261(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_004072e6(int param_1,undefined8 *param_2,char *param_3,char **param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
ulong FUN_0040787a(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
void FUN_00407d1d(void);
void FUN_00407d73(void);
void FUN_00407d89(long lParm1);
ulong FUN_00407da3(uint *puParm1,byte *pbParm2,long lParm3);
ulong * FUN_00407e02(ulong *puParm1,undefined8 uParm2,ulong uParm3);
void FUN_00407ed9(long lParm1,long lParm2);
ulong FUN_00407f05(int iParm1);
void FUN_00407f1a(long lParm1,long lParm2);
void FUN_00407f4b(long lParm1,long lParm2);
ulong FUN_00407f7e(long lParm1,long lParm2);
void FUN_00407fad(ulong *puParm1);
void FUN_00407fbe(long lParm1,long lParm2);
void FUN_00407fd6(long lParm1,long lParm2);
ulong FUN_00407fee(long lParm1,long lParm2);
ulong FUN_00408028(long lParm1,long lParm2);
undefined8 FUN_00408042(void);
void FUN_00408048(undefined8 uParm1,undefined8 uParm2,undefined8 *puParm3,long lParm4,byte bParm5,long lParm6);
void FUN_004080ac(long *plParm1);
ulong FUN_004080f5(long lParm1,long lParm2);
long FUN_00408135(long lParm1,long lParm2);
void FUN_00408183(long lParm1,long lParm2);
long FUN_004081c1(long lParm1,uint uParm2);
long FUN_004082ce(long *plParm1,long lParm2,uint uParm3);
undefined8 FUN_0040833d(char *pcParm1,long lParm2,ulong uParm3);
ulong FUN_00408460(long *plParm1,long lParm2,ulong uParm3);
ulong FUN_004084d3(long lParm1,uint uParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_0040868e(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long lParm5,undefined8 uParm6);
long FUN_004086f3(long *plParm1,long lParm2,long lParm3,uint uParm4);
long FUN_00408746(long lParm1,long lParm2);
undefined8 FUN_004087bc(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_004088b2(long *plParm1,long lParm2);
ulong FUN_0040893b(long *plParm1);
undefined8 FUN_004089f4(undefined4 *puParm1,long lParm2,char *pcParm3);
undefined8 FUN_00408ae9(undefined4 *param_1,long param_2,undefined *param_3,int param_4,undefined8 param_5,undefined8 param_6,char param_7);
void FUN_00408bc9(long *plParm1,code *pcParm2,undefined8 uParm3);
void FUN_00408c1a(long *plParm1,code *pcParm2,undefined8 uParm3);
undefined8 FUN_00408c68(long lParm1,ulong uParm2);
void FUN_00408d11(long param_1,undefined8 param_2,long param_3,long param_4,undefined8 param_5,byte param_6,long param_7);
undefined8 FUN_00408dc9(long *plParm1,undefined8 uParm2);
undefined8 FUN_00408e28(long lParm1);
void FUN_00408ef5(undefined8 *puParm1);
void FUN_00408f15(undefined8 *puParm1);
void FUN_00408f35(undefined8 *puParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
void FUN_00408f5d(long lParm1,long *plParm2,long *plParm3,undefined *puParm4);
long ** FUN_00409020(long **pplParm1,undefined8 uParm2);
void FUN_004090b1(void);
long FUN_004090c6(undefined4 *puParm1,long *plParm2,long lParm3);
undefined8 FUN_004092a9(undefined8 *puParm1,long lParm2,long lParm3);
undefined8 FUN_0040931f(undefined8 *puParm1,undefined8 uParm2);
ulong FUN_00409372(long *plParm1,long lParm2);
ulong FUN_0040945d(long *plParm1,undefined8 uParm2,long lParm3,long lParm4,uint uParm5);
undefined8 FUN_0040954a(long *plParm1,long lParm2);
undefined8 FUN_0040958f(long *plParm1,ulong *puParm2,ulong uParm3);
undefined8 FUN_00409683(long lParm1,undefined4 uParm2,ulong uParm3);
undefined8 FUN_0040971d(long lParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_004097ea(long lParm1,long lParm2,undefined8 uParm3);
void FUN_00409870(long lParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_004098e9(long *plParm1,ulong uParm2);
ulong FUN_00409aeb(byte bParm1,long lParm2);
void FUN_00409b14(long lParm1);
void FUN_00409b7d(long *plParm1);
void FUN_00409d1c(long *plParm1,long lParm2,uint *puParm3);
undefined8 FUN_00409dd2(long *plParm1);
undefined8 FUN_0040a360(undefined8 *puParm1,undefined8 uParm2,long lParm3,long lParm4,byte bParm5,long lParm6);
ulong FUN_0040a495(long lParm1,int iParm2);
undefined8 FUN_0040a562(long lParm1,long lParm2);
undefined8 FUN_0040a5dd(long *plParm1,long lParm2);
undefined8 FUN_0040a7a1(long *plParm1,long lParm2);
undefined8 FUN_0040a827(long *plParm1,long lParm2,long lParm3);
void FUN_0040a9e2(undefined4 *puParm1,undefined *puParm2);
undefined8 FUN_0040a9f3(long *plParm1,long lParm2,long lParm3);
void FUN_0040ab72(long *plParm1,long lParm2,long lParm3,long lParm4,long lParm5,long lParm6);
undefined8 FUN_0040ac0c(long *plParm1,undefined8 uParm2,undefined8 uParm3,long lParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_0040ace0(long lParm1,long lParm2,ulong uParm3);
ulong FUN_0040adb1(long lParm1,undefined8 *puParm2,long lParm3);
undefined8 FUN_0040aee2(long lParm1,long lParm2,undefined8 uParm3);
undefined8 FUN_0040af5d(long *plParm1,long lParm2,ulong uParm3);
void FUN_0040b5b8(undefined8 uParm1,long lParm2);
long FUN_0040b5c9(undefined8 uParm1,byte *pbParm2,undefined8 uParm3);
long FUN_0040b66d(long lParm1,long lParm2,undefined8 uParm3,undefined4 *puParm4,ulong uParm5,undefined4 *puParm6);
void FUN_0040ba2c(long lParm1);
void FUN_0040bb16(undefined8 *puParm1);
void FUN_0040bb35(undefined8 *puParm1);
long FUN_0040bb7b(long *plParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040be15(long *plParm1,long lParm2,undefined8 uParm3);
ulong FUN_0040beac(long *plParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
ulong FUN_0040c140(undefined4 *puParm1,long *plParm2,long lParm3,char cParm4);
undefined8 FUN_0040c35d(long lParm1);
undefined8 FUN_0040c3f7(long *plParm1);
void FUN_0040c5eb(long lParm1);
long FUN_0040c645(long *plParm1,long lParm2,uint uParm3,undefined8 uParm4);
ulong * FUN_0040c85f(undefined4 *puParm1,long lParm2,long lParm3,ulong uParm4);
ulong FUN_0040c93c(long *plParm1);
void FUN_0040cb0f(long *plParm1);
void FUN_0040cb63(long lParm1);
void FUN_0040cb8f(long *plParm1);
long FUN_0040ccf5(long *plParm1,long lParm2,undefined8 uParm3);
ulong * FUN_0040ce23(undefined4 *puParm1,long lParm2,long lParm3);
ulong FUN_0040cee6(long lParm1,long lParm2,undefined8 uParm3);
ulong FUN_0040cfa3(long lParm1,undefined4 *puParm2,undefined8 uParm3,ulong uParm4);
ulong FUN_0040d0a2(long lParm1,undefined8 uParm2,long lParm3,undefined8 uParm4,uint uParm5);
undefined8 FUN_0040d2c9(long lParm1,undefined8 uParm2,long lParm3,long lParm4,long lParm5);
long FUN_0040d464(long *plParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_0040da2e(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_0040dad8(long *plParm1,long lParm2,undefined8 uParm3,undefined8 uParm4);
undefined8 FUN_0040dca9(long *plParm1,long lParm2,undefined8 uParm3,long lParm4,long lParm5,long lParm6);
void FUN_0040debc(long lParm1);
undefined8 FUN_0040df77(long *plParm1);
void FUN_0040dfd8(long lParm1);
undefined8 FUN_0040e190(long *plParm1,undefined8 *puParm2,long lParm3,undefined8 uParm4,undefined4 *puParm5);
ulong FUN_0040e24f(ulong *puParm1,long lParm2,ulong uParm3,uint uParm4);
undefined8 FUN_0040e414(ulong uParm1,undefined8 uParm2,long lParm3,long *plParm4,int *piParm5,int *piParm6);
undefined8 FUN_0040e6d7(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,byte *pbParm4);
undefined8 FUN_0040e70b(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,byte *pbParm4);
ulong FUN_0040e73f(byte *pbParm1,undefined8 uParm2,long lParm3,long *plParm4,byte *pbParm5,uint uParm6);
long FUN_0040ee13(long lParm1,long lParm2,long lParm3,ulong uParm4,int *piParm5);
long FUN_0040f3e0(long lParm1,undefined8 uParm2,undefined8 uParm3,char *pcParm4,byte bParm5,int *piParm6);
long FUN_0040f5e4(long lParm1,long *plParm2,undefined8 *puParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_0040fb65(undefined8 uParm1,undefined8 *puParm2,long lParm3,undefined8 uParm4,long lParm5,int *piParm6);
long FUN_0040fca4(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_0040fdf0(undefined8 uParm1,long *plParm2,ulong uParm3,int *piParm4);
long FUN_0040feaa(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,undefined8 uParm5,int *piParm6);
void FUN_0040ffa2(long **pplParm1);
ulong FUN_004100d6(long *plParm1,undefined8 uParm2,undefined8 uParm3,ulong uParm4);
void FUN_004102d6(long **pplParm1,long lParm2,undefined8 *puParm3);
undefined8 FUN_0041071e(byte **ppbParm1,byte *pbParm2,ulong uParm3);
ulong FUN_00410d2f(long *plParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_0041100f(long lParm1,long lParm2,long lParm3,undefined8 uParm4);
ulong FUN_00411206(long param_1,long *param_2,long param_3,long param_4,undefined8 param_5,long param_6,uint param_7);
void FUN_0041175c(undefined8 uParm1,long *plParm2,undefined8 *puParm3,undefined8 uParm4,long lParm5);
undefined8 FUN_004117cd(long lParm1,long lParm2,long lParm3);
ulong FUN_00411b04(long lParm1,long lParm2);
long FUN_00411e6d(int *piParm1,long lParm2,long lParm3);
long FUN_0041201b(int *piParm1,long lParm2);
ulong FUN_00412077(long lParm1,long lParm2);
ulong FUN_00412294(long lParm1,long *plParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_00412314(long lParm1,long *plParm2,long lParm3,undefined8 uParm4);
ulong FUN_0041246a(long lParm1,long *plParm2);
ulong FUN_0041256a(long lParm1);
ulong FUN_00412790(long lParm1,long *plParm2,long lParm3,long lParm4);
ulong FUN_00412ad4(long lParm1,long *plParm2,long lParm3,long lParm4);
long FUN_00412c12(long param_1,undefined8 param_2,long param_3,long *param_4,long param_5,long param_6,long param_7);
ulong FUN_00412f27(long lParm1,long lParm2);
undefined8 FUN_00413461(int *piParm1,long lParm2,long lParm3);
long FUN_00413524(long lParm1,char cParm2,long *plParm3);
undefined8 FUN_004138a4(long *plParm1,long lParm2,long lParm3,long *plParm4,char cParm5);
ulong FUN_00413b8e(long *param_1,long param_2,long param_3,long param_4,long param_5,undefined8 param_6,ulong param_7,ulong *param_8,uint param_9);
undefined1 * FUN_004145b1(undefined8 uParm1,undefined8 uParm2,long lParm3);
undefined8 FUN_0041460b(long *plParm1);
long FUN_004146ab(long param_1,undefined8 param_2,long param_3,long param_4,long param_5,undefined8 param_6,ulong *param_7,char param_8);
void FUN_004148f4(void);
ulong FUN_00414909(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,long param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_00414a15(char *pcParm1);
ulong FUN_00414afa(char *pcParm1,long lParm2);
long FUN_00414b30(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
char * FUN_00414c3b(ulong uParm1,long lParm2,long lParm3);
ulong FUN_00414dde(void);
int * FUN_00414e12(undefined8 param_1,long *param_2);
int * FUN_0041504c(int *param_1,long *param_2);
int * FUN_004151aa(long **pplParm1,ulong uParm2,long **pplParm3,uint *puParm4,long **pplParm5,ulong uParm6);
long FUN_00415701(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_00415b9d(uint param_1);
uint * FUN_00415bda(undefined8 uParm1,ulong *puParm2);
uint * FUN_00415e0a(undefined8 uParm1,int *piParm2,ulong *puParm3);
void FUN_00415fb5(uint uParm1);
void FUN_00415fe7(char *param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
long * FUN_00416168(long *plParm1,long **pplParm2,long lParm3,undefined8 uParm4);
ulong FUN_0041b91f(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined FUN_0041b9ee(int iParm1);;
ulong FUN_0041b9f8(uint uParm1);
ulong FUN_0041ba07(byte *pbParm1,byte *pbParm2);
ulong FUN_0041ba4f(undefined8 uParm1);
undefined8 FUN_0041bab2(void);
ulong FUN_0041baba(ulong uParm1);
undefined * FUN_0041bb55(void);
char * FUN_0041be2d(void);
double FUN_0041bf00(int *piParm1);
void FUN_0041bf49(int *param_1);
long FUN_0041bfca(ulong uParm1,ulong uParm2);
void FUN_0041bfdc(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0041bff2(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_0041c018(ulong uParm1,ulong uParm2);
void FUN_0041c023(void);
undefined8 FUN_0041c031(char *pcParm1,long lParm2);
undefined8 FUN_0041c0a8(uint *puParm1,ulong *puParm2);
undefined8 FUN_0041c4d2(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_0041cf7e(undefined auParm1 [16]);
ulong FUN_0041cf9d(void);
void FUN_0041cfc1(void);
undefined8 FUN_0041cfcf(char *pcParm1,long lParm2);
ulong FUN_0041d034(uint uParm1);
void FUN_0041d040(void);
undefined8 _DT_FINI(void);

