typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void entry(void);
void FUN_004025c0(void);
void FUN_00402630(void);
void FUN_004026c0(void);
void FUN_00402701(undefined8 *puParm1,undefined8 uParm2);
ulong FUN_0040272e(long *plParm1);
ulong FUN_00402742(ulong *puParm1);
undefined8 FUN_0040274d(undefined8 *puParm1);
undefined8 FUN_00402751(char *pcParm1);
long FUN_0040277d(undefined8 uParm1);
ulong FUN_004027c1(void);
ulong FUN_004027d0(undefined8 uParm1);
ulong FUN_00402c18(void);
ulong FUN_00402c60(int *piParm1);
void FUN_00402cc1(int *piParm1);
long FUN_00402cdf(long lParm1,ulong uParm2);
long FUN_00402d50(char *pcParm1,char *pcParm2);
undefined4 * FUN_00402e9b(undefined8 uParm1);
undefined4 * FUN_00402ecd(undefined8 uParm1);
undefined * FUN_00402eff(char *pcParm1,ulong uParm2,ulong uParm3);
ulong FUN_0040302f(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00403058(int *piParm1);
void FUN_004030ce(undefined8 uParm1,undefined8 uParm2,undefined8 *puParm3);
ulong FUN_004030ea(uint *puParm1,long lParm2);
undefined8 FUN_0040311f(long lParm1,long lParm2);
undefined8 FUN_00403328(byte bParm1);
undefined8 FUN_00403544(byte bParm1);
long FUN_004035ab(byte bParm1);
long FUN_004036c2(byte bParm1);
long FUN_00403789(ulong uParm1);
undefined8 FUN_004039c8(byte bParm1);
undefined8 FUN_00403a62(byte bParm1);
ulong FUN_00403afc(ulong uParm1);
char * FUN_00403e2c(long lParm1,long lParm2);
byte * FUN_00403fc2(byte *pbParm1,ulong uParm2);
long FUN_00404044(char *pcParm1);
void FUN_004040c1(char *pcParm1);
int * FUN_004042b8(int *piParm1,long lParm2);
ulong FUN_0040431b(long lParm1,long lParm2);
undefined8 FUN_00404339(long lParm1,undefined8 uParm2,byte bParm3);
undefined8 FUN_00404387(long lParm1,undefined8 uParm2,byte bParm3,char cParm4);
undefined8 FUN_004043e8(long lParm1,undefined8 uParm2,byte bParm3,char cParm4,char cParm5);
undefined8 FUN_0040444c(long lParm1,undefined8 uParm2,byte bParm3,char cParm4,char cParm5,char cParm6);
undefined8 FUN_004044c3(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7);
undefined8 FUN_00404547(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8);
undefined8 FUN_004045e1(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined8 FUN_00404684(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10);
undefined8 FUN_0040473a(byte *param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10,char param_11);
undefined * FUN_004047ff(char *pcParm1,int iParm2);
ulong FUN_004048c2(undefined *param_1,ulong param_2,char *param_3,ulong param_4,uint param_5,ulong param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_00405834(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_004059f8(uint uParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_00405aba(long lParm1,long lParm2);
ulong FUN_00405aeb(char *pcParm1,char *pcParm2,char cParm3);
ulong FUN_00405b99(byte *pbParm1,byte *pbParm2,undefined8 uParm3,uint uParm4);
void FUN_00405f75(undefined8 uParm1,undefined8 uParm2);
void FUN_0040621b(long lParm1);
void FUN_00406235(void);
long FUN_00406243(long lParm1,long lParm2);
void FUN_00406276(undefined8 uParm1,undefined8 uParm2);
void FUN_0040629f(char *pcParm1);
undefined8 FUN_004062c7(void);
undefined8 FUN_004062ef(long *plParm1,int iParm2);
ulong FUN_00406350(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_0040638d(char *pcParm1,char **ppcParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_0040666d(ulong uParm1,ulong uParm2);
ulong FUN_004066b7(undefined8 uParm1);
void FUN_004066d4(void);
void FUN_004066f9(undefined8 uParm1);
void FUN_00406739(undefined auParm1 [16],undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong uParm9,ulong uParm10,undefined8 uParm11,undefined8 uParm12,undefined8 uParm13,undefined8 uParm14);
void FUN_0040678f(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00406862(long lParm1,int *piParm2);
char * FUN_0040694b(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_004069d0(int param_1,undefined8 *param_2,char *param_3,char **param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
ulong FUN_00406f64(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
void FUN_00407473(long lParm1);
ulong FUN_0040748d(uint *puParm1,byte *pbParm2,long lParm3);
ulong * FUN_004074ec(ulong *puParm1,undefined8 uParm2,ulong uParm3);
void FUN_004075c3(long lParm1,long lParm2);
ulong FUN_004075ef(int iParm1);
void FUN_00407604(long lParm1,long lParm2);
void FUN_00407635(long lParm1,long lParm2);
ulong FUN_00407668(long lParm1,long lParm2);
void FUN_00407697(ulong *puParm1);
void FUN_004076a8(long lParm1,long lParm2);
void FUN_004076c0(long lParm1,long lParm2);
ulong FUN_004076d8(long lParm1,long lParm2);
ulong FUN_00407712(long lParm1,long lParm2);
undefined8 FUN_0040772c(void);
void FUN_00407732(undefined8 uParm1,undefined8 uParm2,undefined8 *puParm3,long lParm4,byte bParm5,long lParm6);
void FUN_00407796(long *plParm1);
ulong FUN_004077df(long lParm1,long lParm2);
long FUN_0040781f(long lParm1,long lParm2);
void FUN_0040786d(long lParm1,long lParm2);
long FUN_004078ab(long lParm1,uint uParm2);
long FUN_004079b8(long *plParm1,long lParm2,uint uParm3);
undefined8 FUN_00407a27(char *pcParm1,long lParm2,ulong uParm3);
ulong FUN_00407b4a(long *plParm1,long lParm2,ulong uParm3);
ulong FUN_00407bbd(long lParm1,uint uParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_00407d78(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long lParm5,undefined8 uParm6);
long FUN_00407ddd(long *plParm1,long lParm2,long lParm3,uint uParm4);
long FUN_00407e30(long lParm1,long lParm2);
undefined8 FUN_00407ea6(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_00407f9c(long *plParm1,long lParm2);
ulong FUN_00408025(long *plParm1);
undefined8 FUN_004080de(undefined4 *puParm1,long lParm2,char *pcParm3);
undefined8 FUN_004081d3(undefined4 *param_1,long param_2,undefined *param_3,int param_4,undefined8 param_5,undefined8 param_6,char param_7);
void FUN_004082b3(long *plParm1,code *pcParm2,undefined8 uParm3);
void FUN_00408304(long *plParm1,code *pcParm2,undefined8 uParm3);
undefined8 FUN_00408352(long lParm1,ulong uParm2);
void FUN_004083fb(long param_1,undefined8 param_2,long param_3,long param_4,undefined8 param_5,byte param_6,long param_7);
undefined8 FUN_004084b3(long *plParm1,undefined8 uParm2);
undefined8 FUN_00408512(long lParm1);
void FUN_004085df(undefined8 *puParm1);
void FUN_004085ff(undefined8 *puParm1);
void FUN_0040861f(undefined8 *puParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
void FUN_00408647(long lParm1,long *plParm2,long *plParm3,undefined *puParm4);
long ** FUN_0040870a(long **pplParm1,undefined8 uParm2);
void FUN_0040879b(void);
long FUN_004087b0(undefined4 *puParm1,long *plParm2,long lParm3);
undefined8 FUN_00408993(undefined8 *puParm1,long lParm2,long lParm3);
undefined8 FUN_00408a09(undefined8 *puParm1,undefined8 uParm2);
ulong FUN_00408a5c(long *plParm1,long lParm2);
ulong FUN_00408b47(long *plParm1,undefined8 uParm2,long lParm3,long lParm4,uint uParm5);
undefined8 FUN_00408c34(long *plParm1,long lParm2);
undefined8 FUN_00408c79(long *plParm1,ulong *puParm2,ulong uParm3);
undefined8 FUN_00408d6d(long lParm1,undefined4 uParm2,ulong uParm3);
undefined8 FUN_00408e07(long lParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_00408ed4(long lParm1,long lParm2,undefined8 uParm3);
void FUN_00408f5a(long lParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00408fd3(long *plParm1,ulong uParm2);
ulong FUN_004091d5(byte bParm1,long lParm2);
void FUN_004091fe(long lParm1);
void FUN_00409267(long *plParm1);
void FUN_00409406(long *plParm1,long lParm2,uint *puParm3);
undefined8 FUN_004094bc(long *plParm1);
undefined8 FUN_00409a4a(undefined8 *puParm1,undefined8 uParm2,long lParm3,long lParm4,byte bParm5,long lParm6);
ulong FUN_00409b7f(long lParm1,int iParm2);
undefined8 FUN_00409c4c(long lParm1,long lParm2);
undefined8 FUN_00409cc7(long *plParm1,long lParm2);
undefined8 FUN_00409e8b(long *plParm1,long lParm2);
undefined8 FUN_00409f11(long *plParm1,long lParm2,long lParm3);
void FUN_0040a0cc(undefined4 *puParm1,undefined *puParm2);
undefined8 FUN_0040a0dd(long *plParm1,long lParm2,long lParm3);
void FUN_0040a25c(long *plParm1,long lParm2,long lParm3,long lParm4,long lParm5,long lParm6);
undefined8 FUN_0040a2f6(long *plParm1,undefined8 uParm2,undefined8 uParm3,long lParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_0040a3ca(long lParm1,long lParm2,ulong uParm3);
ulong FUN_0040a49b(long lParm1,undefined8 *puParm2,long lParm3);
undefined8 FUN_0040a5cc(long lParm1,long lParm2,undefined8 uParm3);
undefined8 FUN_0040a647(long *plParm1,long lParm2,ulong uParm3);
void FUN_0040aca2(undefined8 uParm1,long lParm2);
long FUN_0040acb3(undefined8 uParm1,byte *pbParm2,undefined8 uParm3);
long FUN_0040ad57(long lParm1,long lParm2,undefined8 uParm3,undefined4 *puParm4,ulong uParm5,undefined4 *puParm6);
void FUN_0040b116(long lParm1);
void FUN_0040b200(undefined8 *puParm1);
void FUN_0040b21f(undefined8 *puParm1);
long FUN_0040b265(long *plParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040b4ff(long *plParm1,long lParm2,undefined8 uParm3);
ulong FUN_0040b596(long *plParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
ulong FUN_0040b82a(undefined4 *puParm1,long *plParm2,long lParm3,char cParm4);
undefined8 FUN_0040ba47(long lParm1);
undefined8 FUN_0040bae1(long *plParm1);
void FUN_0040bcd5(long lParm1);
long FUN_0040bd2f(long *plParm1,long lParm2,uint uParm3,undefined8 uParm4);
ulong * FUN_0040bf49(undefined4 *puParm1,long lParm2,long lParm3,ulong uParm4);
ulong FUN_0040c026(long *plParm1);
void FUN_0040c1f9(long *plParm1);
void FUN_0040c24d(long lParm1);
void FUN_0040c279(long *plParm1);
long FUN_0040c3df(long *plParm1,long lParm2,undefined8 uParm3);
ulong * FUN_0040c50d(undefined4 *puParm1,long lParm2,long lParm3);
ulong FUN_0040c5d0(long lParm1,long lParm2,undefined8 uParm3);
ulong FUN_0040c68d(long lParm1,undefined4 *puParm2,undefined8 uParm3,ulong uParm4);
ulong FUN_0040c78c(long lParm1,undefined8 uParm2,long lParm3,undefined8 uParm4,uint uParm5);
undefined8 FUN_0040c9b3(long lParm1,undefined8 uParm2,long lParm3,long lParm4,long lParm5);
long FUN_0040cb4e(long *plParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_0040d118(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_0040d1c2(long *plParm1,long lParm2,undefined8 uParm3,undefined8 uParm4);
undefined8 FUN_0040d393(long *plParm1,long lParm2,undefined8 uParm3,long lParm4,long lParm5,long lParm6);
void FUN_0040d5a6(long lParm1);
undefined8 FUN_0040d661(long *plParm1);
void FUN_0040d6c2(long lParm1);
undefined8 FUN_0040d87a(long *plParm1,undefined8 *puParm2,long lParm3,undefined8 uParm4,undefined4 *puParm5);
ulong FUN_0040d939(ulong *puParm1,long lParm2,ulong uParm3,uint uParm4);
undefined8 FUN_0040dafe(ulong uParm1,undefined8 uParm2,long lParm3,long *plParm4,int *piParm5,int *piParm6);
undefined8 FUN_0040ddc1(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,byte *pbParm4);
undefined8 FUN_0040ddf5(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,byte *pbParm4);
ulong FUN_0040de29(byte *pbParm1,undefined8 uParm2,long lParm3,long *plParm4,byte *pbParm5,uint uParm6);
long FUN_0040e4fd(long lParm1,long lParm2,long lParm3,ulong uParm4,int *piParm5);
long FUN_0040eaca(long lParm1,undefined8 uParm2,undefined8 uParm3,char *pcParm4,byte bParm5,int *piParm6);
long FUN_0040ecce(long lParm1,long *plParm2,undefined8 *puParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_0040f24f(undefined8 uParm1,undefined8 *puParm2,long lParm3,undefined8 uParm4,long lParm5,int *piParm6);
long FUN_0040f38e(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_0040f4da(undefined8 uParm1,long *plParm2,ulong uParm3,int *piParm4);
long FUN_0040f594(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,undefined8 uParm5,int *piParm6);
void FUN_0040f68c(long **pplParm1);
ulong FUN_0040f7c0(long *plParm1,undefined8 uParm2,undefined8 uParm3,ulong uParm4);
void FUN_0040f9c0(long **pplParm1,long lParm2,undefined8 *puParm3);
undefined8 FUN_0040fe08(byte **ppbParm1,byte *pbParm2,ulong uParm3);
ulong FUN_00410419(long *plParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_004106f9(long lParm1,long lParm2,long lParm3,undefined8 uParm4);
ulong FUN_004108f0(long param_1,long *param_2,long param_3,long param_4,undefined8 param_5,long param_6,uint param_7);
void FUN_00410e46(undefined8 uParm1,long *plParm2,undefined8 *puParm3,undefined8 uParm4,long lParm5);
undefined8 FUN_00410eb7(long lParm1,long lParm2,long lParm3);
ulong FUN_004111ee(long lParm1,long lParm2);
long FUN_00411557(int *piParm1,long lParm2,long lParm3);
long FUN_00411705(int *piParm1,long lParm2);
ulong FUN_00411761(long lParm1,long lParm2);
ulong FUN_0041197e(long lParm1,long *plParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_004119fe(long lParm1,long *plParm2,long lParm3,undefined8 uParm4);
ulong FUN_00411b54(long lParm1,long *plParm2);
ulong FUN_00411c54(long lParm1);
ulong FUN_00411e7a(long lParm1,long *plParm2,long lParm3,long lParm4);
ulong FUN_004121be(long lParm1,long *plParm2,long lParm3,long lParm4);
long FUN_004122fc(long param_1,undefined8 param_2,long param_3,long *param_4,long param_5,long param_6,long param_7);
ulong FUN_00412611(long lParm1,long lParm2);
undefined8 FUN_00412b4b(int *piParm1,long lParm2,long lParm3);
long FUN_00412c0e(long lParm1,char cParm2,long *plParm3);
undefined8 FUN_00412f8e(long *plParm1,long lParm2,long lParm3,long *plParm4,char cParm5);
ulong FUN_00413278(long *param_1,long param_2,long param_3,long param_4,long param_5,undefined8 param_6,ulong param_7,ulong *param_8,uint param_9);
undefined1 * FUN_00413c9b(undefined8 uParm1,undefined8 uParm2,long lParm3);
undefined8 FUN_00413cf5(long *plParm1);
long FUN_00413d95(long param_1,undefined8 param_2,long param_3,long param_4,long param_5,undefined8 param_6,ulong *param_7,char param_8);
void FUN_00413fde(long *plParm1);
void FUN_00414022(void);
ulong FUN_0041403d(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined FUN_0041410c(int iParm1);;
ulong FUN_00414116(uint uParm1);
ulong FUN_00414125(byte *pbParm1,byte *pbParm2);
undefined8 FUN_004141d0(void);
ulong FUN_004141d8(ulong uParm1);
undefined * FUN_00414273(void);
char * FUN_0041454b(void);
ulong FUN_0041461e(byte bParm1);
undefined8 FUN_00414635(undefined8 uParm1);
void FUN_004146b8(undefined8 uParm1);
ulong FUN_004146d0(long lParm1);
undefined8 FUN_00414752(void);
void FUN_00414765(void);
ulong FUN_00414773(char *pcParm1,long lParm2);
long FUN_004147a9(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
char * FUN_004148b4(ulong uParm1,long lParm2,long lParm3);
ulong FUN_00414a57(void);
int * FUN_00414a8b(undefined8 param_1,long *param_2);
int * FUN_00414cc5(int *param_1,long *param_2);
int * FUN_00414e23(long **pplParm1,ulong uParm2,long **pplParm3,uint *puParm4,long **pplParm5,ulong uParm6);
long FUN_0041537a(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_00415816(uint param_1);
uint * FUN_00415853(undefined8 uParm1,ulong *puParm2);
uint * FUN_00415a83(undefined8 uParm1,int *piParm2,ulong *puParm3);
void FUN_00415c2e(uint uParm1);
void FUN_00415c60(char *param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
long * FUN_00415de1(long *plParm1,long **pplParm2,long lParm3,undefined8 uParm4);
double FUN_0041b598(int *piParm1);
void FUN_0041b5e1(int *param_1);
long FUN_0041b662(ulong uParm1,ulong uParm2);
void FUN_0041b674(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0041b68a(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_0041b6b0(ulong uParm1,ulong uParm2);
undefined8 FUN_0041b6bb(uint *puParm1,ulong *puParm2);
undefined8 FUN_0041bae5(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_0041c591(undefined auParm1 [16]);
ulong FUN_0041c5b0(void);
void FUN_0041c5e0(void);
undefined8 _DT_FINI(void);

