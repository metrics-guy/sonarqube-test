typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void entry(void);
void FUN_00402630(void);
void FUN_004026a0(void);
void FUN_00402730(void);
ulong FUN_00402765(int iParm1);
ulong FUN_0040277a(uint uParm1);
ulong FUN_0040277d(long *plParm1,long lParm2);
undefined8 FUN_00402886(undefined8 uParm1,long *plParm2);
void FUN_004028fa(void);
void FUN_0040291e(void);
void FUN_00402942(long lParm1);
char * FUN_00402b18(void);
char * FUN_00402b37(char *pcParm1);
void FUN_00402d1e(char **ppcParm1);
void FUN_00402db9(void);
void FUN_00402eb3(void);
void FUN_00402edf(byte *pbParm1,long *plParm2);
void FUN_00402f87(undefined8 uParm1);
void FUN_004030e6(undefined8 uParm1,long *plParm2);
void FUN_004031d1(int iParm1);
void FUN_004037c5(void);
void FUN_00403a4d(char **ppcParm1);
void FUN_00404423(long lParm1);
void FUN_0040443e(byte *pbParm1,byte *pbParm2);
void FUN_0040478e(void);
void FUN_0040496e(void);
void FUN_00404d45(void);
void FUN_00404ef6(void);
void FUN_00404f93(uint uParm1);
long FUN_00405723(char *pcParm1,char **ppcParm2,long lParm3,long lParm4);
void FUN_0040583d(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_0040589a(long *plParm1,long lParm2,long lParm3);
long FUN_0040596d(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
undefined8 FUN_00405a80(int iParm1);
long FUN_00405ac4(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_00405cfb(undefined8 uParm1,long lParm2);
int * FUN_00405d45(int *piParm1,long lParm2);
ulong FUN_00405da8(long lParm1,long lParm2);
undefined8 FUN_00405dc6(long lParm1,undefined8 uParm2,byte bParm3);
undefined8 FUN_00405e14(long lParm1,undefined8 uParm2,byte bParm3,char cParm4);
undefined8 FUN_00405e75(long lParm1,undefined8 uParm2,byte bParm3,char cParm4,char cParm5);
undefined8 FUN_00405ed9(long lParm1,undefined8 uParm2,byte bParm3,char cParm4,char cParm5,char cParm6);
undefined8 FUN_00405f50(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7);
undefined8 FUN_00405fd4(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8);
undefined8 FUN_0040606e(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined8 FUN_00406111(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10);
undefined8 FUN_004061c7(byte *param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10,char param_11);
undefined * FUN_0040628c(char *pcParm1,int iParm2);
ulong FUN_0040634f(undefined *param_1,ulong param_2,char *param_3,ulong param_4,uint param_5,ulong param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_004072c1(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
ulong FUN_00407452(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
void FUN_00407485(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00407547(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004075bb(void);
void FUN_004075ce(undefined8 uParm1,undefined8 uParm2);
void FUN_004075e3(undefined8 uParm1);
long FUN_004075f9(undefined8 uParm1,ulong *puParm2);
long FUN_0040777d(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004077e7(undefined8 uParm1,undefined8 uParm2);
void FUN_004077fa(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_00407a3e(void);
void FUN_00407a90(void);
void FUN_00407b15(ulong uParm1,ulong uParm2);
void FUN_00407b2f(ulong uParm1,ulong uParm2);
void FUN_00407b62(void);
long FUN_00407b70(long lParm1,ulong *puParm2);
long FUN_00407ba3(long lParm1,ulong *puParm2,ulong uParm3);
long FUN_00407c31(void);
long FUN_00407c59(long *plParm1,int iParm2);
undefined8 FUN_00407c7b(long *plParm1,int iParm2);
ulong FUN_00407cdc(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_00407d19(char *pcParm1,char **ppcParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_00407ff9(ulong uParm1,ulong uParm2);
ulong FUN_00408043(undefined8 uParm1);
void FUN_00408060(void);
void FUN_00408085(undefined8 uParm1);
void FUN_004080c5(undefined auParm1 [16],undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong uParm9,ulong uParm10,undefined8 uParm11,undefined8 uParm12,undefined8 uParm13,undefined8 uParm14);
void FUN_0040811b(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_004081ee(undefined8 uParm1);
void FUN_00408271(undefined8 uParm1);
ulong FUN_00408289(long lParm1);
undefined8 FUN_0040830b(void);
void FUN_0040831e(void);
void FUN_0040832c(long lParm1,int *piParm2);
char * FUN_00408415(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_0040849a(int param_1,undefined8 *param_2,char *param_3,char **param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
ulong FUN_00408a2e(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
void FUN_00408ed1(void);
void FUN_00408f27(void);
void FUN_00408f3d(long lParm1);
ulong FUN_00408f57(uint *puParm1,byte *pbParm2,long lParm3);
void FUN_00408fb6(long lParm1,long lParm2);
ulong FUN_00408fe2(int iParm1);
void FUN_00408ff7(long lParm1,long lParm2);
void FUN_00409028(long lParm1,long lParm2);
ulong FUN_0040905b(long lParm1,long lParm2);
void FUN_0040908a(ulong *puParm1);
void FUN_0040909b(long lParm1,long lParm2);
void FUN_004090b3(long lParm1,long lParm2);
ulong FUN_004090cb(long lParm1,long lParm2);
ulong FUN_00409105(long lParm1,long lParm2);
undefined8 FUN_0040911f(void);
void FUN_00409125(undefined8 uParm1,undefined8 uParm2,undefined8 *puParm3,long lParm4,byte bParm5,long lParm6);
void FUN_00409189(long *plParm1);
ulong FUN_004091d2(long lParm1,long lParm2);
long FUN_00409212(long lParm1,long lParm2);
void FUN_00409260(long lParm1,long lParm2);
long FUN_0040929e(long lParm1,uint uParm2);
long FUN_004093ab(long *plParm1,long lParm2,uint uParm3);
undefined8 FUN_0040941a(char *pcParm1,long lParm2,ulong uParm3);
ulong FUN_0040953d(long *plParm1,long lParm2,ulong uParm3);
ulong FUN_004095b0(long lParm1,uint uParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_0040976b(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long lParm5,undefined8 uParm6);
long FUN_004097d0(long *plParm1,long lParm2,long lParm3,uint uParm4);
long FUN_00409823(long lParm1,long lParm2);
undefined8 FUN_00409899(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_0040998f(long *plParm1,long lParm2);
ulong FUN_00409a18(long *plParm1);
undefined8 FUN_00409ad1(undefined4 *puParm1,long lParm2,char *pcParm3);
undefined8 FUN_00409bc6(undefined4 *param_1,long param_2,undefined *param_3,int param_4,undefined8 param_5,undefined8 param_6,char param_7);
void FUN_00409ca6(long *plParm1,code *pcParm2,undefined8 uParm3);
void FUN_00409cf7(long *plParm1,code *pcParm2,undefined8 uParm3);
undefined8 FUN_00409d45(long lParm1,ulong uParm2);
void FUN_00409dee(long param_1,undefined8 param_2,long param_3,long param_4,undefined8 param_5,byte param_6,long param_7);
undefined8 FUN_00409ea6(long *plParm1,undefined8 uParm2);
undefined8 FUN_00409f05(long lParm1);
void FUN_00409fd2(undefined8 *puParm1);
void FUN_00409ff2(undefined8 *puParm1);
void FUN_0040a012(undefined8 *puParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
void FUN_0040a03a(long lParm1,long *plParm2,long *plParm3,undefined *puParm4);
long ** FUN_0040a0fd(long **pplParm1,undefined8 uParm2);
void FUN_0040a18e(void);
long FUN_0040a1a3(undefined4 *puParm1,long *plParm2,long lParm3);
undefined8 FUN_0040a386(undefined8 *puParm1,long lParm2,long lParm3);
undefined8 FUN_0040a3fc(undefined8 *puParm1,undefined8 uParm2);
ulong FUN_0040a44f(long *plParm1,long lParm2);
ulong FUN_0040a53a(long *plParm1,undefined8 uParm2,long lParm3,long lParm4,uint uParm5);
undefined8 FUN_0040a627(long *plParm1,long lParm2);
undefined8 FUN_0040a66c(long *plParm1,ulong *puParm2,ulong uParm3);
undefined8 FUN_0040a760(long lParm1,undefined4 uParm2,ulong uParm3);
undefined8 FUN_0040a7fa(long lParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_0040a8c7(long lParm1,long lParm2,undefined8 uParm3);
void FUN_0040a94d(long lParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040a9c6(long *plParm1,ulong uParm2);
ulong FUN_0040abc8(byte bParm1,long lParm2);
void FUN_0040abf1(long lParm1);
void FUN_0040ac5a(long *plParm1);
void FUN_0040adf9(long *plParm1,long lParm2,uint *puParm3);
undefined8 FUN_0040aeaf(long *plParm1);
undefined8 FUN_0040b43d(undefined8 *puParm1,undefined8 uParm2,long lParm3,long lParm4,byte bParm5,long lParm6);
ulong FUN_0040b572(long lParm1,int iParm2);
undefined8 FUN_0040b63f(long lParm1,long lParm2);
undefined8 FUN_0040b6ba(long *plParm1,long lParm2);
undefined8 FUN_0040b87e(long *plParm1,long lParm2);
undefined8 FUN_0040b904(long *plParm1,long lParm2,long lParm3);
void FUN_0040babf(undefined4 *puParm1,undefined *puParm2);
undefined8 FUN_0040bad0(long *plParm1,long lParm2,long lParm3);
void FUN_0040bc4f(long *plParm1,long lParm2,long lParm3,long lParm4,long lParm5,long lParm6);
undefined8 FUN_0040bce9(long *plParm1,undefined8 uParm2,undefined8 uParm3,long lParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_0040bdbd(long lParm1,long lParm2,ulong uParm3);
ulong FUN_0040be8e(long lParm1,undefined8 *puParm2,long lParm3);
undefined8 FUN_0040bfbf(long lParm1,long lParm2,undefined8 uParm3);
undefined8 FUN_0040c03a(long *plParm1,long lParm2,ulong uParm3);
void FUN_0040c695(undefined8 uParm1,long lParm2);
long FUN_0040c6a6(undefined8 uParm1,byte *pbParm2,undefined8 uParm3);
long FUN_0040c74a(long lParm1,long lParm2,undefined8 uParm3,undefined4 *puParm4,ulong uParm5,undefined4 *puParm6);
void FUN_0040cb09(long lParm1);
void FUN_0040cbf3(undefined8 *puParm1);
void FUN_0040cc12(undefined8 *puParm1);
long FUN_0040cc58(long *plParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040cef2(long *plParm1,long lParm2,undefined8 uParm3);
ulong FUN_0040cf89(long *plParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
ulong FUN_0040d21d(undefined4 *puParm1,long *plParm2,long lParm3,char cParm4);
undefined8 FUN_0040d43a(long lParm1);
undefined8 FUN_0040d4d4(long *plParm1);
void FUN_0040d6c8(long lParm1);
long FUN_0040d722(long *plParm1,long lParm2,uint uParm3,undefined8 uParm4);
ulong * FUN_0040d93c(undefined4 *puParm1,long lParm2,long lParm3,ulong uParm4);
ulong FUN_0040da19(long *plParm1);
void FUN_0040dbec(long *plParm1);
void FUN_0040dc40(long lParm1);
void FUN_0040dc6c(long *plParm1);
long FUN_0040ddd2(long *plParm1,long lParm2,undefined8 uParm3);
ulong * FUN_0040df00(undefined4 *puParm1,long lParm2,long lParm3);
ulong FUN_0040dfc3(long lParm1,long lParm2,undefined8 uParm3);
ulong FUN_0040e080(long lParm1,undefined4 *puParm2,undefined8 uParm3,ulong uParm4);
ulong FUN_0040e17f(long lParm1,undefined8 uParm2,long lParm3,undefined8 uParm4,uint uParm5);
undefined8 FUN_0040e3a6(long lParm1,undefined8 uParm2,long lParm3,long lParm4,long lParm5);
long FUN_0040e541(long *plParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_0040eb0b(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_0040ebb5(long *plParm1,long lParm2,undefined8 uParm3,undefined8 uParm4);
undefined8 FUN_0040ed86(long *plParm1,long lParm2,undefined8 uParm3,long lParm4,long lParm5,long lParm6);
void FUN_0040ef99(long lParm1);
undefined8 FUN_0040f054(long *plParm1);
void FUN_0040f0b5(long lParm1);
undefined8 FUN_0040f26d(long *plParm1,undefined8 *puParm2,long lParm3,undefined8 uParm4,undefined4 *puParm5);
ulong FUN_0040f32c(ulong *puParm1,long lParm2,ulong uParm3,uint uParm4);
undefined8 FUN_0040f4f1(ulong uParm1,undefined8 uParm2,long lParm3,long *plParm4,int *piParm5,int *piParm6);
undefined8 FUN_0040f7b4(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,byte *pbParm4);
undefined8 FUN_0040f7e8(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,byte *pbParm4);
ulong FUN_0040f81c(byte *pbParm1,undefined8 uParm2,long lParm3,long *plParm4,byte *pbParm5,uint uParm6);
long FUN_0040fef0(long lParm1,long lParm2,long lParm3,ulong uParm4,int *piParm5);
long FUN_004104bd(long lParm1,undefined8 uParm2,undefined8 uParm3,char *pcParm4,byte bParm5,int *piParm6);
long FUN_004106c1(long lParm1,long *plParm2,undefined8 *puParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_00410c42(undefined8 uParm1,undefined8 *puParm2,long lParm3,undefined8 uParm4,long lParm5,int *piParm6);
long FUN_00410d81(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_00410ecd(undefined8 uParm1,long *plParm2,ulong uParm3,int *piParm4);
long FUN_00410f87(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,undefined8 uParm5,int *piParm6);
void FUN_0041107f(long **pplParm1);
ulong FUN_004111b3(long *plParm1,undefined8 uParm2,undefined8 uParm3,ulong uParm4);
void FUN_004113b3(long **pplParm1,long lParm2,undefined8 *puParm3);
undefined8 FUN_004117fb(byte **ppbParm1,byte *pbParm2,ulong uParm3);
ulong FUN_00411e0c(long *plParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_004120ec(long lParm1,long lParm2,long lParm3,undefined8 uParm4);
ulong FUN_004122e3(long param_1,long *param_2,long param_3,long param_4,undefined8 param_5,long param_6,uint param_7);
void FUN_00412839(undefined8 uParm1,long *plParm2,undefined8 *puParm3,undefined8 uParm4,long lParm5);
undefined8 FUN_004128aa(long lParm1,long lParm2,long lParm3);
ulong FUN_00412be1(long lParm1,long lParm2);
long FUN_00412f4a(int *piParm1,long lParm2,long lParm3);
long FUN_004130f8(int *piParm1,long lParm2);
ulong FUN_00413154(long lParm1,long lParm2);
ulong FUN_00413371(long lParm1,long *plParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_004133f1(long lParm1,long *plParm2,long lParm3,undefined8 uParm4);
ulong FUN_00413547(long lParm1,long *plParm2);
ulong FUN_00413647(long lParm1);
ulong FUN_0041386d(long lParm1,long *plParm2,long lParm3,long lParm4);
ulong FUN_00413bb1(long lParm1,long *plParm2,long lParm3,long lParm4);
long FUN_00413cef(long param_1,undefined8 param_2,long param_3,long *param_4,long param_5,long param_6,long param_7);
ulong FUN_00414004(long lParm1,long lParm2);
undefined8 FUN_0041453e(int *piParm1,long lParm2,long lParm3);
long FUN_00414601(long lParm1,char cParm2,long *plParm3);
undefined8 FUN_00414981(long *plParm1,long lParm2,long lParm3,long *plParm4,char cParm5);
ulong FUN_00414c6b(long *param_1,long param_2,long param_3,long param_4,long param_5,undefined8 param_6,ulong param_7,ulong *param_8,uint param_9);
undefined1 * FUN_0041568e(undefined8 uParm1,undefined8 uParm2,long lParm3);
undefined8 FUN_004156e8(long *plParm1);
long FUN_00415788(long param_1,undefined8 param_2,long param_3,long param_4,long param_5,undefined8 param_6,ulong *param_7,char param_8);
void FUN_004159d1(void);
void FUN_004159ec(void);
ulong FUN_00415a01(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined FUN_00415ad0(int iParm1);;
ulong FUN_00415ada(uint uParm1);
ulong FUN_00415ae9(byte *pbParm1,byte *pbParm2);
undefined8 FUN_00415b94(void);
ulong FUN_00415b9c(ulong uParm1);
undefined * FUN_00415c37(void);
char * FUN_00415f0f(void);
long FUN_00415fe2(ulong *puParm1,ulong *puParm2,char *pcParm3,long *plParm4,ulong *puParm5);
long FUN_0041625c(char *pcParm1,undefined8 uParm2);
long FUN_00416309(char *pcParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_004163c3(char *pcParm1,long lParm2);
long FUN_004163f9(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
char * FUN_00416504(ulong uParm1,long lParm2,long lParm3);
ulong FUN_004166a7(void);
int * FUN_004166db(undefined8 param_1,long *param_2);
int * FUN_00416915(int *param_1,long *param_2);
int * FUN_00416a73(long **pplParm1,ulong uParm2,long **pplParm3,uint *puParm4,long **pplParm5,ulong uParm6);
long FUN_00416fca(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_00417466(uint param_1);
uint * FUN_004174a3(undefined8 uParm1,ulong *puParm2);
uint * FUN_004176d3(undefined8 uParm1,int *piParm2,ulong *puParm3);
void FUN_0041787e(uint uParm1);
void FUN_004178b0(char *param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
long * FUN_00417a31(long *plParm1,long **pplParm2,long lParm3,undefined8 uParm4);
double FUN_0041d1e8(int *piParm1);
void FUN_0041d231(int *param_1);
long FUN_0041d2b2(ulong uParm1,ulong uParm2);
void FUN_0041d2c4(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0041d2da(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_0041d300(ulong uParm1,ulong uParm2);
undefined8 FUN_0041d30b(uint *puParm1,ulong *puParm2);
undefined8 FUN_0041d735(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_0041e1e1(undefined auParm1 [16]);
ulong FUN_0041e200(void);
void FUN_0041e230(void);
undefined8 _DT_FINI(void);

