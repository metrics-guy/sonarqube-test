typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_004025d0(void);
void entry(void);
void FUN_00402640(void);
void FUN_004026b0(void);
void FUN_00402740(void);
void FUN_00402775(long lParm1);
ulong FUN_0040294b(char *pcParm1);
void FUN_004029d1(uint uParm1);
ulong FUN_00402b2f(uint uParm1,undefined8 *puParm2);
ulong FUN_00402ec8(uint param_1,undefined8 param_2,long *param_3,uint param_4,uint param_5,uint param_6,uint param_7);
char * FUN_00403083(char *pcParm1,char *pcParm2);
void FUN_0040310b(undefined8 uParm1,int iParm2,undefined8 uParm3,undefined *puParm4,long lParm5,undefined *puParm6);
void FUN_004032a2(undefined4 *puParm1);
void FUN_004032cd(ulong uParm1);
void FUN_004032fb(ulong uParm1);
ulong FUN_00403329(long param_1,long param_2,uint param_3,uint param_4,uint param_5,ulong param_6,int *param_7);
ulong FUN_00403a88(undefined8 param_1,uint param_2,uint param_3,ulong param_4,uint param_5,uint param_6,int *param_7);
void FUN_00403c34(void);
void FUN_00403c48(void);
char * FUN_00403c5c(ulong uParm1,long lParm2);
int * FUN_00403ca1(byte *pbParm1);
int * FUN_00403d3a(int *piParm1,long lParm2);
ulong FUN_00403d9d(long lParm1,long lParm2);
undefined8 FUN_00403dbb(long lParm1,undefined8 uParm2,byte bParm3);
undefined8 FUN_00403e09(long lParm1,undefined8 uParm2,byte bParm3,char cParm4);
undefined8 FUN_00403e6a(long lParm1,undefined8 uParm2,byte bParm3,char cParm4,char cParm5);
undefined8 FUN_00403ece(long lParm1,undefined8 uParm2,byte bParm3,char cParm4,char cParm5,char cParm6);
undefined8 FUN_00403f45(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7);
undefined8 FUN_00403fc9(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8);
undefined8 FUN_00404063(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined8 FUN_00404106(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10);
undefined8 FUN_004041bc(byte *param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10,char param_11);
undefined * FUN_00404281(char *pcParm1,int iParm2);
ulong FUN_00404344(undefined *param_1,ulong param_2,char *param_3,ulong param_4,uint param_5,ulong param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_004052b6(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
ulong FUN_00405447(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
void FUN_0040547a(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004054a8(ulong uParm1,undefined8 uParm2);
void FUN_00405554(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004055c8(void);
void FUN_004055db(undefined8 uParm1,undefined8 uParm2);
void FUN_004055f0(undefined8 uParm1);
undefined8 * FUN_00405606(undefined8 *puParm1);
void FUN_00405645(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_00405889(void);
void FUN_004058db(void);
void FUN_00405960(long lParm1);
void FUN_0040597a(void);
long FUN_00405988(long lParm1,long lParm2);
void FUN_004059bb(undefined8 uParm1,undefined8 uParm2);
void FUN_004059e4(char *pcParm1);
void FUN_00405a0c(void);
void FUN_00405a34(undefined8 uParm1,uint uParm2);
ulong FUN_00405a77(long lParm1,long lParm2);
ulong FUN_00405aa2(int iParm1);
ulong FUN_00405ab7(ulong *puParm1,int iParm2);
ulong FUN_00405ae1(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_00405b1e(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_00405e55(ulong uParm1,ulong uParm2);
ulong FUN_00405e9f(undefined8 uParm1);
void FUN_00405ebc(void);
void FUN_00405ee1(undefined8 uParm1);
void FUN_00405f21(undefined auParm1 [16],undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong uParm9,ulong uParm10,undefined8 uParm11,undefined8 uParm12,undefined8 uParm13,undefined8 uParm14);
void FUN_00405f77(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_0040604a(uint uParm1,char *pcParm2,uint uParm3,ulong uParm4,uint uParm5);
void FUN_004062a5(void);
void FUN_004062b3(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_004062c8(ulong uParm1,char *pcParm2,long lParm3,ulong uParm4);
void FUN_00406405(long lParm1,undefined4 uParm2);
void FUN_0040644d(long lParm1,long lParm2);
long FUN_004064c6(char **ppcParm1);
undefined8 FUN_0040650d(long lParm1,long lParm2);
ulong FUN_0040657a(long lParm1,long lParm2);
ulong FUN_0040659f(long lParm1,long lParm2,char cParm3);
long FUN_0040672a(long lParm1,long lParm2,ulong uParm3);
long FUN_00406806(long lParm1,undefined8 uParm2,long lParm3);
ulong FUN_00406895(long lParm1);
void FUN_004068f4(long lParm1,undefined8 uParm2);
void FUN_00406947(long lParm1);
void FUN_00406980(long lParm1);
void FUN_004069ab(undefined8 uParm1);
long FUN_004069d2(undefined8 uParm1,undefined8 uParm2,uint uParm3,uint *puParm4);
undefined8 FUN_00406a28(long lParm1);
ulong FUN_00406b30(void);
ulong FUN_00406b56(void);
void FUN_00406bbc(long lParm1,long lParm2);
undefined8 FUN_00406c4c(long lParm1,undefined8 *puParm2);
void FUN_00406cf6(long lParm1,uint uParm2,char cParm3);
ulong FUN_00406d49(long lParm1);
ulong FUN_00406d9d(long lParm1,long lParm2,uint uParm3,byte *pbParm4);
long FUN_00406f2e(long *plParm1,long lParm2);
long FUN_00406fc0(long *plParm1,int iParm2);
long * FUN_004077ce(char **ppcParm1,ulong uParm2,long lParm3);
ulong FUN_00407b41(long *plParm1);
long FUN_00407c69(long *plParm1);
undefined8 FUN_0040823a(undefined8 uParm1,long lParm2,uint uParm3);
void FUN_00408262(long lParm1,int *piParm2);
char * FUN_0040834b(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_004083d0(int param_1,undefined8 *param_2,char *param_3,char **param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
ulong FUN_00408964(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
void FUN_00408e07(void);
void FUN_00408e5d(void);
undefined8 FUN_00408e73(char *pcParm1,uint uParm2,uint uParm3);
void FUN_00408f3d(void);
undefined8 FUN_00408f4b(char *pcParm1,long lParm2);
void FUN_00408fc2(long lParm1);
ulong FUN_00408fdc(uint *puParm1,byte *pbParm2,long lParm3);
void FUN_0040903b(void);
ulong FUN_0040904e(uint param_1,undefined8 param_2,uint param_3,ulong param_4);
undefined * FUN_00409135(undefined *puParm1,uint uParm2,char *pcParm3);
void FUN_0040923f(long lParm1,long lParm2);
void FUN_0040926b(void);
undefined8 FUN_00409279(char *pcParm1,long lParm2);
ulong FUN_004092de(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined FUN_004093ad(int iParm1);;
ulong FUN_004093b7(uint uParm1);
ulong FUN_004093c6(byte *pbParm1,byte *pbParm2);
ulong FUN_0040940e(ulong uParm1,char cParm2);
ulong FUN_004094d8(ulong uParm1);
void FUN_004094e3(long lParm1);
undefined8 FUN_004094f3(long *plParm1,long *plParm2);
void FUN_0040958e(undefined8 param_1,byte param_2,ulong param_3);
undefined8 FUN_004095da(void);
ulong FUN_004095e2(ulong uParm1);
ulong FUN_0040967d(ulong uParm1);
ulong FUN_004096f5(ulong uParm1);
undefined8 FUN_0040974a(long lParm1);
ulong FUN_004097cb(ulong uParm1,long lParm2);
void FUN_00409861(long lParm1,undefined8 *puParm2);
long * FUN_00409875(long *plParm1,long lParm2,undefined8 uParm3,char cParm4);
long * FUN_00409897(long lParm1,long *plParm2,long **pplParm3,char cParm4);
void FUN_00409953(long lParm1);
undefined8 FUN_00409978(long lParm1,long **pplParm2,char cParm3);
long * FUN_00409a7d(long lParm1,long *plParm2);
long * FUN_00409ac6(undefined8 uParm1,undefined1 *puParm2,undefined *puParm3,undefined *puParm4,long lParm5);
void FUN_00409ba6(ulong **ppuParm1);
ulong FUN_00409c51(long *plParm1,undefined8 uParm2);
undefined8 FUN_00409d8e(long lParm1,long lParm2,long *plParm3);
undefined8 FUN_00409fbe(undefined8 uParm1,undefined8 uParm2);
long FUN_00409fed(long lParm1,undefined8 uParm2);
void FUN_0040a1cd(undefined4 *puParm1,undefined4 uParm2);
ulong FUN_0040a1ee(long lParm1);
ulong FUN_0040a1f3(long lParm1,uint uParm2);
ulong FUN_0040a22e(long lParm1,undefined8 uParm2,undefined8 uParm3,ulong uParm4);
undefined * FUN_0040a268(void);
char * FUN_0040a540(void);
void FUN_0040a613(ulong uParm1);
void FUN_0040a633(ulong uParm1);
void FUN_0040a653(void);
ulong FUN_0040a69e(int *piParm1);
void FUN_0040a6ed(uint *puParm1);
void FUN_0040a70e(int *piParm1);
ulong FUN_0040a72a(uint uParm1);
ulong FUN_0040a72d(uint uParm1);
void FUN_0040a761(undefined4 *puParm1);
long FUN_0040a768(long lParm1);
void FUN_0040a77b(uint *puParm1);
void FUN_0040a78b(int *piParm1);
undefined8 FUN_0040a7bf(uint *puParm1,undefined8 uParm2);
ulong FUN_0040a7fc(char *pcParm1);
undefined8 FUN_0040aa86(char *pcParm1,uint uParm2,uint uParm3);
undefined8 FUN_0040aba6(undefined8 uParm1);
ulong FUN_0040ac29(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4);
undefined8 FUN_0040ada8(ulong uParm1);
void FUN_0040adf7(undefined8 uParm1);
ulong FUN_0040ae0f(long lParm1);
undefined8 FUN_0040ae91(void);
void FUN_0040aea4(void);
long FUN_0040aeb2(long lParm1,ulong uParm2);
ulong * FUN_0040b345(ulong *puParm1,undefined8 uParm2,ulong uParm3);
ulong FUN_0040b41c(char *pcParm1,long lParm2);
long FUN_0040b452(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
char * FUN_0040b55d(ulong uParm1,long lParm2,long lParm3);
ulong FUN_0040b700(void);
int * FUN_0040b734(undefined8 param_1,long *param_2);
int * FUN_0040b96e(int *param_1,long *param_2);
int * FUN_0040bacc(long **pplParm1,ulong uParm2,long **pplParm3,uint *puParm4,long **pplParm5,ulong uParm6);
long FUN_0040c023(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_0040c4bf(uint param_1);
uint * FUN_0040c4fc(undefined8 uParm1,ulong *puParm2);
uint * FUN_0040c72c(undefined8 uParm1,int *piParm2,ulong *puParm3);
void FUN_0040c8d7(uint uParm1);
void FUN_0040c909(char *param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
long * FUN_0040ca8a(long *plParm1,long **pplParm2,long lParm3,undefined8 uParm4);
double FUN_0041224a(int *piParm1);
void FUN_00412293(int *param_1);
void FUN_00412314(undefined8 uParm1);
long FUN_00412331(ulong uParm1,ulong uParm2);
void FUN_00412343(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00412359(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_0041237f(ulong uParm1,ulong uParm2);
undefined8 FUN_0041238a(uint *puParm1,ulong *puParm2);
undefined8 FUN_004127b4(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_00413260(undefined auParm1 [16]);
ulong FUN_0041327f(void);
void FUN_004132b0(void);
undefined8 _DT_FINI(void);

