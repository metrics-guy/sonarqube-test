typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined5;
typedef unsigned long    undefined6;
typedef unsigned long    undefined7;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_004032d0(void);
void FUN_00403510(void);
void FUN_004035c0(void);
void FUN_004037b0(void);
void entry(void);
void FUN_00403830(void);
void FUN_004038a0(void);
void FUN_00403930(void);
ulong FUN_00403971(uint uParm1);
ulong FUN_00403974(char *pcParm1);
undefined8 FUN_0040399d(undefined8 uParm1);
undefined FUN_004039a1(int iParm1);;
undefined8 FUN_004039a8(void);
char * FUN_004039ae(char *pcParm1);
void FUN_00403a9c(char cParm1);
undefined8 FUN_00403ac0(undefined8 uParm1);
ulong FUN_00403ac4(int iParm1);
ulong FUN_00403ae0(long lParm1);
void FUN_00404453(void);
ulong FUN_00404485(char cParm1,ulong uParm2,int iParm3);
void FUN_0040454a(char *pcParm1,char *pcParm2,char *pcParm3);
void FUN_004045a4(void);
void FUN_004046ff(undefined8 *puParm1);
void FUN_00404742(void);
void FUN_00404804(undefined8 *puParm1);
uint FUN_00404823(byte bParm1);
void FUN_0040484c(ulong uParm1,ulong uParm2);
ulong FUN_00404918(byte **ppbParm1,byte **ppbParm2,char cParm3,long *plParm4);
void FUN_00404b85(undefined8 uParm1);
ulong FUN_00404baa(ulong uParm1);
void FUN_00404c72(void);
void FUN_00404c96(void);
undefined8 FUN_00404cba(undefined8 uParm1);
undefined8 FUN_00404d10(long lParm1);
void FUN_00404dc2(void);
void FUN_00404edc(long lParm1,long lParm2,undefined uParm3);
void FUN_00404f44(long lParm1);
undefined8 FUN_0040511a(undefined8 *puParm1,undefined8 uParm2);
ulong FUN_00405162(char *pcParm1);
ulong * FUN_0040551b(char **ppcParm1,char cParm2);
undefined8 FUN_004057f8(void);
ulong FUN_00405811(undefined8 uParm1,long lParm2);
ulong FUN_0040586e(undefined8 uParm1,long lParm2,char cParm3);
undefined * FUN_004058e2(undefined8 uParm1,ulong uParm2,long lParm3);
void FUN_00405937(byte bParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00405979(undefined8 uParm1,long lParm2);
void FUN_004059e7(undefined8 uParm1,long lParm2,ulong uParm3);
char * FUN_00405a1e(long lParm1,char *pcParm2);
ulong FUN_00405aba(char *pcParm1);
ulong FUN_00405b23(long lParm1,undefined8 uParm2);
void FUN_00405b7b(ulong uParm1);
char * FUN_00405ba0(char *pcParm1,char cParm2,undefined8 uParm3);
void FUN_00405c63(ulong uParm1);
undefined8 FUN_00405c88(char *pcParm1,int iParm2,long lParm3,bool bParm4,char *pcParm5);
void FUN_004067b5(void);
void FUN_00406c0d(void);
void FUN_00406c23(long lParm1,byte bParm2);
char * FUN_00406d97(char **param_1,char *param_2,char *param_3,undefined8 param_4,int param_5,char **param_6,byte *param_7);
long FUN_004071a0(undefined *puParm1,undefined8 uParm2,uint uParm3);
long FUN_0040720c(undefined8 *puParm1);
ulong FUN_0040736a(char cParm1);
void FUN_004074ae(char cParm1);
void FUN_00407602(void);
void FUN_00407615(undefined8 *puParm1);
void FUN_0040765f(void);
void FUN_0040769c(void);
void FUN_004076dd(void);
void FUN_004076fa(void);
undefined FUN_00407799(long lParm1);;
long FUN_004077db(char *param_1,undefined8 param_2,uint param_3,long param_4,char param_5,long param_6,long param_7);
long FUN_00407a7b(undefined8 *puParm1,byte bParm2,undefined8 uParm3,ulong uParm4);
long FUN_00407bde(long lParm1,undefined8 uParm2);
void FUN_00407d1c(void);
void FUN_00407e03(void);
void FUN_00407ef8(char cParm1);
void FUN_00407ffd(char *pcParm1,undefined8 uParm2,ulong uParm3);
void FUN_00408094(ulong uParm1,ulong uParm2,char cParm3);
void FUN_004080d0(ulong uParm1,ulong uParm2,char cParm3);
void FUN_0040819f(undefined8 uParm1,undefined8 uParm2,byte bParm3,long lParm4);
ulong FUN_004081df(void);
void FUN_00408282(void);
void FUN_00408290(long lParm1);
void FUN_004089f0(void);
void FUN_00408fc1(uint uParm1);
long FUN_0040a695(undefined8 uParm1,ulong uParm2);
long FUN_0040a77f(char *pcParm1,char **ppcParm2,long lParm3,long lParm4);
void FUN_0040a899(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_0040a8f6(long *plParm1,long lParm2,long lParm3);
long FUN_0040a9c9(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
ulong FUN_0040aa36(int iParm1);
undefined FUN_0040aa59(int iParm1);;
undefined FUN_0040aa6f(int iParm1);;
undefined FUN_0040aa79(int iParm1);;
ulong FUN_0040aa83(uint uParm1);
ulong FUN_0040aa92(long *plParm1,undefined8 uParm2,undefined8 uParm3);
char * FUN_0040ab08(char *pcParm1,ulong uParm2);
void FUN_0040b14a(char *pcParm1);
char * FUN_0040b182(char *pcParm1);
void FUN_0040b1cf(char *pcParm1);
undefined8 FUN_0040b201(void);
void FUN_0040b207(long lParm1,undefined8 uParm2,undefined8 *puParm3);
ulong FUN_0040b276(long lParm1,undefined8 uParm2,undefined8 *puParm3);
undefined8 FUN_0040b2ad(undefined8 uParm1);
void FUN_0040b315(uint uParm1,undefined *puParm2);
void FUN_0040b426(long lParm1);
void FUN_0040b42f(char *pcParm1);
void FUN_0040b447(char *pcParm1);
long FUN_0040b45b(long lParm1,char *pcParm2,undefined8 *puParm3);
char * FUN_0040b537(char **ppcParm1);
ulong FUN_0040b5ed(byte bParm1);
ulong FUN_0040b632(long lParm1,ulong uParm2,long lParm3,ulong uParm4);
ulong FUN_0040b7e0(byte *pbParm1,byte *pbParm2);
void FUN_0040b998(undefined8 *puParm1);
ulong FUN_0040b9da(ulong uParm1);
ulong FUN_0040ba75(ulong uParm1);
ulong FUN_0040baed(ulong uParm1);
undefined8 FUN_0040bb42(long lParm1);
ulong FUN_0040bbc3(ulong uParm1,long lParm2);
void FUN_0040bc59(long lParm1,undefined8 *puParm2);
long * FUN_0040bc6d(long *plParm1,long lParm2,undefined8 uParm3,char cParm4);
long * FUN_0040bc8f(long lParm1,long *plParm2,long **pplParm3,char cParm4);
void FUN_0040bd4b(long lParm1);
undefined8 FUN_0040bd70(long lParm1,long **pplParm2,char cParm3);
long * FUN_0040be7a(long lParm1,long *plParm2);
long * FUN_0040bec3(undefined8 uParm1,undefined1 *puParm2,undefined *puParm3,undefined *puParm4,long lParm5);
void FUN_0040bfa3(ulong **ppuParm1);
ulong FUN_0040c04e(long *plParm1,undefined8 uParm2);
undefined8 FUN_0040c18b(long lParm1,long lParm2,long *plParm3);
undefined8 FUN_0040c3bb(undefined8 uParm1,undefined8 uParm2);
void FUN_0040c630(undefined8 *puParm1);
void FUN_0040c646(void);
void FUN_0040c70a(long lParm1,ulong uParm2,byte *pbParm3,char *pcParm4);
long FUN_0040c7ae(void);
undefined8 FUN_0040c7d3(char *pcParm1,undefined8 *puParm2,uint *puParm3);
char * FUN_0040c8ef(ulong uParm1,char *pcParm2,ulong uParm3,ulong uParm4,ulong uParm5);
undefined8 FUN_0040d048(undefined8 uParm1,undefined8 uParm2,long *plParm3);
uint * FUN_0040d06e(uint uParm1);
uint * FUN_0040d0fa(uint uParm1);
char * FUN_0040d186(long lParm1,long lParm2);
char * FUN_0040d213(ulong uParm1,long lParm2);
void FUN_0040d258(undefined *puParm1,undefined *puParm2,long lParm3);
undefined8 FUN_0040d286(int *piParm1);
ulong FUN_0040d2ca(int *piParm1,ulong uParm2);
long FUN_0040d337(char *pcParm1,long lParm2,long lParm3,ulong *puParm4,int iParm5,uint uParm6);
ulong FUN_0040d5d7(byte *pbParm1,long lParm2,ulong uParm3);
void FUN_0040d77e(char *pcParm1,ulong uParm2);
void FUN_0040d7a9(undefined8 *puParm1,ulong uParm2,long lParm3,code *pcParm4);
void FUN_0040d87e(undefined8 *puParm1,ulong uParm2,undefined8 *puParm3,code *pcParm4);
void FUN_0040d9ac(long lParm1,long lParm2,undefined8 uParm3);
ulong FUN_0040d9c1(int iParm1,int iParm2);
long FUN_0040d9f5(long lParm1,long lParm2,long lParm3);
long FUN_0040da28(long lParm1,long lParm2,long lParm3);
char * FUN_0040da5b(char *pcParm1,long lParm2,char *pcParm3,uint *puParm4,undefined uParm5,undefined8 uParm6,undefined8 uParm7,uint uParm8);
void FUN_0040f2c2(void);
int * FUN_0040f37d(int *piParm1,long lParm2);
ulong FUN_0040f3e0(long lParm1,long lParm2);
undefined8 FUN_0040f3fe(long lParm1,undefined8 uParm2,byte bParm3);
undefined8 FUN_0040f44c(long lParm1,undefined8 uParm2,byte bParm3,char cParm4);
undefined8 FUN_0040f4ad(long lParm1,undefined8 uParm2,byte bParm3,char cParm4,char cParm5);
undefined8 FUN_0040f511(long lParm1,undefined8 uParm2,byte bParm3,char cParm4,char cParm5,char cParm6);
undefined8 FUN_0040f588(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7);
undefined8 FUN_0040f60c(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8);
undefined8 FUN_0040f6a6(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined8 FUN_0040f749(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10);
undefined8 FUN_0040f7ff(byte *param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10,char param_11);
undefined * FUN_0040f8c4(char *pcParm1,int iParm2);
ulong FUN_0040f987(undefined *param_1,ulong param_2,char *param_3,ulong param_4,uint param_5,ulong param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_004108f9(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
undefined8 FUN_00410a8a(undefined1 *puParm1);
ulong FUN_00410ac7(undefined1 *puParm1);
void FUN_00410ad6(undefined1 *puParm1,undefined4 uParm2);
ulong FUN_00410ae5(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
undefined8 FUN_00410b18(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined1 *puParm5);
void FUN_00410b91(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00410bbf(ulong uParm1,undefined8 uParm2);
void FUN_00410c6b(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00410cdf(void);
void FUN_00410cf2(undefined8 uParm1,undefined8 uParm2);
void FUN_00410d07(undefined8 uParm1);
undefined8 FUN_00410d1d(void);
undefined8 FUN_00410d36(void);
void FUN_00410d4f(long lParm1);
void FUN_00410d58(long lParm1);
void FUN_00410d61(long lParm1);
ulong FUN_00410d6a(uint uParm1);
ulong FUN_00410d6d(long lParm1,int iParm2,long lParm3,int iParm4);
void FUN_004110a1(ulong uParm1,ulong uParm2);
void FUN_004110bb(ulong uParm1,ulong uParm2);
void FUN_004110ee(void);
long FUN_004110fc(long lParm1,ulong *puParm2);
void FUN_0041112f(long lParm1,ulong *puParm2,ulong uParm3);
void FUN_00411165(long lParm1,ulong *puParm2,ulong uParm3);
void FUN_004111f3(undefined8 uParm1,undefined8 uParm2);
void FUN_00411206(undefined8 uParm1,undefined8 uParm2);
void FUN_0041122f(char *pcParm1);
undefined * FUN_00411257(void);
ulong FUN_0041127f(undefined8 param_1,ulong param_2,ulong param_3,ulong param_4,undefined8 param_5,undefined8 param_6,uint param_7);
long FUN_0041135b(void);
ulong FUN_00411431(int iParm1);
ulong FUN_00411446(ulong *puParm1,int iParm2);
ulong FUN_00411470(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_004114ad(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
void FUN_004117e4(uint uParm1,int iParm2,undefined uParm3,long lParm4,undefined8 uParm5,uint uParm6);
ulong FUN_00411865(int iParm1,undefined8 uParm2,char cParm3,undefined8 uParm4,undefined8 uParm5);
ulong FUN_0041187d(int iParm1);
ulong FUN_00411892(ulong *puParm1,int iParm2);
ulong FUN_004118bc(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_004118f9(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_00411c30(ulong uParm1,ulong uParm2);
ulong FUN_00411c7a(undefined8 uParm1);
void FUN_00411c97(void);
void FUN_00411cbc(undefined8 uParm1);
void FUN_00411cfc(undefined auParm1 [16],undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong uParm9,ulong uParm10,undefined8 uParm11,undefined8 uParm12,undefined8 uParm13,undefined8 uParm14);
void FUN_00411d52(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
int * FUN_00411e25(int *piParm1);
char * FUN_00411ef1(char *pcParm1);
undefined8 FUN_00411fbd(int *piParm1);
undefined8 FUN_00412036(int iParm1,long lParm2,ulong uParm3,ulong uParm4,undefined8 uParm5,uint uParm6);
ulong FUN_004125e7(uint *puParm1,uint *puParm2,uint *puParm3,byte bParm4,uint uParm5);
undefined8 FUN_00412fde(int iParm1,char *pcParm2,ulong uParm3,ulong uParm4,undefined8 uParm5,uint uParm6);
ulong FUN_0041352d(byte *pbParm1,byte *pbParm2,byte *pbParm3,byte bParm4,uint uParm5);
ulong FUN_00413f7d(undefined8 uParm1,char *pcParm2,ulong uParm3);
long FUN_0041426d(long lParm1,ulong uParm2);
void FUN_00414700(long lParm1,int *piParm2);
char * FUN_004147e9(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_0041486e(int param_1,undefined8 *param_2,char *param_3,char **param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
ulong FUN_00414e02(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
void FUN_004152a5(void);
void FUN_004152fb(void);
void FUN_00415311(void);
void FUN_004153bd(void);
undefined8 FUN_004153cb(char *pcParm1,long lParm2);
void FUN_00415442(long lParm1);
ulong FUN_0041545c(uint *puParm1,byte *pbParm2,long lParm3);
ulong * FUN_004154bb(ulong *puParm1,undefined8 uParm2,ulong uParm3);
void FUN_00415592(long lParm1,undefined8 uParm2);
void FUN_004155b3(long lParm1,undefined8 uParm2);
undefined8 FUN_004155d4(long *plParm1,long lParm2,long lParm3,undefined8 uParm4,undefined8 uParm5);
void FUN_0041568f(ulong *puParm1,long lParm2);
void FUN_00415765(void);
ulong FUN_00415778(uint param_1,undefined8 param_2,uint param_3,ulong param_4);
undefined8 FUN_0041585f(char *pcParm1);
void FUN_004158bd(long lParm1,long lParm2);
ulong FUN_004158e9(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,long param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_004159f5(void);
undefined8 FUN_00415a03(char *pcParm1,long lParm2);
char * FUN_00415a68(char *pcParm1,char *pcParm2,ulong uParm3);
ulong FUN_00415b15(uint uParm1,uint uParm2);
ulong FUN_00415b35(uint *puParm1,uint *puParm2);
void FUN_00415b7a(long lParm1,undefined8 uParm2,long lParm3);
void FUN_00415b97(void);
void FUN_00415baa(long lParm1);
ulong FUN_00415bd6(long lParm1);
undefined8 * FUN_00415c0d(char *pcParm1);
undefined8 FUN_00415c98(long *plParm1,char *pcParm2);
void FUN_00415dcb(long *plParm1);
long FUN_00415ded(long lParm1);
ulong FUN_00415e7b(long lParm1);
long FUN_00415ec4(long lParm1,undefined8 uParm2,long lParm3);
long FUN_00415f51(long lParm1,undefined8 uParm2);
void FUN_00415ff9(long lParm1);
void FUN_00416018(void);
ulong FUN_004160c0(char *pcParm1);
ulong FUN_00416119(char *pcParm1,long lParm2);
long FUN_0041614f(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
char * FUN_0041625a(ulong uParm1,long lParm2,long lParm3);
ulong FUN_004163fd(void);
int * FUN_00416431(undefined8 param_1,long *param_2);
int * FUN_0041666b(int *param_1,long *param_2);
int * FUN_004167c9(long **pplParm1,ulong uParm2,long **pplParm3,uint *puParm4,long **pplParm5,ulong uParm6);
long FUN_00416d20(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_004171bc(uint param_1);
uint * FUN_004171f9(undefined8 uParm1,ulong *puParm2);
uint * FUN_00417429(undefined8 uParm1,int *piParm2,ulong *puParm3);
void FUN_004175d4(uint uParm1);
void FUN_00417606(char *param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
long * FUN_00417787(long *plParm1,long **pplParm2,long lParm3,undefined8 uParm4);
ulong FUN_0041cf3e(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0041d00d(int *piParm1,long lParm2);
ulong FUN_0041d07b(long lParm1,long lParm2);
undefined8 FUN_0041d099(long lParm1,undefined8 uParm2,char cParm3);
undefined8 FUN_0041d0bb(long lParm1,undefined8 uParm2,char cParm3,char cParm4);
undefined8 FUN_0041d0e0(long lParm1,undefined8 uParm2,char cParm3,char cParm4,char cParm5);
undefined8 FUN_0041d10c(long lParm1,undefined8 uParm2,char cParm3,char cParm4,char cParm5,char cParm6);
undefined8 FUN_0041d13b(long param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7);
undefined8 FUN_0041d175(long param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8);
undefined8 FUN_0041d1c2(long param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined8 FUN_0041d20d(long param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10);
undefined8 FUN_0041d26d(char *param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10,char param_11);
void FUN_0041d2df(ulong uParm1);
ulong FUN_0041d34b(byte *pbParm1,byte *pbParm2);
ulong FUN_0041d393(ulong uParm1,char cParm2);
undefined8 FUN_0041d45d(void);
undefined * FUN_0041d49a(void);
char * FUN_0041d772(void);
double FUN_0041d845(int *piParm1);
void FUN_0041d88e(int *param_1);
ulong FUN_0041d90f(long lParm1,long lParm2);
undefined8 FUN_0041d92d(long lParm1,undefined8 uParm2,char cParm3);
undefined8 FUN_0041d94f(long lParm1,undefined8 uParm2,char cParm3,char cParm4);
undefined8 FUN_0041d974(long lParm1,undefined8 uParm2,char cParm3,char cParm4,char cParm5);
undefined8 FUN_0041d9a0(long lParm1,undefined8 uParm2,char cParm3,char cParm4,char cParm5,char cParm6);
undefined8 FUN_0041d9cf(long param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7);
undefined8 FUN_0041da09(long param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8);
undefined8 FUN_0041da56(long param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined8 FUN_0041daa1(long param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10);
undefined8 FUN_0041db01(char *param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10,char param_11);
ulong FUN_0041db73(undefined8 uParm1);
ulong FUN_0041dd87(uint uParm1,undefined8 uParm2);
long FUN_0041df2b(ulong uParm1,ulong uParm2);
void FUN_0041df3d(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0041df53(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_0041df79(ulong uParm1,ulong uParm2);
undefined8 FUN_0041df84(undefined8 uParm1);
ulong FUN_0041e007(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4);
undefined8 FUN_0041e186(ulong uParm1);
void FUN_0041e1d5(undefined8 uParm1);
ulong FUN_0041e1ed(long lParm1);
undefined8 FUN_0041e26f(void);
void FUN_0041e282(void);
void FUN_0041e290(void);
void FUN_0041e29e(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0041e2b3(ulong uParm1,char *pcParm2,long lParm3,ulong uParm4);
long FUN_0041e381(long lParm1,undefined4 uParm2);
ulong FUN_0041e38a(ulong uParm1);
ulong FUN_0041e3fb(uint uParm1,uint uParm2);
long FUN_0041e41b(ulong param_1,long param_2,int param_3,int param_4,int param_5,uint param_6,int param_7,int param_8,int param_9,int param_10);
long FUN_0041e589(undefined8 uParm1,undefined8 uParm2);
long FUN_0041e5be(void);
void FUN_0041e655(code *pcParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_0041e673(undefined8 uParm1,long *plParm2,undefined8 uParm3);
long FUN_0041e713(uint *puParm1,undefined8 uParm2,long *plParm3);
void FUN_0041eb09(undefined8 uParm1);
undefined8 FUN_0041eb26(uint *puParm1,ulong *puParm2);
undefined8 FUN_0041ef50(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_0041f9fc(char *pcParm1,char *pcParm2,ulong uParm3);
ulong FUN_0041fb17(undefined auParm1 [16]);
ulong FUN_0041fb36(void);
undefined8 * FUN_0041fb5a(ulong uParm1);
void FUN_0041fbce(ulong uParm1);
void FUN_0041fc50(void);
undefined8 _DT_FINI(void);

