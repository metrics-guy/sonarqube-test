typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined3;
typedef unsigned int    undefined4;
typedef unsigned long    undefined5;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00403840(void);
void entry(void);
void FUN_004038b0(void);
void FUN_00403920(void);
void FUN_004039b0(void);
void FUN_004039e5(int iParm1);
ulong FUN_004039f1(uint uParm1);
undefined8 FUN_004039f4(undefined8 uParm1);
void FUN_00403a12(undefined8 uParm1);
long FUN_00403a64(long *plParm1);
char * FUN_00403a6c(char **ppcParm1,long *plParm2);
char * FUN_00403b82(char **ppcParm1,long lParm2);
ulong FUN_00403cb5(byte **ppbParm1);
ulong FUN_00403d75(char *pcParm1);
ulong FUN_00403dc3(char *pcParm1,char **ppcParm2);
undefined8 FUN_00403eb9(long lParm1);
ulong FUN_00403ecb(long lParm1);
void FUN_00403f1f(long lParm1,undefined *puParm2);
void FUN_00403ff4(char *pcParm1,long lParm2,uint uParm3);
void FUN_0040408a(void);
void FUN_004040ae(void);
void FUN_004040e6(long lParm1);
void FUN_004042bc(void);
void FUN_004042dc(void);
void FUN_00404343(undefined8 uParm1,undefined8 uParm2);
void FUN_0040436b(long lParm1);
void FUN_00404389(char *pcParm1);
ulong FUN_00404428(byte bParm1);
void FUN_00404445(void);
undefined8 * FUN_004045af(undefined8 *puParm1);
ulong FUN_004045fa(undefined8 uParm1,undefined8 *puParm2,long lParm3);
void FUN_00404682(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_004047ea(uint uParm1,char cParm2,undefined8 uParm3);
undefined8 FUN_004049dd(byte *pbParm1,byte *pbParm2);
void FUN_00404aca(undefined8 uParm1,undefined *puParm2);
void FUN_00404b15(undefined8 uParm1);
void FUN_00404b32(undefined8 uParm1,undefined8 uParm2);
void FUN_00404b9b(undefined8 uParm1);
void FUN_00404bda(undefined *puParm1,char cParm2);
void FUN_004050bc(long lParm1);
void FUN_00405130(long *plParm1,long lParm2,ulong uParm3);
long FUN_0040519c(char *pcParm1,char *pcParm2);
void FUN_004051d9(long lParm1,long lParm2);
void FUN_00405231(byte **ppbParm1,long *plParm2);
void FUN_004053c3(undefined8 uParm1);
void FUN_00405405(char **ppcParm1,undefined8 uParm2,long lParm3);
void FUN_004054ca(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00405508(long lParm1,long lParm2);
void FUN_004055b7(char *pcParm1,char *pcParm2);
ulong FUN_00405614(char *pcParm1,char *pcParm2);
ulong FUN_0040568a(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00405727(char *pcParm1,ulong uParm2,char *pcParm3,ulong uParm4);
ulong FUN_00405cbd(char **ppcParm1,char **ppcParm2);
ulong FUN_0040641c(undefined8 *puParm1,undefined8 *puParm2);
void FUN_004064e8(long lParm1,ulong uParm2,long lParm3);
void FUN_004065ad(long lParm1,ulong uParm2,long lParm3,byte bParm4);
void FUN_0040673b(undefined *puParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00406793(long *plParm1,ulong uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00406aef(char **ppcParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_00406ddf(undefined8 uParm1,char cParm2);
void FUN_00406ffb(byte **ppbParm1,ulong uParm2);
void FUN_00407069(uint uParm1,uint uParm2);
void FUN_0040707f(long lParm1);
ulong FUN_004070bc(undefined4 uParm1);
ulong FUN_004070ed(uint uParm1);
void FUN_004071a5(ulong uParm1);
void FUN_004071bc(long lParm1);
void FUN_0040728a(long lParm1,ulong uParm2,ulong uParm3,undefined8 uParm4,undefined8 uParm5,long lParm6);
void FUN_004078df(void);
void FUN_0040790b(void);
void FUN_00407923(void);
ulong FUN_00407949(uint *puParm1,long lParm2);
void FUN_00407aad(ulong uParm1,char *pcParm2);
long FUN_00407b42(long lParm1);
long FUN_00407b9f(long lParm1);
long FUN_00407cfb(undefined8 *puParm1,long lParm2,long *plParm3);
undefined8 *FUN_00407d7c(long lParm1,undefined8 uParm2,ulong uParm3,undefined8 uParm4,undefined8 uParm5);
undefined8 * FUN_00407df2(uint *puParm1,char cParm2);
long FUN_00407f6c(long *plParm1,byte bParm2);
void FUN_00408053(undefined8 uParm1);
void FUN_00408066(ulong uParm1,ulong uParm2,ulong uParm3,long lParm4);
ulong FUN_004081d5(long *plParm1,long *plParm2,long *plParm3,undefined8 uParm4);
ulong FUN_004084ac(void);
long FUN_0040862e(long lParm1,ulong uParm2,long lParm3,ulong uParm4,long lParm5);
long * FUN_004087da(long lParm1,long *plParm2,long lParm3,ulong uParm4,long lParm5,char cParm6);
undefined8 * FUN_00408905(long lParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004089a0(undefined8 *puParm1,long lParm2);
void FUN_004089d2(long lParm1);
void FUN_004089e4(undefined8 *puParm1,long lParm2);
void FUN_00408a1f(undefined8 uParm1,long *plParm2);
void FUN_00408a7d(long lParm1);
void FUN_00408a8f(undefined8 uParm1,long lParm2);
long FUN_00408adb(undefined8 *puParm1);
void FUN_00408b38(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_00408baa(long param_1,ulong param_2,long param_3,long *param_4,undefined8 param_5,undefined8 param_6,undefined8 param_7);
void FUN_00408d4e(undefined8 *puParm1,ulong uParm2,long lParm3,ulong uParm4);
void FUN_004090bf(uint uParm1);
ulong FUN_004092da(uint uParm1,undefined8 *puParm2);
long FUN_0040a3ac(char *pcParm1,char **ppcParm2,long lParm3,long lParm4);
void FUN_0040a4c6(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_0040a523(long *plParm1,long lParm2,long lParm3);
long FUN_0040a5f6(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
void FUN_0040a709(undefined4 *puParm1,undefined4 uParm2);
void FUN_0040a70c(undefined4 *puParm1);
long FUN_0040a73d(uint *puParm1,long lParm2);
void FUN_0040a779(int *piParm1,ulong uParm2,int *piParm3);
void FUN_0040aee2(long lParm1,undefined8 uParm2);
void FUN_0040afd4(undefined *puParm1,ulong uParm2,long lParm3);
void FUN_0040b1f7(void);
void FUN_0040b205(long lParm1,ulong uParm2);
char * FUN_0040b228(char **ppcParm1);
ulong FUN_0040b2de(byte bParm1);
ulong FUN_0040b323(long lParm1,ulong uParm2,long lParm3,ulong uParm4);
ulong FUN_0040b4d1(byte *pbParm1,byte *pbParm2);
ulong FUN_0040b689(ulong uParm1);
ulong FUN_0040b724(ulong uParm1);
ulong FUN_0040b79c(ulong uParm1);
undefined8 FUN_0040b7f1(long lParm1);
ulong FUN_0040b872(ulong uParm1,long lParm2);
void FUN_0040b908(long lParm1,undefined8 *puParm2);
long * FUN_0040b91c(long *plParm1,long lParm2,undefined8 uParm3,char cParm4);
long * FUN_0040b93e(long lParm1,long *plParm2,long **pplParm3,char cParm4);
void FUN_0040b9fa(long lParm1);
undefined8 FUN_0040ba1f(long lParm1,long **pplParm2,char cParm3);
long * FUN_0040bb24(undefined8 uParm1,undefined1 *puParm2,undefined *puParm3,undefined *puParm4,long lParm5);
ulong FUN_0040bc04(long *plParm1,undefined8 uParm2);
undefined8 FUN_0040bd41(long lParm1,long lParm2,long *plParm3);
undefined8 FUN_0040bf71(undefined8 uParm1,undefined8 uParm2);
long FUN_0040bfa0(long lParm1,undefined8 uParm2);
ulong FUN_0040c186(long lParm1,ulong uParm2,ulong uParm3,code *pcParm4);
void FUN_0040c224(long lParm1,ulong uParm2,code *pcParm3);
undefined8 * FUN_0040c299(undefined *puParm1,long lParm2);
undefined8 FUN_0040c2f7(long *plParm1,undefined8 uParm2);
undefined8 FUN_0040c356(long *plParm1);
char * FUN_0040c396(int iParm1,long lParm2);
char * FUN_0040c40f(ulong uParm1,long lParm2);
char * FUN_0040c44a(ulong uParm1,long lParm2);
ulong FUN_0040c48f(byte *pbParm1,long lParm2,ulong uParm3);
undefined8 FUN_0040c636(char *pcParm1);
long FUN_0040c6fa(void);
long FUN_0040c73a(int iParm1);
ulong FUN_0040c780(undefined8 uParm1);
void FUN_0040c7f3(void);
void FUN_0040c8a5(void);
ulong FUN_0040c98e(uint *puParm1,uint uParm2);
long FUN_0040cb59(void);
int * FUN_0040cbc3(byte *pbParm1);
int * FUN_0040cc5c(int *piParm1,long lParm2);
ulong FUN_0040ccbf(long lParm1,long lParm2);
undefined8 FUN_0040ccdd(long lParm1,undefined8 uParm2,byte bParm3);
undefined8 FUN_0040cd2b(long lParm1,undefined8 uParm2,byte bParm3,char cParm4);
undefined8 FUN_0040cd8c(long lParm1,undefined8 uParm2,byte bParm3,char cParm4,char cParm5);
undefined8 FUN_0040cdf0(long lParm1,undefined8 uParm2,byte bParm3,char cParm4,char cParm5,char cParm6);
undefined8 FUN_0040ce67(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7);
undefined8 FUN_0040ceeb(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8);
undefined8 FUN_0040cf85(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined8 FUN_0040d028(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10);
undefined8 FUN_0040d0de(byte *param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10,char param_11);
undefined * FUN_0040d1a3(char *pcParm1,int iParm2);
ulong FUN_0040d266(undefined *param_1,ulong param_2,char *param_3,ulong param_4,uint param_5,ulong param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_0040e1d8(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
ulong FUN_0040e369(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
void FUN_0040e39c(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040e3ca(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_0040e3fb(ulong uParm1,undefined8 uParm2);
void FUN_0040e4a7(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040e51b(void);
void FUN_0040e52e(undefined8 uParm1,undefined8 uParm2);
void FUN_0040e543(undefined8 uParm1);
void FUN_0040e559(undefined8 uParm1,undefined8 uParm2);
void FUN_0040e5d0(long lParm1,ulong uParm2,ulong uParm3);
void FUN_0040e81b(undefined8 *puParm1,long lParm2,long lParm3);
void FUN_0040e87f(ulong *puParm1,ulong uParm2,ulong uParm3);
long FUN_0040e970(long lParm1,ulong uParm2);
void FUN_0040ea1e(long *plParm1);
undefined8 FUN_0040ea3d(long *plParm1);
undefined8 FUN_0040ea74(undefined8 uParm1);
undefined8 FUN_0040ea78(long lParm1,undefined8 uParm2);
void FUN_0040ea83(ulong *puParm1,long *plParm2);
void FUN_0040ed9c(ulong *puParm1);
void FUN_0040f112(long *plParm1);
void FUN_0040f1e5(undefined8 *puParm1);
ulong FUN_0040f25f(undefined8 uParm1,long lParm2);
void FUN_0040f41e(undefined8 uParm1,ulong uParm2);
ulong FUN_0040f431(char *pcParm1,char *pcParm2,char cParm3);
ulong FUN_0040f4df(byte *pbParm1,byte *pbParm2,undefined8 uParm3,uint uParm4);
void FUN_0040f8bb(void);
void FUN_0040f8c1(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_0040fb05(void);
void FUN_0040fb57(void);
void FUN_0040fbdc(ulong uParm1,ulong uParm2);
void FUN_0040fbf6(ulong uParm1,ulong uParm2);
void FUN_0040fc29(void);
long FUN_0040fc37(ulong uParm1,ulong *puParm2);
void FUN_0040fc6a(ulong uParm1,ulong *puParm2,ulong uParm3);
void FUN_0040fcf8(ulong uParm1,ulong uParm2);
void FUN_0040fd2c(undefined8 uParm1,undefined8 uParm2);
void FUN_0040fd55(void);
void FUN_0040fd7d(ulong uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
ulong FUN_0040fe1a(undefined8 uParm1,long lParm2,undefined8 uParm3,long lParm4);
undefined8 FUN_0040fe62(void);
ulong FUN_0040feae(int iParm1);
ulong FUN_0040fec3(ulong *puParm1,int iParm2);
ulong FUN_0040feed(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_0040ff2a(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
void FUN_00410261(uint uParm1,int iParm2,undefined uParm3,long lParm4,undefined8 uParm5,uint uParm6);
ulong FUN_004102e2(int iParm1,undefined8 uParm2,char cParm3,undefined8 uParm4,undefined8 uParm5);
ulong FUN_004102fa(int iParm1);
ulong FUN_0041030f(ulong *puParm1,int iParm2);
ulong FUN_00410339(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_00410376(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_004106ad(ulong uParm1,ulong uParm2);
ulong FUN_004106f7(undefined8 uParm1);
void FUN_00410714(void);
void FUN_00410739(undefined8 uParm1);
void FUN_00410779(undefined auParm1 [16],undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong uParm9,ulong uParm10,undefined8 uParm11,undefined8 uParm12,undefined8 uParm13,undefined8 uParm14);
void FUN_004107cf(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_004108a2(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_004108b8(undefined8 uParm1);
ulong FUN_0041093b(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4);
void FUN_00410aba(undefined8 uParm1);
ulong FUN_00410ad2(long lParm1);
undefined8 FUN_00410b54(void);
void FUN_00410b67(void);
void FUN_00410b75(long lParm1,int *piParm2);
char * FUN_00410c5e(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_00410ce3(int param_1,undefined8 *param_2,char *param_3,char **param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
ulong FUN_00411277(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
void FUN_0041171a(void);
void FUN_00411770(void);
void FUN_00411786(void);
void FUN_00411832(long lParm1);
ulong FUN_0041184c(uint *puParm1,byte *pbParm2,long lParm3);
ulong * FUN_004118ab(ulong *puParm1,undefined8 uParm2,ulong uParm3);
undefined8 FUN_00411982(long *plParm1,long *plParm2);
void FUN_00411a0b(long lParm1,undefined8 uParm2);
void FUN_00411a2c(long lParm1,undefined8 uParm2);
undefined8 FUN_00411a4d(long *plParm1,long lParm2,long lParm3,undefined8 uParm4,undefined8 uParm5);
void FUN_00411aee(long lParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
void FUN_00411b08(ulong *puParm1,long lParm2);
void FUN_00411bde(long lParm1,long lParm2);
void FUN_00411c0a(void);
undefined8 FUN_00411c18(char *pcParm1,long lParm2);
undefined8 FUN_00411c7d(char *pcParm1);
ulong FUN_00411d62(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00411e31(long lParm1,long lParm2);
undefined8 FUN_00411e4f(long lParm1,undefined8 uParm2,char cParm3);
undefined8 FUN_00411e71(long lParm1,undefined8 uParm2,char cParm3,char cParm4);
undefined8 FUN_00411e96(long lParm1,undefined8 uParm2,char cParm3,char cParm4,char cParm5);
undefined8 FUN_00411ec2(long lParm1,undefined8 uParm2,char cParm3,char cParm4,char cParm5,char cParm6);
undefined8 FUN_00411ef1(long param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7);
undefined8 FUN_00411f2b(long param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8);
undefined8 FUN_00411f78(long param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined8 FUN_00411fc3(long param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10);
undefined8 FUN_00412023(char *param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10,char param_11);
void FUN_00412095(ulong uParm1);
ulong FUN_00412101(int iParm1);
undefined FUN_00412124(int iParm1);;
undefined FUN_0041213a(int iParm1);;
undefined FUN_00412144(int iParm1);;
undefined FUN_00412157(int iParm1);;
ulong FUN_00412161(uint uParm1);
ulong FUN_00412170(byte *pbParm1,byte *pbParm2);
void FUN_0041221b(double dParm1);
ulong FUN_004122e6(uint uParm1);
void FUN_0041231a(undefined8 uParm1,ulong uParm2);
long FUN_00412340(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_004123d0(void);
undefined * FUN_004123d8(void);
char * FUN_004126b0(void);
ulong FUN_00412783(char *pcParm1,long lParm2,char *pcParm3,long lParm4);
ulong FUN_00412827(undefined8 uParm1,long lParm2,undefined8 uParm3,long lParm4);
ulong FUN_0041287f(uint uParm1);
void FUN_00412882(undefined8 uParm1,undefined8 uParm2);
void FUN_00412889(undefined8 uParm1);
ulong FUN_004128a6(long lParm1,long lParm2);
undefined8 FUN_004128c4(long lParm1,undefined8 uParm2,char cParm3);
undefined8 FUN_004128e6(long lParm1,undefined8 uParm2,char cParm3,char cParm4);
undefined8 FUN_0041290b(long lParm1,undefined8 uParm2,char cParm3,char cParm4,char cParm5);
undefined8 FUN_00412937(long lParm1,undefined8 uParm2,char cParm3,char cParm4,char cParm5,char cParm6);
undefined8 FUN_00412966(long param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7);
undefined8 FUN_004129a0(long param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8);
undefined8 FUN_004129ed(long param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined8 FUN_00412a38(long param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10);
undefined8 FUN_00412a98(char *param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10,char param_11);
ulong FUN_00412b0a(undefined8 uParm1);
ulong FUN_00412d1e(uint uParm1,undefined8 uParm2);
void FUN_00412ec2(void);
undefined8 FUN_00412ed0(char *pcParm1,long lParm2);
ulong FUN_00412f47(char *pcParm1,long lParm2);
long FUN_00412f7d(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
char * FUN_00413088(ulong uParm1,long lParm2,long lParm3);
ulong FUN_0041322b(void);
int * FUN_0041325f(undefined8 param_1,long *param_2);
int * FUN_00413499(int *param_1,long *param_2);
int * FUN_004135f7(long **pplParm1,ulong uParm2,long **pplParm3,uint *puParm4,long **pplParm5,ulong uParm6);
long FUN_00413b4e(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_00413fea(uint param_1);
uint * FUN_00414027(undefined8 uParm1,ulong *puParm2);
uint * FUN_00414257(undefined8 uParm1,int *piParm2,ulong *puParm3);
void FUN_00414402(uint uParm1);
void FUN_00414434(char *param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
long * FUN_004145b5(long *plParm1,long **pplParm2,long lParm3,undefined8 uParm4);
double FUN_00419d6c(int *piParm1);
void FUN_00419db5(int *param_1);
long FUN_00419e36(ulong uParm1,ulong uParm2);
void FUN_00419e48(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00419e5e(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00419e84(ulong uParm1,ulong uParm2);
undefined8 FUN_00419e8f(uint *puParm1,ulong *puParm2);
undefined8 FUN_0041a2b9(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_0041ad65(undefined auParm1 [16]);
ulong FUN_0041ad84(void);
void FUN_0041adb0(void);
undefined8 _DT_FINI(void);

