typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_004021c0(void);
void FUN_00402370(void);
void FUN_00402630(void);
void entry(void);
void FUN_004026a0(void);
void FUN_00402710(void);
void FUN_004027a0(void);
ulong FUN_004027d5(uint uParm1);
void FUN_004027f5(void);
void FUN_00402819(void);
void FUN_00402851(long lParm1);
ulong FUN_00402a27(undefined8 uParm1);
long FUN_00402b2b(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00402be7(char *pcParm1,undefined8 uParm2);
void FUN_00403307(uint uParm1);
ulong FUN_00403414(uint uParm1,undefined8 *puParm2);
long FUN_00403937(long lParm1,long lParm2);
ulong FUN_004039aa(undefined4 uParm1,undefined8 uParm2,ulong uParm3,undefined8 uParm4,undefined4 uParm5,char cParm6);
ulong FUN_00403ac3(undefined8 uParm1,ulong uParm2,undefined8 uParm3,char cParm4);
ulong FUN_00403bcd(char *pcParm1,char *pcParm2);
undefined8 FUN_00403c37(char *pcParm1,long *plParm2,ulong *puParm3,undefined8 uParm4);
ulong FUN_00403ca0(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00403dc2(long *plParm1,ulong uParm2,long lParm3,long lParm4,long *plParm5);
void FUN_0040405e(long lParm1,long lParm2,undefined uParm3);
void FUN_0040410e(char *pcParm1);
long FUN_0040414b(long lParm1,int iParm2,char cParm3);
ulong FUN_0040439f(undefined8 uParm1,char *pcParm2);
ulong FUN_004043bc(undefined8 uParm1,char *pcParm2);
void FUN_004043fe(undefined8 uParm1,char *pcParm2);
ulong FUN_0040442f(long *plParm1,undefined8 uParm2,undefined8 uParm3);
char * FUN_004044a5(char *pcParm1,ulong uParm2);
void FUN_00404a41(undefined8 uParm1);
long FUN_00404bc6(void);
void FUN_00404c59(char *pcParm1);
void FUN_00404c71(char *pcParm1);
undefined * FUN_00404ca9(undefined8 uParm1);
char * FUN_00404cfc(char *pcParm1);
void FUN_00404d49(char *pcParm1);
undefined FUN_00404d7b(char *pcParm1);;
void FUN_00404dae(long lParm1,undefined8 uParm2,undefined8 *puParm3);
ulong FUN_00404e1d(long lParm1,undefined8 uParm2,undefined8 *puParm3);
void FUN_00404e54(char *pcParm1);
void FUN_00404e6c(char *pcParm1);
long FUN_00404e80(long lParm1,char *pcParm2,undefined8 *puParm3);
ulong FUN_00404f5c(ulong uParm1);
ulong FUN_00404fd4(ulong uParm1);
undefined8 FUN_00405029(long lParm1);
ulong FUN_004050aa(ulong uParm1,long lParm2);
void FUN_00405140(long lParm1,undefined8 *puParm2);
long * FUN_00405154(long *plParm1,long lParm2,undefined8 uParm3,char cParm4);
long * FUN_00405176(long lParm1,long *plParm2,long **pplParm3,char cParm4);
void FUN_00405232(long lParm1);
undefined8 FUN_00405257(long lParm1,long **pplParm2,char cParm3);
long * FUN_0040535c(long lParm1,long *plParm2);
long * FUN_004053a5(undefined8 uParm1,undefined1 *puParm2,undefined *puParm3,undefined *puParm4,long lParm5);
void FUN_00405485(ulong **ppuParm1);
ulong FUN_00405530(long *plParm1,undefined8 uParm2);
undefined8 FUN_0040566d(long lParm1,long lParm2,long *plParm3);
undefined8 FUN_0040589d(undefined8 uParm1,undefined8 uParm2);
void FUN_00405969(undefined8 *puParm1);
int * FUN_0040597f(byte *pbParm1);
int * FUN_00405a18(int *piParm1,long lParm2);
ulong FUN_00405a7b(long lParm1,long lParm2);
undefined8 FUN_00405a99(long lParm1,undefined8 uParm2,byte bParm3);
undefined8 FUN_00405ae7(long lParm1,undefined8 uParm2,byte bParm3,char cParm4);
undefined8 FUN_00405b48(long lParm1,undefined8 uParm2,byte bParm3,char cParm4,char cParm5);
undefined8 FUN_00405bac(long lParm1,undefined8 uParm2,byte bParm3,char cParm4,char cParm5,char cParm6);
undefined8 FUN_00405c23(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7);
undefined8 FUN_00405ca7(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8);
undefined8 FUN_00405d41(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined8 FUN_00405de4(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10);
undefined8 FUN_00405e9a(byte *param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10,char param_11);
undefined * FUN_00405f5f(char *pcParm1,int iParm2);
ulong FUN_00406022(undefined *param_1,ulong param_2,char *param_3,ulong param_4,uint param_5,ulong param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_00406f94(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
ulong FUN_00407125(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
void FUN_00407158(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00407186(ulong uParm1,undefined8 uParm2);
void FUN_00407232(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004072a6(void);
void FUN_004072b9(undefined8 uParm1,undefined8 uParm2);
void FUN_004072ce(undefined8 uParm1);
undefined8 FUN_004072e4(undefined4 uParm1);
ulong FUN_004072f5(uint uParm1,char *pcParm2,uint uParm3,char *pcParm4,uint uParm5);
ulong FUN_004076cb(undefined8 uParm1,undefined8 uParm2);
void FUN_004077fc(void);
undefined FUN_0040780f(undefined8 uParm1,ulong uParm2);;
ulong FUN_00407825(char *pcParm1,int iParm2,undefined8 uParm3,code *pcParm4,long lParm5);
void FUN_00407978(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_00407bbc(void);
void FUN_00407c0e(void);
void FUN_00407c93(long lParm1);
void FUN_00407cad(void);
long FUN_00407cbb(long lParm1,long lParm2);
void FUN_00407cee(undefined8 uParm1,undefined8 uParm2);
void FUN_00407d17(char *pcParm1);
ulong FUN_00407d3f(void);
ulong FUN_00407d67(void);
ulong FUN_00407d93(void);
undefined8 FUN_00407dc1(ulong uParm1,ulong uParm2);
ulong FUN_00407e0b(undefined8 uParm1);
void FUN_00407e28(void);
void FUN_00407e4d(undefined8 uParm1);
void FUN_00407e8d(undefined auParm1 [16],undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong uParm9,ulong uParm10,undefined8 uParm11,undefined8 uParm12,undefined8 uParm13,undefined8 uParm14);
void FUN_00407ee3(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_00407fb6(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4);
undefined8 FUN_00408135(ulong uParm1);
void FUN_00408184(undefined8 uParm1);
ulong FUN_0040819c(long lParm1);
undefined8 FUN_0040821e(void);
void FUN_00408231(void);
void FUN_0040823f(void);
void FUN_0040824d(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00408262(ulong uParm1,char *pcParm2,long lParm3,ulong uParm4);
long FUN_00408330(long lParm1,ulong uParm2);
void FUN_004087c3(long lParm1,int *piParm2);
char * FUN_004088ac(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_00408931(int param_1,undefined8 *param_2,char *param_3,char **param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
ulong FUN_00408ec5(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
void FUN_00409368(void);
void FUN_004093be(void);
ulong FUN_004093d4(uint uParm1,char *pcParm2,uint uParm3,undefined8 uParm4);
ulong FUN_00409538(ulong uParm1,char *pcParm2,ulong uParm3,char *pcParm4,uint uParm5);
void FUN_00409685(void);
undefined8 FUN_00409693(char *pcParm1,long lParm2);
void FUN_0040970a(long lParm1);
ulong FUN_00409724(uint *puParm1,byte *pbParm2,long lParm3);
void FUN_00409783(void);
ulong FUN_00409796(uint param_1,undefined8 param_2,uint param_3,ulong param_4);
void FUN_0040987d(long lParm1,long lParm2);
ulong FUN_004098a9(char *pcParm1,char *pcParm2);
void FUN_00409d31(void);
ulong FUN_00409d45(char *pcParm1);
void FUN_00409dc3(void);
undefined8 FUN_00409dd1(char *pcParm1,long lParm2);
undefined8 FUN_00409e36(undefined8 uParm1,uint uParm2,char *pcParm3);
undefined8 FUN_00409ea5(ulong uParm1,char *pcParm2,ulong uParm3);
ulong FUN_00409fb0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_0040a07f(undefined8 uParm1,ulong uParm2);
void FUN_0040a156(undefined8 uParm1,undefined8 uParm2);
long FUN_0040a18d(char *pcParm1,char **ppcParm2,long lParm3,long lParm4);
void FUN_0040a2a7(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_0040a304(long *plParm1,long lParm2,long lParm3);
long FUN_0040a3d7(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
undefined FUN_0040a44d(int iParm1);;
ulong FUN_0040a457(uint uParm1);
ulong FUN_0040a466(byte *pbParm1,byte *pbParm2);
undefined *FUN_0040a4ae(uint uParm1,undefined8 uParm2,undefined *puParm3,ulong uParm4,undefined1 *puParm5,code *pcParm6);
ulong FUN_0040a654(ulong uParm1,char cParm2);
undefined8 FUN_0040a71e(void);
ulong FUN_0040a726(ulong uParm1);
undefined * FUN_0040a7f6(void);
char * FUN_0040aace(void);
long FUN_0040aba1(long lParm1);
void FUN_0040aba9(undefined8 uParm1);
void FUN_0040abcc(void);
ulong FUN_0040abe7(undefined8 *puParm1,ulong uParm2);
void FUN_0040acf3(undefined8 uParm1);
ulong FUN_0040ad0b(undefined8 *puParm1);
void FUN_0040ad3b(undefined8 uParm1,undefined8 uParm2);
void FUN_0040adb2(long lParm1,ulong uParm2,ulong uParm3);
void FUN_0040affd(undefined8 *puParm1,long lParm2,long lParm3);
void FUN_0040b061(ulong *puParm1,ulong uParm2,ulong uParm3);
long FUN_0040b152(long lParm1,ulong uParm2);
void FUN_0040b200(long *plParm1);
undefined8 FUN_0040b21f(long *plParm1);
undefined8 FUN_0040b256(undefined8 uParm1);
undefined8 FUN_0040b25a(long lParm1,undefined8 uParm2);
void FUN_0040b265(ulong *puParm1,long *plParm2);
void FUN_0040b57e(ulong *puParm1);
ulong FUN_0040b8f4(uint uParm1);
void FUN_0040b8f7(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_0040b90d(undefined8 uParm1);
void FUN_0040b990(void);
undefined8 FUN_0040ba3c(char *pcParm1);
ulong FUN_0040bb07(char *pcParm1,long lParm2);
long FUN_0040bb3d(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
char * FUN_0040bc48(ulong uParm1,long lParm2,long lParm3);
ulong FUN_0040bdeb(void);
int * FUN_0040be1f(undefined8 param_1,long *param_2);
int * FUN_0040c059(int *param_1,long *param_2);
int * FUN_0040c1b7(long **pplParm1,ulong uParm2,long **pplParm3,uint *puParm4,long **pplParm5,ulong uParm6);
long FUN_0040c70e(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_0040cbaa(uint param_1);
uint * FUN_0040cbe7(undefined8 uParm1,ulong *puParm2);
uint * FUN_0040ce17(undefined8 uParm1,int *piParm2,ulong *puParm3);
void FUN_0040cfc2(uint uParm1);
void FUN_0040cff4(char *param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
long * FUN_0040d175(long *plParm1,long **pplParm2,long lParm3,undefined8 uParm4);
long FUN_0041292c(undefined8 uParm1,undefined8 uParm2);
double FUN_004129bc(int *piParm1);
void FUN_00412a05(int *param_1);
void FUN_00412a86(undefined8 uParm1);
long FUN_00412aa3(ulong uParm1,ulong uParm2);
void FUN_00412ab5(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00412acb(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00412af1(ulong uParm1,ulong uParm2);
undefined8 FUN_00412afc(uint *puParm1,ulong *puParm2);
undefined8 FUN_00412f26(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_004139d2(undefined auParm1 [16]);
ulong FUN_004139f1(void);
void FUN_00413a20(void);
undefined8 _DT_FINI(void);

