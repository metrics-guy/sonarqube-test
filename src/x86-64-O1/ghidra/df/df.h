typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void entry(void);
void FUN_00402680(void);
void FUN_004026f0(void);
void FUN_00402780(void);
ulong FUN_004027b5(uint uParm1);
void FUN_004027b8(void);
undefined FUN_0040281e(ulong uParm1);;
void FUN_00402826(ulong *puParm1,char *pcParm2,ulong uParm3,char cParm4);
void FUN_00402865(undefined8 *puParm1,undefined8 *puParm2,undefined8 *puParm3);
void FUN_0040295e(long *plParm1,long lParm2);
void FUN_004029fd(undefined8 uParm1);
void FUN_00402a18(void);
void FUN_00402a3c(void);
ulong FUN_00402a60(long lParm1);
ulong FUN_00402aa8(long lParm1);
void FUN_00402af8(long lParm1);
void FUN_00402cce(undefined8 uParm1);
void FUN_00402cf3(undefined8 uParm1);
void FUN_00402d18(int iParm1,long lParm2);
undefined * FUN_00402dd8(byte bParm1,long lParm2,long lParm3,undefined8 uParm4,undefined8 uParm5);
char * FUN_00402e5a(char *pcParm1);
void FUN_00402ea0(void);
undefined8 * FUN_004030fe(undefined8 uParm1);
void FUN_0040316d(undefined8 uParm1);
void FUN_00403192(char cParm1);
long FUN_00403491(void);
ulong FUN_004034a8(char *pcParm1);
void FUN_004034f4(char *param_1,char *param_2,undefined *param_3,char *param_4,undefined *param_5,char param_6,char param_7,undefined *param_8,char param_9);
ulong FUN_00403d93(char *pcParm1);
void FUN_00403ff0(undefined8 uParm1,ulong *puParm2);
void FUN_004042de(undefined8 uParm1,long lParm2);
void FUN_00404314(void);
void FUN_00404370(void);
void FUN_0040448d(uint uParm1);
void FUN_004045cc(void);
void FUN_00404736(void);
ulong FUN_00404970(uint uParm1,undefined8 *puParm2);
ulong FUN_00405147(undefined8 uParm1,undefined *puParm2);
ulong FUN_004054af(long *plParm1,undefined8 uParm2,undefined8 uParm3);
char * FUN_00405525(char *pcParm1,ulong uParm2);
void FUN_00405ac1(undefined8 uParm1);
void FUN_00405b7a(char *pcParm1);
void FUN_00405b92(char *pcParm1);
undefined * FUN_00405bca(undefined8 uParm1);
char * FUN_00405c1d(char *pcParm1);
void FUN_00405c6a(long lParm1,undefined8 uParm2,undefined8 *puParm3);
ulong FUN_00405cd9(long lParm1,undefined8 uParm2,undefined8 *puParm3);
ulong FUN_00405d10(ulong uParm1);
ulong FUN_00405d88(ulong uParm1);
undefined8 FUN_00405ddd(long lParm1);
ulong FUN_00405e5e(ulong uParm1,long lParm2);
void FUN_00405ef4(long lParm1,undefined8 *puParm2);
long * FUN_00405f08(long *plParm1,long lParm2,undefined8 uParm3,char cParm4);
long * FUN_00405f2a(long lParm1,long *plParm2,long **pplParm3,char cParm4);
void FUN_00405fe6(long lParm1);
undefined8 FUN_0040600b(long lParm1,long **pplParm2,char cParm3);
long * FUN_00406110(long lParm1,long *plParm2);
long * FUN_00406159(undefined8 uParm1,undefined1 *puParm2,undefined *puParm3,undefined *puParm4,long lParm5);
void FUN_00406239(ulong **ppuParm1);
ulong FUN_004062e4(long *plParm1,undefined8 uParm2);
undefined8 FUN_00406421(long lParm1,long lParm2,long *plParm3);
undefined8 FUN_00406651(undefined8 uParm1,undefined8 uParm2);
void FUN_004066e6(undefined8 *puParm1);
void FUN_004066fc(void);
void FUN_004067c0(long lParm1,ulong uParm2,byte *pbParm3,char *pcParm4);
long FUN_00406864(void);
undefined8 FUN_00406889(char *pcParm1,undefined8 *puParm2,uint *puParm3);
char * FUN_004069a5(ulong uParm1,char *pcParm2,ulong uParm3,ulong uParm4,ulong uParm5);
undefined8 FUN_004070fe(undefined8 uParm1,undefined8 uParm2,long *plParm3);
char * FUN_00407124(ulong uParm1,long lParm2);
void FUN_00407169(undefined *puParm1,undefined *puParm2,long lParm3);
undefined8 FUN_00407197(int *piParm1);
ulong FUN_004071db(int *piParm1,ulong uParm2);
long FUN_00407248(char *pcParm1,long lParm2,long lParm3,ulong *puParm4,int iParm5,uint uParm6);
long FUN_004074e8(undefined8 uParm1,ulong *puParm2,ulong uParm3,uint uParm4);
ulong FUN_0040757f(byte *pbParm1,long lParm2,ulong uParm3);
void FUN_00407726(char *pcParm1,ulong uParm2);
int * FUN_00407751(byte *pbParm1);
int * FUN_004077ea(int *piParm1,long lParm2);
ulong FUN_0040784d(long lParm1,long lParm2);
undefined8 FUN_0040786b(long lParm1,undefined8 uParm2,byte bParm3);
undefined8 FUN_004078b9(long lParm1,undefined8 uParm2,byte bParm3,char cParm4);
undefined8 FUN_0040791a(long lParm1,undefined8 uParm2,byte bParm3,char cParm4,char cParm5);
undefined8 FUN_0040797e(long lParm1,undefined8 uParm2,byte bParm3,char cParm4,char cParm5,char cParm6);
undefined8 FUN_004079f5(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7);
undefined8 FUN_00407a79(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8);
undefined8 FUN_00407b13(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined8 FUN_00407bb6(long param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10);
undefined8 FUN_00407c6c(byte *param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10,char param_11);
undefined * FUN_00407d31(char *pcParm1,int iParm2);
ulong FUN_00407df4(undefined *param_1,ulong param_2,char *param_3,ulong param_4,uint param_5,ulong param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_00408d66(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
ulong FUN_00408ef7(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
void FUN_00408f2a(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00408f58(ulong uParm1,undefined8 uParm2);
void FUN_00409004(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00409078(void);
void FUN_0040908b(undefined8 uParm1,undefined8 uParm2);
void FUN_004090a0(undefined8 uParm1);
ulong FUN_004090b6(int *piParm1);
void FUN_00409105(uint *puParm1);
void FUN_00409126(int *piParm1);
void FUN_00409142(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_00409386(void);
void FUN_004093d8(void);
void FUN_0040945d(ulong uParm1,ulong uParm2);
void FUN_00409477(ulong uParm1,ulong uParm2);
void FUN_004094aa(void);
long FUN_004094b8(long lParm1,ulong uParm2);
void FUN_004094eb(undefined8 uParm1,ulong uParm2,ulong uParm3);
void FUN_00409521(undefined8 uParm1,undefined8 uParm2);
void FUN_0040954a(char *pcParm1);
long FUN_00409572(void);
long FUN_0040959a(void);
void FUN_004095c6(uint uParm1,int iParm2,undefined uParm3,long lParm4,undefined8 uParm5,uint uParm6);
ulong FUN_00409647(int iParm1,undefined8 uParm2,char cParm3,undefined8 uParm4,undefined8 uParm5);
ulong FUN_0040965f(int iParm1);
ulong FUN_00409674(ulong *puParm1,int iParm2);
ulong FUN_0040969e(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_004096db(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
void FUN_00409a12(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_00409aa1(ulong uParm1,ulong uParm2);
void FUN_00409aeb(undefined4 *puParm1);
long FUN_00409af2(long lParm1);
void FUN_00409b05(uint *puParm1);
void FUN_00409b15(int *piParm1);
undefined8 FUN_00409b49(uint *puParm1,undefined8 uParm2);
ulong FUN_00409b86(char *pcParm1);
ulong FUN_00409e10(undefined8 uParm1);
void FUN_00409e2d(void);
void FUN_00409e52(undefined8 uParm1);
void FUN_00409e92(undefined auParm1 [16],undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong uParm9,ulong uParm10,undefined8 uParm11,undefined8 uParm12,undefined8 uParm13,undefined8 uParm14);
void FUN_00409ee8(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_00409fbb(void);
undefined8 FUN_00409fc1(undefined8 uParm1,undefined8 uParm2,long *plParm3);
long FUN_0040a03e(long lParm1,ulong uParm2);
void FUN_0040a4d1(long lParm1,int *piParm2);
char * FUN_0040a5ba(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_0040a63f(int param_1,undefined8 *param_2,char *param_3,char **param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
ulong FUN_0040abd3(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
void FUN_0040b076(void);
void FUN_0040b0cc(void);
void FUN_0040b0e2(void);
undefined8 FUN_0040b0f0(char *pcParm1,long lParm2);
void FUN_0040b167(long lParm1);
ulong FUN_0040b181(uint *puParm1,byte *pbParm2,long lParm3);
ulong * FUN_0040b1e0(ulong *puParm1,undefined8 uParm2,ulong uParm3);
undefined8 FUN_0040b2b7(void);
void FUN_0040b2bf(char *pcParm1);
void FUN_0040b34d(undefined8 *puParm1);
byte * FUN_0040b386(void);
void FUN_0040bb43(void);
ulong FUN_0040bb56(uint param_1,undefined8 param_2,uint param_3,ulong param_4);
void FUN_0040bc3d(long lParm1,long lParm2);
void FUN_0040bc69(void);
undefined8 FUN_0040bc77(char *pcParm1,long lParm2);
ulong FUN_0040bcdc(long lParm1,ulong uParm2,long *plParm3);
long FUN_0040be00(long lParm1,long lParm2,long lParm3,ulong uParm4);
byte * FUN_0040c03b(long lParm1,byte *pbParm2,byte *pbParm3,byte *pbParm4);
char * FUN_0040c354(char *pcParm1,char *pcParm2);
ulong FUN_0040c3f7(long *plParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040c453(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040c522(int *piParm1,long lParm2);
ulong FUN_0040c590(long lParm1,long lParm2);
undefined8 FUN_0040c5ae(long lParm1,undefined8 uParm2,char cParm3);
undefined8 FUN_0040c5d0(long lParm1,undefined8 uParm2,char cParm3,char cParm4);
undefined8 FUN_0040c5f5(long lParm1,undefined8 uParm2,char cParm3,char cParm4,char cParm5);
undefined8 FUN_0040c621(long lParm1,undefined8 uParm2,char cParm3,char cParm4,char cParm5,char cParm6);
undefined8 FUN_0040c650(long param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7);
undefined8 FUN_0040c68a(long param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8);
undefined8 FUN_0040c6d7(long param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined8 FUN_0040c722(long param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10);
undefined8 FUN_0040c782(char *param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10,char param_11);
void FUN_0040c7f4(ulong uParm1);
long FUN_0040c857(undefined8 uParm1,ulong uParm2);
long FUN_0040c92e(char *pcParm1,char **ppcParm2,long lParm3,long lParm4);
undefined FUN_0040ca51(int iParm1);;
ulong FUN_0040ca5b(uint uParm1);
ulong FUN_0040ca6a(byte *pbParm1,byte *pbParm2);
ulong FUN_0040cab2(ulong uParm1,char cParm2);
void FUN_0040cb7c(undefined8 param_1,byte param_2,ulong param_3);
undefined8 FUN_0040cbc8(void);
ulong FUN_0040cbd0(ulong uParm1);
undefined * FUN_0040cca0(void);
char * FUN_0040cf78(void);
ulong FUN_0040d04b(uint uParm1);
ulong FUN_0040d04e(uint uParm1);
ulong FUN_0040d082(long lParm1,long lParm2);
undefined8 FUN_0040d0a0(long lParm1,undefined8 uParm2,char cParm3);
undefined8 FUN_0040d0c2(long lParm1,undefined8 uParm2,char cParm3,char cParm4);
undefined8 FUN_0040d0e7(long lParm1,undefined8 uParm2,char cParm3,char cParm4,char cParm5);
undefined8 FUN_0040d113(long lParm1,undefined8 uParm2,char cParm3,char cParm4,char cParm5,char cParm6);
undefined8 FUN_0040d142(long param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7);
undefined8 FUN_0040d17c(long param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8);
undefined8 FUN_0040d1c9(long param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined8 FUN_0040d214(long param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10);
undefined8 FUN_0040d274(char *param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10,char param_11);
ulong FUN_0040d2e6(undefined8 uParm1);
ulong FUN_0040d4fa(uint uParm1,undefined8 uParm2);
undefined8 FUN_0040d69e(undefined8 uParm1);
ulong FUN_0040d721(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4);
undefined8 FUN_0040d8a0(ulong uParm1);
void FUN_0040d8ef(undefined8 uParm1);
ulong FUN_0040d907(long lParm1);
undefined8 FUN_0040d989(void);
void FUN_0040d99c(void);
void FUN_0040d9aa(void);
void FUN_0040d9b8(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040d9cd(ulong uParm1,char *pcParm2,long lParm3,ulong uParm4);
void FUN_0040da9b(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_0040dab1(char *pcParm1);
ulong FUN_0040db0f(char *pcParm1,long lParm2);
long FUN_0040db45(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
char * FUN_0040dc50(ulong uParm1,long lParm2,long lParm3);
ulong FUN_0040ddf3(void);
int * FUN_0040de27(undefined8 param_1,long *param_2);
int * FUN_0040e061(int *param_1,long *param_2);
int * FUN_0040e1bf(long **pplParm1,ulong uParm2,long **pplParm3,uint *puParm4,long **pplParm5,ulong uParm6);
long FUN_0040e716(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_0040ebb2(uint param_1);
uint * FUN_0040ebef(undefined8 uParm1,ulong *puParm2);
uint * FUN_0040ee1f(undefined8 uParm1,int *piParm2,ulong *puParm3);
void FUN_0040efca(uint uParm1);
void FUN_0040effc(char *param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
double FUN_00414934(int *piParm1);
void FUN_0041497d(int *param_1);
void FUN_004149fe(undefined8 uParm1);
long FUN_00414a1b(ulong uParm1,ulong uParm2);
void FUN_00414a2d(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00414a43(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00414a69(ulong uParm1,ulong uParm2);
ulong FUN_00414a74(long *plParm1,ulong *puParm2,int iParm3,long lParm4);
undefined8 FUN_00414b9b(uint *puParm1,ulong *puParm2);
undefined8 FUN_00414fc5(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_00415a71(undefined auParm1 [16]);
ulong FUN_00415a90(void);
void FUN_00415ac0(void);
undefined8 _DT_FINI(void);

