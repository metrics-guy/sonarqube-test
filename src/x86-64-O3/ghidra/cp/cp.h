typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined5;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_004037f0(void);
void entry(void);
void FUN_00403ee0(void);
void FUN_00403f50(void);
void FUN_00403fe0(void);
void FUN_00404020(void);
void FUN_00404040(void);
ulong FUN_00404080(undefined8 uParm1,long lParm2,undefined *puParm3);
void FUN_00404120(long lParm1);
undefined * FUN_00404180(undefined8 uParm1,undefined8 uParm2,ulong uParm3);
undefined8 FUN_004042c0(undefined8 uParm1,long lParm2,long lParm3,long *plParm4,char *pcParm5,long lParm6);
void FUN_00404810(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00404820(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
undefined8 FUN_00404a50(void);
void FUN_00404a70(void);
void FUN_00404c20(uint uParm1);
ulong FUN_00404df0(int iParm1,undefined8 *puParm2,long lParm3,char cParm4,undefined1 *puParm5);
ulong FUN_00405230(char *pcParm1);
long FUN_00405260(long lParm1,ulong uParm2);
undefined FUN_00405280(int iParm1);;
ulong FUN_00405290(int iParm1,undefined8 uParm2,uint uParm3);
ulong FUN_004052a0(long lParm1);
ulong FUN_004052e0(long lParm1,uint uParm2);
undefined8 FUN_00405300(uint *puParm1);
ulong FUN_00405410(undefined8 uParm1,ulong *puParm2,undefined8 uParm3,ulong *puParm4,int *piParm5,undefined *puParm6);
ulong FUN_00405920(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00405938(undefined8 uParm1);
void FUN_00405970(undefined8 uParm1,undefined8 uParm2,long lParm3);
ulong FUN_004059f0(undefined8 uParm1,undefined8 uParm2,byte bParm3,byte bParm4,char cParm5);
void FUN_00405ae0(undefined8 uParm1,ulong uParm2);
undefined8 FUN_00405af0(uint uParm1,ulong uParm2);
ulong FUN_00405ba0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_00405bf0(ulong uParm1,undefined8 uParm2,char cParm3,long lParm4);
ulong FUN_00405ca0(char *pcParm1,ulong uParm2);
ulong FUN_00405d00(uint param_1,uint param_2,long param_3,ulong param_4,ulong param_5,byte param_6,undefined8 param_7,undefined8 param_8,ulong param_9,long *param_10,char *param_11);
void FUN_004060f0(undefined8 uParm1,undefined8 uParm2,uint uParm3,undefined8 uParm4);
ulong FUN_00406110(void);
long FUN_00406140(long lParm1);
void FUN_00406170(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00406180(long *plParm1,long *plParm2,long *plParm3);
undefined8 FUN_004061c0(void);
undefined8 FUN_004061d0(void);
ulong FUN_004061f0(void);
ulong FUN_00406220(byte *pbParm1);
void FUN_00406250(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00406280(ulong param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,long param_6,int param_7,undefined8 param_8,undefined8 param_9,undefined *param_10);
void FUN_00406650(long lParm1,undefined8 uParm2,uint *puParm3);
ulong FUN_00406740(long lParm1,undefined8 uParm2,long lParm3);
ulong FUN_004067d0(long lParm1,long *plParm2,long *plParm3,undefined8 uParm4);
undefined8 FUN_004068f0(void);
ulong FUN_00406910(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,ulong uParm4,long lParm5);
ulong FUN_00406ac0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,long lParm4);
void FUN_00406b70(long lParm1);
void FUN_00406ba0(long lParm1);
void FUN_00406bd0(undefined4 *puParm1);
ulong FUN_00406c10(long lParm1);
ulong FUN_00406c40(long lParm1,undefined8 uParm2,uint uParm3,long lParm4,char cParm5,long lParm6);
ulong FUN_00406e00(void);
ulong FUN_00406e40(undefined8 param_1,char *param_2,long param_3,uint param_4,uint param_5,byte *param_6,long *param_7);
ulong FUN_004079b0(char *param_1,char *param_2,byte param_3,char **param_4,char **param_5,uint *param_6,uint param_7,char *param_8,undefined *param_9,undefined *param_10);
void FUN_00409b70(undefined8 uParm1,undefined8 uParm2,ulong uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_00409c00(undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined *param_6,byte *param_7,byte *param_8);
void FUN_00409e00(long lParm1);
void FUN_00409e20(undefined8 uParm1,undefined8 uParm2);
long FUN_00409e60(undefined8 uParm1,undefined8 uParm2);
long FUN_00409ea0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
long FUN_00409f20(void);
undefined8 FUN_00409f60(void);
void FUN_00409f70(undefined4 uParm1,undefined4 *puParm2);
ulong FUN_00409fa0(uint *puParm1);
long FUN_0040a2a0(long lParm1,long lParm2);
ulong FUN_0040a330(undefined4 uParm1,undefined8 uParm2,ulong uParm3,undefined8 uParm4,undefined4 uParm5,char cParm6);
ulong FUN_0040a460(undefined8 uParm1,ulong uParm2,undefined8 uParm3,char cParm4);
ulong FUN_0040a570(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040a5f0(undefined8 uParm1);
long FUN_0040a640(undefined8 uParm1,ulong uParm2);
long FUN_0040a750(undefined8 uParm1,long *plParm2,long lParm3,long lParm4);
void FUN_0040a8d0(undefined auParm1 [16],undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined auParm8 [16],undefined8 uParm9,undefined8 uParm10,long lParm11,undefined8 uParm12,undefined8 uParm13,undefined8 uParm14);
void FUN_0040a930(long *plParm1,long lParm2,long lParm3);
long FUN_0040aa10(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
ulong FUN_0040aa80(long *plParm1,ulong uParm2,long lParm3,long lParm4,long *plParm5);
void FUN_0040ad50(long lParm1,long lParm2);
void FUN_0040ae30(char *pcParm1);
long FUN_0040ae90(long lParm1,int iParm2,char cParm3);
long FUN_0040b0f0(long lParm1,int iParm2);
ulong FUN_0040b100(undefined8 uParm1,char *pcParm2);
ulong FUN_0040b170(undefined *puParm1,char *pcParm2);
ulong FUN_0040b1a0(ulong uParm1,ulong uParm2,ulong uParm3);
char * FUN_0040b210(char **ppcParm1,undefined8 uParm2,undefined8 uParm3);
char * FUN_0040b830(char *pcParm1);
long FUN_0040b9c0(void);
void FUN_0040ba70(char *pcParm1);
void FUN_0040ba90(char *pcParm1);
undefined * FUN_0040bad0(undefined8 uParm1);
char * FUN_0040bb40(char *pcParm1);
void FUN_0040bba0(long lParm1);
undefined FUN_0040bbd0(char *pcParm1);;
void FUN_0040bc10(void);
void FUN_0040bc20(undefined8 param_1,byte param_2,ulong param_3);
undefined8 * FUN_0040bc70(long lParm1,undefined8 uParm2,undefined *puParm3);
ulong FUN_0040bcf0(long lParm1,undefined8 uParm2,undefined *puParm3);
undefined8 FUN_0040bd30(undefined8 uParm1);
void FUN_0040bda0(uint uParm1,undefined *puParm2);
void FUN_0040beb0(char *pcParm1);
void FUN_0040bed0(char *pcParm1);
long FUN_0040bef0(long lParm1,char *pcParm2,undefined8 *puParm3);
long FUN_0040bfc0(uint uParm1,long lParm2,long lParm3);
ulong FUN_0040c040(ulong uParm1);
ulong FUN_0040c0b0(ulong uParm1);
undefined * FUN_0040c120(long *plParm1,ulong uParm2);
undefined8 FUN_0040c150(float **ppfParm1);
ulong FUN_0040c1d0(float fParm1,ulong uParm2,char cParm3);
void FUN_0040c260(undefined8 *puParm1,undefined8 *puParm2);
long FUN_0040c280(long lParm1,long lParm2,long **pplParm3,char cParm4);
void FUN_0040c390(long *plParm1);
undefined8 FUN_0040c3b0(long lParm1,long **pplParm2,char cParm3);
long FUN_0040c4f0(long lParm1,long lParm2);
long * FUN_0040c550(undefined8 uParm1,undefined1 *puParm2,undefined *puParm3,undefined *puParm4,long lParm5);
void FUN_0040c650(long **pplParm1);
undefined * FUN_0040c730(undefined8 *puParm1,undefined8 uParm2);
undefined * FUN_0040c8a0(ulong uParm1,undefined8 *puParm2,long *plParm3);
undefined8 FUN_0040cb00(undefined8 uParm1,undefined8 uParm2);
long FUN_0040cb40(long lParm1,undefined8 uParm2);
ulong FUN_0040cd20(undefined8 *puParm1,ulong uParm2);
void FUN_0040cdd0(undefined8 *puParm1);
long FUN_0040cdf0(long lParm1);
undefined8 FUN_0040cea0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4,uint uParm5);
void FUN_0040cef0(undefined8 uParm1,ulong uParm2,undefined4 uParm3);
int * FUN_0040cf10(int *piParm1,undefined8 *puParm2);
ulong FUN_0040d250(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8);
ulong FUN_0040d310(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9);
ulong FUN_0040d3e0(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9,byte param_10);
undefined8 FUN_0040d4d0(byte *param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined * FUN_0040d580(char *pcParm1,int iParm2);
undefined8 *FUN_0040d650(undefined *puParm1,undefined8 *puParm2,ulong uParm3,ulong uParm4,uint uParm5,uint uParm6,long lParm7,char *pcParm8,char *pcParm9);
undefined1 * FUN_0040e900(undefined1 *puParm1,undefined8 *puParm2,undefined8 uParm3,uint *puParm4);
ulong FUN_0040eaa0(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
void FUN_0040eae0(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040eb10(ulong uParm1,undefined8 uParm2);
void FUN_0040ebb0(uint uParm1,undefined8 uParm2,undefined8 uParm3);
undefined1 * FUN_0040ec40(undefined1 *puParm1,undefined8 *puParm2);
undefined1 * FUN_0040ec50(undefined8 *puParm1);
undefined8 FUN_0040ec60(undefined4 uParm1);
ulong FUN_0040ec80(uint uParm1,long lParm2,uint uParm3,long lParm4,uint uParm5);
long FUN_0040f080(uint uParm1,undefined8 uParm2,ulong uParm3);
ulong FUN_0040f0f0(undefined8 uParm1,undefined8 uParm2);
undefined * FUN_0040f220(long lParm1,uint uParm2);
undefined8 FUN_0040f4b0(undefined8 uParm1,ulong uParm2);
ulong FUN_0040f530(ulong uParm1,long lParm2,long lParm3);
undefined FUN_0040f540(undefined8 uParm1,ulong uParm2);;
ulong FUN_0040f5e0(long lParm1,int iParm2,undefined8 uParm3,code *pcParm4,long lParm5);
ulong FUN_0040f750(uint uParm1);
undefined8 FUN_0040f7b0(undefined8 uParm1);
ulong FUN_0040f7c0(undefined8 uParm1,undefined8 *puParm2,long lParm3,ulong uParm4);
ulong FUN_004103c0(undefined8 *puParm1);
void FUN_00410480(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00410490(long lParm1,long *plParm2);
ulong FUN_00410570(uint uParm1,long lParm2,undefined *puParm3);
ulong FUN_004108f0(long lParm1,undefined *puParm2);
ulong FUN_00410900(undefined8 uParm1,undefined8 *puParm2);
void FUN_00410b20(undefined8 uParm1,long lParm2,long lParm3,long lParm4,long lParm5);
void FUN_00410d70(void);
void FUN_00411000(void);
ulong FUN_00411090(void);
void FUN_004110d0(long lParm1);
void FUN_004110f0(long lParm1);
long FUN_00411100(long lParm1,ulong *puParm2);
long FUN_00411140(long lParm1,ulong *puParm2,ulong uParm3);
long FUN_00411170(long lParm1,ulong *puParm2,ulong uParm3);
void FUN_00411230(undefined8 uParm1);
ulong FUN_00411250(void);
ulong FUN_00411280(void);
ulong FUN_004112b0(void);
undefined8 FUN_004112f0(ulong uParm1,ulong uParm2);
void FUN_00411340(undefined8 uParm1);
ulong FUN_00411380(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_004113f0(void);
void FUN_00411410(void);
void FUN_00411440(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_00411520(ulong param_1,undefined8 param_2,ulong param_3);
undefined8 FUN_00411670(ulong uParm1);
void FUN_004116e0(undefined8 uParm1);
ulong FUN_004116f0(long lParm1);
undefined8 FUN_00411780(void);
void FUN_004117a0(void);
ulong FUN_004117c0(void);
ulong FUN_004117e0(ulong uParm1,long lParm2,long lParm3,ulong uParm4);
long FUN_004118a0(long lParm1,ulong uParm2);
void FUN_00411d20(long lParm1,int *piParm2);
ulong FUN_00411f70(int param_1,undefined8 *param_2,char *param_3,char **param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_004126e0(char *pcParm1,int *piParm2,int iParm3);
ulong FUN_004127a0(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,uint *param_7,uint param_8);
void FUN_00412c50(void);
void FUN_00412cb0(void);
undefined8 FUN_00412cd0(long lParm1,uint uParm2,uint uParm3);
ulong FUN_00412db0(uint uParm1,char *pcParm2,uint uParm3,undefined8 uParm4);
ulong FUN_00412f00(ulong uParm1,long lParm2,ulong uParm3,long lParm4,uint uParm5);
void FUN_00413070(void);
undefined8 FUN_00413090(long lParm1,long lParm2);
void FUN_00413100(void);
ulong FUN_00413120(uint *puParm1,byte *pbParm2,long lParm3);
ulong FUN_00413190(long lParm1,ulong uParm2);
undefined8 FUN_00413290(long lParm1,ulong uParm2);
undefined8 FUN_004132f0(long lParm1,uint uParm2,long lParm3);
void FUN_00413390(void);
ulong FUN_004133a0(uint param_1,undefined8 param_2,uint param_3,ulong param_4);
undefined8 FUN_004134a0(long lParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00413510(long lParm1,long lParm2);
ulong FUN_00413550(long lParm1,long lParm2);
ulong FUN_00413a70(uint uParm1,long lParm2,uint uParm3,long lParm4);
ulong FUN_00413a80(long lParm1);
void FUN_00413b10(void);
undefined8 FUN_00413b30(long lParm1,long lParm2);
undefined8 FUN_00413b90(undefined8 uParm1,ulong uParm2,long lParm3);
undefined8 FUN_00413c00(long lParm1);
undefined8 FUN_00413cd0(uint uParm1,long lParm2,uint uParm3);
ulong FUN_00413dc0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_00413e80(undefined8 uParm1,undefined8 uParm2,undefined4 uParm3,undefined4 *puParm4);
void FUN_00413e90(undefined8 uParm1,int iParm2,uint uParm3);
ulong FUN_00413eb0(uint *puParm1,undefined8 uParm2,uint uParm3);
undefined * FUN_00413ee0(uint uParm1,undefined8 uParm2);
undefined FUN_00413f20(int iParm1);;
ulong FUN_00413f30(uint uParm1);
ulong FUN_00413f40(byte *pbParm1,byte *pbParm2);
ulong FUN_00414150(ulong uParm1,char cParm2);
undefined8 FUN_00414230(void);
void FUN_00414240(undefined8 *puParm1);
ulong FUN_00414280(ulong uParm1);
ulong FUN_00414320(char *pcParm1,ulong uParm2);
undefined * FUN_00414360(void);
char * FUN_004146c0(void);
long FUN_00414780(long lParm1);
void FUN_00414790(undefined8 uParm1);
long * FUN_004147b0(void);
ulong FUN_004147e0(undefined8 *puParm1,ulong uParm2);
void FUN_004149f0(undefined8 uParm1);
ulong FUN_00414a10(undefined8 *puParm1);
void FUN_00414a40(undefined8 uParm1,undefined8 uParm2);
void FUN_00414c60(undefined4 *puParm1,ulong uParm2);
undefined8 * FUN_00414e70(long lParm1,ulong uParm2);
void FUN_00414f20(long *plParm1,ulong uParm2,ulong uParm3);
undefined8 FUN_00414f40(long *plParm1);
undefined8 FUN_00414f90(undefined8 uParm1);
undefined8 FUN_00414fa0(long lParm1,undefined8 uParm2);
void FUN_00414fb0(long *plParm1,long *plParm2);
void FUN_00415290(ulong *puParm1);
ulong FUN_00415530(ulong uParm1);
undefined8 FUN_00415540(long lParm1,uint uParm2,uint uParm3);
void FUN_00415670(undefined8 uParm1,undefined8 uParm2);
void FUN_00415690(undefined8 uParm1);
void FUN_00415720(void);
ulong FUN_00415870(char *pcParm1,long lParm2);
char * FUN_004158b0(ulong uParm1,long lParm2,long lParm3);
ulong FUN_00415af0(void);
undefined * FUN_00415b30(undefined8 param_1,undefined8 *param_2);
uint * FUN_00415da0(int *param_1,ulong *param_2);
undefined * FUN_00415f90(undefined8 uParm1,undefined8 *puParm2);
uint * FUN_004161a0(undefined8 uParm1,int *piParm2,ulong *puParm3);
undefined4 *FUN_00416310(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5,undefined8 *puParm6);
int * FUN_00416450(undefined8 *puParm1,long lParm2,undefined8 *puParm3,undefined8 *puParm4,uint **ppuParm5,int **ppiParm6);
undefined8 FUN_00416a00(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_00416fb0(uint param_1);
void FUN_00416ff0(uint uParm1);
long FUN_0041c320(undefined8 uParm1,undefined8 uParm2);
double FUN_0041c3c0(int *piParm1);
void FUN_0041c410(int *param_1);
long FUN_0041c490(ulong uParm1,ulong uParm2);
long FUN_0041c4b0(void);
undefined8 FUN_0041c4e0(uint *puParm1,ulong *puParm2);
undefined8 FUN_0041c700(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_0041d3f0(undefined auParm1 [16]);
ulong FUN_0041d410(void);
void FUN_0041d440(void);
undefined8 _DT_FINI(void);

