typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void entry(void);
void FUN_00402ce0(void);
void FUN_00402d50(void);
void FUN_00402de0(void);
ulong FUN_00402e20(int iParm1);
ulong FUN_00402e40(uint uParm1);
ulong FUN_00402e50(long *plParm1,long lParm2);
ulong FUN_00402f20(ulong *puParm1,ulong *puParm2);
void FUN_00402f50(void);
void FUN_00402f70(void);
undefined2 * FUN_00402f90(void);
undefined2 * FUN_00402fb0(byte *pbParm1);
char * FUN_00403290(undefined1 *puParm1);
char * FUN_00403320(void);
void FUN_00403540(void);
void FUN_00403570(void);
void FUN_004038b0(char **ppcParm1);
void FUN_00404320(long lParm1);
void FUN_00404350(byte *pbParm1,byte *pbParm2);
void FUN_004046a0(void);
void FUN_00404890(void);
void FUN_00404ca0(void);
void FUN_00404e60(void);
undefined8 FUN_00404f00(undefined8 uParm1,long lParm2,long lParm3);
void FUN_00404f80(int iParm1);
void FUN_004055a0(char *pcParm1,long *plParm2,long *plParm3);
void FUN_00405660(undefined8 uParm1);
void FUN_00405750(undefined8 uParm1,long *plParm2);
void FUN_00405840(void);
void FUN_004059f0(uint uParm1);
long FUN_00405b10(undefined8 uParm1,long *plParm2,long lParm3,long lParm4);
void FUN_00405c90(undefined auParm1 [16],undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined auParm8 [16],undefined8 uParm9,undefined8 uParm10,long lParm11,undefined8 uParm12,undefined8 uParm13,undefined8 uParm14);
void FUN_00405cf0(long *plParm1,long lParm2,long lParm3);
long FUN_00405dd0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
undefined8 FUN_00405ee0(uint uParm1);
long FUN_00405f40(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined * FUN_00406120(long lParm1);
long FUN_004061d0(undefined8 uParm1,long lParm2);
undefined8 * FUN_00406230(int *piParm1,undefined8 *puParm2);
ulong FUN_00406570(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8);
ulong FUN_00406630(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9);
ulong FUN_00406700(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9,byte param_10);
undefined8 FUN_004067f0(byte *param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined * FUN_004068a0(char *pcParm1,int iParm2);
undefined8 *FUN_00406970(undefined *puParm1,undefined8 *puParm2,ulong uParm3,ulong uParm4,uint uParm5,uint uParm6,long lParm7,char *pcParm8,char *pcParm9);
undefined8 * FUN_00407c20(undefined1 *puParm1,undefined8 *puParm2,char *pcParm3,uint *puParm4);
ulong FUN_00407dc0(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
void FUN_00407e00(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00407ec0(uint uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 * FUN_00407f50(undefined1 *puParm1,undefined8 *puParm2);
undefined8 * FUN_00407f60(undefined8 *puParm1);
long FUN_00407f70(undefined8 uParm1,ulong *puParm2);
long FUN_00408190(undefined8 uParm1,undefined8 uParm2);
void FUN_004081a0(undefined8 uParm1,long lParm2,long lParm3,long lParm4,long lParm5);
void FUN_004083f0(void);
void FUN_00408680(void);
void FUN_00408710(ulong uParm1,ulong uParm2);
void FUN_00408730(ulong uParm1,ulong uParm2);
void FUN_00408760(ulong uParm1,ulong uParm2);
long FUN_00408770(long lParm1,ulong *puParm2);
long FUN_004087b0(long lParm1,ulong *puParm2,ulong uParm3);
long FUN_00408840(void);
long FUN_00408870(long *plParm1,int iParm2);
undefined8 FUN_004088a0(long *plParm1,int iParm2);
ulong FUN_004088f0(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_00408920(char *pcParm1,char **ppcParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_00408c70(ulong uParm1,ulong uParm2);
void FUN_00408cc0(undefined8 uParm1);
ulong FUN_00408d00(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00408d70(void);
void FUN_00408d90(void);
void FUN_00408dc0(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00408ea0(undefined8 uParm1);
void FUN_00408f30(undefined8 uParm1);
ulong FUN_00408f40(long lParm1);
undefined8 FUN_00408fd0(void);
void FUN_00408ff0(long lParm1,int *piParm2);
ulong FUN_00409240(int param_1,undefined8 *param_2,char *param_3,char **param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_004099b0(char *pcParm1,int *piParm2,int iParm3);
ulong FUN_00409a70(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,uint *param_7,uint param_8);
void FUN_00409f20(void);
void FUN_00409f80(void);
void FUN_00409fa0(long lParm1);
ulong FUN_00409fc0(uint *puParm1,byte *pbParm2,long lParm3);
void FUN_0040a030(long lParm1,long lParm2);
ulong FUN_0040a070(int iParm1);
void FUN_0040a090(long lParm1,long lParm2);
void FUN_0040a0d0(ulong *puParm1,undefined4 uParm2);
ulong FUN_0040a0e0(long lParm1,long lParm2);
void FUN_0040a100(undefined4 *puParm1);
void FUN_0040a120(ulong *puParm1,ulong *puParm2);
void FUN_0040a170(ulong *puParm1,ulong *puParm2);
ulong FUN_0040a1c0(long lParm1,long lParm2);
ulong FUN_0040a210(long lParm1,long lParm2);
void FUN_0040a240(undefined8 uParm1,undefined8 uParm2,undefined8 *puParm3,long lParm4,byte bParm5,long lParm6);
void FUN_0040a2b0(long *plParm1);
ulong FUN_0040a320(long lParm1,long lParm2);
long FUN_0040a460(long *plParm1,long lParm2,uint uParm3);
undefined8 FUN_0040a4d0(char *pcParm1,long lParm2,ulong uParm3);
ulong FUN_0040a620(long lParm1,uint uParm2,long lParm3,long lParm4,long lParm5);
ulong FUN_0040a990(long lParm1,long lParm2,long lParm3,long lParm4,long lParm5,long lParm6);
ulong FUN_0040aa00(long *plParm1);
void FUN_0040aa90(long *plParm1,code *pcParm2,undefined8 uParm3);
void FUN_0040ab00(long *plParm1,code *pcParm2,undefined8 uParm3);
undefined8 FUN_0040ab60(long lParm1,ulong uParm2);
void FUN_0040ac00(long param_1,undefined8 param_2,long param_3,long param_4,undefined8 param_5,byte param_6,long param_7);
undefined8 FUN_0040acc0(long *plParm1,undefined8 uParm2);
void FUN_0040ae10(undefined4 *puParm1);
void FUN_0040ae20(undefined4 *puParm1);
void FUN_0040ae30(undefined4 *puParm1,undefined8 uParm2,undefined8 uParm3,ulong uParm4,undefined8 uParm5);
undefined8 FUN_0040af10(undefined4 *puParm1,undefined8 uParm2);
ulong FUN_0040af70(long *plParm1,long lParm2);
undefined8 FUN_0040b060(long *plParm1,long lParm2);
undefined8 FUN_0040b0b0(long *plParm1,ulong *puParm2,ulong uParm3);
undefined8 FUN_0040b1a0(long lParm1,undefined4 uParm2,long lParm3);
undefined8 FUN_0040b240(long lParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040b300(long lParm1,ulong uParm2,undefined8 uParm3);
undefined8 FUN_0040b390(long *plParm1,ulong uParm2);
ulong FUN_0040b560(ulong uParm1,long lParm2);
void FUN_0040b590(long lParm1);
void FUN_0040b610(long *plParm1);
long FUN_0040b7e0(long *plParm1,long lParm2,uint *puParm3);
undefined8 FUN_0040b890(long *plParm1);
undefined8 FUN_0040c2d0(undefined8 *puParm1,undefined8 uParm2,long lParm3,long lParm4,byte bParm5,long lParm6);
ulong FUN_0040c430(long *plParm1,int iParm2);
undefined8 FUN_0040c540(long lParm1,long lParm2);
undefined8 FUN_0040c5d0(long *plParm1,long lParm2);
void FUN_0040c7a0(undefined4 *puParm1,undefined *puParm2);
ulong FUN_0040c7c0(long lParm1,long lParm2,ulong uParm3);
ulong FUN_0040c8a0(long lParm1,undefined8 *puParm2,long lParm3);
void FUN_0040c9c0(long lParm1);
void FUN_0040cab0(undefined8 *puParm1);
void FUN_0040cad0(undefined8 *puParm1);
long FUN_0040cb20(long *plParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040cd90(long *plParm1,long lParm2,ulong uParm3);
undefined8 FUN_0040ce20(long *plParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
ulong FUN_0040d060(undefined4 *puParm1,long *plParm2,long lParm3,char cParm4);
undefined8 FUN_0040d2d0(long lParm1);
undefined8 FUN_0040d3d0(long *plParm1);
void FUN_0040d5c0(long lParm1);
void FUN_0040d620(long lParm1);
void FUN_0040d660(long *plParm1);
undefined8 FUN_0040d800(long lParm1,ulong uParm2,long lParm3,long lParm4,long lParm5);
void FUN_0040d950(long lParm1);
undefined8 FUN_0040da10(long *plParm1);
void FUN_0040da80(long lParm1);
undefined8 FUN_0040dc70(ulong *puParm1,ulong uParm2,ulong uParm3,int iParm4);
undefined8 FUN_0040df20(ulong uParm1,undefined8 uParm2,long lParm3,long *plParm4,int *piParm5,int *piParm6);
long * FUN_0040e1f0(long **pplParm1);
undefined8 FUN_0040e370(byte **ppbParm1,byte *pbParm2,uint uParm3);
undefined8 FUN_0040eb10(void);
long FUN_0040eb20(long lParm1,long *plParm2,long lParm3);
undefined8 FUN_0040eb80(long *plParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_0040ec80(long *plParm1,long *plParm2,long lParm3);
long FUN_0040ecc0(ulong uParm1,undefined8 *puParm2,uint uParm3);
ulong FUN_0040ed60(long lParm1,long lParm2,ulong uParm3);
long FUN_0040ede0(long *plParm1,long lParm2,long *plParm3,long lParm4,uint uParm5);
ulong FUN_0040ee30(long lParm1,undefined4 *puParm2,undefined8 uParm3,ulong uParm4);
long FUN_0040ef30(long lParm1,long *plParm2,long lParm3);
undefined8 FUN_0040efa0(long param_1,long *param_2,long *param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6,undefined8 param_7);
undefined8 FUN_0040f0b0(undefined4 *puParm1,long *plParm2,long lParm3,char *pcParm4);
long FUN_0040f1a0(undefined8 *puParm1,int *piParm2,long *plParm3,long *plParm4,undefined *puParm5);
undefined8 FUN_0040f280(long lParm1,long *plParm2,long *plParm3,undefined8 uParm4);
undefined8 FUN_0040f320(long *plParm1,long *plParm2,undefined8 *puParm3);
undefined8 FUN_0040f3a0(long *plParm1,long lParm2,long lParm3);
undefined8 FUN_0040f540(long *plParm1,ulong uParm2,undefined8 uParm3,long lParm4,undefined8 uParm5,long lParm6);
long FUN_0040f630(long *plParm1,long lParm2,ulong uParm3,undefined8 uParm4);
ulong * FUN_0040f890(undefined4 *puParm1,long lParm2,long lParm3,ulong uParm4);
ulong FUN_0040f970(long *plParm1);
long FUN_0040fb60(long *plParm1,long lParm2,undefined8 uParm3);
ulong * FUN_0040fce0(undefined4 *puParm1,long lParm2,long lParm3);
ulong FUN_0040fda0(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_0040fe50(long lParm1,long lParm2,long lParm3,undefined8 uParm4,uint uParm5);
undefined8 FUN_004100e0(long *plParm1,long *plParm2,long *plParm3,long *plParm4,long *plParm5);
ulong FUN_004102a0(long lParm1,long lParm2,long lParm3);
ulong FUN_00410390(long *plParm1,long lParm2,long lParm3,long lParm4);
void FUN_00410580(long lParm1,long lParm2,long lParm3,long lParm4,long lParm5,long lParm6);
undefined8 FUN_00410620(long lParm1,long lParm2,long *plParm3,undefined8 uParm4);
void FUN_004106a0(long lParm1);
undefined8 FUN_00410700(long *param_1,long param_2,undefined8 param_3,long *param_4,long *param_5,long param_6,long param_7);
undefined8 FUN_00410970(long *plParm1,long *plParm2,undefined8 *puParm3,long lParm4,undefined8 uParm5,undefined4 *puParm6);
undefined8 FUN_00410a30(undefined8 uParm1,byte *pbParm2);
undefined8 FUN_00410a70(long param_1,undefined8 param_2,long *param_3,ulong *param_4,ulong *param_5,byte *param_6,ulong param_7);
void FUN_00411190(long **pplParm1,long *plParm2,long *plParm3,undefined4 *puParm4);
ulong FUN_00411660(long lParm1,ulong *puParm2,long lParm3,long lParm4,long lParm5);
long FUN_00411960(long param_1,undefined8 param_2,long param_3,long *param_4,long param_5,long param_6,long param_7);
ulong FUN_00411cb0(undefined8 *puParm1,long lParm2,long lParm3,long *plParm4,char cParm5);
ulong FUN_00411fb0(long lParm1,long lParm2,long *plParm3,long *plParm4,undefined8 uParm5);
ulong FUN_00412200(long param_1,long *param_2,long param_3,long param_4,undefined8 param_5,long param_6,uint param_7);
undefined8 FUN_004126f0(long lParm1,long *plParm2,undefined8 *puParm3,undefined8 uParm4,long lParm5);
undefined8 FUN_00412770(long lParm1,long lParm2,long lParm3);
ulong FUN_00412b10(long lParm1,long *plParm2,long *plParm3);
long FUN_00412ea0(int *piParm1,long lParm2,long lParm3);
long FUN_00413060(int *piParm1,long lParm2);
ulong FUN_004130d0(long lParm1,long *plParm2,long *plParm3);
ulong FUN_00413370(long lParm1,long *plParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_00413400(long lParm1,long *plParm2,long lParm3,undefined8 uParm4);
ulong FUN_00413590(long lParm1,long *plParm2,long lParm3,long *plParm4,long *plParm5);
ulong FUN_00413910(long lParm1,long *plParm2,long lParm3,long lParm4);
ulong FUN_00413ac0(long lParm1,long *plParm2);
ulong FUN_00413bd0(long lParm1);
undefined8 FUN_00413e10(undefined4 *puParm1,long lParm2,undefined *puParm3,int iParm4,undefined8 uParm5,char cParm6);
void FUN_00413ef0(long lParm1,undefined8 uParm2,undefined uParm3);
void FUN_00413f20(long lParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00413f50(long lParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_00413f80(undefined4 *puParm1,long *plParm2,long lParm3);
ulong FUN_00414100(undefined8 uParm1,long lParm2);
long FUN_00414180(undefined8 *puParm1,int *piParm2,undefined *puParm3);
long ** FUN_00414230(long **pplParm1,long lParm2);
long FUN_004142e0(long lParm1,undefined8 uParm2,undefined8 uParm3,char *pcParm4,byte bParm5,int *piParm6);
long FUN_004144f0(long lParm1,long lParm2,long lParm3,ulong uParm4,int *piParm5);
ulong FUN_00414a90(long *plParm1);
undefined8 FUN_00414b20(long *plParm1,long lParm2,ulong uParm3);
void FUN_00415190(undefined8 uParm1,long lParm2);
long FUN_004151b0(undefined8 uParm1,byte *pbParm2,undefined8 uParm3);
long FUN_00415260(long lParm1,long lParm2,long lParm3,undefined4 *puParm4,ulong uParm5,undefined4 *puParm6);
long FUN_00415640(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,undefined8 uParm5,int *piParm6);
long FUN_00415750(long lParm1,long *plParm2,undefined8 *puParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_00415d10(undefined8 uParm1,undefined8 *puParm2,long lParm3,undefined8 uParm4,long lParm5,int *piParm6);
long FUN_00415e50(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_00415fa0(undefined8 uParm1,long *plParm2,ulong uParm3,int *piParm4);
ulong FUN_00416070(long *plParm1,undefined8 uParm2,undefined8 uParm3,ulong uParm4);
ulong FUN_00416210(ulong *puParm1);
long FUN_00416220(long *plParm1,long lParm2,long lParm3,ulong *puParm4);
ulong FUN_00416870(long lParm1,long lParm2);
undefined8 FUN_00416d90(int *piParm1,long lParm2,long lParm3);
long FUN_00416e60(long lParm1,char cParm2,long *plParm3);
ulong FUN_00417200(long *param_1,long param_2,long param_3,long param_4,long param_5,undefined8 param_6,ulong param_7,ulong *param_8,uint param_9);
undefined1 * FUN_00417c70(undefined8 uParm1,undefined8 uParm2,long lParm3);
undefined8 FUN_00417cd0(long *plParm1);
long FUN_00417d90(long param_1,undefined8 param_2,long param_3,long param_4,long param_5,undefined8 param_6,ulong *param_7,char param_8);
void FUN_00418010(void);
void FUN_00418030(void);
ulong FUN_00418050(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined FUN_00418110(int iParm1);;
ulong FUN_00418120(uint uParm1);
ulong FUN_00418130(byte *pbParm1,byte *pbParm2);
undefined8 FUN_004181f0(void);
ulong FUN_00418200(ulong uParm1);
undefined * FUN_004182a0(void);
char * FUN_00418600(void);
undefined8 * FUN_004186c0(ulong *puParm1,ulong *puParm2,ulong uParm3,long *plParm4,ulong *puParm5);
undefined8 * FUN_00418950(ulong uParm1,char *pcParm2);
long FUN_00418a10(char *pcParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00418ad0(char *pcParm1,long lParm2);
char * FUN_00418b10(ulong uParm1,long lParm2,long lParm3);
ulong FUN_00418d50(void);
undefined8 * FUN_00418d90(undefined8 param_1,undefined8 *param_2);
undefined8 * FUN_00419000(int *param_1,ulong *param_2);
undefined8 * FUN_004191f0(undefined8 uParm1,undefined8 *puParm2);
undefined8 * FUN_00419400(undefined8 uParm1,int *piParm2,ulong *puParm3);
undefined4 *FUN_00419570(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5,undefined8 *puParm6);
undefined8 *FUN_004196b0(undefined8 *puParm1,long lParm2,undefined8 *puParm3,undefined8 *puParm4,uint **ppuParm5,int **ppiParm6);
undefined8 FUN_00419c60(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_0041a210(uint param_1);
void FUN_0041a250(uint uParm1);
double FUN_0041f580(int *piParm1);
void FUN_0041f5d0(int *param_1);
long FUN_0041f650(ulong uParm1,ulong uParm2);
long FUN_0041f670(void);
undefined8 FUN_0041f6a0(uint *puParm1,ulong *puParm2);
undefined8 FUN_0041f8c0(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_004205b0(undefined auParm1 [16]);
ulong FUN_004205d0(void);
void FUN_00420600(void);
undefined8 _DT_FINI(void);

