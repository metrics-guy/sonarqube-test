typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00402650(void);
void entry(void);
void FUN_00402be0(void);
void FUN_00402c50(void);
void FUN_00402ce0(void);
ulong FUN_00402d20(uint uParm1);
void FUN_00402d40(void);
void FUN_00402d60(void);
ulong FUN_00402da0(undefined8 uParm1);
long FUN_00402eb0(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00402f80(char *pcParm1,undefined8 uParm2);
void FUN_00403620(void);
void FUN_004037d0(uint uParm1);
long FUN_004038f0(long lParm1,long lParm2);
ulong FUN_00403980(undefined4 uParm1,undefined8 uParm2,ulong uParm3,undefined8 uParm4,undefined4 uParm5,char cParm6);
ulong FUN_00403ab0(undefined8 uParm1,ulong uParm2,undefined8 uParm3,char cParm4);
ulong FUN_00403bc0(char *pcParm1,char *pcParm2);
undefined8 FUN_00403c30(undefined8 uParm1,long *plParm2,ulong *puParm3);
ulong FUN_00403cb0(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00403e00(long *plParm1,ulong uParm2,long lParm3,long lParm4,long *plParm5);
void FUN_004040d0(long lParm1,long lParm2);
void FUN_004041b0(char *pcParm1);
long FUN_00404210(long lParm1,int iParm2,char cParm3);
ulong FUN_00404470(undefined8 uParm1,char *pcParm2);
ulong FUN_004044e0(undefined *puParm1,char *pcParm2);
char * FUN_00404510(char **ppcParm1,undefined8 uParm2,undefined8 uParm3);
char * FUN_00404590(char *pcParm1,ulong uParm2);
char * FUN_00404b30(char *pcParm1);
long FUN_00404cc0(void);
void FUN_00404d70(char *pcParm1);
void FUN_00404d90(char *pcParm1);
undefined * FUN_00404dd0(undefined8 uParm1);
char * FUN_00404e40(char *pcParm1);
void FUN_00404ea0(long lParm1);
undefined FUN_00404ed0(char *pcParm1);;
undefined8 * FUN_00404f10(long lParm1,undefined8 uParm2,undefined *puParm3);
ulong FUN_00404f90(long lParm1,undefined8 uParm2,undefined *puParm3);
void FUN_00404fd0(char *pcParm1);
void FUN_00404ff0(char *pcParm1);
long FUN_00405010(long lParm1,char *pcParm2,undefined8 *puParm3);
ulong FUN_004050e0(ulong uParm1);
ulong FUN_00405150(ulong uParm1);
ulong FUN_004051c0(long *plParm1,ulong uParm2);
undefined8 FUN_004051f0(float **ppfParm1);
ulong FUN_00405270(float fParm1,ulong uParm2,char cParm3);
void FUN_00405300(undefined8 *puParm1,undefined8 *puParm2);
long FUN_00405320(long lParm1,long lParm2,long **pplParm3,char cParm4);
void FUN_00405430(long *plParm1);
undefined8 FUN_00405450(long lParm1,long **pplParm2,char cParm3);
long FUN_00405590(long lParm1,long lParm2);
long * FUN_004055f0(undefined8 uParm1,undefined1 *puParm2,undefined *puParm3,undefined *puParm4,long lParm5);
void FUN_004056f0(long **pplParm1);
ulong FUN_004057d0(undefined8 *puParm1,undefined8 uParm2);
ulong FUN_00405940(ulong uParm1,undefined8 *puParm2,long *plParm3);
undefined8 FUN_00405ba0(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00405be0(undefined8 *puParm1,ulong uParm2);
void FUN_00405c80(undefined8 *puParm1);
int * FUN_00405ca0(long lParm1);
int * FUN_00405d50(int *piParm1,undefined8 *puParm2);
ulong FUN_00406090(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8);
ulong FUN_00406150(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9);
ulong FUN_00406220(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9,byte param_10);
undefined8 FUN_00406310(byte *param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined * FUN_004063c0(char *pcParm1,int iParm2);
undefined8 *FUN_00406490(undefined *puParm1,undefined8 *puParm2,ulong uParm3,ulong uParm4,uint uParm5,uint uParm6,long lParm7,char *pcParm8,char *pcParm9);
undefined1 * FUN_00407740(undefined1 *puParm1,undefined8 *puParm2,undefined8 uParm3,uint *puParm4);
ulong FUN_004078e0(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
void FUN_00407920(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00407950(ulong uParm1,undefined8 uParm2);
void FUN_004079f0(uint uParm1,undefined8 uParm2,undefined8 uParm3);
undefined1 * FUN_00407a80(undefined1 *puParm1,undefined8 *puParm2);
undefined1 * FUN_00407a90(undefined8 *puParm1);
undefined8 FUN_00407aa0(undefined4 uParm1);
ulong FUN_00407ac0(uint uParm1,long lParm2,uint uParm3,long lParm4,uint uParm5);
ulong FUN_00407ec0(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00407fe0(ulong uParm1,long lParm2,long lParm3);
undefined FUN_00407ff0(undefined8 uParm1,ulong uParm2);;
ulong FUN_00408090(long lParm1,int iParm2,undefined8 uParm3,code *pcParm4,long lParm5);
ulong FUN_00408200(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long *plParm5,undefined8 uParm6);
void FUN_00408450(void);
void FUN_004086e0(void);
void FUN_00408770(long lParm1);
void FUN_00408790(long lParm1);
long FUN_004087a0(long lParm1,long lParm2);
void FUN_00408810(undefined8 uParm1);
ulong FUN_00408830(void);
ulong FUN_00408860(void);
ulong FUN_00408890(void);
undefined8 FUN_004088d0(ulong uParm1,ulong uParm2);
void FUN_00408920(undefined8 uParm1);
ulong FUN_00408960(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_004089d0(void);
void FUN_004089f0(void);
void FUN_00408a20(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_00408b00(ulong param_1,undefined8 param_2,ulong param_3);
undefined8 FUN_00408c50(ulong uParm1);
void FUN_00408cc0(undefined8 uParm1);
ulong FUN_00408cd0(long lParm1);
undefined8 FUN_00408d60(void);
void FUN_00408d80(void);
ulong FUN_00408da0(void);
ulong FUN_00408dc0(ulong uParm1,long lParm2,long lParm3,ulong uParm4);
long FUN_00408e80(long lParm1,ulong uParm2);
void FUN_00409300(long lParm1,int *piParm2);
ulong FUN_00409550(int param_1,undefined8 *param_2,char *param_3,char **param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_00409cc0(char *pcParm1,int *piParm2,int iParm3);
ulong FUN_00409d80(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,uint *param_7,uint param_8);
void FUN_0040a230(void);
void FUN_0040a290(void);
ulong FUN_0040a2b0(uint uParm1,char *pcParm2,uint uParm3,undefined8 uParm4);
ulong FUN_0040a400(ulong uParm1,long lParm2,ulong uParm3,long lParm4,uint uParm5);
void FUN_0040a570(void);
undefined8 FUN_0040a590(long lParm1,long lParm2);
void FUN_0040a600(void);
ulong FUN_0040a620(uint *puParm1,byte *pbParm2,long lParm3);
void FUN_0040a790(void);
ulong FUN_0040a7a0(uint param_1,undefined8 param_2,uint param_3,ulong param_4);
void FUN_0040a8a0(long lParm1,long lParm2);
ulong FUN_0040a8e0(long lParm1,long lParm2);
ulong FUN_0040ae00(uint uParm1,long lParm2,uint uParm3,long lParm4);
ulong FUN_0040ae10(long lParm1);
void FUN_0040aea0(void);
undefined8 FUN_0040aec0(long lParm1,long lParm2);
undefined8 FUN_0040af20(undefined8 uParm1,ulong uParm2,long lParm3);
undefined8 FUN_0040af90(uint uParm1,long lParm2,uint uParm3);
ulong FUN_0040b080(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_0040b140(undefined8 uParm1,ulong uParm2);
undefined * FUN_0040b240(uint uParm1,undefined8 uParm2);
long FUN_0040b270(undefined8 uParm1,long *plParm2,long lParm3,long lParm4);
void FUN_0040b3f0(undefined auParm1 [16],undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined auParm8 [16],undefined8 uParm9,undefined8 uParm10,long lParm11,undefined8 uParm12,undefined8 uParm13,undefined8 uParm14);
void FUN_0040b450(long *plParm1,long lParm2,long lParm3);
long FUN_0040b530(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
undefined FUN_0040b5c0(int iParm1);;
ulong FUN_0040b5d0(uint uParm1);
ulong FUN_0040b5e0(byte *pbParm1,byte *pbParm2);
ulong FUN_0040b7f0(ulong uParm1,char cParm2);
undefined8 FUN_0040b8d0(void);
ulong FUN_0040b8e0(ulong uParm1);
ulong FUN_0040b980(char *pcParm1,ulong uParm2);
undefined * FUN_0040b9c0(void);
char * FUN_0040bd20(void);
long FUN_0040bde0(long lParm1);
void FUN_0040bdf0(undefined8 uParm1);
long * FUN_0040be10(void);
ulong FUN_0040be40(undefined8 *puParm1,ulong uParm2);
void FUN_0040c050(undefined8 uParm1);
ulong FUN_0040c070(undefined8 *puParm1);
void FUN_0040c0a0(undefined8 uParm1,undefined8 uParm2);
void FUN_0040c2c0(undefined4 *puParm1,ulong uParm2);
undefined8 * FUN_0040c4d0(long lParm1,ulong uParm2);
void FUN_0040c580(long *plParm1,ulong uParm2,ulong uParm3);
undefined8 FUN_0040c5a0(long *plParm1);
undefined8 FUN_0040c5f0(undefined8 uParm1);
undefined8 FUN_0040c600(long lParm1,undefined8 uParm2);
void FUN_0040c610(long *plParm1,long *plParm2);
void FUN_0040c8f0(ulong *puParm1);
void FUN_0040cb90(undefined8 uParm1,undefined8 uParm2);
void FUN_0040cbb0(undefined8 uParm1);
void FUN_0040cc40(void);
undefined8 FUN_0040cd10(long lParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040ce00(char *pcParm1,long lParm2);
char * FUN_0040ce40(ulong uParm1,long lParm2,long lParm3);
ulong FUN_0040d080(void);
ulong FUN_0040d0c0(undefined8 param_1,undefined8 *param_2);
uint * FUN_0040d330(int *param_1,ulong *param_2);
ulong FUN_0040d520(undefined8 uParm1,undefined8 *puParm2);
uint * FUN_0040d730(undefined8 uParm1,int *piParm2,ulong *puParm3);
undefined4 *FUN_0040d8a0(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5,undefined8 *puParm6);
int * FUN_0040d9e0(undefined8 *puParm1,long lParm2,undefined8 *puParm3,undefined8 *puParm4,uint **ppuParm5,int **ppiParm6);
undefined8 FUN_0040df90(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_0040e540(uint param_1);
void FUN_0040e580(uint uParm1);
long FUN_004138b0(undefined8 uParm1,undefined8 uParm2);
double FUN_00413950(int *piParm1);
void FUN_004139a0(int *param_1);
ulong FUN_00413a20(ulong uParm1);
long FUN_00413a30(ulong uParm1,ulong uParm2);
long FUN_00413a50(void);
undefined8 FUN_00413a80(uint *puParm1,ulong *puParm2);
undefined8 FUN_00413ca0(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_00414990(undefined auParm1 [16]);
ulong FUN_004149b0(void);
void FUN_004149e0(void);
undefined8 _DT_FINI(void);

