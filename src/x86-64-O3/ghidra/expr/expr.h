typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void entry(void);
void FUN_00402740(void);
void FUN_004027b0(void);
void FUN_00402840(void);
void FUN_00402880(undefined8 *puParm1,undefined8 uParm2);
undefined8 FUN_004028d0(char *pcParm1);
ulong FUN_00402910(void);
ulong FUN_00402920(undefined8 uParm1);
void FUN_00402a50(void);
void FUN_00402a90(int *piParm1);
long FUN_00402ac0(long lParm1,ulong uParm2);
long FUN_00402b40(char *pcParm1,char *pcParm2);
undefined4 * FUN_00402cc0(undefined8 uParm1);
void FUN_00402d00(undefined8 uParm1);
undefined * FUN_00402d30(long lParm1,ulong uParm2,ulong uParm3);
ulong FUN_00402e80(long lParm1);
undefined8 *FUN_00402ea0(int *piParm1,undefined8 *puParm2,undefined8 uParm3,undefined8 uParm4,ulong uParm5);
ulong FUN_00402f30(ulong uParm1);
undefined8 FUN_00402f40(undefined8 uParm1);
long FUN_00402f50(void);
void FUN_00402f90(undefined8 uParm1);
undefined8 * FUN_00402fb0(uint *puParm1,undefined8 *puParm2);
ulong FUN_00402fe0(undefined8 uParm1,undefined8 uParm2);
undefined8 * FUN_00403010(int *piParm1,undefined8 *puParm2);
void FUN_00403050(void);
undefined8 FUN_00403080(long lParm1,long lParm2);
ulong FUN_00403260(undefined8 uParm1,undefined8 uParm2);
undefined8 * FUN_00403290(int *piParm1,undefined8 *puParm2);
long FUN_00403310(byte bParm1);
long FUN_00403400(byte bParm1);
undefined8 FUN_00403670(byte bParm1);
undefined8 FUN_00403780(byte bParm1);
undefined4 * FUN_00403980(ulong uParm1);
undefined8 FUN_00403bc0(byte bParm1);
long FUN_00403c50(byte bParm1);
void FUN_00403da0(void);
void FUN_00403f50(uint uParm1);
void FUN_00404090(void);
char * FUN_00404140(ulong uParm1,long lParm2);
void FUN_004041e0(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,code *param_6);
byte * FUN_004042f0(byte *pbParm1,ulong uParm2);
long FUN_004043a0(long lParm1);
void FUN_00404430(char *pcParm1);
undefined8 * FUN_004045b0(long lParm1);
undefined8 * FUN_00404660(int *piParm1,undefined8 *puParm2);
ulong FUN_004049a0(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8);
ulong FUN_00404a60(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9);
ulong FUN_00404b30(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9,byte param_10);
undefined8 FUN_00404c20(byte *param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined * FUN_00404cd0(char *pcParm1,int iParm2);
undefined8 *FUN_00404da0(undefined *puParm1,undefined8 *puParm2,ulong uParm3,ulong uParm4,uint uParm5,uint uParm6,long lParm7,char *pcParm8,char *pcParm9);
undefined8 * FUN_00406050(undefined1 *puParm1,undefined8 *puParm2,undefined8 uParm3,uint *puParm4);
void FUN_00406230(uint uParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_004062f0(long lParm1,long lParm2);
ulong FUN_00406320(byte *pbParm1,byte *pbParm2);
ulong thunk_FUN_00406320(byte *pbParm1,byte *pbParm2);
void FUN_004065f0(undefined8 uParm1,long lParm2,long lParm3,long lParm4,long lParm5);
void FUN_00406840(void);
void FUN_00406ad0(long lParm1);
void FUN_00406af0(long lParm1);
long FUN_00406b00(long lParm1,long lParm2);
void FUN_00406b70(undefined8 uParm1);
undefined8 FUN_00406b90(void);
undefined8 FUN_00406bc0(long *plParm1,int iParm2);
ulong FUN_00406c10(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_00406c40(char *pcParm1,char **ppcParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_00406f90(ulong uParm1,ulong uParm2);
void FUN_00406fe0(undefined8 uParm1);
ulong FUN_00407020(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00407090(void);
void FUN_004070b0(void);
void FUN_004070e0(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_004071c0(long lParm1,int *piParm2);
ulong FUN_00407410(int param_1,undefined8 *param_2,char *param_3,char **param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_00407b80(char *pcParm1,int *piParm2,int iParm3);
ulong FUN_00407c40(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,uint *param_7,uint param_8);
void FUN_004080f0(void);
void FUN_00408150(void);
void FUN_00408170(long lParm1);
ulong FUN_00408190(uint *puParm1,byte *pbParm2,long lParm3);
ulong * FUN_00408200(ulong *puParm1,undefined8 uParm2,ulong uParm3);
void FUN_004082e0(long lParm1,long lParm2);
ulong FUN_00408320(int iParm1);
void FUN_00408340(long lParm1,long lParm2);
void FUN_00408380(ulong *puParm1,undefined4 uParm2);
ulong FUN_00408390(long lParm1,long lParm2);
void FUN_004083b0(undefined4 *puParm1);
void FUN_004083d0(ulong *puParm1,ulong *puParm2);
void FUN_00408420(ulong *puParm1,ulong *puParm2);
ulong FUN_00408470(long lParm1,long lParm2);
ulong FUN_004084c0(long lParm1,long lParm2);
void FUN_004084f0(undefined8 uParm1,undefined8 uParm2,undefined8 *puParm3,long lParm4,byte bParm5,long lParm6);
void FUN_00408560(long *plParm1);
ulong FUN_004085d0(long lParm1,long lParm2);
long FUN_00408710(long *plParm1,long lParm2,uint uParm3);
undefined8 FUN_00408780(char *pcParm1,long lParm2,ulong uParm3);
ulong FUN_004088d0(long lParm1,uint uParm2,long lParm3,long lParm4,long lParm5);
ulong FUN_00408c40(long lParm1,long lParm2,long lParm3,long lParm4,long lParm5,long lParm6);
ulong FUN_00408cb0(long *plParm1);
void FUN_00408d40(long *plParm1,code *pcParm2,undefined8 uParm3);
void FUN_00408db0(long *plParm1,code *pcParm2,undefined8 uParm3);
undefined8 FUN_00408e10(long lParm1,ulong uParm2);
void FUN_00408eb0(long param_1,undefined8 param_2,long param_3,long param_4,undefined8 param_5,byte param_6,long param_7);
undefined8 FUN_00408f70(long *plParm1,undefined8 uParm2);
void FUN_004090c0(undefined4 *puParm1);
void FUN_004090d0(undefined4 *puParm1);
void FUN_004090e0(undefined4 *puParm1,undefined8 uParm2,undefined8 uParm3,ulong uParm4,undefined8 uParm5);
undefined8 FUN_004091c0(undefined4 *puParm1,undefined8 uParm2);
ulong FUN_00409220(long *plParm1,long lParm2);
undefined8 FUN_00409310(long *plParm1,long lParm2);
undefined8 FUN_00409360(long *plParm1,ulong *puParm2,ulong uParm3);
undefined8 FUN_00409450(long lParm1,undefined4 uParm2,long lParm3);
undefined8 FUN_004094f0(long lParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004095b0(long lParm1,ulong uParm2,undefined8 uParm3);
undefined8 FUN_00409640(long *plParm1,ulong uParm2);
ulong FUN_00409810(ulong uParm1,long lParm2);
void FUN_00409840(long lParm1);
void FUN_004098c0(long *plParm1);
long FUN_00409a90(long *plParm1,long lParm2,uint *puParm3);
undefined8 FUN_00409b40(long *plParm1);
undefined8 FUN_0040a580(undefined8 *puParm1,undefined8 uParm2,long lParm3,long lParm4,byte bParm5,long lParm6);
ulong FUN_0040a6e0(long *plParm1,int iParm2);
undefined8 FUN_0040a7f0(long lParm1,long lParm2);
undefined8 FUN_0040a880(long *plParm1,long lParm2);
void FUN_0040aa50(undefined4 *puParm1,undefined *puParm2);
ulong FUN_0040aa70(long lParm1,long lParm2,ulong uParm3);
ulong FUN_0040ab50(long lParm1,undefined8 *puParm2,long lParm3);
void FUN_0040ac70(long lParm1);
void FUN_0040ad60(undefined8 *puParm1);
void FUN_0040ad80(undefined8 *puParm1);
long FUN_0040add0(long *plParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040b040(long *plParm1,long lParm2,ulong uParm3);
undefined8 FUN_0040b0d0(long *plParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
ulong FUN_0040b310(undefined4 *puParm1,long *plParm2,long lParm3,char cParm4);
undefined8 FUN_0040b580(long lParm1);
undefined8 FUN_0040b680(long *plParm1);
void FUN_0040b870(long lParm1);
void FUN_0040b8d0(long lParm1);
void FUN_0040b910(long *plParm1);
undefined8 FUN_0040bab0(long lParm1,ulong uParm2,long lParm3,long lParm4,long lParm5);
void FUN_0040bc00(long lParm1);
undefined8 FUN_0040bcc0(long *plParm1);
void FUN_0040bd30(long lParm1);
undefined8 FUN_0040bf20(ulong *puParm1,ulong uParm2,ulong uParm3,int iParm4);
undefined8 FUN_0040c1d0(ulong uParm1,undefined8 uParm2,long lParm3,long *plParm4,int *piParm5,int *piParm6);
long * FUN_0040c4a0(long **pplParm1);
undefined8 FUN_0040c620(byte **ppbParm1,byte *pbParm2,uint uParm3);
undefined8 FUN_0040cdc0(void);
long FUN_0040cdd0(long lParm1,long *plParm2,long lParm3);
undefined8 FUN_0040ce30(long *plParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_0040cf30(long *plParm1,long *plParm2,long lParm3);
long FUN_0040cf70(ulong uParm1,undefined8 *puParm2,uint uParm3);
ulong FUN_0040d010(long lParm1,long lParm2,ulong uParm3);
long FUN_0040d090(long *plParm1,long lParm2,long *plParm3,long lParm4,uint uParm5);
ulong FUN_0040d0e0(long lParm1,undefined4 *puParm2,undefined8 uParm3,ulong uParm4);
long FUN_0040d1e0(long lParm1,long *plParm2,long lParm3);
undefined8 FUN_0040d250(long param_1,long *param_2,long *param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6,undefined8 param_7);
undefined8 FUN_0040d360(undefined4 *puParm1,long *plParm2,long lParm3,char *pcParm4);
long FUN_0040d450(undefined8 *puParm1,int *piParm2,long *plParm3,long *plParm4,undefined *puParm5);
undefined8 FUN_0040d530(long lParm1,long *plParm2,long *plParm3,undefined8 uParm4);
undefined8 FUN_0040d5d0(long *plParm1,long *plParm2,undefined8 *puParm3);
undefined8 FUN_0040d650(long *plParm1,long lParm2,long lParm3);
undefined8 FUN_0040d7f0(long *plParm1,ulong uParm2,undefined8 uParm3,long lParm4,undefined8 uParm5,long lParm6);
long FUN_0040d8e0(long *plParm1,long lParm2,ulong uParm3,undefined8 uParm4);
ulong * FUN_0040db40(undefined4 *puParm1,long lParm2,long lParm3,ulong uParm4);
ulong FUN_0040dc20(long *plParm1);
long FUN_0040de10(long *plParm1,long lParm2,undefined8 uParm3);
ulong * FUN_0040df90(undefined4 *puParm1,long lParm2,long lParm3);
ulong FUN_0040e050(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_0040e100(long lParm1,long lParm2,long lParm3,undefined8 uParm4,uint uParm5);
undefined8 FUN_0040e390(long *plParm1,long *plParm2,long *plParm3,long *plParm4,long *plParm5);
ulong FUN_0040e550(long lParm1,long lParm2,long lParm3);
ulong FUN_0040e640(long *plParm1,long lParm2,long lParm3,long lParm4);
void FUN_0040e830(long lParm1,long lParm2,long lParm3,long lParm4,long lParm5,long lParm6);
undefined8 FUN_0040e8d0(long lParm1,long lParm2,long *plParm3,undefined8 uParm4);
void FUN_0040e950(long lParm1);
undefined8 FUN_0040e9b0(long *param_1,long param_2,undefined8 param_3,long *param_4,long *param_5,long param_6,long param_7);
undefined8 FUN_0040ec20(long *plParm1,long *plParm2,undefined8 *puParm3,long lParm4,undefined8 uParm5,undefined4 *puParm6);
undefined8 FUN_0040ece0(undefined8 uParm1,byte *pbParm2);
undefined8 FUN_0040ed20(long param_1,undefined8 param_2,long *param_3,ulong *param_4,ulong *param_5,byte *param_6,ulong param_7);
void FUN_0040f440(long **pplParm1,long *plParm2,long *plParm3,undefined4 *puParm4);
ulong FUN_0040f910(long lParm1,ulong *puParm2,long lParm3,long lParm4,long lParm5);
long FUN_0040fc10(long param_1,undefined8 param_2,long param_3,long *param_4,long param_5,long param_6,long param_7);
ulong FUN_0040ff60(undefined8 *puParm1,long lParm2,long lParm3,long *plParm4,char cParm5);
ulong FUN_00410260(long lParm1,long lParm2,long *plParm3,long *plParm4,undefined8 uParm5);
ulong FUN_004104b0(long param_1,long *param_2,long param_3,long param_4,undefined8 param_5,long param_6,uint param_7);
undefined8 FUN_004109a0(long lParm1,long *plParm2,undefined8 *puParm3,undefined8 uParm4,long lParm5);
undefined8 FUN_00410a20(long lParm1,long lParm2,long lParm3);
ulong FUN_00410dc0(long lParm1,long *plParm2,long *plParm3);
long FUN_00411150(int *piParm1,long lParm2,long lParm3);
long FUN_00411310(int *piParm1,long lParm2);
ulong FUN_00411380(long lParm1,long *plParm2,long *plParm3);
ulong FUN_00411620(long lParm1,long *plParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_004116b0(long lParm1,long *plParm2,long lParm3,undefined8 uParm4);
ulong FUN_00411840(long lParm1,long *plParm2,long lParm3,long *plParm4,long *plParm5);
ulong FUN_00411bc0(long lParm1,long *plParm2,long lParm3,long lParm4);
ulong FUN_00411d70(long lParm1,long *plParm2);
ulong FUN_00411e80(long lParm1);
undefined8 FUN_004120c0(undefined4 *puParm1,long lParm2,undefined *puParm3,int iParm4,undefined8 uParm5,char cParm6);
void FUN_004121a0(long lParm1,undefined8 uParm2,undefined uParm3);
void FUN_004121d0(long lParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00412200(long lParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_00412230(undefined4 *puParm1,long *plParm2,long lParm3);
ulong FUN_004123b0(undefined8 uParm1,long lParm2);
long FUN_00412430(undefined8 *puParm1,int *piParm2,undefined *puParm3);
long ** FUN_004124e0(long **pplParm1,long lParm2);
long FUN_00412590(long lParm1,undefined8 uParm2,undefined8 uParm3,char *pcParm4,byte bParm5,int *piParm6);
long FUN_004127a0(long lParm1,long lParm2,long lParm3,ulong uParm4,int *piParm5);
ulong FUN_00412d40(long *plParm1);
undefined8 FUN_00412dd0(long *plParm1,long lParm2,ulong uParm3);
void FUN_00413440(undefined8 uParm1,long lParm2);
long FUN_00413460(undefined8 uParm1,byte *pbParm2,undefined8 uParm3);
long FUN_00413510(long lParm1,long lParm2,long lParm3,undefined4 *puParm4,ulong uParm5,undefined4 *puParm6);
long FUN_004138f0(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,undefined8 uParm5,int *piParm6);
long FUN_00413a00(long lParm1,long *plParm2,undefined8 *puParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_00413fc0(undefined8 uParm1,undefined8 *puParm2,long lParm3,undefined8 uParm4,long lParm5,int *piParm6);
long FUN_00414100(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_00414250(undefined8 uParm1,long *plParm2,ulong uParm3,int *piParm4);
ulong FUN_00414320(long *plParm1,undefined8 uParm2,undefined8 uParm3,ulong uParm4);
ulong FUN_004144c0(ulong *puParm1);
long FUN_004144d0(long *plParm1,long lParm2,long lParm3,ulong *puParm4);
ulong FUN_00414b20(long lParm1,long lParm2);
undefined8 FUN_00415040(int *piParm1,long lParm2,long lParm3);
long FUN_00415110(long lParm1,char cParm2,long *plParm3);
ulong FUN_004154b0(long *param_1,long param_2,long param_3,long param_4,long param_5,undefined8 param_6,ulong param_7,ulong *param_8,uint param_9);
undefined1 * FUN_00415f20(undefined8 uParm1,undefined8 uParm2,long lParm3);
undefined8 FUN_00415f80(long *plParm1);
long FUN_00416040(long param_1,undefined8 param_2,long param_3,long param_4,long param_5,undefined8 param_6,ulong *param_7,char param_8);
void FUN_004162c0(long *plParm1);
void FUN_00416310(void);
ulong FUN_00416330(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined FUN_004163f0(int iParm1);;
ulong FUN_00416400(uint uParm1);
ulong FUN_00416410(byte *pbParm1,byte *pbParm2);
undefined8 FUN_004164d0(void);
ulong FUN_004164e0(ulong uParm1);
undefined * FUN_00416580(void);
char * FUN_004168e0(void);
ulong FUN_004169a0(byte bParm1);
void FUN_004169c0(undefined8 uParm1);
void FUN_00416a50(undefined8 uParm1);
ulong FUN_00416a60(long lParm1);
undefined8 FUN_00416af0(void);
ulong FUN_00416b10(char *pcParm1,long lParm2);
char * FUN_00416b50(ulong uParm1,long lParm2,long lParm3);
ulong FUN_00416d90(void);
undefined8 * FUN_00416dd0(undefined8 param_1,undefined8 *param_2);
undefined8 * FUN_00417040(int *param_1,ulong *param_2);
undefined8 * FUN_00417230(undefined8 uParm1,undefined8 *puParm2);
undefined8 * FUN_00417440(undefined8 uParm1,int *piParm2,ulong *puParm3);
undefined4 *FUN_004175b0(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5,undefined8 *puParm6);
undefined8 *FUN_004176f0(undefined8 *puParm1,long lParm2,undefined8 *puParm3,undefined8 *puParm4,uint **ppuParm5,int **ppiParm6);
undefined8 FUN_00417ca0(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_00418250(uint param_1);
void FUN_00418290(uint uParm1);
double FUN_0041d5c0(int *piParm1);
void FUN_0041d610(int *param_1);
long FUN_0041d690(ulong uParm1,ulong uParm2);
long FUN_0041d6b0(void);
undefined8 FUN_0041d6e0(uint *puParm1,ulong *puParm2);
undefined8 FUN_0041d900(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_0041e5f0(undefined auParm1 [16]);
ulong FUN_0041e610(void);
void FUN_0041e640(void);
undefined8 _DT_FINI(void);

