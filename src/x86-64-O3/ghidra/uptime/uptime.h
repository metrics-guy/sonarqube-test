typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00402380(void);
void entry(void);
void FUN_00402680(void);
void FUN_004026f0(void);
void FUN_00402780(void);
undefined8 FUN_004027c0(undefined8 uParm1);
void FUN_004027d0(long lParm1,short *psParm2);
void FUN_00402a30(undefined8 uParm1,ulong uParm2);
void FUN_00402aa0(void);
void FUN_00402c50(uint uParm1);
ulong FUN_00402da0(int iParm1,int iParm2);
void FUN_00402de0(undefined8 uParm1,byte *pbParm2,long lParm3);
void FUN_00402e30(undefined8 uParm1,byte *pbParm2,long lParm3);
char * FUN_00402e80(long lParm1,char *pcParm2,uint *puParm3,byte bParm4,undefined8 uParm5,undefined8 uParm6,uint uParm7);
void FUN_00404890(void);
void FUN_004048b0(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,code *param_6);
int * FUN_004049c0(long lParm1);
int * FUN_00404a70(int *piParm1,undefined8 *puParm2);
ulong FUN_00404db0(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8);
ulong FUN_00404e70(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9);
ulong FUN_00404f40(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9,byte param_10);
undefined8 FUN_00405030(byte *param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined * FUN_004050e0(char *pcParm1,int iParm2);
undefined8 *FUN_004051b0(undefined *puParm1,undefined8 *puParm2,ulong uParm3,ulong uParm4,uint uParm5,uint uParm6,long lParm7,char *pcParm8,char *pcParm9);
undefined1 * FUN_00406460(undefined1 *puParm1,undefined8 *puParm2,undefined8 uParm3,uint *puParm4);
ulong FUN_00406600(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
void FUN_004066d0(uint uParm1,undefined8 uParm2,undefined8 uParm3);
undefined1 * FUN_00406770(undefined8 *puParm1);
void FUN_00406780(undefined8 uParm1,long lParm2,long lParm3,long lParm4,long lParm5);
void FUN_004069d0(void);
void FUN_00406c60(long lParm1);
void FUN_00406c80(long lParm1);
long FUN_00406c90(long lParm1,ulong *puParm2);
long FUN_00406cd0(long lParm1,ulong *puParm2,ulong uParm3);
void FUN_00406d60(void);
void FUN_00406d90(undefined8 uParm1);
ulong FUN_00406dd0(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00406e40(void);
void FUN_00406e60(void);
void FUN_00406e90(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00406f70(long lParm1,int *piParm2);
ulong FUN_004071c0(int param_1,undefined8 *param_2,char *param_3,char **param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_00407930(char *pcParm1,int *piParm2,int iParm3);
ulong FUN_004079f0(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,uint *param_7,uint param_8);
void FUN_00407ea0(void);
void FUN_00407f00(void);
void FUN_00407f20(void);
void FUN_00407f50(long lParm1);
ulong FUN_00407f70(uint *puParm1,byte *pbParm2,long lParm3);
ulong FUN_00407fe0(short *psParm1,ulong uParm2);
undefined8 FUN_00408050(undefined8 uParm1,long *plParm2,long *plParm3,ulong uParm4);
void FUN_00408170(long lParm1,long lParm2);
ulong FUN_004081b0(uint uParm1,uint uParm2);
ulong FUN_004081d0(uint *puParm1,uint *puParm2);
void FUN_00408220(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_00408230(void);
ulong FUN_00408240(char *pcParm1);
ulong FUN_00408270(long lParm1);
undefined8 * FUN_004082b0(long lParm1);
undefined8 FUN_00408350(long *plParm1,char *pcParm2);
void FUN_00408490(long *plParm1);
long FUN_004084c0(long lParm1);
ulong FUN_00408570(long lParm1);
long FUN_004085c0(long lParm1,undefined8 uParm2);
long FUN_004086a0(uint *puParm1);
void FUN_004086c0(void);
ulong FUN_00408800(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined FUN_004088c0(int iParm1);;
ulong FUN_004088d0(uint uParm1);
ulong FUN_004088e0(byte *pbParm1,byte *pbParm2);
undefined8 FUN_004089a0(void);
ulong FUN_004089b0(ulong uParm1);
undefined * FUN_00408a50(void);
char * FUN_00408db0(void);
void FUN_00408e70(undefined8 uParm1);
void FUN_00408f00(undefined8 uParm1);
ulong FUN_00408f10(long lParm1);
undefined8 FUN_00408fa0(void);
ulong FUN_00408fc0(ulong uParm1);
ulong FUN_00409040(uint uParm1,uint uParm2);
void FUN_00409060(code *pcParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
long FUN_00409080(long lParm1);
long FUN_00409090(ulong param_1,long param_2,undefined8 param_3,int param_4,int param_5,uint param_6,int param_7,int param_8,int param_9,int param_10);
long FUN_004091c0(long lParm1);
long FUN_004091d0(undefined8 uParm1,undefined8 uParm2);
long FUN_004091f0(void);
long FUN_004092a0(code *pcParm1,long *plParm2,undefined8 uParm3,undefined8 uParm4);
long FUN_004097c0(uint *puParm1);
ulong FUN_004097e0(char *pcParm1,char *pcParm2,ulong uParm3);
ulong FUN_00409910(char *pcParm1,long lParm2);
char * FUN_00409950(ulong uParm1,long lParm2,long lParm3);
ulong FUN_00409b90(void);
undefined * FUN_00409bd0(undefined8 param_1,undefined8 *param_2);
uint * FUN_00409e40(int *param_1,ulong *param_2);
undefined * FUN_0040a030(undefined8 uParm1,undefined8 *puParm2);
uint * FUN_0040a240(undefined8 uParm1,int *piParm2,ulong *puParm3);
undefined4 *FUN_0040a3b0(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5,undefined8 *puParm6);
int * FUN_0040a4f0(undefined8 *puParm1,long lParm2,undefined8 *puParm3,undefined8 *puParm4,uint **ppuParm5,int **ppiParm6);
undefined8 FUN_0040aaa0(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_0040b050(uint param_1);
void FUN_0040b090(uint uParm1);
undefined8 * FUN_004103c0(ulong uParm1);
void FUN_00410440(ulong uParm1);
double FUN_004104d0(int *piParm1);
void FUN_00410520(int *param_1);
long FUN_004105a0(ulong uParm1,ulong uParm2);
long FUN_004105c0(void);
undefined8 FUN_004105f0(uint *puParm1,ulong *puParm2);
undefined8 FUN_00410810(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_00411500(undefined auParm1 [16]);
ulong FUN_00411520(void);
void FUN_00411550(void);
undefined8 _DT_FINI(void);

