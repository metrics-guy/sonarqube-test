typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void entry(void);
void FUN_00402a00(void);
void FUN_00402a70(void);
void FUN_00402b00(void);
void FUN_00402b40(long lParm1);
void FUN_00402b80(void);
long FUN_00402c00(char *pcParm1,uint *puParm2);
long FUN_00402c50(uint uParm1);
void FUN_00402d00(char cParm1);
void FUN_00402dc0(void);
long * FUN_00402e00(void);
long * FUN_00402e20(void);
long * FUN_00402fb0(long lParm1);
void FUN_00403000(ulong uParm1);
void FUN_00403080(byte *pbParm1,ulong uParm2);
ulong FUN_00403130(char *pcParm1);
ulong FUN_00403190(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_004031d0(undefined4 uParm1,undefined uParm2,char *pcParm3);
void FUN_004032e0(undefined8 uParm1,undefined8 uParm2);
long FUN_00403310(undefined8 uParm1,long lParm2);
void FUN_00403370(void);
void FUN_00403450(void);
void FUN_00403470(int iParm1,undefined8 *puParm2,undefined *puParm3,long lParm4);
void FUN_00403540(int iParm1,uint uParm2,long lParm3);
void FUN_004036e0(undefined8 *puParm1,long *plParm2);
void FUN_00403730(long *plParm1,undefined8 uParm2);
void FUN_004037b0(undefined8 uParm1,long lParm2);
void FUN_00403820(undefined4 *puParm1);
void FUN_00403830(void);
void FUN_00403860(long **pplParm1,long **pplParm2,long lParm3,long lParm4);
long FUN_00403900(long lParm1);
void FUN_00403a10(void);
long * FUN_00403bc0(void);
long * FUN_00403be0(char *pcParm1);
long * FUN_00403cf0(void);
long * FUN_00403e50(void);
void FUN_00403f30(void);
void FUN_00403f60(long lParm1,long lParm2,char cParm3);
long FUN_00403ff0(ulong uParm1);
ulong FUN_004040d0(void);
long * FUN_004040f0(void);
long * FUN_00404140(ulong uParm1,ulong uParm2,int iParm3);
long * FUN_004041f0(long *plParm1,undefined8 uParm2);
long * FUN_00404440(long lParm1,long lParm2);
long * FUN_004044f0(void);
void FUN_004045b0(uint uParm1);
ulong FUN_00404760(uint uParm1,undefined8 uParm2,uint uParm3,uint uParm4);
long FUN_004047c0(undefined8 uParm1,undefined8 uParm2);
char * FUN_00404860(ulong uParm1,long lParm2);
int * FUN_004048b0(long lParm1);
int * FUN_00404960(int *piParm1,undefined8 *puParm2);
ulong FUN_00404ca0(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8);
ulong FUN_00404d60(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9);
ulong FUN_00404e30(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9,byte param_10);
undefined8 FUN_00404f20(byte *param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined * FUN_00404fd0(char *pcParm1,int iParm2);
undefined8 *FUN_004050a0(undefined *puParm1,undefined8 *puParm2,ulong uParm3,ulong uParm4,uint uParm5,uint uParm6,long lParm7,char *pcParm8,char *pcParm9);
undefined1 * FUN_00406350(undefined1 *puParm1,undefined8 *puParm2,undefined8 uParm3,uint *puParm4);
ulong FUN_004064f0(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
void FUN_00406560(ulong uParm1,undefined8 uParm2);
void FUN_00406600(uint uParm1,undefined8 uParm2,undefined8 uParm3);
undefined1 * FUN_004066a0(undefined8 *puParm1);
long FUN_004066b0(uint uParm1,undefined8 uParm2,ulong uParm3);
ulong FUN_00406720(ulong uParm1);
long FUN_00406730(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long *plParm5,undefined8 uParm6);
void FUN_00406980(void);
void FUN_00406c10(void);
void FUN_00406ca0(long lParm1);
void FUN_00406cc0(long lParm1);
long FUN_00406cd0(long lParm1,ulong *puParm2);
long FUN_00406d10(long lParm1,ulong *puParm2,ulong uParm3);
void FUN_00406da0(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00406ee0(long *plParm1,int iParm2);
ulong FUN_00406f30(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_00406f60(char *pcParm1,char **ppcParm2,uint uParm3,undefined8 *puParm4,long lParm5);
ulong FUN_004072b0(int iParm1);
ulong FUN_004072d0(ulong *puParm1,int iParm2);
ulong FUN_00407300(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_00407330(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_004076a0(ulong uParm1,ulong uParm2);
void FUN_004076f0(undefined8 uParm1);
ulong FUN_00407730(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_004077a0(void);
void FUN_004077c0(void);
void FUN_004077f0(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_004078d0(undefined8 uParm1);
ulong FUN_00407960(ulong param_1,undefined8 param_2,ulong param_3);
void FUN_00407ab0(undefined8 uParm1);
ulong FUN_00407ac0(long lParm1);
undefined8 FUN_00407b50(void);
void FUN_00407b70(long lParm1,int *piParm2);
ulong FUN_00407dc0(int param_1,undefined8 *param_2,char *param_3,char **param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_00408530(char *pcParm1,int *piParm2,int iParm3);
ulong FUN_004085f0(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,uint *param_7,uint param_8);
void FUN_00408aa0(void);
void FUN_00408b00(void);
void FUN_00408b20(long lParm1);
ulong FUN_00408b40(uint *puParm1,byte *pbParm2,long lParm3);
ulong * FUN_00408bb0(ulong *puParm1,undefined8 uParm2,ulong uParm3);
void FUN_00408c90(long lParm1,long lParm2);
ulong FUN_00408cd0(int iParm1);
void FUN_00408cf0(long lParm1,long lParm2);
void FUN_00408d30(ulong *puParm1,undefined4 uParm2);
ulong FUN_00408d40(long lParm1,long lParm2);
void FUN_00408d60(undefined4 *puParm1);
void FUN_00408d80(ulong *puParm1,ulong *puParm2);
void FUN_00408dd0(ulong *puParm1,ulong *puParm2);
ulong FUN_00408e20(long lParm1,long lParm2);
ulong FUN_00408e70(long lParm1,long lParm2);
void FUN_00408ea0(undefined8 uParm1,undefined8 uParm2,undefined8 *puParm3,long lParm4,byte bParm5,long lParm6);
void FUN_00408f10(long *plParm1);
ulong FUN_00408f80(long lParm1,long lParm2);
long FUN_004090c0(long *plParm1,long lParm2,uint uParm3);
undefined8 FUN_00409130(char *pcParm1,long lParm2,ulong uParm3);
ulong FUN_00409280(long lParm1,uint uParm2,long lParm3,long lParm4,long lParm5);
ulong FUN_004095f0(long lParm1,long lParm2,long lParm3,long lParm4,long lParm5,long lParm6);
ulong FUN_00409660(long *plParm1);
void FUN_004096f0(long *plParm1,code *pcParm2,undefined8 uParm3);
void FUN_00409760(long *plParm1,code *pcParm2,undefined8 uParm3);
undefined8 FUN_004097c0(long lParm1,ulong uParm2);
void FUN_00409860(long param_1,undefined8 param_2,long param_3,long param_4,undefined8 param_5,byte param_6,long param_7);
undefined8 FUN_00409920(long *plParm1,undefined8 uParm2);
void FUN_00409a70(undefined4 *puParm1);
void FUN_00409a80(undefined4 *puParm1);
void FUN_00409a90(undefined4 *puParm1,undefined8 uParm2,undefined8 uParm3,ulong uParm4,undefined8 uParm5);
undefined8 FUN_00409b70(undefined4 *puParm1,undefined8 uParm2);
ulong FUN_00409bd0(long *plParm1,long lParm2);
undefined8 FUN_00409cc0(long *plParm1,long lParm2);
undefined8 FUN_00409d10(long *plParm1,ulong *puParm2,ulong uParm3);
undefined8 FUN_00409e00(long lParm1,undefined4 uParm2,long lParm3);
undefined8 FUN_00409ea0(long lParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00409f60(long lParm1,ulong uParm2,undefined8 uParm3);
undefined8 FUN_00409ff0(long *plParm1,ulong uParm2);
ulong FUN_0040a1c0(ulong uParm1,long lParm2);
void FUN_0040a1f0(long lParm1);
void FUN_0040a270(long *plParm1);
long FUN_0040a440(long *plParm1,long lParm2,uint *puParm3);
undefined8 FUN_0040a4f0(long *plParm1);
undefined8 FUN_0040af30(undefined8 *puParm1,undefined8 uParm2,long lParm3,long lParm4,byte bParm5,long lParm6);
ulong FUN_0040b090(long *plParm1,int iParm2);
undefined8 FUN_0040b1a0(long lParm1,long lParm2);
undefined8 FUN_0040b230(long *plParm1,long lParm2);
void FUN_0040b400(undefined4 *puParm1,undefined *puParm2);
ulong FUN_0040b420(long lParm1,long lParm2,ulong uParm3);
ulong FUN_0040b500(long lParm1,undefined8 *puParm2,long lParm3);
void FUN_0040b620(long lParm1);
void FUN_0040b710(undefined8 *puParm1);
void FUN_0040b730(undefined8 *puParm1);
long FUN_0040b780(long *plParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040b9f0(long *plParm1,long lParm2,ulong uParm3);
undefined8 FUN_0040ba80(long *plParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
ulong FUN_0040bcc0(undefined4 *puParm1,long *plParm2,long lParm3,char cParm4);
undefined8 FUN_0040bf30(long lParm1);
undefined8 FUN_0040c030(long *plParm1);
void FUN_0040c220(long lParm1);
void FUN_0040c280(long lParm1);
void FUN_0040c2c0(long *plParm1);
undefined8 FUN_0040c460(long lParm1,ulong uParm2,long lParm3,long lParm4,long lParm5);
void FUN_0040c5b0(long lParm1);
undefined8 FUN_0040c670(long *plParm1);
void FUN_0040c6e0(long lParm1);
undefined8 FUN_0040c8d0(ulong *puParm1,ulong uParm2,ulong uParm3,int iParm4);
undefined8 FUN_0040cb80(ulong uParm1,undefined8 uParm2,long lParm3,long *plParm4,int *piParm5,int *piParm6);
long * FUN_0040ce50(long **pplParm1);
undefined8 FUN_0040cfd0(byte **ppbParm1,byte *pbParm2,uint uParm3);
undefined8 FUN_0040d770(void);
long FUN_0040d780(long lParm1,long *plParm2,long lParm3);
undefined8 FUN_0040d7e0(long *plParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_0040d8e0(long *plParm1,long *plParm2,long lParm3);
long FUN_0040d920(ulong uParm1,undefined8 *puParm2,uint uParm3);
ulong FUN_0040d9c0(long lParm1,long lParm2,ulong uParm3);
long FUN_0040da40(long *plParm1,long lParm2,long *plParm3,long lParm4,uint uParm5);
ulong FUN_0040da90(long lParm1,undefined4 *puParm2,undefined8 uParm3,ulong uParm4);
long FUN_0040db90(long lParm1,long *plParm2,long lParm3);
undefined8 FUN_0040dc00(long param_1,long *param_2,long *param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6,undefined8 param_7);
undefined8 FUN_0040dd10(undefined4 *puParm1,long *plParm2,long lParm3,char *pcParm4);
long FUN_0040de00(undefined8 *puParm1,int *piParm2,long *plParm3,long *plParm4,undefined *puParm5);
undefined8 FUN_0040dee0(long lParm1,long *plParm2,long *plParm3,undefined8 uParm4);
undefined8 FUN_0040df80(long *plParm1,long *plParm2,undefined8 *puParm3);
undefined8 FUN_0040e000(long *plParm1,long lParm2,long lParm3);
undefined8 FUN_0040e1a0(long *plParm1,ulong uParm2,undefined8 uParm3,long lParm4,undefined8 uParm5,long lParm6);
long FUN_0040e290(long *plParm1,long lParm2,ulong uParm3,undefined8 uParm4);
ulong * FUN_0040e4f0(undefined4 *puParm1,long lParm2,long lParm3,ulong uParm4);
ulong FUN_0040e5d0(long *plParm1);
long FUN_0040e7c0(long *plParm1,long lParm2,undefined8 uParm3);
ulong * FUN_0040e940(undefined4 *puParm1,long lParm2,long lParm3);
ulong FUN_0040ea00(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_0040eab0(long lParm1,long lParm2,long lParm3,undefined8 uParm4,uint uParm5);
undefined8 FUN_0040ed40(long *plParm1,long *plParm2,long *plParm3,long *plParm4,long *plParm5);
ulong FUN_0040ef00(long lParm1,long lParm2,long lParm3);
ulong FUN_0040eff0(long *plParm1,long lParm2,long lParm3,long lParm4);
void FUN_0040f1e0(long lParm1,long lParm2,long lParm3,long lParm4,long lParm5,long lParm6);
undefined8 FUN_0040f280(long lParm1,long lParm2,long *plParm3,undefined8 uParm4);
void FUN_0040f300(long lParm1);
undefined8 FUN_0040f360(long *param_1,long param_2,undefined8 param_3,long *param_4,long *param_5,long param_6,long param_7);
undefined8 FUN_0040f5d0(long *plParm1,long *plParm2,undefined8 *puParm3,long lParm4,undefined8 uParm5,undefined4 *puParm6);
undefined8 FUN_0040f690(undefined8 uParm1,byte *pbParm2);
undefined8 FUN_0040f6d0(long param_1,undefined8 param_2,long *param_3,ulong *param_4,ulong *param_5,byte *param_6,ulong param_7);
void FUN_0040fdf0(long **pplParm1,long *plParm2,long *plParm3,undefined4 *puParm4);
ulong FUN_004102c0(long lParm1,ulong *puParm2,long lParm3,long lParm4,long lParm5);
long FUN_004105c0(long param_1,undefined8 param_2,long param_3,long *param_4,long param_5,long param_6,long param_7);
ulong FUN_00410910(undefined8 *puParm1,long lParm2,long lParm3,long *plParm4,char cParm5);
ulong FUN_00410c10(long lParm1,long lParm2,long *plParm3,long *plParm4,undefined8 uParm5);
ulong FUN_00410e60(long param_1,long *param_2,long param_3,long param_4,undefined8 param_5,long param_6,uint param_7);
undefined8 FUN_00411350(long lParm1,long *plParm2,undefined8 *puParm3,undefined8 uParm4,long lParm5);
undefined8 FUN_004113d0(long lParm1,long lParm2,long lParm3);
ulong FUN_00411770(long lParm1,long *plParm2,long *plParm3);
long FUN_00411b00(int *piParm1,long lParm2,long lParm3);
long FUN_00411cc0(int *piParm1,long lParm2);
ulong FUN_00411d30(long lParm1,long *plParm2,long *plParm3);
ulong FUN_00411fd0(long lParm1,long *plParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_00412060(long lParm1,long *plParm2,long lParm3,undefined8 uParm4);
ulong FUN_004121f0(long lParm1,long *plParm2,long lParm3,long *plParm4,long *plParm5);
ulong FUN_00412570(long lParm1,long *plParm2,long lParm3,long lParm4);
ulong FUN_00412720(long lParm1,long *plParm2);
ulong FUN_00412830(long lParm1);
undefined8 FUN_00412a70(undefined4 *puParm1,long lParm2,undefined *puParm3,int iParm4,undefined8 uParm5,char cParm6);
void FUN_00412b50(long lParm1,undefined8 uParm2,undefined uParm3);
void FUN_00412b80(long lParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00412bb0(long lParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_00412be0(undefined4 *puParm1,long *plParm2,long lParm3);
ulong FUN_00412d60(undefined8 uParm1,long lParm2);
long FUN_00412de0(undefined8 *puParm1,int *piParm2,undefined *puParm3);
long ** FUN_00412e90(long **pplParm1,long lParm2);
long FUN_00412f40(long lParm1,undefined8 uParm2,undefined8 uParm3,char *pcParm4,byte bParm5,int *piParm6);
long FUN_00413150(long lParm1,long lParm2,long lParm3,ulong uParm4,int *piParm5);
ulong FUN_004136f0(long *plParm1);
undefined8 FUN_00413780(long *plParm1,long lParm2,ulong uParm3);
void FUN_00413df0(undefined8 uParm1,long lParm2);
long FUN_00413e10(undefined8 uParm1,byte *pbParm2,undefined8 uParm3);
long FUN_00413ec0(long lParm1,long lParm2,long lParm3,undefined4 *puParm4,ulong uParm5,undefined4 *puParm6);
long FUN_004142a0(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,undefined8 uParm5,int *piParm6);
long FUN_004143b0(long lParm1,long *plParm2,undefined8 *puParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_00414970(undefined8 uParm1,undefined8 *puParm2,long lParm3,undefined8 uParm4,long lParm5,int *piParm6);
long FUN_00414ab0(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_00414c00(undefined8 uParm1,long *plParm2,ulong uParm3,int *piParm4);
ulong FUN_00414cd0(long *plParm1,undefined8 uParm2,undefined8 uParm3,ulong uParm4);
ulong FUN_00414e70(ulong *puParm1);
long FUN_00414e80(long *plParm1,long lParm2,long lParm3,ulong *puParm4);
ulong FUN_004154d0(long lParm1,long lParm2);
undefined8 FUN_004159f0(int *piParm1,long lParm2,long lParm3);
long FUN_00415ac0(long lParm1,char cParm2,long *plParm3);
ulong FUN_00415e60(long *param_1,long param_2,long param_3,long param_4,long param_5,undefined8 param_6,ulong param_7,ulong *param_8,uint param_9);
undefined1 * FUN_004168d0(undefined8 uParm1,undefined8 uParm2,long lParm3);
undefined8 FUN_00416930(long *plParm1);
long FUN_004169f0(long param_1,undefined8 param_2,long param_3,long param_4,long param_5,undefined8 param_6,ulong *param_7,char param_8);
void FUN_00416c70(void);
ulong FUN_00416c90(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,long param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_00416da0(long lParm1);
ulong FUN_00416e70(char *pcParm1,long lParm2);
char * FUN_00416eb0(ulong uParm1,long lParm2,long lParm3);
ulong FUN_004170f0(void);
ulong FUN_00417130(undefined8 param_1,undefined8 *param_2);
uint * FUN_004173a0(int *param_1,ulong *param_2);
ulong FUN_00417590(undefined8 uParm1,undefined8 *puParm2);
uint * FUN_004177a0(undefined8 uParm1,int *piParm2,ulong *puParm3);
undefined4 *FUN_00417910(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5,undefined8 *puParm6);
int * FUN_00417a50(undefined8 *puParm1,long lParm2,undefined8 *puParm3,undefined8 *puParm4,uint **ppuParm5,int **ppiParm6);
undefined8 FUN_00418000(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_004185b0(uint param_1);
void FUN_004185f0(uint uParm1);
ulong FUN_0041d920(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined FUN_0041d9e0(int iParm1);;
ulong FUN_0041d9f0(uint uParm1);
ulong FUN_0041da00(byte *pbParm1,byte *pbParm2);
undefined8 FUN_0041dac0(void);
ulong FUN_0041dad0(ulong uParm1);
undefined * FUN_0041db70(void);
char * FUN_0041ded0(void);
double FUN_0041df90(int *piParm1);
void FUN_0041dfe0(int *param_1);
long FUN_0041e060(ulong uParm1,ulong uParm2);
long FUN_0041e080(void);
void FUN_0041e0b0(void);
undefined8 FUN_0041e0d0(long lParm1,long lParm2);
undefined8 FUN_0041e140(uint *puParm1,ulong *puParm2);
undefined8 FUN_0041e360(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_0041f050(undefined auParm1 [16]);
ulong FUN_0041f070(void);
void FUN_0041f0a0(void);
void FUN_0041f120(void);
undefined8 _DT_FINI(void);

