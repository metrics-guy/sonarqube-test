typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_004024a0(void);
void entry(void);
void FUN_00402980(void);
void FUN_004029f0(void);
void FUN_00402a80(void);
ulong FUN_00402ac0(uint uParm1);
void FUN_00402ad0(long lParm1,long lParm2);
void FUN_00402af0(void);
void FUN_00402b10(undefined auParm1 [16],undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8);
undefined8 FUN_00402b90(char cParm1,char cParm2,char cParm3);
void FUN_00402c50(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_00402e70(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00402ea0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00402ed0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00402f00(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00402f30(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00402f60(undefined8 uParm1,undefined8 uParm2);
undefined * FUN_00402f90(ulong *puParm1);
void FUN_00403b20(char cParm1);
ulong FUN_00403bf0(char *pcParm1,uint uParm2,undefined8 uParm3,code *pcParm4,undefined8 uParm5);
ulong FUN_00403ef0(char *pcParm1,undefined8 uParm2);
ulong FUN_00403fb0(char *pcParm1,undefined8 uParm2,undefined8 uParm3);
undefined1 * FUN_004040c0(undefined8 uParm1);
char * FUN_004040e0(undefined8 uParm1);
char * thunk_FUN_00406bb0(ulong uParm1,long lParm2);
undefined1 * FUN_004042f0(undefined8 uParm1,ulong uParm2);
void FUN_004043b0(undefined8 uParm1,undefined8 uParm2);
void FUN_004043c0(void);
void thunkFUN__004043c0(void);
undefined8 FUN_004043e0(void);
undefined8 FUN_004044a0(char *pcParm1,char *pcParm2,long lParm3,ulong uParm4);
void FUN_00404c40(void);
void FUN_00404df0(uint uParm1);
long FUN_00404f80(undefined8 uParm1,undefined *puParm2);
long FUN_004052c0(undefined8 uParm1,ulong uParm2);
long FUN_004053c0(undefined8 uParm1,long *plParm2,long lParm3,long lParm4);
char * FUN_00405540(char **ppcParm1,undefined8 uParm2,undefined8 uParm3);
char * FUN_00405b60(char *pcParm1);
void FUN_00405c10(char *pcParm1);
void FUN_00405c30(char *pcParm1);
undefined * FUN_00405c70(undefined8 uParm1);
char * FUN_00405ce0(char *pcParm1);
undefined8 * FUN_00405d40(long lParm1,undefined8 uParm2,undefined *puParm3);
ulong FUN_00405dc0(long lParm1,undefined8 uParm2,undefined *puParm3);
undefined * FUN_00405e00(long lParm1);
undefined8 FUN_00405e90(undefined8 uParm1);
void FUN_00406010(long lParm1,undefined *puParm2);
ulong FUN_00406020(ulong uParm1);
ulong FUN_00406090(ulong uParm1);
ulong FUN_00406100(long *plParm1,ulong uParm2);
undefined8 FUN_00406130(float **ppfParm1);
ulong FUN_004061b0(float fParm1,ulong uParm2,char cParm3);
void FUN_00406240(undefined8 *puParm1,undefined8 *puParm2);
long FUN_00406260(long lParm1,long lParm2,long **pplParm3,char cParm4);
void FUN_00406370(long *plParm1);
undefined8 FUN_00406390(long lParm1,long **pplParm2,char cParm3);
long FUN_004064d0(long lParm1,long lParm2);
long * FUN_00406530(undefined8 uParm1,undefined1 *puParm2,undefined *puParm3,undefined *puParm4,long lParm5);
void FUN_00406630(long **pplParm1);
ulong FUN_00406710(undefined8 *puParm1,undefined8 uParm2);
ulong FUN_00406880(ulong uParm1,undefined8 *puParm2,long *plParm3);
undefined8 FUN_00406ae0(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00406b20(undefined8 *puParm1,ulong uParm2);
void FUN_00406b90(undefined8 *puParm1);
char * FUN_00406bb0(ulong uParm1,long lParm2);
ulong FUN_00406c50(int iParm1,int iParm2);
long FUN_00406c90(long lParm1,long lParm2,long lParm3);
long FUN_00406cd0(long lParm1,long lParm2,long lParm3);
char * FUN_00406d10(char *pcParm1,long lParm2,char *pcParm3,uint *puParm4,byte bParm5,undefined8 uParm6,undefined8 uParm7,uint uParm8);
void FUN_00408510(void);
int * FUN_00408530(long lParm1);
void FUN_004085e0(int *piParm1,undefined8 *puParm2,undefined8 uParm3);
ulong FUN_00408920(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8);
ulong FUN_004089e0(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9);
ulong FUN_00408ab0(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9,byte param_10);
undefined8 FUN_00408ba0(byte *param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined * FUN_00408c50(char *pcParm1,int iParm2);
void FUN_00408d20(undefined *puParm1,undefined8 *puParm2,long lParm3,ulong uParm4,uint uParm5,uint uParm6,long lParm7,char *pcParm8,byte *pbParm9);
undefined1 * FUN_00409fd0(undefined1 *puParm1,undefined8 *puParm2,undefined8 uParm3,uint *puParm4);
ulong FUN_0040a170(undefined1 *puParm1);
void FUN_0040a180(undefined1 *puParm1,undefined4 uParm2);
void FUN_0040a200(ulong uParm1,undefined8 uParm2);
undefined1 * FUN_0040a2c0(undefined8 *puParm1);
ulong FUN_0040a2d0(int *piParm1);
ulong FUN_0040a320(uint *puParm1);
void FUN_0040a340(int *piParm1);
ulong FUN_0040a360(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long *plParm5,undefined8 uParm6);
void FUN_0040a5b0(void);
void FUN_0040a840(void);
void FUN_0040a8d0(long lParm1);
void FUN_0040a8f0(long lParm1);
long FUN_0040a900(long lParm1,long lParm2);
void FUN_0040a970(undefined8 uParm1);
long FUN_0040a990(void);
long FUN_0040a9c0(void);
void FUN_0040a9f0(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0040aa90(ulong uParm1,ulong uParm2);
long FUN_0040aae0(long lParm1);
void FUN_0040ab00(undefined4 *puParm1);
void FUN_0040ab10(void);
void FUN_0040ab20(int iParm1);
undefined8 FUN_0040ab60(uint *puParm1,undefined8 uParm2);
void FUN_0040ae30(undefined8 uParm1);
ulong FUN_0040ae70(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_0040aee0(void);
void FUN_0040af00(void);
void FUN_0040af30(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
long FUN_0040b010(long lParm1,ulong uParm2);
void FUN_0040b490(long lParm1,int *piParm2);
ulong FUN_0040b6e0(int param_1,undefined8 *param_2,char *param_3,char **param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_0040be50(char *pcParm1,int *piParm2,int iParm3);
ulong FUN_0040bf10(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,uint *param_7,uint param_8);
void FUN_0040c3c0(void);
void FUN_0040c420(void);
void FUN_0040c440(void);
undefined8 FUN_0040c460(long lParm1,long lParm2);
void FUN_0040c4d0(long lParm1);
ulong FUN_0040c4f0(uint *puParm1,byte *pbParm2,long lParm3);
ulong * FUN_0040c560(ulong *puParm1,undefined8 uParm2,ulong uParm3);
void FUN_0040c640(char *pcParm1);
undefined8 FUN_0040c6d0(void);
void FUN_0040c6e0(undefined8 *puParm1);
byte * FUN_0040c730(void);
void FUN_0040cee0(void);
ulong FUN_0040cef0(uint param_1,undefined8 param_2,uint param_3,ulong param_4);
undefined8 FUN_0040cff0(long lParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040d060(long lParm1,long lParm2);
void FUN_0040d0a0(void);
undefined8 FUN_0040d0c0(long lParm1,long lParm2);
ulong FUN_0040d120(long lParm1,ulong uParm2,long *plParm3);
char * FUN_0040d7d0(char *pcParm1,char *pcParm2);
ulong FUN_0040d8c0(uint uParm1,uint uParm2);
ulong FUN_0040d8e0(uint *puParm1,uint *puParm2);
void FUN_0040d930(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_0040d940(void);
ulong FUN_0040d950(char *pcParm1);
ulong FUN_0040d980(long lParm1);
undefined8 * FUN_0040d9c0(long lParm1);
undefined8 FUN_0040da60(long *plParm1,char *pcParm2);
void FUN_0040dba0(long *plParm1);
long FUN_0040dbd0(long lParm1);
ulong FUN_0040dc80(long lParm1);
undefined8 FUN_0040dcd0(long lParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_0040dd70(long lParm1,undefined8 uParm2);
long FUN_0040de50(uint *puParm1);
void FUN_0040de70(void);
ulong FUN_0040dfb0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined FUN_0040e090(int iParm1);;
ulong FUN_0040e0a0(uint uParm1);
ulong FUN_0040e0b0(byte *pbParm1,byte *pbParm2);
ulong FUN_0040e100(ulong uParm1,char cParm2);
void FUN_0040e1e0(undefined8 param_1,byte param_2,ulong param_3);
undefined8 FUN_0040e230(void);
ulong FUN_0040e240(ulong uParm1);
ulong FUN_0040e2e0(char *pcParm1,ulong uParm2);
undefined * FUN_0040e320(void);
char * FUN_0040e680(void);
ulong FUN_0040e740(uint uParm1);
undefined * FUN_0040e780(long lParm1,uint *puParm2);
undefined8 FUN_0040e8d0(char *pcParm1,undefined8 uParm2);
void FUN_0040e950(undefined8 uParm1);
ulong FUN_0040e9e0(ulong param_1,undefined8 param_2,ulong param_3);
undefined8 FUN_0040eb30(ulong uParm1);
void FUN_0040eba0(undefined8 uParm1);
ulong FUN_0040ebb0(long lParm1);
undefined8 FUN_0040ec40(void);
void FUN_0040ec60(void);
ulong FUN_0040ec80(void);
ulong FUN_0040eca0(ulong uParm1,long lParm2,long lParm3,ulong uParm4);
ulong FUN_0040ed60(long *plParm1,ulong *puParm2,long lParm3);
ulong FUN_0040ed70(ulong uParm1);
ulong FUN_0040edf0(uint uParm1,uint uParm2);
void FUN_0040ee10(code *pcParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
long FUN_0040ee30(long lParm1);
long FUN_0040ee40(ulong param_1,long param_2,undefined8 param_3,int param_4,int param_5,uint param_6,int param_7,int param_8,int param_9,int param_10);
long FUN_0040ef70(long lParm1);
long FUN_0040ef80(undefined8 uParm1,undefined8 uParm2);
long FUN_0040efa0(void);
long FUN_0040f050(code *pcParm1,long *plParm2,undefined8 uParm3,undefined8 uParm4);
long FUN_0040f570(uint *puParm1);
ulong FUN_0040f590(char *pcParm1,char *pcParm2,ulong uParm3);
ulong FUN_0040f6c0(char *pcParm1,long lParm2);
char * FUN_0040f700(ulong uParm1,long lParm2,long lParm3);
ulong FUN_0040f940(void);
void FUN_0040f980(undefined8 param_1,undefined8 *param_2);
uint * FUN_0040fbf0(int *param_1,ulong *param_2);
ulong FUN_0040fde0(undefined8 uParm1,undefined8 *puParm2);
uint * FUN_0040fff0(undefined8 uParm1,int *piParm2,ulong *puParm3);
undefined4 *FUN_00410160(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5,undefined8 *puParm6);
int * FUN_004102a0(ulong uParm1,long lParm2,ulong uParm3,undefined8 *puParm4,ulong *puParm5,int **ppiParm6);
undefined8 FUN_00410850(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_00410e00(uint param_1);
void FUN_00410e40(uint uParm1);
ulong FUN_00416170(long *plParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 * FUN_004161d0(ulong uParm1);
void FUN_00416250(ulong uParm1);
double FUN_004162e0(int *piParm1);
void FUN_00416330(int *param_1);
ulong FUN_004163b0(ulong uParm1);
long FUN_004163c0(ulong uParm1,ulong uParm2);
long FUN_004163e0(void);
ulong FUN_00416410(long *plParm1,ulong *puParm2,int iParm3,long lParm4);
undefined8 FUN_00416560(uint *puParm1,ulong *puParm2);
undefined8 FUN_00416780(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_00417470(undefined auParm1 [16]);
ulong FUN_00417490(void);
void FUN_004174c0(void);
undefined8 _DT_FINI(void);

