typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined5;
typedef unsigned long    undefined6;
typedef unsigned long    undefined7;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00403560(void);
void FUN_00403760(void);
void entry(void);
void FUN_00404230(void);
void FUN_004042a0(void);
void FUN_00404330(void);
void FUN_00404370(undefined4 *puParm1);
void FUN_00404390(undefined8 *puParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_004043b0(ulong *puParm1,ulong *puParm2);
void FUN_00404410(void);
void FUN_00404430(void);
uint FUN_00404450(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00404480(void);
undefined8 FUN_00404500(long *plParm1);
void FUN_00404590(long lParm1);
char * thunk_FUN_0040a060(ulong uParm1,long lParm2);
void FUN_004045f0(undefined8 uParm1,undefined8 uParm2,ulong uParm3,undefined8 uParm4);
void FUN_004046a0(undefined8 *puParm1,undefined8 uParm2);
void FUN_00404730(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00404740(long lParm1,long lParm2);
ulong FUN_00404d20(ulong uParm1);
undefined * FUN_00404e00(void);
void FUN_00404e10(void);
void FUN_00404fc0(void);
void FUN_00404fe0(uint uParm1);
long FUN_00405140(undefined8 uParm1,long *plParm2,long lParm3,long lParm4);
void FUN_004052c0(undefined auParm1 [16],undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined auParm8 [16],undefined8 uParm9,undefined8 uParm10,long lParm11,undefined8 uParm12,undefined8 uParm13,undefined8 uParm14);
void FUN_00405320(long *plParm1,long lParm2,long lParm3);
long FUN_00405400(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
void FUN_00405470(undefined8 uParm1);
void FUN_004054b0(undefined8 uParm1);
long FUN_004054f0(long *plParm1,int *piParm2);
long FUN_00405580(long *plParm1);
void FUN_004055a0(long *plParm1);
void FUN_00405690(long lParm1);
long FUN_004056b0(undefined8 *puParm1,long **pplParm2,long lParm3);
long FUN_00405770(long *plParm1,long lParm2);
long * FUN_004057e0(void);
void FUN_00405840(undefined8 *puParm1);
undefined * FUN_00405870(long lParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_004058d0(long lParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00405950(char *pcParm1);
undefined8 FUN_00405b90(ulong uParm1,undefined8 uParm2,long lParm3,undefined8 uParm4);
void FUN_00405c50(undefined8 *puParm1,int iParm2,uint uParm3);
void FUN_00405cd0(long lParm1,undefined8 uParm2);
undefined8 FUN_00405d00(char *pcParm1,ulong uParm2);
void FUN_00405ea0(void);
ulong FUN_00405f60(uint *puParm1,char *pcParm2,undefined8 uParm3,undefined8 uParm4,ulong uParm5);
undefined8 FUN_00405fa0(long lParm1,long lParm2,undefined8 uParm3);
ulong FUN_00406000(long **pplParm1,undefined8 uParm2);
void FUN_004060c0(long *plParm1,long lParm2,uint uParm3);
ulong FUN_004065b0(int iParm1,int iParm2);
void FUN_004065f0(undefined8 uParm1,byte *pbParm2,long lParm3);
void FUN_00406640(undefined8 uParm1,byte *pbParm2,long lParm3);
char * FUN_00406690(long lParm1,char *pcParm2,uint *puParm3,byte bParm4,undefined8 uParm5,undefined8 uParm6,uint uParm7);
void FUN_004080a0(void);
undefined8 FUN_004080c0(uint uParm1);
long FUN_00408120(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00408300(ulong uParm1);
ulong FUN_00408370(ulong uParm1);
undefined * FUN_004083e0(long *plParm1,uint uParm2);
undefined8 FUN_00408410(float **ppfParm1);
ulong FUN_00408490(float fParm1,ulong uParm2,char cParm3);
void FUN_00408520(undefined8 *puParm1,undefined8 *puParm2);
long FUN_00408540(long lParm1,long lParm2,long **pplParm3,char cParm4);
void FUN_00408650(long *plParm1);
undefined8 FUN_00408670(long lParm1,long **pplParm2,char cParm3);
long FUN_004087b0(long lParm1,long lParm2);
long * FUN_00408850(undefined8 uParm1,undefined1 *puParm2,undefined *puParm3,undefined *puParm4,long lParm5);
void FUN_00408950(long **pplParm1);
undefined * FUN_00408a30(undefined8 *puParm1,undefined8 uParm2);
undefined * FUN_00408ba0(long lParm1,undefined8 *puParm2,long *plParm3);
undefined8 FUN_00408e00(undefined8 uParm1,undefined8 uParm2);
long FUN_00408e40(long lParm1,undefined8 uParm2);
void FUN_00409020(void);
void FUN_004090f0(long lParm1,ulong uParm2,byte *pbParm3,undefined8 uParm4);
long FUN_004091b0(void);
undefined8 FUN_004091e0(char *pcParm1,undefined8 *puParm2,uint *puParm3);
char * FUN_00409330(ulong uParm1,long lParm2,ulong uParm3,ulong uParm4,ulong uParm5);
undefined8 FUN_00409f00(undefined8 uParm1,undefined8 uParm2,long *plParm3);
long * FUN_00409f50(long lParm1);
char * FUN_0040a060(ulong uParm1,long lParm2);
int * FUN_0040a460(long lParm1);
int * FUN_0040a510(int *piParm1,undefined8 *puParm2);
ulong FUN_0040a850(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8);
ulong FUN_0040a910(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9);
ulong FUN_0040a9e0(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9,byte param_10);
undefined8 FUN_0040aad0(byte *param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined * FUN_0040ab80(char *pcParm1,int iParm2);
undefined8 *FUN_0040ac50(undefined *puParm1,undefined8 *puParm2,ulong uParm3,ulong uParm4,uint uParm5,uint uParm6,long lParm7,char *pcParm8,char *pcParm9);
undefined1 * FUN_0040bf00(undefined1 *puParm1,undefined8 *puParm2,undefined8 uParm3,uint *puParm4);
ulong FUN_0040c0a0(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
void FUN_0040c0e0(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040c110(ulong uParm1,undefined8 uParm2);
void FUN_0040c1b0(uint uParm1,undefined8 uParm2,undefined8 uParm3);
undefined1 * FUN_0040c240(undefined1 *puParm1,undefined8 *puParm2);
undefined1 * FUN_0040c250(undefined8 *puParm1);
ulong FUN_0040c290(long lParm1,int iParm2,long lParm3,int iParm4);
void FUN_0040c2b0(undefined8 uParm1,long lParm2,long lParm3,long lParm4,long lParm5);
void FUN_0040c500(void);
void FUN_0040c790(void);
void FUN_0040c820(long lParm1);
void FUN_0040c840(long lParm1);
long FUN_0040c850(long lParm1,ulong *puParm2);
long FUN_0040c890(long lParm1,ulong *puParm2,ulong uParm3);
long FUN_0040c8c0(long lParm1,ulong *puParm2,ulong uParm3);
void FUN_0040c960(undefined8 uParm1);
void FUN_0040c980(ulong uParm1,ulong uParm2);
void FUN_0040c9e0(undefined8 uParm1);
void FUN_0040ca00(void);
void FUN_0040ca30(undefined8 uParm1,uint uParm2);
ulong FUN_0040ca80(long lParm1,long lParm2);
undefined8 FUN_0040cab0(long *plParm1,int iParm2);
ulong FUN_0040cb00(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_0040cb30(char *pcParm1,char **ppcParm2,uint uParm3,undefined8 *puParm4,long lParm5);
ulong FUN_0040ce80(int iParm1);
ulong FUN_0040cea0(ulong *puParm1,int iParm2);
ulong FUN_0040ced0(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_0040cf00(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined *FUN_0040d270(int iParm1,undefined8 *puParm2,undefined uParm3,long lParm4,undefined8 uParm5,ulong uParm6);
ulong FUN_0040d2e0(int iParm1,undefined8 uParm2,char cParm3,undefined8 uParm4,undefined8 uParm5);
ulong FUN_0040d300(int iParm1);
ulong FUN_0040d320(ulong *puParm1,int iParm2);
ulong FUN_0040d350(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_0040d380(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_0040d6f0(ulong uParm1,ulong uParm2);
void FUN_0040d740(undefined8 uParm1);
ulong FUN_0040d780(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_0040d7f0(void);
void FUN_0040d810(void);
void FUN_0040d840(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_0040d920(undefined8 uParm1);
void FUN_0040d9b0(undefined8 uParm1);
ulong FUN_0040d9c0(long lParm1);
int * FUN_0040da50(int *piParm1);
char * FUN_0040db50(char *pcParm1);
undefined8 FUN_0040dc50(int *piParm1);
undefined8 FUN_0040e220(uint uParm1,long lParm2,ulong uParm3,ulong uParm4,ulong uParm5,uint uParm6);
ulong FUN_0040e800(uint *puParm1,uint *puParm2,uint *puParm3,bool bParm4,uint uParm5);
undefined8 FUN_0040f800(int iParm1,long lParm2,ulong uParm3,ulong uParm4,ulong uParm5,uint uParm6);
ulong FUN_0040fd40(byte *pbParm1,byte *pbParm2,byte *pbParm3,bool bParm4,uint uParm5);
ulong FUN_00410d90(undefined8 uParm1,long lParm2,ulong uParm3);
undefined8 FUN_00411030(void);
long FUN_004110d0(long *plParm1);
undefined * FUN_00411120(long lParm1,undefined8 *puParm2);
ulong FUN_00411150(long lParm1,long lParm2,char cParm3);
long FUN_004112f0(long lParm1,long lParm2,ulong uParm3);
long FUN_004113f0(long lParm1,undefined8 uParm2,long lParm3);
void FUN_00411490(long lParm1);
void FUN_004114f0(undefined8 uParm1);
long FUN_00411530(undefined8 uParm1,undefined8 uParm2,uint uParm3,uint *puParm4);
undefined * FUN_00411590(long lParm1);
ulong FUN_004116a0(void);
ulong FUN_004116d0(void);
undefined * FUN_00411740(long lParm1,ulong uParm2,char cParm3);
ulong FUN_004117b0(long lParm1);
void FUN_00411820(undefined4 *puParm1,int iParm2);
void FUN_00411840(long lParm1,long lParm2,long lParm3);
undefined8 FUN_004118e0(long *plParm1,ulong *puParm2,long lParm3);
ulong FUN_00411950(ulong uParm1,ulong *puParm2);
void FUN_004119b0(uint param_1,uint param_2,undefined8 param_3,ulong param_4);
ulong FUN_00411a00(long lParm1,long lParm2,uint uParm3,char *pcParm4);
void FUN_00411be0(ulong uParm1,long **pplParm2);
void FUN_00411c10(undefined8 *puParm1,long lParm2);
undefined8 FUN_00411ca0(ulong uParm1,undefined8 *puParm2,undefined8 *puParm3);
long ** FUN_00411d40(ulong uParm1,long **pplParm2,long lParm3);
long FUN_00411dd0(long *plParm1,int iParm2);
void FUN_00412670(long lParm1);
long * FUN_00412680(long *plParm1,uint uParm2,long lParm3);
ulong FUN_00412a10(long *plParm1);
undefined8 * FUN_00412bb0(undefined8 *puParm1,undefined8 *puParm2);
undefined8 FUN_00413290(undefined8 uParm1,long lParm2,uint uParm3);
ulong FUN_004132c0(long *plParm1,ulong *puParm2,int iParm3,long lParm4);
void FUN_00413410(long lParm1,int *piParm2);
ulong FUN_00413660(int param_1,undefined8 *param_2,char *param_3,char **param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_00413dd0(char *pcParm1,int *piParm2,int iParm3);
ulong FUN_00413e90(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,uint *param_7,uint param_8);
void FUN_00414340(void);
void FUN_004143a0(void);
void FUN_004143c0(void);
undefined8 FUN_004143e0(long lParm1,long lParm2);
void FUN_00414450(long lParm1);
ulong FUN_00414470(uint *puParm1,byte *pbParm2,long lParm3);
ulong * FUN_004144e0(ulong *puParm1,undefined8 uParm2,ulong uParm3);
void FUN_004145c0(char *pcParm1);
undefined8 FUN_00414650(void);
void FUN_00414660(undefined8 *puParm1);
byte * FUN_004146b0(void);
void FUN_00414e60(long lParm1,long lParm2);
ulong FUN_00414ea0(int iParm1);
void FUN_00414ec0(long lParm1,long lParm2);
void FUN_00414f00(ulong *puParm1,undefined4 uParm2);
ulong FUN_00414f10(long lParm1,long lParm2);
void FUN_00414f30(undefined4 *puParm1);
void FUN_00414f50(ulong *puParm1,ulong *puParm2);
void FUN_00414fa0(ulong *puParm1,ulong *puParm2);
ulong FUN_00414ff0(long lParm1,long lParm2);
ulong FUN_00415040(long lParm1,long lParm2);
void FUN_00415070(undefined8 uParm1,undefined8 uParm2,undefined8 *puParm3,long lParm4,byte bParm5,long lParm6);
void FUN_004150e0(long *plParm1);
ulong FUN_00415150(long lParm1,long lParm2);
long FUN_00415290(long *plParm1,long lParm2,uint uParm3);
undefined8 FUN_00415300(char *pcParm1,long lParm2,ulong uParm3);
ulong FUN_00415450(long lParm1,uint uParm2,long lParm3,long lParm4,long lParm5);
ulong FUN_004157c0(long lParm1,long lParm2,long lParm3,long lParm4,long lParm5,long lParm6);
ulong FUN_00415830(long *plParm1);
void FUN_004158c0(long *plParm1,code *pcParm2,undefined8 uParm3);
void FUN_00415930(long *plParm1,code *pcParm2,undefined8 uParm3);
undefined8 FUN_00415990(long lParm1,ulong uParm2);
void FUN_00415a30(long param_1,undefined8 param_2,long param_3,long param_4,undefined8 param_5,byte param_6,long param_7);
undefined8 FUN_00415af0(long *plParm1,undefined8 uParm2);
void FUN_00415c40(undefined4 *puParm1);
void FUN_00415c50(undefined4 *puParm1);
void FUN_00415c60(undefined4 *puParm1,undefined8 uParm2,undefined8 uParm3,ulong uParm4,undefined8 uParm5);
undefined8 FUN_00415d40(undefined4 *puParm1,undefined8 uParm2);
ulong FUN_00415da0(long *plParm1,long lParm2);
undefined8 FUN_00415e90(long *plParm1,long lParm2);
undefined8 FUN_00415ee0(long *plParm1,ulong *puParm2,ulong uParm3);
undefined8 FUN_00415fd0(long lParm1,undefined4 uParm2,long lParm3);
undefined8 FUN_00416070(long lParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00416130(long lParm1,ulong uParm2,undefined8 uParm3);
undefined8 FUN_004161c0(long *plParm1,ulong uParm2);
ulong FUN_00416390(ulong uParm1,long lParm2);
void FUN_004163c0(long lParm1);
void FUN_00416440(long *plParm1);
long FUN_00416610(long *plParm1,long lParm2,uint *puParm3);
undefined8 FUN_004166c0(long *plParm1);
undefined8 FUN_00417100(undefined8 *puParm1,undefined8 uParm2,long lParm3,long lParm4,byte bParm5,long lParm6);
ulong FUN_00417260(long *plParm1,int iParm2);
undefined8 FUN_00417370(long lParm1,long lParm2);
undefined8 FUN_00417400(long *plParm1,long lParm2);
void FUN_004175d0(undefined4 *puParm1,undefined *puParm2);
ulong FUN_004175f0(long lParm1,long lParm2,ulong uParm3);
ulong FUN_004176d0(long lParm1,undefined8 *puParm2,long lParm3);
void FUN_004177f0(long lParm1);
void FUN_004178e0(undefined8 *puParm1);
void FUN_00417900(undefined8 *puParm1);
long FUN_00417950(long *plParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00417bc0(long *plParm1,long lParm2,ulong uParm3);
undefined8 FUN_00417c50(long *plParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
ulong FUN_00417e90(undefined4 *puParm1,long *plParm2,long lParm3,char cParm4);
undefined8 FUN_00418100(long lParm1);
undefined8 FUN_00418200(long *plParm1);
void FUN_004183f0(long lParm1);
void FUN_00418450(long lParm1);
void FUN_00418490(long *plParm1);
undefined8 FUN_00418630(long lParm1,ulong uParm2,long lParm3,long lParm4,long lParm5);
void FUN_00418780(long lParm1);
undefined8 FUN_00418840(long *plParm1);
void FUN_004188b0(long lParm1);
undefined8 FUN_00418aa0(ulong uParm1,undefined8 uParm2,long lParm3,long *plParm4,int *piParm5,int *piParm6);
long * FUN_00418d70(long **pplParm1);
undefined8 FUN_00418ef0(byte **ppbParm1,byte *pbParm2,uint uParm3);
undefined8 FUN_00419690(void);
long FUN_004196a0(long lParm1,long *plParm2,long lParm3);
undefined8 FUN_00419700(long *plParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_00419800(long *plParm1,long *plParm2,long lParm3);
long FUN_00419840(ulong uParm1,undefined8 *puParm2,uint uParm3);
ulong FUN_004198e0(long lParm1,long lParm2,ulong uParm3);
long FUN_00419960(long *plParm1,long lParm2,long *plParm3,long lParm4,uint uParm5);
ulong FUN_004199b0(long lParm1,undefined4 *puParm2,undefined8 uParm3,ulong uParm4);
long FUN_00419ab0(long lParm1,long *plParm2,long lParm3);
undefined8 FUN_00419b20(long param_1,long *param_2,long *param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6,undefined8 param_7);
undefined8 FUN_00419c30(undefined4 *puParm1,long *plParm2,long lParm3,char *pcParm4);
long FUN_00419d20(undefined8 *puParm1,int *piParm2,long *plParm3,long *plParm4,undefined *puParm5);
undefined8 FUN_00419e00(long lParm1,long *plParm2,long *plParm3,undefined8 uParm4);
undefined8 FUN_00419ea0(long *plParm1,long *plParm2,undefined8 *puParm3);
undefined8 FUN_00419f20(long *plParm1,long lParm2,long lParm3);
undefined8 FUN_0041a0c0(long *plParm1,ulong uParm2,undefined8 uParm3,long lParm4,undefined8 uParm5,long lParm6);
long FUN_0041a1b0(long *plParm1,long lParm2,ulong uParm3,undefined8 uParm4);
ulong * FUN_0041a410(undefined4 *puParm1,long lParm2,long lParm3,ulong uParm4);
ulong FUN_0041a4f0(long *plParm1);
long FUN_0041a6e0(long *plParm1,long lParm2,undefined8 uParm3);
ulong * FUN_0041a860(undefined4 *puParm1,long lParm2,long lParm3);
ulong FUN_0041a920(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_0041a9d0(long lParm1,long lParm2,long lParm3,undefined8 uParm4,uint uParm5);
undefined8 FUN_0041ac60(long *plParm1,long *plParm2,long *plParm3,long *plParm4,long *plParm5);
ulong FUN_0041ae20(long lParm1,long lParm2,long lParm3);
ulong FUN_0041af10(long *plParm1,long lParm2,long lParm3,long lParm4);
void FUN_0041b100(long lParm1,long lParm2,long lParm3,long lParm4,long lParm5,long lParm6);
undefined8 FUN_0041b1a0(long lParm1,long lParm2,long *plParm3,undefined8 uParm4);
void FUN_0041b220(long lParm1);
undefined8 FUN_0041b280(long *param_1,long param_2,undefined8 param_3,long *param_4,long *param_5,long param_6,long param_7);
undefined8 FUN_0041b4f0(long *plParm1,long *plParm2,undefined8 *puParm3,long lParm4,undefined8 uParm5,undefined4 *puParm6);
undefined8 FUN_0041b5b0(undefined8 uParm1,byte *pbParm2);
undefined8 FUN_0041b5f0(long param_1,undefined8 param_2,long *param_3,ulong *param_4,ulong *param_5,byte *param_6,ulong param_7);
void FUN_0041bd10(long **pplParm1,long *plParm2,long *plParm3,undefined4 *puParm4);
ulong FUN_0041c1e0(long lParm1,ulong *puParm2,long lParm3,long lParm4,long lParm5);
long FUN_0041c4e0(long param_1,undefined8 param_2,long param_3,long *param_4,long param_5,long param_6,long param_7);
ulong FUN_0041c830(undefined8 *puParm1,long lParm2,long lParm3,long *plParm4,char cParm5);
ulong FUN_0041cb30(long lParm1,long lParm2,long *plParm3,long *plParm4,undefined8 uParm5);
ulong FUN_0041cd80(long param_1,long *param_2,long param_3,long param_4,undefined8 param_5,long param_6,uint param_7);
undefined8 FUN_0041d270(long lParm1,long *plParm2,undefined8 *puParm3,undefined8 uParm4,long lParm5);
undefined8 FUN_0041d2f0(long lParm1,long lParm2,long lParm3);
ulong FUN_0041d690(long lParm1,long *plParm2,long *plParm3);
long FUN_0041da20(int *piParm1,long lParm2,long lParm3);
long FUN_0041dbe0(int *piParm1,long lParm2);
ulong FUN_0041dc50(long lParm1,long *plParm2,long *plParm3);
ulong FUN_0041def0(long lParm1,long *plParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_0041df80(long lParm1,long *plParm2,long lParm3,undefined8 uParm4);
ulong FUN_0041e110(long lParm1,long *plParm2,long lParm3,long *plParm4,long *plParm5);
ulong FUN_0041e490(long lParm1,long *plParm2,long lParm3,long lParm4);
ulong FUN_0041e640(long lParm1,long *plParm2);
ulong FUN_0041e750(long lParm1);
undefined8 FUN_0041e990(undefined4 *puParm1,long lParm2,undefined *puParm3,int iParm4,undefined8 uParm5,char cParm6);
void FUN_0041ea70(long lParm1,undefined8 uParm2,undefined uParm3);
void FUN_0041eaa0(long lParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0041ead0(long lParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_0041eb00(undefined4 *puParm1,long *plParm2,long lParm3);
ulong FUN_0041ec80(undefined8 uParm1,long lParm2);
long FUN_0041ed00(undefined8 *puParm1,int *piParm2,undefined *puParm3);
long ** FUN_0041edb0(long **pplParm1,long lParm2);
long FUN_0041ee60(long lParm1,undefined8 uParm2,undefined8 uParm3,char *pcParm4,byte bParm5,int *piParm6);
long FUN_0041f070(long lParm1,long lParm2,long lParm3,ulong uParm4,int *piParm5);
ulong FUN_0041f610(long *plParm1);
undefined8 FUN_0041f6a0(long *plParm1,long lParm2,ulong uParm3);
void FUN_0041fd10(undefined8 uParm1,long lParm2);
long FUN_0041fd30(undefined8 uParm1,byte *pbParm2,undefined8 uParm3);
long FUN_0041fde0(long lParm1,long lParm2,long lParm3,undefined4 *puParm4,ulong uParm5,undefined4 *puParm6);
long FUN_004201c0(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,undefined8 uParm5,int *piParm6);
long FUN_004202d0(long lParm1,long *plParm2,undefined8 *puParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_00420890(undefined8 uParm1,undefined8 *puParm2,long lParm3,undefined8 uParm4,long lParm5,int *piParm6);
long FUN_004209d0(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_00420b20(undefined8 uParm1,long *plParm2,ulong uParm3,int *piParm4);
ulong FUN_00420bf0(long *plParm1,undefined8 uParm2,undefined8 uParm3,ulong uParm4);
ulong FUN_00420d90(ulong *puParm1);
long FUN_00420da0(long *plParm1,long lParm2,long lParm3,ulong *puParm4);
ulong FUN_004213f0(long lParm1,long lParm2);
undefined8 FUN_00421910(int *piParm1,long lParm2,long lParm3);
long FUN_004219e0(long lParm1,char cParm2,long *plParm3);
ulong FUN_00421d80(long *param_1,long param_2,long param_3,long param_4,long param_5,undefined8 param_6,ulong param_7,ulong *param_8,uint param_9);
undefined8 FUN_004227f0(long *plParm1);
ulong FUN_004228b0(undefined8 *puParm1,undefined8 uParm2,ulong uParm3);
ulong FUN_004229d0(long lParm1,undefined8 uParm2,undefined8 uParm3,undefined8 *puParm4,ulong uParm5);
void FUN_00422a70(void);
undefined8 FUN_00422a90(long lParm1,long lParm2);
ulong FUN_00422af0(long lParm1,ulong uParm2,long *plParm3);
char * FUN_004231a0(char *pcParm1,char *pcParm2);
ulong FUN_00423290(uint uParm1,uint uParm2);
ulong FUN_004232b0(uint *puParm1,uint *puParm2);
void FUN_00423300(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_00423310(void);
ulong FUN_00423320(char *pcParm1);
ulong FUN_00423350(long lParm1);
undefined8 * FUN_00423390(long lParm1);
undefined8 FUN_00423430(long *plParm1,char *pcParm2);
void FUN_00423570(long *plParm1);
long FUN_004235a0(long lParm1);
ulong FUN_00423650(long lParm1);
undefined8 FUN_004236a0(long lParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_00423740(long lParm1,undefined8 uParm2);
long FUN_00423820(uint *puParm1);
void FUN_00423840(void);
ulong FUN_00423980(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined FUN_00423a60(int iParm1);;
ulong FUN_00423a70(uint uParm1);
ulong FUN_00423a80(byte *pbParm1,byte *pbParm2);
ulong FUN_00423b40(ulong uParm1);
void FUN_00423b50(long lParm1);
undefined8 FUN_00423b60(long *plParm1,long *plParm2);
undefined8 FUN_00423c60(void);
ulong FUN_00423c70(ulong uParm1);
void FUN_00423d10(undefined4 *puParm1,undefined4 uParm2);
ulong FUN_00423d30(long lParm1);
ulong FUN_00423d40(long lParm1,uint uParm2);
undefined * FUN_00423d80(long lParm1,undefined8 *puParm2);
undefined * FUN_00423dd0(void);
char * FUN_00424130(void);
void FUN_00424210(void);
ulong FUN_00424260(uint uParm1);
ulong FUN_004242a0(ulong param_1,undefined8 param_2,ulong param_3);
undefined8 FUN_004243f0(ulong uParm1);
void FUN_00424460(void);
ulong FUN_00424480(void);
ulong FUN_004244a0(ulong uParm1,long lParm2,long lParm3,ulong uParm4);
ulong FUN_00424560(long *plParm1,ulong *puParm2,long lParm3);
ulong FUN_00424570(ulong uParm1);
ulong FUN_004245f0(uint uParm1,uint uParm2);
void FUN_00424610(code *pcParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
long FUN_00424630(long lParm1);
long FUN_00424640(ulong param_1,long param_2,undefined8 param_3,int param_4,int param_5,uint param_6,int param_7,int param_8,int param_9,int param_10);
long FUN_00424770(long lParm1);
long FUN_00424780(undefined8 uParm1,undefined8 uParm2);
long FUN_004247a0(void);
long FUN_00424850(code *pcParm1,long *plParm2,undefined8 uParm3,undefined8 uParm4);
long FUN_00424d70(uint *puParm1);
void FUN_00424d90(void);
ulong FUN_00424da0(uint param_1,undefined8 param_2,uint param_3,ulong param_4);
ulong FUN_00424ea0(char *pcParm1,char *pcParm2,ulong uParm3);
ulong FUN_00424fd0(char *pcParm1,long lParm2);
char * FUN_00425010(ulong uParm1,long lParm2,long lParm3);
ulong FUN_00425250(void);
undefined * FUN_00425290(undefined8 param_1,undefined8 *param_2);
uint * FUN_00425500(int *param_1,ulong *param_2);
undefined * FUN_004256f0(undefined8 uParm1,undefined8 *puParm2);
uint * FUN_00425900(undefined8 uParm1,int *piParm2,ulong *puParm3);
undefined4 *FUN_00425a70(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5,undefined8 *puParm6);
int * FUN_00425bb0(undefined8 *puParm1,long lParm2,undefined8 *puParm3,undefined8 *puParm4,uint **ppuParm5,int **ppiParm6);
undefined8 FUN_00426160(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_00426710(uint param_1);
void FUN_00426750(uint uParm1);
ulong FUN_0042ba80(ulong uParm1,char cParm2);
undefined8 * FUN_0042baf0(ulong uParm1);
void FUN_0042bb70(ulong uParm1);
double FUN_0042bc00(int *piParm1);
void FUN_0042bc50(int *param_1);
ulong FUN_0042bcd0(ulong uParm1);
long FUN_0042bce0(ulong uParm1,ulong uParm2);
long FUN_0042bd00(void);
undefined8 FUN_0042bd30(uint *puParm1,ulong *puParm2);
undefined8 FUN_0042bf50(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_0042cc40(undefined auParm1 [16]);
ulong FUN_0042cc60(void);
void FUN_0042cc90(void);
undefined8 _DT_FINI(void);

