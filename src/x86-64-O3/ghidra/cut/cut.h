typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void entry(void);
void FUN_00402900(void);
void FUN_00402970(void);
void FUN_00402a00(void);
ulong FUN_00402a40(uint uParm1);
void FUN_00402a50(ulong *puParm1);
ulong FUN_00402a80(ulong uParm1);
ulong FUN_00402a90(long lParm1);
void FUN_00402aa0(void);
void FUN_00402ac0(void);
ulong FUN_00402bf0(void);
void FUN_00402c00(undefined8 uParm1);
void FUN_00403050(undefined8 uParm1);
undefined8 FUN_00403070(char *pcParm1);
void FUN_00403150(void);
void FUN_00403300(uint uParm1);
ulong FUN_00403440(uint uParm1);
void FUN_00403450(ulong uParm1,undefined8 uParm2);
void FUN_004034f0(void);
void FUN_004035b0(byte *pbParm1,ulong uParm2);
void FUN_00403a80(long lParm1,ulong uParm2);
undefined *FUN_00403ab0(undefined8 *param_1,undefined8 *param_2,undefined *param_3,undefined *param_4,uint param_5,uint param_6,undefined8 param_7);
ulong * FUN_00403dd0(ulong *puParm1,ulong uParm2,undefined8 uParm3,ulong *puParm4);
int * FUN_00403f20(long lParm1);
void FUN_00403fd0(int *piParm1,undefined8 *puParm2,undefined8 uParm3);
ulong FUN_00404310(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8);
ulong FUN_004043d0(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9);
ulong FUN_004044a0(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9,byte param_10);
undefined8 FUN_00404590(byte *param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined * FUN_00404640(char *pcParm1,int iParm2);
void FUN_00404710(undefined *puParm1,undefined8 *puParm2,ulong uParm3,ulong uParm4,uint uParm5,uint uParm6,long lParm7,char *pcParm8,byte *pbParm9);
undefined1 * FUN_004059c0(undefined1 *puParm1,undefined8 *puParm2,char *pcParm3,uint *puParm4);
ulong FUN_00405b60(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
void FUN_00405c30(uint uParm1,undefined8 uParm2,undefined8 uParm3);
undefined1 * FUN_00405cd0(undefined8 *puParm1);
ulong FUN_00405ce0(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long *plParm5,undefined8 uParm6);
void FUN_00405f30(void);
void FUN_004061c0(void);
void FUN_00406250(long lParm1);
void FUN_00406270(long lParm1);
long FUN_00406280(long lParm1,ulong *puParm2);
long FUN_004062c0(long lParm1,ulong *puParm2,ulong uParm3);
void FUN_00406380(undefined8 uParm1);
void FUN_004063a0(void);
void FUN_004063d0(void);
void FUN_004063f0(undefined8 uParm1);
ulong FUN_00406430(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_004064a0(void);
void FUN_004064c0(void);
void FUN_004064f0(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_004065d0(undefined8 uParm1);
void FUN_00406660(undefined8 uParm1);
ulong FUN_00406670(long lParm1);
undefined8 FUN_00406700(void);
void FUN_00406710(void);
void FUN_00406720(long lParm1,int *piParm2);
ulong FUN_00406970(int param_1,undefined8 *param_2,char *param_3,char **param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_004070e0(char *pcParm1,int *piParm2,int iParm3);
ulong FUN_004071a0(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,uint *param_7,uint param_8);
void FUN_00407650(void);
void FUN_004076b0(void);
void FUN_004076d0(long lParm1);
ulong FUN_004076f0(uint *puParm1,byte *pbParm2,long lParm3);
void FUN_00407840(long lParm1,long lParm2);
ulong FUN_00407880(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined FUN_00407940(int iParm1);;
ulong FUN_00407950(uint uParm1);
ulong FUN_00407960(byte *pbParm1,byte *pbParm2);
void FUN_00407a20(void);
ulong FUN_00407a30(undefined8 uParm1,ulong uParm2);
undefined8 FUN_00407b80(void);
ulong FUN_00407b90(ulong uParm1);
undefined * FUN_00407c30(void);
char * FUN_00407f90(void);
ulong FUN_00408050(char *pcParm1,long lParm2);
char * FUN_00408090(ulong uParm1,long lParm2,long lParm3);
ulong FUN_004082d0(void);
void FUN_00408310(undefined8 param_1,undefined8 *param_2);
uint * FUN_00408580(int *param_1,ulong *param_2);
ulong FUN_00408770(undefined8 uParm1,undefined8 *puParm2);
uint * FUN_00408980(undefined8 uParm1,int *piParm2,ulong *puParm3);
undefined4 *FUN_00408af0(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5,undefined8 *puParm6);
int * FUN_00408c30(undefined8 *puParm1,long lParm2,undefined8 *puParm3,undefined8 *puParm4,uint **ppuParm5,int **ppiParm6);
undefined8 FUN_004091e0(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_00409790(uint param_1);
void FUN_004097d0(uint uParm1);
double FUN_0040eb00(int *piParm1);
void FUN_0040eb50(int *param_1);
long FUN_0040ebd0(ulong uParm1,ulong uParm2);
long FUN_0040ebf0(void);
undefined8 FUN_0040ec20(uint *puParm1,ulong *puParm2);
undefined8 FUN_0040ee40(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_0040fb30(undefined auParm1 [16]);
ulong FUN_0040fb50(void);
void FUN_0040fb80(void);
undefined8 _DT_FINI(void);

