typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined3;
typedef unsigned int    undefined4;
typedef unsigned long    undefined5;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00403850(void);
undefined * FUN_004038e0(ulong uParm1,undefined8 *puParm2);
void entry(void);
void FUN_00404ab0(void);
void FUN_00404b20(void);
void FUN_00404bb0(void);
ulong FUN_00404bf0(uint uParm1);
undefined8 FUN_00404c00(undefined8 uParm1);
void FUN_00404c30(undefined8 uParm1);
ulong FUN_00404c90(byte **ppbParm1);
ulong FUN_00404d70(char *pcParm1);
ulong FUN_00404dc0(char *pcParm1,char **ppcParm2);
undefined8 FUN_00404ea0(long lParm1);
ulong FUN_00404ec0(long lParm1);
void FUN_00404f10(long lParm1,undefined *puParm2);
void FUN_00404ff0(char *pcParm1,long lParm2,uint uParm3);
void FUN_00405340(void);
void FUN_00405360(void);
void FUN_00405390(void);
void FUN_004053b0(void);
void FUN_00405420(undefined8 uParm1,undefined8 uParm2);
void FUN_00405450(long lParm1);
void FUN_00405470(char *pcParm1);
ulong FUN_00405520(byte bParm1);
void FUN_00405540(void);
undefined4 * FUN_004056b0(undefined4 *puParm1);
ulong FUN_004056e0(undefined8 uParm1,undefined8 *puParm2,long lParm3);
void FUN_00405780(uint uParm1,ulong uParm2,undefined8 uParm3);
void FUN_004058c0(uint uParm1,char cParm2,undefined8 uParm3);
long FUN_00405a50(ulong uParm1,char cParm2,undefined8 uParm3);
long FUN_00405ae0(char *pcParm1,char *pcParm2);
void FUN_00405be0(undefined8 uParm1,undefined *puParm2);
void FUN_00405c30(undefined8 uParm1);
void FUN_00405c50(undefined8 uParm1,undefined8 uParm2);
void FUN_00405cd0(undefined8 uParm1);
void FUN_00405d10(undefined *puParm1,char cParm2);
long FUN_00406270(char *pcParm1);
long FUN_004062f0(long *plParm1,char *pcParm2,char *pcParm3);
long FUN_00406360(char *pcParm1,char *pcParm2);
void FUN_004065f0(long lParm1,long lParm2);
void FUN_00406660(undefined8 uParm1,undefined8 uParm2);
ulong FUN_004066a0(long lParm1,long lParm2);
ulong FUN_00406780(byte *pbParm1,byte *pbParm2);
ulong FUN_004067e0(byte *pbParm1,byte *pbParm2);
ulong FUN_00406870(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00406900(undefined *puParm1,ulong uParm2,undefined *puParm3,ulong uParm4);
void FUN_00406e80(char **ppcParm1,long lParm2);
void FUN_00406ee0(uint uParm1,uint uParm2);
ulong FUN_00406f00(long lParm1);
undefined8 FUN_00406f40(undefined4 uParm1);
ulong FUN_00406f80(ulong uParm1);
ulong FUN_00407040(ulong uParm1);
void FUN_00407060(long lParm1);
void FUN_00407140(void);
void FUN_00407180(ulong uParm1,uint *puParm2);
undefined8 * FUN_00407240(long lParm1);
undefined8 * FUN_004072a0(uint *puParm1,char cParm2);
ulong FUN_00407400(void);
undefined4 *FUN_00407560(undefined4 *puParm1,undefined4 *puParm2,long lParm3,ulong uParm4,long lParm5,char cParm6);
undefined4 * FUN_004076a0(long lParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00407740(undefined8 *puParm1,long lParm2);
void FUN_00407770(long lParm1);
void FUN_00407780(undefined8 *puParm1,long lParm2);
void FUN_004077c0(undefined8 *puParm1,long *plParm2);
void FUN_00407800(long lParm1);
void FUN_00407810(undefined8 *puParm1,long lParm2);
long FUN_00407870(undefined8 *puParm1);
long FUN_004078d0(long lParm1,long lParm2);
char * FUN_004078e0(char *pcParm1,long lParm2,long *plParm3);
char * FUN_00407a20(char *pcParm1,long lParm2,long lParm3);
ulong FUN_00407ba0(char **ppcParm1,char **ppcParm2);
ulong FUN_00408750(undefined8 *puParm1,undefined8 *puParm2);
void FUN_00408910(undefined4 *puParm1,ulong uParm2,undefined4 *puParm3,char cParm4);
undefined8 FUN_00408aa0(char **ppcParm1,undefined8 uParm2,long lParm3);
void FUN_00408de0(byte *pbParm1,long lParm2,long *plParm3);
undefined8 FUN_00408f70(byte **ppbParm1);
char * FUN_00408fd0(char **ppcParm1,long lParm2,long lParm3);
char * FUN_004090a0(char **ppcParm1,long lParm2,long lParm3);
void FUN_00409110(long *plParm1,ulong uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_00409590(undefined8 *puParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
undefined8 FUN_00409610(long param_1,ulong param_2,long param_3,undefined4 *param_4,undefined8 param_5,undefined8 param_6,undefined8 param_7);
long FUN_00409830(long *plParm1,ulong uParm2);
undefined8 FUN_00409890(undefined8 uParm1,char cParm2);
void FUN_00409ac0(long lParm1,ulong uParm2,ulong uParm3,undefined8 uParm4,undefined8 uParm5,long lParm6);
ulong FUN_0040a400(undefined8 *puParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_0040a590(long lParm1);
ulong FUN_0040a5f0(void);
void FUN_0040a690(void);
ulong FUN_0040a6b0(uint *puParm1,long lParm2);
ulong FUN_0040a810(long lParm1);
long FUN_0040a990(undefined8 *puParm1,long lParm2,long *plParm3);
long * FUN_0040aa20(long lParm1,undefined8 uParm2,long *plParm3,undefined8 uParm4,undefined8 uParm5);
long FUN_0040aaa0(long *plParm1,ulong uParm2);
long FUN_0040ab90(long *plParm1);
void FUN_0040aba0(long lParm1,ulong uParm2,ulong uParm3,long lParm4);
void FUN_0040ad10(undefined4 *puParm1,ulong uParm2,ulong uParm3,ulong uParm4,undefined8 uParm5,undefined4 *puParm6);
void FUN_0040b080(void);
void FUN_0040b0b0(undefined8 *puParm1,ulong uParm2,long lParm3,ulong uParm4);
void FUN_0040b3e0(void);
void FUN_0040b590(void);
void FUN_0040b5a0(uint uParm1);
long FUN_0040b7c0(undefined8 uParm1,long *plParm2,long lParm3,long lParm4);
void FUN_0040b940(undefined auParm1 [16],undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined auParm8 [16],undefined8 uParm9,undefined8 uParm10,long lParm11,undefined8 uParm12,undefined8 uParm13,undefined8 uParm14);
void FUN_0040b9a0(long *plParm1,long lParm2,long lParm3);
long FUN_0040ba80(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
void FUN_0040bb90(undefined4 *puParm1,undefined4 uParm2);
void FUN_0040bba0(undefined4 *puParm1);
void FUN_0040bc00(int *piParm1,ulong uParm2,int *piParm3);
void FUN_0040c390(uint *puParm1,undefined8 uParm2);
void FUN_0040c4b0(undefined8 *puParm1,ulong uParm2,long lParm3);
void FUN_0040c7d0(long lParm1,ulong uParm2);
char * FUN_0040c800(char **ppcParm1);
ulong FUN_0040c8d0(byte bParm1);
ulong FUN_0040c930(long lParm1,ulong uParm2,long lParm3,ulong uParm4);
ulong FUN_0040caf0(char *pcParm1,char *pcParm2);
ulong FUN_0040ccb0(ulong uParm1);
ulong FUN_0040cd50(ulong uParm1);
ulong FUN_0040cdc0(ulong uParm1);
undefined * FUN_0040ce30(long *plParm1,ulong uParm2);
undefined8 FUN_0040ce60(float **ppfParm1);
ulong FUN_0040cee0(float fParm1,ulong uParm2,char cParm3);
void FUN_0040cf70(undefined8 *puParm1,undefined8 *puParm2);
long FUN_0040cf90(long lParm1,long lParm2,long **pplParm3,char cParm4);
void FUN_0040d0a0(long *plParm1);
undefined8 FUN_0040d0c0(long lParm1,long **pplParm2,char cParm3);
long * FUN_0040d200(undefined8 uParm1,undefined1 *puParm2,undefined *puParm3,undefined *puParm4,long lParm5);
undefined * FUN_0040d300(undefined8 *puParm1,undefined8 uParm2);
undefined *FUN_0040d470(ulong uParm1,undefined8 *puParm2,long *plParm3,undefined8 uParm4,undefined8 uParm5,long *plParm6);
undefined8 FUN_0040d6d0(undefined8 uParm1,undefined8 uParm2);
long FUN_0040d710(long lParm1,undefined8 uParm2);
void FUN_0040d900(long lParm1,ulong uParm2,code *pcParm3);
ulong FUN_0040d980(long lParm1,ulong uParm2,code *pcParm3);
undefined8 * FUN_0040da60(undefined *puParm1,long lParm2);
undefined8 FUN_0040dad0(long *plParm1,undefined8 uParm2);
undefined8 FUN_0040db40(long *plParm1);
char * FUN_0040db80(uint uParm1,long lParm2);
char * FUN_0040dc10(ulong uParm1,long lParm2);
char * FUN_0040dc50(ulong uParm1,long lParm2);
ulong FUN_0040dca0(byte *pbParm1,long lParm2,ulong uParm3);
undefined8 FUN_0040df40(char *pcParm1);
long FUN_0040e030(void);
long FUN_0040e080(int iParm1);
ulong FUN_0040e0d0(undefined8 uParm1);
void FUN_0040e150(void);
void FUN_0040e210(void);
ulong FUN_0040e310(undefined8 *puParm1,uint uParm2);
ulong FUN_0040e4e0(void);
int * FUN_0040e560(long lParm1);
int * FUN_0040e610(int *piParm1,undefined8 *puParm2);
ulong FUN_0040e950(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8);
ulong FUN_0040ea10(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9);
ulong FUN_0040eae0(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9,byte param_10);
undefined8 FUN_0040ebd0(byte *param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined * FUN_0040ec80(char *pcParm1,int iParm2);
undefined8 *FUN_0040ed50(undefined *puParm1,undefined8 *puParm2,ulong uParm3,ulong uParm4,uint uParm5,long *plParm6,long lParm7,char *pcParm8,char *pcParm9);
undefined1 *FUN_00410000(undefined1 *puParm1,undefined8 *puParm2,undefined8 uParm3,uint *puParm4,undefined8 uParm5,long *plParm6);
ulong FUN_004101a0(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
void FUN_004101e0(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00410210(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_00410250(ulong uParm1,undefined8 uParm2);
void FUN_004102f0(uint uParm1,undefined8 uParm2,undefined8 uParm3);
undefined1 * FUN_00410380(undefined1 *puParm1,undefined8 *puParm2);
undefined1 * FUN_00410390(undefined8 *puParm1);
void FUN_004103a0(undefined8 uParm1,undefined8 uParm2);
void FUN_004105c0(undefined4 *puParm1,ulong uParm2);
undefined8 * FUN_004107d0(long lParm1,ulong uParm2);
void FUN_00410880(long *plParm1,ulong uParm2,ulong uParm3);
undefined8 FUN_004108a0(long *plParm1);
undefined8 FUN_004108f0(undefined8 uParm1);
undefined8 FUN_00410900(long lParm1,undefined8 uParm2);
void FUN_00410910(long *plParm1,long *plParm2);
void FUN_00410bf0(ulong *puParm1);
void FUN_00410e90(long *plParm1);
undefined8 FUN_00410f80(undefined8 *puParm1);
ulong FUN_00410fe0(undefined8 uParm1,long lParm2);
ulong FUN_004111f0(undefined8 uParm1,ulong uParm2);
void FUN_00411710(undefined8 uParm1,long lParm2,long lParm3,long lParm4,long lParm5);
void FUN_00411960(void);
void FUN_00411bf0(void);
void FUN_00411c80(ulong uParm1,ulong uParm2);
void FUN_00411ca0(ulong uParm1,ulong uParm2);
void FUN_00411cd0(ulong uParm1,ulong uParm2);
long FUN_00411ce0(long lParm1,ulong *puParm2);
long FUN_00411d20(long lParm1,ulong *puParm2,ulong uParm3);
void FUN_00411db0(ulong uParm1,ulong uParm2);
void FUN_00411de0(undefined8 uParm1,undefined8 uParm2);
void FUN_00411e10(undefined auParm1 [16],undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined auParm7 [16]);
void FUN_00411e40(undefined auParm1 [16],undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined auParm7 [16],ulong uParm8,undefined8 uParm9,undefined8 uParm10,undefined8 uParm11,undefined8 uParm12,undefined8 uParm13);
ulong FUN_00411ed0(undefined8 uParm1,long lParm2,undefined8 uParm3,long lParm4);
undefined8 FUN_00411f30(void);
ulong FUN_00411f90(int iParm1);
ulong FUN_00411fb0(ulong *puParm1,int iParm2);
ulong FUN_00411fe0(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_00412010(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined *FUN_00412380(int iParm1,undefined8 *puParm2,undefined uParm3,long lParm4,undefined8 uParm5,long *plParm6);
ulong FUN_004123f0(int iParm1,undefined8 uParm2,char cParm3,undefined8 uParm4,undefined8 uParm5);
ulong FUN_00412410(int iParm1);
ulong FUN_00412430(ulong *puParm1,int iParm2);
ulong FUN_00412460(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_00412490(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_00412800(ulong uParm1,ulong uParm2);
void FUN_00412850(undefined8 uParm1);
ulong FUN_00412890(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00412900(void);
void FUN_00412920(void);
void FUN_00412950(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00412a30(undefined8 uParm1,undefined8 uParm2);
void FUN_00412a50(undefined8 uParm1);
ulong FUN_00412ae0(ulong param_1,undefined8 param_2,ulong param_3);
void FUN_00412c30(undefined8 uParm1);
ulong FUN_00412c40(long lParm1);
undefined8 FUN_00412cd0(void);
void FUN_00412cf0(long lParm1,int *piParm2);
ulong FUN_00412f40(int param_1,undefined8 *param_2,char *param_3,char **param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_004136b0(char *pcParm1,int *piParm2,int iParm3);
ulong FUN_00413770(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,uint *param_7,uint param_8);
void FUN_00413c20(void);
void FUN_00413c80(void);
void FUN_00413ca0(void);
void FUN_00413d70(long lParm1);
ulong FUN_00413d90(uint *puParm1,byte *pbParm2,long lParm3);
ulong * FUN_00413e00(ulong *puParm1,undefined8 uParm2,ulong uParm3);
undefined8 FUN_00413ee0(long *plParm1,long *plParm2);
void FUN_00413f90(long lParm1,undefined8 uParm2);
void FUN_00413fb0(long lParm1,undefined8 uParm2);
undefined8 FUN_004140a0(long *plParm1,undefined *puParm2,long lParm3,ulong uParm4,undefined8 uParm5);
void FUN_004140c0(ulong *puParm1,ulong uParm2);
void FUN_004141b0(long lParm1,long lParm2);
void FUN_004141f0(void);
undefined8 FUN_00414210(long lParm1,long lParm2);
undefined8 FUN_00414270(long lParm1);
ulong FUN_00414340(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_004145d0(long param_1,char param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined8 FUN_00414630(char *pcParm1);
ulong FUN_00414670(ulong uParm1);
ulong FUN_004146d0(int iParm1);
undefined FUN_00414700(int iParm1);;
undefined FUN_00414720(int iParm1);;
undefined FUN_00414730(int iParm1);;
undefined FUN_00414750(int iParm1);;
ulong FUN_00414760(uint uParm1);
ulong FUN_00414770(byte *pbParm1,byte *pbParm2);
void FUN_00414830(double dParm1);
ulong FUN_00414940(ulong uParm1,ulong uParm2);
long FUN_00414960(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00414a00(void);
undefined * FUN_00414a10(void);
char * FUN_00414d70(void);
ulong FUN_00414ee0(long lParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_00414f60(ulong uParm1);
ulong FUN_00415120(long param_1,long param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10);
undefined8 FUN_00415190(char *param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8);
ulong FUN_004151d0(undefined8 uParm1);
void FUN_00415520(void);
undefined8 FUN_00415540(long lParm1,long lParm2);
ulong FUN_004155b0(char *pcParm1,long lParm2);
char * FUN_004155f0(ulong uParm1,long lParm2,long lParm3);
ulong FUN_00415830(void);
undefined * FUN_00415870(undefined8 param_1,undefined8 *param_2);
uint * FUN_00415ae0(int *param_1,ulong *param_2);
undefined * FUN_00415cd0(undefined8 uParm1,undefined8 *puParm2);
uint * FUN_00415ee0(undefined auParm1 [16],int *piParm2,ulong *puParm3);
undefined4 *FUN_00416050(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5,undefined8 *puParm6);
int * FUN_00416190(long *plParm1,long lParm2,long *plParm3,long *plParm4,long **pplParm5,int **ppiParm6);
undefined8 FUN_00416740(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_00416cf0(uint param_1);
void FUN_00416d30(uint uParm1);
double FUN_0041c060(int *piParm1);
void FUN_0041c0b0(int *param_1);
long FUN_0041c130(ulong uParm1,ulong uParm2);
long FUN_0041c150(void);
undefined8 FUN_0041c180(uint *puParm1,ulong *puParm2);
undefined8 FUN_0041c3a0(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_0041d090(undefined auParm1 [16]);
ulong FUN_0041d0b0(void);
void FUN_0041d0e0(void);
undefined8 _DT_FINI(void);

