typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined5;
typedef unsigned long    undefined6;
typedef unsigned long    undefined7;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00403510(void);
void FUN_004035c0(void);
void FUN_004037c0(void);
void entry(void);
void FUN_00403e60(void);
void FUN_00403ed0(void);
void FUN_00403f60(void);
ulong FUN_00403fa0(uint uParm1);
undefined8 FUN_00403fe0(undefined8 uParm1);
undefined FUN_00403ff0(int iParm1);;
undefined8 FUN_00404000(void);
char * FUN_00404010(char *pcParm1);
void FUN_00404060(void);
ulong FUN_00404130(byte **ppbParm1,byte **ppbParm2,uint uParm3,long *plParm4);
void FUN_00404350(char cParm1);
undefined8 FUN_00404380(undefined8 uParm1);
ulong FUN_00404390(int iParm1);
void FUN_004043b0(void);
ulong FUN_004043f0(char cParm1,ulong uParm2,int iParm3);
void FUN_004044e0(char *pcParm1,char *pcParm2,char *pcParm3);
void FUN_00404540(void);
void FUN_004046e0(undefined8 *puParm1);
void FUN_00404730(void);
void thunk_FUN_004037c0(void);
void FUN_00404800(undefined8 *puParm1);
uint FUN_00404820(byte bParm1);
ulong FUN_00404860(ulong uParm1,ulong uParm2);
void FUN_004048f0(void);
void FUN_00404950(undefined8 uParm1);
ulong FUN_00404980(uint uParm1);
void FUN_004049f0(undefined auParm1 [16],undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8);
void FUN_00404a50(void);
void FUN_00404a70(void);
undefined8 FUN_00404a90(undefined8 uParm1);
undefined8 FUN_00404b00(long lParm1);
void FUN_00404bb0(void);
void FUN_00404d10(long lParm1,long lParm2,undefined uParm3);
void FUN_00404d80(undefined8 uParm1);
undefined8 FUN_00404f30(undefined8 *puParm1,undefined8 uParm2);
undefined8 FUN_00404f90(char *pcParm1);
undefined8 FUN_00404ff0(void);
void FUN_00405080(void);
ulong * FUN_004052d0(long *plParm1,char cParm2);
void FUN_00405630(char cParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00405670(undefined8 uParm1,long lParm2);
void FUN_00405700(undefined8 uParm1,long lParm2,char cParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
undefined8 FUN_00405740(long lParm1,char *pcParm2);
ulong FUN_004057e0(char *pcParm1);
ulong FUN_004058c0(ulong uParm1);
undefined * FUN_00405900(char *pcParm1,char cParm2);
ulong FUN_004059b0(ulong uParm1);
void FUN_004059f0(void);
ulong FUN_00405b70(void);
void FUN_00405b90(undefined8 uParm1,ulong uParm2);
void FUN_00405d60(void);
void FUN_00405d70(void);
void FUN_00405d80(long lParm1,undefined8 uParm2,ulong uParm3);
void FUN_00405e10(ulong uParm1,uint uParm2,char cParm3);
void FUN_00405e60(ulong uParm1,uint uParm2,char cParm3);
undefined8 FUN_00405eb0(undefined8 uParm1,long lParm2);
char * thunk_FUN_0040e4a0(ulong uParm1,long lParm2);
ulong FUN_00405f60(undefined8 uParm1,undefined8 uParm2);
void FUN_00405fd0(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00406230(int iParm1,undefined8 uParm2,uint uParm3);
void FUN_00406720(long lParm1,byte bParm2);
undefined8 FUN_00406b50(void);
ulong FUN_00406b70(long *plParm1);
undefined8 FUN_00406bf0(void);
char * FUN_00406c10(long lParm1,undefined8 uParm2,ulong *puParm3,char *pcParm4);
void FUN_00406fd0(undefined8 *puParm1,undefined8 *puParm2);
void FUN_00407040(void);
void FUN_004070a0(void);
void FUN_00407110(void);
void FUN_00407140(void);
undefined FUN_004071f0(long lParm1);;
ulong FUN_00407450(long lParm1,char cParm2);
undefined8 FUN_004074d0(undefined8 **ppuParm1,int iParm2,byte bParm3,undefined8 *puParm4);
void FUN_004080a0(undefined8 uParm1,ulong uParm2,long lParm3,undefined8 uParm4,uint uParm5);
ulong FUN_00408100(void);
char * FUN_004081a0(char **ppcParm1,char *pcParm2,undefined8 uParm3,int iParm4,char **ppcParm5,byte *pbParm6);
long FUN_00408700(char *param_1,undefined8 param_2,ulong param_3,long param_4,char param_5,long param_6,long param_7);
long FUN_004089f0(undefined8 *puParm1,byte bParm2,undefined8 uParm3,ulong uParm4);
long FUN_00408b00(long lParm1,undefined8 uParm2);
undefined * FUN_00408c60(ulong uParm1);
long FUN_00409410(undefined *puParm1,undefined8 uParm2,ulong uParm3);
long FUN_00409470(undefined8 *puParm1);
ulong FUN_00409720(void);
void FUN_00409830(void);
void FUN_00409b60(void);
void FUN_00409c60(long lParm1,long lParm2,ulong uParm3);
void FUN_0040a0f0(void);
void FUN_0040a100(uint uParm1);
undefined * FUN_0040a400(ulong uParm1,undefined8 *puParm2);
long FUN_0040b180(undefined8 uParm1,ulong uParm2);
long FUN_0040b290(undefined8 uParm1,long *plParm2,long lParm3,long lParm4);
void FUN_0040b410(undefined auParm1 [16],undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined auParm8 [16],undefined8 uParm9,undefined8 uParm10,long lParm11,undefined8 uParm12,undefined8 uParm13,undefined8 uParm14);
void FUN_0040b470(long *plParm1,long lParm2,long lParm3);
long FUN_0040b550(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
ulong FUN_0040b5c0(int iParm1);
undefined FUN_0040b5f0(int iParm1);;
undefined FUN_0040b620(int iParm1);;
ulong FUN_0040b630(uint uParm1);
char * FUN_0040b640(char **ppcParm1,undefined8 uParm2,undefined8 uParm3);
char * FUN_0040b6c0(char *pcParm1,ulong uParm2);
void FUN_0040bd00(char *pcParm1);
char * FUN_0040bd40(char *pcParm1);
void FUN_0040bda0(long lParm1);
undefined8 FUN_0040bdd0(void);
undefined8 * FUN_0040bde0(long lParm1,undefined8 uParm2,undefined *puParm3);
ulong FUN_0040be60(long lParm1,undefined8 uParm2,undefined *puParm3);
undefined8 FUN_0040bea0(undefined8 uParm1);
void FUN_0040c020(long lParm1,undefined *puParm2);
void FUN_0040c030(char *pcParm1);
void FUN_0040c050(char *pcParm1);
long FUN_0040c070(long lParm1,char *pcParm2,undefined8 *puParm3);
char * FUN_0040c140(char **ppcParm1);
void FUN_0040c5f0(undefined8 *puParm1);
ulong FUN_0040c630(ulong uParm1);
ulong FUN_0040c6d0(ulong uParm1);
ulong FUN_0040c740(ulong uParm1);
undefined * FUN_0040c7b0(long *plParm1,ulong uParm2);
undefined8 FUN_0040c7e0(float **ppfParm1);
ulong FUN_0040c860(float fParm1,ulong uParm2,char cParm3);
void FUN_0040c8f0(undefined8 *puParm1,undefined8 *puParm2);
long FUN_0040c910(long lParm1,long lParm2,long **pplParm3,char cParm4);
void FUN_0040ca20(long *plParm1);
undefined8 FUN_0040ca40(long lParm1,long **pplParm2,char cParm3);
undefined8 FUN_0040cb80(long lParm1);
long FUN_0040cb90(long lParm1,long lParm2);
long * FUN_0040cbf0(undefined8 uParm1,undefined1 *puParm2,undefined *puParm3,undefined *puParm4,long lParm5);
void FUN_0040ccf0(long **pplParm1);
undefined * FUN_0040cdd0(undefined8 *puParm1,undefined8 uParm2);
undefined * FUN_0040cf40(ulong uParm1,undefined8 *puParm2,long *plParm3);
undefined8 FUN_0040d1a0(undefined8 uParm1,undefined8 uParm2);
long FUN_0040d1e0(long lParm1,undefined8 uParm2);
ulong FUN_0040d3c0(undefined8 *puParm1,ulong uParm2);
void FUN_0040d430(undefined8 *puParm1);
void FUN_0040d450(void);
void FUN_0040d520(long lParm1,ulong uParm2,byte *pbParm3,undefined8 uParm4);
long FUN_0040d5e0(void);
undefined8 FUN_0040d610(char *pcParm1,undefined8 *puParm2,uint *puParm3);
char * FUN_0040d760(ulong uParm1,long lParm2,ulong uParm3,ulong uParm4,ulong uParm5);
undefined8 FUN_0040e330(undefined8 uParm1,undefined8 uParm2,long *plParm3);
uint * FUN_0040e360(uint uParm1);
uint * FUN_0040e400(uint uParm1);
char * FUN_0040e4a0(ulong uParm1,long lParm2);
char * FUN_0040e540(ulong uParm1,long lParm2);
void FUN_0040e590(undefined *puParm1,undefined *puParm2,long lParm3);
undefined8 FUN_0040e5c0(int *piParm1);
ulong FUN_0040e620(int *piParm1,ulong uParm2);
ulong FUN_0040e680(long lParm1,long lParm2,long lParm3,ulong *puParm4,int iParm5,ulong uParm6);
ulong FUN_0040e9d0(byte *pbParm1,long lParm2,ulong uParm3);
ulong FUN_0040ec70(byte *pbParm1,ulong uParm2);
void FUN_0040eca0(undefined8 *puParm1,ulong uParm2,undefined8 *puParm3,code *pcParm4);
void FUN_0040ed80(undefined8 *puParm1,ulong uParm2,undefined8 *puParm3,code *pcParm4);
void FUN_0040eee0(undefined8 *puParm1,ulong uParm2,code *pcParm3);
ulong FUN_0040eef0(int iParm1,int iParm2);
long FUN_0040ef30(long lParm1,long lParm2,long lParm3);
long FUN_0040ef70(long lParm1,long lParm2,long lParm3);
char * FUN_0040efb0(char *pcParm1,long lParm2,char *pcParm3,uint *puParm4,byte bParm5,undefined8 uParm6,undefined8 uParm7,uint uParm8);
int * FUN_004107d0(long lParm1);
int * FUN_00410880(int *piParm1,undefined8 *puParm2);
ulong FUN_00410bc0(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8);
ulong FUN_00410c80(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9);
ulong FUN_00410d50(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9,byte param_10);
undefined8 FUN_00410e40(byte *param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined * FUN_00410ef0(char *pcParm1,int iParm2);
undefined8 *FUN_00410fc0(undefined *puParm1,undefined8 *puParm2,ulong uParm3,ulong uParm4,uint uParm5,uint uParm6,long lParm7,char *pcParm8,char *pcParm9);
undefined1 * FUN_00412270(undefined1 *puParm1,undefined8 *puParm2,undefined8 uParm3,uint *puParm4);
undefined8 FUN_00412410(undefined1 *puParm1);
ulong FUN_00412450(undefined1 *puParm1);
void FUN_00412460(undefined1 *puParm1,undefined4 uParm2);
ulong FUN_00412470(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
undefined8 FUN_004124b0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined1 *puParm5);
void FUN_00412530(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00412560(ulong uParm1,undefined8 uParm2);
void FUN_00412600(uint uParm1,undefined8 uParm2,undefined8 uParm3);
undefined1 * FUN_00412690(undefined1 *puParm1,undefined8 *puParm2);
undefined1 * FUN_004126a0(undefined8 *puParm1);
ulong FUN_004126b0(long lParm1,int iParm2,long lParm3,int iParm4);
void FUN_004126d0(undefined8 uParm1,long lParm2,long lParm3,long lParm4,long lParm5);
void FUN_00412920(void);
void FUN_00412bb0(void);
void FUN_00412c40(ulong uParm1,ulong uParm2);
void FUN_00412c60(ulong uParm1,ulong uParm2);
void FUN_00412c90(ulong uParm1,ulong uParm2);
long FUN_00412ca0(long lParm1,ulong *puParm2);
long FUN_00412ce0(long lParm1,ulong *puParm2,ulong uParm3);
long FUN_00412da0(long lParm1,ulong *puParm2);
void FUN_00412db0(undefined8 uParm1,undefined8 uParm2);
void FUN_00412de0(undefined8 uParm1);
undefined * FUN_00412e00(void);
ulong FUN_00412e30(undefined8 param_1,ulong param_2,ulong param_3,ulong param_4,undefined8 param_5,undefined8 param_6,uint param_7);
long FUN_00412f00(void);
long FUN_00412f30(void);
ulong FUN_00412fe0(int iParm1);
ulong FUN_00413000(ulong *puParm1,int iParm2);
ulong FUN_00413030(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_00413060(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined *FUN_004133d0(int iParm1,undefined8 *puParm2,undefined uParm3,long lParm4,undefined8 uParm5,ulong uParm6);
ulong FUN_00413440(int iParm1,undefined8 uParm2,char cParm3,undefined8 uParm4,undefined8 uParm5);
ulong FUN_00413460(int iParm1);
ulong FUN_00413480(ulong *puParm1,int iParm2);
ulong FUN_004134b0(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_004134e0(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_00413850(ulong uParm1,ulong uParm2);
void FUN_004138a0(undefined8 uParm1);
ulong FUN_004138e0(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00413950(void);
void FUN_00413970(void);
void FUN_004139a0(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
int * FUN_00413a80(int *piParm1);
char * FUN_00413b80(char *pcParm1);
undefined8 FUN_00413c80(int *piParm1);
undefined8 FUN_00414250(uint uParm1,long lParm2,ulong uParm3,ulong uParm4,ulong uParm5,uint uParm6);
ulong FUN_00414830(uint *puParm1,uint *puParm2,uint *puParm3,bool bParm4,uint uParm5);
undefined8 FUN_00415830(int iParm1,long lParm2,ulong uParm3,ulong uParm4,ulong uParm5,uint uParm6);
ulong FUN_00415d70(byte *pbParm1,byte *pbParm2,byte *pbParm3,bool bParm4,uint uParm5);
ulong FUN_00416dc0(undefined8 uParm1,long lParm2,ulong uParm3);
long FUN_00417060(long lParm1,ulong uParm2);
void FUN_004174e0(long lParm1,int *piParm2);
ulong FUN_00417730(int param_1,undefined8 *param_2,char *param_3,char **param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_00417ea0(char *pcParm1,int *piParm2,int iParm3);
ulong FUN_00417f60(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,uint *param_7,uint param_8);
void FUN_00418410(void);
void FUN_00418470(void);
void FUN_00418490(void);
void FUN_00418560(void);
undefined8 FUN_00418580(long lParm1,long lParm2);
void FUN_004185f0(long lParm1);
ulong FUN_00418610(uint *puParm1,byte *pbParm2,long lParm3);
ulong * FUN_00418680(ulong *puParm1,undefined8 uParm2,ulong uParm3);
void FUN_00418760(long lParm1,undefined8 uParm2);
void FUN_00418780(long lParm1,undefined8 uParm2);
undefined8 FUN_00418870(long *plParm1,undefined *puParm2,long lParm3,ulong uParm4,undefined8 uParm5);
void FUN_00418890(ulong *puParm1,ulong uParm2);
void FUN_00418980(void);
ulong FUN_00418990(uint param_1,undefined8 param_2,uint param_3,ulong param_4);
undefined8 FUN_00418a90(long lParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00418b00(long lParm1,long lParm2);
ulong FUN_00418b40(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,long param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00418c50(void);
undefined8 FUN_00418c70(long lParm1,long lParm2);
char * FUN_00418cd0(char *pcParm1,char *pcParm2,ulong uParm3);
ulong FUN_00418e10(uint uParm1,uint uParm2);
ulong FUN_00418e30(uint *puParm1,uint *puParm2);
void FUN_00418e80(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_00418e90(void);
ulong FUN_00418ea0(char *pcParm1);
ulong FUN_00418ed0(long lParm1);
undefined8 * FUN_00418f10(long lParm1);
undefined8 FUN_00418fb0(long *plParm1,char *pcParm2);
void FUN_004190f0(long *plParm1);
long FUN_00419120(long lParm1);
ulong FUN_004191d0(long lParm1);
undefined8 FUN_00419220(long lParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_004192c0(long lParm1,undefined8 uParm2);
long FUN_004193a0(uint *puParm1);
void FUN_004193c0(void);
ulong FUN_00419500(char *pcParm1,long lParm2);
char * FUN_00419540(ulong uParm1,long lParm2,long lParm3);
ulong FUN_00419780(void);
undefined * FUN_004197c0(undefined8 param_1,undefined8 *param_2);
uint * FUN_00419a30(int *param_1,ulong *param_2);
undefined * FUN_00419c20(undefined8 uParm1,undefined8 *puParm2);
uint * FUN_00419e30(undefined auParm1 [16],int *piParm2,ulong *puParm3);
undefined4 *FUN_00419fa0(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5,undefined8 *puParm6);
int * FUN_0041a0e0(undefined8 *puParm1,long lParm2,undefined8 *puParm3,undefined8 *puParm4,uint **ppuParm5,int **ppiParm6);
undefined8 FUN_0041a690(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_0041ac40(uint param_1);
void FUN_0041ac80(uint uParm1);
ulong FUN_0041ffb0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00420070(int *piParm1,long lParm2);
ulong FUN_004202d0(long param_1,char param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined8 FUN_00420330(char *pcParm1);
ulong FUN_00420370(ulong uParm1);
ulong FUN_004203d0(byte *pbParm1,byte *pbParm2);
ulong FUN_00420420(ulong uParm1,char cParm2);
undefined8 FUN_00420500(void);
ulong FUN_00420510(char *pcParm1,ulong uParm2);
undefined * FUN_00420550(void);
char * FUN_004208b0(void);
double FUN_00420970(int *piParm1);
void FUN_004209c0(int *param_1);
ulong FUN_00420bf0(long param_1,long param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9,char param_10);
undefined8 FUN_00420c60(char *param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8);
ulong FUN_00420ca0(undefined8 uParm1);
long FUN_00420ff0(ulong uParm1,ulong uParm2);
long FUN_00421010(void);
void FUN_00421040(undefined8 uParm1);
ulong FUN_004210d0(ulong param_1,undefined8 param_2,ulong param_3);
undefined8 FUN_00421220(ulong uParm1);
void FUN_00421290(undefined8 uParm1);
ulong FUN_004212a0(long lParm1);
undefined8 FUN_00421330(void);
void FUN_00421350(void);
ulong FUN_00421370(void);
ulong FUN_00421390(ulong uParm1,long lParm2,long lParm3,ulong uParm4);
ulong FUN_00421450(ulong uParm1);
ulong FUN_004214d0(uint uParm1,uint uParm2);
void FUN_004214f0(code *pcParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
long FUN_00421510(long lParm1);
long FUN_00421520(ulong param_1,long param_2,undefined8 param_3,int param_4,int param_5,uint param_6,int param_7,int param_8,int param_9,int param_10);
long FUN_00421650(long lParm1);
long FUN_00421660(undefined8 uParm1,undefined8 uParm2);
long FUN_00421680(void);
long FUN_00421730(code *pcParm1,long *plParm2,undefined8 uParm3,undefined8 uParm4);
long FUN_00421c50(uint *puParm1);
undefined8 FUN_00421c70(uint *puParm1,ulong *puParm2);
undefined8 FUN_00421e90(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_00422b80(char *pcParm1,char *pcParm2,ulong uParm3);
ulong FUN_00422cb0(undefined auParm1 [16]);
ulong FUN_00422cd0(void);
undefined8 * FUN_00422d00(ulong uParm1);
void FUN_00422d80(ulong uParm1);
void FUN_00422e10(void);
undefined8 _DT_FINI(void);

