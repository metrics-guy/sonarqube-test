typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined3;
typedef unsigned int    undefined4;
typedef unsigned long    undefined5;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00403830(void);
void entry(void);
void FUN_00403db0(void);
void FUN_00403e20(void);
void FUN_00403eb0(void);
void FUN_00403ef0(void);
void FUN_00403f10(void);
ulong FUN_00403f50(undefined8 uParm1);
void FUN_00403fc0(long lParm1);
ulong FUN_00404020(undefined *puParm1);
ulong FUN_004040a0(long lParm1,undefined8 uParm2,long lParm3);
ulong FUN_00404150(long lParm1,undefined8 uParm2,char cParm3,undefined8 uParm4);
void FUN_004041f0(void);
void FUN_004043a0(uint uParm1);
ulong FUN_00404490(char *pcParm1);
long FUN_004044c0(long lParm1);
void FUN_00404510(long lParm1);
long FUN_00404540(undefined8 uParm1);
ulong FUN_00404580(undefined8 uParm1,undefined8 uParm2);
undefined8 * FUN_004045f0(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00404610(char cParm1,int iParm2);
undefined8 FUN_00404630(long lParm1,long lParm2,byte *pbParm3,char cParm4);
undefined8 FUN_004047c0(undefined8 uParm1,undefined8 uParm2,long lParm3);
undefined8 FUN_00404830(ulong uParm1,undefined8 uParm2,long lParm3);
undefined8 FUN_004048b0(long lParm1,long lParm2,char cParm3,char *pcParm4,int iParm5,int *piParm6);
ulong FUN_00404c10(long lParm1,long lParm2,long lParm3);
ulong FUN_00404ff0(long *plParm1,long lParm2);
ulong FUN_00405140(char *pcParm1);
long FUN_00405170(long lParm1,ulong uParm2);
undefined FUN_00405190(int iParm1);;
ulong FUN_004051a0(int iParm1,undefined8 uParm2,uint uParm3);
ulong FUN_004051b0(long lParm1);
ulong FUN_004051f0(long lParm1,uint uParm2);
undefined8 FUN_00405210(uint *puParm1);
ulong FUN_00405320(undefined8 uParm1,ulong *puParm2,undefined8 uParm3,ulong *puParm4,int *piParm5,undefined *puParm6);
ulong FUN_00405830(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00405848(undefined8 uParm1);
void FUN_00405880(undefined8 uParm1,undefined8 uParm2,long lParm3);
ulong FUN_00405900(undefined8 uParm1,undefined8 uParm2,byte bParm3,byte bParm4,char cParm5);
void FUN_004059f0(undefined8 uParm1,ulong uParm2);
undefined8 FUN_00405a00(uint uParm1,ulong uParm2);
ulong FUN_00405ab0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_00405b00(ulong uParm1,undefined8 uParm2,char cParm3,long lParm4);
ulong FUN_00405bb0(char *pcParm1,ulong uParm2);
ulong FUN_00405c10(uint param_1,uint param_2,long param_3,ulong param_4,ulong param_5,byte param_6,undefined8 param_7,undefined8 param_8,ulong param_9,long *param_10,char *param_11);
void FUN_00406000(undefined8 uParm1,undefined8 uParm2,uint uParm3,undefined8 uParm4);
ulong FUN_00406020(void);
long FUN_00406050(long lParm1);
void FUN_00406080(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00406090(long *plParm1,long *plParm2,long *plParm3);
undefined8 FUN_004060d0(void);
undefined8 FUN_004060e0(void);
ulong FUN_00406100(void);
ulong FUN_00406130(byte *pbParm1);
void FUN_00406160(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00406190(ulong param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,long param_6,int param_7,undefined8 param_8,undefined8 param_9,undefined *param_10);
void FUN_00406560(long lParm1,undefined8 uParm2,uint *puParm3);
ulong FUN_00406650(long lParm1,undefined8 uParm2,long lParm3);
ulong FUN_004066e0(long lParm1,long *plParm2,long *plParm3,undefined8 uParm4);
undefined8 FUN_00406800(void);
ulong FUN_00406820(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,ulong uParm4,long lParm5);
ulong FUN_004069d0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,long lParm4);
void FUN_00406a80(long lParm1);
void FUN_00406ab0(undefined4 *puParm1);
ulong FUN_00406af0(long lParm1);
ulong FUN_00406b20(long lParm1,undefined8 uParm2,uint uParm3,long lParm4,char cParm5,long lParm6);
ulong FUN_00406ce0(void);
ulong FUN_00406d20(undefined8 param_1,char *param_2,long param_3,uint param_4,uint param_5,byte *param_6,long *param_7);
ulong FUN_00407890(char *param_1,char *param_2,byte param_3,char **param_4,char **param_5,uint *param_6,uint param_7,char *param_8,undefined *param_9,undefined *param_10);
void FUN_00409a50(undefined8 uParm1,undefined8 uParm2,ulong uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_00409ae0(undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined *param_6,byte *param_7,byte *param_8);
void FUN_00409ce0(long lParm1);
void FUN_00409d00(undefined8 uParm1,undefined8 uParm2);
long FUN_00409d40(undefined8 uParm1,undefined8 uParm2);
long FUN_00409d80(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
long FUN_00409e00(void);
undefined8 FUN_00409e40(void);
void FUN_00409e50(undefined4 uParm1,undefined4 *puParm2);
ulong FUN_00409e80(uint *puParm1);
long FUN_0040a180(long lParm1,long lParm2);
ulong FUN_0040a210(undefined4 uParm1,undefined8 uParm2,ulong uParm3,undefined8 uParm4,undefined4 uParm5,char cParm6);
ulong FUN_0040a340(undefined8 uParm1,ulong uParm2,undefined8 uParm3,char cParm4);
ulong FUN_0040a450(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040a4d0(undefined8 uParm1);
long FUN_0040a520(undefined8 uParm1,ulong uParm2);
ulong FUN_0040a620(long *plParm1,ulong uParm2,long lParm3,long lParm4,long *plParm5);
void FUN_0040a8f0(long lParm1,long lParm2);
void FUN_0040a9d0(char *pcParm1);
long FUN_0040aa30(long lParm1,int iParm2,char cParm3);
long FUN_0040ac90(long lParm1,int iParm2);
ulong FUN_0040acf0(undefined *puParm1,char *pcParm2);
ulong FUN_0040ad20(ulong uParm1,ulong uParm2,ulong uParm3);
char * FUN_0040ad90(char **ppcParm1,undefined8 uParm2,undefined8 uParm3);
char * FUN_0040b3b0(char *pcParm1);
long FUN_0040b540(void);
void FUN_0040b5f0(char *pcParm1);
void FUN_0040b610(char *pcParm1);
undefined * FUN_0040b650(undefined8 uParm1);
char * FUN_0040b6c0(char *pcParm1);
void FUN_0040b720(long lParm1);
undefined FUN_0040b750(char *pcParm1);;
void FUN_0040b790(void);
void FUN_0040b7a0(undefined8 param_1,byte param_2,ulong param_3);
undefined8 * FUN_0040b7f0(long lParm1,undefined8 uParm2,undefined *puParm3);
ulong FUN_0040b870(long lParm1,undefined8 uParm2,undefined *puParm3);
undefined * FUN_0040b8b0(long lParm1);
undefined8 FUN_0040b940(undefined8 uParm1);
void FUN_0040b9b0(uint uParm1,undefined *puParm2);
void FUN_0040bac0(char *pcParm1);
void FUN_0040bae0(char *pcParm1);
long FUN_0040bb00(long lParm1,char *pcParm2,undefined8 *puParm3);
long FUN_0040bbd0(uint uParm1,long lParm2,long lParm3);
ulong FUN_0040bc50(ulong uParm1);
ulong FUN_0040bcc0(ulong uParm1);
ulong FUN_0040bd30(long *plParm1,ulong uParm2);
undefined8 FUN_0040bd60(float **ppfParm1);
ulong FUN_0040bde0(float fParm1,ulong uParm2,char cParm3);
void FUN_0040be70(undefined8 *puParm1,undefined8 *puParm2);
long FUN_0040be90(long lParm1,long lParm2,long **pplParm3,char cParm4);
void FUN_0040bfa0(long *plParm1);
undefined8 FUN_0040bfc0(long lParm1,long **pplParm2,char cParm3);
long FUN_0040c100(long lParm1,long lParm2);
long * FUN_0040c160(undefined8 uParm1,undefined1 *puParm2,undefined *puParm3,undefined *puParm4,long lParm5);
void FUN_0040c260(long **pplParm1);
ulong FUN_0040c340(undefined8 *puParm1,undefined8 uParm2);
ulong FUN_0040c4b0(ulong uParm1,undefined8 *puParm2,long *plParm3);
undefined8 FUN_0040c710(undefined8 uParm1,undefined8 uParm2);
long FUN_0040c750(long lParm1,undefined8 uParm2);
ulong FUN_0040c930(undefined8 *puParm1,ulong uParm2);
void FUN_0040c9d0(undefined8 *puParm1);
long FUN_0040c9f0(long lParm1);
undefined8 FUN_0040caa0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4,uint uParm5);
void FUN_0040caf0(undefined8 uParm1,ulong uParm2,undefined4 uParm3);
int * FUN_0040cb10(int *piParm1,undefined8 *puParm2);
ulong FUN_0040ce50(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8);
ulong FUN_0040cf10(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9);
ulong FUN_0040cfe0(long param_1,long param_2,byte param_3,byte param_4,byte param_5,byte param_6,byte param_7,byte param_8,byte param_9,byte param_10);
undefined8 FUN_0040d0d0(byte *param_1,undefined8 param_2,byte param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
undefined * FUN_0040d180(char *pcParm1,int iParm2);
undefined8 *FUN_0040d250(undefined *puParm1,undefined8 *puParm2,ulong uParm3,ulong uParm4,uint uParm5,uint uParm6,long lParm7,char *pcParm8,char *pcParm9);
undefined1 * FUN_0040e500(undefined1 *puParm1,undefined8 *puParm2,undefined8 uParm3,uint *puParm4);
ulong FUN_0040e6a0(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
void FUN_0040e6e0(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040e710(ulong uParm1,undefined8 uParm2);
void FUN_0040e7b0(uint uParm1,undefined8 uParm2,undefined8 uParm3);
undefined1 * FUN_0040e840(undefined1 *puParm1,undefined8 *puParm2);
undefined1 * FUN_0040e850(undefined8 *puParm1);
undefined8 FUN_0040e860(undefined4 uParm1);
ulong FUN_0040e880(uint uParm1,long lParm2,uint uParm3,long lParm4,uint uParm5);
undefined4 * FUN_0040ec80(undefined4 *puParm1);
long FUN_0040ecd0(uint uParm1,undefined8 uParm2,ulong uParm3);
ulong FUN_0040ed40(undefined8 uParm1,undefined8 uParm2);
undefined * FUN_0040ee70(long lParm1,uint uParm2);
undefined8 FUN_0040f100(undefined8 uParm1,ulong uParm2);
ulong FUN_0040f180(ulong uParm1,long lParm2,long lParm3);
undefined FUN_0040f190(undefined8 uParm1,ulong uParm2);;
ulong FUN_0040f230(long lParm1,int iParm2,undefined8 uParm3,code *pcParm4,long lParm5);
ulong FUN_0040f3a0(uint uParm1);
undefined8 FUN_0040f400(undefined8 uParm1);
ulong FUN_0040f410(undefined8 uParm1,undefined8 *puParm2,long lParm3,ulong uParm4);
ulong FUN_00410010(undefined8 *puParm1);
void FUN_004100d0(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_004100e0(long lParm1,long *plParm2);
ulong FUN_004101c0(uint uParm1,long lParm2,undefined *puParm3);
ulong FUN_00410540(long lParm1,undefined *puParm2);
ulong FUN_00410550(undefined8 uParm1,undefined8 *puParm2);
ulong FUN_00410770(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long *plParm5,undefined8 uParm6);
void FUN_004109c0(void);
void FUN_00410c50(void);
ulong FUN_00410ce0(void);
void FUN_00410d20(long lParm1);
void FUN_00410d40(long lParm1);
long FUN_00410d50(long lParm1,ulong *puParm2);
long FUN_00410d90(long lParm1,ulong *puParm2,ulong uParm3);
long FUN_00410dc0(long lParm1,ulong *puParm2,ulong uParm3);
void FUN_00410e80(undefined8 uParm1);
void FUN_00410ea0(void);
void FUN_00410ed0(undefined8 uParm1,uint uParm2);
ulong FUN_00410f20(void);
ulong FUN_00410f50(void);
undefined8 FUN_00410f90(ulong uParm1,ulong uParm2);
void FUN_00410fe0(undefined8 uParm1);
ulong FUN_00411020(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00411090(void);
void FUN_004110b0(void);
void FUN_004110e0(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_004111c0(void);
ulong FUN_004111d0(ulong uParm1,long lParm2);
ulong FUN_00411250(ulong param_1,undefined8 param_2,ulong param_3);
undefined8 FUN_004113a0(ulong uParm1);
void FUN_00411410(undefined8 uParm1);
ulong FUN_00411420(long lParm1);
undefined8 FUN_004114b0(void);
void FUN_004114d0(void);
ulong FUN_004114f0(void);
ulong FUN_00411510(ulong uParm1,long lParm2,long lParm3,ulong uParm4);
long FUN_00411650(long *plParm1);
ulong FUN_004116a0(ulong uParm1,undefined8 *puParm2);
ulong FUN_004116d0(long lParm1,long lParm2,char cParm3);
long FUN_00411870(long lParm1,long lParm2,ulong uParm3);
long FUN_00411970(long lParm1,undefined8 uParm2,long lParm3);
void FUN_00411a10(long lParm1);
void FUN_00411a70(undefined8 uParm1);
long FUN_00411ab0(undefined8 uParm1,undefined8 uParm2,uint uParm3,uint *puParm4);
ulong FUN_00411b10(long lParm1);
ulong FUN_00411c20(void);
ulong FUN_00411c50(void);
ulong FUN_00411cc0(ulong uParm1,ulong uParm2,char cParm3);
ulong FUN_00411d30(long lParm1);
void FUN_00411da0(undefined4 *puParm1,int iParm2);
void FUN_00411dc0(long lParm1,long lParm2,long lParm3);
undefined8 FUN_00411e60(long *plParm1,ulong *puParm2,long lParm3);
ulong FUN_00411ed0(ulong uParm1,ulong *puParm2);
void FUN_00411f30(uint uParm1,uint uParm2,undefined8 uParm3,ulong uParm4);
ulong FUN_00411f80(long lParm1,long lParm2,uint uParm3,char *pcParm4);
void FUN_00412160(ulong uParm1,long **pplParm2);
void FUN_00412190(undefined8 *puParm1,long lParm2);
undefined8 FUN_00412220(ulong uParm1,undefined8 *puParm2,undefined8 *puParm3);
long ** FUN_004122c0(ulong uParm1,long **pplParm2,long lParm3);
long FUN_00412350(long *plParm1,int iParm2);
void FUN_00412bf0(long lParm1);
long * FUN_00412c00(long *plParm1,uint uParm2,long lParm3);
ulong FUN_00412f90(long *plParm1);
undefined8 * FUN_00413130(undefined8 *puParm1,undefined8 *puParm2);
undefined8 FUN_00413810(undefined8 uParm1,long lParm2,uint uParm3);
long FUN_00413840(long lParm1,ulong uParm2);
void FUN_00413cc0(long lParm1,int *piParm2);
ulong FUN_00413f10(int param_1,undefined8 *param_2,char *param_3,char **param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_00414680(char *pcParm1,int *piParm2,int iParm3);
ulong FUN_00414740(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,uint *param_7,uint param_8);
void FUN_00414bf0(void);
void FUN_00414c50(void);
undefined8 FUN_00414c70(long lParm1,uint uParm2,uint uParm3);
ulong FUN_00414d50(uint uParm1,char *pcParm2,uint uParm3,undefined8 uParm4);
ulong FUN_00414ea0(ulong uParm1,long lParm2,ulong uParm3,long lParm4,uint uParm5);
void FUN_00415010(void);
undefined8 FUN_00415030(long lParm1,long lParm2);
void FUN_004150a0(void);
ulong FUN_004150c0(uint *puParm1,byte *pbParm2,long lParm3);
ulong FUN_00415130(long lParm1,ulong uParm2);
undefined8 FUN_00415230(long lParm1,ulong uParm2);
undefined8 FUN_00415290(long lParm1,uint uParm2,long lParm3);
void FUN_00415330(void);
ulong FUN_00415340(uint param_1,undefined8 param_2,uint param_3,ulong param_4);
undefined8 FUN_00415440(long lParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004154b0(long lParm1,long lParm2);
ulong FUN_004154f0(long lParm1,long lParm2);
ulong FUN_00415a10(uint uParm1,long lParm2,uint uParm3,long lParm4);
ulong FUN_00415a20(long lParm1);
void FUN_00415ab0(void);
undefined8 FUN_00415ad0(long lParm1,long lParm2);
undefined8 FUN_00415b30(undefined8 uParm1,ulong uParm2,long lParm3);
undefined8 FUN_00415ba0(long lParm1);
undefined8 FUN_00415c70(uint uParm1,long lParm2,uint uParm3);
ulong FUN_00415d60(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_00415e20(undefined8 uParm1,undefined8 uParm2,undefined4 uParm3,undefined4 *puParm4);
void FUN_00415e30(undefined8 uParm1,int iParm2,uint uParm3);
ulong FUN_00415e50(uint *puParm1,undefined8 uParm2,uint uParm3);
undefined * FUN_00415e80(uint uParm1,undefined8 uParm2);
long FUN_00415eb0(undefined8 uParm1,long *plParm2,long lParm3,long lParm4);
void FUN_00416030(undefined auParm1 [16],undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined auParm8 [16],undefined8 uParm9,undefined8 uParm10,long lParm11,undefined8 uParm12,undefined8 uParm13,undefined8 uParm14);
void FUN_00416090(long *plParm1,long lParm2,long lParm3);
long FUN_00416170(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
undefined FUN_00416200(int iParm1);;
ulong FUN_00416210(uint uParm1);
ulong FUN_00416220(byte *pbParm1,byte *pbParm2);
ulong FUN_00416430(ulong uParm1,char cParm2);
ulong FUN_00416510(ulong uParm1);
void FUN_00416520(long lParm1);
undefined8 FUN_00416530(long *plParm1,long *plParm2);
undefined8 FUN_004165e0(void);
void FUN_004165f0(undefined8 *puParm1);
ulong FUN_00416630(ulong uParm1);
ulong FUN_004166d0(char *pcParm1,ulong uParm2);
void FUN_00416710(undefined4 *puParm1,undefined4 uParm2);
ulong FUN_00416730(long lParm1);
ulong FUN_00416740(long lParm1,uint uParm2);
ulong FUN_00416780(ulong uParm1,undefined8 *puParm2);
undefined * FUN_004167d0(void);
char * FUN_00416b30(void);
void FUN_00416bf0(void);
long FUN_00416c40(long lParm1);
void FUN_00416c50(undefined8 uParm1);
long * FUN_00416c70(void);
ulong FUN_00416ca0(undefined8 *puParm1,ulong uParm2);
void FUN_00416eb0(undefined8 uParm1);
ulong FUN_00416ed0(undefined8 *puParm1);
void FUN_00416f00(undefined8 uParm1,undefined8 uParm2);
void FUN_00417120(undefined4 *puParm1,ulong uParm2);
undefined8 * FUN_00417330(long lParm1,ulong uParm2);
void FUN_004173e0(long *plParm1,ulong uParm2,ulong uParm3);
undefined8 FUN_00417400(long *plParm1);
undefined8 FUN_00417450(undefined8 uParm1);
undefined8 FUN_00417460(long lParm1,undefined8 uParm2);
void FUN_00417470(long *plParm1,long *plParm2);
void FUN_00417750(ulong *puParm1);
ulong FUN_004179f0(ulong uParm1);
undefined8 FUN_00417a00(long lParm1,uint uParm2,uint uParm3);
void FUN_00417b30(undefined8 uParm1,undefined8 uParm2);
void FUN_00417b50(undefined8 uParm1);
void FUN_00417be0(void);
ulong FUN_00417d30(char *pcParm1,long lParm2);
char * FUN_00417d70(ulong uParm1,long lParm2,long lParm3);
ulong FUN_00417fb0(void);
ulong FUN_00417ff0(undefined8 param_1,undefined8 *param_2);
uint * FUN_00418260(int *param_1,ulong *param_2);
ulong FUN_00418450(undefined8 uParm1,undefined8 *puParm2);
uint * FUN_00418660(undefined8 uParm1,int *piParm2,ulong *puParm3);
undefined4 *FUN_004187d0(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5,undefined8 *puParm6);
int * FUN_00418910(undefined8 *puParm1,long lParm2,undefined8 *puParm3,undefined8 *puParm4,uint **ppuParm5,int **ppiParm6);
undefined8 FUN_00418ec0(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_00419470(uint param_1);
void FUN_004194b0(uint uParm1);
long FUN_0041e7e0(undefined8 uParm1,undefined8 uParm2);
double FUN_0041e880(int *piParm1);
void FUN_0041e8d0(int *param_1);
long FUN_0041e950(ulong uParm1,ulong uParm2);
long FUN_0041e970(void);
undefined8 FUN_0041e9a0(uint *puParm1,ulong *puParm2);
undefined8 FUN_0041ebc0(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_0041f8b0(undefined auParm1 [16]);
ulong FUN_0041f8d0(void);
void FUN_0041f900(void);
undefined8 _DT_FINI(void);

