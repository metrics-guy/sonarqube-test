typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_compute_number_width_ret_type;
struct bb_compute_number_width_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_r8(void);
struct bb_compute_number_width_ret_type bb_compute_number_width(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t r8_1;
    uint32_t var_1;
    uint64_t var_2;
    uint32_t var_3;
    uint64_t rdx_0;
    uint64_t rsi5_0;
    uint64_t r8_0;
    uint64_t rdx_1;
    uint64_t var_4;
    uint64_t rcx_1;
    uint64_t rcx_0;
    uint64_t rdx_2;
    uint64_t var_5;
    struct bb_compute_number_width_ret_type mrv;
    struct bb_compute_number_width_ret_type mrv1;
    struct bb_compute_number_width_ret_type mrv3 = {1UL, /*implicit*/(int)0};
    revng_init_local_sp(0UL);
    var_0 = init_r8();
    rdx_0 = 0UL;
    rsi5_0 = rsi;
    r8_0 = 1UL;
    rcx_1 = 1UL;
    rcx_0 = 1UL;
    if (rdi == 0UL) {
        mrv3.field_1 = var_0;
        return mrv3;
    }
    var_1 = *(uint32_t *)rsi;
    var_3 = var_1;
    if ((int)var_1 > (int)0U) {
        return;
    }
    var_2 = (rdi * 152UL) + rsi;
    while (1U)
        {
            rdx_1 = rdx_0;
            r8_1 = r8_0;
            r8_1 = 7UL;
            if (var_3 != 0U & (uint32_t)((uint16_t)*(uint32_t *)(rsi5_0 + 32UL) & (unsigned short)61440U) == 32768U) {
                rdx_1 = rdx_0 + *(uint64_t *)(rsi5_0 + 56UL);
                r8_1 = r8_0;
            }
            var_4 = rsi5_0 + 152UL;
            rdx_0 = rdx_1;
            rsi5_0 = var_4;
            r8_0 = r8_1;
            rdx_2 = rdx_1;
            if (var_2 == var_4) {
                break;
            }
            var_3 = *(uint32_t *)var_4;
            continue;
        }
    if (rdx_1 <= 9UL) {
        var_5 = (uint64_t)((uint32_t)rcx_0 + 1U);
        rcx_0 = var_5;
        rcx_1 = var_5;
        while (rdx_2 <= 99UL)
            {
                rdx_2 = (uint64_t)(((unsigned __int128)rdx_2 * 14757395258967641293ULL) >> 67ULL);
                var_5 = (uint64_t)((uint32_t)rcx_0 + 1U);
                rcx_0 = var_5;
                rcx_1 = var_5;
            }
    }
    mrv.field_0 = (uint64_t)(uint32_t)(((long)(rcx_1 << 32UL) < (long)(r8_1 << 32UL)) ? r8_1 : rcx_1);
    mrv1 = mrv;
    mrv1.field_1 = r8_1;
    return mrv1;
}
