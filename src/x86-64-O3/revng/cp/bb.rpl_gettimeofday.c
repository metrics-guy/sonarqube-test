typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct helper_punpckldq_xmm_wrapper_323_ret_type;
struct type_3;
struct type_5;
struct helper_punpcklqdq_xmm_wrapper_ret_type;
struct helper_movl_mm_T0_xmm_wrapper_ret_type;
struct helper_movl_mm_T0_xmm_wrapper_316_ret_type;
struct helper_movl_mm_T0_xmm_wrapper_317_ret_type;
struct helper_movl_mm_T0_xmm_wrapper_318_ret_type;
struct helper_movl_mm_T0_xmm_wrapper_319_ret_type;
struct helper_movl_mm_T0_xmm_wrapper_320_ret_type;
struct helper_punpckldq_xmm_wrapper_ret_type;
struct helper_punpckldq_xmm_wrapper_321_ret_type;
struct helper_punpckldq_xmm_wrapper_322_ret_type;
struct helper_punpckldq_xmm_wrapper_323_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
};
struct type_3 {
};
struct type_5 {
};
struct helper_punpcklqdq_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
};
struct helper_movl_mm_T0_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_movl_mm_T0_xmm_wrapper_316_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_movl_mm_T0_xmm_wrapper_317_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_movl_mm_T0_xmm_wrapper_318_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_movl_mm_T0_xmm_wrapper_319_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_movl_mm_T0_xmm_wrapper_320_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_punpckldq_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
};
struct helper_punpckldq_xmm_wrapper_321_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
};
struct helper_punpckldq_xmm_wrapper_322_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern struct helper_punpckldq_xmm_wrapper_323_ret_type helper_punpckldq_xmm_wrapper_323(struct type_3 *param_0, struct type_5 *param_1, struct type_5 *param_2, uint64_t param_3, uint64_t param_4);
extern struct helper_punpcklqdq_xmm_wrapper_ret_type helper_punpcklqdq_xmm_wrapper(struct type_3 *param_0, struct type_5 *param_1, struct type_5 *param_2, uint64_t param_3, uint64_t param_4);
extern struct helper_movl_mm_T0_xmm_wrapper_ret_type helper_movl_mm_T0_xmm_wrapper(struct type_5 *param_0, uint32_t param_1);
extern struct helper_movl_mm_T0_xmm_wrapper_316_ret_type helper_movl_mm_T0_xmm_wrapper_316(struct type_5 *param_0, uint32_t param_1);
extern struct helper_movl_mm_T0_xmm_wrapper_317_ret_type helper_movl_mm_T0_xmm_wrapper_317(struct type_5 *param_0, uint32_t param_1);
extern struct helper_movl_mm_T0_xmm_wrapper_318_ret_type helper_movl_mm_T0_xmm_wrapper_318(struct type_5 *param_0, uint32_t param_1);
extern struct helper_movl_mm_T0_xmm_wrapper_319_ret_type helper_movl_mm_T0_xmm_wrapper_319(struct type_5 *param_0, uint32_t param_1);
extern struct helper_movl_mm_T0_xmm_wrapper_320_ret_type helper_movl_mm_T0_xmm_wrapper_320(struct type_5 *param_0, uint32_t param_1);
extern struct helper_punpckldq_xmm_wrapper_ret_type helper_punpckldq_xmm_wrapper(struct type_3 *param_0, struct type_5 *param_1, struct type_5 *param_2, uint64_t param_3, uint64_t param_4);
extern struct helper_punpckldq_xmm_wrapper_321_ret_type helper_punpckldq_xmm_wrapper_321(struct type_3 *param_0, struct type_5 *param_1, struct type_5 *param_2, uint64_t param_3, uint64_t param_4);
extern struct helper_punpckldq_xmm_wrapper_322_ret_type helper_punpckldq_xmm_wrapper_322(struct type_3 *param_0, struct type_5 *param_1, struct type_5 *param_2, uint64_t param_3, uint64_t param_4);
void bb_rpl_gettimeofday(void) {
    struct helper_punpckldq_xmm_wrapper_323_ret_type var_47;
    uint64_t var_48;
    struct helper_punpcklqdq_xmm_wrapper_ret_type var_49;
    uint64_t var_50;
    struct helper_punpcklqdq_xmm_wrapper_ret_type var_38;
    struct helper_punpckldq_xmm_wrapper_321_ret_type var_37;
    struct helper_punpckldq_xmm_wrapper_ret_type var_35;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint32_t var_8;
    uint32_t var_9;
    uint32_t var_10;
    uint32_t var_11;
    uint32_t var_12;
    uint32_t var_13;
    uint32_t *var_14;
    uint32_t var_15;
    uint32_t var_16;
    uint32_t *var_17;
    uint32_t var_18;
    uint64_t var_19;
    uint32_t *var_20;
    uint64_t var_21;
    struct helper_movl_mm_T0_xmm_wrapper_ret_type var_22;
    uint64_t var_23;
    struct helper_movl_mm_T0_xmm_wrapper_316_ret_type var_24;
    uint64_t var_25;
    struct helper_movl_mm_T0_xmm_wrapper_317_ret_type var_26;
    uint64_t var_27;
    struct helper_movl_mm_T0_xmm_wrapper_318_ret_type var_28;
    uint64_t var_29;
    struct helper_movl_mm_T0_xmm_wrapper_319_ret_type var_30;
    uint64_t var_31;
    struct helper_movl_mm_T0_xmm_wrapper_320_ret_type var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_36;
    uint64_t var_39;
    uint64_t var_40;
    struct helper_movl_mm_T0_xmm_wrapper_ret_type var_41;
    uint64_t var_42;
    struct helper_movl_mm_T0_xmm_wrapper_316_ret_type var_43;
    uint64_t var_44;
    struct helper_punpckldq_xmm_wrapper_322_ret_type var_45;
    uint64_t var_46;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r15();
    var_3 = init_r14();
    var_4 = init_r12();
    var_5 = init_rbx();
    var_6 = init_r13();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_6;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_1;
    *(uint64_t *)(var_0 + (-48L)) = var_5;
    var_7 = *(uint64_t *)4363456UL;
    var_8 = *(uint32_t *)(var_7 + 8UL);
    var_9 = *(uint32_t *)(var_7 + 16UL);
    var_10 = *(uint32_t *)var_7;
    var_11 = *(uint32_t *)(var_7 + 20UL);
    var_12 = *(uint32_t *)(var_7 + 24UL);
    var_13 = *(uint32_t *)(var_7 + 4UL);
    var_14 = (uint32_t *)(var_0 + (-72L));
    *var_14 = var_8;
    var_15 = *(uint32_t *)(var_7 + 12UL);
    var_16 = *(uint32_t *)(var_7 + 28UL);
    var_17 = (uint32_t *)(var_0 + (-68L));
    *var_17 = var_9;
    var_18 = *(uint32_t *)(var_7 + 32UL);
    var_19 = *(uint64_t *)(var_7 + 40UL);
    var_20 = (uint32_t *)(var_0 + (-76L));
    *var_20 = var_10;
    var_21 = *(uint64_t *)(var_7 + 48UL);
    *(uint32_t *)(var_0 + (-64L)) = var_11;
    *(uint32_t *)(var_0 + (-60L)) = var_12;
    *(uint64_t *)(var_0 + (-96L)) = 4282232UL;
    indirect_placeholder_1();
    var_22 = helper_movl_mm_T0_xmm_wrapper((struct type_5 *)(840UL), *(uint32_t *)(var_0 + (-80L)));
    var_23 = var_22.field_0;
    var_24 = helper_movl_mm_T0_xmm_wrapper_316((struct type_5 *)(776UL), *(uint32_t *)(var_0 + (-84L)));
    var_25 = var_24.field_0;
    var_26 = helper_movl_mm_T0_xmm_wrapper_317((struct type_5 *)(904UL), var_15);
    var_27 = var_26.field_0;
    var_28 = helper_movl_mm_T0_xmm_wrapper_318((struct type_5 *)(968UL), var_13);
    var_29 = var_28.field_0;
    var_30 = helper_movl_mm_T0_xmm_wrapper_319((struct type_5 *)(1096UL), *var_14);
    var_31 = var_30.field_0;
    var_32 = helper_movl_mm_T0_xmm_wrapper_320((struct type_5 *)(1032UL), var_16);
    var_33 = var_32.field_0;
    var_34 = *(uint64_t *)4363456UL;
    var_35 = helper_punpckldq_xmm_wrapper((struct type_3 *)(0UL), (struct type_5 *)(840UL), (struct type_5 *)(904UL), var_23, var_27);
    var_36 = var_35.field_0;
    var_37 = helper_punpckldq_xmm_wrapper_321((struct type_3 *)(0UL), (struct type_5 *)(776UL), (struct type_5 *)(968UL), var_25, var_29);
    var_38 = helper_punpcklqdq_xmm_wrapper((struct type_3 *)(0UL), (struct type_5 *)(776UL), (struct type_5 *)(840UL), var_37.field_0, var_36);
    var_39 = var_38.field_0;
    var_40 = var_38.field_1;
    var_41 = helper_movl_mm_T0_xmm_wrapper((struct type_5 *)(840UL), *var_17);
    var_42 = var_41.field_0;
    *(uint32_t *)(var_34 + 32UL) = var_18;
    *(uint64_t *)var_34 = var_39;
    *(uint64_t *)(var_34 + 8UL) = var_40;
    var_43 = helper_movl_mm_T0_xmm_wrapper_316((struct type_5 *)(776UL), *var_20);
    var_44 = var_43.field_0;
    var_45 = helper_punpckldq_xmm_wrapper_322((struct type_3 *)(0UL), (struct type_5 *)(840UL), (struct type_5 *)(1032UL), var_42, var_33);
    var_46 = var_45.field_0;
    *(uint64_t *)(var_34 + 40UL) = var_19;
    var_47 = helper_punpckldq_xmm_wrapper_323((struct type_3 *)(0UL), (struct type_5 *)(776UL), (struct type_5 *)(1096UL), var_44, var_31);
    var_48 = var_47.field_0;
    *(uint64_t *)(var_34 + 48UL) = var_21;
    var_49 = helper_punpcklqdq_xmm_wrapper((struct type_3 *)(0UL), (struct type_5 *)(776UL), (struct type_5 *)(840UL), var_48, var_46);
    var_50 = var_49.field_1;
    *(uint64_t *)(var_34 + 16UL) = var_49.field_0;
    *(uint64_t *)(var_34 + 24UL) = var_50;
    return;
}
