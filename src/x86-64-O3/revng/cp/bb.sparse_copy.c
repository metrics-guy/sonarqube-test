typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_7_ret_type;
struct indirect_placeholder_9_ret_type;
struct indirect_placeholder_8_ret_type;
struct indirect_placeholder_7_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_9_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_8_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_7_ret_type indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_9_ret_type indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_8_ret_type indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
uint64_t bb_sparse_copy(uint64_t rcx, uint64_t rdx, uint64_t r9, uint64_t rdi, uint64_t rsi, uint64_t r8) {
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_0;
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t r15_3;
    uint64_t rbp_1;
    uint64_t r12_5;
    uint64_t r12_4;
    uint64_t rbp_0;
    uint64_t r10_0;
    unsigned char r93_0_in;
    uint64_t local_sp_0;
    uint64_t r13_0;
    uint64_t r93_0;
    bool var_70;
    bool var_71;
    uint64_t *_pre_phi276;
    uint64_t rbp_6;
    uint64_t *_pre275;
    uint64_t rbp_4;
    uint64_t *var_72;
    uint64_t var_73;
    uint64_t rbp_3;
    uint64_t r10_1;
    unsigned char r93_1_in;
    uint64_t local_sp_1;
    uint64_t r13_1;
    uint64_t r93_1;
    uint64_t r13_5;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t local_sp_5_ph;
    uint64_t var_19;
    uint64_t local_sp_5;
    uint64_t var_23;
    uint64_t rax_0;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t *var_27;
    uint64_t rbp_6_ph;
    uint64_t var_14;
    uint64_t rbp_5_ph;
    uint64_t r12_3;
    uint64_t rbp_8;
    unsigned char _pre_phi;
    unsigned char _pre;
    uint64_t r93_6;
    uint64_t *var_28;
    uint64_t rbp_7;
    uint64_t rbp_2;
    uint64_t rbx_0;
    uint64_t r10_2;
    uint64_t local_sp_7;
    uint64_t r93_2;
    uint64_t r13_3;
    uint64_t local_sp_2;
    uint64_t r10_4;
    uint64_t var_45;
    uint64_t r12_3_ph;
    uint64_t var_48;
    uint64_t r14_2_ph_pn;
    uint64_t r12_0;
    uint64_t r10_1_pn;
    uint64_t r93_3;
    uint64_t local_sp_3;
    uint64_t r13_2;
    uint64_t r14_0;
    uint64_t r12_1;
    uint64_t r93_4;
    uint64_t local_sp_4;
    uint64_t var_74;
    uint64_t *var_75;
    uint64_t var_76;
    uint64_t var_77;
    bool var_78;
    uint64_t var_79;
    uint64_t r15_1_ph;
    uint64_t r12_2_ph;
    uint64_t r93_5_ph;
    uint64_t *var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t r15_2;
    uint64_t r15_4_ph;
    uint64_t r14_2_ph;
    uint64_t r10_4_ph;
    uint64_t rbx_0_ph;
    uint64_t r93_6_ph;
    uint64_t local_sp_7_ph;
    uint64_t r13_3_ph;
    uint64_t r15_4;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    bool var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t r10_5;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint32_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    bool brmerge;
    bool var_42;
    unsigned char var_43;
    uint64_t var_44;
    uint64_t local_sp_8;
    uint64_t var_49;
    uint64_t rbp_9;
    uint64_t rbp_10;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t r13_4;
    uint64_t r10_6;
    uint64_t r93_7;
    uint64_t local_sp_9;
    uint64_t *var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t *var_61;
    uint64_t var_62;
    uint64_t var_63;
    unsigned char var_64;
    uint64_t var_65;
    uint64_t r10_7;
    uint64_t r93_8;
    uint64_t local_sp_10;
    uint64_t r13_6;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    unsigned char var_56;
    uint64_t var_57;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r15();
    var_3 = init_r14();
    var_4 = init_r12();
    var_5 = init_rbx();
    var_6 = init_cc_src2();
    var_7 = init_r13();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_7;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_1;
    *(uint64_t *)(var_0 + (-48L)) = var_5;
    var_8 = *(uint64_t *)(var_0 | 8UL);
    *(uint32_t *)(var_0 + (-144L)) = (uint32_t)rsi;
    var_9 = (uint64_t *)(var_0 + (-128L));
    *var_9 = rcx;
    var_10 = *(uint64_t *)(var_0 | 40UL);
    *(uint64_t *)(var_0 + (-80L)) = var_8;
    var_11 = *(uint64_t *)(var_0 | 16UL);
    var_12 = *(uint64_t *)(var_0 | 32UL);
    *(unsigned char *)var_10 = (unsigned char)'\x00';
    *(uint64_t *)(var_0 + (-152L)) = var_11;
    var_13 = *(uint64_t *)(var_0 | 24UL);
    *(uint32_t *)(var_0 + (-64L)) = (uint32_t)rdi;
    *(uint64_t *)(var_0 + (-136L)) = rdx;
    *(uint64_t *)(var_0 + (-104L)) = r8;
    *(uint32_t *)(var_0 + (-60L)) = (uint32_t)r9;
    *(uint64_t *)(var_0 + (-120L)) = var_13;
    *(uint64_t *)(var_0 + (-96L)) = var_12;
    *(uint64_t *)(var_0 + (-72L)) = var_10;
    *(unsigned char *)(var_0 + (-137L)) = (unsigned char)r9;
    *(uint64_t *)var_12 = 0UL;
    r15_3 = 1UL;
    r13_5 = 1UL;
    rbp_5_ph = var_1;
    rbp_8 = 0UL;
    rbp_7 = 0UL;
    r12_1 = 0UL;
    r15_1_ph = 0UL;
    r12_2_ph = 0UL;
    r93_5_ph = r9;
    r13_4 = 1UL;
    r93_7 = 0UL;
    r93_8 = 0UL;
    r13_6 = 1UL;
    if (var_13 == 0UL) {
        return (uint64_t)(uint32_t)r15_3;
    }
    var_14 = var_0 + (-184L);
    *(uint64_t *)(var_0 + (-88L)) = ((r8 == 0UL) ? *var_9 : r8);
    local_sp_5_ph = var_14;
    r15_3 = 0UL;
    while (1U)
        {
            local_sp_5 = local_sp_5_ph;
            rbp_6_ph = rbp_5_ph;
            r12_3_ph = r12_2_ph;
            r15_2 = r15_1_ph;
            r15_4_ph = r15_1_ph;
            r93_6_ph = r93_5_ph;
            while (1U)
                {
                    var_15 = (uint64_t *)(local_sp_5 + 64UL);
                    var_16 = *var_15;
                    var_17 = *(uint64_t *)(local_sp_5 + 56UL);
                    var_18 = local_sp_5 + (-8L);
                    *(uint64_t *)var_18 = 4218303UL;
                    indirect_placeholder_1();
                    *var_15 = var_16;
                    rax_0 = var_16;
                    r14_2_ph = var_16;
                    local_sp_7_ph = var_18;
                    r13_3_ph = var_16;
                    if ((long)var_16 <= (long)18446744073709551615UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_19 = local_sp_5 + (-16L);
                    *(uint64_t *)var_19 = 4218318UL;
                    indirect_placeholder_1();
                    local_sp_5 = var_19;
                    if (*(uint32_t *)var_16 == 4U) {
                        continue;
                    }
                    var_20 = *(uint64_t *)(local_sp_5 + 88UL);
                    *(uint64_t *)(local_sp_5 + (-24L)) = 4218341UL;
                    var_21 = indirect_placeholder(4UL, var_20);
                    *(uint64_t *)(local_sp_5 + (-32L)) = 4218349UL;
                    indirect_placeholder_1();
                    var_22 = (uint64_t)*(uint32_t *)var_21;
                    *(uint64_t *)(local_sp_5 + (-40L)) = 4218368UL;
                    indirect_placeholder_9(0UL, var_21, 4325591UL, r93_5_ph, 0UL, var_22, r8);
                    loop_state_var = 1U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    var_23 = helper_cc_compute_all_wrapper(var_16, var_17, var_6, 25U);
                    r15_3 = 1UL;
                    if ((var_23 & 64UL) != 0UL) {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    var_24 = *(uint64_t *)(local_sp_5 + 40UL);
                    var_25 = *(uint64_t *)(local_sp_5 + 80UL);
                    var_26 = *(uint64_t *)(local_sp_5 + 88UL);
                    *(uint64_t *)(local_sp_5 + 8UL) = var_24;
                    var_27 = (uint64_t *)var_25;
                    *var_27 = (*var_27 + var_16);
                    *(unsigned char *)(local_sp_5 + 38UL) = (*(uint64_t *)(local_sp_5 + 72UL) != 0UL);
                    r10_4_ph = var_24;
                    rbx_0_ph = var_26;
                    while (1U)
                        {
                            rbp_6 = rbp_6_ph;
                            r12_3 = r12_3_ph;
                            r93_6 = r93_6_ph;
                            rbx_0 = rbx_0_ph;
                            local_sp_7 = local_sp_7_ph;
                            r13_3 = r13_3_ph;
                            r10_4 = r10_4_ph;
                            r14_2_ph_pn = r14_2_ph;
                            r15_4 = r15_4_ph;
                            while (1U)
                                {
                                    var_28 = (uint64_t *)local_sp_7;
                                    *var_28 = r14_2_ph;
                                    var_29 = (rbx_0 > r14_2_ph) ? r14_2_ph : rbx_0;
                                    var_30 = (var_29 != 0UL);
                                    var_31 = rbp_6 & (-256L);
                                    var_32 = (r14_2_ph <= rbx_0);
                                    var_33 = (r13_3 & (-256L)) | var_32;
                                    var_34 = var_30 & (uint64_t)*(unsigned char *)(local_sp_7 + 46UL);
                                    var_35 = var_31 | var_34;
                                    r15_3 = r15_4;
                                    r12_5 = r12_3;
                                    r12_4 = r12_3;
                                    r12_3 = var_29;
                                    rbx_0 = 0UL;
                                    r10_2 = r10_4;
                                    r93_2 = r93_6;
                                    local_sp_2 = local_sp_7;
                                    r12_0 = var_29;
                                    r13_2 = var_33;
                                    rbx_0_ph = var_29;
                                    r10_5 = r10_4;
                                    local_sp_8 = local_sp_7;
                                    if (var_34 == 0UL) {
                                        var_43 = (unsigned char)r15_4;
                                        _pre_phi = var_43;
                                        rbp_7 = var_35;
                                        if (!(((uint64_t)(var_43 + '\xff') == 0UL) || (var_32 ^ 1))) {
                                            var_44 = (uint64_t)(uint32_t)r15_4;
                                            rbp_2 = var_44;
                                            rbp_7 = var_44;
                                            if (var_29 == 0UL) {
                                                loop_state_var = 0U;
                                                break;
                                            }
                                        }
                                        var_49 = r12_3 + var_29;
                                        r12_5 = var_49;
                                        r12_4 = var_49;
                                        rbp_9 = rbp_7;
                                        rbp_10 = rbp_7;
                                        r10_6 = r10_5;
                                        local_sp_9 = local_sp_8;
                                        r10_7 = r10_5;
                                        local_sp_10 = local_sp_8;
                                        if ((uint64_t)_pre_phi != 0UL) {
                                            var_58 = (uint64_t *)(local_sp_9 + 16UL);
                                            var_59 = *var_58;
                                            var_60 = (uint64_t)*(uint32_t *)(local_sp_9 + 40UL);
                                            var_61 = (uint64_t *)(local_sp_9 + 24UL);
                                            *var_61 = r10_6;
                                            *(unsigned char *)(local_sp_9 + 8UL) = (unsigned char)r93_7;
                                            var_62 = local_sp_9 + (-8L);
                                            *(uint64_t *)var_62 = 4218594UL;
                                            var_63 = indirect_placeholder_2(r12_4, var_60, var_59);
                                            var_64 = *(unsigned char *)local_sp_9;
                                            var_65 = *var_58;
                                            rbp_1 = rbp_9;
                                            rbp_0 = rbp_9;
                                            r10_0 = var_65;
                                            r93_0_in = var_64;
                                            local_sp_0 = var_62;
                                            r13_0 = r13_5;
                                            r10_1 = var_65;
                                            r93_1_in = var_64;
                                            local_sp_1 = var_62;
                                            r13_1 = r13_5;
                                            if (r12_4 != var_63) {
                                                if ((uint64_t)(uint32_t)r13_5 == 0UL) {
                                                    loop_state_var = 1U;
                                                    break;
                                                }
                                                r93_0 = (uint64_t)r93_0_in;
                                                var_70 = (var_29 == 0UL);
                                                var_71 = (r93_0_in == '\x00');
                                                rbp_6 = rbp_0;
                                                rbp_4 = rbp_0;
                                                rbp_3 = rbp_0;
                                                r93_6 = r93_0;
                                                local_sp_7 = local_sp_0;
                                                r13_3 = r13_0;
                                                r10_4 = r10_0;
                                                r12_0 = 0UL;
                                                r10_1_pn = r10_0;
                                                r93_3 = r93_0;
                                                local_sp_3 = local_sp_0;
                                                r13_2 = r13_0;
                                                r93_4 = r93_0;
                                                local_sp_4 = local_sp_0;
                                                if (var_70) {
                                                    loop_state_var = 2U;
                                                    break;
                                                }
                                                if (var_71) {
                                                    loop_state_var = 3U;
                                                    break;
                                                }
                                                *(uint64_t *)(local_sp_0 + 16UL) = r10_0;
                                                r15_4 = (uint64_t)(uint32_t)rbp_0;
                                                continue;
                                            }
                                            var_66 = (uint64_t)var_64;
                                            var_67 = *var_61;
                                            *(uint64_t *)(local_sp_9 + (-16L)) = 4219001UL;
                                            var_68 = indirect_placeholder(4UL, var_67);
                                            *(uint64_t *)(local_sp_9 + (-24L)) = 4219009UL;
                                            indirect_placeholder_1();
                                            var_69 = (uint64_t)*(uint32_t *)var_68;
                                            *(uint64_t *)(local_sp_9 + (-32L)) = 4219028UL;
                                            indirect_placeholder_7(0UL, var_68, 4325608UL, var_66, 0UL, var_69, r8);
                                            loop_state_var = 4U;
                                            break;
                                        }
                                    }
                                    *(uint64_t *)(local_sp_7 + 8UL) = r10_4;
                                    var_36 = local_sp_7 + (-8L);
                                    *(uint64_t *)var_36 = 4218500UL;
                                    var_37 = indirect_placeholder(r10_4, var_29);
                                    var_38 = *var_28;
                                    var_39 = (uint32_t)var_37;
                                    var_40 = (uint64_t)var_39;
                                    var_41 = (uint64_t)((var_39 ^ (uint32_t)r15_4) & (-255)) & ((var_37 & (-256L)) | (r12_3 != 0UL));
                                    brmerge = (((uint64_t)((unsigned char)var_37 + '\xff') == 0UL) || (var_32 ^ 1));
                                    var_42 = ((var_41 & 1UL) == 0UL);
                                    rbp_2 = var_40;
                                    r10_2 = var_38;
                                    r93_2 = var_41;
                                    local_sp_2 = var_36;
                                    r10_5 = var_38;
                                    local_sp_8 = var_36;
                                    r10_6 = var_38;
                                    r93_7 = var_41;
                                    local_sp_9 = var_36;
                                    r10_7 = var_38;
                                    r93_8 = var_41;
                                    local_sp_10 = var_36;
                                    if (brmerge) {
                                        rbp_8 = var_40;
                                        r13_4 = 0UL;
                                        if (var_42) {
                                            loop_state_var = 0U;
                                            break;
                                        }
                                        rbp_9 = rbp_8;
                                        r13_5 = r13_4;
                                        rbp_10 = rbp_8;
                                        r13_6 = r13_4;
                                        if ((uint64_t)(unsigned char)r15_4 == 0UL) {
                                            var_58 = (uint64_t *)(local_sp_9 + 16UL);
                                            var_59 = *var_58;
                                            var_60 = (uint64_t)*(uint32_t *)(local_sp_9 + 40UL);
                                            var_61 = (uint64_t *)(local_sp_9 + 24UL);
                                            *var_61 = r10_6;
                                            *(unsigned char *)(local_sp_9 + 8UL) = (unsigned char)r93_7;
                                            var_62 = local_sp_9 + (-8L);
                                            *(uint64_t *)var_62 = 4218594UL;
                                            var_63 = indirect_placeholder_2(r12_4, var_60, var_59);
                                            var_64 = *(unsigned char *)local_sp_9;
                                            var_65 = *var_58;
                                            rbp_1 = rbp_9;
                                            rbp_0 = rbp_9;
                                            r10_0 = var_65;
                                            r93_0_in = var_64;
                                            local_sp_0 = var_62;
                                            r13_0 = r13_5;
                                            r10_1 = var_65;
                                            r93_1_in = var_64;
                                            local_sp_1 = var_62;
                                            r13_1 = r13_5;
                                            if (r12_4 != var_63) {
                                                if ((uint64_t)(uint32_t)r13_5 == 0UL) {
                                                    loop_state_var = 1U;
                                                    break;
                                                }
                                                r93_0 = (uint64_t)r93_0_in;
                                                var_70 = (var_29 == 0UL);
                                                var_71 = (r93_0_in == '\x00');
                                                rbp_6 = rbp_0;
                                                rbp_4 = rbp_0;
                                                rbp_3 = rbp_0;
                                                r93_6 = r93_0;
                                                local_sp_7 = local_sp_0;
                                                r13_3 = r13_0;
                                                r10_4 = r10_0;
                                                r12_0 = 0UL;
                                                r10_1_pn = r10_0;
                                                r93_3 = r93_0;
                                                local_sp_3 = local_sp_0;
                                                r13_2 = r13_0;
                                                r93_4 = r93_0;
                                                local_sp_4 = local_sp_0;
                                                if (var_70) {
                                                    loop_state_var = 2U;
                                                    break;
                                                }
                                                if (var_71) {
                                                    loop_state_var = 3U;
                                                    break;
                                                }
                                                *(uint64_t *)(local_sp_0 + 16UL) = r10_0;
                                                r15_4 = (uint64_t)(uint32_t)rbp_0;
                                                continue;
                                            }
                                            var_66 = (uint64_t)var_64;
                                            var_67 = *var_61;
                                            *(uint64_t *)(local_sp_9 + (-16L)) = 4219001UL;
                                            var_68 = indirect_placeholder(4UL, var_67);
                                            *(uint64_t *)(local_sp_9 + (-24L)) = 4219009UL;
                                            indirect_placeholder_1();
                                            var_69 = (uint64_t)*(uint32_t *)var_68;
                                            *(uint64_t *)(local_sp_9 + (-32L)) = 4219028UL;
                                            indirect_placeholder_7(0UL, var_68, 4325608UL, var_66, 0UL, var_69, r8);
                                            loop_state_var = 4U;
                                            break;
                                        }
                                    }
                                    if (var_42) {
                                        _pre = (unsigned char)r15_4;
                                        _pre_phi = _pre;
                                        var_49 = r12_3 + var_29;
                                        r12_5 = var_49;
                                        r12_4 = var_49;
                                        rbp_9 = rbp_7;
                                        rbp_10 = rbp_7;
                                        r10_6 = r10_5;
                                        local_sp_9 = local_sp_8;
                                        r10_7 = r10_5;
                                        local_sp_10 = local_sp_8;
                                        if ((uint64_t)_pre_phi == 0UL) {
                                            var_58 = (uint64_t *)(local_sp_9 + 16UL);
                                            var_59 = *var_58;
                                            var_60 = (uint64_t)*(uint32_t *)(local_sp_9 + 40UL);
                                            var_61 = (uint64_t *)(local_sp_9 + 24UL);
                                            *var_61 = r10_6;
                                            *(unsigned char *)(local_sp_9 + 8UL) = (unsigned char)r93_7;
                                            var_62 = local_sp_9 + (-8L);
                                            *(uint64_t *)var_62 = 4218594UL;
                                            var_63 = indirect_placeholder_2(r12_4, var_60, var_59);
                                            var_64 = *(unsigned char *)local_sp_9;
                                            var_65 = *var_58;
                                            rbp_1 = rbp_9;
                                            rbp_0 = rbp_9;
                                            r10_0 = var_65;
                                            r93_0_in = var_64;
                                            local_sp_0 = var_62;
                                            r13_0 = r13_5;
                                            r10_1 = var_65;
                                            r93_1_in = var_64;
                                            local_sp_1 = var_62;
                                            r13_1 = r13_5;
                                            if (r12_4 == var_63) {
                                                if ((uint64_t)(uint32_t)r13_5 != 0UL) {
                                                    loop_state_var = 1U;
                                                    break;
                                                }
                                                r93_0 = (uint64_t)r93_0_in;
                                                var_70 = (var_29 == 0UL);
                                                var_71 = (r93_0_in == '\x00');
                                                rbp_6 = rbp_0;
                                                rbp_4 = rbp_0;
                                                rbp_3 = rbp_0;
                                                r93_6 = r93_0;
                                                local_sp_7 = local_sp_0;
                                                r13_3 = r13_0;
                                                r10_4 = r10_0;
                                                r12_0 = 0UL;
                                                r10_1_pn = r10_0;
                                                r93_3 = r93_0;
                                                local_sp_3 = local_sp_0;
                                                r13_2 = r13_0;
                                                r93_4 = r93_0;
                                                local_sp_4 = local_sp_0;
                                                if (!var_70) {
                                                    loop_state_var = 2U;
                                                    break;
                                                }
                                                if (!var_71) {
                                                    loop_state_var = 3U;
                                                    break;
                                                }
                                                *(uint64_t *)(local_sp_0 + 16UL) = r10_0;
                                                r15_4 = (uint64_t)(uint32_t)rbp_0;
                                                continue;
                                            }
                                            var_66 = (uint64_t)var_64;
                                            var_67 = *var_61;
                                            *(uint64_t *)(local_sp_9 + (-16L)) = 4219001UL;
                                            var_68 = indirect_placeholder(4UL, var_67);
                                            *(uint64_t *)(local_sp_9 + (-24L)) = 4219009UL;
                                            indirect_placeholder_1();
                                            var_69 = (uint64_t)*(uint32_t *)var_68;
                                            *(uint64_t *)(local_sp_9 + (-32L)) = 4219028UL;
                                            indirect_placeholder_7(0UL, var_68, 4325608UL, var_66, 0UL, var_69, r8);
                                            loop_state_var = 4U;
                                            break;
                                        }
                                    }
                                    rbp_9 = rbp_8;
                                    r13_5 = r13_4;
                                    rbp_10 = rbp_8;
                                    r13_6 = r13_4;
                                    if ((uint64_t)(unsigned char)r15_4 == 0UL) {
                                        var_50 = (uint64_t)*(unsigned char *)(local_sp_10 + 47UL);
                                        var_51 = *(uint64_t *)(local_sp_10 + 32UL);
                                        *(uint64_t *)(local_sp_10 + 16UL) = r10_7;
                                        var_52 = (uint64_t)*(uint32_t *)(local_sp_10 + 40UL);
                                        var_53 = local_sp_10 + 8UL;
                                        *(unsigned char *)var_53 = (unsigned char)r93_8;
                                        var_54 = local_sp_10 + (-8L);
                                        *(uint64_t *)var_54 = 4218736UL;
                                        var_55 = indirect_placeholder_10(r12_5, var_50, var_52, var_51);
                                        var_56 = *(unsigned char *)local_sp_10;
                                        var_57 = *(uint64_t *)var_53;
                                        rbp_1 = rbp_10;
                                        rbp_0 = rbp_10;
                                        r10_0 = var_57;
                                        r93_0_in = var_56;
                                        local_sp_0 = var_54;
                                        r13_0 = r13_6;
                                        r10_1 = var_57;
                                        r93_1_in = var_56;
                                        local_sp_1 = var_54;
                                        r13_1 = r13_6;
                                        if ((uint64_t)(unsigned char)var_55 != 0UL) {
                                            r15_3 = (uint64_t)(uint32_t)var_55;
                                            loop_state_var = 4U;
                                            break;
                                        }
                                        if ((uint64_t)(uint32_t)r13_6 != 0UL) {
                                            loop_state_var = 1U;
                                            break;
                                        }
                                        r93_0 = (uint64_t)r93_0_in;
                                        var_70 = (var_29 == 0UL);
                                        var_71 = (r93_0_in == '\x00');
                                        rbp_6 = rbp_0;
                                        rbp_4 = rbp_0;
                                        rbp_3 = rbp_0;
                                        r93_6 = r93_0;
                                        local_sp_7 = local_sp_0;
                                        r13_3 = r13_0;
                                        r10_4 = r10_0;
                                        r12_0 = 0UL;
                                        r10_1_pn = r10_0;
                                        r93_3 = r93_0;
                                        local_sp_3 = local_sp_0;
                                        r13_2 = r13_0;
                                        r93_4 = r93_0;
                                        local_sp_4 = local_sp_0;
                                        if (!var_70) {
                                            loop_state_var = 2U;
                                            break;
                                        }
                                        if (!var_71) {
                                            loop_state_var = 3U;
                                            break;
                                        }
                                        *(uint64_t *)(local_sp_0 + 16UL) = r10_0;
                                        r15_4 = (uint64_t)(uint32_t)rbp_0;
                                        continue;
                                    }
                                    var_58 = (uint64_t *)(local_sp_9 + 16UL);
                                    var_59 = *var_58;
                                    var_60 = (uint64_t)*(uint32_t *)(local_sp_9 + 40UL);
                                    var_61 = (uint64_t *)(local_sp_9 + 24UL);
                                    *var_61 = r10_6;
                                    *(unsigned char *)(local_sp_9 + 8UL) = (unsigned char)r93_7;
                                    var_62 = local_sp_9 + (-8L);
                                    *(uint64_t *)var_62 = 4218594UL;
                                    var_63 = indirect_placeholder_2(r12_4, var_60, var_59);
                                    var_64 = *(unsigned char *)local_sp_9;
                                    var_65 = *var_58;
                                    rbp_1 = rbp_9;
                                    rbp_0 = rbp_9;
                                    r10_0 = var_65;
                                    r93_0_in = var_64;
                                    local_sp_0 = var_62;
                                    r13_0 = r13_5;
                                    r10_1 = var_65;
                                    r93_1_in = var_64;
                                    local_sp_1 = var_62;
                                    r13_1 = r13_5;
                                    if (r12_4 == var_63) {
                                        if ((uint64_t)(uint32_t)r13_5 != 0UL) {
                                            loop_state_var = 1U;
                                            break;
                                        }
                                        r93_0 = (uint64_t)r93_0_in;
                                        var_70 = (var_29 == 0UL);
                                        var_71 = (r93_0_in == '\x00');
                                        rbp_6 = rbp_0;
                                        rbp_4 = rbp_0;
                                        rbp_3 = rbp_0;
                                        r93_6 = r93_0;
                                        local_sp_7 = local_sp_0;
                                        r13_3 = r13_0;
                                        r10_4 = r10_0;
                                        r12_0 = 0UL;
                                        r10_1_pn = r10_0;
                                        r93_3 = r93_0;
                                        local_sp_3 = local_sp_0;
                                        r13_2 = r13_0;
                                        r93_4 = r93_0;
                                        local_sp_4 = local_sp_0;
                                        if (!var_70) {
                                            loop_state_var = 2U;
                                            break;
                                        }
                                        if (!var_71) {
                                            loop_state_var = 3U;
                                            break;
                                        }
                                        *(uint64_t *)(local_sp_0 + 16UL) = r10_0;
                                        r15_4 = (uint64_t)(uint32_t)rbp_0;
                                        continue;
                                    }
                                    var_66 = (uint64_t)var_64;
                                    var_67 = *var_61;
                                    *(uint64_t *)(local_sp_9 + (-16L)) = 4219001UL;
                                    var_68 = indirect_placeholder(4UL, var_67);
                                    *(uint64_t *)(local_sp_9 + (-24L)) = 4219009UL;
                                    indirect_placeholder_1();
                                    var_69 = (uint64_t)*(uint32_t *)var_68;
                                    *(uint64_t *)(local_sp_9 + (-32L)) = 4219028UL;
                                    indirect_placeholder_7(0UL, var_68, 4325608UL, var_66, 0UL, var_69, r8);
                                    loop_state_var = 4U;
                                    break;
                                }
                            switch_state_var = 0;
                            switch (loop_state_var) {
                              case 0U:
                                {
                                    var_45 = helper_cc_compute_c_wrapper((9223372036854775807UL - var_29) - r12_3, r12_3, var_6, 17U);
                                    r15_3 = 0UL;
                                    rbp_3 = rbp_2;
                                    r10_1_pn = r10_2;
                                    r93_3 = r93_2;
                                    local_sp_3 = local_sp_2;
                                    if (var_45 == 0UL) {
                                        var_48 = r12_3 + var_29;
                                        r12_0 = var_48;
                                        r14_0 = r14_2_ph_pn - var_29;
                                        rbp_4 = rbp_3;
                                        rbp_6_ph = rbp_3;
                                        r12_3_ph = r12_0;
                                        r12_1 = r12_0;
                                        r93_4 = r93_3;
                                        local_sp_4 = local_sp_3;
                                        r14_2_ph = r14_0;
                                        r93_6_ph = r93_3;
                                        local_sp_7_ph = local_sp_3;
                                        r13_3_ph = r13_2;
                                        if (r14_0 != 0UL) {
                                            loop_state_var = 1U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                        r15_4_ph = (uint64_t)(uint32_t)rbp_3;
                                        r10_4_ph = r10_1_pn + var_29;
                                        continue;
                                    }
                                    var_46 = *(uint64_t *)(local_sp_2 + 104UL);
                                    *(uint64_t *)(local_sp_2 + (-8L)) = 4218962UL;
                                    var_47 = indirect_placeholder(4UL, var_46);
                                    *(uint64_t *)(local_sp_2 + (-16L)) = 4218981UL;
                                    indirect_placeholder_8(0UL, var_47, 4325625UL, r93_2, 0UL, 0UL, r8);
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              case 2U:
                                {
                                    if (!var_71) {
                                        loop_state_var = 1U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    var_72 = (uint64_t *)local_sp_0;
                                    *var_72 = 0UL;
                                    _pre_phi276 = var_72;
                                    var_73 = *_pre_phi276;
                                    *(uint64_t *)(local_sp_0 + 16UL) = r10_0;
                                    r14_2_ph_pn = var_73;
                                    r14_0 = r14_2_ph_pn - var_29;
                                    rbp_4 = rbp_3;
                                    rbp_6_ph = rbp_3;
                                    r12_3_ph = r12_0;
                                    r12_1 = r12_0;
                                    r93_4 = r93_3;
                                    local_sp_4 = local_sp_3;
                                    r14_2_ph = r14_0;
                                    r93_6_ph = r93_3;
                                    local_sp_7_ph = local_sp_3;
                                    r13_3_ph = r13_2;
                                    if (r14_0 == 0UL) {
                                        loop_state_var = 1U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    r15_4_ph = (uint64_t)(uint32_t)rbp_3;
                                    r10_4_ph = r10_1_pn + var_29;
                                    continue;
                                }
                                break;
                              case 4U:
                                {
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              case 3U:
                              case 1U:
                                {
                                    switch (loop_state_var) {
                                      case 1U:
                                        {
                                            r93_1 = (uint64_t)r93_1_in;
                                            *(uint64_t *)(local_sp_1 + 16UL) = r10_1;
                                            rbp_3 = rbp_1;
                                            r10_1_pn = r10_1;
                                            r93_3 = r93_1;
                                            local_sp_3 = local_sp_1;
                                            r13_2 = r13_1;
                                        }
                                        break;
                                      case 3U:
                                        {
                                            _pre275 = (uint64_t *)local_sp_0;
                                            _pre_phi276 = _pre275;
                                            var_73 = *_pre_phi276;
                                            *(uint64_t *)(local_sp_0 + 16UL) = r10_0;
                                            r14_2_ph_pn = var_73;
                                        }
                                        break;
                                    }
                                    r14_0 = r14_2_ph_pn - var_29;
                                    rbp_4 = rbp_3;
                                    rbp_6_ph = rbp_3;
                                    r12_3_ph = r12_0;
                                    r12_1 = r12_0;
                                    r93_4 = r93_3;
                                    local_sp_4 = local_sp_3;
                                    r14_2_ph = r14_0;
                                    r93_6_ph = r93_3;
                                    local_sp_7_ph = local_sp_3;
                                    r13_3_ph = r13_2;
                                    if (r14_0 == 0UL) {
                                        loop_state_var = 1U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    r15_4_ph = (uint64_t)(uint32_t)rbp_3;
                                    r10_4_ph = r10_1_pn + var_29;
                                    continue;
                                }
                                break;
                            }
                            if (switch_state_var)
                                break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 1U:
                        {
                            var_74 = *(uint64_t *)(local_sp_4 + 72UL);
                            var_75 = (uint64_t *)(local_sp_4 + 64UL);
                            var_76 = *var_75 - var_74;
                            *var_75 = var_76;
                            var_77 = *(uint64_t *)(local_sp_4 + 112UL);
                            *(unsigned char *)var_77 = (unsigned char)rbp_4;
                            var_78 = (var_76 == 0UL);
                            var_79 = (uint64_t)(uint32_t)rbp_4;
                            local_sp_5_ph = local_sp_4;
                            rax_0 = var_77;
                            rbp_5_ph = rbp_4;
                            r15_1_ph = var_79;
                            r12_2_ph = r12_1;
                            r93_5_ph = r93_4;
                            r15_2 = var_79;
                            if (var_78) {
                                continue;
                            }
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 0U:
                        {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
                break;
              case 1U:
                {
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    if ((uint64_t)(unsigned char)r15_2 != 0UL) {
        return;
    }
    indirect_placeholder_1();
    return rax_0;
}
