typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_46_ret_type;
struct indirect_placeholder_49_ret_type;
struct indirect_placeholder_48_ret_type;
struct indirect_placeholder_47_ret_type;
struct indirect_placeholder_46_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_49_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_48_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_47_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_17(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_rax(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0);
extern uint64_t indirect_placeholder_16(void);
extern uint64_t indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_46_ret_type indirect_placeholder_46(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_49_ret_type indirect_placeholder_49(uint64_t param_0);
extern struct indirect_placeholder_48_ret_type indirect_placeholder_48(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_47_ret_type indirect_placeholder_47(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
uint64_t bb_make_dir_parents_private(uint64_t rcx, uint64_t rdx, uint64_t r9, uint64_t rdi, uint64_t rsi, uint64_t r8) {
    uint64_t var_127;
    uint64_t var_92;
    uint64_t local_sp_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t *var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t **var_18;
    uint64_t *var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t r93_10;
    uint64_t r93_3;
    uint64_t r93_5;
    uint64_t rax_0;
    uint64_t rdx2_0;
    uint64_t rsi5_2;
    uint64_t r93_0;
    uint64_t var_124;
    uint64_t local_sp_9;
    uint64_t var_122;
    uint64_t var_123;
    unsigned char var_112;
    uint64_t var_113;
    uint64_t rdx2_1;
    uint64_t local_sp_3;
    uint64_t local_sp_1;
    unsigned char *var_93;
    uint64_t var_94;
    uint64_t r14_0;
    uint64_t r93_1;
    uint64_t rsi5_0;
    uint64_t local_sp_4;
    uint64_t local_sp_2;
    uint64_t r14_1;
    uint64_t r93_2;
    uint64_t rsi5_1;
    uint32_t var_119;
    uint64_t var_114;
    uint64_t var_115;
    uint64_t var_116;
    uint64_t var_117;
    uint64_t var_118;
    uint64_t var_120;
    uint64_t var_121;
    uint64_t r93_4;
    uint64_t var_125;
    uint64_t local_sp_8;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t local_sp_5;
    uint64_t var_106;
    uint64_t var_107;
    uint64_t var_108;
    uint64_t var_109;
    uint64_t var_104;
    uint64_t var_105;
    unsigned char var_95;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t r14_2;
    uint64_t var_98;
    unsigned char var_99;
    uint64_t var_100;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t var_91;
    uint64_t local_sp_7;
    uint64_t rax_2;
    uint64_t local_sp_6;
    uint64_t var_67;
    struct indirect_placeholder_49_ret_type var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t *var_88;
    uint64_t r93_9;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t local_sp_10;
    uint32_t r86_0;
    uint32_t *var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_62;
    uint32_t var_63;
    uint64_t local_sp_12;
    uint64_t r93_6;
    uint64_t rax_1;
    uint64_t r93_7;
    uint64_t var_131;
    unsigned char var_132;
    uint64_t rdi4_0;
    uint64_t var_133;
    uint64_t local_sp_11;
    uint64_t var_128;
    uint64_t var_129;
    uint64_t var_130;
    uint64_t merge;
    uint64_t r93_8;
    uint64_t var_126;
    uint64_t var_60;
    uint64_t var_61;
    unsigned char *var_56;
    uint64_t var_57;
    uint64_t var_58;
    bool var_59;
    uint64_t var_23;
    uint64_t rdi4_1;
    uint64_t var_24;
    uint32_t *var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint32_t *var_28;
    uint64_t *var_29;
    uint64_t *var_30;
    uint64_t *var_31;
    uint64_t *var_32;
    uint64_t *var_33;
    uint64_t *var_34;
    uint64_t *var_35;
    uint64_t *var_36;
    uint64_t *var_37;
    uint64_t *var_38;
    uint64_t *var_39;
    uint64_t *var_40;
    uint64_t *var_41;
    uint64_t *var_42;
    uint64_t *var_43;
    uint64_t *var_44;
    uint64_t *var_45;
    uint64_t *var_46;
    uint64_t *var_47;
    unsigned char *var_48;
    unsigned char *var_49;
    uint64_t var_50;
    unsigned char *var_51;
    uint32_t *var_52;
    uint64_t *var_53;
    unsigned char *var_54;
    unsigned char *var_55;
    uint64_t r86_1;
    uint64_t storemerge;
    uint64_t var_134;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbp();
    var_3 = init_r15();
    var_4 = init_r14();
    var_5 = init_r12();
    var_6 = init_rbx();
    var_7 = init_r13();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_7;
    *(uint64_t *)(var_0 + (-40L)) = var_5;
    *(uint64_t *)(var_0 + (-48L)) = var_6;
    var_8 = var_0 + (-360L);
    *(uint64_t *)var_8 = rcx;
    var_9 = var_0 + (-352L);
    var_10 = (uint64_t *)var_9;
    *var_10 = r8;
    var_11 = (uint64_t *)(var_0 + (-376L));
    *var_11 = rdx;
    var_12 = var_0 + (-384L);
    *(uint64_t *)var_12 = 4211447UL;
    indirect_placeholder_1();
    var_13 = (var_1 + 24UL) & (-16L);
    var_14 = var_12 - var_13;
    *(uint64_t *)(var_14 + (-8L)) = 4211479UL;
    indirect_placeholder_1();
    var_15 = var_14 + (-16L);
    *(uint64_t *)var_15 = 4211490UL;
    var_16 = indirect_placeholder_5(var_13);
    var_17 = var_15 - ((var_16 + 24UL) & (-16L));
    *(uint64_t *)(var_17 + (-8L)) = 4211527UL;
    indirect_placeholder_1();
    var_18 = (uint64_t **)var_8;
    var_19 = *var_18;
    *(unsigned char *)(var_16 + var_17) = (unsigned char)'\x00';
    var_20 = var_0 + (-344L);
    *var_19 = 0UL;
    var_21 = var_17 + (-16L);
    *(uint64_t *)var_21 = 4211561UL;
    var_22 = indirect_placeholder(var_17, var_20);
    r93_10 = r9;
    rdx2_0 = 4323028UL;
    rdx2_1 = 4294967295UL;
    r14_2 = 0UL;
    rax_2 = var_22;
    r93_9 = r9;
    r86_0 = 20U;
    local_sp_12 = var_21;
    rax_1 = 280375465148160UL;
    local_sp_11 = var_21;
    merge = 1UL;
    r86_1 = r8;
    storemerge = var_17;
    if ((uint64_t)(uint32_t)var_22 != 0UL) {
        if ((uint32_t)((uint16_t)*(uint32_t *)(var_0 + (-320L)) & (unsigned short)61440U) != 16384U) {
            **(unsigned char **)var_9 = (unsigned char)'\x00';
            return merge;
        }
        *(uint64_t *)(local_sp_12 + (-8L)) = 4212589UL;
        var_134 = indirect_placeholder(4UL, storemerge);
        *(uint64_t *)(local_sp_12 + (-16L)) = 4212608UL;
        indirect_placeholder_47(0UL, var_134, 4317744UL, r93_10, 0UL, 0UL, r86_1);
        merge = 0UL;
        return merge;
    }
    var_23 = var_13 + rsi;
    rdi4_1 = var_23;
    r86_1 = r9;
    storemerge = var_13;
    if (*(unsigned char *)var_23 == '/') {
        var_24 = rdi4_1 + 1UL;
        rdi4_1 = var_24;
        do {
            var_24 = rdi4_1 + 1UL;
            rdi4_1 = var_24;
        } while (*(unsigned char *)var_24 != '/');
    }
    var_25 = (uint32_t *)(r9 + 28UL);
    var_26 = var_0 + (-200L);
    var_27 = var_0 + (-176L);
    var_28 = (uint32_t *)var_27;
    var_29 = (uint64_t *)var_26;
    var_30 = (uint64_t *)(var_0 + (-192L));
    var_31 = (uint64_t *)(var_0 + (-184L));
    var_32 = (uint64_t *)var_27;
    var_33 = (uint64_t *)(var_0 + (-168L));
    var_34 = (uint64_t *)(var_0 + (-160L));
    var_35 = (uint64_t *)(var_0 + (-152L));
    var_36 = (uint64_t *)(var_0 + (-144L));
    var_37 = (uint64_t *)(var_0 + (-136L));
    var_38 = (uint64_t *)(var_0 + (-128L));
    var_39 = (uint64_t *)(var_0 + (-120L));
    var_40 = (uint64_t *)(var_0 + (-112L));
    var_41 = (uint64_t *)(var_0 + (-104L));
    var_42 = (uint64_t *)(var_0 + (-96L));
    var_43 = (uint64_t *)(var_0 + (-88L));
    var_44 = (uint64_t *)(var_0 + (-80L));
    var_45 = (uint64_t *)(var_0 + (-72L));
    var_46 = (uint64_t *)(var_0 + (-64L));
    var_47 = (uint64_t *)(var_0 + (-368L));
    var_48 = (unsigned char *)(r9 + 29UL);
    var_49 = (unsigned char *)(r9 + 30UL);
    var_50 = r9 + 32UL;
    var_51 = (unsigned char *)var_50;
    var_52 = (uint32_t *)(var_0 + (-320L));
    var_53 = (uint64_t *)var_50;
    var_54 = (unsigned char *)(r9 + 37UL);
    var_55 = (unsigned char *)(r9 + 38UL);
    while (1U)
        {
            *(uint64_t *)(local_sp_11 + (-8L)) = 4211610UL;
            indirect_placeholder_1();
            r93_8 = r93_9;
            if (rax_2 != 0UL) {
                loop_state_var = 2U;
                break;
            }
            var_56 = (unsigned char *)rax_2;
            *var_56 = (unsigned char)'\x00';
            var_57 = local_sp_11 + (-16L);
            *(uint64_t *)var_57 = 4211640UL;
            var_58 = indirect_placeholder(var_13, var_20);
            var_59 = ((uint64_t)(uint32_t)var_58 == 0UL);
            merge = 0UL;
            local_sp_10 = var_57;
            if (!var_59) {
                if (*var_25 >= 256U) {
                    var_126 = local_sp_10 + (-8L);
                    *(uint64_t *)var_126 = 4211679UL;
                    var_127 = indirect_placeholder_17(0UL, 0UL, var_23, var_13, r9);
                    local_sp_8 = var_126;
                    r93_6 = r93_8;
                    local_sp_12 = var_126;
                    r93_10 = r93_8;
                    if ((uint64_t)(unsigned char)var_127 != 0UL) {
                        loop_state_var = 2U;
                        break;
                    }
                    if ((uint32_t)((uint16_t)*var_52 & (unsigned short)61440U) != 16384U) {
                        loop_state_var = 1U;
                        break;
                    }
                    **(unsigned char **)var_9 = (unsigned char)'\x00';
                    local_sp_9 = local_sp_8;
                    r93_7 = r93_6;
                    if ((*var_53 & 280375465148160UL) != 0UL) {
                        var_128 = (uint64_t)*var_54;
                        var_129 = local_sp_8 + (-8L);
                        *(uint64_t *)var_129 = 4212426UL;
                        var_130 = indirect_placeholder_10(r9, 0UL, var_13, var_128);
                        local_sp_9 = var_129;
                        rax_1 = var_130;
                        if ((uint64_t)(unsigned char)var_130 != 0UL & *var_55 != '\x00') {
                            loop_state_var = 2U;
                            break;
                        }
                    }
                    var_131 = rax_2 + 1UL;
                    var_132 = *(unsigned char *)var_131;
                    *var_56 = (unsigned char)'/';
                    rdi4_0 = var_131;
                    local_sp_11 = local_sp_9;
                    rax_2 = rax_1;
                    r93_9 = r93_7;
                    if (var_132 == '/') {
                        var_133 = rdi4_0 + 1UL;
                        rdi4_0 = var_133;
                        do {
                            var_133 = rdi4_0 + 1UL;
                            rdi4_0 = var_133;
                        } while (*(unsigned char *)var_133 != '/');
                    }
                    continue;
                }
            }
            var_60 = local_sp_11 + (-24L);
            *(uint64_t *)var_60 = 4211791UL;
            var_61 = indirect_placeholder(var_23, var_26);
            local_sp_6 = var_60;
            local_sp_7 = var_60;
            if ((uint64_t)(uint32_t)var_61 == 0UL) {
                if ((uint32_t)((uint16_t)*var_28 & (unsigned short)61440U) != 16384U) {
                    loop_state_var = 3U;
                    break;
                }
            }
            var_62 = local_sp_11 + (-32L);
            *(uint64_t *)var_62 = 4212341UL;
            indirect_placeholder_1();
            var_63 = *(uint32_t *)var_61;
            local_sp_6 = var_62;
            local_sp_7 = var_62;
            r86_0 = var_63;
            if (var_63 != 0U) {
                loop_state_var = 3U;
                break;
            }
            var_67 = local_sp_6 + (-8L);
            *(uint64_t *)var_67 = 4211831UL;
            var_68 = indirect_placeholder_49(168UL);
            var_69 = var_68.field_0;
            var_70 = *var_30;
            var_71 = *var_31;
            var_72 = *var_32;
            var_73 = *var_33;
            var_74 = *var_34;
            var_75 = *var_35;
            var_76 = *var_36;
            *(uint64_t *)var_69 = *var_29;
            *(uint64_t *)(var_69 + 8UL) = var_70;
            var_77 = *var_37;
            var_78 = *var_38;
            var_79 = *var_39;
            var_80 = *var_40;
            var_81 = *var_41;
            var_82 = *var_42;
            var_83 = *var_43;
            var_84 = *var_44;
            *(uint64_t *)(var_69 + 16UL) = var_71;
            var_85 = var_69 + 24UL;
            *(uint64_t *)var_85 = var_72;
            var_86 = *var_45;
            var_87 = *var_46;
            *(uint64_t *)(var_69 + 32UL) = var_73;
            *(uint64_t *)(var_69 + 40UL) = var_74;
            var_88 = *var_18;
            *(uint64_t *)(var_69 + 48UL) = var_75;
            *(uint64_t *)(var_69 + 56UL) = var_76;
            *(uint64_t *)(var_69 + 64UL) = var_77;
            *(uint64_t *)(var_69 + 72UL) = var_78;
            *(uint64_t *)(var_69 + 80UL) = var_79;
            *(uint64_t *)(var_69 + 88UL) = var_80;
            *(uint64_t *)(var_69 + 96UL) = var_81;
            *(uint64_t *)(var_69 + 104UL) = var_82;
            *(uint64_t *)(var_69 + 112UL) = var_83;
            *(uint64_t *)(var_69 + 120UL) = var_84;
            *(uint64_t *)(var_69 + 128UL) = var_86;
            *(uint64_t *)(var_69 + 136UL) = var_87;
            var_89 = rax_2 - var_13;
            *(unsigned char *)(var_69 + 144UL) = (unsigned char)'\x00';
            *(uint64_t *)(var_69 + 152UL) = var_89;
            var_90 = *var_88;
            *var_88 = var_69;
            *(uint64_t *)(var_69 + 160UL) = var_90;
            local_sp_10 = var_67;
            r93_8 = var_69;
            if (!var_59) {
                var_91 = (uint64_t)*(uint32_t *)var_85;
                *var_47 = var_69;
                *(uint64_t *)(local_sp_6 + (-16L)) = 4212009UL;
                var_92 = indirect_placeholder_17(1UL, var_91, var_23, var_13, r9);
                if ((uint64_t)(unsigned char)var_92 != 0UL) {
                    loop_state_var = 2U;
                    break;
                }
                var_93 = *(unsigned char **)var_9;
                var_94 = *var_47;
                *var_93 = (unsigned char)'\x01';
                var_95 = *var_48;
                var_96 = (uint64_t)*(uint32_t *)(var_94 + 24UL);
                if (var_95 == '\x00') {
                    var_97 = var_96 & 63UL;
                    r14_2 = var_97;
                    rdx2_1 = var_97 ^ 4294967295UL;
                } else {
                    if (*var_49 == '\x00') {
                        var_98 = var_96 & 18UL;
                        r14_2 = var_98;
                        rdx2_1 = var_98 ^ 4294967295UL;
                    }
                }
                var_99 = *var_51;
                *var_47 = var_94;
                var_100 = (uint64_t)(((uint16_t)((var_99 == '\x00') ? var_96 : 511UL) & (uint16_t)rdx2_1) & (unsigned short)4095U);
                var_101 = local_sp_6 + (-24L);
                *(uint64_t *)var_101 = 4212097UL;
                var_102 = indirect_placeholder(var_13, var_100);
                var_103 = *var_47;
                r93_0 = var_103;
                r14_0 = r14_2;
                local_sp_5 = var_101;
                r93_5 = var_103;
                if ((uint64_t)(uint32_t)var_102 != 0UL) {
                    *(uint64_t *)(local_sp_6 + (-32L)) = 4212642UL;
                    var_104 = indirect_placeholder(4UL, var_13);
                    var_105 = local_sp_6 + (-40L);
                    *(uint64_t *)var_105 = 4212650UL;
                    indirect_placeholder_1();
                    local_sp_0 = var_105;
                    rax_0 = var_104;
                    loop_state_var = 0U;
                    break;
                }
                rdx2_0 = 4323053UL;
                if (*var_11 == 0UL) {
                    var_106 = local_sp_6 + (-32L);
                    *(uint64_t *)var_106 = 4212144UL;
                    indirect_placeholder_1();
                    local_sp_5 = var_106;
                    r93_5 = *var_47;
                }
                *var_47 = r93_5;
                var_107 = local_sp_5 + (-8L);
                *(uint64_t *)var_107 = 4212173UL;
                var_108 = indirect_placeholder(var_13, var_20);
                var_109 = *var_47;
                r93_3 = var_109;
                r93_0 = var_109;
                local_sp_3 = var_107;
                local_sp_1 = var_107;
                r93_1 = var_109;
                if ((uint64_t)(uint32_t)var_108 != 0UL) {
                    *(uint64_t *)(local_sp_5 + (-16L)) = 4212699UL;
                    var_110 = indirect_placeholder(4UL, var_13);
                    var_111 = local_sp_5 + (-24L);
                    *(uint64_t *)var_111 = 4212707UL;
                    indirect_placeholder_1();
                    local_sp_0 = var_111;
                    rax_0 = var_110;
                    rdx2_0 = 4317712UL;
                    loop_state_var = 0U;
                    break;
                }
                var_112 = *var_49;
                var_113 = (uint64_t)*var_52;
                rsi5_0 = var_113;
                rsi5_2 = var_113;
                if (var_112 != '\x00') {
                    var_120 = (uint64_t)((uint16_t)rsi5_2 & (unsigned short)448U);
                    r93_0 = r93_3;
                    local_sp_4 = local_sp_3;
                    r93_4 = r93_3;
                    if ((uint64_t)(((uint32_t)var_120 + (-448)) & (-64)) == 0UL) {
                        var_125 = *var_10;
                        local_sp_9 = local_sp_4;
                        local_sp_8 = local_sp_4;
                        r93_6 = r93_4;
                        rax_1 = var_125;
                        r93_7 = r93_4;
                        if (*(unsigned char *)var_125 != '\x00') {
                            var_131 = rax_2 + 1UL;
                            var_132 = *(unsigned char *)var_131;
                            *var_56 = (unsigned char)'/';
                            rdi4_0 = var_131;
                            local_sp_11 = local_sp_9;
                            rax_2 = rax_1;
                            r93_9 = r93_7;
                            if (var_132 == '/') {
                                var_133 = rdi4_0 + 1UL;
                                rdi4_0 = var_133;
                                do {
                                    var_133 = rdi4_0 + 1UL;
                                    rdi4_0 = var_133;
                                } while (*(unsigned char *)var_133 != '/');
                            }
                            continue;
                        }
                        local_sp_9 = local_sp_8;
                        r93_7 = r93_6;
                        if ((*var_53 & 280375465148160UL) != 0UL) {
                            var_128 = (uint64_t)*var_54;
                            var_129 = local_sp_8 + (-8L);
                            *(uint64_t *)var_129 = 4212426UL;
                            var_130 = indirect_placeholder_10(r9, 0UL, var_13, var_128);
                            local_sp_9 = var_129;
                            rax_1 = var_130;
                            if ((uint64_t)(unsigned char)var_130 != 0UL & *var_55 != '\x00') {
                                loop_state_var = 2U;
                                break;
                            }
                        }
                        var_131 = rax_2 + 1UL;
                        var_132 = *(unsigned char *)var_131;
                        *var_56 = (unsigned char)'/';
                        rdi4_0 = var_131;
                        local_sp_11 = local_sp_9;
                        rax_2 = rax_1;
                        r93_9 = r93_7;
                        if (var_132 == '/') {
                            var_133 = rdi4_0 + 1UL;
                            rdi4_0 = var_133;
                            do {
                                var_133 = rdi4_0 + 1UL;
                                rdi4_0 = var_133;
                            } while (*(unsigned char *)var_133 != '/');
                        }
                        continue;
                    }
                    var_121 = local_sp_3 + (-8L);
                    *(uint64_t *)var_121 = 4212271UL;
                    indirect_placeholder_1();
                    local_sp_4 = var_121;
                    if (var_120 == 0UL) {
                        *(uint64_t *)(local_sp_3 + (-16L)) = 4212730UL;
                        var_122 = indirect_placeholder(4UL, var_13);
                        var_123 = local_sp_3 + (-24L);
                        *(uint64_t *)var_123 = 4212738UL;
                        indirect_placeholder_1();
                        local_sp_0 = var_123;
                        rax_0 = var_122;
                        loop_state_var = 0U;
                        break;
                    }
                }
                if (((var_113 ^ 63UL) & r14_2) == 0UL) {
                    local_sp_4 = local_sp_1;
                    local_sp_2 = local_sp_1;
                    r14_1 = r14_0;
                    r93_2 = r93_1;
                    rsi5_1 = rsi5_0;
                    r93_4 = r93_1;
                    if ((uint64_t)(((uint32_t)(uint64_t)((uint16_t)rsi5_0 & (unsigned short)448U) + (-448)) & (-64)) == 0UL) {
                        var_125 = *var_10;
                        local_sp_9 = local_sp_4;
                        local_sp_8 = local_sp_4;
                        r93_6 = r93_4;
                        rax_1 = var_125;
                        r93_7 = r93_4;
                        if (*(unsigned char *)var_125 != '\x00') {
                            var_131 = rax_2 + 1UL;
                            var_132 = *(unsigned char *)var_131;
                            *var_56 = (unsigned char)'/';
                            rdi4_0 = var_131;
                            local_sp_11 = local_sp_9;
                            rax_2 = rax_1;
                            r93_9 = r93_7;
                            if (var_132 == '/') {
                                var_133 = rdi4_0 + 1UL;
                                rdi4_0 = var_133;
                                do {
                                    var_133 = rdi4_0 + 1UL;
                                    rdi4_0 = var_133;
                                } while (*(unsigned char *)var_133 != '/');
                            }
                            continue;
                        }
                    }
                    var_119 = (uint32_t)r14_1 | (uint32_t)rsi5_1;
                    *(unsigned char *)(r93_2 + 144UL) = (unsigned char)'\x01';
                    *(uint32_t *)(r93_2 + 24UL) = var_119;
                    local_sp_3 = local_sp_2;
                    r93_3 = r93_2;
                    rsi5_2 = rsi5_1;
                    var_120 = (uint64_t)((uint16_t)rsi5_2 & (unsigned short)448U);
                    r93_0 = r93_3;
                    local_sp_4 = local_sp_3;
                    r93_4 = r93_3;
                    var_121 = local_sp_3 + (-8L);
                    *(uint64_t *)var_121 = 4212271UL;
                    indirect_placeholder_1();
                    local_sp_4 = var_121;
                    if ((uint64_t)(((uint32_t)var_120 + (-448)) & (-64)) != 0UL & var_120 != 0UL) {
                        *(uint64_t *)(local_sp_3 + (-16L)) = 4212730UL;
                        var_122 = indirect_placeholder(4UL, var_13);
                        var_123 = local_sp_3 + (-24L);
                        *(uint64_t *)var_123 = 4212738UL;
                        indirect_placeholder_1();
                        local_sp_0 = var_123;
                        rax_0 = var_122;
                        loop_state_var = 0U;
                        break;
                    }
                }
                var_114 = local_sp_5 + (-16L);
                *(uint64_t *)var_114 = 4212489UL;
                var_115 = indirect_placeholder_16();
                var_116 = (uint64_t)*var_52;
                var_117 = *var_47;
                var_118 = r14_2 & ((uint64_t)(uint32_t)var_115 ^ 4294967295UL);
                local_sp_1 = var_114;
                r14_0 = var_118;
                r93_1 = var_117;
                rsi5_0 = var_116;
                local_sp_2 = var_114;
                r14_1 = var_118;
                r93_2 = var_117;
                rsi5_1 = var_116;
                if (((var_116 ^ 63UL) & var_118) != 0UL) {
                    var_119 = (uint32_t)r14_1 | (uint32_t)rsi5_1;
                    *(unsigned char *)(r93_2 + 144UL) = (unsigned char)'\x01';
                    *(uint32_t *)(r93_2 + 24UL) = var_119;
                    local_sp_3 = local_sp_2;
                    r93_3 = r93_2;
                    rsi5_2 = rsi5_1;
                    var_120 = (uint64_t)((uint16_t)rsi5_2 & (unsigned short)448U);
                    r93_0 = r93_3;
                    local_sp_4 = local_sp_3;
                    r93_4 = r93_3;
                    if ((uint64_t)(((uint32_t)var_120 + (-448)) & (-64)) == 0UL) {
                        var_125 = *var_10;
                        local_sp_9 = local_sp_4;
                        local_sp_8 = local_sp_4;
                        r93_6 = r93_4;
                        rax_1 = var_125;
                        r93_7 = r93_4;
                        if (*(unsigned char *)var_125 != '\x00') {
                            var_131 = rax_2 + 1UL;
                            var_132 = *(unsigned char *)var_131;
                            *var_56 = (unsigned char)'/';
                            rdi4_0 = var_131;
                            local_sp_11 = local_sp_9;
                            rax_2 = rax_1;
                            r93_9 = r93_7;
                            if (var_132 == '/') {
                                var_133 = rdi4_0 + 1UL;
                                rdi4_0 = var_133;
                                do {
                                    var_133 = rdi4_0 + 1UL;
                                    rdi4_0 = var_133;
                                } while (*(unsigned char *)var_133 != '/');
                            }
                            continue;
                        }
                        local_sp_9 = local_sp_8;
                        r93_7 = r93_6;
                        if ((*var_53 & 280375465148160UL) != 0UL) {
                            var_128 = (uint64_t)*var_54;
                            var_129 = local_sp_8 + (-8L);
                            *(uint64_t *)var_129 = 4212426UL;
                            var_130 = indirect_placeholder_10(r9, 0UL, var_13, var_128);
                            local_sp_9 = var_129;
                            rax_1 = var_130;
                            if ((uint64_t)(unsigned char)var_130 != 0UL & *var_55 != '\x00') {
                                loop_state_var = 2U;
                                break;
                            }
                        }
                        var_131 = rax_2 + 1UL;
                        var_132 = *(unsigned char *)var_131;
                        *var_56 = (unsigned char)'/';
                        rdi4_0 = var_131;
                        local_sp_11 = local_sp_9;
                        rax_2 = rax_1;
                        r93_9 = r93_7;
                        if (var_132 == '/') {
                            var_133 = rdi4_0 + 1UL;
                            rdi4_0 = var_133;
                            do {
                                var_133 = rdi4_0 + 1UL;
                                rdi4_0 = var_133;
                            } while (*(unsigned char *)var_133 != '/');
                        }
                        continue;
                    }
                    var_121 = local_sp_3 + (-8L);
                    *(uint64_t *)var_121 = 4212271UL;
                    indirect_placeholder_1();
                    local_sp_4 = var_121;
                    if (var_120 != 0UL) {
                        *(uint64_t *)(local_sp_3 + (-16L)) = 4212730UL;
                        var_122 = indirect_placeholder(4UL, var_13);
                        var_123 = local_sp_3 + (-24L);
                        *(uint64_t *)var_123 = 4212738UL;
                        indirect_placeholder_1();
                        local_sp_0 = var_123;
                        rax_0 = var_122;
                        loop_state_var = 0U;
                        break;
                    }
                }
            }
            var_126 = local_sp_10 + (-8L);
            *(uint64_t *)var_126 = 4211679UL;
            var_127 = indirect_placeholder_17(0UL, 0UL, var_23, var_13, r9);
            local_sp_8 = var_126;
            r93_6 = r93_8;
            local_sp_12 = var_126;
            r93_10 = r93_8;
            if ((uint64_t)(unsigned char)var_127 != 0UL) {
                loop_state_var = 2U;
                break;
            }
            if ((uint32_t)((uint16_t)*var_52 & (unsigned short)61440U) != 16384U) {
                loop_state_var = 1U;
                break;
            }
            **(unsigned char **)var_9 = (unsigned char)'\x00';
        }
    switch (loop_state_var) {
      case 0U:
        {
            var_124 = (uint64_t)*(uint32_t *)rax_0;
            *(uint64_t *)(local_sp_0 + (-8L)) = 4212669UL;
            indirect_placeholder_46(0UL, rax_0, rdx2_0, r93_0, 0UL, var_124, r9);
            return 0UL;
        }
        break;
      case 3U:
      case 2U:
      case 1U:
        {
            switch (loop_state_var) {
              case 1U:
                {
                    *(uint64_t *)(local_sp_12 + (-8L)) = 4212589UL;
                    var_134 = indirect_placeholder(4UL, storemerge);
                    *(uint64_t *)(local_sp_12 + (-16L)) = 4212608UL;
                    indirect_placeholder_47(0UL, var_134, 4317744UL, r93_10, 0UL, 0UL, r86_1);
                    merge = 0UL;
                }
                break;
              case 2U:
                {
                    return merge;
                }
                break;
              case 3U:
                {
                    var_64 = (uint32_t *)var_9;
                    *var_64 = r86_0;
                    *(uint64_t *)(local_sp_7 + (-8L)) = 4212373UL;
                    var_65 = indirect_placeholder(4UL, var_23);
                    var_66 = (uint64_t)*var_64;
                    *(uint64_t *)(local_sp_7 + (-16L)) = 4212400UL;
                    indirect_placeholder_48(0UL, var_65, 4317712UL, r93_9, 0UL, var_66, var_66);
                }
                break;
            }
        }
        break;
    }
}
