typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct helper_pxor_xmm_wrapper_281_ret_type;
struct type_3;
struct type_5;
struct helper_divq_EAX_wrapper_ret_type;
struct indirect_placeholder_205_ret_type;
struct indirect_placeholder_204_ret_type;
struct indirect_placeholder_207_ret_type;
struct indirect_placeholder_206_ret_type;
struct indirect_placeholder_209_ret_type;
struct indirect_placeholder_208_ret_type;
struct indirect_placeholder_211_ret_type;
struct indirect_placeholder_210_ret_type;
struct indirect_placeholder_213_ret_type;
struct indirect_placeholder_212_ret_type;
struct indirect_placeholder_215_ret_type;
struct indirect_placeholder_214_ret_type;
struct indirect_placeholder_217_ret_type;
struct indirect_placeholder_216_ret_type;
struct indirect_placeholder_219_ret_type;
struct indirect_placeholder_218_ret_type;
struct indirect_placeholder_220_ret_type;
struct helper_pxor_xmm_wrapper_281_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_3 {
};
struct type_5 {
};
struct helper_divq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint32_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint32_t field_5;
    uint64_t field_6;
    uint32_t field_7;
    uint64_t field_8;
    uint32_t field_9;
    uint32_t field_10;
    unsigned char field_11;
    uint32_t field_12;
};
struct indirect_placeholder_205_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_204_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_207_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_206_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_209_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_208_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_211_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_210_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_213_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_212_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_215_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_214_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_217_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_216_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_219_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_218_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_220_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_state_0x8558(void);
extern uint64_t init_state_0x8560(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern struct helper_pxor_xmm_wrapper_281_ret_type helper_pxor_xmm_wrapper_281(struct type_3 *param_0, struct type_5 *param_1, struct type_5 *param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern uint32_t init_state_0x8248(void);
extern struct helper_divq_EAX_wrapper_ret_type helper_divq_EAX_wrapper(struct type_3 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint64_t param_13, uint32_t param_14, uint64_t param_15, uint64_t param_16, uint64_t param_17, uint32_t param_18, uint32_t param_19);
extern struct indirect_placeholder_205_ret_type indirect_placeholder_205(uint64_t param_0);
extern struct indirect_placeholder_204_ret_type indirect_placeholder_204(uint64_t param_0);
extern struct indirect_placeholder_207_ret_type indirect_placeholder_207(uint64_t param_0);
extern struct indirect_placeholder_206_ret_type indirect_placeholder_206(uint64_t param_0);
extern struct indirect_placeholder_209_ret_type indirect_placeholder_209(uint64_t param_0);
extern struct indirect_placeholder_208_ret_type indirect_placeholder_208(uint64_t param_0);
extern struct indirect_placeholder_211_ret_type indirect_placeholder_211(uint64_t param_0);
extern struct indirect_placeholder_210_ret_type indirect_placeholder_210(uint64_t param_0);
extern struct indirect_placeholder_213_ret_type indirect_placeholder_213(uint64_t param_0);
extern struct indirect_placeholder_212_ret_type indirect_placeholder_212(uint64_t param_0);
extern struct indirect_placeholder_215_ret_type indirect_placeholder_215(uint64_t param_0);
extern struct indirect_placeholder_214_ret_type indirect_placeholder_214(uint64_t param_0);
extern struct indirect_placeholder_217_ret_type indirect_placeholder_217(uint64_t param_0);
extern struct indirect_placeholder_216_ret_type indirect_placeholder_216(uint64_t param_0);
extern void indirect_placeholder_18(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_219_ret_type indirect_placeholder_219(uint64_t param_0);
extern struct indirect_placeholder_218_ret_type indirect_placeholder_218(uint64_t param_0);
extern struct indirect_placeholder_220_ret_type indirect_placeholder_220(uint64_t param_0);
uint64_t bb_randint_genmax(uint64_t rcx, uint64_t r10, uint64_t r9, uint64_t rdi, uint64_t rsi, uint64_t r8) {
    struct helper_pxor_xmm_wrapper_281_ret_type var_96;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint32_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint32_t var_15;
    uint32_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t *var_20;
    uint64_t var_21;
    uint64_t *var_22;
    struct indirect_placeholder_205_ret_type var_79;
    uint64_t var_80;
    uint64_t var_81;
    struct indirect_placeholder_204_ret_type var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t r12_3;
    uint64_t r12_2;
    struct indirect_placeholder_207_ret_type var_72;
    uint64_t var_73;
    uint64_t var_74;
    struct indirect_placeholder_206_ret_type var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t state_0x9018_0;
    uint64_t r12_1;
    struct indirect_placeholder_209_ret_type var_65;
    uint64_t var_66;
    uint64_t var_67;
    struct indirect_placeholder_208_ret_type var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    struct indirect_placeholder_211_ret_type var_58;
    uint64_t var_59;
    uint64_t var_60;
    struct indirect_placeholder_210_ret_type var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    struct indirect_placeholder_213_ret_type var_51;
    uint64_t var_52;
    uint64_t var_53;
    struct indirect_placeholder_212_ret_type var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    struct indirect_placeholder_215_ret_type var_44;
    uint64_t var_45;
    uint64_t var_46;
    struct indirect_placeholder_214_ret_type var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    struct indirect_placeholder_217_ret_type var_37;
    uint64_t var_38;
    uint64_t var_39;
    struct indirect_placeholder_216_ret_type var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_28;
    uint64_t rbp_2;
    struct indirect_placeholder_219_ret_type var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t rsi5_0;
    struct indirect_placeholder_218_ret_type var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t r12_4;
    uint64_t r12_0;
    uint64_t rdi4_0;
    uint64_t rbp_0;
    uint64_t rcx1_0;
    uint32_t state_0x9010_0;
    uint64_t local_sp_0;
    uint64_t state_0x82d8_0;
    uint32_t state_0x9080_0;
    uint32_t state_0x8248_0;
    uint64_t var_23;
    uint64_t rdx_0;
    uint64_t rdi4_1;
    uint64_t local_sp_1;
    uint64_t var_24;
    struct indirect_placeholder_220_ret_type var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t rdi4_2;
    uint64_t rbp_1;
    uint64_t rsi5_1;
    uint64_t local_sp_2;
    uint64_t rdi4_3;
    uint64_t rsi5_2;
    uint64_t local_sp_3;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint32_t var_89;
    struct helper_divq_EAX_wrapper_ret_type var_85;
    uint64_t var_90;
    uint32_t var_91;
    uint32_t var_92;
    uint64_t var_93;
    struct helper_divq_EAX_wrapper_ret_type var_94;
    uint64_t var_95;
    uint64_t var_97;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r15();
    var_3 = init_r14();
    var_4 = init_r12();
    var_5 = init_rbx();
    var_6 = init_cc_src2();
    var_7 = init_r13();
    var_8 = init_state_0x8558();
    var_9 = init_state_0x8560();
    var_10 = init_state_0x9018();
    var_11 = init_state_0x9010();
    var_12 = init_state_0x8408();
    var_13 = init_state_0x8328();
    var_14 = init_state_0x82d8();
    var_15 = init_state_0x9080();
    var_16 = init_state_0x8248();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_7;
    var_17 = rsi + 1UL;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_1;
    *(uint64_t *)(var_0 + (-48L)) = var_5;
    var_18 = var_0 + (-72L);
    var_19 = *(uint64_t *)rdi;
    var_20 = (uint64_t *)(rdi + 8UL);
    var_21 = *var_20;
    var_22 = (uint64_t *)(rdi + 16UL);
    state_0x9018_0 = var_10;
    rsi5_0 = rsi;
    r12_0 = var_21;
    rdi4_0 = rdi;
    rbp_0 = *var_22;
    rcx1_0 = rcx;
    state_0x9010_0 = var_11;
    local_sp_0 = var_18;
    state_0x82d8_0 = var_14;
    state_0x9080_0 = var_15;
    state_0x8248_0 = var_16;
    rdx_0 = 0UL;
    while (1U)
        {
            var_23 = helper_cc_compute_c_wrapper(rbp_0 - rsi, rsi, var_6, 17U);
            r12_1 = r12_0;
            rdi4_1 = rbp_0;
            local_sp_1 = local_sp_0;
            rdi4_2 = rdi4_0;
            rbp_1 = rbp_0;
            rsi5_1 = rsi5_0;
            local_sp_2 = local_sp_0;
            if (var_23 != 0UL) {
                r12_3 = r12_1;
                r12_2 = r12_1;
                rbp_2 = rbp_1;
                rdi4_3 = rdi4_2;
                rsi5_2 = rsi5_1;
                local_sp_3 = local_sp_2;
                if (rbp_1 != rsi) {
                    loop_state_var = 1U;
                    break;
                }
                var_85 = helper_divq_EAX_wrapper((struct type_3 *)(0UL), var_17, 4278297UL, rbp_2 - rsi, rcx1_0, rbp_2, r10, rsi, 0UL, r9, rdi4_3, rsi5_2, r8, state_0x9018_0, state_0x9010_0, var_12, var_13, state_0x82d8_0, state_0x9080_0, state_0x8248_0);
                var_86 = var_85.field_1;
                var_87 = var_85.field_4;
                var_88 = var_85.field_6;
                var_89 = var_85.field_7;
                var_90 = var_85.field_8;
                var_91 = var_85.field_9;
                var_92 = var_85.field_10;
                var_93 = rbp_2 - var_87;
                var_94 = helper_divq_EAX_wrapper((struct type_3 *)(0UL), var_17, 4278314UL, r12_2, var_86, var_93, r10, rsi, 0UL, r9, rdi4_3, var_87, r8, var_88, var_89, var_12, var_13, var_90, var_91, var_92);
                rdi4_0 = rdi4_3;
                rsi5_0 = var_87;
                rcx1_0 = var_86;
                local_sp_0 = local_sp_3;
                if (r12_2 <= var_93) {
                    var_95 = var_94.field_4;
                    *var_20 = var_94.field_1;
                    *var_22 = var_86;
                    r12_4 = var_95;
                    loop_state_var = 0U;
                    break;
                }
                state_0x9018_0 = var_94.field_6;
                r12_0 = var_94.field_4;
                rbp_0 = var_87 + (-1L);
                state_0x9010_0 = var_94.field_7;
                state_0x82d8_0 = var_94.field_8;
                state_0x9080_0 = var_94.field_9;
                state_0x8248_0 = var_94.field_10;
                continue;
            }
            var_24 = local_sp_1 + (-8L);
            *(uint64_t *)var_24 = 4278357UL;
            var_25 = indirect_placeholder_220(rdi4_1);
            var_26 = var_25.field_0 + 255UL;
            var_27 = rdx_0 + 1UL;
            rdx_0 = var_27;
            rdi4_1 = var_26;
            local_sp_1 = var_24;
            rsi5_1 = local_sp_1;
            rsi5_2 = local_sp_1;
            do {
                var_24 = local_sp_1 + (-8L);
                *(uint64_t *)var_24 = 4278357UL;
                var_25 = indirect_placeholder_220(rdi4_1);
                var_26 = var_25.field_0 + 255UL;
                var_27 = rdx_0 + 1UL;
                rdx_0 = var_27;
                rdi4_1 = var_26;
                local_sp_1 = var_24;
                rsi5_1 = local_sp_1;
                rsi5_2 = local_sp_1;
            } while (var_26 >= rsi);
            var_28 = local_sp_1 + (-16L);
            *(uint64_t *)var_28 = 4278386UL;
            indirect_placeholder_18(var_27, var_19, local_sp_1);
            *(uint64_t *)(local_sp_1 + (-24L)) = 4278394UL;
            var_29 = indirect_placeholder_219(r12_0);
            var_30 = var_29.field_0 + (uint64_t)*(unsigned char *)var_28;
            var_31 = local_sp_1 + (-32L);
            *(uint64_t *)var_31 = 4278411UL;
            var_32 = indirect_placeholder_218(rbp_0);
            var_33 = var_32.field_0;
            var_34 = var_32.field_1;
            var_35 = var_33 + 255UL;
            var_36 = helper_cc_compute_c_wrapper(var_35 - rsi, rsi, var_6, 17U);
            r12_1 = var_30;
            rdi4_2 = var_34;
            rbp_1 = var_35;
            local_sp_2 = var_31;
            if (var_36 != 0UL) {
                r12_3 = r12_1;
                r12_2 = r12_1;
                rbp_2 = rbp_1;
                rdi4_3 = rdi4_2;
                rsi5_2 = rsi5_1;
                local_sp_3 = local_sp_2;
                if (rbp_1 != rsi) {
                    loop_state_var = 1U;
                    break;
                }
            }
            *(uint64_t *)(local_sp_1 + (-40L)) = 4278435UL;
            var_37 = indirect_placeholder_217(var_30);
            var_38 = var_37.field_0 + (uint64_t)*(unsigned char *)(local_sp_1 + (-31L));
            var_39 = local_sp_1 + (-48L);
            *(uint64_t *)var_39 = 4278452UL;
            var_40 = indirect_placeholder_216(var_35);
            var_41 = var_40.field_0;
            var_42 = var_40.field_1;
            var_43 = var_41 + 255UL;
            r12_1 = var_38;
            rdi4_2 = var_42;
            rbp_1 = var_43;
            local_sp_2 = var_39;
            if (var_43 >= rsi) {
                r12_3 = r12_1;
                r12_2 = r12_1;
                rbp_2 = rbp_1;
                rdi4_3 = rdi4_2;
                rsi5_2 = rsi5_1;
                local_sp_3 = local_sp_2;
                if (rbp_1 != rsi) {
                    loop_state_var = 1U;
                    break;
                }
            }
            *(uint64_t *)(local_sp_1 + (-56L)) = 4278476UL;
            var_44 = indirect_placeholder_215(var_38);
            var_45 = var_44.field_0 + (uint64_t)*(unsigned char *)(local_sp_1 + (-46L));
            var_46 = local_sp_1 + (-64L);
            *(uint64_t *)var_46 = 4278493UL;
            var_47 = indirect_placeholder_214(var_43);
            var_48 = var_47.field_0;
            var_49 = var_47.field_1;
            var_50 = var_48 + 255UL;
            r12_1 = var_45;
            rdi4_2 = var_49;
            rbp_1 = var_50;
            local_sp_2 = var_46;
            if (var_50 >= rsi) {
                r12_3 = r12_1;
                r12_2 = r12_1;
                rbp_2 = rbp_1;
                rdi4_3 = rdi4_2;
                rsi5_2 = rsi5_1;
                local_sp_3 = local_sp_2;
                if (rbp_1 != rsi) {
                    loop_state_var = 1U;
                    break;
                }
            }
            *(uint64_t *)(local_sp_1 + (-72L)) = 4278517UL;
            var_51 = indirect_placeholder_213(var_45);
            var_52 = var_51.field_0 + (uint64_t)*(unsigned char *)(local_sp_1 + (-61L));
            var_53 = local_sp_1 + (-80L);
            *(uint64_t *)var_53 = 4278534UL;
            var_54 = indirect_placeholder_212(var_50);
            var_55 = var_54.field_0;
            var_56 = var_54.field_1;
            var_57 = var_55 + 255UL;
            r12_1 = var_52;
            rdi4_2 = var_56;
            rbp_1 = var_57;
            local_sp_2 = var_53;
            if (var_57 >= rsi) {
                r12_3 = r12_1;
                r12_2 = r12_1;
                rbp_2 = rbp_1;
                rdi4_3 = rdi4_2;
                rsi5_2 = rsi5_1;
                local_sp_3 = local_sp_2;
                if (rbp_1 != rsi) {
                    loop_state_var = 1U;
                    break;
                }
            }
            *(uint64_t *)(local_sp_1 + (-88L)) = 4278558UL;
            var_58 = indirect_placeholder_211(var_52);
            var_59 = var_58.field_0 + (uint64_t)*(unsigned char *)(local_sp_1 + (-76L));
            var_60 = local_sp_1 + (-96L);
            *(uint64_t *)var_60 = 4278575UL;
            var_61 = indirect_placeholder_210(var_57);
            var_62 = var_61.field_0;
            var_63 = var_61.field_1;
            var_64 = var_62 + 255UL;
            r12_1 = var_59;
            rdi4_2 = var_63;
            rbp_1 = var_64;
            local_sp_2 = var_60;
            if (var_64 >= rsi) {
                r12_3 = r12_1;
                r12_2 = r12_1;
                rbp_2 = rbp_1;
                rdi4_3 = rdi4_2;
                rsi5_2 = rsi5_1;
                local_sp_3 = local_sp_2;
                if (rbp_1 != rsi) {
                    loop_state_var = 1U;
                    break;
                }
            }
            *(uint64_t *)(local_sp_1 + (-104L)) = 4278599UL;
            var_65 = indirect_placeholder_209(var_59);
            var_66 = var_65.field_0 + (uint64_t)*(unsigned char *)(local_sp_1 + (-91L));
            var_67 = local_sp_1 + (-112L);
            *(uint64_t *)var_67 = 4278616UL;
            var_68 = indirect_placeholder_208(var_64);
            var_69 = var_68.field_0;
            var_70 = var_68.field_1;
            var_71 = var_69 + 255UL;
            r12_1 = var_66;
            rdi4_2 = var_70;
            rbp_1 = var_71;
            local_sp_2 = var_67;
            if (var_71 >= rsi) {
                r12_3 = r12_1;
                r12_2 = r12_1;
                rbp_2 = rbp_1;
                rdi4_3 = rdi4_2;
                rsi5_2 = rsi5_1;
                local_sp_3 = local_sp_2;
                if (rbp_1 != rsi) {
                    loop_state_var = 1U;
                    break;
                }
            }
            *(uint64_t *)(local_sp_1 + (-120L)) = 4278640UL;
            var_72 = indirect_placeholder_207(var_66);
            var_73 = var_72.field_0 + (uint64_t)*(unsigned char *)(local_sp_1 + (-106L));
            var_74 = local_sp_1 + (-128L);
            *(uint64_t *)var_74 = 4278657UL;
            var_75 = indirect_placeholder_206(var_71);
            var_76 = var_75.field_0;
            var_77 = var_75.field_1;
            var_78 = var_76 + 255UL;
            r12_1 = var_73;
            rdi4_2 = var_77;
            rbp_1 = var_78;
            local_sp_2 = var_74;
            if (var_78 >= rsi) {
                r12_3 = r12_1;
                r12_2 = r12_1;
                rbp_2 = rbp_1;
                rdi4_3 = rdi4_2;
                rsi5_2 = rsi5_1;
                local_sp_3 = local_sp_2;
                if (rbp_1 != rsi) {
                    loop_state_var = 1U;
                    break;
                }
            }
            *(uint64_t *)(local_sp_1 + (-136L)) = 4278681UL;
            var_79 = indirect_placeholder_205(var_73);
            var_80 = var_79.field_0 + (uint64_t)*(unsigned char *)(local_sp_1 + (-121L));
            var_81 = local_sp_1 + (-144L);
            *(uint64_t *)var_81 = 4278698UL;
            var_82 = indirect_placeholder_204(var_78);
            var_83 = var_82.field_1;
            var_84 = var_82.field_0 + 255UL;
            r12_3 = var_80;
            r12_2 = var_80;
            rbp_2 = var_84;
            rdi4_3 = var_83;
            local_sp_3 = var_81;
            if (var_84 != rsi) {
                loop_state_var = 1U;
                break;
            }
        }
    switch (loop_state_var) {
      case 0U:
        {
            return r12_4;
        }
        break;
      case 1U:
        {
            var_96 = helper_pxor_xmm_wrapper_281((struct type_3 *)(0UL), (struct type_5 *)(776UL), (struct type_5 *)(776UL), var_8, var_9);
            var_97 = var_96.field_1;
            *var_20 = var_96.field_0;
            *var_22 = var_97;
            r12_4 = r12_3;
        }
        break;
    }
}
