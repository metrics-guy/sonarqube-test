typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_key_warnings_ret_type;
struct indirect_placeholder_300_ret_type;
struct indirect_placeholder_301_ret_type;
struct indirect_placeholder_302_ret_type;
struct indirect_placeholder_303_ret_type;
struct indirect_placeholder_307_ret_type;
struct indirect_placeholder_306_ret_type;
struct indirect_placeholder_305_ret_type;
struct indirect_placeholder_308_ret_type;
struct indirect_placeholder_304_ret_type;
struct indirect_placeholder_309_ret_type;
struct indirect_placeholder_310_ret_type;
struct indirect_placeholder_311_ret_type;
struct bb_key_warnings_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_300_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_301_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_302_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_303_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_307_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
    uint64_t field_8;
    uint64_t field_9;
};
struct indirect_placeholder_306_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
    uint64_t field_8;
    uint64_t field_9;
};
struct indirect_placeholder_305_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_308_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_304_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_309_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_310_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_311_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t indirect_placeholder_7(uint64_t param_0);
extern uint32_t init_state_0x82fc(void);
extern void indirect_placeholder_5(uint64_t param_0, uint64_t param_1);
extern uint64_t init_rcx(void);
extern uint64_t init_r10(void);
extern struct indirect_placeholder_300_ret_type indirect_placeholder_300(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_301_ret_type indirect_placeholder_301(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_302_ret_type indirect_placeholder_302(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_303_ret_type indirect_placeholder_303(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_307_ret_type indirect_placeholder_307(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
extern struct indirect_placeholder_306_ret_type indirect_placeholder_306(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
extern struct indirect_placeholder_305_ret_type indirect_placeholder_305(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_308_ret_type indirect_placeholder_308(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_304_ret_type indirect_placeholder_304(uint64_t param_0);
extern struct indirect_placeholder_309_ret_type indirect_placeholder_309(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_310_ret_type indirect_placeholder_310(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_311_ret_type indirect_placeholder_311(uint64_t param_0);
struct bb_key_warnings_ret_type bb_key_warnings(uint64_t r9, uint64_t rdi, uint64_t rsi, uint64_t r8) {
    struct indirect_placeholder_300_ret_type var_140;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint32_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t rdi5_0;
    uint64_t rcx_0;
    uint64_t rcx_1;
    uint64_t r87_5;
    uint64_t rcx_11;
    uint64_t local_sp_6;
    unsigned char var_120;
    uint64_t var_121;
    uint64_t var_122;
    uint64_t var_123;
    uint64_t var_124;
    uint64_t rcx_3;
    uint64_t r14_0;
    uint64_t local_sp_0;
    uint64_t storemerge3;
    uint64_t var_139;
    uint64_t r87_9;
    unsigned char rbp_3_in;
    uint64_t var_141;
    uint64_t var_142;
    uint64_t var_143;
    uint64_t rcx_12;
    uint64_t r94_2;
    uint64_t rdi5_1;
    uint64_t rcx_2;
    unsigned char var_133;
    uint64_t var_134;
    uint64_t var_135;
    uint64_t var_136;
    uint64_t var_137;
    uint64_t var_138;
    uint64_t r94_0;
    uint64_t r87_0;
    uint64_t rcx_4;
    uint64_t local_sp_1;
    uint64_t rcx_6;
    unsigned char _pr;
    uint64_t r87_1;
    unsigned char _pr_pre;
    uint64_t local_sp_2;
    uint64_t storemerge4;
    uint64_t storemerge;
    uint64_t var_125;
    struct indirect_placeholder_301_ret_type var_126;
    uint64_t var_127;
    uint64_t var_128;
    uint64_t var_129;
    uint64_t var_111;
    struct indirect_placeholder_302_ret_type var_112;
    uint64_t var_113;
    uint64_t var_114;
    uint64_t var_115;
    uint64_t rdi5_2;
    uint64_t rcx_5;
    uint64_t r87_7;
    unsigned char var_106;
    uint64_t var_107;
    uint64_t var_108;
    uint64_t var_109;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_110;
    uint64_t local_sp_3267;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t var_105;
    unsigned char var_116;
    uint64_t var_117;
    uint64_t var_118;
    uint64_t var_119;
    uint64_t r94_1_ph;
    uint64_t r87_2_ph;
    uint64_t local_sp_10;
    uint64_t rcx_7_ph;
    uint64_t local_sp_3_ph;
    uint64_t rcx_7266;
    uint64_t r87_2265;
    uint64_t r94_1264;
    struct indirect_placeholder_303_ret_type var_144;
    uint64_t var_145;
    uint64_t var_146;
    uint64_t var_147;
    uint64_t r87_3;
    uint64_t rcx_8;
    struct bb_key_warnings_ret_type mrv;
    struct bb_key_warnings_ret_type mrv1;
    struct bb_key_warnings_ret_type mrv2;
    struct bb_key_warnings_ret_type mrv3;
    uint64_t var_130;
    uint64_t var_131;
    uint64_t var_132;
    uint64_t r12_1;
    uint64_t r94_7;
    uint64_t rcx_9;
    uint64_t local_sp_9;
    uint64_t r87_8;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t r12_0;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t rbp_0;
    uint64_t local_sp_4;
    uint64_t r87_6;
    uint64_t rbx_0;
    struct indirect_placeholder_307_ret_type var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    struct indirect_placeholder_306_ret_type var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    struct indirect_placeholder_305_ret_type var_55;
    uint64_t r94_6;
    uint64_t _pre_phi272;
    uint64_t rbx_1;
    uint64_t rdx_0;
    uint64_t rbp_2;
    struct indirect_placeholder_308_ret_type var_61;
    uint64_t var_62;
    struct indirect_placeholder_304_ret_type var_66;
    uint64_t r94_4;
    uint64_t r94_3;
    uint64_t r87_4;
    uint64_t rcx_10;
    uint64_t local_sp_5;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    struct indirect_placeholder_309_ret_type var_71;
    uint64_t *var_72;
    uint64_t var_73;
    uint64_t *var_74;
    uint64_t var_75;
    uint64_t r94_8;
    unsigned char var_76;
    unsigned char var_77;
    unsigned char *var_78;
    unsigned char var_79;
    unsigned char var_80;
    unsigned char *var_81;
    unsigned char var_82;
    unsigned char var_83;
    unsigned char *var_84;
    unsigned char var_85;
    unsigned char *var_86;
    unsigned char var_87;
    unsigned char *var_88;
    unsigned char var_89;
    unsigned char *var_90;
    unsigned char var_91;
    unsigned char *var_92;
    unsigned char var_93;
    unsigned char var_94;
    unsigned char *var_95;
    unsigned char var_96;
    uint64_t var_97;
    unsigned char var_98;
    unsigned char *var_99;
    uint64_t r94_5;
    uint64_t r13_0;
    uint64_t local_sp_7;
    unsigned char var_22;
    uint64_t var_23;
    uint64_t *var_24;
    uint64_t var_25;
    bool var_26;
    uint64_t spec_select;
    uint64_t spec_select268;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t *var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t rdi5_4;
    uint64_t r13_1;
    uint64_t rbp_1;
    uint64_t local_sp_8;
    uint64_t var_56;
    struct indirect_placeholder_310_ret_type var_57;
    uint64_t var_58;
    struct indirect_placeholder_311_ret_type var_59;
    uint64_t var_60;
    uint64_t var_100;
    uint64_t var_101;
    uint64_t var_102;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rcx();
    var_2 = init_rbp();
    var_3 = init_r15();
    var_4 = init_r14();
    var_5 = init_r12();
    var_6 = init_r10();
    var_7 = init_rbx();
    var_8 = init_r13();
    var_9 = init_cc_src2();
    var_10 = init_state_0x82fc();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_8;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    *(uint64_t *)(var_0 + (-48L)) = var_7;
    var_11 = var_0 + (-280L);
    var_12 = *(uint64_t *)rdi;
    var_13 = *(uint64_t *)(rdi + 8UL);
    var_14 = *(uint64_t *)(rdi + 64UL);
    var_15 = *(uint64_t *)(rdi + 16UL);
    var_16 = *(uint64_t *)(rdi + 24UL);
    var_17 = *(uint64_t *)(rdi + 32UL);
    var_18 = *(uint64_t *)(rdi + 40UL);
    var_19 = *(uint64_t *)(rdi + 48UL);
    var_20 = *(uint64_t *)(rdi + 56UL);
    var_21 = *(uint64_t *)4368336UL;
    *(uint64_t *)(var_0 + (-72L)) = var_14;
    *(uint64_t *)(var_0 + (-136L)) = var_12;
    *(uint64_t *)(var_0 + (-128L)) = var_13;
    *(uint64_t *)(var_0 + (-120L)) = var_15;
    *(uint64_t *)(var_0 + (-112L)) = var_16;
    *(uint64_t *)(var_0 + (-104L)) = var_17;
    *(uint64_t *)(var_0 + (-96L)) = var_18;
    *(uint64_t *)(var_0 + (-88L)) = var_19;
    *(uint64_t *)(var_0 + (-80L)) = var_20;
    rcx_0 = 18446744073709551615UL;
    rcx_1 = 0UL;
    rcx_3 = 0UL;
    storemerge3 = 4326537UL;
    r87_9 = r8;
    rcx_12 = var_1;
    rcx_2 = 18446744073709551615UL;
    rcx_6 = 0UL;
    storemerge = 4326563UL;
    rcx_5 = 18446744073709551615UL;
    local_sp_10 = var_11;
    rcx_9 = 1UL;
    r12_0 = 1UL;
    r87_6 = r8;
    rbx_0 = var_21;
    r94_8 = r9;
    r94_5 = r9;
    local_sp_7 = var_11;
    if (var_21 != 0UL) {
        r13_0 = (uint64_t)(uint32_t)rsi;
        while (1U)
            {
                var_22 = *(unsigned char *)(rbx_0 + 57UL);
                var_23 = *(uint64_t *)rbx_0;
                var_24 = (uint64_t *)(rbx_0 + 16UL);
                var_25 = *var_24;
                r87_7 = r87_6;
                r12_1 = r12_0;
                rbp_0 = var_25;
                r94_6 = r94_5;
                rbx_1 = rbx_0;
                rdi5_4 = var_23;
                r13_1 = r13_0;
                rbp_1 = var_25;
                local_sp_8 = local_sp_7;
                if (var_22 != '\x00') {
                    var_26 = (var_23 == 18446744073709551615UL);
                    spec_select = var_26 ? 0UL : var_23;
                    spec_select268 = var_26 ? 1UL : (var_23 + 1UL);
                    var_27 = local_sp_7 + 16UL;
                    *(uint64_t *)(local_sp_7 + (-8L)) = 4218555UL;
                    var_28 = indirect_placeholder_1(spec_select, var_27);
                    *(unsigned char *)(local_sp_7 + 40UL) = (unsigned char)'+';
                    var_29 = local_sp_7 + (-16L);
                    *(uint64_t *)var_29 = 4218573UL;
                    indirect_placeholder();
                    var_30 = local_sp_7 + 80UL;
                    var_31 = (uint64_t *)(local_sp_7 + (-24L));
                    *var_31 = 4218594UL;
                    var_32 = indirect_placeholder_1(spec_select268, local_sp_7);
                    *(uint32_t *)(local_sp_7 + 72UL) = 2124589U;
                    var_33 = local_sp_7 + (-32L);
                    *(uint64_t *)var_33 = 4218615UL;
                    indirect_placeholder();
                    var_34 = *var_24;
                    *var_31 = var_32;
                    local_sp_4 = var_33;
                    if (var_34 != 18446744073709551615UL) {
                        var_35 = var_25 + 1UL;
                        *(uint64_t *)(local_sp_7 + (-40L)) = 4218644UL;
                        indirect_placeholder_1(var_35, var_29);
                        *(uint16_t *)var_28 = (uint16_t)(unsigned short)11552U;
                        var_36 = local_sp_7 + (-48L);
                        *(uint64_t *)var_36 = 4218665UL;
                        indirect_placeholder();
                        var_37 = var_35 + (*(uint64_t *)(rbx_0 + 24UL) == 18446744073709551615UL);
                        *(uint64_t *)(local_sp_7 + (-56L)) = 4218689UL;
                        indirect_placeholder_1(var_37, var_33);
                        **(unsigned char **)var_36 = (unsigned char)',';
                        var_38 = local_sp_7 + (-64L);
                        *(uint64_t *)var_38 = 4218709UL;
                        indirect_placeholder();
                        rbp_0 = var_35;
                        local_sp_4 = var_38;
                    }
                    *(uint64_t *)(local_sp_4 + (-8L)) = 4218722UL;
                    var_39 = indirect_placeholder_307(rbp_0, var_28, var_30, r12_0, rbx_0, 1UL, r13_0, var_30);
                    var_40 = var_39.field_0;
                    var_41 = var_39.field_3;
                    var_42 = var_39.field_4;
                    var_43 = var_39.field_5;
                    var_44 = var_39.field_6;
                    var_45 = var_39.field_9;
                    var_46 = local_sp_4 + 40UL;
                    *(uint64_t *)(local_sp_4 + (-16L)) = 4218737UL;
                    var_47 = indirect_placeholder_306(var_40, var_41, var_42, var_43, var_44, 0UL, var_45, var_46);
                    var_48 = var_47.field_0;
                    var_49 = var_47.field_2;
                    var_50 = var_47.field_5;
                    var_51 = var_47.field_6;
                    var_52 = var_47.field_8;
                    var_53 = var_47.field_9;
                    var_54 = local_sp_4 + (-24L);
                    *(uint64_t *)var_54 = 4218759UL;
                    var_55 = indirect_placeholder_305(0UL, var_48, 4321440UL, var_52, 0UL, 0UL, var_49);
                    r87_7 = var_55.field_3;
                    r12_1 = var_50;
                    r94_6 = var_55.field_1;
                    rbx_1 = var_51;
                    rdi5_4 = *(uint64_t *)var_51;
                    r13_1 = var_53;
                    rbp_1 = *(uint64_t *)(var_51 + 16UL);
                    local_sp_8 = var_54;
                }
                r94_7 = r94_6;
                local_sp_9 = local_sp_8;
                r87_8 = r87_7;
                rdx_0 = (uint64_t)(uint32_t)r13_1;
                rbp_2 = rbp_1;
                r13_0 = r13_1;
                if ((rdi5_4 != 18446744073709551615UL) && (rdi5_4 > rbp_1)) {
                    var_56 = local_sp_8 + (-8L);
                    *(uint64_t *)var_56 = 4218799UL;
                    var_57 = indirect_placeholder_310(0UL, r12_1, 4321664UL, r94_6, 0UL, 0UL, r87_7);
                    r94_7 = var_57.field_1;
                    local_sp_9 = var_56;
                    r87_8 = var_57.field_3;
                    rdx_0 = 1UL;
                    rbp_2 = *(uint64_t *)(rbx_1 + 16UL);
                }
                var_58 = local_sp_9 + (-8L);
                *(uint64_t *)var_58 = 4218816UL;
                var_59 = indirect_placeholder_311(rbx_1);
                var_60 = (uint64_t)(unsigned char)var_59.field_0;
                r87_5 = r87_8;
                local_sp_6 = var_58;
                _pre_phi272 = var_60;
                r94_4 = r94_7;
                r94_3 = r94_7;
                r87_4 = r87_8;
                local_sp_5 = var_58;
                if (var_60 == 0UL) {
                    rcx_9 = (uint64_t)*(unsigned char *)(rbx_1 + 54UL);
                }
                rcx_10 = rcx_9;
                rcx_11 = rcx_9;
                if (rbp_2 == 0UL) {
                    if (*(uint64_t *)(rbx_1 + 24UL) == 0UL) {
                        if ((uint64_t)(unsigned char)rdx_0 == 0UL) {
                            if (*(uint32_t *)4367460UL != 128U) {
                                if (*(unsigned char *)(rbx_1 + 48UL) == '\x00') {
                                    if (rcx_9 == 0UL) {
                                        *(uint64_t *)(local_sp_9 + (-16L)) = 4218908UL;
                                        var_61 = indirect_placeholder_308(0UL, r12_1, 4321488UL, r94_7, 0UL, 0UL, r87_8);
                                        var_62 = var_61.field_0;
                                        var_63 = var_61.field_1;
                                        var_64 = var_61.field_3;
                                        var_65 = local_sp_9 + (-24L);
                                        *(uint64_t *)var_65 = 4218916UL;
                                        var_66 = indirect_placeholder_304(rbx_1);
                                        _pre_phi272 = (uint64_t)(unsigned char)var_66.field_0;
                                        r94_3 = var_63;
                                        r87_4 = var_64;
                                        rcx_10 = var_62;
                                        local_sp_5 = var_65;
                                    } else {
                                        if (*(uint64_t *)(rbx_1 + 8UL) == 0UL) {
                                            *(uint64_t *)(local_sp_9 + (-16L)) = 4218908UL;
                                            var_61 = indirect_placeholder_308(0UL, r12_1, 4321488UL, r94_7, 0UL, 0UL, r87_8);
                                            var_62 = var_61.field_0;
                                            var_63 = var_61.field_1;
                                            var_64 = var_61.field_3;
                                            var_65 = local_sp_9 + (-24L);
                                            *(uint64_t *)var_65 = 4218916UL;
                                            var_66 = indirect_placeholder_304(rbx_1);
                                            _pre_phi272 = (uint64_t)(unsigned char)var_66.field_0;
                                            r94_3 = var_63;
                                            r87_4 = var_64;
                                            rcx_10 = var_62;
                                            local_sp_5 = var_65;
                                        } else {
                                            if (*(unsigned char *)(rbx_1 + 49UL) != '\x00' & *(uint64_t *)(rbx_1 + 24UL) == 0UL) {
                                                *(uint64_t *)(local_sp_9 + (-16L)) = 4218908UL;
                                                var_61 = indirect_placeholder_308(0UL, r12_1, 4321488UL, r94_7, 0UL, 0UL, r87_8);
                                                var_62 = var_61.field_0;
                                                var_63 = var_61.field_1;
                                                var_64 = var_61.field_3;
                                                var_65 = local_sp_9 + (-24L);
                                                *(uint64_t *)var_65 = 4218916UL;
                                                var_66 = indirect_placeholder_304(rbx_1);
                                                _pre_phi272 = (uint64_t)(unsigned char)var_66.field_0;
                                                r94_3 = var_63;
                                                r87_4 = var_64;
                                                rcx_10 = var_62;
                                                local_sp_5 = var_65;
                                            }
                                        }
                                    }
                                } else {
                                    if (*(unsigned char *)(rbx_1 + 49UL) != '\x00' & *(uint64_t *)(rbx_1 + 24UL) == 0UL) {
                                        *(uint64_t *)(local_sp_9 + (-16L)) = 4218908UL;
                                        var_61 = indirect_placeholder_308(0UL, r12_1, 4321488UL, r94_7, 0UL, 0UL, r87_8);
                                        var_62 = var_61.field_0;
                                        var_63 = var_61.field_1;
                                        var_64 = var_61.field_3;
                                        var_65 = local_sp_9 + (-24L);
                                        *(uint64_t *)var_65 = 4218916UL;
                                        var_66 = indirect_placeholder_304(rbx_1);
                                        _pre_phi272 = (uint64_t)(unsigned char)var_66.field_0;
                                        r94_3 = var_63;
                                        r87_4 = var_64;
                                        rcx_10 = var_62;
                                        local_sp_5 = var_65;
                                    }
                                }
                            }
                            r94_4 = r94_3;
                            r87_5 = r87_4;
                            rcx_11 = rcx_10;
                            local_sp_6 = local_sp_5;
                            var_67 = *(uint64_t *)(rbx_1 + 16UL) + 1UL;
                            var_68 = *(uint64_t *)rbx_1 + 1UL;
                            var_69 = helper_cc_compute_c_wrapper(((var_68 == 0UL) ? 1UL : var_68) - var_67, var_67, var_9, 17U);
                            rcx_11 = 1UL;
                            if (_pre_phi272 != 0UL & (var_69 != 0UL) || (var_67 == 0UL)) {
                                var_70 = local_sp_5 + (-8L);
                                *(uint64_t *)var_70 = 4219339UL;
                                var_71 = indirect_placeholder_309(0UL, r12_1, 4321560UL, r94_3, 0UL, 0UL, r87_4);
                                r94_4 = var_71.field_1;
                                r87_5 = var_71.field_3;
                                rcx_11 = var_71.field_0;
                                local_sp_6 = var_70;
                            }
                        } else {
                            r94_4 = r94_3;
                            r87_5 = r87_4;
                            rcx_11 = rcx_10;
                            local_sp_6 = local_sp_5;
                            var_67 = *(uint64_t *)(rbx_1 + 16UL) + 1UL;
                            var_68 = *(uint64_t *)rbx_1 + 1UL;
                            var_69 = helper_cc_compute_c_wrapper(((var_68 == 0UL) ? 1UL : var_68) - var_67, var_67, var_9, 17U);
                            rcx_11 = 1UL;
                            if ((uint64_t)(unsigned char)r13_1 != 0UL & _pre_phi272 != 0UL & (var_69 != 0UL) || (var_67 == 0UL)) {
                                var_70 = local_sp_5 + (-8L);
                                *(uint64_t *)var_70 = 4219339UL;
                                var_71 = indirect_placeholder_309(0UL, r12_1, 4321560UL, r94_3, 0UL, 0UL, r87_4);
                                r94_4 = var_71.field_1;
                                r87_5 = var_71.field_3;
                                rcx_11 = var_71.field_0;
                                local_sp_6 = var_70;
                            }
                        }
                    } else {
                        if ((uint64_t)(unsigned char)rdx_0 == 0UL) {
                            r94_4 = r94_3;
                            r87_5 = r87_4;
                            rcx_11 = rcx_10;
                            local_sp_6 = local_sp_5;
                            var_67 = *(uint64_t *)(rbx_1 + 16UL) + 1UL;
                            var_68 = *(uint64_t *)rbx_1 + 1UL;
                            var_69 = helper_cc_compute_c_wrapper(((var_68 == 0UL) ? 1UL : var_68) - var_67, var_67, var_9, 17U);
                            rcx_11 = 1UL;
                            if ((uint64_t)(unsigned char)r13_1 != 0UL & _pre_phi272 != 0UL & (var_69 != 0UL) || (var_67 == 0UL)) {
                                var_70 = local_sp_5 + (-8L);
                                *(uint64_t *)var_70 = 4219339UL;
                                var_71 = indirect_placeholder_309(0UL, r12_1, 4321560UL, r94_3, 0UL, 0UL, r87_4);
                                r94_4 = var_71.field_1;
                                r87_5 = var_71.field_3;
                                rcx_11 = var_71.field_0;
                                local_sp_6 = var_70;
                            }
                        } else {
                            r94_4 = r94_3;
                            r87_5 = r87_4;
                            rcx_11 = rcx_10;
                            local_sp_6 = local_sp_5;
                            var_67 = *(uint64_t *)(rbx_1 + 16UL) + 1UL;
                            var_68 = *(uint64_t *)rbx_1 + 1UL;
                            var_69 = helper_cc_compute_c_wrapper(((var_68 == 0UL) ? 1UL : var_68) - var_67, var_67, var_9, 17U);
                            rcx_11 = 1UL;
                            if (_pre_phi272 != 0UL & (var_69 != 0UL) || (var_67 == 0UL)) {
                                var_70 = local_sp_5 + (-8L);
                                *(uint64_t *)var_70 = 4219339UL;
                                var_71 = indirect_placeholder_309(0UL, r12_1, 4321560UL, r94_3, 0UL, 0UL, r87_4);
                                r94_4 = var_71.field_1;
                                r87_5 = var_71.field_3;
                                rcx_11 = var_71.field_0;
                                local_sp_6 = var_70;
                            }
                        }
                    }
                } else {
                    if ((uint64_t)(unsigned char)rdx_0 == 0UL) {
                        r94_4 = r94_3;
                        r87_5 = r87_4;
                        rcx_11 = rcx_10;
                        local_sp_6 = local_sp_5;
                        var_67 = *(uint64_t *)(rbx_1 + 16UL) + 1UL;
                        var_68 = *(uint64_t *)rbx_1 + 1UL;
                        var_69 = helper_cc_compute_c_wrapper(((var_68 == 0UL) ? 1UL : var_68) - var_67, var_67, var_9, 17U);
                        rcx_11 = 1UL;
                        if ((uint64_t)(unsigned char)r13_1 != 0UL & _pre_phi272 != 0UL & (var_69 != 0UL) || (var_67 == 0UL)) {
                            var_70 = local_sp_5 + (-8L);
                            *(uint64_t *)var_70 = 4219339UL;
                            var_71 = indirect_placeholder_309(0UL, r12_1, 4321560UL, r94_3, 0UL, 0UL, r87_4);
                            r94_4 = var_71.field_1;
                            r87_5 = var_71.field_3;
                            rcx_11 = var_71.field_0;
                            local_sp_6 = var_70;
                        }
                    } else {
                        if (*(uint32_t *)4367460UL != 128U) {
                            if (*(unsigned char *)(rbx_1 + 48UL) == '\x00') {
                                if (rcx_9 == 0UL) {
                                    *(uint64_t *)(local_sp_9 + (-16L)) = 4218908UL;
                                    var_61 = indirect_placeholder_308(0UL, r12_1, 4321488UL, r94_7, 0UL, 0UL, r87_8);
                                    var_62 = var_61.field_0;
                                    var_63 = var_61.field_1;
                                    var_64 = var_61.field_3;
                                    var_65 = local_sp_9 + (-24L);
                                    *(uint64_t *)var_65 = 4218916UL;
                                    var_66 = indirect_placeholder_304(rbx_1);
                                    _pre_phi272 = (uint64_t)(unsigned char)var_66.field_0;
                                    r94_3 = var_63;
                                    r87_4 = var_64;
                                    rcx_10 = var_62;
                                    local_sp_5 = var_65;
                                } else {
                                    if (*(uint64_t *)(rbx_1 + 8UL) == 0UL) {
                                        *(uint64_t *)(local_sp_9 + (-16L)) = 4218908UL;
                                        var_61 = indirect_placeholder_308(0UL, r12_1, 4321488UL, r94_7, 0UL, 0UL, r87_8);
                                        var_62 = var_61.field_0;
                                        var_63 = var_61.field_1;
                                        var_64 = var_61.field_3;
                                        var_65 = local_sp_9 + (-24L);
                                        *(uint64_t *)var_65 = 4218916UL;
                                        var_66 = indirect_placeholder_304(rbx_1);
                                        _pre_phi272 = (uint64_t)(unsigned char)var_66.field_0;
                                        r94_3 = var_63;
                                        r87_4 = var_64;
                                        rcx_10 = var_62;
                                        local_sp_5 = var_65;
                                    } else {
                                        if (*(unsigned char *)(rbx_1 + 49UL) != '\x00' & *(uint64_t *)(rbx_1 + 24UL) == 0UL) {
                                            *(uint64_t *)(local_sp_9 + (-16L)) = 4218908UL;
                                            var_61 = indirect_placeholder_308(0UL, r12_1, 4321488UL, r94_7, 0UL, 0UL, r87_8);
                                            var_62 = var_61.field_0;
                                            var_63 = var_61.field_1;
                                            var_64 = var_61.field_3;
                                            var_65 = local_sp_9 + (-24L);
                                            *(uint64_t *)var_65 = 4218916UL;
                                            var_66 = indirect_placeholder_304(rbx_1);
                                            _pre_phi272 = (uint64_t)(unsigned char)var_66.field_0;
                                            r94_3 = var_63;
                                            r87_4 = var_64;
                                            rcx_10 = var_62;
                                            local_sp_5 = var_65;
                                        }
                                    }
                                }
                            } else {
                                if (*(unsigned char *)(rbx_1 + 49UL) != '\x00' & *(uint64_t *)(rbx_1 + 24UL) == 0UL) {
                                    *(uint64_t *)(local_sp_9 + (-16L)) = 4218908UL;
                                    var_61 = indirect_placeholder_308(0UL, r12_1, 4321488UL, r94_7, 0UL, 0UL, r87_8);
                                    var_62 = var_61.field_0;
                                    var_63 = var_61.field_1;
                                    var_64 = var_61.field_3;
                                    var_65 = local_sp_9 + (-24L);
                                    *(uint64_t *)var_65 = 4218916UL;
                                    var_66 = indirect_placeholder_304(rbx_1);
                                    _pre_phi272 = (uint64_t)(unsigned char)var_66.field_0;
                                    r94_3 = var_63;
                                    r87_4 = var_64;
                                    rcx_10 = var_62;
                                    local_sp_5 = var_65;
                                }
                            }
                        }
                        r94_4 = r94_3;
                        r87_5 = r87_4;
                        rcx_11 = rcx_10;
                        local_sp_6 = local_sp_5;
                        var_67 = *(uint64_t *)(rbx_1 + 16UL) + 1UL;
                        var_68 = *(uint64_t *)rbx_1 + 1UL;
                        var_69 = helper_cc_compute_c_wrapper(((var_68 == 0UL) ? 1UL : var_68) - var_67, var_67, var_9, 17U);
                        rcx_11 = 1UL;
                        if (_pre_phi272 != 0UL & (var_69 != 0UL) || (var_67 == 0UL)) {
                            var_70 = local_sp_5 + (-8L);
                            *(uint64_t *)var_70 = 4219339UL;
                            var_71 = indirect_placeholder_309(0UL, r12_1, 4321560UL, r94_3, 0UL, 0UL, r87_4);
                            r94_4 = var_71.field_1;
                            r87_5 = var_71.field_3;
                            rcx_11 = var_71.field_0;
                            local_sp_6 = var_70;
                        }
                    }
                }
                var_72 = (uint64_t *)(local_sp_6 + 176UL);
                var_73 = *var_72;
                r87_9 = r87_5;
                rcx_12 = rcx_11;
                local_sp_10 = local_sp_6;
                r87_6 = r87_5;
                r94_8 = r94_4;
                r94_5 = r94_4;
                local_sp_7 = local_sp_6;
                if (var_73 != 0UL & var_73 == *(uint64_t *)(rbx_1 + 32UL)) {
                    *var_72 = 0UL;
                }
                var_74 = (uint64_t *)(local_sp_6 + 184UL);
                var_75 = *var_74;
                if (var_75 != 0UL & var_75 == *(uint64_t *)(rbx_1 + 40UL)) {
                    *var_74 = 0UL;
                }
                var_76 = *(unsigned char *)(rbx_1 + 55UL);
                var_77 = *(unsigned char *)(rbx_1 + 48UL) ^ '\x01';
                var_78 = (unsigned char *)(local_sp_6 + 192UL);
                *var_78 = (var_77 & *var_78);
                var_79 = *(unsigned char *)(rbx_1 + 49UL);
                var_80 = var_76 ^ '\x01';
                var_81 = (unsigned char *)(local_sp_6 + 199UL);
                var_82 = *var_81 & var_80;
                var_83 = var_79 ^ '\x01';
                var_84 = (unsigned char *)(local_sp_6 + 193UL);
                *var_84 = (var_83 & *var_84);
                var_85 = *(unsigned char *)(rbx_1 + 54UL) ^ '\x01';
                var_86 = (unsigned char *)(local_sp_6 + 198UL);
                *var_86 = (var_85 & *var_86);
                var_87 = *(unsigned char *)(rbx_1 + 50UL) ^ '\x01';
                var_88 = (unsigned char *)(local_sp_6 + 194UL);
                *var_88 = (var_87 & *var_88);
                var_89 = *(unsigned char *)(rbx_1 + 52UL) ^ '\x01';
                var_90 = (unsigned char *)(local_sp_6 + 196UL);
                *var_90 = (var_89 & *var_90);
                var_91 = *(unsigned char *)(rbx_1 + 53UL) ^ '\x01';
                var_92 = (unsigned char *)(local_sp_6 + 197UL);
                *var_92 = (var_91 & *var_92);
                var_93 = *(unsigned char *)(rbx_1 + 51UL);
                *var_81 = var_82;
                var_94 = var_93 ^ '\x01';
                var_95 = (unsigned char *)(local_sp_6 + 195UL);
                *var_95 = (var_94 & *var_95);
                var_96 = *(unsigned char *)(rbx_1 + 56UL);
                var_97 = *(uint64_t *)(rbx_1 + 64UL);
                var_98 = var_96 ^ '\x01';
                var_99 = (unsigned char *)(local_sp_6 + 200UL);
                *var_99 = (*var_99 & var_98);
                rbx_0 = var_97;
                rbp_3_in = var_82;
                if (var_97 == 0UL) {
                    break;
                }
                r12_0 = r12_1 + 1UL;
                continue;
            }
    }
    rbp_3_in = *(unsigned char *)(var_0 + (-81L));
    var_100 = local_sp_10 + 144UL;
    var_101 = local_sp_10 + (-8L);
    *(uint64_t *)var_101 = 4219013UL;
    var_102 = indirect_placeholder_7(var_100);
    r94_2 = r94_8;
    r87_1 = r87_9;
    local_sp_3267 = var_101;
    r94_1_ph = r94_8;
    r87_2_ph = r87_9;
    rcx_7_ph = rcx_12;
    local_sp_3_ph = var_101;
    rcx_7266 = rcx_12;
    r87_2265 = r87_9;
    r94_1264 = r94_8;
    r87_3 = r87_9;
    rcx_8 = rcx_12;
    if ((uint64_t)(unsigned char)var_102 == 0UL) {
        if (*(unsigned char *)4368346UL != '\x00' & *(unsigned char *)4368345UL == '\x00') {
            *(unsigned char *)(local_sp_10 + 191UL) = (unsigned char)'\x00';
        }
        var_130 = local_sp_10 + 88UL;
        var_131 = local_sp_10 + 136UL;
        *(uint64_t *)(local_sp_10 + (-16L)) = 4219186UL;
        indirect_placeholder_5(var_131, var_130);
        var_132 = (uint64_t)var_10;
        r14_0 = var_130;
        rdi5_1 = var_130;
        while (rcx_2 != 0UL)
            {
                var_133 = *(unsigned char *)rdi5_1;
                var_134 = rcx_2 + (-1L);
                rcx_2 = var_134;
                rcx_3 = var_134;
                if (var_133 == '\x00') {
                    break;
                }
                rdi5_1 = rdi5_1 + var_132;
            }
        var_135 = 18446744073709551614UL - (-2L);
        var_136 = local_sp_10 + (-24L);
        *(uint64_t *)var_136 = 4219209UL;
        var_137 = indirect_placeholder_7(var_135);
        var_138 = (var_137 == 1UL) ? 4326563UL : 4326537UL;
        local_sp_0 = var_136;
        storemerge3 = var_138;
        var_139 = local_sp_0 + (-8L);
        *(uint64_t *)var_139 = 4219241UL;
        var_140 = indirect_placeholder_300(0UL, r14_0, storemerge3, r94_8, 0UL, 0UL, r87_9);
        var_141 = var_140.field_0;
        var_142 = var_140.field_1;
        var_143 = var_140.field_3;
        *(unsigned char *)(local_sp_0 + 191UL) = rbp_3_in;
        r94_2 = var_142;
        r94_0 = var_142;
        r87_0 = var_143;
        rcx_4 = var_141;
        local_sp_1 = var_139;
        r87_3 = var_143;
        rcx_8 = var_141;
        if (rbp_3_in == '\x00') {
            mrv.field_0 = rcx_8;
            mrv1 = mrv;
            mrv1.field_1 = var_6;
            mrv2 = mrv1;
            mrv2.field_2 = r94_2;
            mrv3 = mrv2;
            mrv3.field_3 = r87_3;
            return mrv3;
        }
    }
    if (rbp_3_in == '\x00') {
        mrv.field_0 = rcx_8;
        mrv1 = mrv;
        mrv1.field_1 = var_6;
        mrv2 = mrv1;
        mrv2.field_2 = r94_2;
        mrv3 = mrv2;
        mrv3.field_3 = r87_3;
        return mrv3;
    }
    if (*(unsigned char *)4368346UL != '\x00') {
        storemerge = 4326537UL;
        if (*(uint64_t *)4368336UL == 0UL) {
            mrv.field_0 = rcx_8;
            mrv1 = mrv;
            mrv1.field_1 = var_6;
            mrv2 = mrv1;
            mrv2.field_2 = r94_2;
            mrv3 = mrv2;
            mrv3.field_3 = r87_3;
            return mrv3;
        }
        var_103 = local_sp_10 + 88UL;
        var_104 = local_sp_10 + 136UL;
        *(uint64_t *)(local_sp_10 + (-16L)) = 4219074UL;
        indirect_placeholder_5(var_104, var_103);
        var_105 = (uint64_t)var_10;
        r87_1 = var_103;
        storemerge4 = var_103;
        rdi5_2 = var_103;
        while (rcx_5 != 0UL)
            {
                var_106 = *(unsigned char *)rdi5_2;
                var_107 = rcx_5 + (-1L);
                rcx_5 = var_107;
                rcx_6 = var_107;
                if (var_106 == '\x00') {
                    break;
                }
                rdi5_2 = rdi5_2 + var_105;
            }
        var_108 = 18446744073709551614UL - (-2L);
        var_109 = local_sp_10 + (-24L);
        *(uint64_t *)var_109 = 4219100UL;
        var_110 = indirect_placeholder_7(var_108);
        local_sp_2 = var_109;
        if (var_110 == 1UL) {
            var_111 = local_sp_10 + (-32L);
            *(uint64_t *)var_111 = 4219469UL;
            var_112 = indirect_placeholder_302(0UL, var_103, 4326563UL, r94_8, 0UL, 0UL, var_103);
            var_113 = var_112.field_0;
            var_114 = var_112.field_1;
            var_115 = var_112.field_3;
            *(unsigned char *)(local_sp_10 + 167UL) = (unsigned char)'\x01';
            r94_0 = var_114;
            r87_0 = var_115;
            rcx_4 = var_113;
            local_sp_1 = var_111;
        } else {
            var_125 = local_sp_2 + (-8L);
            *(uint64_t *)var_125 = 4219129UL;
            var_126 = indirect_placeholder_301(0UL, storemerge4, storemerge, r94_8, 0UL, 0UL, r87_1);
            var_127 = var_126.field_0;
            var_128 = var_126.field_1;
            var_129 = var_126.field_3;
            *(unsigned char *)(local_sp_2 + 191UL) = rbp_3_in;
            r94_0 = var_128;
            r87_0 = var_129;
            rcx_4 = var_127;
            local_sp_1 = var_125;
        }
        r94_2 = r94_0;
        r94_1_ph = r94_0;
        r87_2_ph = r87_0;
        rcx_7_ph = rcx_4;
        local_sp_3_ph = local_sp_1;
        r87_3 = r87_0;
        rcx_8 = rcx_4;
        if (*(unsigned char *)4368346UL == '\x00') {
            mrv.field_0 = rcx_8;
            mrv1 = mrv;
            mrv1.field_1 = var_6;
            mrv2 = mrv1;
            mrv2.field_2 = r94_2;
            mrv3 = mrv2;
            mrv3.field_3 = r87_3;
            return mrv3;
        }
        _pr_pre = *(unsigned char *)4368345UL;
        _pr = _pr_pre;
        r94_2 = r94_1_ph;
        local_sp_3267 = local_sp_3_ph;
        rcx_7266 = rcx_7_ph;
        r87_2265 = r87_2_ph;
        r94_1264 = r94_1_ph;
        r87_3 = r87_2_ph;
        rcx_8 = rcx_7_ph;
        if (_pr == '\x00') {
            mrv.field_0 = rcx_8;
            mrv1 = mrv;
            mrv1.field_1 = var_6;
            mrv2 = mrv1;
            mrv2.field_2 = r94_2;
            mrv3 = mrv2;
            mrv3.field_3 = r87_3;
            return mrv3;
        }
        r94_2 = r94_1264;
        r87_3 = r87_2265;
        rcx_8 = rcx_7266;
        if (*(uint64_t *)4368336UL != 0UL) {
            *(uint64_t *)(local_sp_3267 + (-8L)) = 4219298UL;
            var_144 = indirect_placeholder_303(0UL, rcx_7266, 4321608UL, r94_1264, 0UL, 0UL, r87_2265);
            var_145 = var_144.field_0;
            var_146 = var_144.field_1;
            var_147 = var_144.field_3;
            r94_2 = var_146;
            r87_3 = var_147;
            rcx_8 = var_145;
        }
        mrv.field_0 = rcx_8;
        mrv1 = mrv;
        mrv1.field_1 = var_6;
        mrv2 = mrv1;
        mrv2.field_2 = r94_2;
        mrv3 = mrv2;
        mrv3.field_3 = r87_3;
        return mrv3;
    }
    var_116 = *(unsigned char *)4368345UL;
    _pr = var_116;
    if (var_116 != '\x00') {
        r94_2 = r94_1264;
        r87_3 = r87_2265;
        rcx_8 = rcx_7266;
        if (*(uint64_t *)4368336UL != 0UL) {
            *(uint64_t *)(local_sp_3267 + (-8L)) = 4219298UL;
            var_144 = indirect_placeholder_303(0UL, rcx_7266, 4321608UL, r94_1264, 0UL, 0UL, r87_2265);
            var_145 = var_144.field_0;
            var_146 = var_144.field_1;
            var_147 = var_144.field_3;
            r94_2 = var_146;
            r87_3 = var_147;
            rcx_8 = var_145;
        }
        mrv.field_0 = rcx_8;
        mrv1 = mrv;
        mrv1.field_1 = var_6;
        mrv2 = mrv1;
        mrv2.field_2 = r94_2;
        mrv3 = mrv2;
        mrv3.field_3 = r87_3;
        return mrv3;
    }
    if (*(uint64_t *)4368336UL != 0UL) {
        r94_2 = r94_1_ph;
        local_sp_3267 = local_sp_3_ph;
        rcx_7266 = rcx_7_ph;
        r87_2265 = r87_2_ph;
        r94_1264 = r94_1_ph;
        r87_3 = r87_2_ph;
        rcx_8 = rcx_7_ph;
        if (_pr == '\x00') {
            mrv.field_0 = rcx_8;
            mrv1 = mrv;
            mrv1.field_1 = var_6;
            mrv2 = mrv1;
            mrv2.field_2 = r94_2;
            mrv3 = mrv2;
            mrv3.field_3 = r87_3;
            return mrv3;
        }
        r94_2 = r94_1264;
        r87_3 = r87_2265;
        rcx_8 = rcx_7266;
        if (*(uint64_t *)4368336UL != 0UL) {
            *(uint64_t *)(local_sp_3267 + (-8L)) = 4219298UL;
            var_144 = indirect_placeholder_303(0UL, rcx_7266, 4321608UL, r94_1264, 0UL, 0UL, r87_2265);
            var_145 = var_144.field_0;
            var_146 = var_144.field_1;
            var_147 = var_144.field_3;
            r94_2 = var_146;
            r87_3 = var_147;
            rcx_8 = var_145;
        }
        mrv.field_0 = rcx_8;
        mrv1 = mrv;
        mrv1.field_1 = var_6;
        mrv2 = mrv1;
        mrv2.field_2 = r94_2;
        mrv3 = mrv2;
        mrv3.field_3 = r87_3;
        return mrv3;
    }
    var_117 = local_sp_10 + 88UL;
    var_118 = local_sp_10 + 136UL;
    *(uint64_t *)(local_sp_10 + (-16L)) = 4219408UL;
    indirect_placeholder_5(var_118, var_117);
    var_119 = (uint64_t)var_10;
    rdi5_0 = var_117;
    r14_0 = var_117;
    storemerge4 = var_117;
    while (rcx_0 != 0UL)
        {
            var_120 = *(unsigned char *)rdi5_0;
            var_121 = rcx_0 + (-1L);
            rcx_0 = var_121;
            rcx_1 = var_121;
            if (var_120 == '\x00') {
                break;
            }
            rdi5_0 = rdi5_0 + var_119;
        }
    var_122 = 18446744073709551614UL - (-2L);
    var_123 = local_sp_10 + (-24L);
    *(uint64_t *)var_123 = 4219434UL;
    var_124 = indirect_placeholder_7(var_122);
    local_sp_0 = var_123;
    local_sp_2 = var_123;
    if (var_124 != 1UL) {
        var_139 = local_sp_0 + (-8L);
        *(uint64_t *)var_139 = 4219241UL;
        var_140 = indirect_placeholder_300(0UL, r14_0, storemerge3, r94_8, 0UL, 0UL, r87_9);
        var_141 = var_140.field_0;
        var_142 = var_140.field_1;
        var_143 = var_140.field_3;
        *(unsigned char *)(local_sp_0 + 191UL) = rbp_3_in;
        r94_2 = var_142;
        r94_0 = var_142;
        r87_0 = var_143;
        rcx_4 = var_141;
        local_sp_1 = var_139;
        r87_3 = var_143;
        rcx_8 = var_141;
        if (rbp_3_in == '\x00') {
            mrv.field_0 = rcx_8;
            mrv1 = mrv;
            mrv1.field_1 = var_6;
            mrv2 = mrv1;
            mrv2.field_2 = r94_2;
            mrv3 = mrv2;
            mrv3.field_3 = r87_3;
            return mrv3;
        }
    }
    var_125 = local_sp_2 + (-8L);
    *(uint64_t *)var_125 = 4219129UL;
    var_126 = indirect_placeholder_301(0UL, storemerge4, storemerge, r94_8, 0UL, 0UL, r87_1);
    var_127 = var_126.field_0;
    var_128 = var_126.field_1;
    var_129 = var_126.field_3;
    *(unsigned char *)(local_sp_2 + 191UL) = rbp_3_in;
    r94_0 = var_128;
    r87_0 = var_129;
    rcx_4 = var_127;
    local_sp_1 = var_125;
}
