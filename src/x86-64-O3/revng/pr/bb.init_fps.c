typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_init_fps_ret_type;
struct helper_punpcklqdq_xmm_wrapper_ret_type;
struct type_5;
struct type_7;
struct helper_movq_mm_T0_xmm_wrapper_ret_type;
struct helper_movq_mm_T0_xmm_wrapper_108_ret_type;
struct indirect_placeholder_42_ret_type;
struct indirect_placeholder_40_ret_type;
struct indirect_placeholder_41_ret_type;
struct bb_init_fps_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct helper_punpcklqdq_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
};
struct type_5 {
};
struct type_7 {
};
struct helper_movq_mm_T0_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_movq_mm_T0_xmm_wrapper_108_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_42_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_40_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_41_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_rcx(void);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern void indirect_placeholder_14(uint64_t param_0, uint64_t param_1);
extern struct helper_punpcklqdq_xmm_wrapper_ret_type helper_punpcklqdq_xmm_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4);
extern struct helper_movq_mm_T0_xmm_wrapper_ret_type helper_movq_mm_T0_xmm_wrapper(struct type_7 *param_0, uint64_t param_1);
extern struct helper_movq_mm_T0_xmm_wrapper_108_ret_type helper_movq_mm_T0_xmm_wrapper_108(struct type_7 *param_0, uint64_t param_1);
extern struct indirect_placeholder_42_ret_type indirect_placeholder_42(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_40_ret_type indirect_placeholder_40(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_41_ret_type indirect_placeholder_41(uint64_t param_0, uint64_t param_1, uint64_t param_2);
struct bb_init_fps_ret_type bb_init_fps(uint64_t rdi, uint64_t rsi) {
    uint64_t local_sp_0;
    uint32_t var_20;
    uint32_t var_21;
    uint64_t rbp_1_be;
    uint64_t local_sp_1_ph;
    struct helper_punpcklqdq_xmm_wrapper_ret_type var_38;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    struct indirect_placeholder_42_ret_type var_11;
    uint64_t var_12;
    unsigned char var_13;
    uint64_t rbp_0;
    uint32_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    struct helper_movq_mm_T0_xmm_wrapper_ret_type var_35;
    uint64_t var_36;
    struct helper_movq_mm_T0_xmm_wrapper_108_ret_type var_37;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t rax_0;
    uint64_t rbp_1;
    uint64_t var_43;
    uint64_t rcx_0;
    struct bb_init_fps_ret_type mrv1 = {1UL, /*implicit*/(int)0, /*implicit*/(int)0, /*implicit*/(int)0, /*implicit*/(int)0};
    struct bb_init_fps_ret_type mrv2;
    struct bb_init_fps_ret_type mrv3;
    struct bb_init_fps_ret_type mrv4;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_14;
    uint64_t rbx_0;
    uint64_t var_15;
    uint64_t var_16;
    struct indirect_placeholder_40_ret_type var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t local_sp_1;
    struct bb_init_fps_ret_type mrv6 = {0UL, /*implicit*/(int)0, /*implicit*/(int)0, /*implicit*/(int)0, /*implicit*/(int)0};
    struct bb_init_fps_ret_type mrv7;
    struct bb_init_fps_ret_type mrv8;
    struct bb_init_fps_ret_type mrv9;
    uint64_t var_22;
    uint64_t *var_24;
    uint64_t var_25;
    struct indirect_placeholder_41_ret_type var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_23;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rcx();
    var_2 = init_rbp();
    var_3 = init_r14();
    var_4 = init_r12();
    var_5 = init_rbx();
    var_6 = init_r9();
    var_7 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_4;
    var_8 = (uint64_t)(uint32_t)rdi;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint32_t *)4326516UL = 0U;
    *(uint64_t *)(var_0 + (-32L)) = 4211936UL;
    indirect_placeholder_1();
    var_9 = (uint64_t)*(uint32_t *)4326120UL;
    var_10 = var_0 + (-40L);
    *(uint64_t *)var_10 = 4211953UL;
    var_11 = indirect_placeholder_42(var_6, var_9, 64UL, var_7);
    var_12 = var_11.field_0;
    var_13 = *(unsigned char *)4326623UL;
    *(uint64_t *)4326664UL = var_12;
    local_sp_0 = var_10;
    local_sp_1_ph = var_10;
    rbp_0 = var_12;
    rbp_1 = var_12;
    rcx_0 = var_1;
    rbx_0 = rsi;
    if (var_13 == '\x00') {
        var_22 = helper_cc_compute_all_wrapper(var_8, 0UL, 0UL, 24U);
        if ((uint64_t)(((unsigned char)(var_22 >> 4UL) ^ (unsigned char)var_22) & '\xc0') == 0UL) {
            *(uint64_t *)(var_12 + 8UL) = 4284483UL;
            var_23 = *(uint64_t *)4324936UL;
            *(uint32_t *)(var_12 + 16UL) = 0U;
            *(uint64_t *)var_12 = var_23;
            *(unsigned char *)(var_12 + 57UL) = (unsigned char)'\x00';
            *(uint32_t *)4326516UL = (*(uint32_t *)4326516UL + 1U);
            *(unsigned char *)4326614UL = (unsigned char)'\x01';
            *(uint64_t *)(var_0 + (-48L)) = 4212211UL;
            indirect_placeholder_14(4284434UL, 4294967295UL);
            *(uint32_t *)(var_12 + 44UL) = 0U;
        } else {
            var_24 = (uint64_t *)rsi;
            var_25 = *var_24;
            *(uint64_t *)(var_0 + (-48L)) = 4212072UL;
            var_26 = indirect_placeholder_41(var_12, var_25, var_12);
            var_27 = var_26.field_0;
            var_28 = var_26.field_1;
            rbp_0 = var_28;
            if ((uint64_t)(unsigned char)var_27 != 0UL) {
                mrv6.field_1 = var_1;
                mrv7 = mrv6;
                mrv7.field_2 = var_3;
                mrv8 = mrv7;
                mrv8.field_3 = var_6;
                mrv9 = mrv8;
                mrv9.field_4 = var_7;
                return mrv9;
            }
            *(uint64_t *)(var_0 + (-56L)) = 4212085UL;
            indirect_placeholder_1();
            var_29 = *var_24;
            var_30 = (uint64_t)(uint32_t)var_27;
            *(uint64_t *)(var_0 + (-64L)) = 4212095UL;
            indirect_placeholder_14(var_29, var_30);
            *(uint32_t *)(var_28 + 44UL) = 0U;
        }
        var_31 = *(uint32_t *)4326120UL;
        var_32 = (uint64_t)var_31;
        rcx_0 = var_32;
        if ((uint64_t)(var_31 + (-1)) != 0UL) {
            var_33 = rbp_0 + 64UL;
            var_34 = *(uint64_t *)(rbp_0 + 8UL);
            var_35 = helper_movq_mm_T0_xmm_wrapper((struct type_7 *)(776UL), *(uint64_t *)rbp_0);
            var_36 = var_35.field_0;
            var_37 = helper_movq_mm_T0_xmm_wrapper_108((struct type_7 *)(840UL), var_34);
            var_38 = helper_punpcklqdq_xmm_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(840UL), var_36, var_37.field_0);
            var_39 = var_38.field_0;
            var_40 = var_38.field_1;
            var_41 = (((var_32 << 6UL) + 274877906816UL) & 274877906880UL) + 128UL;
            var_42 = rbp_0 + var_41;
            rax_0 = var_33;
            rcx_0 = var_41;
            *(uint32_t *)(rax_0 + 16UL) = 0U;
            var_43 = rax_0 + 64UL;
            *(uint64_t *)rax_0 = var_39;
            *(uint64_t *)(rax_0 + 8UL) = var_40;
            *(unsigned char *)(rax_0 + 57UL) = (unsigned char)'\x00';
            *(uint32_t *)(rax_0 + 44UL) = 0U;
            rax_0 = var_43;
            do {
                *(uint32_t *)(rax_0 + 16UL) = 0U;
                var_43 = rax_0 + 64UL;
                *(uint64_t *)rax_0 = var_39;
                *(uint64_t *)(rax_0 + 8UL) = var_40;
                *(unsigned char *)(rax_0 + 57UL) = (unsigned char)'\x00';
                *(uint32_t *)(rax_0 + 44UL) = 0U;
                rax_0 = var_43;
            } while (var_43 != var_42);
        }
    } else {
        if (var_8 == 0UL) {
            var_14 = (((rdi << 3UL) + 34359738360UL) & 34359738360UL) + rsi;
            while (1U)
                {
                    var_15 = *(uint64_t *)rbx_0;
                    var_16 = local_sp_0 + (-8L);
                    *(uint64_t *)var_16 = 4212016UL;
                    var_17 = indirect_placeholder_40(rbp_1, var_15, rbp_1);
                    var_18 = var_17.field_0;
                    var_19 = var_17.field_1;
                    rbp_1_be = var_19;
                    local_sp_0 = var_16;
                    local_sp_1_ph = var_16;
                    local_sp_1 = var_16;
                    if ((uint64_t)(unsigned char)var_18 == 0UL) {
                        var_20 = *(uint32_t *)4326120UL + (-1);
                        *(uint32_t *)4326120UL = var_20;
                        var_21 = var_20;
                        if (var_14 != rbx_0) {
                            loop_state_var = 0U;
                            break;
                        }
                    }
                    if (var_14 != rbx_0) {
                        loop_state_var = 1U;
                        break;
                    }
                    rbp_1_be = var_19 + 64UL;
                    rbp_1 = rbp_1_be;
                    rbx_0 = rbx_0 + 8UL;
                    continue;
                }
            var_21 = *(uint32_t *)4326120UL;
            local_sp_1 = local_sp_1_ph;
        } else {
            var_21 = *(uint32_t *)4326120UL;
            local_sp_1 = local_sp_1_ph;
            if (var_21 != 0U) {
                mrv6.field_1 = var_1;
                mrv7 = mrv6;
                mrv7.field_2 = var_3;
                mrv8 = mrv7;
                mrv8.field_3 = var_6;
                mrv9 = mrv8;
                mrv9.field_4 = var_7;
                return mrv9;
            }
            *(uint64_t *)(local_sp_1 + (-8L)) = 4212127UL;
            indirect_placeholder_14(4284434UL, 4294967295UL);
        }
    }
}
