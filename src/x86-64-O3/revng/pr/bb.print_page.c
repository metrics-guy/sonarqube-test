typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_37_ret_type;
struct indirect_placeholder_32_ret_type;
struct indirect_placeholder_33_ret_type;
struct indirect_placeholder_34_ret_type;
struct indirect_placeholder_35_ret_type;
struct indirect_placeholder_36_ret_type;
struct indirect_placeholder_37_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_32_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_33_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
};
struct indirect_placeholder_34_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_35_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_36_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_11(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_14(uint64_t param_0, uint64_t param_1);
extern uint64_t init_r10(void);
extern struct indirect_placeholder_37_ret_type indirect_placeholder_37(void);
extern struct indirect_placeholder_32_ret_type indirect_placeholder_32(void);
extern struct indirect_placeholder_33_ret_type indirect_placeholder_33(uint64_t param_0);
extern struct indirect_placeholder_34_ret_type indirect_placeholder_34(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
extern struct indirect_placeholder_35_ret_type indirect_placeholder_35(void);
extern struct indirect_placeholder_36_ret_type indirect_placeholder_36(void);
typedef _Bool bool;
uint64_t bb_print_page(uint64_t r9, uint64_t rsi, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    struct indirect_placeholder_37_ret_type var_9;
    uint64_t r12_8;
    uint64_t r14_7;
    uint64_t var_64;
    uint64_t r14_8;
    uint64_t rbx_5;
    uint64_t r14_0;
    uint64_t r10_8;
    uint64_t r12_0;
    uint64_t rbx_9;
    uint64_t local_sp_9;
    uint64_t r10_0;
    uint64_t r13_1;
    uint64_t r91_8;
    uint64_t r13_8;
    uint64_t rbx_0;
    uint64_t local_sp_11;
    uint64_t local_sp_4;
    uint64_t r91_0;
    uint64_t local_sp_0;
    uint64_t r13_0;
    uint64_t r12_2;
    uint64_t r14_1;
    uint64_t r12_1;
    uint64_t r10_1;
    uint64_t rbx_1;
    uint64_t r91_1;
    uint64_t local_sp_1;
    uint64_t r83_0;
    uint32_t var_33;
    uint64_t local_sp_2;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t r13_2;
    uint64_t var_55;
    uint64_t r14_9;
    uint64_t rbx_2;
    uint64_t local_sp_3;
    uint64_t r13_3;
    uint64_t r12_11;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t rax_2;
    uint64_t r83_5;
    uint32_t *_pre_phi65;
    uint64_t r10_6;
    uint64_t r14_2;
    uint64_t r10_2;
    uint64_t rbx_3;
    uint64_t r91_2;
    uint64_t r13_4;
    uint64_t r13_7;
    uint64_t r83_4;
    uint64_t var_44;
    unsigned char var_34;
    uint64_t var_35;
    uint32_t var_36;
    uint64_t var_37;
    uint64_t rbx_7;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t local_sp_5;
    uint64_t r83_1;
    uint64_t r12_6;
    uint32_t var_42;
    uint64_t var_43;
    uint64_t rax_1;
    uint64_t r14_5;
    uint64_t rbp_0;
    uint64_t r14_3;
    uint64_t r12_3;
    uint64_t r10_3;
    uint64_t r91_5;
    uint64_t rbx_4;
    uint64_t r91_3;
    uint64_t local_sp_6;
    uint64_t r13_5;
    uint64_t r83_2;
    uint64_t var_46;
    uint64_t var_40;
    struct indirect_placeholder_32_ret_type var_41;
    uint64_t var_28;
    struct indirect_placeholder_33_ret_type var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t rbp_2;
    uint64_t r12_7;
    uint64_t rax_0;
    uint64_t rbp_1;
    uint64_t r12_9;
    uint64_t r14_4;
    uint64_t r10_9;
    uint64_t r12_4;
    uint64_t rbx_10;
    uint64_t r10_4;
    uint64_t local_sp_12;
    uint64_t r91_4;
    uint64_t local_sp_7;
    uint64_t r13_6;
    uint64_t r83_3;
    uint32_t *var_24;
    uint64_t var_25;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t r12_5;
    uint64_t r10_5;
    uint64_t rbx_6;
    uint64_t local_sp_8;
    struct indirect_placeholder_34_ret_type var_45;
    uint64_t r14_6;
    uint64_t r91_6;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t r10_7;
    uint64_t rbx_8;
    uint64_t r91_7;
    uint64_t local_sp_10;
    uint64_t r13_9;
    uint64_t var_49;
    uint64_t var_50;
    uint32_t var_26;
    uint64_t var_27;
    uint64_t r13_10;
    uint64_t var_51;
    struct indirect_placeholder_35_ret_type var_52;
    uint64_t var_10;
    unsigned char var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t r91_9;
    uint64_t r13_11;
    uint64_t var_18;
    struct indirect_placeholder_36_ret_type var_19;
    uint64_t var_20;
    uint64_t var_14;
    uint64_t local_sp_15;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t r12_10;
    uint64_t rbx_11;
    uint64_t rdx_0;
    uint64_t local_sp_13;
    uint64_t var_59;
    uint64_t rbx_12;
    uint64_t rax_3;
    uint64_t local_sp_14;
    uint64_t var_60;
    uint64_t r13_12;
    unsigned char var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t local_sp_16;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r14();
    var_3 = init_r12();
    var_4 = init_r10();
    var_5 = init_rbx();
    var_6 = init_cc_src2();
    var_7 = init_r13();
    *(uint64_t *)(var_0 + (-8L)) = var_7;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-48L)) = 4215519UL;
    indirect_placeholder_11(r9, rsi, r8);
    var_8 = var_0 + (-56L);
    *(uint64_t *)var_8 = 4215524UL;
    var_9 = indirect_placeholder_37();
    r14_9 = var_2;
    rbp_1 = 1UL;
    r12_9 = 0UL;
    r10_9 = var_4;
    rbx_10 = var_5;
    local_sp_12 = var_8;
    r91_9 = r9;
    local_sp_15 = var_8;
    r12_10 = 0UL;
    rbx_11 = var_5;
    local_sp_13 = var_8;
    r13_12 = 0UL;
    if ((uint64_t)(uint32_t)var_9.field_0 == 0UL) {
        return 0UL;
    }
    if (*(unsigned char *)4326149UL == '\x00') {
        *(unsigned char *)4326616UL = (unsigned char)'\x01';
    }
    var_10 = (uint64_t)*(uint32_t *)4326608UL;
    var_11 = *(unsigned char *)4326520UL;
    *(unsigned char *)4326496UL = (unsigned char)'\x00';
    var_12 = (uint64_t)(uint32_t)(var_10 << (var_11 != '\x00'));
    var_13 = helper_cc_compute_all_wrapper(var_12, 0UL, 0UL, 24U);
    r13_11 = var_12;
    if ((uint64_t)(((unsigned char)(var_13 >> 4UL) ^ (unsigned char)var_13) & '\xc0') == 0UL) {
        var_14 = helper_cc_compute_all_wrapper(var_12, var_13, var_6, 1U);
        if ((var_14 & 64UL) != 0UL) {
            local_sp_16 = local_sp_15;
            if (*(unsigned char *)4326618UL != '\x00' & *(unsigned char *)4326617UL == '\x00') {
                var_64 = local_sp_15 + (-8L);
                *(uint64_t *)var_64 = 4216300UL;
                indirect_placeholder_1();
                *(unsigned char *)4326617UL = (unsigned char)'\x00';
                local_sp_16 = var_64;
            }
            var_65 = *(uint64_t *)4326552UL + 1UL;
            var_66 = *(uint64_t *)4326112UL;
            *(uint64_t *)4326552UL = var_65;
            if (var_65 > var_66) {
                return 0UL;
            }
            *(uint64_t *)(local_sp_16 + (-8L)) = 4216260UL;
            indirect_placeholder_1();
            return 1UL;
        }
        var_15 = (uint64_t)*(uint32_t *)4326120UL;
        var_16 = *(uint64_t *)4326664UL;
        var_17 = helper_cc_compute_all_wrapper(var_15, 0UL, 0UL, 24U);
        rax_2 = var_16;
        rdx_0 = var_15;
        if ((uint64_t)(((unsigned char)(var_17 >> 4UL) ^ (unsigned char)var_17) & '\xc0') == 0UL) {
            local_sp_16 = local_sp_15;
            if (*(unsigned char *)4326618UL != '\x00' & *(unsigned char *)4326617UL == '\x00') {
                var_64 = local_sp_15 + (-8L);
                *(uint64_t *)var_64 = 4216300UL;
                indirect_placeholder_1();
                *(unsigned char *)4326617UL = (unsigned char)'\x00';
                local_sp_16 = var_64;
            }
            var_65 = *(uint64_t *)4326552UL + 1UL;
            var_66 = *(uint64_t *)4326112UL;
            *(uint64_t *)4326552UL = var_65;
            if (var_65 > var_66) {
                return 0UL;
            }
            *(uint64_t *)(local_sp_16 + (-8L)) = 4216260UL;
            indirect_placeholder_1();
            return 1UL;
        }
        var_59 = ((((rdx_0 << 6UL) + 274877906880UL) & 274877906880UL) + 64UL) + rax_2;
        rax_3 = rax_2;
        r12_11 = r12_10;
        rbx_12 = rbx_11;
        local_sp_14 = local_sp_13;
        var_60 = rax_3 + 64UL;
        rax_3 = var_60;
        do {
            if (*(uint32_t *)(rax_3 + 16UL) == 0U) {
                *(unsigned char *)(rax_3 + 57UL) = (unsigned char)'\x01';
            }
            var_60 = rax_3 + 64UL;
            rax_3 = var_60;
        } while (var_60 != var_59);
    } else {
        while (1U)
            {
                var_18 = local_sp_12 + (-8L);
                *(uint64_t *)var_18 = 4215597UL;
                var_19 = indirect_placeholder_36();
                var_20 = var_19.field_1;
                r12_8 = r12_9;
                r14_8 = r14_9;
                r10_8 = r10_9;
                r91_8 = r91_9;
                local_sp_11 = var_18;
                r12_11 = r12_9;
                r14_4 = r14_9;
                r12_4 = r12_9;
                r10_4 = r10_9;
                r91_4 = r91_9;
                local_sp_7 = var_18;
                r13_6 = r13_11;
                r83_3 = var_20;
                r13_10 = r13_11;
                rbx_12 = rbx_10;
                local_sp_14 = var_18;
                r13_12 = r13_11;
                if ((uint64_t)(uint32_t)var_19.field_0 != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                var_21 = (uint64_t)*(uint32_t *)4326120UL;
                *(uint32_t *)4326584UL = 0U;
                *(unsigned char *)4326496UL = (unsigned char)'\x00';
                var_22 = *(uint64_t *)4326664UL;
                *(uint32_t *)4326592UL = 0U;
                *(uint32_t *)4326504UL = 0U;
                *(unsigned char *)4326622UL = (unsigned char)'\x00';
                *(unsigned char *)4326621UL = (unsigned char)'\x01';
                var_23 = helper_cc_compute_all_wrapper(var_21, 0UL, 0UL, 24U);
                rax_0 = var_21;
                rbx_5 = var_22;
                rbx_9 = var_22;
                if ((uint64_t)(((unsigned char)(var_23 >> 4UL) ^ (unsigned char)var_23) & '\xc0') != 0UL) {
                    while (1U)
                        {
                            *(uint32_t *)4326580UL = 0U;
                            var_24 = (uint32_t *)(rbx_5 + 48UL);
                            var_25 = helper_cc_compute_all_wrapper((uint64_t)*var_24, 0UL, 0UL, 24U);
                            local_sp_9 = local_sp_7;
                            r13_8 = r13_6;
                            r83_0 = r83_3;
                            r83_5 = r83_3;
                            _pre_phi65 = var_24;
                            r10_6 = r10_4;
                            r14_2 = r14_4;
                            r10_2 = r10_4;
                            rbx_3 = rbx_5;
                            r91_2 = r91_4;
                            r13_4 = r13_6;
                            r13_7 = r13_6;
                            r83_4 = r83_3;
                            rbx_7 = rbx_5;
                            r12_6 = r12_4;
                            rax_1 = rax_0;
                            r14_5 = r14_4;
                            rbp_0 = rbp_1;
                            r91_5 = r91_4;
                            rbp_2 = rbp_1;
                            r12_5 = r12_4;
                            r10_5 = r10_4;
                            rbx_6 = rbx_5;
                            local_sp_8 = local_sp_7;
                            r14_6 = r14_4;
                            r91_6 = r91_4;
                            if ((uint64_t)(((unsigned char)(var_25 >> 4UL) ^ (unsigned char)var_25) & '\xc0') != 0UL) {
                                if (*(uint32_t *)(rbx_5 + 16UL) != 1U) {
                                    if (*(unsigned char *)4326623UL != '\x00') {
                                        r12_8 = r12_6;
                                        r14_7 = r14_6;
                                        r14_8 = r14_6;
                                        r10_8 = r10_6;
                                        r91_8 = r91_6;
                                        local_sp_11 = local_sp_9;
                                        r12_7 = r12_6;
                                        rax_0 = rax_1;
                                        r14_4 = r14_6;
                                        r12_4 = r12_6;
                                        r10_4 = r10_6;
                                        r91_4 = r91_6;
                                        local_sp_7 = local_sp_9;
                                        r13_6 = r13_8;
                                        r83_3 = r83_5;
                                        r10_7 = r10_6;
                                        r91_7 = r91_6;
                                        local_sp_10 = local_sp_9;
                                        r13_9 = r13_8;
                                        r13_10 = r13_8;
                                        if (*(unsigned char *)4326512UL == '\x00') {
                                            *(uint32_t *)4326504UL = (*(uint32_t *)4326504UL + 1U);
                                        }
                                        var_47 = rbp_2 + 1UL;
                                        var_48 = rbx_7 + 64UL;
                                        rbx_5 = var_48;
                                        rbx_8 = var_48;
                                        rbx_9 = var_48;
                                        if ((long)(var_47 << 32UL) <= (long)(rax_1 << 32UL)) {
                                            loop_state_var = 1U;
                                            break;
                                        }
                                        rbp_1 = (uint64_t)(uint32_t)var_47;
                                        continue;
                                    }
                                    if (*(unsigned char *)4326621UL != '\x00') {
                                        *(unsigned char *)4326622UL = (unsigned char)'\x01';
                                        r12_8 = r12_6;
                                        r14_7 = r14_6;
                                        r14_8 = r14_6;
                                        r10_8 = r10_6;
                                        r91_8 = r91_6;
                                        local_sp_11 = local_sp_9;
                                        r12_7 = r12_6;
                                        rax_0 = rax_1;
                                        r14_4 = r14_6;
                                        r12_4 = r12_6;
                                        r10_4 = r10_6;
                                        r91_4 = r91_6;
                                        local_sp_7 = local_sp_9;
                                        r13_6 = r13_8;
                                        r83_3 = r83_5;
                                        r10_7 = r10_6;
                                        r91_7 = r91_6;
                                        local_sp_10 = local_sp_9;
                                        r13_9 = r13_8;
                                        r13_10 = r13_8;
                                        if (*(unsigned char *)4326512UL == '\x00') {
                                            *(uint32_t *)4326504UL = (*(uint32_t *)4326504UL + 1U);
                                        }
                                        var_47 = rbp_2 + 1UL;
                                        var_48 = rbx_7 + 64UL;
                                        rbx_5 = var_48;
                                        rbx_8 = var_48;
                                        rbx_9 = var_48;
                                        if ((long)(var_47 << 32UL) <= (long)(rax_1 << 32UL)) {
                                            loop_state_var = 1U;
                                            break;
                                        }
                                        rbp_1 = (uint64_t)(uint32_t)var_47;
                                        continue;
                                    }
                                    var_44 = local_sp_8 + (-8L);
                                    *(uint64_t *)var_44 = 4215936UL;
                                    var_45 = indirect_placeholder_34(rbp_1, r14_5, r12_5, r10_5, r91_5, rbx_6, r13_7, r83_4);
                                    rbp_0 = var_45.field_0;
                                    r14_3 = var_45.field_1;
                                    r12_3 = var_45.field_2;
                                    r10_3 = var_45.field_3;
                                    rbx_4 = rbx_6;
                                    r91_3 = var_45.field_4;
                                    local_sp_6 = var_44;
                                    r13_5 = var_45.field_5;
                                    r83_2 = var_45.field_6;
                                    var_46 = (uint64_t)*(uint32_t *)4326120UL;
                                    local_sp_9 = local_sp_6;
                                    r13_8 = r13_5;
                                    r83_5 = r83_2;
                                    r10_6 = r10_3;
                                    rbx_7 = rbx_4;
                                    r12_6 = r12_3;
                                    rax_1 = var_46;
                                    rbp_2 = rbp_0;
                                    r14_6 = r14_3;
                                    r91_6 = r91_3;
                                    r12_8 = r12_6;
                                    r14_7 = r14_6;
                                    r14_8 = r14_6;
                                    r10_8 = r10_6;
                                    r91_8 = r91_6;
                                    local_sp_11 = local_sp_9;
                                    r12_7 = r12_6;
                                    rax_0 = rax_1;
                                    r14_4 = r14_6;
                                    r12_4 = r12_6;
                                    r10_4 = r10_6;
                                    r91_4 = r91_6;
                                    local_sp_7 = local_sp_9;
                                    r13_6 = r13_8;
                                    r83_3 = r83_5;
                                    r10_7 = r10_6;
                                    r91_7 = r91_6;
                                    local_sp_10 = local_sp_9;
                                    r13_9 = r13_8;
                                    r13_10 = r13_8;
                                    if (*(unsigned char *)4326512UL == '\x00') {
                                        *(uint32_t *)4326504UL = (*(uint32_t *)4326504UL + 1U);
                                    }
                                    var_47 = rbp_2 + 1UL;
                                    var_48 = rbx_7 + 64UL;
                                    rbx_5 = var_48;
                                    rbx_8 = var_48;
                                    rbx_9 = var_48;
                                    if ((long)(var_47 << 32UL) <= (long)(rax_1 << 32UL)) {
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    rbp_1 = (uint64_t)(uint32_t)var_47;
                                    continue;
                                }
                            }
                            var_26 = *(uint32_t *)(rbx_5 + 52UL);
                            *(unsigned char *)4326620UL = (unsigned char)'\x00';
                            *(uint32_t *)4326500UL = var_26;
                            var_27 = local_sp_7 + (-8L);
                            *(uint64_t *)var_27 = 4215718UL;
                            indirect_placeholder_1();
                            local_sp_4 = var_27;
                            if ((uint32_t)(unsigned char)var_26 == 0U) {
                                var_28 = local_sp_7 + (-16L);
                                *(uint64_t *)var_28 = 4216168UL;
                                var_29 = indirect_placeholder_33(rbx_5);
                                var_30 = var_29.field_1;
                                var_31 = var_29.field_2;
                                var_32 = var_29.field_3;
                                local_sp_4 = var_28;
                                r83_0 = var_29.field_7;
                                _pre_phi65 = (uint32_t *)(var_32 + 48UL);
                                r14_2 = var_30;
                                r10_2 = var_31;
                                rbx_3 = var_32;
                                r91_2 = var_29.field_4;
                                r13_4 = var_29.field_6;
                            }
                            var_33 = *_pre_phi65;
                            var_34 = *(unsigned char *)4326496UL;
                            var_35 = (uint64_t)var_34;
                            var_36 = var_33 + (-1);
                            var_37 = (uint64_t)var_36;
                            var_38 = (uint64_t)(uint32_t)r12_4 | var_35;
                            *_pre_phi65 = var_36;
                            var_39 = helper_cc_compute_all_wrapper(var_37, 0UL, 0UL, 24U);
                            r14_7 = r14_2;
                            r14_0 = r14_2;
                            r12_0 = var_38;
                            r10_0 = r10_2;
                            r13_8 = r13_4;
                            rbx_0 = rbx_3;
                            r91_0 = r91_2;
                            r13_0 = r13_4;
                            r10_6 = r10_2;
                            r13_7 = r13_4;
                            rbx_7 = rbx_3;
                            local_sp_5 = local_sp_4;
                            r83_1 = r83_0;
                            r12_6 = var_38;
                            r14_5 = r14_2;
                            r14_3 = r14_2;
                            r12_3 = var_38;
                            r10_3 = r10_2;
                            r91_5 = r91_2;
                            rbx_4 = rbx_3;
                            r91_3 = r91_2;
                            r13_5 = r13_4;
                            r12_7 = var_38;
                            r12_5 = var_38;
                            r10_5 = r10_2;
                            rbx_6 = rbx_3;
                            r14_6 = r14_2;
                            r91_6 = r91_2;
                            r10_7 = r10_2;
                            rbx_8 = rbx_3;
                            r91_7 = r91_2;
                            r13_9 = r13_4;
                            if ((uint64_t)(((unsigned char)(var_39 >> 4UL) ^ (unsigned char)var_39) & '\xc0') != 0UL) {
                                var_40 = local_sp_4 + (-8L);
                                *(uint64_t *)var_40 = 4215957UL;
                                var_41 = indirect_placeholder_32();
                                local_sp_0 = var_40;
                                local_sp_5 = var_40;
                                r83_1 = var_41.field_1;
                                local_sp_10 = var_40;
                                if ((uint64_t)(uint32_t)var_41.field_0 != 0UL) {
                                    loop_state_var = 0U;
                                    break;
                                }
                            }
                            local_sp_9 = local_sp_5;
                            r83_5 = r83_1;
                            r83_4 = r83_1;
                            local_sp_6 = local_sp_5;
                            r83_2 = r83_1;
                            local_sp_8 = local_sp_5;
                            var_42 = *(uint32_t *)(rbx_3 + 16UL);
                            if (*(unsigned char *)4326623UL != '\x00' & var_42 != 0U) {
                                if (*(unsigned char *)4326621UL != '\x00') {
                                    *(unsigned char *)4326622UL = (unsigned char)'\x01';
                                    var_43 = (uint64_t)*(uint32_t *)4326120UL;
                                    rax_1 = var_43;
                                    r12_8 = r12_6;
                                    r14_7 = r14_6;
                                    r14_8 = r14_6;
                                    r10_8 = r10_6;
                                    r91_8 = r91_6;
                                    local_sp_11 = local_sp_9;
                                    r12_7 = r12_6;
                                    rax_0 = rax_1;
                                    r14_4 = r14_6;
                                    r12_4 = r12_6;
                                    r10_4 = r10_6;
                                    r91_4 = r91_6;
                                    local_sp_7 = local_sp_9;
                                    r13_6 = r13_8;
                                    r83_3 = r83_5;
                                    r10_7 = r10_6;
                                    r91_7 = r91_6;
                                    local_sp_10 = local_sp_9;
                                    r13_9 = r13_8;
                                    r13_10 = r13_8;
                                    if (*(unsigned char *)4326512UL == '\x00') {
                                        *(uint32_t *)4326504UL = (*(uint32_t *)4326504UL + 1U);
                                    }
                                    var_47 = rbp_2 + 1UL;
                                    var_48 = rbx_7 + 64UL;
                                    rbx_5 = var_48;
                                    rbx_8 = var_48;
                                    rbx_9 = var_48;
                                    if ((long)(var_47 << 32UL) <= (long)(rax_1 << 32UL)) {
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    rbp_1 = (uint64_t)(uint32_t)var_47;
                                    continue;
                                }
                                if ((uint64_t)(var_42 + (-3)) == 0UL) {
                                    var_44 = local_sp_8 + (-8L);
                                    *(uint64_t *)var_44 = 4215936UL;
                                    var_45 = indirect_placeholder_34(rbp_1, r14_5, r12_5, r10_5, r91_5, rbx_6, r13_7, r83_4);
                                    rbp_0 = var_45.field_0;
                                    r14_3 = var_45.field_1;
                                    r12_3 = var_45.field_2;
                                    r10_3 = var_45.field_3;
                                    rbx_4 = rbx_6;
                                    r91_3 = var_45.field_4;
                                    local_sp_6 = var_44;
                                    r13_5 = var_45.field_5;
                                    r83_2 = var_45.field_6;
                                } else {
                                    if ((uint64_t)(var_42 + (-2)) != 0UL & *(unsigned char *)4326620UL == '\x00') {
                                        var_44 = local_sp_8 + (-8L);
                                        *(uint64_t *)var_44 = 4215936UL;
                                        var_45 = indirect_placeholder_34(rbp_1, r14_5, r12_5, r10_5, r91_5, rbx_6, r13_7, r83_4);
                                        rbp_0 = var_45.field_0;
                                        r14_3 = var_45.field_1;
                                        r12_3 = var_45.field_2;
                                        r10_3 = var_45.field_3;
                                        rbx_4 = rbx_6;
                                        r91_3 = var_45.field_4;
                                        local_sp_6 = var_44;
                                        r13_5 = var_45.field_5;
                                        r83_2 = var_45.field_6;
                                    }
                                }
                            }
                            var_46 = (uint64_t)*(uint32_t *)4326120UL;
                            local_sp_9 = local_sp_6;
                            r13_8 = r13_5;
                            r83_5 = r83_2;
                            r10_6 = r10_3;
                            rbx_7 = rbx_4;
                            r12_6 = r12_3;
                            rax_1 = var_46;
                            rbp_2 = rbp_0;
                            r14_6 = r14_3;
                            r91_6 = r91_3;
                            r12_8 = r12_6;
                            r14_7 = r14_6;
                            r14_8 = r14_6;
                            r10_8 = r10_6;
                            r91_8 = r91_6;
                            local_sp_11 = local_sp_9;
                            r12_7 = r12_6;
                            rax_0 = rax_1;
                            r14_4 = r14_6;
                            r12_4 = r12_6;
                            r10_4 = r10_6;
                            r91_4 = r91_6;
                            local_sp_7 = local_sp_9;
                            r13_6 = r13_8;
                            r83_3 = r83_5;
                            r10_7 = r10_6;
                            r91_7 = r91_6;
                            local_sp_10 = local_sp_9;
                            r13_9 = r13_8;
                            r13_10 = r13_8;
                            if (*(unsigned char *)4326512UL == '\x00') {
                                *(uint32_t *)4326504UL = (*(uint32_t *)4326504UL + 1U);
                            }
                            var_47 = rbp_2 + 1UL;
                            var_48 = rbx_7 + 64UL;
                            rbx_5 = var_48;
                            rbx_8 = var_48;
                            rbx_9 = var_48;
                            if ((long)(var_47 << 32UL) <= (long)(rax_1 << 32UL)) {
                                loop_state_var = 1U;
                                break;
                            }
                            rbp_1 = (uint64_t)(uint32_t)var_47;
                            continue;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 0U:
                        {
                            if (var_34 != '\x00') {
                                r13_1 = r13_0;
                                r12_2 = r12_0;
                                r14_1 = r14_0;
                                r12_1 = r12_0;
                                r10_1 = r10_0;
                                rbx_1 = rbx_0;
                                r91_1 = r91_0;
                                local_sp_1 = local_sp_0;
                                rbx_2 = rbx_0;
                                local_sp_3 = local_sp_0;
                                r13_3 = r13_0;
                                if (*(unsigned char *)4326149UL != '\x00') {
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                                r12_2 = r12_1;
                                local_sp_2 = local_sp_1;
                                r13_2 = r13_1;
                                r14_9 = r14_1;
                                rbx_2 = rbx_1;
                                r12_9 = r12_1;
                                r10_9 = r10_1;
                                rbx_10 = rbx_1;
                                r91_9 = r91_1;
                                if ((*(unsigned char *)4326520UL == '\x00') || ((uint64_t)(unsigned char)r12_1 == 0UL)) {
                                    var_53 = (uint64_t)((uint32_t)r13_1 + (-1));
                                    var_54 = local_sp_1 + (-8L);
                                    *(uint64_t *)var_54 = 4216238UL;
                                    indirect_placeholder_1();
                                    local_sp_2 = var_54;
                                    r13_2 = var_53;
                                }
                                var_55 = helper_cc_compute_all_wrapper(r13_2, 0UL, 0UL, 24U);
                                local_sp_3 = local_sp_2;
                                r13_3 = r13_2;
                                local_sp_12 = local_sp_2;
                                r13_11 = r13_2;
                                if ((uint64_t)(((unsigned char)(var_55 >> 4UL) ^ (unsigned char)var_55) & '\xc0') == 0UL) {
                                    continue;
                                }
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            var_49 = (uint64_t)((uint32_t)r13_9 + (-1));
                            var_50 = local_sp_10 + (-8L);
                            *(uint64_t *)var_50 = 4216199UL;
                            indirect_placeholder_1();
                            r12_8 = r12_7;
                            r14_8 = r14_7;
                            r10_8 = r10_7;
                            rbx_9 = rbx_8;
                            r91_8 = r91_7;
                            local_sp_11 = var_50;
                            r13_10 = var_49;
                        }
                        break;
                      case 1U:
                        {
                            if (*(unsigned char *)4326496UL == '\x00') {
                                var_49 = (uint64_t)((uint32_t)r13_9 + (-1));
                                var_50 = local_sp_10 + (-8L);
                                *(uint64_t *)var_50 = 4216199UL;
                                indirect_placeholder_1();
                                r12_8 = r12_7;
                                r14_8 = r14_7;
                                r10_8 = r10_7;
                                rbx_9 = rbx_8;
                                r91_8 = r91_7;
                                local_sp_11 = var_50;
                                r13_10 = var_49;
                            }
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
                var_51 = local_sp_11 + (-8L);
                *(uint64_t *)var_51 = 4216204UL;
                var_52 = indirect_placeholder_35();
                r14_0 = r14_8;
                r12_0 = r12_8;
                r10_0 = r10_8;
                r13_1 = r13_10;
                rbx_0 = rbx_9;
                r91_0 = r91_8;
                local_sp_0 = var_51;
                r13_0 = r13_10;
                r14_1 = r14_8;
                r12_1 = r12_8;
                r10_1 = r10_8;
                rbx_1 = rbx_9;
                r91_1 = r91_8;
                local_sp_1 = var_51;
                if ((uint64_t)(uint32_t)var_52.field_0 != 0UL) {
                    r13_1 = r13_0;
                    r12_2 = r12_0;
                    r14_1 = r14_0;
                    r12_1 = r12_0;
                    r10_1 = r10_0;
                    rbx_1 = rbx_0;
                    r91_1 = r91_0;
                    local_sp_1 = local_sp_0;
                    rbx_2 = rbx_0;
                    local_sp_3 = local_sp_0;
                    r13_3 = r13_0;
                    if (*(unsigned char *)4326149UL != '\x00') {
                        loop_state_var = 1U;
                        break;
                    }
                }
                r12_2 = r12_1;
                local_sp_2 = local_sp_1;
                r13_2 = r13_1;
                r14_9 = r14_1;
                rbx_2 = rbx_1;
                r12_9 = r12_1;
                r10_9 = r10_1;
                rbx_10 = rbx_1;
                r91_9 = r91_1;
                if ((*(unsigned char *)4326520UL == '\x00') || ((uint64_t)(unsigned char)r12_1 == 0UL)) {
                    var_53 = (uint64_t)((uint32_t)r13_1 + (-1));
                    var_54 = local_sp_1 + (-8L);
                    *(uint64_t *)var_54 = 4216238UL;
                    indirect_placeholder_1();
                    local_sp_2 = var_54;
                    r13_2 = var_53;
                }
                var_55 = helper_cc_compute_all_wrapper(r13_2, 0UL, 0UL, 24U);
                local_sp_3 = local_sp_2;
                r13_3 = r13_2;
                local_sp_12 = local_sp_2;
                r13_11 = r13_2;
                if ((uint64_t)(((unsigned char)(var_55 >> 4UL) ^ (unsigned char)var_55) & '\xc0') == 0UL) {
                    continue;
                }
                loop_state_var = 1U;
                break;
            }
        r12_11 = r12_2;
        r12_10 = r12_2;
        rbx_11 = rbx_2;
        local_sp_13 = local_sp_3;
        rbx_12 = rbx_2;
        local_sp_14 = local_sp_3;
        r13_12 = r13_3;
        var_56 = (uint64_t)*(uint32_t *)4326120UL;
        var_57 = *(uint64_t *)4326664UL;
        var_58 = helper_cc_compute_all_wrapper(var_56, 0UL, 0UL, 24U);
        rax_2 = var_57;
        rdx_0 = var_56;
        if ((uint64_t)(uint32_t)r13_3 != 0UL & (uint64_t)(((unsigned char)(var_58 >> 4UL) ^ (unsigned char)var_58) & '\xc0') != 0UL) {
            var_59 = ((((rdx_0 << 6UL) + 274877906880UL) & 274877906880UL) + 64UL) + rax_2;
            rax_3 = rax_2;
            r12_11 = r12_10;
            rbx_12 = rbx_11;
            local_sp_14 = local_sp_13;
            var_60 = rax_3 + 64UL;
            rax_3 = var_60;
            do {
                if (*(uint32_t *)(rax_3 + 16UL) == 0U) {
                    *(unsigned char *)(rax_3 + 57UL) = (unsigned char)'\x01';
                }
                var_60 = rax_3 + 64UL;
                rax_3 = var_60;
            } while (var_60 != var_59);
        }
    }
}
