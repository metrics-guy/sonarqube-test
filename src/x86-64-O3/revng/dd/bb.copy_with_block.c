typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_copy_with_block_ret_type;
struct indirect_placeholder_210_ret_type;
struct indirect_placeholder_211_ret_type;
struct bb_copy_with_block_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_210_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_211_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r13(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_rdx(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_rcx(void);
extern struct indirect_placeholder_210_ret_type indirect_placeholder_210(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_211_ret_type indirect_placeholder_211(uint64_t param_0, uint64_t param_1);
typedef _Bool bool;
struct bb_copy_with_block_ret_type bb_copy_with_block(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t rcx_2;
    uint64_t rbp_1;
    uint64_t rbx_0_be;
    uint64_t rbp_0;
    uint64_t r13_3;
    uint64_t rbx_0;
    uint64_t rdx_3;
    uint64_t local_sp_0;
    uint64_t r13_0;
    uint64_t var_28;
    unsigned char var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t rcx_0;
    uint64_t rbx_1;
    uint64_t rdx_0;
    uint64_t local_sp_1;
    uint64_t r13_1;
    uint64_t var_26;
    unsigned char var_27;
    uint64_t rax_0;
    uint64_t rbx_2;
    uint64_t r13_4;
    struct bb_copy_with_block_ret_type mrv;
    struct bb_copy_with_block_ret_type mrv1;
    struct bb_copy_with_block_ret_type mrv2;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t rbp_2;
    uint64_t rdx_1;
    uint64_t local_sp_2;
    uint64_t r13_2;
    uint64_t var_39;
    uint64_t rcx_1;
    uint64_t rbp_0_be;
    uint64_t local_sp_0_be;
    uint64_t r13_0_be;
    uint64_t var_34;
    struct indirect_placeholder_210_ret_type var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_14;
    uint64_t rbp_3;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    struct indirect_placeholder_211_ret_type var_23;
    uint64_t rbx_3;
    uint64_t rdx_2;
    uint64_t local_sp_3;
    uint64_t var_24;
    uint64_t var_25;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rcx();
    var_2 = init_rbp();
    var_3 = init_r12();
    var_4 = init_rbx();
    var_5 = init_rdx();
    var_6 = init_cc_src2();
    var_7 = init_r13();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    var_8 = rsi + rdi;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_9 = var_0 + (-24L);
    *(uint64_t *)var_9 = var_4;
    rcx_2 = var_1;
    rbx_0_be = 0UL;
    rbp_0 = rdi;
    rdx_3 = var_5;
    local_sp_0 = var_9;
    r13_0 = var_7;
    r13_4 = var_7;
    if (rsi == 0UL) {
        mrv.field_0 = rcx_2;
        mrv1 = mrv;
        mrv1.field_1 = rdx_3;
        mrv2 = mrv1;
        mrv2.field_2 = r13_4;
        return mrv2;
    }
    rbx_0 = *(uint64_t *)4310528UL;
    while (1U)
        {
            var_10 = *(unsigned char *)rbp_0;
            var_11 = (uint64_t)var_10;
            var_12 = (uint64_t)(var_10 - *(unsigned char *)4309738UL);
            var_13 = *(uint64_t *)4310720UL;
            rbp_1 = rbp_0;
            r13_3 = r13_0;
            rcx_0 = var_11;
            rbx_1 = rbx_0;
            rdx_0 = var_13;
            local_sp_1 = local_sp_0;
            r13_1 = r13_0;
            rbp_2 = rbp_0;
            rdx_1 = var_13;
            local_sp_2 = local_sp_0;
            r13_2 = r13_0;
            rcx_1 = var_11;
            rbp_3 = rbp_0;
            rbx_3 = rbx_0;
            rdx_2 = var_13;
            local_sp_3 = local_sp_0;
            if (var_12 != 0UL) {
                var_14 = var_13 - rbx_0;
                if (var_14 == 0UL) {
                    *(uint64_t *)4310560UL = (*(uint64_t *)4310560UL + 1UL);
                } else {
                    var_15 = helper_cc_compute_all_wrapper(var_14, rbx_0, var_6, 17U);
                    var_16 = *(uint64_t *)4310536UL;
                    var_17 = *(uint64_t *)4310544UL;
                    var_18 = var_16 + 1UL;
                    var_19 = *(uint64_t *)4310728UL;
                    var_20 = var_18 - var_19;
                    *(unsigned char *)(var_16 + var_17) = var_10;
                    *(uint64_t *)4310536UL = var_18;
                    var_21 = helper_cc_compute_c_wrapper(var_20, var_19, var_6, 17U);
                    rdx_2 = var_18;
                    if ((var_15 & 65UL) != 0UL & var_21 == 0UL) {
                        var_22 = local_sp_0 + (-8L);
                        *(uint64_t *)var_22 = 4212165UL;
                        var_23 = indirect_placeholder_211(var_11, var_18);
                        r13_3 = var_23.field_3;
                        rcx_1 = var_23.field_0;
                        rbp_3 = var_23.field_1;
                        rbx_3 = *(uint64_t *)4310528UL;
                        rdx_2 = var_23.field_2;
                        local_sp_3 = var_22;
                    }
                }
                var_24 = rbx_3 + 1UL;
                var_25 = rbp_3 + 1UL;
                *(uint64_t *)4310528UL = var_24;
                rcx_2 = rcx_1;
                rbx_0_be = var_24;
                rdx_3 = rdx_2;
                r13_4 = r13_3;
                rbp_0_be = var_25;
                local_sp_0_be = local_sp_3;
                r13_0_be = r13_3;
                if (var_8 == var_25) {
                    break;
                }
                rbp_0 = rbp_0_be;
                rbx_0 = rbx_0_be;
                local_sp_0 = local_sp_0_be;
                r13_0 = r13_0_be;
                continue;
            }
            if (var_13 > rbx_0) {
                var_39 = rbp_2 + 1UL;
                *(uint64_t *)4310528UL = 0UL;
                rcx_2 = rcx_0;
                rdx_3 = rdx_1;
                r13_4 = r13_2;
                rbp_0_be = var_39;
                local_sp_0_be = local_sp_2;
                r13_0_be = r13_2;
                if (var_8 == var_39) {
                    break;
                }
            }
            while (1U)
                {
                    var_26 = *(uint64_t *)4310544UL;
                    var_27 = *(unsigned char *)4309737UL;
                    rax_0 = *(uint64_t *)4310536UL;
                    rbx_2 = rbx_1;
                    rbp_2 = rbp_1;
                    rdx_1 = rdx_0;
                    local_sp_2 = local_sp_1;
                    r13_2 = r13_1;
                    while (1U)
                        {
                            var_28 = rax_0 + 1UL;
                            var_29 = *(uint64_t *)4310728UL;
                            var_30 = var_28 - var_29;
                            *(uint64_t *)4310536UL = var_28;
                            *(unsigned char *)(rax_0 + var_26) = var_27;
                            var_31 = helper_cc_compute_c_wrapper(var_30, var_29, var_6, 17U);
                            rax_0 = var_28;
                            rcx_0 = rax_0;
                            if (var_31 != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            var_32 = rbx_2 + 1UL;
                            var_33 = helper_cc_compute_c_wrapper(var_32 - rdx_0, rdx_0, var_6, 17U);
                            rbx_2 = var_32;
                            if (var_33 == 0UL) {
                                continue;
                            }
                            loop_state_var = 1U;
                            break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 0U:
                        {
                            var_34 = local_sp_1 + (-8L);
                            *(uint64_t *)var_34 = 4212069UL;
                            var_35 = indirect_placeholder_210(rax_0, rdx_0);
                            var_36 = *(uint64_t *)4310720UL;
                            var_37 = rbx_2 + 1UL;
                            var_38 = helper_cc_compute_c_wrapper(var_37 - var_36, var_36, var_6, 17U);
                            rbx_1 = var_37;
                            rdx_0 = var_36;
                            local_sp_1 = var_34;
                            rdx_1 = var_36;
                            local_sp_2 = var_34;
                            if (var_38 != 0UL) {
                                loop_state_var = 0U;
                                switch_state_var = 1;
                                break;
                            }
                            rbp_1 = var_35.field_1;
                            r13_1 = var_35.field_3;
                            continue;
                        }
                        break;
                      case 1U:
                        {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            rcx_0 = var_35.field_0;
            rbp_2 = var_35.field_1;
            r13_2 = var_35.field_3;
        }
    mrv.field_0 = rcx_2;
    mrv1 = mrv;
    mrv1.field_1 = rdx_3;
    mrv2 = mrv1;
    mrv2.field_2 = r13_4;
    return mrv2;
}
