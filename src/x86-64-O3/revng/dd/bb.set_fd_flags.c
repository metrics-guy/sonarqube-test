typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_149_ret_type;
struct indirect_placeholder_150_ret_type;
struct indirect_placeholder_148_ret_type;
struct indirect_placeholder_151_ret_type;
struct indirect_placeholder_149_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_150_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
};
struct indirect_placeholder_148_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_151_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r13(void);
extern void indirect_placeholder_1(void);
extern struct indirect_placeholder_149_ret_type indirect_placeholder_149(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_150_ret_type indirect_placeholder_150(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_148_ret_type indirect_placeholder_148(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_151_ret_type indirect_placeholder_151(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_set_fd_flags(uint64_t rcx, uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_148_ret_type var_26;
    struct indirect_placeholder_151_ret_type var_8;
    struct indirect_placeholder_149_ret_type var_33;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint32_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t local_sp_2;
    uint32_t _pre_phi;
    uint64_t local_sp_0;
    uint64_t rbp_1;
    uint64_t r12_1;
    uint64_t rcx1_0;
    uint64_t rbp_0;
    uint64_t r12_0;
    uint32_t var_28;
    uint64_t var_29;
    uint32_t _pre_phi42;
    uint32_t _pre41;
    uint32_t var_14;
    uint64_t local_sp_1;
    uint64_t rcx1_1;
    uint64_t rbx_1;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_15;
    uint64_t rbp_2;
    uint64_t r12_2;
    uint64_t rbx_2;
    struct indirect_placeholder_150_ret_type var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_27;
    uint32_t _pre;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_9;
    uint64_t var_10;
    uint32_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r12();
    var_3 = init_rbx();
    var_4 = init_r13();
    *(uint64_t *)(var_0 + (-8L)) = var_4;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_3;
    var_5 = (uint64_t)((uint32_t)rsi & (-131329));
    r12_0 = rdx;
    r12_1 = rdx;
    r12_2 = rdx;
    if (var_5 == 0UL) {
        return;
    }
    var_6 = (uint64_t)(uint32_t)rdi;
    var_7 = var_0 + (-192L);
    *(uint64_t *)var_7 = 4207702UL;
    var_8 = indirect_placeholder_151(0UL, rcx, rdx, rdi, 3UL);
    var_9 = var_8.field_0;
    var_10 = var_8.field_1;
    var_11 = (uint32_t)var_9;
    var_12 = (uint64_t)var_11;
    var_13 = var_5 | var_12;
    local_sp_2 = var_7;
    rbp_1 = var_6;
    rcx1_0 = var_10;
    rbp_0 = var_6;
    local_sp_1 = var_7;
    rcx1_1 = var_10;
    rbx_1 = var_13;
    rbp_2 = var_6;
    rbx_2 = var_13;
    if ((int)var_11 < (int)0U) {
        while (1U)
            {
                *(uint64_t *)(local_sp_2 + (-8L)) = 4207800UL;
                var_19 = indirect_placeholder_150(rbp_2, r12_2, 4UL, r12_2);
                var_20 = var_19.field_0;
                var_21 = var_19.field_1;
                var_22 = var_19.field_4;
                var_23 = var_19.field_7;
                *(uint64_t *)(local_sp_2 + (-16L)) = 4207808UL;
                indirect_placeholder_1();
                var_24 = (uint64_t)*(uint32_t *)var_20;
                var_25 = local_sp_2 + (-24L);
                *(uint64_t *)var_25 = 4207830UL;
                var_26 = indirect_placeholder_148(0UL, var_20, 4276303UL, var_22, 1UL, var_24, var_23);
                var_27 = var_26.field_1;
                _pre = (uint32_t)rbx_2;
                _pre_phi = _pre;
                local_sp_0 = var_25;
                rcx1_0 = var_27;
                rbp_0 = var_21;
                r12_0 = var_20;
                var_28 = _pre_phi & (-65537);
                var_29 = (uint64_t)var_28;
                rbp_1 = rbp_0;
                r12_1 = r12_0;
                local_sp_1 = local_sp_0;
                rcx1_1 = rcx1_0;
                rbx_1 = var_29;
                if ((uint64_t)(var_28 - var_11) == 0UL) {
                    break;
                }
                _pre41 = (uint32_t)var_29;
                _pre_phi42 = _pre41;
                var_30 = (uint64_t)_pre_phi42;
                var_31 = (uint64_t)(uint32_t)rbp_1;
                var_32 = local_sp_1 + (-8L);
                *(uint64_t *)var_32 = 4207739UL;
                var_33 = indirect_placeholder_149(0UL, rcx1_1, var_30, var_31, 4UL);
                local_sp_2 = var_32;
                rbp_2 = rbp_1;
                r12_2 = r12_1;
                rbx_2 = rbx_1;
                if ((uint64_t)((uint32_t)var_33.field_0 + 1U) != 0UL) {
                    continue;
                }
                break;
            }
    }
    var_14 = (uint32_t)var_13;
    _pre_phi = var_14;
    _pre_phi42 = var_14;
    if ((uint64_t)(var_11 - var_14) == 0UL) {
        return;
    }
    if ((uint64_t)(var_14 & 65536U) != 0UL) {
        var_30 = (uint64_t)_pre_phi42;
        var_31 = (uint64_t)(uint32_t)rbp_1;
        var_32 = local_sp_1 + (-8L);
        *(uint64_t *)var_32 = 4207739UL;
        var_33 = indirect_placeholder_149(0UL, rcx1_1, var_30, var_31, 4UL);
        local_sp_2 = var_32;
        rbp_2 = rbp_1;
        r12_2 = r12_1;
        rbx_2 = rbx_1;
        if ((uint64_t)((uint32_t)var_33.field_0 + 1U) == 0UL) {
            return;
        }
        while (1U)
            {
                *(uint64_t *)(local_sp_2 + (-8L)) = 4207800UL;
                var_19 = indirect_placeholder_150(rbp_2, r12_2, 4UL, r12_2);
                var_20 = var_19.field_0;
                var_21 = var_19.field_1;
                var_22 = var_19.field_4;
                var_23 = var_19.field_7;
                *(uint64_t *)(local_sp_2 + (-16L)) = 4207808UL;
                indirect_placeholder_1();
                var_24 = (uint64_t)*(uint32_t *)var_20;
                var_25 = local_sp_2 + (-24L);
                *(uint64_t *)var_25 = 4207830UL;
                var_26 = indirect_placeholder_148(0UL, var_20, 4276303UL, var_22, 1UL, var_24, var_23);
                var_27 = var_26.field_1;
                _pre = (uint32_t)rbx_2;
                _pre_phi = _pre;
                local_sp_0 = var_25;
                rcx1_0 = var_27;
                rbp_0 = var_21;
                r12_0 = var_20;
                var_28 = _pre_phi & (-65537);
                var_29 = (uint64_t)var_28;
                rbp_1 = rbp_0;
                r12_1 = r12_0;
                local_sp_1 = local_sp_0;
                rcx1_1 = rcx1_0;
                rbx_1 = var_29;
                if ((uint64_t)(var_28 - var_11) == 0UL) {
                    break;
                }
                _pre41 = (uint32_t)var_29;
                _pre_phi42 = _pre41;
                var_30 = (uint64_t)_pre_phi42;
                var_31 = (uint64_t)(uint32_t)rbp_1;
                var_32 = local_sp_1 + (-8L);
                *(uint64_t *)var_32 = 4207739UL;
                var_33 = indirect_placeholder_149(0UL, rcx1_1, var_30, var_31, 4UL);
                local_sp_2 = var_32;
                rbp_2 = rbp_1;
                r12_2 = r12_1;
                rbx_2 = rbx_1;
                if ((uint64_t)((uint32_t)var_33.field_0 + 1U) != 0UL) {
                    continue;
                }
                break;
            }
        return;
    }
    var_15 = var_0 + (-200L);
    *(uint64_t *)var_15 = 4207756UL;
    indirect_placeholder_1();
    local_sp_0 = var_15;
    local_sp_2 = var_15;
    if (var_12 == 0UL) {
        return;
    }
    var_16 = (uint32_t)((uint16_t)*(uint32_t *)(var_0 + (-176L)) & (unsigned short)61440U);
    var_17 = (uint64_t)var_16;
    if ((uint64_t)((var_16 + (-16384)) & (-4096)) != 0UL) {
        var_28 = _pre_phi & (-65537);
        var_29 = (uint64_t)var_28;
        rbp_1 = rbp_0;
        r12_1 = r12_0;
        local_sp_1 = local_sp_0;
        rcx1_1 = rcx1_0;
        rbx_1 = var_29;
        if ((uint64_t)(var_28 - var_11) == 0UL) {
            return;
        }
        _pre41 = (uint32_t)var_29;
        _pre_phi42 = _pre41;
        var_30 = (uint64_t)_pre_phi42;
        var_31 = (uint64_t)(uint32_t)rbp_1;
        var_32 = local_sp_1 + (-8L);
        *(uint64_t *)var_32 = 4207739UL;
        var_33 = indirect_placeholder_149(0UL, rcx1_1, var_30, var_31, 4UL);
        local_sp_2 = var_32;
        rbp_2 = rbp_1;
        r12_2 = r12_1;
        rbx_2 = rbx_1;
        if ((uint64_t)((uint32_t)var_33.field_0 + 1U) == 0UL) {
            return;
        }
    }
    var_18 = var_0 + (-208L);
    *(uint64_t *)var_18 = 4207781UL;
    indirect_placeholder_1();
    *(uint32_t *)var_17 = 20U;
    local_sp_2 = var_18;
}
