typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_divq_EAX_wrapper_ret_type;
struct type_5;
struct indirect_placeholder_33_ret_type;
struct indirect_placeholder_34_ret_type;
struct helper_divq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint32_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint32_t field_5;
    uint32_t field_6;
    uint64_t field_7;
    uint32_t field_8;
    uint64_t field_9;
    uint32_t field_10;
    unsigned char field_11;
    uint32_t field_12;
};
struct type_5 {
};
struct indirect_placeholder_33_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_34_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r13(void);
extern void indirect_placeholder_1(void);
extern uint32_t init_state_0x8248(void);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern struct helper_divq_EAX_wrapper_ret_type helper_divq_EAX_wrapper(struct type_5 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint32_t param_13, uint64_t param_14, uint32_t param_15, uint64_t param_16, uint64_t param_17, uint64_t param_18, uint32_t param_19);
extern uint64_t init_r9(void);
extern uint64_t init_r10(void);
extern uint64_t init_r8(void);
extern struct indirect_placeholder_33_ret_type indirect_placeholder_33(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_34_ret_type indirect_placeholder_34(uint64_t param_0, uint64_t param_1);
uint64_t bb_invalidate_cache(uint64_t rcx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint32_t var_10;
    uint64_t var_11;
    uint32_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint32_t var_16;
    unsigned char _v;
    uint64_t var_17;
    struct indirect_placeholder_33_ret_type var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    bool var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t r13_0;
    uint64_t var_28;
    uint64_t var_29;
    struct indirect_placeholder_34_ret_type var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t r15_1;
    uint64_t var_34;
    uint64_t r15_2;
    uint64_t rdi2_0;
    uint64_t rcx1_0;
    uint64_t rax_0;
    uint64_t local_sp_0;
    uint64_t r15_3;
    uint64_t rdi2_1;
    uint64_t rcx1_1;
    uint64_t rax_1;
    uint64_t local_sp_1;
    uint64_t rdi2_358;
    uint64_t rsi3_2;
    uint64_t rdi2_2;
    uint64_t var_37;
    uint64_t var_41;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t rsi3_0;
    uint64_t rcx1_2;
    uint64_t rax_2;
    uint64_t local_sp_2;
    uint64_t var_39;
    uint64_t rcx1_361;
    uint64_t local_sp_369;
    uint64_t rax_365;
    uint64_t rcx1_363;
    uint64_t rdi2_360;
    uint64_t r15_457;
    uint64_t var_42;
    uint64_t local_sp_367;
    uint64_t local_sp_366;
    uint64_t var_40;
    struct helper_divq_EAX_wrapper_ret_type var_43;
    uint64_t rax_4;
    uint64_t var_44;
    uint64_t var_38;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r15();
    var_3 = init_r14();
    var_4 = init_r12();
    var_5 = init_r10();
    var_6 = init_rbx();
    var_7 = init_r9();
    var_8 = init_r13();
    var_9 = init_r8();
    var_10 = init_state_0x8248();
    var_11 = init_state_0x9018();
    var_12 = init_state_0x9010();
    var_13 = init_state_0x8408();
    var_14 = init_state_0x8328();
    var_15 = init_state_0x82d8();
    var_16 = init_state_0x9080();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_8;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_1;
    *(uint64_t *)(var_0 + (-48L)) = var_6;
    _v = ((uint64_t)(uint32_t)rdi == 0UL) ? *(unsigned char *)4310377UL : *(unsigned char *)4310376UL;
    var_17 = var_0 + (-64L);
    *(uint64_t *)var_17 = 4208763UL;
    var_18 = indirect_placeholder_33(rdi, rsi);
    var_19 = var_18.field_0;
    var_20 = var_18.field_1;
    var_21 = var_18.field_2;
    var_22 = (var_19 & (-256L)) | (rsi != 0UL);
    var_23 = (var_19 == 0UL);
    var_24 = var_23;
    var_25 = var_8 & (-256L);
    var_26 = ((var_25 | var_24) & var_22) & 1UL;
    var_27 = var_25 | var_26;
    r13_0 = var_27;
    r15_1 = 0UL;
    r15_2 = 0UL;
    rdi2_0 = var_21;
    rcx1_0 = var_20;
    rax_0 = var_22;
    local_sp_0 = var_17;
    rdi2_1 = var_21;
    rcx1_1 = var_20;
    rax_1 = var_22;
    local_sp_1 = var_17;
    rdi2_2 = 1UL;
    if (var_26 == 0UL) {
        return (uint64_t)(uint32_t)r13_0;
    }
    var_28 = (uint64_t)(uint32_t)var_21;
    r13_0 = 0UL;
    if ((var_19 | rsi) == 0UL) {
        r13_0 = 1UL;
        if (_v == '\x00') {
            return (uint64_t)(uint32_t)r13_0;
        }
        if (var_28 != 0UL) {
            r15_3 = r15_1;
            rdi2_2 = rdi2_0;
            rsi3_0 = *(uint64_t *)4310576UL;
            rcx1_2 = rcx1_0;
            rax_2 = rax_0;
            local_sp_2 = local_sp_0;
            if (*(unsigned char *)4310588UL != '\x00') {
                *(uint64_t *)(local_sp_0 + (-8L)) = 4208981UL;
                indirect_placeholder_1();
                *(uint32_t *)rax_0 = 29U;
                return (uint64_t)(uint32_t)r13_0;
            }
        }
        var_34 = *(uint64_t *)4309728UL;
        r15_3 = r15_2;
        rsi3_0 = rax_1;
        rcx1_2 = rcx1_1;
        rax_2 = rax_1;
        local_sp_2 = local_sp_1;
        local_sp_369 = local_sp_1;
        rax_365 = rax_1;
        rcx1_363 = rcx1_1;
        rdi2_360 = rdi2_1;
        r15_457 = r15_2;
        if (var_34 == 18446744073709551615UL) {
            return (uint64_t)(uint32_t)r13_0;
        }
        if ((long)var_34 < (long)0UL) {
            var_38 = local_sp_1 + (-8L);
            *(uint64_t *)var_38 = 4209065UL;
            indirect_placeholder_1();
            *(uint64_t *)4309728UL = rax_1;
            local_sp_2 = var_38;
        } else {
            rdi2_2 = rdi2_1;
            if (rsi != 0UL) {
                var_37 = var_34 - var_19;
                var_41 = var_37;
                rdi2_358 = rdi2_360;
                rsi3_2 = var_41;
                rcx1_361 = rcx1_363;
                local_sp_367 = local_sp_369;
                local_sp_366 = local_sp_369;
                rax_4 = rax_365;
                if (var_23) {
                    if (_v == '\x00') {
                        var_43 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), *(uint64_t *)4310744UL, 4209012UL, rsi3_2, rcx1_361, var_28, var_5, rsi, 0UL, var_7, rdi2_358, rsi3_2, var_9, var_10, var_11, var_12, var_13, var_14, var_15, var_16);
                        local_sp_366 = local_sp_367;
                        rax_4 = var_43.field_1;
                    }
                } else {
                    var_42 = var_41 - r15_457;
                    rsi3_2 = var_42;
                    var_43 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), *(uint64_t *)4310744UL, 4209012UL, rsi3_2, rcx1_361, var_28, var_5, rsi, 0UL, var_7, rdi2_358, rsi3_2, var_9, var_10, var_11, var_12, var_13, var_14, var_15, var_16);
                    local_sp_366 = local_sp_367;
                    rax_4 = var_43.field_1;
                }
                *(uint64_t *)(local_sp_366 + (-8L)) = 4208880UL;
                indirect_placeholder_1();
                var_44 = var_25 | ((uint64_t)((uint32_t)rax_4 + 1U) != 0UL);
                r13_0 = var_44;
                return (uint64_t)(uint32_t)r13_0;
            }
            var_35 = var_19 + r15_2;
            var_36 = var_34 + var_35;
            *(uint64_t *)4309728UL = var_36;
            rsi3_0 = var_36;
            rax_2 = var_35;
        }
    } else {
        if (rsi == 0UL) {
            if (var_28 == 0UL) {
                r15_3 = r15_1;
                rdi2_2 = rdi2_0;
                rsi3_0 = *(uint64_t *)4310576UL;
                rcx1_2 = rcx1_0;
                rax_2 = rax_0;
                local_sp_2 = local_sp_0;
                if (*(unsigned char *)4310588UL != '\x00') {
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4208981UL;
                    indirect_placeholder_1();
                    *(uint32_t *)rax_0 = 29U;
                    return (uint64_t)(uint32_t)r13_0;
                }
            }
            var_34 = *(uint64_t *)4309728UL;
            r15_3 = r15_2;
            rsi3_0 = rax_1;
            rcx1_2 = rcx1_1;
            rax_2 = rax_1;
            local_sp_2 = local_sp_1;
            local_sp_369 = local_sp_1;
            rax_365 = rax_1;
            rcx1_363 = rcx1_1;
            rdi2_360 = rdi2_1;
            r15_457 = r15_2;
            if (var_34 == 18446744073709551615UL) {
                return (uint64_t)(uint32_t)r13_0;
            }
            if ((long)var_34 < (long)0UL) {
                var_38 = local_sp_1 + (-8L);
                *(uint64_t *)var_38 = 4209065UL;
                indirect_placeholder_1();
                *(uint64_t *)4309728UL = rax_1;
                local_sp_2 = var_38;
            } else {
                rdi2_2 = rdi2_1;
                if (rsi != 0UL) {
                    var_37 = var_34 - var_19;
                    var_41 = var_37;
                    rdi2_358 = rdi2_360;
                    rsi3_2 = var_41;
                    rcx1_361 = rcx1_363;
                    local_sp_367 = local_sp_369;
                    local_sp_366 = local_sp_369;
                    rax_4 = rax_365;
                    if (var_23) {
                        var_42 = var_41 - r15_457;
                        rsi3_2 = var_42;
                        var_43 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), *(uint64_t *)4310744UL, 4209012UL, rsi3_2, rcx1_361, var_28, var_5, rsi, 0UL, var_7, rdi2_358, rsi3_2, var_9, var_10, var_11, var_12, var_13, var_14, var_15, var_16);
                        local_sp_366 = local_sp_367;
                        rax_4 = var_43.field_1;
                    } else {
                        if (_v == '\x00') {
                            var_43 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), *(uint64_t *)4310744UL, 4209012UL, rsi3_2, rcx1_361, var_28, var_5, rsi, 0UL, var_7, rdi2_358, rsi3_2, var_9, var_10, var_11, var_12, var_13, var_14, var_15, var_16);
                            local_sp_366 = local_sp_367;
                            rax_4 = var_43.field_1;
                        }
                    }
                    *(uint64_t *)(local_sp_366 + (-8L)) = 4208880UL;
                    indirect_placeholder_1();
                    var_44 = var_25 | ((uint64_t)((uint32_t)rax_4 + 1U) != 0UL);
                    r13_0 = var_44;
                    return (uint64_t)(uint32_t)r13_0;
                }
                var_35 = var_19 + r15_2;
                var_36 = var_34 + var_35;
                *(uint64_t *)4309728UL = var_36;
                rsi3_0 = var_36;
                rax_2 = var_35;
            }
        } else {
            var_29 = var_0 + (-72L);
            *(uint64_t *)var_29 = 4208927UL;
            var_30 = indirect_placeholder_34(var_21, 0UL);
            var_31 = var_30.field_0;
            var_32 = var_30.field_1;
            var_33 = var_30.field_2;
            r15_1 = var_31;
            r15_2 = var_31;
            rdi2_0 = var_33;
            rcx1_0 = var_32;
            rax_0 = var_31;
            local_sp_0 = var_29;
            rdi2_1 = var_33;
            rcx1_1 = var_32;
            rax_1 = var_31;
            local_sp_1 = var_29;
            if (var_28 != 0UL) {
                r15_3 = r15_1;
                rdi2_2 = rdi2_0;
                rsi3_0 = *(uint64_t *)4310576UL;
                rcx1_2 = rcx1_0;
                rax_2 = rax_0;
                local_sp_2 = local_sp_0;
                if (*(unsigned char *)4310588UL != '\x00') {
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4208981UL;
                    indirect_placeholder_1();
                    *(uint32_t *)rax_0 = 29U;
                    return (uint64_t)(uint32_t)r13_0;
                }
            }
            var_34 = *(uint64_t *)4309728UL;
            r15_3 = r15_2;
            rsi3_0 = rax_1;
            rcx1_2 = rcx1_1;
            rax_2 = rax_1;
            local_sp_2 = local_sp_1;
            local_sp_369 = local_sp_1;
            rax_365 = rax_1;
            rcx1_363 = rcx1_1;
            rdi2_360 = rdi2_1;
            r15_457 = r15_2;
            if (var_34 == 18446744073709551615UL) {
                return (uint64_t)(uint32_t)r13_0;
            }
            if ((long)var_34 < (long)0UL) {
                var_38 = local_sp_1 + (-8L);
                *(uint64_t *)var_38 = 4209065UL;
                indirect_placeholder_1();
                *(uint64_t *)4309728UL = rax_1;
                local_sp_2 = var_38;
            } else {
                rdi2_2 = rdi2_1;
                if (rsi != 0UL) {
                    var_37 = var_34 - var_19;
                    var_41 = var_37;
                    rdi2_358 = rdi2_360;
                    rsi3_2 = var_41;
                    rcx1_361 = rcx1_363;
                    local_sp_367 = local_sp_369;
                    local_sp_366 = local_sp_369;
                    rax_4 = rax_365;
                    if (var_23) {
                        if (_v == '\x00') {
                            var_43 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), *(uint64_t *)4310744UL, 4209012UL, rsi3_2, rcx1_361, var_28, var_5, rsi, 0UL, var_7, rdi2_358, rsi3_2, var_9, var_10, var_11, var_12, var_13, var_14, var_15, var_16);
                            local_sp_366 = local_sp_367;
                            rax_4 = var_43.field_1;
                        }
                    } else {
                        var_42 = var_41 - r15_457;
                        rsi3_2 = var_42;
                        var_43 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), *(uint64_t *)4310744UL, 4209012UL, rsi3_2, rcx1_361, var_28, var_5, rsi, 0UL, var_7, rdi2_358, rsi3_2, var_9, var_10, var_11, var_12, var_13, var_14, var_15, var_16);
                        local_sp_366 = local_sp_367;
                        rax_4 = var_43.field_1;
                    }
                    *(uint64_t *)(local_sp_366 + (-8L)) = 4208880UL;
                    indirect_placeholder_1();
                    var_44 = var_25 | ((uint64_t)((uint32_t)rax_4 + 1U) != 0UL);
                    r13_0 = var_44;
                    return (uint64_t)(uint32_t)r13_0;
                }
                var_35 = var_19 + r15_2;
                var_36 = var_34 + var_35;
                *(uint64_t *)4309728UL = var_36;
                rsi3_0 = var_36;
                rax_2 = var_35;
            }
        }
    }
}
