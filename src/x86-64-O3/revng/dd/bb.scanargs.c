typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_divq_EAX_wrapper_ret_type;
struct type_5;
struct indirect_placeholder_156_ret_type;
struct indirect_placeholder_152_ret_type;
struct indirect_placeholder_157_ret_type;
struct indirect_placeholder_154_ret_type;
struct indirect_placeholder_155_ret_type;
struct indirect_placeholder_158_ret_type;
struct indirect_placeholder_160_ret_type;
struct indirect_placeholder_159_ret_type;
struct indirect_placeholder_161_ret_type;
struct indirect_placeholder_163_ret_type;
struct indirect_placeholder_164_ret_type;
struct indirect_placeholder_162_ret_type;
struct indirect_placeholder_165_ret_type;
struct indirect_placeholder_166_ret_type;
struct indirect_placeholder_167_ret_type;
struct indirect_placeholder_168_ret_type;
struct indirect_placeholder_169_ret_type;
struct indirect_placeholder_170_ret_type;
struct indirect_placeholder_171_ret_type;
struct indirect_placeholder_172_ret_type;
struct indirect_placeholder_173_ret_type;
struct indirect_placeholder_174_ret_type;
struct indirect_placeholder_175_ret_type;
struct indirect_placeholder_176_ret_type;
struct indirect_placeholder_177_ret_type;
struct indirect_placeholder_178_ret_type;
struct indirect_placeholder_179_ret_type;
struct indirect_placeholder_153_ret_type;
struct indirect_placeholder_180_ret_type;
struct indirect_placeholder_181_ret_type;
struct indirect_placeholder_182_ret_type;
struct helper_divq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint32_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint32_t field_5;
    uint32_t field_6;
    uint64_t field_7;
    uint32_t field_8;
    uint64_t field_9;
    uint32_t field_10;
    unsigned char field_11;
    uint32_t field_12;
};
struct type_5 {
};
struct indirect_placeholder_156_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_152_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_157_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_154_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_155_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_158_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_160_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_159_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_161_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_163_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_164_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
    uint64_t field_8;
};
struct indirect_placeholder_162_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_165_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_166_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_167_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_168_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_169_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_170_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_171_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_172_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_173_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_174_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_175_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_176_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_177_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_178_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_179_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_153_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_180_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_181_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_182_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r13(void);
extern void indirect_placeholder_1(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint32_t init_state_0x8248(void);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern struct helper_divq_EAX_wrapper_ret_type helper_divq_EAX_wrapper(struct type_5 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint32_t param_13, uint64_t param_14, uint32_t param_15, uint64_t param_16, uint64_t param_17, uint64_t param_18, uint32_t param_19);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
extern struct indirect_placeholder_156_ret_type indirect_placeholder_156(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_152_ret_type indirect_placeholder_152(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_107(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_157_ret_type indirect_placeholder_157(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_154_ret_type indirect_placeholder_154(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_155_ret_type indirect_placeholder_155(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_158_ret_type indirect_placeholder_158(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_160_ret_type indirect_placeholder_160(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_159_ret_type indirect_placeholder_159(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_161_ret_type indirect_placeholder_161(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_163_ret_type indirect_placeholder_163(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_164_ret_type indirect_placeholder_164(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_162_ret_type indirect_placeholder_162(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_165_ret_type indirect_placeholder_165(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_166_ret_type indirect_placeholder_166(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_167_ret_type indirect_placeholder_167(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_168_ret_type indirect_placeholder_168(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_169_ret_type indirect_placeholder_169(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_170_ret_type indirect_placeholder_170(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_171_ret_type indirect_placeholder_171(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_172_ret_type indirect_placeholder_172(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_173_ret_type indirect_placeholder_173(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_174_ret_type indirect_placeholder_174(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_175_ret_type indirect_placeholder_175(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_176_ret_type indirect_placeholder_176(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_177_ret_type indirect_placeholder_177(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_178_ret_type indirect_placeholder_178(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_179_ret_type indirect_placeholder_179(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_153_ret_type indirect_placeholder_153(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_180_ret_type indirect_placeholder_180(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
extern struct indirect_placeholder_181_ret_type indirect_placeholder_181(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_182_ret_type indirect_placeholder_182(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
void bb_scanargs(uint64_t r10, uint64_t r9, uint64_t rdi, uint64_t rsi, uint64_t r8) {
    uint64_t var_116;
    struct indirect_placeholder_156_ret_type var_117;
    uint64_t var_118;
    uint64_t var_119;
    uint64_t var_115;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t *var_96;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint32_t var_8;
    uint64_t var_9;
    uint32_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint32_t var_14;
    uint64_t var_15;
    uint32_t var_16;
    uint64_t local_sp_16;
    uint64_t local_sp_17;
    uint64_t r92_0;
    uint64_t r15_0;
    uint64_t rcx_0;
    uint64_t r85_0;
    uint64_t local_sp_0;
    uint64_t r15_9;
    uint64_t r101_4;
    uint64_t var_109;
    uint64_t r15_1;
    uint64_t r15_2;
    uint64_t r15_8;
    uint64_t r14_5;
    uint64_t r12_0;
    uint64_t r101_3;
    uint64_t rbx_2;
    uint64_t r92_7;
    uint64_t rdi3_4;
    uint64_t var_110;
    uint64_t *var_111;
    struct indirect_placeholder_157_ret_type var_112;
    uint64_t var_113;
    uint64_t var_114;
    uint64_t var_104;
    uint64_t *var_105;
    struct indirect_placeholder_154_ret_type var_106;
    uint64_t var_107;
    uint64_t var_108;
    uint64_t var_100;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t var_123;
    uint64_t var_124;
    uint64_t var_121;
    struct indirect_placeholder_155_ret_type var_122;
    uint64_t rbx_1;
    uint64_t var_120;
    struct indirect_placeholder_158_ret_type var_97;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t rdi3_0;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t r85_1;
    uint64_t rax_0;
    uint64_t local_sp_1;
    uint64_t local_sp_2;
    uint64_t r15_3;
    uint64_t var_103;
    uint64_t rdi3_1;
    uint64_t r85_2;
    uint64_t local_sp_3;
    uint32_t var_125;
    uint64_t var_126;
    uint64_t rbp_1;
    uint64_t local_sp_4;
    uint64_t r14_4_ph;
    uint64_t r15_4;
    uint64_t r14_1;
    uint64_t r13_1;
    uint64_t local_sp_10;
    uint64_t rax_2;
    struct indirect_placeholder_160_ret_type var_127;
    uint64_t var_128;
    uint64_t var_129;
    uint64_t var_130;
    uint64_t var_131;
    uint64_t r101_2_ph;
    uint64_t var_87;
    struct indirect_placeholder_161_ret_type var_88;
    uint64_t var_198;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_64;
    struct indirect_placeholder_163_ret_type var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    struct indirect_placeholder_164_ret_type var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    struct indirect_placeholder_162_ret_type var_80;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t var_62;
    struct indirect_placeholder_165_ret_type var_63;
    uint64_t r14_0;
    uint64_t r13_0;
    uint64_t local_sp_5;
    uint64_t var_132;
    struct indirect_placeholder_166_ret_type var_133;
    uint64_t var_134;
    uint64_t var_135;
    uint64_t var_136;
    uint64_t var_137;
    uint64_t var_138;
    uint64_t var_139;
    uint64_t var_140;
    uint64_t var_59;
    uint64_t var_60;
    struct indirect_placeholder_167_ret_type var_61;
    uint64_t var_50;
    struct indirect_placeholder_168_ret_type var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_48;
    struct indirect_placeholder_169_ret_type var_49;
    uint64_t var_39;
    struct indirect_placeholder_170_ret_type var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_214;
    uint64_t var_215;
    uint64_t var_201;
    uint64_t var_202;
    uint64_t var_203;
    uint64_t local_sp_6;
    uint64_t var_204;
    uint64_t rdi3_5;
    uint64_t local_sp_15;
    struct indirect_placeholder_171_ret_type var_205;
    uint64_t var_206;
    uint64_t var_207;
    uint64_t var_208;
    uint64_t var_199;
    uint64_t var_200;
    uint64_t r92_1;
    uint64_t rcx_1;
    uint64_t r85_3;
    uint64_t local_sp_7;
    uint64_t var_211;
    uint64_t var_195;
    uint64_t var_196;
    uint64_t var_197;
    uint64_t r92_2;
    uint64_t rcx_2;
    uint64_t r85_4;
    uint64_t local_sp_8;
    uint64_t var_209;
    struct indirect_placeholder_173_ret_type var_210;
    uint64_t var_192;
    uint64_t var_193;
    uint64_t var_194;
    uint64_t r92_3;
    uint64_t rcx_3;
    uint64_t r85_5;
    uint64_t local_sp_9;
    uint64_t var_189;
    uint64_t var_186;
    uint64_t var_187;
    uint64_t var_188;
    uint64_t var_184;
    struct indirect_placeholder_175_ret_type var_185;
    uint64_t var_36;
    uint64_t rbp_3;
    uint64_t var_37;
    struct indirect_placeholder_176_ret_type var_38;
    uint64_t var_33;
    struct indirect_placeholder_177_ret_type var_34;
    uint64_t var_35;
    uint64_t var_32;
    uint64_t local_sp_13;
    uint64_t var_26;
    uint64_t var_27;
    struct indirect_placeholder_178_ret_type var_28;
    uint64_t var_29;
    bool var_30;
    uint64_t var_31;
    uint64_t rbp_0;
    struct indirect_placeholder_179_ret_type var_216;
    uint64_t var_217;
    uint64_t var_218;
    uint64_t var_219;
    uint64_t r15_5;
    uint64_t r14_2;
    uint64_t rbx_1_ph;
    uint64_t r101_0;
    uint64_t rax_2_ph;
    uint64_t r92_4;
    uint64_t rdi3_2;
    uint64_t r13_2;
    uint64_t r85_6;
    uint64_t rax_1;
    uint64_t local_sp_11;
    uint64_t var_141;
    uint64_t rbp_2;
    uint64_t rbp_3_ph;
    uint64_t r15_6;
    uint64_t r14_3;
    uint64_t r101_1;
    uint64_t rbx_0;
    uint64_t r92_5;
    uint64_t rdi3_3;
    uint64_t r13_3;
    uint64_t r85_7;
    uint64_t local_sp_12;
    uint64_t rbp_4;
    uint64_t rbp_5;
    uint64_t var_17;
    uint32_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t r15_7_ph;
    uint64_t r92_6_ph;
    uint64_t local_sp_13_ph;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t r13_4;
    uint64_t r85_8;
    uint64_t local_sp_14;
    uint64_t r14_6;
    uint64_t r12_1;
    uint64_t rbx_3;
    uint64_t r92_8;
    uint64_t r13_5;
    uint64_t r85_9;
    uint64_t rcx_4;
    uint32_t var_142;
    uint64_t var_143;
    uint64_t var_144;
    uint32_t var_145;
    uint64_t var_146;
    uint64_t var_153;
    bool var_154;
    bool var_155;
    uint32_t state_0x8248_0;
    struct indirect_placeholder_181_ret_type var_213;
    uint64_t var_157;
    uint32_t var_158;
    uint64_t var_159;
    struct helper_divq_EAX_wrapper_ret_type var_156;
    uint32_t var_160;
    uint64_t var_161;
    uint32_t var_162;
    uint64_t state_0x9018_0;
    uint32_t state_0x9010_0;
    uint64_t state_0x82d8_0;
    uint32_t state_0x9080_0;
    uint64_t var_163;
    bool var_164;
    bool var_165;
    uint32_t state_0x8248_1;
    uint64_t var_167;
    uint32_t var_168;
    uint64_t var_169;
    struct helper_divq_EAX_wrapper_ret_type var_166;
    uint32_t var_170;
    uint64_t var_171;
    uint32_t var_172;
    uint64_t state_0x9018_1;
    uint32_t state_0x9010_1;
    uint64_t state_0x82d8_1;
    uint32_t state_0x9080_1;
    uint64_t var_173;
    bool var_174;
    bool var_175;
    uint64_t var_177;
    struct helper_divq_EAX_wrapper_ret_type var_176;
    uint64_t var_178;
    uint64_t var_179;
    uint16_t var_180;
    uint64_t rax_3;
    uint64_t var_181;
    uint32_t var_182;
    uint64_t var_183;
    uint64_t var_147;
    uint64_t var_148;
    uint64_t var_149;
    struct indirect_placeholder_180_ret_type var_150;
    uint64_t var_151;
    uint64_t var_152;
    uint64_t var_212;
    uint64_t var_190;
    struct indirect_placeholder_182_ret_type var_191;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r15();
    var_3 = init_r14();
    var_4 = init_r12();
    var_5 = init_rbx();
    var_6 = init_cc_src2();
    var_7 = init_r13();
    var_8 = init_state_0x8248();
    var_9 = init_state_0x9018();
    var_10 = init_state_0x9010();
    var_11 = init_state_0x8408();
    var_12 = init_state_0x8328();
    var_13 = init_state_0x82d8();
    var_14 = init_state_0x9080();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_7;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_1;
    *(uint64_t *)(var_0 + (-48L)) = var_5;
    var_15 = var_0 + (-104L);
    var_16 = *(uint32_t *)4309884UL;
    rcx_0 = 4276879UL;
    r15_8 = var_2;
    r14_5 = var_3;
    r12_0 = var_4;
    r101_3 = r10;
    rbx_2 = var_5;
    r92_7 = r9;
    rdi3_4 = rdi;
    r14_4_ph = 9223372036854775807UL;
    r101_2_ph = r10;
    rbp_3_ph = var_1;
    rbp_4 = var_1;
    r15_7_ph = 0UL;
    r92_6_ph = r9;
    local_sp_13_ph = var_15;
    r13_4 = var_7;
    r85_8 = r8;
    local_sp_14 = var_15;
    state_0x8248_0 = var_8;
    state_0x9018_0 = var_9;
    state_0x9010_0 = var_10;
    state_0x82d8_0 = var_13;
    state_0x9080_0 = var_14;
    rax_3 = 4210240UL;
    if ((long)((uint64_t)var_16 << 32UL) < (long)(rdi << 32UL)) {
        *(uint64_t *)(var_0 + (-80L)) = 0UL;
        var_17 = (uint64_t)var_16;
        var_18 = var_16 ^ (-1);
        *(uint64_t *)(var_0 + (-96L)) = 0UL;
        var_19 = (uint64_t)(var_18 + (uint32_t)rdi);
        var_20 = (var_17 << 3UL) + rsi;
        *(uint64_t *)(var_0 + (-88L)) = 18446744073709551615UL;
        var_21 = var_19 + var_17;
        var_22 = (var_21 << 3UL) + rsi;
        var_23 = var_22 + 8UL;
        r15_8 = 0UL;
        r12_0 = var_23;
        rbx_1_ph = var_20;
        rax_2_ph = var_21;
        r12_1 = var_23;
        while (1U)
            {
                rbx_1 = rbx_1_ph;
                r15_4 = r15_7_ph;
                r14_1 = r14_4_ph;
                rax_2 = rax_2_ph;
                r14_0 = r14_4_ph;
                rbp_3 = rbp_3_ph;
                local_sp_13 = local_sp_13_ph;
                r15_5 = r15_7_ph;
                r14_2 = r14_4_ph;
                r101_0 = r101_2_ph;
                r92_4 = r92_6_ph;
                r15_6 = r15_7_ph;
                r14_3 = r14_4_ph;
                r101_1 = r101_2_ph;
                r92_5 = r92_6_ph;
                while (1U)
                    {
                        var_24 = *(uint64_t *)rbx_1;
                        var_25 = local_sp_13 + (-8L);
                        *(uint64_t *)var_25 = 4217088UL;
                        indirect_placeholder_1();
                        r13_1 = var_24;
                        local_sp_10 = var_25;
                        r13_0 = var_24;
                        rbp_0 = rbp_3;
                        r13_2 = var_24;
                        r13_3 = var_24;
                        if (rax_2 != 0UL) {
                            loop_state_var = 2U;
                            break;
                        }
                        var_26 = rax_2 + 1UL;
                        var_27 = local_sp_13 + (-16L);
                        *(uint64_t *)var_27 = 4217114UL;
                        var_28 = indirect_placeholder_178(var_24, 4276818UL);
                        var_29 = var_28.field_0;
                        var_30 = ((uint64_t)(unsigned char)var_29 == 0UL);
                        var_31 = var_28.field_1;
                        rbp_1 = var_26;
                        rbp_3 = var_26;
                        rbp_0 = var_26;
                        rdi3_2 = var_31;
                        rax_1 = var_29;
                        local_sp_11 = var_27;
                        rbp_2 = var_26;
                        if (!var_30) {
                            loop_state_var = 3U;
                            break;
                        }
                        var_33 = local_sp_13 + (-24L);
                        *(uint64_t *)var_33 = 4217128UL;
                        var_34 = indirect_placeholder_177(var_31, 4276821UL);
                        var_35 = var_34.field_0;
                        local_sp_12 = var_33;
                        rax_2 = var_35;
                        local_sp_13 = var_33;
                        if ((uint64_t)(unsigned char)var_35 != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_36 = rbx_1 + 8UL;
                        *(uint64_t *)4310752UL = var_26;
                        rbx_0 = var_36;
                        rbx_1 = var_36;
                        if (var_22 == rbx_1) {
                            continue;
                        }
                        rdi3_3 = var_34.field_1;
                        r85_7 = var_34.field_2;
                        loop_state_var = 0U;
                        break;
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 1U:
                    {
                        var_37 = var_34.field_1;
                        *(uint64_t *)(local_sp_13 + (-32L)) = 4217682UL;
                        var_38 = indirect_placeholder_176(var_37, 4276824UL);
                        if ((uint64_t)(unsigned char)var_38.field_0 != 0UL) {
                            var_39 = local_sp_13 + (-40L);
                            *(uint64_t *)var_39 = 4217794UL;
                            var_40 = indirect_placeholder_170(4276829UL, var_26, 4282880UL);
                            var_41 = var_40.field_0;
                            var_42 = var_40.field_1;
                            var_43 = var_40.field_2;
                            var_44 = var_40.field_3;
                            var_45 = var_40.field_4;
                            var_46 = var_40.field_5;
                            var_47 = var_40.field_6;
                            *(uint32_t *)4310668UL = (*(uint32_t *)4310668UL | (uint32_t)var_41);
                            rbp_1 = var_42;
                            r15_5 = var_43;
                            r101_0 = var_44;
                            r92_4 = var_45;
                            rdi3_2 = var_46;
                            r85_6 = var_47;
                            rax_1 = var_41;
                            local_sp_11 = var_39;
                            var_141 = rbx_1 + 8UL;
                            r14_4_ph = r14_2;
                            r101_2_ph = r101_0;
                            rbx_1_ph = var_141;
                            rax_2_ph = rax_1;
                            rbp_2 = rbp_1;
                            rbp_3_ph = rbp_1;
                            r15_6 = r15_5;
                            r14_3 = r14_2;
                            r101_1 = r101_0;
                            rbx_0 = var_141;
                            r92_5 = r92_4;
                            rdi3_3 = rdi3_2;
                            r13_3 = r13_2;
                            r85_7 = r85_6;
                            local_sp_12 = local_sp_11;
                            r15_7_ph = r15_5;
                            r92_6_ph = r92_4;
                            local_sp_13_ph = local_sp_11;
                            if (var_22 == rbx_1) {
                                continue;
                            }
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        var_48 = var_38.field_1;
                        *(uint64_t *)(local_sp_13 + (-40L)) = 4217696UL;
                        var_49 = indirect_placeholder_169(var_48, 4276848UL);
                        if ((uint64_t)(unsigned char)var_49.field_0 == 0UL) {
                            var_59 = var_49.field_1;
                            var_60 = local_sp_13 + (-48L);
                            *(uint64_t *)var_60 = 4217710UL;
                            var_61 = indirect_placeholder_167(var_59, 4276873UL);
                            local_sp_5 = var_60;
                            if ((uint64_t)(unsigned char)var_61.field_0 == 0UL) {
                                var_132 = local_sp_5 + (-8L);
                                *(uint64_t *)var_132 = 4218002UL;
                                var_133 = indirect_placeholder_166(4276879UL, var_26, 4282560UL);
                                var_134 = var_133.field_0;
                                var_135 = var_133.field_1;
                                var_136 = var_133.field_2;
                                var_137 = var_133.field_3;
                                var_138 = var_133.field_4;
                                var_139 = var_133.field_5;
                                var_140 = var_133.field_6;
                                *(uint32_t *)4310660UL = (*(uint32_t *)4310660UL | (uint32_t)var_134);
                                rbp_1 = var_135;
                                r15_5 = var_136;
                                r14_2 = r14_0;
                                r101_0 = var_137;
                                r92_4 = var_138;
                                rdi3_2 = var_139;
                                r13_2 = r13_0;
                                r85_6 = var_140;
                                rax_1 = var_134;
                                local_sp_11 = var_132;
                            } else {
                                var_62 = var_61.field_1;
                                *(uint64_t *)(local_sp_13 + (-56L)) = 4217728UL;
                                var_63 = indirect_placeholder_165(var_62, 4276899UL);
                                if ((uint64_t)(unsigned char)var_63.field_0 != 0UL) {
                                    var_64 = local_sp_13 + (-64L);
                                    *(uint64_t *)var_64 = 4217755UL;
                                    var_65 = indirect_placeholder_163(4276906UL, 1UL, var_26, 4282496UL);
                                    var_66 = var_65.field_0;
                                    var_67 = var_65.field_1;
                                    var_68 = var_65.field_2;
                                    var_69 = var_65.field_3;
                                    var_70 = var_65.field_4;
                                    var_71 = var_65.field_5;
                                    *(uint32_t *)4309740UL = (uint32_t)var_66;
                                    r15_5 = var_67;
                                    r101_0 = var_68;
                                    r92_4 = var_69;
                                    rdi3_2 = var_70;
                                    r85_6 = var_71;
                                    rax_1 = var_66;
                                    local_sp_11 = var_64;
                                    var_141 = rbx_1 + 8UL;
                                    r14_4_ph = r14_2;
                                    r101_2_ph = r101_0;
                                    rbx_1_ph = var_141;
                                    rax_2_ph = rax_1;
                                    rbp_2 = rbp_1;
                                    rbp_3_ph = rbp_1;
                                    r15_6 = r15_5;
                                    r14_3 = r14_2;
                                    r101_1 = r101_0;
                                    rbx_0 = var_141;
                                    r92_5 = r92_4;
                                    rdi3_3 = rdi3_2;
                                    r13_3 = r13_2;
                                    r85_7 = r85_6;
                                    local_sp_12 = local_sp_11;
                                    r15_7_ph = r15_5;
                                    r92_6_ph = r92_4;
                                    local_sp_13_ph = local_sp_11;
                                    if (var_22 == rbx_1) {
                                        continue;
                                    }
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                var_72 = local_sp_13 + (-12L);
                                *(uint32_t *)var_72 = 0U;
                                *(uint64_t *)(local_sp_13 + (-64L)) = 4217861UL;
                                var_73 = indirect_placeholder_164(var_26, var_72);
                                var_74 = var_73.field_0;
                                var_75 = var_73.field_1;
                                var_76 = var_73.field_2;
                                var_77 = var_73.field_3;
                                var_78 = var_73.field_7;
                                var_79 = local_sp_13 + (-72L);
                                *(uint64_t *)var_79 = 4217877UL;
                                var_80 = indirect_placeholder_162(var_78, 4276927UL);
                                var_81 = var_80.field_0;
                                var_82 = var_80.field_1;
                                r15_0 = var_75;
                                r15_1 = var_75;
                                r15_2 = var_75;
                                rdi3_0 = var_82;
                                local_sp_1 = var_79;
                                r15_4 = var_75;
                                r14_1 = var_76;
                                r13_1 = var_78;
                                r14_0 = var_76;
                                r13_0 = var_78;
                                r14_2 = var_76;
                                r101_0 = var_77;
                                r92_4 = var_74;
                                r13_2 = var_78;
                                if ((uint64_t)(unsigned char)var_81 == 0UL) {
                                    var_87 = local_sp_13 + (-80L);
                                    *(uint64_t *)var_87 = 4218023UL;
                                    var_88 = indirect_placeholder_161(var_82, 4276931UL);
                                    var_89 = var_88.field_0;
                                    var_90 = var_88.field_1;
                                    r15_0 = var_74;
                                    rdi3_0 = var_90;
                                    local_sp_1 = var_87;
                                    if ((uint64_t)(unsigned char)var_89 == 0UL) {
                                        var_95 = local_sp_13 + (-88L);
                                        var_96 = (uint64_t *)var_95;
                                        *var_96 = 4218252UL;
                                        var_97 = indirect_placeholder_158(var_90, 4276936UL);
                                        var_98 = var_97.field_0;
                                        var_99 = var_97.field_1;
                                        rdi3_0 = var_99;
                                        local_sp_1 = var_95;
                                        if ((uint64_t)(unsigned char)var_98 == 0UL) {
                                            var_104 = local_sp_13 + (-96L);
                                            var_105 = (uint64_t *)var_104;
                                            *var_105 = 4218270UL;
                                            var_106 = indirect_placeholder_154(var_99, 4276935UL);
                                            var_107 = var_106.field_0;
                                            var_108 = var_106.field_1;
                                            local_sp_2 = var_104;
                                            rdi3_1 = var_108;
                                            local_sp_3 = var_104;
                                            if ((uint64_t)(unsigned char)var_107 == 0UL) {
                                                var_110 = local_sp_13 + (-104L);
                                                var_111 = (uint64_t *)var_110;
                                                *var_111 = 4218088UL;
                                                var_112 = indirect_placeholder_157(var_108, 4276537UL);
                                                var_113 = var_112.field_0;
                                                var_114 = var_112.field_1;
                                                rdi3_1 = var_114;
                                                local_sp_3 = var_110;
                                                if ((uint64_t)(unsigned char)var_113 == 0UL) {
                                                    var_115 = var_112.field_2;
                                                    *var_105 = var_74;
                                                    r85_2 = var_115;
                                                } else {
                                                    var_116 = local_sp_13 + (-112L);
                                                    *(uint64_t *)var_116 = 4218106UL;
                                                    var_117 = indirect_placeholder_156(var_114, 4276335UL);
                                                    var_118 = var_117.field_0;
                                                    var_119 = var_117.field_1;
                                                    rdi3_1 = var_119;
                                                    local_sp_3 = var_116;
                                                    if ((uint64_t)(unsigned char)var_118 == 0UL) {
                                                        var_120 = var_117.field_2;
                                                        *var_96 = var_74;
                                                        r85_2 = var_120;
                                                    } else {
                                                        var_121 = local_sp_13 + (-120L);
                                                        *(uint64_t *)var_121 = 4218124UL;
                                                        var_122 = indirect_placeholder_155(var_119, 4276939UL);
                                                        local_sp_3 = var_121;
                                                        local_sp_10 = var_121;
                                                        if ((uint64_t)(unsigned char)var_122.field_0 != 0UL) {
                                                            loop_state_var = 1U;
                                                            switch_state_var = 1;
                                                            break;
                                                        }
                                                        var_123 = var_122.field_2;
                                                        var_124 = var_122.field_1;
                                                        *var_111 = var_74;
                                                        rdi3_1 = var_124;
                                                        r85_2 = var_123;
                                                    }
                                                }
                                                var_125 = *(uint32_t *)(local_sp_3 + 44UL);
                                                var_126 = (uint64_t)var_125;
                                                r15_3 = r15_2;
                                                local_sp_4 = local_sp_3;
                                                r15_5 = r15_2;
                                                rdi3_2 = rdi3_1;
                                                r85_6 = r85_2;
                                                rax_1 = var_126;
                                                local_sp_11 = local_sp_3;
                                                if (var_125 != 0U) {
                                                    var_141 = rbx_1 + 8UL;
                                                    r14_4_ph = r14_2;
                                                    r101_2_ph = r101_0;
                                                    rbx_1_ph = var_141;
                                                    rax_2_ph = rax_1;
                                                    rbp_2 = rbp_1;
                                                    rbp_3_ph = rbp_1;
                                                    r15_6 = r15_5;
                                                    r14_3 = r14_2;
                                                    r101_1 = r101_0;
                                                    rbx_0 = var_141;
                                                    r92_5 = r92_4;
                                                    rdi3_3 = rdi3_2;
                                                    r13_3 = r13_2;
                                                    r85_7 = r85_6;
                                                    local_sp_12 = local_sp_11;
                                                    r15_7_ph = r15_5;
                                                    r92_6_ph = r92_4;
                                                    local_sp_13_ph = local_sp_11;
                                                    if (var_22 == rbx_1) {
                                                        continue;
                                                    }
                                                    loop_state_var = 0U;
                                                    switch_state_var = 1;
                                                    break;
                                                }
                                            }
                                            var_109 = var_106.field_2;
                                            *(uint64_t *)4310720UL = var_74;
                                            r85_2 = var_109;
                                            if (var_74 != 0UL) {
                                                var_125 = *(uint32_t *)(local_sp_3 + 44UL);
                                                var_126 = (uint64_t)var_125;
                                                r15_3 = r15_2;
                                                local_sp_4 = local_sp_3;
                                                r15_5 = r15_2;
                                                rdi3_2 = rdi3_1;
                                                r85_6 = r85_2;
                                                rax_1 = var_126;
                                                local_sp_11 = local_sp_3;
                                                if (var_125 != 0U) {
                                                    var_141 = rbx_1 + 8UL;
                                                    r14_4_ph = r14_2;
                                                    r101_2_ph = r101_0;
                                                    rbx_1_ph = var_141;
                                                    rax_2_ph = rax_1;
                                                    rbp_2 = rbp_1;
                                                    rbp_3_ph = rbp_1;
                                                    r15_6 = r15_5;
                                                    r14_3 = r14_2;
                                                    r101_1 = r101_0;
                                                    rbx_0 = var_141;
                                                    r92_5 = r92_4;
                                                    rdi3_3 = rdi3_2;
                                                    r13_3 = r13_2;
                                                    r85_7 = r85_6;
                                                    local_sp_12 = local_sp_11;
                                                    r15_7_ph = r15_5;
                                                    r92_6_ph = r92_4;
                                                    local_sp_13_ph = local_sp_11;
                                                    if (var_22 == rbx_1) {
                                                        continue;
                                                    }
                                                    loop_state_var = 0U;
                                                    switch_state_var = 1;
                                                    break;
                                                }
                                            }
                                            *(uint32_t *)(local_sp_2 + 44UL) = 4U;
                                            r15_3 = r15_1;
                                            local_sp_4 = local_sp_2;
                                        } else {
                                            var_100 = var_97.field_2;
                                            var_101 = 18446744073709551612UL - (-4L);
                                            var_102 = (var_101 > var_76) ? var_76 : var_101;
                                            r85_1 = var_100;
                                            rax_0 = var_102;
                                            r15_1 = r15_0;
                                            r15_2 = r15_0;
                                            local_sp_2 = local_sp_1;
                                            r15_3 = r15_0;
                                            rdi3_1 = rdi3_0;
                                            r85_2 = r85_1;
                                            local_sp_3 = local_sp_1;
                                            local_sp_4 = local_sp_1;
                                            if (var_74 == 0UL) {
                                                var_103 = helper_cc_compute_c_wrapper(rax_0 - var_74, var_74, var_6, 17U);
                                                if (var_103 != 0UL) {
                                                    var_125 = *(uint32_t *)(local_sp_3 + 44UL);
                                                    var_126 = (uint64_t)var_125;
                                                    r15_3 = r15_2;
                                                    local_sp_4 = local_sp_3;
                                                    r15_5 = r15_2;
                                                    rdi3_2 = rdi3_1;
                                                    r85_6 = r85_2;
                                                    rax_1 = var_126;
                                                    local_sp_11 = local_sp_3;
                                                    if (var_125 != 0U) {
                                                        var_141 = rbx_1 + 8UL;
                                                        r14_4_ph = r14_2;
                                                        r101_2_ph = r101_0;
                                                        rbx_1_ph = var_141;
                                                        rax_2_ph = rax_1;
                                                        rbp_2 = rbp_1;
                                                        rbp_3_ph = rbp_1;
                                                        r15_6 = r15_5;
                                                        r14_3 = r14_2;
                                                        r101_1 = r101_0;
                                                        rbx_0 = var_141;
                                                        r92_5 = r92_4;
                                                        rdi3_3 = rdi3_2;
                                                        r13_3 = r13_2;
                                                        r85_7 = r85_6;
                                                        local_sp_12 = local_sp_11;
                                                        r15_7_ph = r15_5;
                                                        r92_6_ph = r92_4;
                                                        local_sp_13_ph = local_sp_11;
                                                        if (var_22 == rbx_1) {
                                                            continue;
                                                        }
                                                        loop_state_var = 0U;
                                                        switch_state_var = 1;
                                                        break;
                                                    }
                                                }
                                                *(uint32_t *)(local_sp_1 + 44UL) = 1U;
                                            } else {
                                                *(uint32_t *)(local_sp_2 + 44UL) = 4U;
                                                r15_3 = r15_1;
                                                local_sp_4 = local_sp_2;
                                                *(uint64_t *)(local_sp_4 + (-8L)) = 4217941UL;
                                                var_127 = indirect_placeholder_160(var_26, r15_3, var_76, var_23, rbx_1, var_26, var_78);
                                                var_128 = var_127.field_0;
                                                var_129 = var_127.field_3;
                                                var_130 = (*(uint32_t *)(local_sp_4 + 36UL) == 1U) ? 75UL : 0UL;
                                                var_131 = local_sp_4 + (-16L);
                                                *(uint64_t *)var_131 = 4217982UL;
                                                indirect_placeholder_159(0UL, 4276945UL, 4276764UL, var_129, 1UL, var_130, var_128);
                                                local_sp_5 = var_131;
                                            }
                                        }
                                    } else {
                                        var_91 = var_88.field_2;
                                        var_92 = *(uint64_t *)4310744UL;
                                        *(uint64_t *)4310728UL = var_74;
                                        var_93 = 0UL - var_92;
                                        var_94 = (var_76 < var_93) ? var_76 : var_93;
                                        r85_1 = var_91;
                                        rax_0 = var_94;
                                        r15_1 = r15_0;
                                        r15_2 = r15_0;
                                        local_sp_2 = local_sp_1;
                                        r15_3 = r15_0;
                                        rdi3_1 = rdi3_0;
                                        r85_2 = r85_1;
                                        local_sp_3 = local_sp_1;
                                        local_sp_4 = local_sp_1;
                                        if (var_74 != 0UL) {
                                            *(uint32_t *)(local_sp_2 + 44UL) = 4U;
                                            r15_3 = r15_1;
                                            local_sp_4 = local_sp_2;
                                            *(uint64_t *)(local_sp_4 + (-8L)) = 4217941UL;
                                            var_127 = indirect_placeholder_160(var_26, r15_3, var_76, var_23, rbx_1, var_26, var_78);
                                            var_128 = var_127.field_0;
                                            var_129 = var_127.field_3;
                                            var_130 = (*(uint32_t *)(local_sp_4 + 36UL) == 1U) ? 75UL : 0UL;
                                            var_131 = local_sp_4 + (-16L);
                                            *(uint64_t *)var_131 = 4217982UL;
                                            indirect_placeholder_159(0UL, 4276945UL, 4276764UL, var_129, 1UL, var_130, var_128);
                                            local_sp_5 = var_131;
                                            var_132 = local_sp_5 + (-8L);
                                            *(uint64_t *)var_132 = 4218002UL;
                                            var_133 = indirect_placeholder_166(4276879UL, var_26, 4282560UL);
                                            var_134 = var_133.field_0;
                                            var_135 = var_133.field_1;
                                            var_136 = var_133.field_2;
                                            var_137 = var_133.field_3;
                                            var_138 = var_133.field_4;
                                            var_139 = var_133.field_5;
                                            var_140 = var_133.field_6;
                                            *(uint32_t *)4310660UL = (*(uint32_t *)4310660UL | (uint32_t)var_134);
                                            rbp_1 = var_135;
                                            r15_5 = var_136;
                                            r14_2 = r14_0;
                                            r101_0 = var_137;
                                            r92_4 = var_138;
                                            rdi3_2 = var_139;
                                            r13_2 = r13_0;
                                            r85_6 = var_140;
                                            rax_1 = var_134;
                                            local_sp_11 = var_132;
                                            var_141 = rbx_1 + 8UL;
                                            r14_4_ph = r14_2;
                                            r101_2_ph = r101_0;
                                            rbx_1_ph = var_141;
                                            rax_2_ph = rax_1;
                                            rbp_2 = rbp_1;
                                            rbp_3_ph = rbp_1;
                                            r15_6 = r15_5;
                                            r14_3 = r14_2;
                                            r101_1 = r101_0;
                                            rbx_0 = var_141;
                                            r92_5 = r92_4;
                                            rdi3_3 = rdi3_2;
                                            r13_3 = r13_2;
                                            r85_7 = r85_6;
                                            local_sp_12 = local_sp_11;
                                            r15_7_ph = r15_5;
                                            r92_6_ph = r92_4;
                                            local_sp_13_ph = local_sp_11;
                                            if (var_22 == rbx_1) {
                                                continue;
                                            }
                                            loop_state_var = 0U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                        var_103 = helper_cc_compute_c_wrapper(rax_0 - var_74, var_74, var_6, 17U);
                                        if (var_103 != 0UL) {
                                            var_125 = *(uint32_t *)(local_sp_3 + 44UL);
                                            var_126 = (uint64_t)var_125;
                                            r15_3 = r15_2;
                                            local_sp_4 = local_sp_3;
                                            r15_5 = r15_2;
                                            rdi3_2 = rdi3_1;
                                            r85_6 = r85_2;
                                            rax_1 = var_126;
                                            local_sp_11 = local_sp_3;
                                            if (var_125 != 0U) {
                                                var_141 = rbx_1 + 8UL;
                                                r14_4_ph = r14_2;
                                                r101_2_ph = r101_0;
                                                rbx_1_ph = var_141;
                                                rax_2_ph = rax_1;
                                                rbp_2 = rbp_1;
                                                rbp_3_ph = rbp_1;
                                                r15_6 = r15_5;
                                                r14_3 = r14_2;
                                                r101_1 = r101_0;
                                                rbx_0 = var_141;
                                                r92_5 = r92_4;
                                                rdi3_3 = rdi3_2;
                                                r13_3 = r13_2;
                                                r85_7 = r85_6;
                                                local_sp_12 = local_sp_11;
                                                r15_7_ph = r15_5;
                                                r92_6_ph = r92_4;
                                                local_sp_13_ph = local_sp_11;
                                                if (var_22 == rbx_1) {
                                                    continue;
                                                }
                                                loop_state_var = 0U;
                                                switch_state_var = 1;
                                                break;
                                            }
                                        }
                                        *(uint32_t *)(local_sp_1 + 44UL) = 1U;
                                    }
                                } else {
                                    var_83 = var_80.field_2;
                                    var_84 = *(uint64_t *)4310744UL;
                                    *(uint64_t *)4310736UL = var_74;
                                    var_85 = 18446744073709551612UL - (-4L);
                                    var_86 = (var_85 > var_76) ? var_76 : var_85;
                                    r85_1 = var_83;
                                    rax_0 = var_86;
                                    r15_1 = r15_0;
                                    r15_2 = r15_0;
                                    local_sp_2 = local_sp_1;
                                    r15_3 = r15_0;
                                    rdi3_1 = rdi3_0;
                                    r85_2 = r85_1;
                                    local_sp_3 = local_sp_1;
                                    local_sp_4 = local_sp_1;
                                    if (var_74 != 0UL) {
                                        *(uint32_t *)(local_sp_2 + 44UL) = 4U;
                                        r15_3 = r15_1;
                                        local_sp_4 = local_sp_2;
                                        *(uint64_t *)(local_sp_4 + (-8L)) = 4217941UL;
                                        var_127 = indirect_placeholder_160(var_26, r15_3, var_76, var_23, rbx_1, var_26, var_78);
                                        var_128 = var_127.field_0;
                                        var_129 = var_127.field_3;
                                        var_130 = (*(uint32_t *)(local_sp_4 + 36UL) == 1U) ? 75UL : 0UL;
                                        var_131 = local_sp_4 + (-16L);
                                        *(uint64_t *)var_131 = 4217982UL;
                                        indirect_placeholder_159(0UL, 4276945UL, 4276764UL, var_129, 1UL, var_130, var_128);
                                        local_sp_5 = var_131;
                                        var_132 = local_sp_5 + (-8L);
                                        *(uint64_t *)var_132 = 4218002UL;
                                        var_133 = indirect_placeholder_166(4276879UL, var_26, 4282560UL);
                                        var_134 = var_133.field_0;
                                        var_135 = var_133.field_1;
                                        var_136 = var_133.field_2;
                                        var_137 = var_133.field_3;
                                        var_138 = var_133.field_4;
                                        var_139 = var_133.field_5;
                                        var_140 = var_133.field_6;
                                        *(uint32_t *)4310660UL = (*(uint32_t *)4310660UL | (uint32_t)var_134);
                                        rbp_1 = var_135;
                                        r15_5 = var_136;
                                        r14_2 = r14_0;
                                        r101_0 = var_137;
                                        r92_4 = var_138;
                                        rdi3_2 = var_139;
                                        r13_2 = r13_0;
                                        r85_6 = var_140;
                                        rax_1 = var_134;
                                        local_sp_11 = var_132;
                                        var_141 = rbx_1 + 8UL;
                                        r14_4_ph = r14_2;
                                        r101_2_ph = r101_0;
                                        rbx_1_ph = var_141;
                                        rax_2_ph = rax_1;
                                        rbp_2 = rbp_1;
                                        rbp_3_ph = rbp_1;
                                        r15_6 = r15_5;
                                        r14_3 = r14_2;
                                        r101_1 = r101_0;
                                        rbx_0 = var_141;
                                        r92_5 = r92_4;
                                        rdi3_3 = rdi3_2;
                                        r13_3 = r13_2;
                                        r85_7 = r85_6;
                                        local_sp_12 = local_sp_11;
                                        r15_7_ph = r15_5;
                                        r92_6_ph = r92_4;
                                        local_sp_13_ph = local_sp_11;
                                        if (var_22 == rbx_1) {
                                            continue;
                                        }
                                        loop_state_var = 0U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    var_103 = helper_cc_compute_c_wrapper(rax_0 - var_74, var_74, var_6, 17U);
                                    if (var_103 != 0UL) {
                                        var_125 = *(uint32_t *)(local_sp_3 + 44UL);
                                        var_126 = (uint64_t)var_125;
                                        r15_3 = r15_2;
                                        local_sp_4 = local_sp_3;
                                        r15_5 = r15_2;
                                        rdi3_2 = rdi3_1;
                                        r85_6 = r85_2;
                                        rax_1 = var_126;
                                        local_sp_11 = local_sp_3;
                                        if (var_125 != 0U) {
                                            var_141 = rbx_1 + 8UL;
                                            r14_4_ph = r14_2;
                                            r101_2_ph = r101_0;
                                            rbx_1_ph = var_141;
                                            rax_2_ph = rax_1;
                                            rbp_2 = rbp_1;
                                            rbp_3_ph = rbp_1;
                                            r15_6 = r15_5;
                                            r14_3 = r14_2;
                                            r101_1 = r101_0;
                                            rbx_0 = var_141;
                                            r92_5 = r92_4;
                                            rdi3_3 = rdi3_2;
                                            r13_3 = r13_2;
                                            r85_7 = r85_6;
                                            local_sp_12 = local_sp_11;
                                            r15_7_ph = r15_5;
                                            r92_6_ph = r92_4;
                                            local_sp_13_ph = local_sp_11;
                                            if (var_22 == rbx_1) {
                                                continue;
                                            }
                                            loop_state_var = 0U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                    }
                                    *(uint32_t *)(local_sp_1 + 44UL) = 1U;
                                }
                            }
                        } else {
                            var_50 = local_sp_13 + (-48L);
                            *(uint64_t *)var_50 = 4217826UL;
                            var_51 = indirect_placeholder_168(4276854UL, var_26, 4282560UL);
                            var_52 = var_51.field_0;
                            var_53 = var_51.field_1;
                            var_54 = var_51.field_2;
                            var_55 = var_51.field_3;
                            var_56 = var_51.field_4;
                            var_57 = var_51.field_5;
                            var_58 = var_51.field_6;
                            *(uint32_t *)4310664UL = (*(uint32_t *)4310664UL | (uint32_t)var_52);
                            rbp_1 = var_53;
                            r15_5 = var_54;
                            r101_0 = var_55;
                            r92_4 = var_56;
                            rdi3_2 = var_57;
                            r85_6 = var_58;
                            rax_1 = var_52;
                            local_sp_11 = var_50;
                        }
                    }
                    break;
                  case 3U:
                    {
                        var_32 = var_28.field_2;
                        *(uint64_t *)4310760UL = var_26;
                        r85_6 = var_32;
                        var_141 = rbx_1 + 8UL;
                        r14_4_ph = r14_2;
                        r101_2_ph = r101_0;
                        rbx_1_ph = var_141;
                        rax_2_ph = rax_1;
                        rbp_2 = rbp_1;
                        rbp_3_ph = rbp_1;
                        r15_6 = r15_5;
                        r14_3 = r14_2;
                        r101_1 = r101_0;
                        rbx_0 = var_141;
                        r92_5 = r92_4;
                        rdi3_3 = rdi3_2;
                        r13_3 = r13_2;
                        r85_7 = r85_6;
                        local_sp_12 = local_sp_11;
                        r15_7_ph = r15_5;
                        r92_6_ph = r92_4;
                        local_sp_13_ph = local_sp_11;
                        if (var_22 == rbx_1) {
                            continue;
                        }
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 0U:
                    {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 2U:
                    {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
        switch (loop_state_var) {
          case 1U:
            {
                *(uint64_t *)(local_sp_10 + (-8L)) = 4218507UL;
                var_216 = indirect_placeholder_179(rbp_0, r15_4, r14_1, var_23, rbx_1, r13_1, r13_1);
                var_217 = var_216.field_0;
                var_218 = var_216.field_3;
                var_219 = var_216.field_6;
                *(uint64_t *)(local_sp_10 + (-16L)) = 4218526UL;
                indirect_placeholder_153(0UL, var_217, 4276794UL, var_218, 0UL, 0UL, var_219);
                *(uint64_t *)(local_sp_10 + (-24L)) = 4218536UL;
                indirect_placeholder_107(rbp_0, var_23, 1UL);
                abort();
            }
            break;
          case 0U:
            {
                r15_9 = r15_6;
                r101_4 = r101_1;
                r14_5 = r14_3;
                r101_3 = r101_1;
                rbx_2 = rbx_0;
                r92_7 = r92_5;
                rdi3_4 = rdi3_3;
                rdi3_5 = rdi3_3;
                local_sp_15 = local_sp_12;
                rbp_4 = rbp_2;
                rbp_5 = rbp_2;
                r13_4 = r13_3;
                r85_8 = r85_7;
                local_sp_14 = local_sp_12;
                r14_6 = r14_3;
                rbx_3 = rbx_0;
                r92_8 = r92_5;
                r13_5 = r13_3;
                r85_9 = r85_7;
                if (r15_6 == 0UL) {
                    *(uint64_t *)4310728UL = r15_6;
                    *(uint64_t *)4310736UL = r15_6;
                } else {
                    *(uint32_t *)4310668UL = (*(uint32_t *)4310668UL | 2048U);
                    r15_9 = r15_8;
                    r101_4 = r101_3;
                    rdi3_5 = rdi3_4;
                    local_sp_15 = local_sp_14;
                    rbp_5 = rbp_4;
                    r14_6 = r14_5;
                    r12_1 = r12_0;
                    rbx_3 = rbx_2;
                    r92_8 = r92_7;
                    r13_5 = r13_4;
                    r85_9 = r85_8;
                    if (*(uint64_t *)4310736UL == 0UL) {
                        *(uint64_t *)4310736UL = 512UL;
                    }
                    if (*(uint64_t *)4310728UL == 0UL) {
                        *(uint64_t *)4310728UL = 512UL;
                    }
                }
            }
            break;
        }
    } else {
        *(uint64_t *)(var_0 + (-88L)) = 18446744073709551615UL;
        *(uint64_t *)(var_0 + (-80L)) = 0UL;
        *(uint64_t *)(var_0 + (-96L)) = 0UL;
        *(uint32_t *)4310668UL = (*(uint32_t *)4310668UL | 2048U);
        r15_9 = r15_8;
        r101_4 = r101_3;
        rdi3_5 = rdi3_4;
        local_sp_15 = local_sp_14;
        rbp_5 = rbp_4;
        r14_6 = r14_5;
        r12_1 = r12_0;
        rbx_3 = rbx_2;
        r92_8 = r92_7;
        r13_5 = r13_4;
        r85_9 = r85_8;
        if (*(uint64_t *)4310736UL == 0UL) {
            *(uint64_t *)4310736UL = 512UL;
        }
        if (*(uint64_t *)4310728UL == 0UL) {
            *(uint64_t *)4310728UL = 512UL;
        }
        local_sp_16 = local_sp_15;
        local_sp_17 = local_sp_15;
        r92_1 = r92_8;
        r85_3 = r85_9;
        r92_2 = r92_8;
        r85_4 = r85_9;
        r92_3 = r92_8;
        r85_5 = r85_9;
        if (*(uint64_t *)4310720UL == 0UL) {
            *(uint32_t *)4310668UL = (*(uint32_t *)4310668UL & (-25));
        }
        var_142 = *(uint32_t *)4310664UL;
        var_143 = (uint64_t)var_142;
        rcx_4 = var_143;
        if ((uint64_t)(var_142 & 1052672U) == 0UL) {
            var_144 = var_143 | 1052672UL;
            *(uint32_t *)4310664UL = (uint32_t)var_144;
            rcx_4 = var_144;
        }
        var_145 = *(uint32_t *)4310660UL;
        var_146 = (uint64_t)var_145;
        rcx_1 = rcx_4;
        rcx_2 = rcx_4;
        rcx_3 = rcx_4;
        if ((var_146 & 1UL) != 0UL) {
            var_190 = local_sp_17 + (-8L);
            *(uint64_t *)var_190 = 4218584UL;
            var_191 = indirect_placeholder_182(rbp_5, r15_9, r14_6, r12_1, rbx_3, 4276960UL, r13_5);
            r92_0 = var_191.field_3;
            r85_0 = var_191.field_0;
            local_sp_0 = var_190;
            *(uint64_t *)(local_sp_0 + (-8L)) = 4218608UL;
            indirect_placeholder_152(0UL, rcx_0, 4276764UL, r92_0, 0UL, 0UL, r85_0);
            *(uint64_t *)(local_sp_0 + (-16L)) = 4218618UL;
            indirect_placeholder_107(rbp_5, r12_1, 1UL);
            abort();
        }
        rcx_0 = 4276854UL;
        if ((rcx_4 & 16UL) == 0UL) {
            if ((var_146 & 12UL) == 0UL) {
                var_153 = *(uint64_t *)(local_sp_15 + 8UL);
                var_154 = ((rcx_4 & 8UL) == 0UL);
                var_155 = (var_153 == 0UL);
                if (var_154) {
                    if (var_155) {
                        *(uint64_t *)4310712UL = var_153;
                    }
                } else {
                    if (var_155) {
                        var_156 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), *(uint64_t *)4310736UL, 4217274UL, var_153, rcx_4, rbp_5, r101_4, rbx_3, 0UL, r92_8, rdi3_5, var_146, r85_9, var_8, var_9, var_10, var_11, var_12, var_13, var_14);
                        var_157 = var_156.field_4;
                        var_158 = var_156.field_6;
                        var_159 = var_156.field_7;
                        var_160 = var_156.field_8;
                        var_161 = var_156.field_9;
                        var_162 = var_156.field_10;
                        *(uint64_t *)4310712UL = var_156.field_1;
                        *(uint64_t *)4310704UL = var_157;
                        state_0x8248_0 = var_158;
                        state_0x9018_0 = var_159;
                        state_0x9010_0 = var_160;
                        state_0x82d8_0 = var_161;
                        state_0x9080_0 = var_162;
                    }
                }
                var_163 = *(uint64_t *)(local_sp_15 + 16UL);
                var_164 = ((rcx_4 & 4UL) == 0UL);
                var_165 = (var_163 == 18446744073709551615UL);
                state_0x8248_1 = state_0x8248_0;
                state_0x9018_1 = state_0x9018_0;
                state_0x9010_1 = state_0x9010_0;
                state_0x82d8_1 = state_0x82d8_0;
                state_0x9080_1 = state_0x9080_0;
                if (var_164) {
                    if (var_165) {
                        *(uint64_t *)4309744UL = var_163;
                    }
                } else {
                    if (var_165) {
                        var_166 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), *(uint64_t *)4310736UL, 4217317UL, var_163, rcx_4, rbp_5, r101_4, rbx_3, 0UL, r92_8, rdi3_5, var_146, r85_9, state_0x8248_0, state_0x9018_0, state_0x9010_0, var_11, var_12, state_0x82d8_0, state_0x9080_0);
                        var_167 = var_166.field_4;
                        var_168 = var_166.field_6;
                        var_169 = var_166.field_7;
                        var_170 = var_166.field_8;
                        var_171 = var_166.field_9;
                        var_172 = var_166.field_10;
                        *(uint64_t *)4309744UL = var_166.field_1;
                        *(uint64_t *)4310672UL = var_167;
                        state_0x8248_1 = var_168;
                        state_0x9018_1 = var_169;
                        state_0x9010_1 = var_170;
                        state_0x82d8_1 = var_171;
                        state_0x9080_1 = var_172;
                    }
                }
                var_173 = *(uint64_t *)(local_sp_15 + 24UL);
                var_174 = ((var_146 & 16UL) == 0UL);
                var_175 = (var_173 == 0UL);
                if (var_174) {
                    if (var_175) {
                        *(uint64_t *)4310696UL = var_173;
                    }
                } else {
                    if (var_175) {
                        var_176 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), *(uint64_t *)4310728UL, 4217360UL, var_173, rcx_4, rbp_5, r101_4, rbx_3, 0UL, r92_8, rdi3_5, var_146, r85_9, state_0x8248_1, state_0x9018_1, state_0x9010_1, var_11, var_12, state_0x82d8_1, state_0x9080_1);
                        var_177 = var_176.field_4;
                        *(uint64_t *)4310696UL = var_176.field_1;
                        *(uint64_t *)4310688UL = var_177;
                    }
                }
                var_178 = (uint64_t)*(uint32_t *)4310668UL;
                var_179 = rcx_4 & 1UL;
                var_180 = (uint16_t)var_178;
                if (((uint64_t)(var_180 & (unsigned short)2048U) | var_179) == 0UL) {
                    *(unsigned char *)4310568UL = (unsigned char)'\x00';
                    rax_3 = (var_179 == 0UL) ? 4210240UL : 4210512UL;
                } else {
                    if (*(uint64_t *)4310712UL == 0UL) {
                        *(unsigned char *)4310568UL = (unsigned char)'\x01';
                    } else {
                        if ((*(uint64_t *)4309744UL + (-1L)) > 18446744073709551613UL) {
                            *(unsigned char *)4310568UL = (unsigned char)'\x01';
                        } else {
                            if ((uint64_t)(((uint16_t)rcx_4 | (uint16_t)var_146) & (unsigned short)16384U) == 0UL) {
                                *(unsigned char *)4310568UL = (unsigned char)'\x00';
                            } else {
                                *(unsigned char *)4310568UL = (unsigned char)'\x01';
                            }
                        }
                    }
                }
                *(uint64_t *)4310368UL = rax_3;
                var_181 = var_178 & 7UL;
                var_182 = (uint32_t)rcx_4;
                *(uint32_t *)4310664UL = (var_182 & (-2));
                *(uint64_t *)(local_sp_15 + (-8L)) = 4217462UL;
                var_183 = indirect_placeholder_2(var_181);
                if ((uint64_t)(unsigned char)var_183 == 0UL) {
                    var_186 = var_178 & 24UL;
                    var_187 = local_sp_15 + (-16L);
                    *(uint64_t *)var_187 = 4217480UL;
                    var_188 = indirect_placeholder_2(var_186);
                    local_sp_9 = var_187;
                    if ((uint64_t)(unsigned char)var_188 != 0UL) {
                        var_192 = var_178 & 96UL;
                        var_193 = local_sp_15 + (-24L);
                        *(uint64_t *)var_193 = 4217498UL;
                        var_194 = indirect_placeholder_2(var_192);
                        local_sp_8 = var_193;
                        if ((uint64_t)(unsigned char)var_194 != 0UL) {
                            var_209 = local_sp_8 + (-8L);
                            *(uint64_t *)var_209 = 4218656UL;
                            var_210 = indirect_placeholder_173(0UL, rcx_2, 4281424UL, r92_2, 1UL, 0UL, r85_4);
                            r92_1 = var_210.field_3;
                            rcx_1 = var_210.field_1;
                            r85_3 = var_210.field_4;
                            local_sp_7 = var_209;
                            var_211 = local_sp_7 + (-8L);
                            *(uint64_t *)var_211 = 4218675UL;
                            indirect_placeholder_172(0UL, rcx_1, 4281456UL, r92_1, 1UL, 0UL, r85_3);
                            local_sp_16 = var_211;
                            var_212 = local_sp_16 + (-8L);
                            *(uint64_t *)var_212 = 4218685UL;
                            var_213 = indirect_placeholder_181(rbp_5, r15_9, r14_6, r12_1, rbx_3, 4276970UL, r13_5);
                            r92_0 = var_213.field_3;
                            r85_0 = var_213.field_0;
                            local_sp_0 = var_212;
                            *(uint64_t *)(local_sp_0 + (-8L)) = 4218608UL;
                            indirect_placeholder_152(0UL, rcx_0, 4276764UL, r92_0, 0UL, 0UL, r85_0);
                            *(uint64_t *)(local_sp_0 + (-16L)) = 4218618UL;
                            indirect_placeholder_107(rbp_5, r12_1, 1UL);
                            abort();
                        }
                        var_195 = (uint64_t)(var_180 & (unsigned short)12288U);
                        var_196 = local_sp_15 + (-32L);
                        *(uint64_t *)var_196 = 4217519UL;
                        var_197 = indirect_placeholder_2(var_195);
                        local_sp_7 = var_196;
                        if ((uint64_t)(unsigned char)var_197 != 0UL) {
                            var_198 = (uint64_t)((uint16_t)rcx_4 & (unsigned short)16386U);
                            var_199 = local_sp_15 + (-40L);
                            *(uint64_t *)var_199 = 4217540UL;
                            var_200 = indirect_placeholder_2(var_198);
                            local_sp_6 = var_199;
                            if ((uint64_t)(unsigned char)var_200 != 0UL) {
                                var_201 = (uint64_t)((uint16_t)var_146 & (unsigned short)16386U);
                                var_202 = local_sp_15 + (-48L);
                                *(uint64_t *)var_202 = 4217561UL;
                                var_203 = indirect_placeholder_2(var_201);
                                local_sp_6 = var_202;
                                if ((uint64_t)(unsigned char)var_203 != 0UL) {
                                    if ((rcx_4 & 2UL) == 0UL) {
                                        var_214 = *(uint64_t *)4309744UL | *(uint64_t *)4310672UL;
                                        *(unsigned char *)4310379UL = (unsigned char)'\x01';
                                        *(unsigned char *)4310377UL = (var_214 == 0UL);
                                        *(uint32_t *)4310664UL = (var_182 & (-4));
                                    }
                                    if ((var_146 & 2UL) != 0UL) {
                                        var_215 = *(uint64_t *)4309744UL | *(uint64_t *)4310672UL;
                                        *(unsigned char *)4310378UL = (unsigned char)'\x01';
                                        *(unsigned char *)4310376UL = (var_215 == 0UL);
                                        *(uint32_t *)4310660UL = (var_145 & (-3));
                                    }
                                    return;
                                }
                            }
                            var_204 = local_sp_6 + (-8L);
                            *(uint64_t *)var_204 = 4218637UL;
                            var_205 = indirect_placeholder_171(0UL, rcx_4, 4281488UL, r92_8, 1UL, 0UL, r85_9);
                            var_206 = var_205.field_1;
                            var_207 = var_205.field_3;
                            var_208 = var_205.field_4;
                            r92_2 = var_207;
                            rcx_2 = var_206;
                            r85_4 = var_208;
                            local_sp_8 = var_204;
                            var_209 = local_sp_8 + (-8L);
                            *(uint64_t *)var_209 = 4218656UL;
                            var_210 = indirect_placeholder_173(0UL, rcx_2, 4281424UL, r92_2, 1UL, 0UL, r85_4);
                            r92_1 = var_210.field_3;
                            rcx_1 = var_210.field_1;
                            r85_3 = var_210.field_4;
                            local_sp_7 = var_209;
                        }
                    }
                } else {
                    var_184 = local_sp_15 + (-16L);
                    *(uint64_t *)var_184 = 4218555UL;
                    var_185 = indirect_placeholder_175(0UL, rcx_4, 4281336UL, r92_8, 1UL, 0UL, r85_9);
                    r92_3 = var_185.field_3;
                    rcx_3 = var_185.field_1;
                    r85_5 = var_185.field_4;
                    local_sp_9 = var_184;
                    var_189 = local_sp_9 + (-8L);
                    *(uint64_t *)var_189 = 4218574UL;
                    indirect_placeholder_174(0UL, rcx_3, 4281384UL, r92_3, 1UL, 0UL, r85_5);
                    local_sp_17 = var_189;
                    var_190 = local_sp_17 + (-8L);
                    *(uint64_t *)var_190 = 4218584UL;
                    var_191 = indirect_placeholder_182(rbp_5, r15_9, r14_6, r12_1, rbx_3, 4276960UL, r13_5);
                    r92_0 = var_191.field_3;
                    r85_0 = var_191.field_0;
                    local_sp_0 = var_190;
                }
            } else {
                var_147 = (uint64_t)(var_145 & (-252));
                var_148 = ((var_146 & 4UL) == 0UL) ? 4276783UL : 4276771UL;
                var_149 = local_sp_15 + (-8L);
                *(uint64_t *)var_149 = 4218718UL;
                var_150 = indirect_placeholder_180(rbp_5, r15_9, r14_6, r12_1, rbx_3, var_148, var_147, r13_5);
                var_151 = var_150.field_0;
                var_152 = var_150.field_3;
                r92_0 = var_152;
                r85_0 = var_151;
                local_sp_0 = var_149;
            }
        } else {
            var_212 = local_sp_16 + (-8L);
            *(uint64_t *)var_212 = 4218685UL;
            var_213 = indirect_placeholder_181(rbp_5, r15_9, r14_6, r12_1, rbx_3, 4276970UL, r13_5);
            r92_0 = var_213.field_3;
            r85_0 = var_213.field_0;
            local_sp_0 = var_212;
        }
    }
}
