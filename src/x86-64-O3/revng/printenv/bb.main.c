typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern void abort(void);
extern void function_dispatcher(unsigned char *param_0);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern void indirect_placeholder_32(uint64_t param_0);
extern void indirect_placeholder_15(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_42(uint64_t param_0, uint64_t param_1, uint64_t param_2);
void bb_main(uint64_t rdi, uint64_t rsi) {
    uint64_t var_12;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t local_sp_7;
    uint64_t r12_2;
    uint64_t *var_26;
    uint64_t rax_2;
    uint64_t local_sp_1;
    uint64_t local_sp_0;
    uint64_t r8_1;
    uint64_t rax_0;
    uint64_t r15_0;
    uint64_t r8_0;
    uint64_t var_27;
    unsigned char var_28;
    uint64_t var_29;
    uint64_t rax_1;
    uint64_t rcx_0;
    uint64_t rdx_0;
    unsigned char var_30;
    uint64_t var_31;
    uint64_t var_32;
    unsigned char var_33;
    uint64_t var_34;
    uint64_t local_sp_6;
    uint64_t rbx_0;
    uint64_t var_35;
    uint64_t local_sp_2;
    uint64_t r14_0;
    uint64_t rax_3;
    uint64_t r12_0;
    uint64_t var_36;
    uint64_t r12_3;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t local_sp_3;
    uint64_t rax_4;
    uint64_t r12_1;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    bool var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t local_sp_4;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t rax_5;
    uint64_t var_25;
    uint64_t var_11;
    uint32_t var_13;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r15();
    var_3 = init_r14();
    var_4 = init_r12();
    var_5 = init_rbx();
    var_6 = init_r13();
    var_7 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_6;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_1;
    var_8 = (uint64_t)(uint32_t)rdi;
    *(uint64_t *)(var_0 + (-48L)) = var_5;
    var_9 = *(uint64_t *)rsi;
    *(uint64_t *)(var_0 + (-64L)) = 4203646UL;
    indirect_placeholder_32(var_9);
    *(uint64_t *)(var_0 + (-72L)) = 4203661UL;
    indirect_placeholder_1();
    *(uint64_t *)(var_0 + (-80L)) = 4203666UL;
    indirect_placeholder_1();
    var_10 = var_0 + (-88L);
    *(uint64_t *)var_10 = 4203676UL;
    indirect_placeholder_1();
    local_sp_7 = var_10;
    r12_2 = 0UL;
    rax_0 = 0UL;
    r8_0 = 0UL;
    r12_3 = 0UL;
    var_11 = local_sp_7 + (-8L);
    *(uint64_t *)var_11 = 4203699UL;
    var_12 = indirect_placeholder_13(4256736UL, 4255922UL, var_8, rsi, 0UL);
    var_13 = (uint32_t)var_12;
    local_sp_7 = var_11;
    r12_3 = 1UL;
    local_sp_3 = var_11;
    rax_4 = var_12;
    r12_1 = r12_3;
    while ((uint64_t)(var_13 + 1U) != 0UL)
        {
            rax_4 = 0UL;
            r12_1 = 1UL;
            if ((uint64_t)(var_13 + 130U) == 0UL) {
                *(uint64_t *)(local_sp_7 + (-16L)) = 4204072UL;
                indirect_placeholder_42(var_8, r12_3, 0UL);
                abort();
            }
            if ((uint64_t)(var_13 + (-48)) == 0UL) {
                var_11 = local_sp_7 + (-8L);
                *(uint64_t *)var_11 = 4203699UL;
                var_12 = indirect_placeholder_13(4256736UL, 4255922UL, var_8, rsi, 0UL);
                var_13 = (uint32_t)var_12;
                local_sp_7 = var_11;
                r12_3 = 1UL;
                local_sp_3 = var_11;
                rax_4 = var_12;
                r12_1 = r12_3;
                continue;
            }
            if ((uint64_t)(var_13 + 131U) == 0UL) {
                *(uint64_t *)(local_sp_7 + (-16L)) = 4203743UL;
                indirect_placeholder_42(var_8, 1UL, 2UL);
                abort();
            }
            var_14 = *(uint64_t *)4275776UL;
            var_15 = *(uint64_t *)4276768UL;
            *(uint64_t *)(local_sp_7 + (-24L)) = 0UL;
            *(uint64_t *)(local_sp_7 + (-32L)) = 4203793UL;
            indirect_placeholder_15(0UL, var_15, 4255848UL, 4255889UL, var_14, 4255744UL, 4255906UL);
            var_16 = local_sp_7 + (-40L);
            *(uint64_t *)var_16 = 4203800UL;
            indirect_placeholder_1();
            local_sp_3 = var_16;
            break;
        }
    var_17 = (uint64_t)*(uint32_t *)4276892UL;
    var_18 = var_17 << 32UL;
    var_19 = rdi << 32UL;
    var_20 = ((long)var_18 < (long)var_19);
    var_21 = r12_1 + (-1L);
    local_sp_4 = local_sp_3;
    local_sp_6 = local_sp_3;
    rax_5 = rax_4;
    r14_0 = var_17;
    if (!var_20) {
        var_22 = *(uint64_t *)4276896UL;
        helper_cc_compute_c_wrapper(var_21, 1UL, var_7, 14U);
        rbx_0 = var_22;
        while (*(uint64_t *)rbx_0 != 0UL)
            {
                var_23 = rbx_0 + 8UL;
                var_24 = local_sp_4 + (-8L);
                *(uint64_t *)var_24 = 4203848UL;
                indirect_placeholder_1();
                local_sp_4 = var_24;
                rbx_0 = var_23;
            }
    }
    helper_cc_compute_c_wrapper(var_21, 1UL, var_7, 14U);
    var_36 = r14_0 + 1UL;
    local_sp_6 = local_sp_2;
    rax_5 = rax_3;
    r14_0 = var_36;
    r12_2 = r12_0;
    do {
        var_25 = local_sp_6 + (-8L);
        *(uint64_t *)var_25 = 4203922UL;
        indirect_placeholder_1();
        local_sp_0 = var_25;
        local_sp_2 = var_25;
        rax_3 = rax_5;
        r12_0 = r12_2;
        if (rax_5 != 0UL) {
            var_26 = (uint64_t *)((r14_0 << 3UL) + rsi);
            r15_0 = *(uint64_t *)4276896UL;
            var_27 = *(uint64_t *)r15_0;
            local_sp_1 = local_sp_0;
            r8_1 = r8_0;
            rcx_0 = var_27;
            local_sp_2 = local_sp_0;
            rax_3 = rax_0;
            while (var_27 != 0UL)
                {
                    var_28 = *(unsigned char *)var_27;
                    var_29 = (uint64_t)var_28;
                    rax_1 = var_29;
                    rax_2 = var_29;
                    if (var_28 == '\x00') {
                        local_sp_0 = local_sp_1;
                        rax_0 = rax_2;
                        r15_0 = r15_0 + 8UL;
                        r8_0 = r8_1;
                        var_27 = *(uint64_t *)r15_0;
                        local_sp_1 = local_sp_0;
                        r8_1 = r8_0;
                        rcx_0 = var_27;
                        local_sp_2 = local_sp_0;
                        rax_3 = rax_0;
                        continue;
                    }
                    rdx_0 = *var_26;
                    while (1U)
                        {
                            var_30 = *(unsigned char *)rdx_0;
                            rax_2 = rax_1;
                            if (var_30 != '\x00') {
                                loop_state_var = 0U;
                                break;
                            }
                            var_31 = rcx_0 + 1UL;
                            var_32 = rdx_0 + 1UL;
                            rcx_0 = var_31;
                            rdx_0 = var_32;
                            if ((uint64_t)((unsigned char)rax_1 - var_30) != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            var_33 = *(unsigned char *)var_31;
                            var_34 = (uint64_t)var_33;
                            rax_1 = var_34;
                            rax_2 = 0UL;
                            r8_1 = 1UL;
                            if ((uint64_t)(var_33 + '\xc3') != 0UL) {
                                rax_2 = var_34;
                                r8_1 = r8_0;
                                if (var_33 != '\x00') {
                                    continue;
                                }
                                loop_state_var = 0U;
                                break;
                            }
                            if (*(unsigned char *)var_32 == '\x00') {
                                continue;
                            }
                            loop_state_var = 1U;
                            break;
                        }
                    var_35 = local_sp_0 + (-8L);
                    *(uint64_t *)var_35 = 4204028UL;
                    indirect_placeholder_1();
                    local_sp_1 = var_35;
                }
            r12_0 = (uint64_t)((uint32_t)r12_2 + (uint32_t)r8_0);
        }
        var_36 = r14_0 + 1UL;
        local_sp_6 = local_sp_2;
        rax_5 = rax_3;
        r14_0 = var_36;
        r12_2 = r12_0;
    } while ((long)var_19 <= (long)(var_36 << 32UL));
    function_dispatcher((unsigned char *)(0UL));
    return;
}
