typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t indirect_placeholder_4(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t indirect_placeholder_26(void);
uint64_t bb_mgetgroups(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint32_t *var_43;
    uint32_t var_44;
    uint64_t var_45;
    uint64_t rax_5;
    uint64_t var_42;
    uint64_t var_30;
    uint32_t _pre_phi;
    uint64_t rax_0;
    uint64_t rbp_0;
    uint32_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t rbp_2;
    uint64_t rax_2;
    uint64_t rax_1;
    uint64_t rbp_1;
    uint64_t rdx1_0;
    uint32_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint32_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint32_t var_29;
    uint64_t local_sp_0;
    uint64_t rbp_3;
    uint64_t var_31;
    uint64_t var_32;
    uint32_t var_33;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t rsi3_0;
    uint64_t local_sp_1;
    uint64_t rax_3;
    uint32_t *var_34;
    uint32_t var_35;
    uint64_t rax_4;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t local_sp_2;
    uint32_t var_8;
    uint32_t *var_9;
    uint32_t var_10;
    uint64_t var_11;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_16;
    uint32_t *var_17;
    uint32_t var_18;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r14();
    var_3 = init_r12();
    var_4 = init_rbx();
    var_5 = init_r13();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_1;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    rax_3 = 0UL;
    rax_5 = 4294967295UL;
    if (rdi != 0UL) {
        *(uint32_t *)(var_0 + (-44L)) = 10U;
        var_6 = var_0 + (-64L);
        *(uint64_t *)var_6 = 4232674UL;
        var_7 = indirect_placeholder_4(0UL, 10UL);
        local_sp_2 = var_6;
        rax_4 = var_7;
        if (var_7 != 0UL) {
            while (1U)
                {
                    var_8 = *(uint32_t *)(local_sp_2 | 12UL);
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4232706UL;
                    indirect_placeholder();
                    var_9 = (uint32_t *)(local_sp_2 | 4UL);
                    var_10 = *var_9;
                    var_11 = (uint64_t)var_10;
                    rsi3_0 = var_11;
                    if ((int)(uint32_t)rax_4 < (int)0U) {
                        if ((uint64_t)(var_10 - var_8) == 0UL) {
                            *var_9 = (var_10 << 1U);
                            rsi3_0 = (uint64_t)((long)(var_11 << 33UL) >> (long)32UL);
                        }
                        var_14 = local_sp_2 + (-16L);
                        *(uint64_t *)var_14 = 4232790UL;
                        var_15 = indirect_placeholder_4(rax_4, rsi3_0);
                        local_sp_1 = var_14;
                        local_sp_2 = var_14;
                        rax_4 = var_15;
                        if (var_15 != 0UL) {
                            continue;
                        }
                        break;
                    }
                    var_12 = local_sp_2 + (-16L);
                    *(uint64_t *)var_12 = 4232723UL;
                    var_13 = indirect_placeholder_4(rax_4, var_11);
                    local_sp_1 = var_12;
                    if (var_13 != 0UL) {
                        break;
                    }
                    *(uint64_t *)rdx = var_13;
                    return (uint64_t)*(uint32_t *)(local_sp_2 + (-4L));
                }
            *(uint64_t *)(local_sp_1 + (-8L)) = 4232733UL;
            indirect_placeholder();
            var_34 = (uint32_t *)rax_3;
            var_35 = *var_34;
            *(uint64_t *)(local_sp_1 + (-16L)) = 4232743UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_1 + (-24L)) = 4232748UL;
            indirect_placeholder();
            *var_34 = var_35;
        }
        return rax_5;
    }
    *(uint64_t *)(var_0 + (-64L)) = 4232833UL;
    var_16 = indirect_placeholder_26();
    var_17 = (uint32_t *)(var_0 + (-52L));
    var_18 = (uint32_t)var_16;
    *var_17 = var_18;
    if ((int)var_18 < (int)0U) {
        *(uint64_t *)(var_0 + (-72L)) = 4233021UL;
        indirect_placeholder();
        *(uint64_t *)(var_0 + (-80L)) = 4233042UL;
        var_42 = indirect_placeholder_4(0UL, 1UL);
        if (*(uint32_t *)var_16 != 38U & var_42 == 0UL) {
            *(uint64_t *)rdx = var_42;
            var_43 = (uint32_t *)var_42;
            var_44 = (uint32_t)rsi;
            *var_43 = var_44;
            var_45 = ((uint64_t)(var_44 + 1U) != 0UL);
            rax_5 = var_45;
        }
    } else {
        if ((uint64_t)var_18 == 0UL) {
            if ((uint64_t)((uint32_t)rsi + 1U) == 0UL) {
                var_19 = (uint64_t)((long)(var_16 << 32UL) >> (long)32UL);
                var_20 = var_0 + (-72L);
                *(uint64_t *)var_20 = 4233082UL;
                var_21 = indirect_placeholder_4(0UL, var_19);
                local_sp_0 = var_20;
                rbp_3 = var_21;
                if (var_21 == 0UL) {
                    return rax_5;
                }
                var_31 = local_sp_0 + (-8L);
                *(uint64_t *)var_31 = 4233111UL;
                var_32 = indirect_placeholder_26();
                var_33 = (uint32_t)var_32;
                _pre_phi = var_33;
                rax_0 = var_32;
                rbp_0 = rbp_3;
                local_sp_1 = var_31;
                rax_3 = var_32;
                if ((int)var_33 > (int)4294967295U) {
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4232733UL;
                    indirect_placeholder();
                    var_34 = (uint32_t *)rax_3;
                    var_35 = *var_34;
                    *(uint64_t *)(local_sp_1 + (-16L)) = 4232743UL;
                    indirect_placeholder();
                    *(uint64_t *)(local_sp_1 + (-24L)) = 4232748UL;
                    indirect_placeholder();
                    *var_34 = var_35;
                    return rax_5;
                }
            }
            var_22 = var_16 + 1UL;
            var_23 = (uint64_t)((long)(var_22 << 32UL) >> (long)32UL);
            *var_17 = (uint32_t)var_22;
            var_24 = var_0 + (-72L);
            *(uint64_t *)var_24 = 4232881UL;
            var_25 = indirect_placeholder_4(0UL, var_23);
            rbp_0 = var_25;
            local_sp_0 = var_24;
            rbp_3 = var_25;
            if (var_25 == 0UL) {
                return rax_5;
            }
            var_26 = (uint32_t)rsi;
            if ((uint64_t)(var_26 + 1U) == 0UL) {
                var_31 = local_sp_0 + (-8L);
                *(uint64_t *)var_31 = 4233111UL;
                var_32 = indirect_placeholder_26();
                var_33 = (uint32_t)var_32;
                _pre_phi = var_33;
                rax_0 = var_32;
                rbp_0 = rbp_3;
                local_sp_1 = var_31;
                rax_3 = var_32;
                if ((int)var_33 > (int)4294967295U) {
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4232733UL;
                    indirect_placeholder();
                    var_34 = (uint32_t *)rax_3;
                    var_35 = *var_34;
                    *(uint64_t *)(local_sp_1 + (-16L)) = 4232743UL;
                    indirect_placeholder();
                    *(uint64_t *)(local_sp_1 + (-24L)) = 4232748UL;
                    indirect_placeholder();
                    *var_34 = var_35;
                    return rax_5;
                }
            }
            var_27 = var_0 + (-80L);
            *(uint64_t *)var_27 = 4232919UL;
            var_28 = indirect_placeholder_26();
            var_29 = (uint32_t)var_28;
            local_sp_1 = var_27;
            rax_3 = var_28;
            if ((int)var_29 >= (int)0U) {
                *(uint64_t *)(local_sp_1 + (-8L)) = 4232733UL;
                indirect_placeholder();
                var_34 = (uint32_t *)rax_3;
                var_35 = *var_34;
                *(uint64_t *)(local_sp_1 + (-16L)) = 4232743UL;
                indirect_placeholder();
                *(uint64_t *)(local_sp_1 + (-24L)) = 4232748UL;
                indirect_placeholder();
                *var_34 = var_35;
                return rax_5;
            }
            *(uint32_t *)var_25 = var_26;
            var_30 = (uint64_t)(var_29 + 1U);
            _pre_phi = (uint32_t)var_30;
            rax_0 = var_30;
            *(uint64_t *)rdx = rbp_0;
            rax_1 = rax_0;
            rbp_1 = rbp_0;
            rax_5 = rax_0;
            var_36 = *(uint32_t *)rbp_0;
            var_37 = (uint64_t)((long)(rax_0 << 32UL) >> (long)30UL) + rbp_0;
            var_38 = rbp_0 + 4UL;
            rdx1_0 = var_38;
            if ((int)_pre_phi <= (int)1U & var_37 > var_38) {
                var_41 = rdx1_0 + 4UL;
                rax_1 = rax_2;
                rbp_1 = rbp_2;
                rdx1_0 = var_41;
                rax_5 = rax_2;
                do {
                    var_39 = *(uint32_t *)rdx1_0;
                    rax_2 = rax_1;
                    rbp_2 = rbp_1;
                    if ((uint64_t)(var_39 - var_36) == 0UL) {
                        rax_2 = (uint64_t)((uint32_t)rax_1 + (-1));
                    } else {
                        if ((uint64_t)(var_39 - *(uint32_t *)rbp_1) == 0UL) {
                            rax_2 = (uint64_t)((uint32_t)rax_1 + (-1));
                        } else {
                            var_40 = rbp_1 + 4UL;
                            *(uint32_t *)var_40 = var_39;
                            rbp_2 = var_40;
                        }
                    }
                    var_41 = rdx1_0 + 4UL;
                    rax_1 = rax_2;
                    rbp_1 = rbp_2;
                    rdx1_0 = var_41;
                    rax_5 = rax_2;
                } while (var_37 <= var_41);
            }
        } else {
            var_22 = var_16 + 1UL;
            var_23 = (uint64_t)((long)(var_22 << 32UL) >> (long)32UL);
            *var_17 = (uint32_t)var_22;
            var_24 = var_0 + (-72L);
            *(uint64_t *)var_24 = 4232881UL;
            var_25 = indirect_placeholder_4(0UL, var_23);
            rbp_0 = var_25;
            local_sp_0 = var_24;
            rbp_3 = var_25;
            if (var_25 == 0UL) {
                return rax_5;
            }
            var_26 = (uint32_t)rsi;
            if ((uint64_t)(var_26 + 1U) != 0UL) {
                var_31 = local_sp_0 + (-8L);
                *(uint64_t *)var_31 = 4233111UL;
                var_32 = indirect_placeholder_26();
                var_33 = (uint32_t)var_32;
                _pre_phi = var_33;
                rax_0 = var_32;
                rbp_0 = rbp_3;
                local_sp_1 = var_31;
                rax_3 = var_32;
                if ((int)var_33 <= (int)4294967295U) {
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4232733UL;
                    indirect_placeholder();
                    var_34 = (uint32_t *)rax_3;
                    var_35 = *var_34;
                    *(uint64_t *)(local_sp_1 + (-16L)) = 4232743UL;
                    indirect_placeholder();
                    *(uint64_t *)(local_sp_1 + (-24L)) = 4232748UL;
                    indirect_placeholder();
                    *var_34 = var_35;
                    return rax_5;
                }
            }
            var_27 = var_0 + (-80L);
            *(uint64_t *)var_27 = 4232919UL;
            var_28 = indirect_placeholder_26();
            var_29 = (uint32_t)var_28;
            local_sp_1 = var_27;
            rax_3 = var_28;
            if ((int)var_29 >= (int)0U) {
                *(uint64_t *)(local_sp_1 + (-8L)) = 4232733UL;
                indirect_placeholder();
                var_34 = (uint32_t *)rax_3;
                var_35 = *var_34;
                *(uint64_t *)(local_sp_1 + (-16L)) = 4232743UL;
                indirect_placeholder();
                *(uint64_t *)(local_sp_1 + (-24L)) = 4232748UL;
                indirect_placeholder();
                *var_34 = var_35;
                return rax_5;
            }
            *(uint32_t *)var_25 = var_26;
            var_30 = (uint64_t)(var_29 + 1U);
            _pre_phi = (uint32_t)var_30;
            rax_0 = var_30;
        }
    }
}
