typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_pxor_xmm_wrapper_ret_type;
struct type_4;
struct type_6;
struct indirect_placeholder_13_ret_type;
struct indirect_placeholder_23_ret_type;
struct indirect_placeholder_15_ret_type;
struct indirect_placeholder_16_ret_type;
struct indirect_placeholder_17_ret_type;
struct indirect_placeholder_19_ret_type;
struct indirect_placeholder_14_ret_type;
struct indirect_placeholder_20_ret_type;
struct indirect_placeholder_21_ret_type;
struct helper_paddq_xmm_wrapper_ret_type;
struct indirect_placeholder_22_ret_type;
struct helper_pxor_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_4 {
};
struct type_6 {
};
struct indirect_placeholder_13_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_23_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_15_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_16_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
    uint64_t field_8;
    uint64_t field_9;
    uint64_t field_10;
    uint64_t field_11;
};
struct indirect_placeholder_17_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
    uint64_t field_8;
    uint64_t field_9;
    uint64_t field_10;
    uint64_t field_11;
};
struct indirect_placeholder_19_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_14_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_20_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
    uint64_t field_8;
    uint64_t field_9;
    uint64_t field_10;
    uint64_t field_11;
};
struct indirect_placeholder_21_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_paddq_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_22_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
    uint64_t field_8;
    uint64_t field_9;
    uint64_t field_10;
    uint64_t field_11;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_state_0x8558(void);
extern uint64_t init_state_0x8560(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t indirect_placeholder_18(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct helper_pxor_xmm_wrapper_ret_type helper_pxor_xmm_wrapper(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern struct indirect_placeholder_13_ret_type indirect_placeholder_13(uint64_t param_0);
extern struct indirect_placeholder_23_ret_type indirect_placeholder_23(uint64_t param_0);
extern struct indirect_placeholder_15_ret_type indirect_placeholder_15(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_16_ret_type indirect_placeholder_16(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_17_ret_type indirect_placeholder_17(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_19_ret_type indirect_placeholder_19(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_14_ret_type indirect_placeholder_14(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_20_ret_type indirect_placeholder_20(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_21_ret_type indirect_placeholder_21(uint64_t param_0);
extern struct helper_paddq_xmm_wrapper_ret_type helper_paddq_xmm_wrapper(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_22_ret_type indirect_placeholder_22(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
uint64_t bb_elide_tail_lines_pipe(uint64_t rcx, uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t local_sp_3;
    uint64_t r13_0;
    uint64_t var_99;
    uint64_t var_100;
    struct helper_pxor_xmm_wrapper_ret_type var_15;
    struct indirect_placeholder_20_ret_type var_59;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t *var_12;
    struct indirect_placeholder_13_ret_type var_13;
    uint64_t var_14;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    struct indirect_placeholder_23_ret_type var_19;
    uint64_t var_20;
    uint64_t rbp_0_ph_ph;
    uint64_t r15_0_ph;
    uint64_t rbx_3_ph;
    uint64_t rbx_3;
    uint64_t r12_0;
    uint64_t local_sp_0;
    uint64_t r12_1;
    uint64_t r14_0;
    uint64_t var_114;
    uint64_t var_115;
    uint64_t var_116;
    struct indirect_placeholder_15_ret_type var_117;
    uint64_t var_102;
    uint64_t rdi3_0;
    uint64_t rdi3_1;
    uint64_t rbx_1;
    uint64_t var_103;
    uint64_t r13_0_in;
    uint64_t *var_104;
    uint64_t var_105;
    uint64_t local_sp_2;
    struct indirect_placeholder_16_ret_type var_106;
    uint64_t r12_3_ph;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t *var_77;
    uint64_t *var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t *var_43;
    uint64_t var_44;
    uint64_t r12_2;
    uint64_t rbx_0;
    uint64_t local_sp_1;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    struct indirect_placeholder_17_ret_type var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t rax_0;
    uint64_t r14_1;
    uint64_t rsi4_0;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t *var_98;
    uint64_t var_101;
    uint64_t local_sp_5_ph;
    uint64_t local_sp_5;
    uint64_t var_112;
    uint64_t var_113;
    uint64_t var_107;
    struct indirect_placeholder_19_ret_type var_108;
    uint64_t var_109;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t local_sp_7_ph_ph;
    uint64_t var_35;
    uint64_t *var_36;
    uint64_t rdi3_2;
    uint64_t var_37;
    bool var_38;
    uint64_t var_39;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t *var_50;
    uint64_t var_55;
    uint64_t *var_56;
    uint64_t var_57;
    uint64_t *var_58;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t local_sp_7;
    uint64_t rbp_0_ph_ph_be;
    uint64_t r15_0_ph_ph_be;
    uint64_t rbx_5_ph_ph_be;
    uint64_t r9_0_ph_ph_be;
    uint64_t local_sp_7_ph_ph_be;
    uint64_t r8_0_ph_ph_be;
    uint64_t r15_0_ph_ph;
    uint64_t rbx_5_ph_ph;
    uint64_t r9_0_ph_ph;
    uint64_t r8_0_ph_ph;
    uint64_t rbp_0_ph;
    uint64_t rbx_5_ph;
    uint64_t local_sp_7_ph;
    uint64_t *var_21;
    uint64_t *var_22;
    uint64_t *var_23;
    uint64_t rbx_5;
    uint64_t var_51;
    uint64_t *var_52;
    struct indirect_placeholder_21_ret_type var_53;
    uint64_t var_54;
    uint64_t var_40;
    uint64_t *var_41;
    uint64_t var_42;
    struct helper_paddq_xmm_wrapper_ret_type var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t rbx_4;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t local_sp_6;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t *var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t *var_69;
    uint64_t var_70;
    struct indirect_placeholder_22_ret_type var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t *var_74;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t *var_26;
    uint64_t var_27;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r15();
    var_3 = init_r14();
    var_4 = init_r12();
    var_5 = init_rbx();
    var_6 = init_r9();
    var_7 = init_cc_src2();
    var_8 = init_r8();
    var_9 = init_r13();
    var_10 = init_state_0x8558();
    var_11 = init_state_0x8560();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_9;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_1;
    *(uint64_t *)(var_0 + (-48L)) = var_5;
    *(uint64_t *)(var_0 + (-64L)) = rdi;
    *(uint32_t *)(var_0 + (-108L)) = (uint32_t)rsi;
    *(uint64_t *)(var_0 + (-128L)) = rdx;
    *(uint64_t *)(var_0 + (-72L)) = rcx;
    *(uint64_t *)(var_0 + (-104L)) = rcx;
    var_12 = (uint64_t *)(var_0 + (-144L));
    *var_12 = 4205616UL;
    var_13 = indirect_placeholder_13(1048UL);
    var_14 = var_13.field_0;
    var_15 = helper_pxor_xmm_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(776UL), var_10, var_11);
    var_16 = var_15.field_0;
    var_17 = var_15.field_1;
    *(uint64_t *)(var_14 + 1040UL) = 0UL;
    *(uint64_t *)(var_14 + 1024UL) = var_16;
    *(uint64_t *)(var_14 + 1032UL) = var_17;
    *var_12 = var_14;
    var_18 = var_0 + (-152L);
    *(uint64_t *)var_18 = 4205655UL;
    var_19 = indirect_placeholder_23(1048UL);
    var_20 = var_19.field_0;
    *(uint64_t *)(var_0 + (-112L)) = var_14;
    *(uint64_t *)(var_0 + (-136L)) = 0UL;
    rbp_0_ph_ph = var_20;
    r12_0 = 0UL;
    rdi3_0 = 0UL;
    r12_3_ph = 1UL;
    local_sp_7_ph_ph = var_18;
    r15_0_ph_ph = var_20;
    rbx_5_ph_ph = var_14;
    r9_0_ph_ph = var_6;
    r8_0_ph_ph = var_8;
    rbx_4 = 0UL;
    while (1U)
        {
            rbp_0_ph = rbp_0_ph_ph;
            r15_0_ph = r15_0_ph_ph;
            rbx_5_ph = rbx_5_ph_ph;
            local_sp_7_ph = local_sp_7_ph_ph;
            while (1U)
                {
                    var_21 = (uint64_t *)(rbp_0_ph + 1024UL);
                    var_22 = (uint64_t *)(rbp_0_ph + 1032UL);
                    var_23 = (uint64_t *)(rbp_0_ph + 1040UL);
                    rdi3_2 = r15_0_ph;
                    rbx_5 = rbx_5_ph;
                    local_sp_7 = local_sp_7_ph;
                    while (1U)
                        {
                            var_24 = (uint64_t)*(uint32_t *)(local_sp_7 + 28UL);
                            var_25 = local_sp_7 + (-8L);
                            var_26 = (uint64_t *)var_25;
                            *var_26 = 4205697UL;
                            var_27 = indirect_placeholder_18(1024UL, var_24, r15_0_ph);
                            local_sp_6 = var_25;
                            if ((var_27 + (-1L)) <= 18446744073709551613UL) {
                                var_72 = *(uint64_t *)(local_sp_7 + 32UL);
                                var_73 = local_sp_7 + (-16L);
                                var_74 = (uint64_t *)var_73;
                                *var_74 = 4206077UL;
                                indirect_placeholder_1();
                                rbx_3_ph = var_72;
                                r14_0 = var_72;
                                rbx_1 = var_72;
                                local_sp_2 = var_73;
                                rbx_0 = var_72;
                                local_sp_1 = var_73;
                                r14_1 = var_72;
                                if (var_27 == 18446744073709551615UL) {
                                    var_107 = *(uint64_t *)(local_sp_7 + 56UL);
                                    *(uint64_t *)(local_sp_7 + (-24L)) = 4206467UL;
                                    var_108 = indirect_placeholder_19(rbp_0_ph, 18446744073709551615UL, 4UL, var_107);
                                    var_109 = var_108.field_0;
                                    *(uint64_t *)(local_sp_7 + (-32L)) = 4206475UL;
                                    indirect_placeholder_1();
                                    var_110 = (uint64_t)*(uint32_t *)var_109;
                                    var_111 = local_sp_7 + (-40L);
                                    *(uint64_t *)var_111 = 4206497UL;
                                    indirect_placeholder_14(0UL, var_109, 4264073UL, r9_0_ph_ph, 0UL, var_110, r8_0_ph_ph);
                                    local_sp_0 = var_111;
                                    r12_3_ph = 0UL;
                                    local_sp_5_ph = var_111;
                                    if (var_72 != 0UL) {
                                        loop_state_var = 4U;
                                        break;
                                    }
                                    loop_state_var = 0U;
                                    break;
                                }
                                var_75 = *var_74;
                                var_76 = *(uint64_t *)(var_75 + 1024UL);
                                if (var_76 != 0UL) {
                                    loop_state_var = 3U;
                                    break;
                                }
                                if ((uint64_t)(*(unsigned char *)((var_76 + var_75) + (-1L)) - *(unsigned char *)4289329UL) != 0UL) {
                                    loop_state_var = 3U;
                                    break;
                                }
                                var_77 = (uint64_t *)(var_75 + 1032UL);
                                *var_77 = (*var_77 + 1UL);
                                var_78 = (uint64_t *)local_sp_7;
                                *var_78 = (*var_78 + 1UL);
                                loop_state_var = 3U;
                                break;
                            }
                            if (*(uint64_t *)local_sp_7 != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            *var_21 = var_27;
                            var_28 = var_27 + r15_0_ph;
                            *var_22 = 0UL;
                            var_29 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)4289329UL;
                            *var_23 = 0UL;
                            var_30 = var_28 - rdi3_2;
                            var_31 = local_sp_6 + (-8L);
                            var_32 = (uint64_t *)var_31;
                            *var_32 = 4205804UL;
                            var_33 = indirect_placeholder_18(var_30, rdi3_2, var_29);
                            var_34 = rbx_4 + 1UL;
                            rbx_4 = var_34;
                            local_sp_6 = var_31;
                            while (var_33 != 0UL)
                                {
                                    *var_22 = var_34;
                                    rdi3_2 = var_33 + 1UL;
                                    var_30 = var_28 - rdi3_2;
                                    var_31 = local_sp_6 + (-8L);
                                    var_32 = (uint64_t *)var_31;
                                    *var_32 = 4205804UL;
                                    var_33 = indirect_placeholder_18(var_30, rdi3_2, var_29);
                                    var_34 = rbx_4 + 1UL;
                                    rbx_4 = var_34;
                                    local_sp_6 = var_31;
                                }
                            var_35 = *var_32;
                            var_36 = (uint64_t *)(local_sp_6 + 8UL);
                            var_37 = *var_36 + rbx_4;
                            *var_36 = var_37;
                            var_38 = ((var_27 + *(uint64_t *)(var_35 + 1024UL)) > 1023UL);
                            var_39 = *var_32;
                            rbx_5 = var_39;
                            if (!var_38) {
                                loop_state_var = 2U;
                                break;
                            }
                            var_40 = local_sp_6 + (-16L);
                            *(uint64_t *)var_40 = 4206026UL;
                            indirect_placeholder_1();
                            var_41 = (uint64_t *)(var_39 + 1024UL);
                            var_42 = *var_41;
                            var_43 = (uint64_t *)(var_39 + 1032UL);
                            var_44 = *var_43;
                            var_45 = helper_paddq_xmm_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(840UL), *var_21, *var_22, var_42, var_44);
                            var_46 = var_45.field_0;
                            var_47 = var_45.field_1;
                            *(uint64_t *)(local_sp_6 + 32UL) = var_42;
                            *(uint64_t *)(local_sp_6 + 40UL) = var_44;
                            *var_41 = var_46;
                            *var_43 = var_47;
                            local_sp_7 = var_40;
                            continue;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 2U:
                        {
                            var_48 = *(uint64_t *)(local_sp_6 + 32UL);
                            *(uint64_t *)(var_39 + 1040UL) = rbp_0_ph;
                            var_49 = var_37 - *(uint64_t *)(var_48 + 1032UL);
                            var_50 = (uint64_t *)local_sp_6;
                            rbx_5_ph = var_48;
                            if (var_49 <= *var_50) {
                                loop_state_var = 4U;
                                switch_state_var = 1;
                                break;
                            }
                            var_51 = local_sp_6 + (-16L);
                            var_52 = (uint64_t *)var_51;
                            *var_52 = 4205986UL;
                            var_53 = indirect_placeholder_21(1048UL);
                            var_54 = var_53.field_0;
                            *var_52 = rbp_0_ph;
                            rbp_0_ph = var_54;
                            r15_0_ph = var_54;
                            local_sp_7_ph = var_51;
                            continue;
                        }
                        break;
                      case 3U:
                        {
                            var_79 = *(uint64_t *)local_sp_7;
                            var_80 = *(uint64_t *)(var_72 + 1032UL);
                            var_81 = *var_26;
                            var_93 = var_81;
                            rax_0 = var_80;
                            rsi4_0 = var_79;
                            if ((var_79 - var_80) <= var_81) {
                                loop_state_var = 2U;
                                switch_state_var = 1;
                                break;
                            }
                            r12_2 = *(uint64_t *)(local_sp_7 + 16UL);
                            loop_state_var = 3U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 0U:
                        {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 1U:
                        {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 4U:
                        {
                            loop_state_var = 5U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 2U:
                {
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 3U:
                {
                    loop_state_var = 2U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 5U:
                {
                    loop_state_var = 3U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 4U:
              case 1U:
                {
                    switch (loop_state_var) {
                      case 4U:
                        {
                            var_55 = *(uint64_t *)(var_48 + 1024UL);
                            var_56 = (uint64_t *)(local_sp_6 + 24UL);
                            *var_56 = (*var_56 + var_55);
                            var_57 = local_sp_6 + (-16L);
                            var_58 = (uint64_t *)var_57;
                            *var_58 = 4205906UL;
                            var_59 = indirect_placeholder_20(var_48, var_27, var_48, var_48, var_55);
                            var_60 = var_59.field_1;
                            var_61 = var_59.field_2;
                            var_62 = var_59.field_3;
                            var_63 = var_59.field_6;
                            var_64 = var_59.field_7;
                            var_65 = var_59.field_10;
                            var_66 = *(uint64_t *)(var_63 + 1032UL);
                            var_67 = *(uint64_t *)(var_63 + 1040UL);
                            *var_58 = var_60;
                            var_68 = var_62 - var_66;
                            *var_56 = var_67;
                            *var_50 = var_68;
                            rbp_0_ph_ph_be = var_63;
                            r15_0_ph_ph_be = var_61;
                            rbx_5_ph_ph_be = var_63;
                            r9_0_ph_ph_be = var_64;
                            local_sp_7_ph_ph_be = var_57;
                            r8_0_ph_ph_be = var_65;
                        }
                        break;
                      case 1U:
                        {
                            var_69 = (uint64_t *)(local_sp_7 + 24UL);
                            *var_69 = (*var_69 + var_27);
                            var_70 = local_sp_7 + (-16L);
                            *(uint64_t *)var_70 = 4205968UL;
                            var_71 = indirect_placeholder_22(var_27, rbx_5, r15_0_ph, var_27);
                            rbp_0_ph_ph_be = var_71.field_1;
                            r15_0_ph_ph_be = var_71.field_2;
                            rbx_5_ph_ph_be = var_71.field_6;
                            r9_0_ph_ph_be = var_71.field_7;
                            local_sp_7_ph_ph_be = var_70;
                            r8_0_ph_ph_be = var_71.field_10;
                        }
                        break;
                    }
                    rbp_0_ph_ph = rbp_0_ph_ph_be;
                    local_sp_7_ph_ph = local_sp_7_ph_ph_be;
                    r15_0_ph_ph = r15_0_ph_ph_be;
                    rbx_5_ph_ph = rbx_5_ph_ph_be;
                    r9_0_ph_ph = r9_0_ph_ph_be;
                    r8_0_ph_ph = r8_0_ph_ph_be;
                    continue;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 3U:
        {
            r12_1 = r12_0;
            if ((long)*(uint64_t *)(local_sp_0 + 64UL) >= (long)0UL) {
                var_114 = *(uint64_t *)(local_sp_0 + 72UL);
                var_115 = *(uint64_t *)(local_sp_0 + 32UL);
                var_116 = (uint64_t)*(uint32_t *)(local_sp_0 + 28UL);
                *(uint64_t *)(local_sp_0 + (-8L)) = 4206385UL;
                var_117 = indirect_placeholder_15(var_114, 0UL, var_116, var_115);
                r12_1 = ((long)var_117.field_0 < (long)0UL) ? 0UL : (uint64_t)(uint32_t)r12_0;
            }
            return (uint64_t)(uint32_t)r12_1;
        }
        break;
      case 2U:
      case 1U:
      case 0U:
        {
            switch (loop_state_var) {
              case 0U:
                {
                    r12_0 = r12_3_ph;
                    rbx_3 = rbx_3_ph;
                    local_sp_5 = local_sp_5_ph;
                    var_112 = *(uint64_t *)(rbx_3 + 1040UL);
                    var_113 = local_sp_5 + (-8L);
                    *(uint64_t *)var_113 = 4206351UL;
                    indirect_placeholder_1();
                    local_sp_0 = var_113;
                    rbx_3 = var_112;
                    local_sp_5 = var_113;
                    do {
                        var_112 = *(uint64_t *)(rbx_3 + 1040UL);
                        var_113 = local_sp_5 + (-8L);
                        *(uint64_t *)var_113 = 4206351UL;
                        indirect_placeholder_1();
                        local_sp_0 = var_113;
                        rbx_3 = var_112;
                        local_sp_5 = var_113;
                    } while (var_112 != 0UL);
                }
                break;
              case 2U:
              case 1U:
                {
                    switch (loop_state_var) {
                      case 1U:
                        {
                            var_94 = helper_cc_compute_c_wrapper(var_93 - rsi4_0, rsi4_0, var_7, 17U);
                            local_sp_3 = local_sp_2;
                            rbx_3_ph = rbx_1;
                            rdi3_1 = r14_1;
                            r13_0_in = rax_0;
                            local_sp_5_ph = local_sp_2;
                            if (var_94 != 0UL) {
                                var_95 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)4289329UL;
                                var_96 = (rax_0 - var_93) + rsi4_0;
                                var_97 = *(uint64_t *)(r14_1 + 1024UL) + r14_1;
                                var_98 = (uint64_t *)(r14_1 + 1032UL);
                                r13_0 = r13_0_in + 1UL;
                                var_99 = var_97 - rdi3_1;
                                var_100 = local_sp_3 + (-8L);
                                *(uint64_t *)var_100 = 4206277UL;
                                var_101 = indirect_placeholder_18(var_99, rdi3_1, var_95);
                                local_sp_3 = var_100;
                                r13_0_in = r13_0;
                                while (var_101 != 0UL)
                                    {
                                        *var_98 = r13_0;
                                        var_102 = var_101 + 1UL;
                                        rdi3_0 = var_102;
                                        rdi3_1 = var_102;
                                        if (r13_0 == var_96) {
                                            break;
                                        }
                                        r13_0 = r13_0_in + 1UL;
                                        var_99 = var_97 - rdi3_1;
                                        var_100 = local_sp_3 + (-8L);
                                        *(uint64_t *)var_100 = 4206277UL;
                                        var_101 = indirect_placeholder_18(var_99, rdi3_1, var_95);
                                        local_sp_3 = var_100;
                                        r13_0_in = r13_0;
                                    }
                                var_103 = rdi3_0 - r14_1;
                                var_104 = (uint64_t *)(local_sp_3 + 24UL);
                                *var_104 = (*var_104 + var_103);
                                var_105 = local_sp_3 + (-16L);
                                *(uint64_t *)var_105 = 4206324UL;
                                var_106 = indirect_placeholder_16(var_97, rbx_1, r14_1, var_103);
                                rbx_3_ph = var_106.field_6;
                                local_sp_5_ph = var_105;
                            }
                        }
                        break;
                      case 2U:
                        {
                            var_82 = *(uint64_t *)(r14_0 + 1024UL);
                            var_83 = r12_2 + var_82;
                            var_84 = local_sp_1 + (-8L);
                            *(uint64_t *)var_84 = 4206170UL;
                            var_85 = indirect_placeholder_17(var_83, rbx_0, r14_0, var_82);
                            var_86 = var_85.field_1;
                            var_87 = var_85.field_3;
                            var_88 = var_85.field_11;
                            var_89 = var_86 - *(uint64_t *)(var_87 + 1032UL);
                            var_90 = *(uint64_t *)(var_87 + 1040UL);
                            var_91 = *(uint64_t *)(var_90 + 1032UL);
                            r14_0 = var_90;
                            local_sp_2 = var_84;
                            local_sp_1 = var_84;
                            rax_0 = var_91;
                            r14_1 = var_90;
                            rsi4_0 = var_89;
                            while ((var_89 - var_91) <= var_88)
                                {
                                    r12_2 = var_85.field_4;
                                    rbx_0 = var_85.field_6;
                                    var_82 = *(uint64_t *)(r14_0 + 1024UL);
                                    var_83 = r12_2 + var_82;
                                    var_84 = local_sp_1 + (-8L);
                                    *(uint64_t *)var_84 = 4206170UL;
                                    var_85 = indirect_placeholder_17(var_83, rbx_0, r14_0, var_82);
                                    var_86 = var_85.field_1;
                                    var_87 = var_85.field_3;
                                    var_88 = var_85.field_11;
                                    var_89 = var_86 - *(uint64_t *)(var_87 + 1032UL);
                                    var_90 = *(uint64_t *)(var_87 + 1040UL);
                                    var_91 = *(uint64_t *)(var_90 + 1032UL);
                                    r14_0 = var_90;
                                    local_sp_2 = var_84;
                                    local_sp_1 = var_84;
                                    rax_0 = var_91;
                                    r14_1 = var_90;
                                    rsi4_0 = var_89;
                                }
                            var_92 = var_85.field_6;
                            *(uint64_t *)(local_sp_1 + 24UL) = var_85.field_4;
                            *(uint64_t *)(local_sp_1 + 8UL) = var_89;
                            var_93 = *(uint64_t *)local_sp_1;
                            rbx_1 = var_92;
                        }
                        break;
                    }
                }
                break;
            }
        }
        break;
    }
}
