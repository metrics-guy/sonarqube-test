typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_107_ret_type;
struct indirect_placeholder_108_ret_type;
struct indirect_placeholder_109_ret_type;
struct indirect_placeholder_107_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_108_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_109_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t indirect_placeholder_11(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_rax(void);
extern void indirect_placeholder_16(uint64_t param_0);
extern void indirect_placeholder_14(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
extern uint32_t init_state_0x82fc(void);
extern struct indirect_placeholder_107_ret_type indirect_placeholder_107(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_108_ret_type indirect_placeholder_108(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_109_ret_type indirect_placeholder_109(uint64_t param_0);
uint64_t bb_fts_open(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint32_t var_9;
    uint32_t var_10;
    uint64_t rax_3;
    uint64_t local_sp_0;
    uint64_t local_sp_2;
    uint64_t rbp_0;
    uint64_t var_62;
    uint64_t var_68;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint32_t *var_66;
    uint32_t var_67;
    uint64_t local_sp_10;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    struct indirect_placeholder_107_ret_type var_60;
    uint64_t var_61;
    uint64_t rbp_1;
    uint64_t rdx1_1;
    uint64_t r13_0;
    uint64_t rax_2;
    uint64_t rdx1_0;
    uint64_t var_38;
    uint64_t local_sp_5;
    uint64_t var_70;
    uint64_t rax_0;
    uint64_t local_sp_1;
    uint64_t r14_2;
    uint64_t *var_49;
    uint64_t var_50;
    uint64_t rax_1;
    uint64_t local_sp_3;
    uint64_t local_sp_8;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t rbp_2;
    uint64_t rbp_3;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t r14_1;
    uint64_t var_42;
    bool var_43;
    uint64_t var_44;
    uint64_t var_45;
    struct indirect_placeholder_108_ret_type var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t local_sp_4;
    uint64_t var_69;
    uint64_t var_39;
    uint64_t *var_40;
    uint64_t var_41;
    uint64_t var_36;
    uint64_t r15_2;
    uint64_t r15_1;
    uint64_t *var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t r14_0;
    uint64_t local_sp_6;
    uint64_t local_sp_7;
    uint64_t var_37;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t local_sp_11;
    uint64_t var_14;
    uint64_t *_cast;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t rcx_0;
    uint64_t rdi2_0;
    uint32_t *var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint32_t *var_22;
    uint64_t *var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    unsigned char *var_31;
    unsigned char var_32;
    uint64_t r12_0;
    uint64_t var_11;
    struct indirect_placeholder_109_ret_type var_12;
    uint64_t var_13;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbp();
    var_3 = init_r15();
    var_4 = init_r14();
    var_5 = init_r12();
    var_6 = init_rbx();
    var_7 = init_cc_src2();
    var_8 = init_r13();
    var_9 = init_state_0x82fc();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_8;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    *(uint64_t *)(var_0 + (-48L)) = var_6;
    *(uint64_t *)(var_0 + (-80L)) = rdx;
    var_10 = (uint32_t)rsi;
    rax_3 = var_1;
    r13_0 = 0UL;
    rbp_2 = 0UL;
    rbp_3 = 0UL;
    r15_1 = 0UL;
    r14_0 = 0UL;
    r12_0 = 0UL;
    if ((uint64_t)(var_10 & (-8192)) == 0UL) {
        *(uint64_t *)(var_0 + (-96L)) = 4224821UL;
        indirect_placeholder_1();
        *(uint32_t *)rax_3 = 22U;
        return r12_0;
    }
    var_11 = (uint64_t)((uint16_t)rsi & (unsigned short)516U);
    rax_3 = var_11;
    if (!!(((uint64_t)(((uint32_t)var_11 + (-516)) & (-4)) == 0UL) || ((rsi & 18UL) == 0UL))) {
        return;
    }
    *(uint64_t *)(var_0 + (-96L)) = 4224074UL;
    var_12 = indirect_placeholder_109(128UL);
    var_13 = var_12.field_0;
    if (var_13 == 0UL) {
        return r12_0;
    }
    var_14 = var_13 + 8UL;
    _cast = (uint64_t *)var_13;
    *_cast = 0UL;
    var_15 = var_14 & (-8L);
    *(uint64_t *)(var_13 + 120UL) = 0UL;
    var_16 = (uint64_t)var_9 << 3UL;
    rcx_0 = (uint64_t)((uint32_t)((uint64_t)(((uint32_t)var_13 - (uint32_t)var_15) + 128U) >> 3UL) & 536870911U);
    rdi2_0 = var_15;
    r12_0 = var_13;
    while (rcx_0 != 0UL)
        {
            *(uint64_t *)rdi2_0 = 0UL;
            rcx_0 = rcx_0 + (-1L);
            rdi2_0 = rdi2_0 + var_16;
        }
    var_17 = (uint32_t *)(var_13 + 44UL);
    *var_17 = 4294967196U;
    var_18 = (uint64_t)(var_10 & (-517)) | 4UL;
    var_19 = rsi & 2UL;
    var_20 = *(uint64_t *)(var_0 + (-88L));
    var_21 = (var_19 == 0UL) ? rsi : var_18;
    *(uint64_t *)(var_13 + 64UL) = var_20;
    var_22 = (uint32_t *)(var_13 + 72UL);
    *var_22 = (uint32_t)var_21;
    var_23 = (uint64_t *)(var_0 + (-104L));
    *var_23 = 4224173UL;
    var_24 = indirect_placeholder_2(rdi);
    var_25 = var_13 + 48UL;
    var_26 = var_13 + 32UL;
    var_27 = helper_cc_compute_c_wrapper(var_24 + (-4096L), 4096UL, var_7, 17U);
    var_28 = (var_27 == 0UL) ? var_24 : 4096UL;
    var_29 = var_0 + (-112L);
    *(uint64_t *)var_29 = 4224203UL;
    var_30 = indirect_placeholder_11(var_28, var_26, var_25);
    var_31 = (unsigned char *)(var_0 + (-90L));
    var_32 = (unsigned char)var_30;
    *var_31 = var_32;
    local_sp_6 = var_29;
    local_sp_10 = var_29;
    local_sp_11 = var_29;
    if ((uint64_t)var_32 == 0UL) {
        *(uint64_t *)(local_sp_11 + (-8L)) = 4224717UL;
        indirect_placeholder_1();
    } else {
        var_33 = (uint64_t *)rdi;
        if (*var_33 == 0UL) {
            if (*var_23 != 0UL) {
                var_55 = local_sp_10 + (-8L);
                *(uint64_t *)var_55 = 4224582UL;
                var_56 = indirect_placeholder_11(0UL, var_13, 4285687UL);
                *_cast = var_56;
                rbp_1 = rbp_3;
                local_sp_4 = var_55;
                if (var_56 != 0UL) {
                    *(uint64_t *)(var_56 + 16UL) = rbp_3;
                    var_57 = (uint64_t)*var_22;
                    var_58 = var_13 + 88UL;
                    *(uint16_t *)(var_56 + 112UL) = (uint16_t)(unsigned short)9U;
                    var_59 = local_sp_10 + (-16L);
                    *(uint64_t *)var_59 = 4224619UL;
                    var_60 = indirect_placeholder_107(var_57, var_58);
                    var_61 = var_60.field_1;
                    local_sp_0 = var_59;
                    local_sp_4 = var_59;
                    if ((uint64_t)(unsigned char)var_60.field_0 != 0UL) {
                        var_62 = (uint64_t)*var_22;
                        var_63 = (uint64_t)*var_17;
                        var_64 = local_sp_10 + (-24L);
                        *(uint64_t *)var_64 = 4224879UL;
                        var_65 = indirect_placeholder_9(var_61, 4285329UL, var_63, var_62);
                        var_66 = (uint32_t *)(var_13 + 40UL);
                        var_67 = (uint32_t)var_65;
                        *var_66 = var_67;
                        local_sp_0 = var_64;
                        if ((uint64_t)((uint16_t)var_62 & (unsigned short)516U) != 0UL & (int)var_67 > (int)4294967295U) {
                            *var_22 = (*var_22 | 4U);
                        }
                        var_68 = var_13 + 96UL;
                        *(uint64_t *)(local_sp_0 + (-8L)) = 4224655UL;
                        indirect_placeholder_14(var_68, 4294967295UL);
                        return r12_0;
                    }
                }
                *(uint64_t *)(local_sp_4 + (-8L)) = 4224688UL;
                indirect_placeholder_11(rbp_1, rdi, rbp_1);
                var_69 = local_sp_4 + (-16L);
                *(uint64_t *)var_69 = 4224696UL;
                indirect_placeholder_1();
                local_sp_5 = var_69;
                var_70 = local_sp_5 + (-8L);
                *(uint64_t *)var_70 = 4224706UL;
                indirect_placeholder_1();
                local_sp_11 = var_70;
                *(uint64_t *)(local_sp_11 + (-8L)) = 4224717UL;
                indirect_placeholder_1();
                return r12_0;
            }
            *(unsigned char *)(local_sp_6 + 22UL) = ((unsigned char)(*var_22 >> 10U) & '\x01');
            r15_2 = r15_1;
            r14_1 = r14_0;
            local_sp_7 = local_sp_6;
        } else {
            var_34 = var_0 + (-120L);
            *(uint64_t *)var_34 = 4224242UL;
            var_35 = indirect_placeholder_11(0UL, var_13, 4285687UL);
            local_sp_5 = var_34;
            r15_2 = var_35;
            r15_1 = var_35;
            local_sp_6 = var_34;
            local_sp_7 = var_34;
            if (var_35 != 0UL) {
                var_70 = local_sp_5 + (-8L);
                *(uint64_t *)var_70 = 4224706UL;
                indirect_placeholder_1();
                local_sp_11 = var_70;
                *(uint64_t *)(local_sp_11 + (-8L)) = 4224717UL;
                indirect_placeholder_1();
                return r12_0;
            }
            *(uint64_t *)(var_35 + 88UL) = 18446744073709551615UL;
            var_36 = *var_33;
            *(uint64_t *)(var_35 + 104UL) = 18446744073709551615UL;
            r14_0 = var_36;
            r14_1 = var_36;
            if (var_20 == 0UL) {
                *(unsigned char *)(local_sp_6 + 22UL) = ((unsigned char)(*var_22 >> 10U) & '\x01');
                r15_2 = r15_1;
                r14_1 = r14_0;
                local_sp_7 = local_sp_6;
            }
        }
        r14_2 = r14_1;
        local_sp_8 = local_sp_7;
        local_sp_10 = local_sp_7;
        if (r14_1 != 0UL) {
            *(uint64_t *)(local_sp_7 + 24UL) = 0UL;
            var_37 = ((rsi >> 12UL) & 1UL) ^ 1UL;
            *(unsigned char *)(local_sp_7 + 23UL) = (unsigned char)var_37;
            rax_2 = var_37;
            while (1U)
                {
                    *(uint64_t *)(local_sp_8 + (-8L)) = 4224463UL;
                    indirect_placeholder_1();
                    rdx1_0 = rax_2;
                    rbp_1 = rbp_2;
                    rdx1_1 = rax_2;
                    if (rax_2 <= 2UL & *(unsigned char *)(local_sp_8 + 15UL) != '\x00' & *(unsigned char *)((rax_2 + r14_2) + (-1L)) == '/') {
                        rdx1_1 = rdx1_0;
                        while (*(unsigned char *)((rdx1_0 + r14_2) + (-2L)) != '/')
                            {
                                var_38 = rdx1_0 + (-1L);
                                rdx1_0 = var_38;
                                rdx1_1 = var_38;
                                if (rdx1_0 == 2UL) {
                                    break;
                                }
                                rdx1_1 = rdx1_0;
                            }
                    }
                    var_39 = local_sp_8 + (-16L);
                    var_40 = (uint64_t *)var_39;
                    *var_40 = 4224347UL;
                    var_41 = indirect_placeholder_11(rdx1_1, var_13, r14_2);
                    rbp_0 = var_41;
                    local_sp_4 = var_39;
                    if (var_41 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    *(uint64_t *)(var_41 + 88UL) = 0UL;
                    var_42 = var_41 + 264UL;
                    *(uint64_t *)(var_41 + 8UL) = r15_2;
                    *(uint64_t *)(var_41 + 48UL) = var_42;
                    var_43 = (rbp_2 == 0UL);
                    rax_0 = var_42;
                    if (var_43) {
                        var_45 = local_sp_8 + (-24L);
                        *(uint64_t *)var_45 = 4224733UL;
                        var_46 = indirect_placeholder_108(0UL, var_13, var_41);
                        var_47 = var_46.field_0;
                        var_48 = *var_40;
                        *(uint16_t *)(var_41 + 112UL) = (uint16_t)var_47;
                        local_sp_2 = var_45;
                        rax_0 = var_47;
                        local_sp_1 = var_45;
                        rax_1 = var_47;
                        local_sp_3 = var_45;
                        if (var_48 == 0UL) {
                            *(uint64_t *)(var_41 + 16UL) = rbp_2;
                            rax_1 = rax_0;
                            local_sp_3 = local_sp_1;
                        } else {
                            *(uint64_t *)(var_41 + 16UL) = 0UL;
                            if (var_43) {
                                *(uint64_t *)local_sp_8 = var_41;
                            } else {
                                var_49 = (uint64_t *)(local_sp_2 + 24UL);
                                var_50 = *var_49;
                                *var_49 = var_41;
                                *(uint64_t *)(var_50 + 16UL) = var_41;
                                rbp_0 = rbp_2;
                                rax_1 = var_50;
                                local_sp_3 = local_sp_2;
                            }
                        }
                        var_51 = r13_0 + 1UL;
                        var_52 = *(uint64_t *)((var_51 << 3UL) + rdi);
                        local_sp_10 = local_sp_3;
                        r13_0 = var_51;
                        rax_2 = rax_1;
                        r14_2 = var_52;
                        local_sp_8 = local_sp_3;
                        rbp_2 = rbp_0;
                        rbp_3 = rbp_0;
                        if (var_52 == 0UL) {
                            continue;
                        }
                        if (!((*(uint64_t *)(local_sp_3 + 8UL) != 0UL) && (var_51 > 1UL))) {
                            loop_state_var = 0U;
                            break;
                        }
                        var_53 = local_sp_3 + (-8L);
                        *(uint64_t *)var_53 = 4224564UL;
                        var_54 = indirect_placeholder_11(var_51, var_13, rbp_0);
                        rbp_3 = var_54;
                        local_sp_10 = var_53;
                        loop_state_var = 0U;
                        break;
                    }
                    if (*(unsigned char *)(local_sp_8 + 6UL) != '\x00') {
                        *(uint16_t *)(var_41 + 112UL) = (uint16_t)(unsigned short)11U;
                        var_44 = local_sp_8 + (-24L);
                        *(uint64_t *)var_44 = 4224423UL;
                        indirect_placeholder_16(var_41);
                        local_sp_1 = var_44;
                        local_sp_2 = var_44;
                        if (*var_40 == 0UL) {
                            *(uint64_t *)(var_41 + 16UL) = rbp_2;
                            rax_1 = rax_0;
                            local_sp_3 = local_sp_1;
                        } else {
                            *(uint64_t *)(var_41 + 16UL) = 0UL;
                            var_49 = (uint64_t *)(local_sp_2 + 24UL);
                            var_50 = *var_49;
                            *var_49 = var_41;
                            *(uint64_t *)(var_50 + 16UL) = var_41;
                            rbp_0 = rbp_2;
                            rax_1 = var_50;
                            local_sp_3 = local_sp_2;
                        }
                        var_51 = r13_0 + 1UL;
                        var_52 = *(uint64_t *)((var_51 << 3UL) + rdi);
                        local_sp_10 = local_sp_3;
                        r13_0 = var_51;
                        rax_2 = rax_1;
                        r14_2 = var_52;
                        local_sp_8 = local_sp_3;
                        rbp_2 = rbp_0;
                        rbp_3 = rbp_0;
                        if (var_52 == 0UL) {
                            continue;
                        }
                        if (!((*(uint64_t *)(local_sp_3 + 8UL) != 0UL) && (var_51 > 1UL))) {
                            loop_state_var = 0U;
                            break;
                        }
                        var_53 = local_sp_3 + (-8L);
                        *(uint64_t *)var_53 = 4224564UL;
                        var_54 = indirect_placeholder_11(var_51, var_13, rbp_0);
                        rbp_3 = var_54;
                        local_sp_10 = var_53;
                        loop_state_var = 0U;
                        break;
                    }
                }
            switch (loop_state_var) {
              case 0U:
                {
                    break;
                }
                break;
              case 1U:
                {
                    *(uint64_t *)(local_sp_4 + (-8L)) = 4224688UL;
                    indirect_placeholder_11(rbp_1, rdi, rbp_1);
                    var_69 = local_sp_4 + (-16L);
                    *(uint64_t *)var_69 = 4224696UL;
                    indirect_placeholder_1();
                    local_sp_5 = var_69;
                    var_70 = local_sp_5 + (-8L);
                    *(uint64_t *)var_70 = 4224706UL;
                    indirect_placeholder_1();
                    local_sp_11 = var_70;
                    *(uint64_t *)(local_sp_11 + (-8L)) = 4224717UL;
                    indirect_placeholder_1();
                    return r12_0;
                }
                break;
            }
        }
        var_55 = local_sp_10 + (-8L);
        *(uint64_t *)var_55 = 4224582UL;
        var_56 = indirect_placeholder_11(0UL, var_13, 4285687UL);
        *_cast = var_56;
        rbp_1 = rbp_3;
        local_sp_4 = var_55;
        if (var_56 != 0UL) {
            *(uint64_t *)(var_56 + 16UL) = rbp_3;
            var_57 = (uint64_t)*var_22;
            var_58 = var_13 + 88UL;
            *(uint16_t *)(var_56 + 112UL) = (uint16_t)(unsigned short)9U;
            var_59 = local_sp_10 + (-16L);
            *(uint64_t *)var_59 = 4224619UL;
            var_60 = indirect_placeholder_107(var_57, var_58);
            var_61 = var_60.field_1;
            local_sp_0 = var_59;
            local_sp_4 = var_59;
            if ((uint64_t)(unsigned char)var_60.field_0 != 0UL) {
                var_62 = (uint64_t)*var_22;
                var_63 = (uint64_t)*var_17;
                var_64 = local_sp_10 + (-24L);
                *(uint64_t *)var_64 = 4224879UL;
                var_65 = indirect_placeholder_9(var_61, 4285329UL, var_63, var_62);
                var_66 = (uint32_t *)(var_13 + 40UL);
                var_67 = (uint32_t)var_65;
                *var_66 = var_67;
                local_sp_0 = var_64;
                if ((uint64_t)((uint16_t)var_62 & (unsigned short)516U) != 0UL & (int)var_67 > (int)4294967295U) {
                    *var_22 = (*var_22 | 4U);
                }
                var_68 = var_13 + 96UL;
                *(uint64_t *)(local_sp_0 + (-8L)) = 4224655UL;
                indirect_placeholder_14(var_68, 4294967295UL);
                return r12_0;
            }
        }
        *(uint64_t *)(local_sp_4 + (-8L)) = 4224688UL;
        indirect_placeholder_11(rbp_1, rdi, rbp_1);
        var_69 = local_sp_4 + (-16L);
        *(uint64_t *)var_69 = 4224696UL;
        indirect_placeholder_1();
        local_sp_5 = var_69;
        var_70 = local_sp_5 + (-8L);
        *(uint64_t *)var_70 = 4224706UL;
        indirect_placeholder_1();
        local_sp_11 = var_70;
    }
}
