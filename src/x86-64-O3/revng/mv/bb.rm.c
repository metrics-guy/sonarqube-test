typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_65_ret_type;
struct indirect_placeholder_66_ret_type;
struct indirect_placeholder_67_ret_type;
struct indirect_placeholder_68_ret_type;
struct indirect_placeholder_69_ret_type;
struct indirect_placeholder_70_ret_type;
struct indirect_placeholder_65_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_66_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_67_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_68_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_69_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_70_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern struct indirect_placeholder_65_ret_type indirect_placeholder_65(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_66_ret_type indirect_placeholder_66(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_67_ret_type indirect_placeholder_67(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_68_ret_type indirect_placeholder_68(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_69_ret_type indirect_placeholder_69(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_70_ret_type indirect_placeholder_70(uint64_t param_0, uint64_t param_1, uint64_t param_2);
typedef _Bool bool;
uint64_t bb_rm(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_66_ret_type var_30;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t rcx_2;
    uint64_t r12_0;
    uint64_t local_sp_0;
    uint64_t rax_0;
    uint64_t r12_2;
    uint64_t rcx_0;
    uint64_t local_sp_3;
    uint64_t r9_0;
    uint64_t r8_0;
    uint64_t var_38;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t local_sp_1;
    uint64_t local_sp_3_be;
    uint64_t rbx_0;
    uint32_t var_21;
    uint64_t var_22;
    uint64_t rcx_1;
    uint64_t var_23;
    struct indirect_placeholder_67_ret_type var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t r12_3_be;
    uint64_t rbx_1;
    uint64_t r12_1;
    uint64_t rsi2_0;
    uint64_t var_16;
    struct indirect_placeholder_68_ret_type var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint32_t var_20;
    uint64_t local_sp_2;
    uint32_t var_27;
    uint64_t r12_3;
    uint64_t var_13;
    struct indirect_placeholder_69_ret_type var_14;
    uint64_t var_15;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    struct indirect_placeholder_70_ret_type var_11;
    uint64_t var_12;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r12();
    var_3 = init_rbx();
    var_4 = init_r9();
    var_5 = init_cc_src2();
    var_6 = init_r13();
    var_7 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    *(uint64_t *)(var_0 + (-32L)) = var_3;
    r12_0 = 4UL;
    r9_0 = var_4;
    r8_0 = var_7;
    rbx_1 = rsi;
    r12_3 = 2UL;
    if (*(uint64_t *)rdi == 0UL) {
        return 2UL;
    }
    var_8 = helper_cc_compute_c_wrapper((uint64_t)*(unsigned char *)(rsi + 8UL) + (-1L), 1UL, var_5, 14U);
    var_9 = (uint64_t)((((0U - (uint32_t)var_8) & (-64)) + 600U) & (-40));
    var_10 = var_0 + (-48L);
    *(uint64_t *)var_10 = 4214825UL;
    var_11 = indirect_placeholder_70(0UL, rdi, var_9);
    var_12 = var_11.field_0;
    local_sp_3 = var_10;
    while (1U)
        {
            var_13 = local_sp_3 + (-8L);
            *(uint64_t *)var_13 = 4214840UL;
            var_14 = indirect_placeholder_69(rbx_1, var_12);
            var_15 = var_14.field_0;
            r12_2 = r12_3;
            local_sp_1 = var_13;
            r12_1 = r12_3;
            rsi2_0 = var_15;
            local_sp_2 = var_13;
            if (var_15 != 0UL) {
                rcx_2 = var_14.field_1;
                break;
            }
            rcx_1 = var_14.field_1;
            rbx_0 = var_14.field_2;
            while (1U)
                {
                    var_16 = local_sp_1 + (-8L);
                    *(uint64_t *)var_16 = 4214859UL;
                    var_17 = indirect_placeholder_68(rcx_1, rbx_0, var_12, rsi2_0);
                    var_18 = var_17.field_0;
                    var_19 = var_17.field_1;
                    var_20 = (uint32_t)var_18;
                    local_sp_3_be = var_16;
                    r12_3_be = r12_1;
                    rbx_1 = var_19;
                    if ((uint64_t)(var_20 + (-2)) <= 2UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    r12_3_be = 4UL;
                    if ((uint64_t)(var_20 + (-4)) != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    r12_3_be = r12_1;
                    if ((uint64_t)(var_20 + (-3)) != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_21 = (uint32_t)r12_1;
                    var_22 = ((uint64_t)(var_21 + (-2)) == 0UL) ? 3UL : (uint64_t)var_21;
                    var_23 = local_sp_1 + (-16L);
                    *(uint64_t *)var_23 = 4214893UL;
                    var_24 = indirect_placeholder_67(var_19, var_12);
                    var_25 = var_24.field_0;
                    r12_2 = var_22;
                    local_sp_1 = var_23;
                    r12_1 = var_22;
                    rsi2_0 = var_25;
                    local_sp_2 = var_23;
                    if (var_25 == 0UL) {
                        rcx_1 = var_24.field_1;
                        rbx_0 = var_24.field_2;
                        continue;
                    }
                    rcx_2 = var_24.field_1;
                    loop_state_var = 2U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 2U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 1U:
              case 0U:
                {
                    switch (loop_state_var) {
                      case 1U:
                        {
                            var_26 = local_sp_1 + (-16L);
                            *(uint64_t *)var_26 = 4214985UL;
                            indirect_placeholder();
                            local_sp_3_be = var_26;
                        }
                        break;
                      case 0U:
                        {
                            local_sp_3 = local_sp_3_be;
                            r12_3 = r12_3_be;
                            continue;
                        }
                        break;
                    }
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    *(uint64_t *)(local_sp_2 + (-8L)) = 4214906UL;
    indirect_placeholder();
    var_27 = *(volatile uint32_t *)(uint32_t *)0UL;
    rcx_0 = rcx_2;
    if (var_27 == 0U) {
        var_36 = local_sp_2 + (-16L);
        *(uint64_t *)var_36 = 4214920UL;
        var_37 = indirect_placeholder_2(var_12);
        r12_0 = r12_2;
        local_sp_0 = var_36;
        rax_0 = var_37;
        if ((uint64_t)(uint32_t)var_37 != 0UL) {
            return (uint64_t)(uint32_t)r12_0;
        }
    }
    var_28 = (uint64_t)var_27;
    *(uint64_t *)(local_sp_2 + (-16L)) = 4214997UL;
    indirect_placeholder();
    var_29 = (uint64_t)*(uint32_t *)var_28;
    *(uint64_t *)(local_sp_2 + (-24L)) = 4215019UL;
    var_30 = indirect_placeholder_66(0UL, rcx_2, 4329292UL, var_4, 0UL, var_29, var_7);
    var_31 = var_30.field_0;
    var_32 = var_30.field_1;
    var_33 = var_30.field_2;
    var_34 = local_sp_2 + (-32L);
    *(uint64_t *)var_34 = 4215027UL;
    var_35 = indirect_placeholder_2(var_12);
    local_sp_0 = var_34;
    rax_0 = var_35;
    rcx_0 = var_31;
    r9_0 = var_32;
    r8_0 = var_33;
    if ((uint64_t)(uint32_t)var_35 != 0UL) {
        return (uint64_t)(uint32_t)r12_0;
    }
    *(uint64_t *)(local_sp_0 + (-8L)) = 4215036UL;
    indirect_placeholder();
    var_38 = (uint64_t)*(uint32_t *)rax_0;
    *(uint64_t *)(local_sp_0 + (-16L)) = 4215058UL;
    indirect_placeholder_65(0UL, rcx_0, 4329338UL, r9_0, 0UL, var_38, r8_0);
    return 4UL;
    *(uint64_t *)(local_sp_0 + (-8L)) = 4215036UL;
    indirect_placeholder();
    var_38 = (uint64_t)*(uint32_t *)rax_0;
    *(uint64_t *)(local_sp_0 + (-16L)) = 4215058UL;
    indirect_placeholder_65(0UL, rcx_0, 4329338UL, r9_0, 0UL, var_38, r8_0);
    return 4UL;
}
