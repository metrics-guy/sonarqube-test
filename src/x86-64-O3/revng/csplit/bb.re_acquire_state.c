typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_184_ret_type;
struct indirect_placeholder_185_ret_type;
struct indirect_placeholder_184_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_185_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_35(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern uint64_t init_r10(void);
extern uint64_t init_r9(void);
extern struct indirect_placeholder_184_ret_type indirect_placeholder_184(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_185_ret_type indirect_placeholder_185(uint64_t param_0, uint64_t param_1, uint64_t param_2);
uint64_t bb_re_acquire_state(uint64_t rcx, uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_23;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t local_sp_2;
    uint64_t r10_0;
    uint64_t r10_1;
    uint64_t r9_1;
    uint64_t rcx1_1;
    uint64_t local_sp_1;
    uint64_t local_sp_0;
    uint64_t r8_1;
    uint64_t rcx1_0;
    uint64_t r9_0;
    uint64_t r8_0;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    struct indirect_placeholder_184_ret_type var_21;
    uint64_t var_22;
    uint64_t rcx1_2;
    uint64_t r8_2;
    uint64_t var_9;
    uint64_t var_10;
    struct indirect_placeholder_185_ret_type var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r14();
    var_3 = init_r12();
    var_4 = init_r10();
    var_5 = init_rbx();
    var_6 = init_r9();
    var_7 = init_r13();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_7;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_1;
    *(uint64_t *)(var_0 + (-40L)) = var_5;
    var_8 = *(uint64_t *)(rdx + 8UL);
    r9_0 = 0UL;
    r10_1 = var_4;
    r9_1 = var_6;
    if (var_8 == 0UL) {
        *(uint32_t *)rdi = 0U;
        return 0UL;
    }
    var_9 = rdx + 16UL;
    var_10 = var_0 + (-48L);
    *(uint64_t *)var_10 = 4254057UL;
    var_11 = indirect_placeholder_185(0UL, var_8, var_9);
    var_12 = var_11.field_0;
    var_13 = var_11.field_1;
    var_14 = var_11.field_2;
    var_15 = ((*(uint64_t *)(rsi + 136UL) & var_12) * 24UL) + *(uint64_t *)(rsi + 64UL);
    var_16 = *(uint64_t *)var_15;
    var_17 = helper_cc_compute_all_wrapper(var_16, 0UL, 0UL, 25U);
    local_sp_2 = var_10;
    local_sp_0 = var_10;
    rcx1_0 = var_13;
    r8_0 = var_14;
    rcx1_2 = var_13;
    r8_2 = var_14;
    if ((uint64_t)(((unsigned char)(var_17 >> 4UL) ^ (unsigned char)var_17) & '\xc0') == 0UL) {
        *(uint64_t *)(local_sp_2 + (-8L)) = 4254152UL;
        var_23 = indirect_placeholder_35(rcx1_2, r10_1, var_12, r9_1, rsi, r8_2, rdx);
        r10_0 = var_23;
        if (var_23 == 0UL) {
            *(uint32_t *)rdi = 12U;
            r10_0 = 0UL;
        }
        return r10_0;
    }
    r9_1 = var_16;
    while (1U)
        {
            var_18 = *(uint64_t *)((r9_0 << 3UL) + *(uint64_t *)(var_15 + 16UL));
            r10_0 = var_18;
            r10_1 = var_18;
            rcx1_1 = rcx1_0;
            local_sp_1 = local_sp_0;
            r8_1 = r8_0;
            if (*(uint64_t *)var_18 != var_12) {
                var_19 = var_18 + 8UL;
                var_20 = local_sp_0 + (-8L);
                *(uint64_t *)var_20 = 4254125UL;
                var_21 = indirect_placeholder_184(var_19, rdx);
                local_sp_1 = var_20;
                rcx1_1 = var_21.field_1;
                r8_1 = var_21.field_2;
                if ((uint64_t)(unsigned char)var_21.field_0 != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
            }
            var_22 = r9_0 + 1UL;
            local_sp_2 = local_sp_1;
            local_sp_0 = local_sp_1;
            rcx1_0 = rcx1_1;
            r9_0 = var_22;
            r8_0 = r8_1;
            rcx1_2 = rcx1_1;
            r8_2 = r8_1;
            if (var_16 == var_22) {
                continue;
            }
            loop_state_var = 1U;
            break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            return r10_0;
        }
        break;
      case 1U:
        {
            break;
        }
        break;
    }
}
