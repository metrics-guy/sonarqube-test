typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_11(uint64_t param_0);
void bb_free_dfa_content(uint64_t rdi) {
    uint64_t *var_14;
    uint64_t *var_15;
    uint64_t rbx_1;
    uint64_t local_sp_6;
    uint64_t var_16;
    uint64_t local_sp_7;
    uint64_t var_17;
    bool var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t var_8;
    uint64_t *_pre_phi63;
    uint64_t var_33;
    uint64_t local_sp_0;
    uint64_t local_sp_9;
    uint64_t local_sp_4;
    uint64_t var_32;
    uint64_t local_sp_1;
    uint64_t *var_24;
    uint64_t rdi1_0;
    uint64_t r13_0;
    uint64_t var_25;
    uint64_t *_cast;
    uint64_t local_sp_3;
    uint64_t local_sp_2;
    uint64_t rbx_0;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t local_sp_11;
    uint64_t local_sp_5_be;
    uint64_t local_sp_5;
    uint64_t var_21;
    uint64_t *var_22;
    uint64_t var_23;
    uint64_t local_sp_10;
    uint64_t *var_9;
    uint64_t rbx_2;
    uint64_t rdi1_1;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_cc_src2();
    var_2 = init_rbp();
    var_3 = init_r12();
    var_4 = init_rbx();
    var_5 = init_r13();
    *(uint64_t *)(var_0 + (-8L)) = var_5;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    var_6 = var_0 + (-40L);
    var_7 = (uint64_t *)rdi;
    var_8 = *var_7;
    rbx_1 = 0UL;
    r13_0 = 0UL;
    rbx_0 = 0UL;
    local_sp_11 = var_6;
    local_sp_10 = var_6;
    rbx_2 = 0UL;
    rdi1_1 = var_8;
    if (var_8 != 0UL) {
        var_9 = (uint64_t *)(rdi + 16UL);
        _pre_phi63 = var_9;
        if (*var_9 == 0UL) {
            var_10 = rbx_2 + 1UL;
            var_11 = rdi1_1 + (rbx_2 << 4UL);
            var_12 = local_sp_10 + (-8L);
            *(uint64_t *)var_12 = 4244223UL;
            indirect_placeholder_11(var_11);
            local_sp_10 = var_12;
            rbx_2 = var_10;
            local_sp_11 = var_12;
            while (*var_9 <= var_10)
                {
                    rdi1_1 = *var_7;
                    var_10 = rbx_2 + 1UL;
                    var_11 = rdi1_1 + (rbx_2 << 4UL);
                    var_12 = local_sp_10 + (-8L);
                    *(uint64_t *)var_12 = 4244223UL;
                    indirect_placeholder_11(var_11);
                    local_sp_10 = var_12;
                    rbx_2 = var_10;
                    local_sp_11 = var_12;
                }
        }
    }
    _pre_phi63 = (uint64_t *)(rdi + 16UL);
    var_13 = local_sp_11 + (-8L);
    *(uint64_t *)var_13 = 4244242UL;
    indirect_placeholder();
    local_sp_5 = var_13;
    local_sp_9 = var_13;
    if (*_pre_phi63 != 0UL) {
        var_14 = (uint64_t *)(rdi + 56UL);
        var_15 = (uint64_t *)(rdi + 40UL);
        while (1U)
            {
                local_sp_6 = local_sp_5;
                if (*(uint64_t *)(rdi + 48UL) == 0UL) {
                    var_16 = local_sp_5 + (-8L);
                    *(uint64_t *)var_16 = 4244280UL;
                    indirect_placeholder();
                    local_sp_6 = var_16;
                }
                local_sp_7 = local_sp_6;
                if (*var_14 == 0UL) {
                    var_17 = local_sp_6 + (-8L);
                    *(uint64_t *)var_17 = 4244304UL;
                    indirect_placeholder();
                    local_sp_7 = var_17;
                }
                var_18 = (*var_15 == 0UL);
                var_19 = rbx_1 + 1UL;
                local_sp_5_be = local_sp_7;
                rbx_1 = var_19;
                local_sp_9 = local_sp_7;
                if (var_18) {
                    if (*_pre_phi63 > var_19) {
                        break;
                    }
                }
                var_20 = local_sp_7 + (-8L);
                *(uint64_t *)var_20 = 4244336UL;
                indirect_placeholder();
                local_sp_5_be = var_20;
                local_sp_9 = var_20;
                if (*_pre_phi63 > var_19) {
                    break;
                }
                local_sp_5 = local_sp_5_be;
                continue;
            }
    }
    *(uint64_t *)(local_sp_9 + (-8L)) = 4244353UL;
    indirect_placeholder();
    *(uint64_t *)(local_sp_9 + (-16L)) = 4244366UL;
    indirect_placeholder();
    *(uint64_t *)(local_sp_9 + (-24L)) = 4244376UL;
    indirect_placeholder();
    var_21 = local_sp_9 + (-32L);
    *(uint64_t *)var_21 = 4244385UL;
    indirect_placeholder();
    var_22 = (uint64_t *)(rdi + 64UL);
    var_23 = *var_22;
    local_sp_1 = var_21;
    rdi1_0 = var_23;
    local_sp_4 = var_21;
    if (var_23 != 0UL) {
        var_24 = (uint64_t *)(rdi + 136UL);
        while (1U)
            {
                var_25 = (r13_0 * 24UL) + rdi1_0;
                _cast = (uint64_t *)var_25;
                local_sp_2 = local_sp_1;
                local_sp_3 = local_sp_1;
                if ((long)*_cast <= (long)0UL) {
                    var_26 = *(uint64_t *)((rbx_0 << 3UL) + *(uint64_t *)(var_25 + 16UL));
                    var_27 = rbx_0 + 1UL;
                    var_28 = local_sp_2 + (-8L);
                    *(uint64_t *)var_28 = 4244441UL;
                    indirect_placeholder_11(var_26);
                    local_sp_2 = var_28;
                    rbx_0 = var_27;
                    local_sp_3 = var_28;
                    do {
                        var_26 = *(uint64_t *)((rbx_0 << 3UL) + *(uint64_t *)(var_25 + 16UL));
                        var_27 = rbx_0 + 1UL;
                        var_28 = local_sp_2 + (-8L);
                        *(uint64_t *)var_28 = 4244441UL;
                        indirect_placeholder_11(var_26);
                        local_sp_2 = var_28;
                        rbx_0 = var_27;
                        local_sp_3 = var_28;
                    } while ((long)*_cast <= (long)var_27);
                }
                var_29 = r13_0 + 1UL;
                var_30 = local_sp_3 + (-8L);
                *(uint64_t *)var_30 = 4244460UL;
                indirect_placeholder();
                var_31 = helper_cc_compute_c_wrapper(*var_24 - var_29, var_29, var_1, 17U);
                local_sp_1 = var_30;
                r13_0 = var_29;
                local_sp_4 = var_30;
                if (var_31 == 0UL) {
                    break;
                }
                rdi1_0 = *var_22;
                continue;
            }
    }
    var_32 = local_sp_4 + (-8L);
    *(uint64_t *)var_32 = 4244514UL;
    indirect_placeholder();
    local_sp_0 = var_32;
    if (*(uint64_t *)(rdi + 120UL) == 4339840UL) {
        *(uint64_t *)(local_sp_0 + (-8L)) = 4244546UL;
        indirect_placeholder();
        indirect_placeholder();
        return;
    }
    var_33 = local_sp_4 + (-16L);
    *(uint64_t *)var_33 = 4244533UL;
    indirect_placeholder();
    local_sp_0 = var_33;
    *(uint64_t *)(local_sp_0 + (-8L)) = 4244546UL;
    indirect_placeholder();
    indirect_placeholder();
    return;
}
