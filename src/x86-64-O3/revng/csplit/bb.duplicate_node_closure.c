typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_167_ret_type;
struct indirect_placeholder_167_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_167_ret_type indirect_placeholder_167(uint64_t param_0, uint64_t param_1, uint64_t param_2);
typedef _Bool bool;
uint64_t bb_duplicate_node_closure(uint64_t rcx, uint64_t rdx, uint64_t rdi, uint64_t r8, uint64_t rsi) {
    uint64_t var_34;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t *var_10;
    uint64_t *var_11;
    uint64_t local_sp_2_ph;
    uint64_t r14_0_ph;
    uint64_t rbx_1_ph;
    uint64_t r9_0_ph;
    uint64_t var_12;
    uint64_t *_pre_phi;
    uint64_t local_sp_2;
    uint64_t rbx_1;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t *var_33;
    uint64_t rax_0;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t r9_0;
    uint64_t var_51;
    uint64_t local_sp_0;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t rbx_0;
    uint64_t rdi3_0;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t *var_54;
    uint64_t var_55;
    uint64_t local_sp_1;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_23;
    uint64_t *var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t *var_27;
    uint64_t var_28;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    struct indirect_placeholder_167_ret_type var_21;
    uint64_t var_22;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r15();
    var_3 = init_r14();
    var_4 = init_r12();
    var_5 = init_rbx();
    var_6 = init_r13();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    var_7 = (uint64_t)(uint32_t)r8;
    *(uint64_t *)(var_0 + (-24L)) = var_6;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_1;
    *(uint64_t *)(var_0 + (-48L)) = var_5;
    var_8 = var_0 + (-72L);
    *(uint64_t *)(var_0 + (-64L)) = rcx;
    var_9 = (uint64_t *)(rdi + 40UL);
    var_10 = (uint64_t *)rdi;
    var_11 = (uint64_t *)(rdi + 24UL);
    local_sp_2_ph = var_8;
    r14_0_ph = var_7;
    rbx_1_ph = rdx;
    r9_0_ph = rsi;
    rax_0 = 0UL;
    while (1U)
        {
            var_12 = (uint64_t)(uint32_t)r14_0_ph;
            local_sp_2 = local_sp_2_ph;
            rbx_1 = rbx_1_ph;
            r9_0 = r9_0_ph;
            while (1U)
                {
                    var_13 = *var_9;
                    var_14 = ((r9_0 << 4UL) + *var_10) + 8UL;
                    if (*(unsigned char *)var_14 != '\x04') {
                        var_15 = r9_0 * 24UL;
                        var_16 = var_15 + var_13;
                        var_17 = *(uint64_t *)(var_16 + 8UL);
                        if (var_17 != 0UL) {
                            var_44 = *var_11;
                            *(uint64_t *)((rbx_1 << 3UL) + var_44) = *(uint64_t *)((r9_0 << 3UL) + var_44);
                            loop_state_var = 1U;
                            break;
                        }
                        var_18 = rbx_1 * 24UL;
                        var_19 = var_18 + var_13;
                        var_20 = **(uint64_t **)(var_16 + 16UL);
                        *(uint64_t *)(var_19 + 8UL) = 0UL;
                        r9_0_ph = var_20;
                        rdi3_0 = var_18;
                        if (var_17 != 1UL) {
                            loop_state_var = 2U;
                            break;
                        }
                        *(uint64_t *)(local_sp_2 + (-8L)) = 4242183UL;
                        var_21 = indirect_placeholder_167(var_12, rdi, var_20);
                        var_22 = var_21.field_0;
                        if (var_22 != 18446744073709551615UL) {
                            var_23 = local_sp_2 + (-16L);
                            var_24 = (uint64_t *)var_23;
                            *var_24 = 4242204UL;
                            var_25 = indirect_placeholder_1(var_19, var_22);
                            _pre_phi = var_24;
                            local_sp_1 = var_23;
                            if ((uint64_t)(unsigned char)var_25 != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            var_35 = *(uint64_t *)(*(uint64_t *)((var_15 + *var_9) + 16UL) + 8UL);
                            *_pre_phi = var_35;
                            var_36 = local_sp_1 + (-8L);
                            *(uint64_t *)var_36 = 4242239UL;
                            var_37 = indirect_placeholder_10(var_12, rdi, var_35);
                            local_sp_0 = var_36;
                            rbx_0 = var_37;
                            if (var_37 != 18446744073709551615UL) {
                                loop_state_var = 0U;
                                break;
                            }
                        }
                        var_26 = var_21.field_1;
                        var_27 = (uint64_t *)(local_sp_2 + (-16L));
                        *var_27 = 4242491UL;
                        var_28 = indirect_placeholder_10(var_26, rdi, var_20);
                        if (var_28 != 18446744073709551615UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        var_29 = *var_9 + var_18;
                        *(uint64_t *)(local_sp_2 + (-24L)) = 4242519UL;
                        var_30 = indirect_placeholder_1(var_29, var_28);
                        if ((uint64_t)(unsigned char)var_30 != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        var_31 = *var_27;
                        var_32 = local_sp_2 + (-32L);
                        var_33 = (uint64_t *)var_32;
                        *var_33 = 4242549UL;
                        var_34 = indirect_placeholder_8(var_31, var_28, rdi, var_12, var_20);
                        _pre_phi = var_33;
                        local_sp_1 = var_32;
                        rax_0 = var_34;
                        if ((uint64_t)(uint32_t)var_34 != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                    }
                    var_45 = *var_11;
                    var_46 = rbx_1 * 24UL;
                    var_47 = r9_0 << 3UL;
                    var_48 = *(uint64_t *)(var_47 + var_45);
                    *(uint64_t *)((var_46 + var_13) + 8UL) = 0UL;
                    *(uint64_t *)local_sp_2 = var_48;
                    var_49 = local_sp_2 + (-8L);
                    *(uint64_t *)var_49 = 4242435UL;
                    var_50 = indirect_placeholder_10(var_12, rdi, var_48);
                    local_sp_0 = var_49;
                    rbx_0 = var_50;
                    rdi3_0 = var_46;
                    if (var_50 != 18446744073709551615UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_51 = *var_11;
                    *(uint64_t *)((rbx_1 << 3UL) + var_51) = *(uint64_t *)(var_47 + var_51);
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 2U:
                {
                    if (!((r9_0 != *(uint64_t *)(local_sp_2 + 8UL)) || (r9_0 == rbx_1))) {
                        *(uint64_t *)(local_sp_2 + (-8L)) = 4242598UL;
                        var_38 = indirect_placeholder_1(var_19, var_20);
                        if ((uint64_t)(unsigned char)var_38 != 0UL) {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    var_39 = var_12 | (uint64_t)(uint32_t)((uint16_t)(*(uint32_t *)var_14 >> 8U) & (unsigned short)1023U);
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4242348UL;
                    var_40 = indirect_placeholder_10(var_39, rdi, var_20);
                    r14_0_ph = var_39;
                    rbx_1_ph = var_40;
                    if (var_40 != 18446744073709551615UL) {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    var_41 = *var_9 + var_18;
                    var_42 = local_sp_2 + (-16L);
                    *(uint64_t *)var_42 = 4242372UL;
                    var_43 = indirect_placeholder_1(var_41, var_40);
                    local_sp_2_ph = var_42;
                    if ((uint64_t)(unsigned char)var_43 == 0UL) {
                        continue;
                    }
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 0U:
                {
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 1U:
                {
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            rax_0 = 12UL;
        }
        break;
      case 1U:
        {
            return rax_0;
        }
        break;
    }
}
