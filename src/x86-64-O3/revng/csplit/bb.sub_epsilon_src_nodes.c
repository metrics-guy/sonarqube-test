typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct helper_pxor_xmm_wrapper_337_ret_type;
struct type_4;
struct type_6;
struct indirect_placeholder_210_ret_type;
struct indirect_placeholder_211_ret_type;
struct indirect_placeholder_212_ret_type;
struct indirect_placeholder_213_ret_type;
struct indirect_placeholder_214_ret_type;
struct indirect_placeholder_215_ret_type;
struct helper_pxor_xmm_wrapper_337_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_4 {
};
struct type_6 {
};
struct indirect_placeholder_210_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_211_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_212_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_213_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_214_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_215_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_cc_src2(void);
extern uint64_t init_state_0x8558(void);
extern uint64_t init_state_0x8560(void);
extern uint64_t indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct helper_pxor_xmm_wrapper_337_ret_type helper_pxor_xmm_wrapper_337(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4);
extern void indirect_placeholder_17(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_210_ret_type indirect_placeholder_210(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_211_ret_type indirect_placeholder_211(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_212_ret_type indirect_placeholder_212(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_213_ret_type indirect_placeholder_213(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_214_ret_type indirect_placeholder_214(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_215_ret_type indirect_placeholder_215(uint64_t param_0, uint64_t param_1, uint64_t param_2);
typedef _Bool bool;
uint64_t bb_sub_epsilon_src_nodes(uint64_t rcx, uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    struct helper_pxor_xmm_wrapper_337_ret_type var_10;
    uint64_t var_58;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t *var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t *var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t local_sp_7;
    uint64_t local_sp_5;
    uint64_t var_67;
    uint64_t local_sp_5_ph;
    uint64_t r9_0;
    uint64_t var_68;
    uint64_t var_69;
    struct indirect_placeholder_210_ret_type var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t r9_0_ph;
    uint64_t rdx2_0;
    uint64_t rax_0;
    uint64_t local_sp_0;
    uint64_t r8_1;
    uint64_t var_47;
    uint64_t var_48;
    struct indirect_placeholder_211_ret_type var_49;
    uint64_t local_sp_1;
    uint64_t var_37;
    uint64_t local_sp_2;
    uint64_t var_60;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint32_t var_59;
    uint64_t var_38;
    uint64_t var_39;
    struct indirect_placeholder_212_ret_type var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    struct indirect_placeholder_213_ret_type var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_20;
    uint64_t *var_21;
    uint64_t *var_22;
    uint64_t *var_23;
    uint64_t var_24;
    uint64_t *var_25;
    uint64_t var_26;
    uint64_t r15_0;
    uint64_t local_sp_4;
    uint64_t r8_0;
    uint64_t local_sp_3;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    struct indirect_placeholder_214_ret_type var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t r8_2_ph;
    uint64_t r11_0_ph_in_in;
    uint64_t r11_0_ph;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    struct indirect_placeholder_215_ret_type var_66;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_cc_src2();
    var_2 = init_rbp();
    var_3 = init_r15();
    var_4 = init_r14();
    var_5 = init_r12();
    var_6 = init_rbx();
    var_7 = init_r13();
    var_8 = init_state_0x8558();
    var_9 = init_state_0x8560();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    var_10 = helper_pxor_xmm_wrapper_337((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(776UL), var_8, var_9);
    var_11 = var_10.field_0;
    var_12 = var_10.field_1;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_7;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    *(uint64_t *)(var_0 + (-48L)) = var_6;
    var_13 = var_0 + (-104L);
    var_14 = (uint64_t *)(rdi + 56UL);
    var_15 = *var_14;
    *(uint64_t *)(var_0 + (-72L)) = 0UL;
    var_16 = (rsi * 24UL) + var_15;
    *(uint64_t *)(var_0 + (-88L)) = var_11;
    *(uint64_t *)(var_0 + (-80L)) = var_12;
    var_17 = (uint64_t *)(var_16 + 8UL);
    var_18 = *var_17;
    *(uint64_t *)(var_0 + (-96L)) = rcx;
    var_19 = helper_cc_compute_all_wrapper(var_18, 0UL, 0UL, 25U);
    local_sp_7 = var_13;
    r9_0_ph = 0UL;
    r15_0 = 0UL;
    r8_0 = var_18;
    local_sp_3 = var_13;
    if ((uint64_t)(((unsigned char)(var_19 >> 4UL) ^ (unsigned char)var_19) & '\xc0') == 0UL) {
        *(uint64_t *)(local_sp_7 + (-8L)) = 4256105UL;
        indirect_placeholder();
        return 0UL;
    }
    var_20 = var_16 + 16UL;
    var_21 = (uint64_t *)var_20;
    var_22 = (uint64_t *)rdi;
    var_23 = (uint64_t *)(rdi + 40UL);
    var_24 = rdx + 8UL;
    var_25 = (uint64_t *)var_24;
    var_26 = rdx + 16UL;
    while (1U)
        {
            var_27 = *(uint64_t *)((r15_0 << 3UL) + *var_21);
            r8_1 = r8_0;
            local_sp_4 = local_sp_3;
            if (var_27 == rsi) {
                var_61 = r15_0 + 1UL;
                local_sp_7 = local_sp_4;
                local_sp_5_ph = local_sp_4;
                r15_0 = var_61;
                r8_0 = r8_1;
                local_sp_3 = local_sp_4;
                r8_2_ph = r8_1;
                if ((long)var_61 < (long)r8_1) {
                    continue;
                }
                var_62 = helper_cc_compute_all_wrapper(r8_1, 0UL, 0UL, 25U);
                if ((uint64_t)(((unsigned char)(var_62 >> 4UL) ^ (unsigned char)var_62) & '\xc0') == 0UL) {
                    loop_state_var = 1U;
                    break;
                }
                r11_0_ph_in_in = local_sp_4 + 24UL;
                loop_state_var = 0U;
                break;
            }
            if ((*(unsigned char *)(((var_27 << 4UL) + *var_22) + 8UL) & '\b') != '\x00') {
                var_28 = *var_23;
                var_29 = var_27 * 24UL;
                var_30 = var_28 + var_29;
                var_31 = *(uint64_t *)(var_30 + 16UL);
                var_32 = *(uint64_t *)var_31;
                var_33 = local_sp_3 + (-8L);
                *(uint64_t *)var_33 = 4255964UL;
                var_34 = indirect_placeholder_214(var_32, r8_0, var_20);
                var_35 = var_34.field_0;
                var_36 = var_34.field_1;
                rdx2_0 = var_36;
                rax_0 = var_35;
                local_sp_0 = var_33;
                local_sp_2 = var_33;
                if ((long)*(uint64_t *)(var_30 + 8UL) > (long)1UL) {
                    var_37 = helper_cc_compute_all_wrapper(var_35, 1UL, var_1, 25U);
                    if ((var_37 & 64UL) != 0UL) {
                        var_41 = *(uint64_t *)(var_31 + 8UL);
                        var_42 = helper_cc_compute_all_wrapper(var_41, 0UL, 0UL, 25U);
                        local_sp_4 = local_sp_2;
                        if ((uint64_t)(((unsigned char)(var_42 >> 4UL) ^ (unsigned char)var_42) & '\xc0') == 0UL) {
                            var_61 = r15_0 + 1UL;
                            local_sp_7 = local_sp_4;
                            local_sp_5_ph = local_sp_4;
                            r15_0 = var_61;
                            r8_0 = r8_1;
                            local_sp_3 = local_sp_4;
                            r8_2_ph = r8_1;
                            if ((long)var_61 < (long)r8_1) {
                                continue;
                            }
                            var_62 = helper_cc_compute_all_wrapper(r8_1, 0UL, 0UL, 25U);
                            if ((uint64_t)(((unsigned char)(var_62 >> 4UL) ^ (unsigned char)var_62) & '\xc0') == 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            r11_0_ph_in_in = local_sp_4 + 24UL;
                            loop_state_var = 0U;
                            break;
                        }
                        var_43 = local_sp_2 + (-8L);
                        *(uint64_t *)var_43 = 4256032UL;
                        var_44 = indirect_placeholder_213(var_41, r8_0, var_20);
                        var_45 = var_44.field_0;
                        var_46 = var_44.field_1;
                        rdx2_0 = var_46;
                        rax_0 = var_45;
                        local_sp_0 = var_43;
                        local_sp_4 = local_sp_0;
                        if (rax_0 == 0UL) {
                            var_61 = r15_0 + 1UL;
                            local_sp_7 = local_sp_4;
                            local_sp_5_ph = local_sp_4;
                            r15_0 = var_61;
                            r8_0 = r8_1;
                            local_sp_3 = local_sp_4;
                            r8_2_ph = r8_1;
                            if ((long)var_61 < (long)r8_1) {
                                continue;
                            }
                            var_62 = helper_cc_compute_all_wrapper(r8_1, 0UL, 0UL, 25U);
                            if ((uint64_t)(((unsigned char)(var_62 >> 4UL) ^ (unsigned char)var_62) & '\xc0') == 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            r11_0_ph_in_in = local_sp_4 + 24UL;
                            loop_state_var = 0U;
                            break;
                        }
                        var_47 = *var_25;
                        var_48 = local_sp_0 + (-8L);
                        *(uint64_t *)var_48 = 4255993UL;
                        var_49 = indirect_placeholder_211(rdx2_0, var_47, var_26);
                        local_sp_1 = var_48;
                        local_sp_4 = var_48;
                        if (var_49.field_0 == 0UL) {
                            var_61 = r15_0 + 1UL;
                            local_sp_7 = local_sp_4;
                            local_sp_5_ph = local_sp_4;
                            r15_0 = var_61;
                            r8_0 = r8_1;
                            local_sp_3 = local_sp_4;
                            r8_2_ph = r8_1;
                            if ((long)var_61 < (long)r8_1) {
                                continue;
                            }
                            var_62 = helper_cc_compute_all_wrapper(r8_1, 0UL, 0UL, 25U);
                            if ((uint64_t)(((unsigned char)(var_62 >> 4UL) ^ (unsigned char)var_62) & '\xc0') == 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            r11_0_ph_in_in = local_sp_4 + 24UL;
                            loop_state_var = 0U;
                            break;
                        }
                    }
                    var_38 = *var_25;
                    var_39 = local_sp_3 + (-16L);
                    *(uint64_t *)var_39 = 4255830UL;
                    var_40 = indirect_placeholder_212(var_36, var_38, var_26);
                    local_sp_1 = var_39;
                    local_sp_2 = var_39;
                    if (var_40.field_0 == 0UL) {
                        var_50 = *(uint64_t *)(local_sp_1 + 8UL);
                        var_51 = var_29 + *var_14;
                        var_52 = local_sp_1 + 16UL;
                        var_53 = var_51 + 8UL;
                        var_54 = var_51 + 16UL;
                        var_55 = var_50 + 16UL;
                        var_56 = var_50 + 8UL;
                        var_57 = local_sp_1 + (-8L);
                        *(uint64_t *)var_57 = 4255874UL;
                        var_58 = indirect_placeholder_8(var_53, var_55, var_52, var_54, var_56);
                        var_59 = (uint32_t)var_58;
                        local_sp_4 = var_57;
                        if ((uint64_t)var_59 != 0UL) {
                            var_60 = *var_17;
                            r8_1 = var_60;
                            var_61 = r15_0 + 1UL;
                            local_sp_7 = local_sp_4;
                            local_sp_5_ph = local_sp_4;
                            r15_0 = var_61;
                            r8_0 = r8_1;
                            local_sp_3 = local_sp_4;
                            r8_2_ph = r8_1;
                            if ((long)var_61 < (long)r8_1) {
                                continue;
                            }
                            var_62 = helper_cc_compute_all_wrapper(r8_1, 0UL, 0UL, 25U);
                            if ((uint64_t)(((unsigned char)(var_62 >> 4UL) ^ (unsigned char)var_62) & '\xc0') != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            r11_0_ph_in_in = local_sp_4 + 24UL;
                            loop_state_var = 0U;
                            break;
                        }
                        *(uint32_t *)local_sp_1 = var_59;
                        *(uint64_t *)(local_sp_1 + (-16L)) = 4256198UL;
                        indirect_placeholder();
                        return (uint64_t)*(uint32_t *)var_57;
                    }
                    var_41 = *(uint64_t *)(var_31 + 8UL);
                    var_42 = helper_cc_compute_all_wrapper(var_41, 0UL, 0UL, 25U);
                    local_sp_4 = local_sp_2;
                    if ((uint64_t)(((unsigned char)(var_42 >> 4UL) ^ (unsigned char)var_42) & '\xc0') != 0UL) {
                        var_61 = r15_0 + 1UL;
                        local_sp_7 = local_sp_4;
                        local_sp_5_ph = local_sp_4;
                        r15_0 = var_61;
                        r8_0 = r8_1;
                        local_sp_3 = local_sp_4;
                        r8_2_ph = r8_1;
                        if ((long)var_61 < (long)r8_1) {
                            continue;
                        }
                        var_62 = helper_cc_compute_all_wrapper(r8_1, 0UL, 0UL, 25U);
                        if ((uint64_t)(((unsigned char)(var_62 >> 4UL) ^ (unsigned char)var_62) & '\xc0') != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        r11_0_ph_in_in = local_sp_4 + 24UL;
                        loop_state_var = 0U;
                        break;
                    }
                    var_43 = local_sp_2 + (-8L);
                    *(uint64_t *)var_43 = 4256032UL;
                    var_44 = indirect_placeholder_213(var_41, r8_0, var_20);
                    var_45 = var_44.field_0;
                    var_46 = var_44.field_1;
                    rdx2_0 = var_46;
                    rax_0 = var_45;
                    local_sp_0 = var_43;
                    local_sp_4 = local_sp_0;
                    if (rax_0 != 0UL) {
                        var_61 = r15_0 + 1UL;
                        local_sp_7 = local_sp_4;
                        local_sp_5_ph = local_sp_4;
                        r15_0 = var_61;
                        r8_0 = r8_1;
                        local_sp_3 = local_sp_4;
                        r8_2_ph = r8_1;
                        if ((long)var_61 < (long)r8_1) {
                            continue;
                        }
                        var_62 = helper_cc_compute_all_wrapper(r8_1, 0UL, 0UL, 25U);
                        if ((uint64_t)(((unsigned char)(var_62 >> 4UL) ^ (unsigned char)var_62) & '\xc0') != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        r11_0_ph_in_in = local_sp_4 + 24UL;
                        loop_state_var = 0U;
                        break;
                    }
                    var_47 = *var_25;
                    var_48 = local_sp_0 + (-8L);
                    *(uint64_t *)var_48 = 4255993UL;
                    var_49 = indirect_placeholder_211(rdx2_0, var_47, var_26);
                    local_sp_1 = var_48;
                    local_sp_4 = var_48;
                    if (var_49.field_0 == 0UL) {
                        var_61 = r15_0 + 1UL;
                        local_sp_7 = local_sp_4;
                        local_sp_5_ph = local_sp_4;
                        r15_0 = var_61;
                        r8_0 = r8_1;
                        local_sp_3 = local_sp_4;
                        r8_2_ph = r8_1;
                        if ((long)var_61 < (long)r8_1) {
                            continue;
                        }
                        var_62 = helper_cc_compute_all_wrapper(r8_1, 0UL, 0UL, 25U);
                        if ((uint64_t)(((unsigned char)(var_62 >> 4UL) ^ (unsigned char)var_62) & '\xc0') != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        r11_0_ph_in_in = local_sp_4 + 24UL;
                        loop_state_var = 0U;
                        break;
                    }
                }
                local_sp_4 = local_sp_0;
                if (rax_0 == 0UL) {
                    var_61 = r15_0 + 1UL;
                    local_sp_7 = local_sp_4;
                    local_sp_5_ph = local_sp_4;
                    r15_0 = var_61;
                    r8_0 = r8_1;
                    local_sp_3 = local_sp_4;
                    r8_2_ph = r8_1;
                    if ((long)var_61 < (long)r8_1) {
                        continue;
                    }
                    var_62 = helper_cc_compute_all_wrapper(r8_1, 0UL, 0UL, 25U);
                    if ((uint64_t)(((unsigned char)(var_62 >> 4UL) ^ (unsigned char)var_62) & '\xc0') == 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    r11_0_ph_in_in = local_sp_4 + 24UL;
                    loop_state_var = 0U;
                    break;
                }
                var_47 = *var_25;
                var_48 = local_sp_0 + (-8L);
                *(uint64_t *)var_48 = 4255993UL;
                var_49 = indirect_placeholder_211(rdx2_0, var_47, var_26);
                local_sp_1 = var_48;
                local_sp_4 = var_48;
                if (var_49.field_0 != 0UL) {
                    var_61 = r15_0 + 1UL;
                    local_sp_7 = local_sp_4;
                    local_sp_5_ph = local_sp_4;
                    r15_0 = var_61;
                    r8_0 = r8_1;
                    local_sp_3 = local_sp_4;
                    r8_2_ph = r8_1;
                    if ((long)var_61 < (long)r8_1) {
                        continue;
                    }
                    var_62 = helper_cc_compute_all_wrapper(r8_1, 0UL, 0UL, 25U);
                    if ((uint64_t)(((unsigned char)(var_62 >> 4UL) ^ (unsigned char)var_62) & '\xc0') == 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    r11_0_ph_in_in = local_sp_4 + 24UL;
                    loop_state_var = 0U;
                    break;
                }
                var_50 = *(uint64_t *)(local_sp_1 + 8UL);
                var_51 = var_29 + *var_14;
                var_52 = local_sp_1 + 16UL;
                var_53 = var_51 + 8UL;
                var_54 = var_51 + 16UL;
                var_55 = var_50 + 16UL;
                var_56 = var_50 + 8UL;
                var_57 = local_sp_1 + (-8L);
                *(uint64_t *)var_57 = 4255874UL;
                var_58 = indirect_placeholder_8(var_53, var_55, var_52, var_54, var_56);
                var_59 = (uint32_t)var_58;
                local_sp_4 = var_57;
                if ((uint64_t)var_59 == 0UL) {
                    *(uint32_t *)local_sp_1 = var_59;
                    *(uint64_t *)(local_sp_1 + (-16L)) = 4256198UL;
                    indirect_placeholder();
                    return (uint64_t)*(uint32_t *)var_57;
                }
                var_60 = *var_17;
                r8_1 = var_60;
            }
        }
    while (1U)
        {
            r11_0_ph = *(uint64_t *)r11_0_ph_in_in;
            r9_0 = r9_0_ph;
            local_sp_5 = local_sp_5_ph;
            while (1U)
                {
                    var_63 = *(uint64_t *)((r9_0 << 3UL) + *var_21);
                    var_64 = local_sp_5 + 32UL;
                    var_65 = local_sp_5 + (-8L);
                    *(uint64_t *)var_65 = 4256081UL;
                    var_66 = indirect_placeholder_215(var_63, r11_0_ph, var_64);
                    r11_0_ph_in_in = local_sp_5;
                    local_sp_5 = var_65;
                    local_sp_7 = var_65;
                    if (var_66.field_0 != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_67 = r9_0 + 1UL;
                    r9_0 = var_67;
                    if ((long)var_67 < (long)r8_2_ph) {
                        continue;
                    }
                    loop_state_var = 1U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 0U:
                {
                    var_68 = var_66.field_1;
                    var_69 = *var_25;
                    *(uint64_t *)(local_sp_5 + (-16L)) = 4256140UL;
                    var_70 = indirect_placeholder_210(var_68, var_69, var_26);
                    var_71 = var_70.field_0 + (-1L);
                    var_72 = local_sp_5 + (-24L);
                    *(uint64_t *)var_72 = 4256156UL;
                    indirect_placeholder_17(var_71, var_24, var_26);
                    var_73 = *var_17;
                    var_74 = r9_0 + 1UL;
                    r9_0_ph = var_74;
                    r8_2_ph = var_73;
                    local_sp_5_ph = var_72;
                    local_sp_7 = var_72;
                    if ((long)var_74 >= (long)var_73) {
                        continue;
                    }
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
}
