typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct helper_movq_mm_T0_xmm_wrapper_ret_type;
struct type_3;
struct indirect_placeholder_286_ret_type;
struct indirect_placeholder_287_ret_type;
struct indirect_placeholder_288_ret_type;
struct indirect_placeholder_289_ret_type;
struct helper_movq_mm_T0_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_3 {
};
struct indirect_placeholder_286_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_287_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_288_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_289_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern struct helper_movq_mm_T0_xmm_wrapper_ret_type helper_movq_mm_T0_xmm_wrapper(struct type_3 *param_0, uint64_t param_1);
extern void indirect_placeholder_109(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_286_ret_type indirect_placeholder_286(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_287_ret_type indirect_placeholder_287(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8);
extern struct indirect_placeholder_288_ret_type indirect_placeholder_288(uint64_t param_0);
extern struct indirect_placeholder_289_ret_type indirect_placeholder_289(uint64_t param_0);
typedef _Bool bool;
uint64_t bb_prune_impossible_nodes(uint64_t rdi) {
    struct indirect_placeholder_286_ret_type var_43;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t *var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t r14_2;
    uint64_t local_sp_4;
    uint64_t *var_49;
    uint64_t *_pre107;
    uint64_t var_48;
    uint64_t rbx_2;
    uint64_t *_pre_phi108;
    uint64_t local_sp_0;
    uint64_t local_sp_3;
    uint64_t rbx_0;
    struct helper_movq_mm_T0_xmm_wrapper_ret_type var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_37;
    uint64_t rbx_1_in;
    uint64_t rbx_1;
    uint64_t var_38;
    uint64_t r15_0;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t *var_42;
    uint64_t var_33;
    struct indirect_placeholder_287_ret_type var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t *var_23;
    uint64_t *var_24;
    uint64_t *var_25;
    uint64_t *var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t *var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_20;
    struct indirect_placeholder_288_ret_type var_21;
    uint64_t var_22;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t r14_1;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t *var_17;
    struct indirect_placeholder_289_ret_type var_18;
    uint64_t var_19;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r15();
    var_3 = init_r14();
    var_4 = init_r12();
    var_5 = init_rbx();
    var_6 = init_r13();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_6;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_1;
    *(uint64_t *)(var_0 + (-48L)) = var_5;
    var_7 = var_0 + (-136L);
    var_8 = (uint64_t *)(rdi + 176UL);
    var_9 = *var_8;
    var_10 = *(uint64_t *)(rdi + 152UL);
    var_11 = (uint64_t *)(rdi + 168UL);
    var_12 = *var_11;
    *(uint64_t *)var_7 = var_9;
    var_13 = var_0 + (-128L);
    *(uint64_t *)var_13 = var_10;
    rbx_0 = var_12;
    rbx_2 = var_12;
    r14_1 = 12UL;
    r14_2 = 12UL;
    if (var_12 > 2305843009213693950UL) {
        return (uint64_t)(uint32_t)r14_2;
    }
    var_14 = var_12 + 1UL;
    var_15 = var_14 << 3UL;
    var_16 = var_0 + (-144L);
    var_17 = (uint64_t *)var_16;
    *var_17 = 4270214UL;
    var_18 = indirect_placeholder_289(var_15);
    var_19 = var_18.field_0;
    r15_0 = var_14;
    local_sp_4 = var_16;
    if (var_19 == 0UL) {
        *(uint64_t *)(local_sp_4 + (-8L)) = 4270542UL;
        indirect_placeholder();
        *(uint64_t *)(local_sp_4 + (-16L)) = 4270550UL;
        indirect_placeholder();
        r14_2 = r14_1;
    } else {
        r14_1 = 0UL;
        if (*(uint64_t *)(var_10 + 152UL) == 0UL) {
            var_44 = *var_17;
            *(uint64_t *)(var_0 + (-152L)) = 4270446UL;
            indirect_placeholder_109(var_44, 0UL, var_13, var_12, var_19);
            *(uint64_t *)(var_0 + (-160L)) = 4270459UL;
            var_45 = indirect_placeholder_1(rdi, var_7);
            var_46 = (uint64_t)(uint32_t)var_45;
            var_47 = var_0 + (-168L);
            *(uint64_t *)var_47 = 4270472UL;
            indirect_placeholder();
            local_sp_0 = var_47;
            local_sp_4 = var_47;
            r14_1 = var_46;
            r14_1 = 1UL;
            if (var_46 != 0UL & *(uint64_t *)var_19 != 0UL) {
                _pre107 = (uint64_t *)(rdi + 184UL);
                _pre_phi108 = _pre107;
                var_48 = local_sp_0 + (-8L);
                var_49 = (uint64_t *)var_48;
                *var_49 = 4270508UL;
                indirect_placeholder();
                var_50 = helper_movq_mm_T0_xmm_wrapper((struct type_3 *)(776UL), rbx_0);
                var_51 = var_50.field_0;
                *_pre_phi108 = var_19;
                var_52 = *var_49;
                *var_11 = var_51;
                *var_8 = var_52;
                local_sp_4 = var_48;
            }
        } else {
            var_20 = var_0 + (-152L);
            *(uint64_t *)var_20 = 4270248UL;
            var_21 = indirect_placeholder_288(var_15);
            var_22 = var_21.field_0;
            local_sp_3 = var_20;
            local_sp_4 = var_20;
            r14_1 = 12UL;
            if (var_22 != 0UL) {
                var_23 = (uint64_t *)var_19;
                var_24 = (uint64_t *)var_22;
                var_25 = (uint64_t *)(rdi + 184UL);
                _pre_phi108 = var_25;
                while (1U)
                    {
                        var_26 = (uint64_t *)(local_sp_3 + (-8L));
                        *var_26 = 4270278UL;
                        indirect_placeholder();
                        var_27 = *var_26;
                        var_28 = local_sp_3 + 8UL;
                        *(uint64_t *)(local_sp_3 + (-16L)) = 4270301UL;
                        indirect_placeholder_109(var_27, var_22, var_28, rbx_2, var_19);
                        var_29 = (uint64_t *)(local_sp_3 + (-24L));
                        *var_29 = 4270314UL;
                        var_30 = indirect_placeholder_1(rdi, local_sp_3);
                        var_31 = (uint64_t)(uint32_t)var_30;
                        var_32 = local_sp_3 + (-32L);
                        *(uint64_t *)var_32 = 4270327UL;
                        indirect_placeholder();
                        rbx_0 = rbx_2;
                        rbx_1_in = rbx_2;
                        local_sp_4 = var_32;
                        r14_1 = var_31;
                        if (var_31 != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        r14_1 = 1UL;
                        if (*var_23 != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        if (*var_24 != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        if (rbx_2 != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_37 = *var_25;
                        while (1U)
                            {
                                rbx_1 = rbx_1_in + (-1L);
                                var_38 = *(uint64_t *)((rbx_1 << 3UL) + var_37);
                                rbx_1_in = rbx_1;
                                r15_0 = rbx_1_in;
                                rbx_2 = rbx_1;
                                if (var_38 != 0UL) {
                                    if ((*(unsigned char *)(var_38 + 104UL) & '\x10') != '\x00') {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                }
                                if (rbx_1 == 0UL) {
                                    continue;
                                }
                                loop_state_var = 1U;
                                break;
                            }
                        switch_state_var = 0;
                        switch (loop_state_var) {
                          case 0U:
                            {
                                var_39 = *(uint64_t *)(var_38 + 16UL);
                                var_40 = var_38 + 24UL;
                                var_41 = local_sp_3 + (-40L);
                                var_42 = (uint64_t *)var_41;
                                *var_42 = 4270599UL;
                                var_43 = indirect_placeholder_286(var_38, rbx_1, var_40, rdi, var_39);
                                *var_42 = var_43.field_0;
                                local_sp_3 = var_41;
                                continue;
                            }
                            break;
                          case 1U:
                            {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            break;
                        }
                        if (switch_state_var)
                            break;
                    }
                var_33 = *var_29;
                *(uint64_t *)(local_sp_3 + (-40L)) = 4270654UL;
                var_34 = indirect_placeholder_287(r15_0, rdi, 0UL, var_19, rbx_2, var_22, var_33, var_22, var_19);
                var_35 = (uint64_t)(uint32_t)var_34.field_0;
                var_36 = local_sp_3 + (-48L);
                *(uint64_t *)var_36 = 4270665UL;
                indirect_placeholder();
                local_sp_0 = var_36;
                local_sp_4 = var_36;
                r14_1 = var_35;
                if (var_35 == 0UL) {
                    var_48 = local_sp_0 + (-8L);
                    var_49 = (uint64_t *)var_48;
                    *var_49 = 4270508UL;
                    indirect_placeholder();
                    var_50 = helper_movq_mm_T0_xmm_wrapper((struct type_3 *)(776UL), rbx_0);
                    var_51 = var_50.field_0;
                    *_pre_phi108 = var_19;
                    var_52 = *var_49;
                    *var_11 = var_51;
                    *var_8 = var_52;
                    local_sp_4 = var_48;
                }
            }
        }
    }
}
