typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_pcmpeql_xmm_wrapper_ret_type;
struct type_4;
struct type_6;
struct indirect_placeholder_325_ret_type;
struct indirect_placeholder_324_ret_type;
struct helper_pcmpeql_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_4 {
};
struct type_6 {
};
struct indirect_placeholder_325_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_324_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t init_state_0x8558(void);
extern uint64_t init_state_0x8560(void);
extern uint64_t indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
extern struct helper_pcmpeql_xmm_wrapper_ret_type helper_pcmpeql_xmm_wrapper(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4);
extern void indirect_placeholder_11(uint64_t param_0);
extern void indirect_placeholder_16(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_325_ret_type indirect_placeholder_325(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_324_ret_type indirect_placeholder_324(uint64_t param_0, uint64_t param_1);
uint64_t bb_group_nodes_into_DFAstates(uint64_t rcx, uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t *_pre306;
    uint64_t *_pre_phi307;
    uint64_t var_109;
    struct helper_pcmpeql_xmm_wrapper_ret_type var_25;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t r12_9;
    uint64_t state_0x8560_5;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t local_sp_7;
    uint64_t r12_7;
    uint64_t var_172;
    uint64_t local_sp_8;
    uint64_t local_sp_15;
    uint64_t local_sp_13;
    uint64_t local_sp_12;
    uint64_t local_sp_0;
    uint64_t r12_0;
    uint64_t local_sp_1;
    uint64_t rbp_0;
    uint64_t var_170;
    uint64_t var_171;
    uint64_t local_sp_4;
    uint64_t state_0x8558_1;
    uint64_t state_0x8560_1;
    uint64_t local_sp_2;
    uint64_t var_36;
    uint64_t _pre;
    uint64_t local_sp_5;
    uint64_t local_sp_16;
    uint64_t var_11;
    uint64_t r12_8;
    uint64_t local_sp_3;
    uint64_t state_0x8558_4;
    uint64_t r12_1;
    uint64_t state_0x8560_4;
    uint64_t state_0x8558_0;
    uint64_t state_0x8560_0;
    uint64_t var_12;
    uint64_t *var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint32_t var_18;
    uint64_t var_19;
    uint32_t var_20;
    uint64_t var_21;
    uint32_t var_22;
    bool var_23;
    uint64_t var_24;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_37;
    uint64_t _pre_phi;
    uint64_t local_sp_6;
    uint64_t local_sp_17;
    uint64_t rax_0;
    uint64_t state_0x8558_2;
    uint64_t state_0x8560_2;
    uint64_t state_0x8558_3;
    uint64_t state_0x8560_3;
    uint64_t var_103;
    uint64_t local_sp_9;
    uint64_t var_52;
    uint64_t *var_53;
    uint64_t var_54;
    uint64_t *var_55;
    uint64_t var_56;
    uint64_t *var_57;
    uint64_t var_58;
    uint32_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t *var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t rbx_1;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_79;
    uint64_t *var_80;
    uint64_t var_81;
    uint64_t *var_82;
    uint64_t var_83;
    uint64_t *var_84;
    uint64_t var_85;
    uint32_t var_86;
    uint64_t *var_87;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_102;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t var_107;
    uint64_t var_108;
    uint64_t rax_1;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t var_100;
    uint64_t var_101;
    uint64_t r12_2;
    uint64_t var_173;
    uint64_t var_174;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t r12_5;
    uint64_t local_sp_10;
    uint64_t rbp_1;
    uint64_t r15_0;
    uint64_t r14_0;
    uint64_t r14_1;
    uint64_t r12_3;
    uint64_t local_sp_11;
    uint64_t *var_115;
    uint64_t var_116;
    uint64_t *var_117;
    uint64_t var_118;
    uint64_t *var_119;
    uint64_t var_120;
    uint64_t var_121;
    uint64_t *var_122;
    uint64_t var_123;
    uint64_t var_124;
    uint64_t *var_125;
    uint64_t var_126;
    uint64_t *var_127;
    uint64_t var_128;
    uint64_t var_129;
    uint64_t var_130;
    uint64_t *var_131;
    uint64_t var_132;
    uint64_t var_133;
    uint64_t *var_134;
    uint64_t var_135;
    uint64_t var_136;
    uint64_t var_137;
    uint64_t var_138;
    uint64_t var_139;
    uint64_t var_140;
    uint64_t var_141;
    uint64_t var_142;
    uint64_t var_143;
    uint64_t var_144;
    uint64_t var_145;
    uint64_t var_146;
    uint64_t var_147;
    uint64_t var_148;
    uint64_t var_149;
    uint64_t var_150;
    uint64_t var_151;
    uint64_t var_152;
    uint64_t var_153;
    uint64_t var_154;
    uint64_t var_155;
    uint64_t var_156;
    uint64_t var_157;
    uint64_t var_158;
    uint64_t var_159;
    uint64_t r12_4;
    uint64_t var_160;
    uint64_t var_161;
    uint64_t var_162;
    uint64_t var_163;
    uint64_t var_112;
    uint64_t var_113;
    struct indirect_placeholder_325_ret_type var_114;
    uint64_t var_164;
    uint64_t var_165;
    uint64_t var_166;
    uint64_t var_167;
    uint64_t var_168;
    struct indirect_placeholder_324_ret_type var_169;
    uint64_t var_175;
    uint64_t *var_176;
    uint64_t var_177;
    uint64_t state_0x8558_5;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_30;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r15();
    var_3 = init_r14();
    var_4 = init_r12();
    var_5 = init_rbx();
    var_6 = init_r13();
    var_7 = init_state_0x8558();
    var_8 = init_state_0x8560();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_6;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_1;
    *(uint64_t *)(var_0 + (-48L)) = var_5;
    *(uint64_t *)(var_0 + (-192L)) = rdi;
    var_9 = var_0 + (-152L);
    *(uint64_t *)(var_0 + (-200L)) = rsi;
    *(uint64_t *)(var_0 + (-168L)) = rdx;
    *(uint64_t *)(var_0 + (-160L)) = rcx;
    var_10 = var_0 + (-224L);
    *(uint64_t *)var_10 = 4279986UL;
    indirect_placeholder_11(var_9);
    r12_9 = 0UL;
    rbp_0 = 0UL;
    var_11 = 0UL;
    local_sp_3 = var_10;
    r12_1 = 0UL;
    state_0x8558_0 = var_7;
    state_0x8560_0 = var_8;
    r15_0 = 0UL;
    r14_1 = 0UL;
    if ((long)*(uint64_t *)(rsi + 16UL) > (long)0UL) {
        return r12_9;
    }
    *(uint64_t *)(var_0 + (-216L)) = 0UL;
    r12_9 = 18446744073709551615UL;
    while (1U)
        {
            var_12 = var_11 << 3UL;
            *(uint64_t *)(local_sp_3 + 32UL) = var_12;
            var_13 = (uint64_t *)(local_sp_3 + 16UL);
            var_14 = *(uint64_t *)(var_12 + *(uint64_t *)(*var_13 + 24UL));
            var_15 = local_sp_3 + 24UL;
            var_16 = (var_14 << 4UL) + **(uint64_t **)var_15;
            var_17 = var_16 + 8UL;
            var_18 = *(uint32_t *)var_17;
            var_19 = (uint64_t)*(unsigned char *)var_17;
            *(uint64_t *)local_sp_3 = var_16;
            *(uint32_t *)(local_sp_3 + 44UL) = var_18;
            var_20 = var_18 >> 8U;
            var_21 = (uint64_t)(uint32_t)((uint16_t)var_20 & (unsigned short)1023U);
            var_22 = (uint32_t)var_19;
            var_23 = ((uint64_t)(var_22 + (-1)) == 0UL);
            _pre_phi307 = var_13;
            r12_7 = r12_1;
            local_sp_4 = local_sp_3;
            state_0x8558_1 = state_0x8558_0;
            state_0x8560_1 = state_0x8560_0;
            local_sp_5 = local_sp_3;
            local_sp_16 = local_sp_3;
            r12_8 = r12_1;
            state_0x8558_4 = state_0x8558_0;
            state_0x8560_4 = state_0x8560_0;
            state_0x8558_2 = state_0x8558_0;
            state_0x8560_2 = state_0x8560_0;
            state_0x8558_3 = state_0x8558_0;
            state_0x8560_3 = state_0x8560_0;
            r12_2 = r12_1;
            r12_3 = r12_1;
            if (var_23) {
                var_43 = (uint64_t)*(unsigned char *)var_16;
                var_44 = local_sp_3 + 64UL;
                var_45 = local_sp_3 + (-8L);
                *(uint64_t *)var_45 = 4281150UL;
                indirect_placeholder_1(var_44, var_43);
                local_sp_6 = var_45;
            } else {
                if ((uint64_t)(var_22 + (-3)) == 0UL) {
                    var_40 = local_sp_3 + 64UL;
                    var_41 = **(uint64_t **)local_sp_3;
                    var_42 = local_sp_3 + (-8L);
                    *(uint64_t *)var_42 = 4281297UL;
                    indirect_placeholder_16(var_40, var_41);
                    local_sp_6 = var_42;
                } else {
                    if ((uint64_t)(var_22 + (-5)) == 0UL) {
                        var_30 = *(uint64_t *)var_15;
                        if ((int)*(uint32_t *)(var_30 + 180UL) > (int)1U) {
                            var_33 = *(uint64_t *)(var_30 + 120UL);
                            var_34 = local_sp_3 + 64UL;
                            var_35 = local_sp_3 + (-8L);
                            *(uint64_t *)var_35 = 4280200UL;
                            indirect_placeholder_16(var_34, var_33);
                            local_sp_2 = var_35;
                        } else {
                            var_31 = local_sp_3 + 64UL;
                            var_32 = local_sp_3 + (-8L);
                            *(uint64_t *)var_32 = 4281439UL;
                            indirect_placeholder_11(var_31);
                            local_sp_2 = var_32;
                        }
                        var_36 = *(uint64_t *)(*(uint64_t *)(local_sp_2 + 24UL) + 216UL);
                        local_sp_4 = local_sp_2;
                        local_sp_5 = local_sp_2;
                        rax_0 = var_36;
                        if ((var_36 & 64UL) == 0UL) {
                            _pre = local_sp_2 + 64UL;
                            _pre306 = (uint64_t *)(local_sp_2 + 16UL);
                            _pre_phi307 = _pre306;
                            _pre_phi = _pre;
                            var_37 = local_sp_4 + (-8L);
                            *(uint64_t *)var_37 = 4281346UL;
                            indirect_placeholder_16(_pre_phi, 10UL);
                            state_0x8560_5 = state_0x8560_1;
                            local_sp_6 = var_37;
                            local_sp_17 = var_37;
                            state_0x8558_3 = state_0x8558_1;
                            state_0x8560_3 = state_0x8560_1;
                            state_0x8558_5 = state_0x8558_1;
                            if ((signed char)(unsigned char)*(uint64_t *)(*_pre_phi307 + 216UL) > '\xff') {
                                var_38 = local_sp_17 + 64UL;
                                var_39 = local_sp_17 + (-8L);
                                *(uint64_t *)var_39 = 4281378UL;
                                indirect_placeholder_16(var_38, 0UL);
                                local_sp_6 = var_39;
                                state_0x8558_3 = state_0x8558_5;
                                state_0x8560_3 = state_0x8560_5;
                            }
                        } else {
                            state_0x8560_5 = state_0x8560_2;
                            local_sp_6 = local_sp_5;
                            local_sp_17 = local_sp_5;
                            state_0x8558_3 = state_0x8558_2;
                            state_0x8560_3 = state_0x8560_2;
                            state_0x8558_5 = state_0x8558_2;
                            if ((signed char)(unsigned char)rax_0 > '\xff') {
                                var_38 = local_sp_17 + 64UL;
                                var_39 = local_sp_17 + (-8L);
                                *(uint64_t *)var_39 = 4281378UL;
                                indirect_placeholder_16(var_38, 0UL);
                                local_sp_6 = var_39;
                                state_0x8558_3 = state_0x8558_5;
                                state_0x8560_3 = state_0x8560_5;
                            }
                        }
                    } else {
                        if ((uint64_t)(var_22 + (-7)) != 0UL) {
                            var_175 = *(uint64_t *)(local_sp_16 + 16UL);
                            var_176 = (uint64_t *)(local_sp_16 + 8UL);
                            var_177 = *var_176 + 1UL;
                            *var_176 = var_177;
                            r12_9 = r12_8;
                            var_11 = var_177;
                            local_sp_3 = local_sp_16;
                            r12_1 = r12_8;
                            state_0x8558_0 = state_0x8558_4;
                            state_0x8560_0 = state_0x8560_4;
                            if ((long)*(uint64_t *)(var_175 + 16UL) > (long)var_177) {
                                continue;
                            }
                            loop_state_var = 0U;
                            break;
                        }
                        var_24 = *(uint64_t *)var_15;
                        var_25 = helper_pcmpeql_xmm_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(776UL), state_0x8558_0, state_0x8560_0);
                        var_26 = var_25.field_0;
                        var_27 = var_25.field_1;
                        var_28 = local_sp_3 + 64UL;
                        *(uint64_t *)var_28 = var_26;
                        *(uint64_t *)(local_sp_3 + 72UL) = var_27;
                        var_29 = *(uint64_t *)(var_24 + 216UL);
                        state_0x8558_1 = var_26;
                        state_0x8560_1 = var_27;
                        _pre_phi = var_28;
                        rax_0 = var_29;
                        state_0x8558_2 = var_26;
                        state_0x8560_2 = var_27;
                        if ((var_29 & 64UL) == 0UL) {
                            state_0x8560_5 = state_0x8560_2;
                            local_sp_6 = local_sp_5;
                            local_sp_17 = local_sp_5;
                            state_0x8558_3 = state_0x8558_2;
                            state_0x8560_3 = state_0x8560_2;
                            state_0x8558_5 = state_0x8558_2;
                            if ((signed char)(unsigned char)rax_0 > '\xff') {
                                var_38 = local_sp_17 + 64UL;
                                var_39 = local_sp_17 + (-8L);
                                *(uint64_t *)var_39 = 4281378UL;
                                indirect_placeholder_16(var_38, 0UL);
                                local_sp_6 = var_39;
                                state_0x8558_3 = state_0x8558_5;
                                state_0x8560_3 = state_0x8560_5;
                            }
                        } else {
                            var_37 = local_sp_4 + (-8L);
                            *(uint64_t *)var_37 = 4281346UL;
                            indirect_placeholder_16(_pre_phi, 10UL);
                            state_0x8560_5 = state_0x8560_1;
                            local_sp_6 = var_37;
                            local_sp_17 = var_37;
                            state_0x8558_3 = state_0x8558_1;
                            state_0x8560_3 = state_0x8560_1;
                            state_0x8558_5 = state_0x8558_1;
                            if ((signed char)(unsigned char)*(uint64_t *)(*_pre_phi307 + 216UL) > '\xff') {
                                var_38 = local_sp_17 + 64UL;
                                var_39 = local_sp_17 + (-8L);
                                *(uint64_t *)var_39 = 4281378UL;
                                indirect_placeholder_16(var_38, 0UL);
                                local_sp_6 = var_39;
                                state_0x8558_3 = state_0x8558_5;
                                state_0x8560_3 = state_0x8560_5;
                            }
                        }
                    }
                }
            }
            local_sp_7 = local_sp_6;
            local_sp_9 = local_sp_6;
            state_0x8558_4 = state_0x8558_3;
            state_0x8560_4 = state_0x8560_3;
            if ((uint64_t)((uint16_t)var_21 & (unsigned short)1023U) == 0UL) {
                var_109 = *(uint64_t *)(local_sp_9 + 56UL);
                var_110 = *(uint64_t *)(local_sp_9 + 48UL);
                var_111 = helper_cc_compute_all_wrapper(r12_1, 0UL, 0UL, 25U);
                local_sp_10 = local_sp_9;
                rbp_1 = var_109;
                r14_0 = var_110;
                local_sp_15 = local_sp_9;
                if ((uint64_t)(((unsigned char)(var_111 >> 4UL) ^ (unsigned char)var_111) & '\xc0') == 0UL) {
                    r12_0 = r14_1;
                    local_sp_16 = local_sp_15;
                    r12_8 = r12_7;
                    if (r12_7 != r14_1) {
                        var_164 = local_sp_15 + 64UL;
                        var_165 = (r14_1 << 5UL) + *(uint64_t *)(local_sp_15 + 56UL);
                        *(uint64_t *)(local_sp_15 + (-8L)) = 4281073UL;
                        indirect_placeholder_16(var_165, var_164);
                        var_166 = *(uint64_t *)(*(uint64_t *)(local_sp_15 + 24UL) + *(uint64_t *)(*(uint64_t *)(local_sp_15 + 8UL) + 24UL));
                        var_167 = (r14_1 * 24UL) + *(uint64_t *)(local_sp_15 + 40UL);
                        var_168 = local_sp_15 + (-16L);
                        *(uint64_t *)var_168 = 4281109UL;
                        var_169 = indirect_placeholder_324(var_167, var_166);
                        local_sp_0 = var_168;
                        local_sp_8 = var_168;
                        if ((uint64_t)(uint32_t)var_169.field_0 != 0UL) {
                            if (r14_1 != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            loop_state_var = 0U;
                            break;
                        }
                        var_172 = r14_1 + 1UL;
                        r12_2 = var_172;
                        var_173 = local_sp_8 + 64UL;
                        var_174 = local_sp_8 + (-8L);
                        *(uint64_t *)var_174 = 4281127UL;
                        indirect_placeholder_11(var_173);
                        local_sp_16 = var_174;
                        r12_8 = r12_2;
                    }
                    var_175 = *(uint64_t *)(local_sp_16 + 16UL);
                    var_176 = (uint64_t *)(local_sp_16 + 8UL);
                    var_177 = *var_176 + 1UL;
                    *var_176 = var_177;
                    r12_9 = r12_8;
                    var_11 = var_177;
                    local_sp_3 = local_sp_16;
                    r12_1 = r12_8;
                    state_0x8558_0 = state_0x8558_4;
                    state_0x8560_0 = state_0x8560_4;
                    if ((long)*(uint64_t *)(var_175 + 16UL) > (long)var_177) {
                        continue;
                    }
                    loop_state_var = 0U;
                    break;
                }
                while (1U)
                    {
                        r12_0 = r12_3;
                        r12_5 = r12_3;
                        r14_1 = r15_0;
                        local_sp_11 = local_sp_10;
                        r12_4 = r12_3;
                        if (!var_23) {
                            var_112 = (uint64_t)**(unsigned char **)local_sp_10;
                            var_113 = local_sp_10 + (-8L);
                            *(uint64_t *)var_113 = 4280655UL;
                            var_114 = indirect_placeholder_325(rbp_1, var_112);
                            local_sp_11 = var_113;
                            local_sp_13 = var_113;
                            if ((uint64_t)(unsigned char)var_114.field_0 != 0UL) {
                                var_163 = r15_0 + 1UL;
                                r12_7 = r12_5;
                                local_sp_15 = local_sp_13;
                                local_sp_10 = local_sp_13;
                                r15_0 = var_163;
                                r14_1 = var_163;
                                r12_3 = r12_5;
                                if ((long)r12_5 <= (long)var_163) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                rbp_1 = rbp_1 + 32UL;
                                r14_0 = r14_0 + 24UL;
                                continue;
                            }
                        }
                        var_115 = (uint64_t *)rbp_1;
                        var_116 = *var_115;
                        var_117 = (uint64_t *)(local_sp_11 + 64UL);
                        var_118 = *var_117;
                        var_119 = (uint64_t *)(local_sp_11 + 72UL);
                        var_120 = *var_119;
                        var_121 = var_116 & var_118;
                        *(uint64_t *)(local_sp_11 + 96UL) = var_121;
                        var_122 = (uint64_t *)(rbp_1 + 8UL);
                        var_123 = *var_122 & var_120;
                        *(uint64_t *)(local_sp_11 + 104UL) = var_123;
                        var_124 = var_121 | var_123;
                        var_125 = (uint64_t *)(rbp_1 + 16UL);
                        var_126 = *var_125;
                        var_127 = (uint64_t *)(local_sp_11 + 80UL);
                        var_128 = *var_127;
                        var_129 = var_126 & var_128;
                        *(uint64_t *)(local_sp_11 + 112UL) = var_129;
                        var_130 = var_124 | var_129;
                        var_131 = (uint64_t *)(rbp_1 + 24UL);
                        var_132 = *var_131;
                        var_133 = local_sp_11 + 88UL;
                        var_134 = (uint64_t *)var_133;
                        var_135 = *var_134;
                        var_136 = var_132 & var_135;
                        var_137 = var_130 | var_136;
                        *(uint64_t *)(local_sp_11 + 120UL) = var_136;
                        local_sp_12 = local_sp_11;
                        local_sp_13 = local_sp_11;
                        if (var_137 == 0UL) {
                            var_163 = r15_0 + 1UL;
                            r12_7 = r12_5;
                            local_sp_15 = local_sp_13;
                            local_sp_10 = local_sp_13;
                            r15_0 = var_163;
                            r14_1 = var_163;
                            r12_3 = r12_5;
                            if ((long)r12_5 <= (long)var_163) {
                                loop_state_var = 0U;
                                break;
                            }
                            rbp_1 = rbp_1 + 32UL;
                            r14_0 = r14_0 + 24UL;
                            continue;
                        }
                        var_138 = *var_115 & (var_118 ^ (-1L));
                        var_139 = var_120 ^ (-1L);
                        var_140 = local_sp_11 + 128UL;
                        *(uint64_t *)var_140 = var_138;
                        var_141 = var_118 & (*var_115 ^ (-1L));
                        *var_117 = var_141;
                        var_142 = *var_122 & var_139;
                        *(uint64_t *)(local_sp_11 + 136UL) = var_142;
                        var_143 = var_138 | var_142;
                        var_144 = var_120 & (*var_122 ^ (-1L));
                        *var_119 = var_144;
                        var_145 = var_144 | var_141;
                        var_146 = *var_125 & (var_128 ^ (-1L));
                        *(uint64_t *)(local_sp_11 + 144UL) = var_146;
                        var_147 = var_143 | var_146;
                        var_148 = var_128 & (*var_125 ^ (-1L));
                        *var_127 = var_148;
                        var_149 = var_148 | var_145;
                        var_150 = *var_131 & (var_135 ^ (-1L));
                        *(uint64_t *)(local_sp_11 + 152UL) = var_150;
                        var_151 = var_135 & (*var_131 ^ (-1L));
                        var_152 = var_149 | var_151;
                        var_153 = var_147 | var_150;
                        *var_134 = var_151;
                        if (var_153 != 0UL) {
                            var_154 = (r12_3 << 5UL) + *(uint64_t *)(local_sp_11 + 56UL);
                            *(uint64_t *)(local_sp_11 + (-8L)) = 4280945UL;
                            indirect_placeholder_16(var_154, var_140);
                            *(uint64_t *)(local_sp_11 + (-16L)) = 4280958UL;
                            indirect_placeholder_16(rbp_1, var_133);
                            var_155 = r14_0 + 16UL;
                            var_156 = r14_0 + 8UL;
                            var_157 = (r12_3 * 24UL) + *(uint64_t *)(local_sp_11 + 32UL);
                            var_158 = local_sp_11 + (-24L);
                            *(uint64_t *)var_158 = 4280984UL;
                            var_159 = indirect_placeholder_10(var_155, var_157, var_156);
                            local_sp_0 = var_158;
                            local_sp_12 = var_158;
                            if ((uint64_t)(uint32_t)var_159 != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            r12_4 = r12_3 + 1UL;
                        }
                        var_160 = *(uint64_t *)(*(uint64_t *)(local_sp_12 + 32UL) + *(uint64_t *)(*(uint64_t *)(local_sp_12 + 16UL) + 24UL));
                        var_161 = local_sp_12 + (-8L);
                        *(uint64_t *)var_161 = 4281022UL;
                        var_162 = indirect_placeholder_1(r14_0, var_160);
                        r12_7 = r12_4;
                        local_sp_15 = var_161;
                        local_sp_13 = var_161;
                        local_sp_0 = var_161;
                        r12_0 = r12_4;
                        r12_5 = r12_4;
                        if ((uint64_t)(unsigned char)var_162 != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        if (var_152 != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 0U:
                    {
                        break;
                    }
                    break;
                  case 1U:
                    {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
            if ((var_21 & 32UL) != 0UL) {
                var_46 = local_sp_6 + 64UL;
                *(uint64_t *)(local_sp_6 + (-8L)) = 4281234UL;
                var_47 = indirect_placeholder_2(var_46);
                var_48 = local_sp_6 + 56UL;
                var_49 = local_sp_6 + (-16L);
                *(uint64_t *)var_49 = 4281244UL;
                indirect_placeholder_11(var_48);
                local_sp_16 = var_49;
                if ((uint64_t)(unsigned char)var_47 != 0UL) {
                    var_175 = *(uint64_t *)(local_sp_16 + 16UL);
                    var_176 = (uint64_t *)(local_sp_16 + 8UL);
                    var_177 = *var_176 + 1UL;
                    *var_176 = var_177;
                    r12_9 = r12_8;
                    var_11 = var_177;
                    local_sp_3 = local_sp_16;
                    r12_1 = r12_8;
                    state_0x8558_0 = state_0x8558_4;
                    state_0x8560_0 = state_0x8560_4;
                    if ((long)*(uint64_t *)(var_175 + 16UL) > (long)var_177) {
                        continue;
                    }
                    loop_state_var = 0U;
                    break;
                }
                var_50 = local_sp_6 + 48UL;
                var_51 = local_sp_6 + (-24L);
                *(uint64_t *)var_51 = 4281267UL;
                indirect_placeholder_1(var_50, 10UL);
                local_sp_7 = var_51;
            }
            local_sp_8 = local_sp_7;
            local_sp_9 = local_sp_7;
            local_sp_16 = local_sp_7;
            if ((signed char)(unsigned char)var_20 <= '\xff') {
                var_173 = local_sp_8 + 64UL;
                var_174 = local_sp_8 + (-8L);
                *(uint64_t *)var_174 = 4281127UL;
                indirect_placeholder_11(var_173);
                local_sp_16 = var_174;
                r12_8 = r12_2;
                var_175 = *(uint64_t *)(local_sp_16 + 16UL);
                var_176 = (uint64_t *)(local_sp_16 + 8UL);
                var_177 = *var_176 + 1UL;
                *var_176 = var_177;
                r12_9 = r12_8;
                var_11 = var_177;
                local_sp_3 = local_sp_16;
                r12_1 = r12_8;
                state_0x8558_0 = state_0x8558_4;
                state_0x8560_0 = state_0x8560_4;
                if ((long)*(uint64_t *)(var_175 + 16UL) > (long)var_177) {
                    continue;
                }
                loop_state_var = 0U;
                break;
            }
            if ((var_21 & 4UL) != 0UL) {
                if (!var_23) {
                    if ((*(unsigned char *)(*(uint64_t *)local_sp_7 + 10UL) & '@') != '\x00') {
                        var_173 = local_sp_8 + 64UL;
                        var_174 = local_sp_8 + (-8L);
                        *(uint64_t *)var_174 = 4281127UL;
                        indirect_placeholder_11(var_173);
                        local_sp_16 = var_174;
                        r12_8 = r12_2;
                        var_175 = *(uint64_t *)(local_sp_16 + 16UL);
                        var_176 = (uint64_t *)(local_sp_16 + 8UL);
                        var_177 = *var_176 + 1UL;
                        *var_176 = var_177;
                        r12_9 = r12_8;
                        var_11 = var_177;
                        local_sp_3 = local_sp_16;
                        r12_1 = r12_8;
                        state_0x8558_0 = state_0x8558_4;
                        state_0x8560_0 = state_0x8560_4;
                        if ((long)*(uint64_t *)(var_175 + 16UL) > (long)var_177) {
                            continue;
                        }
                        loop_state_var = 0U;
                        break;
                    }
                }
                var_52 = *(uint64_t *)(local_sp_7 + 24UL);
                var_53 = (uint64_t *)(local_sp_7 + 64UL);
                var_54 = *var_53;
                var_55 = (uint64_t *)(local_sp_7 + 72UL);
                var_56 = *var_55;
                var_57 = (uint64_t *)(local_sp_7 + 80UL);
                var_58 = *var_57;
                var_59 = *(uint32_t *)(var_52 + 180UL);
                var_60 = *(uint64_t *)(var_52 + 184UL);
                var_61 = *(uint64_t *)(var_52 + 192UL);
                var_62 = *(uint64_t *)(var_52 + 200UL);
                var_63 = (uint64_t *)(local_sp_7 + 88UL);
                var_64 = *var_63;
                var_65 = *(uint64_t *)(var_52 + 208UL);
                if ((int)var_59 > (int)1U) {
                    var_72 = *(uint64_t *)(var_52 + 120UL);
                    var_73 = (var_60 | (*(uint64_t *)var_72 ^ (-1L))) & var_54;
                    *var_53 = var_73;
                    var_74 = var_56 & (var_61 | (*(uint64_t *)(var_72 + 8UL) ^ (-1L)));
                    *var_55 = var_74;
                    var_75 = var_73 | var_74;
                    var_76 = var_58 & (var_62 | (*(uint64_t *)(var_72 + 16UL) ^ (-1L)));
                    *var_57 = var_76;
                    var_77 = var_75 | var_76;
                    var_78 = (var_65 | (*(uint64_t *)(var_72 + 24UL) ^ (-1L))) & var_64;
                    *var_63 = var_78;
                    rbx_1 = var_78 | var_77;
                } else {
                    var_66 = var_54 & var_60;
                    var_67 = var_56 & var_61;
                    var_68 = var_58 & var_62;
                    var_69 = var_65 & var_64;
                    *var_53 = var_66;
                    var_70 = (var_66 | var_67) | var_68;
                    *var_63 = var_69;
                    *var_55 = var_67;
                    var_71 = var_69 | var_70;
                    *var_57 = var_68;
                    rbx_1 = var_71;
                }
                if (rbx_1 != 0UL) {
                    var_175 = *(uint64_t *)(local_sp_16 + 16UL);
                    var_176 = (uint64_t *)(local_sp_16 + 8UL);
                    var_177 = *var_176 + 1UL;
                    *var_176 = var_177;
                    r12_9 = r12_8;
                    var_11 = var_177;
                    local_sp_3 = local_sp_16;
                    r12_1 = r12_8;
                    state_0x8558_0 = state_0x8558_4;
                    state_0x8560_0 = state_0x8560_4;
                    if ((long)*(uint64_t *)(var_175 + 16UL) > (long)var_177) {
                        continue;
                    }
                    loop_state_var = 0U;
                    break;
                }
            }
            if ((var_21 & 8UL) != 0UL) {
                if (!var_23) {
                    if ((*(unsigned char *)(*(uint64_t *)local_sp_7 + 10UL) & '@') != '\x00') {
                        var_173 = local_sp_8 + 64UL;
                        var_174 = local_sp_8 + (-8L);
                        *(uint64_t *)var_174 = 4281127UL;
                        indirect_placeholder_11(var_173);
                        local_sp_16 = var_174;
                        r12_8 = r12_2;
                        var_175 = *(uint64_t *)(local_sp_16 + 16UL);
                        var_176 = (uint64_t *)(local_sp_16 + 8UL);
                        var_177 = *var_176 + 1UL;
                        *var_176 = var_177;
                        r12_9 = r12_8;
                        var_11 = var_177;
                        local_sp_3 = local_sp_16;
                        r12_1 = r12_8;
                        state_0x8558_0 = state_0x8558_4;
                        state_0x8560_0 = state_0x8560_4;
                        if ((long)*(uint64_t *)(var_175 + 16UL) > (long)var_177) {
                            continue;
                        }
                        loop_state_var = 0U;
                        break;
                    }
                }
                var_79 = *(uint64_t *)(local_sp_7 + 24UL);
                var_80 = (uint64_t *)(local_sp_7 + 64UL);
                var_81 = *var_80;
                var_82 = (uint64_t *)(local_sp_7 + 72UL);
                var_83 = *var_82;
                var_84 = (uint64_t *)(local_sp_7 + 80UL);
                var_85 = *var_84;
                var_86 = *(uint32_t *)(var_79 + 180UL);
                var_87 = (uint64_t *)(local_sp_7 + 88UL);
                var_88 = *var_87;
                var_89 = *(uint64_t *)(var_79 + 184UL);
                var_90 = *(uint64_t *)(var_79 + 192UL);
                var_91 = *(uint64_t *)(var_79 + 200UL);
                var_92 = *(uint64_t *)(var_79 + 208UL);
                if ((int)var_86 > (int)1U) {
                    var_102 = *(uint64_t *)(var_79 + 120UL);
                    var_103 = var_81 & ((var_89 & *(uint64_t *)var_102) ^ (-1L));
                    *var_80 = var_103;
                    var_104 = var_83 & ((var_90 & *(uint64_t *)(var_102 + 8UL)) ^ (-1L));
                    *var_82 = var_104;
                    var_105 = var_103 | var_104;
                    var_106 = var_85 & ((var_91 & *(uint64_t *)(var_102 + 16UL)) ^ (-1L));
                    *var_84 = var_106;
                    var_107 = var_105 | var_106;
                    var_108 = var_88 & ((var_92 & *(uint64_t *)(var_102 + 24UL)) ^ (-1L));
                    *var_87 = var_108;
                    rax_1 = var_107 | var_108;
                } else {
                    var_93 = var_90 ^ (-1L);
                    var_94 = var_91 ^ (-1L);
                    var_95 = var_92 ^ (-1L);
                    var_96 = var_81 & (var_89 ^ (-1L));
                    var_97 = var_83 & var_93;
                    var_98 = var_85 & var_94;
                    var_99 = var_88 & var_95;
                    *var_80 = var_96;
                    var_100 = (var_96 | var_97) | var_98;
                    *var_82 = var_97;
                    *var_84 = var_98;
                    var_101 = var_100 | var_99;
                    *var_87 = var_99;
                    rax_1 = var_101;
                }
                if (rax_1 != 0UL) {
                    var_175 = *(uint64_t *)(local_sp_16 + 16UL);
                    var_176 = (uint64_t *)(local_sp_16 + 8UL);
                    var_177 = *var_176 + 1UL;
                    *var_176 = var_177;
                    r12_9 = r12_8;
                    var_11 = var_177;
                    local_sp_3 = local_sp_16;
                    r12_1 = r12_8;
                    state_0x8558_0 = state_0x8558_4;
                    state_0x8560_0 = state_0x8560_4;
                    if ((long)*(uint64_t *)(var_175 + 16UL) > (long)var_177) {
                        continue;
                    }
                    loop_state_var = 0U;
                    break;
                }
            }
        }
    local_sp_1 = local_sp_0;
    var_170 = rbp_0 + 1UL;
    var_171 = local_sp_1 + (-8L);
    *(uint64_t *)var_171 = 4281200UL;
    indirect_placeholder();
    local_sp_1 = var_171;
    rbp_0 = var_170;
    do {
        var_170 = rbp_0 + 1UL;
        var_171 = local_sp_1 + (-8L);
        *(uint64_t *)var_171 = 4281200UL;
        indirect_placeholder();
        local_sp_1 = var_171;
        rbp_0 = var_170;
    } while ((long)r12_0 <= (long)var_170);
}
