typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_pxor_xmm_wrapper_337_ret_type;
struct type_3;
struct type_5;
struct indirect_placeholder_225_ret_type;
struct indirect_placeholder_226_ret_type;
struct indirect_placeholder_227_ret_type;
struct indirect_placeholder_228_ret_type;
struct helper_pxor_xmm_wrapper_337_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_3 {
};
struct type_5 {
};
struct indirect_placeholder_225_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_226_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_227_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_228_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct helper_pxor_xmm_wrapper_337_ret_type helper_pxor_xmm_wrapper_337(struct type_3 *param_0, struct type_5 *param_1, struct type_5 *param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_225_ret_type indirect_placeholder_225(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern void indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_226_ret_type indirect_placeholder_226(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_227_ret_type indirect_placeholder_227(uint64_t param_0);
extern struct indirect_placeholder_228_ret_type indirect_placeholder_228(uint64_t param_0);
uint64_t bb_set_regs(uint64_t rcx, uint64_t rdx, uint64_t rdi, uint64_t r8, uint64_t rsi) {
    uint64_t *_pre_phi159;
    uint64_t local_sp_1;
    uint64_t var_67;
    struct helper_pxor_xmm_wrapper_337_ret_type var_20;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t *var_11;
    uint64_t var_12;
    uint64_t *var_13;
    uint64_t var_14;
    uint64_t local_sp_0;
    uint32_t rax_0;
    uint64_t local_sp_8;
    uint32_t *var_60;
    uint64_t var_61;
    uint64_t rax_2;
    uint64_t local_sp_6;
    uint64_t local_sp_2;
    uint64_t var_64;
    struct indirect_placeholder_226_ret_type var_65;
    uint64_t var_66;
    uint64_t var_63;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t local_sp_4;
    uint64_t r84_0;
    uint64_t var_44;
    uint64_t rax_1;
    uint64_t rdx2_0;
    uint64_t local_sp_3;
    uint64_t _pre_phi;
    uint64_t var_45;
    uint64_t r15_2;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t r15_0;
    uint64_t r11_0;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t *var_50;
    uint64_t var_51;
    uint64_t *var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t r15_1;
    uint64_t *_pre_phi155;
    uint64_t local_sp_5;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_62;
    uint64_t local_sp_7;
    uint64_t var_15;
    struct indirect_placeholder_227_ret_type var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t _pre;
    uint64_t r14_0;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t *var_37;
    uint64_t *var_38;
    uint64_t *var_39;
    uint64_t *var_40;
    bool var_41;
    uint64_t var_29;
    uint64_t *var_30;
    uint64_t var_31;
    struct indirect_placeholder_228_ret_type var_32;
    uint64_t var_33;
    uint64_t *_pre158;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r15();
    var_3 = init_r14();
    var_4 = init_r12();
    var_5 = init_rbx();
    var_6 = init_r13();
    var_7 = var_0 + (-8L);
    *(uint64_t *)var_7 = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_6;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_5;
    var_8 = *(uint64_t *)rdi;
    var_9 = *(uint64_t *)4340544UL;
    var_10 = *(uint64_t *)4340552UL;
    var_11 = (uint64_t *)(var_0 + (-72L));
    *var_11 = 0UL;
    var_12 = var_0 + (-160L);
    var_13 = (uint64_t *)var_12;
    *var_13 = var_8;
    var_14 = var_0 + (-88L);
    *(uint64_t *)var_14 = var_9;
    *(uint64_t *)(var_0 + (-80L)) = var_10;
    rax_0 = 1U;
    rax_1 = rcx;
    rdx2_0 = 0UL;
    rax_2 = 12UL;
    if ((uint64_t)(unsigned char)r8 == 0UL) {
        var_18 = var_0 + (-168L);
        *(uint64_t *)(var_0 + (-152L)) = 0UL;
        local_sp_7 = var_18;
    } else {
        var_15 = var_0 + (-176L);
        *(uint64_t *)var_15 = 4262235UL;
        var_16 = indirect_placeholder_227(96UL);
        var_17 = var_16.field_0;
        *var_11 = var_17;
        local_sp_7 = var_15;
        if (var_17 != 0UL) {
            return rax_2;
        }
        *(uint64_t *)(var_0 + (-152L)) = var_14;
    }
    var_19 = *var_13;
    var_20 = helper_pxor_xmm_wrapper_337((struct type_3 *)(0UL), (struct type_5 *)(776UL), (struct type_5 *)(776UL), var_9, var_10);
    var_21 = var_20.field_0;
    var_22 = var_20.field_1;
    *(uint64_t *)(var_0 + (-104L)) = 0UL;
    var_23 = rdx << 4UL;
    var_24 = var_0 + (-120L);
    *(uint64_t *)var_24 = var_21;
    *(uint64_t *)(var_0 + (-112L)) = var_22;
    var_25 = *(uint64_t *)(var_19 + 144UL);
    r15_2 = var_25;
    rax_2 = 1UL;
    if (var_23 > 4031UL) {
        var_29 = var_0 + (-144L);
        var_30 = (uint64_t *)var_29;
        *var_30 = var_23;
        var_31 = local_sp_7 + (-8L);
        *(uint64_t *)var_31 = 4262751UL;
        var_32 = indirect_placeholder_228(var_23);
        var_33 = var_32.field_0;
        *(unsigned char *)(var_0 + (-161L)) = (unsigned char)'\x01';
        local_sp_1 = var_31;
        local_sp_8 = var_31;
        _pre_phi = var_29;
        _pre_phi155 = var_30;
        r14_0 = var_33;
        if (var_33 != 0UL) {
            _pre158 = (uint64_t *)(var_0 + (-152L));
            _pre_phi159 = _pre158;
            var_67 = *_pre_phi159;
            *(uint64_t *)(local_sp_1 + (-8L)) = 4262789UL;
            indirect_placeholder_225(var_7, rsi, rcx, var_67);
            return 12UL;
        }
    }
    var_26 = var_23 + 16UL;
    *(unsigned char *)(var_0 + (-161L)) = (unsigned char)'\x00';
    var_27 = local_sp_7 - var_26;
    var_28 = (var_27 + 15UL) & (-16L);
    _pre = var_0 + (-144L);
    _pre_phi155 = (uint64_t *)_pre;
    _pre_phi = _pre;
    local_sp_8 = var_27;
    r14_0 = var_28;
    var_34 = local_sp_8 + (-8L);
    *(uint64_t *)var_34 = 4262343UL;
    indirect_placeholder();
    var_35 = *(uint64_t *)rcx;
    var_36 = var_0 + (-128L);
    var_37 = (uint64_t *)var_36;
    *var_37 = var_35;
    var_38 = (uint64_t *)(rcx + 8UL);
    var_39 = (uint64_t *)(rsi + 176UL);
    var_40 = (uint64_t *)(var_0 + (-152L));
    var_41 = (rdx == 0UL);
    _pre_phi159 = var_40;
    r11_0 = var_36;
    local_sp_5 = var_34;
    r84_0 = var_35;
    while (1U)
        {
            r15_0 = r15_2;
            local_sp_6 = local_sp_5;
            if ((long)*var_38 >= (long)r84_0) {
                loop_state_var = 0U;
                break;
            }
            var_42 = **(uint64_t **)var_12;
            var_43 = local_sp_5 + (-8L);
            *(uint64_t *)var_43 = 4262393UL;
            indirect_placeholder_7(r15_2, r14_0, rdx, var_42, r84_0, rcx);
            local_sp_3 = var_43;
            local_sp_6 = var_43;
            if (*var_38 == *var_37) {
                var_48 = *var_40;
                var_49 = local_sp_3 + (-16L);
                var_50 = (uint64_t *)var_49;
                *var_50 = var_48;
                *_pre_phi155 = r11_0;
                var_51 = local_sp_3 + (-24L);
                var_52 = (uint64_t *)var_51;
                *var_52 = 4262458UL;
                var_53 = indirect_placeholder_9(r11_0, rcx, var_24, rsi, r15_0, rdx);
                var_54 = *_pre_phi155;
                local_sp_0 = var_49;
                local_sp_1 = var_49;
                local_sp_4 = local_sp_3 + (-8L);
                r15_1 = var_53;
                if ((long)var_53 <= (long)18446744073709551615UL) {
                    local_sp_4 = var_49;
                    if (var_53 != 18446744073709551614UL) {
                        *var_50 = 4262818UL;
                        indirect_placeholder();
                        if (*(unsigned char *)(var_0 + (-161L)) != '\x00') {
                            loop_state_var = 3U;
                            break;
                        }
                        *var_52 = 4262835UL;
                        indirect_placeholder();
                        local_sp_1 = var_51;
                        loop_state_var = 3U;
                        break;
                    }
                    var_55 = *var_40;
                    if (var_55 != 0UL) {
                        *var_50 = 4262905UL;
                        indirect_placeholder();
                        if (*(unsigned char *)(var_0 + (-161L)) != '\x00') {
                            loop_state_var = 2U;
                            break;
                        }
                        loop_state_var = 1U;
                        break;
                    }
                    var_56 = var_55 + 16UL;
                    *var_50 = 4262523UL;
                    var_57 = indirect_placeholder_9(rdx, var_54, var_24, var_55, rcx, var_56);
                    r15_1 = var_57;
                }
                local_sp_5 = local_sp_4;
                r15_2 = r15_1;
                r84_0 = *var_37;
                continue;
            }
            rax_0 = 0U;
            rax_2 = 0UL;
            if (*var_39 != r15_2) {
                var_44 = *var_40;
                if (var_44 != 0UL) {
                    var_59 = local_sp_5 + (-16L);
                    *(uint64_t *)var_59 = 4262856UL;
                    indirect_placeholder();
                    local_sp_0 = var_59;
                    if (*(unsigned char *)(var_0 + (-161L)) != '\x00') {
                        loop_state_var = 2U;
                        break;
                    }
                    loop_state_var = 1U;
                    break;
                }
                if (!var_41) {
                    loop_state_var = 0U;
                    break;
                }
                while (1U)
                    {
                        if ((long)*(uint64_t *)rax_1 >= (long)0UL) {
                            if (*(uint64_t *)(rax_1 + 8UL) != 18446744073709551615UL) {
                                loop_state_var = 1U;
                                break;
                            }
                        }
                        var_58 = rdx2_0 + 1UL;
                        rdx2_0 = var_58;
                        if (var_58 != rdx) {
                            loop_state_var = 0U;
                            break;
                        }
                        rax_1 = rax_1 + 16UL;
                        continue;
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 1U:
                    {
                        if (rdx2_0 != rdx) {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        *_pre_phi155 = var_36;
                        var_45 = var_44 + 16UL;
                        var_46 = local_sp_5 + (-16L);
                        *(uint64_t *)var_46 = 4262685UL;
                        var_47 = indirect_placeholder_9(rdx, var_36, var_24, var_44, rcx, var_45);
                        local_sp_3 = var_46;
                        r15_0 = var_47;
                        r11_0 = *_pre_phi155;
                    }
                    break;
                  case 0U:
                    {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
        }
    switch (loop_state_var) {
      case 3U:
        {
            var_67 = *_pre_phi159;
            *(uint64_t *)(local_sp_1 + (-8L)) = 4262789UL;
            indirect_placeholder_225(var_7, rsi, rcx, var_67);
            return 12UL;
        }
        break;
      case 2U:
      case 1U:
      case 0U:
        {
            switch (loop_state_var) {
              case 1U:
                {
                    return rax_2;
                }
                break;
              case 2U:
                {
                    var_60 = (uint32_t *)_pre_phi;
                    *var_60 = rax_0;
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4262885UL;
                    indirect_placeholder();
                    var_61 = (uint64_t)*var_60;
                    rax_2 = var_61;
                }
                break;
              case 0U:
                {
                    var_62 = local_sp_6 + (-8L);
                    *(uint64_t *)var_62 = 4262595UL;
                    indirect_placeholder();
                    local_sp_2 = var_62;
                    if (*(unsigned char *)(var_0 + (-161L)) != '\x00') {
                        var_63 = local_sp_6 + (-16L);
                        *(uint64_t *)var_63 = 4262708UL;
                        indirect_placeholder();
                        local_sp_2 = var_63;
                    }
                    var_64 = *var_40;
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4262616UL;
                    var_65 = indirect_placeholder_226(var_7, rsi, rcx, var_64);
                    var_66 = var_65.field_0;
                    rax_2 = var_66;
                }
                break;
            }
        }
        break;
    }
}
