typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_pxor_xmm_wrapper_337_ret_type;
struct type_3;
struct type_5;
struct helper_pxor_xmm_wrapper_337_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_3 {
};
struct type_5 {
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern struct helper_pxor_xmm_wrapper_337_ret_type helper_pxor_xmm_wrapper_337(struct type_3 *param_0, struct type_5 *param_1, struct type_5 *param_2, uint64_t param_3, uint64_t param_4);
void bb_init_word_char(uint64_t rdi) {
    struct helper_pxor_xmm_wrapper_337_ret_type var_13;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    unsigned char *var_8;
    unsigned char var_9;
    uint64_t var_10;
    uint64_t storemerge1;
    uint64_t rbx_0_in;
    uint32_t var_23;
    uint64_t rbp_0_ph;
    uint64_t local_sp_1;
    uint64_t var_24;
    uint32_t var_25;
    uint64_t rax_0;
    uint64_t r12_0;
    uint32_t var_26;
    uint64_t rbx_0_ph_in;
    uint64_t local_sp_0;
    uint64_t local_sp_1_ph;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_14;
    uint64_t storemerge;
    uint32_t var_15;
    uint64_t var_16;
    uint32_t var_17;
    uint64_t rax_1;
    uint64_t r14_0;
    uint64_t *var_18;
    uint64_t rax_2_ph;
    bool var_19;
    uint64_t rbp_0;
    uint32_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r15();
    var_3 = init_r14();
    var_4 = init_r12();
    var_5 = init_rbx();
    var_6 = init_r13();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_6;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_1;
    *(uint64_t *)(var_0 + (-48L)) = var_5;
    var_7 = var_0 + (-56L);
    var_8 = (unsigned char *)(rdi + 176UL);
    var_9 = *var_8 | '\x10';
    var_10 = (uint64_t)var_9;
    *var_8 = var_9;
    storemerge1 = 0UL;
    storemerge = 0UL;
    local_sp_0 = var_7;
    rbp_0_ph = 0UL;
    if ((var_10 & 8UL) != 0UL) {
        var_11 = *(uint64_t *)4340512UL;
        var_12 = *(uint64_t *)4340520UL;
        *(uint64_t *)(rdi + 184UL) = var_11;
        *(uint64_t *)(rdi + 192UL) = var_12;
        storemerge1 = 128UL;
        storemerge = 2UL;
        if ((var_10 & 4UL) == 0UL) {
            var_13 = helper_pxor_xmm_wrapper_337((struct type_3 *)(0UL), (struct type_5 *)(776UL), (struct type_5 *)(776UL), var_11, var_12);
            var_14 = var_13.field_1;
            *(uint64_t *)(rdi + 200UL) = var_13.field_0;
            *(uint64_t *)(rdi + 208UL) = var_14;
            return;
        }
    }
    var_15 = (uint32_t)storemerge1 | 256U;
    var_16 = storemerge << 6UL;
    var_17 = var_15 - (uint32_t)var_16;
    rax_1 = var_16;
    r14_0 = storemerge1;
    r12_0 = (storemerge << 3UL) + rdi;
    while (1U)
        {
            var_18 = (uint64_t *)(r12_0 + 184UL);
            local_sp_1_ph = local_sp_0;
            rax_2_ph = rax_1;
            rbx_0_ph_in = r14_0;
            while (1U)
                {
                    var_19 = ((uint64_t)(uint32_t)rax_2_ph == 0UL);
                    rax_0 = rax_2_ph;
                    local_sp_1 = local_sp_1_ph;
                    rbp_0 = rbp_0_ph;
                    rbx_0_in = rbx_0_ph_in;
                    while (1U)
                        {
                            var_20 = (uint32_t)rbx_0_in;
                            var_21 = (uint64_t)var_20;
                            var_22 = local_sp_1 + (-8L);
                            *(uint64_t *)var_22 = 4241079UL;
                            indirect_placeholder();
                            local_sp_0 = var_22;
                            local_sp_1_ph = var_22;
                            local_sp_1 = var_22;
                            if (!var_19) {
                                loop_state_var = 0U;
                                break;
                            }
                            if ((uint64_t)(var_20 + (-95)) != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            var_23 = (uint32_t)rbp_0;
                            if ((uint64_t)(var_23 + (-63)) != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            rbp_0 = (uint64_t)(var_23 + 1U);
                            rbx_0_in = var_21 + 1UL;
                            continue;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 1U:
                        {
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 0U:
                        {
                            var_24 = 1UL << (rbp_0 & 63UL);
                            *var_18 = (*var_18 | var_24);
                            var_25 = (uint32_t)rbp_0;
                            rax_0 = var_24;
                            rax_2_ph = var_24;
                            if ((uint64_t)(var_25 + (-63)) == 0UL) {
                                switch_state_var = 1;
                                break;
                            }
                            rbp_0_ph = (uint64_t)(var_25 + 1U);
                            rbx_0_ph_in = var_21 + 1UL;
                            continue;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            var_26 = (uint32_t)r14_0 + 64U;
            rax_1 = rax_0;
            r14_0 = (uint64_t)var_26;
            if ((uint64_t)(var_17 - var_26) == 0UL) {
                break;
            }
            r12_0 = r12_0 + 8UL;
            continue;
        }
    return;
}
