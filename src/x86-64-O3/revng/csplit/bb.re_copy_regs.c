typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct helper_movq_mm_T0_xmm_wrapper_ret_type;
struct type_4;
struct indirect_placeholder_168_ret_type;
struct helper_movq_mm_T0_xmm_wrapper_370_ret_type;
struct helper_punpcklqdq_xmm_wrapper_377_ret_type;
struct type_9;
struct helper_punpckhqdq_xmm_wrapper_ret_type;
struct helper_punpcklqdq_xmm_wrapper_378_ret_type;
struct indirect_placeholder_169_ret_type;
struct helper_movq_mm_T0_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_4 {
};
struct indirect_placeholder_168_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_movq_mm_T0_xmm_wrapper_370_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_punpcklqdq_xmm_wrapper_377_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
};
struct type_9 {
};
struct helper_punpckhqdq_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
};
struct helper_punpcklqdq_xmm_wrapper_378_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
};
struct indirect_placeholder_169_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rax(void);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct helper_movq_mm_T0_xmm_wrapper_ret_type helper_movq_mm_T0_xmm_wrapper(struct type_4 *param_0, uint64_t param_1);
extern struct indirect_placeholder_168_ret_type indirect_placeholder_168(uint64_t param_0);
extern struct helper_movq_mm_T0_xmm_wrapper_370_ret_type helper_movq_mm_T0_xmm_wrapper_370(struct type_4 *param_0, uint64_t param_1);
extern struct helper_punpcklqdq_xmm_wrapper_377_ret_type helper_punpcklqdq_xmm_wrapper_377(struct type_9 *param_0, struct type_4 *param_1, struct type_4 *param_2, uint64_t param_3, uint64_t param_4);
extern struct helper_punpckhqdq_xmm_wrapper_ret_type helper_punpckhqdq_xmm_wrapper(struct type_9 *param_0, struct type_4 *param_1, struct type_4 *param_2, uint64_t param_3, uint64_t param_4);
extern struct helper_punpcklqdq_xmm_wrapper_378_ret_type helper_punpcklqdq_xmm_wrapper_378(struct type_9 *param_0, struct type_4 *param_1, struct type_4 *param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_169_ret_type indirect_placeholder_169(uint64_t param_0);
uint64_t bb_re_copy_regs(uint64_t rcx, uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    struct helper_punpcklqdq_xmm_wrapper_378_ret_type var_56;
    struct helper_punpckhqdq_xmm_wrapper_ret_type var_53;
    struct helper_punpcklqdq_xmm_wrapper_377_ret_type var_26;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint32_t var_11;
    uint64_t var_36;
    uint64_t *_pre_phi117;
    uint64_t rbx_0;
    uint64_t r13_4;
    struct indirect_placeholder_168_ret_type var_33;
    uint64_t var_34;
    uint64_t *var_35;
    uint64_t *var_19;
    uint64_t var_20;
    uint64_t var_21;
    struct helper_movq_mm_T0_xmm_wrapper_ret_type var_22;
    uint64_t var_23;
    struct helper_movq_mm_T0_xmm_wrapper_370_ret_type var_24;
    uint64_t var_25;
    uint64_t var_27;
    uint64_t r13_0;
    uint64_t local_sp_0;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t *var_14;
    uint64_t var_15;
    uint64_t *var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_28;
    uint64_t *_pre_phi113;
    uint64_t r13_1;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t rax_0;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t rax_1;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t r13_3;
    uint64_t *var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t rbx_1;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_29;
    struct indirect_placeholder_169_ret_type var_30;
    uint64_t var_31;
    uint64_t *var_32;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_cc_src2();
    var_3 = init_rbp();
    var_4 = init_r15();
    var_5 = init_r14();
    var_6 = init_r12();
    var_7 = init_rbx();
    var_8 = init_r13();
    *(uint64_t *)(var_0 + (-8L)) = var_4;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    var_9 = rdx + 1UL;
    *(uint64_t *)(var_0 + (-24L)) = var_8;
    *(uint64_t *)(var_0 + (-32L)) = var_6;
    *(uint64_t *)(var_0 + (-40L)) = var_3;
    *(uint64_t *)(var_0 + (-48L)) = var_7;
    var_10 = var_0 + (-72L);
    var_11 = (uint32_t)rcx;
    rbx_0 = rdx;
    r13_4 = 0UL;
    r13_0 = 1UL;
    local_sp_0 = var_10;
    r13_1 = 1UL;
    rax_0 = 0UL;
    rax_1 = 0UL;
    r13_3 = 1UL;
    if ((uint64_t)var_11 == 0UL) {
        var_29 = var_9 << 3UL;
        *(uint64_t *)(var_0 + (-80L)) = 4246224UL;
        var_30 = indirect_placeholder_169(var_29);
        var_31 = var_30.field_0;
        var_32 = (uint64_t *)(rdi + 8UL);
        *var_32 = var_31;
        _pre_phi113 = var_32;
        rbx_0 = 0UL;
        if (var_31 == 0UL) {
            return (uint64_t)(uint32_t)r13_4;
        }
        *(uint64_t *)(var_0 + (-88L)) = 4246246UL;
        var_33 = indirect_placeholder_168(var_29);
        var_34 = var_33.field_0;
        var_35 = (uint64_t *)(rdi + 16UL);
        *var_35 = var_34;
        _pre_phi117 = var_35;
        if (var_34 != 0UL) {
            *(uint64_t *)(var_0 + (-96L)) = 4246376UL;
            indirect_placeholder();
            return (uint64_t)(uint32_t)r13_4;
        }
        *(uint64_t *)rdi = var_9;
        var_36 = helper_cc_compute_all_wrapper(rdx, 0UL, 0UL, 25U);
        if ((uint64_t)(((unsigned char)(var_36 >> 4UL) ^ (unsigned char)var_36) & '\xc0') != 0UL) {
            var_68 = (uint64_t *)rdi;
            rbx_1 = rbx_0;
            r13_4 = r13_3;
            if (*var_68 <= rbx_0) {
                var_69 = *(uint64_t *)(rdi + 16UL);
                var_70 = *(uint64_t *)(rdi + 8UL);
                var_71 = rbx_1 + 1UL;
                var_72 = rbx_1 << 3UL;
                *(uint64_t *)(var_72 + var_69) = 18446744073709551615UL;
                *(uint64_t *)(var_72 + var_70) = 18446744073709551615UL;
                rbx_1 = var_71;
                do {
                    var_71 = rbx_1 + 1UL;
                    var_72 = rbx_1 << 3UL;
                    *(uint64_t *)(var_72 + var_69) = 18446744073709551615UL;
                    *(uint64_t *)(var_72 + var_70) = 18446744073709551615UL;
                    rbx_1 = var_71;
                } while (*var_68 <= var_71);
            }
            return (uint64_t)(uint32_t)r13_4;
        }
    }
    rbx_0 = 0UL;
    if ((uint64_t)(var_11 + (-1)) == 0UL) {
        var_14 = (uint64_t *)rdi;
        var_15 = helper_cc_compute_c_wrapper(*var_14 - var_9, var_9, var_2, 17U);
        if (var_15 != 0UL) {
            var_16 = (uint64_t *)(rdi + 8UL);
            var_17 = *var_16;
            var_18 = var_9 << 3UL;
            *(uint64_t *)(var_0 + (-64L)) = var_18;
            *(uint64_t *)(var_0 + (-80L)) = 4246133UL;
            indirect_placeholder_1(var_17, var_18);
            if (var_1 != 0UL) {
                return (uint64_t)(uint32_t)r13_4;
            }
            var_19 = (uint64_t *)(rdi + 16UL);
            var_20 = *var_19;
            var_21 = *(uint64_t *)var_10;
            *(uint64_t *)(var_0 + (-88L)) = 4246160UL;
            indirect_placeholder_1(var_20, var_21);
            var_22 = helper_movq_mm_T0_xmm_wrapper((struct type_4 *)(776UL), var_1);
            var_23 = var_22.field_0;
            var_24 = helper_movq_mm_T0_xmm_wrapper_370((struct type_4 *)(968UL), var_1);
            var_25 = var_24.field_0;
            *var_14 = var_9;
            var_26 = helper_punpcklqdq_xmm_wrapper_377((struct type_9 *)(0UL), (struct type_4 *)(776UL), (struct type_4 *)(968UL), var_23, var_25);
            var_27 = var_26.field_1;
            *var_16 = var_26.field_0;
            *var_19 = var_27;
        }
    } else {
        r13_0 = 2UL;
        if ((uint64_t)(var_11 + (-2)) == 0UL) {
            var_12 = var_0 + (-80L);
            *(uint64_t *)var_12 = 4245786UL;
            indirect_placeholder();
            local_sp_0 = var_12;
        }
        var_13 = helper_cc_compute_c_wrapper(*(uint64_t *)rdi - rdx, rdx, var_2, 17U);
        if (var_13 == 0UL) {
            *(uint64_t *)(local_sp_0 + (-8L)) = 4246361UL;
            indirect_placeholder();
        }
    }
    var_28 = helper_cc_compute_all_wrapper(rdx, 0UL, 0UL, 25U);
    r13_1 = r13_0;
    r13_3 = r13_0;
    if ((uint64_t)(((unsigned char)(var_28 >> 4UL) ^ (unsigned char)var_28) & '\xc0') != 0UL) {
        var_68 = (uint64_t *)rdi;
        rbx_1 = rbx_0;
        r13_4 = r13_3;
        if (*var_68 <= rbx_0) {
            var_69 = *(uint64_t *)(rdi + 16UL);
            var_70 = *(uint64_t *)(rdi + 8UL);
            var_71 = rbx_1 + 1UL;
            var_72 = rbx_1 << 3UL;
            *(uint64_t *)(var_72 + var_69) = 18446744073709551615UL;
            *(uint64_t *)(var_72 + var_70) = 18446744073709551615UL;
            rbx_1 = var_71;
            do {
                var_71 = rbx_1 + 1UL;
                var_72 = rbx_1 << 3UL;
                *(uint64_t *)(var_72 + var_69) = 18446744073709551615UL;
                *(uint64_t *)(var_72 + var_70) = 18446744073709551615UL;
                rbx_1 = var_71;
            } while (*var_68 <= var_71);
        }
        return (uint64_t)(uint32_t)r13_4;
    }
    _pre_phi117 = (uint64_t *)(rdi + 16UL);
    _pre_phi113 = (uint64_t *)(rdi + 8UL);
    var_37 = *_pre_phi113;
    var_38 = *_pre_phi117;
    var_39 = rdx << 4UL;
    var_40 = rdx << 3UL;
    var_41 = var_37 + 15UL;
    var_42 = var_39 + rsi;
    var_43 = (((var_41 - var_38) > 30UL) && ((rdx + (-1L)) > 2UL));
    var_44 = var_40 + var_37;
    var_45 = helper_cc_compute_c_wrapper(rsi - var_44, var_44, var_2, 17U);
    r13_3 = r13_1;
    if ((((var_45 ^ 1UL) | (var_42 <= var_37)) & var_43) == 0UL) {
        var_46 = var_40 + var_38;
        var_47 = helper_cc_compute_c_wrapper(rsi - var_46, var_46, var_2, 17U);
        if ((((uint64_t)(unsigned char)var_47 ^ 1UL) | (var_42 <= var_38)) == 0UL) {
            var_66 = (rax_1 << 1UL) + rsi;
            *(uint64_t *)(rax_1 + var_37) = *(uint64_t *)var_66;
            *(uint64_t *)(rax_1 + var_38) = *(uint64_t *)(var_66 + 8UL);
            var_67 = rax_1 + 8UL;
            rax_1 = var_67;
            do {
                var_66 = (rax_1 << 1UL) + rsi;
                *(uint64_t *)(rax_1 + var_37) = *(uint64_t *)var_66;
                *(uint64_t *)(rax_1 + var_38) = *(uint64_t *)(var_66 + 8UL);
                var_67 = rax_1 + 8UL;
                rax_1 = var_67;
            } while (var_67 != var_40);
        } else {
            var_48 = (rdx >> 1UL) << 4UL;
            var_49 = (rax_0 << 1UL) + rsi;
            var_50 = *(uint64_t *)var_49;
            var_51 = *(uint64_t *)(var_49 + 8UL);
            var_52 = *(uint64_t *)(var_49 + 16UL);
            var_53 = helper_punpckhqdq_xmm_wrapper((struct type_9 *)(0UL), (struct type_4 *)(776UL), (struct type_4 *)(840UL), var_51, *(uint64_t *)(var_49 + 24UL));
            var_54 = var_53.field_0;
            var_55 = var_53.field_1;
            var_56 = helper_punpcklqdq_xmm_wrapper_378((struct type_9 *)(0UL), (struct type_4 *)(904UL), (struct type_4 *)(840UL), var_52, var_50);
            var_57 = var_56.field_0;
            var_58 = var_56.field_1;
            var_59 = rax_0 + var_37;
            *(uint64_t *)var_59 = var_57;
            *(uint64_t *)(var_59 + 8UL) = var_58;
            var_60 = rax_0 + var_38;
            *(uint64_t *)var_60 = var_54;
            *(uint64_t *)(var_60 + 8UL) = var_55;
            var_61 = rax_0 + 16UL;
            rax_0 = var_61;
            do {
                var_49 = (rax_0 << 1UL) + rsi;
                var_50 = *(uint64_t *)var_49;
                var_51 = *(uint64_t *)(var_49 + 8UL);
                var_52 = *(uint64_t *)(var_49 + 16UL);
                var_53 = helper_punpckhqdq_xmm_wrapper((struct type_9 *)(0UL), (struct type_4 *)(776UL), (struct type_4 *)(840UL), var_51, *(uint64_t *)(var_49 + 24UL));
                var_54 = var_53.field_0;
                var_55 = var_53.field_1;
                var_56 = helper_punpcklqdq_xmm_wrapper_378((struct type_9 *)(0UL), (struct type_4 *)(904UL), (struct type_4 *)(840UL), var_52, var_50);
                var_57 = var_56.field_0;
                var_58 = var_56.field_1;
                var_59 = rax_0 + var_37;
                *(uint64_t *)var_59 = var_57;
                *(uint64_t *)(var_59 + 8UL) = var_58;
                var_60 = rax_0 + var_38;
                *(uint64_t *)var_60 = var_54;
                *(uint64_t *)(var_60 + 8UL) = var_55;
                var_61 = rax_0 + 16UL;
                rax_0 = var_61;
            } while (var_48 != var_61);
            var_62 = rdx & (-2L);
            if ((rdx & 1UL) == 0UL) {
                var_63 = (var_62 << 4UL) + rsi;
                var_64 = *(uint64_t *)var_63;
                var_65 = var_62 << 3UL;
                *(uint64_t *)(var_65 + var_37) = var_64;
                *(uint64_t *)(var_65 + var_38) = *(uint64_t *)(var_63 + 8UL);
            }
        }
    } else {
        var_66 = (rax_1 << 1UL) + rsi;
        *(uint64_t *)(rax_1 + var_37) = *(uint64_t *)var_66;
        *(uint64_t *)(rax_1 + var_38) = *(uint64_t *)(var_66 + 8UL);
        var_67 = rax_1 + 8UL;
        rax_1 = var_67;
        do {
            var_66 = (rax_1 << 1UL) + rsi;
            *(uint64_t *)(rax_1 + var_37) = *(uint64_t *)var_66;
            *(uint64_t *)(rax_1 + var_38) = *(uint64_t *)(var_66 + 8UL);
            var_67 = rax_1 + 8UL;
            rax_1 = var_67;
        } while (var_67 != var_40);
    }
    var_68 = (uint64_t *)rdi;
    rbx_1 = rbx_0;
    r13_4 = r13_3;
    if (*var_68 > rbx_0) {
        return (uint64_t)(uint32_t)r13_4;
    }
    var_69 = *(uint64_t *)(rdi + 16UL);
    var_70 = *(uint64_t *)(rdi + 8UL);
    var_71 = rbx_1 + 1UL;
    var_72 = rbx_1 << 3UL;
    *(uint64_t *)(var_72 + var_69) = 18446744073709551615UL;
    *(uint64_t *)(var_72 + var_70) = 18446744073709551615UL;
    rbx_1 = var_71;
    do {
        var_71 = rbx_1 + 1UL;
        var_72 = rbx_1 << 3UL;
        *(uint64_t *)(var_72 + var_69) = 18446744073709551615UL;
        *(uint64_t *)(var_72 + var_70) = 18446744073709551615UL;
        rbx_1 = var_71;
    } while (*var_68 <= var_71);
    return (uint64_t)(uint32_t)r13_4;
}
