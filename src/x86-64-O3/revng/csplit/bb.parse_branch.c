typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_295_ret_type;
struct indirect_placeholder_296_ret_type;
struct indirect_placeholder_295_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_296_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_295_ret_type indirect_placeholder_295(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_296_ret_type indirect_placeholder_296(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
uint64_t bb_parse_branch(uint64_t rcx, uint64_t r10, uint64_t rdx, uint64_t r9, uint64_t rdi, uint64_t r8, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    struct indirect_placeholder_296_ret_type var_20;
    uint64_t var_7;
    uint64_t var_8;
    struct indirect_placeholder_295_ret_type var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint32_t *var_12;
    uint64_t r14_1;
    uint64_t local_sp_0;
    uint64_t rax_0;
    uint64_t r102_0;
    unsigned char var_29;
    uint64_t local_sp_0_be;
    uint64_t rax_0_be;
    uint64_t r14_0_be;
    uint64_t var_30;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    unsigned char var_31;
    uint64_t var_32;
    uint64_t var_33;
    unsigned char *var_13;
    unsigned char var_14;
    uint64_t var_15;
    uint64_t r14_0;
    uint64_t *var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t *var_19;
    uint64_t var_21;
    uint64_t var_22;
    bool var_23;
    bool var_24;
    bool or_cond;
    bool var_25;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r15();
    var_3 = init_r14();
    var_4 = init_r12();
    var_5 = init_rbx();
    var_6 = init_r13();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_6;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_1;
    *(uint64_t *)(var_0 + (-48L)) = var_5;
    var_7 = *(uint64_t *)rsi;
    *(uint64_t *)(var_0 + (-88L)) = rdi;
    *(uint64_t *)(var_0 + (-64L)) = var_7;
    var_8 = var_0 + (-96L);
    *(uint64_t *)var_8 = 4278686UL;
    var_9 = indirect_placeholder_295(rcx, r10, rdx, r9, rdi, r8, rsi);
    var_10 = var_9.field_0;
    var_11 = var_9.field_1;
    var_12 = (uint32_t *)r9;
    local_sp_0 = var_8;
    r14_0 = var_10;
    r102_0 = var_11;
    r14_1 = 0UL;
    var_13 = (unsigned char *)(rdx + 8UL);
    var_14 = *var_13;
    var_15 = (uint64_t)var_14;
    rax_0 = var_15;
    r14_1 = var_10;
    if (!(!((*var_12 != 0U) && (var_10 == 0UL)) & (uint64_t)((var_14 & '\xf7') + '\xfe') != 0UL)) {
        return r14_1;
    }
    *(unsigned char *)(var_0 + (-73L)) = (r8 == 0UL);
    r14_1 = 0UL;
    while (1U)
        {
            if ((uint64_t)((unsigned char)rax_0 + '\xf7') != 0UL) {
                r14_1 = r14_0;
                if (*(unsigned char *)(local_sp_0 + 23UL) == '\x00') {
                    break;
                }
            }
            var_16 = (uint64_t *)local_sp_0;
            var_17 = *var_16;
            var_18 = local_sp_0 + (-8L);
            var_19 = (uint64_t *)var_18;
            *var_19 = 4278830UL;
            var_20 = indirect_placeholder_296(rcx, r102_0, rdx, r9, var_17, r8, rsi);
            var_21 = var_20.field_0;
            var_22 = var_20.field_1;
            var_23 = (*var_12 != 0U);
            var_24 = (var_21 == 0UL);
            or_cond = var_23 && var_24;
            var_25 = (r14_0 == 0UL);
            local_sp_0_be = var_18;
            r102_0 = var_22;
            if (!or_cond) {
                if (var_25) {
                    break;
                }
                *(uint64_t *)(local_sp_0 + (-16L)) = 4278911UL;
                indirect_placeholder_10(0UL, r14_0, 4241248UL);
                break;
            }
            if (var_25 || var_24) {
                var_31 = *var_13;
                var_32 = (uint64_t)var_31;
                var_33 = var_25 ? var_21 : r14_0;
                rax_0_be = var_32;
                r14_0_be = var_33;
                r14_1 = var_33;
                if ((uint64_t)((var_31 & '\xf7') + '\xfe') == 0UL) {
                    break;
                }
            }
            var_26 = *(uint64_t *)(local_sp_0 + 16UL);
            *var_16 = var_21;
            var_27 = local_sp_0 + (-16L);
            *(uint64_t *)var_27 = 4278762UL;
            var_28 = indirect_placeholder_10(var_21, var_26, r14_0);
            local_sp_0_be = var_27;
            r14_0_be = var_28;
            r14_1 = var_28;
            if (var_28 == 0UL) {
                var_29 = *var_13;
                rax_0_be = (uint64_t)var_29;
                if ((uint64_t)((var_29 & '\xf7') + '\xfe') == 0UL) {
                    break;
                }
                local_sp_0 = local_sp_0_be;
                rax_0 = rax_0_be;
                r14_0 = r14_0_be;
                continue;
            }
            var_30 = *var_19;
            *(uint64_t *)(local_sp_0 + (-24L)) = 4278931UL;
            indirect_placeholder_10(0UL, var_30, 4241248UL);
            *(uint64_t *)(local_sp_0 + (-32L)) = 4278946UL;
            indirect_placeholder_10(0UL, r14_0, 4241248UL);
            *var_12 = 12U;
            r14_1 = 0UL;
            break;
        }
    return r14_1;
}
