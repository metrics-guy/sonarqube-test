typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_18_ret_type;
struct indirect_placeholder_19_ret_type;
struct indirect_placeholder_20_ret_type;
struct indirect_placeholder_22_ret_type;
struct indirect_placeholder_21_ret_type;
struct indirect_placeholder_23_ret_type;
struct indirect_placeholder_27_ret_type;
struct indirect_placeholder_25_ret_type;
struct indirect_placeholder_26_ret_type;
struct indirect_placeholder_28_ret_type;
struct indirect_placeholder_18_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_19_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_20_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_22_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_21_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_23_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_27_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_25_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_26_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_28_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
extern void indirect_placeholder_17(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder_13(void);
extern struct indirect_placeholder_18_ret_type indirect_placeholder_18(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_19_ret_type indirect_placeholder_19(void);
extern struct indirect_placeholder_20_ret_type indirect_placeholder_20(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_22_ret_type indirect_placeholder_22(void);
extern struct indirect_placeholder_21_ret_type indirect_placeholder_21(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_23_ret_type indirect_placeholder_23(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern void indirect_placeholder_24(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8);
extern struct indirect_placeholder_27_ret_type indirect_placeholder_27(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_25_ret_type indirect_placeholder_25(void);
extern struct indirect_placeholder_26_ret_type indirect_placeholder_26(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_28_ret_type indirect_placeholder_28(void);
void bb_process_regexp(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    unsigned char var_8;
    uint64_t local_sp_8;
    uint64_t local_sp_6;
    uint64_t *_pre_phi;
    uint64_t rbp_4;
    uint64_t *_pre_phi125;
    uint64_t rbx_0;
    uint64_t rdi1_0;
    uint64_t r12_4;
    struct indirect_placeholder_18_ret_type var_77;
    uint64_t r13_0;
    uint64_t r14_5;
    uint64_t local_sp_4;
    uint64_t local_sp_1;
    uint64_t rax_0;
    struct indirect_placeholder_19_ret_type var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t local_sp_10;
    uint64_t local_sp_0;
    uint64_t rbp_1;
    uint64_t r12_1;
    uint64_t r12_7;
    uint64_t r14_0;
    uint64_t rbx_7;
    uint64_t r12_0;
    uint64_t rdi1_2;
    uint64_t rbx_1;
    uint64_t var_34;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    struct indirect_placeholder_20_ret_type var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t *_pre_pre_phi;
    uint64_t local_sp_7;
    uint64_t *_pre126;
    uint64_t local_sp_3;
    uint64_t rbx_9;
    uint64_t r14_1;
    uint64_t r13_1;
    struct indirect_placeholder_22_ret_type var_45;
    uint64_t var_46;
    uint64_t var_47;
    struct indirect_placeholder_21_ret_type var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_44;
    uint64_t local_sp_12;
    uint64_t local_sp_2;
    uint64_t rax_2;
    uint64_t rdi1_3;
    uint64_t r14_2;
    uint64_t r13_3;
    uint64_t rdi1_4;
    uint64_t rbx_2;
    uint64_t r13_2;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    struct indirect_placeholder_23_ret_type var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t r14_3;
    uint64_t rbx_3;
    uint64_t r13_6;
    uint64_t var_31;
    uint64_t rbp_2;
    uint64_t r14_4;
    uint64_t r12_2;
    uint64_t rbx_4;
    uint64_t r13_4;
    uint64_t var_78;
    uint64_t rbx_8;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    struct indirect_placeholder_27_ret_type var_25;
    uint64_t rcx_0;
    uint64_t var_26;
    struct indirect_placeholder_25_ret_type var_27;
    uint64_t local_sp_11;
    uint64_t local_sp_5;
    uint64_t rbp_3;
    uint64_t r12_3;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t rbp_5;
    uint64_t r12_5;
    uint64_t rbx_5;
    uint64_t r9_0;
    uint64_t r8_0;
    uint64_t r12_6;
    unsigned char var_11;
    uint64_t var_12;
    uint64_t local_sp_9;
    uint64_t var_13;
    struct indirect_placeholder_28_ret_type var_14;
    uint64_t rbx_6;
    uint64_t rdi1_1;
    uint64_t *var_15;
    bool var_16;
    uint64_t var_17;
    uint64_t rbp_6;
    uint64_t r13_5;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t r14_6;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_9;
    uint64_t var_10;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r15();
    var_3 = init_r14();
    var_4 = init_r12();
    var_5 = init_rbx();
    var_6 = init_r13();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_6;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_1;
    *(uint64_t *)(var_0 + (-48L)) = var_5;
    var_7 = var_0 + (-56L);
    var_8 = *(unsigned char *)(rdi + 29UL);
    local_sp_8 = var_7;
    rbp_4 = rdi;
    rax_0 = 0UL;
    rax_2 = 0UL;
    rbp_2 = rdi;
    rbp_3 = rdi;
    rbp_5 = rdi;
    r12_6 = (uint64_t)var_8;
    rbx_6 = var_5;
    rbp_6 = rdi;
    r14_6 = var_3;
    if (var_8 == '\x00') {
        var_9 = var_0 + (-64L);
        *(uint64_t *)var_9 = 4211701UL;
        var_10 = indirect_placeholder_13();
        local_sp_8 = var_9;
        r12_6 = var_10;
    }
    var_11 = *(unsigned char *)4371664UL;
    var_12 = *(uint64_t *)4371744UL;
    r12_4 = r12_6;
    r12_1 = r12_6;
    r12_7 = r12_6;
    r12_2 = r12_6;
    r12_3 = r12_6;
    r12_5 = r12_6;
    local_sp_9 = local_sp_8;
    rdi1_1 = var_12;
    if ((var_11 == '\x00') || (var_12 == 0UL)) {
        var_13 = local_sp_8 + (-8L);
        *(uint64_t *)var_13 = 4211717UL;
        var_14 = indirect_placeholder_28();
        local_sp_9 = var_13;
        rbx_6 = var_14.field_1;
        rdi1_1 = *(uint64_t *)4371744UL;
    }
    var_15 = (uint64_t *)rdi;
    var_16 = ((long)*var_15 < (long)0UL);
    var_17 = rdi + 32UL;
    _pre_phi = var_15;
    r14_5 = var_17;
    local_sp_10 = local_sp_9;
    rbx_7 = rbx_6;
    rdi1_2 = rdi1_1;
    _pre_pre_phi = var_15;
    rbx_9 = rbx_6;
    local_sp_12 = local_sp_9;
    rdi1_3 = rdi1_1;
    r13_3 = var_17;
    rdi1_4 = rdi1_1;
    r14_3 = var_17;
    r13_6 = var_17;
    r14_4 = var_17;
    r13_4 = var_17;
    rbx_8 = rbx_6;
    local_sp_11 = local_sp_9;
    r13_5 = var_17;
    if (var_16) {
        while (1U)
            {
                var_31 = rdi1_4 + 1UL;
                *(uint64_t *)4371744UL = var_31;
                var_32 = local_sp_12 + (-8L);
                *(uint64_t *)var_32 = 4211471UL;
                var_33 = indirect_placeholder_2(var_31);
                local_sp_4 = var_32;
                local_sp_1 = var_32;
                local_sp_3 = var_32;
                r14_1 = r14_6;
                r13_1 = r13_6;
                local_sp_2 = var_32;
                rax_2 = var_33;
                r14_2 = r14_6;
                r13_3 = r13_6;
                rbx_2 = rbx_9;
                r13_2 = r13_6;
                r14_3 = r14_6;
                rbx_3 = rbx_9;
                r14_4 = r14_6;
                rbx_4 = rbx_9;
                r13_4 = r13_6;
                if (var_33 != 0UL) {
                    if (*(unsigned char *)(rdi + 28UL) != '\x00') {
                        loop_state_var = 3U;
                        break;
                    }
                    if ((uint64_t)(unsigned char)r12_6 != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_34 = local_sp_3 + (-8L);
                    *(uint64_t *)var_34 = 4211386UL;
                    indirect_placeholder();
                    local_sp_2 = var_34;
                    r14_2 = r14_3;
                    rbx_2 = rbx_3;
                    r13_2 = r13_3;
                }
                var_35 = *(uint64_t *)rax_2;
                var_36 = *(uint64_t *)(rax_2 + 8UL);
                var_37 = var_35 + (*(unsigned char *)((var_35 + var_36) + (-1L)) == '\n');
                var_38 = local_sp_2 + (-8L);
                *(uint64_t *)var_38 = 4211428UL;
                var_39 = indirect_placeholder_23(0UL, var_37, 0UL, r13_2, var_37, var_36);
                var_40 = var_39.field_0;
                var_41 = var_39.field_1;
                var_42 = var_39.field_2;
                var_43 = var_39.field_3;
                local_sp_6 = var_38;
                local_sp_7 = var_38;
                rbx_9 = rbx_2;
                local_sp_12 = var_38;
                r13_6 = r13_2;
                rcx_0 = var_41;
                rbx_5 = rbx_2;
                r9_0 = var_42;
                r8_0 = var_43;
                r14_6 = r14_2;
                if (var_40 != 18446744073709551614UL) {
                    loop_state_var = 2U;
                    break;
                }
                var_44 = *(uint64_t *)4371744UL;
                rdi1_0 = var_44;
                rdi1_4 = var_44;
                if (var_40 == 18446744073709551615UL) {
                    continue;
                }
                loop_state_var = 1U;
                break;
            }
        switch (loop_state_var) {
          case 2U:
            {
                *(uint64_t *)(local_sp_7 + (-8L)) = 4211759UL;
                indirect_placeholder_26(0UL, rcx_0, 4326920UL, r9_0, 0UL, r8_0, 0UL);
                *(uint64_t *)(local_sp_7 + (-16L)) = 4211764UL;
                indirect_placeholder_17(rbp_5, r12_5, rbx_5);
                abort();
            }
            break;
          case 3U:
            {
                var_78 = (uint64_t)(unsigned char)r12_2;
                *(uint64_t *)(local_sp_4 + (-8L)) = 4211622UL;
                indirect_placeholder_24(rbp_2, rsi, r14_4, r12_2, rbx_4, var_78, rbp_2, r13_4, rsi);
                abort();
            }
            break;
          case 1U:
            {
                var_73 = *_pre_phi;
                var_74 = (uint64_t)*(uint32_t *)(rbp_4 + 24UL);
                var_75 = (uint64_t)(unsigned char)r12_4;
                var_76 = var_73 + rdi1_0;
                *(uint64_t *)(local_sp_6 + (-8L)) = 4211661UL;
                indirect_placeholder_17(var_74, var_76, var_75);
                _pre_phi125 = _pre_phi;
                rbx_0 = var_76;
                if (var_75 == 0UL) {
                    *(uint64_t *)(local_sp_6 + (-16L)) = 4211741UL;
                    var_77 = indirect_placeholder_18(rbp_4, r12_4, var_76);
                    _pre_phi125 = (uint64_t *)var_77.field_0;
                    rbx_0 = var_77.field_2;
                }
                if ((long)*_pre_phi125 > (long)0UL) {
                    *(uint64_t *)4371744UL = rbx_0;
                }
                return;
            }
            break;
          case 0U:
            {
                while (1U)
                    {
                        *(uint64_t *)(local_sp_1 + (-8L)) = 4211492UL;
                        var_45 = indirect_placeholder_22();
                        var_46 = var_45.field_0;
                        var_47 = var_45.field_1;
                        *(uint64_t *)(local_sp_1 + (-16L)) = 4211497UL;
                        var_48 = indirect_placeholder_21(var_46, r12_1, var_47);
                        var_49 = var_48.field_0;
                        var_50 = var_48.field_1;
                        var_51 = var_48.field_2;
                        var_52 = local_sp_1 + (-24L);
                        *(uint64_t *)var_52 = 4211504UL;
                        indirect_placeholder();
                        r13_0 = r13_1;
                        local_sp_0 = var_52;
                        rbp_1 = var_49;
                        r14_0 = r14_1;
                        r12_0 = var_50;
                        rbx_1 = var_51;
                        while (1U)
                            {
                                var_53 = *(uint64_t *)rax_0;
                                var_54 = *(uint64_t *)(rax_0 + 8UL);
                                var_55 = var_53 + (*(unsigned char *)((var_53 + var_54) + (-1L)) == '\n');
                                var_56 = local_sp_0 + (-8L);
                                *(uint64_t *)var_56 = 4211540UL;
                                var_57 = indirect_placeholder_20(0UL, var_55, 0UL, r14_0, var_55, var_54);
                                var_58 = var_57.field_0;
                                var_59 = var_57.field_1;
                                var_60 = var_57.field_2;
                                var_61 = var_57.field_3;
                                r14_5 = r14_0;
                                r12_7 = r12_0;
                                local_sp_7 = var_56;
                                rcx_0 = var_59;
                                local_sp_5 = var_56;
                                rbp_3 = rbp_1;
                                r12_3 = r12_0;
                                rbp_5 = rbp_1;
                                r12_5 = r12_0;
                                rbx_5 = rbx_1;
                                r9_0 = var_60;
                                r8_0 = var_61;
                                r13_5 = r13_0;
                                switch_state_var = 0;
                                switch (var_58) {
                                  case 18446744073709551615UL:
                                    {
                                        *(uint64_t *)(local_sp_0 + (-16L)) = 4211561UL;
                                        var_62 = indirect_placeholder_19();
                                        var_63 = var_62.field_0;
                                        var_64 = var_62.field_1;
                                        var_65 = *(uint64_t *)(var_63 + 8UL);
                                        var_66 = local_sp_0 + (-24L);
                                        *(uint64_t *)var_66 = 4211573UL;
                                        var_67 = indirect_placeholder_1(var_63, var_65);
                                        var_68 = *(uint64_t *)4371744UL;
                                        local_sp_10 = var_66;
                                        rbp_6 = var_67;
                                        rbx_7 = var_64;
                                        rdi1_2 = var_68;
                                        var_69 = rdi1_2 + 1UL;
                                        *(uint64_t *)4371744UL = var_69;
                                        var_70 = local_sp_10 + (-8L);
                                        *(uint64_t *)var_70 = 4211596UL;
                                        var_71 = indirect_placeholder_2(var_69);
                                        r13_0 = r13_5;
                                        local_sp_4 = var_70;
                                        local_sp_1 = var_70;
                                        rax_0 = var_71;
                                        local_sp_0 = var_70;
                                        rbp_1 = rbp_6;
                                        r12_1 = r12_7;
                                        r14_0 = r14_5;
                                        r12_0 = r12_7;
                                        rbx_1 = rbx_7;
                                        r14_1 = r14_5;
                                        r13_1 = r13_5;
                                        rbp_2 = rbp_6;
                                        r14_4 = r14_5;
                                        r12_2 = r12_7;
                                        rbx_4 = rbx_7;
                                        r13_4 = r13_5;
                                        if (var_71 == 0UL) {
                                            continue;
                                        }
                                        loop_state_var = 2U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    break;
                                  case 18446744073709551614UL:
                                    {
                                        loop_state_var = 0U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    break;
                                  default:
                                    {
                                        _pre126 = (uint64_t *)rbp_1;
                                        _pre_pre_phi = _pre126;
                                        loop_state_var = 1U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    break;
                                }
                                if (switch_state_var)
                                    break;
                            }
                        switch_state_var = 0;
                        switch (loop_state_var) {
                          case 2U:
                            {
                                if (*(unsigned char *)(rbp_6 + 28UL) == '\x00') {
                                    continue;
                                }
                                loop_state_var = 2U;
                                switch_state_var = 1;
                                break;
                            }
                            break;
                          case 0U:
                            {
                                loop_state_var = 0U;
                                switch_state_var = 1;
                                break;
                            }
                            break;
                          case 1U:
                            {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            break;
                        }
                        if (switch_state_var)
                            break;
                    }
                switch (loop_state_var) {
                  case 0U:
                    {
                        *(uint64_t *)(local_sp_7 + (-8L)) = 4211759UL;
                        indirect_placeholder_26(0UL, rcx_0, 4326920UL, r9_0, 0UL, r8_0, 0UL);
                        *(uint64_t *)(local_sp_7 + (-16L)) = 4211764UL;
                        indirect_placeholder_17(rbp_5, r12_5, rbx_5);
                        abort();
                    }
                    break;
                  case 2U:
                    {
                        var_78 = (uint64_t)(unsigned char)r12_2;
                        *(uint64_t *)(local_sp_4 + (-8L)) = 4211622UL;
                        indirect_placeholder_24(rbp_2, rsi, r14_4, r12_2, rbx_4, var_78, rbp_2, r13_4, rsi);
                        abort();
                    }
                    break;
                  case 1U:
                    {
                        var_72 = *(uint64_t *)4371744UL;
                        local_sp_6 = local_sp_5;
                        _pre_phi = _pre_pre_phi;
                        rbp_4 = rbp_3;
                        rdi1_0 = var_72;
                        r12_4 = r12_3;
                        var_73 = *_pre_phi;
                        var_74 = (uint64_t)*(uint32_t *)(rbp_4 + 24UL);
                        var_75 = (uint64_t)(unsigned char)r12_4;
                        var_76 = var_73 + rdi1_0;
                        *(uint64_t *)(local_sp_6 + (-8L)) = 4211661UL;
                        indirect_placeholder_17(var_74, var_76, var_75);
                        _pre_phi125 = _pre_phi;
                        rbx_0 = var_76;
                        if (var_75 == 0UL) {
                            *(uint64_t *)(local_sp_6 + (-16L)) = 4211741UL;
                            var_77 = indirect_placeholder_18(rbp_4, r12_4, var_76);
                            _pre_phi125 = (uint64_t *)var_77.field_0;
                            rbx_0 = var_77.field_2;
                        }
                        if ((long)*_pre_phi125 > (long)0UL) {
                            *(uint64_t *)4371744UL = rbx_0;
                        }
                        return;
                    }
                    break;
                }
            }
            break;
        }
    }
    if ((uint64_t)(unsigned char)r12_6 != 0UL) {
        var_69 = rdi1_2 + 1UL;
        *(uint64_t *)4371744UL = var_69;
        var_70 = local_sp_10 + (-8L);
        *(uint64_t *)var_70 = 4211596UL;
        var_71 = indirect_placeholder_2(var_69);
        r13_0 = r13_5;
        local_sp_4 = var_70;
        local_sp_1 = var_70;
        rax_0 = var_71;
        local_sp_0 = var_70;
        rbp_1 = rbp_6;
        r12_1 = r12_7;
        r14_0 = r14_5;
        r12_0 = r12_7;
        rbx_1 = rbx_7;
        r14_1 = r14_5;
        r13_1 = r13_5;
        rbp_2 = rbp_6;
        r14_4 = r14_5;
        r12_2 = r12_7;
        rbx_4 = rbx_7;
        r13_4 = r13_5;
        if (var_71 == 0UL) {
            if (*(unsigned char *)(rbp_6 + 28UL) == '\x00') {
                var_78 = (uint64_t)(unsigned char)r12_2;
                *(uint64_t *)(local_sp_4 + (-8L)) = 4211622UL;
                indirect_placeholder_24(rbp_2, rsi, r14_4, r12_2, rbx_4, var_78, rbp_2, r13_4, rsi);
                abort();
            }
            while (1U)
                {
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4211492UL;
                    var_45 = indirect_placeholder_22();
                    var_46 = var_45.field_0;
                    var_47 = var_45.field_1;
                    *(uint64_t *)(local_sp_1 + (-16L)) = 4211497UL;
                    var_48 = indirect_placeholder_21(var_46, r12_1, var_47);
                    var_49 = var_48.field_0;
                    var_50 = var_48.field_1;
                    var_51 = var_48.field_2;
                    var_52 = local_sp_1 + (-24L);
                    *(uint64_t *)var_52 = 4211504UL;
                    indirect_placeholder();
                    r13_0 = r13_1;
                    local_sp_0 = var_52;
                    rbp_1 = var_49;
                    r14_0 = r14_1;
                    r12_0 = var_50;
                    rbx_1 = var_51;
                    while (1U)
                        {
                            var_53 = *(uint64_t *)rax_0;
                            var_54 = *(uint64_t *)(rax_0 + 8UL);
                            var_55 = var_53 + (*(unsigned char *)((var_53 + var_54) + (-1L)) == '\n');
                            var_56 = local_sp_0 + (-8L);
                            *(uint64_t *)var_56 = 4211540UL;
                            var_57 = indirect_placeholder_20(0UL, var_55, 0UL, r14_0, var_55, var_54);
                            var_58 = var_57.field_0;
                            var_59 = var_57.field_1;
                            var_60 = var_57.field_2;
                            var_61 = var_57.field_3;
                            r14_5 = r14_0;
                            r12_7 = r12_0;
                            local_sp_7 = var_56;
                            rcx_0 = var_59;
                            local_sp_5 = var_56;
                            rbp_3 = rbp_1;
                            r12_3 = r12_0;
                            rbp_5 = rbp_1;
                            r12_5 = r12_0;
                            rbx_5 = rbx_1;
                            r9_0 = var_60;
                            r8_0 = var_61;
                            r13_5 = r13_0;
                            switch_state_var = 0;
                            switch (var_58) {
                              case 18446744073709551615UL:
                                {
                                    *(uint64_t *)(local_sp_0 + (-16L)) = 4211561UL;
                                    var_62 = indirect_placeholder_19();
                                    var_63 = var_62.field_0;
                                    var_64 = var_62.field_1;
                                    var_65 = *(uint64_t *)(var_63 + 8UL);
                                    var_66 = local_sp_0 + (-24L);
                                    *(uint64_t *)var_66 = 4211573UL;
                                    var_67 = indirect_placeholder_1(var_63, var_65);
                                    var_68 = *(uint64_t *)4371744UL;
                                    local_sp_10 = var_66;
                                    rbp_6 = var_67;
                                    rbx_7 = var_64;
                                    rdi1_2 = var_68;
                                    var_69 = rdi1_2 + 1UL;
                                    *(uint64_t *)4371744UL = var_69;
                                    var_70 = local_sp_10 + (-8L);
                                    *(uint64_t *)var_70 = 4211596UL;
                                    var_71 = indirect_placeholder_2(var_69);
                                    r13_0 = r13_5;
                                    local_sp_4 = var_70;
                                    local_sp_1 = var_70;
                                    rax_0 = var_71;
                                    local_sp_0 = var_70;
                                    rbp_1 = rbp_6;
                                    r12_1 = r12_7;
                                    r14_0 = r14_5;
                                    r12_0 = r12_7;
                                    rbx_1 = rbx_7;
                                    r14_1 = r14_5;
                                    r13_1 = r13_5;
                                    rbp_2 = rbp_6;
                                    r14_4 = r14_5;
                                    r12_2 = r12_7;
                                    rbx_4 = rbx_7;
                                    r13_4 = r13_5;
                                    if (var_71 == 0UL) {
                                        continue;
                                    }
                                    loop_state_var = 2U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              case 18446744073709551614UL:
                                {
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              default:
                                {
                                    _pre126 = (uint64_t *)rbp_1;
                                    _pre_pre_phi = _pre126;
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                            }
                            if (switch_state_var)
                                break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 2U:
                        {
                            if (*(unsigned char *)(rbp_6 + 28UL) == '\x00') {
                                continue;
                            }
                            loop_state_var = 2U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 0U:
                        {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 1U:
                        {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            switch (loop_state_var) {
              case 0U:
                {
                    *(uint64_t *)(local_sp_7 + (-8L)) = 4211759UL;
                    indirect_placeholder_26(0UL, rcx_0, 4326920UL, r9_0, 0UL, r8_0, 0UL);
                    *(uint64_t *)(local_sp_7 + (-16L)) = 4211764UL;
                    indirect_placeholder_17(rbp_5, r12_5, rbx_5);
                    abort();
                }
                break;
              case 2U:
                {
                    var_78 = (uint64_t)(unsigned char)r12_2;
                    *(uint64_t *)(local_sp_4 + (-8L)) = 4211622UL;
                    indirect_placeholder_24(rbp_2, rsi, r14_4, r12_2, rbx_4, var_78, rbp_2, r13_4, rsi);
                    abort();
                }
                break;
              case 1U:
                {
                    var_72 = *(uint64_t *)4371744UL;
                    local_sp_6 = local_sp_5;
                    _pre_phi = _pre_pre_phi;
                    rbp_4 = rbp_3;
                    rdi1_0 = var_72;
                    r12_4 = r12_3;
                    var_73 = *_pre_phi;
                    var_74 = (uint64_t)*(uint32_t *)(rbp_4 + 24UL);
                    var_75 = (uint64_t)(unsigned char)r12_4;
                    var_76 = var_73 + rdi1_0;
                    *(uint64_t *)(local_sp_6 + (-8L)) = 4211661UL;
                    indirect_placeholder_17(var_74, var_76, var_75);
                    _pre_phi125 = _pre_phi;
                    rbx_0 = var_76;
                    if (var_75 == 0UL) {
                        *(uint64_t *)(local_sp_6 + (-16L)) = 4211741UL;
                        var_77 = indirect_placeholder_18(rbp_4, r12_4, var_76);
                        _pre_phi125 = (uint64_t *)var_77.field_0;
                        rbx_0 = var_77.field_2;
                    }
                    if ((long)*_pre_phi125 > (long)0UL) {
                        *(uint64_t *)4371744UL = rbx_0;
                    }
                    return;
                }
                break;
            }
        }
        while (1U)
            {
                var_53 = *(uint64_t *)rax_0;
                var_54 = *(uint64_t *)(rax_0 + 8UL);
                var_55 = var_53 + (*(unsigned char *)((var_53 + var_54) + (-1L)) == '\n');
                var_56 = local_sp_0 + (-8L);
                *(uint64_t *)var_56 = 4211540UL;
                var_57 = indirect_placeholder_20(0UL, var_55, 0UL, r14_0, var_55, var_54);
                var_58 = var_57.field_0;
                var_59 = var_57.field_1;
                var_60 = var_57.field_2;
                var_61 = var_57.field_3;
                r14_5 = r14_0;
                r12_7 = r12_0;
                local_sp_7 = var_56;
                rcx_0 = var_59;
                local_sp_5 = var_56;
                rbp_3 = rbp_1;
                r12_3 = r12_0;
                rbp_5 = rbp_1;
                r12_5 = r12_0;
                rbx_5 = rbx_1;
                r9_0 = var_60;
                r8_0 = var_61;
                r13_5 = r13_0;
                switch_state_var = 0;
                switch (var_58) {
                  case 18446744073709551615UL:
                    {
                        *(uint64_t *)(local_sp_0 + (-16L)) = 4211561UL;
                        var_62 = indirect_placeholder_19();
                        var_63 = var_62.field_0;
                        var_64 = var_62.field_1;
                        var_65 = *(uint64_t *)(var_63 + 8UL);
                        var_66 = local_sp_0 + (-24L);
                        *(uint64_t *)var_66 = 4211573UL;
                        var_67 = indirect_placeholder_1(var_63, var_65);
                        var_68 = *(uint64_t *)4371744UL;
                        local_sp_10 = var_66;
                        rbp_6 = var_67;
                        rbx_7 = var_64;
                        rdi1_2 = var_68;
                        var_69 = rdi1_2 + 1UL;
                        *(uint64_t *)4371744UL = var_69;
                        var_70 = local_sp_10 + (-8L);
                        *(uint64_t *)var_70 = 4211596UL;
                        var_71 = indirect_placeholder_2(var_69);
                        r13_0 = r13_5;
                        local_sp_4 = var_70;
                        local_sp_1 = var_70;
                        rax_0 = var_71;
                        local_sp_0 = var_70;
                        rbp_1 = rbp_6;
                        r12_1 = r12_7;
                        r14_0 = r14_5;
                        r12_0 = r12_7;
                        rbx_1 = rbx_7;
                        r14_1 = r14_5;
                        r13_1 = r13_5;
                        rbp_2 = rbp_6;
                        r14_4 = r14_5;
                        r12_2 = r12_7;
                        rbx_4 = rbx_7;
                        r13_4 = r13_5;
                        if (var_71 == 0UL) {
                            continue;
                        }
                        loop_state_var = 2U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 18446744073709551614UL:
                    {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  default:
                    {
                        _pre126 = (uint64_t *)rbp_1;
                        _pre_pre_phi = _pre126;
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
        switch (loop_state_var) {
          case 0U:
            {
                *(uint64_t *)(local_sp_7 + (-8L)) = 4211759UL;
                indirect_placeholder_26(0UL, rcx_0, 4326920UL, r9_0, 0UL, r8_0, 0UL);
                *(uint64_t *)(local_sp_7 + (-16L)) = 4211764UL;
                indirect_placeholder_17(rbp_5, r12_5, rbx_5);
                abort();
            }
            break;
          case 2U:
            {
                if (*(unsigned char *)(rbp_6 + 28UL) == '\x00') {
                    var_78 = (uint64_t)(unsigned char)r12_2;
                    *(uint64_t *)(local_sp_4 + (-8L)) = 4211622UL;
                    indirect_placeholder_24(rbp_2, rsi, r14_4, r12_2, rbx_4, var_78, rbp_2, r13_4, rsi);
                    abort();
                }
                while (1U)
                    {
                        *(uint64_t *)(local_sp_1 + (-8L)) = 4211492UL;
                        var_45 = indirect_placeholder_22();
                        var_46 = var_45.field_0;
                        var_47 = var_45.field_1;
                        *(uint64_t *)(local_sp_1 + (-16L)) = 4211497UL;
                        var_48 = indirect_placeholder_21(var_46, r12_1, var_47);
                        var_49 = var_48.field_0;
                        var_50 = var_48.field_1;
                        var_51 = var_48.field_2;
                        var_52 = local_sp_1 + (-24L);
                        *(uint64_t *)var_52 = 4211504UL;
                        indirect_placeholder();
                        r13_0 = r13_1;
                        local_sp_0 = var_52;
                        rbp_1 = var_49;
                        r14_0 = r14_1;
                        r12_0 = var_50;
                        rbx_1 = var_51;
                        while (1U)
                            {
                                var_53 = *(uint64_t *)rax_0;
                                var_54 = *(uint64_t *)(rax_0 + 8UL);
                                var_55 = var_53 + (*(unsigned char *)((var_53 + var_54) + (-1L)) == '\n');
                                var_56 = local_sp_0 + (-8L);
                                *(uint64_t *)var_56 = 4211540UL;
                                var_57 = indirect_placeholder_20(0UL, var_55, 0UL, r14_0, var_55, var_54);
                                var_58 = var_57.field_0;
                                var_59 = var_57.field_1;
                                var_60 = var_57.field_2;
                                var_61 = var_57.field_3;
                                r14_5 = r14_0;
                                r12_7 = r12_0;
                                local_sp_7 = var_56;
                                rcx_0 = var_59;
                                local_sp_5 = var_56;
                                rbp_3 = rbp_1;
                                r12_3 = r12_0;
                                rbp_5 = rbp_1;
                                r12_5 = r12_0;
                                rbx_5 = rbx_1;
                                r9_0 = var_60;
                                r8_0 = var_61;
                                r13_5 = r13_0;
                                switch_state_var = 0;
                                switch (var_58) {
                                  case 18446744073709551615UL:
                                    {
                                        *(uint64_t *)(local_sp_0 + (-16L)) = 4211561UL;
                                        var_62 = indirect_placeholder_19();
                                        var_63 = var_62.field_0;
                                        var_64 = var_62.field_1;
                                        var_65 = *(uint64_t *)(var_63 + 8UL);
                                        var_66 = local_sp_0 + (-24L);
                                        *(uint64_t *)var_66 = 4211573UL;
                                        var_67 = indirect_placeholder_1(var_63, var_65);
                                        var_68 = *(uint64_t *)4371744UL;
                                        local_sp_10 = var_66;
                                        rbp_6 = var_67;
                                        rbx_7 = var_64;
                                        rdi1_2 = var_68;
                                        var_69 = rdi1_2 + 1UL;
                                        *(uint64_t *)4371744UL = var_69;
                                        var_70 = local_sp_10 + (-8L);
                                        *(uint64_t *)var_70 = 4211596UL;
                                        var_71 = indirect_placeholder_2(var_69);
                                        r13_0 = r13_5;
                                        local_sp_4 = var_70;
                                        local_sp_1 = var_70;
                                        rax_0 = var_71;
                                        local_sp_0 = var_70;
                                        rbp_1 = rbp_6;
                                        r12_1 = r12_7;
                                        r14_0 = r14_5;
                                        r12_0 = r12_7;
                                        rbx_1 = rbx_7;
                                        r14_1 = r14_5;
                                        r13_1 = r13_5;
                                        rbp_2 = rbp_6;
                                        r14_4 = r14_5;
                                        r12_2 = r12_7;
                                        rbx_4 = rbx_7;
                                        r13_4 = r13_5;
                                        if (var_71 == 0UL) {
                                            continue;
                                        }
                                        loop_state_var = 2U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    break;
                                  case 18446744073709551614UL:
                                    {
                                        loop_state_var = 0U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    break;
                                  default:
                                    {
                                        _pre126 = (uint64_t *)rbp_1;
                                        _pre_pre_phi = _pre126;
                                        loop_state_var = 1U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    break;
                                }
                                if (switch_state_var)
                                    break;
                            }
                        switch_state_var = 0;
                        switch (loop_state_var) {
                          case 2U:
                            {
                                if (*(unsigned char *)(rbp_6 + 28UL) == '\x00') {
                                    continue;
                                }
                                loop_state_var = 2U;
                                switch_state_var = 1;
                                break;
                            }
                            break;
                          case 0U:
                            {
                                loop_state_var = 0U;
                                switch_state_var = 1;
                                break;
                            }
                            break;
                          case 1U:
                            {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            break;
                        }
                        if (switch_state_var)
                            break;
                    }
                switch (loop_state_var) {
                  case 0U:
                    {
                        *(uint64_t *)(local_sp_7 + (-8L)) = 4211759UL;
                        indirect_placeholder_26(0UL, rcx_0, 4326920UL, r9_0, 0UL, r8_0, 0UL);
                        *(uint64_t *)(local_sp_7 + (-16L)) = 4211764UL;
                        indirect_placeholder_17(rbp_5, r12_5, rbx_5);
                        abort();
                    }
                    break;
                  case 2U:
                    {
                        var_78 = (uint64_t)(unsigned char)r12_2;
                        *(uint64_t *)(local_sp_4 + (-8L)) = 4211622UL;
                        indirect_placeholder_24(rbp_2, rsi, r14_4, r12_2, rbx_4, var_78, rbp_2, r13_4, rsi);
                        abort();
                    }
                    break;
                  case 1U:
                    {
                        var_72 = *(uint64_t *)4371744UL;
                        local_sp_6 = local_sp_5;
                        _pre_phi = _pre_pre_phi;
                        rbp_4 = rbp_3;
                        rdi1_0 = var_72;
                        r12_4 = r12_3;
                        var_73 = *_pre_phi;
                        var_74 = (uint64_t)*(uint32_t *)(rbp_4 + 24UL);
                        var_75 = (uint64_t)(unsigned char)r12_4;
                        var_76 = var_73 + rdi1_0;
                        *(uint64_t *)(local_sp_6 + (-8L)) = 4211661UL;
                        indirect_placeholder_17(var_74, var_76, var_75);
                        _pre_phi125 = _pre_phi;
                        rbx_0 = var_76;
                        if (var_75 == 0UL) {
                            *(uint64_t *)(local_sp_6 + (-16L)) = 4211741UL;
                            var_77 = indirect_placeholder_18(rbp_4, r12_4, var_76);
                            _pre_phi125 = (uint64_t *)var_77.field_0;
                            rbx_0 = var_77.field_2;
                        }
                        if ((long)*_pre_phi125 > (long)0UL) {
                            *(uint64_t *)4371744UL = rbx_0;
                        }
                        return;
                    }
                    break;
                }
            }
            break;
          case 1U:
            {
                var_72 = *(uint64_t *)4371744UL;
                local_sp_6 = local_sp_5;
                _pre_phi = _pre_pre_phi;
                rbp_4 = rbp_3;
                rdi1_0 = var_72;
                r12_4 = r12_3;
                var_73 = *_pre_phi;
                var_74 = (uint64_t)*(uint32_t *)(rbp_4 + 24UL);
                var_75 = (uint64_t)(unsigned char)r12_4;
                var_76 = var_73 + rdi1_0;
                *(uint64_t *)(local_sp_6 + (-8L)) = 4211661UL;
                indirect_placeholder_17(var_74, var_76, var_75);
                _pre_phi125 = _pre_phi;
                rbx_0 = var_76;
                if (var_75 == 0UL) {
                    *(uint64_t *)(local_sp_6 + (-16L)) = 4211741UL;
                    var_77 = indirect_placeholder_18(rbp_4, r12_4, var_76);
                    _pre_phi125 = (uint64_t *)var_77.field_0;
                    rbx_0 = var_77.field_2;
                }
                if ((long)*_pre_phi125 > (long)0UL) {
                    *(uint64_t *)4371744UL = rbx_0;
                }
                return;
            }
            break;
        }
    }
    while (1U)
        {
            var_18 = rdi1_3 + 1UL;
            *(uint64_t *)4371744UL = var_18;
            var_19 = local_sp_11 + (-8L);
            *(uint64_t *)var_19 = 4211364UL;
            var_20 = indirect_placeholder_2(var_18);
            local_sp_4 = var_19;
            local_sp_3 = var_19;
            rbx_3 = rbx_8;
            rbx_4 = rbx_8;
            rbx_5 = rbx_8;
            if (var_20 == 0UL) {
                var_21 = *(uint64_t *)var_20;
                var_22 = *(uint64_t *)(var_20 + 8UL);
                var_23 = var_21 + (*(unsigned char *)((var_21 + var_22) + (-1L)) == '\n');
                var_24 = local_sp_11 + (-16L);
                *(uint64_t *)var_24 = 4211316UL;
                var_25 = indirect_placeholder_27(0UL, var_23, 0UL, var_17, var_23, var_22);
                local_sp_5 = var_24;
                local_sp_7 = var_24;
                switch_state_var = 0;
                switch (var_25.field_0) {
                  case 18446744073709551615UL:
                    {
                        var_26 = local_sp_11 + (-24L);
                        *(uint64_t *)var_26 = 4211341UL;
                        var_27 = indirect_placeholder_25();
                        local_sp_11 = var_26;
                        rbx_8 = var_27.field_1;
                        rdi1_3 = *(uint64_t *)4371744UL;
                        continue;
                    }
                    break;
                  case 18446744073709551614UL:
                    {
                        var_28 = var_25.field_1;
                        var_29 = var_25.field_2;
                        var_30 = var_25.field_3;
                        rcx_0 = var_28;
                        r9_0 = var_29;
                        r8_0 = var_30;
                        loop_state_var = 2U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  default:
                    {
                        loop_state_var = 3U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
            if (*(unsigned char *)(rdi + 28UL) != '\x00') {
                loop_state_var = 4U;
                break;
            }
            var_34 = local_sp_3 + (-8L);
            *(uint64_t *)var_34 = 4211386UL;
            indirect_placeholder();
            local_sp_2 = var_34;
            r14_2 = r14_3;
            rbx_2 = rbx_3;
            r13_2 = r13_3;
            var_35 = *(uint64_t *)rax_2;
            var_36 = *(uint64_t *)(rax_2 + 8UL);
            var_37 = var_35 + (*(unsigned char *)((var_35 + var_36) + (-1L)) == '\n');
            var_38 = local_sp_2 + (-8L);
            *(uint64_t *)var_38 = 4211428UL;
            var_39 = indirect_placeholder_23(0UL, var_37, 0UL, r13_2, var_37, var_36);
            var_40 = var_39.field_0;
            var_41 = var_39.field_1;
            var_42 = var_39.field_2;
            var_43 = var_39.field_3;
            local_sp_6 = var_38;
            local_sp_7 = var_38;
            rbx_9 = rbx_2;
            local_sp_12 = var_38;
            r13_6 = r13_2;
            rcx_0 = var_41;
            rbx_5 = rbx_2;
            r9_0 = var_42;
            r8_0 = var_43;
            r14_6 = r14_2;
            if (var_40 != 18446744073709551614UL) {
                loop_state_var = 2U;
                break;
            }
            var_44 = *(uint64_t *)4371744UL;
            rdi1_0 = var_44;
            rdi1_4 = var_44;
            if (var_40 != 18446744073709551615UL) {
                loop_state_var = 1U;
                break;
            }
            loop_state_var = 0U;
            break;
        }
    switch (loop_state_var) {
      case 2U:
        {
            *(uint64_t *)(local_sp_7 + (-8L)) = 4211759UL;
            indirect_placeholder_26(0UL, rcx_0, 4326920UL, r9_0, 0UL, r8_0, 0UL);
            *(uint64_t *)(local_sp_7 + (-16L)) = 4211764UL;
            indirect_placeholder_17(rbp_5, r12_5, rbx_5);
            abort();
        }
        break;
      case 4U:
        {
            var_78 = (uint64_t)(unsigned char)r12_2;
            *(uint64_t *)(local_sp_4 + (-8L)) = 4211622UL;
            indirect_placeholder_24(rbp_2, rsi, r14_4, r12_2, rbx_4, var_78, rbp_2, r13_4, rsi);
            abort();
        }
        break;
      case 1U:
        {
            while (1U)
                {
                    var_31 = rdi1_4 + 1UL;
                    *(uint64_t *)4371744UL = var_31;
                    var_32 = local_sp_12 + (-8L);
                    *(uint64_t *)var_32 = 4211471UL;
                    var_33 = indirect_placeholder_2(var_31);
                    local_sp_4 = var_32;
                    local_sp_1 = var_32;
                    local_sp_3 = var_32;
                    r14_1 = r14_6;
                    r13_1 = r13_6;
                    local_sp_2 = var_32;
                    rax_2 = var_33;
                    r14_2 = r14_6;
                    r13_3 = r13_6;
                    rbx_2 = rbx_9;
                    r13_2 = r13_6;
                    r14_3 = r14_6;
                    rbx_3 = rbx_9;
                    r14_4 = r14_6;
                    rbx_4 = rbx_9;
                    r13_4 = r13_6;
                    if (var_33 != 0UL) {
                        if (*(unsigned char *)(rdi + 28UL) != '\x00') {
                            loop_state_var = 3U;
                            break;
                        }
                        if ((uint64_t)(unsigned char)r12_6 != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        var_34 = local_sp_3 + (-8L);
                        *(uint64_t *)var_34 = 4211386UL;
                        indirect_placeholder();
                        local_sp_2 = var_34;
                        r14_2 = r14_3;
                        rbx_2 = rbx_3;
                        r13_2 = r13_3;
                    }
                    var_35 = *(uint64_t *)rax_2;
                    var_36 = *(uint64_t *)(rax_2 + 8UL);
                    var_37 = var_35 + (*(unsigned char *)((var_35 + var_36) + (-1L)) == '\n');
                    var_38 = local_sp_2 + (-8L);
                    *(uint64_t *)var_38 = 4211428UL;
                    var_39 = indirect_placeholder_23(0UL, var_37, 0UL, r13_2, var_37, var_36);
                    var_40 = var_39.field_0;
                    var_41 = var_39.field_1;
                    var_42 = var_39.field_2;
                    var_43 = var_39.field_3;
                    local_sp_6 = var_38;
                    local_sp_7 = var_38;
                    rbx_9 = rbx_2;
                    local_sp_12 = var_38;
                    r13_6 = r13_2;
                    rcx_0 = var_41;
                    rbx_5 = rbx_2;
                    r9_0 = var_42;
                    r8_0 = var_43;
                    r14_6 = r14_2;
                    if (var_40 != 18446744073709551614UL) {
                        loop_state_var = 2U;
                        break;
                    }
                    var_44 = *(uint64_t *)4371744UL;
                    rdi1_0 = var_44;
                    rdi1_4 = var_44;
                    if (var_40 == 18446744073709551615UL) {
                        continue;
                    }
                    loop_state_var = 1U;
                    break;
                }
            switch (loop_state_var) {
              case 2U:
                {
                    *(uint64_t *)(local_sp_7 + (-8L)) = 4211759UL;
                    indirect_placeholder_26(0UL, rcx_0, 4326920UL, r9_0, 0UL, r8_0, 0UL);
                    *(uint64_t *)(local_sp_7 + (-16L)) = 4211764UL;
                    indirect_placeholder_17(rbp_5, r12_5, rbx_5);
                    abort();
                }
                break;
              case 3U:
                {
                    var_78 = (uint64_t)(unsigned char)r12_2;
                    *(uint64_t *)(local_sp_4 + (-8L)) = 4211622UL;
                    indirect_placeholder_24(rbp_2, rsi, r14_4, r12_2, rbx_4, var_78, rbp_2, r13_4, rsi);
                    abort();
                }
                break;
              case 1U:
                {
                    var_73 = *_pre_phi;
                    var_74 = (uint64_t)*(uint32_t *)(rbp_4 + 24UL);
                    var_75 = (uint64_t)(unsigned char)r12_4;
                    var_76 = var_73 + rdi1_0;
                    *(uint64_t *)(local_sp_6 + (-8L)) = 4211661UL;
                    indirect_placeholder_17(var_74, var_76, var_75);
                    _pre_phi125 = _pre_phi;
                    rbx_0 = var_76;
                    if (var_75 == 0UL) {
                        *(uint64_t *)(local_sp_6 + (-16L)) = 4211741UL;
                        var_77 = indirect_placeholder_18(rbp_4, r12_4, var_76);
                        _pre_phi125 = (uint64_t *)var_77.field_0;
                        rbx_0 = var_77.field_2;
                    }
                    if ((long)*_pre_phi125 > (long)0UL) {
                        *(uint64_t *)4371744UL = rbx_0;
                    }
                    return;
                }
                break;
              case 0U:
                {
                    while (1U)
                        {
                            *(uint64_t *)(local_sp_1 + (-8L)) = 4211492UL;
                            var_45 = indirect_placeholder_22();
                            var_46 = var_45.field_0;
                            var_47 = var_45.field_1;
                            *(uint64_t *)(local_sp_1 + (-16L)) = 4211497UL;
                            var_48 = indirect_placeholder_21(var_46, r12_1, var_47);
                            var_49 = var_48.field_0;
                            var_50 = var_48.field_1;
                            var_51 = var_48.field_2;
                            var_52 = local_sp_1 + (-24L);
                            *(uint64_t *)var_52 = 4211504UL;
                            indirect_placeholder();
                            r13_0 = r13_1;
                            local_sp_0 = var_52;
                            rbp_1 = var_49;
                            r14_0 = r14_1;
                            r12_0 = var_50;
                            rbx_1 = var_51;
                            while (1U)
                                {
                                    var_53 = *(uint64_t *)rax_0;
                                    var_54 = *(uint64_t *)(rax_0 + 8UL);
                                    var_55 = var_53 + (*(unsigned char *)((var_53 + var_54) + (-1L)) == '\n');
                                    var_56 = local_sp_0 + (-8L);
                                    *(uint64_t *)var_56 = 4211540UL;
                                    var_57 = indirect_placeholder_20(0UL, var_55, 0UL, r14_0, var_55, var_54);
                                    var_58 = var_57.field_0;
                                    var_59 = var_57.field_1;
                                    var_60 = var_57.field_2;
                                    var_61 = var_57.field_3;
                                    r14_5 = r14_0;
                                    r12_7 = r12_0;
                                    local_sp_7 = var_56;
                                    rcx_0 = var_59;
                                    local_sp_5 = var_56;
                                    rbp_3 = rbp_1;
                                    r12_3 = r12_0;
                                    rbp_5 = rbp_1;
                                    r12_5 = r12_0;
                                    rbx_5 = rbx_1;
                                    r9_0 = var_60;
                                    r8_0 = var_61;
                                    r13_5 = r13_0;
                                    switch_state_var = 0;
                                    switch (var_58) {
                                      case 18446744073709551615UL:
                                        {
                                            *(uint64_t *)(local_sp_0 + (-16L)) = 4211561UL;
                                            var_62 = indirect_placeholder_19();
                                            var_63 = var_62.field_0;
                                            var_64 = var_62.field_1;
                                            var_65 = *(uint64_t *)(var_63 + 8UL);
                                            var_66 = local_sp_0 + (-24L);
                                            *(uint64_t *)var_66 = 4211573UL;
                                            var_67 = indirect_placeholder_1(var_63, var_65);
                                            var_68 = *(uint64_t *)4371744UL;
                                            local_sp_10 = var_66;
                                            rbp_6 = var_67;
                                            rbx_7 = var_64;
                                            rdi1_2 = var_68;
                                            var_69 = rdi1_2 + 1UL;
                                            *(uint64_t *)4371744UL = var_69;
                                            var_70 = local_sp_10 + (-8L);
                                            *(uint64_t *)var_70 = 4211596UL;
                                            var_71 = indirect_placeholder_2(var_69);
                                            r13_0 = r13_5;
                                            local_sp_4 = var_70;
                                            local_sp_1 = var_70;
                                            rax_0 = var_71;
                                            local_sp_0 = var_70;
                                            rbp_1 = rbp_6;
                                            r12_1 = r12_7;
                                            r14_0 = r14_5;
                                            r12_0 = r12_7;
                                            rbx_1 = rbx_7;
                                            r14_1 = r14_5;
                                            r13_1 = r13_5;
                                            rbp_2 = rbp_6;
                                            r14_4 = r14_5;
                                            r12_2 = r12_7;
                                            rbx_4 = rbx_7;
                                            r13_4 = r13_5;
                                            if (var_71 == 0UL) {
                                                continue;
                                            }
                                            loop_state_var = 2U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                        break;
                                      case 18446744073709551614UL:
                                        {
                                            loop_state_var = 0U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                        break;
                                      default:
                                        {
                                            _pre126 = (uint64_t *)rbp_1;
                                            _pre_pre_phi = _pre126;
                                            loop_state_var = 1U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                        break;
                                    }
                                    if (switch_state_var)
                                        break;
                                }
                            switch_state_var = 0;
                            switch (loop_state_var) {
                              case 2U:
                                {
                                    if (*(unsigned char *)(rbp_6 + 28UL) == '\x00') {
                                        continue;
                                    }
                                    loop_state_var = 2U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              case 0U:
                                {
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              case 1U:
                                {
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                            }
                            if (switch_state_var)
                                break;
                        }
                    switch (loop_state_var) {
                      case 0U:
                        {
                            *(uint64_t *)(local_sp_7 + (-8L)) = 4211759UL;
                            indirect_placeholder_26(0UL, rcx_0, 4326920UL, r9_0, 0UL, r8_0, 0UL);
                            *(uint64_t *)(local_sp_7 + (-16L)) = 4211764UL;
                            indirect_placeholder_17(rbp_5, r12_5, rbx_5);
                            abort();
                        }
                        break;
                      case 2U:
                        {
                            var_78 = (uint64_t)(unsigned char)r12_2;
                            *(uint64_t *)(local_sp_4 + (-8L)) = 4211622UL;
                            indirect_placeholder_24(rbp_2, rsi, r14_4, r12_2, rbx_4, var_78, rbp_2, r13_4, rsi);
                            abort();
                        }
                        break;
                      case 1U:
                        {
                            var_72 = *(uint64_t *)4371744UL;
                            local_sp_6 = local_sp_5;
                            _pre_phi = _pre_pre_phi;
                            rbp_4 = rbp_3;
                            rdi1_0 = var_72;
                            r12_4 = r12_3;
                            var_73 = *_pre_phi;
                            var_74 = (uint64_t)*(uint32_t *)(rbp_4 + 24UL);
                            var_75 = (uint64_t)(unsigned char)r12_4;
                            var_76 = var_73 + rdi1_0;
                            *(uint64_t *)(local_sp_6 + (-8L)) = 4211661UL;
                            indirect_placeholder_17(var_74, var_76, var_75);
                            _pre_phi125 = _pre_phi;
                            rbx_0 = var_76;
                            if (var_75 == 0UL) {
                                *(uint64_t *)(local_sp_6 + (-16L)) = 4211741UL;
                                var_77 = indirect_placeholder_18(rbp_4, r12_4, var_76);
                                _pre_phi125 = (uint64_t *)var_77.field_0;
                                rbx_0 = var_77.field_2;
                            }
                            if ((long)*_pre_phi125 > (long)0UL) {
                                *(uint64_t *)4371744UL = rbx_0;
                            }
                            return;
                        }
                        break;
                    }
                }
                break;
            }
        }
        break;
      case 3U:
      case 0U:
        {
            switch (loop_state_var) {
              case 3U:
                {
                    var_72 = *(uint64_t *)4371744UL;
                    local_sp_6 = local_sp_5;
                    _pre_phi = _pre_pre_phi;
                    rbp_4 = rbp_3;
                    rdi1_0 = var_72;
                    r12_4 = r12_3;
                }
                break;
              case 0U:
                {
                    var_73 = *_pre_phi;
                    var_74 = (uint64_t)*(uint32_t *)(rbp_4 + 24UL);
                    var_75 = (uint64_t)(unsigned char)r12_4;
                    var_76 = var_73 + rdi1_0;
                    *(uint64_t *)(local_sp_6 + (-8L)) = 4211661UL;
                    indirect_placeholder_17(var_74, var_76, var_75);
                    _pre_phi125 = _pre_phi;
                    rbx_0 = var_76;
                    if (var_75 == 0UL) {
                        *(uint64_t *)(local_sp_6 + (-16L)) = 4211741UL;
                        var_77 = indirect_placeholder_18(rbp_4, r12_4, var_76);
                        _pre_phi125 = (uint64_t *)var_77.field_0;
                        rbx_0 = var_77.field_2;
                    }
                    if ((long)*_pre_phi125 > (long)0UL) {
                        *(uint64_t *)4371744UL = rbx_0;
                    }
                    return;
                }
                break;
            }
        }
        break;
    }
}
