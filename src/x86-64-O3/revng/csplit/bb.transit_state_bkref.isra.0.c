typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_transit_state_bkref_isra_0_ret_type;
struct indirect_placeholder_254_ret_type;
struct indirect_placeholder_255_ret_type;
struct indirect_placeholder_256_ret_type;
struct indirect_placeholder_257_ret_type;
struct indirect_placeholder_258_ret_type;
struct indirect_placeholder_259_ret_type;
struct indirect_placeholder_260_ret_type;
struct indirect_placeholder_261_ret_type;
struct bb_transit_state_bkref_isra_0_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_254_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_255_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_256_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_257_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_258_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_259_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_260_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_261_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r10(void);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern struct indirect_placeholder_254_ret_type indirect_placeholder_254(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_255_ret_type indirect_placeholder_255(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_256_ret_type indirect_placeholder_256(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_257_ret_type indirect_placeholder_257(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_258_ret_type indirect_placeholder_258(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_259_ret_type indirect_placeholder_259(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_260_ret_type indirect_placeholder_260(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_261_ret_type indirect_placeholder_261(uint64_t param_0, uint64_t param_1, uint64_t param_2);
typedef _Bool bool;
struct bb_transit_state_bkref_isra_0_ret_type bb_transit_state_bkref_isra_0(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t r9_1;
    uint64_t r8_1;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    struct indirect_placeholder_255_ret_type var_101;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t rax_0;
    uint64_t r10_1;
    uint64_t var_105;
    struct indirect_placeholder_254_ret_type var_106;
    uint64_t var_107;
    uint64_t var_108;
    uint64_t var_109;
    uint64_t var_110;
    uint32_t *var_111;
    uint32_t var_112;
    uint64_t local_sp_2;
    uint64_t r10_4;
    uint64_t local_sp_0;
    uint64_t r10_0;
    uint64_t rdx4_0;
    uint64_t r9_0;
    uint64_t r8_0;
    uint32_t var_97;
    uint64_t local_sp_1;
    uint64_t rdx4_1;
    uint64_t r9_8;
    uint64_t r8_9;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t var_100;
    uint64_t var_102;
    uint32_t *var_103;
    uint32_t var_104;
    uint64_t r10_2;
    uint64_t r9_2;
    uint64_t r8_2;
    uint64_t local_sp_4;
    uint64_t var_80;
    uint64_t var_81;
    struct indirect_placeholder_256_ret_type var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_79;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    struct indirect_placeholder_257_ret_type var_72;
    uint64_t var_73;
    uint32_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint32_t *var_77;
    uint32_t var_78;
    uint64_t var_89;
    uint64_t var_90;
    struct indirect_placeholder_258_ret_type var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t var_45;
    uint64_t local_sp_5;
    uint64_t var_46;
    uint64_t r10_8;
    uint64_t rcx_1;
    uint64_t local_sp_3;
    uint64_t rcx_0;
    uint64_t r15_0;
    uint64_t rbx_0_ph;
    uint64_t r9_4;
    uint64_t r10_3;
    uint64_t r8_6_ph;
    uint64_t r8_4;
    uint64_t r9_3;
    uint64_t r8_3;
    uint64_t var_47;
    uint64_t *var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t *var_54;
    struct bb_transit_state_bkref_isra_0_ret_type mrv1;
    struct bb_transit_state_bkref_isra_0_ret_type mrv2;
    struct bb_transit_state_bkref_isra_0_ret_type mrv3;
    uint64_t *_pn_in_in;
    uint64_t storemerge;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    struct indirect_placeholder_259_ret_type var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t *var_62;
    uint32_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_113;
    uint64_t r15_1;
    uint64_t r10_5;
    uint64_t r9_5;
    uint64_t r8_5;
    uint64_t *var_114;
    uint64_t var_115;
    uint64_t var_116;
    uint64_t local_sp_6_ph;
    uint64_t local_sp_8;
    uint64_t local_sp_7;
    uint64_t var_15;
    uint64_t *var_16;
    uint32_t *var_17;
    uint64_t *var_18;
    uint64_t *var_19;
    uint64_t *var_20;
    uint64_t *var_21;
    uint64_t *var_22;
    uint64_t *var_23;
    uint64_t r15_2_ph;
    uint64_t r10_6_ph;
    uint64_t r9_6_ph;
    uint64_t r8_7;
    uint64_t local_sp_6;
    uint64_t r15_2;
    uint64_t r8_6;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint32_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    struct indirect_placeholder_260_ret_type var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint16_t var_35;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    struct indirect_placeholder_261_ret_type var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint32_t *var_43;
    uint32_t var_44;
    uint64_t var_36;
    struct bb_transit_state_bkref_isra_0_ret_type mrv;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r15();
    var_3 = init_r14();
    var_4 = init_r12();
    var_5 = init_r10();
    var_6 = init_rbx();
    var_7 = init_r9();
    var_8 = init_r13();
    var_9 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_8;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_1;
    *(uint64_t *)(var_0 + (-48L)) = var_6;
    var_10 = *(uint64_t *)(rdi + 72UL);
    var_11 = *(uint64_t *)rsi;
    *(uint64_t *)(var_0 + (-112L)) = rsi;
    var_12 = *(uint64_t *)(rdi + 152UL);
    *(uint64_t *)(var_0 + (-192L)) = var_10;
    var_13 = var_10 << 3UL;
    *(uint64_t *)(var_0 + (-184L)) = rdx;
    *(uint64_t *)(var_0 + (-160L)) = var_13;
    var_14 = helper_cc_compute_all_wrapper(var_11, 0UL, 0UL, 25U);
    rax_0 = 0UL;
    r9_8 = var_7;
    r8_9 = var_9;
    r10_8 = var_5;
    rbx_0_ph = var_11;
    r8_6_ph = var_9;
    r15_2_ph = 0UL;
    r10_6_ph = var_5;
    r9_6_ph = var_7;
    if ((uint64_t)(((unsigned char)(var_14 >> 4UL) ^ (unsigned char)var_14) & '\xc0') == 0UL) {
        mrv.field_0 = rax_0;
        mrv1 = mrv;
        mrv1.field_1 = r10_8;
        mrv2 = mrv1;
        mrv2.field_2 = r9_8;
        mrv3 = mrv2;
        mrv3.field_3 = r8_9;
        return mrv3;
    }
    var_15 = var_0 + (-200L);
    var_16 = (uint64_t *)var_12;
    var_17 = (uint32_t *)(rdi + 160UL);
    var_18 = (uint64_t *)(rdi + 200UL);
    var_19 = (uint64_t *)(rdi + 216UL);
    var_20 = (uint64_t *)(var_12 + 48UL);
    var_21 = (uint64_t *)(var_12 + 24UL);
    var_22 = (uint64_t *)(var_12 + 40UL);
    var_23 = (uint64_t *)(rdi + 184UL);
    local_sp_6_ph = var_15;
    while (1U)
        {
            r9_8 = r9_6_ph;
            r10_8 = r10_6_ph;
            r10_3 = r10_6_ph;
            r9_3 = r9_6_ph;
            r10_5 = r10_6_ph;
            r9_5 = r9_6_ph;
            local_sp_6 = local_sp_6_ph;
            r15_2 = r15_2_ph;
            r8_6 = r8_6_ph;
            while (1U)
                {
                    var_24 = *(uint64_t *)((r15_2 << 3UL) + **(uint64_t **)(local_sp_6 + 16UL));
                    var_25 = (var_24 << 4UL) + *var_16;
                    var_26 = var_25 + 8UL;
                    r15_1 = r15_2;
                    local_sp_7 = local_sp_6;
                    local_sp_8 = local_sp_6;
                    r8_7 = r8_6;
                    if (*(unsigned char *)var_26 == '\x04') {
                        var_36 = r15_2 + 1UL;
                        local_sp_6 = local_sp_8;
                        r15_2 = var_36;
                        r8_6 = r8_7;
                        r8_9 = r8_7;
                        if ((long)var_36 < (long)rbx_0_ph) {
                            continue;
                        }
                        loop_state_var = 0U;
                        break;
                    }
                    var_27 = *(uint32_t *)var_26;
                    var_28 = (uint64_t)var_27;
                    if ((uint64_t)(var_27 & 261888U) != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_29 = (uint64_t)*var_17;
                    var_30 = *(uint64_t *)(local_sp_6 + 8UL);
                    var_31 = local_sp_6 + (-8L);
                    *(uint64_t *)var_31 = 4266024UL;
                    var_32 = indirect_placeholder_260(var_25, var_29, rdi, var_30);
                    var_33 = var_32.field_0;
                    var_34 = var_32.field_1;
                    var_35 = (uint16_t)var_28;
                    local_sp_7 = var_31;
                    local_sp_8 = var_31;
                    r8_7 = var_34;
                    if ((uint64_t)(var_35 & (unsigned short)1024U) == 0UL) {
                        if (!(((uint64_t)(var_35 & (unsigned short)2048U) == 0UL) || ((var_33 & 1UL) == 0UL))) {
                            var_36 = r15_2 + 1UL;
                            local_sp_6 = local_sp_8;
                            r15_2 = var_36;
                            r8_6 = r8_7;
                            r8_9 = r8_7;
                            if ((long)var_36 < (long)rbx_0_ph) {
                                continue;
                            }
                            loop_state_var = 0U;
                            break;
                        }
                    }
                    if (!(((var_33 & 1UL) != 0UL) && ((uint64_t)(var_35 & (unsigned short)2048U) == 0UL))) {
                        var_36 = r15_2 + 1UL;
                        local_sp_6 = local_sp_8;
                        r15_2 = var_36;
                        r8_6 = r8_7;
                        r8_9 = r8_7;
                        if ((long)var_36 < (long)rbx_0_ph) {
                            continue;
                        }
                        loop_state_var = 0U;
                        break;
                    }
                    if (!(((uint64_t)(var_35 & (unsigned short)8192U) != 0UL) && ((var_33 & 2UL) == 0UL)) && !(((short)(uint16_t)var_27 <= (short)(unsigned short)65535U) && ((var_33 & 8UL) == 0UL))) {
                        loop_state_var = 1U;
                        break;
                    }
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 1U:
                {
                    var_37 = *(uint64_t *)(local_sp_7 + 8UL);
                    var_38 = *var_18;
                    var_39 = local_sp_7 + (-8L);
                    *(uint64_t *)var_39 = 4266080UL;
                    var_40 = indirect_placeholder_261(var_37, rdi, var_24);
                    var_41 = var_40.field_0;
                    var_42 = var_40.field_1;
                    var_43 = (uint32_t *)(local_sp_7 + 100UL);
                    var_44 = (uint32_t)var_41;
                    *var_43 = var_44;
                    rax_0 = var_41;
                    r8_9 = var_42;
                    local_sp_5 = var_39;
                    local_sp_3 = var_39;
                    r15_0 = var_38;
                    r8_3 = var_42;
                    r8_5 = var_42;
                    if ((uint64_t)var_44 == 0UL) {
                        switch_state_var = 1;
                        break;
                    }
                    var_45 = *var_18;
                    rcx_0 = var_45;
                    rax_0 = 0UL;
                    if ((long)var_38 < (long)var_45) {
                        var_114 = *(uint64_t **)(local_sp_5 + 88UL);
                        var_115 = r15_1 + 1UL;
                        var_116 = *var_114;
                        r9_8 = r9_5;
                        r8_9 = r8_5;
                        r10_8 = r10_5;
                        rbx_0_ph = var_116;
                        r8_6_ph = r8_5;
                        local_sp_6_ph = local_sp_5;
                        r15_2_ph = var_115;
                        r10_6_ph = r10_5;
                        r9_6_ph = r9_5;
                        if ((long)var_115 >= (long)var_116) {
                            continue;
                        }
                        switch_state_var = 1;
                        break;
                    }
                    var_46 = var_24 << 3UL;
                    *(uint64_t *)(local_sp_7 + 72UL) = r15_2;
                    *(uint64_t *)(local_sp_7 + 56UL) = var_46;
                    *(uint64_t *)(local_sp_7 + 64UL) = (var_24 * 24UL);
                    while (1U)
                        {
                            var_47 = (r15_0 * 40UL) + *var_19;
                            r10_4 = r10_3;
                            r9_8 = r9_3;
                            local_sp_4 = local_sp_3;
                            rcx_1 = rcx_0;
                            r9_4 = r9_3;
                            r8_4 = r8_3;
                            if (var_24 == *(uint64_t *)var_47) {
                                var_113 = r15_0 + 1UL;
                                local_sp_5 = local_sp_4;
                                local_sp_3 = local_sp_4;
                                rcx_0 = rcx_1;
                                r15_0 = var_113;
                                r10_3 = r10_4;
                                r9_3 = r9_4;
                                r8_3 = r8_4;
                                r10_5 = r10_4;
                                r9_5 = r9_4;
                                r8_5 = r8_4;
                                if ((long)var_113 < (long)rcx_1) {
                                    continue;
                                }
                                loop_state_var = 1U;
                                break;
                            }
                            var_48 = (uint64_t *)(local_sp_3 + 8UL);
                            if (*var_48 != *(uint64_t *)(var_47 + 8UL)) {
                                var_49 = *(uint64_t *)(var_47 + 24UL);
                                var_50 = *(uint64_t *)(var_47 + 16UL);
                                var_51 = *var_20;
                                var_52 = var_49 - var_50;
                                var_53 = local_sp_3 + 32UL;
                                var_54 = (uint64_t *)var_53;
                                *var_54 = var_52;
                                if (var_52 == 0UL) {
                                    _pn_in_in = *(uint64_t **)((*(uint64_t *)(local_sp_3 + 72UL) + *var_22) + 16UL);
                                } else {
                                    _pn_in_in = (uint64_t *)(*(uint64_t *)(local_sp_3 + 64UL) + *var_21);
                                }
                                storemerge = (*_pn_in_in * 24UL) + var_51;
                                var_55 = (uint64_t)*var_17;
                                var_56 = (var_49 + *var_48) - var_50;
                                var_57 = var_56 + (-1L);
                                *(uint64_t *)(local_sp_3 + (-8L)) = 4266205UL;
                                var_58 = indirect_placeholder_259(var_50, var_55, rdi, var_57);
                                var_59 = var_58.field_0;
                                var_60 = *var_54;
                                var_61 = var_56 << 3UL;
                                var_62 = (uint64_t *)(local_sp_3 + 16UL);
                                *var_62 = 0UL;
                                var_63 = (uint32_t)var_59;
                                var_64 = (uint64_t)var_63;
                                var_65 = *var_23;
                                var_66 = var_61 + var_65;
                                var_67 = *(uint64_t *)(var_60 + var_65);
                                var_68 = *(uint64_t *)var_66;
                                r10_8 = var_66;
                                if (var_67 == 0UL) {
                                    *var_62 = *(uint64_t *)(var_67 + 16UL);
                                }
                                if (var_68 == 0UL) {
                                    var_89 = local_sp_3 + 100UL;
                                    *(uint64_t *)(local_sp_3 + 40UL) = var_66;
                                    var_90 = local_sp_3 + (-16L);
                                    *(uint64_t *)var_90 = 4266557UL;
                                    var_91 = indirect_placeholder_258(var_64, storemerge, var_89, var_12);
                                    var_92 = var_91.field_0;
                                    var_93 = var_91.field_2;
                                    var_94 = var_91.field_3;
                                    var_95 = *var_54;
                                    var_96 = *var_23;
                                    *(uint64_t *)var_95 = var_92;
                                    r9_1 = var_93;
                                    r8_1 = var_94;
                                    r10_1 = var_95;
                                    local_sp_0 = var_90;
                                    r10_0 = var_95;
                                    rdx4_0 = var_96;
                                    r9_0 = var_93;
                                    r8_0 = var_94;
                                    local_sp_1 = var_90;
                                    rdx4_1 = var_96;
                                    var_97 = *(uint32_t *)(local_sp_0 + 108UL);
                                    r9_1 = r9_0;
                                    r8_1 = r8_0;
                                    r10_1 = r10_0;
                                    local_sp_1 = local_sp_0;
                                    rdx4_1 = rdx4_0;
                                    r9_8 = r9_0;
                                    r8_9 = r8_0;
                                    r10_8 = r10_0;
                                    if (*(uint64_t *)(var_61 + var_96) != 0UL & var_97 != 0U) {
                                        rax_0 = (uint64_t)var_97;
                                        loop_state_var = 0U;
                                        break;
                                    }
                                }
                                var_69 = *(uint64_t *)(var_68 + 80UL);
                                var_70 = local_sp_3 + 104UL;
                                var_71 = local_sp_3 + 40UL;
                                *(uint32_t *)var_71 = var_63;
                                *(uint64_t *)(local_sp_3 + 48UL) = var_61;
                                *(uint64_t *)(local_sp_3 + (-16L)) = 4266297UL;
                                var_72 = indirect_placeholder_257(storemerge, var_70, var_69);
                                var_73 = var_72.field_0;
                                var_74 = *(uint32_t *)var_53;
                                var_75 = *(uint64_t *)var_71;
                                var_76 = local_sp_3 + 92UL;
                                var_77 = (uint32_t *)var_76;
                                var_78 = (uint32_t)var_73;
                                *var_77 = var_78;
                                if ((uint64_t)var_78 != 0UL) {
                                    var_79 = var_58.field_1;
                                    *(uint64_t *)(local_sp_3 + (-24L)) = 4266741UL;
                                    indirect_placeholder();
                                    rax_0 = (uint64_t)*(uint32_t *)(local_sp_3 + 84UL);
                                    r8_9 = var_79;
                                    loop_state_var = 0U;
                                    break;
                                }
                                var_80 = (uint64_t)var_74;
                                var_81 = local_sp_3 + 96UL;
                                *var_54 = (var_75 + *var_23);
                                *(uint64_t *)(local_sp_3 + (-24L)) = 4266349UL;
                                var_82 = indirect_placeholder_256(var_80, var_81, var_76, var_12);
                                var_83 = var_82.field_0;
                                var_84 = var_82.field_1;
                                var_85 = var_82.field_2;
                                var_86 = var_82.field_3;
                                **(uint64_t **)(local_sp_3 + 24UL) = var_83;
                                var_87 = local_sp_3 + (-32L);
                                *(uint64_t *)var_87 = 4266370UL;
                                indirect_placeholder();
                                var_88 = *var_23;
                                r9_1 = var_85;
                                r8_1 = var_86;
                                r10_1 = var_84;
                                local_sp_0 = var_87;
                                r10_0 = var_84;
                                rdx4_0 = var_88;
                                r9_0 = var_85;
                                r8_0 = var_86;
                                local_sp_1 = var_87;
                                rdx4_1 = var_88;
                                if (*(uint64_t *)(var_61 + var_88) != 0UL) {
                                    var_97 = *(uint32_t *)(local_sp_0 + 108UL);
                                    r9_1 = r9_0;
                                    r8_1 = r8_0;
                                    r10_1 = r10_0;
                                    local_sp_1 = local_sp_0;
                                    rdx4_1 = rdx4_0;
                                    r9_8 = r9_0;
                                    r8_9 = r8_0;
                                    r10_8 = r10_0;
                                    if (var_97 == 0U) {
                                        rax_0 = (uint64_t)var_97;
                                        loop_state_var = 0U;
                                        break;
                                    }
                                }
                                local_sp_2 = local_sp_1;
                                r8_9 = r8_1;
                                r10_2 = r10_1;
                                r9_2 = r9_1;
                                r8_2 = r8_1;
                                if (*(uint64_t *)(local_sp_1 + 32UL) != 0UL) {
                                    if ((long)*(uint64_t *)(*(uint64_t *)(*(uint64_t *)(local_sp_1 + 40UL) + rdx4_1) + 16UL) <= (long)*(uint64_t *)(local_sp_1 + 24UL)) {
                                        var_98 = storemerge + 8UL;
                                        var_99 = *(uint64_t *)(local_sp_1 + 8UL);
                                        var_100 = storemerge + 16UL;
                                        *(uint64_t *)(local_sp_1 + (-8L)) = 4266651UL;
                                        var_101 = indirect_placeholder_255(var_99, r10_1, var_100, r9_1, rdi, r8_1, var_98);
                                        var_102 = var_101.field_0;
                                        var_103 = (uint32_t *)(local_sp_1 + 100UL);
                                        var_104 = (uint32_t)var_102;
                                        *var_103 = var_104;
                                        rax_0 = var_102;
                                        if ((uint64_t)var_104 != 0UL) {
                                            r10_8 = var_101.field_1;
                                            r9_8 = var_101.field_2;
                                            loop_state_var = 0U;
                                            break;
                                        }
                                        var_105 = local_sp_1 + (-16L);
                                        *(uint64_t *)var_105 = 4266673UL;
                                        var_106 = indirect_placeholder_254(var_100, rdi, var_98);
                                        var_107 = var_106.field_0;
                                        var_108 = var_106.field_1;
                                        var_109 = var_106.field_2;
                                        var_110 = var_106.field_3;
                                        var_111 = (uint32_t *)(local_sp_1 + 92UL);
                                        var_112 = (uint32_t)var_107;
                                        *var_111 = var_112;
                                        rax_0 = var_107;
                                        local_sp_2 = var_105;
                                        r9_8 = var_109;
                                        r8_9 = var_110;
                                        r10_2 = var_108;
                                        r9_2 = var_109;
                                        r8_2 = var_110;
                                        r10_8 = var_108;
                                        if ((uint64_t)var_112 != 0UL) {
                                            loop_state_var = 0U;
                                            break;
                                        }
                                    }
                                }
                                r10_4 = r10_2;
                                local_sp_4 = local_sp_2;
                                rcx_1 = *var_18;
                                r9_4 = r9_2;
                                r8_4 = r8_2;
                            }
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 1U:
                        {
                            r15_1 = *(uint64_t *)(local_sp_4 + 80UL);
                        }
                        break;
                      case 0U:
                        {
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    mrv.field_0 = rax_0;
    mrv1 = mrv;
    mrv1.field_1 = r10_8;
    mrv2 = mrv1;
    mrv2.field_2 = r9_8;
    mrv3 = mrv2;
    mrv3.field_3 = r8_9;
    return mrv3;
}
