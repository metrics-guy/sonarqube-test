typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct helper_movq_mm_T0_xmm_wrapper_ret_type;
struct type_4;
struct helper_movq_mm_T0_xmm_wrapper_359_ret_type;
struct helper_punpcklqdq_xmm_wrapper_360_ret_type;
struct type_8;
struct indirect_placeholder_268_ret_type;
struct indirect_placeholder_265_ret_type;
struct indirect_placeholder_266_ret_type;
struct indirect_placeholder_267_ret_type;
struct helper_movq_mm_T0_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_4 {
};
struct helper_movq_mm_T0_xmm_wrapper_359_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_punpcklqdq_xmm_wrapper_360_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
};
struct type_8 {
};
struct indirect_placeholder_268_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_265_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_266_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_267_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct helper_movq_mm_T0_xmm_wrapper_ret_type helper_movq_mm_T0_xmm_wrapper(struct type_4 *param_0, uint64_t param_1);
extern struct helper_movq_mm_T0_xmm_wrapper_359_ret_type helper_movq_mm_T0_xmm_wrapper_359(struct type_4 *param_0, uint64_t param_1);
extern struct helper_punpcklqdq_xmm_wrapper_360_ret_type helper_punpcklqdq_xmm_wrapper_360(struct type_8 *param_0, struct type_4 *param_1, struct type_4 *param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_268_ret_type indirect_placeholder_268(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_17(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_265_ret_type indirect_placeholder_265(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_266_ret_type indirect_placeholder_266(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8);
extern struct indirect_placeholder_267_ret_type indirect_placeholder_267(uint64_t param_0, uint64_t param_1, uint64_t param_2);
typedef _Bool bool;
void bb_sift_states_bkref_isra_0(uint64_t rcx, uint64_t rdx, uint64_t rdi, uint64_t r8, uint64_t rsi) {
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t *var_76;
    uint64_t var_77;
    uint64_t r15_2;
    uint64_t *var_53;
    uint64_t local_sp_1;
    struct helper_movq_mm_T0_xmm_wrapper_ret_type var_65;
    uint64_t var_66;
    struct helper_movq_mm_T0_xmm_wrapper_359_ret_type var_67;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_47;
    uint64_t *var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t local_sp_4;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t *var_19;
    uint64_t var_20;
    uint64_t *var_21;
    uint64_t *var_22;
    uint64_t *var_23;
    uint64_t *var_24;
    uint64_t *var_25;
    uint64_t *var_26;
    uint64_t var_27;
    uint64_t *var_28;
    uint64_t *var_29;
    uint64_t *var_30;
    uint64_t *var_31;
    uint64_t var_32;
    uint64_t *_pre_phi173;
    uint64_t var_33;
    uint64_t rdx2_2;
    uint64_t local_sp_2;
    uint64_t rax_0;
    uint64_t rdx2_1;
    uint64_t *var_34;
    uint64_t *var_35;
    uint64_t var_36;
    unsigned char var_37;
    uint64_t r15_2_ph;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t r14_1_ph;
    uint64_t r13_1_ph;
    uint64_t local_sp_3;
    uint64_t r14_1;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t storemerge_in_in;
    uint64_t storemerge;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    struct indirect_placeholder_268_ret_type var_46;
    uint64_t var_93;
    uint64_t *_pre172;
    uint64_t local_sp_6;
    uint64_t var_94;
    struct helper_punpcklqdq_xmm_wrapper_360_ret_type var_68;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t *var_10;
    uint64_t var_11;
    struct indirect_placeholder_265_ret_type var_12;
    uint64_t var_13;
    uint64_t *var_14;
    uint64_t rdx2_0;
    uint64_t local_sp_3_ph;
    uint64_t r15_0;
    uint64_t *_pre_phi174;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t *var_81;
    struct indirect_placeholder_266_ret_type var_82;
    uint64_t var_83;
    uint64_t local_sp_7;
    uint64_t local_sp_0;
    uint64_t r14_0;
    uint64_t r13_0;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    struct indirect_placeholder_267_ret_type var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t local_sp_5;
    uint64_t var_72;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r15();
    var_3 = init_r14();
    var_4 = init_r12();
    var_5 = init_rbx();
    var_6 = init_r13();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    var_7 = rdi + 216UL;
    *(uint64_t *)(var_0 + (-24L)) = var_6;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_1;
    *(uint64_t *)(var_0 + (-48L)) = var_5;
    *(uint64_t *)(var_0 + (-200L)) = rdi;
    var_8 = *(uint64_t *)(rdi + 200UL);
    var_9 = (uint64_t *)(var_0 + (-136L));
    *var_9 = rcx;
    var_10 = (uint64_t *)(var_0 + (-168L));
    *var_10 = r8;
    var_11 = var_0 + (-224L);
    *(uint64_t *)var_11 = 4268585UL;
    var_12 = indirect_placeholder_265(rdx, var_8, var_7);
    var_13 = var_12.field_0;
    var_14 = (uint64_t *)(var_0 + (-152L));
    *var_14 = var_13;
    local_sp_2 = var_11;
    rax_0 = 12UL;
    *(uint64_t *)(var_0 + (-128L)) = 0UL;
    var_15 = *(uint64_t *)(*(uint64_t *)(var_0 + (-208L)) + 152UL);
    var_16 = **(uint64_t **)(var_0 + (-144L));
    var_17 = helper_cc_compute_all_wrapper(var_16, 0UL, 0UL, 25U);
    rdx2_1 = var_16;
    if (~(var_13 != 18446744073709551615UL & (uint64_t)(((unsigned char)(var_17 >> 4UL) ^ (unsigned char)var_17) & '\xc0') != 0UL)) {
        return;
    }
    var_18 = *var_14;
    var_19 = (uint64_t *)(var_0 + (-216L));
    *var_19 = 0UL;
    *var_9 = (var_18 * 40UL);
    *(uint64_t *)(var_0 + (-160L)) = (rdx << 3UL);
    var_20 = rsi + 40UL;
    *var_10 = var_20;
    var_21 = (uint64_t *)var_15;
    var_22 = (uint64_t *)(rsi + 16UL);
    var_23 = (uint64_t *)(rsi + 24UL);
    var_24 = (uint64_t *)(var_15 + 24UL);
    var_25 = (uint64_t *)(var_15 + 40UL);
    var_26 = (uint64_t *)rsi;
    var_27 = rsi + 48UL;
    var_28 = (uint64_t *)(rsi + 8UL);
    var_29 = (uint64_t *)var_27;
    var_30 = (uint64_t *)(rsi + 32UL);
    var_31 = (uint64_t *)var_20;
    var_32 = rdx + 1UL;
    var_33 = *var_19;
    while (1U)
        {
            var_34 = *(uint64_t **)(local_sp_2 + 48UL);
            var_35 = (uint64_t *)(local_sp_2 + 8UL);
            var_36 = *(uint64_t *)((var_33 << 3UL) + *var_34);
            var_37 = *(unsigned char *)(((var_36 << 4UL) + *var_21) + 8UL);
            _pre_phi173 = var_35;
            rdx2_2 = rdx2_1;
            r13_1_ph = var_36;
            local_sp_6 = local_sp_2;
            local_sp_3_ph = local_sp_2;
            if (var_36 == *var_22) {
                if (*var_23 != rdx) {
                    var_94 = *_pre_phi173 + 1UL;
                    *_pre_phi173 = var_94;
                    var_33 = var_94;
                    local_sp_2 = local_sp_6;
                    rax_0 = 0UL;
                    rdx2_1 = rdx2_2;
                    local_sp_7 = local_sp_6;
                    if ((long)var_94 >= (long)rdx2_2) {
                        continue;
                    }
                    break;
                }
                if ((uint64_t)(var_37 + '\xfc') != 0UL) {
                    var_94 = *_pre_phi173 + 1UL;
                    *_pre_phi173 = var_94;
                    var_33 = var_94;
                    local_sp_2 = local_sp_6;
                    rax_0 = 0UL;
                    rdx2_1 = rdx2_2;
                    local_sp_7 = local_sp_6;
                    if ((long)var_94 >= (long)rdx2_2) {
                        continue;
                    }
                    break;
                }
            }
            if ((uint64_t)(var_37 + '\xfc') != 0UL) {
                var_94 = *_pre_phi173 + 1UL;
                *_pre_phi173 = var_94;
                var_33 = var_94;
                local_sp_2 = local_sp_6;
                rax_0 = 0UL;
                rdx2_1 = rdx2_2;
                local_sp_7 = local_sp_6;
                if ((long)var_94 >= (long)rdx2_2) {
                    continue;
                }
                break;
            }
            var_38 = *(uint64_t *)(local_sp_2 + 72UL);
            var_39 = *(uint64_t *)(local_sp_2 + 88UL) + *(uint64_t *)(*(uint64_t *)(local_sp_2 + 16UL) + 216UL);
            *(uint64_t *)(local_sp_2 + 32UL) = (var_36 * 24UL);
            *(uint64_t *)(local_sp_2 + 24UL) = (var_36 << 3UL);
            r15_2_ph = var_39;
            r14_1_ph = var_38;
            while (1U)
                {
                    r13_0 = r13_1_ph;
                    local_sp_3 = local_sp_3_ph;
                    r15_2 = r15_2_ph;
                    r14_1 = r14_1_ph;
                    while (1U)
                        {
                            r14_0 = r14_1;
                            local_sp_4 = local_sp_3;
                            if (r13_1_ph == *(uint64_t *)r15_2) {
                                local_sp_3 = local_sp_4;
                                local_sp_5 = local_sp_4;
                                if (*(unsigned char *)(r15_2 + 32UL) != '\x00') {
                                    loop_state_var = 1U;
                                    break;
                                }
                                r15_2 = r15_2 + 40UL;
                                r14_1 = r14_1 + 1UL;
                                continue;
                            }
                            var_40 = *(uint64_t *)(r15_2 + 24UL) - *(uint64_t *)(r15_2 + 16UL);
                            var_41 = var_40 + rdx;
                            if (var_40 == 0UL) {
                                storemerge_in_in = *(uint64_t *)((*(uint64_t *)(local_sp_3 + 32UL) + *var_25) + 16UL);
                            } else {
                                storemerge_in_in = *(uint64_t *)(local_sp_3 + 24UL) + *var_24;
                            }
                            storemerge = *(uint64_t *)storemerge_in_in;
                            var_42 = *(uint64_t *)((var_41 << 3UL) + *var_26);
                            var_43 = *(uint64_t *)(var_42 + 16UL);
                            var_44 = var_42 + 24UL;
                            var_45 = local_sp_3 + (-8L);
                            *(uint64_t *)var_45 = 4268915UL;
                            var_46 = indirect_placeholder_268(storemerge, var_43, var_44);
                            local_sp_4 = var_45;
                            var_47 = local_sp_3 + (-24L);
                            var_48 = (uint64_t *)var_47;
                            *var_48 = var_41;
                            var_49 = *(uint64_t *)(local_sp_3 + 48UL);
                            var_50 = *(uint64_t *)(local_sp_3 + 8UL);
                            *(uint64_t *)(local_sp_3 + 32UL) = var_27;
                            *(uint64_t *)(local_sp_3 + (-32L)) = 4268960UL;
                            var_51 = indirect_placeholder_9(r13_1_ph, var_27, storemerge, var_50, rdx, var_49);
                            var_52 = local_sp_3 + (-16L);
                            local_sp_1 = var_52;
                            local_sp_4 = var_52;
                            local_sp_7 = var_47;
                            if ((long)var_41 <= (long)*var_23 & var_42 != 0UL & var_46.field_0 != 0UL & (uint64_t)(unsigned char)var_51 != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 0U:
                        {
                            var_53 = (uint64_t *)(local_sp_3 + 80UL);
                            if (*var_53 == 0UL) {
                                var_54 = *(uint64_t *)(local_sp_3 + 24UL);
                                var_55 = *var_26;
                                var_56 = *var_28;
                                var_57 = *var_29;
                                var_58 = local_sp_3 + 112UL;
                                var_59 = *var_22;
                                var_60 = *var_23;
                                var_61 = *var_30;
                                var_62 = *var_31;
                                var_63 = *(uint64_t *)(local_sp_3 + 40UL);
                                *(uint64_t *)(local_sp_3 + 128UL) = var_57;
                                *var_53 = var_55;
                                *(uint64_t *)(local_sp_3 + 88UL) = var_56;
                                *(uint64_t *)(local_sp_3 + 96UL) = var_59;
                                *(uint64_t *)(local_sp_3 + 104UL) = var_60;
                                *(uint64_t *)var_58 = var_61;
                                *(uint64_t *)(local_sp_3 + 120UL) = var_62;
                                *var_48 = 4269401UL;
                                var_64 = indirect_placeholder_10(var_54, var_58, var_63);
                                local_sp_1 = var_47;
                                r15_0 = var_58;
                                rax_0 = var_64;
                                if ((uint64_t)(uint32_t)var_64 != 0UL) {
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                            }
                            r15_0 = local_sp_3 + 112UL;
                            var_65 = helper_movq_mm_T0_xmm_wrapper((struct type_4 *)(776UL), r13_1_ph);
                            var_66 = var_65.field_0;
                            var_67 = helper_movq_mm_T0_xmm_wrapper_359((struct type_4 *)(840UL), rdx);
                            var_68 = helper_punpcklqdq_xmm_wrapper_360((struct type_8 *)(0UL), (struct type_4 *)(776UL), (struct type_4 *)(840UL), var_66, var_67.field_0);
                            var_69 = var_68.field_1;
                            *(uint64_t *)(local_sp_1 + 112UL) = var_68.field_0;
                            *(uint64_t *)(local_sp_1 + 120UL) = var_69;
                            var_70 = local_sp_1 + (-8L);
                            *(uint64_t *)var_70 = 4269025UL;
                            var_71 = indirect_placeholder_1(r15_0, r14_1);
                            local_sp_7 = var_70;
                            if ((uint64_t)(unsigned char)var_71 != 0UL) {
                                loop_state_var = 0U;
                                switch_state_var = 1;
                                break;
                            }
                            var_72 = local_sp_1 + 88UL;
                            var_73 = *(uint64_t *)(local_sp_1 + 8UL);
                            var_74 = *(uint64_t *)(*(uint64_t *)(local_sp_1 + 56UL) + *(uint64_t *)var_72);
                            var_75 = local_sp_1 + (-16L);
                            var_76 = (uint64_t *)var_75;
                            *var_76 = 4269062UL;
                            var_77 = indirect_placeholder_1(var_73, var_72);
                            _pre_phi174 = var_76;
                            local_sp_0 = var_75;
                            local_sp_7 = var_75;
                            rax_0 = var_77;
                            if ((uint64_t)(uint32_t)var_77 != 0UL) {
                                loop_state_var = 0U;
                                switch_state_var = 1;
                                break;
                            }
                            var_78 = *var_28;
                            var_79 = *(uint64_t *)(local_sp_1 + 80UL);
                            rdx2_0 = var_79;
                            if (var_78 != 0UL) {
                                var_80 = local_sp_1 + (-24L);
                                var_81 = (uint64_t *)var_80;
                                *var_81 = 4269096UL;
                                var_82 = indirect_placeholder_266(var_32, rdx, r14_1, var_15, rsi, var_79, var_15, r13_1_ph, var_78);
                                var_83 = var_82.field_0;
                                _pre_phi174 = var_81;
                                local_sp_0 = var_80;
                                local_sp_7 = var_80;
                                rax_0 = var_83;
                                if ((uint64_t)(uint32_t)var_83 != 0UL) {
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                r14_0 = var_82.field_1;
                                rdx2_0 = *(uint64_t *)(local_sp_1 + 72UL);
                                r13_0 = var_82.field_2;
                            }
                            var_84 = *(uint64_t *)(local_sp_0 + 64UL);
                            var_85 = local_sp_0 + 136UL;
                            var_86 = *(uint64_t *)var_85;
                            var_87 = local_sp_0 + 144UL;
                            *(uint64_t *)(var_84 + rdx2_0) = var_74;
                            *(uint64_t *)(local_sp_0 + (-8L)) = 4269142UL;
                            var_88 = indirect_placeholder_267(r14_0, var_86, var_87);
                            var_89 = local_sp_0 + 128UL;
                            var_90 = var_88.field_0 + (-1L);
                            var_91 = local_sp_0 + (-16L);
                            *(uint64_t *)var_91 = 4269167UL;
                            indirect_placeholder_17(var_90, var_89, var_85);
                            var_92 = (r14_0 * 40UL) + *(uint64_t *)(*_pre_phi174 + 216UL);
                            local_sp_3_ph = var_91;
                            r13_1_ph = r13_0;
                            local_sp_5 = var_91;
                            if (*(unsigned char *)(var_92 + 32UL) != '\x00') {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            r15_2_ph = var_92 + 40UL;
                            r14_1_ph = r14_0 + 1UL;
                            continue;
                        }
                        break;
                      case 1U:
                        {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 1U:
                {
                    var_93 = **(uint64_t **)(local_sp_5 + 80UL);
                    _pre172 = (uint64_t *)(local_sp_5 + 8UL);
                    _pre_phi173 = _pre172;
                    local_sp_6 = local_sp_5;
                    rdx2_2 = var_93;
                    var_94 = *_pre_phi173 + 1UL;
                    *_pre_phi173 = var_94;
                    var_33 = var_94;
                    local_sp_2 = local_sp_6;
                    rax_0 = 0UL;
                    rdx2_1 = rdx2_2;
                    local_sp_7 = local_sp_6;
                    if ((long)var_94 >= (long)rdx2_2) {
                        continue;
                    }
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    if (*(uint64_t *)(local_sp_7 + 96UL) != 0UL) {
        return;
    }
    *(uint32_t *)(local_sp_7 + 8UL) = (uint32_t)rax_0;
    *(uint64_t *)(local_sp_7 + (-8L)) = 4269309UL;
    indirect_placeholder();
    return;
}
