typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_192_ret_type;
struct indirect_placeholder_193_ret_type;
struct indirect_placeholder_192_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_193_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_11(uint64_t param_0);
extern void indirect_placeholder_17(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_192_ret_type indirect_placeholder_192(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
extern struct indirect_placeholder_193_ret_type indirect_placeholder_193(uint64_t param_0);
uint64_t bb_create_cd_newstate(uint64_t rcx, uint64_t r10, uint64_t rdx, uint64_t r9, uint64_t rdi, uint64_t r8, uint64_t rsi) {
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    struct indirect_placeholder_192_ret_type var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t r13_0;
    uint64_t rcx1_0;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t local_sp_5;
    uint64_t local_sp_0;
    unsigned char *var_16;
    unsigned char var_17;
    uint64_t *var_18;
    uint64_t *var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint32_t *var_22;
    uint32_t var_23;
    uint64_t var_24;
    uint64_t *var_25;
    uint64_t *var_26;
    uint64_t var_27;
    uint64_t r102_3;
    uint64_t rbp_0;
    uint64_t local_sp_4;
    uint64_t r102_0;
    uint64_t rsi7_1;
    uint64_t local_sp_1;
    uint64_t rsi7_0;
    uint64_t var_52;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint32_t var_31;
    uint64_t var_32;
    uint32_t var_33;
    uint64_t var_34;
    uint32_t var_35;
    bool var_36;
    bool var_37;
    unsigned char var_38;
    unsigned char var_39;
    uint64_t r102_2;
    uint64_t r102_1;
    uint64_t local_sp_2;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_40;
    uint64_t var_41;
    struct indirect_placeholder_193_ret_type var_42;
    uint64_t var_43;
    uint32_t *var_44;
    uint32_t var_45;
    uint64_t local_sp_3;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_14;
    uint64_t var_15;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r15();
    var_3 = init_r14();
    var_4 = init_r12();
    var_5 = init_rbx();
    var_6 = init_r13();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_6;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_1;
    *(uint64_t *)(var_0 + (-48L)) = var_5;
    *(uint64_t *)(var_0 + (-64L)) = rcx;
    var_7 = var_0 + (-112L);
    *(uint64_t *)var_7 = 4252347UL;
    var_8 = indirect_placeholder_192(rcx, rdi, r10, var_5, r9, 112UL, r8, 1UL);
    var_9 = var_8.field_0;
    var_10 = var_8.field_2;
    rbp_0 = 0UL;
    r102_0 = 0UL;
    r13_0 = 0UL;
    if (var_9 == 0UL) {
        return r13_0;
    }
    var_11 = var_9 + 8UL;
    var_12 = rsi + 16UL;
    var_13 = rsi + 8UL;
    *(uint64_t *)(var_0 + (-80L)) = var_12;
    *(uint64_t *)(var_0 + (-88L)) = var_13;
    var_14 = var_0 + (-120L);
    *(uint64_t *)var_14 = 4252392UL;
    var_15 = indirect_placeholder_10(var_12, var_11, var_13);
    local_sp_1 = var_14;
    local_sp_5 = var_14;
    r13_0 = var_9;
    if ((uint64_t)(uint32_t)var_15 == 0UL) {
        *(uint64_t *)(var_0 + (-128L)) = 4252897UL;
        indirect_placeholder();
    } else {
        var_16 = (unsigned char *)(var_9 + 104UL);
        var_17 = *var_16;
        var_18 = (uint64_t *)(var_9 + 80UL);
        *var_18 = var_11;
        *var_16 = ((var_17 & '\xf0') | ((unsigned char)rdx & '\x0f'));
        var_19 = (uint64_t *)var_13;
        var_20 = *var_19;
        var_21 = helper_cc_compute_all_wrapper(var_20, 0UL, 0UL, 25U);
        rsi7_0 = var_20;
        if ((uint64_t)(((unsigned char)(var_21 >> 4UL) ^ (unsigned char)var_21) & '\xc0') != 0UL) {
            var_22 = (uint32_t *)var_7;
            var_23 = (uint32_t)rdx;
            *var_22 = (var_23 & 1U);
            *(uint32_t *)(var_0 + (-100L)) = (var_23 & 4U);
            var_24 = var_9 + 24UL;
            *(uint32_t *)(var_0 + (-108L)) = (var_23 & 2U);
            var_25 = (uint64_t *)var_12;
            var_26 = (uint64_t *)var_10;
            var_27 = var_9 + 16UL;
            while (1U)
                {
                    var_28 = (*(uint64_t *)((rbp_0 << 3UL) + *var_25) << 4UL) + *var_26;
                    var_29 = var_28 + 8UL;
                    var_30 = (uint64_t)*(unsigned char *)var_29;
                    var_31 = *(uint32_t *)var_29 >> 8U;
                    var_32 = (uint64_t)((uint16_t)(uint64_t)var_31 & (unsigned short)1023U);
                    var_33 = var_31 & 16712703U;
                    var_34 = (uint64_t)var_33;
                    var_35 = (uint32_t)var_30;
                    var_36 = ((uint64_t)(var_35 + (-1)) == 0UL);
                    var_37 = (var_32 == 0UL);
                    rcx1_0 = var_34;
                    r102_3 = r102_0;
                    local_sp_4 = local_sp_1;
                    rsi7_1 = rsi7_0;
                    r102_2 = r102_0;
                    r102_1 = r102_0;
                    local_sp_2 = local_sp_1;
                    local_sp_3 = local_sp_1;
                    if (var_36 && var_37) {
                        var_52 = rbp_0 + 1UL;
                        local_sp_5 = local_sp_4;
                        rbp_0 = var_52;
                        r102_0 = r102_3;
                        local_sp_1 = local_sp_4;
                        rsi7_0 = rsi7_1;
                        if ((long)var_52 < (long)rsi7_1) {
                            continue;
                        }
                        loop_state_var = 0U;
                        break;
                    }
                    var_38 = *var_16;
                    var_39 = (var_38 & '\xdf') | (unsigned char)((((uint64_t)(*(unsigned char *)(var_28 + 10UL) >> '\x04') << 5UL) | (uint64_t)var_38) & 32UL);
                    *var_16 = var_39;
                    if ((uint64_t)(var_35 + (-2)) == 0UL) {
                        *var_16 = (var_39 | '\x10');
                    } else {
                        if ((uint64_t)(var_35 + (-4)) == 0UL) {
                            *var_16 = (var_39 | '@');
                        }
                    }
                    if (var_37) {
                        r102_3 = r102_2;
                        local_sp_4 = local_sp_3;
                        rsi7_1 = *var_19;
                    } else {
                        if (var_11 != *var_18) {
                            var_40 = local_sp_1 + 16UL;
                            *(uint32_t *)var_40 = var_33;
                            var_41 = local_sp_1 + (-8L);
                            *(uint64_t *)var_41 = 4252774UL;
                            var_42 = indirect_placeholder_193(24UL);
                            var_43 = var_42.field_0;
                            var_44 = (uint32_t *)(local_sp_1 + 8UL);
                            var_45 = *var_44;
                            *var_18 = var_43;
                            local_sp_0 = var_41;
                            r102_1 = 0UL;
                            if (var_43 != 0UL) {
                                loop_state_var = 2U;
                                break;
                            }
                            var_46 = *(uint64_t *)(local_sp_1 + 24UL);
                            var_47 = *(uint64_t *)var_40;
                            *var_44 = var_45;
                            var_48 = local_sp_1 + (-16L);
                            *(uint64_t *)var_48 = 4252809UL;
                            var_49 = indirect_placeholder_10(var_46, var_43, var_47);
                            local_sp_2 = var_48;
                            if ((uint64_t)(uint32_t)var_49 != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            *var_16 = (*var_16 | '\x80');
                            rcx1_0 = (uint64_t)*(uint32_t *)local_sp_1;
                        }
                        r102_2 = r102_1;
                        local_sp_3 = local_sp_2;
                        if ((rcx1_0 & 1UL) == 0UL) {
                            if ((rcx1_0 & 2UL) != 0UL & *(uint32_t *)(local_sp_2 + 8UL) != 0U) {
                                var_50 = rbp_0 - r102_1;
                                var_51 = local_sp_2 + (-8L);
                                *(uint64_t *)var_51 = 4252594UL;
                                indirect_placeholder_17(var_50, var_27, var_24);
                                r102_2 = r102_1 + 1UL;
                                local_sp_3 = var_51;
                                r102_3 = r102_2;
                                local_sp_4 = local_sp_3;
                                rsi7_1 = *var_19;
                                var_52 = rbp_0 + 1UL;
                                local_sp_5 = local_sp_4;
                                rbp_0 = var_52;
                                r102_0 = r102_3;
                                local_sp_1 = local_sp_4;
                                rsi7_0 = rsi7_1;
                                if ((long)var_52 < (long)rsi7_1) {
                                    continue;
                                }
                                loop_state_var = 0U;
                                break;
                            }
                        }
                        if (!((*(uint32_t *)(local_sp_2 + 8UL) != 0U) && ((rcx1_0 & 2UL) == 0UL))) {
                            var_50 = rbp_0 - r102_1;
                            var_51 = local_sp_2 + (-8L);
                            *(uint64_t *)var_51 = 4252594UL;
                            indirect_placeholder_17(var_50, var_27, var_24);
                            r102_2 = r102_1 + 1UL;
                            local_sp_3 = var_51;
                            r102_3 = r102_2;
                            local_sp_4 = local_sp_3;
                            rsi7_1 = *var_19;
                            var_52 = rbp_0 + 1UL;
                            local_sp_5 = local_sp_4;
                            rbp_0 = var_52;
                            r102_0 = r102_3;
                            local_sp_1 = local_sp_4;
                            rsi7_0 = rsi7_1;
                            if ((long)var_52 < (long)rsi7_1) {
                                continue;
                            }
                            loop_state_var = 0U;
                            break;
                        }
                        if ((rcx1_0 & 16UL) != 0UL) {
                            if (*(uint32_t *)(local_sp_2 + 12UL) != 0U) {
                                var_50 = rbp_0 - r102_1;
                                var_51 = local_sp_2 + (-8L);
                                *(uint64_t *)var_51 = 4252594UL;
                                indirect_placeholder_17(var_50, var_27, var_24);
                                r102_2 = r102_1 + 1UL;
                                local_sp_3 = var_51;
                                r102_3 = r102_2;
                                local_sp_4 = local_sp_3;
                                rsi7_1 = *var_19;
                                var_52 = rbp_0 + 1UL;
                                local_sp_5 = local_sp_4;
                                rbp_0 = var_52;
                                r102_0 = r102_3;
                                local_sp_1 = local_sp_4;
                                rsi7_0 = rsi7_1;
                                if ((long)var_52 < (long)rsi7_1) {
                                    continue;
                                }
                                loop_state_var = 0U;
                                break;
                            }
                        }
                        if ((rcx1_0 & 64UL) != 0UL & *(uint32_t *)(local_sp_2 + 20UL) == 0U) {
                            var_50 = rbp_0 - r102_1;
                            var_51 = local_sp_2 + (-8L);
                            *(uint64_t *)var_51 = 4252594UL;
                            indirect_placeholder_17(var_50, var_27, var_24);
                            r102_2 = r102_1 + 1UL;
                            local_sp_3 = var_51;
                        }
                    }
                }
            switch (loop_state_var) {
              case 1U:
                {
                    return r13_0;
                }
                break;
              case 2U:
                {
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4252881UL;
                    indirect_placeholder_11(var_9);
                }
                break;
              case 0U:
                {
                    var_53 = *(uint64_t *)(local_sp_5 + 40UL);
                    var_54 = local_sp_5 + (-8L);
                    *(uint64_t *)var_54 = 4252851UL;
                    var_55 = indirect_placeholder_10(var_53, var_10, var_9);
                    local_sp_0 = var_54;
                    if ((uint64_t)(uint32_t)var_55 != 0UL) {
                        return r13_0;
                    }
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4252881UL;
                    indirect_placeholder_11(var_9);
                }
                break;
            }
        }
        var_53 = *(uint64_t *)(local_sp_5 + 40UL);
        var_54 = local_sp_5 + (-8L);
        *(uint64_t *)var_54 = 4252851UL;
        var_55 = indirect_placeholder_10(var_53, var_10, var_9);
        local_sp_0 = var_54;
        if ((uint64_t)(uint32_t)var_55 != 0UL) {
            return r13_0;
        }
        *(uint64_t *)(local_sp_0 + (-8L)) = 4252881UL;
        indirect_placeholder_11(var_9);
    }
    return r13_0;
}
