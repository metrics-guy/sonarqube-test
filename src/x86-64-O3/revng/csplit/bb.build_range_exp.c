typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_movq_mm_T0_xmm_wrapper_ret_type;
struct type_4;
struct helper_movq_mm_T0_xmm_wrapper_359_ret_type;
struct helper_punpcklqdq_xmm_wrapper_360_ret_type;
struct type_8;
struct helper_movq_mm_T0_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_4 {
};
struct helper_movq_mm_T0_xmm_wrapper_359_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_punpcklqdq_xmm_wrapper_360_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
};
struct type_8 {
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct helper_movq_mm_T0_xmm_wrapper_ret_type helper_movq_mm_T0_xmm_wrapper(struct type_4 *param_0, uint64_t param_1);
extern struct helper_movq_mm_T0_xmm_wrapper_359_ret_type helper_movq_mm_T0_xmm_wrapper_359(struct type_4 *param_0, uint64_t param_1);
extern struct helper_punpcklqdq_xmm_wrapper_360_ret_type helper_punpcklqdq_xmm_wrapper_360(struct type_8 *param_0, struct type_4 *param_1, struct type_4 *param_2, uint64_t param_3, uint64_t param_4);
uint64_t bb_build_range_exp(uint64_t rcx, uint64_t rdx, uint64_t r9, uint64_t rdi, uint64_t r8, uint64_t rsi) {
    struct helper_punpcklqdq_xmm_wrapper_360_ret_type var_71;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint32_t var_9;
    uint64_t storemerge7;
    uint64_t var_66;
    uint64_t *var_67;
    struct helper_movq_mm_T0_xmm_wrapper_359_ret_type var_68;
    uint64_t var_69;
    struct helper_movq_mm_T0_xmm_wrapper_ret_type var_70;
    uint64_t var_72;
    uint64_t rax_0;
    uint64_t rcx1_9;
    uint64_t local_sp_2;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t local_sp_1;
    uint64_t local_sp_3;
    uint64_t local_sp_0;
    uint64_t var_35;
    uint64_t rcx1_0;
    unsigned char rdx2_0_in;
    uint64_t r85_0;
    uint64_t rsi6_0;
    uint64_t rdx2_3;
    uint64_t r93_0_in_in;
    uint64_t rdi4_0;
    uint64_t r85_3;
    uint32_t *var_10;
    uint32_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    bool var_14;
    uint64_t var_15;
    uint64_t rcx1_1;
    uint64_t rdx2_1;
    uint64_t r85_1;
    uint64_t rcx1_2;
    uint64_t rdx2_2;
    uint64_t r85_2;
    uint64_t storemerge8_in_in_in;
    uint64_t storemerge8;
    uint64_t local_sp_4;
    uint64_t local_sp_6;
    uint64_t var_36;
    uint64_t rcx1_3;
    uint64_t r15_0;
    uint64_t r14_1;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_37;
    uint64_t local_sp_15;
    uint64_t local_sp_7;
    uint64_t rcx1_4;
    uint64_t rdx2_4;
    uint64_t rdi4_1;
    uint64_t r85_4;
    uint64_t var_38;
    uint64_t rcx1_6;
    uint64_t rdx2_6;
    uint64_t rdi4_3;
    uint64_t r85_6;
    uint64_t storemerge;
    uint64_t rcx1_7;
    uint64_t rdx2_7;
    uint64_t r93_1;
    uint64_t rdi4_4;
    uint64_t r85_7;
    uint64_t var_39;
    uint64_t local_sp_14;
    uint64_t local_sp_8;
    uint64_t rcx1_8;
    uint64_t r14_0;
    uint64_t rbx_0;
    uint64_t r93_2;
    uint64_t local_sp_9;
    uint64_t r93_3;
    uint64_t rbx_1;
    uint64_t storemerge6;
    uint32_t var_51;
    uint32_t var_52;
    bool var_53;
    uint64_t var_54;
    uint64_t local_sp_11;
    uint64_t *var_55;
    uint64_t var_56;
    uint64_t *var_57;
    uint64_t var_58;
    uint64_t local_sp_10;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t *var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t *var_65;
    uint64_t rsi6_2;
    uint64_t rcx1_10;
    uint64_t local_sp_12;
    uint64_t rsi6_1;
    uint64_t local_sp_13;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t rbx_2;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t *var_49;
    uint64_t var_50;
    uint64_t rcx1_11;
    uint64_t r93_4;
    uint64_t rdi4_5;
    uint64_t var_40;
    uint64_t var_41;
    uint32_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_16;
    uint64_t *var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t *var_26;
    uint64_t *var_27;
    uint64_t var_28;
    uint64_t *var_29;
    uint64_t var_30;
    uint64_t var_31;
    unsigned char var_32;
    uint64_t *var_33;
    uint64_t var_34;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_cc_src2();
    var_2 = init_rbp();
    var_3 = init_r15();
    var_4 = init_r14();
    var_5 = init_r12();
    var_6 = init_rbx();
    var_7 = init_r13();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_7;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    *(uint64_t *)(var_0 + (-48L)) = var_6;
    var_8 = var_0 + (-88L);
    var_9 = *(uint32_t *)r8;
    storemerge7 = 11UL;
    local_sp_1 = var_8;
    rdi4_0 = 0UL;
    rcx1_1 = rcx;
    r85_1 = r8;
    local_sp_4 = var_8;
    local_sp_6 = var_8;
    rcx1_4 = rcx;
    rdi4_1 = 0UL;
    r85_4 = r8;
    rcx1_6 = rcx;
    rdi4_3 = 0UL;
    r85_6 = r8;
    storemerge = 0UL;
    rsi6_1 = 0UL;
    if ((uint64_t)((var_9 + (-2)) & (-3)) == 0UL) {
        return storemerge7;
    }
    var_10 = (uint32_t *)r9;
    var_11 = *var_10;
    var_12 = (uint64_t)var_11;
    var_13 = (uint64_t)((var_11 + (-2)) & (-3));
    r14_0 = var_12;
    if (var_13 == 0UL) {
        return;
    }
    var_14 = ((uint64_t)(var_9 + (-3)) == 0UL);
    var_15 = (rdx & (-256L)) | var_14;
    rdx2_1 = var_15;
    rdx2_4 = var_15;
    rdx2_6 = var_15;
    storemerge7 = 3UL;
    if (var_14) {
        var_23 = *(uint64_t *)(r8 + 8UL);
        *(uint64_t *)(var_0 + (-64L)) = rcx;
        var_24 = var_0 + (-72L);
        *(unsigned char *)var_24 = (unsigned char)'\x01';
        var_25 = var_0 + (-80L);
        var_26 = (uint64_t *)var_25;
        *var_26 = r8;
        var_27 = (uint64_t *)var_8;
        *var_27 = var_23;
        var_28 = var_0 + (-96L);
        var_29 = (uint64_t *)var_28;
        *var_29 = 4246875UL;
        indirect_placeholder();
        var_30 = *var_29;
        var_31 = *var_27;
        var_32 = *(unsigned char *)var_25;
        var_33 = (uint64_t *)var_24;
        var_34 = *var_33;
        local_sp_0 = var_28;
        rcx1_0 = var_34;
        rdx2_0_in = var_32;
        r85_0 = var_31;
        rsi6_0 = var_30;
        if (var_13 > 1UL) {
            return storemerge7;
        }
        if ((uint64_t)(var_11 + (-3)) == 0UL) {
            *var_33 = var_31;
            *var_26 = var_34;
            *var_27 = var_30;
            *(unsigned char *)var_28 = var_32;
            var_35 = var_0 + (-104L);
            *(uint64_t *)var_35 = 4246800UL;
            indirect_placeholder();
            local_sp_0 = var_35;
            rcx1_0 = *var_27;
            rdx2_0_in = *(unsigned char *)var_35;
            r85_0 = *var_26;
            rsi6_0 = *var_29;
        }
        local_sp_2 = local_sp_0;
        rcx1_2 = rcx1_0;
        rdx2_2 = (uint64_t)rdx2_0_in;
        r85_2 = r85_0;
        storemerge8_in_in_in = rsi6_0;
    } else {
        if ((uint64_t)(var_11 + (-3)) == 0UL) {
            var_16 = *(uint64_t *)(r9 + 8UL);
            *(uint64_t *)(var_0 + (-64L)) = r8;
            var_17 = (uint64_t *)(var_0 + (-72L));
            *var_17 = rcx;
            var_18 = var_0 + (-80L);
            *(unsigned char *)var_18 = (unsigned char)'\x00';
            *(uint64_t *)var_8 = var_16;
            var_19 = var_0 + (-96L);
            *(uint64_t *)var_19 = 4246948UL;
            indirect_placeholder();
            local_sp_1 = var_19;
            local_sp_3 = var_19;
            r93_0_in_in = var_19;
            if (var_13 > 1UL) {
                return storemerge7;
            }
            var_20 = (uint64_t)*(unsigned char *)var_8;
            var_21 = *(uint64_t *)var_18;
            var_22 = *var_17;
            rdx2_3 = var_20;
            r85_3 = var_22;
            rcx1_1 = var_21;
            rdx2_1 = var_20;
            r85_1 = var_22;
            rcx1_3 = var_21;
            if (var_9 != 0U) {
                var_37 = (uint64_t)**(unsigned char **)r93_0_in_in;
                local_sp_15 = local_sp_3;
                local_sp_7 = local_sp_3;
                rcx1_7 = rcx1_3;
                rdx2_7 = rdx2_3;
                r93_1 = var_37;
                rdi4_4 = rdi4_0;
                r85_7 = r85_3;
                rcx1_11 = rcx1_3;
                r93_4 = var_37;
                rdi4_5 = rdi4_0;
                if (var_9 == 0U) {
                    local_sp_15 = local_sp_7;
                    local_sp_14 = local_sp_7;
                    local_sp_8 = local_sp_7;
                    rcx1_8 = rcx1_7;
                    r93_2 = r93_1;
                    r93_3 = r93_1;
                    rcx1_10 = rcx1_7;
                    rcx1_11 = rcx1_7;
                    r93_4 = r93_1;
                    rdi4_5 = rdi4_4;
                    if ((uint64_t)(unsigned char)rdx2_7 == 0UL) {
                        var_39 = (uint64_t)*(uint32_t *)(r85_7 + 8UL);
                        rbx_0 = var_39;
                        rbx_2 = var_39;
                        if (var_11 == 0U) {
                            var_47 = (uint64_t)(uint32_t)r93_3;
                            *(uint64_t *)local_sp_14 = rcx1_10;
                            var_48 = local_sp_14 + (-8L);
                            var_49 = (uint64_t *)var_48;
                            *var_49 = 4246567UL;
                            var_50 = indirect_placeholder_1(var_47, rdx);
                            local_sp_9 = var_48;
                            rcx1_9 = *var_49;
                            rbx_1 = rbx_2;
                            storemerge6 = (uint64_t)(uint32_t)var_50;
                        } else {
                            rcx1_9 = rcx1_8;
                            local_sp_14 = local_sp_8;
                            local_sp_9 = local_sp_8;
                            r93_3 = r93_2;
                            rbx_1 = rbx_0;
                            rcx1_10 = rcx1_8;
                            rbx_2 = rbx_0;
                            if ((uint64_t)((uint32_t)r14_0 + (-3)) == 0UL) {
                                storemerge6 = (uint64_t)*(uint32_t *)(r9 + 8UL);
                            } else {
                                var_47 = (uint64_t)(uint32_t)r93_3;
                                *(uint64_t *)local_sp_14 = rcx1_10;
                                var_48 = local_sp_14 + (-8L);
                                var_49 = (uint64_t *)var_48;
                                *var_49 = 4246567UL;
                                var_50 = indirect_placeholder_1(var_47, rdx);
                                local_sp_9 = var_48;
                                rcx1_9 = *var_49;
                                rbx_1 = rbx_2;
                                storemerge6 = (uint64_t)(uint32_t)var_50;
                            }
                        }
                    } else {
                        *(uint64_t *)(local_sp_15 + 8UL) = rcx1_11;
                        *(uint32_t *)local_sp_15 = (uint32_t)r93_4;
                        var_40 = local_sp_15 + (-8L);
                        *(uint64_t *)var_40 = 4246532UL;
                        var_41 = indirect_placeholder_1(rdi4_5, rdx);
                        var_42 = *var_10;
                        var_43 = (uint64_t)var_42;
                        var_44 = (uint64_t)*(uint32_t *)var_40;
                        var_45 = *(uint64_t *)local_sp_15;
                        var_46 = (uint64_t)(uint32_t)var_41;
                        local_sp_14 = var_40;
                        local_sp_8 = var_40;
                        rcx1_8 = var_45;
                        r14_0 = var_43;
                        rbx_0 = var_46;
                        r93_2 = var_44;
                        r93_3 = var_44;
                        rcx1_10 = var_45;
                        rbx_2 = var_46;
                        if (var_42 == 0U) {
                            var_47 = (uint64_t)(uint32_t)r93_3;
                            *(uint64_t *)local_sp_14 = rcx1_10;
                            var_48 = local_sp_14 + (-8L);
                            var_49 = (uint64_t *)var_48;
                            *var_49 = 4246567UL;
                            var_50 = indirect_placeholder_1(var_47, rdx);
                            local_sp_9 = var_48;
                            rcx1_9 = *var_49;
                            rbx_1 = rbx_2;
                            storemerge6 = (uint64_t)(uint32_t)var_50;
                        } else {
                            rcx1_9 = rcx1_8;
                            local_sp_14 = local_sp_8;
                            local_sp_9 = local_sp_8;
                            r93_3 = r93_2;
                            rbx_1 = rbx_0;
                            rcx1_10 = rcx1_8;
                            rbx_2 = rbx_0;
                            if ((uint64_t)((uint32_t)r14_0 + (-3)) == 0UL) {
                                var_47 = (uint64_t)(uint32_t)r93_3;
                                *(uint64_t *)local_sp_14 = rcx1_10;
                                var_48 = local_sp_14 + (-8L);
                                var_49 = (uint64_t *)var_48;
                                *var_49 = 4246567UL;
                                var_50 = indirect_placeholder_1(var_47, rdx);
                                local_sp_9 = var_48;
                                rcx1_9 = *var_49;
                                rbx_1 = rbx_2;
                                storemerge6 = (uint64_t)(uint32_t)var_50;
                            } else {
                                storemerge6 = (uint64_t)*(uint32_t *)(r9 + 8UL);
                            }
                        }
                    }
                } else {
                    *(uint64_t *)(local_sp_15 + 8UL) = rcx1_11;
                    *(uint32_t *)local_sp_15 = (uint32_t)r93_4;
                    var_40 = local_sp_15 + (-8L);
                    *(uint64_t *)var_40 = 4246532UL;
                    var_41 = indirect_placeholder_1(rdi4_5, rdx);
                    var_42 = *var_10;
                    var_43 = (uint64_t)var_42;
                    var_44 = (uint64_t)*(uint32_t *)var_40;
                    var_45 = *(uint64_t *)local_sp_15;
                    var_46 = (uint64_t)(uint32_t)var_41;
                    local_sp_14 = var_40;
                    local_sp_8 = var_40;
                    rcx1_8 = var_45;
                    r14_0 = var_43;
                    rbx_0 = var_46;
                    r93_2 = var_44;
                    r93_3 = var_44;
                    rcx1_10 = var_45;
                    rbx_2 = var_46;
                    if (var_42 == 0U) {
                        var_47 = (uint64_t)(uint32_t)r93_3;
                        *(uint64_t *)local_sp_14 = rcx1_10;
                        var_48 = local_sp_14 + (-8L);
                        var_49 = (uint64_t *)var_48;
                        *var_49 = 4246567UL;
                        var_50 = indirect_placeholder_1(var_47, rdx);
                        local_sp_9 = var_48;
                        rcx1_9 = *var_49;
                        rbx_1 = rbx_2;
                        storemerge6 = (uint64_t)(uint32_t)var_50;
                    } else {
                        rcx1_9 = rcx1_8;
                        local_sp_14 = local_sp_8;
                        local_sp_9 = local_sp_8;
                        r93_3 = r93_2;
                        rbx_1 = rbx_0;
                        rcx1_10 = rcx1_8;
                        rbx_2 = rbx_0;
                        if ((uint64_t)((uint32_t)r14_0 + (-3)) == 0UL) {
                            storemerge6 = (uint64_t)*(uint32_t *)(r9 + 8UL);
                        } else {
                            var_47 = (uint64_t)(uint32_t)r93_3;
                            *(uint64_t *)local_sp_14 = rcx1_10;
                            var_48 = local_sp_14 + (-8L);
                            var_49 = (uint64_t *)var_48;
                            *var_49 = 4246567UL;
                            var_50 = indirect_placeholder_1(var_47, rdx);
                            local_sp_9 = var_48;
                            rcx1_9 = *var_49;
                            rbx_1 = rbx_2;
                            storemerge6 = (uint64_t)(uint32_t)var_50;
                        }
                    }
                }
                var_51 = (uint32_t)rbx_1;
                local_sp_10 = local_sp_9;
                local_sp_11 = local_sp_9;
                var_52 = (uint32_t)storemerge6;
                var_53 = ((uint64_t)((uint32_t)rdi & 65536U) != 0UL);
                var_54 = (uint64_t)var_51;
                storemerge7 = 11UL;
                if ((uint64_t)(var_51 + 1U) != 0UL & (uint64_t)(var_52 + 1U) != 0UL & !(var_53 && (var_54 > storemerge6))) {
                    storemerge7 = 0UL;
                    if (rdx != 0UL) {
                        var_55 = (uint64_t *)(rdx + 64UL);
                        var_56 = *var_55;
                        var_57 = (uint64_t *)(rdx + 8UL);
                        var_58 = *var_57;
                        rax_0 = var_56;
                        r15_0 = var_56;
                        r14_1 = var_56;
                        storemerge7 = 12UL;
                        if (*(uint64_t *)rcx1_9 == var_56) {
                            r15_0 = *(uint64_t *)(rdx + 16UL);
                            r14_1 = var_58;
                        } else {
                            var_59 = (var_56 << 1UL) | 1UL;
                            *(uint64_t *)(local_sp_9 + 8UL) = rcx1_9;
                            var_60 = var_59 << 2UL;
                            *(uint64_t *)local_sp_9 = var_59;
                            var_61 = local_sp_9 + (-8L);
                            *(uint64_t *)var_61 = 4247025UL;
                            indirect_placeholder_1(var_58, var_60);
                            var_62 = (uint64_t *)(rdx + 16UL);
                            var_63 = *var_62;
                            var_64 = local_sp_9 + (-16L);
                            var_65 = (uint64_t *)var_64;
                            *var_65 = 4247040UL;
                            indirect_placeholder_1(var_63, var_60);
                            local_sp_10 = var_64;
                            if (var_56 != 0UL) {
                                *(uint64_t *)(local_sp_9 + (-24L)) = 4247101UL;
                                indirect_placeholder();
                                *(uint64_t *)(local_sp_9 + (-32L)) = 4247109UL;
                                indirect_placeholder();
                                return storemerge7;
                            }
                            var_66 = *var_65;
                            var_67 = *(uint64_t **)var_61;
                            var_68 = helper_movq_mm_T0_xmm_wrapper_359((struct type_4 *)(840UL), var_56);
                            var_69 = var_68.field_0;
                            var_70 = helper_movq_mm_T0_xmm_wrapper((struct type_4 *)(776UL), var_56);
                            var_71 = helper_punpcklqdq_xmm_wrapper_360((struct type_8 *)(0UL), (struct type_4 *)(776UL), (struct type_4 *)(840UL), var_70.field_0, var_69);
                            var_72 = var_71.field_1;
                            *var_57 = var_71.field_0;
                            *var_62 = var_72;
                            *var_67 = var_66;
                            rax_0 = *var_55;
                        }
                        var_73 = rax_0 + 1UL;
                        var_74 = rax_0 << 2UL;
                        *(uint32_t *)(var_74 + r14_1) = var_51;
                        *var_55 = var_73;
                        *(uint32_t *)(var_74 + r15_0) = var_52;
                        local_sp_11 = local_sp_10;
                    }
                    local_sp_12 = local_sp_11;
                    while (1U)
                        {
                            local_sp_13 = local_sp_12;
                            rsi6_2 = rsi6_1;
                            var_75 = helper_cc_compute_c_wrapper(storemerge6 - rsi6_1, rsi6_1, var_1, 16U);
                            if (var_54 <= (uint64_t)(uint32_t)rsi6_1 & var_75 == 0UL) {
                                var_76 = local_sp_12 + (-8L);
                                *(uint64_t *)var_76 = 4246712UL;
                                var_77 = indirect_placeholder_1(rsi, rsi6_1);
                                local_sp_13 = var_76;
                                rsi6_2 = var_77;
                            }
                            local_sp_12 = local_sp_13;
                            if (rsi6_2 == 255UL) {
                                break;
                            }
                            rsi6_1 = rsi6_2 + 1UL;
                            continue;
                        }
                }
                return storemerge7;
            }
        }
        if (var_9 != 0U) {
            if (var_11 == 0U) {
                var_38 = (uint64_t)*(unsigned char *)(r9 + 8UL);
                local_sp_6 = local_sp_4;
                rcx1_6 = rcx1_4;
                rdx2_6 = rdx2_4;
                rdi4_3 = rdi4_1;
                r85_6 = r85_4;
                storemerge = var_38;
            }
            local_sp_15 = local_sp_6;
            local_sp_7 = local_sp_6;
            rcx1_7 = rcx1_6;
            rdx2_7 = rdx2_6;
            r93_1 = storemerge;
            rdi4_4 = rdi4_3;
            r85_7 = r85_6;
            rcx1_11 = rcx1_6;
            r93_4 = storemerge;
            rdi4_5 = rdi4_3;
            if (var_9 == 0U) {
                local_sp_15 = local_sp_7;
                local_sp_14 = local_sp_7;
                local_sp_8 = local_sp_7;
                rcx1_8 = rcx1_7;
                r93_2 = r93_1;
                r93_3 = r93_1;
                rcx1_10 = rcx1_7;
                rcx1_11 = rcx1_7;
                r93_4 = r93_1;
                rdi4_5 = rdi4_4;
                if ((uint64_t)(unsigned char)rdx2_7 == 0UL) {
                    var_39 = (uint64_t)*(uint32_t *)(r85_7 + 8UL);
                    rbx_0 = var_39;
                    rbx_2 = var_39;
                    if (var_11 == 0U) {
                        var_47 = (uint64_t)(uint32_t)r93_3;
                        *(uint64_t *)local_sp_14 = rcx1_10;
                        var_48 = local_sp_14 + (-8L);
                        var_49 = (uint64_t *)var_48;
                        *var_49 = 4246567UL;
                        var_50 = indirect_placeholder_1(var_47, rdx);
                        local_sp_9 = var_48;
                        rcx1_9 = *var_49;
                        rbx_1 = rbx_2;
                        storemerge6 = (uint64_t)(uint32_t)var_50;
                    } else {
                        rcx1_9 = rcx1_8;
                        local_sp_14 = local_sp_8;
                        local_sp_9 = local_sp_8;
                        r93_3 = r93_2;
                        rbx_1 = rbx_0;
                        rcx1_10 = rcx1_8;
                        rbx_2 = rbx_0;
                        if ((uint64_t)((uint32_t)r14_0 + (-3)) == 0UL) {
                            storemerge6 = (uint64_t)*(uint32_t *)(r9 + 8UL);
                        } else {
                            var_47 = (uint64_t)(uint32_t)r93_3;
                            *(uint64_t *)local_sp_14 = rcx1_10;
                            var_48 = local_sp_14 + (-8L);
                            var_49 = (uint64_t *)var_48;
                            *var_49 = 4246567UL;
                            var_50 = indirect_placeholder_1(var_47, rdx);
                            local_sp_9 = var_48;
                            rcx1_9 = *var_49;
                            rbx_1 = rbx_2;
                            storemerge6 = (uint64_t)(uint32_t)var_50;
                        }
                    }
                } else {
                    *(uint64_t *)(local_sp_15 + 8UL) = rcx1_11;
                    *(uint32_t *)local_sp_15 = (uint32_t)r93_4;
                    var_40 = local_sp_15 + (-8L);
                    *(uint64_t *)var_40 = 4246532UL;
                    var_41 = indirect_placeholder_1(rdi4_5, rdx);
                    var_42 = *var_10;
                    var_43 = (uint64_t)var_42;
                    var_44 = (uint64_t)*(uint32_t *)var_40;
                    var_45 = *(uint64_t *)local_sp_15;
                    var_46 = (uint64_t)(uint32_t)var_41;
                    local_sp_14 = var_40;
                    local_sp_8 = var_40;
                    rcx1_8 = var_45;
                    r14_0 = var_43;
                    rbx_0 = var_46;
                    r93_2 = var_44;
                    r93_3 = var_44;
                    rcx1_10 = var_45;
                    rbx_2 = var_46;
                    if (var_42 == 0U) {
                        var_47 = (uint64_t)(uint32_t)r93_3;
                        *(uint64_t *)local_sp_14 = rcx1_10;
                        var_48 = local_sp_14 + (-8L);
                        var_49 = (uint64_t *)var_48;
                        *var_49 = 4246567UL;
                        var_50 = indirect_placeholder_1(var_47, rdx);
                        local_sp_9 = var_48;
                        rcx1_9 = *var_49;
                        rbx_1 = rbx_2;
                        storemerge6 = (uint64_t)(uint32_t)var_50;
                    } else {
                        rcx1_9 = rcx1_8;
                        local_sp_14 = local_sp_8;
                        local_sp_9 = local_sp_8;
                        r93_3 = r93_2;
                        rbx_1 = rbx_0;
                        rcx1_10 = rcx1_8;
                        rbx_2 = rbx_0;
                        if ((uint64_t)((uint32_t)r14_0 + (-3)) == 0UL) {
                            var_47 = (uint64_t)(uint32_t)r93_3;
                            *(uint64_t *)local_sp_14 = rcx1_10;
                            var_48 = local_sp_14 + (-8L);
                            var_49 = (uint64_t *)var_48;
                            *var_49 = 4246567UL;
                            var_50 = indirect_placeholder_1(var_47, rdx);
                            local_sp_9 = var_48;
                            rcx1_9 = *var_49;
                            rbx_1 = rbx_2;
                            storemerge6 = (uint64_t)(uint32_t)var_50;
                        } else {
                            storemerge6 = (uint64_t)*(uint32_t *)(r9 + 8UL);
                        }
                    }
                }
            } else {
                *(uint64_t *)(local_sp_15 + 8UL) = rcx1_11;
                *(uint32_t *)local_sp_15 = (uint32_t)r93_4;
                var_40 = local_sp_15 + (-8L);
                *(uint64_t *)var_40 = 4246532UL;
                var_41 = indirect_placeholder_1(rdi4_5, rdx);
                var_42 = *var_10;
                var_43 = (uint64_t)var_42;
                var_44 = (uint64_t)*(uint32_t *)var_40;
                var_45 = *(uint64_t *)local_sp_15;
                var_46 = (uint64_t)(uint32_t)var_41;
                local_sp_14 = var_40;
                local_sp_8 = var_40;
                rcx1_8 = var_45;
                r14_0 = var_43;
                rbx_0 = var_46;
                r93_2 = var_44;
                r93_3 = var_44;
                rcx1_10 = var_45;
                rbx_2 = var_46;
                if (var_42 == 0U) {
                    var_47 = (uint64_t)(uint32_t)r93_3;
                    *(uint64_t *)local_sp_14 = rcx1_10;
                    var_48 = local_sp_14 + (-8L);
                    var_49 = (uint64_t *)var_48;
                    *var_49 = 4246567UL;
                    var_50 = indirect_placeholder_1(var_47, rdx);
                    local_sp_9 = var_48;
                    rcx1_9 = *var_49;
                    rbx_1 = rbx_2;
                    storemerge6 = (uint64_t)(uint32_t)var_50;
                } else {
                    rcx1_9 = rcx1_8;
                    local_sp_14 = local_sp_8;
                    local_sp_9 = local_sp_8;
                    r93_3 = r93_2;
                    rbx_1 = rbx_0;
                    rcx1_10 = rcx1_8;
                    rbx_2 = rbx_0;
                    if ((uint64_t)((uint32_t)r14_0 + (-3)) == 0UL) {
                        storemerge6 = (uint64_t)*(uint32_t *)(r9 + 8UL);
                    } else {
                        var_47 = (uint64_t)(uint32_t)r93_3;
                        *(uint64_t *)local_sp_14 = rcx1_10;
                        var_48 = local_sp_14 + (-8L);
                        var_49 = (uint64_t *)var_48;
                        *var_49 = 4246567UL;
                        var_50 = indirect_placeholder_1(var_47, rdx);
                        local_sp_9 = var_48;
                        rcx1_9 = *var_49;
                        rbx_1 = rbx_2;
                        storemerge6 = (uint64_t)(uint32_t)var_50;
                    }
                }
            }
            var_51 = (uint32_t)rbx_1;
            local_sp_10 = local_sp_9;
            local_sp_11 = local_sp_9;
            var_52 = (uint32_t)storemerge6;
            var_53 = ((uint64_t)((uint32_t)rdi & 65536U) != 0UL);
            var_54 = (uint64_t)var_51;
            storemerge7 = 11UL;
            if ((uint64_t)(var_51 + 1U) != 0UL & (uint64_t)(var_52 + 1U) != 0UL & !(var_53 && (var_54 > storemerge6))) {
                storemerge7 = 0UL;
                if (rdx != 0UL) {
                    var_55 = (uint64_t *)(rdx + 64UL);
                    var_56 = *var_55;
                    var_57 = (uint64_t *)(rdx + 8UL);
                    var_58 = *var_57;
                    rax_0 = var_56;
                    r15_0 = var_56;
                    r14_1 = var_56;
                    storemerge7 = 12UL;
                    if (*(uint64_t *)rcx1_9 == var_56) {
                        r15_0 = *(uint64_t *)(rdx + 16UL);
                        r14_1 = var_58;
                    } else {
                        var_59 = (var_56 << 1UL) | 1UL;
                        *(uint64_t *)(local_sp_9 + 8UL) = rcx1_9;
                        var_60 = var_59 << 2UL;
                        *(uint64_t *)local_sp_9 = var_59;
                        var_61 = local_sp_9 + (-8L);
                        *(uint64_t *)var_61 = 4247025UL;
                        indirect_placeholder_1(var_58, var_60);
                        var_62 = (uint64_t *)(rdx + 16UL);
                        var_63 = *var_62;
                        var_64 = local_sp_9 + (-16L);
                        var_65 = (uint64_t *)var_64;
                        *var_65 = 4247040UL;
                        indirect_placeholder_1(var_63, var_60);
                        local_sp_10 = var_64;
                        if (var_56 != 0UL) {
                            *(uint64_t *)(local_sp_9 + (-24L)) = 4247101UL;
                            indirect_placeholder();
                            *(uint64_t *)(local_sp_9 + (-32L)) = 4247109UL;
                            indirect_placeholder();
                            return storemerge7;
                        }
                        var_66 = *var_65;
                        var_67 = *(uint64_t **)var_61;
                        var_68 = helper_movq_mm_T0_xmm_wrapper_359((struct type_4 *)(840UL), var_56);
                        var_69 = var_68.field_0;
                        var_70 = helper_movq_mm_T0_xmm_wrapper((struct type_4 *)(776UL), var_56);
                        var_71 = helper_punpcklqdq_xmm_wrapper_360((struct type_8 *)(0UL), (struct type_4 *)(776UL), (struct type_4 *)(840UL), var_70.field_0, var_69);
                        var_72 = var_71.field_1;
                        *var_57 = var_71.field_0;
                        *var_62 = var_72;
                        *var_67 = var_66;
                        rax_0 = *var_55;
                    }
                    var_73 = rax_0 + 1UL;
                    var_74 = rax_0 << 2UL;
                    *(uint32_t *)(var_74 + r14_1) = var_51;
                    *var_55 = var_73;
                    *(uint32_t *)(var_74 + r15_0) = var_52;
                    local_sp_11 = local_sp_10;
                }
                local_sp_12 = local_sp_11;
                while (1U)
                    {
                        local_sp_13 = local_sp_12;
                        rsi6_2 = rsi6_1;
                        var_75 = helper_cc_compute_c_wrapper(storemerge6 - rsi6_1, rsi6_1, var_1, 16U);
                        if (var_54 <= (uint64_t)(uint32_t)rsi6_1 & var_75 == 0UL) {
                            var_76 = local_sp_12 + (-8L);
                            *(uint64_t *)var_76 = 4246712UL;
                            var_77 = indirect_placeholder_1(rsi, rsi6_1);
                            local_sp_13 = var_76;
                            rsi6_2 = var_77;
                        }
                        local_sp_12 = local_sp_13;
                        if (rsi6_2 == 255UL) {
                            break;
                        }
                        rsi6_1 = rsi6_2 + 1UL;
                        continue;
                    }
            }
            return storemerge7;
        }
        local_sp_2 = local_sp_1;
        rcx1_2 = rcx1_1;
        rdx2_2 = rdx2_1;
        r85_2 = r85_1;
        storemerge8_in_in_in = r85_1 + 8UL;
    }
}
