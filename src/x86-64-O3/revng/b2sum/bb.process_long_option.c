typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_27_ret_type;
struct indirect_placeholder_28_ret_type;
struct indirect_placeholder_27_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_28_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r9(void);
extern struct indirect_placeholder_27_ret_type indirect_placeholder_27(uint64_t param_0);
extern struct indirect_placeholder_28_ret_type indirect_placeholder_28(uint64_t param_0);
uint64_t bb_process_long_option(uint64_t rcx, uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t *var_11;
    unsigned char var_12;
    uint64_t rbp_5;
    uint64_t rbp_6;
    uint64_t local_sp_0;
    uint64_t rbx_7;
    uint64_t rbx_0;
    uint64_t local_sp_1;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t rax_11;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t local_sp_4;
    uint64_t rax_10;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t local_sp_8;
    uint64_t r15_12;
    uint64_t rbx_6;
    uint64_t local_sp_2;
    uint64_t local_sp_3;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t local_sp_7;
    uint64_t var_28;
    uint64_t *var_29;
    uint64_t r15_10;
    uint64_t r14_3;
    uint64_t rbp_4;
    uint64_t rax_0;
    uint64_t r15_0;
    uint64_t var_30;
    uint64_t var_31;
    struct indirect_placeholder_27_ret_type var_32;
    uint64_t var_33;
    uint64_t local_sp_17;
    uint64_t local_sp_5;
    uint64_t rax_1;
    uint64_t r15_1;
    uint64_t var_36;
    uint64_t local_sp_10;
    uint64_t r14_5;
    uint64_t r12_0;
    uint64_t r12_4;
    uint64_t local_sp_19;
    uint64_t r15_2;
    uint64_t rbx_1;
    uint64_t var_65;
    uint64_t local_sp_6;
    uint64_t rbp_1;
    uint64_t r15_8;
    uint64_t r14_4;
    uint64_t rax_12;
    uint64_t rbx_5;
    uint32_t var_67;
    uint64_t r15_11;
    uint64_t var_68;
    uint64_t r12_3;
    uint64_t local_sp_18;
    uint64_t rax_3;
    uint64_t r15_3;
    uint64_t rbx_2;
    uint64_t var_69;
    uint64_t rax_4;
    uint64_t r15_4;
    uint64_t rbx_3;
    uint64_t var_70;
    uint64_t local_sp_11;
    uint64_t rax_5;
    uint64_t r14_0;
    uint64_t var_71;
    uint64_t var_72;
    uint32_t *var_73;
    uint64_t r12_2;
    uint64_t local_sp_16;
    uint64_t r15_9;
    uint32_t var_38;
    uint64_t rax_6;
    uint64_t var_39;
    uint32_t var_40;
    uint64_t var_41;
    uint64_t r14_2;
    uint64_t rax_7;
    uint64_t r15_5;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    struct indirect_placeholder_28_ret_type var_45;
    uint64_t var_46;
    uint64_t local_sp_9;
    uint64_t rbp_3;
    uint64_t rax_8;
    uint64_t r15_6;
    uint64_t r14_1;
    uint64_t var_49;
    uint64_t rbp_0;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t r15_7;
    uint32_t *var_52;
    uint32_t *_pre276;
    uint32_t *_pre_phi277;
    uint64_t var_61;
    uint64_t local_sp_20;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t rax_13;
    uint64_t local_sp_12;
    uint64_t r13_1;
    uint64_t rbp_2;
    uint64_t r12_1;
    uint64_t rbx_4;
    uint64_t var_74;
    uint32_t **var_75;
    uint32_t *var_76;
    uint64_t *var_77;
    uint64_t var_78;
    uint32_t var_79;
    uint64_t var_80;
    uint64_t var_81;
    unsigned char var_82;
    uint32_t var_83;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t *_pre_phi269;
    uint64_t local_sp_13;
    uint64_t var_88;
    uint64_t var_87;
    uint64_t var_89;
    uint64_t var_90;
    uint32_t var_91;
    uint64_t var_92;
    uint64_t *_pre_phi273;
    uint64_t rax_9;
    uint64_t var_84;
    uint64_t var_17;
    uint64_t r12_5;
    uint64_t local_sp_23;
    uint64_t local_sp_15;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    bool var_22;
    uint32_t var_23;
    bool var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_37;
    uint64_t var_27;
    uint64_t var_62;
    uint64_t var_66;
    bool var_63;
    uint64_t var_64;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t local_sp_21;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t local_sp_22;
    uint64_t var_97;
    uint32_t *var_98;
    uint64_t var_96;
    uint64_t var_16;
    unsigned int loop_state_var;
    var_14 = *var_11;
    var_15 = *(uint64_t *)var_14;
    *(uint64_t *)(var_0 + (-136L)) = var_15;
    rbp_2 = rbp_6;
    rax_13 = var_15;
    rbx_7 = var_14;
    if (var_15 == 0UL) {
        rax_9 = 4294967295UL;
        local_sp_21 = local_sp_20;
        if (*(uint32_t *)(local_sp_20 + 12UL) != 0U) {
            var_93 = local_sp_20 + 160UL;
            var_94 = *(uint64_t *)(*(uint64_t *)var_93 + 32UL);
            var_95 = local_sp_20 + (-8L);
            *(uint64_t *)var_95 = 4236883UL;
            indirect_placeholder();
            local_sp_21 = var_95;
            if (*(unsigned char *)(*(uint64_t *)(((uint64_t)**(uint32_t **)var_93 << 3UL) + *(uint64_t *)(local_sp_20 + 32UL)) + 1UL) != '-' & var_94 == 0UL) {
                return rax_9;
            }
        }
        local_sp_22 = local_sp_21;
        if (*(uint32_t *)(local_sp_21 + 168UL) != 0U) {
            var_96 = local_sp_21 + (-8L);
            *(uint64_t *)var_96 = 4237162UL;
            indirect_placeholder();
            local_sp_22 = var_96;
        }
        var_97 = *(uint64_t *)(local_sp_22 + 160UL);
        var_98 = (uint32_t *)var_97;
        *var_98 = (*var_98 + 1U);
        *(uint64_t *)(var_97 + 32UL) = 0UL;
        *(uint32_t *)(var_97 + 8UL) = 0U;
        return 63UL;
    }
    while (1U)
        {
            var_16 = local_sp_23 + (-8L);
            *(uint64_t *)var_16 = 4236225UL;
            indirect_placeholder();
            r12_0 = r12_5;
            r12_1 = r12_5;
            rbx_4 = rbx_7;
            local_sp_15 = var_16;
            if ((uint64_t)(uint32_t)rax_13 != 0UL) {
                var_17 = local_sp_23 + (-16L);
                *(uint64_t *)var_17 = 4236237UL;
                indirect_placeholder();
                local_sp_12 = var_17;
                local_sp_15 = var_17;
                if (rax_13 != r13_1) {
                    loop_state_var = 0U;
                    break;
                }
            }
            var_18 = rbx_7 + 32UL;
            var_19 = *(uint64_t *)var_18;
            var_20 = r12_5 + 1UL;
            var_21 = (uint64_t)(uint32_t)var_20;
            rbx_7 = var_18;
            local_sp_17 = local_sp_15;
            local_sp_19 = local_sp_15;
            rax_12 = var_21;
            local_sp_18 = local_sp_15;
            rax_4 = var_21;
            local_sp_16 = local_sp_15;
            rax_13 = var_21;
            local_sp_23 = local_sp_15;
            if (var_19 == 0UL) {
                r12_5 = (uint64_t)((long)(var_20 << 32UL) >> (long)32UL);
                continue;
            }
            var_22 = (*(uint32_t *)(local_sp_15 + 168UL) == 0U);
            var_23 = *(uint32_t *)(local_sp_15 + 12UL);
            if (var_22) {
                *(uint64_t *)(local_sp_15 + 40UL) = rbp_6;
                *(uint32_t *)(local_sp_15 + 60UL) = 4294967295U;
                var_62 = *(uint64_t *)local_sp_15;
                r12_3 = var_62;
                r12_4 = var_62;
                if (var_23 == 0U) {
                    var_63 = (var_21 == 0UL);
                    while (1U)
                        {
                            var_64 = local_sp_19 + (-8L);
                            *(uint64_t *)var_64 = 4236798UL;
                            indirect_placeholder();
                            r15_2 = r15_12;
                            rbx_1 = rbx_6;
                            local_sp_6 = var_64;
                            local_sp_19 = var_64;
                            rbx_1 = 1UL;
                            if (!var_63 && r15_12 == 0UL) {
                                *(uint32_t *)(local_sp_19 + 52UL) = (uint32_t)r14_5;
                                r15_2 = r12_4;
                                rbx_1 = rbx_6;
                            }
                            var_65 = r12_4 + 32UL;
                            r15_12 = r15_2;
                            rbx_6 = rbx_1;
                            r12_4 = var_65;
                            r15_4 = r15_2;
                            rbx_3 = rbx_1;
                            if (*(uint64_t *)var_65 == 0UL) {
                                break;
                            }
                            r14_5 = (uint64_t)((uint32_t)r14_5 + 1U);
                            continue;
                        }
                    loop_state_var = 2U;
                    break;
                }
                while (1U)
                    {
                        var_66 = local_sp_18 + (-8L);
                        *(uint64_t *)var_66 = 4236514UL;
                        indirect_placeholder();
                        local_sp_6 = var_66;
                        local_sp_18 = var_66;
                        rax_3 = rax_12;
                        r15_3 = r15_11;
                        rbx_2 = rbx_5;
                        if ((uint64_t)(uint32_t)rax_12 != 0UL) {
                            r15_3 = r12_3;
                            rbx_2 = 1UL;
                            if (r15_11 == 0UL) {
                                *(uint32_t *)(local_sp_18 + 52UL) = (uint32_t)r14_4;
                                rbx_2 = rbx_5;
                            } else {
                                var_67 = *(uint32_t *)(r12_3 + 8UL);
                                rax_3 = (uint64_t)var_67;
                                var_68 = *(uint64_t *)(r12_3 + 16UL);
                                rax_3 = var_68;
                                if ((uint64_t)(*(uint32_t *)(r15_11 + 8UL) - var_67) != 0UL & *(uint64_t *)(r15_11 + 16UL) == var_68) {
                                    rax_3 = 1UL;
                                    rbx_2 = ((uint64_t)(*(uint32_t *)(r15_11 + 24UL) - *(uint32_t *)(r12_3 + 24UL)) == 0UL) ? (uint64_t)(uint32_t)rbx_5 : 1UL;
                                }
                            }
                        }
                        var_69 = r12_3 + 32UL;
                        rax_12 = rax_3;
                        rbx_5 = rbx_2;
                        r15_11 = r15_3;
                        r12_3 = var_69;
                        rax_4 = rax_3;
                        r15_4 = r15_3;
                        rbx_3 = rbx_2;
                        if (*(uint64_t *)var_69 == 0UL) {
                            break;
                        }
                        r14_4 = (uint64_t)((uint32_t)r14_4 + 1U);
                        continue;
                    }
                loop_state_var = 2U;
                break;
            }
            var_24 = (var_23 == 0U);
            var_25 = *(uint64_t *)local_sp_15;
            var_26 = (uint64_t)((long)(var_20 << 32UL) >> (long)32UL);
            *(uint64_t *)(local_sp_15 + 80UL) = rbp_6;
            rax_10 = var_26;
            rbp_3 = var_25;
            rax_11 = var_26;
            rbp_4 = var_25;
            if (var_24) {
                *(uint64_t *)(local_sp_15 + 64UL) = 0UL;
                *(uint32_t *)(local_sp_15 + 60UL) = 4294967295U;
                *(uint32_t *)(local_sp_15 + 72UL) = 0U;
                *(uint32_t *)(local_sp_15 + 76UL) = 0U;
                *(uint64_t *)(local_sp_15 + 88UL) = var_26;
                while (1U)
                    {
                        var_27 = local_sp_17 + (-8L);
                        *(uint64_t *)var_27 = 4237041UL;
                        indirect_placeholder();
                        local_sp_4 = var_27;
                        rax_0 = rax_11;
                        r15_0 = r15_10;
                        local_sp_5 = var_27;
                        rax_1 = rax_11;
                        r15_1 = r15_10;
                        if ((uint64_t)(uint32_t)rax_11 != 0UL) {
                            var_28 = local_sp_17 + 56UL;
                            var_29 = (uint64_t *)var_28;
                            if (*var_29 == 0UL) {
                                *(uint32_t *)(local_sp_17 + 52UL) = (uint32_t)r14_3;
                                *var_29 = rbp_4;
                            } else {
                                if (*(uint32_t *)(local_sp_17 + 64UL) != 0U) {
                                    rax_1 = 0UL;
                                    r15_1 = 0UL;
                                    if (r15_10 == 0UL) {
                                        *(unsigned char *)(r14_3 + r15_0) = (unsigned char)'\x01';
                                        local_sp_5 = local_sp_4;
                                        rax_1 = rax_0;
                                        r15_1 = r15_0;
                                    } else {
                                        var_30 = *(uint64_t *)(local_sp_17 + 80UL);
                                        var_31 = local_sp_17 + (-16L);
                                        *(uint64_t *)var_31 = 4237619UL;
                                        var_32 = indirect_placeholder_27(var_30);
                                        var_33 = var_32.field_0;
                                        r15_0 = var_33;
                                        local_sp_5 = var_31;
                                        if (var_33 == 0UL) {
                                            *(uint32_t *)var_28 = 1U;
                                        } else {
                                            var_34 = local_sp_17 + (-24L);
                                            *(uint64_t *)var_34 = 4237646UL;
                                            indirect_placeholder();
                                            var_35 = (uint64_t)*(uint32_t *)(local_sp_17 + 36UL);
                                            *(uint32_t *)(local_sp_17 + 52UL) = 1U;
                                            *(unsigned char *)(var_33 + var_35) = (unsigned char)'\x01';
                                            local_sp_4 = var_34;
                                            rax_0 = var_35;
                                            *(unsigned char *)(r14_3 + r15_0) = (unsigned char)'\x01';
                                            local_sp_5 = local_sp_4;
                                            rax_1 = rax_0;
                                            r15_1 = r15_0;
                                        }
                                    }
                                }
                            }
                        }
                        var_36 = rbp_4 + 32UL;
                        rax_11 = rax_1;
                        r15_10 = r15_1;
                        rbp_4 = var_36;
                        local_sp_17 = local_sp_5;
                        local_sp_10 = local_sp_5;
                        r15_7 = r15_1;
                        if (*(uint64_t *)var_36 == 0UL) {
                            break;
                        }
                        r14_3 = r14_3 + 1UL;
                        continue;
                    }
                rbp_0 = *(uint64_t *)(local_sp_5 + 80UL);
                loop_state_var = 1U;
                break;
            }
            *(uint32_t *)(local_sp_15 + 72UL) = 0U;
            *(uint32_t *)(local_sp_15 + 76UL) = 0U;
            *(uint32_t *)(local_sp_15 + 60UL) = 4294967295U;
            *(uint64_t *)(local_sp_15 + 64UL) = var_26;
            *(uint32_t *)(local_sp_15 + 40UL) = (uint32_t)r12_5;
            while (1U)
                {
                    var_37 = local_sp_16 + (-8L);
                    *(uint64_t *)var_37 = 4236371UL;
                    indirect_placeholder();
                    local_sp_8 = var_37;
                    r15_5 = r15_9;
                    local_sp_9 = var_37;
                    rax_8 = rax_10;
                    r15_6 = r15_9;
                    r14_1 = r14_2;
                    if ((uint64_t)(uint32_t)rax_10 == 0UL) {
                        var_49 = rbp_3 + 32UL;
                        rax_10 = rax_8;
                        local_sp_10 = local_sp_9;
                        local_sp_16 = local_sp_9;
                        r15_9 = r15_6;
                        r14_2 = r14_1;
                        rbp_3 = var_49;
                        r15_7 = r15_6;
                        if (*(uint64_t *)var_49 == 0UL) {
                            break;
                        }
                        r12_2 = r12_2 + 1UL;
                        continue;
                    }
                    r14_1 = rbp_3;
                    if (r14_2 == 0UL) {
                        *(uint32_t *)(local_sp_16 + 52UL) = (uint32_t)r12_2;
                    } else {
                        var_38 = *(uint32_t *)(rbp_3 + 8UL);
                        rax_6 = (uint64_t)var_38;
                        if ((uint64_t)(*(uint32_t *)(r14_2 + 8UL) - var_38) != 0UL) {
                            var_39 = *(uint64_t *)(rbp_3 + 16UL);
                            rax_6 = var_39;
                            var_40 = *(uint32_t *)(rbp_3 + 24UL);
                            var_41 = (uint64_t)var_40;
                            rax_6 = var_41;
                            rax_8 = var_41;
                            if (*(uint64_t *)(r14_2 + 16UL) != var_39 & (uint64_t)(*(uint32_t *)(r14_2 + 24UL) - var_40) != 0UL) {
                                var_49 = rbp_3 + 32UL;
                                rax_10 = rax_8;
                                local_sp_10 = local_sp_9;
                                local_sp_16 = local_sp_9;
                                r15_9 = r15_6;
                                r14_2 = r14_1;
                                rbp_3 = var_49;
                                r15_7 = r15_6;
                                if (*(uint64_t *)var_49 == 0UL) {
                                    break;
                                }
                                r12_2 = r12_2 + 1UL;
                                continue;
                            }
                        }
                        rax_7 = rax_6;
                        rax_8 = rax_6;
                        if (*(uint32_t *)(local_sp_16 + 64UL) != 0U) {
                            rax_8 = 0UL;
                            r15_6 = 0UL;
                            if (r15_9 == 0UL) {
                                *(unsigned char *)(r12_2 + r15_5) = (unsigned char)'\x01';
                                local_sp_9 = local_sp_8;
                                rax_8 = rax_7;
                                r15_6 = r15_5;
                            } else {
                                var_42 = local_sp_16 + 56UL;
                                var_43 = *(uint64_t *)var_42;
                                var_44 = local_sp_16 + (-16L);
                                *(uint64_t *)var_44 = 4237559UL;
                                var_45 = indirect_placeholder_28(var_43);
                                var_46 = var_45.field_0;
                                r15_5 = var_46;
                                local_sp_9 = var_44;
                                if (var_46 == 0UL) {
                                    *(uint32_t *)var_42 = 1U;
                                } else {
                                    var_47 = local_sp_16 + (-24L);
                                    *(uint64_t *)var_47 = 4237586UL;
                                    indirect_placeholder();
                                    var_48 = (uint64_t)*(uint32_t *)(local_sp_16 + 36UL);
                                    *(uint32_t *)(local_sp_16 + 52UL) = 1U;
                                    *(unsigned char *)(var_46 + var_48) = (unsigned char)'\x01';
                                    local_sp_8 = var_47;
                                    rax_7 = var_48;
                                    *(unsigned char *)(r12_2 + r15_5) = (unsigned char)'\x01';
                                    local_sp_9 = local_sp_8;
                                    rax_8 = rax_7;
                                    r15_6 = r15_5;
                                }
                            }
                        }
                    }
                }
            var_50 = *(uint64_t *)(local_sp_9 + 80UL);
            var_51 = (uint64_t)*(uint32_t *)(local_sp_9 + 40UL);
            *(uint64_t *)(local_sp_9 + 64UL) = r14_1;
            rbp_0 = var_50;
            r12_0 = var_51;
            loop_state_var = 1U;
            break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            var_74 = local_sp_12 + 160UL;
            var_75 = (uint32_t **)var_74;
            var_76 = *var_75;
            var_77 = (uint64_t *)var_74;
            var_78 = *var_77;
            var_79 = *var_76;
            var_80 = (uint64_t)var_79;
            *(uint64_t *)(var_78 + 32UL) = 0UL;
            var_81 = var_80 + 1UL;
            *(uint32_t *)var_78 = (uint32_t)var_81;
            var_82 = *(unsigned char *)rbp_2;
            var_83 = *(uint32_t *)(rbx_4 + 8UL);
            _pre_phi269 = var_77;
            local_sp_13 = local_sp_12;
            _pre_phi273 = var_77;
            if (var_82 == '\x00') {
                if (var_83 != 1U) {
                    var_85 = (uint64_t)*(uint32_t *)(local_sp_12 + 56UL);
                    var_86 = var_81 << 32UL;
                    if ((long)var_86 >= (long)(var_85 << 32UL)) {
                        if (*(uint32_t *)(local_sp_12 + 168UL) == 0U) {
                            var_87 = local_sp_12 + (-8L);
                            *(uint64_t *)var_87 = 4237935UL;
                            indirect_placeholder();
                            _pre_phi269 = (uint64_t *)(var_87 + 160UL);
                            local_sp_13 = var_87;
                        }
                        *(uint32_t *)(*_pre_phi269 + 8UL) = *(uint32_t *)(rbx_4 + 24UL);
                        var_88 = (**(unsigned char **)(local_sp_13 + 48UL) == ':') ? 58UL : 63UL;
                        rax_9 = var_88;
                        return rax_9;
                    }
                    **var_75 = (var_79 + 2U);
                    *(uint64_t *)(*var_77 + 16UL) = *(uint64_t *)((uint64_t)((long)var_86 >> (long)29UL) + *(uint64_t *)(local_sp_12 + 32UL));
                }
            } else {
                if (var_83 != 0U) {
                    if (*(uint32_t *)(local_sp_12 + 168UL) == 0U) {
                        var_84 = local_sp_12 + (-8L);
                        *(uint64_t *)var_84 = 4237534UL;
                        indirect_placeholder();
                        _pre_phi273 = (uint64_t *)(var_84 + 160UL);
                    }
                    *(uint32_t *)(*_pre_phi273 + 8UL) = *(uint32_t *)(rbx_4 + 24UL);
                    return rax_9;
                }
                *(uint64_t *)(*var_77 + 16UL) = (rbp_2 + 1UL);
            }
            var_89 = *(uint64_t *)(local_sp_12 + 24UL);
            if (var_89 == 0UL) {
                *(uint32_t *)var_89 = (uint32_t)r12_1;
            }
            var_90 = *(uint64_t *)(rbx_4 + 16UL);
            var_91 = *(uint32_t *)(rbx_4 + 24UL);
            var_92 = (uint64_t)var_91;
            rax_9 = var_92;
            if (var_90 != 0UL) {
                *(uint32_t *)var_90 = var_91;
                rax_9 = 0UL;
            }
            return rax_9;
        }
        break;
      case 1U:
        {
            rbx_0 = r15_7;
            local_sp_11 = local_sp_10;
            rbp_1 = rbp_0;
            if (r15_7 != 0UL) {
                _pre276 = (uint32_t *)(local_sp_10 + 72UL);
                _pre_phi277 = _pre276;
                if (*_pre_phi277 == 0U) {
                    var_53 = local_sp_10 + (-8L);
                    *(uint64_t *)var_53 = 4237425UL;
                    indirect_placeholder();
                    local_sp_2 = var_53;
                } else {
                    var_54 = r12_0 + r15_7;
                    *(uint64_t *)(local_sp_10 + (-8L)) = 4237769UL;
                    indirect_placeholder();
                    var_55 = local_sp_10 + (-16L);
                    *(uint64_t *)var_55 = 4237812UL;
                    indirect_placeholder();
                    local_sp_0 = var_55;
                    while (1U)
                        {
                            local_sp_1 = local_sp_0;
                            if (*(unsigned char *)rbx_0 == '\x00') {
                                var_56 = local_sp_0 + (-8L);
                                *(uint64_t *)var_56 = 4237869UL;
                                indirect_placeholder();
                                local_sp_1 = var_56;
                            }
                            local_sp_0 = local_sp_1;
                            if (var_54 == rbx_0) {
                                break;
                            }
                            rbx_0 = rbx_0 + 1UL;
                            continue;
                        }
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4237884UL;
                    indirect_placeholder();
                    var_57 = local_sp_1 + (-16L);
                    *(uint64_t *)var_57 = 4237892UL;
                    indirect_placeholder();
                    local_sp_2 = var_57;
                }
                local_sp_3 = local_sp_2;
                if (*(uint32_t *)(local_sp_2 + 76UL) == 0U) {
                    var_58 = local_sp_2 + (-8L);
                    *(uint64_t *)var_58 = 4237441UL;
                    indirect_placeholder();
                    local_sp_3 = var_58;
                }
                var_59 = *(uint64_t *)(local_sp_3 + 160UL);
                var_60 = *(uint64_t *)(var_59 + 32UL);
                local_sp_7 = local_sp_3;
                rax_5 = var_59;
                r14_0 = var_60;
                *(uint64_t *)(local_sp_7 + (-8L)) = 4237461UL;
                indirect_placeholder();
                var_71 = r14_0 + rax_5;
                var_72 = *(uint64_t *)(local_sp_7 + 152UL);
                var_73 = (uint32_t *)var_72;
                *var_73 = (*var_73 + 1U);
                *(uint64_t *)(var_72 + 32UL) = var_71;
                *(uint32_t *)(var_72 + 8UL) = 0U;
                return rax_9;
            }
            var_52 = (uint32_t *)(local_sp_10 + 72UL);
            _pre_phi277 = var_52;
            if (*var_52 != 0U) {
                if (*_pre_phi277 == 0U) {
                    var_53 = local_sp_10 + (-8L);
                    *(uint64_t *)var_53 = 4237425UL;
                    indirect_placeholder();
                    local_sp_2 = var_53;
                } else {
                    var_54 = r12_0 + r15_7;
                    *(uint64_t *)(local_sp_10 + (-8L)) = 4237769UL;
                    indirect_placeholder();
                    var_55 = local_sp_10 + (-16L);
                    *(uint64_t *)var_55 = 4237812UL;
                    indirect_placeholder();
                    local_sp_0 = var_55;
                    while (1U)
                        {
                            local_sp_1 = local_sp_0;
                            if (*(unsigned char *)rbx_0 == '\x00') {
                                var_56 = local_sp_0 + (-8L);
                                *(uint64_t *)var_56 = 4237869UL;
                                indirect_placeholder();
                                local_sp_1 = var_56;
                            }
                            local_sp_0 = local_sp_1;
                            if (var_54 == rbx_0) {
                                break;
                            }
                            rbx_0 = rbx_0 + 1UL;
                            continue;
                        }
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4237884UL;
                    indirect_placeholder();
                    var_57 = local_sp_1 + (-16L);
                    *(uint64_t *)var_57 = 4237892UL;
                    indirect_placeholder();
                    local_sp_2 = var_57;
                }
                local_sp_3 = local_sp_2;
                if (*(uint32_t *)(local_sp_2 + 76UL) == 0U) {
                    var_58 = local_sp_2 + (-8L);
                    *(uint64_t *)var_58 = 4237441UL;
                    indirect_placeholder();
                    local_sp_3 = var_58;
                }
                var_59 = *(uint64_t *)(local_sp_3 + 160UL);
                var_60 = *(uint64_t *)(var_59 + 32UL);
                local_sp_7 = local_sp_3;
                rax_5 = var_59;
                r14_0 = var_60;
                *(uint64_t *)(local_sp_7 + (-8L)) = 4237461UL;
                indirect_placeholder();
                var_71 = r14_0 + rax_5;
                var_72 = *(uint64_t *)(local_sp_7 + 152UL);
                var_73 = (uint32_t *)var_72;
                *var_73 = (*var_73 + 1U);
                *(uint64_t *)(var_72 + 32UL) = var_71;
                *(uint32_t *)(var_72 + 8UL) = 0U;
                return rax_9;
            }
            var_61 = *(uint64_t *)(local_sp_10 + 64UL);
            r15_8 = var_61;
            local_sp_12 = local_sp_11;
            rbp_2 = rbp_1;
            rbx_4 = r15_8;
            local_sp_20 = local_sp_11;
            if (r15_8 != 0UL) {
                rax_9 = 4294967295UL;
                local_sp_21 = local_sp_20;
                if (*(uint32_t *)(local_sp_20 + 12UL) != 0U) {
                    var_93 = local_sp_20 + 160UL;
                    var_94 = *(uint64_t *)(*(uint64_t *)var_93 + 32UL);
                    var_95 = local_sp_20 + (-8L);
                    *(uint64_t *)var_95 = 4236883UL;
                    indirect_placeholder();
                    local_sp_21 = var_95;
                    if (*(unsigned char *)(*(uint64_t *)(((uint64_t)**(uint32_t **)var_93 << 3UL) + *(uint64_t *)(local_sp_20 + 32UL)) + 1UL) != '-' & var_94 == 0UL) {
                        return rax_9;
                    }
                }
                local_sp_22 = local_sp_21;
                if (*(uint32_t *)(local_sp_21 + 168UL) != 0U) {
                    var_96 = local_sp_21 + (-8L);
                    *(uint64_t *)var_96 = 4237162UL;
                    indirect_placeholder();
                    local_sp_22 = var_96;
                }
                var_97 = *(uint64_t *)(local_sp_22 + 160UL);
                var_98 = (uint32_t *)var_97;
                *var_98 = (*var_98 + 1U);
                *(uint64_t *)(var_97 + 32UL) = 0UL;
                *(uint32_t *)(var_97 + 8UL) = 0U;
                return 63UL;
            }
            r12_1 = (uint64_t)*(uint32_t *)(local_sp_11 + 60UL);
        }
        break;
      case 2U:
        {
            var_70 = *(uint64_t *)(local_sp_6 + 40UL);
            local_sp_7 = local_sp_6;
            rbp_1 = var_70;
            r15_8 = r15_4;
            local_sp_11 = local_sp_6;
            rax_5 = rax_4;
            if ((uint64_t)(uint32_t)rbx_3 == 0UL) {
                *(uint64_t *)(local_sp_7 + (-8L)) = 4237461UL;
                indirect_placeholder();
                var_71 = r14_0 + rax_5;
                var_72 = *(uint64_t *)(local_sp_7 + 152UL);
                var_73 = (uint32_t *)var_72;
                *var_73 = (*var_73 + 1U);
                *(uint64_t *)(var_72 + 32UL) = var_71;
                *(uint32_t *)(var_72 + 8UL) = 0U;
                return rax_9;
            }
            local_sp_12 = local_sp_11;
            rbp_2 = rbp_1;
            rbx_4 = r15_8;
            local_sp_20 = local_sp_11;
            if (r15_8 != 0UL) {
                rax_9 = 4294967295UL;
                local_sp_21 = local_sp_20;
                if (*(uint32_t *)(local_sp_20 + 12UL) != 0U) {
                    local_sp_22 = local_sp_21;
                    if (*(uint32_t *)(local_sp_21 + 168UL) == 0U) {
                        var_96 = local_sp_21 + (-8L);
                        *(uint64_t *)var_96 = 4237162UL;
                        indirect_placeholder();
                        local_sp_22 = var_96;
                    }
                    var_97 = *(uint64_t *)(local_sp_22 + 160UL);
                    var_98 = (uint32_t *)var_97;
                    *var_98 = (*var_98 + 1U);
                    *(uint64_t *)(var_97 + 32UL) = 0UL;
                    *(uint32_t *)(var_97 + 8UL) = 0U;
                    return 63UL;
                }
                var_93 = local_sp_20 + 160UL;
                if (*(unsigned char *)(*(uint64_t *)(((uint64_t)**(uint32_t **)var_93 << 3UL) + *(uint64_t *)(local_sp_20 + 32UL)) + 1UL) != '-') {
                    var_94 = *(uint64_t *)(*(uint64_t *)var_93 + 32UL);
                    var_95 = local_sp_20 + (-8L);
                    *(uint64_t *)var_95 = 4236883UL;
                    indirect_placeholder();
                    local_sp_21 = var_95;
                    if (var_94 == 0UL) {
                        return rax_9;
                    }
                }
                local_sp_22 = local_sp_21;
                if (*(uint32_t *)(local_sp_21 + 168UL) == 0U) {
                    var_96 = local_sp_21 + (-8L);
                    *(uint64_t *)var_96 = 4237162UL;
                    indirect_placeholder();
                    local_sp_22 = var_96;
                }
                var_97 = *(uint64_t *)(local_sp_22 + 160UL);
                var_98 = (uint32_t *)var_97;
                *var_98 = (*var_98 + 1U);
                *(uint64_t *)(var_97 + 32UL) = 0UL;
                *(uint32_t *)(var_97 + 8UL) = 0U;
                return 63UL;
            }
            r12_1 = (uint64_t)*(uint32_t *)(local_sp_11 + 60UL);
        }
        break;
    }
}
