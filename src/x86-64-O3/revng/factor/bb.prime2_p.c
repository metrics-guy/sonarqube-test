typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_movq_mm_T0_xmm_wrapper_ret_type;
struct type_4;
struct helper_movq_mm_T0_xmm_wrapper_100_ret_type;
struct helper_punpcklqdq_xmm_wrapper_ret_type;
struct type_8;
struct indirect_placeholder_22_ret_type;
struct indirect_placeholder_23_ret_type;
struct indirect_placeholder_24_ret_type;
struct indirect_placeholder_25_ret_type;
struct helper_movq_mm_T0_xmm_wrapper_101_ret_type;
struct helper_movq_mm_T0_xmm_wrapper_102_ret_type;
struct helper_punpcklqdq_xmm_wrapper_103_ret_type;
struct helper_movq_mm_T0_xmm_wrapper_104_ret_type;
struct helper_punpcklqdq_xmm_wrapper_105_ret_type;
struct helper_movq_mm_T0_xmm_wrapper_106_ret_type;
struct helper_punpcklqdq_xmm_wrapper_107_ret_type;
struct indirect_placeholder_26_ret_type;
struct indirect_placeholder_27_ret_type;
struct helper_movq_mm_T0_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_4 {
};
struct helper_movq_mm_T0_xmm_wrapper_100_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_punpcklqdq_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
};
struct type_8 {
};
struct indirect_placeholder_22_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_23_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_24_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_25_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct helper_movq_mm_T0_xmm_wrapper_101_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_movq_mm_T0_xmm_wrapper_102_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_punpcklqdq_xmm_wrapper_103_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
};
struct helper_movq_mm_T0_xmm_wrapper_104_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_punpcklqdq_xmm_wrapper_105_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
};
struct helper_movq_mm_T0_xmm_wrapper_106_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_punpcklqdq_xmm_wrapper_107_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
};
struct indirect_placeholder_26_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_27_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
    uint64_t field_8;
    uint64_t field_9;
    uint64_t field_10;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct helper_movq_mm_T0_xmm_wrapper_ret_type helper_movq_mm_T0_xmm_wrapper(struct type_4 *param_0, uint64_t param_1);
extern struct helper_movq_mm_T0_xmm_wrapper_100_ret_type helper_movq_mm_T0_xmm_wrapper_100(struct type_4 *param_0, uint64_t param_1);
extern struct helper_punpcklqdq_xmm_wrapper_ret_type helper_punpcklqdq_xmm_wrapper(struct type_8 *param_0, struct type_4 *param_1, struct type_4 *param_2, uint64_t param_3, uint64_t param_4);
extern void indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern uint64_t init_rcx(void);
extern struct indirect_placeholder_22_ret_type indirect_placeholder_22(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_23_ret_type indirect_placeholder_23(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_24_ret_type indirect_placeholder_24(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_25_ret_type indirect_placeholder_25(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct helper_movq_mm_T0_xmm_wrapper_101_ret_type helper_movq_mm_T0_xmm_wrapper_101(struct type_4 *param_0, uint64_t param_1);
extern struct helper_movq_mm_T0_xmm_wrapper_102_ret_type helper_movq_mm_T0_xmm_wrapper_102(struct type_4 *param_0, uint64_t param_1);
extern struct helper_punpcklqdq_xmm_wrapper_103_ret_type helper_punpcklqdq_xmm_wrapper_103(struct type_8 *param_0, struct type_4 *param_1, struct type_4 *param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t helper_ctz_wrapper(uint64_t param_0);
extern struct helper_movq_mm_T0_xmm_wrapper_104_ret_type helper_movq_mm_T0_xmm_wrapper_104(struct type_4 *param_0, uint64_t param_1);
extern struct helper_punpcklqdq_xmm_wrapper_105_ret_type helper_punpcklqdq_xmm_wrapper_105(struct type_8 *param_0, struct type_4 *param_1, struct type_4 *param_2, uint64_t param_3, uint64_t param_4);
extern struct helper_movq_mm_T0_xmm_wrapper_106_ret_type helper_movq_mm_T0_xmm_wrapper_106(struct type_4 *param_0, uint64_t param_1);
extern struct helper_punpcklqdq_xmm_wrapper_107_ret_type helper_punpcklqdq_xmm_wrapper_107(struct type_8 *param_0, struct type_4 *param_1, struct type_4 *param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_26_ret_type indirect_placeholder_26(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_27_ret_type indirect_placeholder_27(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
uint64_t bb_prime2_p(uint64_t rdi, uint64_t rsi) {
    struct helper_punpcklqdq_xmm_wrapper_103_ret_type var_88;
    struct helper_punpcklqdq_xmm_wrapper_ret_type var_152;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t rbp_2;
    unsigned char var_104;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t local_sp_1;
    uint64_t local_sp_4;
    uint64_t var_107;
    uint64_t local_sp_0;
    uint64_t rbx_1;
    uint64_t var_163;
    uint64_t rbp_0;
    uint64_t rbp_1;
    uint64_t var_164;
    uint64_t local_sp_6;
    uint64_t rbx_5;
    uint64_t rbp_5;
    uint64_t var_128;
    uint64_t var_129;
    uint64_t local_sp_5;
    uint64_t var_133;
    uint64_t var_134;
    uint64_t var_130;
    uint64_t var_131;
    uint64_t var_132;
    uint64_t rax_1;
    uint64_t var_145;
    uint64_t r14_5;
    uint64_t r14_2;
    uint64_t cc_src2_0;
    uint64_t r12_5;
    uint64_t r12_2;
    uint64_t r14_0;
    uint64_t rbx_2;
    uint64_t r12_0;
    uint64_t r13_2;
    uint64_t rbx_0;
    uint64_t cc_src2_7;
    uint64_t r13_0;
    uint64_t storemerge;
    uint64_t cc_src2_1;
    uint64_t r14_1;
    uint64_t r12_1;
    uint64_t r13_1;
    uint64_t var_135;
    bool var_136;
    uint64_t var_137;
    uint64_t var_138;
    uint64_t cc_src2_3;
    uint64_t rax_0;
    uint64_t rcx_0;
    uint64_t rdx_1;
    uint64_t cc_src2_2;
    uint64_t rdx_0;
    uint64_t var_139;
    uint64_t var_140;
    uint64_t var_141;
    uint64_t var_142;
    uint64_t var_143;
    uint64_t var_144;
    uint64_t state_0x8558_0;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_146;
    struct helper_movq_mm_T0_xmm_wrapper_ret_type var_147;
    uint64_t var_148;
    struct helper_movq_mm_T0_xmm_wrapper_100_ret_type var_149;
    uint64_t var_150;
    uint64_t var_151;
    uint64_t var_153;
    uint64_t var_154;
    uint64_t var_155;
    uint64_t var_156;
    uint64_t var_157;
    uint64_t var_158;
    struct indirect_placeholder_22_ret_type var_159;
    uint64_t var_160;
    uint64_t var_161;
    uint64_t var_162;
    uint64_t var_103;
    uint64_t local_sp_2;
    uint64_t cc_src2_4;
    uint64_t local_sp_3;
    uint64_t var_108;
    uint64_t var_109;
    uint64_t rbp_3;
    uint64_t cc_src2_5;
    uint64_t r14_3;
    uint64_t r12_3;
    uint64_t r13_5;
    uint64_t rbx_3;
    uint64_t r13_3;
    uint64_t var_110;
    uint64_t var_117;
    uint64_t local_sp_7;
    uint64_t var_111;
    uint64_t var_112;
    uint64_t var_113;
    uint64_t var_114;
    uint64_t var_115;
    uint64_t var_116;
    uint64_t rbp_4;
    uint64_t cc_src2_6;
    uint64_t r14_4;
    uint64_t r12_4;
    uint64_t rbx_4;
    uint64_t rsi2_0;
    uint64_t r13_4;
    uint64_t var_118;
    uint64_t var_119;
    uint64_t var_120;
    uint64_t var_121;
    uint64_t var_122;
    uint64_t var_123;
    struct indirect_placeholder_23_ret_type var_124;
    uint64_t var_125;
    bool var_126;
    uint64_t var_127;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t *var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t var_100;
    uint64_t var_101;
    struct indirect_placeholder_24_ret_type var_102;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t *var_83;
    uint64_t var_84;
    struct helper_movq_mm_T0_xmm_wrapper_101_ret_type var_85;
    uint64_t var_86;
    struct helper_movq_mm_T0_xmm_wrapper_102_ret_type var_87;
    uint64_t var_89;
    uint64_t cc_src2_10;
    uint64_t local_sp_8;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t *var_19;
    struct helper_movq_mm_T0_xmm_wrapper_ret_type var_20;
    uint64_t *_pre_phi;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t *var_24;
    struct helper_movq_mm_T0_xmm_wrapper_ret_type var_25;
    uint64_t var_26;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t cc_src2_9;
    uint64_t rax_2;
    uint64_t rcx_1;
    uint64_t rdx_3;
    uint64_t cc_src2_8;
    uint64_t rdx_2;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t rax_3;
    uint64_t var_46;
    uint64_t var_47;
    struct helper_movq_mm_T0_xmm_wrapper_104_ret_type var_48;
    uint64_t var_49;
    struct helper_movq_mm_T0_xmm_wrapper_ret_type var_50;
    struct helper_punpcklqdq_xmm_wrapper_105_ret_type var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t *var_58;
    uint64_t var_59;
    uint64_t *var_60;
    uint64_t _pre_phi296;
    uint64_t _pre;
    uint64_t _pre295;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t _pre_phi294;
    uint64_t var_65;
    struct helper_movq_mm_T0_xmm_wrapper_106_ret_type var_66;
    uint64_t var_67;
    uint64_t var_68;
    struct helper_movq_mm_T0_xmm_wrapper_ret_type var_69;
    struct helper_punpcklqdq_xmm_wrapper_107_ret_type var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t *var_75;
    struct indirect_placeholder_26_ret_type var_76;
    unsigned char *var_77;
    unsigned char var_78;
    uint64_t var_165;
    struct indirect_placeholder_27_ret_type var_166;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rcx();
    var_2 = init_rbp();
    var_3 = init_cc_src2();
    var_4 = init_r15();
    var_5 = init_r14();
    var_6 = init_r12();
    var_7 = init_rbx();
    var_8 = init_r13();
    *(uint64_t *)(var_0 + (-8L)) = var_4;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    *(uint64_t *)(var_0 + (-24L)) = var_8;
    *(uint64_t *)(var_0 + (-32L)) = var_6;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    *(uint64_t *)(var_0 + (-48L)) = var_7;
    var_9 = var_0 + (-472L);
    rbp_2 = 2UL;
    r12_2 = 4280928UL;
    rbx_2 = rdi;
    r12_3 = 1UL;
    rsi2_0 = 0UL;
    if (rdi == 0UL) {
        var_165 = var_0 + (-480L);
        *(uint64_t *)var_165 = 4212490UL;
        var_166 = indirect_placeholder_27(var_2, rsi, var_5, var_6, var_7, rsi, var_8);
        *(unsigned char *)(var_0 + (-417L)) = (unsigned char)var_166.field_0;
        local_sp_8 = var_165;
    } else {
        var_10 = (rsi == 0UL) + rdi;
        *(uint64_t *)(var_0 + (-440L)) = var_10;
        var_11 = rsi + (-1L);
        var_12 = (uint64_t *)(var_0 + (-448L));
        *var_12 = var_11;
        if (var_11 == 0UL) {
            var_21 = helper_ctz_wrapper(var_10);
            var_22 = (var_10 == 0UL) ? var_1 : var_21;
            var_23 = var_10 >> (var_22 & 63UL);
            var_24 = (uint64_t *)var_9;
            *var_24 = 0UL;
            var_25 = helper_movq_mm_T0_xmm_wrapper((struct type_4 *)(776UL), var_23);
            var_26 = var_25.field_0;
            *(uint32_t *)(var_0 + (-452L)) = ((uint32_t)var_22 + 64U);
            _pre_phi = var_24;
            state_0x8558_0 = var_26;
        } else {
            var_13 = helper_ctz_wrapper(var_11);
            var_14 = 0UL - var_13;
            *(uint32_t *)(var_0 + (-452L)) = (uint32_t)var_13;
            var_15 = var_10 << (var_14 & 63UL);
            var_16 = var_13 & 63UL;
            var_17 = var_10 >> var_16;
            var_18 = var_11 >> var_16;
            var_19 = (uint64_t *)var_9;
            *var_19 = var_17;
            var_20 = helper_movq_mm_T0_xmm_wrapper((struct type_4 *)(776UL), var_15 | var_18);
            _pre_phi = var_19;
            state_0x8558_0 = var_20.field_0;
        }
        var_27 = *_pre_phi;
        var_28 = rsi >> 1UL;
        var_29 = var_0 + (-408L);
        *(uint64_t *)var_29 = state_0x8558_0;
        *(uint64_t *)(var_0 + (-400L)) = var_27;
        var_30 = (uint64_t)*(unsigned char *)((var_28 & 127UL) + 4269280UL);
        var_31 = (var_30 << 1UL) - ((var_30 * var_30) * rsi);
        var_32 = (var_31 << 1UL) - ((var_31 * var_31) * rsi);
        var_33 = (var_32 << 1UL) - ((var_32 * var_32) * rsi);
        var_34 = rdi + (-2L);
        var_35 = helper_cc_compute_c_wrapper(var_34, 2UL, var_3, 17U);
        var_36 = ((0UL - var_35) & 64UL) | 63UL;
        var_37 = helper_cc_compute_c_wrapper(var_34, 2UL, var_35, 17U);
        var_38 = (uint64_t)(unsigned char)var_37;
        var_39 = helper_cc_compute_c_wrapper(var_34, 2UL, var_35, 17U);
        r13_2 = var_33;
        rax_2 = 1UL - var_39;
        rcx_1 = var_36;
        cc_src2_8 = var_39;
        rdx_2 = var_38;
        while (1U)
            {
                var_40 = rax_2 << 1UL;
                var_41 = rdx_2 << 1UL;
                var_42 = var_40 | (rdx_2 >> 63UL);
                var_43 = rdi - var_42;
                var_44 = helper_cc_compute_c_wrapper(var_43, var_42, cc_src2_8, 17U);
                rax_3 = var_42;
                cc_src2_9 = cc_src2_8;
                rdx_3 = var_41;
                if (var_44 == 0UL) {
                    var_46 = var_41 - rsi;
                    var_47 = helper_cc_compute_c_wrapper(var_46, rsi, cc_src2_8, 17U);
                    rax_3 = (var_42 - rdi) - var_47;
                    cc_src2_9 = var_47;
                    rdx_3 = var_46;
                } else {
                    var_45 = helper_cc_compute_all_wrapper(var_43, var_42, cc_src2_8, 17U);
                    if (((var_45 & 64UL) == 0UL) || (var_41 < rsi)) {
                        var_46 = var_41 - rsi;
                        var_47 = helper_cc_compute_c_wrapper(var_46, rsi, cc_src2_8, 17U);
                        rax_3 = (var_42 - rdi) - var_47;
                        cc_src2_9 = var_47;
                        rdx_3 = var_46;
                    }
                }
                rax_2 = rax_3;
                cc_src2_8 = cc_src2_9;
                rdx_2 = rdx_3;
                if (rcx_1 == 0UL) {
                    break;
                }
                rcx_1 = rcx_1 + (-1L);
                continue;
            }
        var_48 = helper_movq_mm_T0_xmm_wrapper_104((struct type_4 *)(1032UL), rax_3);
        var_49 = var_48.field_0;
        var_50 = helper_movq_mm_T0_xmm_wrapper((struct type_4 *)(776UL), rdx_3);
        var_51 = helper_punpcklqdq_xmm_wrapper_105((struct type_8 *)(0UL), (struct type_4 *)(776UL), (struct type_4 *)(1032UL), var_50.field_0, var_49);
        var_52 = var_51.field_0;
        var_53 = var_51.field_1;
        var_54 = rdx_3 << 1UL;
        var_55 = helper_cc_compute_c_wrapper(var_54, rdx_3, cc_src2_9, 9U);
        var_56 = (rax_3 << 1UL) + var_55;
        var_57 = var_0 + (-376L);
        *(uint64_t *)var_57 = var_52;
        *(uint64_t *)(var_0 + (-368L)) = var_53;
        var_58 = (uint64_t *)(var_0 + (-384L));
        *var_58 = var_56;
        var_59 = var_0 + (-392L);
        var_60 = (uint64_t *)var_59;
        *var_60 = var_54;
        cc_src2_10 = var_55;
        if (var_56 > rdi) {
            _pre = var_54 - rsi;
            _pre295 = var_56 - rdi;
            _pre_phi296 = _pre295;
            _pre_phi294 = _pre;
            var_65 = helper_cc_compute_c_wrapper(_pre_phi294, rsi, var_55, 17U);
            *var_58 = (_pre_phi296 - var_65);
            *var_60 = _pre_phi294;
            cc_src2_10 = var_65;
        } else {
            var_61 = var_56 - rdi;
            var_62 = helper_cc_compute_all_wrapper(var_61, rdi, var_55, 17U);
            _pre_phi296 = var_61;
            var_63 = var_54 - rsi;
            var_64 = helper_cc_compute_c_wrapper(var_63, rsi, var_55, 17U);
            _pre_phi294 = var_63;
            if ((var_62 & 64UL) != 0UL & var_64 == 0UL) {
                var_65 = helper_cc_compute_c_wrapper(_pre_phi294, rsi, var_55, 17U);
                *var_58 = (_pre_phi296 - var_65);
                *var_60 = _pre_phi294;
                cc_src2_10 = var_65;
            }
        }
        var_66 = helper_movq_mm_T0_xmm_wrapper_106((struct type_4 *)(1096UL), rdi);
        var_67 = var_66.field_0;
        var_68 = (uint64_t)*(uint32_t *)(var_0 + (-452L));
        var_69 = helper_movq_mm_T0_xmm_wrapper((struct type_4 *)(776UL), rsi);
        var_70 = helper_punpcklqdq_xmm_wrapper_107((struct type_8 *)(0UL), (struct type_4 *)(776UL), (struct type_4 *)(1096UL), var_69.field_0, var_67);
        var_71 = var_70.field_0;
        var_72 = var_70.field_1;
        var_73 = var_0 + (-360L);
        *(uint64_t *)var_73 = var_71;
        *(uint64_t *)(var_0 + (-352L)) = var_72;
        var_74 = var_0 + (-480L);
        var_75 = (uint64_t *)var_74;
        *var_75 = 4211493UL;
        var_76 = indirect_placeholder_26(var_29, var_59, var_57, var_73, var_33, var_68);
        var_77 = (unsigned char *)(var_0 + (-417L));
        var_78 = (unsigned char)var_76.field_0;
        *var_77 = var_78;
        cc_src2_4 = cc_src2_10;
        local_sp_8 = var_74;
        if ((uint64_t)var_78 != 0UL) {
            var_79 = *(uint64_t *)(var_0 + (-456L));
            var_80 = *var_12;
            var_81 = var_0 + (-320L);
            var_82 = var_0 + (-488L);
            var_83 = (uint64_t *)var_82;
            *var_83 = 4211540UL;
            indirect_placeholder_25(var_81, var_80, var_79);
            var_84 = var_80 >> 1UL;
            var_85 = helper_movq_mm_T0_xmm_wrapper_101((struct type_4 *)(968UL), (var_80 << 63UL) | (var_79 >> 1UL));
            var_86 = var_85.field_0;
            var_87 = helper_movq_mm_T0_xmm_wrapper_102((struct type_4 *)(1160UL), var_84);
            var_88 = helper_punpcklqdq_xmm_wrapper_103((struct type_8 *)(0UL), (struct type_4 *)(968UL), (struct type_4 *)(1160UL), var_86, var_87.field_0);
            var_89 = var_88.field_1;
            *var_83 = var_88.field_0;
            *var_75 = var_89;
            local_sp_2 = var_82;
            r14_2 = var_80;
            loop_state_var = 2U;
            while (1U)
                {
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 0U:
                        {
                            var_118 = local_sp_7 + 96UL;
                            var_119 = local_sp_7 + 112UL;
                            var_120 = local_sp_7 + 128UL;
                            var_121 = local_sp_7 + 80UL;
                            var_122 = local_sp_7 + 152UL;
                            var_123 = local_sp_7 + (-8L);
                            *(uint64_t *)var_123 = 4211812UL;
                            var_124 = indirect_placeholder_23(var_119, var_120, var_118, var_122, var_121, rbp_5);
                            var_125 = var_124.field_0;
                            *(uint64_t *)(local_sp_7 + 136UL) = var_125;
                            var_126 = (var_125 == *(uint64_t *)(local_sp_7 + 88UL));
                            var_127 = (uint64_t)*(unsigned char *)(local_sp_7 + 402UL);
                            local_sp_1 = var_123;
                            local_sp_0 = var_123;
                            rbx_1 = r13_5;
                            local_sp_5 = var_123;
                            cc_src2_0 = cc_src2_7;
                            r14_0 = r14_5;
                            rbx_0 = r13_5;
                            r13_0 = rbp_5;
                            cc_src2_1 = cc_src2_7;
                            r14_1 = r14_5;
                            r13_1 = rbp_5;
                            rbp_3 = rbp_5;
                            cc_src2_5 = cc_src2_7;
                            r14_3 = r14_5;
                            rbx_3 = rbx_5;
                            r13_3 = r13_5;
                            local_sp_8 = var_123;
                            if (!var_126) {
                                if ((uint64_t)(uint32_t)r12_5 < var_127) {
                                    switch_state_var = 1;
                                    break;
                                }
                                r12_3 = r12_5 + 1UL;
                                loop_state_var = 2U;
                                continue;
                            }
                            var_128 = *(uint64_t *)(local_sp_7 + 144UL);
                            var_129 = *(uint64_t *)var_118;
                            if ((uint64_t)(uint32_t)r12_5 >= var_127) {
                                var_130 = *(uint64_t *)(local_sp_7 + 40UL);
                                var_131 = *(uint64_t *)(local_sp_7 + 32UL);
                                var_132 = (var_128 != var_129);
                                rbp_0 = var_131;
                                r12_0 = var_130;
                                storemerge = var_132;
                                local_sp_1 = local_sp_0;
                                rbx_1 = rbx_0;
                                rbp_1 = rbp_0;
                                cc_src2_1 = cc_src2_0;
                                r14_1 = r14_0;
                                r12_1 = r12_0;
                                r13_1 = r13_0;
                                local_sp_8 = local_sp_0;
                                if ((uint64_t)(unsigned char)storemerge == 0UL) {
                                    switch_state_var = 1;
                                    break;
                                }
                            }
                            if (var_128 != var_129) {
                                r12_3 = r12_5 + 1UL;
                                loop_state_var = 2U;
                                continue;
                            }
                            var_133 = *(uint64_t *)(local_sp_7 + 40UL);
                            var_134 = *(uint64_t *)(local_sp_7 + 32UL);
                            rbp_1 = var_134;
                            r12_1 = var_133;
                        }
                        break;
                      case 1U:
                        {
                            rbx_1 = rbx_2;
                            rbp_0 = rbp_2;
                            rbp_1 = rbp_2;
                            cc_src2_0 = cc_src2_4;
                            r14_0 = r14_2;
                            r12_0 = r12_2;
                            rbx_0 = rbx_2;
                            r13_0 = r13_2;
                            cc_src2_1 = cc_src2_4;
                            r14_1 = r14_2;
                            r12_1 = r12_2;
                            r13_1 = r13_2;
                            local_sp_3 = local_sp_2;
                            rbp_3 = r13_2;
                            cc_src2_5 = cc_src2_4;
                            r13_3 = rbx_2;
                            if (*(uint64_t *)(local_sp_2 + 168UL) == 0UL) {
                                var_90 = *(uint64_t *)(local_sp_2 + 160UL);
                                var_91 = local_sp_2 + 96UL;
                                var_92 = local_sp_2 + 80UL;
                                var_93 = local_sp_2 + 152UL;
                                var_94 = (uint64_t *)(local_sp_2 + 136UL);
                                *var_94 = 0UL;
                                var_95 = (uint64_t)*(unsigned char *)(((var_90 >> 1UL) & 127UL) + 4269280UL);
                                var_96 = (var_95 << 1UL) - (var_90 * (var_95 * var_95));
                                var_97 = (var_96 << 1UL) - (var_90 * (var_96 * var_96));
                                var_98 = local_sp_2 + 112UL;
                                var_99 = ((var_97 << 1UL) - (var_90 * (var_97 * var_97))) * *(uint64_t *)(local_sp_2 + 24UL);
                                var_100 = local_sp_2 + 128UL;
                                *(uint64_t *)var_100 = var_99;
                                var_101 = local_sp_2 + (-8L);
                                *(uint64_t *)var_101 = 4212361UL;
                                var_102 = indirect_placeholder_24(var_98, var_100, var_91, var_93, var_92, r13_2);
                                var_103 = var_102.field_0;
                                *var_94 = var_103;
                                local_sp_0 = var_101;
                                local_sp_1 = var_101;
                                local_sp_3 = var_101;
                                local_sp_4 = var_101;
                                if (var_103 != *(uint64_t *)(local_sp_2 + 88UL)) {
                                    local_sp_4 = local_sp_3;
                                    local_sp_8 = local_sp_3;
                                    if (*(unsigned char *)(local_sp_3 + 410UL) == '\x00') {
                                        switch_state_var = 1;
                                        break;
                                    }
                                }
                                var_104 = *(unsigned char *)(local_sp_2 + 402UL);
                                var_105 = *(uint64_t *)(local_sp_2 + 144UL);
                                var_106 = *(uint64_t *)var_91;
                                if (var_104 != '\x00') {
                                    var_107 = (var_106 & (-256L)) | (var_105 != var_106);
                                    storemerge = var_107;
                                    local_sp_1 = local_sp_0;
                                    rbx_1 = rbx_0;
                                    rbp_1 = rbp_0;
                                    cc_src2_1 = cc_src2_0;
                                    r14_1 = r14_0;
                                    r12_1 = r12_0;
                                    r13_1 = r13_0;
                                    local_sp_8 = local_sp_0;
                                    if ((uint64_t)(unsigned char)storemerge == 0UL) {
                                        switch_state_var = 1;
                                        break;
                                    }
                                    var_135 = rbp_1 + (uint64_t)*(unsigned char *)r12_1;
                                    var_136 = (var_135 < rbx_1);
                                    var_137 = var_136 ? 0UL : var_135;
                                    var_138 = helper_cc_compute_c_wrapper(var_135 - rbx_1, rbx_1, cc_src2_1, 17U);
                                    rbp_2 = var_135;
                                    r14_2 = r14_1;
                                    rbx_2 = rbx_1;
                                    r13_2 = r13_1;
                                    rax_0 = var_136 ? var_135 : 0UL;
                                    rcx_0 = ((0UL - var_138) & (-64L)) + 127UL;
                                    cc_src2_2 = var_138;
                                    rdx_0 = var_137;
                                    rbp_4 = var_135;
                                    r14_4 = r14_1;
                                    rbx_4 = rbx_1;
                                    r13_4 = r13_1;
                                    while (1U)
                                        {
                                            var_139 = rax_0 << 1UL;
                                            var_140 = rdx_0 << 1UL;
                                            var_141 = var_139 | (rdx_0 >> 63UL);
                                            var_142 = rbx_1 - var_141;
                                            var_143 = helper_cc_compute_c_wrapper(var_142, var_141, cc_src2_2, 17U);
                                            rax_1 = var_141;
                                            cc_src2_3 = cc_src2_2;
                                            rdx_1 = var_140;
                                            if (var_143 == 0UL) {
                                                var_145 = var_140 - rsi;
                                                var_146 = helper_cc_compute_c_wrapper(var_145, rsi, cc_src2_2, 17U);
                                                rax_1 = (var_141 - rbx_1) - var_146;
                                                cc_src2_3 = var_146;
                                                rdx_1 = var_145;
                                            } else {
                                                var_144 = helper_cc_compute_all_wrapper(var_142, var_141, cc_src2_2, 17U);
                                                if (((var_144 & 64UL) == 0UL) || (var_140 < rsi)) {
                                                    var_145 = var_140 - rsi;
                                                    var_146 = helper_cc_compute_c_wrapper(var_145, rsi, cc_src2_2, 17U);
                                                    rax_1 = (var_141 - rbx_1) - var_146;
                                                    cc_src2_3 = var_146;
                                                    rdx_1 = var_145;
                                                }
                                            }
                                            rax_0 = rax_1;
                                            cc_src2_2 = cc_src2_3;
                                            rdx_0 = rdx_1;
                                            cc_src2_4 = cc_src2_3;
                                            cc_src2_6 = cc_src2_3;
                                            if (rcx_0 == 0UL) {
                                                break;
                                            }
                                            rcx_0 = rcx_0 + (-1L);
                                            continue;
                                        }
                                    var_147 = helper_movq_mm_T0_xmm_wrapper((struct type_4 *)(776UL), rdx_1);
                                    var_148 = var_147.field_0;
                                    var_149 = helper_movq_mm_T0_xmm_wrapper_100((struct type_4 *)(840UL), rax_1);
                                    var_150 = (uint64_t)*(uint32_t *)(local_sp_1 + 20UL);
                                    var_151 = local_sp_1 + 96UL;
                                    var_152 = helper_punpcklqdq_xmm_wrapper((struct type_8 *)(0UL), (struct type_4 *)(776UL), (struct type_4 *)(840UL), var_148, var_149.field_0);
                                    var_153 = var_152.field_0;
                                    var_154 = var_152.field_1;
                                    var_155 = local_sp_1 + 64UL;
                                    var_156 = local_sp_1 + 80UL;
                                    var_157 = local_sp_1 + 112UL;
                                    *(uint64_t *)var_156 = var_153;
                                    *(uint64_t *)(local_sp_1 + 88UL) = var_154;
                                    var_158 = local_sp_1 + (-8L);
                                    *(uint64_t *)var_158 = 4212120UL;
                                    var_159 = indirect_placeholder_22(var_155, var_156, var_151, var_157, r13_1, var_150);
                                    var_160 = var_159.field_1;
                                    var_161 = var_159.field_2;
                                    var_162 = var_159.field_3;
                                    local_sp_2 = var_158;
                                    local_sp_8 = var_158;
                                    if ((uint64_t)(unsigned char)var_159.field_0 != 0UL) {
                                        *(unsigned char *)(local_sp_1 + 55UL) = (unsigned char)'\x00';
                                        switch_state_var = 1;
                                        break;
                                    }
                                    var_163 = r12_1 + 1UL;
                                    r12_2 = var_163;
                                    r12_4 = var_163;
                                    if (r12_1 != 4281595UL) {
                                        loop_state_var = 1U;
                                        continue;
                                    }
                                    *(uint64_t *)(local_sp_1 + (-16L)) = 4212158UL;
                                    indirect_placeholder_13(0UL, var_160, 4268480UL, var_161, 0UL, 0UL, var_162);
                                    var_164 = local_sp_1 + (-24L);
                                    *(uint64_t *)var_164 = 4212163UL;
                                    indirect_placeholder_1();
                                    local_sp_6 = var_164;
                                    *(uint64_t *)(local_sp_6 + 128UL) = rsi2_0;
                                    *(uint64_t *)(local_sp_6 + 136UL) = 0UL;
                                    rbx_5 = rbx_4;
                                    rbp_5 = rbp_4;
                                    r14_5 = r14_4;
                                    r12_5 = r12_4;
                                    cc_src2_7 = cc_src2_6;
                                    r13_5 = r13_4;
                                    local_sp_7 = local_sp_6;
                                    loop_state_var = 0U;
                                    continue;
                                }
                                if (var_105 != var_106) {
                                    var_135 = rbp_1 + (uint64_t)*(unsigned char *)r12_1;
                                    var_136 = (var_135 < rbx_1);
                                    var_137 = var_136 ? 0UL : var_135;
                                    var_138 = helper_cc_compute_c_wrapper(var_135 - rbx_1, rbx_1, cc_src2_1, 17U);
                                    rbp_2 = var_135;
                                    r14_2 = r14_1;
                                    rbx_2 = rbx_1;
                                    r13_2 = r13_1;
                                    rax_0 = var_136 ? var_135 : 0UL;
                                    rcx_0 = ((0UL - var_138) & (-64L)) + 127UL;
                                    cc_src2_2 = var_138;
                                    rdx_0 = var_137;
                                    rbp_4 = var_135;
                                    r14_4 = r14_1;
                                    rbx_4 = rbx_1;
                                    r13_4 = r13_1;
                                    while (1U)
                                        {
                                            var_139 = rax_0 << 1UL;
                                            var_140 = rdx_0 << 1UL;
                                            var_141 = var_139 | (rdx_0 >> 63UL);
                                            var_142 = rbx_1 - var_141;
                                            var_143 = helper_cc_compute_c_wrapper(var_142, var_141, cc_src2_2, 17U);
                                            rax_1 = var_141;
                                            cc_src2_3 = cc_src2_2;
                                            rdx_1 = var_140;
                                            if (var_143 == 0UL) {
                                                var_145 = var_140 - rsi;
                                                var_146 = helper_cc_compute_c_wrapper(var_145, rsi, cc_src2_2, 17U);
                                                rax_1 = (var_141 - rbx_1) - var_146;
                                                cc_src2_3 = var_146;
                                                rdx_1 = var_145;
                                            } else {
                                                var_144 = helper_cc_compute_all_wrapper(var_142, var_141, cc_src2_2, 17U);
                                                if (((var_144 & 64UL) == 0UL) || (var_140 < rsi)) {
                                                    var_145 = var_140 - rsi;
                                                    var_146 = helper_cc_compute_c_wrapper(var_145, rsi, cc_src2_2, 17U);
                                                    rax_1 = (var_141 - rbx_1) - var_146;
                                                    cc_src2_3 = var_146;
                                                    rdx_1 = var_145;
                                                }
                                            }
                                            rax_0 = rax_1;
                                            cc_src2_2 = cc_src2_3;
                                            rdx_0 = rdx_1;
                                            cc_src2_4 = cc_src2_3;
                                            cc_src2_6 = cc_src2_3;
                                            if (rcx_0 == 0UL) {
                                                break;
                                            }
                                            rcx_0 = rcx_0 + (-1L);
                                            continue;
                                        }
                                    var_147 = helper_movq_mm_T0_xmm_wrapper((struct type_4 *)(776UL), rdx_1);
                                    var_148 = var_147.field_0;
                                    var_149 = helper_movq_mm_T0_xmm_wrapper_100((struct type_4 *)(840UL), rax_1);
                                    var_150 = (uint64_t)*(uint32_t *)(local_sp_1 + 20UL);
                                    var_151 = local_sp_1 + 96UL;
                                    var_152 = helper_punpcklqdq_xmm_wrapper((struct type_8 *)(0UL), (struct type_4 *)(776UL), (struct type_4 *)(840UL), var_148, var_149.field_0);
                                    var_153 = var_152.field_0;
                                    var_154 = var_152.field_1;
                                    var_155 = local_sp_1 + 64UL;
                                    var_156 = local_sp_1 + 80UL;
                                    var_157 = local_sp_1 + 112UL;
                                    *(uint64_t *)var_156 = var_153;
                                    *(uint64_t *)(local_sp_1 + 88UL) = var_154;
                                    var_158 = local_sp_1 + (-8L);
                                    *(uint64_t *)var_158 = 4212120UL;
                                    var_159 = indirect_placeholder_22(var_155, var_156, var_151, var_157, r13_1, var_150);
                                    var_160 = var_159.field_1;
                                    var_161 = var_159.field_2;
                                    var_162 = var_159.field_3;
                                    local_sp_2 = var_158;
                                    local_sp_8 = var_158;
                                    if ((uint64_t)(unsigned char)var_159.field_0 != 0UL) {
                                        *(unsigned char *)(local_sp_1 + 55UL) = (unsigned char)'\x00';
                                        switch_state_var = 1;
                                        break;
                                    }
                                    var_163 = r12_1 + 1UL;
                                    r12_2 = var_163;
                                    r12_4 = var_163;
                                    if (r12_1 != 4281595UL) {
                                        loop_state_var = 1U;
                                        continue;
                                    }
                                    *(uint64_t *)(local_sp_1 + (-16L)) = 4212158UL;
                                    indirect_placeholder_13(0UL, var_160, 4268480UL, var_161, 0UL, 0UL, var_162);
                                    var_164 = local_sp_1 + (-24L);
                                    *(uint64_t *)var_164 = 4212163UL;
                                    indirect_placeholder_1();
                                    local_sp_6 = var_164;
                                    *(uint64_t *)(local_sp_6 + 128UL) = rsi2_0;
                                    *(uint64_t *)(local_sp_6 + 136UL) = 0UL;
                                    rbx_5 = rbx_4;
                                    rbp_5 = rbp_4;
                                    r14_5 = r14_4;
                                    r12_5 = r12_4;
                                    cc_src2_7 = cc_src2_6;
                                    r13_5 = r13_4;
                                    local_sp_7 = local_sp_6;
                                    loop_state_var = 0U;
                                    continue;
                                }
                            }
                            local_sp_4 = local_sp_3;
                            local_sp_8 = local_sp_3;
                            if (*(unsigned char *)(local_sp_3 + 410UL) == '\x00') {
                                switch_state_var = 1;
                                break;
                            }
                            *(uint64_t *)(local_sp_4 + 40UL) = rbp_2;
                            *(uint64_t *)(local_sp_4 + 48UL) = r12_2;
                            var_108 = *(uint64_t *)(local_sp_4 + 24UL);
                            var_109 = *(uint64_t *)(local_sp_4 + 32UL);
                            local_sp_5 = local_sp_4;
                            r14_3 = var_109;
                            rbx_3 = var_108;
                            var_110 = *(uint64_t *)(((r12_3 << 3UL) + local_sp_5) + 168UL);
                            local_sp_6 = local_sp_5;
                            rbx_5 = rbx_3;
                            rbp_5 = rbp_3;
                            r14_5 = r14_3;
                            r12_5 = r12_3;
                            cc_src2_7 = cc_src2_5;
                            r13_5 = r13_3;
                            local_sp_7 = local_sp_5;
                            rbp_4 = rbp_3;
                            cc_src2_6 = cc_src2_5;
                            r14_4 = r14_3;
                            r12_4 = r12_3;
                            rbx_4 = rbx_3;
                            r13_4 = r13_3;
                            if (var_110 == 2UL) {
                                var_117 = *(uint64_t *)(local_sp_5 + 8UL);
                                *(uint64_t *)(local_sp_5 + 128UL) = *(uint64_t *)local_sp_5;
                                *(uint64_t *)(local_sp_5 + 136UL) = var_117;
                            } else {
                                var_111 = (uint64_t)*(unsigned char *)(((var_110 >> 1UL) & 127UL) + 4269280UL);
                                var_112 = (var_111 << 1UL) - (var_110 * (var_111 * var_111));
                                var_113 = (var_112 << 1UL) - (var_110 * (var_112 * var_112));
                                var_114 = (var_113 << 1UL) - (var_110 * (var_113 * var_113));
                                var_115 = var_114 * rbx_3;
                                var_116 = helper_cc_compute_c_wrapper(r14_3 - var_110, var_110, cc_src2_5, 17U);
                                rsi2_0 = var_115;
                                if (var_116 != 0UL) {
                                    *(uint64_t *)(local_sp_6 + 128UL) = rsi2_0;
                                    *(uint64_t *)(local_sp_6 + 136UL) = 0UL;
                                    rbx_5 = rbx_4;
                                    rbp_5 = rbp_4;
                                    r14_5 = r14_4;
                                    r12_5 = r12_4;
                                    cc_src2_7 = cc_src2_6;
                                    r13_5 = r13_4;
                                    local_sp_7 = local_sp_6;
                                    loop_state_var = 0U;
                                    continue;
                                }
                                *(uint64_t *)(local_sp_5 + 128UL) = var_115;
                                *(uint64_t *)(local_sp_5 + 136UL) = ((r14_3 - (uint64_t)(((unsigned __int128)var_110 * (unsigned __int128)var_115) >> 64ULL)) * var_114);
                            }
                        }
                        break;
                      case 2U:
                        {
                            var_110 = *(uint64_t *)(((r12_3 << 3UL) + local_sp_5) + 168UL);
                            local_sp_6 = local_sp_5;
                            rbx_5 = rbx_3;
                            rbp_5 = rbp_3;
                            r14_5 = r14_3;
                            r12_5 = r12_3;
                            cc_src2_7 = cc_src2_5;
                            r13_5 = r13_3;
                            local_sp_7 = local_sp_5;
                            rbp_4 = rbp_3;
                            cc_src2_6 = cc_src2_5;
                            r14_4 = r14_3;
                            r12_4 = r12_3;
                            rbx_4 = rbx_3;
                            r13_4 = r13_3;
                            if (var_110 == 2UL) {
                                var_117 = *(uint64_t *)(local_sp_5 + 8UL);
                                *(uint64_t *)(local_sp_5 + 128UL) = *(uint64_t *)local_sp_5;
                                *(uint64_t *)(local_sp_5 + 136UL) = var_117;
                            } else {
                                var_111 = (uint64_t)*(unsigned char *)(((var_110 >> 1UL) & 127UL) + 4269280UL);
                                var_112 = (var_111 << 1UL) - (var_110 * (var_111 * var_111));
                                var_113 = (var_112 << 1UL) - (var_110 * (var_112 * var_112));
                                var_114 = (var_113 << 1UL) - (var_110 * (var_113 * var_113));
                                var_115 = var_114 * rbx_3;
                                var_116 = helper_cc_compute_c_wrapper(r14_3 - var_110, var_110, cc_src2_5, 17U);
                                rsi2_0 = var_115;
                                if (var_116 != 0UL) {
                                    *(uint64_t *)(local_sp_6 + 128UL) = rsi2_0;
                                    *(uint64_t *)(local_sp_6 + 136UL) = 0UL;
                                    rbx_5 = rbx_4;
                                    rbp_5 = rbp_4;
                                    r14_5 = r14_4;
                                    r12_5 = r12_4;
                                    cc_src2_7 = cc_src2_6;
                                    r13_5 = r13_4;
                                    local_sp_7 = local_sp_6;
                                    loop_state_var = 0U;
                                    continue;
                                }
                                *(uint64_t *)(local_sp_5 + 128UL) = var_115;
                                *(uint64_t *)(local_sp_5 + 136UL) = ((r14_3 - (uint64_t)(((unsigned __int128)var_110 * (unsigned __int128)var_115) >> 64ULL)) * var_114);
                            }
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
        }
    }
    return (uint64_t)*(unsigned char *)(local_sp_8 + 63UL);
}
