typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_powm_ret_type;
struct indirect_placeholder_14_ret_type;
struct indirect_placeholder_15_ret_type;
struct bb_powm_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_14_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_15_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern struct indirect_placeholder_14_ret_type indirect_placeholder_14(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_15_ret_type indirect_placeholder_15(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
typedef _Bool bool;
struct bb_powm_ret_type bb_powm(uint64_t rcx, uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t r8) {
    uint64_t rcx6_1;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t r10_1_ph;
    uint64_t var_9;
    uint64_t r9_1_ph;
    struct indirect_placeholder_14_ret_type var_10;
    uint64_t rcx6_1_ph;
    uint64_t rcx6_0;
    uint64_t var_11;
    uint64_t r10_0;
    uint64_t r9_0;
    uint64_t rdi8_0;
    struct bb_powm_ret_type mrv;
    struct bb_powm_ret_type mrv1;
    struct bb_powm_ret_type mrv2;
    struct bb_powm_ret_type mrv3;
    struct bb_powm_ret_type mrv4;
    struct bb_powm_ret_type mrv5;
    uint64_t r10_1_ph21;
    uint64_t r11_0_ph;
    uint64_t local_sp_0_ph;
    uint64_t r9_1;
    uint64_t r11_0;
    uint64_t local_sp_0;
    uint64_t var_4;
    struct indirect_placeholder_15_ret_type var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = rsi & 1UL;
    var_3 = var_0 + (-8L);
    *(uint64_t *)var_3 = var_1;
    r10_1_ph = rdi;
    r9_1_ph = rdi;
    rcx6_1_ph = rcx;
    rcx6_0 = rcx;
    r10_0 = r8;
    r9_0 = rdi;
    rdi8_0 = rdi;
    r11_0_ph = rsi;
    local_sp_0_ph = var_3;
    if (var_2 != 0UL) {
        r10_1_ph = r8;
        if (rsi == 0UL) {
            mrv.field_0 = r10_0;
            mrv1 = mrv;
            mrv1.field_1 = rcx6_0;
            mrv2 = mrv1;
            mrv2.field_2 = r10_0;
            mrv3 = mrv2;
            mrv3.field_3 = r9_0;
            mrv4 = mrv3;
            mrv4.field_4 = rdi8_0;
            mrv5 = mrv4;
            mrv5.field_5 = r8;
            return mrv5;
        }
    }
    r10_1_ph21 = r10_1_ph;
    while (1U)
        {
            rcx6_1 = rcx6_1_ph;
            r10_0 = r10_1_ph21;
            r9_1 = r9_1_ph;
            r11_0 = r11_0_ph;
            local_sp_0 = local_sp_0_ph;
            while (1U)
                {
                    var_4 = local_sp_0 + (-8L);
                    *(uint64_t *)var_4 = 4204750UL;
                    var_5 = indirect_placeholder_15(rcx6_1, rdx, r9_1, r9_1);
                    var_6 = var_5.field_0;
                    var_7 = var_5.field_1;
                    var_8 = r11_0 >> 1UL;
                    rcx6_1 = var_7;
                    r9_1_ph = var_6;
                    rcx6_0 = var_7;
                    r9_0 = var_6;
                    r11_0_ph = var_8;
                    r9_1 = var_6;
                    r11_0 = var_8;
                    local_sp_0 = var_4;
                    if ((r11_0 & 2UL) != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    if (var_8 == 0UL) {
                        continue;
                    }
                    var_11 = var_5.field_2;
                    rdi8_0 = var_11;
                    loop_state_var = 1U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 0U:
                {
                    var_9 = local_sp_0 + (-16L);
                    *(uint64_t *)var_9 = 4204776UL;
                    var_10 = indirect_placeholder_14(var_7, rdx, r10_1_ph21, var_6);
                    rcx6_1_ph = var_10.field_1;
                    r10_1_ph21 = var_10.field_0;
                    local_sp_0_ph = var_9;
                    continue;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    mrv.field_0 = r10_0;
    mrv1 = mrv;
    mrv1.field_1 = rcx6_0;
    mrv2 = mrv1;
    mrv2.field_2 = r10_0;
    mrv3 = mrv2;
    mrv3.field_3 = r9_0;
    mrv4 = mrv3;
    mrv4.field_4 = rdi8_0;
    mrv5 = mrv4;
    mrv5.field_5 = r8;
    return mrv5;
}
