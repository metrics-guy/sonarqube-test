typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r12(void);
typedef _Bool bool;
uint64_t bb_strto2uintmax(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t r9_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    unsigned char var_5;
    uint64_t var_6;
    uint64_t rdx1_0;
    uint64_t rdx1_1;
    uint64_t var_7;
    unsigned char var_8;
    uint64_t rax_0;
    uint64_t var_9;
    uint64_t rbp_1;
    uint64_t rcx_0;
    uint32_t var_10;
    uint64_t rbp_2;
    uint64_t rbp_0;
    uint64_t cc_src2_0;
    uint64_t rdx1_2;
    uint64_t r12_0;
    uint32_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint32_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t r12_1;
    uint64_t r12_2;
    uint64_t r9_1;
    uint64_t var_26;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = init_r12();
    var_4 = init_rbx();
    var_5 = *(unsigned char *)rdx;
    var_6 = (uint64_t)var_5;
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    r9_0 = 4UL;
    rdx1_0 = rdx;
    rdx1_1 = rdx;
    rax_0 = var_6;
    rbp_2 = 0UL;
    rbp_0 = 0UL;
    cc_src2_0 = var_2;
    r12_0 = 0UL;
    r12_2 = 0UL;
    if ((uint64_t)(var_5 + '\xe0') != 0UL) {
        var_7 = rdx1_0 + 1UL;
        var_8 = *(unsigned char *)var_7;
        rdx1_0 = var_7;
        rdx1_1 = var_7;
        do {
            var_7 = rdx1_0 + 1UL;
            var_8 = *(unsigned char *)var_7;
            rdx1_0 = var_7;
            rdx1_1 = var_7;
        } while (var_8 != ' ');
        rax_0 = (uint64_t)var_8;
    }
    var_9 = rdx1_1 + ((uint64_t)((unsigned char)rax_0 + '\xd5') == 0UL);
    rcx_0 = var_9;
    rdx1_2 = var_9;
    while (1U)
        {
            var_10 = (uint32_t)(uint64_t)*(unsigned char *)rcx_0;
            r9_0 = 0UL;
            rcx_0 = rcx_0 + 1UL;
            r9_1 = r9_0;
            if ((uint64_t)var_10 == 0UL) {
                r9_1 = 4UL;
                if ((uint64_t)((var_10 + (-48)) & (-2)) <= 9UL) {
                    continue;
                }
                break;
            }
            if (r9_0 == 0UL) {
                break;
            }
            while (1U)
                {
                    var_11 = (uint32_t)(uint64_t)*(unsigned char *)rdx1_2;
                    var_12 = rdx1_2 + 1UL;
                    rbp_1 = rbp_0;
                    rbp_2 = rbp_0;
                    rdx1_2 = var_12;
                    r12_1 = r12_0;
                    r12_2 = r12_0;
                    r9_1 = 0UL;
                    if ((uint64_t)var_11 != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    if (rbp_0 <= 1844674407370955161UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_13 = (uint64_t)(var_11 + (-48));
                    var_14 = r12_0 << 1UL;
                    var_15 = r12_0 * 10UL;
                    var_16 = r12_0 >> 61UL;
                    var_17 = r12_0 >> 63UL;
                    var_18 = helper_cc_compute_c_wrapper(r12_0 << 3UL, var_14, cc_src2_0, 17U);
                    var_19 = ((uint32_t)var_17 + (uint32_t)var_16) + (uint32_t)var_18;
                    var_20 = var_13 + var_15;
                    var_21 = helper_cc_compute_c_wrapper(var_20, var_15, var_18, 9U);
                    var_22 = (uint64_t)(var_19 + (uint32_t)var_21);
                    var_23 = rbp_0 * 10UL;
                    var_24 = var_22 + var_23;
                    var_25 = helper_cc_compute_c_wrapper(var_24, var_23, var_21, 9U);
                    rbp_1 = var_24;
                    rbp_0 = var_24;
                    cc_src2_0 = var_21;
                    r12_0 = var_20;
                    r12_1 = var_20;
                    if (var_25 == 0UL) {
                        continue;
                    }
                    loop_state_var = 1U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    *(uint64_t *)rdi = rbp_1;
                    *(uint64_t *)rsi = r12_1;
                    return 1UL;
                }
                break;
              case 0U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    *(uint64_t *)rdi = rbp_2;
    var_26 = (uint64_t)(uint32_t)r9_1;
    *(uint64_t *)rsi = r12_2;
    return var_26;
}
