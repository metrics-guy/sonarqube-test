typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_80_ret_type;
struct indirect_placeholder_80_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_rax(void);
extern struct indirect_placeholder_80_ret_type indirect_placeholder_80(uint64_t param_0);
uint64_t bb_mem_cd_iconv(uint64_t rcx, uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t rax_0;
    uint64_t rax_1;
    uint64_t rbx_1;
    uint64_t local_sp_2;
    uint64_t *_pre_phi107;
    uint64_t local_sp_0;
    uint64_t var_41;
    uint32_t *var_42;
    uint32_t var_43;
    uint64_t r14_0;
    uint64_t var_35;
    uint64_t *var_36;
    uint64_t var_37;
    uint64_t *var_38;
    uint64_t *_pre_phi;
    uint64_t local_sp_1;
    uint64_t var_39;
    uint64_t *var_40;
    uint64_t var_34;
    uint64_t var_20;
    uint64_t *var_21;
    uint64_t var_22;
    uint64_t var_24;
    uint64_t var_23;
    uint64_t local_sp_3;
    uint64_t rbx_0;
    uint64_t _pre;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    bool var_32;
    bool var_33;
    uint64_t var_25;
    uint64_t *var_26;
    struct indirect_placeholder_80_ret_type var_27;
    uint64_t var_28;
    uint64_t rbx_2;
    uint64_t r12_0;
    uint64_t var_15;
    uint64_t var_14;
    uint64_t local_sp_4;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t local_sp_6;
    uint64_t local_sp_5;
    uint64_t var_18;
    uint64_t *var_19;
    uint64_t var_13;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbp();
    var_3 = init_cc_src2();
    var_4 = init_r15();
    var_5 = init_r14();
    var_6 = init_r12();
    var_7 = init_rbx();
    var_8 = init_r13();
    *(uint64_t *)(var_0 + (-8L)) = var_4;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    *(uint64_t *)(var_0 + (-24L)) = var_8;
    *(uint64_t *)(var_0 + (-32L)) = var_6;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    *(uint64_t *)(var_0 + (-48L)) = var_7;
    var_9 = var_0 + (-4216L);
    var_10 = (uint64_t *)(var_0 + (-4192L));
    *var_10 = rdi;
    var_11 = var_0 + (-4152L);
    *(uint64_t *)var_9 = rcx;
    *(uint64_t *)(var_0 + (-4200L)) = r8;
    var_12 = var_0 + (-4224L);
    *(uint64_t *)var_12 = 4231736UL;
    indirect_placeholder_1();
    *var_10 = rdi;
    *(uint64_t *)(var_0 + (-4184L)) = rsi;
    rax_0 = var_1;
    rax_1 = var_1;
    rbx_1 = 0UL;
    r14_0 = rsi;
    rbx_2 = 0UL;
    r12_0 = 4294967295UL;
    local_sp_6 = var_12;
    local_sp_5 = var_12;
    if (rsi != 0UL) {
        while (1U)
            {
                *(uint64_t *)(local_sp_6 + 48UL) = var_11;
                *(uint64_t *)(local_sp_6 + 56UL) = 4096UL;
                var_13 = local_sp_6 + (-8L);
                *(uint64_t *)var_13 = 4231826UL;
                indirect_placeholder_1();
                local_sp_4 = var_13;
                rax_0 = 18446744073709551615UL;
                rbx_1 = rbx_2;
                switch_state_var = 0;
                switch (rax_1) {
                  case 18446744073709551615UL:
                    {
                        var_14 = local_sp_6 + (-16L);
                        *(uint64_t *)var_14 = 4231837UL;
                        indirect_placeholder_1();
                        local_sp_4 = var_14;
                        if (*(uint32_t *)18446744073709551615UL == 7U) {
                            var_16 = *(uint64_t *)(local_sp_4 + 48UL) - var_11;
                            var_17 = rbx_2 + var_16;
                            rax_0 = var_16;
                            rax_1 = var_16;
                            rbx_1 = var_17;
                            rbx_2 = var_17;
                            local_sp_6 = local_sp_4;
                            local_sp_5 = local_sp_4;
                            if (*(uint64_t *)(local_sp_4 + 40UL) == 0UL) {
                                continue;
                            }
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        var_15 = local_sp_6 + (-24L);
                        *(uint64_t *)var_15 = 4231847UL;
                        indirect_placeholder_1();
                        local_sp_5 = var_15;
                        if (*(uint32_t *)18446744073709551615UL != 22U) {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 0UL:
                    {
                        var_16 = *(uint64_t *)(local_sp_4 + 48UL) - var_11;
                        var_17 = rbx_2 + var_16;
                        rax_0 = var_16;
                        rax_1 = var_16;
                        rbx_1 = var_17;
                        rbx_2 = var_17;
                        local_sp_6 = local_sp_4;
                        local_sp_5 = local_sp_4;
                        if (*(uint64_t *)(local_sp_4 + 40UL) == 0UL) {
                            continue;
                        }
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  default:
                    {
                        *(uint64_t *)(local_sp_6 + (-16L)) = 4232277UL;
                        indirect_placeholder_1();
                        *(uint32_t *)rax_1 = 84U;
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
        switch (loop_state_var) {
          case 0U:
            {
                return r12_0;
            }
            break;
          case 1U:
            {
                break;
            }
            break;
        }
    }
    *(uint64_t *)(local_sp_5 + 48UL) = var_11;
    *(uint64_t *)(local_sp_5 + 56UL) = 4096UL;
    var_18 = local_sp_5 + (-8L);
    var_19 = (uint64_t *)var_18;
    *var_19 = 4231888UL;
    indirect_placeholder_1();
    _pre_phi = var_19;
    local_sp_3 = var_18;
    if (rax_0 == 18446744073709551615UL) {
        return r12_0;
    }
    var_20 = (*(uint64_t *)(local_sp_5 + 40UL) - var_11) + rbx_1;
    var_21 = (uint64_t *)local_sp_5;
    *var_21 = var_20;
    var_24 = var_20;
    r12_0 = 0UL;
    if (var_20 == 0UL) {
        **(uint64_t **)(local_sp_5 + 8UL) = 0UL;
    } else {
        var_22 = **(uint64_t **)var_18;
        rbx_0 = var_22;
        r12_0 = 4294967295UL;
        if (var_22 == 0UL) {
            var_23 = helper_cc_compute_c_wrapper(**(uint64_t **)(local_sp_5 + 8UL) - var_20, var_20, var_3, 17U);
            if (var_23 != 0UL) {
                _pre = *var_21;
                var_24 = _pre;
                var_25 = local_sp_5 + (-16L);
                var_26 = (uint64_t *)var_25;
                *var_26 = 4232178UL;
                var_27 = indirect_placeholder_80(var_24);
                var_28 = var_27.field_0;
                _pre_phi = var_26;
                local_sp_3 = var_25;
                rbx_0 = var_28;
                if (var_28 != 0UL) {
                    *(uint64_t *)(local_sp_5 + (-24L)) = 4232195UL;
                    indirect_placeholder_1();
                    *(volatile uint32_t *)(uint32_t *)0UL = 12U;
                    return r12_0;
                }
            }
        }
        var_25 = local_sp_5 + (-16L);
        var_26 = (uint64_t *)var_25;
        *var_26 = 4232178UL;
        var_27 = indirect_placeholder_80(var_24);
        var_28 = var_27.field_0;
        _pre_phi = var_26;
        local_sp_3 = var_25;
        rbx_0 = var_28;
        if (var_28 != 0UL) {
            *(uint64_t *)(local_sp_5 + (-24L)) = 4232195UL;
            indirect_placeholder_1();
            *(volatile uint32_t *)(uint32_t *)0UL = 12U;
            return r12_0;
        }
        var_29 = local_sp_3 + (-8L);
        *(uint64_t *)var_29 = 4231972UL;
        indirect_placeholder_1();
        var_30 = *(uint64_t *)(local_sp_3 + 16UL);
        *(uint64_t *)(local_sp_3 + 40UL) = rsi;
        *(uint64_t *)(local_sp_3 + 48UL) = rbx_0;
        *(uint64_t *)(local_sp_3 + 32UL) = var_30;
        var_31 = *_pre_phi;
        *(uint64_t *)(local_sp_3 + 56UL) = var_31;
        var_32 = (var_31 == 18446744073709551615UL);
        var_33 = (var_31 == 0UL);
        local_sp_1 = var_29;
        while (1U)
            {
                local_sp_2 = local_sp_1;
                if (r14_0 != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                var_34 = local_sp_1 + (-8L);
                *(uint64_t *)var_34 = 4232030UL;
                indirect_placeholder_1();
                local_sp_1 = var_34;
                if (!var_32) {
                    var_37 = local_sp_1 + (-16L);
                    var_38 = (uint64_t *)var_37;
                    *var_38 = 4232299UL;
                    indirect_placeholder_1();
                    _pre_phi107 = var_38;
                    local_sp_0 = var_37;
                    local_sp_2 = var_37;
                    if (*(uint32_t *)18446744073709551615UL != 22U) {
                        loop_state_var = 1U;
                        break;
                    }
                    loop_state_var = 0U;
                    break;
                }
                if (var_33) {
                    r14_0 = *(uint64_t *)(local_sp_1 + 40UL);
                    continue;
                }
                var_35 = local_sp_1 + (-16L);
                var_36 = (uint64_t *)var_35;
                *var_36 = 4232221UL;
                indirect_placeholder_1();
                *(uint32_t *)var_31 = 84U;
                _pre_phi107 = var_36;
                local_sp_0 = var_35;
                loop_state_var = 1U;
                break;
            }
        switch (loop_state_var) {
          case 1U:
            {
                var_41 = *_pre_phi107;
                if (*(uint64_t *)var_41 == rbx_0) {
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4232247UL;
                    indirect_placeholder_1();
                    var_42 = (uint32_t *)var_41;
                    var_43 = *var_42;
                    *(uint64_t *)(local_sp_0 + (-16L)) = 4232257UL;
                    indirect_placeholder_1();
                    *(uint64_t *)(local_sp_0 + (-24L)) = 4232262UL;
                    indirect_placeholder_1();
                    *var_42 = var_43;
                }
            }
            break;
          case 0U:
            {
                var_39 = local_sp_2 + (-8L);
                var_40 = (uint64_t *)var_39;
                *var_40 = 4232077UL;
                indirect_placeholder_1();
                _pre_phi107 = var_40;
                local_sp_0 = var_39;
                r12_0 = 0UL;
                if (!var_32) {
                    if (*(uint64_t *)(local_sp_2 + 56UL) == 0UL) {
                        *(uint64_t *)(local_sp_2 + (-16L)) = 4203668UL;
                        indirect_placeholder_1();
                        abort();
                    }
                    **(uint64_t **)var_39 = rbx_0;
                    **(uint64_t **)(local_sp_2 + 8UL) = *(uint64_t *)local_sp_2;
                    return r12_0;
                }
            }
            break;
        }
    }
}
