typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder_1(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
void bb_version_etc_va(uint64_t rcx, uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint32_t *_cast;
    uint32_t var_2;
    uint64_t var_3;
    uint64_t r9_0;
    uint64_t *var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t *_pre_phi93;
    uint64_t var_4;
    uint32_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint32_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint32_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint32_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint32_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t *var_21;
    uint64_t var_22;
    uint64_t *_pre_phi103;
    uint64_t *var_23;
    uint64_t var_24;
    uint64_t *_pre_phi101;
    uint64_t *var_25;
    uint64_t var_26;
    uint64_t *_pre_phi99;
    uint64_t *var_27;
    uint64_t var_28;
    uint64_t *_pre_phi97;
    uint64_t *var_29;
    uint64_t var_30;
    uint64_t *_pre_phi95;
    uint64_t *var_31;
    uint64_t rax_0;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t rax_1;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t rax_2;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t rax_3;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t rax_4;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t rax_5;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    var_0 = revng_init_local_sp(0UL);
    var_1 = var_0 + (-88L);
    _cast = (uint32_t *)r8;
    var_2 = *_cast;
    var_3 = (uint64_t)var_2;
    r9_0 = 0UL;
    if (var_2 > 47U) {
        var_32 = (uint64_t *)(r8 + 8UL);
        var_33 = *var_32;
        var_34 = var_33 + 8UL;
        *var_32 = var_34;
        var_35 = *(uint64_t *)var_33;
        *(uint64_t *)var_1 = var_35;
        _pre_phi93 = var_32;
        rax_0 = var_34;
        if (var_35 == 0UL) {
            *(uint64_t *)(var_0 + (-96L)) = 4219535UL;
            indirect_placeholder_1(rcx, rdx, r9_0, rdi, rsi, var_1);
            return;
        }
    }
    var_4 = *(uint64_t *)(r8 + 16UL);
    var_5 = var_2 + 8U;
    var_6 = (uint64_t)var_5;
    *_cast = var_5;
    var_7 = *(uint64_t *)(var_4 + var_3);
    *(uint64_t *)var_1 = var_7;
    if (var_7 == 0UL) {
        *(uint64_t *)(var_0 + (-96L)) = 4219535UL;
        indirect_placeholder_1(rcx, rdx, r9_0, rdi, rsi, var_1);
        return;
    }
    r9_0 = 1UL;
    if ((uint64_t)(var_5 & (-16)) > 47UL) {
        var_31 = (uint64_t *)(r8 + 8UL);
        _pre_phi93 = var_31;
        rax_0 = *var_31;
        var_36 = rax_0 + 8UL;
        *_pre_phi93 = var_36;
        var_37 = *(uint64_t *)rax_0;
        *(uint64_t *)(var_0 + (-80L)) = var_37;
        _pre_phi95 = _pre_phi93;
        rax_1 = var_36;
        r9_0 = 1UL;
        if (var_37 == 0UL) {
            *(uint64_t *)(var_0 + (-96L)) = 4219535UL;
            indirect_placeholder_1(rcx, rdx, r9_0, rdi, rsi, var_1);
            return;
        }
        var_38 = rax_1 + 8UL;
        *_pre_phi95 = var_38;
        var_39 = *(uint64_t *)rax_1;
        *(uint64_t *)(var_0 + (-72L)) = var_39;
        _pre_phi97 = _pre_phi95;
        rax_2 = var_38;
        r9_0 = 2UL;
        if (var_39 == 0UL) {
            *(uint64_t *)(var_0 + (-96L)) = 4219535UL;
            indirect_placeholder_1(rcx, rdx, r9_0, rdi, rsi, var_1);
            return;
        }
        var_40 = rax_2 + 8UL;
        *_pre_phi97 = var_40;
        var_41 = *(uint64_t *)rax_2;
        *(uint64_t *)(var_0 + (-64L)) = var_41;
        _pre_phi99 = _pre_phi97;
        rax_3 = var_40;
        r9_0 = 3UL;
        if (var_41 == 0UL) {
            *(uint64_t *)(var_0 + (-96L)) = 4219535UL;
            indirect_placeholder_1(rcx, rdx, r9_0, rdi, rsi, var_1);
            return;
        }
        var_42 = rax_3 + 8UL;
        *_pre_phi99 = var_42;
        var_43 = *(uint64_t *)rax_3;
        *(uint64_t *)(var_0 + (-56L)) = var_43;
        _pre_phi101 = _pre_phi99;
        rax_4 = var_42;
        r9_0 = 4UL;
        if (var_43 == 0UL) {
            *(uint64_t *)(var_0 + (-96L)) = 4219535UL;
            indirect_placeholder_1(rcx, rdx, r9_0, rdi, rsi, var_1);
            return;
        }
        var_44 = rax_4 + 8UL;
        *_pre_phi101 = var_44;
        var_45 = *(uint64_t *)rax_4;
        *(uint64_t *)(var_0 + (-48L)) = var_45;
        _pre_phi103 = _pre_phi101;
        rax_5 = var_44;
        r9_0 = 5UL;
        if (var_45 == 0UL) {
            *(uint64_t *)(var_0 + (-96L)) = 4219535UL;
            indirect_placeholder_1(rcx, rdx, r9_0, rdi, rsi, var_1);
            return;
        }
        var_46 = rax_5 + 8UL;
        *_pre_phi103 = var_46;
        var_47 = *(uint64_t *)rax_5;
        *(uint64_t *)(var_0 + (-40L)) = var_47;
        r9_0 = 6UL;
        var_48 = rax_5 + 16UL;
        *_pre_phi103 = var_48;
        var_49 = *(uint64_t *)var_46;
        *(uint64_t *)(var_0 + (-32L)) = var_49;
        r9_0 = 7UL;
        var_50 = rax_5 + 24UL;
        *_pre_phi103 = var_50;
        var_51 = *(uint64_t *)var_48;
        *(uint64_t *)(var_0 + (-24L)) = var_51;
        r9_0 = 8UL;
        if (var_47 != 0UL & var_49 != 0UL & var_51 == 0UL) {
            *_pre_phi103 = (rax_5 + 32UL);
            var_52 = *(uint64_t *)var_50;
            *(uint64_t *)(var_0 + (-16L)) = var_52;
            var_53 = (var_52 == 0UL) ? 9UL : 10UL;
            r9_0 = var_53;
        }
        *(uint64_t *)(var_0 + (-96L)) = 4219535UL;
        indirect_placeholder_1(rcx, rdx, r9_0, rdi, rsi, var_1);
        return;
    }
    var_8 = var_2 + 16U;
    var_9 = (uint64_t)var_8;
    *_cast = var_8;
    var_10 = *(uint64_t *)(var_6 + var_4);
    *(uint64_t *)(var_0 + (-80L)) = var_10;
    if (var_10 == 0UL) {
        *(uint64_t *)(var_0 + (-96L)) = 4219535UL;
        indirect_placeholder_1(rcx, rdx, r9_0, rdi, rsi, var_1);
        return;
    }
    r9_0 = 2UL;
    if ((uint64_t)(var_8 & (-16)) > 47UL) {
        var_29 = (uint64_t *)(r8 + 8UL);
        var_30 = *var_29;
        _pre_phi95 = var_29;
        rax_1 = var_30;
        var_38 = rax_1 + 8UL;
        *_pre_phi95 = var_38;
        var_39 = *(uint64_t *)rax_1;
        *(uint64_t *)(var_0 + (-72L)) = var_39;
        _pre_phi97 = _pre_phi95;
        rax_2 = var_38;
        r9_0 = 2UL;
        if (var_39 == 0UL) {
            *(uint64_t *)(var_0 + (-96L)) = 4219535UL;
            indirect_placeholder_1(rcx, rdx, r9_0, rdi, rsi, var_1);
            return;
        }
        var_40 = rax_2 + 8UL;
        *_pre_phi97 = var_40;
        var_41 = *(uint64_t *)rax_2;
        *(uint64_t *)(var_0 + (-64L)) = var_41;
        _pre_phi99 = _pre_phi97;
        rax_3 = var_40;
        r9_0 = 3UL;
        if (var_41 == 0UL) {
            *(uint64_t *)(var_0 + (-96L)) = 4219535UL;
            indirect_placeholder_1(rcx, rdx, r9_0, rdi, rsi, var_1);
            return;
        }
        var_42 = rax_3 + 8UL;
        *_pre_phi99 = var_42;
        var_43 = *(uint64_t *)rax_3;
        *(uint64_t *)(var_0 + (-56L)) = var_43;
        _pre_phi101 = _pre_phi99;
        rax_4 = var_42;
        r9_0 = 4UL;
        if (var_43 == 0UL) {
            *(uint64_t *)(var_0 + (-96L)) = 4219535UL;
            indirect_placeholder_1(rcx, rdx, r9_0, rdi, rsi, var_1);
            return;
        }
        var_44 = rax_4 + 8UL;
        *_pre_phi101 = var_44;
        var_45 = *(uint64_t *)rax_4;
        *(uint64_t *)(var_0 + (-48L)) = var_45;
        _pre_phi103 = _pre_phi101;
        rax_5 = var_44;
        r9_0 = 5UL;
        if (var_45 == 0UL) {
            *(uint64_t *)(var_0 + (-96L)) = 4219535UL;
            indirect_placeholder_1(rcx, rdx, r9_0, rdi, rsi, var_1);
            return;
        }
    }
    var_11 = var_2 + 24U;
    var_12 = (uint64_t)var_11;
    *_cast = var_11;
    var_13 = *(uint64_t *)(var_9 + var_4);
    *(uint64_t *)(var_0 + (-72L)) = var_13;
    if (var_13 == 0UL) {
        *(uint64_t *)(var_0 + (-96L)) = 4219535UL;
        indirect_placeholder_1(rcx, rdx, r9_0, rdi, rsi, var_1);
        return;
    }
    r9_0 = 3UL;
    if ((uint64_t)(var_11 & (-16)) > 47UL) {
        var_27 = (uint64_t *)(r8 + 8UL);
        var_28 = *var_27;
        _pre_phi97 = var_27;
        rax_2 = var_28;
        var_40 = rax_2 + 8UL;
        *_pre_phi97 = var_40;
        var_41 = *(uint64_t *)rax_2;
        *(uint64_t *)(var_0 + (-64L)) = var_41;
        _pre_phi99 = _pre_phi97;
        rax_3 = var_40;
        r9_0 = 3UL;
        if (var_41 == 0UL) {
            *(uint64_t *)(var_0 + (-96L)) = 4219535UL;
            indirect_placeholder_1(rcx, rdx, r9_0, rdi, rsi, var_1);
            return;
        }
        var_42 = rax_3 + 8UL;
        *_pre_phi99 = var_42;
        var_43 = *(uint64_t *)rax_3;
        *(uint64_t *)(var_0 + (-56L)) = var_43;
        _pre_phi101 = _pre_phi99;
        rax_4 = var_42;
        r9_0 = 4UL;
        if (var_43 == 0UL) {
            *(uint64_t *)(var_0 + (-96L)) = 4219535UL;
            indirect_placeholder_1(rcx, rdx, r9_0, rdi, rsi, var_1);
            return;
        }
        var_44 = rax_4 + 8UL;
        *_pre_phi101 = var_44;
        var_45 = *(uint64_t *)rax_4;
        *(uint64_t *)(var_0 + (-48L)) = var_45;
        _pre_phi103 = _pre_phi101;
        rax_5 = var_44;
        r9_0 = 5UL;
        if (var_45 == 0UL) {
            *(uint64_t *)(var_0 + (-96L)) = 4219535UL;
            indirect_placeholder_1(rcx, rdx, r9_0, rdi, rsi, var_1);
            return;
        }
    }
    var_14 = var_2 + 32U;
    var_15 = (uint64_t)var_14;
    *_cast = var_14;
    var_16 = *(uint64_t *)(var_12 + var_4);
    *(uint64_t *)(var_0 + (-64L)) = var_16;
    if (var_16 == 0UL) {
        *(uint64_t *)(var_0 + (-96L)) = 4219535UL;
        indirect_placeholder_1(rcx, rdx, r9_0, rdi, rsi, var_1);
        return;
    }
    r9_0 = 4UL;
    if ((uint64_t)(var_14 & (-16)) > 47UL) {
        var_25 = (uint64_t *)(r8 + 8UL);
        var_26 = *var_25;
        _pre_phi99 = var_25;
        rax_3 = var_26;
        var_42 = rax_3 + 8UL;
        *_pre_phi99 = var_42;
        var_43 = *(uint64_t *)rax_3;
        *(uint64_t *)(var_0 + (-56L)) = var_43;
        _pre_phi101 = _pre_phi99;
        rax_4 = var_42;
        r9_0 = 4UL;
        if (var_43 == 0UL) {
            *(uint64_t *)(var_0 + (-96L)) = 4219535UL;
            indirect_placeholder_1(rcx, rdx, r9_0, rdi, rsi, var_1);
            return;
        }
        var_44 = rax_4 + 8UL;
        *_pre_phi101 = var_44;
        var_45 = *(uint64_t *)rax_4;
        *(uint64_t *)(var_0 + (-48L)) = var_45;
        _pre_phi103 = _pre_phi101;
        rax_5 = var_44;
        r9_0 = 5UL;
        if (var_45 == 0UL) {
            *(uint64_t *)(var_0 + (-96L)) = 4219535UL;
            indirect_placeholder_1(rcx, rdx, r9_0, rdi, rsi, var_1);
            return;
        }
    }
    var_17 = var_2 + 40U;
    var_18 = (uint64_t)var_17;
    *_cast = var_17;
    var_19 = *(uint64_t *)(var_15 + var_4);
    *(uint64_t *)(var_0 + (-56L)) = var_19;
    if (var_19 == 0UL) {
        *(uint64_t *)(var_0 + (-96L)) = 4219535UL;
        indirect_placeholder_1(rcx, rdx, r9_0, rdi, rsi, var_1);
        return;
    }
    r9_0 = 5UL;
    if ((uint64_t)(var_17 & (-16)) > 47UL) {
        var_23 = (uint64_t *)(r8 + 8UL);
        var_24 = *var_23;
        _pre_phi101 = var_23;
        rax_4 = var_24;
        var_44 = rax_4 + 8UL;
        *_pre_phi101 = var_44;
        var_45 = *(uint64_t *)rax_4;
        *(uint64_t *)(var_0 + (-48L)) = var_45;
        _pre_phi103 = _pre_phi101;
        rax_5 = var_44;
        r9_0 = 5UL;
        if (var_45 == 0UL) {
            *(uint64_t *)(var_0 + (-96L)) = 4219535UL;
            indirect_placeholder_1(rcx, rdx, r9_0, rdi, rsi, var_1);
            return;
        }
    }
    *_cast = (var_2 + 48U);
    var_20 = *(uint64_t *)(var_18 + var_4);
    *(uint64_t *)(var_0 + (-48L)) = var_20;
    if (var_20 == 0UL) {
        *(uint64_t *)(var_0 + (-96L)) = 4219535UL;
        indirect_placeholder_1(rcx, rdx, r9_0, rdi, rsi, var_1);
        return;
    }
    var_21 = (uint64_t *)(r8 + 8UL);
    var_22 = *var_21;
    _pre_phi103 = var_21;
    rax_5 = var_22;
    var_46 = rax_5 + 8UL;
    *_pre_phi103 = var_46;
    var_47 = *(uint64_t *)rax_5;
    *(uint64_t *)(var_0 + (-40L)) = var_47;
    r9_0 = 6UL;
    var_48 = rax_5 + 16UL;
    *_pre_phi103 = var_48;
    var_49 = *(uint64_t *)var_46;
    *(uint64_t *)(var_0 + (-32L)) = var_49;
    r9_0 = 7UL;
    var_50 = rax_5 + 24UL;
    *_pre_phi103 = var_50;
    var_51 = *(uint64_t *)var_48;
    *(uint64_t *)(var_0 + (-24L)) = var_51;
    r9_0 = 8UL;
    if (~(var_47 != 0UL & var_49 != 0UL & var_51 == 0UL)) {
        *(uint64_t *)(var_0 + (-96L)) = 4219535UL;
        indirect_placeholder_1(rcx, rdx, r9_0, rdi, rsi, var_1);
        return;
    }
    *_pre_phi103 = (rax_5 + 32UL);
    var_52 = *(uint64_t *)var_50;
    *(uint64_t *)(var_0 + (-16L)) = var_52;
    var_53 = (var_52 == 0UL) ? 9UL : 10UL;
    r9_0 = var_53;
    *(uint64_t *)(var_0 + (-96L)) = 4219535UL;
    indirect_placeholder_1(rcx, rdx, r9_0, rdi, rsi, var_1);
    return;
}
