typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_pxor_xmm_wrapper_81_ret_type;
struct type_5;
struct type_7;
struct helper_mulss_wrapper_ret_type;
struct helper_cvtsq2ss_wrapper_ret_type;
struct helper_addss_wrapper_ret_type;
struct helper_comiss_wrapper_ret_type;
struct helper_cvttss2sq_wrapper_ret_type;
struct helper_subss_wrapper_ret_type;
struct indirect_placeholder_72_ret_type;
struct helper_pxor_xmm_wrapper_81_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_5 {
};
struct type_7 {
};
struct helper_mulss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvtsq2ss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_addss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_comiss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvttss2sq_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_subss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct indirect_placeholder_72_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_cc_src2(void);
extern uint64_t init_state_0x8558(void);
extern unsigned char init_state_0x8549(void);
extern unsigned char init_state_0x854c(void);
extern unsigned char init_state_0x8548(void);
extern unsigned char init_state_0x854b(void);
extern unsigned char init_state_0x8547(void);
extern unsigned char init_state_0x854d(void);
extern uint64_t init_state_0x8560(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0, uint64_t param_1);
extern uint64_t init_state_0x8d58(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct helper_pxor_xmm_wrapper_81_ret_type helper_pxor_xmm_wrapper_81(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4);
extern struct helper_mulss_wrapper_ret_type helper_mulss_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_cvtsq2ss_wrapper_ret_type helper_cvtsq2ss_wrapper(struct type_5 *param_0, struct type_7 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_addss_wrapper_ret_type helper_addss_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9);
extern struct helper_comiss_wrapper_ret_type helper_comiss_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_cvttss2sq_wrapper_ret_type helper_cvttss2sq_wrapper(struct type_5 *param_0, struct type_7 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4);
extern struct helper_subss_wrapper_ret_type helper_subss_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct indirect_placeholder_72_ret_type indirect_placeholder_72(uint64_t param_0);
uint64_t bb_convert_to_decimal(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    struct helper_cvtsq2ss_wrapper_ret_type var_16;
    struct helper_cvttss2sq_wrapper_ret_type var_28;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    unsigned char var_9;
    unsigned char var_10;
    unsigned char var_11;
    unsigned char var_12;
    unsigned char var_13;
    unsigned char var_14;
    bool var_15;
    uint64_t var_36;
    uint64_t rdi2_2;
    uint64_t rdi2_0;
    uint64_t rdi2_4;
    uint64_t rbp_0_ph;
    uint64_t rdi2_1_ph;
    uint32_t *var_37;
    uint64_t rdi2_1;
    uint64_t rsi3_0;
    uint64_t rcx_0;
    uint32_t *var_38;
    uint32_t var_39;
    unsigned __int128 var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    unsigned char var_45;
    uint64_t var_46;
    uint64_t var_47;
    unsigned char var_48;
    uint64_t var_49;
    unsigned char var_50;
    uint64_t var_51;
    unsigned char var_52;
    uint64_t var_53;
    unsigned char var_54;
    uint64_t var_55;
    unsigned char var_56;
    uint64_t var_57;
    uint64_t var_58;
    unsigned char var_59;
    unsigned char var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t rdi2_3;
    uint64_t var_63;
    uint64_t rdi2_5;
    uint64_t cc_dst_0;
    uint64_t var_17;
    struct helper_cvtsq2ss_wrapper_ret_type var_18;
    struct helper_addss_wrapper_ret_type var_19;
    uint64_t state_0x8558_0;
    unsigned char storemerge;
    uint64_t var_20;
    uint64_t var_21;
    struct helper_mulss_wrapper_ret_type var_22;
    uint64_t var_23;
    struct helper_comiss_wrapper_ret_type var_24;
    uint64_t var_25;
    unsigned char var_26;
    uint64_t var_27;
    uint64_t rax_0;
    struct helper_subss_wrapper_ret_type var_29;
    struct helper_cvttss2sq_wrapper_ret_type var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    struct indirect_placeholder_72_ret_type var_34;
    uint64_t var_35;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_state_0x8d58();
    var_2 = init_rbp();
    var_3 = init_r12();
    var_4 = init_rbx();
    var_5 = init_r13();
    var_6 = init_cc_src2();
    var_7 = init_state_0x8558();
    var_8 = init_state_0x8560();
    var_9 = init_state_0x8549();
    var_10 = init_state_0x854c();
    var_11 = init_state_0x8548();
    var_12 = init_state_0x854b();
    var_13 = init_state_0x8547();
    var_14 = init_state_0x854d();
    *(uint64_t *)(var_0 + (-8L)) = var_5;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    var_15 = ((long)rdi < (long)0UL);
    helper_pxor_xmm_wrapper_81((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_7, var_8);
    rbp_0_ph = rdi;
    rcx_0 = 0UL;
    cc_dst_0 = rdi;
    if (var_15) {
        var_17 = (rdi >> 1UL) | (rdi & 1UL);
        var_18 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_17, var_9, var_11, var_12, var_13);
        var_19 = helper_addss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_18.field_0, var_18.field_1, var_10, var_11, var_12, var_13, var_14);
        cc_dst_0 = var_17;
        state_0x8558_0 = var_19.field_0;
        storemerge = var_19.field_1;
    } else {
        var_16 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), rdi, var_9, var_11, var_12, var_13);
        state_0x8558_0 = var_16.field_0;
        storemerge = var_16.field_1;
    }
    var_20 = (uint64_t)*(uint32_t *)4266180UL;
    var_21 = var_1 & (-4294967296L);
    var_22 = helper_mulss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), var_21 | var_20, state_0x8558_0, storemerge, var_10, var_11, var_12, var_13, var_14);
    var_23 = var_22.field_0;
    var_24 = helper_comiss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), var_21 | (uint64_t)*(uint32_t *)4266184UL, var_23, var_22.field_1, var_10);
    var_25 = var_24.field_0;
    var_26 = var_24.field_1;
    var_27 = helper_cc_compute_c_wrapper(cc_dst_0, var_25, var_6, 1U);
    if (var_27 == 0UL) {
        var_29 = helper_subss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), var_21 | (uint64_t)*(uint32_t *)4266184UL, var_23, var_26, var_10, var_11, var_12, var_13, var_14);
        var_30 = helper_cvttss2sq_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_29.field_0, var_29.field_1, var_10);
        var_31 = var_30.field_0 ^ (-9223372036854775808L);
        helper_cc_compute_all_wrapper(cc_dst_0, var_25, var_6, 1U);
        rax_0 = var_31;
    } else {
        var_28 = helper_cvttss2sq_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_23, var_26, var_10);
        rax_0 = var_28.field_0;
    }
    var_32 = (rax_0 * 9UL) + 9UL;
    *(uint64_t *)(var_0 + (-48L)) = 4224271UL;
    var_33 = indirect_placeholder_5(var_32, rdx);
    *(uint64_t *)(var_0 + (-56L)) = 4224279UL;
    var_34 = indirect_placeholder_72(var_33);
    var_35 = var_34.field_0;
    rdi2_0 = var_35;
    if (var_35 == 0UL) {
        return var_35;
    }
    if (rdx == 0UL) {
        if (rdi != 0UL) {
            *(unsigned char *)var_35 = (unsigned char)'0';
            rdi2_5 = var_35 + 1UL;
            *(unsigned char *)rdi2_5 = (unsigned char)'\x00';
            return var_35;
        }
    }
    *(uint64_t *)(var_0 + (-64L)) = 4224316UL;
    indirect_placeholder();
    var_36 = var_35 + rdx;
    rdi2_0 = var_36;
    rdi2_2 = var_36;
    if (rdi != 0UL) {
        var_62 = helper_cc_compute_c_wrapper(var_35 - rdi2_2, rdi2_2, var_6, 17U);
        rdi2_3 = rdi2_2;
        rdi2_4 = rdi2_2;
        if (var_62 == 0UL) {
            rdi2_5 = rdi2_4;
            if (rdi2_4 == var_35) {
                *(unsigned char *)var_35 = (unsigned char)'0';
                rdi2_5 = var_35 + 1UL;
            }
            *(unsigned char *)rdi2_5 = (unsigned char)'\x00';
            return var_35;
        }
        while (1U)
            {
                var_63 = rdi2_3 + (-1L);
                rdi2_3 = var_63;
                rdi2_4 = rdi2_3;
                if (*(unsigned char *)var_63 != '0') {
                    loop_state_var = 1U;
                    break;
                }
                if (var_35 == var_63) {
                    continue;
                }
                loop_state_var = 0U;
                break;
            }
        switch (loop_state_var) {
          case 1U:
            {
                break;
            }
            break;
          case 0U:
            {
                *(unsigned char *)var_35 = (unsigned char)'0';
                rdi2_5 = var_35 + 1UL;
                *(unsigned char *)rdi2_5 = (unsigned char)'\x00';
                return var_35;
            }
            break;
        }
    }
    rdi2_1_ph = rdi2_0;
    var_61 = rbp_0_ph + (-1L);
    rbp_0_ph = var_61;
    do {
        var_37 = (uint32_t *)(((rbp_0_ph << 2UL) + rsi) + (-4L));
        rdi2_1 = rdi2_1_ph;
        rsi3_0 = rbp_0_ph;
        var_44 = (var_42 * 3435973837UL) >> 35UL;
        var_45 = (unsigned char)var_44;
        var_46 = var_44 * 3435973837UL;
        *(unsigned char *)rdi2_1 = (((unsigned char)var_42 + (var_45 * '\xf6')) + '0');
        var_47 = var_46 >> 35UL;
        var_48 = (unsigned char)var_47;
        *(unsigned char *)(rdi2_1 + 1UL) = ((var_45 + (var_48 * '\xf6')) + '0');
        var_49 = (var_47 * 3435973837UL) >> 35UL;
        var_50 = (unsigned char)var_49;
        *(unsigned char *)(rdi2_1 + 2UL) = ((var_48 + (var_50 * '\xf6')) + '0');
        var_51 = (var_49 * 3435973837UL) >> 35UL;
        var_52 = (unsigned char)var_51;
        *(unsigned char *)(rdi2_1 + 3UL) = ((var_50 + (var_52 * '\xf6')) + '0');
        var_53 = (var_51 * 3435973837UL) >> 35UL;
        var_54 = (unsigned char)var_53;
        *(unsigned char *)(rdi2_1 + 4UL) = ((var_52 + (var_54 * '\xf6')) + '0');
        var_55 = (var_53 * 3435973837UL) >> 35UL;
        var_56 = (unsigned char)var_55;
        *(unsigned char *)(rdi2_1 + 5UL) = ((var_54 + (var_56 * '\xf6')) + '0');
        var_57 = (var_55 * 3435973837UL) >> 35UL;
        var_58 = rdi2_1 + 9UL;
        var_59 = (unsigned char)var_57;
        *(unsigned char *)(rdi2_1 + 6UL) = ((var_56 + (var_59 * '\xf6')) + '0');
        var_60 = (unsigned char)((var_57 * 3435973837UL) >> 35UL);
        *(unsigned char *)(rdi2_1 + 7UL) = ((var_59 + (var_60 * '\xf6')) + '0');
        *(unsigned char *)(rdi2_1 + 8UL) = (var_60 + '0');
        rdi2_1_ph = var_58;
        rdi2_1 = var_58;
        rdi2_2 = var_58;
        do {
            var_38 = (uint32_t *)(((rsi3_0 << 2UL) + rsi) + (-4L));
            var_39 = *var_38;
            var_40 = ((unsigned __int128)(((rcx_0 << 32UL) | (uint64_t)var_39) >> 9UL) * 19342813113834067ULL) >> 75ULL;
            var_41 = (uint64_t)var_40;
            *var_38 = (uint32_t)var_40;
            var_42 = (uint64_t)(((uint32_t)var_41 * 3294967296U) + var_39);
            var_43 = rsi3_0 + (-1L);
            rsi3_0 = var_43;
            rcx_0 = var_42;
            do {
                var_38 = (uint32_t *)(((rsi3_0 << 2UL) + rsi) + (-4L));
                var_39 = *var_38;
                var_40 = ((unsigned __int128)(((rcx_0 << 32UL) | (uint64_t)var_39) >> 9UL) * 19342813113834067ULL) >> 75ULL;
                var_41 = (uint64_t)var_40;
                *var_38 = (uint32_t)var_40;
                var_42 = (uint64_t)(((uint32_t)var_41 * 3294967296U) + var_39);
                var_43 = rsi3_0 + (-1L);
                rsi3_0 = var_43;
                rcx_0 = var_42;
            } while (var_43 != 0UL);
            var_44 = (var_42 * 3435973837UL) >> 35UL;
            var_45 = (unsigned char)var_44;
            var_46 = var_44 * 3435973837UL;
            *(unsigned char *)rdi2_1 = (((unsigned char)var_42 + (var_45 * '\xf6')) + '0');
            var_47 = var_46 >> 35UL;
            var_48 = (unsigned char)var_47;
            *(unsigned char *)(rdi2_1 + 1UL) = ((var_45 + (var_48 * '\xf6')) + '0');
            var_49 = (var_47 * 3435973837UL) >> 35UL;
            var_50 = (unsigned char)var_49;
            *(unsigned char *)(rdi2_1 + 2UL) = ((var_48 + (var_50 * '\xf6')) + '0');
            var_51 = (var_49 * 3435973837UL) >> 35UL;
            var_52 = (unsigned char)var_51;
            *(unsigned char *)(rdi2_1 + 3UL) = ((var_50 + (var_52 * '\xf6')) + '0');
            var_53 = (var_51 * 3435973837UL) >> 35UL;
            var_54 = (unsigned char)var_53;
            *(unsigned char *)(rdi2_1 + 4UL) = ((var_52 + (var_54 * '\xf6')) + '0');
            var_55 = (var_53 * 3435973837UL) >> 35UL;
            var_56 = (unsigned char)var_55;
            *(unsigned char *)(rdi2_1 + 5UL) = ((var_54 + (var_56 * '\xf6')) + '0');
            var_57 = (var_55 * 3435973837UL) >> 35UL;
            var_58 = rdi2_1 + 9UL;
            var_59 = (unsigned char)var_57;
            *(unsigned char *)(rdi2_1 + 6UL) = ((var_56 + (var_59 * '\xf6')) + '0');
            var_60 = (unsigned char)((var_57 * 3435973837UL) >> 35UL);
            *(unsigned char *)(rdi2_1 + 7UL) = ((var_59 + (var_60 * '\xf6')) + '0');
            *(unsigned char *)(rdi2_1 + 8UL) = (var_60 + '0');
            rdi2_1_ph = var_58;
            rdi2_1 = var_58;
            rdi2_2 = var_58;
        } while (*var_37 != 0U);
        var_61 = rbp_0_ph + (-1L);
        rbp_0_ph = var_61;
    } while (var_61 != 0UL);
    var_62 = helper_cc_compute_c_wrapper(var_35 - rdi2_2, rdi2_2, var_6, 17U);
    rdi2_3 = rdi2_2;
    rdi2_4 = rdi2_2;
    if (var_62 != 0UL) {
        while (1U)
            {
                var_63 = rdi2_3 + (-1L);
                rdi2_3 = var_63;
                rdi2_4 = rdi2_3;
                if (*(unsigned char *)var_63 != '0') {
                    loop_state_var = 1U;
                    break;
                }
                if (var_35 == var_63) {
                    continue;
                }
                loop_state_var = 0U;
                break;
            }
        switch (loop_state_var) {
          case 1U:
            {
                break;
            }
            break;
          case 0U:
            {
                *(unsigned char *)var_35 = (unsigned char)'0';
                rdi2_5 = var_35 + 1UL;
                *(unsigned char *)rdi2_5 = (unsigned char)'\x00';
                return var_35;
            }
            break;
        }
    }
    rdi2_5 = rdi2_4;
    if (rdi2_4 == var_35) {
        *(unsigned char *)var_35 = (unsigned char)'0';
        rdi2_5 = var_35 + 1UL;
    }
    *(unsigned char *)rdi2_5 = (unsigned char)'\x00';
}
