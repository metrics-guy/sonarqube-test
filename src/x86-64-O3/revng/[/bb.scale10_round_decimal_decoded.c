typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_74_ret_type;
struct helper_pxor_xmm_wrapper_81_ret_type;
struct type_6;
struct type_8;
struct helper_cvtsi2ss_wrapper_ret_type;
struct helper_mulss_wrapper_ret_type;
struct helper_cvttss2si_wrapper_ret_type;
struct indirect_placeholder_75_ret_type;
struct indirect_placeholder_74_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_pxor_xmm_wrapper_81_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_6 {
};
struct type_8 {
};
struct helper_cvtsi2ss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_mulss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvttss2si_wrapper_ret_type {
    uint32_t field_0;
    unsigned char field_1;
};
struct indirect_placeholder_75_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_cc_src2(void);
extern uint64_t init_state_0x8558(void);
extern unsigned char init_state_0x8549(void);
extern unsigned char init_state_0x854c(void);
extern unsigned char init_state_0x8548(void);
extern unsigned char init_state_0x854b(void);
extern unsigned char init_state_0x8547(void);
extern unsigned char init_state_0x854d(void);
extern uint64_t init_state_0x8560(void);
extern uint64_t init_state_0x8d58(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t indirect_placeholder_6(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder_36(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct indirect_placeholder_74_ret_type indirect_placeholder_74(uint64_t param_0);
extern struct helper_pxor_xmm_wrapper_81_ret_type helper_pxor_xmm_wrapper_81(struct type_6 *param_0, struct type_8 *param_1, struct type_8 *param_2, uint64_t param_3, uint64_t param_4);
extern struct helper_cvtsi2ss_wrapper_ret_type helper_cvtsi2ss_wrapper(struct type_6 *param_0, struct type_8 *param_1, uint32_t param_2, unsigned char param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_mulss_wrapper_ret_type helper_mulss_wrapper(struct type_6 *param_0, struct type_8 *param_1, struct type_8 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_cvttss2si_wrapper_ret_type helper_cvttss2si_wrapper(struct type_6 *param_0, struct type_8 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4);
extern struct indirect_placeholder_75_ret_type indirect_placeholder_75(uint64_t param_0);
uint64_t bb_scale10_round_decimal_decoded(uint64_t rcx, uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t r8) {
    struct helper_cvttss2si_wrapper_ret_type var_41;
    uint64_t var_161;
    uint64_t rax_3;
    uint32_t *var_59;
    uint32_t var_60;
    uint64_t var_61;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t rax_2;
    uint64_t var_79;
    uint64_t var_80;
    struct helper_cvtsi2ss_wrapper_ret_type var_34;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    unsigned char var_11;
    unsigned char var_12;
    unsigned char var_13;
    unsigned char var_14;
    unsigned char var_15;
    unsigned char var_16;
    uint64_t var_17;
    uint64_t *var_18;
    uint64_t r12_0;
    uint64_t var_162;
    uint64_t var_163;
    uint64_t var_164;
    uint64_t var_165;
    uint32_t var_113;
    uint64_t var_114;
    uint32_t var_117;
    uint64_t var_149;
    uint64_t var_115;
    uint64_t var_116;
    uint64_t r10_0;
    uint64_t rbx_0;
    uint64_t local_sp_0;
    uint64_t var_118;
    uint64_t r13_3;
    uint64_t var_119;
    uint64_t var_120;
    uint64_t var_121;
    uint64_t var_122;
    uint64_t var_123;
    uint64_t var_124;
    uint64_t rdx2_1;
    uint64_t r13_0;
    uint64_t local_sp_1;
    uint64_t rdi3_0;
    uint64_t var_74;
    uint64_t local_sp_2;
    bool var_75;
    uint64_t *var_76;
    uint64_t var_77;
    bool var_78;
    uint64_t var_88;
    uint64_t rax_1;
    uint64_t rdx2_0;
    uint64_t rax_0;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t var_100;
    uint64_t rsi4_0;
    uint64_t var_128;
    uint64_t var_129;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t var_151;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t local_sp_3;
    uint64_t r13_2;
    uint64_t r10_1;
    uint64_t r13_1;
    uint32_t var_55;
    uint64_t var_56;
    bool var_57;
    uint64_t rcx1_0;
    uint64_t var_58;
    uint64_t rdx2_2;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t r15_0;
    uint64_t var_65;
    uint64_t r15_1;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t *var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    struct indirect_placeholder_74_ret_type var_72;
    uint64_t var_73;
    uint64_t var_106;
    uint64_t var_107;
    uint64_t var_108;
    uint64_t var_109;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t var_112;
    uint64_t r13_4;
    uint64_t r13_5;
    uint64_t var_125;
    uint64_t var_126;
    uint64_t rdx2_3;
    uint64_t rax_4;
    uint32_t *var_127;
    uint64_t var_130;
    uint64_t var_131;
    uint64_t r13_6;
    uint32_t *var_132;
    uint64_t var_133;
    uint64_t var_134;
    uint64_t var_135;
    uint64_t var_136;
    uint64_t var_137;
    uint64_t var_138;
    uint64_t var_139;
    uint64_t var_142;
    uint64_t var_143;
    uint64_t rax_5;
    uint64_t var_144;
    uint64_t var_145;
    uint64_t var_146;
    uint64_t var_147;
    uint64_t var_148;
    uint64_t var_150;
    uint64_t var_152;
    uint64_t var_140;
    uint64_t local_sp_5;
    uint64_t rax_6;
    uint64_t var_141;
    uint64_t var_153;
    uint64_t var_154;
    uint64_t var_155;
    uint64_t var_156;
    bool var_157;
    uint64_t var_158;
    uint64_t *var_159;
    uint64_t var_160;
    uint64_t var_19;
    uint32_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t rbx_1_in;
    uint64_t r9_0;
    uint32_t var_27;
    uint64_t var_28;
    uint32_t *var_29;
    uint32_t var_30;
    uint32_t var_31;
    uint32_t var_32;
    uint64_t var_33;
    struct helper_mulss_wrapper_ret_type var_35;
    uint64_t var_36;
    unsigned char var_37;
    uint32_t var_38;
    uint32_t var_39;
    uint64_t var_40;
    uint32_t var_42;
    uint32_t var_43;
    uint64_t var_44;
    uint32_t *var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t *var_48;
    struct indirect_placeholder_75_ret_type var_49;
    uint64_t var_50;
    uint32_t var_51;
    uint64_t var_52;
    uint32_t var_53;
    uint64_t var_54;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_state_0x8d58();
    var_2 = init_rbp();
    var_3 = init_r15();
    var_4 = init_r14();
    var_5 = init_r12();
    var_6 = init_rbx();
    var_7 = init_r13();
    var_8 = init_cc_src2();
    var_9 = init_state_0x8558();
    var_10 = init_state_0x8560();
    var_11 = init_state_0x8549();
    var_12 = init_state_0x854c();
    var_13 = init_state_0x8548();
    var_14 = init_state_0x854b();
    var_15 = init_state_0x8547();
    var_16 = init_state_0x854d();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_7;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    *(uint64_t *)(var_0 + (-48L)) = var_6;
    var_17 = var_0 + (-128L);
    var_18 = (uint64_t *)var_17;
    *var_18 = rsi;
    rax_3 = 0UL;
    rax_2 = 0UL;
    r12_0 = 0UL;
    var_117 = 0U;
    r13_3 = 1UL;
    rax_1 = 0UL;
    rdx2_0 = 0UL;
    rax_0 = 0UL;
    rsi4_0 = 0UL;
    r10_1 = 0UL;
    r13_1 = 1UL;
    rcx1_0 = 1220703125UL;
    rax_4 = 0UL;
    rax_5 = 0UL;
    rbx_1_in = r8;
    if (rcx == 0UL) {
        return r12_0;
    }
    var_19 = r8 + rdi;
    var_20 = (uint32_t)var_19;
    var_21 = (uint64_t)var_20;
    var_22 = helper_cc_compute_all_wrapper(var_21, 0UL, 0UL, 24U);
    r9_0 = var_21;
    if ((uint64_t)(((unsigned char)(var_22 >> 4UL) ^ (unsigned char)var_22) & '\xc0') == 0UL) {
        *(uint64_t *)(var_0 + (-96L)) = 0UL;
    } else {
        var_23 = helper_cc_compute_all_wrapper(r8, 0UL, 0UL, 24U);
        if ((uint64_t)(((unsigned char)(var_23 >> 4UL) ^ (unsigned char)var_23) & '\xc0') == 0UL) {
            var_24 = ((long)(r8 << 32UL) > (long)(var_19 << 32UL)) ? var_21 : r8;
            var_25 = (uint64_t)(var_20 - (uint32_t)var_24);
            var_26 = r8 - var_24;
            *(uint64_t *)(var_0 + (-96L)) = (uint64_t)((long)(var_24 << 32UL) >> (long)32UL);
            rbx_1_in = var_26;
            r9_0 = var_25;
        } else {
            *(uint64_t *)(var_0 + (-96L)) = 0UL;
        }
    }
    var_27 = (uint32_t)rbx_1_in;
    var_28 = (uint64_t)var_27;
    helper_pxor_xmm_wrapper_81((struct type_6 *)(0UL), (struct type_8 *)(776UL), (struct type_8 *)(776UL), var_9, var_10);
    var_29 = (uint32_t *)(var_0 + (-100L));
    var_30 = (uint32_t)r9_0;
    *var_29 = var_30;
    var_31 = (uint32_t)(uint64_t)((long)(var_28 << 32UL) >> (long)63UL);
    var_32 = (var_31 ^ var_27) - var_31;
    var_33 = (uint64_t)var_32;
    var_34 = helper_cvtsi2ss_wrapper((struct type_6 *)(0UL), (struct type_8 *)(776UL), var_32, var_11, var_13, var_14, var_15);
    var_35 = helper_mulss_wrapper((struct type_6 *)(0UL), (struct type_8 *)(776UL), (struct type_8 *)(2824UL), (var_1 & (-4294967296L)) | (uint64_t)*(uint32_t *)4266232UL, var_34.field_0, var_34.field_1, var_12, var_13, var_14, var_15, var_16);
    var_36 = var_35.field_0;
    var_37 = var_35.field_1;
    var_38 = (uint32_t)(uint64_t)((long)(r9_0 << 32UL) >> (long)63UL);
    var_39 = (var_38 ^ var_30) - var_38;
    var_40 = (uint64_t)var_39;
    *(uint32_t *)(var_0 + (-112L)) = var_39;
    var_41 = helper_cvttss2si_wrapper((struct type_6 *)(0UL), (struct type_8 *)(776UL), var_36, var_37, var_12);
    var_42 = var_41.field_0;
    var_43 = (uint32_t)(var_40 >> 5UL) & 134217727U;
    var_44 = var_0 + (-120L);
    var_45 = (uint32_t *)var_44;
    *var_45 = var_43;
    var_46 = (((uint64_t)(var_43 + var_42) << 2UL) + 8UL) & 17179869180UL;
    var_47 = var_0 + (-144L);
    var_48 = (uint64_t *)var_47;
    *var_48 = 4228789UL;
    var_49 = indirect_placeholder_75(var_46);
    var_50 = var_49.field_0;
    var_51 = *var_45;
    var_52 = (uint64_t)var_51;
    var_53 = *(uint32_t *)(var_0 + (-108L));
    var_54 = (uint64_t)var_53;
    rdx2_2 = var_50;
    rdx2_3 = var_50;
    local_sp_5 = var_47;
    if (var_50 == 0UL) {
        *(uint64_t *)(var_0 + (-152L)) = 4230030UL;
        indirect_placeholder();
    } else {
        *(uint32_t *)var_50 = 1U;
        if (var_28 == 0UL) {
            var_106 = var_52 & 31UL;
            r15_0 = var_106;
            r15_1 = r15_0;
            r13_4 = r13_3;
            if ((int)var_53 <= (int)4294967295U) {
                var_107 = var_0 + (-136L);
                var_108 = *(uint64_t *)var_107;
                var_109 = var_0 + (-72L);
                var_110 = var_0 + (-80L);
                var_111 = var_0 + (-152L);
                *(uint64_t *)var_111 = 4229691UL;
                var_112 = indirect_placeholder_36(var_50, r13_3, var_109, var_108, rdx, var_110);
                local_sp_0 = var_111;
                local_sp_3 = var_111;
                if (var_112 == 0UL) {
                    *(uint64_t *)(local_sp_3 + (-8L)) = 4229878UL;
                    indirect_placeholder();
                    *(uint64_t *)(local_sp_3 + (-16L)) = 4229886UL;
                    indirect_placeholder();
                    return r12_0;
                }
                var_113 = *(uint32_t *)var_107;
                var_114 = (r13_3 << 2UL) + var_50;
                r10_0 = var_114;
                rbx_0 = var_114;
                if (var_113 == 0U) {
                    var_115 = (uint64_t)var_113 << 2UL;
                    var_116 = var_0 + (-160L);
                    *(uint64_t *)var_116 = 4229736UL;
                    indirect_placeholder();
                    var_117 = *(uint32_t *)var_47;
                    r10_0 = var_112;
                    rbx_0 = var_115 + var_112;
                    local_sp_0 = var_116;
                }
                var_118 = *(uint64_t *)(local_sp_0 + 64UL);
                var_119 = *(uint64_t *)(local_sp_0 + 72UL);
                var_120 = local_sp_0 + 56UL;
                *(uint32_t *)rbx_0 = (uint32_t)(1UL << r15_0);
                var_121 = (uint64_t)(var_117 + 1U);
                var_122 = local_sp_0 + 48UL;
                *(uint64_t *)(local_sp_0 + (-8L)) = 4229789UL;
                var_123 = indirect_placeholder_36(r10_0, var_121, var_120, var_118, var_119, var_122);
                var_124 = local_sp_0 + (-16L);
                *(uint64_t *)var_124 = 4229800UL;
                indirect_placeholder();
                r13_0 = var_123;
                local_sp_1 = var_124;
                *(uint64_t *)(local_sp_1 + (-8L)) = 4229808UL;
                indirect_placeholder();
                *(uint64_t *)(local_sp_1 + (-16L)) = 4229816UL;
                indirect_placeholder();
                if (r13_0 == 0UL) {
                    var_162 = *(uint64_t *)(local_sp_1 + 32UL);
                    var_163 = *(uint64_t *)(local_sp_1 + 24UL);
                    var_164 = *(uint64_t *)(local_sp_1 + 40UL);
                    *(uint64_t *)(local_sp_1 + (-24L)) = 4229841UL;
                    var_165 = indirect_placeholder_6(var_163, var_162, var_164);
                    *(uint64_t *)(local_sp_1 + (-32L)) = 4229852UL;
                    indirect_placeholder();
                    r12_0 = var_165;
                }
                return r12_0;
            }
        }
        r13_1 = r13_2;
        r13_3 = r13_2;
        r13_4 = r13_2;
        do {
            var_55 = (uint32_t)r10_1;
            var_56 = (uint64_t)(var_55 + 13U);
            var_57 = (var_56 > var_33);
            r10_1 = var_56;
            r13_2 = r13_1;
            if (var_57) {
                rcx1_0 = (uint64_t)*(uint32_t *)((((uint64_t)(var_32 - var_55) << 2UL) & 17179869180UL) + 4266112UL);
            }
            var_58 = (r13_1 << 2UL) + var_50;
            var_59 = (uint32_t *)rdx2_2;
            var_60 = *var_59;
            var_61 = rdx2_2 + 4UL;
            var_62 = rax_3 + (rcx1_0 * (uint64_t)var_60);
            *var_59 = (uint32_t)var_62;
            var_63 = var_62 >> 32UL;
            rdx2_2 = var_61;
            rax_3 = var_63;
            do {
                var_59 = (uint32_t *)rdx2_2;
                var_60 = *var_59;
                var_61 = rdx2_2 + 4UL;
                var_62 = rax_3 + (rcx1_0 * (uint64_t)var_60);
                *var_59 = (uint32_t)var_62;
                var_63 = var_62 >> 32UL;
                rdx2_2 = var_61;
                rax_3 = var_63;
            } while (var_61 != var_58);
            if (var_63 == 0UL) {
                *(uint32_t *)var_58 = (uint32_t)var_63;
                r13_2 = r13_1 + 1UL;
            }
            r13_1 = r13_2;
            r13_3 = r13_2;
            r13_4 = r13_2;
        } while (!var_57);
        var_64 = var_52 & 31UL;
        r15_0 = var_64;
        r15_1 = var_64;
        if ((int)var_27 > (int)4294967295U) {
            r15_1 = r15_0;
            r13_4 = r13_3;
            if ((int)var_53 > (int)4294967295U) {
                var_107 = var_0 + (-136L);
                var_108 = *(uint64_t *)var_107;
                var_109 = var_0 + (-72L);
                var_110 = var_0 + (-80L);
                var_111 = var_0 + (-152L);
                *(uint64_t *)var_111 = 4229691UL;
                var_112 = indirect_placeholder_36(var_50, r13_3, var_109, var_108, rdx, var_110);
                local_sp_0 = var_111;
                local_sp_3 = var_111;
                if (var_112 != 0UL) {
                    *(uint64_t *)(local_sp_3 + (-8L)) = 4229878UL;
                    indirect_placeholder();
                    *(uint64_t *)(local_sp_3 + (-16L)) = 4229886UL;
                    indirect_placeholder();
                    return r12_0;
                }
                var_113 = *(uint32_t *)var_107;
                var_114 = (r13_3 << 2UL) + var_50;
                r10_0 = var_114;
                rbx_0 = var_114;
                if (var_113 == 0U) {
                    var_115 = (uint64_t)var_113 << 2UL;
                    var_116 = var_0 + (-160L);
                    *(uint64_t *)var_116 = 4229736UL;
                    indirect_placeholder();
                    var_117 = *(uint32_t *)var_47;
                    r10_0 = var_112;
                    rbx_0 = var_115 + var_112;
                    local_sp_0 = var_116;
                }
                var_118 = *(uint64_t *)(local_sp_0 + 64UL);
                var_119 = *(uint64_t *)(local_sp_0 + 72UL);
                var_120 = local_sp_0 + 56UL;
                *(uint32_t *)rbx_0 = (uint32_t)(1UL << r15_0);
                var_121 = (uint64_t)(var_117 + 1U);
                var_122 = local_sp_0 + 48UL;
                *(uint64_t *)(local_sp_0 + (-8L)) = 4229789UL;
                var_123 = indirect_placeholder_36(r10_0, var_121, var_120, var_118, var_119, var_122);
                var_124 = local_sp_0 + (-16L);
                *(uint64_t *)var_124 = 4229800UL;
                indirect_placeholder();
                r13_0 = var_123;
                local_sp_1 = var_124;
                *(uint64_t *)(local_sp_1 + (-8L)) = 4229808UL;
                indirect_placeholder();
                *(uint64_t *)(local_sp_1 + (-16L)) = 4229816UL;
                indirect_placeholder();
                if (r13_0 != 0UL) {
                    var_162 = *(uint64_t *)(local_sp_1 + 32UL);
                    var_163 = *(uint64_t *)(local_sp_1 + 24UL);
                    var_164 = *(uint64_t *)(local_sp_1 + 40UL);
                    *(uint64_t *)(local_sp_1 + (-24L)) = 4229841UL;
                    var_165 = indirect_placeholder_6(var_163, var_162, var_164);
                    *(uint64_t *)(local_sp_1 + (-32L)) = 4229852UL;
                    indirect_placeholder();
                    r12_0 = var_165;
                }
                return r12_0;
            }
        }
        var_65 = helper_cc_compute_all_wrapper(var_54, 0UL, 0UL, 24U);
        if ((uint64_t)(((unsigned char)(var_65 >> 4UL) ^ (unsigned char)var_65) & '\xc0') != 0UL) {
            var_66 = (uint64_t)*(uint32_t *)var_17;
            var_67 = var_0 + (-136L);
            var_68 = (uint64_t *)var_67;
            var_69 = *var_68 + var_66;
            *(uint64_t *)var_44 = var_66;
            var_70 = (var_69 << 2UL) + 4UL;
            var_71 = var_0 + (-152L);
            *(uint64_t *)var_71 = 4228992UL;
            var_72 = indirect_placeholder_74(var_70);
            var_73 = var_72.field_0;
            rdi3_0 = var_73;
            local_sp_2 = var_71;
            local_sp_3 = var_71;
            if (var_73 != 0UL) {
                *(uint64_t *)(local_sp_3 + (-8L)) = 4229878UL;
                indirect_placeholder();
                *(uint64_t *)(local_sp_3 + (-16L)) = 4229886UL;
                indirect_placeholder();
                return r12_0;
            }
            if (*(uint32_t *)var_67 == 0U) {
                *var_68 = (*var_18 << 2UL);
                var_74 = var_0 + (-160L);
                *(uint64_t *)var_74 = 4229039UL;
                indirect_placeholder();
                rdi3_0 = *var_48 + var_73;
                local_sp_2 = var_74;
            }
            var_75 = (var_64 == 0UL);
            var_76 = (uint64_t *)(local_sp_2 + 8UL);
            var_77 = *var_76;
            var_78 = (var_77 == 0UL);
            rdx2_1 = rdi3_0;
            if (var_75) {
                if (!var_78) {
                    if ((((rdi3_0 + 15UL) - rdx) > 30UL) && ((var_77 + (-1L)) > 3UL)) {
                        var_88 = ((var_77 + (-4L)) >> 2UL) + 1UL;
                        var_89 = rax_0 + rdx;
                        var_90 = *(uint64_t *)var_89;
                        var_91 = *(uint64_t *)(var_89 + 8UL);
                        var_92 = rdx2_0 + 1UL;
                        var_93 = rax_0 + rdi3_0;
                        *(uint64_t *)var_93 = var_90;
                        *(uint64_t *)(var_93 + 8UL) = var_91;
                        var_94 = helper_cc_compute_c_wrapper(var_92 - var_88, var_88, var_8, 17U);
                        rdx2_0 = var_92;
                        while (var_94 != 0UL)
                            {
                                rax_0 = rax_0 + 16UL;
                                var_89 = rax_0 + rdx;
                                var_90 = *(uint64_t *)var_89;
                                var_91 = *(uint64_t *)(var_89 + 8UL);
                                var_92 = rdx2_0 + 1UL;
                                var_93 = rax_0 + rdi3_0;
                                *(uint64_t *)var_93 = var_90;
                                *(uint64_t *)(var_93 + 8UL) = var_91;
                                var_94 = helper_cc_compute_c_wrapper(var_92 - var_88, var_88, var_8, 17U);
                                rdx2_0 = var_92;
                            }
                        var_95 = var_88 << 2UL;
                        var_96 = var_88 << 4UL;
                        var_97 = var_96 + rdi3_0;
                        var_98 = var_96 + rdx;
                        var_99 = *var_76;
                        var_100 = var_99 - var_95;
                        *(uint32_t *)var_97 = *(uint32_t *)var_98;
                        *(uint32_t *)(var_97 + 4UL) = *(uint32_t *)(var_98 + 4UL);
                        if (var_99 != var_95 & var_100 != 1UL & var_100 == 2UL) {
                            *(uint32_t *)(var_97 + 8UL) = *(uint32_t *)(var_98 + 8UL);
                        }
                    } else {
                        var_86 = rax_1 << 2UL;
                        *(uint32_t *)(var_86 + rdi3_0) = *(uint32_t *)(var_86 + rdx);
                        var_87 = rax_1 + 1UL;
                        rax_1 = var_87;
                        do {
                            var_86 = rax_1 << 2UL;
                            *(uint32_t *)(var_86 + rdi3_0) = *(uint32_t *)(var_86 + rdx);
                            var_87 = rax_1 + 1UL;
                            rax_1 = var_87;
                        } while (var_77 != var_87);
                    }
                    rdx2_1 = (*var_76 << 2UL) + rdi3_0;
                }
            } else {
                if (!var_78) {
                    var_79 = rsi4_0 << 2UL;
                    var_80 = rax_2 + ((uint64_t)*(uint32_t *)(var_79 + rdx) << var_64);
                    *(uint32_t *)(var_79 + rdi3_0) = (uint32_t)var_80;
                    var_81 = rsi4_0 + 1UL;
                    var_82 = var_80 >> 32UL;
                    rsi4_0 = var_81;
                    rax_2 = var_82;
                    do {
                        var_79 = rsi4_0 << 2UL;
                        var_80 = rax_2 + ((uint64_t)*(uint32_t *)(var_79 + rdx) << var_64);
                        *(uint32_t *)(var_79 + rdi3_0) = (uint32_t)var_80;
                        var_81 = rsi4_0 + 1UL;
                        var_82 = var_80 >> 32UL;
                        rsi4_0 = var_81;
                        rax_2 = var_82;
                    } while (var_81 != var_77);
                    var_83 = var_77 << 2UL;
                    var_84 = var_83 + rdi3_0;
                    rdx2_1 = var_84;
                    if (var_82 == 0UL) {
                        var_85 = var_83 + (-4L);
                        *(uint32_t *)var_84 = (uint32_t)var_82;
                        rdx2_1 = (var_85 + rdi3_0) + 8UL;
                    }
                }
            }
            var_101 = local_sp_2 + 56UL;
            var_102 = local_sp_2 + 48UL;
            var_103 = (uint64_t)((long)(rdx2_1 - var_73) >> (long)2UL);
            *(uint64_t *)(local_sp_2 + (-8L)) = 4229260UL;
            var_104 = indirect_placeholder_36(var_50, r13_2, var_101, var_103, var_73, var_102);
            var_105 = local_sp_2 + (-16L);
            *(uint64_t *)var_105 = 4229271UL;
            indirect_placeholder();
            r13_0 = var_104;
            local_sp_1 = var_105;
            *(uint64_t *)(local_sp_1 + (-8L)) = 4229808UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_1 + (-16L)) = 4229816UL;
            indirect_placeholder();
            if (r13_0 != 0UL) {
                var_162 = *(uint64_t *)(local_sp_1 + 32UL);
                var_163 = *(uint64_t *)(local_sp_1 + 24UL);
                var_164 = *(uint64_t *)(local_sp_1 + 40UL);
                *(uint64_t *)(local_sp_1 + (-24L)) = 4229841UL;
                var_165 = indirect_placeholder_6(var_163, var_162, var_164);
                *(uint64_t *)(local_sp_1 + (-32L)) = 4229852UL;
                indirect_placeholder();
                r12_0 = var_165;
            }
            return r12_0;
        }
        r13_5 = r13_4;
        if ((uint64_t)(uint32_t)r15_1 != 0UL) {
            var_125 = (r13_4 << 2UL) + var_50;
            var_126 = r15_1 & 63UL;
            var_127 = (uint32_t *)rdx2_3;
            var_128 = (uint64_t)*var_127;
            var_129 = rdx2_3 + 4UL;
            var_130 = rax_4 + (var_128 << var_126);
            *var_127 = (uint32_t)var_130;
            var_131 = var_130 >> 32UL;
            rdx2_3 = var_129;
            rax_4 = var_131;
            do {
                var_127 = (uint32_t *)rdx2_3;
                var_128 = (uint64_t)*var_127;
                var_129 = rdx2_3 + 4UL;
                var_130 = rax_4 + (var_128 << var_126);
                *var_127 = (uint32_t)var_130;
                var_131 = var_130 >> 32UL;
                rdx2_3 = var_129;
                rax_4 = var_131;
            } while (var_125 != var_129);
            if (var_131 == 0UL) {
                *(uint32_t *)var_125 = (uint32_t)var_131;
                r13_5 = r13_4 + 1UL;
            }
        }
        r13_6 = r13_5;
        if ((int)var_51 <= (int)31U) {
            var_132 = (uint32_t *)var_17;
            var_133 = (uint64_t)*var_132;
            var_134 = r13_5 << 2UL;
            var_135 = var_134 + (-16L);
            var_136 = r13_5 + (-1L);
            var_137 = r13_5 + var_133;
            var_138 = var_137 << 2UL;
            var_139 = var_138 + (-16L);
            rax_6 = var_136;
            r13_6 = var_137;
            if (!((((long)var_138 <= (long)var_135) || ((long)var_139 >= (long)var_134)) && (var_136 > 3UL))) {
                var_140 = (var_133 << 2UL) + var_50;
                var_141 = rax_6 << 2UL;
                *(uint32_t *)(var_141 + var_140) = *(uint32_t *)(var_141 + var_50);
                while (rax_6 != 0UL)
                    {
                        rax_6 = rax_6 + (-1L);
                        var_141 = rax_6 << 2UL;
                        *(uint32_t *)(var_141 + var_140) = *(uint32_t *)(var_141 + var_50);
                    }
            }
            var_142 = var_139 + var_50;
            var_143 = (r13_5 >> 2UL) << 4UL;
            var_144 = rax_5 + (var_135 + var_50);
            var_145 = *(uint64_t *)var_144;
            var_146 = *(uint64_t *)(var_144 + 8UL);
            var_147 = rax_5 + var_142;
            *(uint64_t *)var_147 = var_145;
            *(uint64_t *)(var_147 + 8UL) = var_146;
            var_148 = rax_5 + (-16L);
            rax_5 = var_148;
            do {
                var_144 = rax_5 + (var_135 + var_50);
                var_145 = *(uint64_t *)var_144;
                var_146 = *(uint64_t *)(var_144 + 8UL);
                var_147 = rax_5 + var_142;
                *(uint64_t *)var_147 = var_145;
                *(uint64_t *)(var_147 + 8UL) = var_146;
                var_148 = rax_5 + (-16L);
                rax_5 = var_148;
            } while (var_148 != (0UL - var_143));
            var_149 = r13_5 & 3UL;
            var_150 = var_149 + (-1L);
            *(uint32_t *)(((var_150 + var_133) << 2UL) + var_50) = *(uint32_t *)((var_150 << 2UL) + var_50);
            var_151 = var_149 + (-2L);
            *(uint32_t *)(((var_151 + var_133) << 2UL) + var_50) = *(uint32_t *)((var_151 << 2UL) + var_50);
            if (var_149 != 0UL & var_150 != 0UL & var_151 == 0UL) {
                var_152 = var_149 + (-3L);
                *(uint32_t *)(((var_152 + var_133) << 2UL) + var_50) = *(uint32_t *)((var_152 << 2UL) + var_50);
            }
            if (*var_132 == 0U) {
                var_153 = var_0 + (-152L);
                *(uint64_t *)var_153 = 4229585UL;
                indirect_placeholder();
                local_sp_5 = var_153;
            }
        }
        var_154 = *(uint64_t *)(local_sp_5 + 8UL);
        var_155 = local_sp_5 + 56UL;
        var_156 = local_sp_5 + 48UL;
        var_157 = ((int)var_27 < (int)0U);
        var_158 = local_sp_5 + (-8L);
        var_159 = (uint64_t *)var_158;
        local_sp_1 = var_158;
        if (var_157) {
            *var_159 = 4229907UL;
            var_161 = indirect_placeholder_36(var_50, r13_6, var_155, var_154, rdx, var_156);
            r13_0 = var_161;
        } else {
            *var_159 = 4229625UL;
            var_160 = indirect_placeholder_36(var_50, r13_6, var_155, var_154, rdx, var_156);
            r13_0 = var_160;
        }
        *(uint64_t *)(local_sp_1 + (-8L)) = 4229808UL;
        indirect_placeholder();
        *(uint64_t *)(local_sp_1 + (-16L)) = 4229816UL;
        indirect_placeholder();
        if (r13_0 == 0UL) {
            var_162 = *(uint64_t *)(local_sp_1 + 32UL);
            var_163 = *(uint64_t *)(local_sp_1 + 24UL);
            var_164 = *(uint64_t *)(local_sp_1 + 40UL);
            *(uint64_t *)(local_sp_1 + (-24L)) = 4229841UL;
            var_165 = indirect_placeholder_6(var_163, var_162, var_164);
            *(uint64_t *)(local_sp_1 + (-32L)) = 4229852UL;
            indirect_placeholder();
            r12_0 = var_165;
        }
    }
}
