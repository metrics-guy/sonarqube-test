typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_divq_EAX_wrapper_ret_type;
struct type_5;
struct indirect_placeholder_78_ret_type;
struct indirect_placeholder_79_ret_type;
struct helper_divq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint32_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint32_t field_5;
    uint32_t field_6;
    uint64_t field_7;
    uint32_t field_8;
    uint64_t field_9;
    uint32_t field_10;
    unsigned char field_11;
    uint32_t field_12;
};
struct type_5 {
};
struct indirect_placeholder_78_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_79_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_cc_src2(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct helper_divq_EAX_wrapper_ret_type helper_divq_EAX_wrapper(struct type_5 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint32_t param_13, uint64_t param_14, uint32_t param_15, uint64_t param_16, uint64_t param_17, uint64_t param_18, uint32_t param_19);
extern uint32_t init_state_0x8248(void);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern struct indirect_placeholder_78_ret_type indirect_placeholder_78(uint64_t param_0);
extern uint64_t helper_clz_wrapper(uint64_t param_0);
extern struct indirect_placeholder_79_ret_type indirect_placeholder_79(uint64_t param_0);
uint64_t bb_divide_isra_0(uint64_t rcx, uint64_t rdx, uint64_t r9, uint64_t rdi, uint64_t rsi, uint64_t r8) {
    uint64_t r15_0;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_27;
    uint64_t rdx2_2;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_91;
    uint64_t var_92;
    uint32_t var_93;
    uint64_t var_94;
    struct helper_divq_EAX_wrapper_ret_type var_90;
    uint32_t var_95;
    uint64_t var_96;
    uint32_t var_97;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t var_100;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t r13_1;
    uint64_t r13_0;
    uint32_t state_0x8248_1;
    uint64_t state_0x9018_1;
    uint32_t state_0x9010_1;
    uint64_t rax_3;
    uint64_t var_134;
    uint64_t var_145;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint32_t var_8;
    uint64_t var_9;
    uint32_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint32_t var_14;
    uint64_t var_15;
    uint64_t *var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t *var_19;
    uint64_t *var_20;
    uint64_t var_21;
    uint64_t *var_22;
    struct indirect_placeholder_78_ret_type var_23;
    uint64_t var_24;
    uint64_t r12_5;
    uint64_t var_46;
    uint64_t rdx2_0;
    uint64_t rax_0;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t r13_5_in;
    uint64_t local_sp_7;
    uint64_t rdx2_1;
    uint64_t rsi5_0;
    uint64_t rax_1;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint32_t rax_2;
    uint64_t var_55;
    uint64_t r13_2;
    uint64_t rbp_0;
    uint64_t r10_0;
    uint64_t r93_0;
    uint64_t r86_0;
    uint64_t local_sp_0;
    uint32_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t *var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t *var_69;
    uint64_t var_70;
    uint64_t *var_71;
    uint64_t var_72;
    uint64_t *var_73;
    uint64_t *var_74;
    uint64_t *var_75;
    uint64_t var_76;
    uint64_t *var_77;
    uint64_t var_78;
    uint64_t r13_4;
    uint64_t rdi4_0;
    uint32_t state_0x8248_5;
    uint64_t rsi5_1;
    uint64_t r86_1;
    uint64_t state_0x9018_5;
    uint32_t state_0x8248_0;
    uint32_t state_0x9010_5;
    uint64_t state_0x9018_0;
    uint64_t state_0x82d8_5;
    uint32_t state_0x9010_0;
    uint32_t state_0x9080_5;
    uint64_t state_0x82d8_0;
    uint32_t state_0x9080_0;
    uint32_t *var_79;
    uint32_t var_80;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t state_0x82d8_1;
    uint32_t state_0x9080_1;
    uint64_t var_103;
    uint64_t var_104;
    uint32_t state_0x8248_2;
    uint64_t state_0x9018_2;
    uint32_t state_0x9010_2;
    uint64_t state_0x82d8_2;
    uint32_t state_0x9080_2;
    uint64_t rdi4_1;
    uint32_t state_0x8248_3;
    uint64_t state_0x9018_3;
    uint32_t state_0x9010_3;
    uint64_t state_0x82d8_3;
    uint32_t state_0x9080_3;
    uint64_t r13_3;
    uint32_t state_0x8248_4;
    uint64_t state_0x9018_4;
    uint32_t state_0x9010_4;
    uint64_t state_0x82d8_4;
    uint32_t state_0x9080_4;
    uint64_t var_105;
    uint64_t rcx1_0;
    uint64_t rax_4;
    uint64_t var_106;
    uint64_t var_107;
    uint32_t *var_108;
    uint32_t var_109;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t var_112;
    uint64_t var_113;
    uint64_t var_114;
    uint64_t var_115;
    uint64_t rsi5_3;
    uint64_t storemerge;
    uint64_t rsi5_2;
    uint64_t rax_5;
    uint64_t var_116;
    uint32_t *var_117;
    uint32_t var_118;
    uint64_t var_119;
    uint32_t var_120;
    uint64_t var_121;
    uint32_t var_122;
    uint64_t var_123;
    uint32_t var_124;
    uint64_t var_125;
    uint64_t var_126;
    uint64_t local_sp_6;
    uint64_t var_163;
    uint64_t var_127;
    uint64_t var_128;
    uint64_t var_129;
    uint64_t var_130;
    uint32_t var_131;
    uint64_t var_132;
    uint64_t rdx2_4;
    uint64_t rdx2_3;
    uint64_t var_133;
    uint64_t rbp_1;
    uint64_t r14_2;
    uint64_t r14_1;
    uint64_t *var_25;
    uint64_t var_26;
    uint64_t r93_1;
    uint64_t r86_4;
    uint64_t r86_2;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t rbx_0;
    uint64_t r13_5;
    uint64_t var_30;
    uint32_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_41;
    struct indirect_placeholder_79_ret_type var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_135;
    uint64_t var_136;
    uint64_t var_137;
    uint64_t var_138;
    uint64_t var_139;
    uint64_t r14_0;
    uint64_t rcx1_1;
    uint32_t state_0x8248_6;
    uint64_t state_0x9018_6;
    uint32_t state_0x9010_6;
    uint64_t rax_6;
    uint64_t state_0x82d8_6;
    uint32_t state_0x9080_6;
    uint64_t var_140;
    struct helper_divq_EAX_wrapper_ret_type var_141;
    uint64_t var_142;
    bool var_143;
    uint32_t var_144;
    uint64_t rdx2_5;
    uint64_t var_146;
    uint64_t r12_0;
    uint64_t rdx2_7;
    uint64_t r93_2;
    uint64_t r13_6;
    uint64_t local_sp_1;
    uint64_t r12_2;
    uint64_t rax_7;
    uint64_t var_147;
    uint64_t var_148;
    uint64_t var_149;
    uint64_t rcx1_3;
    uint64_t _pre_phi336;
    uint64_t var_150;
    uint64_t rcx1_2;
    uint64_t _pre335;
    uint64_t var_151;
    uint64_t var_152;
    uint64_t var_153;
    uint64_t rcx1_4;
    uint64_t var_154;
    uint64_t var_155;
    uint64_t rsi5_4;
    uint64_t r14_3;
    uint64_t r93_4;
    uint64_t r13_8;
    uint64_t local_sp_3;
    uint64_t rdx2_8;
    uint32_t *var_156;
    uint32_t var_157;
    uint64_t var_158;
    uint64_t var_159;
    uint64_t var_160;
    uint64_t r12_3;
    uint64_t var_161;
    uint64_t var_162;
    uint64_t r93_5;
    uint64_t r13_9;
    uint64_t rax_8;
    uint64_t local_sp_4;
    uint64_t r12_4;
    uint64_t r93_6;
    uint64_t r13_10;
    uint64_t local_sp_5;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_164;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r15();
    var_3 = init_r14();
    var_4 = init_r12();
    var_5 = init_rbx();
    var_6 = init_r13();
    var_7 = init_cc_src2();
    var_8 = init_state_0x8248();
    var_9 = init_state_0x9018();
    var_10 = init_state_0x9010();
    var_11 = init_state_0x8408();
    var_12 = init_state_0x8328();
    var_13 = init_state_0x82d8();
    var_14 = init_state_0x9080();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_6;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_1;
    *(uint64_t *)(var_0 + (-48L)) = var_5;
    var_15 = var_0 + (-120L);
    var_16 = (uint64_t *)var_15;
    *var_16 = r8;
    var_17 = (rdi << 2UL) + 8UL;
    var_18 = var_0 + (-112L);
    var_19 = (uint64_t *)var_18;
    *var_19 = r9;
    var_20 = (uint64_t *)(var_0 + (-104L));
    *var_20 = rcx;
    *(uint64_t *)(var_0 + (-128L)) = rdi;
    var_21 = var_0 + (-144L);
    var_22 = (uint64_t *)var_21;
    *var_22 = 4227230UL;
    var_23 = indirect_placeholder_78(var_17);
    var_24 = var_23.field_0;
    r15_0 = 4294967295UL;
    r13_0 = 4294967295UL;
    r12_5 = 0UL;
    rdx2_0 = 0UL;
    rax_0 = 0UL;
    local_sp_7 = var_21;
    rdx2_1 = var_24;
    rsi5_0 = 0UL;
    rax_1 = 0UL;
    rax_2 = 0U;
    r13_2 = 4294967295UL;
    rbp_0 = rcx;
    r93_0 = 0UL;
    r13_4 = 0UL;
    state_0x8248_0 = var_8;
    state_0x9018_0 = var_9;
    state_0x9010_0 = var_10;
    state_0x82d8_0 = var_13;
    state_0x9080_0 = var_14;
    r13_3 = 4294967295UL;
    rcx1_0 = 0UL;
    rax_4 = 0UL;
    rsi5_2 = 0UL;
    rax_5 = 0UL;
    rbp_1 = rcx;
    r14_2 = 1UL;
    r93_1 = 0UL;
    r86_4 = 0UL;
    rbx_0 = rdx;
    state_0x8248_6 = var_8;
    state_0x9018_6 = var_9;
    state_0x9010_6 = var_10;
    rax_6 = 0UL;
    state_0x82d8_6 = var_13;
    state_0x9080_6 = var_14;
    rdx2_5 = 1UL;
    r12_0 = var_24;
    rdx2_7 = 0UL;
    r93_2 = 0UL;
    local_sp_1 = var_21;
    rcx1_2 = 0UL;
    rcx1_4 = 0UL;
    r14_3 = 0UL;
    rdx2_8 = 0UL;
    if (var_24 == 0UL) {
        return r12_5;
    }
    var_25 = (uint64_t *)(var_0 + (-136L));
    var_26 = *var_25;
    var_27 = *var_19;
    r86_2 = var_26;
    if (var_26 == 0UL) {
        var_28 = r86_2 << 2UL;
        r93_1 = var_28;
        r86_4 = r86_2;
        while (*(uint32_t *)((var_28 + rsi) + (-4L)) != 0U)
            {
                var_29 = r86_2 + (-1L);
                r86_2 = var_29;
                r93_1 = 0UL;
                r86_4 = 0UL;
                if (var_29 == 0UL) {
                    break;
                }
                var_28 = r86_2 << 2UL;
                r93_1 = var_28;
                r86_4 = r86_2;
            }
    }
    rcx1_1 = r86_4;
    r14_0 = r86_4;
    if (rdx == 0UL) {
        *(uint64_t *)(local_sp_7 + (-8L)) = 4203650UL;
        indirect_placeholder();
        abort();
    }
    r13_5_in = rdx << 2UL;
    while (1U)
        {
            r13_5 = r13_5_in + (-4L);
            var_30 = r13_5 + var_27;
            var_31 = *(uint32_t *)var_30;
            rdx2_3 = rbx_0;
            rdx2_4 = rbx_0;
            r13_5_in = r13_5;
            rax_7 = rbx_0;
            if (var_31 == 0U) {
                var_164 = rbx_0 + (-1L);
                rbx_0 = var_164;
                if (var_164 == 0UL) {
                    continue;
                }
                loop_state_var = 0U;
                break;
            }
            var_32 = (uint64_t)var_31;
            var_33 = rbx_0 << 2UL;
            var_34 = helper_cc_compute_c_wrapper(r86_4 - rbx_0, rbx_0, var_7, 17U);
            if (var_34 == 0UL) {
                *var_19 = r86_4;
                *var_25 = r93_1;
                var_35 = var_0 + (-152L);
                *(uint64_t *)var_35 = 4228323UL;
                indirect_placeholder();
                var_36 = *var_22;
                var_37 = *var_16;
                var_38 = var_36 + var_24;
                r14_0 = 0UL;
                rdx2_7 = var_37;
                r13_6 = var_38;
                local_sp_1 = var_35;
                loop_state_var = 2U;
                break;
            }
            if (rbx_0 == 1UL) {
                var_135 = (uint64_t)*(uint32_t *)var_27;
                var_136 = var_24 + 4UL;
                rdx2_7 = 1UL;
                r13_6 = var_136;
                if (r86_4 != 0UL) {
                    loop_state_var = 3U;
                    break;
                }
                var_137 = r93_1 - (r86_4 << 2UL);
                var_138 = var_137 + rsi;
                var_139 = var_137 + var_136;
                loop_state_var = 4U;
                break;
            }
            var_39 = helper_clz_wrapper(var_32);
            var_40 = (uint64_t)(uint32_t)var_39 ^ 32UL;
            if (var_40 == 0UL) {
                *var_20 = var_27;
                *(uint32_t *)var_18 = (uint32_t)var_40;
                *var_25 = r86_4;
                var_41 = var_0 + (-152L);
                *(uint64_t *)var_41 = 4228454UL;
                var_42 = indirect_placeholder_79(var_33);
                var_43 = var_42.field_0;
                var_44 = *var_22;
                var_45 = *var_19;
                local_sp_7 = var_41;
                rbp_0 = var_43;
                r93_0 = var_43;
                r86_0 = var_44;
                local_sp_0 = var_41;
                if (var_43 != 0UL) {
                    var_46 = (uint64_t)*(uint32_t *)var_15 & 63UL;
                    loop_state_var = 6U;
                    break;
                }
                *(uint64_t *)(var_0 + (-160L)) = 4228591UL;
                indirect_placeholder();
                loop_state_var = 1U;
                break;
            }
            *var_20 = var_30;
            *var_19 = r86_4;
            *var_25 = r93_1;
            var_56 = var_0 + (-152L);
            *(uint64_t *)var_56 = 4227406UL;
            indirect_placeholder();
            var_57 = *var_22;
            var_58 = *var_16;
            var_59 = *var_19;
            *(uint32_t *)(var_57 + var_24) = 0U;
            r10_0 = var_59;
            r86_0 = var_58;
            local_sp_0 = var_56;
            loop_state_var = 5U;
            break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(uint64_t *)(local_sp_7 + (-8L)) = 4203650UL;
            indirect_placeholder();
            abort();
        }
        break;
      case 6U:
        {
            var_47 = rdx2_0 << 2UL;
            var_48 = rax_0 + ((uint64_t)*(uint32_t *)(var_47 + var_45) << var_46);
            *(uint32_t *)(var_47 + var_43) = (uint32_t)var_48;
            var_49 = rdx2_0 + 1UL;
            var_50 = var_48 >> 32UL;
            rdx2_0 = var_49;
            rax_0 = var_50;
            do {
                var_47 = rdx2_0 << 2UL;
                var_48 = rax_0 + ((uint64_t)*(uint32_t *)(var_47 + var_45) << var_46);
                *(uint32_t *)(var_47 + var_43) = (uint32_t)var_48;
                var_49 = rdx2_0 + 1UL;
                var_50 = var_48 >> 32UL;
                rdx2_0 = var_49;
                rax_0 = var_50;
            } while (rbx_0 != var_49);
            if (var_50 == 0UL) {
                *(uint64_t *)(local_sp_7 + (-8L)) = 4203650UL;
                indirect_placeholder();
                abort();
            }
            if (var_44 != 0UL) {
                var_51 = rsi5_0 << 2UL;
                var_52 = rax_1 + ((uint64_t)*(uint32_t *)(var_51 + rsi) << var_46);
                *(uint32_t *)(var_51 + var_24) = (uint32_t)var_52;
                var_53 = rsi5_0 + 1UL;
                var_54 = var_52 >> 32UL;
                rsi5_0 = var_53;
                rax_1 = var_54;
                do {
                    var_51 = rsi5_0 << 2UL;
                    var_52 = rax_1 + ((uint64_t)*(uint32_t *)(var_51 + rsi) << var_46);
                    *(uint32_t *)(var_51 + var_24) = (uint32_t)var_52;
                    var_53 = rsi5_0 + 1UL;
                    var_54 = var_52 >> 32UL;
                    rsi5_0 = var_53;
                    rax_1 = var_54;
                } while (var_44 != var_53);
                rdx2_1 = (var_44 << 2UL) + var_24;
                rax_2 = (uint32_t)var_54;
            }
            *(uint32_t *)rdx2_1 = rax_2;
            var_55 = r13_5 + var_43;
            r10_0 = var_55;
            var_60 = *(uint32_t *)r10_0;
            var_61 = (uint64_t)var_60;
            var_62 = (uint64_t)*(uint32_t *)((var_33 + rbp_0) + (-8L));
            var_63 = var_33 + var_24;
            var_64 = r86_0 - rbx_0;
            var_65 = var_63 + (-4L);
            var_66 = (uint64_t *)(local_sp_0 + 48UL);
            *var_66 = r93_0;
            var_67 = (r86_0 << 2UL) + var_24;
            var_68 = var_64 + 1UL;
            var_69 = (uint64_t *)(local_sp_0 + 40UL);
            *var_69 = var_64;
            var_70 = var_61 << 32UL;
            var_71 = (uint64_t *)(local_sp_0 + 64UL);
            *var_71 = var_68;
            var_72 = var_70 | var_62;
            var_73 = (uint64_t *)(local_sp_0 + 72UL);
            *var_73 = var_24;
            var_74 = (uint64_t *)(local_sp_0 + 8UL);
            *var_74 = var_72;
            var_75 = (uint64_t *)(local_sp_0 + 56UL);
            *var_75 = var_63;
            var_76 = 0UL - var_33;
            var_77 = (uint64_t *)(local_sp_0 + 32UL);
            *var_77 = var_76;
            var_78 = var_62 * 4294967295UL;
            rax_3 = var_78;
            rdi4_0 = var_62;
            rsi5_1 = var_61;
            r86_1 = var_67;
            rbp_1 = rbp_0;
            local_sp_1 = local_sp_0;
            local_sp_3 = local_sp_0;
            local_sp_4 = local_sp_0;
            while (1U)
                {
                    var_79 = (uint32_t *)r86_1;
                    var_80 = *var_79;
                    var_81 = (uint64_t)var_80;
                    state_0x8248_1 = state_0x8248_0;
                    state_0x9018_1 = state_0x9018_0;
                    state_0x9010_1 = state_0x9010_0;
                    state_0x82d8_1 = state_0x82d8_0;
                    state_0x9080_1 = state_0x9080_0;
                    rdi4_1 = rdi4_0;
                    state_0x8248_3 = state_0x8248_0;
                    state_0x9018_3 = state_0x9018_0;
                    state_0x9010_3 = state_0x9010_0;
                    state_0x82d8_3 = state_0x82d8_0;
                    state_0x9080_3 = state_0x9080_0;
                    state_0x8248_4 = state_0x8248_0;
                    state_0x9018_4 = state_0x9018_0;
                    state_0x9010_4 = state_0x9010_0;
                    state_0x82d8_4 = state_0x82d8_0;
                    state_0x9080_4 = state_0x9080_0;
                    rsi5_3 = rsi5_1;
                    if (var_60 > var_80) {
                        var_87 = (uint64_t)*(uint32_t *)(r86_1 + (-4L));
                        var_88 = var_81 << 32UL;
                        var_89 = (uint64_t)*(uint32_t *)(r86_1 + (-8L));
                        var_90 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), var_61, var_88 | var_87, var_89, 4227569UL, rbp_0, var_61, rbx_0, 0UL, var_65, rdi4_0, rsi5_1, r86_1, state_0x8248_0, state_0x9018_0, state_0x9010_0, var_11, var_12, state_0x82d8_0, state_0x9080_0);
                        var_91 = var_90.field_0;
                        var_92 = var_90.field_4;
                        var_93 = var_90.field_6;
                        var_94 = var_90.field_7;
                        var_95 = var_90.field_8;
                        var_96 = var_90.field_9;
                        var_97 = var_90.field_10;
                        var_98 = (uint64_t)(uint32_t)var_91;
                        var_99 = var_92 << 32UL;
                        var_100 = var_98 * var_62;
                        var_101 = var_99 | var_89;
                        var_102 = helper_cc_compute_c_wrapper(var_101 - var_100, var_100, var_7, 17U);
                        rdx2_2 = var_101;
                        r13_1 = var_98;
                        r13_0 = var_98;
                        state_0x8248_1 = var_93;
                        state_0x9018_1 = var_94;
                        state_0x9010_1 = var_95;
                        rax_3 = var_100;
                        state_0x82d8_1 = var_96;
                        state_0x9080_1 = var_97;
                        state_0x8248_2 = var_93;
                        state_0x9018_2 = var_94;
                        state_0x9010_2 = var_95;
                        state_0x82d8_2 = var_96;
                        state_0x9080_2 = var_97;
                        if (var_102 == 0UL) {
                            var_103 = rax_3 - rdx2_2;
                            var_104 = helper_cc_compute_c_wrapper(*var_74 - var_103, var_103, var_7, 17U);
                            r13_1 = (uint64_t)(((uint32_t)r13_0 + ((uint32_t)(uint64_t)(unsigned char)var_104 ^ 1U)) + (-2));
                            state_0x8248_2 = state_0x8248_1;
                            state_0x9018_2 = state_0x9018_1;
                            state_0x9010_2 = state_0x9010_1;
                            state_0x82d8_2 = state_0x82d8_1;
                            state_0x9080_2 = state_0x9080_1;
                        }
                        r13_2 = r13_1;
                        state_0x8248_5 = state_0x8248_2;
                        state_0x9018_5 = state_0x9018_2;
                        state_0x9010_5 = state_0x9010_2;
                        state_0x82d8_5 = state_0x82d8_2;
                        state_0x9080_5 = state_0x9080_2;
                        state_0x8248_3 = state_0x8248_2;
                        state_0x9018_3 = state_0x9018_2;
                        state_0x9010_3 = state_0x9010_2;
                        state_0x82d8_3 = state_0x82d8_2;
                        state_0x9080_3 = state_0x9080_2;
                        if (r13_1 != 0UL) {
                            *var_79 = (uint32_t)r13_4;
                            rdi4_0 = rdi4_1;
                            rsi5_1 = rsi5_3;
                            state_0x8248_0 = state_0x8248_5;
                            state_0x9018_0 = state_0x9018_5;
                            state_0x9010_0 = state_0x9010_5;
                            state_0x82d8_0 = state_0x82d8_5;
                            state_0x9080_0 = state_0x9080_5;
                            if (r86_1 == var_63) {
                                break;
                            }
                            r86_1 = r86_1 + (-4L);
                            continue;
                        }
                        r15_0 = (uint64_t)(uint32_t)r13_2;
                        r13_3 = r13_2;
                        state_0x8248_4 = state_0x8248_3;
                        state_0x9018_4 = state_0x9018_3;
                        state_0x9010_4 = state_0x9010_3;
                        state_0x82d8_4 = state_0x82d8_3;
                        state_0x9080_4 = state_0x9080_3;
                    } else {
                        var_82 = helper_cc_compute_c_wrapper(var_61 - var_81, var_81, var_7, 16U);
                        var_83 = (uint64_t)*(uint32_t *)(r86_1 + (-4L));
                        var_84 = var_61 + var_83;
                        var_85 = helper_cc_compute_c_wrapper(var_84, var_83, var_7, 8U);
                        if (var_82 != 0UL & var_85 != 0UL) {
                            var_86 = (var_84 << 32UL) | (uint64_t)*(uint32_t *)(r86_1 + (-8L));
                            rdx2_2 = var_86;
                            if (var_78 <= var_86) {
                                var_103 = rax_3 - rdx2_2;
                                var_104 = helper_cc_compute_c_wrapper(*var_74 - var_103, var_103, var_7, 17U);
                                r13_1 = (uint64_t)(((uint32_t)r13_0 + ((uint32_t)(uint64_t)(unsigned char)var_104 ^ 1U)) + (-2));
                                state_0x8248_2 = state_0x8248_1;
                                state_0x9018_2 = state_0x9018_1;
                                state_0x9010_2 = state_0x9010_1;
                                state_0x82d8_2 = state_0x82d8_1;
                                state_0x9080_2 = state_0x9080_1;
                                r13_2 = r13_1;
                                state_0x8248_5 = state_0x8248_2;
                                state_0x9018_5 = state_0x9018_2;
                                state_0x9010_5 = state_0x9010_2;
                                state_0x82d8_5 = state_0x82d8_2;
                                state_0x9080_5 = state_0x9080_2;
                                state_0x8248_3 = state_0x8248_2;
                                state_0x9018_3 = state_0x9018_2;
                                state_0x9010_3 = state_0x9010_2;
                                state_0x82d8_3 = state_0x82d8_2;
                                state_0x9080_3 = state_0x9080_2;
                                if (r13_1 == 0UL) {
                                    *var_79 = (uint32_t)r13_4;
                                    rdi4_0 = rdi4_1;
                                    rsi5_1 = rsi5_3;
                                    state_0x8248_0 = state_0x8248_5;
                                    state_0x9018_0 = state_0x9018_5;
                                    state_0x9010_0 = state_0x9010_5;
                                    state_0x82d8_0 = state_0x82d8_5;
                                    state_0x9080_0 = state_0x9080_5;
                                    if (r86_1 == var_63) {
                                        break;
                                    }
                                    r86_1 = r86_1 + (-4L);
                                    continue;
                                }
                            }
                            r15_0 = (uint64_t)(uint32_t)r13_2;
                            r13_3 = r13_2;
                            state_0x8248_4 = state_0x8248_3;
                            state_0x9018_4 = state_0x9018_3;
                            state_0x9010_4 = state_0x9010_3;
                            state_0x82d8_4 = state_0x82d8_3;
                            state_0x9080_4 = state_0x9080_3;
                        }
                    }
                }
            var_127 = *var_71;
            var_128 = *var_75;
            var_129 = *var_73;
            var_130 = *var_66;
            var_131 = *(uint32_t *)((var_33 + var_129) + (-4L));
            var_132 = (*(uint32_t *)(((var_127 << 2UL) + var_128) + (-4L)) == 0U) ? *var_69 : var_127;
            r14_1 = var_132;
            r14_0 = var_132;
            r12_0 = var_129;
            r93_2 = var_130;
            r13_6 = var_128;
            r12_2 = var_129;
            r93_4 = var_130;
            r13_8 = var_128;
            r12_3 = var_129;
            r93_5 = var_130;
            r13_9 = var_128;
            rax_8 = var_128;
            if (var_131 == 0U) {
                while (1U)
                    {
                        var_133 = rdx2_3 + (-1L);
                        rdx2_3 = var_133;
                        rdx2_4 = var_133;
                        if (var_133 != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        if (*(uint32_t *)(((var_133 << 2UL) + var_129) + (-4L)) == 0U) {
                            continue;
                        }
                        loop_state_var = 1U;
                        break;
                    }
                var_134 = helper_cc_compute_c_wrapper(rbx_0 - rdx2_4, rdx2_4, var_7, 17U);
                rdx2_7 = rdx2_4;
                if (var_134 != 0UL) {
                    if (var_132 != 0UL) {
                        *(uint32_t *)rax_8 = 1U;
                        r14_3 = r14_2;
                        r12_4 = r12_3;
                        r93_6 = r93_5;
                        r13_10 = r13_9;
                        local_sp_5 = local_sp_4;
                        local_sp_6 = local_sp_5;
                        r12_5 = r12_4;
                        if (r93_6 == 0UL) {
                            var_163 = local_sp_5 + (-8L);
                            *(uint64_t *)var_163 = 4227868UL;
                            indirect_placeholder();
                            local_sp_6 = var_163;
                        }
                        **(uint64_t **)(local_sp_6 + 24UL) = r13_10;
                        **(uint64_t **)(local_sp_6 + 16UL) = r14_3;
                        return r12_5;
                    }
                    r14_3 = r14_1;
                    r12_3 = r12_2;
                    r93_5 = r93_4;
                    r13_9 = r13_8;
                    local_sp_4 = local_sp_3;
                    r12_4 = r12_2;
                    r93_6 = r93_4;
                    r13_10 = r13_8;
                    local_sp_5 = local_sp_3;
                    while (1U)
                        {
                            var_156 = (uint32_t *)((rdx2_8 << 2UL) + r13_8);
                            var_157 = *var_156 + 1U;
                            var_158 = (uint64_t)var_157;
                            *var_156 = var_157;
                            if (var_158 != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            var_159 = rdx2_8 + 1UL;
                            var_160 = helper_cc_compute_c_wrapper(var_159 - r14_1, r14_1, var_7, 17U);
                            rdx2_8 = var_159;
                            if (var_160 == 0UL) {
                                continue;
                            }
                            var_161 = (r14_1 << 2UL) + r13_8;
                            var_162 = r14_1 + 1UL;
                            r14_2 = var_162;
                            rax_8 = var_161;
                            loop_state_var = 1U;
                            break;
                        }
                    switch (loop_state_var) {
                      case 1U:
                        {
                            *(uint32_t *)rax_8 = 1U;
                            r14_3 = r14_2;
                            r12_4 = r12_3;
                            r93_6 = r93_5;
                            r13_10 = r13_9;
                            local_sp_5 = local_sp_4;
                        }
                        break;
                      case 0U:
                        {
                            local_sp_6 = local_sp_5;
                            r12_5 = r12_4;
                            if (r93_6 == 0UL) {
                                var_163 = local_sp_5 + (-8L);
                                *(uint64_t *)var_163 = 4227868UL;
                                indirect_placeholder();
                                local_sp_6 = var_163;
                            }
                            **(uint64_t **)(local_sp_6 + 24UL) = r13_10;
                            **(uint64_t **)(local_sp_6 + 16UL) = r14_3;
                            return r12_5;
                        }
                        break;
                    }
                }
            } else {
                var_134 = helper_cc_compute_c_wrapper(rbx_0 - rdx2_4, rdx2_4, var_7, 17U);
                rdx2_7 = rdx2_4;
                if (var_134 == 0UL) {
                    r14_1 = r14_0;
                    r12_2 = r12_0;
                    r93_4 = r93_2;
                    r13_8 = r13_6;
                    local_sp_3 = local_sp_1;
                    r12_3 = r12_0;
                    r93_5 = r93_2;
                    r13_9 = r13_6;
                    rax_8 = r13_6;
                    local_sp_4 = local_sp_1;
                    r12_4 = r12_0;
                    r93_6 = r93_2;
                    r13_10 = r13_6;
                    local_sp_5 = local_sp_1;
                    while (1U)
                        {
                            if (rax_7 > rdx2_7) {
                                var_154 = helper_cc_compute_c_wrapper(rax_7 - rbx_0, rbx_0, var_7, 17U);
                                if (var_154 != 0UL) {
                                    var_155 = (uint64_t)*(uint32_t *)((rax_7 << 2UL) + rbp_1);
                                    rsi5_4 = var_155;
                                    r14_3 = r14_0;
                                    if (rsi5_4 <= (uint64_t)(uint32_t)rcx1_4) {
                                        loop_state_var = 2U;
                                        break;
                                    }
                                }
                            }
                            if (rax_7 == 0UL) {
                                var_150 = helper_cc_compute_c_wrapper(0UL - rdx2_7, rdx2_7, var_7, 17U);
                                if (var_150 != 0UL) {
                                    var_154 = helper_cc_compute_c_wrapper(rax_7 - rbx_0, rbx_0, var_7, 17U);
                                    if (var_154 != 0UL) {
                                        var_155 = (uint64_t)*(uint32_t *)((rax_7 << 2UL) + rbp_1);
                                        rsi5_4 = var_155;
                                        r14_3 = r14_0;
                                        if (rsi5_4 <= (uint64_t)(uint32_t)rcx1_4) {
                                            loop_state_var = 2U;
                                            break;
                                        }
                                    }
                                    if (rax_7 == 0UL) {
                                        rax_7 = rax_7 + (-1L);
                                        continue;
                                    }
                                    if (r14_0 != 0UL) {
                                        loop_state_var = 2U;
                                        break;
                                    }
                                    r14_3 = r14_0;
                                    if ((*(unsigned char *)r13_6 & '\x01') != '\x00') {
                                        loop_state_var = 2U;
                                        break;
                                    }
                                    loop_state_var = 0U;
                                    break;
                                }
                                _pre335 = 0UL + r12_0;
                                _pre_phi336 = _pre335;
                                rcx1_3 = rcx1_2 | (uint64_t)((uint32_t)((uint64_t)*(uint32_t *)_pre_phi336 << 1UL) & (-2));
                            } else {
                                var_147 = (rax_7 << 2UL) + r12_0;
                                var_148 = (uint64_t)(*(uint32_t *)(var_147 + (-4L)) >> 31U);
                                var_149 = helper_cc_compute_c_wrapper(rax_7 - rdx2_7, rdx2_7, var_7, 17U);
                                _pre_phi336 = var_147;
                                rcx1_2 = var_148;
                                rcx1_3 = var_148;
                                if (var_149 == 0UL) {
                                    rcx1_3 = rcx1_2 | (uint64_t)((uint32_t)((uint64_t)*(uint32_t *)_pre_phi336 << 1UL) & (-2));
                                }
                            }
                            var_151 = helper_cc_compute_c_wrapper(rax_7 - rbx_0, rbx_0, var_7, 17U);
                            rcx1_4 = rcx1_3;
                            if (var_151 == 0UL) {
                                if ((uint64_t)(uint32_t)rcx1_3 == 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                            }
                            var_152 = (uint64_t)*(uint32_t *)((rax_7 << 2UL) + rbp_1);
                            var_153 = helper_cc_compute_c_wrapper(var_152 - rcx1_3, rcx1_3, var_7, 16U);
                            rsi5_4 = var_152;
                            if (var_153 == 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            r14_3 = r14_0;
                            if (rsi5_4 <= (uint64_t)(uint32_t)rcx1_4) {
                                loop_state_var = 2U;
                                break;
                            }
                            if (rax_7 == 0UL) {
                                rax_7 = rax_7 + (-1L);
                                continue;
                            }
                            if (r14_0 != 0UL) {
                                loop_state_var = 2U;
                                break;
                            }
                            r14_3 = r14_0;
                            if ((*(unsigned char *)r13_6 & '\x01') != '\x00') {
                                loop_state_var = 2U;
                                break;
                            }
                            loop_state_var = 0U;
                            break;
                        }
                    switch (loop_state_var) {
                      case 2U:
                        {
                            local_sp_6 = local_sp_5;
                            r12_5 = r12_4;
                            if (r93_6 == 0UL) {
                                var_163 = local_sp_5 + (-8L);
                                *(uint64_t *)var_163 = 4227868UL;
                                indirect_placeholder();
                                local_sp_6 = var_163;
                            }
                            **(uint64_t **)(local_sp_6 + 24UL) = r13_10;
                            **(uint64_t **)(local_sp_6 + 16UL) = r14_3;
                            return r12_5;
                        }
                        break;
                      case 1U:
                        {
                            if (r14_0 == 0UL) {
                                *(uint32_t *)rax_8 = 1U;
                                r14_3 = r14_2;
                                r12_4 = r12_3;
                                r93_6 = r93_5;
                                r13_10 = r13_9;
                                local_sp_5 = local_sp_4;
                                local_sp_6 = local_sp_5;
                                r12_5 = r12_4;
                                if (r93_6 == 0UL) {
                                    var_163 = local_sp_5 + (-8L);
                                    *(uint64_t *)var_163 = 4227868UL;
                                    indirect_placeholder();
                                    local_sp_6 = var_163;
                                }
                                **(uint64_t **)(local_sp_6 + 24UL) = r13_10;
                                **(uint64_t **)(local_sp_6 + 16UL) = r14_3;
                                return r12_5;
                            }
                            r14_3 = r14_1;
                            r12_3 = r12_2;
                            r93_5 = r93_4;
                            r13_9 = r13_8;
                            local_sp_4 = local_sp_3;
                            r12_4 = r12_2;
                            r93_6 = r93_4;
                            r13_10 = r13_8;
                            local_sp_5 = local_sp_3;
                            while (1U)
                                {
                                    var_156 = (uint32_t *)((rdx2_8 << 2UL) + r13_8);
                                    var_157 = *var_156 + 1U;
                                    var_158 = (uint64_t)var_157;
                                    *var_156 = var_157;
                                    if (var_158 != 0UL) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    var_159 = rdx2_8 + 1UL;
                                    var_160 = helper_cc_compute_c_wrapper(var_159 - r14_1, r14_1, var_7, 17U);
                                    rdx2_8 = var_159;
                                    if (var_160 == 0UL) {
                                        continue;
                                    }
                                    var_161 = (r14_1 << 2UL) + r13_8;
                                    var_162 = r14_1 + 1UL;
                                    r14_2 = var_162;
                                    rax_8 = var_161;
                                    loop_state_var = 1U;
                                    break;
                                }
                            *(uint32_t *)rax_8 = 1U;
                            r14_3 = r14_2;
                            r12_4 = r12_3;
                            r93_6 = r93_5;
                            r13_10 = r13_9;
                            local_sp_5 = local_sp_4;
                        }
                        break;
                      case 0U:
                        {
                            r14_3 = r14_1;
                            r12_3 = r12_2;
                            r93_5 = r93_4;
                            r13_9 = r13_8;
                            local_sp_4 = local_sp_3;
                            r12_4 = r12_2;
                            r93_6 = r93_4;
                            r13_10 = r13_8;
                            local_sp_5 = local_sp_3;
                            while (1U)
                                {
                                    var_156 = (uint32_t *)((rdx2_8 << 2UL) + r13_8);
                                    var_157 = *var_156 + 1U;
                                    var_158 = (uint64_t)var_157;
                                    *var_156 = var_157;
                                    if (var_158 != 0UL) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    var_159 = rdx2_8 + 1UL;
                                    var_160 = helper_cc_compute_c_wrapper(var_159 - r14_1, r14_1, var_7, 17U);
                                    rdx2_8 = var_159;
                                    if (var_160 == 0UL) {
                                        continue;
                                    }
                                    var_161 = (r14_1 << 2UL) + r13_8;
                                    var_162 = r14_1 + 1UL;
                                    r14_2 = var_162;
                                    rax_8 = var_161;
                                    loop_state_var = 1U;
                                    break;
                                }
                            *(uint32_t *)rax_8 = 1U;
                            r14_3 = r14_2;
                            r12_4 = r12_3;
                            r93_6 = r93_5;
                            r13_10 = r13_9;
                            local_sp_5 = local_sp_4;
                        }
                        break;
                    }
                } else {
                    if (var_132 == 0UL) {
                        *(uint32_t *)rax_8 = 1U;
                        r14_3 = r14_2;
                        r12_4 = r12_3;
                        r93_6 = r93_5;
                        r13_10 = r13_9;
                        local_sp_5 = local_sp_4;
                        local_sp_6 = local_sp_5;
                        r12_5 = r12_4;
                        if (r93_6 == 0UL) {
                            var_163 = local_sp_5 + (-8L);
                            *(uint64_t *)var_163 = 4227868UL;
                            indirect_placeholder();
                            local_sp_6 = var_163;
                        }
                        **(uint64_t **)(local_sp_6 + 24UL) = r13_10;
                        **(uint64_t **)(local_sp_6 + 16UL) = r14_3;
                        return r12_5;
                    }
                    r14_3 = r14_1;
                    r12_3 = r12_2;
                    r93_5 = r93_4;
                    r13_9 = r13_8;
                    local_sp_4 = local_sp_3;
                    r12_4 = r12_2;
                    r93_6 = r93_4;
                    r13_10 = r13_8;
                    local_sp_5 = local_sp_3;
                    while (1U)
                        {
                            var_156 = (uint32_t *)((rdx2_8 << 2UL) + r13_8);
                            var_157 = *var_156 + 1U;
                            var_158 = (uint64_t)var_157;
                            *var_156 = var_157;
                            if (var_158 != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            var_159 = rdx2_8 + 1UL;
                            var_160 = helper_cc_compute_c_wrapper(var_159 - r14_1, r14_1, var_7, 17U);
                            rdx2_8 = var_159;
                            if (var_160 == 0UL) {
                                continue;
                            }
                            var_161 = (r14_1 << 2UL) + r13_8;
                            var_162 = r14_1 + 1UL;
                            r14_2 = var_162;
                            rax_8 = var_161;
                            loop_state_var = 1U;
                            break;
                        }
                    switch (loop_state_var) {
                      case 1U:
                        {
                            *(uint32_t *)rax_8 = 1U;
                            r14_3 = r14_2;
                            r12_4 = r12_3;
                            r93_6 = r93_5;
                            r13_10 = r13_9;
                            local_sp_5 = local_sp_4;
                        }
                        break;
                      case 0U:
                        {
                            local_sp_6 = local_sp_5;
                            r12_5 = r12_4;
                            if (r93_6 == 0UL) {
                                var_163 = local_sp_5 + (-8L);
                                *(uint64_t *)var_163 = 4227868UL;
                                indirect_placeholder();
                                local_sp_6 = var_163;
                            }
                            **(uint64_t **)(local_sp_6 + 24UL) = r13_10;
                            **(uint64_t **)(local_sp_6 + 16UL) = r14_3;
                            return r12_5;
                        }
                        break;
                    }
                }
            }
        }
        break;
      case 5U:
      case 4U:
      case 3U:
      case 2U:
      case 1U:
        {
            switch (loop_state_var) {
              case 1U:
                {
                    return r12_5;
                }
                break;
              case 5U:
              case 4U:
              case 3U:
              case 2U:
                {
                    switch (loop_state_var) {
                      case 4U:
                      case 3U:
                      case 2U:
                        {
                            switch (loop_state_var) {
                              case 2U:
                                {
                                    r14_1 = r14_0;
                                    r12_2 = r12_0;
                                    r93_4 = r93_2;
                                    r13_8 = r13_6;
                                    local_sp_3 = local_sp_1;
                                    r12_3 = r12_0;
                                    r93_5 = r93_2;
                                    r13_9 = r13_6;
                                    rax_8 = r13_6;
                                    local_sp_4 = local_sp_1;
                                    r12_4 = r12_0;
                                    r93_6 = r93_2;
                                    r13_10 = r13_6;
                                    local_sp_5 = local_sp_1;
                                    while (1U)
                                        {
                                            if (rax_7 > rdx2_7) {
                                                var_154 = helper_cc_compute_c_wrapper(rax_7 - rbx_0, rbx_0, var_7, 17U);
                                                if (var_154 != 0UL) {
                                                    var_155 = (uint64_t)*(uint32_t *)((rax_7 << 2UL) + rbp_1);
                                                    rsi5_4 = var_155;
                                                    r14_3 = r14_0;
                                                    if (rsi5_4 <= (uint64_t)(uint32_t)rcx1_4) {
                                                        loop_state_var = 2U;
                                                        break;
                                                    }
                                                }
                                            }
                                            if (rax_7 == 0UL) {
                                                var_150 = helper_cc_compute_c_wrapper(0UL - rdx2_7, rdx2_7, var_7, 17U);
                                                if (var_150 != 0UL) {
                                                    var_154 = helper_cc_compute_c_wrapper(rax_7 - rbx_0, rbx_0, var_7, 17U);
                                                    if (var_154 != 0UL) {
                                                        var_155 = (uint64_t)*(uint32_t *)((rax_7 << 2UL) + rbp_1);
                                                        rsi5_4 = var_155;
                                                        r14_3 = r14_0;
                                                        if (rsi5_4 <= (uint64_t)(uint32_t)rcx1_4) {
                                                            loop_state_var = 2U;
                                                            break;
                                                        }
                                                    }
                                                    if (rax_7 == 0UL) {
                                                        rax_7 = rax_7 + (-1L);
                                                        continue;
                                                    }
                                                    if (r14_0 != 0UL) {
                                                        loop_state_var = 2U;
                                                        break;
                                                    }
                                                    r14_3 = r14_0;
                                                    if ((*(unsigned char *)r13_6 & '\x01') != '\x00') {
                                                        loop_state_var = 2U;
                                                        break;
                                                    }
                                                    loop_state_var = 0U;
                                                    break;
                                                }
                                                _pre335 = 0UL + r12_0;
                                                _pre_phi336 = _pre335;
                                                rcx1_3 = rcx1_2 | (uint64_t)((uint32_t)((uint64_t)*(uint32_t *)_pre_phi336 << 1UL) & (-2));
                                            } else {
                                                var_147 = (rax_7 << 2UL) + r12_0;
                                                var_148 = (uint64_t)(*(uint32_t *)(var_147 + (-4L)) >> 31U);
                                                var_149 = helper_cc_compute_c_wrapper(rax_7 - rdx2_7, rdx2_7, var_7, 17U);
                                                _pre_phi336 = var_147;
                                                rcx1_2 = var_148;
                                                rcx1_3 = var_148;
                                                if (var_149 == 0UL) {
                                                    rcx1_3 = rcx1_2 | (uint64_t)((uint32_t)((uint64_t)*(uint32_t *)_pre_phi336 << 1UL) & (-2));
                                                }
                                            }
                                            var_151 = helper_cc_compute_c_wrapper(rax_7 - rbx_0, rbx_0, var_7, 17U);
                                            rcx1_4 = rcx1_3;
                                            if (var_151 == 0UL) {
                                                if ((uint64_t)(uint32_t)rcx1_3 == 0UL) {
                                                    loop_state_var = 1U;
                                                    break;
                                                }
                                            }
                                            var_152 = (uint64_t)*(uint32_t *)((rax_7 << 2UL) + rbp_1);
                                            var_153 = helper_cc_compute_c_wrapper(var_152 - rcx1_3, rcx1_3, var_7, 16U);
                                            rsi5_4 = var_152;
                                            if (var_153 == 0UL) {
                                                loop_state_var = 1U;
                                                break;
                                            }
                                            r14_3 = r14_0;
                                            if (rsi5_4 <= (uint64_t)(uint32_t)rcx1_4) {
                                                loop_state_var = 2U;
                                                break;
                                            }
                                            if (rax_7 == 0UL) {
                                                rax_7 = rax_7 + (-1L);
                                                continue;
                                            }
                                            if (r14_0 != 0UL) {
                                                loop_state_var = 2U;
                                                break;
                                            }
                                            r14_3 = r14_0;
                                            if ((*(unsigned char *)r13_6 & '\x01') != '\x00') {
                                                loop_state_var = 2U;
                                                break;
                                            }
                                            loop_state_var = 0U;
                                            break;
                                        }
                                    switch (loop_state_var) {
                                      case 2U:
                                        {
                                            local_sp_6 = local_sp_5;
                                            r12_5 = r12_4;
                                            if (r93_6 == 0UL) {
                                                var_163 = local_sp_5 + (-8L);
                                                *(uint64_t *)var_163 = 4227868UL;
                                                indirect_placeholder();
                                                local_sp_6 = var_163;
                                            }
                                            **(uint64_t **)(local_sp_6 + 24UL) = r13_10;
                                            **(uint64_t **)(local_sp_6 + 16UL) = r14_3;
                                            return r12_5;
                                        }
                                        break;
                                      case 0U:
                                        {
                                            r14_3 = r14_1;
                                            r12_3 = r12_2;
                                            r93_5 = r93_4;
                                            r13_9 = r13_8;
                                            local_sp_4 = local_sp_3;
                                            r12_4 = r12_2;
                                            r93_6 = r93_4;
                                            r13_10 = r13_8;
                                            local_sp_5 = local_sp_3;
                                            while (1U)
                                                {
                                                    var_156 = (uint32_t *)((rdx2_8 << 2UL) + r13_8);
                                                    var_157 = *var_156 + 1U;
                                                    var_158 = (uint64_t)var_157;
                                                    *var_156 = var_157;
                                                    if (var_158 != 0UL) {
                                                        loop_state_var = 0U;
                                                        break;
                                                    }
                                                    var_159 = rdx2_8 + 1UL;
                                                    var_160 = helper_cc_compute_c_wrapper(var_159 - r14_1, r14_1, var_7, 17U);
                                                    rdx2_8 = var_159;
                                                    if (var_160 == 0UL) {
                                                        continue;
                                                    }
                                                    var_161 = (r14_1 << 2UL) + r13_8;
                                                    var_162 = r14_1 + 1UL;
                                                    r14_2 = var_162;
                                                    rax_8 = var_161;
                                                    loop_state_var = 1U;
                                                    break;
                                                }
                                            *(uint32_t *)rax_8 = 1U;
                                            r14_3 = r14_2;
                                            r12_4 = r12_3;
                                            r93_6 = r93_5;
                                            r13_10 = r13_9;
                                            local_sp_5 = local_sp_4;
                                        }
                                        break;
                                      case 1U:
                                        {
                                            if (r14_0 == 0UL) {
                                                *(uint32_t *)rax_8 = 1U;
                                                r14_3 = r14_2;
                                                r12_4 = r12_3;
                                                r93_6 = r93_5;
                                                r13_10 = r13_9;
                                                local_sp_5 = local_sp_4;
                                                local_sp_6 = local_sp_5;
                                                r12_5 = r12_4;
                                                if (r93_6 == 0UL) {
                                                    var_163 = local_sp_5 + (-8L);
                                                    *(uint64_t *)var_163 = 4227868UL;
                                                    indirect_placeholder();
                                                    local_sp_6 = var_163;
                                                }
                                                **(uint64_t **)(local_sp_6 + 24UL) = r13_10;
                                                **(uint64_t **)(local_sp_6 + 16UL) = r14_3;
                                                return r12_5;
                                            }
                                            r14_3 = r14_1;
                                            r12_3 = r12_2;
                                            r93_5 = r93_4;
                                            r13_9 = r13_8;
                                            local_sp_4 = local_sp_3;
                                            r12_4 = r12_2;
                                            r93_6 = r93_4;
                                            r13_10 = r13_8;
                                            local_sp_5 = local_sp_3;
                                            while (1U)
                                                {
                                                    var_156 = (uint32_t *)((rdx2_8 << 2UL) + r13_8);
                                                    var_157 = *var_156 + 1U;
                                                    var_158 = (uint64_t)var_157;
                                                    *var_156 = var_157;
                                                    if (var_158 != 0UL) {
                                                        loop_state_var = 0U;
                                                        break;
                                                    }
                                                    var_159 = rdx2_8 + 1UL;
                                                    var_160 = helper_cc_compute_c_wrapper(var_159 - r14_1, r14_1, var_7, 17U);
                                                    rdx2_8 = var_159;
                                                    if (var_160 == 0UL) {
                                                        continue;
                                                    }
                                                    var_161 = (r14_1 << 2UL) + r13_8;
                                                    var_162 = r14_1 + 1UL;
                                                    r14_2 = var_162;
                                                    rax_8 = var_161;
                                                    loop_state_var = 1U;
                                                    break;
                                                }
                                            *(uint32_t *)rax_8 = 1U;
                                            r14_3 = r14_2;
                                            r12_4 = r12_3;
                                            r93_6 = r93_5;
                                            r13_10 = r13_9;
                                            local_sp_5 = local_sp_4;
                                        }
                                        break;
                                    }
                                }
                                break;
                              case 4U:
                                {
                                    var_140 = rcx1_1 << 2UL;
                                    var_141 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), var_135, (rax_6 << 32UL) | (uint64_t)*(uint32_t *)((var_140 + var_138) + (-4L)), rcx1_1, 4227958UL, rcx, var_30, 1UL, 0UL, r93_1, var_139, var_135, r86_4, state_0x8248_6, state_0x9018_6, state_0x9010_6, var_11, var_12, state_0x82d8_6, state_0x9080_6);
                                    *(uint32_t *)((var_140 + var_139) + (-4L)) = (uint32_t)var_141.field_0;
                                    var_142 = rcx1_1 + (-1L);
                                    var_143 = (var_142 == 0UL);
                                    var_144 = (uint32_t)var_141.field_4;
                                    var_145 = (uint64_t)var_144;
                                    rcx1_1 = var_142;
                                    rax_6 = var_145;
                                    while (!var_143)
                                        {
                                            state_0x8248_6 = var_141.field_6;
                                            state_0x9018_6 = var_141.field_7;
                                            state_0x9010_6 = var_141.field_8;
                                            state_0x82d8_6 = var_141.field_9;
                                            state_0x9080_6 = var_141.field_10;
                                            var_140 = rcx1_1 << 2UL;
                                            var_141 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), var_135, (rax_6 << 32UL) | (uint64_t)*(uint32_t *)((var_140 + var_138) + (-4L)), rcx1_1, 4227958UL, rcx, var_30, 1UL, 0UL, r93_1, var_139, var_135, r86_4, state_0x8248_6, state_0x9018_6, state_0x9010_6, var_11, var_12, state_0x82d8_6, state_0x9080_6);
                                            *(uint32_t *)((var_140 + var_139) + (-4L)) = (uint32_t)var_141.field_0;
                                            var_142 = rcx1_1 + (-1L);
                                            var_143 = (var_142 == 0UL);
                                            var_144 = (uint32_t)var_141.field_4;
                                            var_145 = (uint64_t)var_144;
                                            rcx1_1 = var_142;
                                            rax_6 = var_145;
                                        }
                                    if (var_145 == 0UL) {
                                        rdx2_5 = 0UL;
                                        if (*(uint32_t *)(r93_1 + var_24) == 0U) {
                                            var_146 = r86_4 + (-1L);
                                            r14_0 = var_146;
                                            rdx2_7 = rdx2_5;
                                        }
                                    } else {
                                        *(uint32_t *)var_24 = var_144;
                                        if (*(uint32_t *)(r93_1 + var_24) == 0U) {
                                            var_146 = r86_4 + (-1L);
                                            r14_0 = var_146;
                                            rdx2_7 = rdx2_5;
                                        }
                                    }
                                }
                                break;
                              case 3U:
                                {
                                    rdx2_5 = 0UL;
                                    if (*(uint32_t *)(r93_1 + var_24) == 0U) {
                                        var_146 = r86_4 + (-1L);
                                        r14_0 = var_146;
                                        rdx2_7 = rdx2_5;
                                    }
                                }
                                break;
                            }
                        }
                        break;
                      case 5U:
                        {
                            var_60 = *(uint32_t *)r10_0;
                            var_61 = (uint64_t)var_60;
                            var_62 = (uint64_t)*(uint32_t *)((var_33 + rbp_0) + (-8L));
                            var_63 = var_33 + var_24;
                            var_64 = r86_0 - rbx_0;
                            var_65 = var_63 + (-4L);
                            var_66 = (uint64_t *)(local_sp_0 + 48UL);
                            *var_66 = r93_0;
                            var_67 = (r86_0 << 2UL) + var_24;
                            var_68 = var_64 + 1UL;
                            var_69 = (uint64_t *)(local_sp_0 + 40UL);
                            *var_69 = var_64;
                            var_70 = var_61 << 32UL;
                            var_71 = (uint64_t *)(local_sp_0 + 64UL);
                            *var_71 = var_68;
                            var_72 = var_70 | var_62;
                            var_73 = (uint64_t *)(local_sp_0 + 72UL);
                            *var_73 = var_24;
                            var_74 = (uint64_t *)(local_sp_0 + 8UL);
                            *var_74 = var_72;
                            var_75 = (uint64_t *)(local_sp_0 + 56UL);
                            *var_75 = var_63;
                            var_76 = 0UL - var_33;
                            var_77 = (uint64_t *)(local_sp_0 + 32UL);
                            *var_77 = var_76;
                            var_78 = var_62 * 4294967295UL;
                            rax_3 = var_78;
                            rdi4_0 = var_62;
                            rsi5_1 = var_61;
                            r86_1 = var_67;
                            rbp_1 = rbp_0;
                            local_sp_1 = local_sp_0;
                            local_sp_3 = local_sp_0;
                            local_sp_4 = local_sp_0;
                            while (1U)
                                {
                                    var_79 = (uint32_t *)r86_1;
                                    var_80 = *var_79;
                                    var_81 = (uint64_t)var_80;
                                    state_0x8248_1 = state_0x8248_0;
                                    state_0x9018_1 = state_0x9018_0;
                                    state_0x9010_1 = state_0x9010_0;
                                    state_0x82d8_1 = state_0x82d8_0;
                                    state_0x9080_1 = state_0x9080_0;
                                    rdi4_1 = rdi4_0;
                                    state_0x8248_3 = state_0x8248_0;
                                    state_0x9018_3 = state_0x9018_0;
                                    state_0x9010_3 = state_0x9010_0;
                                    state_0x82d8_3 = state_0x82d8_0;
                                    state_0x9080_3 = state_0x9080_0;
                                    state_0x8248_4 = state_0x8248_0;
                                    state_0x9018_4 = state_0x9018_0;
                                    state_0x9010_4 = state_0x9010_0;
                                    state_0x82d8_4 = state_0x82d8_0;
                                    state_0x9080_4 = state_0x9080_0;
                                    rsi5_3 = rsi5_1;
                                    if (var_60 > var_80) {
                                        var_87 = (uint64_t)*(uint32_t *)(r86_1 + (-4L));
                                        var_88 = var_81 << 32UL;
                                        var_89 = (uint64_t)*(uint32_t *)(r86_1 + (-8L));
                                        var_90 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), var_61, var_88 | var_87, var_89, 4227569UL, rbp_0, var_61, rbx_0, 0UL, var_65, rdi4_0, rsi5_1, r86_1, state_0x8248_0, state_0x9018_0, state_0x9010_0, var_11, var_12, state_0x82d8_0, state_0x9080_0);
                                        var_91 = var_90.field_0;
                                        var_92 = var_90.field_4;
                                        var_93 = var_90.field_6;
                                        var_94 = var_90.field_7;
                                        var_95 = var_90.field_8;
                                        var_96 = var_90.field_9;
                                        var_97 = var_90.field_10;
                                        var_98 = (uint64_t)(uint32_t)var_91;
                                        var_99 = var_92 << 32UL;
                                        var_100 = var_98 * var_62;
                                        var_101 = var_99 | var_89;
                                        var_102 = helper_cc_compute_c_wrapper(var_101 - var_100, var_100, var_7, 17U);
                                        rdx2_2 = var_101;
                                        r13_1 = var_98;
                                        r13_0 = var_98;
                                        state_0x8248_1 = var_93;
                                        state_0x9018_1 = var_94;
                                        state_0x9010_1 = var_95;
                                        rax_3 = var_100;
                                        state_0x82d8_1 = var_96;
                                        state_0x9080_1 = var_97;
                                        state_0x8248_2 = var_93;
                                        state_0x9018_2 = var_94;
                                        state_0x9010_2 = var_95;
                                        state_0x82d8_2 = var_96;
                                        state_0x9080_2 = var_97;
                                        if (var_102 == 0UL) {
                                            var_103 = rax_3 - rdx2_2;
                                            var_104 = helper_cc_compute_c_wrapper(*var_74 - var_103, var_103, var_7, 17U);
                                            r13_1 = (uint64_t)(((uint32_t)r13_0 + ((uint32_t)(uint64_t)(unsigned char)var_104 ^ 1U)) + (-2));
                                            state_0x8248_2 = state_0x8248_1;
                                            state_0x9018_2 = state_0x9018_1;
                                            state_0x9010_2 = state_0x9010_1;
                                            state_0x82d8_2 = state_0x82d8_1;
                                            state_0x9080_2 = state_0x9080_1;
                                        }
                                        r13_2 = r13_1;
                                        state_0x8248_5 = state_0x8248_2;
                                        state_0x9018_5 = state_0x9018_2;
                                        state_0x9010_5 = state_0x9010_2;
                                        state_0x82d8_5 = state_0x82d8_2;
                                        state_0x9080_5 = state_0x9080_2;
                                        state_0x8248_3 = state_0x8248_2;
                                        state_0x9018_3 = state_0x9018_2;
                                        state_0x9010_3 = state_0x9010_2;
                                        state_0x82d8_3 = state_0x82d8_2;
                                        state_0x9080_3 = state_0x9080_2;
                                        if (r13_1 != 0UL) {
                                            *var_79 = (uint32_t)r13_4;
                                            rdi4_0 = rdi4_1;
                                            rsi5_1 = rsi5_3;
                                            state_0x8248_0 = state_0x8248_5;
                                            state_0x9018_0 = state_0x9018_5;
                                            state_0x9010_0 = state_0x9010_5;
                                            state_0x82d8_0 = state_0x82d8_5;
                                            state_0x9080_0 = state_0x9080_5;
                                            if (r86_1 == var_63) {
                                                break;
                                            }
                                            r86_1 = r86_1 + (-4L);
                                            continue;
                                        }
                                        r15_0 = (uint64_t)(uint32_t)r13_2;
                                        r13_3 = r13_2;
                                        state_0x8248_4 = state_0x8248_3;
                                        state_0x9018_4 = state_0x9018_3;
                                        state_0x9010_4 = state_0x9010_3;
                                        state_0x82d8_4 = state_0x82d8_3;
                                        state_0x9080_4 = state_0x9080_3;
                                    } else {
                                        var_82 = helper_cc_compute_c_wrapper(var_61 - var_81, var_81, var_7, 16U);
                                        var_83 = (uint64_t)*(uint32_t *)(r86_1 + (-4L));
                                        var_84 = var_61 + var_83;
                                        var_85 = helper_cc_compute_c_wrapper(var_84, var_83, var_7, 8U);
                                        if (var_82 != 0UL & var_85 != 0UL) {
                                            var_86 = (var_84 << 32UL) | (uint64_t)*(uint32_t *)(r86_1 + (-8L));
                                            rdx2_2 = var_86;
                                            if (var_78 <= var_86) {
                                                var_103 = rax_3 - rdx2_2;
                                                var_104 = helper_cc_compute_c_wrapper(*var_74 - var_103, var_103, var_7, 17U);
                                                r13_1 = (uint64_t)(((uint32_t)r13_0 + ((uint32_t)(uint64_t)(unsigned char)var_104 ^ 1U)) + (-2));
                                                state_0x8248_2 = state_0x8248_1;
                                                state_0x9018_2 = state_0x9018_1;
                                                state_0x9010_2 = state_0x9010_1;
                                                state_0x82d8_2 = state_0x82d8_1;
                                                state_0x9080_2 = state_0x9080_1;
                                                r13_2 = r13_1;
                                                state_0x8248_5 = state_0x8248_2;
                                                state_0x9018_5 = state_0x9018_2;
                                                state_0x9010_5 = state_0x9010_2;
                                                state_0x82d8_5 = state_0x82d8_2;
                                                state_0x9080_5 = state_0x9080_2;
                                                state_0x8248_3 = state_0x8248_2;
                                                state_0x9018_3 = state_0x9018_2;
                                                state_0x9010_3 = state_0x9010_2;
                                                state_0x82d8_3 = state_0x82d8_2;
                                                state_0x9080_3 = state_0x9080_2;
                                                if (r13_1 == 0UL) {
                                                    *var_79 = (uint32_t)r13_4;
                                                    rdi4_0 = rdi4_1;
                                                    rsi5_1 = rsi5_3;
                                                    state_0x8248_0 = state_0x8248_5;
                                                    state_0x9018_0 = state_0x9018_5;
                                                    state_0x9010_0 = state_0x9010_5;
                                                    state_0x82d8_0 = state_0x82d8_5;
                                                    state_0x9080_0 = state_0x9080_5;
                                                    if (r86_1 == var_63) {
                                                        break;
                                                    }
                                                    r86_1 = r86_1 + (-4L);
                                                    continue;
                                                }
                                            }
                                            r15_0 = (uint64_t)(uint32_t)r13_2;
                                            r13_3 = r13_2;
                                            state_0x8248_4 = state_0x8248_3;
                                            state_0x9018_4 = state_0x9018_3;
                                            state_0x9010_4 = state_0x9010_3;
                                            state_0x82d8_4 = state_0x82d8_3;
                                            state_0x9080_4 = state_0x9080_3;
                                        }
                                    }
                                }
                            var_127 = *var_71;
                            var_128 = *var_75;
                            var_129 = *var_73;
                            var_130 = *var_66;
                            var_131 = *(uint32_t *)((var_33 + var_129) + (-4L));
                            var_132 = (*(uint32_t *)(((var_127 << 2UL) + var_128) + (-4L)) == 0U) ? *var_69 : var_127;
                            r14_1 = var_132;
                            r14_0 = var_132;
                            r12_0 = var_129;
                            r93_2 = var_130;
                            r13_6 = var_128;
                            r12_2 = var_129;
                            r93_4 = var_130;
                            r13_8 = var_128;
                            r12_3 = var_129;
                            r93_5 = var_130;
                            r13_9 = var_128;
                            rax_8 = var_128;
                            if (var_131 == 0U) {
                                while (1U)
                                    {
                                        var_133 = rdx2_3 + (-1L);
                                        rdx2_3 = var_133;
                                        rdx2_4 = var_133;
                                        if (var_133 != 0UL) {
                                            loop_state_var = 0U;
                                            break;
                                        }
                                        if (*(uint32_t *)(((var_133 << 2UL) + var_129) + (-4L)) == 0U) {
                                            continue;
                                        }
                                        loop_state_var = 1U;
                                        break;
                                    }
                                switch (loop_state_var) {
                                  case 0U:
                                    {
                                        r14_1 = r14_0;
                                        r12_2 = r12_0;
                                        r93_4 = r93_2;
                                        r13_8 = r13_6;
                                        local_sp_3 = local_sp_1;
                                        r12_3 = r12_0;
                                        r93_5 = r93_2;
                                        r13_9 = r13_6;
                                        rax_8 = r13_6;
                                        local_sp_4 = local_sp_1;
                                        r12_4 = r12_0;
                                        r93_6 = r93_2;
                                        r13_10 = r13_6;
                                        local_sp_5 = local_sp_1;
                                        while (1U)
                                            {
                                                if (rax_7 > rdx2_7) {
                                                    var_154 = helper_cc_compute_c_wrapper(rax_7 - rbx_0, rbx_0, var_7, 17U);
                                                    if (var_154 != 0UL) {
                                                        var_155 = (uint64_t)*(uint32_t *)((rax_7 << 2UL) + rbp_1);
                                                        rsi5_4 = var_155;
                                                        r14_3 = r14_0;
                                                        if (rsi5_4 <= (uint64_t)(uint32_t)rcx1_4) {
                                                            loop_state_var = 2U;
                                                            break;
                                                        }
                                                    }
                                                }
                                                if (rax_7 == 0UL) {
                                                    var_150 = helper_cc_compute_c_wrapper(0UL - rdx2_7, rdx2_7, var_7, 17U);
                                                    if (var_150 != 0UL) {
                                                        var_154 = helper_cc_compute_c_wrapper(rax_7 - rbx_0, rbx_0, var_7, 17U);
                                                        if (var_154 != 0UL) {
                                                            var_155 = (uint64_t)*(uint32_t *)((rax_7 << 2UL) + rbp_1);
                                                            rsi5_4 = var_155;
                                                            r14_3 = r14_0;
                                                            if (rsi5_4 <= (uint64_t)(uint32_t)rcx1_4) {
                                                                loop_state_var = 2U;
                                                                break;
                                                            }
                                                        }
                                                        if (rax_7 == 0UL) {
                                                            rax_7 = rax_7 + (-1L);
                                                            continue;
                                                        }
                                                        if (r14_0 != 0UL) {
                                                            loop_state_var = 2U;
                                                            break;
                                                        }
                                                        r14_3 = r14_0;
                                                        if ((*(unsigned char *)r13_6 & '\x01') != '\x00') {
                                                            loop_state_var = 2U;
                                                            break;
                                                        }
                                                        loop_state_var = 0U;
                                                        break;
                                                    }
                                                    _pre335 = 0UL + r12_0;
                                                    _pre_phi336 = _pre335;
                                                    rcx1_3 = rcx1_2 | (uint64_t)((uint32_t)((uint64_t)*(uint32_t *)_pre_phi336 << 1UL) & (-2));
                                                } else {
                                                    var_147 = (rax_7 << 2UL) + r12_0;
                                                    var_148 = (uint64_t)(*(uint32_t *)(var_147 + (-4L)) >> 31U);
                                                    var_149 = helper_cc_compute_c_wrapper(rax_7 - rdx2_7, rdx2_7, var_7, 17U);
                                                    _pre_phi336 = var_147;
                                                    rcx1_2 = var_148;
                                                    rcx1_3 = var_148;
                                                    if (var_149 == 0UL) {
                                                        rcx1_3 = rcx1_2 | (uint64_t)((uint32_t)((uint64_t)*(uint32_t *)_pre_phi336 << 1UL) & (-2));
                                                    }
                                                }
                                                var_151 = helper_cc_compute_c_wrapper(rax_7 - rbx_0, rbx_0, var_7, 17U);
                                                rcx1_4 = rcx1_3;
                                                if (var_151 == 0UL) {
                                                    if ((uint64_t)(uint32_t)rcx1_3 == 0UL) {
                                                        loop_state_var = 1U;
                                                        break;
                                                    }
                                                }
                                                var_152 = (uint64_t)*(uint32_t *)((rax_7 << 2UL) + rbp_1);
                                                var_153 = helper_cc_compute_c_wrapper(var_152 - rcx1_3, rcx1_3, var_7, 16U);
                                                rsi5_4 = var_152;
                                                if (var_153 == 0UL) {
                                                    loop_state_var = 1U;
                                                    break;
                                                }
                                                r14_3 = r14_0;
                                                if (rsi5_4 <= (uint64_t)(uint32_t)rcx1_4) {
                                                    loop_state_var = 2U;
                                                    break;
                                                }
                                                if (rax_7 == 0UL) {
                                                    rax_7 = rax_7 + (-1L);
                                                    continue;
                                                }
                                                if (r14_0 != 0UL) {
                                                    loop_state_var = 2U;
                                                    break;
                                                }
                                                r14_3 = r14_0;
                                                if ((*(unsigned char *)r13_6 & '\x01') != '\x00') {
                                                    loop_state_var = 2U;
                                                    break;
                                                }
                                                loop_state_var = 0U;
                                                break;
                                            }
                                        switch (loop_state_var) {
                                          case 2U:
                                            {
                                                local_sp_6 = local_sp_5;
                                                r12_5 = r12_4;
                                                if (r93_6 == 0UL) {
                                                    var_163 = local_sp_5 + (-8L);
                                                    *(uint64_t *)var_163 = 4227868UL;
                                                    indirect_placeholder();
                                                    local_sp_6 = var_163;
                                                }
                                                **(uint64_t **)(local_sp_6 + 24UL) = r13_10;
                                                **(uint64_t **)(local_sp_6 + 16UL) = r14_3;
                                                return r12_5;
                                            }
                                            break;
                                          case 1U:
                                            {
                                                if (r14_0 == 0UL) {
                                                    *(uint32_t *)rax_8 = 1U;
                                                    r14_3 = r14_2;
                                                    r12_4 = r12_3;
                                                    r93_6 = r93_5;
                                                    r13_10 = r13_9;
                                                    local_sp_5 = local_sp_4;
                                                    local_sp_6 = local_sp_5;
                                                    r12_5 = r12_4;
                                                    if (r93_6 == 0UL) {
                                                        var_163 = local_sp_5 + (-8L);
                                                        *(uint64_t *)var_163 = 4227868UL;
                                                        indirect_placeholder();
                                                        local_sp_6 = var_163;
                                                    }
                                                    **(uint64_t **)(local_sp_6 + 24UL) = r13_10;
                                                    **(uint64_t **)(local_sp_6 + 16UL) = r14_3;
                                                    return r12_5;
                                                }
                                                r14_3 = r14_1;
                                                r12_3 = r12_2;
                                                r93_5 = r93_4;
                                                r13_9 = r13_8;
                                                local_sp_4 = local_sp_3;
                                                r12_4 = r12_2;
                                                r93_6 = r93_4;
                                                r13_10 = r13_8;
                                                local_sp_5 = local_sp_3;
                                                while (1U)
                                                    {
                                                        var_156 = (uint32_t *)((rdx2_8 << 2UL) + r13_8);
                                                        var_157 = *var_156 + 1U;
                                                        var_158 = (uint64_t)var_157;
                                                        *var_156 = var_157;
                                                        if (var_158 != 0UL) {
                                                            loop_state_var = 0U;
                                                            break;
                                                        }
                                                        var_159 = rdx2_8 + 1UL;
                                                        var_160 = helper_cc_compute_c_wrapper(var_159 - r14_1, r14_1, var_7, 17U);
                                                        rdx2_8 = var_159;
                                                        if (var_160 == 0UL) {
                                                            continue;
                                                        }
                                                        var_161 = (r14_1 << 2UL) + r13_8;
                                                        var_162 = r14_1 + 1UL;
                                                        r14_2 = var_162;
                                                        rax_8 = var_161;
                                                        loop_state_var = 1U;
                                                        break;
                                                    }
                                                *(uint32_t *)rax_8 = 1U;
                                                r14_3 = r14_2;
                                                r12_4 = r12_3;
                                                r93_6 = r93_5;
                                                r13_10 = r13_9;
                                                local_sp_5 = local_sp_4;
                                            }
                                            break;
                                          case 0U:
                                            {
                                                r14_3 = r14_1;
                                                r12_3 = r12_2;
                                                r93_5 = r93_4;
                                                r13_9 = r13_8;
                                                local_sp_4 = local_sp_3;
                                                r12_4 = r12_2;
                                                r93_6 = r93_4;
                                                r13_10 = r13_8;
                                                local_sp_5 = local_sp_3;
                                                while (1U)
                                                    {
                                                        var_156 = (uint32_t *)((rdx2_8 << 2UL) + r13_8);
                                                        var_157 = *var_156 + 1U;
                                                        var_158 = (uint64_t)var_157;
                                                        *var_156 = var_157;
                                                        if (var_158 != 0UL) {
                                                            loop_state_var = 0U;
                                                            break;
                                                        }
                                                        var_159 = rdx2_8 + 1UL;
                                                        var_160 = helper_cc_compute_c_wrapper(var_159 - r14_1, r14_1, var_7, 17U);
                                                        rdx2_8 = var_159;
                                                        if (var_160 == 0UL) {
                                                            continue;
                                                        }
                                                        var_161 = (r14_1 << 2UL) + r13_8;
                                                        var_162 = r14_1 + 1UL;
                                                        r14_2 = var_162;
                                                        rax_8 = var_161;
                                                        loop_state_var = 1U;
                                                        break;
                                                    }
                                                *(uint32_t *)rax_8 = 1U;
                                                r14_3 = r14_2;
                                                r12_4 = r12_3;
                                                r93_6 = r93_5;
                                                r13_10 = r13_9;
                                                local_sp_5 = local_sp_4;
                                            }
                                            break;
                                        }
                                    }
                                    break;
                                  case 1U:
                                    {
                                        var_134 = helper_cc_compute_c_wrapper(rbx_0 - rdx2_4, rdx2_4, var_7, 17U);
                                        rdx2_7 = rdx2_4;
                                        if (var_134 == 0UL) {
                                            if (var_132 == 0UL) {
                                                *(uint32_t *)rax_8 = 1U;
                                                r14_3 = r14_2;
                                                r12_4 = r12_3;
                                                r93_6 = r93_5;
                                                r13_10 = r13_9;
                                                local_sp_5 = local_sp_4;
                                                local_sp_6 = local_sp_5;
                                                r12_5 = r12_4;
                                                if (r93_6 == 0UL) {
                                                    var_163 = local_sp_5 + (-8L);
                                                    *(uint64_t *)var_163 = 4227868UL;
                                                    indirect_placeholder();
                                                    local_sp_6 = var_163;
                                                }
                                                **(uint64_t **)(local_sp_6 + 24UL) = r13_10;
                                                **(uint64_t **)(local_sp_6 + 16UL) = r14_3;
                                                return r12_5;
                                            }
                                            r14_3 = r14_1;
                                            r12_3 = r12_2;
                                            r93_5 = r93_4;
                                            r13_9 = r13_8;
                                            local_sp_4 = local_sp_3;
                                            r12_4 = r12_2;
                                            r93_6 = r93_4;
                                            r13_10 = r13_8;
                                            local_sp_5 = local_sp_3;
                                            while (1U)
                                                {
                                                    var_156 = (uint32_t *)((rdx2_8 << 2UL) + r13_8);
                                                    var_157 = *var_156 + 1U;
                                                    var_158 = (uint64_t)var_157;
                                                    *var_156 = var_157;
                                                    if (var_158 != 0UL) {
                                                        loop_state_var = 0U;
                                                        break;
                                                    }
                                                    var_159 = rdx2_8 + 1UL;
                                                    var_160 = helper_cc_compute_c_wrapper(var_159 - r14_1, r14_1, var_7, 17U);
                                                    rdx2_8 = var_159;
                                                    if (var_160 == 0UL) {
                                                        continue;
                                                    }
                                                    var_161 = (r14_1 << 2UL) + r13_8;
                                                    var_162 = r14_1 + 1UL;
                                                    r14_2 = var_162;
                                                    rax_8 = var_161;
                                                    loop_state_var = 1U;
                                                    break;
                                                }
                                            switch (loop_state_var) {
                                              case 1U:
                                                {
                                                    *(uint32_t *)rax_8 = 1U;
                                                    r14_3 = r14_2;
                                                    r12_4 = r12_3;
                                                    r93_6 = r93_5;
                                                    r13_10 = r13_9;
                                                    local_sp_5 = local_sp_4;
                                                }
                                                break;
                                              case 0U:
                                                {
                                                    local_sp_6 = local_sp_5;
                                                    r12_5 = r12_4;
                                                    if (r93_6 == 0UL) {
                                                        var_163 = local_sp_5 + (-8L);
                                                        *(uint64_t *)var_163 = 4227868UL;
                                                        indirect_placeholder();
                                                        local_sp_6 = var_163;
                                                    }
                                                    **(uint64_t **)(local_sp_6 + 24UL) = r13_10;
                                                    **(uint64_t **)(local_sp_6 + 16UL) = r14_3;
                                                    return r12_5;
                                                }
                                                break;
                                            }
                                        }
                                    }
                                    break;
                                }
                            } else {
                                var_134 = helper_cc_compute_c_wrapper(rbx_0 - rdx2_4, rdx2_4, var_7, 17U);
                                rdx2_7 = rdx2_4;
                                if (var_134 == 0UL) {
                                    if (var_132 == 0UL) {
                                        *(uint32_t *)rax_8 = 1U;
                                        r14_3 = r14_2;
                                        r12_4 = r12_3;
                                        r93_6 = r93_5;
                                        r13_10 = r13_9;
                                        local_sp_5 = local_sp_4;
                                        local_sp_6 = local_sp_5;
                                        r12_5 = r12_4;
                                        if (r93_6 == 0UL) {
                                            var_163 = local_sp_5 + (-8L);
                                            *(uint64_t *)var_163 = 4227868UL;
                                            indirect_placeholder();
                                            local_sp_6 = var_163;
                                        }
                                        **(uint64_t **)(local_sp_6 + 24UL) = r13_10;
                                        **(uint64_t **)(local_sp_6 + 16UL) = r14_3;
                                        return r12_5;
                                    }
                                    r14_3 = r14_1;
                                    r12_3 = r12_2;
                                    r93_5 = r93_4;
                                    r13_9 = r13_8;
                                    local_sp_4 = local_sp_3;
                                    r12_4 = r12_2;
                                    r93_6 = r93_4;
                                    r13_10 = r13_8;
                                    local_sp_5 = local_sp_3;
                                    while (1U)
                                        {
                                            var_156 = (uint32_t *)((rdx2_8 << 2UL) + r13_8);
                                            var_157 = *var_156 + 1U;
                                            var_158 = (uint64_t)var_157;
                                            *var_156 = var_157;
                                            if (var_158 != 0UL) {
                                                loop_state_var = 0U;
                                                break;
                                            }
                                            var_159 = rdx2_8 + 1UL;
                                            var_160 = helper_cc_compute_c_wrapper(var_159 - r14_1, r14_1, var_7, 17U);
                                            rdx2_8 = var_159;
                                            if (var_160 == 0UL) {
                                                continue;
                                            }
                                            var_161 = (r14_1 << 2UL) + r13_8;
                                            var_162 = r14_1 + 1UL;
                                            r14_2 = var_162;
                                            rax_8 = var_161;
                                            loop_state_var = 1U;
                                            break;
                                        }
                                    switch (loop_state_var) {
                                      case 1U:
                                        {
                                            *(uint32_t *)rax_8 = 1U;
                                            r14_3 = r14_2;
                                            r12_4 = r12_3;
                                            r93_6 = r93_5;
                                            r13_10 = r13_9;
                                            local_sp_5 = local_sp_4;
                                        }
                                        break;
                                      case 0U:
                                        {
                                            local_sp_6 = local_sp_5;
                                            r12_5 = r12_4;
                                            if (r93_6 == 0UL) {
                                                var_163 = local_sp_5 + (-8L);
                                                *(uint64_t *)var_163 = 4227868UL;
                                                indirect_placeholder();
                                                local_sp_6 = var_163;
                                            }
                                            **(uint64_t **)(local_sp_6 + 24UL) = r13_10;
                                            **(uint64_t **)(local_sp_6 + 16UL) = r14_3;
                                            return r12_5;
                                        }
                                        break;
                                    }
                                } else {
                                    r14_1 = r14_0;
                                    r12_2 = r12_0;
                                    r93_4 = r93_2;
                                    r13_8 = r13_6;
                                    local_sp_3 = local_sp_1;
                                    r12_3 = r12_0;
                                    r93_5 = r93_2;
                                    r13_9 = r13_6;
                                    rax_8 = r13_6;
                                    local_sp_4 = local_sp_1;
                                    r12_4 = r12_0;
                                    r93_6 = r93_2;
                                    r13_10 = r13_6;
                                    local_sp_5 = local_sp_1;
                                    while (1U)
                                        {
                                            if (rax_7 > rdx2_7) {
                                                var_154 = helper_cc_compute_c_wrapper(rax_7 - rbx_0, rbx_0, var_7, 17U);
                                                if (var_154 != 0UL) {
                                                    var_155 = (uint64_t)*(uint32_t *)((rax_7 << 2UL) + rbp_1);
                                                    rsi5_4 = var_155;
                                                    r14_3 = r14_0;
                                                    if (rsi5_4 <= (uint64_t)(uint32_t)rcx1_4) {
                                                        loop_state_var = 2U;
                                                        break;
                                                    }
                                                }
                                            }
                                            if (rax_7 == 0UL) {
                                                var_150 = helper_cc_compute_c_wrapper(0UL - rdx2_7, rdx2_7, var_7, 17U);
                                                if (var_150 != 0UL) {
                                                    var_154 = helper_cc_compute_c_wrapper(rax_7 - rbx_0, rbx_0, var_7, 17U);
                                                    if (var_154 != 0UL) {
                                                        var_155 = (uint64_t)*(uint32_t *)((rax_7 << 2UL) + rbp_1);
                                                        rsi5_4 = var_155;
                                                        r14_3 = r14_0;
                                                        if (rsi5_4 <= (uint64_t)(uint32_t)rcx1_4) {
                                                            loop_state_var = 2U;
                                                            break;
                                                        }
                                                    }
                                                    if (rax_7 == 0UL) {
                                                        rax_7 = rax_7 + (-1L);
                                                        continue;
                                                    }
                                                    if (r14_0 != 0UL) {
                                                        loop_state_var = 2U;
                                                        break;
                                                    }
                                                    r14_3 = r14_0;
                                                    if ((*(unsigned char *)r13_6 & '\x01') != '\x00') {
                                                        loop_state_var = 2U;
                                                        break;
                                                    }
                                                    loop_state_var = 0U;
                                                    break;
                                                }
                                                _pre335 = 0UL + r12_0;
                                                _pre_phi336 = _pre335;
                                                rcx1_3 = rcx1_2 | (uint64_t)((uint32_t)((uint64_t)*(uint32_t *)_pre_phi336 << 1UL) & (-2));
                                            } else {
                                                var_147 = (rax_7 << 2UL) + r12_0;
                                                var_148 = (uint64_t)(*(uint32_t *)(var_147 + (-4L)) >> 31U);
                                                var_149 = helper_cc_compute_c_wrapper(rax_7 - rdx2_7, rdx2_7, var_7, 17U);
                                                _pre_phi336 = var_147;
                                                rcx1_2 = var_148;
                                                rcx1_3 = var_148;
                                                if (var_149 == 0UL) {
                                                    rcx1_3 = rcx1_2 | (uint64_t)((uint32_t)((uint64_t)*(uint32_t *)_pre_phi336 << 1UL) & (-2));
                                                }
                                            }
                                            var_151 = helper_cc_compute_c_wrapper(rax_7 - rbx_0, rbx_0, var_7, 17U);
                                            rcx1_4 = rcx1_3;
                                            if (var_151 == 0UL) {
                                                if ((uint64_t)(uint32_t)rcx1_3 == 0UL) {
                                                    loop_state_var = 1U;
                                                    break;
                                                }
                                            }
                                            var_152 = (uint64_t)*(uint32_t *)((rax_7 << 2UL) + rbp_1);
                                            var_153 = helper_cc_compute_c_wrapper(var_152 - rcx1_3, rcx1_3, var_7, 16U);
                                            rsi5_4 = var_152;
                                            if (var_153 == 0UL) {
                                                loop_state_var = 1U;
                                                break;
                                            }
                                            r14_3 = r14_0;
                                            if (rsi5_4 <= (uint64_t)(uint32_t)rcx1_4) {
                                                loop_state_var = 2U;
                                                break;
                                            }
                                            if (rax_7 == 0UL) {
                                                rax_7 = rax_7 + (-1L);
                                                continue;
                                            }
                                            if (r14_0 != 0UL) {
                                                loop_state_var = 2U;
                                                break;
                                            }
                                            r14_3 = r14_0;
                                            if ((*(unsigned char *)r13_6 & '\x01') != '\x00') {
                                                loop_state_var = 2U;
                                                break;
                                            }
                                            loop_state_var = 0U;
                                            break;
                                        }
                                    switch (loop_state_var) {
                                      case 2U:
                                        {
                                            local_sp_6 = local_sp_5;
                                            r12_5 = r12_4;
                                            if (r93_6 == 0UL) {
                                                var_163 = local_sp_5 + (-8L);
                                                *(uint64_t *)var_163 = 4227868UL;
                                                indirect_placeholder();
                                                local_sp_6 = var_163;
                                            }
                                            **(uint64_t **)(local_sp_6 + 24UL) = r13_10;
                                            **(uint64_t **)(local_sp_6 + 16UL) = r14_3;
                                            return r12_5;
                                        }
                                        break;
                                      case 1U:
                                        {
                                            if (r14_0 == 0UL) {
                                                *(uint32_t *)rax_8 = 1U;
                                                r14_3 = r14_2;
                                                r12_4 = r12_3;
                                                r93_6 = r93_5;
                                                r13_10 = r13_9;
                                                local_sp_5 = local_sp_4;
                                                local_sp_6 = local_sp_5;
                                                r12_5 = r12_4;
                                                if (r93_6 == 0UL) {
                                                    var_163 = local_sp_5 + (-8L);
                                                    *(uint64_t *)var_163 = 4227868UL;
                                                    indirect_placeholder();
                                                    local_sp_6 = var_163;
                                                }
                                                **(uint64_t **)(local_sp_6 + 24UL) = r13_10;
                                                **(uint64_t **)(local_sp_6 + 16UL) = r14_3;
                                                return r12_5;
                                            }
                                            r14_3 = r14_1;
                                            r12_3 = r12_2;
                                            r93_5 = r93_4;
                                            r13_9 = r13_8;
                                            local_sp_4 = local_sp_3;
                                            r12_4 = r12_2;
                                            r93_6 = r93_4;
                                            r13_10 = r13_8;
                                            local_sp_5 = local_sp_3;
                                            while (1U)
                                                {
                                                    var_156 = (uint32_t *)((rdx2_8 << 2UL) + r13_8);
                                                    var_157 = *var_156 + 1U;
                                                    var_158 = (uint64_t)var_157;
                                                    *var_156 = var_157;
                                                    if (var_158 != 0UL) {
                                                        loop_state_var = 0U;
                                                        break;
                                                    }
                                                    var_159 = rdx2_8 + 1UL;
                                                    var_160 = helper_cc_compute_c_wrapper(var_159 - r14_1, r14_1, var_7, 17U);
                                                    rdx2_8 = var_159;
                                                    if (var_160 == 0UL) {
                                                        continue;
                                                    }
                                                    var_161 = (r14_1 << 2UL) + r13_8;
                                                    var_162 = r14_1 + 1UL;
                                                    r14_2 = var_162;
                                                    rax_8 = var_161;
                                                    loop_state_var = 1U;
                                                    break;
                                                }
                                            *(uint32_t *)rax_8 = 1U;
                                            r14_3 = r14_2;
                                            r12_4 = r12_3;
                                            r93_6 = r93_5;
                                            r13_10 = r13_9;
                                            local_sp_5 = local_sp_4;
                                        }
                                        break;
                                      case 0U:
                                        {
                                            r14_3 = r14_1;
                                            r12_3 = r12_2;
                                            r93_5 = r93_4;
                                            r13_9 = r13_8;
                                            local_sp_4 = local_sp_3;
                                            r12_4 = r12_2;
                                            r93_6 = r93_4;
                                            r13_10 = r13_8;
                                            local_sp_5 = local_sp_3;
                                            while (1U)
                                                {
                                                    var_156 = (uint32_t *)((rdx2_8 << 2UL) + r13_8);
                                                    var_157 = *var_156 + 1U;
                                                    var_158 = (uint64_t)var_157;
                                                    *var_156 = var_157;
                                                    if (var_158 != 0UL) {
                                                        loop_state_var = 0U;
                                                        break;
                                                    }
                                                    var_159 = rdx2_8 + 1UL;
                                                    var_160 = helper_cc_compute_c_wrapper(var_159 - r14_1, r14_1, var_7, 17U);
                                                    rdx2_8 = var_159;
                                                    if (var_160 == 0UL) {
                                                        continue;
                                                    }
                                                    var_161 = (r14_1 << 2UL) + r13_8;
                                                    var_162 = r14_1 + 1UL;
                                                    r14_2 = var_162;
                                                    rax_8 = var_161;
                                                    loop_state_var = 1U;
                                                    break;
                                                }
                                            *(uint32_t *)rax_8 = 1U;
                                            r14_3 = r14_2;
                                            r12_4 = r12_3;
                                            r93_6 = r93_5;
                                            r13_10 = r13_9;
                                            local_sp_5 = local_sp_4;
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                        break;
                    }
                }
                break;
            }
        }
        break;
    }
}
