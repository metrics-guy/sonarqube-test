typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_11_ret_type;
struct indirect_placeholder_12_ret_type;
struct indirect_placeholder_14_ret_type;
struct indirect_placeholder_15_ret_type;
struct indirect_placeholder_13_ret_type;
struct indirect_placeholder_16_ret_type;
struct indirect_placeholder_17_ret_type;
struct indirect_placeholder_11_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_12_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_14_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_15_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_13_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_16_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_17_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern uint64_t init_rcx(void);
extern uint64_t init_r10(void);
extern struct indirect_placeholder_11_ret_type indirect_placeholder_11(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_12_ret_type indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_14_ret_type indirect_placeholder_14(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_15_ret_type indirect_placeholder_15(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_13_ret_type indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_16_ret_type indirect_placeholder_16(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_17_ret_type indirect_placeholder_17(uint64_t param_0);
typedef _Bool bool;
void bb_unexpand(void) {
    struct indirect_placeholder_12_ret_type var_65;
    uint64_t r8_12;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t *var_11;
    struct indirect_placeholder_11_ret_type var_12;
    uint64_t var_13;
    uint64_t var_59;
    uint64_t rcx_9;
    uint64_t rax_12_be;
    uint64_t r14_8;
    uint64_t rbp_8;
    uint64_t rcx_11_be;
    uint64_t r12_9;
    uint64_t rbp_10_be;
    uint64_t r12_8;
    uint64_t r14_8_be;
    uint64_t local_sp_12;
    uint64_t r12_9_be;
    uint64_t local_sp_14_be;
    uint64_t r9_11;
    uint64_t rbx_9_be;
    uint64_t r13_6;
    uint64_t r9_0;
    uint64_t r13_11;
    uint64_t r9_13_be;
    uint64_t r8_10;
    uint64_t r13_12_be;
    uint64_t r8_12_be;
    uint64_t rax_12;
    uint64_t rbx_3;
    uint64_t rcx_3;
    uint64_t rax_8;
    uint64_t rax_6;
    uint64_t rax_0;
    uint64_t rcx_0;
    uint64_t local_sp_0;
    uint64_t r12_0;
    uint64_t rbx_0;
    uint64_t r13_2;
    uint64_t r8_0;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t rcx_4;
    uint64_t rcx_11;
    uint64_t rax_3;
    uint64_t var_50;
    uint64_t r9_13;
    uint64_t *var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t local_sp_10;
    uint64_t rax_5;
    unsigned char var_57;
    uint64_t var_58;
    uint64_t local_sp_14;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t r13_0;
    uint64_t rbp_10;
    unsigned char *var_47;
    uint64_t var_48;
    uint64_t rcx_8;
    uint64_t rax_1;
    uint64_t var_49;
    uint64_t r13_1;
    uint64_t r13_12;
    uint64_t rax_2;
    uint64_t r15_0;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    struct indirect_placeholder_14_ret_type var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t rbx_8;
    uint64_t var_24;
    uint64_t rcx_1;
    uint64_t rbp_1;
    uint64_t r15_2;
    uint64_t r12_2;
    uint64_t local_sp_2;
    uint64_t r9_2;
    uint64_t r13_4;
    uint64_t r8_1;
    uint64_t var_25;
    uint64_t rax_4;
    uint64_t rcx_2;
    uint64_t rbp_2;
    uint64_t r15_3;
    uint64_t r14_1;
    uint64_t r12_3;
    uint64_t local_sp_3;
    uint64_t rbx_2;
    uint64_t r9_3;
    uint64_t r13_5;
    uint64_t r8_2;
    uint64_t rbp_3;
    uint64_t r15_4;
    uint64_t r14_2;
    uint64_t r12_4;
    uint64_t local_sp_4;
    uint64_t r9_4;
    uint64_t r8_3;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t rbp_4;
    uint64_t r15_5;
    uint64_t r14_3;
    uint64_t local_sp_5;
    uint64_t rbx_4;
    uint64_t r9_5;
    uint64_t r13_7;
    uint64_t r8_4;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t rax_10;
    uint64_t local_sp_6;
    uint64_t r9_6;
    uint64_t r8_5;
    uint64_t rax_7;
    uint64_t rbp_7;
    uint64_t rcx_5;
    uint64_t r15_8;
    uint64_t rbp_5;
    uint64_t r14_6;
    uint64_t r15_6;
    uint64_t r12_7;
    uint64_t r14_4;
    uint64_t local_sp_11;
    uint64_t r12_5;
    uint64_t local_sp_8;
    uint64_t r9_10;
    uint64_t rbx_5;
    uint64_t r13_10;
    uint64_t r9_7;
    uint64_t r8_9;
    uint64_t r13_8;
    uint64_t r8_6;
    uint64_t rax_11;
    uint64_t var_29;
    uint32_t var_30;
    uint32_t _pre_phi;
    uint64_t rcx_6;
    uint64_t local_sp_9;
    uint64_t r9_8;
    uint64_t r8_7;
    uint64_t var_60;
    uint64_t var_61;
    struct indirect_placeholder_13_ret_type var_62;
    uint64_t rax_9;
    uint64_t rcx_7;
    uint64_t rbp_6;
    uint64_t r14_5;
    uint64_t r12_6;
    uint64_t rbx_6;
    uint64_t r9_9;
    uint64_t r13_9;
    uint64_t r8_8;
    uint64_t var_31;
    uint64_t rbx_7;
    uint64_t var_28;
    uint64_t var_32;
    uint64_t *var_33;
    uint64_t r14_7;
    uint64_t var_20;
    struct indirect_placeholder_16_ret_type var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t rcx_10;
    uint64_t rbp_9;
    uint64_t local_sp_13;
    uint64_t r9_12;
    uint64_t r8_11;
    uint64_t rbx_9;
    uint64_t var_18;
    uint32_t var_19;
    uint64_t var_14;
    uint64_t var_15;
    struct indirect_placeholder_17_ret_type var_16;
    uint64_t var_17;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rcx();
    var_2 = init_rbp();
    var_3 = init_r15();
    var_4 = init_r14();
    var_5 = init_r12();
    var_6 = init_r10();
    var_7 = init_rbx();
    var_8 = init_r9();
    var_9 = init_r13();
    var_10 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_9;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    *(uint64_t *)(var_0 + (-48L)) = var_7;
    var_11 = (uint64_t *)(var_0 + (-96L));
    *var_11 = 4204725UL;
    var_12 = indirect_placeholder_11(var_2, 0UL);
    var_13 = var_12.field_0;
    r14_8 = 1UL;
    r12_9 = 0UL;
    r14_8_be = 0UL;
    r12_9_be = 0UL;
    rbx_0 = 0UL;
    rax_3 = 0UL;
    r13_12 = 0UL;
    rbp_1 = 0UL;
    r15_2 = 0UL;
    r14_1 = 1UL;
    rbx_2 = 1UL;
    r15_4 = 0UL;
    r14_2 = 0UL;
    r12_7 = 0UL;
    rcx_10 = var_1;
    rbp_9 = var_13;
    r9_12 = var_8;
    r8_11 = var_10;
    rbx_9 = 1UL;
    if (var_13 == 0UL) {
        return;
    }
    var_14 = *(uint64_t *)4285760UL;
    var_15 = var_0 + (-104L);
    *(uint64_t *)var_15 = 4204749UL;
    var_16 = indirect_placeholder_17(var_14);
    var_17 = var_16.field_0;
    *var_11 = var_17;
    rax_11 = var_17;
    local_sp_13 = var_15;
    loop_state_var = 7U;
    while (1U)
        {
            switch_state_var = 0;
            switch (loop_state_var) {
              case 5U:
                {
                    rcx_9 = rcx_5;
                    rbp_8 = rbp_5;
                    r12_8 = r12_5;
                    r9_11 = r9_7;
                    r13_11 = r13_8;
                    r8_10 = r8_6;
                    rbx_8 = rbx_5;
                    rax_11 = rax_7;
                    rax_9 = rax_7;
                    rcx_7 = rcx_5;
                    rbp_6 = rbp_5;
                    r14_5 = r14_4;
                    r12_6 = r12_5;
                    rbx_6 = rbx_5;
                    r9_9 = r9_7;
                    r13_9 = r13_8;
                    r8_8 = r8_6;
                    r14_7 = r14_4;
                    rcx_10 = rcx_5;
                    rbp_9 = rbp_5;
                    local_sp_13 = local_sp_8;
                    r9_12 = r9_7;
                    r8_11 = r8_6;
                    if ((uint64_t)((uint32_t)r15_6 + (-10)) != 0UL) {
                        loop_state_var = 0U;
                        continue;
                    }
                    var_29 = local_sp_8 + (-8L);
                    *(uint64_t *)var_29 = 4204845UL;
                    indirect_placeholder_1();
                    var_30 = (uint32_t)rax_7;
                    _pre_phi = var_30;
                    local_sp_10 = var_29;
                    local_sp_12 = var_29;
                    if ((int)var_30 <= (int)4294967295U) {
                        loop_state_var = 1U;
                        continue;
                    }
                    var_31 = (uint64_t)_pre_phi;
                    rcx_11_be = rcx_7;
                    rbp_10_be = rbp_6;
                    r9_13_be = r9_9;
                    r8_12_be = r8_8;
                    rbx_3 = rbx_6;
                    r12_0 = r12_6;
                    r13_2 = r13_9;
                    rax_3 = rax_9;
                    rcx_8 = rcx_7;
                    r15_0 = var_31;
                    rcx_1 = rcx_7;
                    rbp_1 = rbp_6;
                    r12_2 = r12_6;
                    r9_2 = r9_9;
                    r13_4 = r13_9;
                    r8_1 = r8_8;
                    rbp_3 = rbp_6;
                    r12_4 = r12_6;
                    r9_4 = r9_9;
                    r8_3 = r8_8;
                    rax_10 = rax_9;
                    rbp_7 = rbp_6;
                    r15_8 = var_31;
                    r14_6 = r14_5;
                    r15_6 = var_31;
                    r12_7 = r12_6;
                    local_sp_11 = local_sp_10;
                    r9_10 = r9_9;
                    r13_10 = r13_9;
                    r8_9 = r8_8;
                    rcx_6 = rcx_7;
                    r9_8 = r9_9;
                    r8_7 = r8_8;
                    rbx_7 = rbx_6;
                    if ((uint64_t)(unsigned char)rbx_6 != 0UL) {
                        loop_state_var = 3U;
                        continue;
                    }
                    var_32 = local_sp_10 + (-8L);
                    var_33 = (uint64_t *)var_32;
                    *var_33 = 4205008UL;
                    indirect_placeholder_1();
                    local_sp_2 = var_32;
                    local_sp_4 = var_32;
                    if (var_31 == 0UL) {
                        if ((uint64_t)(_pre_phi + (-8)) != 0UL) {
                            loop_state_var = 2U;
                            continue;
                        }
                        var_50 = r13_9 + (r13_9 != 0UL);
                        var_51 = (uint64_t *)(local_sp_10 + 16UL);
                        var_52 = *var_51;
                        var_53 = var_52 + (var_52 != 0UL);
                        *var_51 = var_53;
                        rax_12_be = var_53;
                        r13_6 = var_50;
                        r13_12_be = var_50;
                        rax_8 = var_53;
                        rax_5 = var_53;
                        if (r12_6 == 0UL) {
                            r9_0 = r9_4;
                            rax_6 = rax_5;
                            r8_0 = r8_3;
                            rbp_4 = rbp_3;
                            r15_5 = r15_4;
                            r14_3 = r14_2;
                            rbx_4 = rbx_3;
                            r9_5 = r9_4;
                            r13_7 = r13_6;
                            r8_4 = r8_3;
                            if (r12_4 <= 1UL & *(unsigned char *)(local_sp_4 + 7UL) == '\x00') {
                                var_54 = *(uint64_t *)(local_sp_4 + 8UL);
                                *(unsigned char *)var_54 = (unsigned char)'\t';
                                rax_6 = var_54;
                            }
                            var_55 = *(uint64_t *)4283968UL;
                            var_56 = local_sp_4 + (-8L);
                            *(uint64_t *)var_56 = 4205087UL;
                            indirect_placeholder_1();
                            rcx_3 = var_55;
                            rax_0 = rax_6;
                            rcx_0 = var_55;
                            local_sp_0 = var_56;
                            local_sp_5 = var_56;
                            if (rax_6 != r12_4) {
                                loop_state_var = 3U;
                                switch_state_var = 1;
                                break;
                            }
                            *(unsigned char *)(local_sp_4 + (-1L)) = (unsigned char)'\x00';
                            loop_state_var = 4U;
                            continue;
                        }
                        var_57 = *(unsigned char *)4285268UL;
                        var_58 = local_sp_10 + (-16L);
                        *(uint64_t *)var_58 = 4205217UL;
                        indirect_placeholder_1();
                        local_sp_14_be = var_58;
                        local_sp_9 = var_58;
                        if ((int)(uint32_t)var_53 >= (int)0U) {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        var_59 = (uint64_t)var_57;
                        rbx_9_be = var_59;
                        r8_12 = r8_12_be;
                        r14_8 = r14_8_be;
                        r12_9 = r12_9_be;
                        rax_12 = rax_12_be;
                        rcx_11 = rcx_11_be;
                        r9_13 = r9_13_be;
                        local_sp_14 = local_sp_14_be;
                        rbp_10 = rbp_10_be;
                        r13_12 = r13_12_be;
                        rbx_9 = rbx_9_be;
                        loop_state_var = 7U;
                        continue;
                    }
                    var_34 = local_sp_10 + 15UL;
                    var_35 = local_sp_10 + 16UL;
                    var_36 = local_sp_10 + (-16L);
                    *(uint64_t *)var_36 = 4204910UL;
                    var_37 = indirect_placeholder_14(rbp_6, var_6, rbx_6, var_34, r9_9, r13_9, var_35);
                    var_38 = var_37.field_0;
                    var_39 = var_37.field_1;
                    var_40 = var_37.field_2;
                    var_41 = var_37.field_3;
                    var_42 = var_37.field_4;
                    rcx_4 = var_39;
                    r13_0 = var_38;
                    rax_1 = var_38;
                    r13_1 = var_38;
                    rax_2 = var_38;
                    rcx_2 = var_39;
                    rbp_2 = var_40;
                    local_sp_3 = var_36;
                    r9_3 = var_41;
                    r8_2 = var_42;
                    local_sp_6 = var_36;
                    r9_6 = var_41;
                    r8_5 = var_42;
                    rcx_5 = var_39;
                    rbp_5 = var_40;
                    local_sp_8 = var_36;
                    r9_7 = var_41;
                    r8_6 = var_42;
                    if (*(unsigned char *)(local_sp_10 + 7UL) != '\x00') {
                        r15_0 = 9UL;
                        rbx_0 = 1UL;
                        if (r13_9 <= var_38) {
                            loop_state_var = 2U;
                            switch_state_var = 1;
                            break;
                        }
                        if ((uint64_t)(_pre_phi + (-9)) == 0UL) {
                            if (r12_6 == 0UL) {
                                var_49 = *var_33;
                                *(unsigned char *)var_49 = (unsigned char)'\t';
                                rax_1 = var_49;
                                r13_1 = r13_0;
                            }
                        } else {
                            var_43 = r13_9 + 1UL;
                            var_44 = (uint64_t)(uint32_t)r14_5 ^ 1UL;
                            var_45 = (var_38 != var_43);
                            var_46 = var_44 | var_45;
                            r13_0 = var_43;
                            r14_4 = var_46;
                            rbx_5 = var_46;
                            r13_8 = var_43;
                            if (((uint64_t)(unsigned char)var_44 | var_45) != 0UL) {
                                var_47 = (unsigned char *)(local_sp_10 + (-9L));
                                *var_47 = (unsigned char)((var_38 == var_43) ? var_46 : (uint64_t)*var_47);
                                var_48 = *var_33;
                                *(unsigned char *)(r12_6 + var_48) = (unsigned char)rax_9;
                                rax_7 = var_48;
                                r12_5 = r12_6 + 1UL;
                                loop_state_var = 5U;
                                continue;
                            }
                            var_49 = *var_33;
                            *(unsigned char *)var_49 = (unsigned char)'\t';
                            rax_1 = var_49;
                            r13_1 = r13_0;
                        }
                        rax_2 = rax_1;
                        r12_0 = (uint64_t)*(unsigned char *)(local_sp_10 + (-9L));
                        r13_2 = r13_1;
                    }
                    rax_4 = rax_2;
                    r15_3 = r15_0;
                    r12_3 = r12_0;
                    rbx_2 = rbx_0;
                    r13_5 = r13_2;
                    loop_state_var = 6U;
                    continue;
                }
                break;
              case 1U:
                {
                    var_20 = local_sp_12 + (-8L);
                    *(uint64_t *)var_20 = 4204860UL;
                    var_21 = indirect_placeholder_16(rbp_8, rbp_8);
                    var_22 = var_21.field_0;
                    var_23 = var_21.field_1;
                    rax_12_be = var_22;
                    rcx_11_be = rcx_9;
                    rbp_10_be = var_22;
                    r14_8_be = r14_7;
                    r12_9_be = r12_8;
                    local_sp_14_be = var_20;
                    rbx_9_be = rbx_8;
                    r9_13_be = r9_11;
                    r13_12_be = r13_11;
                    r8_12_be = r8_10;
                    rcx_1 = rcx_9;
                    r15_2 = var_23;
                    r12_2 = r12_8;
                    r9_2 = r9_11;
                    r13_4 = r13_11;
                    r8_1 = r8_10;
                    if (var_22 != 0UL) {
                        r8_12 = r8_12_be;
                        r14_8 = r14_8_be;
                        r12_9 = r12_9_be;
                        rax_12 = rax_12_be;
                        rcx_11 = rcx_11_be;
                        r9_13 = r9_13_be;
                        local_sp_14 = local_sp_14_be;
                        rbp_10 = rbp_10_be;
                        r13_12 = r13_12_be;
                        rbx_9 = rbx_9_be;
                        loop_state_var = 7U;
                        continue;
                    }
                    if ((uint64_t)(unsigned char)rbx_8 != 0UL) {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    var_24 = local_sp_12 + (-16L);
                    *(uint64_t *)var_24 = 4204884UL;
                    indirect_placeholder_1();
                    local_sp_2 = var_24;
                    var_25 = r13_4 + 1UL;
                    rcx_4 = rcx_1;
                    rax_4 = rax_3;
                    rcx_2 = rcx_1;
                    rbp_2 = rbp_1;
                    r15_3 = r15_2;
                    r14_1 = 0UL;
                    r12_3 = r12_2;
                    local_sp_3 = local_sp_2;
                    r9_3 = r9_2;
                    r13_5 = var_25;
                    r8_2 = r8_1;
                    local_sp_6 = local_sp_2;
                    r9_6 = r9_2;
                    r8_5 = r8_1;
                    if (var_25 != 0UL) {
                        loop_state_var = 2U;
                        switch_state_var = 1;
                        break;
                    }
                    r13_6 = r13_5;
                    rbx_3 = rbx_2;
                    rcx_3 = rcx_2;
                    rax_5 = rax_4;
                    rbp_3 = rbp_2;
                    r15_4 = r15_3;
                    r14_2 = r14_1;
                    r12_4 = r12_3;
                    local_sp_4 = local_sp_3;
                    r9_4 = r9_3;
                    r8_3 = r8_2;
                    rbp_4 = rbp_2;
                    r15_5 = r15_3;
                    r14_3 = r14_1;
                    local_sp_5 = local_sp_3;
                    rbx_4 = rbx_2;
                    r9_5 = r9_3;
                    r13_7 = r13_5;
                    r8_4 = r8_2;
                    if (r12_3 == 0UL) {
                        var_26 = (uint64_t)(uint32_t)r14_3 | (uint64_t)*(unsigned char *)4285268UL;
                        rcx_8 = rcx_3;
                        rax_10 = var_26;
                        rbp_7 = rbp_4;
                        r15_8 = r15_5;
                        r14_6 = r14_3;
                        local_sp_11 = local_sp_5;
                        r9_10 = r9_5;
                        r13_10 = r13_7;
                        r8_9 = r8_4;
                        if ((int)(uint32_t)r15_5 >= (int)0U) {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        var_27 = rbx_4 & var_26;
                        rbx_7 = var_27;
                        var_28 = local_sp_11 + (-8L);
                        *(uint64_t *)var_28 = 4204823UL;
                        indirect_placeholder_1();
                        rax_8 = rax_10;
                        rax_7 = rax_10;
                        rcx_5 = rcx_8;
                        rbp_5 = rbp_7;
                        r15_6 = r15_8;
                        r14_4 = r14_6;
                        r12_5 = r12_7;
                        local_sp_8 = var_28;
                        rbx_5 = rbx_7;
                        r9_7 = r9_10;
                        r13_8 = r13_10;
                        r8_6 = r8_9;
                        rcx_6 = rcx_8;
                        local_sp_9 = var_28;
                        r9_8 = r9_10;
                        r8_7 = r8_9;
                        if ((int)(uint32_t)rax_10 >= (int)0U) {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                    }
                    r9_0 = r9_4;
                    rax_6 = rax_5;
                    r8_0 = r8_3;
                    rbp_4 = rbp_3;
                    r15_5 = r15_4;
                    r14_3 = r14_2;
                    rbx_4 = rbx_3;
                    r9_5 = r9_4;
                    r13_7 = r13_6;
                    r8_4 = r8_3;
                    if (r12_4 <= 1UL & *(unsigned char *)(local_sp_4 + 7UL) == '\x00') {
                        var_54 = *(uint64_t *)(local_sp_4 + 8UL);
                        *(unsigned char *)var_54 = (unsigned char)'\t';
                        rax_6 = var_54;
                    }
                    var_55 = *(uint64_t *)4283968UL;
                    var_56 = local_sp_4 + (-8L);
                    *(uint64_t *)var_56 = 4205087UL;
                    indirect_placeholder_1();
                    rcx_3 = var_55;
                    rax_0 = rax_6;
                    rcx_0 = var_55;
                    local_sp_0 = var_56;
                    local_sp_5 = var_56;
                    if (rax_6 != r12_4) {
                        loop_state_var = 3U;
                        switch_state_var = 1;
                        break;
                    }
                    *(unsigned char *)(local_sp_4 + (-1L)) = (unsigned char)'\x00';
                    loop_state_var = 4U;
                    continue;
                }
                break;
              case 2U:
                {
                    var_25 = r13_4 + 1UL;
                    rcx_4 = rcx_1;
                    rax_4 = rax_3;
                    rcx_2 = rcx_1;
                    rbp_2 = rbp_1;
                    r15_3 = r15_2;
                    r14_1 = 0UL;
                    r12_3 = r12_2;
                    local_sp_3 = local_sp_2;
                    r9_3 = r9_2;
                    r13_5 = var_25;
                    r8_2 = r8_1;
                    local_sp_6 = local_sp_2;
                    r9_6 = r9_2;
                    r8_5 = r8_1;
                    if (var_25 == 0UL) {
                        loop_state_var = 2U;
                        switch_state_var = 1;
                        break;
                    }
                    r13_6 = r13_5;
                    rbx_3 = rbx_2;
                    rcx_3 = rcx_2;
                    rax_5 = rax_4;
                    rbp_3 = rbp_2;
                    r15_4 = r15_3;
                    r14_2 = r14_1;
                    r12_4 = r12_3;
                    local_sp_4 = local_sp_3;
                    r9_4 = r9_3;
                    r8_3 = r8_2;
                    rbp_4 = rbp_2;
                    r15_5 = r15_3;
                    r14_3 = r14_1;
                    local_sp_5 = local_sp_3;
                    rbx_4 = rbx_2;
                    r9_5 = r9_3;
                    r13_7 = r13_5;
                    r8_4 = r8_2;
                    if (r12_3 != 0UL) {
                        var_26 = (uint64_t)(uint32_t)r14_3 | (uint64_t)*(unsigned char *)4285268UL;
                        rcx_8 = rcx_3;
                        rax_10 = var_26;
                        rbp_7 = rbp_4;
                        r15_8 = r15_5;
                        r14_6 = r14_3;
                        local_sp_11 = local_sp_5;
                        r9_10 = r9_5;
                        r13_10 = r13_7;
                        r8_9 = r8_4;
                        if ((int)(uint32_t)r15_5 < (int)0U) {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        var_27 = rbx_4 & var_26;
                        rbx_7 = var_27;
                        var_28 = local_sp_11 + (-8L);
                        *(uint64_t *)var_28 = 4204823UL;
                        indirect_placeholder_1();
                        rax_8 = rax_10;
                        rax_7 = rax_10;
                        rcx_5 = rcx_8;
                        rbp_5 = rbp_7;
                        r15_6 = r15_8;
                        r14_4 = r14_6;
                        r12_5 = r12_7;
                        local_sp_8 = var_28;
                        rbx_5 = rbx_7;
                        r9_7 = r9_10;
                        r13_8 = r13_10;
                        r8_6 = r8_9;
                        rcx_6 = rcx_8;
                        local_sp_9 = var_28;
                        r9_8 = r9_10;
                        r8_7 = r8_9;
                        if ((int)(uint32_t)rax_10 >= (int)0U) {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                    }
                    r9_0 = r9_4;
                    rax_6 = rax_5;
                    r8_0 = r8_3;
                    rbp_4 = rbp_3;
                    r15_5 = r15_4;
                    r14_3 = r14_2;
                    rbx_4 = rbx_3;
                    r9_5 = r9_4;
                    r13_7 = r13_6;
                    r8_4 = r8_3;
                    if (r12_4 <= 1UL & *(unsigned char *)(local_sp_4 + 7UL) == '\x00') {
                        var_54 = *(uint64_t *)(local_sp_4 + 8UL);
                        *(unsigned char *)var_54 = (unsigned char)'\t';
                        rax_6 = var_54;
                    }
                    var_55 = *(uint64_t *)4283968UL;
                    var_56 = local_sp_4 + (-8L);
                    *(uint64_t *)var_56 = 4205087UL;
                    indirect_placeholder_1();
                    rcx_3 = var_55;
                    rax_0 = rax_6;
                    rcx_0 = var_55;
                    local_sp_0 = var_56;
                    local_sp_5 = var_56;
                    if (rax_6 == r12_4) {
                        loop_state_var = 3U;
                        switch_state_var = 1;
                        break;
                    }
                    *(unsigned char *)(local_sp_4 + (-1L)) = (unsigned char)'\x00';
                    loop_state_var = 4U;
                    continue;
                }
                break;
              case 3U:
                {
                    var_28 = local_sp_11 + (-8L);
                    *(uint64_t *)var_28 = 4204823UL;
                    indirect_placeholder_1();
                    rax_8 = rax_10;
                    rax_7 = rax_10;
                    rcx_5 = rcx_8;
                    rbp_5 = rbp_7;
                    r15_6 = r15_8;
                    r14_4 = r14_6;
                    r12_5 = r12_7;
                    local_sp_8 = var_28;
                    rbx_5 = rbx_7;
                    r9_7 = r9_10;
                    r13_8 = r13_10;
                    r8_6 = r8_9;
                    rcx_6 = rcx_8;
                    local_sp_9 = var_28;
                    r9_8 = r9_10;
                    r8_7 = r8_9;
                    if ((int)(uint32_t)rax_10 >= (int)0U) {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                }
                break;
              case 4U:
                {
                    var_26 = (uint64_t)(uint32_t)r14_3 | (uint64_t)*(unsigned char *)4285268UL;
                    rcx_8 = rcx_3;
                    rax_10 = var_26;
                    rbp_7 = rbp_4;
                    r15_8 = r15_5;
                    r14_6 = r14_3;
                    local_sp_11 = local_sp_5;
                    r9_10 = r9_5;
                    r13_10 = r13_7;
                    r8_9 = r8_4;
                    if ((int)(uint32_t)r15_5 < (int)0U) {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    var_27 = rbx_4 & var_26;
                    rbx_7 = var_27;
                    var_28 = local_sp_11 + (-8L);
                    *(uint64_t *)var_28 = 4204823UL;
                    indirect_placeholder_1();
                    rax_8 = rax_10;
                    rax_7 = rax_10;
                    rcx_5 = rcx_8;
                    rbp_5 = rbp_7;
                    r15_6 = r15_8;
                    r14_4 = r14_6;
                    r12_5 = r12_7;
                    local_sp_8 = var_28;
                    rbx_5 = rbx_7;
                    r9_7 = r9_10;
                    r13_8 = r13_10;
                    r8_6 = r8_9;
                    rcx_6 = rcx_8;
                    local_sp_9 = var_28;
                    r9_8 = r9_10;
                    r8_7 = r8_9;
                    if ((int)(uint32_t)rax_10 >= (int)0U) {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                }
                break;
              case 6U:
                {
                    r13_6 = r13_5;
                    rbx_3 = rbx_2;
                    rcx_3 = rcx_2;
                    rax_5 = rax_4;
                    rbp_3 = rbp_2;
                    r15_4 = r15_3;
                    r14_2 = r14_1;
                    r12_4 = r12_3;
                    local_sp_4 = local_sp_3;
                    r9_4 = r9_3;
                    r8_3 = r8_2;
                    rbp_4 = rbp_2;
                    r15_5 = r15_3;
                    r14_3 = r14_1;
                    local_sp_5 = local_sp_3;
                    rbx_4 = rbx_2;
                    r9_5 = r9_3;
                    r13_7 = r13_5;
                    r8_4 = r8_2;
                    if (r12_3 != 0UL) {
                        var_26 = (uint64_t)(uint32_t)r14_3 | (uint64_t)*(unsigned char *)4285268UL;
                        rcx_8 = rcx_3;
                        rax_10 = var_26;
                        rbp_7 = rbp_4;
                        r15_8 = r15_5;
                        r14_6 = r14_3;
                        local_sp_11 = local_sp_5;
                        r9_10 = r9_5;
                        r13_10 = r13_7;
                        r8_9 = r8_4;
                        if ((int)(uint32_t)r15_5 < (int)0U) {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        var_27 = rbx_4 & var_26;
                        rbx_7 = var_27;
                        var_28 = local_sp_11 + (-8L);
                        *(uint64_t *)var_28 = 4204823UL;
                        indirect_placeholder_1();
                        rax_8 = rax_10;
                        rax_7 = rax_10;
                        rcx_5 = rcx_8;
                        rbp_5 = rbp_7;
                        r15_6 = r15_8;
                        r14_4 = r14_6;
                        r12_5 = r12_7;
                        local_sp_8 = var_28;
                        rbx_5 = rbx_7;
                        r9_7 = r9_10;
                        r13_8 = r13_10;
                        r8_6 = r8_9;
                        rcx_6 = rcx_8;
                        local_sp_9 = var_28;
                        r9_8 = r9_10;
                        r8_7 = r8_9;
                        if ((int)(uint32_t)rax_10 >= (int)0U) {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                    }
                    r9_0 = r9_4;
                    rax_6 = rax_5;
                    r8_0 = r8_3;
                    rbp_4 = rbp_3;
                    r15_5 = r15_4;
                    r14_3 = r14_2;
                    rbx_4 = rbx_3;
                    r9_5 = r9_4;
                    r13_7 = r13_6;
                    r8_4 = r8_3;
                    if (r12_4 <= 1UL & *(unsigned char *)(local_sp_4 + 7UL) == '\x00') {
                        var_54 = *(uint64_t *)(local_sp_4 + 8UL);
                        *(unsigned char *)var_54 = (unsigned char)'\t';
                        rax_6 = var_54;
                    }
                    var_55 = *(uint64_t *)4283968UL;
                    var_56 = local_sp_4 + (-8L);
                    *(uint64_t *)var_56 = 4205087UL;
                    indirect_placeholder_1();
                    rcx_3 = var_55;
                    rax_0 = rax_6;
                    rcx_0 = var_55;
                    local_sp_0 = var_56;
                    local_sp_5 = var_56;
                    if (rax_6 == r12_4) {
                        loop_state_var = 3U;
                        switch_state_var = 1;
                        break;
                    }
                    *(unsigned char *)(local_sp_4 + (-1L)) = (unsigned char)'\x00';
                    loop_state_var = 4U;
                    continue;
                }
                break;
              case 7U:
              case 0U:
                {
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 0U:
                        {
                            *(unsigned char *)(local_sp_13 + 7UL) = (unsigned char)'\x00';
                            *(uint64_t *)(local_sp_13 + 24UL) = 0UL;
                            r8_12 = r8_11;
                            rax_12 = rax_11;
                            rcx_11 = rcx_10;
                            r9_13 = r9_12;
                            local_sp_14 = local_sp_13;
                            rbp_10 = rbp_9;
                        }
                        break;
                      case 7U:
                        {
                            var_18 = local_sp_14 + (-8L);
                            *(uint64_t *)var_18 = 4204800UL;
                            indirect_placeholder_1();
                            var_19 = (uint32_t)rax_12;
                            rcx_9 = rcx_11;
                            rbp_8 = rbp_10;
                            r12_8 = r12_9;
                            local_sp_12 = var_18;
                            r9_11 = r9_13;
                            r13_11 = r13_12;
                            r8_10 = r8_12;
                            local_sp_10 = var_18;
                            rbx_8 = rbx_9;
                            _pre_phi = var_19;
                            rax_9 = rax_12;
                            rcx_7 = rcx_11;
                            rbp_6 = rbp_10;
                            r14_5 = r14_8;
                            r12_6 = r12_9;
                            rbx_6 = rbx_9;
                            r9_9 = r9_13;
                            r13_9 = r13_12;
                            r8_8 = r8_12;
                            r14_7 = r14_8;
                            if ((int)var_19 >= (int)0U) {
                                var_20 = local_sp_12 + (-8L);
                                *(uint64_t *)var_20 = 4204860UL;
                                var_21 = indirect_placeholder_16(rbp_8, rbp_8);
                                var_22 = var_21.field_0;
                                var_23 = var_21.field_1;
                                rax_12_be = var_22;
                                rcx_11_be = rcx_9;
                                rbp_10_be = var_22;
                                r14_8_be = r14_7;
                                r12_9_be = r12_8;
                                local_sp_14_be = var_20;
                                rbx_9_be = rbx_8;
                                r9_13_be = r9_11;
                                r13_12_be = r13_11;
                                r8_12_be = r8_10;
                                rcx_1 = rcx_9;
                                r15_2 = var_23;
                                r12_2 = r12_8;
                                r9_2 = r9_11;
                                r13_4 = r13_11;
                                r8_1 = r8_10;
                                if (var_22 == 0UL) {
                                    r8_12 = r8_12_be;
                                    r14_8 = r14_8_be;
                                    r12_9 = r12_9_be;
                                    rax_12 = rax_12_be;
                                    rcx_11 = rcx_11_be;
                                    r9_13 = r9_13_be;
                                    local_sp_14 = local_sp_14_be;
                                    rbp_10 = rbp_10_be;
                                    r13_12 = r13_12_be;
                                    rbx_9 = rbx_9_be;
                                    loop_state_var = 7U;
                                    continue;
                                }
                                if ((uint64_t)(unsigned char)rbx_8 == 0UL) {
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                var_24 = local_sp_12 + (-16L);
                                *(uint64_t *)var_24 = 4204884UL;
                                indirect_placeholder_1();
                                local_sp_2 = var_24;
                                var_25 = r13_4 + 1UL;
                                rcx_4 = rcx_1;
                                rax_4 = rax_3;
                                rcx_2 = rcx_1;
                                rbp_2 = rbp_1;
                                r15_3 = r15_2;
                                r14_1 = 0UL;
                                r12_3 = r12_2;
                                local_sp_3 = local_sp_2;
                                r9_3 = r9_2;
                                r13_5 = var_25;
                                r8_2 = r8_1;
                                local_sp_6 = local_sp_2;
                                r9_6 = r9_2;
                                r8_5 = r8_1;
                                if (var_25 == 0UL) {
                                    loop_state_var = 2U;
                                    switch_state_var = 1;
                                    break;
                                }
                                r13_6 = r13_5;
                                rbx_3 = rbx_2;
                                rcx_3 = rcx_2;
                                rax_5 = rax_4;
                                rbp_3 = rbp_2;
                                r15_4 = r15_3;
                                r14_2 = r14_1;
                                r12_4 = r12_3;
                                local_sp_4 = local_sp_3;
                                r9_4 = r9_3;
                                r8_3 = r8_2;
                                rbp_4 = rbp_2;
                                r15_5 = r15_3;
                                r14_3 = r14_1;
                                local_sp_5 = local_sp_3;
                                rbx_4 = rbx_2;
                                r9_5 = r9_3;
                                r13_7 = r13_5;
                                r8_4 = r8_2;
                                if (r12_3 != 0UL) {
                                    var_26 = (uint64_t)(uint32_t)r14_3 | (uint64_t)*(unsigned char *)4285268UL;
                                    rcx_8 = rcx_3;
                                    rax_10 = var_26;
                                    rbp_7 = rbp_4;
                                    r15_8 = r15_5;
                                    r14_6 = r14_3;
                                    local_sp_11 = local_sp_5;
                                    r9_10 = r9_5;
                                    r13_10 = r13_7;
                                    r8_9 = r8_4;
                                    if ((int)(uint32_t)r15_5 < (int)0U) {
                                        loop_state_var = 0U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    var_27 = rbx_4 & var_26;
                                    rbx_7 = var_27;
                                    var_28 = local_sp_11 + (-8L);
                                    *(uint64_t *)var_28 = 4204823UL;
                                    indirect_placeholder_1();
                                    rax_8 = rax_10;
                                    rax_7 = rax_10;
                                    rcx_5 = rcx_8;
                                    rbp_5 = rbp_7;
                                    r15_6 = r15_8;
                                    r14_4 = r14_6;
                                    r12_5 = r12_7;
                                    local_sp_8 = var_28;
                                    rbx_5 = rbx_7;
                                    r9_7 = r9_10;
                                    r13_8 = r13_10;
                                    r8_6 = r8_9;
                                    rcx_6 = rcx_8;
                                    local_sp_9 = var_28;
                                    r9_8 = r9_10;
                                    r8_7 = r8_9;
                                    if ((int)(uint32_t)rax_10 >= (int)0U) {
                                        loop_state_var = 1U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                }
                                r9_0 = r9_4;
                                rax_6 = rax_5;
                                r8_0 = r8_3;
                                rbp_4 = rbp_3;
                                r15_5 = r15_4;
                                r14_3 = r14_2;
                                rbx_4 = rbx_3;
                                r9_5 = r9_4;
                                r13_7 = r13_6;
                                r8_4 = r8_3;
                                if (r12_4 <= 1UL & *(unsigned char *)(local_sp_4 + 7UL) == '\x00') {
                                    var_54 = *(uint64_t *)(local_sp_4 + 8UL);
                                    *(unsigned char *)var_54 = (unsigned char)'\t';
                                    rax_6 = var_54;
                                }
                                var_55 = *(uint64_t *)4283968UL;
                                var_56 = local_sp_4 + (-8L);
                                *(uint64_t *)var_56 = 4205087UL;
                                indirect_placeholder_1();
                                rcx_3 = var_55;
                                rax_0 = rax_6;
                                rcx_0 = var_55;
                                local_sp_0 = var_56;
                                local_sp_5 = var_56;
                                if (rax_6 == r12_4) {
                                    loop_state_var = 3U;
                                    switch_state_var = 1;
                                    break;
                                }
                                *(unsigned char *)(local_sp_4 + (-1L)) = (unsigned char)'\x00';
                                loop_state_var = 4U;
                                continue;
                            }
                            var_31 = (uint64_t)_pre_phi;
                            rcx_11_be = rcx_7;
                            rbp_10_be = rbp_6;
                            r9_13_be = r9_9;
                            r8_12_be = r8_8;
                            rbx_3 = rbx_6;
                            r12_0 = r12_6;
                            r13_2 = r13_9;
                            rax_3 = rax_9;
                            rcx_8 = rcx_7;
                            r15_0 = var_31;
                            rcx_1 = rcx_7;
                            rbp_1 = rbp_6;
                            r12_2 = r12_6;
                            r9_2 = r9_9;
                            r13_4 = r13_9;
                            r8_1 = r8_8;
                            rbp_3 = rbp_6;
                            r12_4 = r12_6;
                            r9_4 = r9_9;
                            r8_3 = r8_8;
                            rax_10 = rax_9;
                            rbp_7 = rbp_6;
                            r15_8 = var_31;
                            r14_6 = r14_5;
                            r15_6 = var_31;
                            r12_7 = r12_6;
                            local_sp_11 = local_sp_10;
                            r9_10 = r9_9;
                            r13_10 = r13_9;
                            r8_9 = r8_8;
                            rcx_6 = rcx_7;
                            r9_8 = r9_9;
                            r8_7 = r8_8;
                            rbx_7 = rbx_6;
                            if ((uint64_t)(unsigned char)rbx_6 != 0UL) {
                                loop_state_var = 3U;
                                continue;
                            }
                            var_32 = local_sp_10 + (-8L);
                            var_33 = (uint64_t *)var_32;
                            *var_33 = 4205008UL;
                            indirect_placeholder_1();
                            local_sp_2 = var_32;
                            local_sp_4 = var_32;
                            if (var_31 == 0UL) {
                                if ((uint64_t)(_pre_phi + (-8)) != 0UL) {
                                    loop_state_var = 2U;
                                    continue;
                                }
                                var_50 = r13_9 + (r13_9 != 0UL);
                                var_51 = (uint64_t *)(local_sp_10 + 16UL);
                                var_52 = *var_51;
                                var_53 = var_52 + (var_52 != 0UL);
                                *var_51 = var_53;
                                rax_12_be = var_53;
                                r13_6 = var_50;
                                r13_12_be = var_50;
                                rax_8 = var_53;
                                rax_5 = var_53;
                                if (r12_6 == 0UL) {
                                    r9_0 = r9_4;
                                    rax_6 = rax_5;
                                    r8_0 = r8_3;
                                    rbp_4 = rbp_3;
                                    r15_5 = r15_4;
                                    r14_3 = r14_2;
                                    rbx_4 = rbx_3;
                                    r9_5 = r9_4;
                                    r13_7 = r13_6;
                                    r8_4 = r8_3;
                                    if (r12_4 <= 1UL & *(unsigned char *)(local_sp_4 + 7UL) == '\x00') {
                                        var_54 = *(uint64_t *)(local_sp_4 + 8UL);
                                        *(unsigned char *)var_54 = (unsigned char)'\t';
                                        rax_6 = var_54;
                                    }
                                    var_55 = *(uint64_t *)4283968UL;
                                    var_56 = local_sp_4 + (-8L);
                                    *(uint64_t *)var_56 = 4205087UL;
                                    indirect_placeholder_1();
                                    rcx_3 = var_55;
                                    rax_0 = rax_6;
                                    rcx_0 = var_55;
                                    local_sp_0 = var_56;
                                    local_sp_5 = var_56;
                                    if (rax_6 != r12_4) {
                                        loop_state_var = 3U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    *(unsigned char *)(local_sp_4 + (-1L)) = (unsigned char)'\x00';
                                    loop_state_var = 4U;
                                    continue;
                                }
                                var_57 = *(unsigned char *)4285268UL;
                                var_58 = local_sp_10 + (-16L);
                                *(uint64_t *)var_58 = 4205217UL;
                                indirect_placeholder_1();
                                local_sp_14_be = var_58;
                                local_sp_9 = var_58;
                                if ((int)(uint32_t)var_53 >= (int)0U) {
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                                var_59 = (uint64_t)var_57;
                                rbx_9_be = var_59;
                                r8_12 = r8_12_be;
                                r14_8 = r14_8_be;
                                r12_9 = r12_9_be;
                                rax_12 = rax_12_be;
                                rcx_11 = rcx_11_be;
                                r9_13 = r9_13_be;
                                local_sp_14 = local_sp_14_be;
                                rbp_10 = rbp_10_be;
                                r13_12 = r13_12_be;
                                rbx_9 = rbx_9_be;
                                loop_state_var = 7U;
                                continue;
                            }
                            var_34 = local_sp_10 + 15UL;
                            var_35 = local_sp_10 + 16UL;
                            var_36 = local_sp_10 + (-16L);
                            *(uint64_t *)var_36 = 4204910UL;
                            var_37 = indirect_placeholder_14(rbp_6, var_6, rbx_6, var_34, r9_9, r13_9, var_35);
                            var_38 = var_37.field_0;
                            var_39 = var_37.field_1;
                            var_40 = var_37.field_2;
                            var_41 = var_37.field_3;
                            var_42 = var_37.field_4;
                            rcx_4 = var_39;
                            r13_0 = var_38;
                            rax_1 = var_38;
                            r13_1 = var_38;
                            rax_2 = var_38;
                            rcx_2 = var_39;
                            rbp_2 = var_40;
                            local_sp_3 = var_36;
                            r9_3 = var_41;
                            r8_2 = var_42;
                            local_sp_6 = var_36;
                            r9_6 = var_41;
                            r8_5 = var_42;
                            rcx_5 = var_39;
                            rbp_5 = var_40;
                            local_sp_8 = var_36;
                            r9_7 = var_41;
                            r8_6 = var_42;
                            if (*(unsigned char *)(local_sp_10 + 7UL) != '\x00') {
                                r15_0 = 9UL;
                                rbx_0 = 1UL;
                                if (r13_9 <= var_38) {
                                    loop_state_var = 2U;
                                    switch_state_var = 1;
                                    break;
                                }
                                if ((uint64_t)(_pre_phi + (-9)) == 0UL) {
                                    if (r12_6 == 0UL) {
                                        var_49 = *var_33;
                                        *(unsigned char *)var_49 = (unsigned char)'\t';
                                        rax_1 = var_49;
                                        r13_1 = r13_0;
                                    }
                                } else {
                                    var_43 = r13_9 + 1UL;
                                    var_44 = (uint64_t)(uint32_t)r14_5 ^ 1UL;
                                    var_45 = (var_38 != var_43);
                                    var_46 = var_44 | var_45;
                                    r13_0 = var_43;
                                    r14_4 = var_46;
                                    rbx_5 = var_46;
                                    r13_8 = var_43;
                                    if (((uint64_t)(unsigned char)var_44 | var_45) != 0UL) {
                                        var_47 = (unsigned char *)(local_sp_10 + (-9L));
                                        *var_47 = (unsigned char)((var_38 == var_43) ? var_46 : (uint64_t)*var_47);
                                        var_48 = *var_33;
                                        *(unsigned char *)(r12_6 + var_48) = (unsigned char)rax_9;
                                        rax_7 = var_48;
                                        r12_5 = r12_6 + 1UL;
                                        loop_state_var = 5U;
                                        continue;
                                    }
                                    var_49 = *var_33;
                                    *(unsigned char *)var_49 = (unsigned char)'\t';
                                    rax_1 = var_49;
                                    r13_1 = r13_0;
                                }
                                rax_2 = rax_1;
                                r12_0 = (uint64_t)*(unsigned char *)(local_sp_10 + (-9L));
                                r13_2 = r13_1;
                            }
                            rax_4 = rax_2;
                            r15_3 = r15_0;
                            r12_3 = r12_0;
                            rbx_2 = rbx_0;
                            r13_5 = r13_2;
                            loop_state_var = 6U;
                            continue;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            indirect_placeholder_1();
            return;
        }
        break;
      case 3U:
      case 2U:
      case 1U:
        {
            switch (loop_state_var) {
              case 2U:
                {
                    *(uint64_t *)(local_sp_6 + (-8L)) = 4205398UL;
                    indirect_placeholder_15(0UL, rcx_4, 4260654UL, r9_6, 1UL, 0UL, r8_5);
                    abort();
                }
                break;
              case 3U:
              case 1U:
                {
                    switch (loop_state_var) {
                      case 1U:
                        {
                            *(uint64_t *)(local_sp_9 + (-8L)) = 4205336UL;
                            indirect_placeholder_1();
                            var_60 = (uint64_t)*(uint32_t *)rax_8;
                            var_61 = local_sp_9 + (-16L);
                            *(uint64_t *)var_61 = 4205355UL;
                            var_62 = indirect_placeholder_13(0UL, rcx_6, 4260677UL, r9_8, 1UL, var_60, r8_7);
                            r9_0 = var_62.field_2;
                            rax_0 = var_62.field_0;
                            rcx_0 = var_62.field_1;
                            local_sp_0 = var_61;
                            r8_0 = var_62.field_3;
                        }
                        break;
                      case 3U:
                        {
                            *(uint64_t *)(local_sp_0 + (-8L)) = 4205360UL;
                            indirect_placeholder_1();
                            var_63 = (uint64_t)*(uint32_t *)rax_0;
                            var_64 = local_sp_0 + (-16L);
                            *(uint64_t *)var_64 = 4205379UL;
                            var_65 = indirect_placeholder_12(0UL, rcx_0, 4260677UL, r9_0, 1UL, var_63, r8_0);
                            rcx_4 = var_65.field_1;
                            local_sp_6 = var_64;
                            r9_6 = var_65.field_2;
                            r8_5 = var_65.field_3;
                        }
                        break;
                    }
                }
                break;
            }
        }
        break;
    }
}
