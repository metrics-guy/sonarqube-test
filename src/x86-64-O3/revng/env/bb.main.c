typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_24_ret_type;
struct indirect_placeholder_25_ret_type;
struct indirect_placeholder_19_ret_type;
struct indirect_placeholder_21_ret_type;
struct indirect_placeholder_20_ret_type;
struct indirect_placeholder_22_ret_type;
struct indirect_placeholder_24_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_25_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_19_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_21_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_20_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_22_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
extern void abort(void);
extern void function_dispatcher(unsigned char *param_0);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
extern void indirect_placeholder_17(uint64_t param_0);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_24_ret_type indirect_placeholder_24(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_25_ret_type indirect_placeholder_25(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_19_ret_type indirect_placeholder_19(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_21_ret_type indirect_placeholder_21(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern void indirect_placeholder_18(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_20_ret_type indirect_placeholder_20(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_22_ret_type indirect_placeholder_22(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_23(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
typedef _Bool bool;
void bb_main(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_19_ret_type var_35;
    struct indirect_placeholder_25_ret_type var_13;
    struct indirect_placeholder_24_ret_type var_26;
    uint64_t r12_6_ph;
    uint64_t r13_0_ph193;
    uint64_t local_sp_14;
    uint64_t r12_5;
    uint32_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t local_sp_10;
    uint64_t var_24;
    uint64_t local_sp_11;
    uint64_t local_sp_12;
    uint64_t var_25;
    uint64_t var_27;
    uint32_t var_28;
    uint64_t var_12;
    uint64_t var_14;
    uint64_t r15_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t local_sp_14_ph;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t r13_0_ph;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t local_sp_5;
    uint64_t local_sp_13_be;
    uint64_t local_sp_13;
    uint64_t local_sp_0;
    uint64_t r12_0;
    uint64_t var_69;
    struct indirect_placeholder_21_ret_type var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t rax_0;
    uint64_t local_sp_4;
    uint64_t var_67;
    uint64_t local_sp_1;
    uint64_t var_68;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    struct indirect_placeholder_20_ret_type var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_58;
    uint64_t var_59;
    uint32_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t local_sp_3;
    uint64_t local_sp_2;
    uint64_t var_53;
    uint32_t var_15;
    struct indirect_placeholder_22_ret_type var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_45;
    uint32_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t r15_0_ph;
    uint64_t r12_1_ph;
    uint64_t r12_1;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint32_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t r12_2;
    uint64_t r12_3;
    uint64_t storemerge;
    uint64_t var_66;
    uint64_t local_sp_8;
    uint64_t var_63;
    uint64_t local_sp_6;
    uint64_t rbx_0;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t local_sp_14_ph192;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t local_sp_9;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r15();
    var_3 = init_r14();
    var_4 = init_r12();
    var_5 = init_rbx();
    var_6 = init_cc_src2();
    var_7 = init_r13();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_7;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_1;
    var_8 = (uint64_t)(uint32_t)rdi;
    *(uint64_t *)(var_0 + (-48L)) = var_5;
    var_9 = *(uint64_t *)rsi;
    *(uint64_t *)(var_0 + (-80L)) = 4203713UL;
    indirect_placeholder_17(var_9);
    *(uint64_t *)(var_0 + (-88L)) = 4203728UL;
    indirect_placeholder_1();
    var_10 = (uint64_t *)(var_0 + (-96L));
    *var_10 = 4203733UL;
    indirect_placeholder_1();
    var_11 = var_0 + (-104L);
    *(uint64_t *)var_11 = 4203743UL;
    indirect_placeholder_1();
    *var_10 = 0UL;
    storemerge = 4256616UL;
    local_sp_14_ph = var_11;
    r12_6_ph = 0UL;
    r13_0_ph = 0UL;
    while (1U)
        {
            r12_6_ph = 1UL;
            local_sp_14_ph192 = local_sp_14_ph;
            r13_0_ph193 = r13_0_ph;
            r12_5 = r12_6_ph;
            while (1U)
                {
                    r13_0_ph = r13_0_ph193;
                    r13_0_ph193 = 1UL;
                    local_sp_14 = local_sp_14_ph192;
                    while (1U)
                        {
                            var_12 = local_sp_14 + (-8L);
                            *(uint64_t *)var_12 = 4203783UL;
                            var_13 = indirect_placeholder_25(4256992UL, 4256868UL, var_8, rsi, 0UL);
                            var_14 = var_13.field_0;
                            var_15 = (uint32_t)var_14;
                            local_sp_14 = var_12;
                            local_sp_14_ph = var_12;
                            local_sp_8 = var_12;
                            local_sp_14_ph192 = var_12;
                            local_sp_9 = var_12;
                            if ((uint64_t)(var_15 + 1U) != 0UL) {
                                loop_state_var = 2U;
                                break;
                            }
                            var_16 = var_14 + (-67L);
                            if ((uint64_t)(uint32_t)var_16 == 0UL) {
                                *(uint64_t *)local_sp_14 = *(uint64_t *)4277504UL;
                                continue;
                            }
                            var_17 = helper_cc_compute_all_wrapper(var_16, 67UL, var_6, 16U);
                            if ((uint64_t)(((unsigned char)(var_17 >> 4UL) ^ (unsigned char)var_17) & '\xc0') != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            if ((uint64_t)(var_15 + (-105)) != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            if ((uint64_t)(var_15 + (-117)) == 0UL) {
                                continue;
                            }
                            loop_state_var = 3U;
                            break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 0U:
                        {
                            if ((uint64_t)(var_15 + 130U) == 0UL) {
                                *(uint64_t *)(local_sp_14 + (-16L)) = 4204492UL;
                                indirect_placeholder_18(var_8, r12_6_ph, 0UL);
                                abort();
                            }
                            if ((uint64_t)(var_15 + (-48)) == 0UL) {
                                continue;
                            }
                            if ((uint64_t)(var_15 + 131U) != 0UL) {
                                loop_state_var = 2U;
                                switch_state_var = 1;
                                break;
                            }
                            var_18 = *(uint64_t *)4275776UL;
                            var_19 = *(uint64_t *)4276800UL;
                            *(uint64_t *)(local_sp_14 + (-24L)) = 0UL;
                            *(uint64_t *)(local_sp_14 + (-32L)) = 4203937UL;
                            indirect_placeholder_23(0UL, var_19, 4256794UL, 4256835UL, var_18, 4256695UL, 4256851UL);
                            var_20 = local_sp_14 + (-40L);
                            *(uint64_t *)var_20 = 4203944UL;
                            indirect_placeholder_1();
                            local_sp_9 = var_20;
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 1U:
                        {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 2U:
                        {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 3U:
                        {
                            loop_state_var = 2U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    continue;
                }
                break;
              case 1U:
                {
                    var_21 = *(uint32_t *)4276924UL;
                    var_22 = (uint64_t)var_21 << 32UL;
                    var_23 = rdi << 32UL;
                    local_sp_10 = local_sp_9;
                    var_24 = local_sp_9 + (-8L);
                    *(uint64_t *)var_24 = 4203973UL;
                    indirect_placeholder_1();
                    local_sp_10 = var_24;
                    local_sp_11 = var_24;
                    if (!((long)var_22 < (long)var_23 && (uint64_t)var_21 == 0UL)) {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 2U:
                {
                    loop_state_var = 2U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 2U:
        {
            *(uint64_t *)(local_sp_8 + (-8L)) = 4203857UL;
            indirect_placeholder_18(var_8, r12_5, 125UL);
            abort();
        }
        break;
      case 1U:
      case 0U:
        {
            switch (loop_state_var) {
              case 0U:
                {
                    *(uint64_t *)4276928UL = 4277040UL;
                    local_sp_12 = local_sp_11;
                }
                break;
              case 1U:
                {
                    local_sp_11 = local_sp_10;
                    local_sp_12 = local_sp_10;
                    if (r12_6_ph == 0UL) {
                        *(uint64_t *)4276928UL = 4277040UL;
                        local_sp_12 = local_sp_11;
                    }
                }
                break;
            }
            *(uint32_t *)4276924UL = 0U;
            local_sp_13 = local_sp_12;
            while (1U)
                {
                    var_25 = local_sp_13 + (-8L);
                    *(uint64_t *)var_25 = 4204040UL;
                    var_26 = indirect_placeholder_24(4256992UL, 4256868UL, var_8, rsi, 0UL);
                    var_27 = var_26.field_0;
                    var_28 = (uint32_t)var_27;
                    local_sp_13_be = var_25;
                    rax_0 = var_27;
                    local_sp_3 = var_25;
                    if ((uint64_t)(var_28 + 1U) != 0UL) {
                        var_39 = var_26.field_1;
                        var_40 = var_26.field_2;
                        var_41 = var_26.field_3;
                        var_42 = *(uint32_t *)4276924UL;
                        var_43 = (uint64_t)var_42;
                        r12_1_ph = var_43;
                        r12_2 = var_43;
                        if ((long)(var_43 << 32UL) >= (long)var_23) {
                            loop_state_var = 2U;
                            break;
                        }
                        var_44 = local_sp_13 + (-16L);
                        *(uint64_t *)var_44 = 4204074UL;
                        indirect_placeholder_1();
                        local_sp_2 = var_44;
                        local_sp_3 = var_44;
                        if ((uint64_t)var_28 == 0UL) {
                            r15_0_ph = (uint64_t)var_42;
                            loop_state_var = 0U;
                            break;
                        }
                        var_45 = var_43 + 1UL;
                        var_46 = (uint32_t)var_45;
                        var_47 = (uint64_t)var_46;
                        *(uint32_t *)4276924UL = var_46;
                        var_48 = var_45 << 32UL;
                        r12_1_ph = var_47;
                        r12_2 = var_47;
                        if ((long)var_48 >= (long)var_23) {
                            loop_state_var = 2U;
                            break;
                        }
                        r15_0_ph = (uint64_t)((long)var_48 >> (long)32UL);
                        loop_state_var = 0U;
                        break;
                    }
                    if ((uint64_t)(var_28 + (-117)) == 0UL) {
                        local_sp_13 = local_sp_13_be;
                        continue;
                    }
                    var_29 = *(uint64_t *)4277504UL;
                    var_30 = local_sp_13 + (-16L);
                    *(uint64_t *)var_30 = 4204383UL;
                    var_31 = indirect_placeholder_2(var_29);
                    local_sp_13_be = var_30;
                    if ((uint64_t)(uint32_t)var_31 != 0UL) {
                        var_32 = var_26.field_2;
                        var_33 = var_26.field_3;
                        var_34 = *(uint64_t *)4277504UL;
                        *(uint64_t *)(local_sp_13 + (-24L)) = 4204403UL;
                        var_35 = indirect_placeholder_19(var_8, var_2, var_3, r12_6_ph, rsi, var_34, r13_0_ph193);
                        var_36 = var_35.field_0;
                        *(uint64_t *)(local_sp_13 + (-32L)) = 4204411UL;
                        indirect_placeholder_1();
                        var_37 = (uint64_t)*(uint32_t *)var_36;
                        var_38 = local_sp_13 + (-40L);
                        *(uint64_t *)var_38 = 4204433UL;
                        indirect_placeholder_3(0UL, var_36, 4256876UL, var_32, 125UL, var_37, var_33);
                        local_sp_5 = var_38;
                        loop_state_var = 1U;
                        break;
                    }
                }
            switch (loop_state_var) {
              case 0U:
                {
                    r15_0 = r15_0_ph;
                    r12_1 = r12_1_ph;
                    while (1U)
                        {
                            var_49 = *(uint64_t *)((r15_0 << 3UL) + rsi);
                            var_50 = local_sp_2 + (-8L);
                            *(uint64_t *)var_50 = 4204218UL;
                            indirect_placeholder_1();
                            r12_0 = r12_1;
                            local_sp_1 = var_50;
                            local_sp_4 = var_50;
                            r12_3 = r12_1;
                            if (rax_0 != 0UL) {
                                if (!(((long)var_23 <= (long)(r12_1 << 32UL)) || ((uint64_t)(unsigned char)r13_0_ph193 == 0UL))) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                var_67 = *(uint64_t *)local_sp_2;
                                if (var_67 != 0UL) {
                                    loop_state_var = 3U;
                                    break;
                                }
                                var_68 = local_sp_2 + (-16L);
                                *(uint64_t *)var_68 = 4204261UL;
                                indirect_placeholder_1();
                                local_sp_0 = var_68;
                                local_sp_1 = var_68;
                                if ((uint64_t)(uint32_t)var_67 != 0UL) {
                                    loop_state_var = 3U;
                                    break;
                                }
                                loop_state_var = 2U;
                                break;
                            }
                            var_51 = local_sp_2 + (-16L);
                            *(uint64_t *)var_51 = 4204168UL;
                            var_52 = indirect_placeholder_2(var_49);
                            local_sp_2 = var_51;
                            local_sp_3 = var_51;
                            if ((uint64_t)(uint32_t)var_52 == 0UL) {
                                var_58 = (uint64_t)*(uint32_t *)4276924UL;
                                var_59 = var_58 + 1UL;
                                var_60 = (uint32_t)var_59;
                                var_61 = (uint64_t)var_60;
                                *(uint32_t *)4276924UL = var_60;
                                var_62 = var_59 << 32UL;
                                rax_0 = var_58;
                                r12_1 = var_61;
                                r12_2 = var_61;
                                if ((long)var_62 >= (long)var_23) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                r15_0 = (uint64_t)((long)var_62 >> (long)32UL);
                                continue;
                            }
                            *(unsigned char *)rax_0 = (unsigned char)'\x00';
                            var_53 = *(uint64_t *)(((uint64_t)*(uint32_t *)4276924UL << 3UL) + rsi);
                            *(uint64_t *)(local_sp_2 + (-24L)) = 4204512UL;
                            var_54 = indirect_placeholder_22(var_8, rax_0, var_49, r12_1, rsi, var_53, r13_0_ph193);
                            var_55 = var_54.field_0;
                            *(uint64_t *)(local_sp_2 + (-32L)) = 4204520UL;
                            indirect_placeholder_1();
                            var_56 = (uint64_t)*(uint32_t *)var_55;
                            var_57 = local_sp_2 + (-40L);
                            *(uint64_t *)var_57 = 4204542UL;
                            indirect_placeholder_3(0UL, var_55, 4256892UL, var_40, 125UL, var_56, var_41);
                            local_sp_0 = var_57;
                            r12_0 = var_55;
                            loop_state_var = 2U;
                            break;
                        }
                    switch (loop_state_var) {
                      case 2U:
                        {
                            var_69 = *(uint64_t *)(local_sp_0 + 8UL);
                            *(uint64_t *)(local_sp_0 + (-8L)) = 4204557UL;
                            var_70 = indirect_placeholder_21(var_8, r12_0, 4UL, var_69);
                            var_71 = var_70.field_0;
                            *(uint64_t *)(local_sp_0 + (-16L)) = 4204565UL;
                            indirect_placeholder_1();
                            var_72 = (uint64_t)*(uint32_t *)var_71;
                            var_73 = local_sp_0 + (-24L);
                            *(uint64_t *)var_73 = 4204587UL;
                            var_74 = indirect_placeholder_3(0UL, var_71, 4256911UL, var_40, 125UL, var_72, var_41);
                            *(uint64_t *)((var_73 & (-16L)) + (-8L)) = 4204609UL;
                            indirect_placeholder_18(var_74, var_73, 4275800UL);
                            abort();
                        }
                        break;
                      case 0U:
                        {
                            var_66 = local_sp_4 + (-8L);
                            *(uint64_t *)var_66 = 4204134UL;
                            indirect_placeholder_3(0UL, var_39, storemerge, var_40, 0UL, 0UL, var_41);
                            local_sp_8 = var_66;
                            r12_5 = r12_3;
                            *(uint64_t *)(local_sp_8 + (-8L)) = 4203857UL;
                            indirect_placeholder_18(var_8, r12_5, 125UL);
                            abort();
                        }
                        break;
                      case 1U:
                        {
                            local_sp_4 = local_sp_3;
                            r12_3 = r12_2;
                            storemerge = 4256656UL;
                            local_sp_5 = local_sp_3;
                            if (!((*(uint64_t *)(local_sp_3 + 8UL) == 0UL) || ((long)(r12_2 << 32UL) < (long)var_23))) {
                                var_66 = local_sp_4 + (-8L);
                                *(uint64_t *)var_66 = 4204134UL;
                                indirect_placeholder_3(0UL, var_39, storemerge, var_40, 0UL, 0UL, var_41);
                                local_sp_8 = var_66;
                                r12_5 = r12_3;
                                *(uint64_t *)(local_sp_8 + (-8L)) = 4203857UL;
                                indirect_placeholder_18(var_8, r12_5, 125UL);
                                abort();
                            }
                            var_63 = *(uint64_t *)4276928UL;
                            helper_cc_compute_c_wrapper(r13_0_ph193 + (-1L), 1UL, var_6, 14U);
                            local_sp_6 = local_sp_5;
                            rbx_0 = var_63;
                            while (*(uint64_t *)rbx_0 != 0UL)
                                {
                                    var_64 = rbx_0 + 8UL;
                                    var_65 = local_sp_6 + (-8L);
                                    *(uint64_t *)var_65 = 4204469UL;
                                    indirect_placeholder_1();
                                    local_sp_6 = var_65;
                                    rbx_0 = var_64;
                                }
                            function_dispatcher((unsigned char *)(0UL));
                            return;
                        }
                        break;
                      case 3U:
                        {
                            var_75 = ((uint64_t)*(uint32_t *)4276924UL << 3UL) + rsi;
                            *(uint64_t *)(local_sp_1 + (-8L)) = 4204294UL;
                            indirect_placeholder_1();
                            *(uint64_t *)(local_sp_1 + (-16L)) = 4204299UL;
                            indirect_placeholder_1();
                            var_76 = *(uint64_t *)(((uint64_t)*(uint32_t *)4276924UL << 3UL) + rsi);
                            var_77 = (*(uint32_t *)var_75 == 2U) ? 127UL : 126UL;
                            *(uint64_t *)(local_sp_1 + (-24L)) = 4204326UL;
                            var_78 = indirect_placeholder_20(var_8, 0UL, var_49, var_77, rsi, var_76, r13_0_ph193);
                            var_79 = var_78.field_0;
                            *(uint64_t *)(local_sp_1 + (-32L)) = 4204334UL;
                            indirect_placeholder_1();
                            var_80 = (uint64_t)*(uint32_t *)var_79;
                            *(uint64_t *)(local_sp_1 + (-40L)) = 4204353UL;
                            indirect_placeholder_3(0UL, var_79, 4257261UL, var_40, 0UL, var_80, var_41);
                            function_dispatcher((unsigned char *)(0UL));
                            return;
                        }
                        break;
                    }
                }
                break;
              case 1U:
                {
                    var_63 = *(uint64_t *)4276928UL;
                    helper_cc_compute_c_wrapper(r13_0_ph193 + (-1L), 1UL, var_6, 14U);
                    local_sp_6 = local_sp_5;
                    rbx_0 = var_63;
                    while (*(uint64_t *)rbx_0 != 0UL)
                        {
                            var_64 = rbx_0 + 8UL;
                            var_65 = local_sp_6 + (-8L);
                            *(uint64_t *)var_65 = 4204469UL;
                            indirect_placeholder_1();
                            local_sp_6 = var_65;
                            rbx_0 = var_64;
                        }
                    function_dispatcher((unsigned char *)(0UL));
                    return;
                }
                break;
              case 2U:
                {
                    local_sp_4 = local_sp_3;
                    r12_3 = r12_2;
                    storemerge = 4256656UL;
                    local_sp_5 = local_sp_3;
                    if ((*(uint64_t *)(local_sp_3 + 8UL) == 0UL) || ((long)(r12_2 << 32UL) < (long)var_23)) {
                        var_66 = local_sp_4 + (-8L);
                        *(uint64_t *)var_66 = 4204134UL;
                        indirect_placeholder_3(0UL, var_39, storemerge, var_40, 0UL, 0UL, var_41);
                        local_sp_8 = var_66;
                        r12_5 = r12_3;
                        *(uint64_t *)(local_sp_8 + (-8L)) = 4203857UL;
                        indirect_placeholder_18(var_8, r12_5, 125UL);
                        abort();
                    }
                    var_63 = *(uint64_t *)4276928UL;
                    helper_cc_compute_c_wrapper(r13_0_ph193 + (-1L), 1UL, var_6, 14U);
                    local_sp_6 = local_sp_5;
                    rbx_0 = var_63;
                    while (*(uint64_t *)rbx_0 != 0UL)
                        {
                            var_64 = rbx_0 + 8UL;
                            var_65 = local_sp_6 + (-8L);
                            *(uint64_t *)var_65 = 4204469UL;
                            indirect_placeholder_1();
                            local_sp_6 = var_65;
                            rbx_0 = var_64;
                        }
                    function_dispatcher((unsigned char *)(0UL));
                    return;
                }
                break;
            }
        }
        break;
    }
}
