typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder_2(void);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_rax(void);
void bb_make_format(uint64_t rcx, uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t rax_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t local_sp_1;
    uint64_t rbx_0;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t rax_1;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t rbp_1;
    bool var_12;
    unsigned char var_13;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbp();
    var_3 = init_r15();
    var_4 = init_r14();
    var_5 = init_r12();
    var_6 = init_rbx();
    var_7 = init_cc_src2();
    var_8 = init_r13();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_8;
    var_9 = rsi + rdi;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    var_10 = rdi + 1UL;
    *(uint64_t *)(var_0 + (-48L)) = var_6;
    *(uint64_t *)(var_0 + (-64L)) = rcx;
    var_11 = helper_cc_compute_c_wrapper(var_10 - var_9, var_9, var_7, 17U);
    rax_0 = 0UL;
    rax_1 = 0UL;
    rbp_1 = var_10;
    rbx_0 = var_10;
    if (var_11 == 0UL) {
        indirect_placeholder_2();
        return;
    }
    var_12 = (var_1 == 0UL);
    local_sp_1 = var_0 + (-72L);
    while (1U)
        {
            var_13 = *(unsigned char *)rbx_0;
            *(uint64_t *)(local_sp_1 + (-8L)) = 4205753UL;
            indirect_placeholder_2();
            if (var_12) {
                var_14 = local_sp_1 + (-16L);
                *(uint64_t *)var_14 = 4205707UL;
                indirect_placeholder_2();
                *(unsigned char *)rbp_1 = var_13;
                var_15 = rbx_0 + 1UL;
                rbx_0 = var_15;
                local_sp_1 = var_14;
                if (var_9 <= var_15) {
                    loop_state_var = 0U;
                    break;
                }
                rbp_1 = rbp_1 + 1UL;
                continue;
            }
            if (var_9 <= rbx_0) {
                loop_state_var = 0U;
                break;
            }
            if (((rbp_1 + 15UL) - rbx_0) <= 30UL) {
                loop_state_var = 1U;
                break;
            }
            if ((var_9 + (rbx_0 ^ (-1L))) <= 14UL) {
                loop_state_var = 1U;
                break;
            }
            var_18 = var_9 - rbx_0;
            var_19 = var_18 & (-16L);
            loop_state_var = 2U;
            break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            break;
        }
        break;
      case 1U:
        {
            var_16 = var_9 - rbx_0;
            *(unsigned char *)(rax_1 + rbp_1) = *(unsigned char *)(rax_1 + rbx_0);
            var_17 = rax_1 + 1UL;
            rax_1 = var_17;
            do {
                *(unsigned char *)(rax_1 + rbp_1) = *(unsigned char *)(rax_1 + rbx_0);
                var_17 = rax_1 + 1UL;
                rax_1 = var_17;
            } while (var_17 != var_16);
        }
        break;
      case 2U:
        {
            var_20 = rax_0 + rbx_0;
            var_21 = *(uint64_t *)var_20;
            var_22 = *(uint64_t *)(var_20 + 8UL);
            var_23 = rax_0 + rbp_1;
            *(uint64_t *)var_23 = var_21;
            *(uint64_t *)(var_23 + 8UL) = var_22;
            var_24 = rax_0 + 16UL;
            rax_0 = var_24;
            do {
                var_20 = rax_0 + rbx_0;
                var_21 = *(uint64_t *)var_20;
                var_22 = *(uint64_t *)(var_20 + 8UL);
                var_23 = rax_0 + rbp_1;
                *(uint64_t *)var_23 = var_21;
                *(uint64_t *)(var_23 + 8UL) = var_22;
                var_24 = rax_0 + 16UL;
                rax_0 = var_24;
            } while (var_24 != var_19);
            var_25 = var_19 + rbp_1;
            var_26 = var_19 + rbx_0;
            *(unsigned char *)var_25 = *(unsigned char *)var_26;
            var_27 = var_26 + 1UL;
            *(unsigned char *)(var_25 + 1UL) = *(unsigned char *)var_27;
            var_28 = var_26 + 2UL;
            *(unsigned char *)(var_25 + 2UL) = *(unsigned char *)var_28;
            var_29 = var_26 + 3UL;
            *(unsigned char *)(var_25 + 3UL) = *(unsigned char *)var_29;
            var_30 = var_26 + 4UL;
            *(unsigned char *)(var_25 + 4UL) = *(unsigned char *)var_30;
            var_31 = var_26 + 5UL;
            *(unsigned char *)(var_25 + 5UL) = *(unsigned char *)var_31;
            var_32 = var_26 + 6UL;
            *(unsigned char *)(var_25 + 6UL) = *(unsigned char *)var_32;
            var_33 = var_26 + 7UL;
            *(unsigned char *)(var_25 + 7UL) = *(unsigned char *)var_33;
            var_34 = var_26 + 8UL;
            *(unsigned char *)(var_25 + 8UL) = *(unsigned char *)var_34;
            var_35 = var_26 + 9UL;
            *(unsigned char *)(var_25 + 9UL) = *(unsigned char *)var_35;
            var_36 = var_26 + 10UL;
            *(unsigned char *)(var_25 + 10UL) = *(unsigned char *)var_36;
            var_37 = var_26 + 11UL;
            *(unsigned char *)(var_25 + 11UL) = *(unsigned char *)var_37;
            var_38 = var_26 + 12UL;
            *(unsigned char *)(var_25 + 12UL) = *(unsigned char *)var_38;
            var_39 = var_26 + 13UL;
            *(unsigned char *)(var_25 + 13UL) = *(unsigned char *)var_39;
            var_40 = var_26 + 14UL;
            if (var_18 != var_19 & var_9 <= var_27 & var_9 <= var_28 & var_9 <= var_29 & var_9 <= var_30 & var_9 <= var_31 & var_9 <= var_32 & var_9 <= var_33 & var_9 <= var_34 & var_9 <= var_35 & var_9 <= var_36 & var_9 <= var_37 & var_9 <= var_38 & var_9 <= var_39 & var_9 > var_40) {
                *(unsigned char *)(var_25 + 14UL) = *(unsigned char *)var_40;
            }
        }
        break;
    }
}
