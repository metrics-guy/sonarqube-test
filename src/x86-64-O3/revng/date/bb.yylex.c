typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_movq_mm_T0_xmm_wrapper_169_ret_type;
struct type_3;
struct helper_movq_mm_T0_xmm_wrapper_170_ret_type;
struct helper_punpcklqdq_xmm_wrapper_ret_type;
struct type_7;
struct indirect_placeholder_40_ret_type;
struct indirect_placeholder_41_ret_type;
struct helper_movq_mm_T0_xmm_wrapper_169_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_3 {
};
struct helper_movq_mm_T0_xmm_wrapper_170_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_punpcklqdq_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
};
struct type_7 {
};
struct indirect_placeholder_40_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_41_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1);
extern uint64_t init_rdx(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_1(void);
extern uint64_t init_r9(void);
extern struct helper_movq_mm_T0_xmm_wrapper_169_ret_type helper_movq_mm_T0_xmm_wrapper_169(struct type_3 *param_0, uint64_t param_1);
extern struct helper_movq_mm_T0_xmm_wrapper_170_ret_type helper_movq_mm_T0_xmm_wrapper_170(struct type_3 *param_0, uint64_t param_1);
extern struct helper_punpcklqdq_xmm_wrapper_ret_type helper_punpcklqdq_xmm_wrapper(struct type_7 *param_0, struct type_3 *param_1, struct type_3 *param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_40_ret_type indirect_placeholder_40(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_41_ret_type indirect_placeholder_41(uint64_t param_0, uint64_t param_1);
uint64_t bb_yylex(uint64_t rdi, uint64_t rsi) {
    unsigned char var_28;
    struct helper_punpcklqdq_xmm_wrapper_ret_type var_187;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t local_sp_19;
    uint64_t r12_2;
    uint64_t var_38;
    uint64_t *var_39;
    uint64_t var_40;
    uint64_t r8_0;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_32;
    uint64_t var_33;
    struct indirect_placeholder_41_ret_type var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t rbx_0;
    uint64_t local_sp_12;
    uint64_t rbx_1;
    uint32_t var_162;
    uint64_t rbx_1_ph;
    uint32_t var_176;
    uint64_t local_sp_11;
    uint64_t local_sp_1_ph;
    uint64_t local_sp_0;
    uint64_t rax_0;
    uint64_t r12_0;
    uint64_t local_sp_2;
    uint64_t local_sp_10;
    uint64_t var_157;
    uint64_t r14_0_ph;
    uint64_t local_sp_1;
    uint64_t var_158;
    uint64_t var_159;
    uint64_t var_160;
    uint64_t var_161;
    uint64_t var_163;
    uint64_t var_164;
    uint64_t var_165;
    uint64_t var_166;
    uint64_t var_150;
    uint64_t var_151;
    uint32_t var_152;
    uint64_t var_153;
    uint64_t r14_5;
    uint64_t var_154;
    uint64_t var_155;
    uint32_t var_156;
    uint64_t var_143;
    uint32_t var_144;
    uint64_t var_145;
    uint64_t var_146;
    uint64_t var_147;
    uint64_t var_148;
    uint64_t var_149;
    uint64_t local_sp_8;
    uint32_t var_135;
    uint64_t var_136;
    uint32_t var_137;
    uint64_t var_138;
    uint64_t var_139;
    uint64_t var_140;
    uint32_t var_141;
    uint64_t var_142;
    uint64_t local_sp_7;
    uint32_t var_127;
    uint64_t var_128;
    uint32_t var_129;
    uint64_t var_130;
    uint64_t var_131;
    uint64_t var_132;
    uint32_t var_133;
    uint64_t var_134;
    uint64_t local_sp_6;
    uint32_t var_119;
    uint64_t var_120;
    uint32_t var_121;
    uint64_t var_122;
    uint64_t var_123;
    uint64_t var_124;
    uint32_t var_125;
    uint64_t var_126;
    uint64_t local_sp_5;
    uint32_t var_111;
    uint64_t var_112;
    uint32_t var_113;
    uint64_t var_114;
    uint64_t var_115;
    uint64_t var_116;
    uint32_t var_117;
    uint64_t var_118;
    uint64_t local_sp_4;
    uint32_t var_103;
    uint64_t var_104;
    uint32_t var_105;
    uint64_t var_106;
    uint64_t var_107;
    uint64_t var_108;
    uint32_t var_109;
    uint64_t var_110;
    uint64_t local_sp_3;
    uint32_t var_95;
    uint64_t var_96;
    uint32_t var_97;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t var_100;
    uint32_t var_101;
    uint64_t var_102;
    uint64_t var_167;
    uint64_t var_168;
    uint64_t r12_1;
    uint64_t rbx_2;
    uint64_t var_169;
    uint64_t rax_1;
    uint64_t rbx_3;
    uint64_t var_170;
    uint64_t rax_2;
    uint64_t rbx_4;
    uint64_t rax_3;
    uint64_t rbx_5;
    uint64_t var_171;
    uint64_t rax_4;
    uint64_t rbx_6;
    uint64_t var_172;
    uint64_t rax_5;
    uint64_t rbx_7;
    uint64_t var_173;
    uint64_t r14_1_in;
    uint64_t rbx_8;
    uint64_t var_174;
    uint32_t var_175;
    uint64_t local_sp_9;
    uint64_t r14_2;
    uint64_t rbx_9;
    uint64_t var_177;
    uint64_t var_178;
    uint64_t var_179;
    uint64_t var_180;
    uint64_t r14_3;
    uint64_t rbx_10;
    uint64_t var_189;
    uint64_t r14_4;
    uint64_t rbx_11;
    uint64_t var_181;
    struct helper_movq_mm_T0_xmm_wrapper_170_ret_type var_182;
    uint64_t var_183;
    struct helper_movq_mm_T0_xmm_wrapper_169_ret_type var_184;
    uint64_t var_185;
    uint64_t var_186;
    uint64_t var_188;
    unsigned char r12_3_in;
    uint32_t var_89;
    uint64_t var_90;
    uint32_t var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_88;
    unsigned __int128 var_81;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t local_sp_15;
    uint64_t rdx_0_be;
    uint64_t rdx_6;
    uint64_t rbx_12;
    uint64_t rdx_0;
    uint64_t var_47;
    unsigned char var_48;
    uint64_t var_50;
    uint64_t r14_9;
    uint64_t var_49;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t rbx_13_in;
    uint64_t r13_0;
    uint64_t rbx_13;
    uint64_t var_27;
    uint64_t r13_1;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    unsigned __int128 var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t local_sp_17;
    uint64_t local_sp_13;
    uint64_t r14_7;
    uint64_t r15_0;
    uint64_t cc_src2_1;
    uint64_t rbx_14;
    uint64_t r13_2;
    uint64_t cc_src2_0;
    uint64_t local_sp_14;
    uint32_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint32_t var_190;
    uint64_t var_191;
    bool var_192;
    uint64_t var_193;
    uint64_t rax_6_in;
    uint64_t local_sp_16;
    uint64_t r15_1;
    uint64_t rdx_2;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    unsigned char var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t *var_79;
    uint64_t var_80;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t local_sp_21;
    uint64_t cc_src2_3;
    uint64_t rdx_4;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t r14_6;
    uint64_t rbx_15;
    uint64_t r15_2;
    uint64_t rdx_3;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    unsigned char var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t *var_67;
    uint64_t var_68;
    uint64_t var_54;
    uint64_t local_sp_18;
    uint64_t rax_6;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t rax_7;
    uint64_t r14_8;
    uint64_t rbx_16;
    uint64_t rdx_5;
    uint64_t cc_src2_2;
    uint64_t local_sp_21_ph;
    uint64_t local_sp_20;
    uint64_t r12_4_in;
    uint64_t r12_4;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t r14_9_ph;
    uint64_t rbx_17_ph;
    uint64_t rbx_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    unsigned char var_21;
    unsigned char var_22;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r15();
    var_3 = init_r14();
    var_4 = init_r12();
    var_5 = init_rbx();
    var_6 = init_rdx();
    var_7 = init_r9();
    var_8 = init_r13();
    var_9 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_8;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_1;
    *(uint64_t *)(var_0 + (-48L)) = var_5;
    var_10 = (uint64_t *)rsi;
    var_11 = *var_10;
    *(uint64_t *)(var_0 + (-96L)) = rdi;
    var_12 = (uint64_t)*(unsigned char *)var_11;
    var_13 = var_0 + (-128L);
    *(uint64_t *)var_13 = 4218264UL;
    var_14 = indirect_placeholder_2(var_12);
    local_sp_19 = var_13;
    r8_0 = 63UL;
    rdx_0 = 0UL;
    rdx_2 = 0UL;
    rdx_3 = 0UL;
    rax_7 = var_14;
    r14_8 = var_12;
    rbx_16 = var_11;
    rdx_5 = var_6;
    cc_src2_2 = var_9;
    while (1U)
        {
            rdx_6 = rdx_5;
            cc_src2_3 = cc_src2_2;
            rdx_5 = 0UL;
            local_sp_21_ph = local_sp_19;
            local_sp_20 = local_sp_19;
            r12_4_in = rbx_16;
            r14_9_ph = r14_8;
            rbx_17_ph = rbx_16;
            if ((uint64_t)(unsigned char)rax_7 == 0UL) {
                r12_4 = r12_4_in + 1UL;
                *var_10 = r12_4;
                var_15 = (uint64_t)*(unsigned char *)r12_4;
                var_16 = local_sp_20 + (-8L);
                *(uint64_t *)var_16 = 4218299UL;
                var_17 = indirect_placeholder_2(var_15);
                local_sp_21_ph = var_16;
                local_sp_20 = var_16;
                r12_4_in = r12_4;
                r14_9_ph = var_15;
                rbx_17_ph = r12_4;
                do {
                    r12_4 = r12_4_in + 1UL;
                    *var_10 = r12_4;
                    var_15 = (uint64_t)*(unsigned char *)r12_4;
                    var_16 = local_sp_20 + (-8L);
                    *(uint64_t *)var_16 = 4218299UL;
                    var_17 = indirect_placeholder_2(var_15);
                    local_sp_21_ph = var_16;
                    local_sp_20 = var_16;
                    r12_4_in = r12_4;
                    r14_9_ph = var_15;
                    rbx_17_ph = r12_4;
                } while ((uint64_t)(unsigned char)var_17 != 0UL);
            }
            local_sp_21 = local_sp_21_ph;
            r14_9 = r14_9_ph;
            rbx_17 = rbx_17_ph;
            while (1U)
                {
                    var_18 = (uint64_t)(uint32_t)r14_9;
                    var_19 = local_sp_21 + (-8L);
                    *(uint64_t *)var_19 = 4218314UL;
                    var_20 = indirect_placeholder_2(var_18);
                    var_21 = (unsigned char)r14_9;
                    var_22 = var_21 + '\xd5';
                    r12_2 = var_18;
                    rbx_12 = rbx_17;
                    rbx_13_in = rbx_17;
                    cc_src2_1 = cc_src2_3;
                    rax_6_in = rbx_17;
                    local_sp_16 = var_19;
                    rbx_15 = rbx_17;
                    local_sp_18 = var_19;
                    cc_src2_2 = cc_src2_3;
                    if ((uint64_t)(unsigned char)var_20 != 0UL) {
                        rdx_4 = (rdx_6 & (-256L)) | ((uint64_t)(var_21 + '\xd3') == 0UL);
                        if ((uint64_t)(var_22 & '\xfd') != 0UL) {
                            *(uint32_t *)(local_sp_21 + 12UL) = 0U;
                            r14_6 = (uint64_t)var_21;
                            loop_state_var = 1U;
                            break;
                        }
                    }
                    r8_0 = 0UL;
                    if ((uint64_t)(var_22 & '\xfd') != 0UL) {
                        loop_state_var = 2U;
                        break;
                    }
                    rdx_4 = (rdx_6 & (-256L)) | ((uint64_t)(var_21 + '\xd3') == 0UL);
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 2U:
                {
                    var_23 = local_sp_21 + (-16L);
                    *(uint64_t *)var_23 = 4218536UL;
                    var_24 = indirect_placeholder_2(var_18);
                    local_sp_12 = var_23;
                    if ((uint64_t)(unsigned char)var_24 != 0UL) {
                        var_25 = local_sp_21 + 16UL;
                        var_26 = local_sp_21 + 35UL;
                        r13_0 = var_25;
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    if ((uint64_t)(var_21 + '\xd8') != 0UL) {
                        *var_10 = (rbx_17 + 1UL);
                        var_44 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)rbx_17;
                        *(uint64_t *)(local_sp_21 + (-24L)) = 4219551UL;
                        var_45 = indirect_placeholder_2(var_44);
                        var_46 = (uint64_t)(unsigned char)var_45;
                        r8_0 = var_46;
                        loop_state_var = 2U;
                        switch_state_var = 1;
                        break;
                    }
                    while (1U)
                        {
                            var_47 = rbx_12 + 1UL;
                            *var_10 = var_47;
                            var_48 = *(unsigned char *)rbx_12;
                            rbx_12 = var_47;
                            rbx_16 = var_47;
                            if (var_48 != '\x00') {
                                loop_state_var = 1U;
                                break;
                            }
                            if ((uint64_t)(var_48 + '\xd8') == 0UL) {
                                var_50 = rdx_0 + 1UL;
                                rdx_0_be = var_50;
                                if (var_50 != 0UL) {
                                    loop_state_var = 0U;
                                    break;
                                }
                            }
                            var_49 = rdx_0 + ((uint64_t)(var_48 + '\xd7') == 0UL);
                            rdx_0_be = var_49;
                            if (var_49 != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            rdx_0 = rdx_0_be;
                            continue;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 0U:
                        {
                            var_51 = (uint64_t)*(unsigned char *)var_47;
                            var_52 = local_sp_21 + (-24L);
                            *(uint64_t *)var_52 = 4218628UL;
                            var_53 = indirect_placeholder_2(var_51);
                            local_sp_19 = var_52;
                            rax_7 = var_53;
                            r14_8 = var_51;
                            continue;
                        }
                        break;
                      case 1U:
                        {
                            loop_state_var = 2U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
                break;
              case 1U:
                {
                    local_sp_17 = local_sp_16;
                    r14_7 = r14_6;
                    rbx_14 = rbx_15;
                    cc_src2_0 = cc_src2_1;
                    r15_2 = rbx_15;
                    loop_state_var = 3U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 0U:
                {
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 2U:
        {
            return r8_0;
        }
        break;
      case 0U:
        {
            while (1U)
                {
                    rbx_13 = rbx_13_in + 1UL;
                    var_27 = helper_cc_compute_c_wrapper(r13_0 - var_26, var_26, cc_src2_3, 17U);
                    rbx_13_in = rbx_13;
                    r13_1 = r13_0;
                    if (var_27 == 0UL) {
                        *(unsigned char *)r13_0 = (unsigned char)r12_2;
                        r13_1 = r13_0 + 1UL;
                    }
                    *var_10 = rbx_13;
                    var_28 = *(unsigned char *)rbx_13;
                    var_29 = (uint64_t)var_28;
                    var_30 = local_sp_12 + (-8L);
                    *(uint64_t *)var_30 = 4219616UL;
                    var_31 = indirect_placeholder_2(var_29);
                    local_sp_12 = var_30;
                    r12_2 = var_29;
                    r13_0 = r13_1;
                    if (!((uint64_t)(unsigned char)var_31 == 0UL && (uint64_t)(var_28 + '\xd2') == 0UL)) {
                        continue;
                    }
                    break;
                }
            *(unsigned char *)r13_1 = (unsigned char)'\x00';
            var_32 = local_sp_12 + 24UL;
            var_33 = local_sp_12 + (-16L);
            *(uint64_t *)var_33 = 4219644UL;
            var_34 = indirect_placeholder_41(rsi, var_32);
            var_35 = var_34.field_0;
            var_36 = var_34.field_1;
            var_37 = var_34.field_2;
            if (var_35 == 0UL) {
                var_38 = (uint64_t)*(uint32_t *)(var_35 + 12UL);
                var_39 = *(uint64_t **)(local_sp_12 + 8UL);
                var_40 = (uint64_t)*(uint32_t *)(var_35 + 8UL);
                *var_39 = var_38;
                r8_0 = var_40;
            } else {
                if (*(unsigned char *)(rsi + 217UL) == '\x00') {
                    var_41 = local_sp_12 + 16UL;
                    *(uint32_t *)var_33 = 63U;
                    var_42 = local_sp_12 + (-24L);
                    *(uint64_t *)var_42 = 4219758UL;
                    indirect_placeholder_40(0UL, var_36, var_37, var_7, 4304925UL, var_41, 63UL);
                    var_43 = (uint64_t)*(uint32_t *)var_42;
                    r8_0 = var_43;
                }
            }
        }
        break;
      case 1U:
        {
            while (1U)
                {
                    var_72 = (uint64_t)((long)(206158430208UL - (r14_5 << 32UL)) >> (long)32UL);
                    var_73 = rdx_2 + var_72;
                    *(uint64_t *)(local_sp_15 + 8UL) = var_73;
                    *(uint64_t *)local_sp_15 = var_73;
                    var_74 = helper_cc_compute_all_wrapper(var_73, var_72, 0UL, 9U);
                    r15_0 = r15_1;
                    if ((uint64_t)((uint16_t)var_74 & (unsigned short)2048U) != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_75 = r15_1 + 1UL;
                    var_76 = *(unsigned char *)var_75;
                    var_77 = (uint64_t)var_76;
                    var_78 = local_sp_15 + (-8L);
                    var_79 = (uint64_t *)var_78;
                    *var_79 = 4218707UL;
                    var_80 = indirect_placeholder_2(var_77);
                    r14_5 = var_77;
                    r12_3_in = var_76;
                    local_sp_15 = var_78;
                    local_sp_13 = var_78;
                    r13_2 = var_75;
                    r15_1 = var_75;
                    if ((uint64_t)(unsigned char)var_80 != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_81 = (unsigned __int128)*var_79 * 10ULL;
                    var_82 = (uint64_t)var_81;
                    var_83 = helper_cc_compute_all_wrapper(var_82, (uint64_t)((long)var_82 >> (long)63UL) - (uint64_t)(var_81 >> 64ULL), 0UL, 5U);
                    rdx_2 = var_82;
                    if ((uint64_t)((uint16_t)var_83 & (unsigned short)2048U) == 0UL) {
                        continue;
                    }
                    loop_state_var = 1U;
                    break;
                }
            switch (loop_state_var) {
              case 1U:
                {
                    return r8_0;
                }
                break;
              case 0U:
                {
                    local_sp_14 = local_sp_13;
                    if (((r12_3_in + '\xd4') & '\xfd') == '\x00') {
                        var_190 = *(uint32_t *)(local_sp_14 + 20UL);
                        var_191 = *(uint64_t *)(local_sp_14 + 24UL);
                        *(unsigned char *)var_191 = (unsigned char)(var_190 >> 31U);
                        *(uint64_t *)(var_191 + 8UL) = *(uint64_t *)(local_sp_14 + 8UL);
                        var_192 = (var_190 == 0U);
                        *(uint64_t *)(var_191 + 16UL) = (r13_2 - rbx_14);
                        var_193 = var_192 ? 275UL : 274UL;
                        *var_10 = r13_2;
                        r8_0 = var_193;
                    } else {
                        var_84 = (uint32_t)(uint64_t)*(unsigned char *)(r13_2 + 1UL);
                        var_85 = (uint64_t)var_84;
                        var_86 = local_sp_13 + (-8L);
                        *(uint64_t *)var_86 = 4218811UL;
                        var_87 = indirect_placeholder_2(var_85);
                        local_sp_14 = var_86;
                        if ((uint64_t)(unsigned char)var_87 != 0UL) {
                            *(uint64_t *)(local_sp_13 + (-16L)) = 4218824UL;
                            var_88 = indirect_placeholder_1();
                            if ((uint64_t)(unsigned char)var_88 == 0UL) {
                                return r8_0;
                            }
                            var_89 = (uint32_t)(uint64_t)*(unsigned char *)(r15_0 + 3UL);
                            var_90 = (uint64_t)var_89;
                            var_91 = var_84 + (-48);
                            var_92 = (uint64_t)var_91;
                            var_93 = local_sp_13 + (-24L);
                            *(uint64_t *)var_93 = 4218859UL;
                            var_94 = indirect_placeholder_2(var_90);
                            local_sp_2 = var_93;
                            r8_0 = 276UL;
                            if ((uint64_t)(unsigned char)var_94 == 0UL) {
                                var_167 = r13_2 + 2UL;
                                var_168 = (uint64_t)(((uint32_t)(uint64_t)((long)(var_92 << 32UL) >> (long)32UL) * 100U) & (-4));
                                r12_1 = var_168;
                                rbx_2 = var_167;
                                var_169 = (uint64_t)(((uint32_t)r12_1 * 10U) & (-4));
                                local_sp_3 = local_sp_2;
                                rax_1 = var_169;
                                rbx_3 = rbx_2;
                                var_170 = (uint64_t)(((uint32_t)rax_1 * 10U) & (-4));
                                local_sp_4 = local_sp_3;
                                rax_2 = var_170;
                                rbx_4 = rbx_3;
                                local_sp_5 = local_sp_4;
                                rax_3 = (uint64_t)(((uint32_t)rax_2 * 10U) & (-4));
                                rbx_5 = rbx_4;
                            } else {
                                var_95 = ((var_91 * 10U) + var_89) + (-48);
                                var_96 = r13_2 + 3UL;
                                var_97 = (uint32_t)(uint64_t)*(unsigned char *)var_96;
                                var_98 = (uint64_t)var_97;
                                var_99 = local_sp_13 + (-32L);
                                *(uint64_t *)var_99 = 4218899UL;
                                var_100 = indirect_placeholder_2(var_98);
                                var_101 = var_95 * 10U;
                                var_102 = (uint64_t)(var_101 & (-2));
                                local_sp_2 = var_99;
                                r12_1 = var_102;
                                rbx_2 = var_96;
                                if ((uint64_t)(unsigned char)var_100 == 0UL) {
                                    var_169 = (uint64_t)(((uint32_t)r12_1 * 10U) & (-4));
                                    local_sp_3 = local_sp_2;
                                    rax_1 = var_169;
                                    rbx_3 = rbx_2;
                                    var_170 = (uint64_t)(((uint32_t)rax_1 * 10U) & (-4));
                                    local_sp_4 = local_sp_3;
                                    rax_2 = var_170;
                                    rbx_4 = rbx_3;
                                    local_sp_5 = local_sp_4;
                                    rax_3 = (uint64_t)(((uint32_t)rax_2 * 10U) & (-4));
                                    rbx_5 = rbx_4;
                                } else {
                                    var_103 = (var_101 + var_97) + (-48);
                                    var_104 = r13_2 + 4UL;
                                    var_105 = (uint32_t)(uint64_t)*(unsigned char *)var_104;
                                    var_106 = (uint64_t)var_105;
                                    var_107 = local_sp_13 + (-40L);
                                    *(uint64_t *)var_107 = 4218942UL;
                                    var_108 = indirect_placeholder_2(var_106);
                                    var_109 = var_103 * 10U;
                                    var_110 = (uint64_t)(var_109 & (-2));
                                    local_sp_3 = var_107;
                                    rax_1 = var_110;
                                    rbx_3 = var_104;
                                    if ((uint64_t)(unsigned char)var_108 == 0UL) {
                                        var_170 = (uint64_t)(((uint32_t)rax_1 * 10U) & (-4));
                                        local_sp_4 = local_sp_3;
                                        rax_2 = var_170;
                                        rbx_4 = rbx_3;
                                        local_sp_5 = local_sp_4;
                                        rax_3 = (uint64_t)(((uint32_t)rax_2 * 10U) & (-4));
                                        rbx_5 = rbx_4;
                                    } else {
                                        var_111 = (var_109 + var_105) + (-48);
                                        var_112 = r13_2 + 5UL;
                                        var_113 = (uint32_t)(uint64_t)*(unsigned char *)var_112;
                                        var_114 = (uint64_t)var_113;
                                        var_115 = local_sp_13 + (-48L);
                                        *(uint64_t *)var_115 = 4218984UL;
                                        var_116 = indirect_placeholder_3(var_110, var_114);
                                        var_117 = var_111 * 10U;
                                        var_118 = (uint64_t)(var_117 & (-2));
                                        local_sp_4 = var_115;
                                        rax_2 = var_118;
                                        rbx_4 = var_112;
                                        if ((uint64_t)(unsigned char)var_116 != 0UL) {
                                            var_119 = (var_117 + var_113) + (-48);
                                            var_120 = r13_2 + 6UL;
                                            var_121 = (uint32_t)(uint64_t)*(unsigned char *)var_120;
                                            var_122 = (uint64_t)var_121;
                                            var_123 = local_sp_13 + (-56L);
                                            *(uint64_t *)var_123 = 4219026UL;
                                            var_124 = indirect_placeholder_3(var_118, var_122);
                                            var_125 = var_119 * 10U;
                                            var_126 = (uint64_t)(var_125 & (-2));
                                            local_sp_5 = var_123;
                                            rax_3 = var_126;
                                            rbx_5 = var_120;
                                            if ((uint64_t)(unsigned char)var_124 != 0UL) {
                                                var_127 = (var_125 + var_121) + (-48);
                                                var_128 = r13_2 + 7UL;
                                                var_129 = (uint32_t)(uint64_t)*(unsigned char *)var_128;
                                                var_130 = (uint64_t)var_129;
                                                var_131 = local_sp_13 + (-64L);
                                                *(uint64_t *)var_131 = 4219068UL;
                                                var_132 = indirect_placeholder_3(var_126, var_130);
                                                var_133 = var_127 * 10U;
                                                var_134 = (uint64_t)(var_133 & (-2));
                                                local_sp_6 = var_131;
                                                rax_4 = var_134;
                                                rbx_6 = var_128;
                                                if ((uint64_t)(unsigned char)var_132 == 0UL) {
                                                    var_172 = (uint64_t)(((uint32_t)rax_4 * 10U) & (-4));
                                                    local_sp_7 = local_sp_6;
                                                    rax_5 = var_172;
                                                    rbx_7 = rbx_6;
                                                    var_173 = (rax_5 * 10UL) & 8589934588UL;
                                                    local_sp_8 = local_sp_7;
                                                    r14_1_in = var_173;
                                                    rbx_8 = rbx_7;
                                                    var_174 = (uint64_t)((uint32_t)r14_1_in & (-2));
                                                    var_175 = *(uint32_t *)(local_sp_8 + 20UL);
                                                    var_176 = var_175;
                                                    local_sp_10 = local_sp_8;
                                                    local_sp_9 = local_sp_8;
                                                    r14_2 = var_174;
                                                    rbx_9 = rbx_8;
                                                    r14_3 = var_174;
                                                    rbx_10 = rbx_8;
                                                    if (var_175 != 4294967295U) {
                                                        local_sp_11 = local_sp_10;
                                                        r14_4 = r14_3;
                                                        rbx_11 = rbx_10;
                                                        if (r14_3 != 0UL) {
                                                            var_189 = *(uint64_t *)(local_sp_10 + 24UL);
                                                            *(uint64_t *)var_189 = *(uint64_t *)(local_sp_10 + 8UL);
                                                            *(uint64_t *)(var_189 + 8UL) = 0UL;
                                                            *var_10 = rbx_10;
                                                            return r8_0;
                                                        }
                                                    }
                                                    var_177 = *(uint64_t *)(local_sp_9 + 24UL);
                                                    var_178 = *(uint64_t *)(local_sp_9 + 8UL);
                                                    var_179 = helper_cc_compute_c_wrapper((uint64_t)var_176 + (-1L), 1UL, cc_src2_0, 16U);
                                                    *(uint64_t *)var_177 = var_178;
                                                    *(uint64_t *)(var_177 + 8UL) = (uint64_t)((long)(r14_2 << 32UL) >> (long)32UL);
                                                    var_180 = (uint64_t)((uint32_t)var_179 + 276U);
                                                    *var_10 = rbx_9;
                                                    r8_0 = var_180;
                                                    return r8_0;
                                                }
                                                var_135 = (var_133 + var_129) + (-48);
                                                var_136 = r13_2 + 8UL;
                                                var_137 = (uint32_t)(uint64_t)*(unsigned char *)var_136;
                                                var_138 = (uint64_t)var_137;
                                                var_139 = local_sp_13 + (-72L);
                                                *(uint64_t *)var_139 = 4219110UL;
                                                var_140 = indirect_placeholder_3(var_134, var_138);
                                                var_141 = var_135 * 10U;
                                                var_142 = (uint64_t)(var_141 & (-2));
                                                local_sp_7 = var_139;
                                                rax_5 = var_142;
                                                rbx_7 = var_136;
                                                if ((uint64_t)(unsigned char)var_140 == 0UL) {
                                                    var_173 = (rax_5 * 10UL) & 8589934588UL;
                                                    local_sp_8 = local_sp_7;
                                                    r14_1_in = var_173;
                                                    rbx_8 = rbx_7;
                                                    var_174 = (uint64_t)((uint32_t)r14_1_in & (-2));
                                                    var_175 = *(uint32_t *)(local_sp_8 + 20UL);
                                                    var_176 = var_175;
                                                    local_sp_10 = local_sp_8;
                                                    local_sp_9 = local_sp_8;
                                                    r14_2 = var_174;
                                                    rbx_9 = rbx_8;
                                                    r14_3 = var_174;
                                                    rbx_10 = rbx_8;
                                                    if (var_175 != 4294967295U) {
                                                        local_sp_11 = local_sp_10;
                                                        r14_4 = r14_3;
                                                        rbx_11 = rbx_10;
                                                        if (r14_3 == 0UL) {
                                                            var_189 = *(uint64_t *)(local_sp_10 + 24UL);
                                                            *(uint64_t *)var_189 = *(uint64_t *)(local_sp_10 + 8UL);
                                                            *(uint64_t *)(var_189 + 8UL) = 0UL;
                                                            *var_10 = rbx_10;
                                                            return r8_0;
                                                        }
                                                    }
                                                    var_177 = *(uint64_t *)(local_sp_9 + 24UL);
                                                    var_178 = *(uint64_t *)(local_sp_9 + 8UL);
                                                    var_179 = helper_cc_compute_c_wrapper((uint64_t)var_176 + (-1L), 1UL, cc_src2_0, 16U);
                                                    *(uint64_t *)var_177 = var_178;
                                                    *(uint64_t *)(var_177 + 8UL) = (uint64_t)((long)(r14_2 << 32UL) >> (long)32UL);
                                                    var_180 = (uint64_t)((uint32_t)var_179 + 276U);
                                                    *var_10 = rbx_9;
                                                    r8_0 = var_180;
                                                    return r8_0;
                                                }
                                                var_143 = r13_2 + 9UL;
                                                var_144 = (uint32_t)(uint64_t)*(unsigned char *)var_143;
                                                var_145 = (uint64_t)var_144;
                                                var_146 = (uint64_t)((var_141 + var_137) + (-48));
                                                var_147 = local_sp_13 + (-80L);
                                                *(uint64_t *)var_147 = 4219152UL;
                                                var_148 = indirect_placeholder_3(var_142, var_145);
                                                var_149 = var_146 * 10UL;
                                                local_sp_8 = var_147;
                                                r14_1_in = var_149;
                                                rbx_8 = var_143;
                                                if ((uint64_t)(unsigned char)var_148 == 0UL) {
                                                    var_174 = (uint64_t)((uint32_t)r14_1_in & (-2));
                                                    var_175 = *(uint32_t *)(local_sp_8 + 20UL);
                                                    var_176 = var_175;
                                                    local_sp_10 = local_sp_8;
                                                    local_sp_9 = local_sp_8;
                                                    r14_2 = var_174;
                                                    rbx_9 = rbx_8;
                                                    r14_3 = var_174;
                                                    rbx_10 = rbx_8;
                                                    if (var_175 != 4294967295U) {
                                                        local_sp_11 = local_sp_10;
                                                        r14_4 = r14_3;
                                                        rbx_11 = rbx_10;
                                                        if (r14_3 == 0UL) {
                                                            var_189 = *(uint64_t *)(local_sp_10 + 24UL);
                                                            *(uint64_t *)var_189 = *(uint64_t *)(local_sp_10 + 8UL);
                                                            *(uint64_t *)(var_189 + 8UL) = 0UL;
                                                            *var_10 = rbx_10;
                                                            return r8_0;
                                                        }
                                                    }
                                                    var_177 = *(uint64_t *)(local_sp_9 + 24UL);
                                                    var_178 = *(uint64_t *)(local_sp_9 + 8UL);
                                                    var_179 = helper_cc_compute_c_wrapper((uint64_t)var_176 + (-1L), 1UL, cc_src2_0, 16U);
                                                    *(uint64_t *)var_177 = var_178;
                                                    *(uint64_t *)(var_177 + 8UL) = (uint64_t)((long)(r14_2 << 32UL) >> (long)32UL);
                                                    var_180 = (uint64_t)((uint32_t)var_179 + 276U);
                                                    *var_10 = rbx_9;
                                                    r8_0 = var_180;
                                                    return r8_0;
                                                }
                                                var_150 = r13_2 + 10UL;
                                                var_151 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)var_150;
                                                var_152 = (uint32_t)var_149 + var_144;
                                                var_153 = (uint64_t)(var_152 + (-48));
                                                var_154 = local_sp_13 + (-88L);
                                                *(uint64_t *)var_154 = 4219190UL;
                                                var_155 = indirect_placeholder_2(var_151);
                                                var_156 = *(uint32_t *)(local_sp_13 + (-68L));
                                                rbx_0 = var_150;
                                                rbx_1_ph = var_150;
                                                var_176 = var_156;
                                                local_sp_1_ph = var_154;
                                                local_sp_0 = var_154;
                                                rax_0 = var_155;
                                                r12_0 = var_151;
                                                r14_0_ph = var_153;
                                                local_sp_9 = var_154;
                                                r14_2 = var_153;
                                                rbx_9 = var_150;
                                                r14_3 = var_153;
                                                if (var_156 == 4294967295U) {
                                                    if ((uint64_t)(unsigned char)var_155 == 0UL) {
                                                        var_177 = *(uint64_t *)(local_sp_9 + 24UL);
                                                        var_178 = *(uint64_t *)(local_sp_9 + 8UL);
                                                        var_179 = helper_cc_compute_c_wrapper((uint64_t)var_176 + (-1L), 1UL, cc_src2_0, 16U);
                                                        *(uint64_t *)var_177 = var_178;
                                                        *(uint64_t *)(var_177 + 8UL) = (uint64_t)((long)(r14_2 << 32UL) >> (long)32UL);
                                                        var_180 = (uint64_t)((uint32_t)var_179 + 276U);
                                                        *var_10 = rbx_9;
                                                        r8_0 = var_180;
                                                        return r8_0;
                                                    }
                                                    local_sp_1 = local_sp_1_ph;
                                                    rbx_1 = rbx_1_ph;
                                                    r14_2 = r14_0_ph;
                                                    r14_4 = r14_0_ph;
                                                    var_158 = rbx_1 + 1UL;
                                                    var_159 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)var_158;
                                                    var_160 = local_sp_1 + (-8L);
                                                    *(uint64_t *)var_160 = 4219229UL;
                                                    var_161 = indirect_placeholder_2(var_159);
                                                    rbx_1 = var_158;
                                                    local_sp_11 = var_160;
                                                    local_sp_1 = var_160;
                                                    local_sp_9 = var_160;
                                                    rbx_9 = var_158;
                                                    rbx_11 = var_158;
                                                    do {
                                                        var_158 = rbx_1 + 1UL;
                                                        var_159 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)var_158;
                                                        var_160 = local_sp_1 + (-8L);
                                                        *(uint64_t *)var_160 = 4219229UL;
                                                        var_161 = indirect_placeholder_2(var_159);
                                                        rbx_1 = var_158;
                                                        local_sp_11 = var_160;
                                                        local_sp_1 = var_160;
                                                        local_sp_9 = var_160;
                                                        rbx_9 = var_158;
                                                        rbx_11 = var_158;
                                                    } while ((uint64_t)(unsigned char)var_161 != 0UL);
                                                    var_162 = *(uint32_t *)(local_sp_1 + 12UL);
                                                    var_176 = var_162;
                                                    if (((int)var_162 > (int)4294967295U) || (r14_0_ph == 0UL)) {
                                                        var_177 = *(uint64_t *)(local_sp_9 + 24UL);
                                                        var_178 = *(uint64_t *)(local_sp_9 + 8UL);
                                                        var_179 = helper_cc_compute_c_wrapper((uint64_t)var_176 + (-1L), 1UL, cc_src2_0, 16U);
                                                        *(uint64_t *)var_177 = var_178;
                                                        *(uint64_t *)(var_177 + 8UL) = (uint64_t)((long)(r14_2 << 32UL) >> (long)32UL);
                                                        var_180 = (uint64_t)((uint32_t)var_179 + 276U);
                                                        *var_10 = rbx_9;
                                                        r8_0 = var_180;
                                                        return r8_0;
                                                    }
                                                }
                                                while (1U)
                                                    {
                                                        local_sp_1_ph = local_sp_0;
                                                        rbx_1_ph = rbx_0;
                                                        local_sp_10 = local_sp_0;
                                                        rbx_10 = rbx_0;
                                                        if ((uint64_t)(unsigned char)rax_0 != 0UL) {
                                                            loop_state_var = 0U;
                                                            break;
                                                        }
                                                        if ((uint64_t)((unsigned char)r12_0 + '\xd0') != 0UL) {
                                                            var_157 = (uint64_t)(var_152 + (-47));
                                                            r14_0_ph = var_157;
                                                            loop_state_var = 1U;
                                                            break;
                                                        }
                                                        var_163 = rbx_0 + 1UL;
                                                        var_164 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)var_163;
                                                        var_165 = local_sp_0 + (-8L);
                                                        *(uint64_t *)var_165 = 4219512UL;
                                                        var_166 = indirect_placeholder_2(var_164);
                                                        local_sp_0 = var_165;
                                                        rax_0 = var_166;
                                                        r12_0 = var_164;
                                                        rbx_0 = var_163;
                                                        continue;
                                                    }
                                                switch (loop_state_var) {
                                                  case 0U:
                                                    {
                                                        local_sp_11 = local_sp_10;
                                                        r14_4 = r14_3;
                                                        rbx_11 = rbx_10;
                                                        if (r14_3 == 0UL) {
                                                            var_189 = *(uint64_t *)(local_sp_10 + 24UL);
                                                            *(uint64_t *)var_189 = *(uint64_t *)(local_sp_10 + 8UL);
                                                            *(uint64_t *)(var_189 + 8UL) = 0UL;
                                                            *var_10 = rbx_10;
                                                            return r8_0;
                                                        }
                                                    }
                                                    break;
                                                  case 1U:
                                                    {
                                                        local_sp_1 = local_sp_1_ph;
                                                        rbx_1 = rbx_1_ph;
                                                        r14_2 = r14_0_ph;
                                                        r14_4 = r14_0_ph;
                                                        var_158 = rbx_1 + 1UL;
                                                        var_159 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)var_158;
                                                        var_160 = local_sp_1 + (-8L);
                                                        *(uint64_t *)var_160 = 4219229UL;
                                                        var_161 = indirect_placeholder_2(var_159);
                                                        rbx_1 = var_158;
                                                        local_sp_11 = var_160;
                                                        local_sp_1 = var_160;
                                                        local_sp_9 = var_160;
                                                        rbx_9 = var_158;
                                                        rbx_11 = var_158;
                                                        do {
                                                            var_158 = rbx_1 + 1UL;
                                                            var_159 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)var_158;
                                                            var_160 = local_sp_1 + (-8L);
                                                            *(uint64_t *)var_160 = 4219229UL;
                                                            var_161 = indirect_placeholder_2(var_159);
                                                            rbx_1 = var_158;
                                                            local_sp_11 = var_160;
                                                            local_sp_1 = var_160;
                                                            local_sp_9 = var_160;
                                                            rbx_9 = var_158;
                                                            rbx_11 = var_158;
                                                        } while ((uint64_t)(unsigned char)var_161 != 0UL);
                                                        var_162 = *(uint32_t *)(local_sp_1 + 12UL);
                                                        var_176 = var_162;
                                                        if (((int)var_162 > (int)4294967295U) || (r14_0_ph == 0UL)) {
                                                            var_181 = *(uint64_t *)(local_sp_11 + 8UL);
                                                            r8_0 = 63UL;
                                                            if (var_181 != 9223372036854775808UL) {
                                                                var_182 = helper_movq_mm_T0_xmm_wrapper_170((struct type_3 *)(776UL), var_181 + (-1L));
                                                                var_183 = var_182.field_0;
                                                                var_184 = helper_movq_mm_T0_xmm_wrapper_169((struct type_3 *)(840UL), (uint64_t)((long)(4294967296000000000UL - (r14_4 << 32UL)) >> (long)32UL));
                                                                var_185 = var_184.field_0;
                                                                var_186 = *(uint64_t *)(local_sp_11 + 24UL);
                                                                var_187 = helper_punpcklqdq_xmm_wrapper((struct type_7 *)(0UL), (struct type_3 *)(776UL), (struct type_3 *)(840UL), var_183, var_185);
                                                                var_188 = var_187.field_1;
                                                                *(uint64_t *)var_186 = var_187.field_0;
                                                                *(uint64_t *)(var_186 + 8UL) = var_188;
                                                                *var_10 = rbx_11;
                                                                r8_0 = 276UL;
                                                            }
                                                            return r8_0;
                                                        }
                                                        var_177 = *(uint64_t *)(local_sp_9 + 24UL);
                                                        var_178 = *(uint64_t *)(local_sp_9 + 8UL);
                                                        var_179 = helper_cc_compute_c_wrapper((uint64_t)var_176 + (-1L), 1UL, cc_src2_0, 16U);
                                                        *(uint64_t *)var_177 = var_178;
                                                        *(uint64_t *)(var_177 + 8UL) = (uint64_t)((long)(r14_2 << 32UL) >> (long)32UL);
                                                        var_180 = (uint64_t)((uint32_t)var_179 + 276U);
                                                        *var_10 = rbx_9;
                                                        r8_0 = var_180;
                                                        return r8_0;
                                                    }
                                                    break;
                                                }
                                            }
                                        }
                                        local_sp_5 = local_sp_4;
                                        rax_3 = (uint64_t)(((uint32_t)rax_2 * 10U) & (-4));
                                        rbx_5 = rbx_4;
                                    }
                                }
                            }
                        }
                    }
                }
                break;
            }
        }
        break;
      case 3U:
        {
            while (1U)
                {
                    var_60 = (uint64_t)((long)((r14_7 << 32UL) + (-206158430208L)) >> (long)32UL);
                    var_61 = rdx_3 + var_60;
                    *(uint64_t *)(local_sp_17 + 8UL) = var_61;
                    *(uint64_t *)local_sp_17 = var_61;
                    var_62 = helper_cc_compute_all_wrapper(var_61, var_60, 0UL, 9U);
                    r15_0 = r15_2;
                    if ((uint64_t)((uint16_t)var_62 & (unsigned short)2048U) != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_63 = r15_2 + 1UL;
                    var_64 = *(unsigned char *)var_63;
                    var_65 = (uint64_t)var_64;
                    var_66 = local_sp_17 + (-8L);
                    var_67 = (uint64_t *)var_66;
                    *var_67 = 4218476UL;
                    var_68 = indirect_placeholder_2(var_65);
                    r12_3_in = var_64;
                    local_sp_17 = var_66;
                    local_sp_13 = var_66;
                    r14_7 = var_65;
                    r13_2 = var_63;
                    r15_2 = var_63;
                    if ((uint64_t)(unsigned char)var_68 != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_69 = (unsigned __int128)*var_67 * 10ULL;
                    var_70 = (uint64_t)var_69;
                    var_71 = helper_cc_compute_all_wrapper(var_70, (uint64_t)((long)var_70 >> (long)63UL) - (uint64_t)(var_69 >> 64ULL), 0UL, 5U);
                    rdx_3 = var_70;
                    if ((uint64_t)((uint16_t)var_71 & (unsigned short)2048U) == 0UL) {
                        continue;
                    }
                    loop_state_var = 1U;
                    break;
                }
            switch (loop_state_var) {
              case 1U:
                {
                    return r8_0;
                }
                break;
              case 0U:
                {
                    local_sp_14 = local_sp_13;
                    if (((r12_3_in + '\xd4') & '\xfd') == '\x00') {
                        var_190 = *(uint32_t *)(local_sp_14 + 20UL);
                        var_191 = *(uint64_t *)(local_sp_14 + 24UL);
                        *(unsigned char *)var_191 = (unsigned char)(var_190 >> 31U);
                        *(uint64_t *)(var_191 + 8UL) = *(uint64_t *)(local_sp_14 + 8UL);
                        var_192 = (var_190 == 0U);
                        *(uint64_t *)(var_191 + 16UL) = (r13_2 - rbx_14);
                        var_193 = var_192 ? 275UL : 274UL;
                        *var_10 = r13_2;
                        r8_0 = var_193;
                    } else {
                        var_84 = (uint32_t)(uint64_t)*(unsigned char *)(r13_2 + 1UL);
                        var_85 = (uint64_t)var_84;
                        var_86 = local_sp_13 + (-8L);
                        *(uint64_t *)var_86 = 4218811UL;
                        var_87 = indirect_placeholder_2(var_85);
                        local_sp_14 = var_86;
                        if ((uint64_t)(unsigned char)var_87 != 0UL) {
                            *(uint64_t *)(local_sp_13 + (-16L)) = 4218824UL;
                            var_88 = indirect_placeholder_1();
                            if ((uint64_t)(unsigned char)var_88 == 0UL) {
                                return r8_0;
                            }
                            var_89 = (uint32_t)(uint64_t)*(unsigned char *)(r15_0 + 3UL);
                            var_90 = (uint64_t)var_89;
                            var_91 = var_84 + (-48);
                            var_92 = (uint64_t)var_91;
                            var_93 = local_sp_13 + (-24L);
                            *(uint64_t *)var_93 = 4218859UL;
                            var_94 = indirect_placeholder_2(var_90);
                            local_sp_2 = var_93;
                            r8_0 = 276UL;
                            if ((uint64_t)(unsigned char)var_94 == 0UL) {
                                var_95 = ((var_91 * 10U) + var_89) + (-48);
                                var_96 = r13_2 + 3UL;
                                var_97 = (uint32_t)(uint64_t)*(unsigned char *)var_96;
                                var_98 = (uint64_t)var_97;
                                var_99 = local_sp_13 + (-32L);
                                *(uint64_t *)var_99 = 4218899UL;
                                var_100 = indirect_placeholder_2(var_98);
                                var_101 = var_95 * 10U;
                                var_102 = (uint64_t)(var_101 & (-2));
                                local_sp_2 = var_99;
                                r12_1 = var_102;
                                rbx_2 = var_96;
                                if ((uint64_t)(unsigned char)var_100 != 0UL) {
                                    var_169 = (uint64_t)(((uint32_t)r12_1 * 10U) & (-4));
                                    local_sp_3 = local_sp_2;
                                    rax_1 = var_169;
                                    rbx_3 = rbx_2;
                                    var_170 = (uint64_t)(((uint32_t)rax_1 * 10U) & (-4));
                                    local_sp_4 = local_sp_3;
                                    rax_2 = var_170;
                                    rbx_4 = rbx_3;
                                    local_sp_5 = local_sp_4;
                                    rax_3 = (uint64_t)(((uint32_t)rax_2 * 10U) & (-4));
                                    rbx_5 = rbx_4;
                                    var_171 = (uint64_t)(((uint32_t)rax_3 * 10U) & (-4));
                                    local_sp_6 = local_sp_5;
                                    rax_4 = var_171;
                                    rbx_6 = rbx_5;
                                    var_172 = (uint64_t)(((uint32_t)rax_4 * 10U) & (-4));
                                    local_sp_7 = local_sp_6;
                                    rax_5 = var_172;
                                    rbx_7 = rbx_6;
                                    var_173 = (rax_5 * 10UL) & 8589934588UL;
                                    local_sp_8 = local_sp_7;
                                    r14_1_in = var_173;
                                    rbx_8 = rbx_7;
                                    var_174 = (uint64_t)((uint32_t)r14_1_in & (-2));
                                    var_175 = *(uint32_t *)(local_sp_8 + 20UL);
                                    var_176 = var_175;
                                    local_sp_10 = local_sp_8;
                                    local_sp_9 = local_sp_8;
                                    r14_2 = var_174;
                                    rbx_9 = rbx_8;
                                    r14_3 = var_174;
                                    rbx_10 = rbx_8;
                                    if (var_175 == 4294967295U) {
                                        var_177 = *(uint64_t *)(local_sp_9 + 24UL);
                                        var_178 = *(uint64_t *)(local_sp_9 + 8UL);
                                        var_179 = helper_cc_compute_c_wrapper((uint64_t)var_176 + (-1L), 1UL, cc_src2_0, 16U);
                                        *(uint64_t *)var_177 = var_178;
                                        *(uint64_t *)(var_177 + 8UL) = (uint64_t)((long)(r14_2 << 32UL) >> (long)32UL);
                                        var_180 = (uint64_t)((uint32_t)var_179 + 276U);
                                        *var_10 = rbx_9;
                                        r8_0 = var_180;
                                        return r8_0;
                                    }
                                    local_sp_11 = local_sp_10;
                                    r14_4 = r14_3;
                                    rbx_11 = rbx_10;
                                    if (r14_3 == 0UL) {
                                        var_189 = *(uint64_t *)(local_sp_10 + 24UL);
                                        *(uint64_t *)var_189 = *(uint64_t *)(local_sp_10 + 8UL);
                                        *(uint64_t *)(var_189 + 8UL) = 0UL;
                                        *var_10 = rbx_10;
                                        return r8_0;
                                    }
                                    var_181 = *(uint64_t *)(local_sp_11 + 8UL);
                                    r8_0 = 63UL;
                                    if (var_181 == 9223372036854775808UL) {
                                        var_182 = helper_movq_mm_T0_xmm_wrapper_170((struct type_3 *)(776UL), var_181 + (-1L));
                                        var_183 = var_182.field_0;
                                        var_184 = helper_movq_mm_T0_xmm_wrapper_169((struct type_3 *)(840UL), (uint64_t)((long)(4294967296000000000UL - (r14_4 << 32UL)) >> (long)32UL));
                                        var_185 = var_184.field_0;
                                        var_186 = *(uint64_t *)(local_sp_11 + 24UL);
                                        var_187 = helper_punpcklqdq_xmm_wrapper((struct type_7 *)(0UL), (struct type_3 *)(776UL), (struct type_3 *)(840UL), var_183, var_185);
                                        var_188 = var_187.field_1;
                                        *(uint64_t *)var_186 = var_187.field_0;
                                        *(uint64_t *)(var_186 + 8UL) = var_188;
                                        *var_10 = rbx_11;
                                        r8_0 = 276UL;
                                    }
                                    return r8_0;
                                }
                                var_103 = (var_101 + var_97) + (-48);
                                var_104 = r13_2 + 4UL;
                                var_105 = (uint32_t)(uint64_t)*(unsigned char *)var_104;
                                var_106 = (uint64_t)var_105;
                                var_107 = local_sp_13 + (-40L);
                                *(uint64_t *)var_107 = 4218942UL;
                                var_108 = indirect_placeholder_2(var_106);
                                var_109 = var_103 * 10U;
                                var_110 = (uint64_t)(var_109 & (-2));
                                local_sp_3 = var_107;
                                rax_1 = var_110;
                                rbx_3 = var_104;
                                if ((uint64_t)(unsigned char)var_108 != 0UL) {
                                    var_170 = (uint64_t)(((uint32_t)rax_1 * 10U) & (-4));
                                    local_sp_4 = local_sp_3;
                                    rax_2 = var_170;
                                    rbx_4 = rbx_3;
                                    local_sp_5 = local_sp_4;
                                    rax_3 = (uint64_t)(((uint32_t)rax_2 * 10U) & (-4));
                                    rbx_5 = rbx_4;
                                    var_171 = (uint64_t)(((uint32_t)rax_3 * 10U) & (-4));
                                    local_sp_6 = local_sp_5;
                                    rax_4 = var_171;
                                    rbx_6 = rbx_5;
                                    var_172 = (uint64_t)(((uint32_t)rax_4 * 10U) & (-4));
                                    local_sp_7 = local_sp_6;
                                    rax_5 = var_172;
                                    rbx_7 = rbx_6;
                                    var_173 = (rax_5 * 10UL) & 8589934588UL;
                                    local_sp_8 = local_sp_7;
                                    r14_1_in = var_173;
                                    rbx_8 = rbx_7;
                                    var_174 = (uint64_t)((uint32_t)r14_1_in & (-2));
                                    var_175 = *(uint32_t *)(local_sp_8 + 20UL);
                                    var_176 = var_175;
                                    local_sp_10 = local_sp_8;
                                    local_sp_9 = local_sp_8;
                                    r14_2 = var_174;
                                    rbx_9 = rbx_8;
                                    r14_3 = var_174;
                                    rbx_10 = rbx_8;
                                    if (var_175 == 4294967295U) {
                                        var_177 = *(uint64_t *)(local_sp_9 + 24UL);
                                        var_178 = *(uint64_t *)(local_sp_9 + 8UL);
                                        var_179 = helper_cc_compute_c_wrapper((uint64_t)var_176 + (-1L), 1UL, cc_src2_0, 16U);
                                        *(uint64_t *)var_177 = var_178;
                                        *(uint64_t *)(var_177 + 8UL) = (uint64_t)((long)(r14_2 << 32UL) >> (long)32UL);
                                        var_180 = (uint64_t)((uint32_t)var_179 + 276U);
                                        *var_10 = rbx_9;
                                        r8_0 = var_180;
                                        return r8_0;
                                    }
                                    local_sp_11 = local_sp_10;
                                    r14_4 = r14_3;
                                    rbx_11 = rbx_10;
                                    if (r14_3 == 0UL) {
                                        var_189 = *(uint64_t *)(local_sp_10 + 24UL);
                                        *(uint64_t *)var_189 = *(uint64_t *)(local_sp_10 + 8UL);
                                        *(uint64_t *)(var_189 + 8UL) = 0UL;
                                        *var_10 = rbx_10;
                                        return r8_0;
                                    }
                                    var_181 = *(uint64_t *)(local_sp_11 + 8UL);
                                    r8_0 = 63UL;
                                    if (var_181 == 9223372036854775808UL) {
                                        var_182 = helper_movq_mm_T0_xmm_wrapper_170((struct type_3 *)(776UL), var_181 + (-1L));
                                        var_183 = var_182.field_0;
                                        var_184 = helper_movq_mm_T0_xmm_wrapper_169((struct type_3 *)(840UL), (uint64_t)((long)(4294967296000000000UL - (r14_4 << 32UL)) >> (long)32UL));
                                        var_185 = var_184.field_0;
                                        var_186 = *(uint64_t *)(local_sp_11 + 24UL);
                                        var_187 = helper_punpcklqdq_xmm_wrapper((struct type_7 *)(0UL), (struct type_3 *)(776UL), (struct type_3 *)(840UL), var_183, var_185);
                                        var_188 = var_187.field_1;
                                        *(uint64_t *)var_186 = var_187.field_0;
                                        *(uint64_t *)(var_186 + 8UL) = var_188;
                                        *var_10 = rbx_11;
                                        r8_0 = 276UL;
                                    }
                                    return r8_0;
                                }
                                var_111 = (var_109 + var_105) + (-48);
                                var_112 = r13_2 + 5UL;
                                var_113 = (uint32_t)(uint64_t)*(unsigned char *)var_112;
                                var_114 = (uint64_t)var_113;
                                var_115 = local_sp_13 + (-48L);
                                *(uint64_t *)var_115 = 4218984UL;
                                var_116 = indirect_placeholder_3(var_110, var_114);
                                var_117 = var_111 * 10U;
                                var_118 = (uint64_t)(var_117 & (-2));
                                local_sp_4 = var_115;
                                rax_2 = var_118;
                                rbx_4 = var_112;
                                if ((uint64_t)(unsigned char)var_116 != 0UL) {
                                    local_sp_5 = local_sp_4;
                                    rax_3 = (uint64_t)(((uint32_t)rax_2 * 10U) & (-4));
                                    rbx_5 = rbx_4;
                                    var_171 = (uint64_t)(((uint32_t)rax_3 * 10U) & (-4));
                                    local_sp_6 = local_sp_5;
                                    rax_4 = var_171;
                                    rbx_6 = rbx_5;
                                    var_172 = (uint64_t)(((uint32_t)rax_4 * 10U) & (-4));
                                    local_sp_7 = local_sp_6;
                                    rax_5 = var_172;
                                    rbx_7 = rbx_6;
                                    var_173 = (rax_5 * 10UL) & 8589934588UL;
                                    local_sp_8 = local_sp_7;
                                    r14_1_in = var_173;
                                    rbx_8 = rbx_7;
                                    var_174 = (uint64_t)((uint32_t)r14_1_in & (-2));
                                    var_175 = *(uint32_t *)(local_sp_8 + 20UL);
                                    var_176 = var_175;
                                    local_sp_10 = local_sp_8;
                                    local_sp_9 = local_sp_8;
                                    r14_2 = var_174;
                                    rbx_9 = rbx_8;
                                    r14_3 = var_174;
                                    rbx_10 = rbx_8;
                                    if (var_175 == 4294967295U) {
                                        var_177 = *(uint64_t *)(local_sp_9 + 24UL);
                                        var_178 = *(uint64_t *)(local_sp_9 + 8UL);
                                        var_179 = helper_cc_compute_c_wrapper((uint64_t)var_176 + (-1L), 1UL, cc_src2_0, 16U);
                                        *(uint64_t *)var_177 = var_178;
                                        *(uint64_t *)(var_177 + 8UL) = (uint64_t)((long)(r14_2 << 32UL) >> (long)32UL);
                                        var_180 = (uint64_t)((uint32_t)var_179 + 276U);
                                        *var_10 = rbx_9;
                                        r8_0 = var_180;
                                        return r8_0;
                                    }
                                    local_sp_11 = local_sp_10;
                                    r14_4 = r14_3;
                                    rbx_11 = rbx_10;
                                    if (r14_3 == 0UL) {
                                        var_189 = *(uint64_t *)(local_sp_10 + 24UL);
                                        *(uint64_t *)var_189 = *(uint64_t *)(local_sp_10 + 8UL);
                                        *(uint64_t *)(var_189 + 8UL) = 0UL;
                                        *var_10 = rbx_10;
                                        return r8_0;
                                    }
                                    var_181 = *(uint64_t *)(local_sp_11 + 8UL);
                                    r8_0 = 63UL;
                                    if (var_181 == 9223372036854775808UL) {
                                        var_182 = helper_movq_mm_T0_xmm_wrapper_170((struct type_3 *)(776UL), var_181 + (-1L));
                                        var_183 = var_182.field_0;
                                        var_184 = helper_movq_mm_T0_xmm_wrapper_169((struct type_3 *)(840UL), (uint64_t)((long)(4294967296000000000UL - (r14_4 << 32UL)) >> (long)32UL));
                                        var_185 = var_184.field_0;
                                        var_186 = *(uint64_t *)(local_sp_11 + 24UL);
                                        var_187 = helper_punpcklqdq_xmm_wrapper((struct type_7 *)(0UL), (struct type_3 *)(776UL), (struct type_3 *)(840UL), var_183, var_185);
                                        var_188 = var_187.field_1;
                                        *(uint64_t *)var_186 = var_187.field_0;
                                        *(uint64_t *)(var_186 + 8UL) = var_188;
                                        *var_10 = rbx_11;
                                        r8_0 = 276UL;
                                    }
                                    return r8_0;
                                }
                                var_119 = (var_117 + var_113) + (-48);
                                var_120 = r13_2 + 6UL;
                                var_121 = (uint32_t)(uint64_t)*(unsigned char *)var_120;
                                var_122 = (uint64_t)var_121;
                                var_123 = local_sp_13 + (-56L);
                                *(uint64_t *)var_123 = 4219026UL;
                                var_124 = indirect_placeholder_3(var_118, var_122);
                                var_125 = var_119 * 10U;
                                var_126 = (uint64_t)(var_125 & (-2));
                                local_sp_5 = var_123;
                                rax_3 = var_126;
                                rbx_5 = var_120;
                                if ((uint64_t)(unsigned char)var_124 == 0UL) {
                                    var_127 = (var_125 + var_121) + (-48);
                                    var_128 = r13_2 + 7UL;
                                    var_129 = (uint32_t)(uint64_t)*(unsigned char *)var_128;
                                    var_130 = (uint64_t)var_129;
                                    var_131 = local_sp_13 + (-64L);
                                    *(uint64_t *)var_131 = 4219068UL;
                                    var_132 = indirect_placeholder_3(var_126, var_130);
                                    var_133 = var_127 * 10U;
                                    var_134 = (uint64_t)(var_133 & (-2));
                                    local_sp_6 = var_131;
                                    rax_4 = var_134;
                                    rbx_6 = var_128;
                                    if ((uint64_t)(unsigned char)var_132 != 0UL) {
                                        var_172 = (uint64_t)(((uint32_t)rax_4 * 10U) & (-4));
                                        local_sp_7 = local_sp_6;
                                        rax_5 = var_172;
                                        rbx_7 = rbx_6;
                                        var_173 = (rax_5 * 10UL) & 8589934588UL;
                                        local_sp_8 = local_sp_7;
                                        r14_1_in = var_173;
                                        rbx_8 = rbx_7;
                                        var_174 = (uint64_t)((uint32_t)r14_1_in & (-2));
                                        var_175 = *(uint32_t *)(local_sp_8 + 20UL);
                                        var_176 = var_175;
                                        local_sp_10 = local_sp_8;
                                        local_sp_9 = local_sp_8;
                                        r14_2 = var_174;
                                        rbx_9 = rbx_8;
                                        r14_3 = var_174;
                                        rbx_10 = rbx_8;
                                        if (var_175 == 4294967295U) {
                                            var_177 = *(uint64_t *)(local_sp_9 + 24UL);
                                            var_178 = *(uint64_t *)(local_sp_9 + 8UL);
                                            var_179 = helper_cc_compute_c_wrapper((uint64_t)var_176 + (-1L), 1UL, cc_src2_0, 16U);
                                            *(uint64_t *)var_177 = var_178;
                                            *(uint64_t *)(var_177 + 8UL) = (uint64_t)((long)(r14_2 << 32UL) >> (long)32UL);
                                            var_180 = (uint64_t)((uint32_t)var_179 + 276U);
                                            *var_10 = rbx_9;
                                            r8_0 = var_180;
                                            return r8_0;
                                        }
                                        local_sp_11 = local_sp_10;
                                        r14_4 = r14_3;
                                        rbx_11 = rbx_10;
                                        if (r14_3 == 0UL) {
                                            var_189 = *(uint64_t *)(local_sp_10 + 24UL);
                                            *(uint64_t *)var_189 = *(uint64_t *)(local_sp_10 + 8UL);
                                            *(uint64_t *)(var_189 + 8UL) = 0UL;
                                            *var_10 = rbx_10;
                                            return r8_0;
                                        }
                                        var_181 = *(uint64_t *)(local_sp_11 + 8UL);
                                        r8_0 = 63UL;
                                        if (var_181 == 9223372036854775808UL) {
                                            var_182 = helper_movq_mm_T0_xmm_wrapper_170((struct type_3 *)(776UL), var_181 + (-1L));
                                            var_183 = var_182.field_0;
                                            var_184 = helper_movq_mm_T0_xmm_wrapper_169((struct type_3 *)(840UL), (uint64_t)((long)(4294967296000000000UL - (r14_4 << 32UL)) >> (long)32UL));
                                            var_185 = var_184.field_0;
                                            var_186 = *(uint64_t *)(local_sp_11 + 24UL);
                                            var_187 = helper_punpcklqdq_xmm_wrapper((struct type_7 *)(0UL), (struct type_3 *)(776UL), (struct type_3 *)(840UL), var_183, var_185);
                                            var_188 = var_187.field_1;
                                            *(uint64_t *)var_186 = var_187.field_0;
                                            *(uint64_t *)(var_186 + 8UL) = var_188;
                                            *var_10 = rbx_11;
                                            r8_0 = 276UL;
                                        }
                                        return r8_0;
                                    }
                                    var_135 = (var_133 + var_129) + (-48);
                                    var_136 = r13_2 + 8UL;
                                    var_137 = (uint32_t)(uint64_t)*(unsigned char *)var_136;
                                    var_138 = (uint64_t)var_137;
                                    var_139 = local_sp_13 + (-72L);
                                    *(uint64_t *)var_139 = 4219110UL;
                                    var_140 = indirect_placeholder_3(var_134, var_138);
                                    var_141 = var_135 * 10U;
                                    var_142 = (uint64_t)(var_141 & (-2));
                                    local_sp_7 = var_139;
                                    rax_5 = var_142;
                                    rbx_7 = var_136;
                                    if ((uint64_t)(unsigned char)var_140 != 0UL) {
                                        var_173 = (rax_5 * 10UL) & 8589934588UL;
                                        local_sp_8 = local_sp_7;
                                        r14_1_in = var_173;
                                        rbx_8 = rbx_7;
                                        var_174 = (uint64_t)((uint32_t)r14_1_in & (-2));
                                        var_175 = *(uint32_t *)(local_sp_8 + 20UL);
                                        var_176 = var_175;
                                        local_sp_10 = local_sp_8;
                                        local_sp_9 = local_sp_8;
                                        r14_2 = var_174;
                                        rbx_9 = rbx_8;
                                        r14_3 = var_174;
                                        rbx_10 = rbx_8;
                                        if (var_175 == 4294967295U) {
                                            var_177 = *(uint64_t *)(local_sp_9 + 24UL);
                                            var_178 = *(uint64_t *)(local_sp_9 + 8UL);
                                            var_179 = helper_cc_compute_c_wrapper((uint64_t)var_176 + (-1L), 1UL, cc_src2_0, 16U);
                                            *(uint64_t *)var_177 = var_178;
                                            *(uint64_t *)(var_177 + 8UL) = (uint64_t)((long)(r14_2 << 32UL) >> (long)32UL);
                                            var_180 = (uint64_t)((uint32_t)var_179 + 276U);
                                            *var_10 = rbx_9;
                                            r8_0 = var_180;
                                            return r8_0;
                                        }
                                        local_sp_11 = local_sp_10;
                                        r14_4 = r14_3;
                                        rbx_11 = rbx_10;
                                        if (r14_3 == 0UL) {
                                            var_189 = *(uint64_t *)(local_sp_10 + 24UL);
                                            *(uint64_t *)var_189 = *(uint64_t *)(local_sp_10 + 8UL);
                                            *(uint64_t *)(var_189 + 8UL) = 0UL;
                                            *var_10 = rbx_10;
                                            return r8_0;
                                        }
                                        var_181 = *(uint64_t *)(local_sp_11 + 8UL);
                                        r8_0 = 63UL;
                                        if (var_181 == 9223372036854775808UL) {
                                            var_182 = helper_movq_mm_T0_xmm_wrapper_170((struct type_3 *)(776UL), var_181 + (-1L));
                                            var_183 = var_182.field_0;
                                            var_184 = helper_movq_mm_T0_xmm_wrapper_169((struct type_3 *)(840UL), (uint64_t)((long)(4294967296000000000UL - (r14_4 << 32UL)) >> (long)32UL));
                                            var_185 = var_184.field_0;
                                            var_186 = *(uint64_t *)(local_sp_11 + 24UL);
                                            var_187 = helper_punpcklqdq_xmm_wrapper((struct type_7 *)(0UL), (struct type_3 *)(776UL), (struct type_3 *)(840UL), var_183, var_185);
                                            var_188 = var_187.field_1;
                                            *(uint64_t *)var_186 = var_187.field_0;
                                            *(uint64_t *)(var_186 + 8UL) = var_188;
                                            *var_10 = rbx_11;
                                            r8_0 = 276UL;
                                        }
                                        return r8_0;
                                    }
                                    var_143 = r13_2 + 9UL;
                                    var_144 = (uint32_t)(uint64_t)*(unsigned char *)var_143;
                                    var_145 = (uint64_t)var_144;
                                    var_146 = (uint64_t)((var_141 + var_137) + (-48));
                                    var_147 = local_sp_13 + (-80L);
                                    *(uint64_t *)var_147 = 4219152UL;
                                    var_148 = indirect_placeholder_3(var_142, var_145);
                                    var_149 = var_146 * 10UL;
                                    local_sp_8 = var_147;
                                    r14_1_in = var_149;
                                    rbx_8 = var_143;
                                    if ((uint64_t)(unsigned char)var_148 != 0UL) {
                                        var_174 = (uint64_t)((uint32_t)r14_1_in & (-2));
                                        var_175 = *(uint32_t *)(local_sp_8 + 20UL);
                                        var_176 = var_175;
                                        local_sp_10 = local_sp_8;
                                        local_sp_9 = local_sp_8;
                                        r14_2 = var_174;
                                        rbx_9 = rbx_8;
                                        r14_3 = var_174;
                                        rbx_10 = rbx_8;
                                        if (var_175 == 4294967295U) {
                                            var_177 = *(uint64_t *)(local_sp_9 + 24UL);
                                            var_178 = *(uint64_t *)(local_sp_9 + 8UL);
                                            var_179 = helper_cc_compute_c_wrapper((uint64_t)var_176 + (-1L), 1UL, cc_src2_0, 16U);
                                            *(uint64_t *)var_177 = var_178;
                                            *(uint64_t *)(var_177 + 8UL) = (uint64_t)((long)(r14_2 << 32UL) >> (long)32UL);
                                            var_180 = (uint64_t)((uint32_t)var_179 + 276U);
                                            *var_10 = rbx_9;
                                            r8_0 = var_180;
                                            return r8_0;
                                        }
                                        local_sp_11 = local_sp_10;
                                        r14_4 = r14_3;
                                        rbx_11 = rbx_10;
                                        if (r14_3 == 0UL) {
                                            var_189 = *(uint64_t *)(local_sp_10 + 24UL);
                                            *(uint64_t *)var_189 = *(uint64_t *)(local_sp_10 + 8UL);
                                            *(uint64_t *)(var_189 + 8UL) = 0UL;
                                            *var_10 = rbx_10;
                                            return r8_0;
                                        }
                                        var_181 = *(uint64_t *)(local_sp_11 + 8UL);
                                        r8_0 = 63UL;
                                        if (var_181 == 9223372036854775808UL) {
                                            var_182 = helper_movq_mm_T0_xmm_wrapper_170((struct type_3 *)(776UL), var_181 + (-1L));
                                            var_183 = var_182.field_0;
                                            var_184 = helper_movq_mm_T0_xmm_wrapper_169((struct type_3 *)(840UL), (uint64_t)((long)(4294967296000000000UL - (r14_4 << 32UL)) >> (long)32UL));
                                            var_185 = var_184.field_0;
                                            var_186 = *(uint64_t *)(local_sp_11 + 24UL);
                                            var_187 = helper_punpcklqdq_xmm_wrapper((struct type_7 *)(0UL), (struct type_3 *)(776UL), (struct type_3 *)(840UL), var_183, var_185);
                                            var_188 = var_187.field_1;
                                            *(uint64_t *)var_186 = var_187.field_0;
                                            *(uint64_t *)(var_186 + 8UL) = var_188;
                                            *var_10 = rbx_11;
                                            r8_0 = 276UL;
                                        }
                                        return r8_0;
                                    }
                                    var_150 = r13_2 + 10UL;
                                    var_151 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)var_150;
                                    var_152 = (uint32_t)var_149 + var_144;
                                    var_153 = (uint64_t)(var_152 + (-48));
                                    var_154 = local_sp_13 + (-88L);
                                    *(uint64_t *)var_154 = 4219190UL;
                                    var_155 = indirect_placeholder_2(var_151);
                                    var_156 = *(uint32_t *)(local_sp_13 + (-68L));
                                    rbx_0 = var_150;
                                    rbx_1_ph = var_150;
                                    var_176 = var_156;
                                    local_sp_1_ph = var_154;
                                    local_sp_0 = var_154;
                                    rax_0 = var_155;
                                    r12_0 = var_151;
                                    r14_0_ph = var_153;
                                    local_sp_9 = var_154;
                                    r14_2 = var_153;
                                    rbx_9 = var_150;
                                    r14_3 = var_153;
                                    if (var_156 != 4294967295U) {
                                        if ((uint64_t)(unsigned char)var_155 == 0UL) {
                                            var_177 = *(uint64_t *)(local_sp_9 + 24UL);
                                            var_178 = *(uint64_t *)(local_sp_9 + 8UL);
                                            var_179 = helper_cc_compute_c_wrapper((uint64_t)var_176 + (-1L), 1UL, cc_src2_0, 16U);
                                            *(uint64_t *)var_177 = var_178;
                                            *(uint64_t *)(var_177 + 8UL) = (uint64_t)((long)(r14_2 << 32UL) >> (long)32UL);
                                            var_180 = (uint64_t)((uint32_t)var_179 + 276U);
                                            *var_10 = rbx_9;
                                            r8_0 = var_180;
                                            return r8_0;
                                        }
                                        local_sp_1 = local_sp_1_ph;
                                        rbx_1 = rbx_1_ph;
                                        r14_2 = r14_0_ph;
                                        r14_4 = r14_0_ph;
                                        var_158 = rbx_1 + 1UL;
                                        var_159 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)var_158;
                                        var_160 = local_sp_1 + (-8L);
                                        *(uint64_t *)var_160 = 4219229UL;
                                        var_161 = indirect_placeholder_2(var_159);
                                        rbx_1 = var_158;
                                        local_sp_11 = var_160;
                                        local_sp_1 = var_160;
                                        local_sp_9 = var_160;
                                        rbx_9 = var_158;
                                        rbx_11 = var_158;
                                        do {
                                            var_158 = rbx_1 + 1UL;
                                            var_159 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)var_158;
                                            var_160 = local_sp_1 + (-8L);
                                            *(uint64_t *)var_160 = 4219229UL;
                                            var_161 = indirect_placeholder_2(var_159);
                                            rbx_1 = var_158;
                                            local_sp_11 = var_160;
                                            local_sp_1 = var_160;
                                            local_sp_9 = var_160;
                                            rbx_9 = var_158;
                                            rbx_11 = var_158;
                                        } while ((uint64_t)(unsigned char)var_161 != 0UL);
                                        var_162 = *(uint32_t *)(local_sp_1 + 12UL);
                                        var_176 = var_162;
                                        if (!(((int)var_162 > (int)4294967295U) || (r14_0_ph == 0UL))) {
                                            var_181 = *(uint64_t *)(local_sp_11 + 8UL);
                                            r8_0 = 63UL;
                                            if (var_181 == 9223372036854775808UL) {
                                                var_182 = helper_movq_mm_T0_xmm_wrapper_170((struct type_3 *)(776UL), var_181 + (-1L));
                                                var_183 = var_182.field_0;
                                                var_184 = helper_movq_mm_T0_xmm_wrapper_169((struct type_3 *)(840UL), (uint64_t)((long)(4294967296000000000UL - (r14_4 << 32UL)) >> (long)32UL));
                                                var_185 = var_184.field_0;
                                                var_186 = *(uint64_t *)(local_sp_11 + 24UL);
                                                var_187 = helper_punpcklqdq_xmm_wrapper((struct type_7 *)(0UL), (struct type_3 *)(776UL), (struct type_3 *)(840UL), var_183, var_185);
                                                var_188 = var_187.field_1;
                                                *(uint64_t *)var_186 = var_187.field_0;
                                                *(uint64_t *)(var_186 + 8UL) = var_188;
                                                *var_10 = rbx_11;
                                                r8_0 = 276UL;
                                            }
                                            return r8_0;
                                        }
                                        var_177 = *(uint64_t *)(local_sp_9 + 24UL);
                                        var_178 = *(uint64_t *)(local_sp_9 + 8UL);
                                        var_179 = helper_cc_compute_c_wrapper((uint64_t)var_176 + (-1L), 1UL, cc_src2_0, 16U);
                                        *(uint64_t *)var_177 = var_178;
                                        *(uint64_t *)(var_177 + 8UL) = (uint64_t)((long)(r14_2 << 32UL) >> (long)32UL);
                                        var_180 = (uint64_t)((uint32_t)var_179 + 276U);
                                        *var_10 = rbx_9;
                                        r8_0 = var_180;
                                        return r8_0;
                                    }
                                    while (1U)
                                        {
                                            local_sp_1_ph = local_sp_0;
                                            rbx_1_ph = rbx_0;
                                            local_sp_10 = local_sp_0;
                                            rbx_10 = rbx_0;
                                            if ((uint64_t)(unsigned char)rax_0 != 0UL) {
                                                loop_state_var = 0U;
                                                break;
                                            }
                                            if ((uint64_t)((unsigned char)r12_0 + '\xd0') != 0UL) {
                                                var_157 = (uint64_t)(var_152 + (-47));
                                                r14_0_ph = var_157;
                                                loop_state_var = 1U;
                                                break;
                                            }
                                            var_163 = rbx_0 + 1UL;
                                            var_164 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)var_163;
                                            var_165 = local_sp_0 + (-8L);
                                            *(uint64_t *)var_165 = 4219512UL;
                                            var_166 = indirect_placeholder_2(var_164);
                                            local_sp_0 = var_165;
                                            rax_0 = var_166;
                                            r12_0 = var_164;
                                            rbx_0 = var_163;
                                            continue;
                                        }
                                    switch (loop_state_var) {
                                      case 1U:
                                        {
                                            local_sp_1 = local_sp_1_ph;
                                            rbx_1 = rbx_1_ph;
                                            r14_2 = r14_0_ph;
                                            r14_4 = r14_0_ph;
                                            var_158 = rbx_1 + 1UL;
                                            var_159 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)var_158;
                                            var_160 = local_sp_1 + (-8L);
                                            *(uint64_t *)var_160 = 4219229UL;
                                            var_161 = indirect_placeholder_2(var_159);
                                            rbx_1 = var_158;
                                            local_sp_11 = var_160;
                                            local_sp_1 = var_160;
                                            local_sp_9 = var_160;
                                            rbx_9 = var_158;
                                            rbx_11 = var_158;
                                            do {
                                                var_158 = rbx_1 + 1UL;
                                                var_159 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)var_158;
                                                var_160 = local_sp_1 + (-8L);
                                                *(uint64_t *)var_160 = 4219229UL;
                                                var_161 = indirect_placeholder_2(var_159);
                                                rbx_1 = var_158;
                                                local_sp_11 = var_160;
                                                local_sp_1 = var_160;
                                                local_sp_9 = var_160;
                                                rbx_9 = var_158;
                                                rbx_11 = var_158;
                                            } while ((uint64_t)(unsigned char)var_161 != 0UL);
                                            var_162 = *(uint32_t *)(local_sp_1 + 12UL);
                                            var_176 = var_162;
                                            if (((int)var_162 > (int)4294967295U) || (r14_0_ph == 0UL)) {
                                                var_177 = *(uint64_t *)(local_sp_9 + 24UL);
                                                var_178 = *(uint64_t *)(local_sp_9 + 8UL);
                                                var_179 = helper_cc_compute_c_wrapper((uint64_t)var_176 + (-1L), 1UL, cc_src2_0, 16U);
                                                *(uint64_t *)var_177 = var_178;
                                                *(uint64_t *)(var_177 + 8UL) = (uint64_t)((long)(r14_2 << 32UL) >> (long)32UL);
                                                var_180 = (uint64_t)((uint32_t)var_179 + 276U);
                                                *var_10 = rbx_9;
                                                r8_0 = var_180;
                                                return r8_0;
                                            }
                                        }
                                        break;
                                      case 0U:
                                        {
                                            local_sp_11 = local_sp_10;
                                            r14_4 = r14_3;
                                            rbx_11 = rbx_10;
                                            if (r14_3 != 0UL) {
                                                var_189 = *(uint64_t *)(local_sp_10 + 24UL);
                                                *(uint64_t *)var_189 = *(uint64_t *)(local_sp_10 + 8UL);
                                                *(uint64_t *)(var_189 + 8UL) = 0UL;
                                                *var_10 = rbx_10;
                                                return r8_0;
                                            }
                                            var_181 = *(uint64_t *)(local_sp_11 + 8UL);
                                            r8_0 = 63UL;
                                            if (var_181 == 9223372036854775808UL) {
                                                var_182 = helper_movq_mm_T0_xmm_wrapper_170((struct type_3 *)(776UL), var_181 + (-1L));
                                                var_183 = var_182.field_0;
                                                var_184 = helper_movq_mm_T0_xmm_wrapper_169((struct type_3 *)(840UL), (uint64_t)((long)(4294967296000000000UL - (r14_4 << 32UL)) >> (long)32UL));
                                                var_185 = var_184.field_0;
                                                var_186 = *(uint64_t *)(local_sp_11 + 24UL);
                                                var_187 = helper_punpcklqdq_xmm_wrapper((struct type_7 *)(0UL), (struct type_3 *)(776UL), (struct type_3 *)(840UL), var_183, var_185);
                                                var_188 = var_187.field_1;
                                                *(uint64_t *)var_186 = var_187.field_0;
                                                *(uint64_t *)(var_186 + 8UL) = var_188;
                                                *var_10 = rbx_11;
                                                r8_0 = 276UL;
                                            }
                                            return r8_0;
                                        }
                                        break;
                                    }
                                }
                            }
                            var_167 = r13_2 + 2UL;
                            var_168 = (uint64_t)(((uint32_t)(uint64_t)((long)(var_92 << 32UL) >> (long)32UL) * 100U) & (-4));
                            r12_1 = var_168;
                            rbx_2 = var_167;
                            var_169 = (uint64_t)(((uint32_t)r12_1 * 10U) & (-4));
                            local_sp_3 = local_sp_2;
                            rax_1 = var_169;
                            rbx_3 = rbx_2;
                            var_170 = (uint64_t)(((uint32_t)rax_1 * 10U) & (-4));
                            local_sp_4 = local_sp_3;
                            rax_2 = var_170;
                            rbx_4 = rbx_3;
                            local_sp_5 = local_sp_4;
                            rax_3 = (uint64_t)(((uint32_t)rax_2 * 10U) & (-4));
                            rbx_5 = rbx_4;
                            var_171 = (uint64_t)(((uint32_t)rax_3 * 10U) & (-4));
                            local_sp_6 = local_sp_5;
                            rax_4 = var_171;
                            rbx_6 = rbx_5;
                            var_172 = (uint64_t)(((uint32_t)rax_4 * 10U) & (-4));
                            local_sp_7 = local_sp_6;
                            rax_5 = var_172;
                            rbx_7 = rbx_6;
                            var_173 = (rax_5 * 10UL) & 8589934588UL;
                            local_sp_8 = local_sp_7;
                            r14_1_in = var_173;
                            rbx_8 = rbx_7;
                            var_174 = (uint64_t)((uint32_t)r14_1_in & (-2));
                            var_175 = *(uint32_t *)(local_sp_8 + 20UL);
                            var_176 = var_175;
                            local_sp_10 = local_sp_8;
                            local_sp_9 = local_sp_8;
                            r14_2 = var_174;
                            rbx_9 = rbx_8;
                            r14_3 = var_174;
                            rbx_10 = rbx_8;
                            if (var_175 == 4294967295U) {
                                var_177 = *(uint64_t *)(local_sp_9 + 24UL);
                                var_178 = *(uint64_t *)(local_sp_9 + 8UL);
                                var_179 = helper_cc_compute_c_wrapper((uint64_t)var_176 + (-1L), 1UL, cc_src2_0, 16U);
                                *(uint64_t *)var_177 = var_178;
                                *(uint64_t *)(var_177 + 8UL) = (uint64_t)((long)(r14_2 << 32UL) >> (long)32UL);
                                var_180 = (uint64_t)((uint32_t)var_179 + 276U);
                                *var_10 = rbx_9;
                                r8_0 = var_180;
                                return r8_0;
                            }
                            local_sp_11 = local_sp_10;
                            r14_4 = r14_3;
                            rbx_11 = rbx_10;
                            if (r14_3 == 0UL) {
                                var_189 = *(uint64_t *)(local_sp_10 + 24UL);
                                *(uint64_t *)var_189 = *(uint64_t *)(local_sp_10 + 8UL);
                                *(uint64_t *)(var_189 + 8UL) = 0UL;
                                *var_10 = rbx_10;
                                return r8_0;
                            }
                            var_181 = *(uint64_t *)(local_sp_11 + 8UL);
                            r8_0 = 63UL;
                            if (var_181 == 9223372036854775808UL) {
                                var_182 = helper_movq_mm_T0_xmm_wrapper_170((struct type_3 *)(776UL), var_181 + (-1L));
                                var_183 = var_182.field_0;
                                var_184 = helper_movq_mm_T0_xmm_wrapper_169((struct type_3 *)(840UL), (uint64_t)((long)(4294967296000000000UL - (r14_4 << 32UL)) >> (long)32UL));
                                var_185 = var_184.field_0;
                                var_186 = *(uint64_t *)(local_sp_11 + 24UL);
                                var_187 = helper_punpcklqdq_xmm_wrapper((struct type_7 *)(0UL), (struct type_3 *)(776UL), (struct type_3 *)(840UL), var_183, var_185);
                                var_188 = var_187.field_1;
                                *(uint64_t *)var_186 = var_187.field_0;
                                *(uint64_t *)(var_186 + 8UL) = var_188;
                                *var_10 = rbx_11;
                                r8_0 = 276UL;
                            }
                        }
                    }
                }
                break;
            }
        }
        break;
    }
}
