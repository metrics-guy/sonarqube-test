typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_validate_case_classes_ret_type;
struct indirect_placeholder_53_ret_type;
struct bb_validate_case_classes_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_53_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r9(void);
extern void indirect_placeholder_19(uint64_t param_0, uint64_t param_1);
extern uint64_t init_r8(void);
extern uint64_t init_rcx(void);
extern struct indirect_placeholder_53_ret_type indirect_placeholder_53(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
struct bb_validate_case_classes_ret_type bb_validate_case_classes(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_53_ret_type var_37;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    unsigned char var_10;
    uint64_t rcx_5;
    uint64_t local_sp_0;
    uint64_t rcx_0_in;
    uint64_t rcx_4;
    uint32_t var_42;
    uint64_t var_43;
    uint64_t rcx_1_in;
    uint64_t rcx_1;
    uint64_t r12_0;
    uint64_t var_39;
    uint64_t *var_40;
    uint64_t var_41;
    uint64_t var_36;
    uint64_t r13_2;
    uint64_t local_sp_4;
    uint64_t rcx_2;
    uint64_t r13_1;
    uint64_t var_28;
    uint64_t local_sp_3;
    uint64_t local_sp_1;
    uint64_t rcx_3;
    uint64_t var_44;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t *var_13;
    uint64_t *var_14;
    uint64_t var_15;
    uint64_t *var_16;
    uint64_t var_17;
    uint64_t *var_18;
    uint64_t var_19;
    uint64_t *var_20;
    uint64_t var_21;
    uint64_t *var_22;
    uint64_t rdx_0;
    uint64_t local_sp_2_ph;
    uint64_t rax_0_ph;
    uint64_t r14_0_ph;
    uint64_t r8_1;
    uint64_t r9_0_ph;
    uint64_t r8_0_ph;
    uint64_t r13_0_ph;
    uint64_t rax_0_ph86;
    uint64_t r14_0_ph87;
    uint64_t rax_0;
    uint32_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t *var_32;
    uint64_t var_33;
    bool var_34;
    bool var_35;
    uint64_t r9_1;
    uint64_t *var_38;
    uint64_t r9_2;
    uint64_t r8_2;
    struct bb_validate_case_classes_ret_type mrv;
    struct bb_validate_case_classes_ret_type mrv1;
    struct bb_validate_case_classes_ret_type mrv2;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rcx();
    var_2 = init_rbp();
    var_3 = init_r15();
    var_4 = init_r14();
    var_5 = init_r12();
    var_6 = init_rbx();
    var_7 = init_r9();
    var_8 = init_r8();
    var_9 = init_r13();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_9;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    *(uint64_t *)(var_0 + (-48L)) = var_6;
    var_10 = *(unsigned char *)(rsi + 49UL);
    rcx_5 = var_1;
    rcx_3 = 4269472UL;
    rax_0_ph = 0UL;
    r14_0_ph = 0UL;
    r9_0_ph = var_7;
    r8_0_ph = var_8;
    r9_2 = var_7;
    r8_2 = var_8;
    if (var_10 == '\x00') {
        mrv.field_0 = rcx_5;
        mrv1 = mrv;
        mrv1.field_1 = r9_2;
        mrv2 = mrv1;
        mrv2.field_2 = r8_2;
        return mrv2;
    }
    var_11 = var_0 + (-120L);
    var_12 = (uint64_t)var_10;
    *(uint64_t *)(var_0 + (-112L)) = 0UL;
    var_13 = (uint64_t *)(rdi + 24UL);
    var_14 = (uint64_t *)(rsi + 24UL);
    var_15 = rdi + 8UL;
    var_16 = (uint64_t *)var_15;
    var_17 = rsi + 8UL;
    var_18 = (uint64_t *)var_17;
    var_19 = rdi + 16UL;
    var_20 = (uint64_t *)var_19;
    var_21 = rsi + 16UL;
    var_22 = (uint64_t *)var_21;
    local_sp_2_ph = var_11;
    r13_0_ph = var_12;
    while (1U)
        {
            r13_2 = r13_0_ph;
            local_sp_4 = local_sp_2_ph;
            r13_1 = r13_0_ph;
            local_sp_3 = local_sp_2_ph;
            r8_1 = r8_0_ph;
            rax_0_ph86 = rax_0_ph;
            r14_0_ph87 = r14_0_ph;
            r9_1 = r9_0_ph;
            r9_2 = r9_0_ph;
            r8_2 = r8_0_ph;
            while (1U)
                {
                    rcx_0_in = r14_0_ph87;
                    rcx_1_in = r14_0_ph87;
                    r14_0_ph = r14_0_ph87;
                    rax_0 = rax_0_ph86;
                    while (1U)
                        {
                            var_23 = (uint32_t)rax_0;
                            var_24 = (uint64_t)(var_23 + (-65));
                            var_25 = (uint64_t)(var_23 + 1U);
                            rax_0_ph86 = var_25;
                            rax_0 = var_25;
                            rcx_4 = var_24;
                            rdx_0 = var_25;
                            if (var_24 <= 25UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            if ((uint64_t)((var_23 + (-97)) & (-2)) <= 25UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            if ((uint64_t)(var_23 + (-255)) == 0UL) {
                                continue;
                            }
                            loop_state_var = 2U;
                            break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 0U:
                        {
                            r14_0_ph87 = r14_0_ph87 + 1UL;
                            continue;
                        }
                        break;
                      case 1U:
                        {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 2U:
                        {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    var_38 = (uint64_t *)(local_sp_4 + 8UL);
                    *var_38 = (*var_38 + 1UL);
                    local_sp_2_ph = local_sp_4;
                    rax_0_ph = (uint64_t)(uint32_t)rdx_0;
                    r9_0_ph = r9_1;
                    r8_0_ph = r8_1;
                    r13_0_ph = r13_2;
                    continue;
                }
                break;
              case 1U:
                {
                    var_26 = (uint64_t)(uint32_t)r13_0_ph;
                    *(uint64_t *)(local_sp_2_ph + 16UL) = *var_13;
                    *(uint64_t *)(local_sp_2_ph + 40UL) = *var_14;
                    *(uint64_t *)(local_sp_2_ph + 24UL) = *var_16;
                    var_27 = *var_18;
                    *var_20 = 18446744073709551614UL;
                    *(uint64_t *)(local_sp_2_ph + 32UL) = var_27;
                    *var_22 = 18446744073709551614UL;
                    *(uint64_t *)local_sp_2_ph = var_19;
                    r12_0 = var_26;
                    while (1U)
                        {
                            var_28 = local_sp_3 + 56UL;
                            *(uint64_t *)(local_sp_3 + (-8L)) = 4210796UL;
                            var_29 = indirect_placeholder_1(rdi, var_28);
                            var_30 = local_sp_3 + 52UL;
                            var_31 = local_sp_3 + (-16L);
                            var_32 = (uint64_t *)var_31;
                            *var_32 = 4210811UL;
                            var_33 = indirect_placeholder_1(rsi, var_30);
                            var_34 = ((uint64_t)(unsigned char)r12_0 == 0UL);
                            var_35 = (*(uint32_t *)(local_sp_3 + 44UL) == 2U);
                            local_sp_0 = var_31;
                            rcx_2 = rcx_4;
                            r13_2 = r13_1;
                            if (var_34) {
                                if (!var_35) {
                                    rcx_4 = rcx_2;
                                    r12_0 = (r12_0 & (-256L)) | (*var_22 == 18446744073709551615UL);
                                    r13_1 = (r13_1 & (-256L)) | (*var_20 == 18446744073709551615UL);
                                    local_sp_3 = local_sp_0;
                                    local_sp_1 = local_sp_0;
                                    if ((uint64_t)((uint32_t)var_29 + 1U) != 0UL) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    if ((uint64_t)((uint32_t)var_33 + 1U) == 0UL) {
                                        continue;
                                    }
                                    loop_state_var = 0U;
                                    break;
                                }
                            }
                            if (!var_35) {
                                rcx_4 = rcx_2;
                                r12_0 = (r12_0 & (-256L)) | (*var_22 == 18446744073709551615UL);
                                r13_1 = (r13_1 & (-256L)) | (*var_20 == 18446744073709551615UL);
                                local_sp_3 = local_sp_0;
                                local_sp_1 = local_sp_0;
                                if ((uint64_t)((uint32_t)var_29 + 1U) != 0UL) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                if ((uint64_t)((uint32_t)var_33 + 1U) == 0UL) {
                                    continue;
                                }
                                loop_state_var = 0U;
                                break;
                            }
                            if ((uint64_t)(unsigned char)r13_1 != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            if (*(uint32_t *)(local_sp_3 + 40UL) != 2U) {
                                loop_state_var = 1U;
                                break;
                            }
                            var_39 = *var_32;
                            var_40 = (uint64_t *)(local_sp_3 + (-24L));
                            *var_40 = 4210680UL;
                            indirect_placeholder_19(var_15, var_39);
                            var_41 = local_sp_3 + (-32L);
                            *(uint64_t *)var_41 = 4210693UL;
                            indirect_placeholder_19(var_17, var_21);
                            local_sp_0 = var_41;
                            if (*(uint32_t *)(local_sp_3 + 24UL) == 1U) {
                                rcx_0_in = *var_40;
                            }
                            var_42 = *(uint32_t *)(local_sp_3 + 28UL);
                            *var_13 = (*var_13 - (rcx_0_in + (-1L)));
                            var_43 = *var_14;
                            if (var_42 == 1U) {
                                rcx_1_in = *var_40;
                            }
                            rcx_1 = rcx_1_in + (-1L);
                            *var_14 = (var_43 - rcx_1);
                            rcx_2 = rcx_1;
                            rcx_4 = rcx_2;
                            r12_0 = (r12_0 & (-256L)) | (*var_22 == 18446744073709551615UL);
                            r13_1 = (r13_1 & (-256L)) | (*var_20 == 18446744073709551615UL);
                            local_sp_3 = local_sp_0;
                            local_sp_1 = local_sp_0;
                            if ((uint64_t)((uint32_t)var_29 + 1U) != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            if ((uint64_t)((uint32_t)var_33 + 1U) == 0UL) {
                                continue;
                            }
                            loop_state_var = 0U;
                            break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 1U:
                        {
                            var_36 = local_sp_3 + (-24L);
                            *(uint64_t *)var_36 = 4210933UL;
                            var_37 = indirect_placeholder_53(0UL, rcx_4, 4264968UL, r9_0_ph, 1UL, r8_0_ph, 0UL);
                            local_sp_4 = var_36;
                            rdx_0 = var_37.field_2;
                            r9_1 = var_37.field_3;
                            r8_1 = var_37.field_4;
                        }
                        break;
                      case 0U:
                        {
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    if (*var_13 > *(uint64_t *)(local_sp_0 + 16UL)) {
        var_44 = local_sp_0 + (-8L);
        *(uint64_t *)var_44 = 4210881UL;
        indirect_placeholder();
        local_sp_1 = var_44;
    } else {
        rcx_3 = rcx_2;
        if (*var_14 > *(uint64_t *)(local_sp_0 + 40UL)) {
            var_44 = local_sp_0 + (-8L);
            *(uint64_t *)var_44 = 4210881UL;
            indirect_placeholder();
            local_sp_1 = var_44;
        }
    }
    *var_16 = *(uint64_t *)(local_sp_1 + 24UL);
    *var_18 = *(uint64_t *)(local_sp_1 + 32UL);
    rcx_5 = rcx_3;
    mrv.field_0 = rcx_5;
    mrv1 = mrv;
    mrv1.field_1 = r9_2;
    mrv2 = mrv1;
    mrv2.field_2 = r8_2;
    return mrv2;
}
