typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_6_ret_type;
struct indirect_placeholder_5_ret_type;
struct indirect_placeholder_4_ret_type;
struct indirect_placeholder_6_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_5_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_4_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_rax(void);
extern uint64_t init_r9(void);
extern struct indirect_placeholder_6_ret_type indirect_placeholder_6(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_5_ret_type indirect_placeholder_5(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_4_ret_type indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
void bb_wrap_write(uint64_t rcx, uint64_t rdx, uint64_t rdi, uint64_t r8, uint64_t rsi) {
    struct indirect_placeholder_6_ret_type var_29;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_27;
    uint64_t var_28;
    uint32_t *rax_0;
    uint64_t local_sp_2_ph;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t rcx1_2_ph;
    uint64_t r15_0_ph;
    uint64_t rcx1_0;
    uint64_t r9_0;
    uint64_t r84_0;
    uint64_t local_sp_0;
    uint64_t var_30;
    uint64_t var_31;
    struct indirect_placeholder_5_ret_type var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint32_t *rax_1;
    uint64_t r15_0;
    uint32_t *phitmp74;
    uint64_t rcx1_1;
    uint64_t r9_1;
    uint64_t r84_1;
    uint64_t local_sp_1;
    uint64_t var_36;
    uint64_t *var_11;
    uint64_t r13_0_ph;
    uint64_t var_12;
    bool var_13;
    bool var_14;
    uint64_t local_sp_2;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbp();
    var_3 = init_r15();
    var_4 = init_r14();
    var_5 = init_r12();
    var_6 = init_rbx();
    var_7 = init_r9();
    var_8 = init_r13();
    var_9 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_8;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    *(uint64_t *)(var_0 + (-48L)) = var_6;
    var_10 = var_0 + (-72L);
    *(uint64_t *)(var_0 + (-64L)) = rdi;
    rax_0 = (uint32_t *)0UL;
    local_sp_2_ph = var_10;
    rcx1_2_ph = rcx;
    r9_0 = var_7;
    r84_0 = r8;
    rax_1 = (uint32_t *)0UL;
    r9_1 = var_7;
    r84_1 = r8;
    r13_0_ph = 0UL;
    if (rdx != 0UL) {
        if (rsi == 0UL) {
            return;
        }
        var_11 = (uint64_t *)rcx;
        r15_0_ph = *var_11;
        while (1U)
            {
                var_12 = rsi - r13_0_ph;
                var_13 = ((uint64_t)((uint32_t)var_12 + 1U) == 0UL);
                var_14 = (r13_0_ph < rsi);
                rcx1_1 = rcx1_2_ph;
                r15_0 = r15_0_ph;
                local_sp_2 = local_sp_2_ph;
                while (1U)
                    {
                        var_15 = rdx - r15_0;
                        var_16 = (var_12 > var_15) ? var_15 : var_12;
                        r15_0 = 0UL;
                        if (var_16 != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_24 = local_sp_2 + (-8L);
                        *(uint64_t *)var_24 = 4205485UL;
                        indirect_placeholder();
                        local_sp_1 = var_24;
                        local_sp_2 = var_24;
                        if (var_13) {
                            *var_11 = 0UL;
                            if (var_14) {
                                continue;
                            }
                            loop_state_var = 0U;
                            break;
                        }
                        phitmp74 = (uint32_t *)var_12;
                        rax_1 = phitmp74;
                        loop_state_var = 2U;
                        break;
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 1U:
                    {
                        var_17 = *(uint64_t *)(local_sp_2 + 8UL);
                        var_18 = *(uint64_t *)4283968UL;
                        var_19 = local_sp_2 + (-8L);
                        *(uint64_t *)var_19 = 4205560UL;
                        indirect_placeholder();
                        var_20 = helper_cc_compute_c_wrapper(var_17 - var_16, var_16, var_9, 17U);
                        rcx1_0 = var_18;
                        local_sp_0 = var_19;
                        rcx1_2_ph = var_18;
                        local_sp_2_ph = var_19;
                        if (var_20 == 0UL) {
                            var_21 = *var_11;
                            var_22 = r13_0_ph + var_16;
                            var_23 = var_21 + var_16;
                            *var_11 = var_23;
                            r15_0_ph = var_23;
                            r13_0_ph = var_22;
                            if (var_22 < rsi) {
                                continue;
                            }
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        rax_0 = (uint32_t *)var_17;
                        loop_state_var = 2U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 0U:
                    {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 2U:
                    {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
        switch (loop_state_var) {
          case 0U:
            {
                return;
            }
            break;
          case 2U:
          case 1U:
            {
                switch (loop_state_var) {
                  case 1U:
                    {
                        *(uint64_t *)(local_sp_1 + (-8L)) = 4205693UL;
                        indirect_placeholder();
                        var_36 = (uint64_t)*rax_1;
                        *(uint64_t *)(local_sp_1 + (-16L)) = 4205712UL;
                        indirect_placeholder_4(0UL, rcx1_1, 4260951UL, r9_1, 1UL, r84_1, var_36);
                        abort();
                    }
                    break;
                  case 2U:
                    {
                        break;
                    }
                    break;
                }
            }
            break;
        }
    }
    var_25 = *(uint64_t *)4283968UL;
    *(uint64_t *)(var_0 + (-80L)) = 4205628UL;
    indirect_placeholder();
    var_26 = helper_cc_compute_c_wrapper(var_1 - rsi, rsi, var_9, 17U);
    if (var_26 != 0UL) {
        return;
    }
    *(uint64_t *)(var_0 + (-88L)) = 4205638UL;
    indirect_placeholder();
    var_27 = (uint64_t)*(uint32_t *)var_1;
    var_28 = var_0 + (-96L);
    *(uint64_t *)var_28 = 4205657UL;
    var_29 = indirect_placeholder_6(0UL, var_25, 4260951UL, var_7, 1UL, r8, var_27);
    rcx1_0 = var_29.field_0;
    r9_0 = var_29.field_1;
    r84_0 = var_29.field_2;
    local_sp_0 = var_28;
}
