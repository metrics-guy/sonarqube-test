typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_16_ret_type;
struct indirect_placeholder_17_ret_type;
struct indirect_placeholder_16_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_17_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_rax(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
extern struct indirect_placeholder_16_ret_type indirect_placeholder_16(uint64_t param_0);
extern struct indirect_placeholder_17_ret_type indirect_placeholder_17(uint64_t param_0);
void bb_print_long_entry(uint64_t rdi) {
    uint64_t rax_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t local_sp_0;
    uint64_t var_23;
    uint64_t local_sp_5;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t local_sp_7;
    uint64_t local_sp_3;
    uint64_t var_16;
    uint64_t local_sp_6;
    uint64_t local_sp_4;
    uint64_t rax_2;
    uint64_t var_17;
    struct indirect_placeholder_16_ret_type var_18;
    uint64_t var_19;
    bool var_20;
    uint64_t var_21;
    uint64_t *var_22;
    struct indirect_placeholder_17_ret_type var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_10;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbp();
    var_3 = init_r14();
    var_4 = init_r12();
    var_5 = init_rbx();
    var_6 = init_r13();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_2;
    *(uint64_t *)(var_0 + (-40L)) = var_5;
    *(uint64_t *)(var_0 + (-1072L)) = 4205415UL;
    indirect_placeholder_1();
    *(uint64_t *)(var_0 + (-1080L)) = 4205430UL;
    indirect_placeholder_1();
    *(uint64_t *)(var_0 + (-1088L)) = 4205445UL;
    indirect_placeholder_1();
    *(uint64_t *)(var_0 + (-1096L)) = 4205457UL;
    indirect_placeholder_1();
    rax_0 = 0UL;
    rax_2 = 0UL;
    if (var_1 == 0UL) {
        *(uint64_t *)(var_0 + (-1104L)) = 4206041UL;
        indirect_placeholder_1();
    } else {
        var_7 = *(uint64_t *)(var_1 + 24UL);
        *(uint64_t *)(var_0 + (-1104L)) = 4205483UL;
        indirect_placeholder_1();
        var_8 = *(uint64_t *)var_1;
        *(uint64_t *)(var_0 + (-1112L)) = 4205506UL;
        indirect_placeholder(var_7, var_8);
        *(uint64_t *)(var_0 + (-1120L)) = 4205524UL;
        indirect_placeholder_1();
        *(uint64_t *)(var_0 + (-1128L)) = 4205532UL;
        indirect_placeholder_1();
        var_9 = var_0 + (-1136L);
        *(uint64_t *)var_9 = 4205542UL;
        indirect_placeholder_1();
        local_sp_4 = var_9;
        local_sp_7 = var_9;
        if (*(unsigned char *)4285058UL == '\x00') {
            if (*(unsigned char *)4285060UL != '\x00') {
                local_sp_5 = local_sp_4;
                local_sp_6 = local_sp_4;
                if (*(unsigned char *)4285059UL != '\x00') {
                    *(uint64_t *)(local_sp_5 + (-8L)) = 4205583UL;
                    indirect_placeholder_1();
                    return;
                }
                *(uint64_t *)(local_sp_6 + (-8L)) = 4205859UL;
                indirect_placeholder_1();
                var_17 = rax_2 + 7UL;
                *(uint64_t *)(local_sp_6 + (-16L)) = 4205868UL;
                var_18 = indirect_placeholder_16(var_17);
                var_19 = var_18.field_0;
                *(uint64_t *)(local_sp_6 + (-24L)) = 4205883UL;
                indirect_placeholder_1();
                *(uint32_t *)var_19 = 1819291183U;
                *(uint16_t *)(var_19 + 4UL) = (uint16_t)(unsigned short)28257U;
                *(unsigned char *)(var_19 + 6UL) = (unsigned char)'\x00';
                *(uint64_t *)(local_sp_6 + (-32L)) = 4205915UL;
                indirect_placeholder_1();
                var_20 = (var_19 == 0UL);
                var_21 = local_sp_6 + (-40L);
                var_22 = (uint64_t *)var_21;
                local_sp_0 = var_21;
                local_sp_5 = var_21;
                if (var_20) {
                    *var_22 = 4206013UL;
                    indirect_placeholder_1();
                    *(uint64_t *)(local_sp_5 + (-8L)) = 4205583UL;
                    indirect_placeholder_1();
                    return;
                }
                *var_22 = 4205936UL;
                indirect_placeholder_1();
                while (1U)
                    {
                        *(uint64_t *)(local_sp_0 + (-8L)) = 4205989UL;
                        indirect_placeholder_1();
                        var_23 = local_sp_0 + (-16L);
                        *(uint64_t *)var_23 = 4205968UL;
                        indirect_placeholder_1();
                        local_sp_0 = var_23;
                        continue;
                    }
            }
        }
        *(uint64_t *)(var_0 + (-1144L)) = 4205612UL;
        indirect_placeholder_1();
        *(uint64_t *)(var_0 + (-1152L)) = 4205628UL;
        indirect_placeholder_1();
        *(uint64_t *)(var_0 + (-1160L)) = 4205640UL;
        indirect_placeholder_1();
        *(uint64_t *)(var_0 + (-1168L)) = 4205656UL;
        indirect_placeholder_1();
        var_10 = var_0 + (-1176L);
        *(uint64_t *)var_10 = 4205666UL;
        indirect_placeholder_1();
        local_sp_4 = var_10;
        local_sp_7 = var_10;
        if (*(unsigned char *)4285060UL != '\x00') {
            local_sp_5 = local_sp_4;
            local_sp_6 = local_sp_4;
            if (*(unsigned char *)4285059UL != '\x00') {
                *(uint64_t *)(local_sp_5 + (-8L)) = 4205583UL;
                indirect_placeholder_1();
                return;
            }
            *(uint64_t *)(local_sp_6 + (-8L)) = 4205859UL;
            indirect_placeholder_1();
            var_17 = rax_2 + 7UL;
            *(uint64_t *)(local_sp_6 + (-16L)) = 4205868UL;
            var_18 = indirect_placeholder_16(var_17);
            var_19 = var_18.field_0;
            *(uint64_t *)(local_sp_6 + (-24L)) = 4205883UL;
            indirect_placeholder_1();
            *(uint32_t *)var_19 = 1819291183U;
            *(uint16_t *)(var_19 + 4UL) = (uint16_t)(unsigned short)28257U;
            *(unsigned char *)(var_19 + 6UL) = (unsigned char)'\x00';
            *(uint64_t *)(local_sp_6 + (-32L)) = 4205915UL;
            indirect_placeholder_1();
            var_20 = (var_19 == 0UL);
            var_21 = local_sp_6 + (-40L);
            var_22 = (uint64_t *)var_21;
            local_sp_0 = var_21;
            local_sp_5 = var_21;
            if (var_20) {
                *var_22 = 4206013UL;
                indirect_placeholder_1();
                *(uint64_t *)(local_sp_5 + (-8L)) = 4205583UL;
                indirect_placeholder_1();
                return;
            }
            *var_22 = 4205936UL;
            indirect_placeholder_1();
            while (1U)
                {
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4205989UL;
                    indirect_placeholder_1();
                    var_23 = local_sp_0 + (-16L);
                    *(uint64_t *)var_23 = 4205968UL;
                    indirect_placeholder_1();
                    local_sp_0 = var_23;
                    continue;
                }
        }
        *(uint64_t *)(local_sp_7 + (-8L)) = 4205684UL;
        indirect_placeholder_1();
        *(uint64_t *)(local_sp_7 + (-16L)) = 4205693UL;
        var_11 = indirect_placeholder_17(10UL);
        var_12 = var_11.field_0;
        *(uint64_t *)(local_sp_7 + (-24L)) = 4205708UL;
        indirect_placeholder_1();
        *(uint64_t *)var_12 = 7162247809296510511UL;
        *(uint16_t *)(var_12 + 8UL) = (uint16_t)(unsigned short)116U;
        var_13 = local_sp_7 + (-32L);
        *(uint64_t *)var_13 = 4205743UL;
        indirect_placeholder_1();
        local_sp_3 = var_13;
        if (var_12 == 0UL) {
            *(uint64_t *)(local_sp_7 + (-40L)) = 4205766UL;
            indirect_placeholder_1();
            *(uint64_t *)(local_sp_7 + (-48L)) = 4205813UL;
            indirect_placeholder_1();
            var_14 = local_sp_7 + (-56L);
            *(uint64_t *)var_14 = 4205829UL;
            var_15 = indirect_placeholder_2(var_12);
            local_sp_3 = var_14;
            rax_0 = var_15;
        }
        var_16 = local_sp_3 + (-8L);
        *(uint64_t *)var_16 = 4205837UL;
        indirect_placeholder_1();
        local_sp_5 = var_16;
        local_sp_6 = var_16;
        rax_2 = rax_0;
        if (*(unsigned char *)4285059UL != '\x00') {
            *(uint64_t *)(local_sp_5 + (-8L)) = 4205583UL;
            indirect_placeholder_1();
            return;
        }
        *(uint64_t *)(local_sp_6 + (-8L)) = 4205859UL;
        indirect_placeholder_1();
        var_17 = rax_2 + 7UL;
        *(uint64_t *)(local_sp_6 + (-16L)) = 4205868UL;
        var_18 = indirect_placeholder_16(var_17);
        var_19 = var_18.field_0;
        *(uint64_t *)(local_sp_6 + (-24L)) = 4205883UL;
        indirect_placeholder_1();
        *(uint32_t *)var_19 = 1819291183U;
        *(uint16_t *)(var_19 + 4UL) = (uint16_t)(unsigned short)28257U;
        *(unsigned char *)(var_19 + 6UL) = (unsigned char)'\x00';
        *(uint64_t *)(local_sp_6 + (-32L)) = 4205915UL;
        indirect_placeholder_1();
        var_20 = (var_19 == 0UL);
        var_21 = local_sp_6 + (-40L);
        var_22 = (uint64_t *)var_21;
        local_sp_0 = var_21;
        local_sp_5 = var_21;
        if (!var_20) {
            *var_22 = 4205936UL;
            indirect_placeholder_1();
            while (1U)
                {
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4205989UL;
                    indirect_placeholder_1();
                    var_23 = local_sp_0 + (-16L);
                    *(uint64_t *)var_23 = 4205968UL;
                    indirect_placeholder_1();
                    local_sp_0 = var_23;
                    continue;
                }
        }
        *var_22 = 4206013UL;
        indirect_placeholder_1();
        *(uint64_t *)(local_sp_5 + (-8L)) = 4205583UL;
        indirect_placeholder_1();
    }
}
