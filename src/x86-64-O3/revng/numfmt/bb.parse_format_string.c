typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_parse_format_string_ret_type;
struct indirect_placeholder_110_ret_type;
struct indirect_placeholder_108_ret_type;
struct indirect_placeholder_114_ret_type;
struct indirect_placeholder_112_ret_type;
struct indirect_placeholder_111_ret_type;
struct indirect_placeholder_115_ret_type;
struct indirect_placeholder_105_ret_type;
struct indirect_placeholder_116_ret_type;
struct indirect_placeholder_104_ret_type;
struct indirect_placeholder_117_ret_type;
struct indirect_placeholder_106_ret_type;
struct indirect_placeholder_118_ret_type;
struct indirect_placeholder_107_ret_type;
struct indirect_placeholder_119_ret_type;
struct indirect_placeholder_109_ret_type;
struct bb_parse_format_string_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_110_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
    uint64_t field_8;
};
struct indirect_placeholder_108_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_114_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
    uint64_t field_8;
    uint64_t field_9;
};
struct indirect_placeholder_112_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
    uint64_t field_8;
    uint64_t field_9;
};
struct indirect_placeholder_111_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
    uint64_t field_8;
    uint64_t field_9;
};
struct indirect_placeholder_115_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
    uint64_t field_8;
};
struct indirect_placeholder_105_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_116_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
    uint64_t field_8;
};
struct indirect_placeholder_104_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_117_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
    uint64_t field_8;
};
struct indirect_placeholder_106_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_118_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
    uint64_t field_8;
};
struct indirect_placeholder_107_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_119_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
    uint64_t field_8;
};
struct indirect_placeholder_109_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r13(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0);
extern uint64_t indirect_placeholder_10(uint64_t param_0, uint64_t param_1);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern uint64_t init_rcx(void);
extern struct indirect_placeholder_110_ret_type indirect_placeholder_110(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_108_ret_type indirect_placeholder_108(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_114_ret_type indirect_placeholder_114(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
extern struct indirect_placeholder_112_ret_type indirect_placeholder_112(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
extern struct indirect_placeholder_111_ret_type indirect_placeholder_111(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
extern struct indirect_placeholder_115_ret_type indirect_placeholder_115(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_105_ret_type indirect_placeholder_105(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_116_ret_type indirect_placeholder_116(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_104_ret_type indirect_placeholder_104(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_117_ret_type indirect_placeholder_117(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_106_ret_type indirect_placeholder_106(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_118_ret_type indirect_placeholder_118(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_107_ret_type indirect_placeholder_107(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_119_ret_type indirect_placeholder_119(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_109_ret_type indirect_placeholder_109(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
struct bb_parse_format_string_ret_type bb_parse_format_string(uint64_t rdi) {
    struct indirect_placeholder_110_ret_type var_67;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t rbx_7;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t local_sp_0;
    uint64_t r15_0;
    uint64_t storemerge;
    uint64_t r12_0;
    uint64_t rbx_0;
    uint64_t r13_0;
    uint64_t rcx_5;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_28;
    unsigned char var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t local_sp_9;
    uint64_t local_sp_8;
    uint64_t var_32;
    unsigned char *_cast2;
    unsigned char var_33;
    uint64_t local_sp_7;
    uint64_t local_sp_5;
    uint64_t rcx_3;
    uint64_t rcx_4;
    uint64_t var_43;
    uint64_t spec_select;
    struct indirect_placeholder_114_ret_type var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t rsi_0;
    struct indirect_placeholder_112_ret_type var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    struct indirect_placeholder_111_ret_type var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t r9_3;
    uint64_t r8_3;
    struct bb_parse_format_string_ret_type mrv;
    struct bb_parse_format_string_ret_type mrv1;
    struct bb_parse_format_string_ret_type mrv2;
    uint64_t rdx_0_be;
    uint64_t rax_1_be;
    uint64_t rax_1;
    uint64_t rdx_0;
    unsigned char var_34;
    uint64_t var_35;
    uint64_t local_sp_6;
    uint64_t var_36;
    unsigned char var_37;
    uint64_t r15_1;
    uint64_t r12_1;
    uint64_t rbx_2;
    uint64_t r13_1;
    struct indirect_placeholder_115_ret_type var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_38;
    uint64_t var_39;
    unsigned char var_40;
    uint64_t rcx_6;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t r15_2;
    uint64_t r12_2;
    uint64_t rbx_3;
    uint64_t r13_2;
    struct indirect_placeholder_116_ret_type var_95;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t var_98;
    uint64_t var_62;
    uint32_t *var_63;
    uint64_t var_64;
    uint64_t r15_3;
    uint64_t r12_3;
    uint64_t r13_4;
    uint64_t rbx_4;
    uint64_t r13_3;
    struct indirect_placeholder_117_ret_type var_81;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t local_sp_10;
    uint64_t r15_4;
    uint64_t r12_4;
    uint64_t rbx_5;
    struct indirect_placeholder_118_ret_type var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_27;
    uint64_t rbx_8;
    uint64_t rbx_7_be;
    uint64_t local_sp_11;
    uint64_t local_sp_11_ph;
    uint64_t rax_2;
    uint64_t r12_5;
    uint64_t rbx_6;
    uint64_t var_11;
    unsigned char var_19;
    uint64_t var_20;
    bool var_21;
    uint64_t rbx_7_ph;
    uint64_t r13_5_ph;
    uint64_t var_22;
    uint64_t *var_23;
    uint64_t var_24;
    uint64_t var_25;
    unsigned char var_26;
    struct indirect_placeholder_119_ret_type var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rcx();
    var_2 = init_rbp();
    var_3 = init_r15();
    var_4 = init_r14();
    var_5 = init_r12();
    var_6 = init_rbx();
    var_7 = init_r9();
    var_8 = init_r13();
    var_9 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_8;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    *(uint64_t *)(var_0 + (-48L)) = var_6;
    var_10 = var_0 + (-88L);
    *(uint64_t *)(var_0 + (-64L)) = 0UL;
    r15_0 = var_3;
    storemerge = 1UL;
    r13_0 = var_8;
    rcx_5 = var_1;
    rcx_3 = var_1;
    r9_3 = var_7;
    r8_3 = var_9;
    r15_1 = var_3;
    r15_2 = var_3;
    r15_3 = var_3;
    r15_4 = var_3;
    rbx_5 = 0UL;
    local_sp_11_ph = var_10;
    rax_2 = 0UL;
    r12_5 = 0UL;
    rbx_6 = var_6;
    r13_5_ph = 0UL;
    while (1U)
        {
            var_11 = rax_2 + rdi;
            r12_0 = r12_5;
            rbx_0 = rbx_6;
            r12_1 = r12_5;
            r12_2 = r12_5;
            r12_3 = r12_5;
            r12_4 = r12_5;
            rbx_8 = rbx_6;
            switch_state_var = 0;
            switch (*(unsigned char *)var_11) {
              case '%':
                {
                    var_19 = *(unsigned char *)(var_11 + 1UL);
                    var_20 = rax_2 + 1UL;
                    rbx_7_ph = var_20;
                    rbx_8 = var_20;
                    storemerge = 2UL;
                    if (var_19 == '%') {
                        rax_2 = rax_2 + storemerge;
                        r12_5 = r12_5 + 1UL;
                        rbx_6 = rbx_8;
                        continue;
                    }
                    var_21 = (rax_2 == 0UL);
                    while (1U)
                        {
                            rbx_7 = rbx_7_ph;
                            r13_0 = r13_5_ph;
                            r13_2 = r13_5_ph;
                            r13_4 = r13_5_ph;
                            r13_3 = r13_5_ph;
                            local_sp_11 = local_sp_11_ph;
                            r13_5_ph = 1UL;
                            while (1U)
                                {
                                    var_22 = local_sp_11 + (-8L);
                                    var_23 = (uint64_t *)var_22;
                                    *var_23 = 4208330UL;
                                    indirect_placeholder();
                                    var_24 = rbx_7 + rax_2;
                                    var_25 = var_24 + rdi;
                                    var_26 = *(unsigned char *)var_25;
                                    rbx_7_be = var_24;
                                    local_sp_11_ph = var_22;
                                    local_sp_11 = var_22;
                                    if ((uint64_t)(var_26 + '\xd9') != 0UL) {
                                        if ((uint64_t)(var_26 + '\xd0') != 0UL) {
                                            loop_state_var = 0U;
                                            break;
                                        }
                                        if (!var_21) {
                                            *(uint64_t *)local_sp_11 = var_25;
                                            *(uint64_t *)(local_sp_11 + (-16L)) = 4208374UL;
                                            indirect_placeholder();
                                            *(volatile uint32_t *)(uint32_t *)0UL = 0U;
                                            *(uint64_t *)(local_sp_11 + (-24L)) = 4208400UL;
                                            indirect_placeholder();
                                            var_27 = local_sp_11 + (-32L);
                                            *(uint64_t *)var_27 = 4208408UL;
                                            indirect_placeholder();
                                            local_sp_9 = var_27;
                                            local_sp_8 = var_27;
                                            local_sp_7 = var_27;
                                            local_sp_5 = var_27;
                                            local_sp_6 = var_27;
                                            local_sp_10 = var_27;
                                            if (*(volatile uint32_t *)(uint32_t *)0UL != 34U) {
                                                loop_state_var = 5U;
                                                break;
                                            }
                                            var_28 = *var_23;
                                            var_29 = *(unsigned char *)var_28;
                                            var_30 = (uint64_t)var_29;
                                            var_31 = var_28 - rdi;
                                            rbx_2 = var_31;
                                            rbx_3 = var_31;
                                            rbx_4 = var_31;
                                            if (var_29 != '\x00') {
                                                loop_state_var = 6U;
                                                break;
                                            }
                                            if ((uint64_t)(var_29 + '\xd2') != 0UL) {
                                                if ((uint64_t)(var_29 + '\x9a') != 0UL) {
                                                    loop_state_var = 4U;
                                                    break;
                                                }
                                                var_32 = var_28 + 1UL;
                                                _cast2 = (unsigned char *)var_32;
                                                var_33 = *_cast2;
                                                r13_1 = var_32;
                                                if (var_33 != '\x00') {
                                                    if (r12_5 != 0UL) {
                                                        loop_state_var = 3U;
                                                        break;
                                                    }
                                                    loop_state_var = 1U;
                                                    break;
                                                }
                                                rax_1 = var_31 + 1UL;
                                                rdx_0 = (uint64_t)var_33;
                                                loop_state_var = 2U;
                                                break;
                                            }
                                            *(uint64_t *)(local_sp_11 + (-40L)) = 4208976UL;
                                            indirect_placeholder();
                                            var_62 = var_28 + 1UL;
                                            var_63 = (uint32_t *)var_30;
                                            *var_63 = 0U;
                                            *(uint64_t *)(local_sp_11 + (-48L)) = 4209005UL;
                                            indirect_placeholder();
                                            *(uint64_t *)4309680UL = var_30;
                                            var_64 = local_sp_11 + (-56L);
                                            *(uint64_t *)var_64 = 4209017UL;
                                            indirect_placeholder();
                                            local_sp_0 = var_64;
                                            rbx_0 = var_62;
                                            if (*var_63 != 34U) {
                                                loop_state_var = 7U;
                                                break;
                                            }
                                            if ((long)*(uint64_t *)4309680UL >= (long)0UL) {
                                                loop_state_var = 7U;
                                                break;
                                            }
                                            var_65 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)var_62;
                                            var_66 = local_sp_11 + (-64L);
                                            *(uint64_t *)var_66 = 4209042UL;
                                            indirect_placeholder();
                                            local_sp_0 = var_66;
                                            rbx_0 = var_65;
                                            loop_state_var = 7U;
                                            break;
                                        }
                                    }
                                    *(uint32_t *)4310064UL = 1U;
                                    rbx_7_be = var_24 + 1UL;
                                }
                            switch_state_var = 0;
                            switch (loop_state_var) {
                              case 0U:
                                {
                                    rbx_7_ph = var_24 + 1UL;
                                    continue;
                                }
                                break;
                              case 1U:
                                {
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              case 2U:
                                {
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              case 3U:
                                {
                                    loop_state_var = 2U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              case 4U:
                                {
                                    loop_state_var = 3U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              case 5U:
                                {
                                    loop_state_var = 4U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              case 6U:
                                {
                                    loop_state_var = 5U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              case 7U:
                                {
                                    loop_state_var = 6U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                            }
                            if (switch_state_var)
                                break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 1U:
                        {
                            while (1U)
                                {
                                    var_34 = *(unsigned char *)((rax_1 + rdi) + 1UL);
                                    var_35 = (uint64_t)var_34;
                                    rax_1_be = rax_1 + 1UL;
                                    rdx_0_be = var_35;
                                    rcx_5 = var_35;
                                    rcx_6 = var_35;
                                    if ((uint64_t)((unsigned char)rdx_0 + '\xdb') != 0UL) {
                                        if (var_34 != '\x00') {
                                            loop_state_var = 1U;
                                            break;
                                        }
                                    }
                                    if ((uint64_t)(var_34 + '\xdb') != 0UL) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    var_36 = rax_1 + 2UL;
                                    var_37 = *(unsigned char *)(var_36 + rdi);
                                    rax_1_be = var_36;
                                    if (var_37 != '\x00') {
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    rdx_0_be = (uint64_t)var_37;
                                }
                            switch_state_var = 0;
                            switch (loop_state_var) {
                              case 1U:
                                {
                                    if (r12_5 != 0UL) {
                                        loop_state_var = 2U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              case 0U:
                                {
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                            }
                            if (switch_state_var)
                                break;
                        }
                        break;
                      case 0U:
                        {
                            loop_state_var = 2U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 2U:
                        {
                            loop_state_var = 3U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 3U:
                        {
                            loop_state_var = 4U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 4U:
                        {
                            loop_state_var = 5U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 5U:
                        {
                            loop_state_var = 6U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 6U:
                        {
                            loop_state_var = 7U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
                break;
              case '\x00':
                {
                    *(uint64_t *)(var_0 + (-96L)) = 4209112UL;
                    var_12 = indirect_placeholder_119(rdi, var_3, var_4, r12_5, rbx_6, rdi, var_8);
                    var_13 = var_12.field_0;
                    var_14 = var_12.field_2;
                    var_15 = var_12.field_3;
                    var_16 = var_12.field_5;
                    var_17 = var_12.field_8;
                    var_18 = var_0 + (-104L);
                    *(uint64_t *)var_18 = 4209134UL;
                    indirect_placeholder_109(0UL, var_13, 4272195UL, var_16, 1UL, 0UL, var_17);
                    local_sp_0 = var_18;
                    r15_0 = var_14;
                    r12_0 = var_15;
                    loop_state_var = 7U;
                    switch_state_var = 1;
                    break;
                }
                break;
              default:
                {
                    rax_2 = rax_2 + storemerge;
                    r12_5 = r12_5 + 1UL;
                    rbx_6 = rbx_8;
                    continue;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 3U:
      case 2U:
      case 1U:
        {
            switch (loop_state_var) {
              case 1U:
                {
                    var_41 = local_sp_7 + (-8L);
                    *(uint64_t *)var_41 = 4208934UL;
                    var_42 = indirect_placeholder_5(var_32);
                    *(uint64_t *)4310008UL = var_42;
                    local_sp_5 = var_41;
                    rcx_3 = rcx_6;
                }
                break;
              case 3U:
                {
                    rcx_4 = rcx_3;
                    if (*(unsigned char *)4309968UL != '\x00') {
                        var_43 = *(uint64_t *)4310008UL;
                        spec_select = (var_43 == 0UL) ? 4288031UL : var_43;
                        *(uint64_t *)(local_sp_5 + (-8L)) = 4208682UL;
                        var_44 = indirect_placeholder_114(rdi, var_3, var_4, r12_5, var_31, 2UL, spec_select, var_32);
                        var_45 = var_44.field_0;
                        var_46 = var_44.field_2;
                        var_47 = var_44.field_3;
                        var_48 = var_44.field_9;
                        var_49 = *(uint64_t *)4310016UL;
                        rsi_0 = (var_49 == 0UL) ? 4288031UL : var_49;
                        *(uint64_t *)(local_sp_5 + (-16L)) = 4208722UL;
                        var_50 = indirect_placeholder_112(var_46, var_47, var_45, 4272188UL, 4272177UL, 1UL, rsi_0, var_48);
                        var_51 = var_50.field_0;
                        var_52 = var_50.field_2;
                        var_53 = var_50.field_4;
                        var_54 = *(uint64_t *)4310040UL;
                        var_55 = (*(uint32_t *)4309672UL == 0U) ? var_50.field_6 : 4272182UL;
                        var_56 = (*(uint32_t *)4310064UL == 0U) ? 4272192UL : var_50.field_5;
                        *(uint64_t *)(local_sp_5 + (-24L)) = 4208774UL;
                        var_57 = indirect_placeholder_111(var_52, var_54, var_53, var_56, var_55, 0UL, var_52, var_51);
                        var_58 = var_57.field_3;
                        var_59 = var_57.field_5;
                        var_60 = var_57.field_6;
                        var_61 = var_57.field_9;
                        *(uint64_t *)(local_sp_5 + (-32L)) = var_57.field_4;
                        *(uint64_t *)(local_sp_5 + (-40L)) = var_61;
                        *(uint64_t *)(local_sp_5 + (-48L)) = 4208809UL;
                        indirect_placeholder();
                        rcx_4 = var_59;
                        r9_3 = var_60;
                        r8_3 = var_58;
                    }
                    mrv.field_0 = rcx_4;
                    mrv1 = mrv;
                    mrv1.field_1 = r9_3;
                    mrv2 = mrv1;
                    mrv2.field_2 = r8_3;
                    return mrv2;
                }
                break;
              case 2U:
                {
                    var_38 = var_27 + (-8L);
                    *(uint64_t *)var_38 = 4208625UL;
                    var_39 = indirect_placeholder_10(rdi, r12_5);
                    var_40 = *_cast2;
                    *(uint64_t *)4310016UL = var_39;
                    local_sp_5 = var_38;
                    rcx_3 = rcx_5;
                    local_sp_7 = var_38;
                    rcx_6 = rcx_5;
                    if (var_40 == '\x00') {
                        var_41 = local_sp_7 + (-8L);
                        *(uint64_t *)var_41 = 4208934UL;
                        var_42 = indirect_placeholder_5(var_32);
                        *(uint64_t *)4310008UL = var_42;
                        local_sp_5 = var_41;
                        rcx_3 = rcx_6;
                    }
                }
                break;
            }
        }
        break;
      case 7U:
      case 6U:
      case 5U:
      case 4U:
      case 0U:
        {
            switch (loop_state_var) {
              case 4U:
                {
                    *(uint64_t *)(local_sp_8 + (-8L)) = 4209262UL;
                    var_95 = indirect_placeholder_116(rdi, r15_2, var_4, r12_2, rbx_3, rdi, r13_2);
                    var_96 = var_95.field_0;
                    var_97 = var_95.field_5;
                    var_98 = var_95.field_8;
                    *(uint64_t *)(local_sp_8 + (-16L)) = 4209284UL;
                    indirect_placeholder_104(0UL, var_96, 4274768UL, var_97, 1UL, 0UL, var_98);
                    abort();
                }
                break;
              case 7U:
              case 6U:
              case 5U:
              case 0U:
                {
                    switch (loop_state_var) {
                      case 0U:
                        {
                            *(uint64_t *)(local_sp_6 + (-8L)) = 4209232UL;
                            var_88 = indirect_placeholder_115(rdi, r15_1, var_4, r12_1, rbx_2, rdi, r13_1);
                            var_89 = var_88.field_0;
                            var_90 = var_88.field_2;
                            var_91 = var_88.field_3;
                            var_92 = var_88.field_5;
                            var_93 = var_88.field_8;
                            var_94 = local_sp_6 + (-16L);
                            *(uint64_t *)var_94 = 4209254UL;
                            indirect_placeholder_105(0UL, var_89, 4274832UL, var_92, 1UL, 0UL, var_93);
                            local_sp_8 = var_94;
                            r15_2 = var_90;
                            r12_2 = var_91;
                            rbx_3 = rbx_2;
                            r13_2 = r13_1;
                        }
                        break;
                      case 7U:
                      case 6U:
                      case 5U:
                        {
                            switch (loop_state_var) {
                              case 6U:
                                {
                                    *(uint64_t *)(local_sp_9 + (-8L)) = 4209202UL;
                                    var_81 = indirect_placeholder_117(rdi, r15_3, var_4, r12_3, rbx_4, rdi, r13_3);
                                    var_82 = var_81.field_0;
                                    var_83 = var_81.field_2;
                                    var_84 = var_81.field_3;
                                    var_85 = var_81.field_5;
                                    var_86 = var_81.field_8;
                                    var_87 = local_sp_9 + (-16L);
                                    *(uint64_t *)var_87 = 4209224UL;
                                    indirect_placeholder_106(0UL, var_82, 4272225UL, var_85, 1UL, 0UL, var_86);
                                    local_sp_6 = var_87;
                                    r15_1 = var_83;
                                    r12_1 = var_84;
                                    rbx_2 = rbx_4;
                                    r13_1 = r13_3;
                                }
                                break;
                              case 7U:
                              case 5U:
                                {
                                    switch (loop_state_var) {
                                      case 5U:
                                        {
                                            *(uint64_t *)(local_sp_10 + (-8L)) = 4209172UL;
                                            var_74 = indirect_placeholder_118(rdi, r15_4, var_4, r12_4, rbx_5, rdi, r13_4);
                                            var_75 = var_74.field_0;
                                            var_76 = var_74.field_2;
                                            var_77 = var_74.field_3;
                                            var_78 = var_74.field_5;
                                            var_79 = var_74.field_8;
                                            var_80 = local_sp_10 + (-16L);
                                            *(uint64_t *)var_80 = 4209194UL;
                                            indirect_placeholder_107(0UL, var_75, 4274656UL, var_78, 1UL, 0UL, var_79);
                                            local_sp_9 = var_80;
                                            r15_3 = var_76;
                                            r12_3 = var_77;
                                            rbx_4 = rbx_5;
                                            r13_3 = r13_4;
                                        }
                                        break;
                                      case 7U:
                                        {
                                            *(uint64_t *)(local_sp_0 + (-8L)) = 4209142UL;
                                            var_67 = indirect_placeholder_110(rdi, r15_0, var_4, r12_0, rbx_0, rdi, r13_0);
                                            var_68 = var_67.field_0;
                                            var_69 = var_67.field_2;
                                            var_70 = var_67.field_3;
                                            var_71 = var_67.field_5;
                                            var_72 = var_67.field_8;
                                            var_73 = local_sp_0 + (-16L);
                                            *(uint64_t *)var_73 = 4209164UL;
                                            indirect_placeholder_108(0UL, var_68, 4274736UL, var_71, 1UL, 0UL, var_72);
                                            r13_4 = r13_0;
                                            local_sp_10 = var_73;
                                            r15_4 = var_69;
                                            r12_4 = var_70;
                                            rbx_5 = rbx_0;
                                        }
                                        break;
                                    }
                                }
                                break;
                            }
                        }
                        break;
                    }
                }
                break;
            }
        }
        break;
    }
}
