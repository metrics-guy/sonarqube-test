typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_63_ret_type;
struct indirect_placeholder_63_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_15(uint64_t param_0);
extern uint32_t init_state_0x82fc(void);
extern struct indirect_placeholder_63_ret_type indirect_placeholder_63(uint64_t param_0, uint64_t param_1);
void bb_set_control_char_isra_0(uint64_t rcx, uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint32_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint32_t cc_op_0;
    uint64_t rcx1_0;
    uint64_t cc_src_0;
    uint64_t cc_dst_0;
    uint64_t rdi3_0;
    uint64_t rsi4_0;
    uint32_t cc_op_1;
    uint64_t cc_src_1;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t cc_dst_1;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint32_t cc_op_2;
    uint64_t rcx1_1;
    uint64_t cc_src_2;
    uint64_t rsi4_2;
    uint64_t cc_dst_2;
    uint64_t rdi3_1;
    uint64_t rsi4_1;
    uint32_t cc_op_3;
    uint64_t cc_src_3;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t cc_dst_3;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    unsigned char var_18;
    unsigned char var_19;
    uint64_t var_20;
    uint64_t cc_src_4;
    uint64_t var_21;
    unsigned char var_22;
    uint64_t rax_0;
    uint64_t cc_dst_4;
    uint64_t rcx1_2;
    uint64_t cc_src_5;
    uint64_t cc_dst_5;
    uint64_t rdi3_2;
    uint64_t cc_src_6;
    uint64_t cc_dst_6;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    struct indirect_placeholder_63_ret_type var_33;
    uint64_t var_34;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = init_state_0x82fc();
    var_4 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_5 = var_0 + (-24L);
    var_6 = (uint64_t)var_3;
    cc_op_0 = 17U;
    rcx1_0 = 4UL;
    cc_src_0 = 8UL;
    cc_dst_0 = var_5;
    rdi3_0 = 4268221UL;
    rsi4_0 = rdi;
    cc_op_2 = 22U;
    rcx1_1 = 5UL;
    cc_src_2 = 0UL;
    rsi4_2 = rdx;
    rdi3_1 = 4268225UL;
    rsi4_1 = rdi;
    cc_src_4 = 94UL;
    rax_0 = 0UL;
    rcx1_2 = 6UL;
    rdi3_2 = 4268230UL;
    cc_op_0 = 14U;
    cc_op_1 = cc_op_0;
    cc_src_1 = cc_src_0;
    cc_dst_1 = cc_dst_0;
    while (rcx1_0 != 0UL)
        {
            rcx1_0 = rcx1_0 + (-1L);
            rdi3_0 = rdi3_0 + var_6;
            rsi4_0 = rsi4_0 + var_6;
            cc_op_0 = 14U;
            cc_op_1 = cc_op_0;
            cc_src_1 = cc_src_0;
            cc_dst_1 = cc_dst_0;
        }
    var_9 = helper_cc_compute_all_wrapper(cc_dst_1, cc_src_1, var_4, cc_op_1);
    var_10 = ((var_9 & 65UL) == 0UL);
    var_11 = helper_cc_compute_c_wrapper(cc_dst_1, var_9, var_4, 1U);
    var_12 = (uint64_t)((unsigned char)var_10 - (unsigned char)var_11);
    if (var_12 == 0UL) {
        *(uint64_t *)(var_0 + (-32L)) = 4208768UL;
        var_33 = indirect_placeholder_63(rdx, 255UL);
        var_34 = var_33.field_0;
        rax_0 = var_34;
        *(unsigned char *)((*(uint64_t *)rsi + rcx) + 17UL) = (unsigned char)rax_0;
        return;
    }
    cc_dst_2 = (rdx & (-256L)) | var_12;
    cc_op_2 = 14U;
    cc_op_3 = cc_op_2;
    cc_src_3 = cc_src_2;
    cc_dst_3 = cc_dst_2;
    while (rcx1_1 != 0UL)
        {
            var_13 = (uint64_t)*(unsigned char *)rdi3_1;
            var_14 = (uint64_t)*(unsigned char *)rsi4_1 - var_13;
            cc_src_2 = var_13;
            cc_dst_2 = var_14;
            cc_op_3 = 14U;
            cc_src_3 = var_13;
            cc_dst_3 = var_14;
            if ((uint64_t)(unsigned char)var_14 == 0UL) {
                break;
            }
            rcx1_1 = rcx1_1 + (-1L);
            rdi3_1 = rdi3_1 + var_6;
            rsi4_1 = rsi4_1 + var_6;
            cc_op_2 = 14U;
            cc_op_3 = cc_op_2;
            cc_src_3 = cc_src_2;
            cc_dst_3 = cc_dst_2;
        }
    var_15 = helper_cc_compute_all_wrapper(cc_dst_3, cc_src_3, var_11, cc_op_3);
    var_16 = ((var_15 & 65UL) == 0UL);
    var_17 = helper_cc_compute_c_wrapper(cc_dst_3, var_15, var_11, 1U);
    if ((uint64_t)((unsigned char)var_16 - (unsigned char)var_17) == 0UL) {
        return;
    }
    var_18 = *(unsigned char *)rdx;
    if (var_18 == '\x00') {
        var_31 = (uint64_t)(uint32_t)(uint64_t)var_18;
        *(uint64_t *)(var_0 + (-32L)) = 4208904UL;
        var_32 = indirect_placeholder_15(var_31);
        *(unsigned char *)((*(uint64_t *)rsi + rcx) + 17UL) = (unsigned char)var_32;
        return;
    }
    var_19 = *(unsigned char *)(rdx + 1UL);
    if (var_19 != '\x00') {
        var_20 = (uint64_t)var_18 + (-94L);
        cc_dst_4 = var_20;
        if ((uint64_t)(unsigned char)var_20 != 0UL) {
            var_21 = (uint64_t)var_19 + (-45L);
            cc_src_4 = 45UL;
            cc_dst_4 = var_21;
            var_22 = *(unsigned char *)(rdx + 2UL);
            cc_src_4 = 0UL;
            cc_dst_4 = (uint64_t)var_22;
            if ((uint64_t)(unsigned char)var_21 != 0UL & var_22 == '\x00') {
                *(unsigned char *)((*(uint64_t *)rsi + rcx) + 17UL) = (unsigned char)rax_0;
                return;
            }
        }
        cc_src_5 = cc_src_4;
        cc_dst_5 = cc_dst_4;
        cc_src_6 = cc_src_5;
        cc_dst_6 = cc_dst_5;
        while (rcx1_2 != 0UL)
            {
                var_23 = (uint64_t)*(unsigned char *)rdi3_2;
                var_24 = (uint64_t)*(unsigned char *)rsi4_2 - var_23;
                cc_src_5 = var_23;
                cc_dst_5 = var_24;
                cc_src_6 = var_23;
                cc_dst_6 = var_24;
                if ((uint64_t)(unsigned char)var_24 == 0UL) {
                    break;
                }
                rcx1_2 = rcx1_2 + (-1L);
                rdi3_2 = rdi3_2 + var_6;
                rsi4_2 = rsi4_2 + var_6;
                cc_src_6 = cc_src_5;
                cc_dst_6 = cc_dst_5;
            }
        var_25 = helper_cc_compute_all_wrapper(cc_dst_6, cc_src_6, var_17, 14U);
        var_26 = ((var_25 & 65UL) == 0UL);
        var_27 = helper_cc_compute_c_wrapper(cc_dst_6, var_25, var_17, 1U);
        if ((uint64_t)((unsigned char)var_26 - (unsigned char)var_27) != 0UL) {
            rax_0 = 127UL;
            if (var_18 == '^') {
                *(uint64_t *)(var_0 + (-32L)) = 4208768UL;
                var_33 = indirect_placeholder_63(rdx, 255UL);
                var_34 = var_33.field_0;
                rax_0 = var_34;
            } else {
                if (var_19 == '?') {
                    var_28 = (uint64_t)(uint32_t)(uint64_t)var_19;
                    *(uint64_t *)(var_0 + (-32L)) = 4208884UL;
                    var_29 = indirect_placeholder(127UL, var_28);
                    var_30 = (uint64_t)((uint32_t)var_29 & (-97));
                    rax_0 = var_30;
                }
            }
        }
        *(unsigned char *)((*(uint64_t *)rsi + rcx) + 17UL) = (unsigned char)rax_0;
        return;
    }
}
