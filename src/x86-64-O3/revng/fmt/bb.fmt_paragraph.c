typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_23_ret_type;
struct indirect_placeholder_24_ret_type;
struct indirect_placeholder_25_ret_type;
struct indirect_placeholder_26_ret_type;
struct indirect_placeholder_27_ret_type;
struct indirect_placeholder_23_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_24_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_25_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_26_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_27_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct indirect_placeholder_23_ret_type indirect_placeholder_23(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_24_ret_type indirect_placeholder_24(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_25_ret_type indirect_placeholder_25(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_26_ret_type indirect_placeholder_26(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_27_ret_type indirect_placeholder_27(uint64_t param_0, uint64_t param_1);
void bb_fmt_paragraph(void) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint32_t var_11;
    uint64_t var_12;
    uint32_t *var_13;
    uint32_t var_14;
    uint64_t var_15;
    uint64_t local_sp_4;
    uint64_t r9_3;
    uint64_t r15_2;
    uint64_t r10_0;
    uint64_t r9_0_ph82;
    uint64_t r10_5;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t rbp_1;
    uint64_t r9_4;
    uint64_t r15_3;
    uint64_t r10_1;
    uint64_t rbp_0;
    uint64_t r9_0_ph;
    uint64_t r10_6;
    uint64_t local_sp_5;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t r10_2;
    uint64_t r9_0;
    uint64_t local_sp_0;
    uint64_t rsi_0;
    uint64_t r9_2;
    struct indirect_placeholder_23_ret_type var_55;
    uint64_t _pre_phi;
    uint64_t var_40;
    uint64_t r8_0_in;
    uint64_t rcx_0;
    uint64_t r10_3;
    uint64_t r9_1;
    uint64_t r10_4;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t r15_0;
    uint64_t local_sp_1;
    struct indirect_placeholder_24_ret_type var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t *var_20;
    uint32_t *var_21;
    uint64_t local_sp_2;
    uint32_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    struct indirect_placeholder_25_ret_type var_26;
    uint64_t var_27;
    uint64_t local_sp_3;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_41;
    struct indirect_placeholder_26_ret_type var_42;
    uint64_t var_43;
    uint64_t var_46;
    uint64_t var_47;
    struct indirect_placeholder_27_ret_type var_48;
    uint64_t var_49;
    uint32_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r15();
    var_3 = init_r14();
    var_4 = init_r12();
    var_5 = init_rbx();
    var_6 = init_r13();
    var_7 = init_cc_src2();
    var_8 = *(uint64_t *)4285312UL;
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_6;
    var_9 = var_8 + (-40L);
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_1;
    var_10 = var_0 + (-48L);
    *(uint64_t *)var_10 = var_5;
    var_11 = *(uint32_t *)4330400UL;
    var_12 = (uint64_t)var_11;
    var_13 = (uint32_t *)(var_8 + 8UL);
    var_14 = *var_13;
    *(uint64_t *)(var_8 + 24UL) = 0UL;
    *var_13 = var_11;
    var_15 = helper_cc_compute_c_wrapper(var_8 + (-4285384L), 4285344UL, var_7, 17U);
    r15_2 = 4285344UL;
    r10_5 = 9223372036854775807UL;
    r15_3 = 4285344UL;
    r10_6 = 9223372036854775807UL;
    r10_4 = 9223372036854775807UL;
    r15_0 = var_9;
    local_sp_1 = var_10;
    local_sp_3 = var_10;
    if (var_15 == 0UL) {
        *var_13 = var_14;
        return;
    }
    var_16 = (uint64_t)*(uint32_t *)4285292UL;
    var_17 = (uint64_t)*(uint32_t *)4285280UL;
    var_18 = (uint64_t)*(uint32_t *)4285296UL;
    rbp_0 = var_17;
    r9_1 = var_18;
    rbp_1 = var_17;
    r9_2 = var_18;
    if (var_9 != 4285344UL) {
        var_19 = var_12 << 32UL;
        _pre_phi = var_19;
        while (1U)
            {
                var_20 = (uint64_t *)(r15_0 + 32UL);
                var_21 = (uint32_t *)(r15_0 + 20UL);
                rcx_0 = r15_0;
                r8_0_in = var_16 + (uint64_t)*(uint32_t *)(r15_0 + 8UL);
                local_sp_2 = local_sp_1;
                while (1U)
                    {
                        var_22 = (uint32_t)r8_0_in;
                        var_23 = (uint64_t)var_22;
                        var_24 = rcx_0 + 40UL;
                        var_25 = local_sp_2 + (-8L);
                        *(uint64_t *)var_25 = 4205629UL;
                        var_26 = indirect_placeholder_25(var_24, var_23);
                        var_27 = var_26.field_0 + *(uint64_t *)(rcx_0 + 64UL);
                        r10_3 = r10_4;
                        rcx_0 = var_24;
                        local_sp_2 = var_25;
                        if ((long)var_27 < (long)r10_4) {
                            *var_20 = var_24;
                            *var_21 = var_22;
                            r10_3 = var_27;
                        }
                        r10_4 = r10_3;
                        if (var_8 == var_24) {
                            break;
                        }
                        var_28 = var_23 + ((uint64_t)*(uint32_t *)(rcx_0 + 48UL) + (uint64_t)*(uint32_t *)(rcx_0 + 12UL));
                        r8_0_in = var_28;
                        if ((long)var_19 <= (long)(var_28 << 32UL)) {
                            continue;
                        }
                        break;
                    }
                var_29 = var_26.field_1;
                var_30 = r15_0 + (-40L);
                var_31 = local_sp_2 + (-16L);
                *(uint64_t *)var_31 = 4205666UL;
                var_32 = indirect_placeholder_24(rbp_0, r10_3, var_12, r9_1, r15_0, var_29);
                var_33 = var_32.field_0;
                var_34 = var_32.field_1;
                var_35 = var_32.field_2;
                var_36 = var_32.field_3;
                *(uint64_t *)(r15_0 + 24UL) = (var_35 + var_33);
                var_37 = helper_cc_compute_c_wrapper(r15_0 + (-4285384L), 4285344UL, var_7, 17U);
                rbp_1 = var_34;
                rbp_0 = var_34;
                r9_2 = var_36;
                r9_1 = var_36;
                r15_0 = var_30;
                local_sp_1 = var_31;
                local_sp_3 = var_31;
                if (var_37 != 0UL) {
                    loop_state_var = 1U;
                    break;
                }
                if (var_30 == 4285344UL) {
                    continue;
                }
                loop_state_var = 0U;
                break;
            }
        switch (loop_state_var) {
          case 1U:
            {
                *var_13 = var_14;
                return;
            }
            break;
          case 0U:
            {
                break;
            }
            break;
        }
    }
    _pre_phi = var_12 << 32UL;
}
