typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_rax(void);
extern void indirect_placeholder_2(uint64_t param_0);
uint64_t bb_copy_rest(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint32_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint32_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t merge;
    uint64_t local_sp_0;
    uint64_t var_37;
    uint64_t local_sp_1;
    uint64_t rax_0;
    uint64_t rax_0_ph;
    uint64_t rdx_0_ph;
    uint64_t local_sp_1_ph;
    uint64_t rdx_0;
    uint64_t r13_0;
    uint64_t rax_1;
    uint64_t rdx_1;
    uint64_t local_sp_2;
    uint32_t _pre_phi;
    uint32_t var_30;
    uint32_t var_31;
    uint64_t var_32;
    uint64_t rdi1_0;
    uint64_t local_sp_3;
    uint64_t var_33;
    uint64_t var_34;
    bool var_35;
    bool var_36;
    uint64_t var_24;
    uint32_t var_25;
    uint32_t var_26;
    uint64_t var_27;
    uint32_t var_28;
    uint64_t var_29;
    uint64_t var_14;
    uint32_t var_15;
    uint64_t var_16;
    uint32_t var_17;
    uint64_t var_18;
    uint32_t _pre;
    uint64_t var_19;
    uint32_t var_20;
    uint64_t var_21;
    uint32_t var_22;
    uint64_t var_23;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbp();
    var_3 = init_r12();
    var_4 = init_rbx();
    var_5 = init_r13();
    *(uint64_t *)(var_0 + (-8L)) = var_5;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    var_6 = (uint32_t)rsi;
    var_7 = (uint64_t)var_6;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    var_8 = (uint64_t)(var_6 + (-10));
    var_9 = *(uint32_t *)4285284UL;
    var_10 = (uint64_t)var_9;
    *(uint32_t *)4330376UL = 0U;
    var_11 = (var_3 & 4294967040UL) | (var_8 != 0UL);
    var_12 = (uint64_t)(var_6 + 1U);
    var_13 = var_11 & ((var_1 & 4294967040UL) | (var_12 != 0UL));
    merge = var_7;
    rdi1_0 = 0UL;
    if ((int)var_9 < (int)*(uint32_t *)4330380UL) {
        var_19 = var_0 + (-48L);
        *(uint64_t *)var_19 = 4206773UL;
        indirect_placeholder_2(var_10);
        var_20 = *(uint32_t *)4330376UL;
        var_21 = (uint64_t)var_20;
        var_22 = *(uint32_t *)4330380UL;
        var_23 = (uint64_t)var_22;
        rax_0_ph = var_21;
        rdx_0_ph = var_23;
        local_sp_1_ph = var_19;
        rax_1 = var_21;
        rdx_1 = var_23;
        local_sp_2 = var_19;
        if ((uint64_t)(var_20 - var_22) != 0UL) {
            rax_0 = rax_0_ph;
            rdx_0 = rdx_0_ph;
            r13_0 = *(uint64_t *)4330408UL;
            local_sp_1 = local_sp_1_ph;
            rax_1 = rax_0;
            rdx_1 = rdx_0;
            local_sp_2 = local_sp_1;
            while (*(unsigned char *)r13_0 != '\x00')
                {
                    var_24 = local_sp_1 + (-8L);
                    *(uint64_t *)var_24 = 4206629UL;
                    indirect_placeholder();
                    var_25 = *(uint32_t *)4330376UL;
                    var_26 = *(uint32_t *)4330380UL;
                    var_27 = (uint64_t)var_26;
                    var_28 = var_25 + 1U;
                    var_29 = (uint64_t)var_28;
                    *(uint32_t *)4330376UL = var_28;
                    local_sp_1 = var_24;
                    rax_0 = var_29;
                    rdx_0 = var_27;
                    rax_1 = var_29;
                    rdx_1 = var_27;
                    local_sp_2 = var_24;
                    if ((uint64_t)(var_28 - var_26) == 0UL) {
                        break;
                    }
                    r13_0 = r13_0 + 1UL;
                    rax_1 = rax_0;
                    rdx_1 = rdx_0;
                    local_sp_2 = local_sp_1;
                }
        }
    }
    if ((var_13 & 1UL) == 0UL) {
        return merge;
    }
    var_14 = var_0 + (-48L);
    *(uint64_t *)var_14 = 4206597UL;
    indirect_placeholder_2(var_10);
    var_15 = *(uint32_t *)4330376UL;
    var_16 = (uint64_t)var_15;
    var_17 = *(uint32_t *)4330380UL;
    var_18 = (uint64_t)var_17;
    rax_0_ph = var_16;
    rdx_0_ph = var_18;
    local_sp_1_ph = var_14;
    local_sp_3 = var_14;
    if ((uint64_t)(var_15 - var_17) == 0UL) {
        _pre = (uint32_t)var_16;
        _pre_phi = _pre;
        var_33 = local_sp_3 + (-8L);
        *(uint64_t *)var_33 = 4206721UL;
        indirect_placeholder_2(rdi1_0);
        var_34 = (uint64_t)_pre_phi;
        var_35 = ((uint64_t)(_pre_phi + (-10)) == 0UL);
        var_36 = ((uint64_t)(_pre_phi + 1U) == 0UL);
        merge = var_34;
        local_sp_0 = var_33;
        *(uint64_t *)(local_sp_0 + (-8L)) = 4206735UL;
        indirect_placeholder();
        var_37 = local_sp_0 + (-16L);
        *(uint64_t *)var_37 = 4206743UL;
        indirect_placeholder();
        local_sp_0 = var_37;
        do {
            *(uint64_t *)(local_sp_0 + (-8L)) = 4206735UL;
            indirect_placeholder();
            var_37 = local_sp_0 + (-16L);
            *(uint64_t *)var_37 = 4206743UL;
            indirect_placeholder();
            local_sp_0 = var_37;
        } while (!(var_35 || var_36));
        return merge;
    }
    rax_0 = rax_0_ph;
    rdx_0 = rdx_0_ph;
    r13_0 = *(uint64_t *)4330408UL;
    local_sp_1 = local_sp_1_ph;
    rax_1 = rax_0;
    rdx_1 = rdx_0;
    local_sp_2 = local_sp_1;
    while (*(unsigned char *)r13_0 != '\x00')
        {
            var_24 = local_sp_1 + (-8L);
            *(uint64_t *)var_24 = 4206629UL;
            indirect_placeholder();
            var_25 = *(uint32_t *)4330376UL;
            var_26 = *(uint32_t *)4330380UL;
            var_27 = (uint64_t)var_26;
            var_28 = var_25 + 1U;
            var_29 = (uint64_t)var_28;
            *(uint32_t *)4330376UL = var_28;
            local_sp_1 = var_24;
            rax_0 = var_29;
            rdx_0 = var_27;
            rax_1 = var_29;
            rdx_1 = var_27;
            local_sp_2 = var_24;
            if ((uint64_t)(var_28 - var_26) == 0UL) {
                break;
            }
            r13_0 = r13_0 + 1UL;
            rax_1 = rax_0;
            rdx_1 = rdx_0;
            local_sp_2 = local_sp_1;
        }
    local_sp_3 = local_sp_2;
    if ((var_13 & 1UL) != 0UL) {
        if (var_12 != 0UL & (long)(((uint64_t)*(uint32_t *)4330388UL + (uint64_t)*(uint32_t *)4285284UL) << 32UL) > (long)(rdx_1 << 32UL)) {
            *(uint64_t *)(local_sp_2 + (-8L)) = 4206815UL;
            indirect_placeholder();
        }
        return merge;
    }
    var_30 = (uint32_t)rdx_1;
    var_31 = (uint32_t)rax_1;
    var_32 = (uint64_t)(var_30 - var_31);
    _pre_phi = var_31;
    rdi1_0 = var_32;
    var_33 = local_sp_3 + (-8L);
    *(uint64_t *)var_33 = 4206721UL;
    indirect_placeholder_2(rdi1_0);
    var_34 = (uint64_t)_pre_phi;
    var_35 = ((uint64_t)(_pre_phi + (-10)) == 0UL);
    var_36 = ((uint64_t)(_pre_phi + 1U) == 0UL);
    merge = var_34;
    local_sp_0 = var_33;
    *(uint64_t *)(local_sp_0 + (-8L)) = 4206735UL;
    indirect_placeholder();
    var_37 = local_sp_0 + (-16L);
    *(uint64_t *)var_37 = 4206743UL;
    indirect_placeholder();
    local_sp_0 = var_37;
    do {
        *(uint64_t *)(local_sp_0 + (-8L)) = 4206735UL;
        indirect_placeholder();
        var_37 = local_sp_0 + (-16L);
        *(uint64_t *)var_37 = 4206743UL;
        indirect_placeholder();
        local_sp_0 = var_37;
    } while (!(var_35 || var_36));
    return merge;
}
