typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct helper_divq_EAX_wrapper_ret_type;
struct type_5;
struct indirect_placeholder_74_ret_type;
struct indirect_placeholder_75_ret_type;
struct indirect_placeholder_76_ret_type;
struct helper_divq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint32_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint32_t field_5;
    uint32_t field_6;
    uint64_t field_7;
    uint32_t field_8;
    uint64_t field_9;
    uint32_t field_10;
    unsigned char field_11;
    uint32_t field_12;
};
struct type_5 {
};
struct indirect_placeholder_74_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_75_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_76_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint32_t init_state_0x8248(void);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern struct helper_divq_EAX_wrapper_ret_type helper_divq_EAX_wrapper(struct type_5 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint32_t param_13, uint64_t param_14, uint32_t param_15, uint64_t param_16, uint64_t param_17, uint64_t param_18, uint32_t param_19);
extern void indirect_placeholder_4(uint64_t param_0, uint64_t param_1);
extern uint64_t init_r10(void);
extern struct indirect_placeholder_74_ret_type indirect_placeholder_74(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_75_ret_type indirect_placeholder_75(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_76_ret_type indirect_placeholder_76(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_init_column_info(uint64_t r9, uint64_t r8) {
    struct indirect_placeholder_76_ret_type var_21;
    struct indirect_placeholder_75_ret_type var_18;
    struct indirect_placeholder_74_ret_type var_31;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint32_t var_5;
    uint64_t var_6;
    uint32_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint32_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t r91_0;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t rdx_0;
    uint64_t rax_0_in;
    uint64_t rax_0;
    uint64_t rbp_0;
    uint64_t r82_0;
    uint64_t local_sp_0;
    uint64_t storemerge;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    struct helper_divq_EAX_wrapper_ret_type var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_32;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t rdi_0;
    uint64_t rsi_0;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t rax_2;
    uint64_t var_40;
    uint64_t rax_1;
    uint64_t var_41;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r10();
    var_3 = init_rbx();
    var_4 = init_cc_src2();
    var_5 = init_state_0x8248();
    var_6 = init_state_0x9018();
    var_7 = init_state_0x9010();
    var_8 = init_state_0x8408();
    var_9 = init_state_0x8328();
    var_10 = init_state_0x82d8();
    var_11 = init_state_0x9080();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    var_12 = *(uint64_t *)4408832UL;
    var_13 = *(uint64_t *)4409264UL;
    var_14 = (var_13 > var_12) ? var_12 : var_13;
    var_15 = helper_cc_compute_c_wrapper(*(uint64_t *)4404960UL - var_14, var_14, var_4, 17U);
    rdi_0 = 3UL;
    rsi_0 = 0UL;
    rax_2 = 0UL;
    if (var_15 != 0UL) {
        var_16 = *(uint64_t *)4408840UL;
        if ((var_12 >> 1UL) > var_14) {
            var_19 = var_14 << 1UL;
            var_20 = var_0 + (-32L);
            *(uint64_t *)var_20 = 4212418UL;
            var_21 = indirect_placeholder_76(48UL, r9, var_16, var_14, r8);
            r91_0 = var_21.field_1;
            rbp_0 = var_19;
            r82_0 = var_21.field_2;
            local_sp_0 = var_20;
            storemerge = var_21.field_0;
        } else {
            var_17 = var_0 + (-32L);
            *(uint64_t *)var_17 = 4212108UL;
            var_18 = indirect_placeholder_75(24UL, r9, var_16, var_12, r8);
            r91_0 = var_18.field_1;
            rbp_0 = *(uint64_t *)4408832UL;
            r82_0 = var_18.field_2;
            local_sp_0 = var_17;
            storemerge = var_18.field_0;
        }
        *(uint64_t *)4408840UL = storemerge;
        var_22 = *(uint64_t *)4404960UL;
        var_23 = rbp_0 - var_22;
        var_24 = (var_22 + 1UL) + rbp_0;
        var_25 = helper_cc_compute_c_wrapper(var_24, rbp_0, var_4, 9U);
        var_26 = (uint64_t)(unsigned char)var_25;
        var_27 = var_24 * var_23;
        if (var_26 == 0UL) {
            *(uint64_t *)(local_sp_0 + (-8L)) = 4212435UL;
            indirect_placeholder_4(r91_0, r82_0);
            abort();
        }
        var_28 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), var_23, 4212168UL, var_27, var_24, rbp_0, var_2, var_14, 0UL, r91_0, var_27, var_23, r82_0, var_5, var_6, var_7, var_8, var_9, var_10, var_11);
        if (var_28.field_1 == var_24) {
            *(uint64_t *)(local_sp_0 + (-8L)) = 4212435UL;
            indirect_placeholder_4(r91_0, r82_0);
            abort();
        }
        var_29 = var_28.field_4;
        var_30 = var_27 >> 1UL;
        *(uint64_t *)(local_sp_0 + (-8L)) = 4212193UL;
        var_31 = indirect_placeholder_74(var_29, r91_0, var_30, 8UL, r82_0);
        var_32 = *(uint64_t *)4404960UL;
        if (rbp_0 <= var_32) {
            var_33 = *(uint64_t *)4408840UL;
            var_34 = rbp_0 << 3UL;
            rdx_0 = var_31.field_0;
            rax_0_in = var_32 << 3UL;
            rax_0 = rax_0_in + 8UL;
            *(uint64_t *)(((rax_0 * 3UL) + var_33) + (-8L)) = rdx_0;
            rax_0_in = rax_0;
            while (rax_0 != var_34)
                {
                    rdx_0 = rdx_0 + rax_0;
                    rax_0 = rax_0_in + 8UL;
                    *(uint64_t *)(((rax_0 * 3UL) + var_33) + (-8L)) = rdx_0;
                    rax_0_in = rax_0;
                }
        }
        *(uint64_t *)4404960UL = rbp_0;
    }
    var_35 = *(uint64_t *)4357488UL;
    var_36 = *(uint64_t *)4357496UL;
    if (var_14 == 0UL) {
        return;
    }
    while (1U)
        {
            var_37 = (rdi_0 << 3UL) + *(uint64_t *)4408840UL;
            *(unsigned char *)(var_37 + (-24L)) = (unsigned char)'\x01';
            var_38 = *(uint64_t *)(var_37 + (-8L));
            var_39 = rsi_0 + 1UL;
            *(uint64_t *)(var_37 + (-16L)) = rdi_0;
            rsi_0 = var_39;
            rax_1 = var_38;
            if (rsi_0 != 0UL) {
                var_40 = ((var_39 >> 1UL) << 4UL) + var_38;
                *(uint64_t *)rax_1 = var_35;
                *(uint64_t *)(rax_1 + 8UL) = var_36;
                var_41 = rax_1 + 16UL;
                rax_1 = var_41;
                do {
                    *(uint64_t *)rax_1 = var_35;
                    *(uint64_t *)(rax_1 + 8UL) = var_36;
                    var_41 = rax_1 + 16UL;
                    rax_1 = var_41;
                } while (var_40 != var_41);
                rax_2 = var_39 & (-2L);
                if ((var_39 & 1UL) != 0UL) {
                    if (var_39 == var_14) {
                        break;
                    }
                    rdi_0 = rdi_0 + 3UL;
                    continue;
                }
            }
            *(uint64_t *)((rax_2 << 3UL) + var_38) = 3UL;
            if (var_39 == var_14) {
                break;
            }
            rdi_0 = rdi_0 + 3UL;
            continue;
        }
    return;
}
