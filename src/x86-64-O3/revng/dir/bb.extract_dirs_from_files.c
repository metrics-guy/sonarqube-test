typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r13(void);
extern void indirect_placeholder_1(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder_5(uint64_t param_0);
extern uint64_t indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_17(uint64_t param_0);
void bb_extract_dirs_from_files(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t rbx_1_in;
    uint64_t var_33;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t local_sp_5;
    uint64_t local_sp_0;
    uint64_t rax_0;
    uint64_t local_sp_2;
    uint64_t local_sp_1;
    uint64_t local_sp_4_be;
    uint64_t local_sp_4;
    uint64_t rbx_0_in;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t rdx_0;
    uint64_t var_37;
    uint32_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_23;
    uint64_t local_sp_3;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t rbx_0;
    uint64_t var_10;
    uint32_t *var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_24;
    uint64_t rbx_1;
    uint64_t var_25;
    uint32_t *var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r14();
    var_3 = init_r12();
    var_4 = init_rbx();
    var_5 = init_r13();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    var_6 = (uint64_t)(unsigned char)rsi;
    *(uint64_t *)(var_0 + (-32L)) = var_1;
    var_7 = var_0 + (-40L);
    *(uint64_t *)var_7 = var_4;
    rdx_0 = 0UL;
    local_sp_3 = var_7;
    local_sp_5 = var_7;
    if (rdi != 0UL) {
        if (*(uint64_t *)4409288UL == 0UL) {
            var_8 = var_0 + (-48L);
            *(uint64_t *)var_8 = 4220750UL;
            indirect_placeholder_1(0UL, 0UL, rdi);
            local_sp_3 = var_8;
        }
        var_9 = *(uint64_t *)4409264UL;
        local_sp_4 = local_sp_3;
        rbx_0_in = var_9;
        if (var_9 != 0UL) {
            *(uint64_t *)4409264UL = 0UL;
            return;
        }
        while (1U)
            {
                rbx_0 = rbx_0_in + (-1L);
                var_10 = *(uint64_t *)((rbx_0 << 3UL) + *(uint64_t *)4409248UL);
                var_11 = (uint32_t *)(var_10 + 168UL);
                var_12 = (uint64_t)*var_11;
                var_13 = local_sp_4 + (-8L);
                *(uint64_t *)var_13 = 4220864UL;
                var_14 = indirect_placeholder_5(var_12);
                local_sp_1 = var_13;
                rbx_0_in = rbx_0;
                if ((uint64_t)(unsigned char)var_14 == 0UL) {
                    local_sp_4_be = local_sp_1;
                    if (rbx_0 == 0UL) {
                        break;
                    }
                    local_sp_4 = local_sp_4_be;
                    continue;
                }
                var_15 = *(uint64_t *)var_10;
                var_16 = local_sp_4 + (-16L);
                *(uint64_t *)var_16 = 4220880UL;
                var_17 = indirect_placeholder_5(var_15);
                local_sp_1 = var_16;
                if ((uint64_t)(unsigned char)var_17 != 0UL) {
                    if (*(unsigned char *)var_15 == '/') {
                        var_21 = *(uint64_t *)(var_10 + 8UL);
                        var_22 = local_sp_4 + (-24L);
                        *(uint64_t *)var_22 = 4220905UL;
                        indirect_placeholder_1(var_6, var_15, var_21);
                        local_sp_1 = var_22;
                        local_sp_2 = var_22;
                        if (*var_11 != 9U) {
                            local_sp_4_be = local_sp_1;
                            if (rbx_0 == 0UL) {
                                break;
                            }
                            local_sp_4 = local_sp_4_be;
                            continue;
                        }
                    }
                    *(uint64_t *)(local_sp_4 + (-24L)) = 4220789UL;
                    var_18 = indirect_placeholder_8(0UL, rdi, var_15);
                    var_19 = *(uint64_t *)(var_10 + 8UL);
                    *(uint64_t *)(local_sp_4 + (-32L)) = 4220807UL;
                    indirect_placeholder_1(var_6, var_18, var_19);
                    var_20 = local_sp_4 + (-40L);
                    *(uint64_t *)var_20 = 4220815UL;
                    indirect_placeholder();
                    local_sp_1 = var_20;
                    local_sp_2 = var_20;
                    if (*var_11 != 9U) {
                        local_sp_4_be = local_sp_1;
                        if (rbx_0 == 0UL) {
                            break;
                        }
                        local_sp_4 = local_sp_4_be;
                        continue;
                    }
                    var_23 = local_sp_2 + (-8L);
                    *(uint64_t *)var_23 = 4220926UL;
                    indirect_placeholder_17(var_10);
                    local_sp_4_be = var_23;
                    if (rbx_0 == 0UL) {
                        break;
                    }
                    local_sp_4 = local_sp_4_be;
                    continue;
                }
            }
    }
    var_24 = *(uint64_t *)4409264UL;
    rbx_1_in = var_24;
    if (var_24 != 0UL) {
        *(uint64_t *)4409264UL = 0UL;
        return;
    }
    local_sp_5 = local_sp_0;
    do {
        rbx_1 = rbx_1_in + (-1L);
        var_25 = *(uint64_t *)((rbx_1 << 3UL) + *(uint64_t *)4409248UL);
        var_26 = (uint32_t *)(var_25 + 168UL);
        var_27 = (uint64_t)*var_26;
        var_28 = local_sp_5 + (-8L);
        *(uint64_t *)var_28 = 4221062UL;
        var_29 = indirect_placeholder_5(var_27);
        local_sp_0 = var_28;
        rbx_1_in = rbx_1;
        var_30 = *(uint64_t *)(var_25 + 8UL);
        var_31 = *(uint64_t *)var_25;
        var_32 = local_sp_5 + (-16L);
        *(uint64_t *)var_32 = 4221082UL;
        indirect_placeholder_1(var_6, var_31, var_30);
        local_sp_0 = var_32;
        if ((uint64_t)(unsigned char)var_29 != 0UL & *var_26 == 9U) {
            var_33 = local_sp_5 + (-24L);
            *(uint64_t *)var_33 = 4221120UL;
            indirect_placeholder_17(var_25);
            local_sp_0 = var_33;
        }
        local_sp_5 = local_sp_0;
    } while (rbx_1 != 0UL);
    var_34 = *(uint64_t *)4409264UL;
    if (var_34 == 0UL) {
        *(uint64_t *)4409264UL = 0UL;
        return;
    }
    var_35 = *(uint64_t *)4409248UL;
    var_36 = (var_34 << 3UL) + var_35;
    rax_0 = var_35;
    var_37 = *(uint64_t *)rax_0;
    var_38 = *(uint32_t *)(var_37 + 168UL);
    *(uint64_t *)((rdx_0 << 3UL) + var_35) = var_37;
    var_39 = (var_38 != 9U);
    var_40 = rax_0 + 8UL;
    var_41 = rdx_0 + var_39;
    rax_0 = var_40;
    rdx_0 = var_41;
    do {
        var_37 = *(uint64_t *)rax_0;
        var_38 = *(uint32_t *)(var_37 + 168UL);
        *(uint64_t *)((rdx_0 << 3UL) + var_35) = var_37;
        var_39 = (var_38 != 9U);
        var_40 = rax_0 + 8UL;
        var_41 = rdx_0 + var_39;
        rax_0 = var_40;
        rdx_0 = var_41;
    } while (var_40 != var_36);
    *(uint64_t *)4409264UL = var_41;
    return;
}
