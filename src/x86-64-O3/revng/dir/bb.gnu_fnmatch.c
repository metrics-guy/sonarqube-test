typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_rax(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_rbp(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r13(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0);
extern uint64_t init_r15(void);
extern uint64_t indirect_placeholder_26(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
uint64_t bb_gnu_fnmatch(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_55;
    uint64_t var_48;
    uint64_t var_35;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t *var_11;
    uint64_t var_12;
    uint64_t rax_2;
    uint64_t local_sp_0;
    uint64_t var_47;
    uint32_t *var_49;
    uint64_t var_50;
    uint64_t rax_1;
    uint64_t var_46;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    bool var_43;
    uint64_t var_44;
    uint64_t *var_45;
    uint64_t local_sp_1;
    uint64_t var_34;
    uint64_t var_33;
    uint64_t local_sp_2;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t rax_0;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    bool var_31;
    uint64_t *var_32;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t local_sp_3;
    uint64_t var_20;
    uint64_t var_19;
    uint64_t local_sp_4;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_16;
    uint64_t var_15;
    uint64_t local_sp_5;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_13;
    uint64_t var_14;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbp();
    var_3 = init_r15();
    var_4 = init_r14();
    var_5 = init_r12();
    var_6 = init_rbx();
    var_7 = init_cc_src2();
    var_8 = init_r13();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_8;
    *(uint64_t *)(var_0 + (-40L)) = var_5;
    *(uint64_t *)(var_0 + (-48L)) = var_6;
    var_9 = (uint64_t)(uint32_t)rdx;
    var_10 = (uint64_t *)(var_0 + (-80L));
    *var_10 = rdi;
    var_11 = (uint64_t *)(var_0 + (-88L));
    *var_11 = rsi;
    var_12 = var_0 + (-112L);
    *(uint64_t *)var_12 = 4287968UL;
    indirect_placeholder();
    rax_1 = 4294967295UL;
    rax_2 = 1UL;
    local_sp_5 = var_12;
    if (var_1 == 1UL) {
        var_51 = *var_11;
        *(uint64_t *)(local_sp_5 + (-8L)) = 4287986UL;
        indirect_placeholder();
        var_52 = *var_10;
        var_53 = rax_2 + var_51;
        var_54 = (rdx >> 2UL) & 1UL;
        *(uint64_t *)(local_sp_5 + (-16L)) = 4288013UL;
        var_55 = indirect_placeholder_26(var_54, var_53, var_52, var_51, var_9);
        rax_1 = var_55;
        return rax_1;
    }
    *(uint64_t *)(var_0 + (-64L)) = 0UL;
    var_13 = var_0 + (-120L);
    *(uint64_t *)var_13 = 4288057UL;
    indirect_placeholder();
    var_14 = var_1 + 1UL;
    rax_2 = 0UL;
    local_sp_5 = var_13;
    if (var_14 == 0UL) {
        return;
    }
    var_15 = var_0 + (-128L);
    *(uint64_t *)var_15 = 4288075UL;
    indirect_placeholder();
    local_sp_4 = var_15;
    if ((uint64_t)(uint32_t)var_14 == 0UL) {
        var_16 = var_0 + (-136L);
        *(uint64_t *)var_16 = 4288377UL;
        indirect_placeholder();
        local_sp_4 = var_16;
    }
    var_17 = local_sp_4 + (-8L);
    *(uint64_t *)var_17 = 4288100UL;
    indirect_placeholder();
    var_18 = var_1 + 2UL;
    local_sp_5 = var_17;
    if (var_18 != 0UL) {
        var_19 = local_sp_4 + (-16L);
        *(uint64_t *)var_19 = 4288122UL;
        indirect_placeholder();
        local_sp_2 = var_19;
        if ((uint64_t)(uint32_t)var_18 == 0UL) {
            var_20 = local_sp_4 + (-24L);
            *(uint64_t *)var_20 = 4288409UL;
            indirect_placeholder();
            local_sp_2 = var_20;
        }
        var_21 = var_14 + var_18;
        var_22 = helper_cc_compute_c_wrapper(var_21, var_18, var_7, 9U);
        rax_0 = var_21;
        local_sp_3 = local_sp_2;
        if ((var_22 != 0UL) || (var_21 > 4611686018427387903UL)) {
            *(uint64_t *)(local_sp_3 + (-8L)) = 4288419UL;
            indirect_placeholder();
            *(uint32_t *)rax_0 = 12U;
            return rax_1;
        }
        var_23 = var_21 << 2UL;
        rax_0 = 0UL;
        if (var_21 <= 1999UL) {
            var_24 = local_sp_2 - ((var_23 + 23UL) & (-16L));
            var_25 = (var_24 + 15UL) & (-16L);
            var_26 = (var_14 << 2UL) + var_25;
            *(uint64_t *)(var_24 + (-8L)) = 4288227UL;
            indirect_placeholder();
            var_27 = rdx >> 2UL;
            var_28 = var_24 + (-16L);
            *(uint64_t *)var_28 = 4288240UL;
            indirect_placeholder();
            var_29 = var_27 & 1UL;
            var_30 = ((var_18 << 2UL) + var_26) + (-4L);
            var_31 = ((uint64_t)(uint32_t)var_21 == 0UL);
            var_32 = (uint64_t *)(var_0 + (-96L));
            *var_32 = var_30;
            local_sp_1 = var_28;
            if (var_31) {
                var_33 = var_24 + (-24L);
                *(uint64_t *)var_33 = 4288341UL;
                indirect_placeholder();
                local_sp_1 = var_33;
            }
            *(uint64_t *)(local_sp_1 + (-8L)) = 4288276UL;
            indirect_placeholder();
            var_34 = *var_32;
            *(uint64_t *)(local_sp_1 + (-16L)) = 4288300UL;
            var_35 = indirect_placeholder_26(var_29, var_34, var_25, var_26, var_9);
            rax_1 = var_35;
            return rax_1;
        }
        var_36 = local_sp_2 + (-8L);
        *(uint64_t *)var_36 = 4288440UL;
        var_37 = indirect_placeholder_5(var_23);
        local_sp_3 = var_36;
        if (var_37 != 0UL) {
            var_38 = (var_14 << 2UL) + var_37;
            *(uint64_t *)(local_sp_2 + (-16L)) = 4288474UL;
            indirect_placeholder();
            var_39 = rdx >> 2UL;
            var_40 = local_sp_2 + (-24L);
            *(uint64_t *)var_40 = 4288487UL;
            indirect_placeholder();
            var_41 = var_39 & 1UL;
            var_42 = ((var_18 << 2UL) + var_38) + (-4L);
            var_43 = ((uint64_t)(uint32_t)var_37 == 0UL);
            var_44 = var_0 + (-96L);
            var_45 = (uint64_t *)var_44;
            *var_45 = var_42;
            local_sp_0 = var_40;
            if (var_43) {
                var_46 = local_sp_2 + (-32L);
                *(uint64_t *)var_46 = 4288595UL;
                indirect_placeholder();
                local_sp_0 = var_46;
            }
            *(uint64_t *)(local_sp_0 + (-8L)) = 4288523UL;
            indirect_placeholder();
            var_47 = *var_45;
            *(uint64_t *)(local_sp_0 + (-16L)) = 4288547UL;
            var_48 = indirect_placeholder_26(var_41, var_47, var_37, var_38, var_9);
            var_49 = (uint32_t *)var_44;
            *var_49 = (uint32_t)var_48;
            *(uint64_t *)(local_sp_0 + (-24L)) = 4288558UL;
            indirect_placeholder();
            var_50 = (uint64_t)*var_49;
            rax_1 = var_50;
            return rax_1;
        }
    }
}
