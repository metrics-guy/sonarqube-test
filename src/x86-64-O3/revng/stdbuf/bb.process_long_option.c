typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_5_ret_type;
struct indirect_placeholder_6_ret_type;
struct indirect_placeholder_5_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_6_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t init_r15(void);
extern struct indirect_placeholder_5_ret_type indirect_placeholder_5(uint64_t param_0);
extern struct indirect_placeholder_6_ret_type indirect_placeholder_6(uint64_t param_0);
uint64_t bb_process_long_option(uint64_t rdi, uint64_t r9, uint64_t rcx, uint64_t rdx, uint64_t r8, uint64_t rsi) {
    uint64_t local_sp_1;
    uint64_t local_sp_0;
    uint64_t rbx_4;
    uint64_t *var_14;
    bool var_15;
    bool var_16;
    uint64_t var_68;
    uint64_t rax_13_ph;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t var_10;
    unsigned char var_11;
    uint64_t rax_14;
    uint64_t rbx_3;
    uint64_t rax_11;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t *var_35;
    uint64_t var_36;
    uint64_t _pre;
    uint64_t var_37;
    uint64_t rbx_1;
    uint64_t rax_10;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t *var_52;
    uint64_t var_53;
    uint64_t _pre276;
    uint64_t var_54;
    uint64_t rbp_0;
    uint64_t var_12;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t r15_11_ph;
    uint64_t var_61;
    uint64_t r13_4;
    uint64_t local_sp_15;
    uint64_t r12_6;
    uint64_t var_27;
    uint64_t local_sp_2;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    struct indirect_placeholder_5_ret_type var_31;
    uint64_t var_32;
    uint64_t local_sp_3;
    uint64_t r15_9;
    uint64_t rax_0;
    uint64_t rbp_2;
    uint64_t r15_0;
    uint64_t r13_1;
    uint64_t var_38;
    uint64_t local_sp_9;
    uint64_t rbp_4;
    uint64_t var_65;
    uint64_t local_sp_4;
    uint64_t local_sp_17;
    uint64_t r13_6_ph;
    uint64_t r13_6;
    uint64_t var_66;
    uint64_t rax_1;
    uint64_t r15_1;
    uint64_t r14_0;
    uint64_t var_67;
    uint64_t local_sp_17_ph;
    uint64_t r13_5;
    uint64_t rax_12;
    uint64_t rbp_3;
    uint32_t var_69;
    uint64_t r15_10;
    uint64_t var_70;
    uint64_t r14_4;
    uint64_t local_sp_16;
    uint64_t rax_2;
    uint64_t r15_2;
    uint64_t r14_1;
    uint64_t var_71;
    uint64_t rax_3;
    uint64_t r15_3;
    uint64_t r14_2;
    uint64_t var_72;
    uint64_t local_sp_10;
    uint64_t local_sp_6;
    uint64_t local_sp_5;
    uint64_t var_62;
    uint64_t rax_5;
    uint32_t *var_73;
    uint64_t r13_3;
    uint64_t local_sp_14;
    uint64_t r12_5;
    uint32_t var_40;
    uint64_t rax_6;
    uint64_t var_41;
    uint32_t var_42;
    uint64_t var_43;
    uint64_t rbp_1;
    uint64_t var_44;
    uint64_t local_sp_7;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    struct indirect_placeholder_6_ret_type var_48;
    uint64_t var_49;
    uint64_t local_sp_8;
    uint64_t r15_8;
    uint64_t rax_7;
    uint64_t r15_4;
    uint64_t r12_1;
    uint64_t var_55;
    uint64_t r15_12;
    uint64_t rax_8_in;
    uint64_t r15_5;
    uint64_t storemerge;
    uint64_t var_56;
    uint64_t rbx_0_in_in;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t r15_6;
    uint64_t rbx_1_in_in;
    uint64_t local_sp_19;
    uint64_t var_74;
    uint64_t local_sp_11;
    uint64_t r14_6;
    uint64_t r15_7;
    uint64_t rbp_5;
    uint64_t rbx_2;
    uint64_t r14_3;
    uint32_t *var_75;
    uint32_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    bool var_79;
    uint32_t var_80;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t local_sp_12;
    uint64_t var_85;
    uint64_t var_84;
    uint64_t var_83;
    uint64_t merge;
    uint64_t var_86;
    uint64_t var_87;
    bool var_88;
    uint32_t var_89;
    uint64_t var_18;
    uint64_t local_sp_18;
    uint64_t local_sp_13;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    bool var_22;
    uint32_t var_23;
    uint64_t var_24;
    bool var_25;
    uint64_t var_39;
    uint64_t var_26;
    uint64_t rbp_4_ph;
    uint64_t r14_5_ph;
    bool var_63;
    uint64_t var_64;
    unsigned char var_13;
    uint64_t var_17;
    uint64_t local_sp_20;
    uint64_t var_90;
    uint64_t var_91;
    uint32_t *var_92;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r15();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r12();
    var_5 = init_rbp();
    var_6 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_5;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_7 = var_0 + (-136L);
    var_8 = *(uint64_t *)(var_0 | 8UL);
    *(uint64_t *)(var_0 + (-120L)) = rsi;
    *(uint32_t *)(var_0 + (-96L)) = (uint32_t)rdi;
    *(uint64_t *)(var_0 + (-112L)) = rdx;
    *(uint64_t *)(var_0 + (-128L)) = r8;
    *(uint32_t *)(var_0 + (-132L)) = (uint32_t)r9;
    var_9 = (uint64_t *)(var_8 + 32UL);
    var_10 = *var_9;
    var_11 = *(unsigned char *)var_10;
    rbx_4 = var_10;
    rax_13_ph = 0UL;
    rax_14 = (uint64_t)var_11;
    rbx_3 = var_10;
    rbp_0 = 0UL;
    r15_11_ph = 0UL;
    r13_4 = 0UL;
    r12_6 = rcx;
    r15_9 = 0UL;
    rbp_2 = 0UL;
    r13_6_ph = rcx;
    r13_5 = rcx;
    rax_12 = 0UL;
    rbp_3 = 0UL;
    r15_10 = 0UL;
    r14_4 = 0UL;
    rax_5 = 0UL;
    r13_3 = rcx;
    r12_5 = 0UL;
    rbp_1 = 0UL;
    r15_8 = 0UL;
    r15_12 = rcx;
    local_sp_19 = var_7;
    r14_6 = 0UL;
    rbp_5 = 0UL;
    merge = 4294967295UL;
    local_sp_18 = var_7;
    rbp_4_ph = 0UL;
    r14_5_ph = 0UL;
    if (!(((uint64_t)(var_11 + '\xc3') == 0UL) || (var_11 == '\x00'))) {
        while (1U)
            {
                switch_state_var = 1;
                break;
            }
        rax_14 = (uint64_t)var_13;
        rbp_5 = var_12 - var_10;
    }
    var_14 = (uint64_t *)rcx;
    rbx_2 = rbx_4;
    if (*var_14 != 0UL) {
        local_sp_20 = local_sp_19;
        if (*(uint32_t *)(local_sp_19 + 4UL) != 0U) {
            var_90 = *var_9;
            var_91 = local_sp_19 + (-8L);
            *(uint64_t *)var_91 = 4217799UL;
            indirect_placeholder();
            local_sp_20 = var_91;
            if (*(unsigned char *)(*(uint64_t *)(((uint64_t)*(uint32_t *)var_8 << 3UL) + *(uint64_t *)(local_sp_19 + 16UL)) + 1UL) != '-' & var_90 == 0UL) {
                return merge;
            }
        }
        if (*(uint32_t *)(local_sp_20 + 152UL) != 0U) {
            *(uint64_t *)(local_sp_20 + (-8L)) = 4217412UL;
            indirect_placeholder();
        }
        *var_9 = 0UL;
        var_92 = (uint32_t *)var_8;
        *var_92 = (*var_92 + 1U);
        *(uint32_t *)(var_8 + 8UL) = 0U;
        return 63UL;
    }
    var_15 = (rax_14 == 0UL);
    var_16 = (rbp_5 == rax_14);
    merge = 63UL;
    while (1U)
        {
            var_17 = local_sp_18 + (-8L);
            *(uint64_t *)var_17 = 4216545UL;
            indirect_placeholder();
            r15_7 = r15_12;
            r14_3 = r14_6;
            local_sp_13 = var_17;
            if (!var_15) {
                var_18 = local_sp_18 + (-16L);
                *(uint64_t *)var_18 = 4216557UL;
                indirect_placeholder();
                local_sp_11 = var_18;
                local_sp_13 = var_18;
                if (!var_16) {
                    loop_state_var = 1U;
                    break;
                }
            }
            var_19 = r15_12 + 32UL;
            var_20 = *(uint64_t *)var_19;
            var_21 = r14_6 + 1UL;
            local_sp_15 = local_sp_13;
            local_sp_17_ph = local_sp_13;
            local_sp_16 = local_sp_13;
            local_sp_14 = local_sp_13;
            r15_12 = var_19;
            local_sp_19 = local_sp_13;
            local_sp_18 = local_sp_13;
            if (var_20 == 0UL) {
                r14_6 = (uint64_t)(uint32_t)var_21;
                continue;
            }
            if (*var_14 != 0UL) {
                loop_state_var = 0U;
                break;
            }
            var_22 = (*(uint32_t *)(local_sp_13 + 152UL) == 0U);
            var_23 = *(uint32_t *)(local_sp_13 + 4UL);
            if (var_22) {
                *(uint64_t *)(local_sp_13 + 32UL) = rbx_4;
                *(uint32_t *)(local_sp_13 + 44UL) = 4294967295U;
                if (var_23 != 0U) {
                    while (1U)
                        {
                            var_68 = local_sp_16 + (-8L);
                            *(uint64_t *)var_68 = 4217092UL;
                            indirect_placeholder();
                            local_sp_4 = var_68;
                            local_sp_16 = var_68;
                            rax_2 = rax_12;
                            r15_2 = r15_10;
                            r14_1 = r14_4;
                            if ((uint64_t)(uint32_t)rax_12 != 0UL) {
                                rax_2 = 1UL;
                                r15_2 = r13_5;
                                if (r15_10 == 0UL) {
                                    *(uint32_t *)(local_sp_16 + 36UL) = (uint32_t)rbp_3;
                                } else {
                                    if ((uint64_t)(*(uint32_t *)(r15_10 + 8UL) - *(uint32_t *)(r13_5 + 8UL)) == 0UL) {
                                        var_70 = (uint64_t)(uint32_t)r14_4;
                                        r14_1 = (var_70 == 0UL) ? 1UL : var_70;
                                    } else {
                                        if (*(uint64_t *)(r15_10 + 16UL) == *(uint64_t *)(r13_5 + 16UL)) {
                                            var_70 = (uint64_t)(uint32_t)r14_4;
                                            r14_1 = (var_70 == 0UL) ? 1UL : var_70;
                                        } else {
                                            var_69 = *(uint32_t *)(r13_5 + 24UL);
                                            rax_2 = (uint64_t)var_69;
                                            if ((uint64_t)(*(uint32_t *)(r15_10 + 24UL) - var_69) == 0UL) {
                                                var_70 = (uint64_t)(uint32_t)r14_4;
                                                r14_1 = (var_70 == 0UL) ? 1UL : var_70;
                                            }
                                        }
                                    }
                                }
                            }
                            var_71 = r13_5 + 32UL;
                            r13_5 = var_71;
                            rax_12 = rax_2;
                            r15_10 = r15_2;
                            r14_4 = r14_1;
                            rax_3 = rax_2;
                            r15_3 = r15_2;
                            r14_2 = r14_1;
                            if (*(uint64_t *)var_71 == 0UL) {
                                break;
                            }
                            rbp_3 = (uint64_t)((uint32_t)rbp_3 + 1U);
                            continue;
                        }
                    loop_state_var = 3U;
                    break;
                }
                while (1U)
                    {
                        var_63 = ((uint64_t)(uint32_t)rax_13_ph == 0UL);
                        rbp_4 = rbp_4_ph;
                        local_sp_17 = local_sp_17_ph;
                        r13_6 = r13_6_ph;
                        rax_1 = rax_13_ph;
                        r14_0 = r14_5_ph;
                        rax_3 = rax_13_ph;
                        r15_3 = r15_11_ph;
                        r14_2 = r14_5_ph;
                        while (1U)
                            {
                                var_64 = local_sp_17 + (-8L);
                                *(uint64_t *)var_64 = 4217263UL;
                                indirect_placeholder();
                                r15_1 = r13_6;
                                local_sp_4 = var_64;
                                local_sp_17_ph = var_64;
                                local_sp_17 = var_64;
                                if (!var_63) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                var_65 = r13_6 + 32UL;
                                r13_6 = var_65;
                                if (*(uint64_t *)var_65 != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                rbp_4 = (uint64_t)((uint32_t)rbp_4 + 1U);
                                continue;
                            }
                        switch_state_var = 0;
                        switch (loop_state_var) {
                          case 1U:
                            {
                                switch_state_var = 1;
                                break;
                            }
                            break;
                          case 0U:
                            {
                                if (r15_11_ph == 0UL) {
                                    *(uint32_t *)(local_sp_17 + 36UL) = (uint32_t)rbp_4;
                                } else {
                                    var_66 = (rax_13_ph & (-256L)) | 1UL;
                                    rax_1 = var_66;
                                    r15_1 = r15_11_ph;
                                    r14_0 = (uint64_t)(uint32_t)(((uint64_t)(uint32_t)r14_5_ph == 0UL) ? var_66 : r14_5_ph);
                                }
                                var_67 = r13_6 + 32UL;
                                rax_13_ph = rax_1;
                                r15_11_ph = r15_1;
                                r13_6_ph = var_67;
                                rax_3 = rax_1;
                                r15_3 = r15_1;
                                r14_2 = r14_0;
                                r14_5_ph = r14_0;
                                if (*(uint64_t *)var_67 == 0UL) {
                                    switch_state_var = 1;
                                    break;
                                }
                                rbp_4_ph = (uint64_t)((uint32_t)rbp_4 + 1U);
                                continue;
                            }
                            break;
                        }
                        if (switch_state_var)
                            break;
                    }
                loop_state_var = 3U;
                break;
            }
            *(uint64_t *)(local_sp_13 + 56UL) = rbx_4;
            *(uint32_t *)(local_sp_13 + 44UL) = 4294967295U;
            *(uint32_t *)(local_sp_13 + 64UL) = 0U;
            *(uint64_t *)(local_sp_13 + 32UL) = 0UL;
            var_24 = (uint64_t)((long)(var_21 << 32UL) >> (long)32UL);
            *(uint64_t *)(local_sp_13 + 48UL) = var_24;
            var_25 = (var_23 == 0U);
            *(uint32_t *)(local_sp_13 + 68UL) = (uint32_t)var_21;
            *(uint64_t *)(local_sp_13 + 72UL) = rcx;
            rax_10 = var_24;
            rax_11 = var_24;
            if (!var_25) {
                while (1U)
                    {
                        var_26 = local_sp_15 + (-8L);
                        *(uint64_t *)var_26 = 4217628UL;
                        indirect_placeholder();
                        local_sp_2 = var_26;
                        local_sp_3 = var_26;
                        rax_0 = rax_11;
                        r15_0 = r15_9;
                        r13_1 = r13_4;
                        if ((uint64_t)(uint32_t)rax_11 != 0UL) {
                            r15_0 = r12_6;
                            if (r15_9 == 0UL) {
                                *(uint32_t *)(local_sp_15 + 36UL) = (uint32_t)rbp_2;
                            } else {
                                if ((uint64_t)(uint32_t)r13_4 != 0UL) {
                                    var_27 = *(uint64_t *)(local_sp_15 + 24UL);
                                    var_37 = var_27;
                                    rax_0 = 0UL;
                                    r13_1 = 1UL;
                                    if (var_27 == 0UL) {
                                        *(unsigned char *)(rbp_2 + var_37) = (unsigned char)'\x01';
                                        local_sp_3 = local_sp_2;
                                        rax_0 = var_37;
                                    } else {
                                        var_28 = local_sp_15 + 40UL;
                                        var_29 = *(uint64_t *)var_28;
                                        var_30 = local_sp_15 + (-16L);
                                        *(uint64_t *)var_30 = 4218169UL;
                                        var_31 = indirect_placeholder_5(var_29);
                                        var_32 = var_31.field_0;
                                        *(uint64_t *)(local_sp_15 + 16UL) = var_32;
                                        local_sp_3 = var_30;
                                        if (var_32 != 0UL) {
                                            var_33 = local_sp_15 + (-24L);
                                            *(uint64_t *)var_33 = 4218194UL;
                                            indirect_placeholder();
                                            var_34 = (uint64_t)*(uint32_t *)(local_sp_15 + 20UL);
                                            var_35 = (uint64_t *)(local_sp_15 + 8UL);
                                            var_36 = *var_35;
                                            *(uint32_t *)var_28 = 1U;
                                            *(unsigned char *)(var_36 + var_34) = (unsigned char)'\x01';
                                            _pre = *var_35;
                                            var_37 = _pre;
                                            local_sp_2 = var_33;
                                            *(unsigned char *)(rbp_2 + var_37) = (unsigned char)'\x01';
                                            local_sp_3 = local_sp_2;
                                            rax_0 = var_37;
                                        }
                                    }
                                }
                            }
                        }
                        var_38 = r12_6 + 32UL;
                        rax_11 = rax_0;
                        r13_4 = r13_1;
                        local_sp_15 = local_sp_3;
                        r12_6 = var_38;
                        r15_9 = r15_0;
                        local_sp_9 = local_sp_3;
                        rax_8_in = r13_1;
                        r15_5 = r15_0;
                        if (*(uint64_t *)var_38 == 0UL) {
                            break;
                        }
                        rbp_2 = rbp_2 + 1UL;
                        continue;
                    }
                loop_state_var = 2U;
                break;
            }
            while (1U)
                {
                    var_39 = local_sp_14 + (-8L);
                    *(uint64_t *)var_39 = 4216788UL;
                    indirect_placeholder();
                    local_sp_7 = var_39;
                    local_sp_8 = var_39;
                    rax_7 = rax_10;
                    r15_4 = r15_8;
                    r12_1 = r12_5;
                    if ((uint64_t)(uint32_t)rax_10 == 0UL) {
                        var_55 = r13_3 + 32UL;
                        rax_10 = rax_7;
                        local_sp_9 = local_sp_8;
                        r13_3 = var_55;
                        local_sp_14 = local_sp_8;
                        r12_5 = r12_1;
                        r15_8 = r15_4;
                        rax_8_in = r12_1;
                        r15_5 = r15_4;
                        if (*(uint64_t *)var_55 == 0UL) {
                            break;
                        }
                        rbp_1 = rbp_1 + 1UL;
                        continue;
                    }
                    r15_4 = r13_3;
                    if (r15_8 == 0UL) {
                        *(uint32_t *)(local_sp_14 + 36UL) = (uint32_t)rbp_1;
                    } else {
                        var_40 = *(uint32_t *)(r13_3 + 8UL);
                        rax_6 = (uint64_t)var_40;
                        if ((uint64_t)(*(uint32_t *)(r15_8 + 8UL) - var_40) != 0UL) {
                            var_41 = *(uint64_t *)(r13_3 + 16UL);
                            rax_6 = var_41;
                            var_42 = *(uint32_t *)(r13_3 + 24UL);
                            var_43 = (uint64_t)var_42;
                            rax_6 = var_43;
                            rax_7 = var_43;
                            if (*(uint64_t *)(r15_8 + 16UL) != var_41 & (uint64_t)(*(uint32_t *)(r15_8 + 24UL) - var_42) != 0UL) {
                                var_55 = r13_3 + 32UL;
                                rax_10 = rax_7;
                                local_sp_9 = local_sp_8;
                                r13_3 = var_55;
                                local_sp_14 = local_sp_8;
                                r12_5 = r12_1;
                                r15_8 = r15_4;
                                rax_8_in = r12_1;
                                r15_5 = r15_4;
                                if (*(uint64_t *)var_55 == 0UL) {
                                    break;
                                }
                                rbp_1 = rbp_1 + 1UL;
                                continue;
                            }
                        }
                        rax_7 = rax_6;
                        if ((uint64_t)(uint32_t)r12_5 != 0UL) {
                            var_44 = *(uint64_t *)(local_sp_14 + 24UL);
                            var_54 = var_44;
                            rax_7 = 0UL;
                            r12_1 = 1UL;
                            if (var_44 == 0UL) {
                                *(unsigned char *)(rbp_1 + var_54) = (unsigned char)'\x01';
                                local_sp_8 = local_sp_7;
                                rax_7 = var_54;
                            } else {
                                var_45 = local_sp_14 + 40UL;
                                var_46 = *(uint64_t *)var_45;
                                var_47 = local_sp_14 + (-16L);
                                *(uint64_t *)var_47 = 4218107UL;
                                var_48 = indirect_placeholder_6(var_46);
                                var_49 = var_48.field_0;
                                *(uint64_t *)(local_sp_14 + 16UL) = var_49;
                                local_sp_8 = var_47;
                                if (var_49 != 0UL) {
                                    var_50 = local_sp_14 + (-24L);
                                    *(uint64_t *)var_50 = 4218132UL;
                                    indirect_placeholder();
                                    var_51 = (uint64_t)*(uint32_t *)(local_sp_14 + 20UL);
                                    var_52 = (uint64_t *)(local_sp_14 + 8UL);
                                    var_53 = *var_52;
                                    *(uint32_t *)var_45 = 1U;
                                    *(unsigned char *)(var_53 + var_51) = (unsigned char)'\x01';
                                    _pre276 = *var_52;
                                    var_54 = _pre276;
                                    local_sp_7 = var_50;
                                    *(unsigned char *)(rbp_1 + var_54) = (unsigned char)'\x01';
                                    local_sp_8 = local_sp_7;
                                    rax_7 = var_54;
                                }
                            }
                        }
                    }
                }
            loop_state_var = 2U;
            break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            local_sp_20 = local_sp_19;
            if (*(uint32_t *)(local_sp_19 + 4UL) != 0U) {
                var_90 = *var_9;
                var_91 = local_sp_19 + (-8L);
                *(uint64_t *)var_91 = 4217799UL;
                indirect_placeholder();
                local_sp_20 = var_91;
                if (*(unsigned char *)(*(uint64_t *)(((uint64_t)*(uint32_t *)var_8 << 3UL) + *(uint64_t *)(local_sp_19 + 16UL)) + 1UL) != '-' & var_90 == 0UL) {
                    return merge;
                }
            }
            if (*(uint32_t *)(local_sp_20 + 152UL) != 0U) {
                *(uint64_t *)(local_sp_20 + (-8L)) = 4217412UL;
                indirect_placeholder();
            }
            *var_9 = 0UL;
            var_92 = (uint32_t *)var_8;
            *var_92 = (*var_92 + 1U);
            *(uint32_t *)(var_8 + 8UL) = 0U;
            return 63UL;
        }
        break;
      case 1U:
        {
            var_75 = (uint32_t *)var_8;
            var_76 = *var_75;
            var_77 = (uint64_t)var_76;
            *var_9 = 0UL;
            var_78 = var_77 + 1UL;
            *var_75 = (uint32_t)var_78;
            var_79 = (*(unsigned char *)rbx_2 == '\x00');
            var_80 = *(uint32_t *)(r15_7 + 8UL);
            local_sp_12 = local_sp_11;
            if (var_79) {
                if (var_80 != 1U) {
                    var_81 = (uint64_t)*(uint32_t *)(local_sp_11 + 40UL);
                    var_82 = var_78 << 32UL;
                    if ((long)var_82 >= (long)(var_81 << 32UL)) {
                        if (*(uint32_t *)(local_sp_11 + 152UL) == 0U) {
                            var_83 = local_sp_11 + (-8L);
                            *(uint64_t *)var_83 = 4217753UL;
                            indirect_placeholder();
                            local_sp_12 = var_83;
                        }
                        *(uint32_t *)(var_8 + 8UL) = *(uint32_t *)(r15_7 + 24UL);
                        var_84 = (**(unsigned char **)(local_sp_12 + 24UL) == ':') ? 58UL : 63UL;
                        merge = var_84;
                        return merge;
                    }
                    var_85 = *(uint64_t *)(local_sp_11 + 16UL);
                    *var_75 = (var_76 + 2U);
                    *(uint64_t *)(var_8 + 16UL) = *(uint64_t *)((uint64_t)((long)var_82 >> (long)29UL) + var_85);
                }
            } else {
                if (var_80 != 0U) {
                    if (*(uint32_t *)(local_sp_11 + 152UL) == 0U) {
                        *(uint32_t *)(var_8 + 8UL) = *(uint32_t *)(r15_7 + 24UL);
                    } else {
                        *(uint64_t *)(local_sp_11 + (-8L)) = 4217326UL;
                        indirect_placeholder();
                        *(uint32_t *)(var_8 + 8UL) = *(uint32_t *)(r15_7 + 24UL);
                    }
                    return merge;
                }
                *(uint64_t *)(var_8 + 16UL) = (rbx_2 + 1UL);
            }
            var_86 = *(uint64_t *)(local_sp_11 + 8UL);
            merge = 0UL;
            if (var_86 == 0UL) {
                *(uint32_t *)var_86 = (uint32_t)r14_3;
            }
            var_87 = *(uint64_t *)(r15_7 + 16UL);
            var_88 = (var_87 == 0UL);
            var_89 = *(uint32_t *)(r15_7 + 24UL);
            if (var_88) {
                return (uint64_t)var_89;
            }
            *(uint32_t *)var_87 = var_89;
            return merge;
        }
        break;
      case 2U:
        {
            storemerge = (uint64_t)*(uint32_t *)(local_sp_9 + 68UL);
            local_sp_10 = local_sp_9;
            r15_6 = r15_5;
            if ((uint64_t)(uint32_t)rax_8_in != 0UL) {
                var_56 = local_sp_9 + (-8L);
                *(uint64_t *)var_56 = 4216934UL;
                indirect_placeholder();
                local_sp_5 = var_56;
                local_sp_6 = local_sp_5;
                if (*(uint32_t *)(local_sp_5 + 64UL) != 0U) {
                    var_62 = local_sp_5 + (-8L);
                    *(uint64_t *)var_62 = 4216958UL;
                    indirect_placeholder();
                    local_sp_6 = var_62;
                }
                *(uint64_t *)(local_sp_6 + (-8L)) = 4217171UL;
                indirect_placeholder();
                var_73 = (uint32_t *)var_8;
                *var_73 = (*var_73 + 1U);
                *var_9 = (*var_9 + rax_5);
                *(uint32_t *)(var_8 + 8UL) = 0U;
                return 63UL;
            }
            rbx_0_in_in = local_sp_9 + 56UL;
            rbx_1_in_in = rbx_0_in_in;
            if (*(uint64_t *)(local_sp_9 + 32UL) != 0UL) {
                *(uint64_t *)(local_sp_9 + (-8L)) = 4217853UL;
                indirect_placeholder();
                var_57 = local_sp_9 + (-16L);
                *(uint64_t *)var_57 = 4217889UL;
                indirect_placeholder();
                var_58 = storemerge << 32UL;
                local_sp_0 = var_57;
                var_60 = rbp_0 + 1UL;
                local_sp_0 = local_sp_1;
                rbp_0 = var_60;
                do {
                    local_sp_1 = local_sp_0;
                    if (*(unsigned char *)(rbp_0 + *(uint64_t *)(local_sp_9 + 16UL)) == '\x00') {
                        var_59 = local_sp_0 + (-8L);
                        *(uint64_t *)var_59 = 4217951UL;
                        indirect_placeholder();
                        local_sp_1 = var_59;
                    }
                    var_60 = rbp_0 + 1UL;
                    local_sp_0 = local_sp_1;
                    rbp_0 = var_60;
                } while ((long)var_58 <= (long)(var_60 << 32UL));
                *(uint64_t *)(local_sp_1 + (-8L)) = 4218016UL;
                indirect_placeholder();
                var_61 = local_sp_1 + (-16L);
                *(uint64_t *)var_61 = 4218024UL;
                indirect_placeholder();
                local_sp_5 = var_61;
                local_sp_6 = local_sp_5;
                if (*(uint32_t *)(local_sp_5 + 64UL) != 0U) {
                    var_62 = local_sp_5 + (-8L);
                    *(uint64_t *)var_62 = 4216958UL;
                    indirect_placeholder();
                    local_sp_6 = var_62;
                }
                *(uint64_t *)(local_sp_6 + (-8L)) = 4217171UL;
                indirect_placeholder();
                var_73 = (uint32_t *)var_8;
                *var_73 = (*var_73 + 1U);
                *var_9 = (*var_9 + rax_5);
                *(uint32_t *)(var_8 + 8UL) = 0U;
                return 63UL;
            }
            local_sp_11 = local_sp_10;
            r15_7 = r15_6;
            local_sp_19 = local_sp_10;
            if (r15_6 != 0UL) {
                local_sp_20 = local_sp_19;
                if (*(uint32_t *)(local_sp_19 + 4UL) != 0U) {
                    var_90 = *var_9;
                    var_91 = local_sp_19 + (-8L);
                    *(uint64_t *)var_91 = 4217799UL;
                    indirect_placeholder();
                    local_sp_20 = var_91;
                    if (*(unsigned char *)(*(uint64_t *)(((uint64_t)*(uint32_t *)var_8 << 3UL) + *(uint64_t *)(local_sp_19 + 16UL)) + 1UL) != '-' & var_90 == 0UL) {
                        return merge;
                    }
                }
                if (*(uint32_t *)(local_sp_20 + 152UL) != 0U) {
                    *(uint64_t *)(local_sp_20 + (-8L)) = 4217412UL;
                    indirect_placeholder();
                }
                *var_9 = 0UL;
                var_92 = (uint32_t *)var_8;
                *var_92 = (*var_92 + 1U);
                *(uint32_t *)(var_8 + 8UL) = 0U;
                return 63UL;
            }
            rbx_1 = *(uint64_t *)rbx_1_in_in;
            var_74 = (uint64_t)*(uint32_t *)(local_sp_10 + 44UL);
            rbx_2 = rbx_1;
            r14_3 = var_74;
            var_75 = (uint32_t *)var_8;
            var_76 = *var_75;
            var_77 = (uint64_t)var_76;
            *var_9 = 0UL;
            var_78 = var_77 + 1UL;
            *var_75 = (uint32_t)var_78;
            var_79 = (*(unsigned char *)rbx_2 == '\x00');
            var_80 = *(uint32_t *)(r15_7 + 8UL);
            local_sp_12 = local_sp_11;
            if (var_79) {
                if (var_80 != 0U) {
                    if (*(uint32_t *)(local_sp_11 + 152UL) == 0U) {
                        *(uint64_t *)(local_sp_11 + (-8L)) = 4217326UL;
                        indirect_placeholder();
                        *(uint32_t *)(var_8 + 8UL) = *(uint32_t *)(r15_7 + 24UL);
                    } else {
                        *(uint32_t *)(var_8 + 8UL) = *(uint32_t *)(r15_7 + 24UL);
                    }
                    return merge;
                }
                *(uint64_t *)(var_8 + 16UL) = (rbx_2 + 1UL);
            } else {
                if (var_80 != 1U) {
                    var_81 = (uint64_t)*(uint32_t *)(local_sp_11 + 40UL);
                    var_82 = var_78 << 32UL;
                    if ((long)var_82 >= (long)(var_81 << 32UL)) {
                        if (*(uint32_t *)(local_sp_11 + 152UL) == 0U) {
                            var_83 = local_sp_11 + (-8L);
                            *(uint64_t *)var_83 = 4217753UL;
                            indirect_placeholder();
                            local_sp_12 = var_83;
                        }
                        *(uint32_t *)(var_8 + 8UL) = *(uint32_t *)(r15_7 + 24UL);
                        var_84 = (**(unsigned char **)(local_sp_12 + 24UL) == ':') ? 58UL : 63UL;
                        merge = var_84;
                        return merge;
                    }
                    var_85 = *(uint64_t *)(local_sp_11 + 16UL);
                    *var_75 = (var_76 + 2U);
                    *(uint64_t *)(var_8 + 16UL) = *(uint64_t *)((uint64_t)((long)var_82 >> (long)29UL) + var_85);
                }
            }
            var_86 = *(uint64_t *)(local_sp_11 + 8UL);
            merge = 0UL;
            if (var_86 == 0UL) {
                *(uint32_t *)var_86 = (uint32_t)r14_3;
            }
            var_87 = *(uint64_t *)(r15_7 + 16UL);
            var_88 = (var_87 == 0UL);
            var_89 = *(uint32_t *)(r15_7 + 24UL);
            if (var_88) {
                return (uint64_t)var_89;
            }
            *(uint32_t *)var_87 = var_89;
            return merge;
        }
        break;
      case 3U:
        {
            var_72 = local_sp_4 + 32UL;
            local_sp_10 = local_sp_4;
            local_sp_6 = local_sp_4;
            rax_5 = rax_3;
            r15_6 = r15_3;
            rbx_1_in_in = var_72;
            if ((uint64_t)(uint32_t)r14_2 == 0UL) {
                *(uint64_t *)(local_sp_6 + (-8L)) = 4217171UL;
                indirect_placeholder();
                var_73 = (uint32_t *)var_8;
                *var_73 = (*var_73 + 1U);
                *var_9 = (*var_9 + rax_5);
                *(uint32_t *)(var_8 + 8UL) = 0U;
                return 63UL;
            }
            local_sp_11 = local_sp_10;
            r15_7 = r15_6;
            local_sp_19 = local_sp_10;
            if (r15_6 == 0UL) {
                local_sp_20 = local_sp_19;
                if (*(uint32_t *)(local_sp_19 + 4UL) != 0U) {
                    if (*(uint32_t *)(local_sp_20 + 152UL) == 0U) {
                        *(uint64_t *)(local_sp_20 + (-8L)) = 4217412UL;
                        indirect_placeholder();
                    }
                    *var_9 = 0UL;
                    var_92 = (uint32_t *)var_8;
                    *var_92 = (*var_92 + 1U);
                    *(uint32_t *)(var_8 + 8UL) = 0U;
                    return 63UL;
                }
                if (*(unsigned char *)(*(uint64_t *)(((uint64_t)*(uint32_t *)var_8 << 3UL) + *(uint64_t *)(local_sp_19 + 16UL)) + 1UL) != '-') {
                    var_90 = *var_9;
                    var_91 = local_sp_19 + (-8L);
                    *(uint64_t *)var_91 = 4217799UL;
                    indirect_placeholder();
                    local_sp_20 = var_91;
                    if (var_90 == 0UL) {
                        return merge;
                    }
                }
                if (*(uint32_t *)(local_sp_20 + 152UL) == 0U) {
                    *(uint64_t *)(local_sp_20 + (-8L)) = 4217412UL;
                    indirect_placeholder();
                }
                *var_9 = 0UL;
                var_92 = (uint32_t *)var_8;
                *var_92 = (*var_92 + 1U);
                *(uint32_t *)(var_8 + 8UL) = 0U;
                return 63UL;
            }
            rbx_1 = *(uint64_t *)rbx_1_in_in;
            var_74 = (uint64_t)*(uint32_t *)(local_sp_10 + 44UL);
            rbx_2 = rbx_1;
            r14_3 = var_74;
            var_75 = (uint32_t *)var_8;
            var_76 = *var_75;
            var_77 = (uint64_t)var_76;
            *var_9 = 0UL;
            var_78 = var_77 + 1UL;
            *var_75 = (uint32_t)var_78;
            var_79 = (*(unsigned char *)rbx_2 == '\x00');
            var_80 = *(uint32_t *)(r15_7 + 8UL);
            local_sp_12 = local_sp_11;
            if (var_79) {
                if (var_80 != 0U) {
                    if (*(uint32_t *)(local_sp_11 + 152UL) == 0U) {
                        *(uint64_t *)(local_sp_11 + (-8L)) = 4217326UL;
                        indirect_placeholder();
                        *(uint32_t *)(var_8 + 8UL) = *(uint32_t *)(r15_7 + 24UL);
                    } else {
                        *(uint32_t *)(var_8 + 8UL) = *(uint32_t *)(r15_7 + 24UL);
                    }
                    return merge;
                }
                *(uint64_t *)(var_8 + 16UL) = (rbx_2 + 1UL);
            } else {
                if (var_80 != 1U) {
                    var_81 = (uint64_t)*(uint32_t *)(local_sp_11 + 40UL);
                    var_82 = var_78 << 32UL;
                    if ((long)var_82 >= (long)(var_81 << 32UL)) {
                        if (*(uint32_t *)(local_sp_11 + 152UL) == 0U) {
                            var_83 = local_sp_11 + (-8L);
                            *(uint64_t *)var_83 = 4217753UL;
                            indirect_placeholder();
                            local_sp_12 = var_83;
                        }
                        *(uint32_t *)(var_8 + 8UL) = *(uint32_t *)(r15_7 + 24UL);
                        var_84 = (**(unsigned char **)(local_sp_12 + 24UL) == ':') ? 58UL : 63UL;
                        merge = var_84;
                        return merge;
                    }
                    var_85 = *(uint64_t *)(local_sp_11 + 16UL);
                    *var_75 = (var_76 + 2U);
                    *(uint64_t *)(var_8 + 16UL) = *(uint64_t *)((uint64_t)((long)var_82 >> (long)29UL) + var_85);
                }
            }
            var_86 = *(uint64_t *)(local_sp_11 + 8UL);
            merge = 0UL;
            if (var_86 == 0UL) {
                *(uint32_t *)var_86 = (uint32_t)r14_3;
            }
            var_87 = *(uint64_t *)(r15_7 + 16UL);
            var_88 = (var_87 == 0UL);
            var_89 = *(uint32_t *)(r15_7 + 24UL);
            if (var_88) {
                return (uint64_t)var_89;
            }
            *(uint32_t *)var_87 = var_89;
            return merge;
        }
        break;
    }
}
