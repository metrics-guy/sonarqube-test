typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_pxor_xmm_wrapper_83_ret_type;
struct type_5;
struct type_7;
struct helper_cvtsq2ss_wrapper_ret_type;
struct helper_addss_wrapper_ret_type;
struct helper_mulss_wrapper_ret_type;
struct helper_ucomiss_wrapper_ret_type;
struct helper_cvttss2sq_wrapper_ret_type;
struct helper_subss_wrapper_ret_type;
struct indirect_placeholder_72_ret_type;
struct indirect_placeholder_73_ret_type;
struct helper_cvttss2si_wrapper_ret_type;
struct indirect_placeholder_74_ret_type;
struct helper_pxor_xmm_wrapper_83_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_5 {
};
struct type_7 {
};
struct helper_cvtsq2ss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_addss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_mulss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_ucomiss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvttss2sq_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_subss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct indirect_placeholder_72_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_73_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_cvttss2si_wrapper_ret_type {
    uint32_t field_0;
    unsigned char field_1;
};
struct indirect_placeholder_74_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_state_0x8558(void);
extern unsigned char init_state_0x8549(void);
extern unsigned char init_state_0x854c(void);
extern unsigned char init_state_0x8548(void);
extern unsigned char init_state_0x854b(void);
extern unsigned char init_state_0x8547(void);
extern unsigned char init_state_0x854d(void);
extern uint64_t init_state_0x8560(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_r15(void);
extern uint64_t init_state_0x8d58(void);
extern struct helper_pxor_xmm_wrapper_83_ret_type helper_pxor_xmm_wrapper_83(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4);
extern struct helper_cvtsq2ss_wrapper_ret_type helper_cvtsq2ss_wrapper(struct type_5 *param_0, struct type_7 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_addss_wrapper_ret_type helper_addss_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9);
extern struct helper_mulss_wrapper_ret_type helper_mulss_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_ucomiss_wrapper_ret_type helper_ucomiss_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_cvttss2sq_wrapper_ret_type helper_cvttss2sq_wrapper(struct type_5 *param_0, struct type_7 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4);
extern struct helper_subss_wrapper_ret_type helper_subss_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct indirect_placeholder_72_ret_type indirect_placeholder_72(uint64_t param_0);
extern struct indirect_placeholder_73_ret_type indirect_placeholder_73(uint64_t param_0);
extern struct helper_cvttss2si_wrapper_ret_type helper_cvttss2si_wrapper(struct type_5 *param_0, struct type_7 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4);
extern struct indirect_placeholder_74_ret_type indirect_placeholder_74(uint64_t param_0);
uint64_t bb_scale10_round_decimal_decoded(uint64_t rdi, uint64_t rcx, uint64_t rdx, uint64_t r8, uint64_t rsi) {
    uint64_t var_199;
    uint64_t var_200;
    uint64_t var_201;
    uint64_t state_0x8560_4;
    uint64_t rax_4;
    uint64_t var_237;
    uint64_t var_238;
    uint64_t var_239;
    uint64_t rdx3_12;
    uint64_t var_261;
    uint64_t var_262;
    uint64_t rsi5_0;
    uint64_t rcx2_1;
    struct helper_cvtsq2ss_wrapper_ret_type var_243;
    uint64_t state_0x8560_3;
    uint64_t state_0x8558_12;
    uint64_t rdx3_10;
    uint64_t rax_16;
    uint64_t var_149;
    struct helper_pxor_xmm_wrapper_83_ret_type var_186;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    unsigned char var_11;
    unsigned char var_12;
    unsigned char var_13;
    unsigned char var_14;
    unsigned char var_15;
    unsigned char var_16;
    uint64_t storemerge10;
    uint64_t *_cast;
    uint64_t var_17;
    uint64_t *var_18;
    uint64_t state_0x8558_2;
    uint64_t rax_5;
    uint64_t var_264;
    uint64_t var_265;
    uint64_t var_173;
    uint64_t var_174;
    uint64_t rax_0;
    uint64_t rdi1_1;
    uint64_t var_175;
    uint64_t rdi1_0;
    uint64_t var_176;
    uint64_t var_177;
    uint64_t var_178;
    uint64_t var_179;
    uint64_t rdx3_0;
    uint64_t var_180;
    uint64_t state_0x8558_1;
    uint64_t rdx3_1;
    uint64_t rax_1;
    uint64_t var_181;
    uint64_t var_182;
    uint64_t var_183;
    uint64_t var_184;
    uint64_t var_185;
    uint64_t rdi1_2;
    uint64_t var_187;
    uint64_t var_188;
    uint64_t r10_0;
    uint64_t rax_2;
    uint64_t var_189;
    uint64_t state_0x8558_0;
    uint64_t var_190;
    uint64_t var_191;
    uint64_t state_0x8560_0;
    uint64_t rdx3_2;
    uint64_t var_192;
    uint64_t var_193;
    uint64_t state_0x8560_1;
    uint64_t state_0x8560_2;
    uint64_t rax_3;
    bool var_194;
    uint64_t var_195;
    bool var_196;
    uint64_t rsi5_1;
    uint64_t var_266;
    uint64_t state_0x8558_4;
    uint64_t var_202;
    uint64_t var_203;
    uint64_t var_204;
    uint64_t var_205;
    unsigned char var_206;
    uint64_t var_207;
    uint64_t var_208;
    uint64_t var_209;
    uint64_t var_210;
    uint64_t rdi1_3;
    uint32_t var_211;
    uint64_t var_212;
    uint64_t var_213;
    uint32_t var_214;
    uint64_t var_215;
    uint64_t var_216;
    uint64_t r9_0;
    uint64_t var_217;
    uint64_t var_218;
    uint64_t rcx2_0;
    uint64_t var_219;
    uint64_t var_220;
    uint64_t var_221;
    uint64_t var_222;
    uint64_t var_223;
    uint64_t var_224;
    uint64_t var_225;
    uint64_t rdx3_4;
    uint64_t rdx3_3;
    uint64_t r11_0;
    uint64_t var_226;
    uint64_t var_227;
    uint64_t var_228;
    uint64_t var_229;
    uint64_t var_230;
    uint64_t var_231;
    uint64_t var_232;
    uint64_t var_233;
    uint64_t var_234;
    uint64_t state_0x8558_3;
    uint64_t var_235;
    uint64_t var_236;
    uint64_t var_147;
    uint64_t rdx3_5;
    uint64_t r84_0;
    uint64_t var_197;
    uint64_t var_198;
    uint64_t var_263;
    uint64_t rdx3_6;
    uint64_t rdx3_7;
    uint64_t var_267;
    uint64_t var_268;
    uint64_t var_269;
    uint64_t rcx2_2;
    uint64_t var_270;
    uint64_t var_271;
    uint64_t rdi1_4;
    uint64_t rsi5_2;
    uint64_t var_272;
    uint64_t rdx3_8;
    uint64_t var_273;
    uint64_t var_274;
    uint64_t var_121;
    uint64_t var_275;
    uint64_t rbx_0;
    uint64_t var_276;
    uint64_t rbx_3;
    uint64_t rbx_2_ph;
    uint64_t rbx_1;
    uint64_t r12_0;
    uint64_t cc_src2_0;
    uint64_t var_277;
    uint64_t rdi1_5;
    uint64_t rcx2_3;
    uint64_t rsi5_3;
    uint64_t var_278;
    uint32_t *_cast3;
    uint32_t var_279;
    unsigned __int128 var_280;
    uint64_t var_281;
    uint64_t var_282;
    uint64_t var_283;
    uint64_t var_284;
    unsigned char var_285;
    unsigned char var_286;
    uint64_t var_287;
    unsigned char var_288;
    unsigned char var_289;
    uint64_t var_290;
    unsigned char var_291;
    unsigned char var_292;
    uint64_t var_293;
    unsigned char var_294;
    unsigned char var_295;
    uint64_t var_296;
    unsigned char var_297;
    unsigned char var_298;
    uint64_t var_299;
    unsigned char var_300;
    unsigned char var_301;
    uint64_t var_302;
    unsigned char var_303;
    unsigned char var_304;
    uint64_t var_305;
    uint64_t var_306;
    unsigned char var_307;
    uint64_t var_308;
    unsigned char var_309;
    uint64_t var_310;
    uint64_t var_311;
    uint64_t var_312;
    uint64_t rbx_2;
    uint64_t var_313;
    uint64_t rbx_4;
    uint64_t var_314;
    uint64_t r14_4;
    uint64_t var_240;
    uint64_t var_241;
    bool var_242;
    uint64_t local_sp_0;
    uint64_t cc_dst_0;
    uint64_t var_244;
    struct helper_cvtsq2ss_wrapper_ret_type var_245;
    struct helper_addss_wrapper_ret_type var_246;
    uint64_t state_0x8558_5;
    unsigned char storemerge9;
    struct helper_mulss_wrapper_ret_type var_247;
    uint64_t var_248;
    struct helper_ucomiss_wrapper_ret_type var_249;
    uint64_t var_250;
    unsigned char var_251;
    uint64_t var_252;
    struct helper_cvttss2sq_wrapper_ret_type var_253;
    uint64_t rax_6;
    struct helper_subss_wrapper_ret_type var_254;
    struct helper_cvttss2sq_wrapper_ret_type var_255;
    uint64_t var_256;
    uint64_t var_257;
    uint64_t var_258;
    struct indirect_placeholder_72_ret_type var_259;
    uint64_t var_260;
    uint64_t state_0x8558_9;
    uint64_t rbp_0;
    uint64_t state_0x8560_12;
    uint64_t state_0x8560_8;
    uint64_t state_0x8558_6;
    uint64_t storemerge;
    uint64_t state_0x8560_5;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t rax_7;
    uint64_t rcx2_5;
    uint64_t rcx2_4;
    uint64_t state_0x8558_8;
    uint64_t rax_8;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t rcx2_6;
    uint64_t var_94;
    struct helper_pxor_xmm_wrapper_83_ret_type var_95;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t r10_1;
    uint64_t rax_9;
    uint64_t var_98;
    uint64_t state_0x8558_7;
    uint64_t var_99;
    uint64_t state_0x8560_6;
    uint64_t var_100;
    uint64_t var_101;
    uint64_t state_0x8560_7;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t local_sp_1;
    uint64_t var_315;
    uint64_t r14_2;
    uint64_t r14_1;
    uint64_t rdi1_6;
    uint64_t r14_0;
    uint32_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t rax_10;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t r9_1;
    uint64_t rax_11;
    uint32_t *var_73;
    uint32_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint32_t var_80;
    uint64_t var_81;
    bool var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_107;
    uint64_t r14_3;
    uint64_t rdi1_7;
    uint64_t r9_2;
    uint64_t rax_12;
    uint32_t *var_108;
    uint64_t var_109;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t state_0x8558_13;
    uint64_t state_0x8558_10;
    uint64_t var_112;
    uint64_t var_113;
    uint64_t var_114;
    uint64_t var_115;
    uint64_t var_119;
    uint64_t var_120;
    uint64_t rax_13;
    uint64_t var_122;
    uint64_t var_123;
    uint64_t var_124;
    uint64_t var_125;
    uint64_t var_126;
    uint64_t var_127;
    uint64_t var_128;
    uint64_t var_129;
    uint64_t var_130;
    uint64_t rax_14;
    uint64_t rdi1_8;
    uint64_t rdx3_9;
    uint64_t var_131;
    uint64_t var_132;
    uint64_t var_133;
    uint64_t var_134;
    uint64_t var_135;
    uint64_t var_136;
    uint64_t var_137;
    uint64_t var_138;
    uint64_t var_139;
    uint64_t var_116;
    uint64_t var_117;
    uint64_t var_118;
    uint64_t state_0x8560_9;
    uint64_t var_140;
    uint64_t var_141;
    uint64_t var_142;
    uint64_t var_143;
    uint64_t _pre;
    uint64_t _pre_phi;
    uint64_t rcx2_9;
    uint64_t rax_15;
    uint64_t rcx2_8;
    uint64_t var_144;
    uint64_t var_145;
    uint64_t var_146;
    uint64_t var_148;
    uint64_t var_150;
    uint64_t var_151;
    uint64_t var_152;
    uint64_t rcx2_10;
    struct helper_pxor_xmm_wrapper_83_ret_type var_153;
    uint64_t var_154;
    uint64_t var_155;
    uint64_t rdx3_11;
    uint64_t rax_17;
    uint64_t var_156;
    uint64_t state_0x8558_11;
    uint64_t var_157;
    uint64_t state_0x8560_10;
    uint64_t var_158;
    uint64_t var_159;
    uint64_t state_0x8560_11;
    uint64_t var_160;
    uint64_t var_161;
    uint64_t *var_162;
    uint64_t var_163;
    uint64_t var_164;
    uint64_t var_165;
    uint64_t var_166;
    uint64_t var_167;
    uint64_t *var_168;
    struct indirect_placeholder_73_ret_type var_169;
    uint64_t var_170;
    uint64_t var_171;
    uint64_t var_172;
    uint64_t rbx_5;
    uint64_t local_sp_2;
    uint64_t var_19;
    uint32_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t r84_1;
    uint64_t var_24;
    uint64_t var_25;
    uint32_t var_26;
    struct helper_pxor_xmm_wrapper_83_ret_type var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint32_t *var_31;
    uint32_t var_32;
    uint32_t *var_33;
    uint32_t var_34;
    uint32_t var_35;
    uint32_t var_36;
    uint64_t var_37;
    uint32_t var_38;
    uint32_t var_39;
    uint64_t var_40;
    struct helper_cvtsq2ss_wrapper_ret_type var_41;
    uint64_t var_42;
    unsigned char var_43;
    uint64_t var_44;
    uint32_t *var_45;
    uint32_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    struct helper_mulss_wrapper_ret_type var_50;
    uint64_t var_51;
    struct helper_cvttss2si_wrapper_ret_type var_52;
    uint32_t var_53;
    unsigned char var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t *var_57;
    struct indirect_placeholder_74_ret_type var_58;
    uint64_t var_59;
    uint32_t *var_60;
    uint32_t var_61;
    uint32_t var_62;
    uint64_t var_63;
    uint32_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r15();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r12();
    var_5 = init_rbp();
    var_6 = init_cc_src2();
    var_7 = init_state_0x8558();
    var_8 = init_state_0x8560();
    var_9 = init_r14();
    var_10 = init_state_0x8d58();
    var_11 = init_state_0x8549();
    var_12 = init_state_0x854c();
    var_13 = init_state_0x8548();
    var_14 = init_state_0x854b();
    var_15 = init_state_0x8547();
    var_16 = init_state_0x854d();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_9;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_5;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    _cast = (uint64_t *)(var_0 + (-136L));
    *_cast = rsi;
    var_17 = var_0 + (-128L);
    var_18 = (uint64_t *)var_17;
    *var_18 = rcx;
    rax_16 = 0UL;
    storemerge10 = 0UL;
    rdx3_0 = 1UL;
    rdx3_1 = 0UL;
    rax_1 = 0UL;
    rax_2 = 0UL;
    rsi5_1 = 0UL;
    rdx3_4 = 0UL;
    rdx3_3 = 0UL;
    r11_0 = 0UL;
    rdx3_5 = 0UL;
    r84_0 = 0UL;
    rdi1_4 = 0UL;
    cc_src2_0 = var_6;
    rcx2_3 = 0UL;
    storemerge = 0UL;
    rcx2_5 = 0UL;
    rcx2_4 = 1UL;
    rax_8 = 0UL;
    rax_9 = 0UL;
    r14_2 = 1UL;
    rdi1_6 = 0UL;
    r14_0 = 1UL;
    rax_10 = 13UL;
    rax_11 = 0UL;
    rax_12 = 0UL;
    rdi1_8 = 0UL;
    rdx3_9 = 0UL;
    rdx3_11 = 0UL;
    rbx_5 = 0UL;
    r84_1 = r8;
    if (rcx == 0UL) {
        return storemerge10;
    }
    var_19 = r8 + rdi;
    var_20 = (uint32_t)var_19;
    var_21 = (uint64_t)var_20;
    var_22 = helper_cc_compute_all_wrapper(var_21, 0UL, 0UL, 24U);
    rdx3_12 = var_21;
    var_23 = helper_cc_compute_all_wrapper(r8, 0UL, 0UL, 24U);
    if ((uint64_t)(((unsigned char)(var_22 >> 4UL) ^ (unsigned char)var_22) & '\xc0') != 0UL & (uint64_t)(((unsigned char)(var_23 >> 4UL) ^ (unsigned char)var_23) & '\xc0') == 0UL) {
        var_24 = ((long)(var_19 << 32UL) > (long)(r8 << 32UL)) ? r8 : var_21;
        var_25 = (uint64_t)((long)(var_24 << 32UL) >> (long)32UL);
        var_26 = (uint32_t)var_24;
        rbx_5 = var_25;
        rdx3_12 = (uint64_t)(var_20 - var_26);
        r84_1 = (uint64_t)((uint32_t)r8 - var_26);
    }
    var_27 = helper_pxor_xmm_wrapper_83((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_7, var_8);
    var_28 = var_27.field_1;
    var_29 = (uint64_t)((long)(r84_1 << 32UL) >> (long)63UL);
    var_30 = var_0 + (-96L);
    *(uint64_t *)var_30 = rdx;
    var_31 = (uint32_t *)(var_0 + (-100L));
    var_32 = (uint32_t)r84_1;
    *var_31 = var_32;
    var_33 = (uint32_t *)(var_0 + (-112L));
    var_34 = (uint32_t)rdx3_12;
    *var_33 = var_34;
    var_35 = (uint32_t)var_29;
    var_36 = (var_35 ^ var_32) - var_35;
    var_37 = (uint64_t)var_36;
    var_38 = (uint32_t)(uint64_t)((long)(rdx3_12 << 32UL) >> (long)63UL);
    var_39 = (var_38 ^ var_34) - var_38;
    var_40 = (uint64_t)var_39;
    var_41 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_37, var_11, var_13, var_14, var_15);
    var_42 = var_41.field_0;
    var_43 = var_41.field_1;
    var_44 = var_0 + (-120L);
    var_45 = (uint32_t *)var_44;
    *var_45 = var_39;
    var_46 = (uint32_t)(var_40 >> 5UL) & 134217727U;
    var_47 = (uint64_t)var_46;
    var_48 = (uint64_t)*(uint32_t *)4268788UL;
    var_49 = var_10 & (-4294967296L);
    var_50 = helper_mulss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), var_42, var_49 | var_48, var_43, var_12, var_13, var_14, var_15, var_16);
    var_51 = var_50.field_0;
    var_52 = helper_cvttss2si_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_51, var_50.field_1, var_12);
    var_53 = var_52.field_0;
    var_54 = var_52.field_1;
    var_55 = (((uint64_t)(var_46 + var_53) << 2UL) + 8UL) & 17179869180UL;
    var_56 = var_0 + (-144L);
    var_57 = (uint64_t *)var_56;
    *var_57 = 4226002UL;
    var_58 = indirect_placeholder_74(var_55);
    var_59 = var_58.field_0;
    var_60 = (uint32_t *)var_17;
    var_61 = *var_60;
    var_62 = *var_45;
    var_63 = (uint64_t)var_62;
    var_64 = *(uint32_t *)(var_0 + (-108L));
    var_65 = var_0 + (-104L);
    var_66 = *(uint64_t *)var_65;
    rsi5_0 = rbx_5;
    state_0x8558_2 = var_51;
    state_0x8558_1 = var_51;
    state_0x8558_0 = var_51;
    state_0x8560_0 = var_28;
    state_0x8560_1 = var_28;
    state_0x8560_2 = var_28;
    rdx3_6 = rbx_5;
    rdx3_7 = rbx_5;
    state_0x8558_9 = var_51;
    state_0x8560_12 = var_28;
    state_0x8560_8 = var_28;
    rax_7 = var_47;
    state_0x8558_8 = var_51;
    state_0x8558_7 = var_51;
    state_0x8560_6 = var_28;
    state_0x8560_7 = var_28;
    r9_1 = var_59;
    rdi1_7 = var_59;
    state_0x8558_13 = var_51;
    state_0x8558_10 = var_51;
    state_0x8560_9 = var_28;
    rcx2_9 = var_47;
    rax_15 = var_47;
    local_sp_2 = var_56;
    if (var_59 == 0UL) {
        *(uint64_t *)(local_sp_2 + (-8L)) = 4228235UL;
        indirect_placeholder();
    } else {
        *(uint32_t *)var_59 = 1U;
        if (var_37 != 0UL) {
            *var_60 = (var_36 + 13U);
            var_79 = helper_cc_compute_c_wrapper(var_69, var_68, var_6, 16U);
            r14_0 = r14_1;
            r14_2 = r14_1;
            do {
                var_67 = (uint32_t)rdi1_6 + 13U;
                var_68 = (uint64_t)var_67;
                var_69 = var_37 - var_68;
                var_70 = helper_cc_compute_c_wrapper(var_69, var_68, var_6, 16U);
                rdi1_6 = var_68;
                r14_1 = r14_0;
                if (var_70 == 0UL) {
                    rax_10 = (uint64_t)(*var_60 - var_67);
                }
                var_71 = (r14_0 << 2UL) + var_59;
                var_72 = (uint64_t)*(uint32_t *)((rax_10 << 2UL) + 4268672UL);
                var_73 = (uint32_t *)r9_1;
                var_74 = *var_73;
                var_75 = r9_1 + 4UL;
                var_76 = rax_11 + (var_72 * (uint64_t)var_74);
                *var_73 = (uint32_t)var_76;
                var_77 = var_76 >> 32UL;
                r9_1 = var_75;
                rax_11 = var_77;
                do {
                    var_73 = (uint32_t *)r9_1;
                    var_74 = *var_73;
                    var_75 = r9_1 + 4UL;
                    var_76 = rax_11 + (var_72 * (uint64_t)var_74);
                    *var_73 = (uint32_t)var_76;
                    var_77 = var_76 >> 32UL;
                    r9_1 = var_75;
                    rax_11 = var_77;
                } while (var_75 != var_71);
                if (var_77 == 0UL) {
                    var_78 = r14_0 + 1UL;
                    *(uint32_t *)var_71 = (uint32_t)var_77;
                    r14_1 = var_78;
                }
                var_79 = helper_cc_compute_c_wrapper(var_69, var_68, var_6, 16U);
                r14_0 = r14_1;
                r14_2 = r14_1;
            } while (var_79 != 0UL);
        }
        var_80 = var_61 & 31U;
        var_81 = (uint64_t)var_80;
        var_82 = ((int)var_64 < (int)0U);
        r9_2 = r14_2;
        r14_3 = r14_2;
        if (var_82) {
            var_107 = helper_cc_compute_all_wrapper(var_63, 0UL, 0UL, 24U);
            if ((uint64_t)(((unsigned char)(var_107 >> 4UL) ^ (unsigned char)var_107) & '\xc0') == 0UL) {
                var_165 = *var_57;
                *(uint64_t *)var_44 = var_66;
                *var_18 = var_47;
                var_166 = ((var_165 + var_47) << 2UL) + 4UL;
                var_167 = var_0 + (-152L);
                var_168 = (uint64_t *)var_167;
                *var_168 = 4228313UL;
                var_169 = indirect_placeholder_73(var_166);
                var_170 = var_169.field_0;
                var_171 = *_cast;
                var_172 = *var_18;
                rax_0 = var_171;
                rdi1_1 = var_170;
                rax_3 = var_170;
                r9_0 = var_172;
                local_sp_1 = var_167;
                if (var_170 != 0UL) {
                    var_315 = local_sp_1 + (-8L);
                    *(uint64_t *)var_315 = 4228225UL;
                    indirect_placeholder();
                    local_sp_2 = var_315;
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4228235UL;
                    indirect_placeholder();
                    return storemerge10;
                }
                if (var_47 != 0UL) {
                    var_173 = (0UL - (var_170 >> 2UL)) & 3UL;
                    var_174 = (var_171 > var_173) ? var_173 : var_171;
                    if (var_171 > 6UL) {
                        rax_0 = var_174;
                        if (var_174 == 0UL) {
                            var_175 = var_170 + 4UL;
                            *(uint32_t *)var_170 = 0U;
                            rdi1_0 = var_175;
                            rax_1 = rax_0;
                            var_176 = var_170 + 8UL;
                            *(uint32_t *)var_175 = 0U;
                            rdi1_0 = var_176;
                            rdx3_0 = 2UL;
                            var_177 = var_170 + 12UL;
                            *(uint32_t *)var_176 = 0U;
                            rdi1_0 = var_177;
                            rdx3_0 = 3UL;
                            var_178 = var_170 + 16UL;
                            *(uint32_t *)var_177 = 0U;
                            rdi1_0 = var_178;
                            rdx3_0 = 4UL;
                            var_179 = var_170 + 20UL;
                            *(uint32_t *)var_178 = 0U;
                            rdi1_0 = var_179;
                            rdx3_0 = 5UL;
                            if (rax_0 != 1UL & rax_0 != 2UL & rax_0 != 3UL & rax_0 != 4UL & rax_0 == 6UL) {
                                var_180 = var_170 + 24UL;
                                *(uint32_t *)var_179 = 0U;
                                rdi1_0 = var_180;
                                rdx3_0 = 6UL;
                            }
                            rdi1_1 = rdi1_0;
                            rdx3_1 = rdx3_0;
                            if (var_171 != rax_0) {
                                var_181 = var_171 - rax_1;
                                var_182 = var_171 + (rax_1 ^ (-1L));
                                var_183 = (var_181 + (-4L)) >> 2UL;
                                var_184 = var_183 + 1UL;
                                var_185 = var_184 << 2UL;
                                rdi1_2 = rdi1_1;
                                rdx3_2 = rdx3_1;
                                if (var_182 > 2UL) {
                                    var_186 = helper_pxor_xmm_wrapper_83((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_51, var_28);
                                    var_187 = var_186.field_0;
                                    var_188 = var_186.field_1;
                                    state_0x8558_1 = var_187;
                                    r10_0 = (rax_1 << 2UL) + var_170;
                                    state_0x8558_0 = var_187;
                                    state_0x8560_0 = var_188;
                                    state_0x8560_1 = var_188;
                                    var_189 = rax_2 + 1UL;
                                    *(uint64_t *)r10_0 = var_187;
                                    *(uint64_t *)(r10_0 + 8UL) = var_188;
                                    rax_2 = var_189;
                                    while (var_183 >= var_189)
                                        {
                                            r10_0 = r10_0 + 16UL;
                                            var_189 = rax_2 + 1UL;
                                            *(uint64_t *)r10_0 = var_187;
                                            *(uint64_t *)(r10_0 + 8UL) = var_188;
                                            rax_2 = var_189;
                                        }
                                    var_190 = rdx3_1 + var_185;
                                    var_191 = (var_184 << 4UL) + rdi1_1;
                                    rdi1_2 = var_191;
                                    rdx3_2 = var_190;
                                    var_192 = rdx3_2 + 1UL;
                                    *(uint32_t *)rdi1_2 = 0U;
                                    state_0x8558_1 = state_0x8558_0;
                                    state_0x8560_1 = state_0x8560_0;
                                    var_193 = rdx3_2 + 2UL;
                                    *(uint32_t *)(rdi1_2 + 4UL) = 0U;
                                    if (var_185 != var_181 & var_171 <= var_192 & var_171 > var_193) {
                                        *(uint32_t *)(rdi1_2 + 8UL) = 0U;
                                    }
                                } else {
                                    var_192 = rdx3_2 + 1UL;
                                    *(uint32_t *)rdi1_2 = 0U;
                                    state_0x8558_1 = state_0x8558_0;
                                    state_0x8560_1 = state_0x8560_0;
                                    var_193 = rdx3_2 + 2UL;
                                    *(uint32_t *)(rdi1_2 + 4UL) = 0U;
                                    if (var_171 <= var_192 & var_171 > var_193) {
                                        *(uint32_t *)(rdi1_2 + 8UL) = 0U;
                                    }
                                }
                            }
                        } else {
                            var_181 = var_171 - rax_1;
                            var_182 = var_171 + (rax_1 ^ (-1L));
                            var_183 = (var_181 + (-4L)) >> 2UL;
                            var_184 = var_183 + 1UL;
                            var_185 = var_184 << 2UL;
                            rdi1_2 = rdi1_1;
                            rdx3_2 = rdx3_1;
                            if (var_182 > 2UL) {
                                var_186 = helper_pxor_xmm_wrapper_83((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_51, var_28);
                                var_187 = var_186.field_0;
                                var_188 = var_186.field_1;
                                state_0x8558_1 = var_187;
                                r10_0 = (rax_1 << 2UL) + var_170;
                                state_0x8558_0 = var_187;
                                state_0x8560_0 = var_188;
                                state_0x8560_1 = var_188;
                                var_189 = rax_2 + 1UL;
                                *(uint64_t *)r10_0 = var_187;
                                *(uint64_t *)(r10_0 + 8UL) = var_188;
                                rax_2 = var_189;
                                while (var_183 >= var_189)
                                    {
                                        r10_0 = r10_0 + 16UL;
                                        var_189 = rax_2 + 1UL;
                                        *(uint64_t *)r10_0 = var_187;
                                        *(uint64_t *)(r10_0 + 8UL) = var_188;
                                        rax_2 = var_189;
                                    }
                                var_190 = rdx3_1 + var_185;
                                var_191 = (var_184 << 4UL) + rdi1_1;
                                rdi1_2 = var_191;
                                rdx3_2 = var_190;
                                var_192 = rdx3_2 + 1UL;
                                *(uint32_t *)rdi1_2 = 0U;
                                state_0x8558_1 = state_0x8558_0;
                                state_0x8560_1 = state_0x8560_0;
                                var_193 = rdx3_2 + 2UL;
                                *(uint32_t *)(rdi1_2 + 4UL) = 0U;
                                if (var_185 != var_181 & var_171 <= var_192 & var_171 > var_193) {
                                    *(uint32_t *)(rdi1_2 + 8UL) = 0U;
                                }
                            } else {
                                var_192 = rdx3_2 + 1UL;
                                *(uint32_t *)rdi1_2 = 0U;
                                state_0x8558_1 = state_0x8558_0;
                                state_0x8560_1 = state_0x8560_0;
                                var_193 = rdx3_2 + 2UL;
                                *(uint32_t *)(rdi1_2 + 4UL) = 0U;
                                if (var_171 <= var_192 & var_171 > var_193) {
                                    *(uint32_t *)(rdi1_2 + 8UL) = 0U;
                                }
                            }
                        }
                    } else {
                        var_175 = var_170 + 4UL;
                        *(uint32_t *)var_170 = 0U;
                        rdi1_0 = var_175;
                        rax_1 = rax_0;
                        var_176 = var_170 + 8UL;
                        *(uint32_t *)var_175 = 0U;
                        rdi1_0 = var_176;
                        rdx3_0 = 2UL;
                        var_177 = var_170 + 12UL;
                        *(uint32_t *)var_176 = 0U;
                        rdi1_0 = var_177;
                        rdx3_0 = 3UL;
                        var_178 = var_170 + 16UL;
                        *(uint32_t *)var_177 = 0U;
                        rdi1_0 = var_178;
                        rdx3_0 = 4UL;
                        var_179 = var_170 + 20UL;
                        *(uint32_t *)var_178 = 0U;
                        rdi1_0 = var_179;
                        rdx3_0 = 5UL;
                        if (rax_0 != 1UL & rax_0 != 2UL & rax_0 != 3UL & rax_0 != 4UL & rax_0 == 6UL) {
                            var_180 = var_170 + 24UL;
                            *(uint32_t *)var_179 = 0U;
                            rdi1_0 = var_180;
                            rdx3_0 = 6UL;
                        }
                        rdi1_1 = rdi1_0;
                        rdx3_1 = rdx3_0;
                        if (var_171 != rax_0) {
                            var_181 = var_171 - rax_1;
                            var_182 = var_171 + (rax_1 ^ (-1L));
                            var_183 = (var_181 + (-4L)) >> 2UL;
                            var_184 = var_183 + 1UL;
                            var_185 = var_184 << 2UL;
                            rdi1_2 = rdi1_1;
                            rdx3_2 = rdx3_1;
                            if (var_182 > 2UL) {
                                var_186 = helper_pxor_xmm_wrapper_83((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_51, var_28);
                                var_187 = var_186.field_0;
                                var_188 = var_186.field_1;
                                state_0x8558_1 = var_187;
                                r10_0 = (rax_1 << 2UL) + var_170;
                                state_0x8558_0 = var_187;
                                state_0x8560_0 = var_188;
                                state_0x8560_1 = var_188;
                                var_189 = rax_2 + 1UL;
                                *(uint64_t *)r10_0 = var_187;
                                *(uint64_t *)(r10_0 + 8UL) = var_188;
                                rax_2 = var_189;
                                while (var_183 >= var_189)
                                    {
                                        r10_0 = r10_0 + 16UL;
                                        var_189 = rax_2 + 1UL;
                                        *(uint64_t *)r10_0 = var_187;
                                        *(uint64_t *)(r10_0 + 8UL) = var_188;
                                        rax_2 = var_189;
                                    }
                                var_190 = rdx3_1 + var_185;
                                var_191 = (var_184 << 4UL) + rdi1_1;
                                rdi1_2 = var_191;
                                rdx3_2 = var_190;
                                var_192 = rdx3_2 + 1UL;
                                *(uint32_t *)rdi1_2 = 0U;
                                state_0x8558_1 = state_0x8558_0;
                                state_0x8560_1 = state_0x8560_0;
                                var_193 = rdx3_2 + 2UL;
                                *(uint32_t *)(rdi1_2 + 4UL) = 0U;
                                if (var_185 != var_181 & var_171 <= var_192 & var_171 > var_193) {
                                    *(uint32_t *)(rdi1_2 + 8UL) = 0U;
                                }
                            } else {
                                var_192 = rdx3_2 + 1UL;
                                *(uint32_t *)rdi1_2 = 0U;
                                state_0x8558_1 = state_0x8558_0;
                                state_0x8560_1 = state_0x8560_0;
                                var_193 = rdx3_2 + 2UL;
                                *(uint32_t *)(rdi1_2 + 4UL) = 0U;
                                if (var_171 <= var_192 & var_171 > var_193) {
                                    *(uint32_t *)(rdi1_2 + 8UL) = 0U;
                                }
                            }
                        }
                    }
                    state_0x8558_2 = state_0x8558_1;
                    state_0x8560_2 = state_0x8560_1;
                    rax_3 = (var_171 << 2UL) + var_170;
                }
                var_194 = (var_80 == 0U);
                var_195 = *var_168;
                var_196 = (var_195 == 0UL);
                state_0x8560_4 = state_0x8560_2;
                rax_4 = rax_3;
                state_0x8560_3 = state_0x8560_2;
                state_0x8558_4 = state_0x8558_2;
                rcx2_0 = rax_3;
                state_0x8558_3 = state_0x8558_2;
                if (var_194) {
                    if (!var_196) {
                        var_202 = rax_3 + 16UL;
                        var_203 = var_172 - var_202;
                        var_204 = var_172 + 16UL;
                        var_205 = helper_cc_compute_c_wrapper(var_203, var_202, var_6, 17U);
                        var_206 = (unsigned char)var_205 ^ '\x01';
                        var_207 = helper_cc_compute_c_wrapper(rax_3 - var_204, var_204, var_6, 17U);
                        if ((uint64_t)(var_206 | ((unsigned char)var_207 ^ '\x01')) == 0UL) {
                            var_208 = *var_168;
                            rdi1_3 = var_208;
                            if (var_208 > 12UL) {
                                var_209 = (0UL - (var_172 >> 2UL)) & 3UL;
                                var_210 = (var_209 > var_208) ? var_208 : var_209;
                                var_211 = *(uint32_t *)var_172;
                                var_212 = rax_3 + 4UL;
                                var_213 = var_172 + 4UL;
                                *(uint32_t *)rax_3 = var_211;
                                rdi1_3 = var_208 + (-1L);
                                r9_0 = var_213;
                                rcx2_0 = var_212;
                                var_214 = *(uint32_t *)var_213;
                                var_215 = rax_3 + 8UL;
                                var_216 = var_172 + 8UL;
                                *(uint32_t *)var_212 = var_214;
                                rdi1_3 = var_208 + (-2L);
                                r9_0 = var_216;
                                rcx2_0 = var_215;
                                if (var_210 != 0UL & var_210 != 1UL & var_210 == 3UL) {
                                    var_217 = rax_3 + 12UL;
                                    var_218 = var_172 + 12UL;
                                    *(uint32_t *)var_215 = *(uint32_t *)var_216;
                                    rdi1_3 = var_208 + (-3L);
                                    r9_0 = var_218;
                                    rcx2_0 = var_217;
                                }
                                var_219 = *var_168 - var_210;
                                var_220 = var_210 << 2UL;
                                var_221 = var_172 + var_220;
                                var_222 = var_220 + rax_3;
                                var_223 = (var_219 + (-4L)) >> 2UL;
                                var_224 = var_223 + 1UL;
                                var_225 = var_224 << 2UL;
                                var_226 = rdx3_3 + var_221;
                                var_227 = *(uint64_t *)var_226;
                                var_228 = *(uint64_t *)(var_226 + 8UL);
                                var_229 = r11_0 + 1UL;
                                var_230 = rdx3_3 + var_222;
                                *(uint64_t *)var_230 = var_227;
                                *(uint64_t *)(var_230 + 8UL) = var_228;
                                r11_0 = var_229;
                                state_0x8558_3 = var_227;
                                state_0x8560_3 = var_228;
                                while (var_223 >= var_229)
                                    {
                                        rdx3_3 = rdx3_3 + 16UL;
                                        var_226 = rdx3_3 + var_221;
                                        var_227 = *(uint64_t *)var_226;
                                        var_228 = *(uint64_t *)(var_226 + 8UL);
                                        var_229 = r11_0 + 1UL;
                                        var_230 = rdx3_3 + var_222;
                                        *(uint64_t *)var_230 = var_227;
                                        *(uint64_t *)(var_230 + 8UL) = var_228;
                                        r11_0 = var_229;
                                        state_0x8558_3 = var_227;
                                        state_0x8560_3 = var_228;
                                    }
                                var_231 = var_224 << 4UL;
                                var_232 = rdi1_3 - var_225;
                                var_233 = rcx2_0 + var_231;
                                var_234 = r9_0 + var_231;
                                *(uint32_t *)var_233 = *(uint32_t *)var_234;
                                *(uint32_t *)(var_233 + 4UL) = *(uint32_t *)(var_234 + 4UL);
                                if (var_225 != var_219 & var_232 != 1UL & var_232 == 2UL) {
                                    *(uint32_t *)(var_233 + 8UL) = *(uint32_t *)(var_234 + 8UL);
                                }
                            } else {
                                var_235 = rdx3_4 << 2UL;
                                *(uint32_t *)(var_235 + rax_3) = *(uint32_t *)(var_235 + var_172);
                                var_236 = rdx3_4 + 1UL;
                                rdx3_4 = var_236;
                                do {
                                    var_235 = rdx3_4 << 2UL;
                                    *(uint32_t *)(var_235 + rax_3) = *(uint32_t *)(var_235 + var_172);
                                    var_236 = rdx3_4 + 1UL;
                                    rdx3_4 = var_236;
                                } while (var_236 != *var_168);
                            }
                        } else {
                            var_235 = rdx3_4 << 2UL;
                            *(uint32_t *)(var_235 + rax_3) = *(uint32_t *)(var_235 + var_172);
                            var_236 = rdx3_4 + 1UL;
                            rdx3_4 = var_236;
                            do {
                                var_235 = rdx3_4 << 2UL;
                                *(uint32_t *)(var_235 + rax_3) = *(uint32_t *)(var_235 + var_172);
                                var_236 = rdx3_4 + 1UL;
                                rdx3_4 = var_236;
                            } while (var_236 != *var_168);
                        }
                        state_0x8558_4 = state_0x8558_3;
                        state_0x8560_4 = state_0x8560_3;
                        rax_4 = (*var_168 << 2UL) + rax_3;
                    }
                } else {
                    if (!var_196) {
                        var_197 = r84_0 << 2UL;
                        var_198 = rdx3_5 + ((uint64_t)*(uint32_t *)(var_197 + var_172) << var_81);
                        *(uint32_t *)(var_197 + rax_3) = (uint32_t)var_198;
                        var_199 = r84_0 + 1UL;
                        var_200 = var_198 >> 32UL;
                        rdx3_5 = var_200;
                        r84_0 = var_199;
                        do {
                            var_197 = r84_0 << 2UL;
                            var_198 = rdx3_5 + ((uint64_t)*(uint32_t *)(var_197 + var_172) << var_81);
                            *(uint32_t *)(var_197 + rax_3) = (uint32_t)var_198;
                            var_199 = r84_0 + 1UL;
                            var_200 = var_198 >> 32UL;
                            rdx3_5 = var_200;
                            r84_0 = var_199;
                        } while (var_199 != var_195);
                        var_201 = (var_195 << 2UL) + rax_3;
                        rax_4 = var_201;
                        if (var_200 == 0UL) {
                            *(uint32_t *)var_201 = (uint32_t)var_200;
                            rax_4 = var_201 + 4UL;
                        }
                    }
                }
                var_237 = (uint64_t)((long)(rax_4 - var_170) >> (long)2UL);
                *(uint64_t *)(var_0 + (-160L)) = 4228953UL;
                var_238 = indirect_placeholder_4(var_237, var_59, r14_2, var_65, var_170);
                var_239 = var_0 + (-168L);
                *(uint64_t *)var_239 = 4228964UL;
                indirect_placeholder();
                rbp_0 = var_238;
                state_0x8558_6 = state_0x8558_4;
                state_0x8560_5 = state_0x8560_4;
                local_sp_0 = var_239;
            } else {
                if (var_80 != 0U) {
                    var_108 = (uint32_t *)rdi1_7;
                    var_109 = rax_12 + ((uint64_t)*var_108 << var_81);
                    *var_108 = (uint32_t)var_109;
                    var_110 = var_109 >> 32UL;
                    var_111 = r9_2 + (-1L);
                    r9_2 = var_111;
                    rax_12 = var_110;
                    while (var_111 != 0UL)
                        {
                            rdi1_7 = rdi1_7 + 4UL;
                            var_108 = (uint32_t *)rdi1_7;
                            var_109 = rax_12 + ((uint64_t)*var_108 << var_81);
                            *var_108 = (uint32_t)var_109;
                            var_110 = var_109 >> 32UL;
                            var_111 = r9_2 + (-1L);
                            r9_2 = var_111;
                            rax_12 = var_110;
                        }
                    if (var_110 == 0UL) {
                        *(uint32_t *)((r14_2 << 2UL) + var_59) = (uint32_t)var_110;
                        r14_3 = r14_2 + 1UL;
                    }
                }
                rax_13 = r14_3;
                rax_14 = r14_3;
                r14_4 = r14_3;
                if (var_47 != 0UL) {
                    if (r14_3 != 0UL) {
                        var_112 = var_47 + r14_3;
                        var_113 = r14_3 << 2UL;
                        var_114 = var_112 << 2UL;
                        var_115 = var_113 + (-16L);
                        if ((((long)var_115 >= (long)var_114) || ((long)(var_114 + (-16L)) >= (long)var_113)) && (r14_3 > 12UL)) {
                            var_119 = ((var_115 + var_59) >> 2UL) & 3UL;
                            var_120 = (var_119 > r14_3) ? r14_3 : var_119;
                            var_121 = r14_3 + (-1L);
                            *(uint32_t *)(((var_47 + var_121) << 2UL) + var_59) = *(uint32_t *)((var_121 << 2UL) + var_59);
                            rax_13 = var_121;
                            var_122 = r14_3 + (-2L);
                            *(uint32_t *)(((var_47 + var_122) << 2UL) + var_59) = *(uint32_t *)((var_122 << 2UL) + var_59);
                            rax_13 = var_122;
                            if (var_120 != 0UL & var_120 != 1UL & var_120 == 3UL) {
                                var_123 = r14_3 + (-3L);
                                *(uint32_t *)(((var_47 + var_123) << 2UL) + var_59) = *(uint32_t *)((var_123 << 2UL) + var_59);
                                rax_13 = var_123;
                            }
                            var_124 = r14_3 - var_120;
                            var_125 = var_124 + (-4L);
                            var_126 = (var_120 << 2UL) ^ (-4L);
                            var_127 = var_125 >> 2UL;
                            var_128 = ((var_126 + var_113) + (-12L)) + var_59;
                            var_129 = ((var_126 + var_114) + (-12L)) + var_59;
                            var_130 = var_124 & (-4L);
                            var_131 = rdx3_9 + var_128;
                            var_132 = *(uint64_t *)var_131;
                            var_133 = *(uint64_t *)(var_131 + 8UL);
                            var_134 = rdi1_8 + 1UL;
                            var_135 = rdx3_9 + var_129;
                            *(uint64_t *)var_135 = var_132;
                            *(uint64_t *)(var_135 + 8UL) = var_133;
                            rdi1_8 = var_134;
                            state_0x8558_10 = var_132;
                            state_0x8560_9 = var_133;
                            while (var_127 >= var_134)
                                {
                                    rdx3_9 = rdx3_9 + (-16L);
                                    var_131 = rdx3_9 + var_128;
                                    var_132 = *(uint64_t *)var_131;
                                    var_133 = *(uint64_t *)(var_131 + 8UL);
                                    var_134 = rdi1_8 + 1UL;
                                    var_135 = rdx3_9 + var_129;
                                    *(uint64_t *)var_135 = var_132;
                                    *(uint64_t *)(var_135 + 8UL) = var_133;
                                    rdi1_8 = var_134;
                                    state_0x8558_10 = var_132;
                                    state_0x8560_9 = var_133;
                                }
                            var_136 = rax_13 - var_130;
                            var_137 = var_136 + (-1L);
                            *(uint32_t *)(((var_47 + var_137) << 2UL) + var_59) = *(uint32_t *)((var_137 << 2UL) + var_59);
                            var_138 = var_136 + (-2L);
                            *(uint32_t *)(((var_47 + var_138) << 2UL) + var_59) = *(uint32_t *)((var_138 << 2UL) + var_59);
                            if (var_130 != var_124 & var_137 != 0UL & var_138 == 0UL) {
                                var_139 = var_136 + (-3L);
                                *(uint32_t *)(((var_139 + var_47) << 2UL) + var_59) = *(uint32_t *)((var_139 << 2UL) + var_59);
                            }
                        } else {
                            var_116 = (var_47 << 2UL) + var_59;
                            var_117 = rax_14 + (-1L);
                            var_118 = var_117 << 2UL;
                            *(uint32_t *)(var_118 + var_116) = *(uint32_t *)(var_118 + var_59);
                            rax_14 = var_117;
                            do {
                                var_117 = rax_14 + (-1L);
                                var_118 = var_117 << 2UL;
                                *(uint32_t *)(var_118 + var_116) = *(uint32_t *)(var_118 + var_59);
                                rax_14 = var_117;
                            } while (var_117 != 0UL);
                        }
                    }
                    var_140 = var_47 << 2UL;
                    var_141 = ((var_140 + var_59) >> 2UL) & 3UL;
                    var_142 = (var_141 > var_47) ? var_47 : var_141;
                    state_0x8558_11 = state_0x8558_10;
                    state_0x8560_10 = state_0x8560_9;
                    state_0x8558_12 = state_0x8558_10;
                    state_0x8560_11 = state_0x8560_9;
                    if (var_47 > 6UL) {
                        var_143 = var_47 + (-1L);
                        _pre_phi = var_143;
                        rax_15 = var_142;
                        rdx3_10 = var_143;
                        if (var_142 == 0UL) {
                            *(uint32_t *)((_pre_phi << 2UL) + var_59) = 0U;
                            rcx2_8 = _pre_phi;
                            rdx3_10 = _pre_phi;
                            rax_16 = rax_15;
                            var_144 = var_47 + (-2L);
                            *(uint32_t *)((var_144 << 2UL) + var_59) = 0U;
                            rcx2_8 = var_144;
                            var_145 = var_47 + (-3L);
                            *(uint32_t *)((var_145 << 2UL) + var_59) = 0U;
                            rcx2_8 = var_145;
                            var_146 = var_47 + (-4L);
                            *(uint32_t *)((var_146 << 2UL) + var_59) = 0U;
                            rcx2_8 = var_146;
                            var_147 = var_47 + (-5L);
                            *(uint32_t *)((var_147 << 2UL) + var_59) = 0U;
                            rcx2_8 = var_147;
                            if (rax_15 != 1UL & rax_15 != 2UL & rax_15 != 3UL & rax_15 != 4UL & rax_15 == 6UL) {
                                var_148 = var_47 + (-6L);
                                *(uint32_t *)((var_148 << 2UL) + var_59) = 0U;
                                rcx2_8 = var_148;
                            }
                            rcx2_9 = rcx2_8;
                            if (rax_15 != var_47) {
                                var_149 = rdx3_10 - rax_16;
                                var_150 = var_47 - rax_16;
                                var_151 = (var_150 + (-4L)) >> 2UL;
                                var_152 = var_150 & (-4L);
                                rcx2_10 = rcx2_9;
                                if (var_149 > 2UL) {
                                    var_153 = helper_pxor_xmm_wrapper_83((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), state_0x8558_10, state_0x8560_9);
                                    var_154 = var_153.field_0;
                                    var_155 = var_153.field_1;
                                    state_0x8558_12 = var_154;
                                    rax_17 = ((((rax_16 << 2UL) ^ (-4L)) + var_140) + (-12L)) + var_59;
                                    state_0x8558_11 = var_154;
                                    state_0x8560_10 = var_155;
                                    state_0x8560_11 = var_155;
                                    var_156 = rdx3_11 + 1UL;
                                    *(uint64_t *)rax_17 = var_154;
                                    *(uint64_t *)(rax_17 + 8UL) = var_155;
                                    rdx3_11 = var_156;
                                    while (var_151 >= var_156)
                                        {
                                            rax_17 = rax_17 + (-16L);
                                            var_156 = rdx3_11 + 1UL;
                                            *(uint64_t *)rax_17 = var_154;
                                            *(uint64_t *)(rax_17 + 8UL) = var_155;
                                            rdx3_11 = var_156;
                                        }
                                    var_157 = rcx2_9 - var_152;
                                    rcx2_10 = var_157;
                                    var_158 = rcx2_10 + (-1L);
                                    *(uint32_t *)((var_158 << 2UL) + var_59) = 0U;
                                    state_0x8558_12 = state_0x8558_11;
                                    state_0x8560_11 = state_0x8560_10;
                                    var_159 = rcx2_10 + (-2L);
                                    *(uint32_t *)((var_159 << 2UL) + var_59) = 0U;
                                    if (var_152 != var_150 & var_158 != 0UL & var_159 == 0UL) {
                                        *(uint32_t *)(((rcx2_10 << 2UL) + var_59) + (-12L)) = 0U;
                                    }
                                } else {
                                    var_158 = rcx2_10 + (-1L);
                                    *(uint32_t *)((var_158 << 2UL) + var_59) = 0U;
                                    state_0x8558_12 = state_0x8558_11;
                                    state_0x8560_11 = state_0x8560_10;
                                    var_159 = rcx2_10 + (-2L);
                                    *(uint32_t *)((var_159 << 2UL) + var_59) = 0U;
                                    if (var_158 != 0UL & var_159 == 0UL) {
                                        *(uint32_t *)(((rcx2_10 << 2UL) + var_59) + (-12L)) = 0U;
                                    }
                                }
                            }
                        } else {
                            var_149 = rdx3_10 - rax_16;
                            var_150 = var_47 - rax_16;
                            var_151 = (var_150 + (-4L)) >> 2UL;
                            var_152 = var_150 & (-4L);
                            rcx2_10 = rcx2_9;
                            if (var_149 > 2UL) {
                                var_153 = helper_pxor_xmm_wrapper_83((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), state_0x8558_10, state_0x8560_9);
                                var_154 = var_153.field_0;
                                var_155 = var_153.field_1;
                                state_0x8558_12 = var_154;
                                rax_17 = ((((rax_16 << 2UL) ^ (-4L)) + var_140) + (-12L)) + var_59;
                                state_0x8558_11 = var_154;
                                state_0x8560_10 = var_155;
                                state_0x8560_11 = var_155;
                                var_156 = rdx3_11 + 1UL;
                                *(uint64_t *)rax_17 = var_154;
                                *(uint64_t *)(rax_17 + 8UL) = var_155;
                                rdx3_11 = var_156;
                                while (var_151 >= var_156)
                                    {
                                        rax_17 = rax_17 + (-16L);
                                        var_156 = rdx3_11 + 1UL;
                                        *(uint64_t *)rax_17 = var_154;
                                        *(uint64_t *)(rax_17 + 8UL) = var_155;
                                        rdx3_11 = var_156;
                                    }
                                var_157 = rcx2_9 - var_152;
                                rcx2_10 = var_157;
                                var_158 = rcx2_10 + (-1L);
                                *(uint32_t *)((var_158 << 2UL) + var_59) = 0U;
                                state_0x8558_12 = state_0x8558_11;
                                state_0x8560_11 = state_0x8560_10;
                                var_159 = rcx2_10 + (-2L);
                                *(uint32_t *)((var_159 << 2UL) + var_59) = 0U;
                                if (var_152 != var_150 & var_158 != 0UL & var_159 == 0UL) {
                                    *(uint32_t *)(((rcx2_10 << 2UL) + var_59) + (-12L)) = 0U;
                                }
                            } else {
                                var_158 = rcx2_10 + (-1L);
                                *(uint32_t *)((var_158 << 2UL) + var_59) = 0U;
                                state_0x8558_12 = state_0x8558_11;
                                state_0x8560_11 = state_0x8560_10;
                                var_159 = rcx2_10 + (-2L);
                                *(uint32_t *)((var_159 << 2UL) + var_59) = 0U;
                                if (var_158 != 0UL & var_159 == 0UL) {
                                    *(uint32_t *)(((rcx2_10 << 2UL) + var_59) + (-12L)) = 0U;
                                }
                            }
                        }
                    } else {
                        _pre = var_47 + (-1L);
                        _pre_phi = _pre;
                        *(uint32_t *)((_pre_phi << 2UL) + var_59) = 0U;
                        rcx2_8 = _pre_phi;
                        rdx3_10 = _pre_phi;
                        rax_16 = rax_15;
                        var_144 = var_47 + (-2L);
                        *(uint32_t *)((var_144 << 2UL) + var_59) = 0U;
                        rcx2_8 = var_144;
                        var_145 = var_47 + (-3L);
                        *(uint32_t *)((var_145 << 2UL) + var_59) = 0U;
                        rcx2_8 = var_145;
                        var_146 = var_47 + (-4L);
                        *(uint32_t *)((var_146 << 2UL) + var_59) = 0U;
                        rcx2_8 = var_146;
                        var_147 = var_47 + (-5L);
                        *(uint32_t *)((var_147 << 2UL) + var_59) = 0U;
                        rcx2_8 = var_147;
                        if (rax_15 != 1UL & rax_15 != 2UL & rax_15 != 3UL & rax_15 != 4UL & rax_15 == 6UL) {
                            var_148 = var_47 + (-6L);
                            *(uint32_t *)((var_148 << 2UL) + var_59) = 0U;
                            rcx2_8 = var_148;
                        }
                        rcx2_9 = rcx2_8;
                        if (rax_15 != var_47) {
                            var_149 = rdx3_10 - rax_16;
                            var_150 = var_47 - rax_16;
                            var_151 = (var_150 + (-4L)) >> 2UL;
                            var_152 = var_150 & (-4L);
                            rcx2_10 = rcx2_9;
                            if (var_149 > 2UL) {
                                var_153 = helper_pxor_xmm_wrapper_83((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), state_0x8558_10, state_0x8560_9);
                                var_154 = var_153.field_0;
                                var_155 = var_153.field_1;
                                state_0x8558_12 = var_154;
                                rax_17 = ((((rax_16 << 2UL) ^ (-4L)) + var_140) + (-12L)) + var_59;
                                state_0x8558_11 = var_154;
                                state_0x8560_10 = var_155;
                                state_0x8560_11 = var_155;
                                var_156 = rdx3_11 + 1UL;
                                *(uint64_t *)rax_17 = var_154;
                                *(uint64_t *)(rax_17 + 8UL) = var_155;
                                rdx3_11 = var_156;
                                while (var_151 >= var_156)
                                    {
                                        rax_17 = rax_17 + (-16L);
                                        var_156 = rdx3_11 + 1UL;
                                        *(uint64_t *)rax_17 = var_154;
                                        *(uint64_t *)(rax_17 + 8UL) = var_155;
                                        rdx3_11 = var_156;
                                    }
                                var_157 = rcx2_9 - var_152;
                                rcx2_10 = var_157;
                                var_158 = rcx2_10 + (-1L);
                                *(uint32_t *)((var_158 << 2UL) + var_59) = 0U;
                                state_0x8558_12 = state_0x8558_11;
                                state_0x8560_11 = state_0x8560_10;
                                var_159 = rcx2_10 + (-2L);
                                *(uint32_t *)((var_159 << 2UL) + var_59) = 0U;
                                if (var_152 != var_150 & var_158 != 0UL & var_159 == 0UL) {
                                    *(uint32_t *)(((rcx2_10 << 2UL) + var_59) + (-12L)) = 0U;
                                }
                            } else {
                                var_158 = rcx2_10 + (-1L);
                                *(uint32_t *)((var_158 << 2UL) + var_59) = 0U;
                                state_0x8558_12 = state_0x8558_11;
                                state_0x8560_11 = state_0x8560_10;
                                var_159 = rcx2_10 + (-2L);
                                *(uint32_t *)((var_159 << 2UL) + var_59) = 0U;
                                if (var_158 != 0UL & var_159 == 0UL) {
                                    *(uint32_t *)(((rcx2_10 << 2UL) + var_59) + (-12L)) = 0U;
                                }
                            }
                        }
                    }
                    state_0x8558_13 = state_0x8558_12;
                    state_0x8560_12 = state_0x8560_11;
                    r14_4 = r14_3 + var_47;
                }
                var_160 = *var_57;
                var_161 = var_0 + (-152L);
                var_162 = (uint64_t *)var_161;
                state_0x8558_6 = state_0x8558_13;
                state_0x8560_5 = state_0x8560_12;
                local_sp_0 = var_161;
                if (var_82) {
                    *var_162 = 4228209UL;
                    var_164 = indirect_placeholder_4(var_160, var_59, r14_4, var_30, var_66);
                    rbp_0 = var_164;
                } else {
                    *var_162 = 4227208UL;
                    var_163 = indirect_placeholder_4(var_160, var_59, r14_4, var_30, var_66);
                    rbp_0 = var_163;
                }
            }
        } else {
            if ((int)var_62 > (int)4294967295U) {
                var_83 = *var_57;
                var_84 = var_0 + (-80L);
                var_85 = var_0 + (-152L);
                *(uint64_t *)var_85 = 4226190UL;
                var_86 = indirect_placeholder_4(var_83, var_59, r14_2, var_84, var_66);
                local_sp_1 = var_85;
                if (var_86 != 0UL) {
                    var_315 = local_sp_1 + (-8L);
                    *(uint64_t *)var_315 = 4228225UL;
                    indirect_placeholder();
                    local_sp_2 = var_315;
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4228235UL;
                    indirect_placeholder();
                    return storemerge10;
                }
                var_87 = (r14_2 << 2UL) + var_59;
                if (var_47 != 0UL) {
                    var_88 = (0UL - (var_87 >> 2UL)) & 3UL;
                    var_89 = (var_47 > var_88) ? var_88 : var_47;
                    if (var_47 > 6UL) {
                        rax_7 = var_89;
                        if (var_89 == 0UL) {
                            *(uint32_t *)var_87 = 0U;
                            rax_8 = rax_7;
                            *(uint32_t *)(var_87 + 4UL) = 0U;
                            rcx2_4 = 2UL;
                            *(uint32_t *)(var_87 + 8UL) = 0U;
                            rcx2_4 = 3UL;
                            *(uint32_t *)(var_87 + 12UL) = 0U;
                            rcx2_4 = 4UL;
                            *(uint32_t *)(var_87 + 16UL) = 0U;
                            rcx2_4 = 5UL;
                            if (rax_7 != 1UL & rax_7 != 2UL & rax_7 != 3UL & rax_7 != 4UL & rax_7 == 6UL) {
                                *(uint32_t *)(var_87 + 20UL) = 0U;
                                rcx2_4 = 6UL;
                            }
                            rcx2_5 = rcx2_4;
                            if (var_47 != rax_7) {
                                var_90 = var_47 - rax_8;
                                var_91 = var_47 + (rax_8 ^ (-1L));
                                var_92 = (var_90 + (-4L)) >> 2UL;
                                var_93 = var_90 & (-4L);
                                rcx2_6 = rcx2_5;
                                if (var_91 > 2UL) {
                                    var_94 = r14_2 + rax_8;
                                    var_95 = helper_pxor_xmm_wrapper_83((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_51, var_28);
                                    var_96 = var_95.field_0;
                                    var_97 = var_95.field_1;
                                    state_0x8558_8 = var_96;
                                    r10_1 = (var_94 << 2UL) + var_59;
                                    state_0x8558_7 = var_96;
                                    state_0x8560_6 = var_97;
                                    state_0x8560_7 = var_97;
                                    var_98 = rax_9 + 1UL;
                                    *(uint64_t *)r10_1 = var_96;
                                    *(uint64_t *)(r10_1 + 8UL) = var_97;
                                    rax_9 = var_98;
                                    while (var_92 >= var_98)
                                        {
                                            r10_1 = r10_1 + 16UL;
                                            var_98 = rax_9 + 1UL;
                                            *(uint64_t *)r10_1 = var_96;
                                            *(uint64_t *)(r10_1 + 8UL) = var_97;
                                            rax_9 = var_98;
                                        }
                                    var_99 = rcx2_5 + var_93;
                                    rcx2_6 = var_99;
                                    var_100 = rcx2_6 + 1UL;
                                    *(uint32_t *)((rcx2_6 << 2UL) + var_87) = 0U;
                                    state_0x8558_8 = state_0x8558_7;
                                    state_0x8560_7 = state_0x8560_6;
                                    *(uint32_t *)((var_100 << 2UL) + var_87) = 0U;
                                    var_101 = rcx2_6 + 2UL;
                                    if (var_93 != var_90 & var_47 <= var_100 & var_47 > var_101) {
                                        *(uint32_t *)((var_101 << 2UL) + var_87) = 0U;
                                    }
                                } else {
                                    var_100 = rcx2_6 + 1UL;
                                    *(uint32_t *)((rcx2_6 << 2UL) + var_87) = 0U;
                                    state_0x8558_8 = state_0x8558_7;
                                    state_0x8560_7 = state_0x8560_6;
                                    *(uint32_t *)((var_100 << 2UL) + var_87) = 0U;
                                    var_101 = rcx2_6 + 2UL;
                                    if (var_47 <= var_100 & var_47 > var_101) {
                                        *(uint32_t *)((var_101 << 2UL) + var_87) = 0U;
                                    }
                                }
                            }
                        } else {
                            var_90 = var_47 - rax_8;
                            var_91 = var_47 + (rax_8 ^ (-1L));
                            var_92 = (var_90 + (-4L)) >> 2UL;
                            var_93 = var_90 & (-4L);
                            rcx2_6 = rcx2_5;
                            if (var_91 > 2UL) {
                                var_94 = r14_2 + rax_8;
                                var_95 = helper_pxor_xmm_wrapper_83((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_51, var_28);
                                var_96 = var_95.field_0;
                                var_97 = var_95.field_1;
                                state_0x8558_8 = var_96;
                                r10_1 = (var_94 << 2UL) + var_59;
                                state_0x8558_7 = var_96;
                                state_0x8560_6 = var_97;
                                state_0x8560_7 = var_97;
                                var_98 = rax_9 + 1UL;
                                *(uint64_t *)r10_1 = var_96;
                                *(uint64_t *)(r10_1 + 8UL) = var_97;
                                rax_9 = var_98;
                                while (var_92 >= var_98)
                                    {
                                        r10_1 = r10_1 + 16UL;
                                        var_98 = rax_9 + 1UL;
                                        *(uint64_t *)r10_1 = var_96;
                                        *(uint64_t *)(r10_1 + 8UL) = var_97;
                                        rax_9 = var_98;
                                    }
                                var_99 = rcx2_5 + var_93;
                                rcx2_6 = var_99;
                                var_100 = rcx2_6 + 1UL;
                                *(uint32_t *)((rcx2_6 << 2UL) + var_87) = 0U;
                                state_0x8558_8 = state_0x8558_7;
                                state_0x8560_7 = state_0x8560_6;
                                *(uint32_t *)((var_100 << 2UL) + var_87) = 0U;
                                var_101 = rcx2_6 + 2UL;
                                if (var_93 != var_90 & var_47 <= var_100 & var_47 > var_101) {
                                    *(uint32_t *)((var_101 << 2UL) + var_87) = 0U;
                                }
                            } else {
                                var_100 = rcx2_6 + 1UL;
                                *(uint32_t *)((rcx2_6 << 2UL) + var_87) = 0U;
                                state_0x8558_8 = state_0x8558_7;
                                state_0x8560_7 = state_0x8560_6;
                                *(uint32_t *)((var_100 << 2UL) + var_87) = 0U;
                                var_101 = rcx2_6 + 2UL;
                                if (var_47 <= var_100 & var_47 > var_101) {
                                    *(uint32_t *)((var_101 << 2UL) + var_87) = 0U;
                                }
                            }
                        }
                    } else {
                        *(uint32_t *)var_87 = 0U;
                        rax_8 = rax_7;
                        *(uint32_t *)(var_87 + 4UL) = 0U;
                        rcx2_4 = 2UL;
                        *(uint32_t *)(var_87 + 8UL) = 0U;
                        rcx2_4 = 3UL;
                        *(uint32_t *)(var_87 + 12UL) = 0U;
                        rcx2_4 = 4UL;
                        *(uint32_t *)(var_87 + 16UL) = 0U;
                        rcx2_4 = 5UL;
                        if (rax_7 != 1UL & rax_7 != 2UL & rax_7 != 3UL & rax_7 != 4UL & rax_7 == 6UL) {
                            *(uint32_t *)(var_87 + 20UL) = 0U;
                            rcx2_4 = 6UL;
                        }
                        rcx2_5 = rcx2_4;
                        if (var_47 != rax_7) {
                            var_90 = var_47 - rax_8;
                            var_91 = var_47 + (rax_8 ^ (-1L));
                            var_92 = (var_90 + (-4L)) >> 2UL;
                            var_93 = var_90 & (-4L);
                            rcx2_6 = rcx2_5;
                            if (var_91 > 2UL) {
                                var_94 = r14_2 + rax_8;
                                var_95 = helper_pxor_xmm_wrapper_83((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_51, var_28);
                                var_96 = var_95.field_0;
                                var_97 = var_95.field_1;
                                state_0x8558_8 = var_96;
                                r10_1 = (var_94 << 2UL) + var_59;
                                state_0x8558_7 = var_96;
                                state_0x8560_6 = var_97;
                                state_0x8560_7 = var_97;
                                var_98 = rax_9 + 1UL;
                                *(uint64_t *)r10_1 = var_96;
                                *(uint64_t *)(r10_1 + 8UL) = var_97;
                                rax_9 = var_98;
                                while (var_92 >= var_98)
                                    {
                                        r10_1 = r10_1 + 16UL;
                                        var_98 = rax_9 + 1UL;
                                        *(uint64_t *)r10_1 = var_96;
                                        *(uint64_t *)(r10_1 + 8UL) = var_97;
                                        rax_9 = var_98;
                                    }
                                var_99 = rcx2_5 + var_93;
                                rcx2_6 = var_99;
                                var_100 = rcx2_6 + 1UL;
                                *(uint32_t *)((rcx2_6 << 2UL) + var_87) = 0U;
                                state_0x8558_8 = state_0x8558_7;
                                state_0x8560_7 = state_0x8560_6;
                                *(uint32_t *)((var_100 << 2UL) + var_87) = 0U;
                                var_101 = rcx2_6 + 2UL;
                                if (var_93 != var_90 & var_47 <= var_100 & var_47 > var_101) {
                                    *(uint32_t *)((var_101 << 2UL) + var_87) = 0U;
                                }
                            } else {
                                var_100 = rcx2_6 + 1UL;
                                *(uint32_t *)((rcx2_6 << 2UL) + var_87) = 0U;
                                state_0x8558_8 = state_0x8558_7;
                                state_0x8560_7 = state_0x8560_6;
                                *(uint32_t *)((var_100 << 2UL) + var_87) = 0U;
                                var_101 = rcx2_6 + 2UL;
                                if (var_47 <= var_100 & var_47 > var_101) {
                                    *(uint32_t *)((var_101 << 2UL) + var_87) = 0U;
                                }
                            }
                        }
                    }
                    state_0x8558_9 = state_0x8558_8;
                    state_0x8560_8 = state_0x8560_7;
                    storemerge = var_47 << 2UL;
                }
                var_102 = *(uint64_t *)(var_0 + (-88L));
                *(uint32_t *)(storemerge + var_87) = (uint32_t)(1UL << var_81);
                var_103 = *(uint64_t *)var_84;
                var_104 = var_47 + 1UL;
                *(uint64_t *)(var_0 + (-160L)) = 4226519UL;
                var_105 = indirect_placeholder_4(var_102, var_87, var_104, var_65, var_103);
                var_106 = var_0 + (-168L);
                *(uint64_t *)var_106 = 4226530UL;
                indirect_placeholder();
                rbp_0 = var_105;
                state_0x8558_6 = state_0x8558_9;
                state_0x8560_5 = state_0x8560_8;
                local_sp_0 = var_106;
            } else {
                if (var_80 != 0U) {
                    var_108 = (uint32_t *)rdi1_7;
                    var_109 = rax_12 + ((uint64_t)*var_108 << var_81);
                    *var_108 = (uint32_t)var_109;
                    var_110 = var_109 >> 32UL;
                    var_111 = r9_2 + (-1L);
                    r9_2 = var_111;
                    rax_12 = var_110;
                    while (var_111 != 0UL)
                        {
                            rdi1_7 = rdi1_7 + 4UL;
                            var_108 = (uint32_t *)rdi1_7;
                            var_109 = rax_12 + ((uint64_t)*var_108 << var_81);
                            *var_108 = (uint32_t)var_109;
                            var_110 = var_109 >> 32UL;
                            var_111 = r9_2 + (-1L);
                            r9_2 = var_111;
                            rax_12 = var_110;
                        }
                    if (var_110 == 0UL) {
                        *(uint32_t *)((r14_2 << 2UL) + var_59) = (uint32_t)var_110;
                        r14_3 = r14_2 + 1UL;
                    }
                }
                rax_13 = r14_3;
                rax_14 = r14_3;
                r14_4 = r14_3;
                if (var_47 != 0UL) {
                    if (r14_3 != 0UL) {
                        var_112 = var_47 + r14_3;
                        var_113 = r14_3 << 2UL;
                        var_114 = var_112 << 2UL;
                        var_115 = var_113 + (-16L);
                        if ((((long)var_115 >= (long)var_114) || ((long)(var_114 + (-16L)) >= (long)var_113)) && (r14_3 > 12UL)) {
                            var_116 = (var_47 << 2UL) + var_59;
                            var_117 = rax_14 + (-1L);
                            var_118 = var_117 << 2UL;
                            *(uint32_t *)(var_118 + var_116) = *(uint32_t *)(var_118 + var_59);
                            rax_14 = var_117;
                            do {
                                var_117 = rax_14 + (-1L);
                                var_118 = var_117 << 2UL;
                                *(uint32_t *)(var_118 + var_116) = *(uint32_t *)(var_118 + var_59);
                                rax_14 = var_117;
                            } while (var_117 != 0UL);
                        } else {
                            var_119 = ((var_115 + var_59) >> 2UL) & 3UL;
                            var_120 = (var_119 > r14_3) ? r14_3 : var_119;
                            var_121 = r14_3 + (-1L);
                            *(uint32_t *)(((var_47 + var_121) << 2UL) + var_59) = *(uint32_t *)((var_121 << 2UL) + var_59);
                            rax_13 = var_121;
                            var_122 = r14_3 + (-2L);
                            *(uint32_t *)(((var_47 + var_122) << 2UL) + var_59) = *(uint32_t *)((var_122 << 2UL) + var_59);
                            rax_13 = var_122;
                            if (var_120 != 0UL & var_120 != 1UL & var_120 == 3UL) {
                                var_123 = r14_3 + (-3L);
                                *(uint32_t *)(((var_47 + var_123) << 2UL) + var_59) = *(uint32_t *)((var_123 << 2UL) + var_59);
                                rax_13 = var_123;
                            }
                            var_124 = r14_3 - var_120;
                            var_125 = var_124 + (-4L);
                            var_126 = (var_120 << 2UL) ^ (-4L);
                            var_127 = var_125 >> 2UL;
                            var_128 = ((var_126 + var_113) + (-12L)) + var_59;
                            var_129 = ((var_126 + var_114) + (-12L)) + var_59;
                            var_130 = var_124 & (-4L);
                            var_131 = rdx3_9 + var_128;
                            var_132 = *(uint64_t *)var_131;
                            var_133 = *(uint64_t *)(var_131 + 8UL);
                            var_134 = rdi1_8 + 1UL;
                            var_135 = rdx3_9 + var_129;
                            *(uint64_t *)var_135 = var_132;
                            *(uint64_t *)(var_135 + 8UL) = var_133;
                            rdi1_8 = var_134;
                            state_0x8558_10 = var_132;
                            state_0x8560_9 = var_133;
                            while (var_127 >= var_134)
                                {
                                    rdx3_9 = rdx3_9 + (-16L);
                                    var_131 = rdx3_9 + var_128;
                                    var_132 = *(uint64_t *)var_131;
                                    var_133 = *(uint64_t *)(var_131 + 8UL);
                                    var_134 = rdi1_8 + 1UL;
                                    var_135 = rdx3_9 + var_129;
                                    *(uint64_t *)var_135 = var_132;
                                    *(uint64_t *)(var_135 + 8UL) = var_133;
                                    rdi1_8 = var_134;
                                    state_0x8558_10 = var_132;
                                    state_0x8560_9 = var_133;
                                }
                            var_136 = rax_13 - var_130;
                            var_137 = var_136 + (-1L);
                            *(uint32_t *)(((var_47 + var_137) << 2UL) + var_59) = *(uint32_t *)((var_137 << 2UL) + var_59);
                            var_138 = var_136 + (-2L);
                            *(uint32_t *)(((var_47 + var_138) << 2UL) + var_59) = *(uint32_t *)((var_138 << 2UL) + var_59);
                            if (var_130 != var_124 & var_137 != 0UL & var_138 == 0UL) {
                                var_139 = var_136 + (-3L);
                                *(uint32_t *)(((var_139 + var_47) << 2UL) + var_59) = *(uint32_t *)((var_139 << 2UL) + var_59);
                            }
                        }
                    }
                    var_140 = var_47 << 2UL;
                    var_141 = ((var_140 + var_59) >> 2UL) & 3UL;
                    var_142 = (var_141 > var_47) ? var_47 : var_141;
                    state_0x8558_11 = state_0x8558_10;
                    state_0x8560_10 = state_0x8560_9;
                    state_0x8558_12 = state_0x8558_10;
                    state_0x8560_11 = state_0x8560_9;
                    if (var_47 > 6UL) {
                        _pre = var_47 + (-1L);
                        _pre_phi = _pre;
                        *(uint32_t *)((_pre_phi << 2UL) + var_59) = 0U;
                        rcx2_8 = _pre_phi;
                        rdx3_10 = _pre_phi;
                        rax_16 = rax_15;
                        var_144 = var_47 + (-2L);
                        *(uint32_t *)((var_144 << 2UL) + var_59) = 0U;
                        rcx2_8 = var_144;
                        var_145 = var_47 + (-3L);
                        *(uint32_t *)((var_145 << 2UL) + var_59) = 0U;
                        rcx2_8 = var_145;
                        var_146 = var_47 + (-4L);
                        *(uint32_t *)((var_146 << 2UL) + var_59) = 0U;
                        rcx2_8 = var_146;
                        var_147 = var_47 + (-5L);
                        *(uint32_t *)((var_147 << 2UL) + var_59) = 0U;
                        rcx2_8 = var_147;
                        if (rax_15 != 1UL & rax_15 != 2UL & rax_15 != 3UL & rax_15 != 4UL & rax_15 == 6UL) {
                            var_148 = var_47 + (-6L);
                            *(uint32_t *)((var_148 << 2UL) + var_59) = 0U;
                            rcx2_8 = var_148;
                        }
                        rcx2_9 = rcx2_8;
                        if (rax_15 != var_47) {
                            var_149 = rdx3_10 - rax_16;
                            var_150 = var_47 - rax_16;
                            var_151 = (var_150 + (-4L)) >> 2UL;
                            var_152 = var_150 & (-4L);
                            rcx2_10 = rcx2_9;
                            if (var_149 > 2UL) {
                                var_153 = helper_pxor_xmm_wrapper_83((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), state_0x8558_10, state_0x8560_9);
                                var_154 = var_153.field_0;
                                var_155 = var_153.field_1;
                                state_0x8558_12 = var_154;
                                rax_17 = ((((rax_16 << 2UL) ^ (-4L)) + var_140) + (-12L)) + var_59;
                                state_0x8558_11 = var_154;
                                state_0x8560_10 = var_155;
                                state_0x8560_11 = var_155;
                                var_156 = rdx3_11 + 1UL;
                                *(uint64_t *)rax_17 = var_154;
                                *(uint64_t *)(rax_17 + 8UL) = var_155;
                                rdx3_11 = var_156;
                                while (var_151 >= var_156)
                                    {
                                        rax_17 = rax_17 + (-16L);
                                        var_156 = rdx3_11 + 1UL;
                                        *(uint64_t *)rax_17 = var_154;
                                        *(uint64_t *)(rax_17 + 8UL) = var_155;
                                        rdx3_11 = var_156;
                                    }
                                var_157 = rcx2_9 - var_152;
                                rcx2_10 = var_157;
                                var_158 = rcx2_10 + (-1L);
                                *(uint32_t *)((var_158 << 2UL) + var_59) = 0U;
                                state_0x8558_12 = state_0x8558_11;
                                state_0x8560_11 = state_0x8560_10;
                                var_159 = rcx2_10 + (-2L);
                                *(uint32_t *)((var_159 << 2UL) + var_59) = 0U;
                                if (var_152 != var_150 & var_158 != 0UL & var_159 == 0UL) {
                                    *(uint32_t *)(((rcx2_10 << 2UL) + var_59) + (-12L)) = 0U;
                                }
                            } else {
                                var_158 = rcx2_10 + (-1L);
                                *(uint32_t *)((var_158 << 2UL) + var_59) = 0U;
                                state_0x8558_12 = state_0x8558_11;
                                state_0x8560_11 = state_0x8560_10;
                                var_159 = rcx2_10 + (-2L);
                                *(uint32_t *)((var_159 << 2UL) + var_59) = 0U;
                                if (var_158 != 0UL & var_159 == 0UL) {
                                    *(uint32_t *)(((rcx2_10 << 2UL) + var_59) + (-12L)) = 0U;
                                }
                            }
                        }
                    } else {
                        var_143 = var_47 + (-1L);
                        _pre_phi = var_143;
                        rax_15 = var_142;
                        rdx3_10 = var_143;
                        if (var_142 == 0UL) {
                            *(uint32_t *)((_pre_phi << 2UL) + var_59) = 0U;
                            rcx2_8 = _pre_phi;
                            rdx3_10 = _pre_phi;
                            rax_16 = rax_15;
                            var_144 = var_47 + (-2L);
                            *(uint32_t *)((var_144 << 2UL) + var_59) = 0U;
                            rcx2_8 = var_144;
                            var_145 = var_47 + (-3L);
                            *(uint32_t *)((var_145 << 2UL) + var_59) = 0U;
                            rcx2_8 = var_145;
                            var_146 = var_47 + (-4L);
                            *(uint32_t *)((var_146 << 2UL) + var_59) = 0U;
                            rcx2_8 = var_146;
                            var_147 = var_47 + (-5L);
                            *(uint32_t *)((var_147 << 2UL) + var_59) = 0U;
                            rcx2_8 = var_147;
                            if (rax_15 != 1UL & rax_15 != 2UL & rax_15 != 3UL & rax_15 != 4UL & rax_15 == 6UL) {
                                var_148 = var_47 + (-6L);
                                *(uint32_t *)((var_148 << 2UL) + var_59) = 0U;
                                rcx2_8 = var_148;
                            }
                            rcx2_9 = rcx2_8;
                            if (rax_15 != var_47) {
                                var_149 = rdx3_10 - rax_16;
                                var_150 = var_47 - rax_16;
                                var_151 = (var_150 + (-4L)) >> 2UL;
                                var_152 = var_150 & (-4L);
                                rcx2_10 = rcx2_9;
                                if (var_149 > 2UL) {
                                    var_153 = helper_pxor_xmm_wrapper_83((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), state_0x8558_10, state_0x8560_9);
                                    var_154 = var_153.field_0;
                                    var_155 = var_153.field_1;
                                    state_0x8558_12 = var_154;
                                    rax_17 = ((((rax_16 << 2UL) ^ (-4L)) + var_140) + (-12L)) + var_59;
                                    state_0x8558_11 = var_154;
                                    state_0x8560_10 = var_155;
                                    state_0x8560_11 = var_155;
                                    var_156 = rdx3_11 + 1UL;
                                    *(uint64_t *)rax_17 = var_154;
                                    *(uint64_t *)(rax_17 + 8UL) = var_155;
                                    rdx3_11 = var_156;
                                    while (var_151 >= var_156)
                                        {
                                            rax_17 = rax_17 + (-16L);
                                            var_156 = rdx3_11 + 1UL;
                                            *(uint64_t *)rax_17 = var_154;
                                            *(uint64_t *)(rax_17 + 8UL) = var_155;
                                            rdx3_11 = var_156;
                                        }
                                    var_157 = rcx2_9 - var_152;
                                    rcx2_10 = var_157;
                                    var_158 = rcx2_10 + (-1L);
                                    *(uint32_t *)((var_158 << 2UL) + var_59) = 0U;
                                    state_0x8558_12 = state_0x8558_11;
                                    state_0x8560_11 = state_0x8560_10;
                                    var_159 = rcx2_10 + (-2L);
                                    *(uint32_t *)((var_159 << 2UL) + var_59) = 0U;
                                    if (var_152 != var_150 & var_158 != 0UL & var_159 == 0UL) {
                                        *(uint32_t *)(((rcx2_10 << 2UL) + var_59) + (-12L)) = 0U;
                                    }
                                } else {
                                    var_158 = rcx2_10 + (-1L);
                                    *(uint32_t *)((var_158 << 2UL) + var_59) = 0U;
                                    state_0x8558_12 = state_0x8558_11;
                                    state_0x8560_11 = state_0x8560_10;
                                    var_159 = rcx2_10 + (-2L);
                                    *(uint32_t *)((var_159 << 2UL) + var_59) = 0U;
                                    if (var_158 != 0UL & var_159 == 0UL) {
                                        *(uint32_t *)(((rcx2_10 << 2UL) + var_59) + (-12L)) = 0U;
                                    }
                                }
                            }
                        } else {
                            var_149 = rdx3_10 - rax_16;
                            var_150 = var_47 - rax_16;
                            var_151 = (var_150 + (-4L)) >> 2UL;
                            var_152 = var_150 & (-4L);
                            rcx2_10 = rcx2_9;
                            if (var_149 > 2UL) {
                                var_153 = helper_pxor_xmm_wrapper_83((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), state_0x8558_10, state_0x8560_9);
                                var_154 = var_153.field_0;
                                var_155 = var_153.field_1;
                                state_0x8558_12 = var_154;
                                rax_17 = ((((rax_16 << 2UL) ^ (-4L)) + var_140) + (-12L)) + var_59;
                                state_0x8558_11 = var_154;
                                state_0x8560_10 = var_155;
                                state_0x8560_11 = var_155;
                                var_156 = rdx3_11 + 1UL;
                                *(uint64_t *)rax_17 = var_154;
                                *(uint64_t *)(rax_17 + 8UL) = var_155;
                                rdx3_11 = var_156;
                                while (var_151 >= var_156)
                                    {
                                        rax_17 = rax_17 + (-16L);
                                        var_156 = rdx3_11 + 1UL;
                                        *(uint64_t *)rax_17 = var_154;
                                        *(uint64_t *)(rax_17 + 8UL) = var_155;
                                        rdx3_11 = var_156;
                                    }
                                var_157 = rcx2_9 - var_152;
                                rcx2_10 = var_157;
                                var_158 = rcx2_10 + (-1L);
                                *(uint32_t *)((var_158 << 2UL) + var_59) = 0U;
                                state_0x8558_12 = state_0x8558_11;
                                state_0x8560_11 = state_0x8560_10;
                                var_159 = rcx2_10 + (-2L);
                                *(uint32_t *)((var_159 << 2UL) + var_59) = 0U;
                                if (var_152 != var_150 & var_158 != 0UL & var_159 == 0UL) {
                                    *(uint32_t *)(((rcx2_10 << 2UL) + var_59) + (-12L)) = 0U;
                                }
                            } else {
                                var_158 = rcx2_10 + (-1L);
                                *(uint32_t *)((var_158 << 2UL) + var_59) = 0U;
                                state_0x8558_12 = state_0x8558_11;
                                state_0x8560_11 = state_0x8560_10;
                                var_159 = rcx2_10 + (-2L);
                                *(uint32_t *)((var_159 << 2UL) + var_59) = 0U;
                                if (var_158 != 0UL & var_159 == 0UL) {
                                    *(uint32_t *)(((rcx2_10 << 2UL) + var_59) + (-12L)) = 0U;
                                }
                            }
                        }
                    }
                    state_0x8558_13 = state_0x8558_12;
                    state_0x8560_12 = state_0x8560_11;
                    r14_4 = r14_3 + var_47;
                }
                var_160 = *var_57;
                var_161 = var_0 + (-152L);
                var_162 = (uint64_t *)var_161;
                state_0x8558_6 = state_0x8558_13;
                state_0x8560_5 = state_0x8560_12;
                local_sp_0 = var_161;
                if (var_82) {
                    *var_162 = 4227208UL;
                    var_163 = indirect_placeholder_4(var_160, var_59, r14_4, var_30, var_66);
                    rbp_0 = var_163;
                } else {
                    *var_162 = 4228209UL;
                    var_164 = indirect_placeholder_4(var_160, var_59, r14_4, var_30, var_66);
                    rbp_0 = var_164;
                }
            }
        }
        *(uint64_t *)(local_sp_0 + (-8L)) = 4227219UL;
        indirect_placeholder();
        *(uint64_t *)(local_sp_0 + (-16L)) = 4227229UL;
        indirect_placeholder();
        if (rbp_0 != 0UL) {
            var_240 = *(uint64_t *)(local_sp_0 + 32UL);
            var_241 = *(uint64_t *)(local_sp_0 + 40UL);
            var_242 = ((long)var_240 < (long)0UL);
            helper_pxor_xmm_wrapper_83((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), state_0x8558_6, state_0x8560_5);
            r12_0 = var_240;
            cc_dst_0 = var_240;
            if (var_242) {
                var_244 = (var_240 >> 1UL) | (var_240 & 1UL);
                var_245 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_244, var_54, var_13, var_14, var_15);
                var_246 = helper_addss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_245.field_0, var_245.field_1, var_12, var_13, var_14, var_15, var_16);
                cc_dst_0 = var_244;
                state_0x8558_5 = var_246.field_0;
                storemerge9 = var_246.field_1;
            } else {
                var_243 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_240, var_54, var_13, var_14, var_15);
                state_0x8558_5 = var_243.field_0;
                storemerge9 = var_243.field_1;
            }
            var_247 = helper_mulss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), state_0x8558_5, var_49 | (uint64_t)*(uint32_t *)4268792UL, storemerge9, var_12, var_13, var_14, var_15, var_16);
            var_248 = var_247.field_0;
            var_249 = helper_ucomiss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), var_248, var_49 | (uint64_t)*(uint32_t *)4268796UL, var_247.field_1, var_12);
            var_250 = var_249.field_0;
            var_251 = var_249.field_1;
            var_252 = helper_cc_compute_c_wrapper(cc_dst_0, var_250, var_6, 1U);
            if (var_252 == 0UL) {
                var_254 = helper_subss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), var_248, var_49 | (uint64_t)*(uint32_t *)4268796UL, var_251, var_12, var_13, var_14, var_15, var_16);
                var_255 = helper_cvttss2sq_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_254.field_0, var_254.field_1, var_12);
                rax_6 = var_255.field_0 ^ (-9223372036854775808L);
            } else {
                var_253 = helper_cvttss2sq_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_248, var_251, var_12);
                rax_6 = var_253.field_0;
            }
            var_256 = ((rax_6 * 9UL) + 9UL) + rbx_5;
            var_257 = helper_cc_compute_c_wrapper(var_256, rbx_5, var_6, 9U);
            var_258 = (var_257 == 0UL) ? var_256 : 18446744073709551615UL;
            *(uint64_t *)(local_sp_0 + (-24L)) = 4227316UL;
            var_259 = indirect_placeholder_72(var_258);
            var_260 = var_259.field_0;
            rax_5 = var_260;
            rcx2_1 = var_260;
            rbx_0 = var_260;
            storemerge10 = var_260;
            if (var_260 == 0UL) {
                *(uint64_t *)(local_sp_0 + (-32L)) = 4228035UL;
                indirect_placeholder();
            } else {
                if (rbx_5 == 0UL) {
                    if (var_240 != 0UL) {
                        var_314 = var_260 + 1UL;
                        *(unsigned char *)var_260 = (unsigned char)'0';
                        rbx_4 = var_314;
                        *(unsigned char *)rbx_4 = (unsigned char)'\x00';
                        *(uint64_t *)(local_sp_0 + (-32L)) = 4228035UL;
                        indirect_placeholder();
                        return storemerge10;
                    }
                }
                var_261 = (0UL - var_260) & 15UL;
                var_262 = (var_261 > rbx_5) ? rbx_5 : var_261;
                if (rbx_5 > 18UL) {
                    rsi5_0 = var_262;
                    if (var_262 == 0UL) {
                        var_263 = rsi5_0 + var_260;
                        rcx2_1 = var_263;
                        rsi5_1 = rsi5_0;
                        var_264 = rax_5 + 1UL;
                        var_265 = rdx3_6 + (-1L);
                        *(unsigned char *)rax_5 = (unsigned char)'0';
                        rdx3_6 = var_265;
                        rax_5 = var_264;
                        rdx3_7 = var_265;
                        do {
                            var_264 = rax_5 + 1UL;
                            var_265 = rdx3_6 + (-1L);
                            *(unsigned char *)rax_5 = (unsigned char)'0';
                            rdx3_6 = var_265;
                            rax_5 = var_264;
                            rdx3_7 = var_265;
                        } while (var_264 != var_263);
                        if (rbx_5 != rsi5_0) {
                            var_266 = rbx_5 - rsi5_1;
                            var_267 = rbx_5 + (rsi5_1 ^ (-1L));
                            var_268 = (var_266 + (-16L)) >> 4UL;
                            var_269 = var_266 & (-16L);
                            rcx2_2 = rcx2_1;
                            rdx3_8 = rdx3_7;
                            if (var_267 > 14UL) {
                                var_270 = *(uint64_t *)4269008UL;
                                var_271 = *(uint64_t *)4269016UL;
                                rsi5_2 = rsi5_1 + var_260;
                                var_272 = rdi1_4 + 1UL;
                                *(uint64_t *)rsi5_2 = var_270;
                                *(uint64_t *)(rsi5_2 + 8UL) = var_271;
                                rdi1_4 = var_272;
                                while (var_268 >= var_272)
                                    {
                                        rsi5_2 = rsi5_2 + 16UL;
                                        var_272 = rdi1_4 + 1UL;
                                        *(uint64_t *)rsi5_2 = var_270;
                                        *(uint64_t *)(rsi5_2 + 8UL) = var_271;
                                        rdi1_4 = var_272;
                                    }
                                var_273 = rcx2_1 + var_269;
                                var_274 = rdx3_7 - var_269;
                                rcx2_2 = var_273;
                                rdx3_8 = var_274;
                                *(unsigned char *)rcx2_2 = (unsigned char)'0';
                                *(unsigned char *)(rcx2_2 + 1UL) = (unsigned char)'0';
                                *(unsigned char *)(rcx2_2 + 2UL) = (unsigned char)'0';
                                *(unsigned char *)(rcx2_2 + 3UL) = (unsigned char)'0';
                                *(unsigned char *)(rcx2_2 + 4UL) = (unsigned char)'0';
                                *(unsigned char *)(rcx2_2 + 5UL) = (unsigned char)'0';
                                *(unsigned char *)(rcx2_2 + 6UL) = (unsigned char)'0';
                                *(unsigned char *)(rcx2_2 + 7UL) = (unsigned char)'0';
                                *(unsigned char *)(rcx2_2 + 8UL) = (unsigned char)'0';
                                *(unsigned char *)(rcx2_2 + 9UL) = (unsigned char)'0';
                                *(unsigned char *)(rcx2_2 + 10UL) = (unsigned char)'0';
                                *(unsigned char *)(rcx2_2 + 11UL) = (unsigned char)'0';
                                *(unsigned char *)(rcx2_2 + 12UL) = (unsigned char)'0';
                                *(unsigned char *)(rcx2_2 + 13UL) = (unsigned char)'0';
                                if (var_269 != var_266 & rdx3_8 != 1UL & rdx3_8 != 2UL & rdx3_8 != 3UL & rdx3_8 != 4UL & rdx3_8 != 5UL & rdx3_8 != 6UL & rdx3_8 != 7UL & rdx3_8 != 8UL & rdx3_8 != 9UL & rdx3_8 != 10UL & rdx3_8 != 11UL & rdx3_8 != 12UL & rdx3_8 != 13UL & rdx3_8 == 14UL) {
                                    *(unsigned char *)(rcx2_2 + 14UL) = (unsigned char)'0';
                                }
                            } else {
                                *(unsigned char *)rcx2_2 = (unsigned char)'0';
                                *(unsigned char *)(rcx2_2 + 1UL) = (unsigned char)'0';
                                *(unsigned char *)(rcx2_2 + 2UL) = (unsigned char)'0';
                                *(unsigned char *)(rcx2_2 + 3UL) = (unsigned char)'0';
                                *(unsigned char *)(rcx2_2 + 4UL) = (unsigned char)'0';
                                *(unsigned char *)(rcx2_2 + 5UL) = (unsigned char)'0';
                                *(unsigned char *)(rcx2_2 + 6UL) = (unsigned char)'0';
                                *(unsigned char *)(rcx2_2 + 7UL) = (unsigned char)'0';
                                *(unsigned char *)(rcx2_2 + 8UL) = (unsigned char)'0';
                                *(unsigned char *)(rcx2_2 + 9UL) = (unsigned char)'0';
                                *(unsigned char *)(rcx2_2 + 10UL) = (unsigned char)'0';
                                *(unsigned char *)(rcx2_2 + 11UL) = (unsigned char)'0';
                                *(unsigned char *)(rcx2_2 + 12UL) = (unsigned char)'0';
                                *(unsigned char *)(rcx2_2 + 13UL) = (unsigned char)'0';
                                if (rdx3_8 != 1UL & rdx3_8 != 2UL & rdx3_8 != 3UL & rdx3_8 != 4UL & rdx3_8 != 5UL & rdx3_8 != 6UL & rdx3_8 != 7UL & rdx3_8 != 8UL & rdx3_8 != 9UL & rdx3_8 != 10UL & rdx3_8 != 11UL & rdx3_8 != 12UL & rdx3_8 != 13UL & rdx3_8 == 14UL) {
                                    *(unsigned char *)(rcx2_2 + 14UL) = (unsigned char)'0';
                                }
                            }
                        }
                    } else {
                        var_266 = rbx_5 - rsi5_1;
                        var_267 = rbx_5 + (rsi5_1 ^ (-1L));
                        var_268 = (var_266 + (-16L)) >> 4UL;
                        var_269 = var_266 & (-16L);
                        rcx2_2 = rcx2_1;
                        rdx3_8 = rdx3_7;
                        if (var_267 > 14UL) {
                            var_270 = *(uint64_t *)4269008UL;
                            var_271 = *(uint64_t *)4269016UL;
                            rsi5_2 = rsi5_1 + var_260;
                            var_272 = rdi1_4 + 1UL;
                            *(uint64_t *)rsi5_2 = var_270;
                            *(uint64_t *)(rsi5_2 + 8UL) = var_271;
                            rdi1_4 = var_272;
                            while (var_268 >= var_272)
                                {
                                    rsi5_2 = rsi5_2 + 16UL;
                                    var_272 = rdi1_4 + 1UL;
                                    *(uint64_t *)rsi5_2 = var_270;
                                    *(uint64_t *)(rsi5_2 + 8UL) = var_271;
                                    rdi1_4 = var_272;
                                }
                            var_273 = rcx2_1 + var_269;
                            var_274 = rdx3_7 - var_269;
                            rcx2_2 = var_273;
                            rdx3_8 = var_274;
                            *(unsigned char *)rcx2_2 = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 1UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 2UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 3UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 4UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 5UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 6UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 7UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 8UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 9UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 10UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 11UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 12UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 13UL) = (unsigned char)'0';
                            if (var_269 != var_266 & rdx3_8 != 1UL & rdx3_8 != 2UL & rdx3_8 != 3UL & rdx3_8 != 4UL & rdx3_8 != 5UL & rdx3_8 != 6UL & rdx3_8 != 7UL & rdx3_8 != 8UL & rdx3_8 != 9UL & rdx3_8 != 10UL & rdx3_8 != 11UL & rdx3_8 != 12UL & rdx3_8 != 13UL & rdx3_8 == 14UL) {
                                *(unsigned char *)(rcx2_2 + 14UL) = (unsigned char)'0';
                            }
                        } else {
                            *(unsigned char *)rcx2_2 = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 1UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 2UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 3UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 4UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 5UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 6UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 7UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 8UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 9UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 10UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 11UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 12UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 13UL) = (unsigned char)'0';
                            if (rdx3_8 != 1UL & rdx3_8 != 2UL & rdx3_8 != 3UL & rdx3_8 != 4UL & rdx3_8 != 5UL & rdx3_8 != 6UL & rdx3_8 != 7UL & rdx3_8 != 8UL & rdx3_8 != 9UL & rdx3_8 != 10UL & rdx3_8 != 11UL & rdx3_8 != 12UL & rdx3_8 != 13UL & rdx3_8 == 14UL) {
                                *(unsigned char *)(rcx2_2 + 14UL) = (unsigned char)'0';
                            }
                        }
                    }
                } else {
                    var_263 = rsi5_0 + var_260;
                    rcx2_1 = var_263;
                    rsi5_1 = rsi5_0;
                    var_264 = rax_5 + 1UL;
                    var_265 = rdx3_6 + (-1L);
                    *(unsigned char *)rax_5 = (unsigned char)'0';
                    rdx3_6 = var_265;
                    rax_5 = var_264;
                    rdx3_7 = var_265;
                    do {
                        var_264 = rax_5 + 1UL;
                        var_265 = rdx3_6 + (-1L);
                        *(unsigned char *)rax_5 = (unsigned char)'0';
                        rdx3_6 = var_265;
                        rax_5 = var_264;
                        rdx3_7 = var_265;
                    } while (var_264 != var_263);
                    if (rbx_5 != rsi5_0) {
                        var_266 = rbx_5 - rsi5_1;
                        var_267 = rbx_5 + (rsi5_1 ^ (-1L));
                        var_268 = (var_266 + (-16L)) >> 4UL;
                        var_269 = var_266 & (-16L);
                        rcx2_2 = rcx2_1;
                        rdx3_8 = rdx3_7;
                        if (var_267 > 14UL) {
                            var_270 = *(uint64_t *)4269008UL;
                            var_271 = *(uint64_t *)4269016UL;
                            rsi5_2 = rsi5_1 + var_260;
                            var_272 = rdi1_4 + 1UL;
                            *(uint64_t *)rsi5_2 = var_270;
                            *(uint64_t *)(rsi5_2 + 8UL) = var_271;
                            rdi1_4 = var_272;
                            while (var_268 >= var_272)
                                {
                                    rsi5_2 = rsi5_2 + 16UL;
                                    var_272 = rdi1_4 + 1UL;
                                    *(uint64_t *)rsi5_2 = var_270;
                                    *(uint64_t *)(rsi5_2 + 8UL) = var_271;
                                    rdi1_4 = var_272;
                                }
                            var_273 = rcx2_1 + var_269;
                            var_274 = rdx3_7 - var_269;
                            rcx2_2 = var_273;
                            rdx3_8 = var_274;
                            *(unsigned char *)rcx2_2 = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 1UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 2UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 3UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 4UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 5UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 6UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 7UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 8UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 9UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 10UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 11UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 12UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 13UL) = (unsigned char)'0';
                            if (var_269 != var_266 & rdx3_8 != 1UL & rdx3_8 != 2UL & rdx3_8 != 3UL & rdx3_8 != 4UL & rdx3_8 != 5UL & rdx3_8 != 6UL & rdx3_8 != 7UL & rdx3_8 != 8UL & rdx3_8 != 9UL & rdx3_8 != 10UL & rdx3_8 != 11UL & rdx3_8 != 12UL & rdx3_8 != 13UL & rdx3_8 == 14UL) {
                                *(unsigned char *)(rcx2_2 + 14UL) = (unsigned char)'0';
                            }
                        } else {
                            *(unsigned char *)rcx2_2 = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 1UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 2UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 3UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 4UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 5UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 6UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 7UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 8UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 9UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 10UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 11UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 12UL) = (unsigned char)'0';
                            *(unsigned char *)(rcx2_2 + 13UL) = (unsigned char)'0';
                            if (rdx3_8 != 1UL & rdx3_8 != 2UL & rdx3_8 != 3UL & rdx3_8 != 4UL & rdx3_8 != 5UL & rdx3_8 != 6UL & rdx3_8 != 7UL & rdx3_8 != 8UL & rdx3_8 != 9UL & rdx3_8 != 10UL & rdx3_8 != 11UL & rdx3_8 != 12UL & rdx3_8 != 13UL & rdx3_8 == 14UL) {
                                *(unsigned char *)(rcx2_2 + 14UL) = (unsigned char)'0';
                            }
                        }
                    }
                }
                var_275 = rbx_5 + var_260;
                rbx_0 = var_275;
                rbx_2_ph = var_275;
                rbx_3 = var_275;
                if (var_240 == 0UL) {
                    rbx_1 = rbx_0;
                    var_284 = (var_282 * 3435973837UL) >> 35UL;
                    var_285 = (unsigned char)var_284;
                    var_286 = (unsigned char)var_282 + (var_285 * '\xf6');
                    var_287 = (var_284 * 3435973837UL) >> 35UL;
                    *(unsigned char *)rbx_1 = (var_286 + '0');
                    var_288 = (unsigned char)var_287;
                    var_289 = var_285 + (var_288 * '\xf6');
                    var_290 = (var_287 * 3435973837UL) >> 35UL;
                    *(unsigned char *)(rbx_1 + 1UL) = (var_289 + '0');
                    var_291 = (unsigned char)var_290;
                    var_292 = var_288 + (var_291 * '\xf6');
                    var_293 = (var_290 * 3435973837UL) >> 35UL;
                    *(unsigned char *)(rbx_1 + 2UL) = (var_292 + '0');
                    var_294 = (unsigned char)var_293;
                    var_295 = var_291 + (var_294 * '\xf6');
                    var_296 = (var_293 * 3435973837UL) >> 35UL;
                    *(unsigned char *)(rbx_1 + 3UL) = (var_295 + '0');
                    var_297 = (unsigned char)var_296;
                    var_298 = var_294 + (var_297 * '\xf6');
                    var_299 = (var_296 * 3435973837UL) >> 35UL;
                    *(unsigned char *)(rbx_1 + 4UL) = (var_298 + '0');
                    var_300 = (unsigned char)var_299;
                    var_301 = var_297 + (var_300 * '\xf6');
                    var_302 = (var_299 * 3435973837UL) >> 35UL;
                    *(unsigned char *)(rbx_1 + 5UL) = (var_301 + '0');
                    var_303 = (unsigned char)var_302;
                    var_304 = var_300 + (var_303 * '\xf6');
                    var_305 = rbx_1 + 9UL;
                    var_306 = (var_302 * 3435973837UL) >> 35UL;
                    *(unsigned char *)(rbx_1 + 6UL) = (var_304 + '0');
                    var_307 = (unsigned char)var_306;
                    var_308 = (var_306 * 3435973837UL) >> 35UL;
                    *(unsigned char *)(rbx_1 + 7UL) = ((var_303 + (var_307 * '\xf6')) + '0');
                    var_309 = var_307 + ((unsigned char)var_308 * '\xf6');
                    var_310 = (uint64_t)var_309;
                    *(unsigned char *)(rbx_1 + 8UL) = (var_309 + '0');
                    var_311 = helper_cc_compute_c_wrapper((uint64_t)*(uint32_t *)(var_277 + (-4L)) + (-1L), 1UL, cc_src2_0, 16U);
                    var_312 = r12_0 - var_311;
                    rbx_3 = var_305;
                    rbx_2_ph = var_305;
                    rbx_1 = var_305;
                    r12_0 = var_312;
                    cc_src2_0 = var_311;
                    do {
                        var_277 = (r12_0 << 2UL) + var_241;
                        rdi1_5 = r12_0;
                        rsi5_3 = var_277;
                        var_278 = rsi5_3 + (-4L);
                        _cast3 = (uint32_t *)var_278;
                        var_279 = *_cast3;
                        var_280 = ((unsigned __int128)(((rcx2_3 << 32UL) | (uint64_t)var_279) >> 9UL) * 19342813113834067ULL) >> 75ULL;
                        var_281 = (uint64_t)var_280;
                        *_cast3 = (uint32_t)var_280;
                        var_282 = (uint64_t)(((uint32_t)var_281 * 3294967296U) + var_279);
                        var_283 = rdi1_5 + (-1L);
                        rdi1_5 = var_283;
                        rcx2_3 = var_282;
                        rsi5_3 = var_278;
                        do {
                            var_278 = rsi5_3 + (-4L);
                            _cast3 = (uint32_t *)var_278;
                            var_279 = *_cast3;
                            var_280 = ((unsigned __int128)(((rcx2_3 << 32UL) | (uint64_t)var_279) >> 9UL) * 19342813113834067ULL) >> 75ULL;
                            var_281 = (uint64_t)var_280;
                            *_cast3 = (uint32_t)var_280;
                            var_282 = (uint64_t)(((uint32_t)var_281 * 3294967296U) + var_279);
                            var_283 = rdi1_5 + (-1L);
                            rdi1_5 = var_283;
                            rcx2_3 = var_282;
                            rsi5_3 = var_278;
                        } while (var_283 != 0UL);
                        var_284 = (var_282 * 3435973837UL) >> 35UL;
                        var_285 = (unsigned char)var_284;
                        var_286 = (unsigned char)var_282 + (var_285 * '\xf6');
                        var_287 = (var_284 * 3435973837UL) >> 35UL;
                        *(unsigned char *)rbx_1 = (var_286 + '0');
                        var_288 = (unsigned char)var_287;
                        var_289 = var_285 + (var_288 * '\xf6');
                        var_290 = (var_287 * 3435973837UL) >> 35UL;
                        *(unsigned char *)(rbx_1 + 1UL) = (var_289 + '0');
                        var_291 = (unsigned char)var_290;
                        var_292 = var_288 + (var_291 * '\xf6');
                        var_293 = (var_290 * 3435973837UL) >> 35UL;
                        *(unsigned char *)(rbx_1 + 2UL) = (var_292 + '0');
                        var_294 = (unsigned char)var_293;
                        var_295 = var_291 + (var_294 * '\xf6');
                        var_296 = (var_293 * 3435973837UL) >> 35UL;
                        *(unsigned char *)(rbx_1 + 3UL) = (var_295 + '0');
                        var_297 = (unsigned char)var_296;
                        var_298 = var_294 + (var_297 * '\xf6');
                        var_299 = (var_296 * 3435973837UL) >> 35UL;
                        *(unsigned char *)(rbx_1 + 4UL) = (var_298 + '0');
                        var_300 = (unsigned char)var_299;
                        var_301 = var_297 + (var_300 * '\xf6');
                        var_302 = (var_299 * 3435973837UL) >> 35UL;
                        *(unsigned char *)(rbx_1 + 5UL) = (var_301 + '0');
                        var_303 = (unsigned char)var_302;
                        var_304 = var_300 + (var_303 * '\xf6');
                        var_305 = rbx_1 + 9UL;
                        var_306 = (var_302 * 3435973837UL) >> 35UL;
                        *(unsigned char *)(rbx_1 + 6UL) = (var_304 + '0');
                        var_307 = (unsigned char)var_306;
                        var_308 = (var_306 * 3435973837UL) >> 35UL;
                        *(unsigned char *)(rbx_1 + 7UL) = ((var_303 + (var_307 * '\xf6')) + '0');
                        var_309 = var_307 + ((unsigned char)var_308 * '\xf6');
                        var_310 = (uint64_t)var_309;
                        *(unsigned char *)(rbx_1 + 8UL) = (var_309 + '0');
                        var_311 = helper_cc_compute_c_wrapper((uint64_t)*(uint32_t *)(var_277 + (-4L)) + (-1L), 1UL, cc_src2_0, 16U);
                        var_312 = r12_0 - var_311;
                        rbx_3 = var_305;
                        rbx_2_ph = var_305;
                        rbx_1 = var_305;
                        r12_0 = var_312;
                        cc_src2_0 = var_311;
                    } while (var_312 != 0UL);
                    if (!((var_305 > var_260) && (var_310 == 0UL))) {
                        rbx_4 = rbx_3;
                        if (var_260 == rbx_3) {
                            var_314 = var_260 + 1UL;
                            *(unsigned char *)var_260 = (unsigned char)'0';
                            rbx_4 = var_314;
                        }
                        *(unsigned char *)rbx_4 = (unsigned char)'\x00';
                        *(uint64_t *)(local_sp_0 + (-32L)) = 4228035UL;
                        indirect_placeholder();
                        return storemerge10;
                    }
                    rbx_2 = rbx_2_ph;
                    while (1U)
                        {
                            var_313 = rbx_2 + (-1L);
                            rbx_2 = var_313;
                            rbx_4 = var_313;
                            if (var_313 != var_260) {
                                loop_state_var = 1U;
                                break;
                            }
                            if (*(unsigned char *)(rbx_2 + (-2L)) == '0') {
                                continue;
                            }
                            loop_state_var = 0U;
                            break;
                        }
                    switch (loop_state_var) {
                      case 0U:
                        {
                            *(unsigned char *)rbx_4 = (unsigned char)'\x00';
                        }
                        break;
                      case 1U:
                        {
                            var_314 = var_260 + 1UL;
                            *(unsigned char *)var_260 = (unsigned char)'0';
                            rbx_4 = var_314;
                        }
                        break;
                    }
                } else {
                    var_276 = helper_cc_compute_c_wrapper(0UL - rbx_5, var_275, var_6, 17U);
                    if (var_276 != 0UL) {
                        rbx_4 = rbx_3;
                        if (var_260 == rbx_3) {
                            var_314 = var_260 + 1UL;
                            *(unsigned char *)var_260 = (unsigned char)'0';
                            rbx_4 = var_314;
                        }
                        *(unsigned char *)rbx_4 = (unsigned char)'\x00';
                        *(uint64_t *)(local_sp_0 + (-32L)) = 4228035UL;
                        indirect_placeholder();
                        return storemerge10;
                    }
                    rbx_2 = rbx_2_ph;
                    while (1U)
                        {
                            var_313 = rbx_2 + (-1L);
                            rbx_2 = var_313;
                            rbx_4 = var_313;
                            if (var_313 != var_260) {
                                loop_state_var = 1U;
                                break;
                            }
                            if (*(unsigned char *)(rbx_2 + (-2L)) == '0') {
                                continue;
                            }
                            loop_state_var = 0U;
                            break;
                        }
                    switch (loop_state_var) {
                      case 1U:
                        {
                            var_314 = var_260 + 1UL;
                            *(unsigned char *)var_260 = (unsigned char)'0';
                            rbx_4 = var_314;
                        }
                        break;
                      case 0U:
                        {
                            *(unsigned char *)rbx_4 = (unsigned char)'\x00';
                            *(uint64_t *)(local_sp_0 + (-32L)) = 4228035UL;
                            indirect_placeholder();
                            return storemerge10;
                        }
                        break;
                    }
                }
            }
        }
    }
}
