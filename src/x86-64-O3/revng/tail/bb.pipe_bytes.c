typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_59_ret_type;
struct indirect_placeholder_65_ret_type;
struct indirect_placeholder_61_ret_type;
struct indirect_placeholder_62_ret_type;
struct indirect_placeholder_63_ret_type;
struct indirect_placeholder_60_ret_type;
struct indirect_placeholder_64_ret_type;
struct indirect_placeholder_59_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_65_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_61_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_62_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_63_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
};
struct indirect_placeholder_60_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_64_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_59_ret_type indirect_placeholder_59(uint64_t param_0);
extern struct indirect_placeholder_65_ret_type indirect_placeholder_65(uint64_t param_0);
extern struct indirect_placeholder_61_ret_type indirect_placeholder_61(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_62_ret_type indirect_placeholder_62(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_63_ret_type indirect_placeholder_63(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_60_ret_type indirect_placeholder_60(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_64_ret_type indirect_placeholder_64(uint64_t param_0);
uint64_t bb_pipe_bytes(uint64_t rcx, uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    struct indirect_placeholder_59_ret_type var_9;
    uint64_t var_10;
    uint64_t var_11;
    struct indirect_placeholder_65_ret_type var_12;
    uint64_t rbp_3_ph_ph;
    uint64_t r15_1_ph;
    uint64_t rbx_2_ph;
    uint64_t rbx_2;
    uint64_t r12_0;
    uint64_t rbp_0;
    uint64_t local_sp_3;
    uint64_t var_30;
    uint64_t r12_1;
    uint64_t rbx_0;
    uint64_t local_sp_0;
    uint64_t var_45;
    uint64_t var_46;
    struct indirect_placeholder_61_ret_type var_47;
    uint64_t var_48;
    bool var_49;
    uint64_t var_50;
    uint64_t var_37;
    uint64_t r12_2_ph;
    uint64_t local_sp_2_ph;
    uint64_t local_sp_2;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t *var_14;
    uint64_t *var_15;
    uint64_t *var_16;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t local_sp_3_ph_ph;
    uint64_t rax_0;
    uint64_t rbp_1;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t r15_0;
    uint64_t rsi4_0;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t spec_select;
    uint64_t var_40;
    struct indirect_placeholder_62_ret_type var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_51;
    struct indirect_placeholder_63_ret_type var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t *var_23;
    uint64_t var_24;
    uint64_t local_sp_3_ph;
    uint64_t var_26;
    uint64_t r15_1_ph_ph;
    uint64_t r14_0_ph_ph;
    uint64_t rbx_3_ph_ph;
    uint64_t *var_13;
    uint64_t rbp_3_ph;
    uint64_t r14_0_ph;
    uint64_t var_27;
    struct indirect_placeholder_64_ret_type var_28;
    uint64_t var_25;
    uint64_t *var_29;
    uint64_t r15_1;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t *var_19;
    uint64_t var_20;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r15();
    var_3 = init_r14();
    var_4 = init_r12();
    var_5 = init_rbx();
    var_6 = init_cc_src2();
    var_7 = init_r13();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_7;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_1;
    *(uint64_t *)(var_0 + (-48L)) = var_5;
    var_8 = var_0 + (-88L);
    *(uint64_t *)(var_0 + (-64L)) = rdi;
    *(uint32_t *)(var_0 + (-76L)) = (uint32_t)rsi;
    *(uint64_t *)var_8 = rdx;
    *(uint64_t *)(var_0 + (-72L)) = rcx;
    *(uint64_t *)(var_0 + (-96L)) = 4209901UL;
    var_9 = indirect_placeholder_59(1040UL);
    var_10 = var_9.field_0;
    *(uint64_t *)(var_10 + 1024UL) = 0UL;
    *(uint64_t *)(var_10 + 1032UL) = 0UL;
    var_11 = var_0 + (-104L);
    *(uint64_t *)var_11 = 4209939UL;
    var_12 = indirect_placeholder_65(1040UL);
    rbp_3_ph_ph = var_12.field_0;
    r12_0 = 0UL;
    r12_2_ph = 1UL;
    local_sp_3_ph_ph = var_11;
    r15_1_ph_ph = 0UL;
    r14_0_ph_ph = var_10;
    rbx_3_ph_ph = var_10;
    while (1U)
        {
            var_13 = (uint64_t *)(rbx_3_ph_ph + 1024UL);
            rbp_3_ph_ph = rbx_3_ph_ph;
            r15_1_ph = r15_1_ph_ph;
            rbp_1 = rbx_3_ph_ph;
            local_sp_3_ph = local_sp_3_ph_ph;
            rbp_3_ph = rbp_3_ph_ph;
            r14_0_ph = r14_0_ph_ph;
            while (1U)
                {
                    var_14 = (uint64_t *)(rbp_3_ph + 1024UL);
                    var_15 = (uint64_t *)(r14_0_ph + 1024UL);
                    var_16 = (uint64_t *)(rbp_3_ph + 1032UL);
                    r14_0_ph_ph = rbp_3_ph;
                    r14_0_ph = rbp_3_ph;
                    r15_1 = r15_1_ph;
                    local_sp_3 = local_sp_3_ph;
                    while (1U)
                        {
                            var_17 = (uint64_t)*(uint32_t *)(local_sp_3 + 12UL);
                            var_18 = local_sp_3 + (-8L);
                            var_19 = (uint64_t *)var_18;
                            *var_19 = 4209969UL;
                            var_20 = indirect_placeholder_12(1024UL, var_17, rbp_3_ph);
                            r15_0 = r15_1;
                            local_sp_3_ph_ph = var_18;
                            if ((var_20 + (-1L)) > 18446744073709551613UL) {
                                var_21 = *(uint64_t *)(local_sp_3 + 8UL);
                                var_22 = r15_1 + var_20;
                                var_23 = (uint64_t *)var_21;
                                *var_23 = (*var_23 + var_20);
                                *var_14 = var_20;
                                var_24 = *var_15;
                                *var_16 = 0UL;
                                r15_1_ph = var_22;
                                r15_1 = var_22;
                                if ((var_20 + var_24) <= 1023UL) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                var_25 = local_sp_3 + (-16L);
                                *(uint64_t *)var_25 = 4210094UL;
                                indirect_placeholder();
                                *var_15 = (*var_15 + *var_14);
                                local_sp_3 = var_25;
                                continue;
                            }
                            var_29 = (uint64_t *)(local_sp_3 + (-16L));
                            *var_29 = 4210152UL;
                            indirect_placeholder();
                            if (var_20 == 18446744073709551615UL) {
                                var_51 = *(uint64_t *)(local_sp_3 + 8UL);
                                *(uint64_t *)(local_sp_3 + (-24L)) = 4210383UL;
                                var_52 = indirect_placeholder_63(rbp_3_ph, 18446744073709551615UL, 4UL, var_51);
                                var_53 = var_52.field_0;
                                var_54 = var_52.field_2;
                                var_55 = var_52.field_4;
                                var_56 = var_52.field_7;
                                *(uint64_t *)(local_sp_3 + (-32L)) = 4210391UL;
                                indirect_placeholder();
                                var_57 = (uint64_t)*(uint32_t *)var_53;
                                var_58 = local_sp_3 + (-40L);
                                *(uint64_t *)var_58 = 4210413UL;
                                indirect_placeholder_60(0UL, var_53, 4280413UL, var_55, 0UL, var_57, var_56);
                                r12_2_ph = 0UL;
                                rbx_2_ph = var_54;
                                local_sp_2_ph = var_58;
                                if (var_54 != 0UL) {
                                    loop_state_var = 4U;
                                    break;
                                }
                                loop_state_var = 3U;
                                break;
                            }
                            var_30 = *var_13;
                            var_31 = r15_1 - var_30;
                            var_32 = helper_cc_compute_c_wrapper(*var_29 - var_31, var_31, var_6, 17U);
                            rax_0 = var_31;
                            rsi4_0 = var_30;
                            if (var_32 != 0UL) {
                                var_33 = *var_29;
                                var_37 = var_33;
                                loop_state_var = 2U;
                                break;
                            }
                            var_37 = *var_29;
                            loop_state_var = 1U;
                            break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 0U:
                        {
                            *(uint64_t *)(r14_0_ph + 1032UL) = rbp_3_ph;
                            var_26 = var_22 - *var_13;
                            r15_1_ph_ph = var_26;
                            if (var_26 <= *var_19) {
                                loop_state_var = 0U;
                                switch_state_var = 1;
                                break;
                            }
                            var_27 = local_sp_3 + (-16L);
                            *(uint64_t *)var_27 = 4210133UL;
                            var_28 = indirect_placeholder_64(1040UL);
                            rbp_3_ph = var_28.field_0;
                            local_sp_3_ph = var_27;
                            continue;
                        }
                        break;
                      case 1U:
                        {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 2U:
                        {
                            loop_state_var = 2U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 3U:
                        {
                            loop_state_var = 3U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 4U:
                        {
                            loop_state_var = 4U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    rbx_3_ph_ph = *(uint64_t *)(rbx_3_ph_ph + 1032UL);
                    continue;
                }
                break;
              case 1U:
                {
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 2U:
                {
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 3U:
                {
                    loop_state_var = 2U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 4U:
                {
                    loop_state_var = 3U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 2U:
        {
            return (uint64_t)(uint32_t)r12_0;
        }
        break;
      case 3U:
      case 1U:
      case 0U:
        {
            switch (loop_state_var) {
              case 3U:
                {
                    r12_0 = r12_2_ph;
                    rbx_2 = rbx_2_ph;
                    local_sp_2 = local_sp_2_ph;
                    var_59 = *(uint64_t *)(rbx_2 + 1032UL);
                    var_60 = local_sp_2 + (-8L);
                    *(uint64_t *)var_60 = 4210311UL;
                    indirect_placeholder();
                    rbx_2 = var_59;
                    local_sp_2 = var_60;
                    do {
                        var_59 = *(uint64_t *)(rbx_2 + 1032UL);
                        var_60 = local_sp_2 + (-8L);
                        *(uint64_t *)var_60 = 4210311UL;
                        indirect_placeholder();
                        rbx_2 = var_59;
                        local_sp_2 = var_60;
                    } while (var_59 != 0UL);
                }
                break;
              case 1U:
              case 0U:
                {
                    switch (loop_state_var) {
                      case 1U:
                        {
                            var_34 = *(uint64_t *)(rbp_1 + 1032UL);
                            var_35 = *(uint64_t *)(var_34 + 1024UL);
                            var_36 = rax_0 - var_35;
                            rax_0 = var_36;
                            rbp_1 = var_34;
                            r15_0 = rax_0;
                            rsi4_0 = var_35;
                            do {
                                var_34 = *(uint64_t *)(rbp_1 + 1032UL);
                                var_35 = *(uint64_t *)(var_34 + 1024UL);
                                var_36 = rax_0 - var_35;
                                rax_0 = var_36;
                                rbp_1 = var_34;
                                r15_0 = rax_0;
                                rsi4_0 = var_35;
                            } while (var_36 <= var_33);
                        }
                        break;
                      case 0U:
                        {
                            var_38 = var_37 - r15_0;
                            var_39 = helper_cc_compute_c_wrapper(var_38, r15_0, var_6, 17U);
                            spec_select = (var_39 == 0UL) ? rsi4_0 : (rsi4_0 + var_38);
                            var_40 = local_sp_3 + (-24L);
                            *(uint64_t *)var_40 = 4210235UL;
                            var_41 = indirect_placeholder_62(var_20, rbx_3_ph_ph, spec_select);
                            var_42 = var_41.field_0;
                            var_43 = var_41.field_4;
                            var_44 = *(uint64_t *)(var_42 + 1032UL);
                            rbx_2_ph = var_43;
                            rbp_0 = var_44;
                            rbx_0 = var_43;
                            local_sp_0 = var_40;
                            local_sp_2_ph = var_40;
                            if (var_44 != 0UL) {
                                r12_1 = var_41.field_3;
                                var_45 = *(uint64_t *)(rbp_0 + 1024UL);
                                var_46 = local_sp_0 + (-8L);
                                *(uint64_t *)var_46 = 4210271UL;
                                var_47 = indirect_placeholder_61(r12_1, rbx_0, rbp_0, var_45);
                                var_48 = *(uint64_t *)(var_47.field_0 + 1032UL);
                                var_49 = (var_48 == 0UL);
                                var_50 = var_47.field_4;
                                rbx_2_ph = var_50;
                                rbp_0 = var_48;
                                rbx_0 = var_50;
                                local_sp_0 = var_46;
                                local_sp_2_ph = var_46;
                                while (!var_49)
                                    {
                                        r12_1 = var_47.field_3;
                                        var_45 = *(uint64_t *)(rbp_0 + 1024UL);
                                        var_46 = local_sp_0 + (-8L);
                                        *(uint64_t *)var_46 = 4210271UL;
                                        var_47 = indirect_placeholder_61(r12_1, rbx_0, rbp_0, var_45);
                                        var_48 = *(uint64_t *)(var_47.field_0 + 1032UL);
                                        var_49 = (var_48 == 0UL);
                                        var_50 = var_47.field_4;
                                        rbx_2_ph = var_50;
                                        rbp_0 = var_48;
                                        rbx_0 = var_50;
                                        local_sp_0 = var_46;
                                        local_sp_2_ph = var_46;
                                    }
                            }
                        }
                        break;
                    }
                }
                break;
            }
        }
        break;
    }
}
