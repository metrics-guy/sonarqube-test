typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r12(void);
uint64_t bb_mode_adjust(uint64_t rcx, uint64_t rdx, uint64_t rdi, uint64_t r8, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    unsigned char var_5;
    uint64_t var_6;
    uint64_t rbx_2;
    uint64_t var_7;
    bool var_8;
    uint64_t rax_1;
    uint64_t cc_src2_7;
    uint64_t rbx_0;
    uint64_t rcx1_0;
    unsigned char r11_0_be_in;
    uint64_t cc_src2_0;
    uint64_t rdi3_1;
    uint64_t var_21;
    uint64_t rax_2;
    unsigned char r11_0_in;
    uint64_t r9_4;
    uint64_t rax_0;
    uint32_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t r10_1;
    uint64_t r10_0;
    uint64_t rdi3_0;
    uint64_t r10_2;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t rdi3_5;
    uint64_t cc_src2_4;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t r10_3;
    uint64_t r9_2;
    uint64_t r9_0;
    uint64_t rdi3_2;
    uint64_t cc_src2_1;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t r9_1;
    uint64_t rdi3_3;
    uint64_t cc_src2_2;
    uint64_t r11_1;
    uint64_t var_30;
    unsigned char var_31;
    uint64_t r9_5;
    uint64_t r10_4;
    unsigned char _pre_phi;
    uint64_t rdi3_4;
    uint64_t cc_src2_3;
    uint64_t r11_2;
    uint64_t var_26;
    unsigned char var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t r9_3;
    unsigned char var_36;
    uint32_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t rbx_1;
    uint64_t rbx_0_be;
    uint64_t cc_src2_0_be;
    uint64_t rax_0_be;
    uint64_t cc_src2_5;
    uint32_t var_32;
    uint32_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t cc_src2_6;
    uint32_t var_40;
    uint32_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    unsigned char var_44;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r12();
    var_3 = init_rbx();
    var_4 = init_cc_src2();
    var_5 = *(unsigned char *)(rcx + 1UL);
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    var_6 = (uint64_t)((uint16_t)rdi & (unsigned short)4095U);
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    rbx_2 = 0UL;
    rbx_0 = 0UL;
    rcx1_0 = rcx;
    cc_src2_0 = var_4;
    rdi3_1 = 0UL;
    rax_2 = var_6;
    r11_0_in = var_5;
    rax_0 = var_6;
    r10_1 = 4294967295UL;
    r10_0 = 4294967295UL;
    rdi3_0 = 0UL;
    r10_2 = 4294967295UL;
    rdi3_2 = 0UL;
    if (var_5 != '\x00') {
        var_7 = (uint64_t)(uint32_t)rdx ^ 4294967295UL;
        var_8 = ((uint64_t)(unsigned char)rsi == 0UL);
        while (1U)
            {
                var_9 = *(uint32_t *)(rcx1_0 + 4UL);
                var_10 = (uint64_t)var_9;
                var_11 = (uint64_t)*(uint32_t *)(rcx1_0 + 8UL);
                rax_1 = rax_0;
                r9_0 = var_11;
                cc_src2_1 = cc_src2_0;
                cc_src2_2 = cc_src2_0;
                cc_src2_3 = cc_src2_0;
                rbx_1 = rbx_0;
                if (!var_8) {
                    var_12 = (uint64_t)*(uint32_t *)(rcx1_0 + 12UL);
                    var_13 = var_12 | 4294964223UL;
                    var_14 = (uint64_t)((uint16_t)var_12 & (unsigned short)3072U) ^ 3072UL;
                    r10_0 = var_13;
                    rdi3_0 = var_14;
                    r10_1 = var_13;
                    rdi3_1 = var_14;
                    if ((uint64_t)(r11_0_in + '\xfe') == 0UL) {
                        var_21 = var_11 | 73UL;
                        var_22 = (uint64_t)*(unsigned char *)rcx1_0;
                        var_23 = var_21 & r10_1;
                        r10_3 = r10_1;
                        r9_2 = var_23;
                        r9_1 = var_23;
                        rdi3_3 = rdi3_1;
                        r11_1 = var_22;
                        rdi3_4 = rdi3_1;
                        r11_2 = var_22;
                        if (var_9 == 0U) {
                            var_30 = r9_1 & var_7;
                            var_31 = (unsigned char)r11_1;
                            r9_4 = var_30;
                            rdi3_5 = rdi3_3;
                            cc_src2_4 = cc_src2_2;
                            r9_5 = var_30;
                            r10_4 = r10_3;
                            _pre_phi = var_31;
                            r9_3 = var_30;
                            cc_src2_5 = cc_src2_2;
                            cc_src2_6 = cc_src2_2;
                            if ((uint64_t)(var_31 + '\xd3') == 0UL) {
                                if ((uint64_t)(var_31 + '\xc3') != 0UL) {
                                    var_36 = *(unsigned char *)(rcx1_0 + 17UL);
                                    var_37 = (uint32_t)rax_0 & (uint32_t)rdi3_5;
                                    var_38 = (uint64_t)(uint32_t)rbx_0 | (uint64_t)((uint16_t)r10_4 & (unsigned short)4095U);
                                    var_39 = (uint64_t)(var_37 | (uint32_t)r9_3);
                                    rbx_2 = var_38;
                                    r11_0_be_in = var_36;
                                    rax_2 = var_39;
                                    rbx_0_be = var_38;
                                    cc_src2_0_be = cc_src2_4;
                                    rax_0_be = var_39;
                                    if (var_36 == '\x00') {
                                        break;
                                    }
                                    rbx_0 = rbx_0_be;
                                    rcx1_0 = rcx1_0 + 16UL;
                                    cc_src2_0 = cc_src2_0_be;
                                    r11_0_in = r11_0_be_in;
                                    rax_0 = rax_0_be;
                                    continue;
                                }
                            }
                            var_40 = (uint32_t)rbx_0;
                            var_41 = (uint32_t)r9_5;
                            var_42 = (uint64_t)(var_40 | var_41);
                            var_43 = rax_0 & ((uint64_t)var_41 ^ 4294967295UL);
                            rbx_1 = var_42;
                            cc_src2_7 = cc_src2_6;
                            rax_1 = var_43;
                            var_44 = *(unsigned char *)(rcx1_0 + 17UL);
                            rbx_2 = rbx_1;
                            r11_0_be_in = var_44;
                            rax_2 = rax_1;
                            rbx_0_be = rbx_1;
                            cc_src2_0_be = cc_src2_7;
                            rax_0_be = rax_1;
                            if (var_44 == '\x00') {
                                break;
                            }
                            rbx_0 = rbx_0_be;
                            rcx1_0 = rcx1_0 + 16UL;
                            cc_src2_0 = cc_src2_0_be;
                            r11_0_in = r11_0_be_in;
                            rax_0 = rax_0_be;
                            continue;
                        }
                        var_26 = r9_2 & var_10;
                        var_27 = (unsigned char)r11_2;
                        r9_4 = var_26;
                        cc_src2_4 = cc_src2_3;
                        r9_5 = var_26;
                        _pre_phi = var_27;
                        r9_3 = var_26;
                        cc_src2_5 = cc_src2_3;
                        cc_src2_6 = cc_src2_3;
                        if ((uint64_t)(var_27 + '\xd3') != 0UL) {
                            var_40 = (uint32_t)rbx_0;
                            var_41 = (uint32_t)r9_5;
                            var_42 = (uint64_t)(var_40 | var_41);
                            var_43 = rax_0 & ((uint64_t)var_41 ^ 4294967295UL);
                            rbx_1 = var_42;
                            cc_src2_7 = cc_src2_6;
                            rax_1 = var_43;
                            var_44 = *(unsigned char *)(rcx1_0 + 17UL);
                            rbx_2 = rbx_1;
                            r11_0_be_in = var_44;
                            rax_2 = rax_1;
                            rbx_0_be = rbx_1;
                            cc_src2_0_be = cc_src2_7;
                            rax_0_be = rax_1;
                            if (var_44 == '\x00') {
                                break;
                            }
                            rbx_0 = rbx_0_be;
                            rcx1_0 = rcx1_0 + 16UL;
                            cc_src2_0 = cc_src2_0_be;
                            r11_0_in = r11_0_be_in;
                            rax_0 = rax_0_be;
                            continue;
                        }
                        if ((uint64_t)(var_27 + '\xc3') != 0UL) {
                            var_28 = (uint64_t)(uint32_t)rdi3_4 | (var_10 ^ 4294967295UL);
                            var_29 = var_28 ^ 4294967295UL;
                            r10_4 = var_29;
                            rdi3_5 = var_28;
                            var_36 = *(unsigned char *)(rcx1_0 + 17UL);
                            var_37 = (uint32_t)rax_0 & (uint32_t)rdi3_5;
                            var_38 = (uint64_t)(uint32_t)rbx_0 | (uint64_t)((uint16_t)r10_4 & (unsigned short)4095U);
                            var_39 = (uint64_t)(var_37 | (uint32_t)r9_3);
                            rbx_2 = var_38;
                            r11_0_be_in = var_36;
                            rax_2 = var_39;
                            rbx_0_be = var_38;
                            cc_src2_0_be = cc_src2_4;
                            rax_0_be = var_39;
                            if (var_36 == '\x00') {
                                break;
                            }
                            rbx_0 = rbx_0_be;
                            rcx1_0 = rcx1_0 + 16UL;
                            cc_src2_0 = cc_src2_0_be;
                            r11_0_in = r11_0_be_in;
                            rax_0 = rax_0_be;
                            continue;
                        }
                        cc_src2_7 = cc_src2_5;
                        if ((uint64_t)(_pre_phi + '\xd5') == 0UL) {
                            var_32 = (uint32_t)rbx_0;
                            var_33 = (uint32_t)r9_4;
                            var_34 = (uint64_t)(var_32 | var_33);
                            var_35 = (uint64_t)((uint32_t)rax_0 | var_33);
                            rbx_1 = var_34;
                            rax_1 = var_35;
                        }
                        var_44 = *(unsigned char *)(rcx1_0 + 17UL);
                        rbx_2 = rbx_1;
                        r11_0_be_in = var_44;
                        rax_2 = rax_1;
                        rbx_0_be = rbx_1;
                        cc_src2_0_be = cc_src2_7;
                        rax_0_be = rax_1;
                        if (var_44 == '\x00') {
                            break;
                        }
                        rbx_0 = rbx_0_be;
                        rcx1_0 = rcx1_0 + 16UL;
                        cc_src2_0 = cc_src2_0_be;
                        r11_0_in = r11_0_be_in;
                        rax_0 = rax_0_be;
                        continue;
                    }
                    r10_2 = r10_0;
                    rdi3_2 = rdi3_0;
                    if ((uint64_t)(r11_0_in + '\xfd') == 0UL) {
                        var_15 = rax_0 & var_11;
                        var_16 = (uint64_t)((uint16_t)var_15 & (unsigned short)292U) + (-1L);
                        var_17 = helper_cc_compute_c_wrapper(var_16, 1UL, cc_src2_0, 16U);
                        var_18 = (uint64_t)((((0U - (uint32_t)var_17) & (-292)) + 438U) & (-2));
                        var_19 = helper_cc_compute_c_wrapper(var_16, 1UL, var_17, 16U);
                        var_20 = ((uint64_t)((unsigned char)var_15 & '\x92') == 0UL) ? ((uint64_t)(((unsigned short)0U - (uint16_t)var_19) & (unsigned short)292U) ^ 292UL) : var_18;
                        r9_0 = var_15 | (((var_15 & 73UL) == 0UL) ? var_20 : (var_20 | 73UL));
                        cc_src2_1 = var_19;
                    }
                    var_24 = (uint64_t)*(unsigned char *)rcx1_0;
                    var_25 = (uint64_t)((uint32_t)r9_0 & (uint32_t)r10_2);
                    r10_3 = r10_2;
                    r9_2 = var_25;
                    r9_1 = var_25;
                    rdi3_3 = rdi3_2;
                    cc_src2_2 = cc_src2_1;
                    r11_1 = var_24;
                    rdi3_4 = rdi3_2;
                    cc_src2_3 = cc_src2_1;
                    r11_2 = var_24;
                    if (var_9 == 0U) {
                        var_30 = r9_1 & var_7;
                        var_31 = (unsigned char)r11_1;
                        r9_4 = var_30;
                        rdi3_5 = rdi3_3;
                        cc_src2_4 = cc_src2_2;
                        r9_5 = var_30;
                        r10_4 = r10_3;
                        _pre_phi = var_31;
                        r9_3 = var_30;
                        cc_src2_5 = cc_src2_2;
                        cc_src2_6 = cc_src2_2;
                        if ((uint64_t)(var_31 + '\xd3') == 0UL) {
                            if ((uint64_t)(var_31 + '\xc3') != 0UL) {
                                var_36 = *(unsigned char *)(rcx1_0 + 17UL);
                                var_37 = (uint32_t)rax_0 & (uint32_t)rdi3_5;
                                var_38 = (uint64_t)(uint32_t)rbx_0 | (uint64_t)((uint16_t)r10_4 & (unsigned short)4095U);
                                var_39 = (uint64_t)(var_37 | (uint32_t)r9_3);
                                rbx_2 = var_38;
                                r11_0_be_in = var_36;
                                rax_2 = var_39;
                                rbx_0_be = var_38;
                                cc_src2_0_be = cc_src2_4;
                                rax_0_be = var_39;
                                if (var_36 == '\x00') {
                                    break;
                                }
                                rbx_0 = rbx_0_be;
                                rcx1_0 = rcx1_0 + 16UL;
                                cc_src2_0 = cc_src2_0_be;
                                r11_0_in = r11_0_be_in;
                                rax_0 = rax_0_be;
                                continue;
                            }
                        }
                        var_40 = (uint32_t)rbx_0;
                        var_41 = (uint32_t)r9_5;
                        var_42 = (uint64_t)(var_40 | var_41);
                        var_43 = rax_0 & ((uint64_t)var_41 ^ 4294967295UL);
                        rbx_1 = var_42;
                        cc_src2_7 = cc_src2_6;
                        rax_1 = var_43;
                        var_44 = *(unsigned char *)(rcx1_0 + 17UL);
                        rbx_2 = rbx_1;
                        r11_0_be_in = var_44;
                        rax_2 = rax_1;
                        rbx_0_be = rbx_1;
                        cc_src2_0_be = cc_src2_7;
                        rax_0_be = rax_1;
                        if (var_44 == '\x00') {
                            break;
                        }
                        rbx_0 = rbx_0_be;
                        rcx1_0 = rcx1_0 + 16UL;
                        cc_src2_0 = cc_src2_0_be;
                        r11_0_in = r11_0_be_in;
                        rax_0 = rax_0_be;
                        continue;
                    }
                    var_26 = r9_2 & var_10;
                    var_27 = (unsigned char)r11_2;
                    r9_4 = var_26;
                    cc_src2_4 = cc_src2_3;
                    r9_5 = var_26;
                    _pre_phi = var_27;
                    r9_3 = var_26;
                    cc_src2_5 = cc_src2_3;
                    cc_src2_6 = cc_src2_3;
                    if ((uint64_t)(var_27 + '\xd3') != 0UL) {
                        var_40 = (uint32_t)rbx_0;
                        var_41 = (uint32_t)r9_5;
                        var_42 = (uint64_t)(var_40 | var_41);
                        var_43 = rax_0 & ((uint64_t)var_41 ^ 4294967295UL);
                        rbx_1 = var_42;
                        cc_src2_7 = cc_src2_6;
                        rax_1 = var_43;
                        var_44 = *(unsigned char *)(rcx1_0 + 17UL);
                        rbx_2 = rbx_1;
                        r11_0_be_in = var_44;
                        rax_2 = rax_1;
                        rbx_0_be = rbx_1;
                        cc_src2_0_be = cc_src2_7;
                        rax_0_be = rax_1;
                        if (var_44 == '\x00') {
                            break;
                        }
                        rbx_0 = rbx_0_be;
                        rcx1_0 = rcx1_0 + 16UL;
                        cc_src2_0 = cc_src2_0_be;
                        r11_0_in = r11_0_be_in;
                        rax_0 = rax_0_be;
                        continue;
                    }
                    if ((uint64_t)(var_27 + '\xc3') != 0UL) {
                        var_28 = (uint64_t)(uint32_t)rdi3_4 | (var_10 ^ 4294967295UL);
                        var_29 = var_28 ^ 4294967295UL;
                        r10_4 = var_29;
                        rdi3_5 = var_28;
                        var_36 = *(unsigned char *)(rcx1_0 + 17UL);
                        var_37 = (uint32_t)rax_0 & (uint32_t)rdi3_5;
                        var_38 = (uint64_t)(uint32_t)rbx_0 | (uint64_t)((uint16_t)r10_4 & (unsigned short)4095U);
                        var_39 = (uint64_t)(var_37 | (uint32_t)r9_3);
                        rbx_2 = var_38;
                        r11_0_be_in = var_36;
                        rax_2 = var_39;
                        rbx_0_be = var_38;
                        cc_src2_0_be = cc_src2_4;
                        rax_0_be = var_39;
                        if (var_36 == '\x00') {
                            break;
                        }
                        rbx_0 = rbx_0_be;
                        rcx1_0 = rcx1_0 + 16UL;
                        cc_src2_0 = cc_src2_0_be;
                        r11_0_in = r11_0_be_in;
                        rax_0 = rax_0_be;
                        continue;
                    }
                    cc_src2_7 = cc_src2_5;
                    if ((uint64_t)(_pre_phi + '\xd5') == 0UL) {
                        var_32 = (uint32_t)rbx_0;
                        var_33 = (uint32_t)r9_4;
                        var_34 = (uint64_t)(var_32 | var_33);
                        var_35 = (uint64_t)((uint32_t)rax_0 | var_33);
                        rbx_1 = var_34;
                        rax_1 = var_35;
                    }
                    var_44 = *(unsigned char *)(rcx1_0 + 17UL);
                    rbx_2 = rbx_1;
                    r11_0_be_in = var_44;
                    rax_2 = rax_1;
                    rbx_0_be = rbx_1;
                    cc_src2_0_be = cc_src2_7;
                    rax_0_be = rax_1;
                    if (var_44 == '\x00') {
                        break;
                    }
                    rbx_0 = rbx_0_be;
                    rcx1_0 = rcx1_0 + 16UL;
                    cc_src2_0 = cc_src2_0_be;
                    r11_0_in = r11_0_be_in;
                    rax_0 = rax_0_be;
                    continue;
                }
                if ((uint64_t)(r11_0_in + '\xfe') == 0UL) {
                    if ((rax_0 & 73UL) != 0UL) {
                        var_21 = var_11 | 73UL;
                        var_22 = (uint64_t)*(unsigned char *)rcx1_0;
                        var_23 = var_21 & r10_1;
                        r10_3 = r10_1;
                        r9_2 = var_23;
                        r9_1 = var_23;
                        rdi3_3 = rdi3_1;
                        r11_1 = var_22;
                        rdi3_4 = rdi3_1;
                        r11_2 = var_22;
                        if (var_9 == 0U) {
                            var_30 = r9_1 & var_7;
                            var_31 = (unsigned char)r11_1;
                            r9_4 = var_30;
                            rdi3_5 = rdi3_3;
                            cc_src2_4 = cc_src2_2;
                            r9_5 = var_30;
                            r10_4 = r10_3;
                            _pre_phi = var_31;
                            r9_3 = var_30;
                            cc_src2_5 = cc_src2_2;
                            cc_src2_6 = cc_src2_2;
                            if ((uint64_t)(var_31 + '\xd3') == 0UL) {
                                if ((uint64_t)(var_31 + '\xc3') != 0UL) {
                                    var_36 = *(unsigned char *)(rcx1_0 + 17UL);
                                    var_37 = (uint32_t)rax_0 & (uint32_t)rdi3_5;
                                    var_38 = (uint64_t)(uint32_t)rbx_0 | (uint64_t)((uint16_t)r10_4 & (unsigned short)4095U);
                                    var_39 = (uint64_t)(var_37 | (uint32_t)r9_3);
                                    rbx_2 = var_38;
                                    r11_0_be_in = var_36;
                                    rax_2 = var_39;
                                    rbx_0_be = var_38;
                                    cc_src2_0_be = cc_src2_4;
                                    rax_0_be = var_39;
                                    if (var_36 == '\x00') {
                                        break;
                                    }
                                    rbx_0 = rbx_0_be;
                                    rcx1_0 = rcx1_0 + 16UL;
                                    cc_src2_0 = cc_src2_0_be;
                                    r11_0_in = r11_0_be_in;
                                    rax_0 = rax_0_be;
                                    continue;
                                }
                            }
                            var_40 = (uint32_t)rbx_0;
                            var_41 = (uint32_t)r9_5;
                            var_42 = (uint64_t)(var_40 | var_41);
                            var_43 = rax_0 & ((uint64_t)var_41 ^ 4294967295UL);
                            rbx_1 = var_42;
                            cc_src2_7 = cc_src2_6;
                            rax_1 = var_43;
                            var_44 = *(unsigned char *)(rcx1_0 + 17UL);
                            rbx_2 = rbx_1;
                            r11_0_be_in = var_44;
                            rax_2 = rax_1;
                            rbx_0_be = rbx_1;
                            cc_src2_0_be = cc_src2_7;
                            rax_0_be = rax_1;
                            if (var_44 == '\x00') {
                                break;
                            }
                            rbx_0 = rbx_0_be;
                            rcx1_0 = rcx1_0 + 16UL;
                            cc_src2_0 = cc_src2_0_be;
                            r11_0_in = r11_0_be_in;
                            rax_0 = rax_0_be;
                            continue;
                        }
                        var_26 = r9_2 & var_10;
                        var_27 = (unsigned char)r11_2;
                        r9_4 = var_26;
                        cc_src2_4 = cc_src2_3;
                        r9_5 = var_26;
                        _pre_phi = var_27;
                        r9_3 = var_26;
                        cc_src2_5 = cc_src2_3;
                        cc_src2_6 = cc_src2_3;
                        if ((uint64_t)(var_27 + '\xd3') != 0UL) {
                            var_40 = (uint32_t)rbx_0;
                            var_41 = (uint32_t)r9_5;
                            var_42 = (uint64_t)(var_40 | var_41);
                            var_43 = rax_0 & ((uint64_t)var_41 ^ 4294967295UL);
                            rbx_1 = var_42;
                            cc_src2_7 = cc_src2_6;
                            rax_1 = var_43;
                            var_44 = *(unsigned char *)(rcx1_0 + 17UL);
                            rbx_2 = rbx_1;
                            r11_0_be_in = var_44;
                            rax_2 = rax_1;
                            rbx_0_be = rbx_1;
                            cc_src2_0_be = cc_src2_7;
                            rax_0_be = rax_1;
                            if (var_44 == '\x00') {
                                break;
                            }
                            rbx_0 = rbx_0_be;
                            rcx1_0 = rcx1_0 + 16UL;
                            cc_src2_0 = cc_src2_0_be;
                            r11_0_in = r11_0_be_in;
                            rax_0 = rax_0_be;
                            continue;
                        }
                        if ((uint64_t)(var_27 + '\xc3') != 0UL) {
                            var_28 = (uint64_t)(uint32_t)rdi3_4 | (var_10 ^ 4294967295UL);
                            var_29 = var_28 ^ 4294967295UL;
                            r10_4 = var_29;
                            rdi3_5 = var_28;
                            var_36 = *(unsigned char *)(rcx1_0 + 17UL);
                            var_37 = (uint32_t)rax_0 & (uint32_t)rdi3_5;
                            var_38 = (uint64_t)(uint32_t)rbx_0 | (uint64_t)((uint16_t)r10_4 & (unsigned short)4095U);
                            var_39 = (uint64_t)(var_37 | (uint32_t)r9_3);
                            rbx_2 = var_38;
                            r11_0_be_in = var_36;
                            rax_2 = var_39;
                            rbx_0_be = var_38;
                            cc_src2_0_be = cc_src2_4;
                            rax_0_be = var_39;
                            if (var_36 == '\x00') {
                                break;
                            }
                            rbx_0 = rbx_0_be;
                            rcx1_0 = rcx1_0 + 16UL;
                            cc_src2_0 = cc_src2_0_be;
                            r11_0_in = r11_0_be_in;
                            rax_0 = rax_0_be;
                            continue;
                        }
                        cc_src2_7 = cc_src2_5;
                        if ((uint64_t)(_pre_phi + '\xd5') == 0UL) {
                            var_32 = (uint32_t)rbx_0;
                            var_33 = (uint32_t)r9_4;
                            var_34 = (uint64_t)(var_32 | var_33);
                            var_35 = (uint64_t)((uint32_t)rax_0 | var_33);
                            rbx_1 = var_34;
                            rax_1 = var_35;
                        }
                        var_44 = *(unsigned char *)(rcx1_0 + 17UL);
                        rbx_2 = rbx_1;
                        r11_0_be_in = var_44;
                        rax_2 = rax_1;
                        rbx_0_be = rbx_1;
                        cc_src2_0_be = cc_src2_7;
                        rax_0_be = rax_1;
                        if (var_44 == '\x00') {
                            break;
                        }
                        rbx_0 = rbx_0_be;
                        rcx1_0 = rcx1_0 + 16UL;
                        cc_src2_0 = cc_src2_0_be;
                        r11_0_in = r11_0_be_in;
                        rax_0 = rax_0_be;
                        continue;
                    }
                }
                r10_2 = r10_0;
                rdi3_2 = rdi3_0;
                if ((uint64_t)(r11_0_in + '\xfd') == 0UL) {
                    var_15 = rax_0 & var_11;
                    var_16 = (uint64_t)((uint16_t)var_15 & (unsigned short)292U) + (-1L);
                    var_17 = helper_cc_compute_c_wrapper(var_16, 1UL, cc_src2_0, 16U);
                    var_18 = (uint64_t)((((0U - (uint32_t)var_17) & (-292)) + 438U) & (-2));
                    var_19 = helper_cc_compute_c_wrapper(var_16, 1UL, var_17, 16U);
                    var_20 = ((uint64_t)((unsigned char)var_15 & '\x92') == 0UL) ? ((uint64_t)(((unsigned short)0U - (uint16_t)var_19) & (unsigned short)292U) ^ 292UL) : var_18;
                    r9_0 = var_15 | (((var_15 & 73UL) == 0UL) ? var_20 : (var_20 | 73UL));
                    cc_src2_1 = var_19;
                }
            }
    }
    if (r8 == 0UL) {
        return rax_2;
    }
    *(uint32_t *)r8 = (uint32_t)rbx_2;
    return rax_2;
}
