typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_31_ret_type;
struct indirect_placeholder_31_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern struct indirect_placeholder_31_ret_type indirect_placeholder_31(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
typedef _Bool bool;
uint64_t bb_relpath(uint64_t rcx, uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t rax_1;
    unsigned char *_pre_phi;
    uint64_t local_sp_0_ph;
    unsigned char rax_0_ph_in;
    uint64_t r12_0_ph;
    uint64_t r12_2;
    uint64_t rbx_0_ph;
    unsigned char rax_0_in;
    uint64_t rbx_0;
    uint64_t var_23;
    unsigned char var_24;
    uint64_t local_sp_1;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    unsigned char var_30;
    uint64_t r12_1;
    uint64_t rbx_1;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t rbp_1;
    uint64_t local_sp_2;
    uint64_t var_11;
    uint64_t var_12;
    unsigned char *_cast;
    unsigned char var_13;
    uint64_t rbp_0;
    uint64_t var_14;
    uint64_t var_15;
    unsigned char *var_16;
    unsigned char var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    unsigned char var_22;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r12();
    var_3 = init_rbx();
    var_4 = init_r9();
    var_5 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    var_6 = var_0 + (-40L);
    *(uint64_t *)(var_0 + (-32L)) = rdx;
    *(uint64_t *)var_6 = rcx;
    var_7 = var_0 + (-48L);
    *(uint64_t *)var_7 = 4209874UL;
    var_8 = indirect_placeholder_1(rsi, rdi);
    var_9 = var_8 << 32UL;
    var_10 = (uint64_t)((long)var_9 >> (long)32UL);
    rax_1 = 0UL;
    if (var_9 == 0UL) {
        return rax_1;
    }
    var_11 = var_10 + rsi;
    var_12 = var_10 + rdi;
    _cast = (unsigned char *)var_11;
    var_13 = *_cast;
    _pre_phi = _cast;
    rbx_1 = var_11;
    if (var_13 == '/') {
        var_15 = var_11 + 1UL;
        var_16 = (unsigned char *)var_15;
        var_17 = *var_16;
        var_18 = var_12 + (*(unsigned char *)var_12 == '/');
        _pre_phi = var_16;
        rbp_0 = var_18;
        rbx_1 = var_15;
        rbp_1 = var_18;
        if (var_17 != '\x00') {
            var_37 = (*(unsigned char *)rbp_1 == '\x00') ? 4285347UL : rbp_1;
            var_38 = var_0 + (-56L);
            *(uint64_t *)var_38 = 4209941UL;
            var_39 = indirect_placeholder_4(var_7, var_37, var_6);
            var_40 = (uint64_t)(uint32_t)var_39;
            local_sp_2 = var_38;
            r12_2 = var_40;
            if ((uint64_t)(unsigned char)r12_2 == 0UL) {
                *(uint64_t *)(local_sp_2 + (-8L)) = 4210160UL;
                indirect_placeholder_31(0UL, 4285349UL, 4290955UL, var_4, 0UL, 36UL, var_5);
            }
            rax_1 = (uint64_t)(uint32_t)r12_2 ^ 1UL;
            return rax_1;
        }
    }
    var_14 = var_12 + (*(unsigned char *)var_12 == '/');
    rbp_0 = var_14;
    rbp_1 = var_14;
    if (var_13 != '\x00') {
        var_37 = (*(unsigned char *)rbp_1 == '\x00') ? 4285347UL : rbp_1;
        var_38 = var_0 + (-56L);
        *(uint64_t *)var_38 = 4209941UL;
        var_39 = indirect_placeholder_4(var_7, var_37, var_6);
        var_40 = (uint64_t)(uint32_t)var_39;
        local_sp_2 = var_38;
        r12_2 = var_40;
        if ((uint64_t)(unsigned char)r12_2 == 0UL) {
            *(uint64_t *)(local_sp_2 + (-8L)) = 4210160UL;
            indirect_placeholder_31(0UL, 4285349UL, 4290955UL, var_4, 0UL, 36UL, var_5);
        }
        rax_1 = (uint64_t)(uint32_t)r12_2 ^ 1UL;
        return rax_1;
    }
    var_19 = var_0 + (-56L);
    *(uint64_t *)var_19 = 4210010UL;
    var_20 = indirect_placeholder_4(var_7, 4285346UL, var_6);
    var_21 = (uint64_t)(uint32_t)var_20;
    var_22 = *_pre_phi;
    local_sp_0_ph = var_19;
    rax_0_ph_in = var_22;
    r12_0_ph = var_21;
    rbx_0_ph = rbx_1;
    local_sp_1 = var_19;
    r12_1 = var_21;
    if (var_22 == '\x00') {
        while (1U)
            {
                rax_0_in = rax_0_ph_in;
                rbx_0 = rbx_0_ph;
                local_sp_1 = local_sp_0_ph;
                r12_1 = r12_0_ph;
                while (1U)
                    {
                        if (rax_0_in != '/') {
                            loop_state_var = 0U;
                            break;
                        }
                        var_23 = rbx_0 + 1UL;
                        var_24 = *(unsigned char *)var_23;
                        rax_0_in = var_24;
                        rbx_0 = var_23;
                        if (var_24 == '\x00') {
                            continue;
                        }
                        loop_state_var = 1U;
                        break;
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 1U:
                    {
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 0U:
                    {
                        var_25 = local_sp_0_ph + 8UL;
                        var_26 = rbx_0 + 1UL;
                        var_27 = local_sp_0_ph + (-8L);
                        *(uint64_t *)var_27 = 4210118UL;
                        var_28 = indirect_placeholder_4(local_sp_0_ph, 4285345UL, var_25);
                        var_29 = r12_0_ph | (uint64_t)(uint32_t)var_28;
                        var_30 = *(unsigned char *)var_26;
                        local_sp_0_ph = var_27;
                        rax_0_ph_in = var_30;
                        r12_0_ph = var_29;
                        rbx_0_ph = var_26;
                        local_sp_1 = var_27;
                        r12_1 = var_29;
                        if (var_30 != '\x00') {
                            continue;
                        }
                        switch_state_var = 1;
                        break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
    }
    local_sp_2 = local_sp_1;
    r12_2 = r12_1;
    if (*(unsigned char *)rbp_0 == '\x00') {
        var_31 = local_sp_1 + 8UL;
        var_32 = local_sp_1 + (-8L);
        *(uint64_t *)var_32 = 4210064UL;
        var_33 = indirect_placeholder_4(local_sp_1, 4285758UL, var_31);
        var_34 = local_sp_1 + (-16L);
        *(uint64_t *)var_34 = 4210082UL;
        var_35 = indirect_placeholder_4(var_32, rbp_0, local_sp_1);
        var_36 = (uint64_t)((uint32_t)r12_1 | ((uint32_t)var_33 | (uint32_t)var_35));
        local_sp_2 = var_34;
        r12_2 = var_36;
    }
    if ((uint64_t)(unsigned char)r12_2 != 0UL) {
        *(uint64_t *)(local_sp_2 + (-8L)) = 4210160UL;
        indirect_placeholder_31(0UL, 4285349UL, 4290955UL, var_4, 0UL, 36UL, var_5);
    }
    rax_1 = (uint64_t)(uint32_t)r12_2 ^ 1UL;
}
