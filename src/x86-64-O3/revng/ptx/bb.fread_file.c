typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_74_ret_type;
struct indirect_placeholder_74_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_rax(void);
extern struct indirect_placeholder_74_ret_type indirect_placeholder_74(uint64_t param_0);
uint64_t bb_fread_file(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t local_sp_5;
    uint64_t rbx_2;
    uint64_t rax_1;
    uint64_t rbp_0;
    uint64_t r14_2;
    uint64_t var_23;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t local_sp_2;
    uint64_t local_sp_1;
    uint64_t var_26;
    uint64_t var_27;
    uint32_t *_pre_phi;
    uint64_t local_sp_0;
    uint32_t r15_0_shrunk;
    uint32_t *var_20;
    uint32_t var_21;
    uint64_t var_22;
    uint64_t rbx_0;
    uint64_t local_sp_3;
    uint64_t var_28;
    uint32_t *var_29;
    uint64_t rbx_1;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint32_t var_9;
    uint64_t var_10;
    uint64_t var_14;
    struct indirect_placeholder_74_ret_type var_15;
    uint64_t var_16;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbp();
    var_3 = init_r15();
    var_4 = init_r14();
    var_5 = init_r12();
    var_6 = init_rbx();
    var_7 = init_r13();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_7;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    *(uint64_t *)(var_0 + (-48L)) = var_6;
    *(uint64_t *)(var_0 + (-208L)) = 4226956UL;
    indirect_placeholder();
    var_8 = var_0 + (-216L);
    *(uint64_t *)var_8 = 4226966UL;
    indirect_placeholder();
    local_sp_5 = var_8;
    rbx_2 = 1024UL;
    rbp_0 = 0UL;
    r14_2 = 0UL;
    r15_0_shrunk = 12U;
    rbx_0 = 18446744073709551615UL;
    if ((int)(uint32_t)var_1 >= (int)0U) {
        var_9 = (uint32_t)((uint16_t)*(uint32_t *)(var_0 + (-192L)) & (unsigned short)61440U);
        var_10 = (uint64_t)var_9;
        var_11 = var_0 + (-224L);
        *(uint64_t *)var_11 = 4227160UL;
        indirect_placeholder();
        var_12 = *(uint64_t *)(var_0 + (-176L));
        local_sp_5 = var_11;
        var_13 = var_12 - var_10;
        rbx_2 = var_13 + 1UL;
        if ((uint64_t)((var_9 + (-32768)) & (-4096)) != 0UL & (long)var_12 <= (long)var_10 & var_13 != 18446744073709551615UL) {
            *(uint64_t *)(var_0 + (-232L)) = 4227205UL;
            indirect_placeholder();
            *(uint32_t *)var_10 = 12U;
            return r14_2;
        }
    }
    var_14 = local_sp_5 + (-8L);
    *(uint64_t *)var_14 = 4227003UL;
    var_15 = indirect_placeholder_74(rbx_2);
    var_16 = var_15.field_0;
    local_sp_3 = var_14;
    rax_1 = var_16;
    rbx_1 = rbx_2;
    if (var_16 == 0UL) {
        return r14_2;
    }
    while (1U)
        {
            var_17 = rbx_1 - rbp_0;
            var_18 = local_sp_3 + (-8L);
            *(uint64_t *)var_18 = 4227076UL;
            indirect_placeholder();
            var_19 = rbp_0 + rax_1;
            local_sp_0 = var_18;
            rbp_0 = var_19;
            r14_2 = rax_1;
            if (var_17 != rax_1) {
                *(uint64_t *)(local_sp_3 + (-16L)) = 4227245UL;
                indirect_placeholder();
                var_20 = (uint32_t *)rax_1;
                var_21 = *var_20;
                var_22 = local_sp_3 + (-24L);
                *(uint64_t *)var_22 = 4227256UL;
                indirect_placeholder();
                _pre_phi = var_20;
                local_sp_0 = var_22;
                r15_0_shrunk = var_21;
                if ((uint64_t)(uint32_t)rax_1 != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                if ((rbx_1 + (-1L)) <= var_19) {
                    loop_state_var = 2U;
                    break;
                }
                var_23 = var_19 + 1UL;
                *(uint64_t *)(local_sp_3 + (-32L)) = 4227334UL;
                indirect_placeholder_1(rax_1, var_23);
                loop_state_var = 2U;
                break;
            }
            if (rbx_1 != 18446744073709551615UL) {
                _pre_phi = (uint32_t *)rax_1;
                loop_state_var = 0U;
                break;
            }
            var_24 = rbx_1 >> 1UL;
            rax_1 = var_24;
            if (rbx_1 < (var_24 ^ (-1L))) {
                var_26 = rbx_1 + var_24;
                var_27 = local_sp_3 + (-16L);
                *(uint64_t *)var_27 = 4227038UL;
                indirect_placeholder_1(rax_1, var_26);
                local_sp_1 = var_27;
                rbx_0 = var_26;
                local_sp_2 = var_27;
                if (var_24 != 0UL) {
                    loop_state_var = 1U;
                    break;
                }
            }
            var_25 = local_sp_3 + (-16L);
            *(uint64_t *)var_25 = 4227130UL;
            indirect_placeholder_1(rax_1, 18446744073709551615UL);
            local_sp_1 = var_25;
            local_sp_2 = var_25;
            if (var_24 != 0UL) {
                loop_state_var = 1U;
                break;
            }
            local_sp_3 = local_sp_1;
            rbx_1 = rbx_0;
            continue;
        }
    switch (loop_state_var) {
      case 2U:
        {
            *(unsigned char *)(var_19 + rax_1) = (unsigned char)'\x00';
            *(uint64_t *)rsi = var_19;
        }
        break;
      case 1U:
      case 0U:
        {
            switch (loop_state_var) {
              case 0U:
                {
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4227297UL;
                    indirect_placeholder();
                    *(uint64_t *)(local_sp_0 + (-16L)) = 4227302UL;
                    indirect_placeholder();
                    *_pre_phi = r15_0_shrunk;
                }
                break;
              case 1U:
                {
                    var_28 = local_sp_2 + (-8L);
                    *(uint64_t *)var_28 = 4227317UL;
                    indirect_placeholder();
                    var_29 = (uint32_t *)var_24;
                    _pre_phi = var_29;
                    local_sp_0 = var_28;
                    r15_0_shrunk = *var_29;
                }
                break;
            }
        }
        break;
    }
}
