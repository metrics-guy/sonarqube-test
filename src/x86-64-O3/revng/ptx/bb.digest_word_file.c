typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_71_ret_type;
struct indirect_placeholder_72_ret_type;
struct indirect_placeholder_71_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_72_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern struct indirect_placeholder_71_ret_type indirect_placeholder_71(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_72_ret_type indirect_placeholder_72(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
typedef _Bool bool;
void bb_digest_word_file(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_72_ret_type var_24;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t *var_10;
    struct indirect_placeholder_71_ret_type var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t *var_16;
    uint64_t *var_17;
    uint64_t *var_18;
    uint64_t var_19;
    uint64_t local_sp_3;
    uint64_t rbx_3;
    uint64_t rcx_0;
    uint64_t rbp_0;
    uint64_t rdi1_2;
    uint64_t rdx_0;
    uint64_t local_sp_2;
    uint64_t rdi1_0;
    uint64_t rsi2_2;
    uint64_t local_sp_0;
    uint64_t rsi2_0;
    uint64_t rdx_1;
    uint64_t rbx_0;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t rbx_1;
    uint64_t var_22;
    uint64_t rcx_1;
    uint64_t rbp_1;
    uint64_t rbx_2;
    uint64_t rdi1_1;
    uint64_t local_sp_1;
    uint64_t rsi2_1;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_23;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t rdx_2;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r12();
    var_3 = init_rbx();
    var_4 = init_cc_src2();
    var_5 = init_r13();
    *(uint64_t *)(var_0 + (-8L)) = var_5;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    var_6 = rsi + 8UL;
    *(uint64_t *)(var_0 + (-32L)) = var_3;
    var_7 = var_0 + (-56L);
    var_8 = var_0 + (-48L);
    var_9 = var_0 + (-64L);
    var_10 = (uint64_t *)var_9;
    *var_10 = 4216687UL;
    var_11 = indirect_placeholder_71(var_8, rdi, var_6, var_7);
    var_12 = var_11.field_1;
    var_13 = var_11.field_2;
    var_14 = *var_10;
    var_15 = *(uint64_t *)var_7;
    var_16 = (uint64_t *)rsi;
    *var_16 = 0UL;
    var_17 = (uint64_t *)var_6;
    *var_17 = 0UL;
    var_18 = (uint64_t *)(rsi + 16UL);
    *var_18 = 0UL;
    var_19 = helper_cc_compute_c_wrapper(var_14 - var_15, var_15, var_4, 17U);
    local_sp_3 = var_9;
    rbp_0 = var_14;
    rdx_0 = var_15;
    rdi1_0 = 0UL;
    local_sp_0 = var_9;
    rsi2_0 = 0UL;
    if (var_19 == 0UL) {
        *(uint64_t *)(local_sp_3 + (-8L)) = 4216778UL;
        indirect_placeholder();
        return;
    }
    rcx_0 = var_11.field_0;
    while (1U)
        {
            local_sp_3 = local_sp_0;
            rdi1_2 = rdi1_0;
            local_sp_2 = local_sp_0;
            rsi2_2 = rsi2_0;
            rdx_1 = rdx_0;
            rbx_0 = rbp_0;
            rcx_1 = rcx_0;
            rbp_1 = rbp_0;
            rdi1_1 = rdi1_0;
            local_sp_1 = local_sp_0;
            rsi2_1 = rsi2_0;
            rdx_2 = rdx_0;
            while (1U)
                {
                    rbx_1 = rbx_0;
                    rbx_3 = rbx_0;
                    if (*(unsigned char *)rbx_0 != '\n') {
                        loop_state_var = 1U;
                        break;
                    }
                    var_20 = rbx_0 + 1UL;
                    var_21 = helper_cc_compute_c_wrapper(var_20 - rdx_0, rdx_0, var_4, 17U);
                    rbx_0 = var_20;
                    rbx_1 = var_20;
                    if (var_21 == 0UL) {
                        continue;
                    }
                    loop_state_var = 0U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    if (var_20 > rbp_0) {
                        switch_state_var = 1;
                        break;
                    }
                }
                break;
              case 1U:
                {
                    var_22 = helper_cc_compute_c_wrapper(rbp_0 - rbx_0, rbx_0, var_4, 17U);
                    if (var_22 != 0UL) {
                        var_33 = helper_cc_compute_c_wrapper(rbx_3 - rdx_2, rdx_2, var_4, 17U);
                        local_sp_3 = local_sp_2;
                        rcx_0 = rcx_1;
                        rdx_0 = rdx_2;
                        rdi1_0 = rdi1_2;
                        local_sp_0 = local_sp_2;
                        rsi2_0 = rsi2_2;
                        if (var_33 == 0UL) {
                            switch_state_var = 1;
                            break;
                        }
                        var_34 = rbx_3 + 1UL;
                        var_35 = helper_cc_compute_c_wrapper(var_34 - rdx_2, rdx_2, var_4, 17U);
                        rbp_0 = var_34;
                        if (var_35 != 0UL) {
                            continue;
                        }
                        switch_state_var = 1;
                        break;
                    }
                    rbx_2 = rbx_1;
                    if (*var_17 == rsi2_0) {
                        var_23 = local_sp_0 + (-8L);
                        *(uint64_t *)var_23 = 4216869UL;
                        var_24 = indirect_placeholder_72(rcx_0, rbp_0, var_12, rbx_1, 16UL, rdi1_0, var_13);
                        var_25 = var_24.field_0;
                        var_26 = var_24.field_1;
                        var_27 = var_24.field_2;
                        var_28 = *var_18;
                        var_29 = *(uint64_t *)local_sp_0;
                        *var_16 = var_25;
                        rdx_1 = var_29;
                        rbp_1 = var_26;
                        rbx_2 = var_27;
                        rdi1_1 = var_25;
                        local_sp_1 = var_23;
                        rsi2_1 = var_28;
                    }
                    var_30 = rsi2_1 + 1UL;
                    var_31 = rbx_2 - rbp_1;
                    var_32 = (rsi2_1 << 4UL) + rdi1_1;
                    *(uint64_t *)var_32 = rbp_1;
                    *(uint64_t *)(var_32 + 8UL) = var_31;
                    *var_18 = var_30;
                    rbx_3 = rbx_2;
                    rdi1_2 = rdi1_1;
                    local_sp_2 = local_sp_1;
                    rsi2_2 = var_30;
                    rcx_1 = var_32;
                    rdx_2 = rdx_1;
                    var_33 = helper_cc_compute_c_wrapper(rbx_3 - rdx_2, rdx_2, var_4, 17U);
                    local_sp_3 = local_sp_2;
                    rcx_0 = rcx_1;
                    rdx_0 = rdx_2;
                    rdi1_0 = rdi1_2;
                    local_sp_0 = local_sp_2;
                    rsi2_0 = rsi2_2;
                    if (var_33 == 0UL) {
                        switch_state_var = 1;
                        break;
                    }
                    var_34 = rbx_3 + 1UL;
                    var_35 = helper_cc_compute_c_wrapper(var_34 - rdx_2, rdx_2, var_4, 17U);
                    rbp_0 = var_34;
                    if (var_35 != 0UL) {
                        continue;
                    }
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    *(uint64_t *)(local_sp_3 + (-8L)) = 4216778UL;
    indirect_placeholder();
    return;
}
