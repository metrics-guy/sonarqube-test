typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct helper_pxor_xmm_wrapper_ret_type;
struct type_3;
struct type_5;
struct helper_pcmpeqb_xmm_wrapper_ret_type;
struct helper_pandn_xmm_wrapper_ret_type;
struct helper_pcmpeqb_xmm_wrapper_321_ret_type;
struct helper_pandn_xmm_wrapper_322_ret_type;
struct indirect_placeholder_75_ret_type;
struct helper_pxor_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_3 {
};
struct type_5 {
};
struct helper_pcmpeqb_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_pandn_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_pcmpeqb_xmm_wrapper_321_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_pandn_xmm_wrapper_322_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_75_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t init_state_0x8598(void);
extern uint64_t init_state_0x85a0(void);
extern struct helper_pxor_xmm_wrapper_ret_type helper_pxor_xmm_wrapper(struct type_3 *param_0, struct type_5 *param_1, struct type_5 *param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_r12(void);
extern uint32_t init_state_0x82fc(void);
extern uint64_t indirect_placeholder_10(uint64_t param_0);
extern struct helper_pcmpeqb_xmm_wrapper_ret_type helper_pcmpeqb_xmm_wrapper(struct type_3 *param_0, struct type_5 *param_1, struct type_5 *param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct helper_pandn_xmm_wrapper_ret_type helper_pandn_xmm_wrapper(struct type_3 *param_0, struct type_5 *param_1, struct type_5 *param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct helper_pcmpeqb_xmm_wrapper_321_ret_type helper_pcmpeqb_xmm_wrapper_321(struct type_3 *param_0, struct type_5 *param_1, struct type_5 *param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct helper_pandn_xmm_wrapper_322_ret_type helper_pandn_xmm_wrapper_322(struct type_3 *param_0, struct type_5 *param_1, struct type_5 *param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_75_ret_type indirect_placeholder_75(uint64_t param_0);
void bb_fix_output_parameters(void) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint32_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t local_sp_2;
    struct helper_pcmpeqb_xmm_wrapper_ret_type var_52;
    struct helper_pxor_xmm_wrapper_ret_type var_45;
    uint64_t var_10;
    uint64_t rbp_0;
    uint64_t local_sp_0;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t local_sp_7;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    struct helper_pandn_xmm_wrapper_ret_type var_53;
    uint64_t var_54;
    struct helper_pcmpeqb_xmm_wrapper_ret_type var_55;
    struct helper_pandn_xmm_wrapper_ret_type var_56;
    uint64_t var_57;
    struct helper_pcmpeqb_xmm_wrapper_ret_type var_58;
    struct helper_pandn_xmm_wrapper_ret_type var_59;
    uint64_t var_60;
    struct helper_pcmpeqb_xmm_wrapper_ret_type var_61;
    struct helper_pandn_xmm_wrapper_ret_type var_62;
    uint64_t var_63;
    struct helper_pcmpeqb_xmm_wrapper_ret_type var_64;
    struct helper_pandn_xmm_wrapper_ret_type var_65;
    uint64_t var_66;
    struct helper_pcmpeqb_xmm_wrapper_ret_type var_67;
    struct helper_pandn_xmm_wrapper_ret_type var_68;
    uint64_t var_69;
    struct helper_pcmpeqb_xmm_wrapper_ret_type var_70;
    uint64_t var_71;
    uint64_t var_72;
    struct helper_pcmpeqb_xmm_wrapper_321_ret_type var_73;
    uint64_t var_74;
    uint64_t var_75;
    struct helper_pandn_xmm_wrapper_ret_type var_76;
    uint64_t var_77;
    uint64_t var_78;
    struct helper_pandn_xmm_wrapper_322_ret_type var_79;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t local_sp_3;
    uint64_t rdx_0;
    uint32_t var_37;
    uint64_t local_sp_5;
    uint64_t rdi_0;
    uint64_t local_sp_1;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    unsigned char var_41;
    uint64_t var_42;
    uint64_t rcx_2;
    uint64_t local_sp_4_ph;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t local_sp_4;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t rcx_0;
    uint64_t rdi_1;
    uint64_t rcx_1;
    unsigned char var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_36;
    uint32_t var_8;
    uint64_t local_sp_6;
    uint64_t var_16;
    uint64_t var_17;
    struct indirect_placeholder_75_ret_type var_18;
    unsigned char var_19;
    uint64_t var_9;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r12();
    var_3 = init_rbx();
    var_4 = init_state_0x82fc();
    var_5 = init_state_0x8598();
    var_6 = init_state_0x85a0();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    var_7 = var_0 + (-56L);
    local_sp_2 = var_7;
    rbp_0 = 0UL;
    local_sp_7 = var_7;
    var_15 = 0UL;
    rdx_0 = 4333747UL;
    rdi_0 = 36UL;
    rcx_2 = 0UL;
    rcx_0 = 18446744073709551615UL;
    rcx_1 = 0UL;
    local_sp_6 = var_7;
    if (*(unsigned char *)4377482UL == '\x00') {
        var_8 = *(uint32_t *)4376088UL;
        *(uint64_t *)4376152UL = 0UL;
        if (var_8 == 0U) {
            var_13 = (uint64_t)*(uint32_t *)4376088UL;
            var_14 = rbp_0 + 1UL;
            var_15 = var_12;
            rbp_0 = var_14;
            do {
                var_9 = local_sp_7 + (-8L);
                *(uint64_t *)var_9 = 4208655UL;
                indirect_placeholder();
                local_sp_0 = var_9;
                if (*(uint64_t *)((rbp_0 << 3UL) + *(uint64_t *)4376072UL) == 0UL) {
                    var_10 = local_sp_7 + (-16L);
                    *(uint64_t *)var_10 = 4208689UL;
                    indirect_placeholder();
                    local_sp_0 = var_10;
                }
                var_11 = *(uint64_t *)4376152UL;
                var_12 = var_11;
                local_sp_6 = local_sp_0;
                local_sp_7 = local_sp_0;
                if ((long)var_11 < (long)0UL) {
                    *(uint64_t *)4376152UL = 0UL;
                    var_12 = 0UL;
                }
                var_13 = (uint64_t)*(uint32_t *)4376088UL;
                var_14 = rbp_0 + 1UL;
                var_15 = var_12;
                rbp_0 = var_14;
            } while (var_14 >= var_13);
        }
        var_16 = var_15 + 2UL;
        *(uint64_t *)4376152UL = (var_15 + 1UL);
        var_17 = local_sp_6 + (-8L);
        *(uint64_t *)var_17 = 4208536UL;
        var_18 = indirect_placeholder_75(var_16);
        var_19 = *(unsigned char *)4377482UL;
        *(uint64_t *)4375584UL = var_18.field_0;
        local_sp_2 = var_17;
        local_sp_3 = var_17;
        if (var_19 == '\x00') {
            local_sp_3 = local_sp_2;
            local_sp_4_ph = local_sp_2;
            if (*(unsigned char *)4377481UL == '\x00') {
                var_21 = *(uint64_t *)4375320UL;
                local_sp_4 = local_sp_4_ph;
            } else {
                local_sp_4_ph = local_sp_3;
                local_sp_4 = local_sp_3;
                if (*(unsigned char *)4377480UL == '\x00') {
                    var_20 = *(uint64_t *)4375320UL - (*(uint64_t *)4375312UL + *(uint64_t *)4376152UL);
                    *(uint64_t *)4375320UL = var_20;
                    var_21 = var_20;
                } else {
                    var_21 = *(uint64_t *)4375320UL;
                    local_sp_4 = local_sp_4_ph;
                }
            }
        } else {
            local_sp_4_ph = local_sp_3;
            local_sp_4 = local_sp_3;
            if (*(unsigned char *)4377480UL == '\x00') {
                var_20 = *(uint64_t *)4375320UL - (*(uint64_t *)4375312UL + *(uint64_t *)4376152UL);
                *(uint64_t *)4375320UL = var_20;
                var_21 = var_20;
            } else {
                var_21 = *(uint64_t *)4375320UL;
                local_sp_4 = local_sp_4_ph;
            }
        }
    } else {
        local_sp_3 = local_sp_2;
        local_sp_4_ph = local_sp_2;
        if (*(unsigned char *)4377481UL == '\x00') {
            var_21 = *(uint64_t *)4375320UL;
            local_sp_4 = local_sp_4_ph;
        } else {
            local_sp_4_ph = local_sp_3;
            local_sp_4 = local_sp_3;
            if (*(unsigned char *)4377480UL == '\x00') {
                var_20 = *(uint64_t *)4375320UL - (*(uint64_t *)4375312UL + *(uint64_t *)4376152UL);
                *(uint64_t *)4375320UL = var_20;
                var_21 = var_20;
            } else {
                var_21 = *(uint64_t *)4375320UL;
                local_sp_4 = local_sp_4_ph;
            }
        }
    }
    var_22 = var_21;
    local_sp_5 = local_sp_4;
    if ((long)var_21 > (long)18446744073709551615UL) {
        *(uint64_t *)4375320UL = 0UL;
        var_22 = 0UL;
    }
    var_23 = *(uint64_t *)4375304UL;
    var_24 = (uint64_t)((long)((var_22 >> 63UL) + var_22) >> (long)1UL);
    var_25 = var_24 - *(uint64_t *)4375312UL;
    *(uint64_t *)4375752UL = var_24;
    *(uint64_t *)4375744UL = var_25;
    *(uint64_t *)4375736UL = var_24;
    rdi_1 = var_23;
    if (var_23 == 0UL) {
        *(uint64_t *)4375304UL = 0UL;
        var_30 = *(uint64_t *)4375728UL;
    } else {
        var_26 = (uint64_t)var_4;
        while (rcx_0 != 0UL)
            {
                var_27 = *(unsigned char *)rdi_1;
                var_28 = rcx_0 + (-1L);
                rcx_0 = var_28;
                rcx_1 = var_28;
                if (var_27 == '\x00') {
                    break;
                }
                rdi_1 = rdi_1 + var_26;
            }
        var_29 = 18446744073709551614UL - (-2L);
        *(uint64_t *)4375728UL = var_29;
        var_30 = var_29;
    }
    var_31 = var_30 << 1UL;
    if (*(unsigned char *)4375328UL == '\x00') {
        *(uint64_t *)4375736UL = (var_24 - (var_31 | 1UL));
    } else {
        var_32 = var_25 - var_31;
        var_33 = ((long)var_32 > (long)0UL) ? var_32 : 0UL;
        *(uint64_t *)4375736UL = (var_24 - var_31);
        *(uint64_t *)4375744UL = var_33;
    }
    var_34 = (uint64_t)(uint32_t)rcx_2;
    var_35 = local_sp_5 + (-8L);
    *(uint64_t *)var_35 = 4208183UL;
    var_36 = indirect_placeholder_10(var_34);
    *(unsigned char *)(rcx_2 + 4375776UL) = ((uint64_t)(uint32_t)var_36 != 0UL);
    local_sp_1 = var_35;
    local_sp_5 = var_35;
    while (rcx_2 != 255UL)
        {
            rcx_2 = rcx_2 + 1UL;
            var_34 = (uint64_t)(uint32_t)rcx_2;
            var_35 = local_sp_5 + (-8L);
            *(uint64_t *)var_35 = 4208183UL;
            var_36 = indirect_placeholder_10(var_34);
            *(unsigned char *)(rcx_2 + 4375776UL) = ((uint64_t)(uint32_t)var_36 != 0UL);
            local_sp_1 = var_35;
            local_sp_5 = var_35;
        }
    var_37 = *(uint32_t *)4377476UL;
    *(unsigned char *)4375788UL = (unsigned char)'\x01';
    if ((uint64_t)(var_37 + (-2)) == 0UL) {
        *(unsigned char *)4375810UL = (unsigned char)'\x01';
    } else {
        if ((uint64_t)(var_37 + (-3)) != 0UL) {
            var_38 = local_sp_1 + (-8L);
            *(uint64_t *)var_38 = 4208261UL;
            var_39 = indirect_placeholder_10(rdi_0);
            var_40 = rdx_0 + 1UL;
            var_41 = *(unsigned char *)var_40;
            var_42 = (uint64_t)var_41;
            *(unsigned char *)((uint64_t)(unsigned char)var_39 + 4375776UL) = (unsigned char)'\x01';
            rdx_0 = var_40;
            local_sp_1 = var_38;
            while ((uint64_t)var_41 != 0UL)
                {
                    rdi_0 = (uint64_t)(uint32_t)var_42;
                    var_38 = local_sp_1 + (-8L);
                    *(uint64_t *)var_38 = 4208261UL;
                    var_39 = indirect_placeholder_10(rdi_0);
                    var_40 = rdx_0 + 1UL;
                    var_41 = *(unsigned char *)var_40;
                    var_42 = (uint64_t)var_41;
                    *(unsigned char *)((uint64_t)(unsigned char)var_39 + 4375776UL) = (unsigned char)'\x01';
                    rdx_0 = var_40;
                    local_sp_1 = var_38;
                }
            var_43 = *(uint64_t *)4334816UL;
            var_44 = *(uint64_t *)4334824UL;
            var_45 = helper_pxor_xmm_wrapper((struct type_3 *)(0UL), (struct type_5 *)(840UL), (struct type_5 *)(840UL), var_5, var_6);
            var_46 = var_45.field_0;
            var_47 = var_45.field_1;
            var_48 = *(uint64_t *)4334528UL;
            var_49 = *(uint64_t *)4334536UL;
            var_50 = *(uint64_t *)4334928UL;
            var_51 = *(uint64_t *)4334936UL;
            var_52 = helper_pcmpeqb_xmm_wrapper((struct type_3 *)(0UL), (struct type_5 *)(904UL), (struct type_5 *)(840UL), var_46, var_47, var_43, var_44);
            var_53 = helper_pandn_xmm_wrapper((struct type_3 *)(0UL), (struct type_5 *)(904UL), (struct type_5 *)(776UL), var_48, var_49, var_52.field_0, var_52.field_1);
            var_54 = var_53.field_1;
            *(uint64_t *)4375904UL = var_53.field_0;
            *(uint64_t *)4375912UL = var_54;
            var_55 = helper_pcmpeqb_xmm_wrapper((struct type_3 *)(0UL), (struct type_5 *)(904UL), (struct type_5 *)(840UL), var_46, var_47, *(uint64_t *)4334832UL, *(uint64_t *)4334840UL);
            var_56 = helper_pandn_xmm_wrapper((struct type_3 *)(0UL), (struct type_5 *)(904UL), (struct type_5 *)(776UL), var_48, var_49, var_55.field_0, var_55.field_1);
            var_57 = var_56.field_1;
            *(uint64_t *)4375920UL = var_56.field_0;
            *(uint64_t *)4375928UL = var_57;
            var_58 = helper_pcmpeqb_xmm_wrapper((struct type_3 *)(0UL), (struct type_5 *)(904UL), (struct type_5 *)(840UL), var_46, var_47, *(uint64_t *)4334848UL, *(uint64_t *)4334856UL);
            var_59 = helper_pandn_xmm_wrapper((struct type_3 *)(0UL), (struct type_5 *)(904UL), (struct type_5 *)(776UL), var_48, var_49, var_58.field_0, var_58.field_1);
            var_60 = var_59.field_1;
            *(uint64_t *)4375936UL = var_59.field_0;
            *(uint64_t *)4375944UL = var_60;
            var_61 = helper_pcmpeqb_xmm_wrapper((struct type_3 *)(0UL), (struct type_5 *)(904UL), (struct type_5 *)(840UL), var_46, var_47, *(uint64_t *)4334864UL, *(uint64_t *)4334872UL);
            var_62 = helper_pandn_xmm_wrapper((struct type_3 *)(0UL), (struct type_5 *)(904UL), (struct type_5 *)(776UL), var_48, var_49, var_61.field_0, var_61.field_1);
            var_63 = var_62.field_1;
            *(uint64_t *)4375952UL = var_62.field_0;
            *(uint64_t *)4375960UL = var_63;
            var_64 = helper_pcmpeqb_xmm_wrapper((struct type_3 *)(0UL), (struct type_5 *)(904UL), (struct type_5 *)(840UL), var_46, var_47, *(uint64_t *)4334880UL, *(uint64_t *)4334888UL);
            var_65 = helper_pandn_xmm_wrapper((struct type_3 *)(0UL), (struct type_5 *)(904UL), (struct type_5 *)(776UL), var_48, var_49, var_64.field_0, var_64.field_1);
            var_66 = var_65.field_1;
            *(uint64_t *)4375968UL = var_65.field_0;
            *(uint64_t *)4375976UL = var_66;
            var_67 = helper_pcmpeqb_xmm_wrapper((struct type_3 *)(0UL), (struct type_5 *)(904UL), (struct type_5 *)(840UL), var_46, var_47, *(uint64_t *)4334896UL, *(uint64_t *)4334904UL);
            var_68 = helper_pandn_xmm_wrapper((struct type_3 *)(0UL), (struct type_5 *)(904UL), (struct type_5 *)(776UL), var_48, var_49, var_67.field_0, var_67.field_1);
            var_69 = var_68.field_1;
            *(uint64_t *)4375984UL = var_68.field_0;
            *(uint64_t *)4375992UL = var_69;
            var_70 = helper_pcmpeqb_xmm_wrapper((struct type_3 *)(0UL), (struct type_5 *)(904UL), (struct type_5 *)(840UL), var_46, var_47, *(uint64_t *)4334912UL, *(uint64_t *)4334920UL);
            var_71 = var_70.field_0;
            var_72 = var_70.field_1;
            var_73 = helper_pcmpeqb_xmm_wrapper_321((struct type_3 *)(0UL), (struct type_5 *)(840UL), (struct type_5 *)(968UL), var_50, var_51, var_46, var_47);
            var_74 = var_73.field_0;
            var_75 = var_73.field_1;
            var_76 = helper_pandn_xmm_wrapper((struct type_3 *)(0UL), (struct type_5 *)(904UL), (struct type_5 *)(776UL), var_48, var_49, var_71, var_72);
            var_77 = var_76.field_0;
            var_78 = var_76.field_1;
            var_79 = helper_pandn_xmm_wrapper_322((struct type_3 *)(0UL), (struct type_5 *)(840UL), (struct type_5 *)(776UL), var_48, var_49, var_74, var_75);
            var_80 = var_79.field_0;
            var_81 = var_79.field_1;
            *(uint64_t *)4376000UL = var_77;
            *(uint64_t *)4376008UL = var_78;
            *(uint64_t *)4376016UL = var_80;
            *(uint64_t *)4376024UL = var_81;
        }
    }
    return;
}
