typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_long_double_format_isra_0_ret_type;
struct indirect_placeholder_54_ret_type;
struct indirect_placeholder_55_ret_type;
struct indirect_placeholder_56_ret_type;
struct indirect_placeholder_57_ret_type;
struct indirect_placeholder_58_ret_type;
struct bb_long_double_format_isra_0_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_54_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
};
struct indirect_placeholder_55_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_56_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
};
struct indirect_placeholder_57_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
};
struct indirect_placeholder_58_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern void indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern uint64_t init_r9(void);
extern uint64_t init_rcx(void);
extern struct indirect_placeholder_54_ret_type indirect_placeholder_54(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_55_ret_type indirect_placeholder_55(uint64_t param_0);
extern struct indirect_placeholder_56_ret_type indirect_placeholder_56(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_57_ret_type indirect_placeholder_57(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_58_ret_type indirect_placeholder_58(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
typedef _Bool bool;
struct bb_long_double_format_isra_0_ret_type bb_long_double_format_isra_0(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_54_ret_type var_35;
    uint64_t r13_2;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t storemerge6;
    uint64_t rdi5_0;
    uint64_t storemerge5;
    uint64_t var_29;
    struct bb_long_double_format_isra_0_ret_type mrv3;
    uint64_t local_sp_0;
    uint64_t r15_0;
    uint64_t rbx_0;
    uint64_t r14_0;
    uint64_t r12_0;
    uint64_t r13_0;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_30;
    struct indirect_placeholder_55_ret_type var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    struct bb_long_double_format_isra_0_ret_type mrv;
    struct bb_long_double_format_isra_0_ret_type mrv1;
    struct bb_long_double_format_isra_0_ret_type mrv2;
    uint64_t r14_3;
    uint64_t local_sp_1;
    uint64_t r15_1;
    uint64_t r14_1;
    uint64_t r12_1;
    uint64_t r13_1;
    struct indirect_placeholder_56_ret_type var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t local_sp_3;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t storemerge;
    uint64_t var_22;
    unsigned char *_pre_phi142;
    uint64_t local_sp_2;
    uint64_t var_23;
    uint64_t var_24;
    unsigned char var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t r15_2;
    uint64_t r14_2;
    uint64_t r12_2;
    uint64_t r13_3;
    struct indirect_placeholder_57_ret_type var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t r14_4;
    uint64_t rax_0;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    unsigned char *var_19;
    struct indirect_placeholder_58_ret_type var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rcx();
    var_2 = init_rbp();
    var_3 = init_r15();
    var_4 = init_r14();
    var_5 = init_r12();
    var_6 = init_rbx();
    var_7 = init_r9();
    var_8 = init_r13();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_8;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    *(uint64_t *)(var_0 + (-48L)) = var_6;
    *(uint64_t *)(var_0 + (-72L)) = rsi;
    *(uint64_t *)(var_0 + (-64L)) = rdx;
    storemerge6 = 1UL;
    storemerge5 = 0UL;
    r15_0 = var_3;
    rbx_0 = 0UL;
    r13_0 = var_8;
    r14_3 = var_4;
    storemerge = 1UL;
    r12_2 = var_5;
    rax_0 = 0UL;
    while (1U)
        {
            r14_0 = r14_3;
            r14_4 = r14_3;
            switch_state_var = 0;
            switch (*(unsigned char *)(rax_0 + rdi)) {
              case '%':
                {
                    var_15 = rax_0 + 1UL;
                    r14_4 = var_15;
                    storemerge = 2UL;
                    if (*(unsigned char *)(var_15 + rdi) == '%') {
                        rax_0 = rax_0 + storemerge;
                        r14_3 = r14_4;
                        rbx_0 = rbx_0 + 1UL;
                        continue;
                    }
                    *(uint64_t *)(var_0 + (-80L)) = 4207634UL;
                    indirect_placeholder();
                    var_16 = var_15 + rax_0;
                    var_17 = var_0 + (-88L);
                    *(uint64_t *)var_17 = 4207652UL;
                    indirect_placeholder();
                    var_18 = rax_0 + var_16;
                    var_19 = (unsigned char *)(var_18 + rdi);
                    _pre_phi142 = var_19;
                    local_sp_2 = var_17;
                    r13_2 = var_18;
                    if (*var_19 != '.') {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    var_20 = var_18 + 1UL;
                    var_21 = var_0 + (-96L);
                    *(uint64_t *)var_21 = 4207683UL;
                    indirect_placeholder();
                    var_22 = var_20 + rax_0;
                    _pre_phi142 = (unsigned char *)(var_22 + rdi);
                    local_sp_2 = var_21;
                    r13_2 = var_22;
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case '\x00':
                {
                    *(uint64_t *)(var_0 + (-80L)) = 4207893UL;
                    var_9 = indirect_placeholder_58(rdi, var_3, r14_3, var_5, rbx_0, rdi, var_8);
                    var_10 = var_9.field_0;
                    var_11 = var_9.field_2;
                    var_12 = var_9.field_4;
                    var_13 = var_9.field_7;
                    var_14 = var_0 + (-88L);
                    *(uint64_t *)var_14 = 4207915UL;
                    indirect_placeholder_4(0UL, var_10, 4261630UL, var_12, 1UL, 0UL, var_13);
                    local_sp_0 = var_14;
                    r12_0 = var_11;
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
              default:
                {
                    rax_0 = rax_0 + storemerge;
                    r14_3 = r14_4;
                    rbx_0 = rbx_0 + 1UL;
                    continue;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 1U:
        {
            *(uint64_t *)(local_sp_0 + (-8L)) = 4207923UL;
            var_35 = indirect_placeholder_54(rdi, r15_0, r14_0, r12_0, rbx_0, rdi, r13_0);
            var_36 = var_35.field_0;
            var_37 = var_35.field_2;
            var_38 = var_35.field_4;
            var_39 = var_35.field_7;
            var_40 = local_sp_0 + (-16L);
            *(uint64_t *)var_40 = 4207945UL;
            indirect_placeholder_4(0UL, var_36, 4259960UL, var_38, 1UL, 0UL, var_39);
            local_sp_1 = var_40;
            r15_1 = r15_0;
            r14_1 = r14_0;
            r12_1 = var_37;
            r13_1 = r13_0;
        }
        break;
      case 0U:
        {
            var_23 = r13_2 + (*_pre_phi142 == 'L');
            var_24 = var_23 + rdi;
            var_25 = *(unsigned char *)var_24;
            var_26 = (uint64_t)var_25;
            r14_0 = var_24;
            r13_0 = r13_2;
            r15_1 = var_23;
            r14_1 = var_24;
            r13_1 = r13_2;
            local_sp_3 = local_sp_2;
            r15_2 = var_23;
            r14_2 = var_24;
            r13_3 = r13_2;
            if (var_25 == '\x00') {
                *(uint64_t *)(local_sp_3 + (-8L)) = 4207986UL;
                var_47 = indirect_placeholder_57(rdi, r15_2, r14_2, r12_2, rbx_0, rdi, r13_3);
                var_48 = var_47.field_0;
                var_49 = var_47.field_4;
                var_50 = var_47.field_7;
                *(uint64_t *)(local_sp_3 + (-16L)) = 4208008UL;
                indirect_placeholder_4(0UL, var_48, 4261667UL, var_49, 1UL, 0UL, var_50);
                abort();
            }
            var_27 = (uint64_t)(uint32_t)(uint64_t)var_25;
            var_28 = local_sp_2 + (-8L);
            *(uint64_t *)var_28 = 4207738UL;
            indirect_placeholder();
            local_sp_0 = var_28;
            r12_0 = var_27;
            local_sp_1 = var_28;
            r12_1 = var_27;
            if (rax_0 != 0UL) {
                rdi5_0 = var_23 + 1UL;
                while (1U)
                    {
                        var_29 = rdi5_0 + rdi;
                        r15_0 = storemerge5;
                        switch_state_var = 0;
                        switch (*(unsigned char *)var_29) {
                          case '\x00':
                            {
                                var_30 = rdi5_0 + 2UL;
                                *(uint64_t *)(local_sp_2 + (-16L)) = 4207817UL;
                                var_31 = indirect_placeholder_55(var_30);
                                var_32 = var_31.field_0;
                                var_33 = local_sp_2 + (-24L);
                                *(uint64_t *)var_33 = 4207834UL;
                                indirect_placeholder();
                                *(unsigned char *)(r13_2 + var_32) = (unsigned char)'L';
                                var_34 = local_sp_2 + (-32L);
                                *(uint64_t *)var_34 = 4207852UL;
                                indirect_placeholder();
                                **(uint64_t **)var_34 = rbx_0;
                                **(uint64_t **)var_33 = storemerge5;
                                mrv.field_0 = var_32;
                                mrv1 = mrv;
                                mrv1.field_1 = var_1;
                                mrv2 = mrv1;
                                mrv2.field_2 = var_7;
                                mrv3 = mrv2;
                                mrv3.field_3 = var_26;
                                return mrv3;
                            }
                            break;
                          case '%':
                            {
                                storemerge6 = 2UL;
                                if (*(unsigned char *)(var_29 + 1UL) == '%') {
                                    switch_state_var = 1;
                                    break;
                                }
                                rdi5_0 = rdi5_0 + storemerge6;
                                storemerge5 = storemerge5 + 1UL;
                                continue;
                            }
                            break;
                          default:
                            {
                                rdi5_0 = rdi5_0 + storemerge6;
                                storemerge5 = storemerge5 + 1UL;
                                continue;
                            }
                            break;
                        }
                        if (switch_state_var)
                            break;
                    }
                *(uint64_t *)(local_sp_0 + (-8L)) = 4207923UL;
                var_35 = indirect_placeholder_54(rdi, r15_0, r14_0, r12_0, rbx_0, rdi, r13_0);
                var_36 = var_35.field_0;
                var_37 = var_35.field_2;
                var_38 = var_35.field_4;
                var_39 = var_35.field_7;
                var_40 = local_sp_0 + (-16L);
                *(uint64_t *)var_40 = 4207945UL;
                indirect_placeholder_4(0UL, var_36, 4259960UL, var_38, 1UL, 0UL, var_39);
                local_sp_1 = var_40;
                r15_1 = r15_0;
                r14_1 = r14_0;
                r12_1 = var_37;
                r13_1 = r13_0;
            }
        }
        break;
    }
    *(uint64_t *)(local_sp_1 + (-8L)) = 4207953UL;
    var_41 = indirect_placeholder_56(rdi, r15_1, r14_1, r12_1, rbx_0, rdi, r13_1);
    var_42 = var_41.field_0;
    var_43 = var_41.field_2;
    var_44 = var_41.field_4;
    var_45 = (uint64_t)(uint32_t)var_43;
    var_46 = local_sp_1 + (-16L);
    *(uint64_t *)var_46 = 4207978UL;
    indirect_placeholder_4(0UL, var_42, 4259920UL, var_44, 1UL, 0UL, var_45);
    local_sp_3 = var_46;
    r15_2 = r15_1;
    r14_2 = r14_1;
    r12_2 = var_43;
    r13_3 = r13_1;
    *(uint64_t *)(local_sp_3 + (-8L)) = 4207986UL;
    var_47 = indirect_placeholder_57(rdi, r15_2, r14_2, r12_2, rbx_0, rdi, r13_3);
    var_48 = var_47.field_0;
    var_49 = var_47.field_4;
    var_50 = var_47.field_7;
    *(uint64_t *)(local_sp_3 + (-16L)) = 4208008UL;
    indirect_placeholder_4(0UL, var_48, 4261667UL, var_49, 1UL, 0UL, var_50);
    abort();
}
