typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_find_dir_entry_ret_type;
struct indirect_placeholder_11_ret_type;
struct indirect_placeholder_10_ret_type;
struct indirect_placeholder_2_ret_type;
struct indirect_placeholder_1_ret_type;
struct indirect_placeholder_6_ret_type;
struct indirect_placeholder_5_ret_type;
struct indirect_placeholder_4_ret_type;
struct indirect_placeholder_3_ret_type;
struct indirect_placeholder_8_ret_type;
struct indirect_placeholder_7_ret_type;
struct bb_find_dir_entry_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_11_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_10_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_2_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_1_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_6_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_5_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_4_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_3_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_8_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_7_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t indirect_placeholder_9(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_rax(void);
extern uint64_t indirect_placeholder_14(uint64_t param_0);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern struct indirect_placeholder_11_ret_type indirect_placeholder_11(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_10_ret_type indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_2_ret_type indirect_placeholder_2(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_1_ret_type indirect_placeholder_1(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_6_ret_type indirect_placeholder_6(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_5_ret_type indirect_placeholder_5(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_4_ret_type indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_3_ret_type indirect_placeholder_3(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_8_ret_type indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_7_ret_type indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
struct bb_find_dir_entry_ret_type bb_find_dir_entry(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_11_ret_type var_63;
    uint64_t rbp_8;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    bool var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t r9_5;
    uint64_t var_17;
    uint64_t var_18;
    uint32_t _pre;
    uint64_t var_62;
    uint64_t local_sp_3;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    struct indirect_placeholder_10_ret_type var_68;
    bool rax_5;
    uint64_t local_sp_6;
    uint64_t rax_0;
    uint64_t local_sp_8;
    uint64_t rbp_0;
    uint64_t local_sp_0;
    uint64_t r12_0;
    uint64_t r9_0;
    uint64_t r8_0;
    uint32_t *var_97;
    uint32_t var_98;
    uint64_t var_99;
    uint64_t var_100;
    uint64_t local_sp_2;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_14;
    struct bb_find_dir_entry_ret_type mrv;
    struct bb_find_dir_entry_ret_type mrv1;
    uint64_t local_sp_9;
    uint64_t rbp_4;
    uint64_t local_sp_4;
    uint64_t rax_1;
    uint64_t rbp_1;
    uint64_t local_sp_1;
    uint64_t r12_1;
    uint64_t rbx_0;
    uint64_t r9_1;
    uint64_t r8_1;
    uint64_t var_101;
    struct indirect_placeholder_2_ret_type var_102;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t var_37;
    uint64_t rax_4_be;
    uint64_t rax_4;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t rbp_2;
    uint64_t *var_38;
    uint64_t *var_39;
    uint64_t var_40;
    uint64_t *var_41;
    uint64_t rax_3_be;
    uint64_t rax_3;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t local_sp_5;
    uint64_t rbp_3;
    uint64_t var_59;
    uint32_t var_60;
    uint64_t var_61;
    uint32_t _pre_phi;
    uint64_t r8_5;
    uint64_t r9_2;
    uint64_t r8_2;
    uint64_t rbp_7;
    uint64_t *var_19;
    uint64_t var_20;
    bool var_21;
    uint64_t *var_22;
    uint64_t rbp_5;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t rbp_6;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t local_sp_7;
    uint64_t r12_2;
    uint64_t r9_3;
    uint64_t r8_3;
    uint64_t var_79;
    struct indirect_placeholder_6_ret_type var_80;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    struct indirect_placeholder_5_ret_type var_86;
    uint64_t r12_3;
    uint64_t r9_4;
    uint64_t r8_4;
    uint64_t var_87;
    struct indirect_placeholder_4_ret_type var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_93;
    struct indirect_placeholder_3_ret_type var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t var_15;
    uint64_t rbp_9;
    uint64_t var_16;
    uint32_t var_13;
    uint64_t var_69;
    struct indirect_placeholder_8_ret_type var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    struct indirect_placeholder_7_ret_type var_76;
    uint64_t var_77;
    uint64_t var_78;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbp();
    var_3 = init_r15();
    var_4 = init_r14();
    var_5 = init_r12();
    var_6 = init_rbx();
    var_7 = init_r9();
    var_8 = init_r8();
    var_9 = init_r13();
    *(uint64_t *)(var_0 + (-8L)) = var_4;
    *(uint64_t *)(var_0 + (-16L)) = var_9;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_2;
    *(uint64_t *)(var_0 + (-40L)) = var_6;
    *(uint64_t *)(var_0 + (-336L)) = 4205298UL;
    indirect_placeholder();
    var_10 = (var_1 == 0UL);
    var_11 = var_0 + (-344L);
    var_12 = (uint64_t *)var_11;
    r9_5 = var_7;
    rax_5 = 1;
    rax_0 = 0UL;
    r12_0 = var_1;
    local_sp_9 = var_11;
    r12_1 = var_1;
    rbx_0 = rdi;
    r8_5 = var_8;
    r9_2 = var_7;
    r8_2 = var_8;
    rbp_7 = var_2;
    r12_2 = var_1;
    r12_3 = var_1;
    if (var_10) {
        *var_12 = 4205795UL;
        var_69 = indirect_placeholder_12(var_7, rdx, var_8);
        *(uint64_t *)(var_0 + (-352L)) = 4205803UL;
        var_70 = indirect_placeholder_8(var_2, var_3, rdx, var_5, rdi, var_69, rsi);
        var_71 = var_70.field_0;
        var_72 = var_70.field_3;
        var_73 = var_70.field_5;
        *(uint64_t *)(var_0 + (-360L)) = 4205811UL;
        indirect_placeholder();
        var_74 = (uint64_t)*(uint32_t *)var_71;
        var_75 = var_0 + (-368L);
        *(uint64_t *)var_75 = 4205833UL;
        var_76 = indirect_placeholder_7(0UL, var_71, 4259850UL, var_72, 1UL, var_73, var_74);
        var_77 = var_76.field_0;
        var_78 = var_76.field_1;
        local_sp_7 = var_75;
        r12_2 = var_71;
        r9_3 = var_77;
        r8_3 = var_78;
        *(uint64_t *)(local_sp_7 + (-8L)) = 4205841UL;
        var_79 = indirect_placeholder_12(r9_3, rdx, r8_3);
        *(uint64_t *)(local_sp_7 + (-16L)) = 4205849UL;
        var_80 = indirect_placeholder_6(rbp_7, var_3, rdx, r12_2, rdi, var_79, rsi);
        var_81 = var_80.field_0;
        var_82 = var_80.field_3;
        var_83 = var_80.field_5;
        *(uint64_t *)(local_sp_7 + (-24L)) = 4205857UL;
        indirect_placeholder();
        var_84 = (uint64_t)*(uint32_t *)var_81;
        var_85 = local_sp_7 + (-32L);
        *(uint64_t *)var_85 = 4205879UL;
        var_86 = indirect_placeholder_5(0UL, var_81, 4259897UL, var_82, 1UL, var_83, var_84);
        rbp_8 = rbp_7;
        local_sp_8 = var_85;
        r12_3 = var_81;
        r9_4 = var_86.field_0;
        r8_4 = var_86.field_1;
    } else {
        *var_12 = 4205318UL;
        indirect_placeholder();
        var_13 = (uint32_t)var_1;
        var_14 = (uint64_t)var_13;
        _pre_phi = var_13;
        rbp_4 = var_14;
        rbp_9 = var_14;
        if ((int)var_13 < (int)0U) {
            while (1U)
                {
                    var_16 = local_sp_9 + (-8L);
                    *(uint64_t *)var_16 = 4205762UL;
                    indirect_placeholder();
                    rbp_8 = rbp_9;
                    local_sp_8 = var_16;
                    rbp_4 = rbp_9;
                    r9_2 = r9_5;
                    r8_2 = r8_5;
                    r9_4 = r9_5;
                    r8_4 = r8_5;
                    if (!rax_5) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_17 = local_sp_9 + (-16L);
                    *(uint64_t *)var_17 = 4205779UL;
                    var_18 = indirect_placeholder_9(4259848UL, var_16);
                    _pre = (uint32_t)var_18;
                    _pre_phi = _pre;
                    local_sp_4 = var_17;
                    rax_5 = 0;
                    local_sp_6 = local_sp_4;
                    r9_0 = r9_2;
                    r8_0 = r8_2;
                    r9_1 = r9_2;
                    r8_1 = r8_2;
                    local_sp_5 = local_sp_4;
                    rbp_7 = rbp_4;
                    rbp_5 = rbp_4;
                    rbp_6 = rbp_4;
                    local_sp_7 = local_sp_4;
                    r9_3 = r9_2;
                    r8_3 = r8_2;
                    if ((int)_pre_phi <= (int)4294967295U) {
                        loop_state_var = 2U;
                        break;
                    }
                    var_19 = (uint64_t *)rdi;
                    var_20 = *var_19;
                    var_21 = (*(uint64_t *)local_sp_4 == var_20);
                    var_22 = (uint64_t *)(rdi + 8UL);
                    rax_3 = var_20;
                    rax_4 = var_20;
                    if (var_21) {
                        while (1U)
                            {
                                *(uint64_t *)(local_sp_5 + (-8L)) = 4205381UL;
                                indirect_placeholder();
                                *(uint32_t *)rax_3 = 0U;
                                var_23 = local_sp_5 + (-16L);
                                *(uint64_t *)var_23 = 4205395UL;
                                var_24 = indirect_placeholder_14(var_1);
                                rbp_3 = rbp_5;
                                local_sp_3 = var_23;
                                if (var_24 != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                var_25 = var_24 + 19UL;
                                var_26 = local_sp_5 + 128UL;
                                var_27 = local_sp_5 + (-24L);
                                *(uint64_t *)var_27 = 4205424UL;
                                var_28 = indirect_placeholder_9(var_25, var_26);
                                local_sp_2 = var_27;
                                rbp_2 = var_25;
                                rax_3_be = var_28;
                                local_sp_5 = var_27;
                                rbp_5 = var_25;
                                if ((int)(uint32_t)var_28 < (int)0U) {
                                    rax_3 = rax_3_be;
                                    continue;
                                }
                                var_29 = *var_22;
                                rax_3_be = var_29;
                                var_30 = *var_19;
                                rax_1 = var_30;
                                rax_3_be = var_30;
                                if (*(uint64_t *)var_26 != var_29 & *(uint64_t *)(local_sp_5 + 120UL) != var_30) {
                                    loop_state_var = 0U;
                                    break;
                                }
                            }
                        switch_state_var = 0;
                        switch (loop_state_var) {
                          case 1U:
                            {
                                break;
                            }
                            break;
                          case 0U:
                            {
                                loop_state_var = 0U;
                                switch_state_var = 1;
                                break;
                            }
                            break;
                        }
                        if (switch_state_var)
                            break;
                    }
                    while (1U)
                        {
                            *(uint64_t *)(local_sp_6 + (-8L)) = 4205469UL;
                            indirect_placeholder();
                            *(uint32_t *)rax_4 = 0U;
                            var_31 = local_sp_6 + (-16L);
                            *(uint64_t *)var_31 = 4205483UL;
                            var_32 = indirect_placeholder_14(var_1);
                            rbp_3 = rbp_6;
                            local_sp_3 = var_31;
                            if (var_32 != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            var_33 = var_32 + 19UL;
                            var_34 = local_sp_6 + 128UL;
                            var_35 = local_sp_6 + (-24L);
                            *(uint64_t *)var_35 = 4205512UL;
                            var_36 = indirect_placeholder_9(var_33, var_34);
                            local_sp_6 = var_35;
                            local_sp_2 = var_35;
                            rax_4_be = var_36;
                            rbp_2 = var_33;
                            rbp_6 = var_33;
                            if ((int)(uint32_t)var_36 < (int)0U) {
                                rax_4 = rax_4_be;
                                continue;
                            }
                            var_37 = *(uint64_t *)var_34;
                            rax_4_be = var_37;
                            rax_1 = var_37;
                            if (*var_22 != var_37) {
                                loop_state_var = 0U;
                                break;
                            }
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 1U:
                        {
                            var_59 = local_sp_3 + (-8L);
                            *(uint64_t *)var_59 = 4205685UL;
                            indirect_placeholder();
                            var_60 = *(volatile uint32_t *)(uint32_t *)0UL;
                            var_61 = (uint64_t)var_60;
                            rax_0 = var_61;
                            rbp_0 = rbp_3;
                            local_sp_0 = var_59;
                            rbp_9 = rbp_3;
                            if (var_60 != 0U) {
                                loop_state_var = 3U;
                                switch_state_var = 1;
                                break;
                            }
                            *(uint64_t *)(local_sp_3 + (-16L)) = 4205703UL;
                            indirect_placeholder();
                            *(uint64_t *)(local_sp_3 + (-24L)) = 4205719UL;
                            var_62 = indirect_placeholder_12(r9_2, rdx, r8_2);
                            *(uint64_t *)(local_sp_3 + (-32L)) = 4205727UL;
                            var_63 = indirect_placeholder_11(rbp_3, var_3, rdx, var_1, rdi, var_62, rsi);
                            var_64 = var_63.field_0;
                            var_65 = var_63.field_3;
                            var_66 = var_63.field_5;
                            var_67 = local_sp_3 + (-40L);
                            *(uint64_t *)var_67 = 4205749UL;
                            var_68 = indirect_placeholder_10(0UL, var_64, 4260192UL, var_65, 1UL, var_66, 0UL);
                            local_sp_9 = var_67;
                            r9_5 = var_68.field_0;
                            r8_5 = var_68.field_1;
                            continue;
                        }
                        break;
                      case 0U:
                        {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            switch (loop_state_var) {
              case 3U:
              case 2U:
              case 1U:
                {
                    switch (loop_state_var) {
                      case 2U:
                      case 1U:
                        {
                            *(uint64_t *)(local_sp_7 + (-8L)) = 4205841UL;
                            var_79 = indirect_placeholder_12(r9_3, rdx, r8_3);
                            *(uint64_t *)(local_sp_7 + (-16L)) = 4205849UL;
                            var_80 = indirect_placeholder_6(rbp_7, var_3, rdx, r12_2, rdi, var_79, rsi);
                            var_81 = var_80.field_0;
                            var_82 = var_80.field_3;
                            var_83 = var_80.field_5;
                            *(uint64_t *)(local_sp_7 + (-24L)) = 4205857UL;
                            indirect_placeholder();
                            var_84 = (uint64_t)*(uint32_t *)var_81;
                            var_85 = local_sp_7 + (-32L);
                            *(uint64_t *)var_85 = 4205879UL;
                            var_86 = indirect_placeholder_5(0UL, var_81, 4259897UL, var_82, 1UL, var_83, var_84);
                            rbp_8 = rbp_7;
                            local_sp_8 = var_85;
                            r12_3 = var_81;
                            r9_4 = var_86.field_0;
                            r8_4 = var_86.field_1;
                        }
                        break;
                      case 3U:
                        {
                            *(uint64_t *)(local_sp_0 + (-8L)) = 4205930UL;
                            indirect_placeholder();
                            var_97 = (uint32_t *)rax_0;
                            var_98 = *var_97;
                            var_99 = (uint64_t)var_98;
                            *(uint64_t *)(local_sp_0 + (-16L)) = 4205940UL;
                            indirect_placeholder();
                            var_100 = local_sp_0 + (-24L);
                            *(uint64_t *)var_100 = 4205945UL;
                            indirect_placeholder();
                            *var_97 = var_98;
                            rbp_1 = rbp_0;
                            local_sp_1 = var_100;
                            r12_1 = r12_0;
                            rbx_0 = var_99;
                            r9_1 = r9_0;
                            r8_1 = r8_0;
                            *(uint64_t *)(local_sp_1 + (-8L)) = 4205955UL;
                            var_101 = indirect_placeholder_12(r9_1, rdx, r8_1);
                            *(uint64_t *)(local_sp_1 + (-16L)) = 4205963UL;
                            var_102 = indirect_placeholder_2(rbp_1, var_3, rdx, r12_1, rbx_0, var_101, rsi);
                            var_103 = var_102.field_0;
                            var_104 = var_102.field_3;
                            var_105 = var_102.field_5;
                            *(uint64_t *)(local_sp_1 + (-24L)) = 4205971UL;
                            indirect_placeholder();
                            var_106 = (uint64_t)*(uint32_t *)var_103;
                            *(uint64_t *)(local_sp_1 + (-32L)) = 4205993UL;
                            indirect_placeholder_1(0UL, var_103, 4259915UL, var_104, 1UL, var_105, var_106);
                            abort();
                        }
                        break;
                    }
                }
                break;
              case 0U:
                {
                    var_38 = (uint64_t *)(local_sp_2 + (-8L));
                    *var_38 = 4205538UL;
                    indirect_placeholder();
                    var_39 = (uint64_t *)(local_sp_2 + (-16L));
                    *var_39 = 4205552UL;
                    indirect_placeholder_13(rax_1, r9_2, rsi, r8_2, rbp_2);
                    var_40 = local_sp_2 + (-24L);
                    var_41 = (uint64_t *)var_40;
                    *var_41 = 4205560UL;
                    indirect_placeholder();
                    rbp_1 = rbp_2;
                    local_sp_1 = var_40;
                    if ((uint64_t)(uint32_t)rax_1 == 0UL) {
                        *(uint64_t *)(local_sp_1 + (-8L)) = 4205955UL;
                        var_101 = indirect_placeholder_12(r9_1, rdx, r8_1);
                        *(uint64_t *)(local_sp_1 + (-16L)) = 4205963UL;
                        var_102 = indirect_placeholder_2(rbp_1, var_3, rdx, r12_1, rbx_0, var_101, rsi);
                        var_103 = var_102.field_0;
                        var_104 = var_102.field_3;
                        var_105 = var_102.field_5;
                        *(uint64_t *)(local_sp_1 + (-24L)) = 4205971UL;
                        indirect_placeholder();
                        var_106 = (uint64_t)*(uint32_t *)var_103;
                        *(uint64_t *)(local_sp_1 + (-32L)) = 4205993UL;
                        indirect_placeholder_1(0UL, var_103, 4259915UL, var_104, 1UL, var_105, var_106);
                        abort();
                    }
                    var_42 = *var_39;
                    var_43 = *var_38;
                    var_44 = *(uint64_t *)local_sp_2;
                    var_45 = *(uint64_t *)(local_sp_2 + 8UL);
                    var_46 = *(uint64_t *)(local_sp_2 + 16UL);
                    var_47 = *(uint64_t *)(local_sp_2 + 24UL);
                    var_48 = *(uint64_t *)(local_sp_2 + 32UL);
                    *var_19 = *var_41;
                    *var_22 = var_42;
                    var_49 = *(uint64_t *)(local_sp_2 + 40UL);
                    var_50 = *(uint64_t *)(local_sp_2 + 48UL);
                    var_51 = *(uint64_t *)(local_sp_2 + 56UL);
                    var_52 = *(uint64_t *)(local_sp_2 + 64UL);
                    var_53 = *(uint64_t *)(local_sp_2 + 72UL);
                    var_54 = *(uint64_t *)(local_sp_2 + 80UL);
                    var_55 = *(uint64_t *)(local_sp_2 + 88UL);
                    var_56 = *(uint64_t *)(local_sp_2 + 96UL);
                    *(uint64_t *)(rdi + 16UL) = var_43;
                    *(uint64_t *)(rdi + 24UL) = var_44;
                    var_57 = *(uint64_t *)(local_sp_2 + 104UL);
                    var_58 = *(uint64_t *)(local_sp_2 + 112UL);
                    *(uint64_t *)(rdi + 32UL) = var_45;
                    *(uint64_t *)(rdi + 40UL) = var_46;
                    *(uint64_t *)(rdi + 48UL) = var_47;
                    *(uint64_t *)(rdi + 56UL) = var_48;
                    *(uint64_t *)(rdi + 64UL) = var_49;
                    *(uint64_t *)(rdi + 72UL) = var_50;
                    *(uint64_t *)(rdi + 80UL) = var_51;
                    *(uint64_t *)(rdi + 88UL) = var_52;
                    *(uint64_t *)(rdi + 96UL) = var_53;
                    *(uint64_t *)(rdi + 104UL) = var_54;
                    *(uint64_t *)(rdi + 112UL) = var_55;
                    *(uint64_t *)(rdi + 120UL) = var_56;
                    *(uint64_t *)(rdi + 128UL) = var_57;
                    *(uint64_t *)(rdi + 136UL) = var_58;
                    mrv.field_0 = r9_2;
                    mrv1 = mrv;
                    mrv1.field_1 = r8_2;
                    return mrv1;
                }
                break;
            }
        } else {
            *(uint64_t *)(var_0 + (-352L)) = 4205335UL;
            indirect_placeholder();
            var_15 = var_0 + (-360L);
            *(uint64_t *)var_15 = 4205353UL;
            indirect_placeholder();
            local_sp_4 = var_15;
            rax_5 = 0;
            local_sp_6 = local_sp_4;
            r9_0 = r9_2;
            r8_0 = r8_2;
            r9_1 = r9_2;
            r8_1 = r8_2;
            local_sp_5 = local_sp_4;
            rbp_7 = rbp_4;
            rbp_5 = rbp_4;
            rbp_6 = rbp_4;
            local_sp_7 = local_sp_4;
            r9_3 = r9_2;
            r8_3 = r8_2;
            if ((int)_pre_phi <= (int)4294967295U) {
                *(uint64_t *)(local_sp_7 + (-8L)) = 4205841UL;
                var_79 = indirect_placeholder_12(r9_3, rdx, r8_3);
                *(uint64_t *)(local_sp_7 + (-16L)) = 4205849UL;
                var_80 = indirect_placeholder_6(rbp_7, var_3, rdx, r12_2, rdi, var_79, rsi);
                var_81 = var_80.field_0;
                var_82 = var_80.field_3;
                var_83 = var_80.field_5;
                *(uint64_t *)(local_sp_7 + (-24L)) = 4205857UL;
                indirect_placeholder();
                var_84 = (uint64_t)*(uint32_t *)var_81;
                var_85 = local_sp_7 + (-32L);
                *(uint64_t *)var_85 = 4205879UL;
                var_86 = indirect_placeholder_5(0UL, var_81, 4259897UL, var_82, 1UL, var_83, var_84);
                rbp_8 = rbp_7;
                local_sp_8 = var_85;
                r12_3 = var_81;
                r9_4 = var_86.field_0;
                r8_4 = var_86.field_1;
                *(uint64_t *)(local_sp_8 + (-8L)) = 4205887UL;
                var_87 = indirect_placeholder_12(r9_4, rdx, r8_4);
                *(uint64_t *)(local_sp_8 + (-16L)) = 4205895UL;
                var_88 = indirect_placeholder_4(rbp_8, var_3, rdx, r12_3, rdi, var_87, rsi);
                var_89 = var_88.field_0;
                var_90 = var_88.field_3;
                var_91 = var_88.field_5;
                *(uint64_t *)(local_sp_8 + (-24L)) = 4205903UL;
                indirect_placeholder();
                var_92 = (uint64_t)*(uint32_t *)var_89;
                var_93 = local_sp_8 + (-32L);
                *(uint64_t *)var_93 = 4205925UL;
                var_94 = indirect_placeholder_3(0UL, var_89, 4259875UL, var_90, 1UL, var_91, var_92);
                var_95 = var_94.field_0;
                var_96 = var_94.field_1;
                rbp_0 = rbp_8;
                local_sp_0 = var_93;
                r12_0 = var_89;
                r9_0 = var_95;
                r8_0 = var_96;
                *(uint64_t *)(local_sp_0 + (-8L)) = 4205930UL;
                indirect_placeholder();
                var_97 = (uint32_t *)rax_0;
                var_98 = *var_97;
                var_99 = (uint64_t)var_98;
                *(uint64_t *)(local_sp_0 + (-16L)) = 4205940UL;
                indirect_placeholder();
                var_100 = local_sp_0 + (-24L);
                *(uint64_t *)var_100 = 4205945UL;
                indirect_placeholder();
                *var_97 = var_98;
                rbp_1 = rbp_0;
                local_sp_1 = var_100;
                r12_1 = r12_0;
                rbx_0 = var_99;
                r9_1 = r9_0;
                r8_1 = r8_0;
                *(uint64_t *)(local_sp_1 + (-8L)) = 4205955UL;
                var_101 = indirect_placeholder_12(r9_1, rdx, r8_1);
                *(uint64_t *)(local_sp_1 + (-16L)) = 4205963UL;
                var_102 = indirect_placeholder_2(rbp_1, var_3, rdx, r12_1, rbx_0, var_101, rsi);
                var_103 = var_102.field_0;
                var_104 = var_102.field_3;
                var_105 = var_102.field_5;
                *(uint64_t *)(local_sp_1 + (-24L)) = 4205971UL;
                indirect_placeholder();
                var_106 = (uint64_t)*(uint32_t *)var_103;
                *(uint64_t *)(local_sp_1 + (-32L)) = 4205993UL;
                indirect_placeholder_1(0UL, var_103, 4259915UL, var_104, 1UL, var_105, var_106);
                abort();
            }
            var_19 = (uint64_t *)rdi;
            var_20 = *var_19;
            var_21 = (*(uint64_t *)local_sp_4 == var_20);
            var_22 = (uint64_t *)(rdi + 8UL);
            rax_3 = var_20;
            rax_4 = var_20;
            if (var_21) {
                while (1U)
                    {
                        *(uint64_t *)(local_sp_5 + (-8L)) = 4205381UL;
                        indirect_placeholder();
                        *(uint32_t *)rax_3 = 0U;
                        var_23 = local_sp_5 + (-16L);
                        *(uint64_t *)var_23 = 4205395UL;
                        var_24 = indirect_placeholder_14(var_1);
                        rbp_3 = rbp_5;
                        local_sp_3 = var_23;
                        if (var_24 != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_25 = var_24 + 19UL;
                        var_26 = local_sp_5 + 128UL;
                        var_27 = local_sp_5 + (-24L);
                        *(uint64_t *)var_27 = 4205424UL;
                        var_28 = indirect_placeholder_9(var_25, var_26);
                        local_sp_2 = var_27;
                        rbp_2 = var_25;
                        rax_3_be = var_28;
                        local_sp_5 = var_27;
                        rbp_5 = var_25;
                        if ((int)(uint32_t)var_28 < (int)0U) {
                            rax_3 = rax_3_be;
                            continue;
                        }
                        var_29 = *var_22;
                        rax_3_be = var_29;
                        var_30 = *var_19;
                        rax_1 = var_30;
                        rax_3_be = var_30;
                        if (*(uint64_t *)var_26 != var_29 & *(uint64_t *)(local_sp_5 + 120UL) != var_30) {
                            loop_state_var = 0U;
                            break;
                        }
                    }
                switch (loop_state_var) {
                  case 1U:
                    {
                        break;
                    }
                    break;
                  case 0U:
                    {
                        var_38 = (uint64_t *)(local_sp_2 + (-8L));
                        *var_38 = 4205538UL;
                        indirect_placeholder();
                        var_39 = (uint64_t *)(local_sp_2 + (-16L));
                        *var_39 = 4205552UL;
                        indirect_placeholder_13(rax_1, r9_2, rsi, r8_2, rbp_2);
                        var_40 = local_sp_2 + (-24L);
                        var_41 = (uint64_t *)var_40;
                        *var_41 = 4205560UL;
                        indirect_placeholder();
                        rbp_1 = rbp_2;
                        local_sp_1 = var_40;
                        if ((uint64_t)(uint32_t)rax_1 == 0UL) {
                            *(uint64_t *)(local_sp_1 + (-8L)) = 4205955UL;
                            var_101 = indirect_placeholder_12(r9_1, rdx, r8_1);
                            *(uint64_t *)(local_sp_1 + (-16L)) = 4205963UL;
                            var_102 = indirect_placeholder_2(rbp_1, var_3, rdx, r12_1, rbx_0, var_101, rsi);
                            var_103 = var_102.field_0;
                            var_104 = var_102.field_3;
                            var_105 = var_102.field_5;
                            *(uint64_t *)(local_sp_1 + (-24L)) = 4205971UL;
                            indirect_placeholder();
                            var_106 = (uint64_t)*(uint32_t *)var_103;
                            *(uint64_t *)(local_sp_1 + (-32L)) = 4205993UL;
                            indirect_placeholder_1(0UL, var_103, 4259915UL, var_104, 1UL, var_105, var_106);
                            abort();
                        }
                        var_42 = *var_39;
                        var_43 = *var_38;
                        var_44 = *(uint64_t *)local_sp_2;
                        var_45 = *(uint64_t *)(local_sp_2 + 8UL);
                        var_46 = *(uint64_t *)(local_sp_2 + 16UL);
                        var_47 = *(uint64_t *)(local_sp_2 + 24UL);
                        var_48 = *(uint64_t *)(local_sp_2 + 32UL);
                        *var_19 = *var_41;
                        *var_22 = var_42;
                        var_49 = *(uint64_t *)(local_sp_2 + 40UL);
                        var_50 = *(uint64_t *)(local_sp_2 + 48UL);
                        var_51 = *(uint64_t *)(local_sp_2 + 56UL);
                        var_52 = *(uint64_t *)(local_sp_2 + 64UL);
                        var_53 = *(uint64_t *)(local_sp_2 + 72UL);
                        var_54 = *(uint64_t *)(local_sp_2 + 80UL);
                        var_55 = *(uint64_t *)(local_sp_2 + 88UL);
                        var_56 = *(uint64_t *)(local_sp_2 + 96UL);
                        *(uint64_t *)(rdi + 16UL) = var_43;
                        *(uint64_t *)(rdi + 24UL) = var_44;
                        var_57 = *(uint64_t *)(local_sp_2 + 104UL);
                        var_58 = *(uint64_t *)(local_sp_2 + 112UL);
                        *(uint64_t *)(rdi + 32UL) = var_45;
                        *(uint64_t *)(rdi + 40UL) = var_46;
                        *(uint64_t *)(rdi + 48UL) = var_47;
                        *(uint64_t *)(rdi + 56UL) = var_48;
                        *(uint64_t *)(rdi + 64UL) = var_49;
                        *(uint64_t *)(rdi + 72UL) = var_50;
                        *(uint64_t *)(rdi + 80UL) = var_51;
                        *(uint64_t *)(rdi + 88UL) = var_52;
                        *(uint64_t *)(rdi + 96UL) = var_53;
                        *(uint64_t *)(rdi + 104UL) = var_54;
                        *(uint64_t *)(rdi + 112UL) = var_55;
                        *(uint64_t *)(rdi + 120UL) = var_56;
                        *(uint64_t *)(rdi + 128UL) = var_57;
                        *(uint64_t *)(rdi + 136UL) = var_58;
                        mrv.field_0 = r9_2;
                        mrv1 = mrv;
                        mrv1.field_1 = r8_2;
                        return mrv1;
                    }
                    break;
                }
            }
            while (1U)
                {
                    *(uint64_t *)(local_sp_6 + (-8L)) = 4205469UL;
                    indirect_placeholder();
                    *(uint32_t *)rax_4 = 0U;
                    var_31 = local_sp_6 + (-16L);
                    *(uint64_t *)var_31 = 4205483UL;
                    var_32 = indirect_placeholder_14(var_1);
                    rbp_3 = rbp_6;
                    local_sp_3 = var_31;
                    if (var_32 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_33 = var_32 + 19UL;
                    var_34 = local_sp_6 + 128UL;
                    var_35 = local_sp_6 + (-24L);
                    *(uint64_t *)var_35 = 4205512UL;
                    var_36 = indirect_placeholder_9(var_33, var_34);
                    local_sp_6 = var_35;
                    local_sp_2 = var_35;
                    rax_4_be = var_36;
                    rbp_2 = var_33;
                    rbp_6 = var_33;
                    if ((int)(uint32_t)var_36 < (int)0U) {
                        rax_4 = rax_4_be;
                        continue;
                    }
                    var_37 = *(uint64_t *)var_34;
                    rax_4_be = var_37;
                    rax_1 = var_37;
                    if (*var_22 != var_37) {
                        loop_state_var = 0U;
                        break;
                    }
                }
            switch (loop_state_var) {
              case 1U:
                {
                    var_59 = local_sp_3 + (-8L);
                    *(uint64_t *)var_59 = 4205685UL;
                    indirect_placeholder();
                    var_60 = *(volatile uint32_t *)(uint32_t *)0UL;
                    var_61 = (uint64_t)var_60;
                    rax_0 = var_61;
                    rbp_0 = rbp_3;
                    local_sp_0 = var_59;
                    rbp_9 = rbp_3;
                    if (var_60 != 0U) {
                        *(uint64_t *)(local_sp_0 + (-8L)) = 4205930UL;
                        indirect_placeholder();
                        var_97 = (uint32_t *)rax_0;
                        var_98 = *var_97;
                        var_99 = (uint64_t)var_98;
                        *(uint64_t *)(local_sp_0 + (-16L)) = 4205940UL;
                        indirect_placeholder();
                        var_100 = local_sp_0 + (-24L);
                        *(uint64_t *)var_100 = 4205945UL;
                        indirect_placeholder();
                        *var_97 = var_98;
                        rbp_1 = rbp_0;
                        local_sp_1 = var_100;
                        r12_1 = r12_0;
                        rbx_0 = var_99;
                        r9_1 = r9_0;
                        r8_1 = r8_0;
                        *(uint64_t *)(local_sp_1 + (-8L)) = 4205955UL;
                        var_101 = indirect_placeholder_12(r9_1, rdx, r8_1);
                        *(uint64_t *)(local_sp_1 + (-16L)) = 4205963UL;
                        var_102 = indirect_placeholder_2(rbp_1, var_3, rdx, r12_1, rbx_0, var_101, rsi);
                        var_103 = var_102.field_0;
                        var_104 = var_102.field_3;
                        var_105 = var_102.field_5;
                        *(uint64_t *)(local_sp_1 + (-24L)) = 4205971UL;
                        indirect_placeholder();
                        var_106 = (uint64_t)*(uint32_t *)var_103;
                        *(uint64_t *)(local_sp_1 + (-32L)) = 4205993UL;
                        indirect_placeholder_1(0UL, var_103, 4259915UL, var_104, 1UL, var_105, var_106);
                        abort();
                    }
                    *(uint64_t *)(local_sp_3 + (-16L)) = 4205703UL;
                    indirect_placeholder();
                    *(uint64_t *)(local_sp_3 + (-24L)) = 4205719UL;
                    var_62 = indirect_placeholder_12(r9_2, rdx, r8_2);
                    *(uint64_t *)(local_sp_3 + (-32L)) = 4205727UL;
                    var_63 = indirect_placeholder_11(rbp_3, var_3, rdx, var_1, rdi, var_62, rsi);
                    var_64 = var_63.field_0;
                    var_65 = var_63.field_3;
                    var_66 = var_63.field_5;
                    var_67 = local_sp_3 + (-40L);
                    *(uint64_t *)var_67 = 4205749UL;
                    var_68 = indirect_placeholder_10(0UL, var_64, 4260192UL, var_65, 1UL, var_66, 0UL);
                    local_sp_9 = var_67;
                    r9_5 = var_68.field_0;
                    r8_5 = var_68.field_1;
                }
                break;
              case 0U:
                {
                    var_38 = (uint64_t *)(local_sp_2 + (-8L));
                    *var_38 = 4205538UL;
                    indirect_placeholder();
                    var_39 = (uint64_t *)(local_sp_2 + (-16L));
                    *var_39 = 4205552UL;
                    indirect_placeholder_13(rax_1, r9_2, rsi, r8_2, rbp_2);
                    var_40 = local_sp_2 + (-24L);
                    var_41 = (uint64_t *)var_40;
                    *var_41 = 4205560UL;
                    indirect_placeholder();
                    rbp_1 = rbp_2;
                    local_sp_1 = var_40;
                    if ((uint64_t)(uint32_t)rax_1 == 0UL) {
                        *(uint64_t *)(local_sp_1 + (-8L)) = 4205955UL;
                        var_101 = indirect_placeholder_12(r9_1, rdx, r8_1);
                        *(uint64_t *)(local_sp_1 + (-16L)) = 4205963UL;
                        var_102 = indirect_placeholder_2(rbp_1, var_3, rdx, r12_1, rbx_0, var_101, rsi);
                        var_103 = var_102.field_0;
                        var_104 = var_102.field_3;
                        var_105 = var_102.field_5;
                        *(uint64_t *)(local_sp_1 + (-24L)) = 4205971UL;
                        indirect_placeholder();
                        var_106 = (uint64_t)*(uint32_t *)var_103;
                        *(uint64_t *)(local_sp_1 + (-32L)) = 4205993UL;
                        indirect_placeholder_1(0UL, var_103, 4259915UL, var_104, 1UL, var_105, var_106);
                        abort();
                    }
                    var_42 = *var_39;
                    var_43 = *var_38;
                    var_44 = *(uint64_t *)local_sp_2;
                    var_45 = *(uint64_t *)(local_sp_2 + 8UL);
                    var_46 = *(uint64_t *)(local_sp_2 + 16UL);
                    var_47 = *(uint64_t *)(local_sp_2 + 24UL);
                    var_48 = *(uint64_t *)(local_sp_2 + 32UL);
                    *var_19 = *var_41;
                    *var_22 = var_42;
                    var_49 = *(uint64_t *)(local_sp_2 + 40UL);
                    var_50 = *(uint64_t *)(local_sp_2 + 48UL);
                    var_51 = *(uint64_t *)(local_sp_2 + 56UL);
                    var_52 = *(uint64_t *)(local_sp_2 + 64UL);
                    var_53 = *(uint64_t *)(local_sp_2 + 72UL);
                    var_54 = *(uint64_t *)(local_sp_2 + 80UL);
                    var_55 = *(uint64_t *)(local_sp_2 + 88UL);
                    var_56 = *(uint64_t *)(local_sp_2 + 96UL);
                    *(uint64_t *)(rdi + 16UL) = var_43;
                    *(uint64_t *)(rdi + 24UL) = var_44;
                    var_57 = *(uint64_t *)(local_sp_2 + 104UL);
                    var_58 = *(uint64_t *)(local_sp_2 + 112UL);
                    *(uint64_t *)(rdi + 32UL) = var_45;
                    *(uint64_t *)(rdi + 40UL) = var_46;
                    *(uint64_t *)(rdi + 48UL) = var_47;
                    *(uint64_t *)(rdi + 56UL) = var_48;
                    *(uint64_t *)(rdi + 64UL) = var_49;
                    *(uint64_t *)(rdi + 72UL) = var_50;
                    *(uint64_t *)(rdi + 80UL) = var_51;
                    *(uint64_t *)(rdi + 88UL) = var_52;
                    *(uint64_t *)(rdi + 96UL) = var_53;
                    *(uint64_t *)(rdi + 104UL) = var_54;
                    *(uint64_t *)(rdi + 112UL) = var_55;
                    *(uint64_t *)(rdi + 120UL) = var_56;
                    *(uint64_t *)(rdi + 128UL) = var_57;
                    *(uint64_t *)(rdi + 136UL) = var_58;
                    mrv.field_0 = r9_2;
                    mrv1 = mrv;
                    mrv1.field_1 = r8_2;
                    return mrv1;
                }
                break;
            }
        }
    }
}
