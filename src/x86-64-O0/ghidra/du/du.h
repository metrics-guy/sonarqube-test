typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined5;
typedef unsigned long    undefined6;
typedef unsigned long    undefined7;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00403290(void);
void FUN_00403580(void);
void FUN_00403780(void);
void entry(void);
void FUN_004037f0(void);
void FUN_00403860(void);
void FUN_004038f0(void);
void FUN_00403925(void);
void FUN_0040394a(void);
void FUN_0040396f(undefined8 uParm1);
void FUN_00403994(undefined *puParm1);
void FUN_00403b31(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00403b56(undefined8 uParm1);
void FUN_00403b64(undefined8 *puParm1);
void FUN_00403ba4(undefined8 *puParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_00403be6(ulong *puParm1,long *plParm2);
ulong FUN_00403c89(uint uParm1);
ulong FUN_00403e33(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00403e75(undefined8 uParm1,undefined8 uParm2,ulong uParm3,undefined8 uParm4);
void FUN_00403f30(long lParm1);
void FUN_00403f9c(undefined8 *puParm1,undefined8 uParm2);
void FUN_00404049(void);
undefined8 FUN_004040ef(long *plParm1);
ulong FUN_0040418a(long lParm1,long lParm2);
ulong FUN_004048b0(long *plParm1,uint uParm2);
ulong FUN_004049c4(uint uParm1,undefined8 *puParm2);
void FUN_004055c1(void);
long FUN_004055d2(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
void FUN_00405701(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_00405783(long lParm1,long lParm2,long lParm3);
long FUN_004058c4(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
undefined8 * FUN_0040594a(undefined8 uParm1);
undefined8 * FUN_0040599b(undefined8 uParm1);
long FUN_00405a04(long *plParm1,undefined4 *puParm2);
long FUN_00405aeb(long *plParm1);
void FUN_00405b25(long *plParm1);
void FUN_00405b5c(void);
ulong FUN_00405c42(ulong *puParm1,ulong uParm2);
ulong FUN_00405cad(ulong *puParm1,ulong *puParm2);
void FUN_00405cdf(long lParm1);
long * FUN_00405d12(void);
void FUN_00405d92(undefined8 *puParm1);
ulong FUN_00405ddc(ulong uParm1,ulong uParm2);
long FUN_00405dfa(undefined8 *puParm1,long lParm2);
ulong FUN_00405eee(long lParm1,ulong uParm2);
undefined8 FUN_00405f6e(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00405fe6(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_00406062(int iParm1);
void FUN_00406088(long lParm1,undefined8 uParm2);
undefined8 FUN_004060d0(char *pcParm1,uint uParm2);
void FUN_0040619d(char *pcParm1);
void FUN_00406201(void);
void FUN_00406211(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040623e(char *pcParm1,ulong uParm2);
ulong FUN_0040636a(undefined8 uParm1,undefined8 uParm2);
ulong FUN_004063a4(undefined8 uParm1,undefined8 uParm2);
void FUN_004063de(undefined8 uParm1);
void FUN_004063f9(undefined8 *puParm1,int iParm2,uint uParm3);
ulong FUN_004064a5(undefined8 uParm1,long lParm2,uint uParm3);
ulong FUN_004065dc(undefined8 uParm1,char *pcParm2,uint uParm3);
ulong FUN_0040669c(uint *puParm1,undefined8 uParm2);
undefined8 FUN_00406710(long lParm1,undefined8 uParm2);
undefined8 FUN_0040678e(long lParm1,long lParm2,undefined8 uParm3);
ulong FUN_0040685c(long **pplParm1,undefined8 uParm2);
void FUN_0040694b(long *plParm1,long lParm2,uint uParm3);
undefined8 FUN_00406c71(code *pcParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4,byte bParm5,undefined8 uParm6);
void FUN_00406e5f(undefined8 uParm1,undefined8 uParm2,uint uParm3,code **ppcParm4);
ulong FUN_00406e9c(undefined8 uParm1,undefined8 uParm2,char *pcParm3,uint uParm4,char cParm5);
void FUN_00406f73(undefined8 uParm1,byte *pbParm2,long lParm3);
void FUN_00406fc4(undefined8 uParm1,byte *pbParm2,long lParm3);
ulong FUN_00407015(int iParm1,int iParm2);
void FUN_00407069(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,uint uParm5);
char * FUN_004070be(long lParm1,char *pcParm2,undefined8 *puParm3,undefined uParm4,undefined8 uParm5,undefined8 uParm6,uint uParm7);
undefined8 FUN_00409576(uint uParm1);
long FUN_004095c9(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_0040971f(long *plParm1,undefined8 uParm2);
long FUN_00409776(long lParm1,long lParm2);
ulong FUN_00409809(byte *pbParm1,ulong uParm2);
ulong FUN_0040985f(ulong uParm1);
ulong FUN_004098ca(ulong uParm1);
ulong FUN_00409911(undefined8 uParm1,ulong uParm2);
ulong FUN_00409948(ulong uParm1,ulong uParm2);
undefined8 FUN_00409961(long lParm1);
ulong FUN_00409a64(ulong uParm1,long lParm2);
long * FUN_00409b71(undefined8 uParm1,undefined1 *puParm2,code *pcParm3,code *pcParm4,long lParm5);
void FUN_00409cd6(long **pplParm1,undefined8 uParm2);
long FUN_00409e01(long lParm1);
void FUN_00409e4c(long lParm1,undefined8 *puParm2);
long FUN_00409e82(long lParm1,long lParm2,long **pplParm3,char cParm4);
undefined8 FUN_0040a017(long lParm1,long **pplParm2,char cParm3);
undefined8 FUN_0040a1e7(long *plParm1,undefined8 uParm2);
undefined8 FUN_0040a3e8(long lParm1,long lParm2,long *plParm3);
undefined8 FUN_0040a702(undefined8 uParm1,undefined8 uParm2);
long FUN_0040a74b(long lParm1,undefined8 uParm2);
void FUN_0040a9fa(void);
long FUN_0040aadf(long lParm1,ulong uParm2,byte *pbParm3,undefined8 uParm4);
char * FUN_0040abdb(ulong uParm1,long lParm2,uint uParm3,ulong uParm4,ulong uParm5);
undefined8 FUN_0040b53f(void);
ulong FUN_0040b560(char *pcParm1,undefined8 *puParm2,uint *puParm3);
ulong FUN_0040b6c7(undefined8 uParm1,undefined8 uParm2,long *plParm3);
ulong FUN_0040b719(ulong *puParm1,ulong uParm2);
ulong FUN_0040b784(ulong *puParm1,ulong *puParm2);
long * FUN_0040b7b6(long lParm1);
long FUN_0040b83a(undefined8 *puParm1,long lParm2);
char * FUN_0040b927(long lParm1,long lParm2);
ulong FUN_0040ba59(byte *pbParm1,byte *pbParm2);
void FUN_0040bdc5(char *pcParm1);
void FUN_0040bfa8(long lParm1);
ulong FUN_0040c08a(undefined1 *puParm1,byte bParm2,uint uParm3);
ulong * FUN_0040c112(ulong *puParm1,uint uParm2);
char * FUN_0040c1b1(char *pcParm1,int iParm2);
ulong FUN_0040c251(long param_1,ulong param_2,long param_3,ulong param_4,uint param_5,uint param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_0040d173(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_0040d3e3(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_0040d424(uint uParm1,undefined8 uParm2);
void FUN_0040d448(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_0040d4dc(undefined8 uParm1,char cParm2);
void FUN_0040d506(undefined8 uParm1);
void FUN_0040d525(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_0040d5c0(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040d5ec(uint uParm1,undefined8 uParm2);
void FUN_0040d615(undefined8 uParm1);
void FUN_0040d634(long lParm1);
void FUN_0040d64a(long lParm1);
void FUN_0040d660(long lParm1);
ulong FUN_0040d676(uint uParm1);
long FUN_0040d686(long lParm1,long lParm2);
ulong FUN_0040d6ca(long lParm1,int iParm2,long lParm3,int iParm4);
void FUN_0040d751(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_0040dbc0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
void FUN_0040dc93(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_0040dd4a(undefined8 uParm1,ulong uParm2,ulong uParm3);
void FUN_0040dda8(long lParm1,ulong *puParm2,ulong uParm3);
void FUN_0040de82(undefined8 uParm1);
long FUN_0040de9c(long lParm1);
long FUN_0040ded1(long lParm1,long lParm2);
void FUN_0040df32(undefined8 uParm1,undefined8 uParm2);
void FUN_0040df5c(undefined8 uParm1);
long FUN_0040df8d(ulong uParm1,ulong uParm2);
void FUN_0040dfee(undefined8 uParm1,undefined8 uParm2);
void FUN_0040e022(undefined8 uParm1);
long FUN_0040e04f(void);
long FUN_0040e079(undefined8 uParm1,uint uParm2,undefined8 uParm3);
ulong FUN_0040e0e1(long lParm1,long lParm2);
undefined8 FUN_0040e143(long *plParm1,int iParm2);
ulong FUN_0040e1e1(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_0040e222(char *pcParm1,char **ppcParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_0040e5c9(int iParm1);
ulong FUN_0040e5ef(ulong *puParm1,int iParm2);
ulong FUN_0040e64e(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_0040e68f(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
void FUN_0040ea78(uint uParm1,int iParm2,undefined uParm3,long lParm4,undefined8 uParm5,uint uParm6);
undefined8 FUN_0040eb52(uint uParm1,uint uParm2,char cParm3,undefined8 uParm4,undefined8 uParm5);
undefined8 FUN_0040eb98(int iParm1);
ulong FUN_0040ebbe(ulong *puParm1,int iParm2);
ulong FUN_0040ec1d(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_0040ec5e(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_0040f047(ulong uParm1,ulong uParm2);
ulong FUN_0040f0c6(uint uParm1);
void FUN_0040f0ef(void);
void FUN_0040f124(uint uParm1);
void FUN_0040f199(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_0040f21e(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_0040f303(undefined8 uParm1);
void FUN_0040f3b8(undefined8 uParm1);
void FUN_0040f3dd(void);
ulong FUN_0040f3ec(long lParm1);
ulong FUN_0040f4bc(byte *pbParm1,byte *pbParm2,byte *pbParm3,byte bParm4,uint uParm5);
char * FUN_004100c7(char *pcParm1);
undefined8 FUN_0041020d(int iParm1,long lParm2,ulong uParm3,ulong uParm4,byte bParm5,uint uParm6);
undefined8 FUN_00410a63(int *piParm1);
ulong FUN_00410aff(uint *puParm1,uint *puParm2,uint *puParm3,byte bParm4,uint uParm5);
int * FUN_004116a1(int *piParm1);
undefined8 FUN_004117e5(int iParm1,long lParm2,ulong uParm3,ulong uParm4,byte bParm5,uint uParm6);
ulong FUN_0041208d(undefined8 uParm1,long lParm2,uint uParm3);
undefined8 FUN_00412380(undefined8 uParm1);
void FUN_0041239f(undefined8 uParm1,undefined8 uParm2,uint uParm3);
ulong FUN_004123ca(long *plParm1,long *plParm2);
ulong FUN_0041241f(long lParm1,ulong uParm2);
undefined8 FUN_00412449(long lParm1);
undefined8 FUN_004124dd(long lParm1,undefined8 *puParm2);
void FUN_004125eb(long lParm1,long lParm2);
void FUN_004126f9(long lParm1);
void FUN_00412747(undefined8 uParm1);
void FUN_0041278b(long lParm1,char cParm2);
long FUN_004127cf(uint uParm1,undefined8 uParm2,uint uParm3,uint *puParm4);
void FUN_0041285d(long lParm1,uint uParm2,char cParm3);
ulong FUN_004128e9(long lParm1);
ulong FUN_00412993(long lParm1,undefined8 uParm2);
long * FUN_00412a1a(long *plParm1,uint uParm2,long lParm3);
void FUN_00412de2(long lParm1,long lParm2);
undefined8 FUN_00412e9d(long *plParm1);
ulong FUN_00413026(ulong *puParm1,ulong uParm2);
ulong FUN_00413057(ulong *puParm1,ulong *puParm2);
undefined8 FUN_00413089(long lParm1);
undefined8 FUN_00413200(undefined8 uParm1);
undefined8 FUN_00413236(undefined8 uParm1);
long FUN_004132b2(long *plParm1);
undefined8 FUN_004139a5(undefined8 uParm1,long lParm2,int iParm3);
ulong FUN_004139fc(long *plParm1,long *plParm2);
void FUN_00413a57(long lParm1,undefined4 uParm2);
long FUN_00413ac8(long *plParm1,int iParm2);
undefined8 FUN_00414407(long lParm1,long lParm2,char cParm3);
long FUN_004145e8(long lParm1,long lParm2,ulong uParm3);
long FUN_0041472f(long lParm1,undefined8 uParm2,long lParm3);
void FUN_004147e1(long lParm1);
undefined8 FUN_0041481e(long lParm1,long lParm2);
void FUN_004148eb(long lParm1,long lParm2);
long FUN_004149fa(long *plParm1);
ulong FUN_00414a50(long lParm1,long lParm2,uint uParm3,long lParm4);
void FUN_00414c82(void);
long FUN_00414c89(long *plParm1,ulong *puParm2,int iParm3,long lParm4);
void FUN_00414e62(long lParm1,int *piParm2);
ulong FUN_00415045(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_0041562e(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_004156fc(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
ulong FUN_00415ec5(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,uint param_6,uint param_7);
void FUN_00415f55(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
void FUN_00415f9f(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00415fc4(long lParm1,long lParm2);
undefined8 FUN_0041607b(long lParm1);
ulong FUN_004160ac(uint *puParm1,byte *pbParm2,long lParm3,undefined8 uParm4);
ulong * FUN_0041612f(ulong *puParm1,char cParm2,ulong uParm3);
undefined8 FUN_00416242(void);
void FUN_00416253(long lParm1);
char ** FUN_004163f8(void);
void FUN_00416d30(undefined8 *puParm1);
undefined8 FUN_00416d99(long lParm1,long lParm2);
undefined8 FUN_00416e02(int iParm1);
void FUN_00416e28(long lParm1,long lParm2);
void FUN_00416e97(long lParm1,long lParm2);
ulong FUN_00416f0f(long lParm1,long lParm2);
void FUN_00416f66(undefined8 uParm1);
void FUN_00416f8b(undefined8 uParm1);
void FUN_00416fb0(undefined8 uParm1,undefined8 uParm2);
void FUN_00416fdb(long lParm1);
void FUN_0041702b(long lParm1,long lParm2);
void FUN_00417096(long lParm1,long lParm2);
ulong FUN_00417101(long lParm1,long lParm2);
ulong FUN_00417175(long lParm1,long lParm2);
undefined8 FUN_004171be(void);
ulong FUN_004171d1(long param_1,undefined8 param_2,long param_3,long param_4,undefined8 param_5,byte param_6,long param_7);
ulong FUN_0041731a(long lParm1,undefined8 uParm2,long lParm3,long lParm4,byte bParm5,long lParm6);
undefined8 FUN_004174f5(long lParm1,ulong uParm2);
void FUN_0041766c(undefined8 uParm1,undefined8 uParm2,undefined8 *puParm3,long lParm4,char cParm5,long lParm6);
void FUN_0041775d(long *plParm1);
undefined8 FUN_00417a8a(long *plParm1);
long FUN_0041852c(long *plParm1,long lParm2,uint *puParm3);
void FUN_0041865f(long *plParm1);
void FUN_0041872e(long *plParm1);
ulong FUN_004187d0(byte **ppbParm1,byte *pbParm2,uint uParm3);
ulong FUN_00419495(long *plParm1,long lParm2);
ulong FUN_00419617(long *plParm1);
void FUN_004197a1(long lParm1);
ulong FUN_004197ef(long lParm1,ulong uParm2,uint uParm3);
undefined8 FUN_00419975(long *plParm1,long lParm2);
undefined8 FUN_004199df(undefined8 *puParm1,undefined8 uParm2);
undefined8 FUN_00419a66(undefined8 *puParm1,long lParm2,long lParm3);
undefined8 FUN_00419b41(long *plParm1,long lParm2);
undefined8 FUN_00419c1e(long *plParm1,long lParm2,long lParm3);
undefined8 FUN_0041a007(long *plParm1,long lParm2,long lParm3);
undefined8 FUN_0041a30f(long *plParm1,long lParm2);
ulong FUN_0041a6d1(long *plParm1,long lParm2);
undefined8 FUN_0041a8be(long *plParm1,undefined8 uParm2);
undefined8 FUN_0041a973(long lParm1,long lParm2);
long FUN_0041aa02(long lParm1,long lParm2);
void FUN_0041aaba(long lParm1,long lParm2);
long FUN_0041ab3a(long *plParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_0041aed5(long lParm1,uint uParm2);
ulong * FUN_0041af2f(undefined4 *puParm1,long lParm2,long lParm3);
ulong * FUN_0041b050(undefined4 *puParm1,long lParm2,long lParm3,uint uParm4);
undefined8 FUN_0041b17d(long *plParm1,ulong *puParm2,ulong uParm3);
void FUN_0041b331(long lParm1);
long FUN_0041b3d5(long *plParm1,long lParm2,undefined8 uParm3);
long FUN_0041b5a7(long *plParm1,long lParm2,uint uParm3,undefined8 uParm4);
undefined8 FUN_0041b88b(long *plParm1);
void FUN_0041b984(long **pplParm1,long lParm2,long lParm3);
ulong FUN_0041c066(undefined8 *puParm1,undefined8 uParm2,uint uParm3);
void FUN_0041c1d5(long *plParm1,undefined8 uParm2);
ulong FUN_0041c438(long *plParm1,undefined8 uParm2,undefined8 uParm3,ulong uParm4);
undefined8 FUN_0041c7b4(long *plParm1,ulong uParm2);
void FUN_0041cb31(long lParm1);
void FUN_0041ccb4(long *plParm1);
ulong FUN_0041cd44(long *plParm1);
void FUN_0041d08c(long *plParm1);
ulong FUN_0041d305(long *plParm1);
ulong FUN_0041d68a(long **pplParm1,code *pcParm2,undefined8 uParm3);
ulong FUN_0041d768(long *plParm1,code *pcParm2,undefined8 uParm3);
undefined8 FUN_0041d823(long lParm1,long lParm2);
ulong FUN_0041d99b(undefined8 uParm1,long lParm2);
long FUN_0041da77(undefined4 *puParm1,long *plParm2,long lParm3);
undefined8 FUN_0041dc68(long *plParm1,long lParm2);
undefined8 FUN_0041dd56(undefined8 uParm1,long lParm2);
ulong FUN_0041de03(long lParm1,long lParm2);
ulong FUN_0041e078(long *plParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
long FUN_0041e5a8(long *plParm1,long lParm2,uint uParm3);
long FUN_0041e641(long *plParm1,long lParm2,undefined4 uParm3);
undefined8 FUN_0041e773(long lParm1);
ulong FUN_0041e8b1(long lParm1);
ulong FUN_0041e994(undefined8 *puParm1,long *plParm2,long lParm3,char cParm4);
void FUN_0041ed66(undefined8 uParm1,long lParm2,undefined8 uParm3);
undefined8 FUN_0041eda9(long *plParm1,long lParm2,ulong uParm3);
ulong FUN_0041f63a(byte *pbParm1,long lParm2,ulong uParm3);
long FUN_0041f85d(undefined8 uParm1,long *plParm2,ulong uParm3,int *piParm4);
long FUN_0041f989(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_0041fb8b(undefined8 uParm1,undefined8 *puParm2,long lParm3,undefined8 uParm4,long lParm5,int *piParm6);
long FUN_0041fd4a(long lParm1,long *plParm2,undefined8 *puParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_004205ef(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,undefined8 uParm5,int *piParm6);
long FUN_00420783(long lParm1,long lParm2,undefined8 uParm3,undefined8 *puParm4,ulong uParm5,undefined4 *puParm6);
ulong FUN_00420cb4(byte bParm1,long lParm2);
undefined8 FUN_00420ceb(ulong uParm1,undefined8 uParm2,long lParm3,long *plParm4,int *piParm5,int *piParm6);
undefined8 FUN_004210ac(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,byte *pbParm4);
long FUN_0042110b(long lParm1,long lParm2,long lParm3,ulong uParm4,int *piParm5);
undefined8 FUN_00421a40(undefined4 *param_1,long param_2,undefined *param_3,int param_4,undefined8 param_5,undefined8 param_6,char param_7);
undefined8 FUN_00421b8b(undefined4 *puParm1,long lParm2,char *pcParm3);
undefined8 FUN_00421cf7(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,byte *pbParm4);
undefined8 FUN_00421d51(long lParm1,undefined8 uParm2,long lParm3,long *plParm4,undefined *puParm5,ulong uParm6);
long FUN_004226b8(long lParm1,undefined8 uParm2,undefined8 uParm3,char *pcParm4,byte bParm5,int *piParm6);
long FUN_00422950(undefined8 uParm1,byte *pbParm2,undefined8 uParm3);
void FUN_00422a37(undefined8 *puParm1);
void FUN_00422a71(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined uParm4);
undefined8 * FUN_00422aa8(long lParm1,undefined8 *puParm2,undefined8 *puParm3,undefined8 *puParm4);
undefined8 FUN_00422bf4(long lParm1,long lParm2);
void FUN_00422c37(undefined8 *puParm1);
undefined8 FUN_00422c9c(undefined8 uParm1,long lParm2);
long * FUN_00422cc3(long **pplParm1,undefined8 uParm2);
ulong FUN_00422def(long lParm1,undefined8 uParm2,undefined8 uParm3,undefined8 *puParm4,uint uParm5);
ulong FUN_00422eea(long *param_1,long param_2,long param_3,long param_4,long param_5,undefined8 param_6,ulong param_7,undefined8 *param_8,uint param_9);
ulong FUN_00423cab(long lParm1);
long FUN_00423fd0(long lParm1,char cParm2,long *plParm3);
undefined8 FUN_004244ec(long *plParm1,long lParm2,uint uParm3);
undefined8 FUN_004245b3(long lParm1,long lParm2,undefined8 uParm3);
long FUN_00424656(long param_1,undefined8 param_2,long param_3,long *param_4,long param_5,long param_6,long param_7);
ulong FUN_00424b95(long *plParm1,undefined8 uParm2,undefined8 uParm3,long lParm4,undefined8 uParm5,undefined8 uParm6);
undefined8 FUN_00424d65(long *plParm1,undefined8 *puParm2,long lParm3,undefined8 uParm4,undefined8 *puParm5);
undefined8 FUN_00424eb3(long *plParm1,long lParm2,ulong uParm3,long *plParm4,char cParm5);
undefined8 FUN_004252c3(long *plParm1);
void FUN_00425359(long *plParm1,long lParm2,long lParm3,long lParm4,long lParm5,long lParm6);
ulong FUN_004254fd(long lParm1,long *plParm2);
undefined8 FUN_0042569e(long lParm1,long *plParm2,long lParm3,undefined8 uParm4);
ulong FUN_004258a9(long lParm1,long lParm2);
ulong FUN_00425993(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_00425aed(long lParm1,long *plParm2,long lParm3,long lParm4);
ulong FUN_00425ccc(long lParm1,long lParm2,undefined8 uParm3);
ulong FUN_00425dfb(long *plParm1,long lParm2,undefined8 uParm3,undefined8 uParm4);
undefined8 FUN_00426089(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_004261f5(long lParm1,uint uParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_00426474(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long lParm5,undefined8 uParm6);
ulong FUN_00426541(long *plParm1,long lParm2,undefined8 uParm3,long lParm4,long lParm5,long lParm6);
ulong FUN_004268ba(long lParm1,long *plParm2,long lParm3,long lParm4);
ulong FUN_00426d8f(long lParm1,long *plParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_00426e69(int *piParm1,long lParm2,long lParm3);
long FUN_00426fdf(int *piParm1,long lParm2,long lParm3);
long FUN_00427265(int *piParm1,long lParm2);
ulong FUN_0042730f(long lParm1,long lParm2,undefined8 uParm3);
ulong FUN_0042740a(long lParm1,long lParm2);
ulong FUN_0042778c(long lParm1,long lParm2);
ulong FUN_00427cf8(long lParm1,long lParm2,long lParm3);
ulong FUN_0042826f(undefined8 uParm1,long *plParm2,undefined8 *puParm3,undefined8 uParm4,long lParm5);
long FUN_00428346(long *plParm1,long lParm2,long lParm3,uint uParm4);
ulong FUN_004283d2(long param_1,long *param_2,long param_3,long param_4,undefined8 param_5,long param_6,uint param_7);
ulong FUN_00428b5a(long lParm1,long lParm2,long lParm3,undefined8 uParm4);
ulong FUN_00428e1d(long lParm1,undefined8 *puParm2,undefined8 uParm3,uint uParm4);
ulong FUN_00428f91(long *plParm1,undefined8 uParm2,long lParm3,long lParm4,uint uParm5);
ulong FUN_00429156(long lParm1,undefined8 uParm2,long lParm3,undefined8 uParm4,uint uParm5);
undefined8 FUN_00429536(long lParm1,long lParm2);
long FUN_00429f5e(long *plParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_0042a804(long *plParm1,long lParm2,long lParm3,long lParm4);
undefined8 FUN_0042ac40(long lParm1,undefined8 *puParm2,long lParm3);
ulong FUN_0042ae1b(long lParm1,int iParm2);
undefined8 FUN_0042afa3(long lParm1,undefined4 uParm2,ulong uParm3);
void FUN_0042b0e1(long lParm1);
void FUN_0042b1f2(long lParm1);
undefined8 FUN_0042b233(long lParm1,undefined8 uParm2,long lParm3,long lParm4,long lParm5);
long FUN_0042b553(long lParm1,long lParm2);
undefined8 FUN_0042b629(long lParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 * FUN_0042b7a1(long lParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0042b8ad(undefined8 *puParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
void FUN_0042b915(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0042b93a(long lParm1,long lParm2);
long FUN_0042b9ca(long lParm1,ulong uParm2,long *plParm3);
long FUN_0042bbf2(long lParm1,long lParm2,long lParm3,ulong uParm4);
long FUN_0042beee(long lParm1,long lParm2,long lParm3,ulong uParm4);
char * FUN_0042c3a0(char *pcParm1,char *pcParm2);
ulong FUN_0042c4ee(int iParm1,int iParm2);
ulong FUN_0042c529(uint *puParm1,uint *puParm2);
void FUN_0042c5d1(long lParm1,undefined8 uParm2,long lParm3);
undefined8 * FUN_0042c60d(long lParm1);
undefined8 FUN_0042c6bd(long **pplParm1,char *pcParm2);
void FUN_0042c88a(undefined8 *puParm1);
void FUN_0042c8cc(void);
void FUN_0042c8dc(long lParm1);
ulong FUN_0042c913(long lParm1);
long FUN_0042c959(long lParm1);
ulong FUN_0042ca22(long lParm1);
undefined8 FUN_0042ca8a(long lParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_0042cb36(long lParm1,undefined8 uParm2);
void FUN_0042cc0b(long lParm1);
void FUN_0042cc3a(void);
ulong FUN_0042cccc(char *pcParm1);
ulong FUN_0042cd3e(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0042ce5d(ulong uParm1,undefined4 uParm2);
ulong FUN_0042ce94(byte *pbParm1,byte *pbParm2);
undefined8 FUN_0042cf0c(undefined8 uParm1);
ulong FUN_0042cf97(ulong uParm1);
void FUN_0042cfb3(long lParm1);
undefined8 FUN_0042cfd5(long *plParm1,long *plParm2);
void FUN_0042d0a9(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0042d1b8(void);
ulong FUN_0042d1c5(uint uParm1);
void FUN_0042d296(long lParm1,undefined4 uParm2);
ulong FUN_0042d2ef(long lParm1);
ulong FUN_0042d301(long lParm1,undefined4 uParm2);
ulong FUN_0042d389(long lParm1);
undefined * FUN_0042d40b(void);
char * FUN_0042d7d0(void);
ulong FUN_0042d89e(byte bParm1);
void FUN_0042d8d4(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,undefined8 param_2,uint param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_0042d9e5(uint uParm1);
ulong FUN_0042da32(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0042dceb(uint uParm1);
void FUN_0042dd4e(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
void FUN_0042dd7b(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
ulong FUN_0042ddb8(uint uParm1,long lParm2,long lParm3,uint uParm4);
void FUN_0042decb(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_0042defd(long lParm1,undefined4 uParm2);
ulong FUN_0042df76(ulong uParm1);
ulong FUN_0042e021(int iParm1,int iParm2);
long FUN_0042e05c(ulong param_1,long param_2,int param_3,int param_4,int param_5,uint param_6,int param_7,int param_8,int param_9,int param_10);
long FUN_0042e2c9(undefined8 uParm1,undefined8 uParm2);
long FUN_0042e318(undefined8 param_1,undefined8 param_2,uint param_3,uint param_4,uint param_5,long param_6,uint *param_7);
void FUN_0042e441(code *pcParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_0042e473(undefined8 uParm1,long *plParm2,undefined8 uParm3);
long FUN_0042e5b4(undefined8 *puParm1,undefined8 uParm2,long *plParm3);
void FUN_0042eb8e(undefined8 uParm1);
void FUN_0042ebb7(uint uParm1,undefined8 uParm2,uint uParm3,uint uParm4);
ulong FUN_0042ebe7(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,undefined8 param_2,uint param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_0042edae(char *pcParm1,char *pcParm2,uint uParm3);
ulong FUN_0042ef3e(void);
long FUN_0042ef8f(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
int * FUN_0042f1d8(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_0042fcba(ulong uParm1,long lParm2,long lParm3);
long FUN_0042ff2d(int *param_1,long *param_2);
long FUN_00430104(undefined auParm1 [16],int *piParm2,long *plParm3);
undefined8 FUN_0043047f(int iParm1,long lParm2,uint *puParm3,long lParm4,uint uParm5);
void FUN_00430c82(uint param_1);
void FUN_00430ccc(undefined auParm1 [16],uint uParm2);
ulong FUN_00430d1a(void);
ulong FUN_0043105c(undefined auParm1 [16],undefined8 uParm2,undefined8 uParm3);
ulong FUN_004313e6(char *pcParm1,long lParm2);
undefined8 FUN_0043143f(long param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
ulong FUN_00438b9b(uint uParm1);
undefined8 FUN_00438bba(uint uParm1,char cParm2);
undefined8 * FUN_00438c35(ulong uParm1);
void FUN_00438cf8(ulong uParm1);
void FUN_00438dd1(undefined auParm1 [16],int *piParm2,undefined8 uParm3);
void FUN_00438e5a(int *param_1);
void FUN_00438eef(uint uParm1);
ulong FUN_00438f15(ulong uParm1,long lParm2);
void FUN_00438f49(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00438f84(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00438fd5(ulong uParm1,ulong uParm2);
undefined8 FUN_00438ff0(uint *puParm1,ulong *puParm2);
undefined8 FUN_0043978c(char *pcParm1,ulong *puParm2,char **ppcParm3);
ulong FUN_0043aa5c(void);
ulong FUN_0043aa93(void);
void FUN_0043aac0(void);
undefined8 _DT_FINI(void);

