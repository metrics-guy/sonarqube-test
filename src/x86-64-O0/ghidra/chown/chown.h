typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00402620(void);
void entry(void);
void FUN_00402690(void);
void FUN_00402700(void);
void FUN_00402790(void);
void FUN_004027c5(undefined *puParm1);
undefined8 FUN_00402962(undefined8 uParm1);
ulong FUN_00402970(uint uParm1);
void FUN_004030a3(undefined4 *puParm1);
void FUN_004030f4(uint uParm1);
void FUN_00403139(uint uParm1);
undefined8 FUN_0040317e(long lParm1,long lParm2);
void FUN_00403234(undefined8 uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4,long lParm5,long lParm6);
ulong FUN_004033fe(uint param_1,undefined8 param_2,long *param_3,uint param_4,uint param_5,int param_6,int param_7);
ulong FUN_00403615(long param_1,long param_2,uint param_3,uint param_4,uint param_5,uint param_6,int *param_7);
ulong FUN_00403f55(undefined8 param_1,uint param_2,uint param_3,uint param_4,uint param_5,uint param_6,int *param_7);
void FUN_00404097(void);
void FUN_0040417d(uint uParm1,undefined8 uParm2,uint uParm3,uint uParm4);
void FUN_004041ae(uint uParm1,undefined8 uParm2,uint uParm3,uint uParm4);
char * FUN_004041df(ulong uParm1,long lParm2);
void FUN_00404264(long lParm1);
ulong FUN_00404346(undefined1 *puParm1,byte bParm2,uint uParm3);
ulong * FUN_004043ce(ulong *puParm1,uint uParm2);
char * FUN_0040446d(char *pcParm1,int iParm2);
ulong FUN_0040450d(long param_1,ulong param_2,long param_3,ulong param_4,uint param_5,uint param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_0040542f(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_0040569f(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_004056e0(uint uParm1,undefined8 uParm2);
void FUN_00405704(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_00405798(undefined8 uParm1,char cParm2);
void FUN_004057c2(undefined8 uParm1);
void FUN_004057e1(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_0040587c(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004058a8(uint uParm1,undefined8 uParm2);
void FUN_004058d1(undefined8 uParm1);
undefined8 * FUN_004058f0(undefined8 *puParm1);
undefined *FUN_0040594d(char *pcParm1,long lParm2,int *piParm3,uint *puParm4,char **ppcParm5,undefined8 *puParm6);
long FUN_00405d01(undefined8 uParm1,undefined8 uParm2,long lParm3,undefined8 uParm4,undefined8 uParm5);
void FUN_00405dd4(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_00406243(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
void FUN_00406316(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_004063cd(undefined8 uParm1);
long FUN_004063e7(long lParm1);
long FUN_0040641c(long lParm1,long lParm2);
void FUN_0040647d(undefined8 uParm1,undefined8 uParm2);
void FUN_004064b1(undefined8 uParm1);
long FUN_004064de(void);
long FUN_00406508(undefined8 uParm1,uint uParm2,undefined8 uParm3);
ulong FUN_00406570(long lParm1,long lParm2);
undefined8 FUN_004065d2(int iParm1);
ulong FUN_004065f8(ulong *puParm1,int iParm2);
ulong FUN_00406657(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_00406698(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_00406a81(ulong uParm1,ulong uParm2);
ulong FUN_00406b00(uint uParm1);
void FUN_00406b29(void);
void FUN_00406b5e(uint uParm1);
void FUN_00406bd3(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_00406c58(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_00406d3d(uint uParm1,char *pcParm2,uint uParm3,uint uParm4,uint uParm5);
void FUN_00406ff9(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
void FUN_00407026(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
ulong FUN_00407063(uint uParm1,long lParm2,long lParm3,uint uParm4);
ulong FUN_00407176(long *plParm1,long *plParm2);
ulong FUN_004071cb(long lParm1,ulong uParm2);
undefined8 FUN_004071f5(long lParm1);
undefined8 FUN_00407289(long lParm1,undefined8 *puParm2);
void FUN_00407397(long lParm1,long lParm2);
void FUN_004074a5(long lParm1);
void FUN_004074f3(undefined8 uParm1);
void FUN_00407537(long lParm1,char cParm2);
long FUN_0040757b(uint uParm1,undefined8 uParm2,uint uParm3,uint *puParm4);
void FUN_00407609(long lParm1,uint uParm2,char cParm3);
ulong FUN_00407695(long lParm1);
ulong FUN_0040773f(long lParm1,undefined8 uParm2);
long * FUN_004077c6(long *plParm1,uint uParm2,long lParm3);
void FUN_00407b8e(long lParm1,long lParm2);
undefined8 FUN_00407c49(long *plParm1);
ulong FUN_00407dd2(ulong *puParm1,ulong uParm2);
ulong FUN_00407e03(ulong *puParm1,ulong *puParm2);
undefined8 FUN_00407e35(long lParm1);
undefined8 FUN_00407fac(undefined8 uParm1);
undefined8 FUN_00407fe2(undefined8 uParm1);
long FUN_0040805e(long *plParm1);
undefined8 FUN_00408751(undefined8 uParm1,long lParm2,int iParm3);
ulong FUN_004087a8(long *plParm1,long *plParm2);
void FUN_00408803(long lParm1,undefined4 uParm2);
long FUN_00408874(long *plParm1,int iParm2);
undefined8 FUN_004091b3(long lParm1,long lParm2,char cParm3);
long FUN_00409394(long lParm1,long lParm2,ulong uParm3);
long FUN_004094db(long lParm1,undefined8 uParm2,long lParm3);
void FUN_0040958d(long lParm1);
undefined8 FUN_004095ca(long lParm1,long lParm2);
void FUN_00409697(long lParm1,long lParm2);
long FUN_004097a6(long *plParm1);
ulong FUN_004097fc(long lParm1,long lParm2,uint uParm3,long lParm4);
void FUN_00409a2e(long lParm1,int *piParm2);
ulong FUN_00409c11(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_0040a1fa(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_0040a2c8(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
ulong FUN_0040aa91(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,uint param_6,uint param_7);
void FUN_0040ab21(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
ulong FUN_0040ab6b(long lParm1,uint uParm2,uint uParm3);
void FUN_0040acd8(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040acfd(long lParm1,long lParm2);
undefined8 FUN_0040adb4(long lParm1);
ulong FUN_0040ade5(uint *puParm1,byte *pbParm2,long lParm3,undefined8 uParm4);
void FUN_0040ae68(uint uParm1,undefined8 uParm2,uint uParm3,uint uParm4);
ulong FUN_0040ae98(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,undefined8 param_2,uint param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined * FUN_0040b05f(undefined *puParm1,uint uParm2,char *pcParm3);
undefined8 FUN_0040b19d(long lParm1,long lParm2);
void FUN_0040b206(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040b22b(long lParm1,long lParm2);
ulong FUN_0040b2bb(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040b3da(byte *pbParm1,byte *pbParm2);
undefined8 FUN_0040b452(uint uParm1,char cParm2);
undefined8 FUN_0040b4cd(undefined8 uParm1);
ulong FUN_0040b558(ulong uParm1);
void FUN_0040b574(long lParm1);
undefined8 FUN_0040b596(long *plParm1,long *plParm2);
void FUN_0040b66a(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0040b779(void);
ulong FUN_0040b786(uint uParm1);
long FUN_0040b857(long *plParm1,undefined8 uParm2);
long FUN_0040b8ae(long lParm1,long lParm2);
ulong FUN_0040b941(ulong uParm1);
ulong FUN_0040b9ac(ulong uParm1);
ulong FUN_0040b9f3(undefined8 uParm1,ulong uParm2);
ulong FUN_0040ba2a(ulong uParm1,ulong uParm2);
undefined8 FUN_0040ba43(long lParm1);
ulong FUN_0040bb46(ulong uParm1,long lParm2);
long * FUN_0040bc53(undefined8 uParm1,undefined1 *puParm2,code *pcParm3,code *pcParm4,long lParm5);
void FUN_0040bdb8(long **pplParm1,undefined8 uParm2);
long FUN_0040bee3(long lParm1);
void FUN_0040bf2e(long lParm1,undefined8 *puParm2);
long FUN_0040bf64(long lParm1,long lParm2,long **pplParm3,char cParm4);
undefined8 FUN_0040c0f9(long lParm1,long **pplParm2,char cParm3);
undefined8 FUN_0040c2c9(long *plParm1,undefined8 uParm2);
undefined8 FUN_0040c4ca(long lParm1,long lParm2,long *plParm3);
undefined8 FUN_0040c7e4(undefined8 uParm1,undefined8 uParm2);
long FUN_0040c82d(long lParm1,undefined8 uParm2);
void FUN_0040cadc(long lParm1,undefined4 uParm2);
ulong FUN_0040cb35(long lParm1);
ulong FUN_0040cb47(long lParm1,undefined4 uParm2);
ulong FUN_0040cbcf(long lParm1);
undefined * FUN_0040cc51(void);
char * FUN_0040d016(void);
void FUN_0040d0e4(undefined auParm1 [16],undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint uParm9);
void FUN_0040d110(undefined auParm1 [16],undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint uParm9);
void FUN_0040d13c(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,undefined8 param_2,uint param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0040d24d(int *piParm1);
void FUN_0040d2c2(uint *puParm1);
void FUN_0040d2f9(uint *puParm1);
ulong FUN_0040d32f(uint uParm1);
ulong FUN_0040d33f(uint uParm1);
void FUN_0040d38c(undefined4 *puParm1);
void FUN_0040d3a1(uint *puParm1);
void FUN_0040d3bc(uint *puParm1);
undefined8 FUN_0040d40e(uint *puParm1,undefined8 uParm2);
long FUN_0040d468(long lParm1);
ulong FUN_0040d496(char *pcParm1);
ulong FUN_0040d781(long lParm1,uint uParm2,uint uParm3);
ulong FUN_0040d92a(undefined8 uParm1);
ulong FUN_0040d9df(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0040dc98(uint uParm1);
void FUN_0040dcfb(undefined8 uParm1);
void FUN_0040dd20(void);
ulong FUN_0040dd2f(long lParm1);
undefined8 FUN_0040ddff(undefined8 uParm1);
void FUN_0040de1e(undefined8 uParm1,undefined8 uParm2,uint uParm3);
long FUN_0040de49(long lParm1,undefined *puParm2);
ulong * FUN_0040e3e7(ulong *puParm1,char cParm2,ulong uParm3);
ulong FUN_0040e4fa(void);
long FUN_0040e54b(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
int * FUN_0040e794(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_0040f276(ulong uParm1,long lParm2,long lParm3);
long FUN_0040f4e9(int *param_1,long *param_2);
long FUN_0040f6c0(undefined auParm1 [16],int *piParm2,long *plParm3);
undefined8 FUN_0040fa3b(int iParm1,long lParm2,uint *puParm3,long lParm4,uint uParm5);
void FUN_0041023e(uint param_1);
void FUN_00410288(undefined auParm1 [16],uint uParm2);
ulong FUN_004102d6(void);
ulong FUN_00410618(undefined auParm1 [16],undefined8 uParm2,undefined8 uParm3);
ulong FUN_004109a2(char *pcParm1,long lParm2);
undefined8 FUN_004109fb(long param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
ulong FUN_00418157(ulong uParm1,undefined4 uParm2);
ulong FUN_0041818e(uint uParm1);
void FUN_004181ad(undefined auParm1 [16],int *piParm2,undefined8 uParm3);
void FUN_00418236(int *param_1);
void FUN_004182cb(uint uParm1);
ulong FUN_004182f1(ulong uParm1,long lParm2);
void FUN_00418325(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00418360(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_004183b1(ulong uParm1,ulong uParm2);
undefined8 FUN_004183cc(uint *puParm1,ulong *puParm2);
undefined8 FUN_00418b68(char *pcParm1,ulong *puParm2,char **ppcParm3);
ulong FUN_00419e38(void);
ulong FUN_00419e6f(void);
void FUN_00419ea0(void);
undefined8 _DT_FINI(void);

