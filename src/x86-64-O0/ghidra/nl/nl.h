typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined3;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined7;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void entry(void);
void FUN_00402600(void);
void FUN_00402670(void);
void FUN_00402700(void);
void FUN_00402735(void);
void FUN_0040275a(void);
void FUN_0040277f(undefined *puParm1);
ulong FUN_0040291c(uint uParm1);
ulong FUN_00402a79(char **ppcParm1,undefined8 *puParm2,undefined8 uParm3);
void FUN_00402b8e(void);
void FUN_00402c07(void);
void FUN_00402c4a(void);
void FUN_00402c8d(void);
void FUN_00402cd0(void);
undefined8 FUN_00402e98(void);
void FUN_00402f91(undefined8 uParm1);
undefined8 FUN_00402ff8(undefined8 uParm1);
ulong FUN_0040317a(uint uParm1,undefined8 *puParm2);
void FUN_004037ce(void);
void FUN_004038b4(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
void FUN_004038e5(long lParm1,uint uParm2);
void FUN_00403920(undefined8 uParm1);
void FUN_00403945(undefined8 uParm1,undefined8 uParm2);
long * FUN_0040396f(long *plParm1,undefined8 uParm2,char cParm3);
void FUN_00403aaf(long lParm1);
ulong FUN_00403b91(undefined1 *puParm1,byte bParm2,uint uParm3);
ulong * FUN_00403c19(ulong *puParm1,uint uParm2);
char * FUN_00403cb8(char *pcParm1,int iParm2);
ulong FUN_00403d58(long param_1,ulong param_2,long param_3,ulong param_4,uint param_5,uint param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_00404c7a(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_00404eea(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_00404f7e(undefined8 uParm1,char cParm2);
void FUN_00404fa8(undefined8 uParm1);
void FUN_00404fc7(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_00405062(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040508e(uint uParm1,undefined8 uParm2);
void FUN_004050b7(undefined8 uParm1);
void FUN_004050d6(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_00405545(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
void FUN_00405618(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_004056cf(long lParm1,ulong *puParm2,ulong uParm3);
void FUN_004057a9(undefined8 uParm1);
long FUN_004057c3(long lParm1);
long FUN_004057f8(long lParm1,long lParm2);
void FUN_00405859(undefined8 uParm1,undefined8 uParm2);
long FUN_00405883(void);
long FUN_004058ad(undefined8 param_1,uint param_2,long param_3,long param_4,undefined8 param_5,undefined8 param_6,uint param_7);
void FUN_004059d8(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,uint uParm6);
undefined8 FUN_00405a2d(long *plParm1,int iParm2);
ulong FUN_00405acb(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_00405b0c(char *pcParm1,char **ppcParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_00405eb3(ulong uParm1,ulong uParm2);
ulong FUN_00405f32(uint uParm1);
void FUN_00405f5b(void);
void FUN_00405f90(uint uParm1);
void FUN_00406005(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_0040608a(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_0040616f(undefined8 uParm1);
void FUN_00406224(undefined8 uParm1);
void FUN_00406249(void);
ulong FUN_00406258(long lParm1);
undefined8 FUN_00406328(undefined8 uParm1);
void FUN_00406347(undefined8 uParm1,undefined8 uParm2,uint uParm3);
void FUN_00406372(long lParm1,int *piParm2);
ulong FUN_00406555(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_00406b3e(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_00406c0c(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
ulong FUN_004073d5(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,uint param_6,uint param_7);
void FUN_00407465(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
undefined8 FUN_004074af(long lParm1);
ulong FUN_004074e0(uint *puParm1,byte *pbParm2,long lParm3,undefined8 uParm4);
undefined8 FUN_00407563(long lParm1,long lParm2);
undefined8 FUN_004075cc(int iParm1);
void FUN_004075f2(long lParm1,long lParm2);
void FUN_00407661(long lParm1,long lParm2);
ulong FUN_004076d9(long lParm1,long lParm2);
void FUN_00407730(undefined8 uParm1);
void FUN_00407755(undefined8 uParm1);
void FUN_0040777a(undefined8 uParm1,undefined8 uParm2);
void FUN_004077a5(long lParm1);
void FUN_004077f5(long lParm1,long lParm2);
void FUN_00407860(long lParm1,long lParm2);
ulong FUN_004078cb(long lParm1,long lParm2);
ulong FUN_0040793f(long lParm1,long lParm2);
undefined8 FUN_00407988(void);
ulong FUN_0040799b(long param_1,undefined8 param_2,long param_3,long param_4,undefined8 param_5,byte param_6,long param_7);
ulong FUN_00407ae4(long lParm1,undefined8 uParm2,long lParm3,long lParm4,byte bParm5,long lParm6);
undefined8 FUN_00407cbf(long lParm1,ulong uParm2);
void FUN_00407e36(undefined8 uParm1,undefined8 uParm2,undefined8 *puParm3,long lParm4,char cParm5,long lParm6);
void FUN_00407f27(long *plParm1);
undefined8 FUN_00408254(long *plParm1);
long FUN_00408cf6(long *plParm1,long lParm2,uint *puParm3);
void FUN_00408e29(long *plParm1);
void FUN_00408ef8(long *plParm1);
ulong FUN_00408f9a(byte **ppbParm1,byte *pbParm2,uint uParm3);
ulong FUN_00409c5f(long *plParm1,long lParm2);
ulong FUN_00409de1(long *plParm1);
void FUN_00409f6b(long lParm1);
ulong FUN_00409fb9(long lParm1,ulong uParm2,uint uParm3);
undefined8 FUN_0040a13f(long *plParm1,long lParm2);
undefined8 FUN_0040a1a9(undefined8 *puParm1,undefined8 uParm2);
undefined8 FUN_0040a230(undefined8 *puParm1,long lParm2,long lParm3);
undefined8 FUN_0040a30b(long *plParm1,long lParm2);
undefined8 FUN_0040a3e8(long *plParm1,long lParm2,long lParm3);
undefined8 FUN_0040a7d1(long *plParm1,long lParm2,long lParm3);
undefined8 FUN_0040aad9(long *plParm1,long lParm2);
ulong FUN_0040ae9b(long *plParm1,long lParm2);
undefined8 FUN_0040b088(long *plParm1,undefined8 uParm2);
undefined8 FUN_0040b13d(long lParm1,long lParm2);
long FUN_0040b1cc(long lParm1,long lParm2);
void FUN_0040b284(long lParm1,long lParm2);
long FUN_0040b304(long *plParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_0040b69f(long lParm1,uint uParm2);
ulong * FUN_0040b6f9(undefined4 *puParm1,long lParm2,long lParm3);
ulong * FUN_0040b81a(undefined4 *puParm1,long lParm2,long lParm3,uint uParm4);
undefined8 FUN_0040b947(long *plParm1,ulong *puParm2,ulong uParm3);
void FUN_0040bafb(long lParm1);
long FUN_0040bb9f(long *plParm1,long lParm2,undefined8 uParm3);
long FUN_0040bd71(long *plParm1,long lParm2,uint uParm3,undefined8 uParm4);
undefined1 * FUN_0040c055(undefined8 uParm1,undefined8 uParm2,long lParm3);
undefined8 FUN_0040c0e1(long *plParm1);
void FUN_0040c1da(long **pplParm1,long lParm2,long lParm3);
void FUN_0040c8bc(long *plParm1,undefined8 uParm2);
ulong FUN_0040cb1f(long *plParm1,undefined8 uParm2,undefined8 uParm3,ulong uParm4);
undefined8 FUN_0040ce9b(long *plParm1,ulong uParm2);
void FUN_0040d218(long lParm1);
void FUN_0040d39b(long *plParm1);
ulong FUN_0040d42b(long *plParm1);
void FUN_0040d773(long *plParm1);
ulong FUN_0040d9ec(long *plParm1);
ulong FUN_0040dd71(long **pplParm1,code *pcParm2,undefined8 uParm3);
ulong FUN_0040de4f(long *plParm1,code *pcParm2,undefined8 uParm3);
undefined8 FUN_0040df0a(long lParm1,long lParm2);
ulong FUN_0040e082(undefined8 uParm1,long lParm2);
long FUN_0040e15e(undefined4 *puParm1,long *plParm2,long lParm3);
undefined8 FUN_0040e34f(long *plParm1,long lParm2);
undefined8 FUN_0040e43d(undefined8 uParm1,long lParm2);
ulong FUN_0040e4ea(long lParm1,long lParm2);
ulong FUN_0040e75f(long *plParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
long FUN_0040ec8f(long *plParm1,long lParm2,uint uParm3);
long FUN_0040ed28(long *plParm1,long lParm2,undefined4 uParm3);
undefined8 FUN_0040ee5a(long lParm1);
ulong FUN_0040ef98(long lParm1);
ulong FUN_0040f07b(undefined8 *puParm1,long *plParm2,long lParm3,char cParm4);
void FUN_0040f44d(undefined8 uParm1,long lParm2,undefined8 uParm3);
undefined8 FUN_0040f490(long *plParm1,long lParm2,ulong uParm3);
ulong FUN_0040fd21(byte *pbParm1,long lParm2,ulong uParm3);
long FUN_0040ff44(undefined8 uParm1,long *plParm2,ulong uParm3,int *piParm4);
long FUN_00410070(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_00410272(undefined8 uParm1,undefined8 *puParm2,long lParm3,undefined8 uParm4,long lParm5,int *piParm6);
long FUN_00410431(long lParm1,long *plParm2,undefined8 *puParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_00410cd6(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,undefined8 uParm5,int *piParm6);
long FUN_00410e6a(long lParm1,long lParm2,undefined8 uParm3,undefined8 *puParm4,ulong uParm5,undefined4 *puParm6);
ulong FUN_0041139b(byte bParm1,long lParm2);
undefined8 FUN_004113d2(ulong uParm1,undefined8 uParm2,long lParm3,long *plParm4,int *piParm5,int *piParm6);
undefined8 FUN_00411793(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,byte *pbParm4);
long FUN_004117f2(long lParm1,long lParm2,long lParm3,ulong uParm4,int *piParm5);
undefined8 FUN_00412127(undefined4 *param_1,long param_2,undefined *param_3,int param_4,undefined8 param_5,undefined8 param_6,char param_7);
undefined8 FUN_00412272(undefined4 *puParm1,long lParm2,char *pcParm3);
undefined8 FUN_004123de(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,byte *pbParm4);
undefined8 FUN_00412438(long lParm1,undefined8 uParm2,long lParm3,long *plParm4,undefined *puParm5,ulong uParm6);
long FUN_00412d9f(long lParm1,undefined8 uParm2,undefined8 uParm3,char *pcParm4,byte bParm5,int *piParm6);
long FUN_00413037(undefined8 uParm1,byte *pbParm2,undefined8 uParm3);
void FUN_0041311e(undefined8 *puParm1);
void FUN_00413158(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined uParm4);
undefined8 * FUN_0041318f(long lParm1,undefined8 *puParm2,undefined8 *puParm3,undefined8 *puParm4);
undefined8 FUN_004132db(long lParm1,long lParm2);
void FUN_0041331e(undefined8 *puParm1);
undefined8 FUN_00413383(undefined8 uParm1,long lParm2);
long * FUN_004133aa(long **pplParm1,undefined8 uParm2);
void FUN_004134d6(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_00413527(long param_1,undefined8 param_2,ulong param_3,ulong param_4,ulong param_5,undefined8 param_6,ulong *param_7,char param_8);
ulong FUN_0041389c(ulong *puParm1,long lParm2,ulong uParm3,int iParm4);
ulong FUN_00413b42(long *param_1,long param_2,long param_3,long param_4,long param_5,undefined8 param_6,ulong param_7,undefined8 *param_8,uint param_9);
ulong FUN_00414903(long lParm1);
long FUN_00414c28(long lParm1,char cParm2,long *plParm3);
undefined8 FUN_00415144(long *plParm1,long lParm2,uint uParm3);
undefined8 FUN_0041520b(long lParm1,long lParm2,undefined8 uParm3);
long FUN_004152ae(long param_1,undefined8 param_2,long param_3,long *param_4,long param_5,long param_6,long param_7);
ulong FUN_004157ed(long *plParm1,undefined8 uParm2,undefined8 uParm3,long lParm4,undefined8 uParm5,undefined8 uParm6);
undefined8 FUN_004159bd(long *plParm1,undefined8 *puParm2,long lParm3,undefined8 uParm4,undefined8 *puParm5);
undefined8 FUN_00415b0b(long *plParm1,long lParm2,ulong uParm3,long *plParm4,char cParm5);
undefined8 FUN_00415f1b(long *plParm1);
void FUN_00415fb1(long *plParm1,long lParm2,long lParm3,long lParm4,long lParm5,long lParm6);
ulong FUN_00416155(long lParm1,long *plParm2);
undefined8 FUN_004162f6(long lParm1,long *plParm2,long lParm3,undefined8 uParm4);
ulong FUN_00416501(long lParm1,long lParm2);
ulong FUN_004165eb(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_00416745(long lParm1,long *plParm2,long lParm3,long lParm4);
ulong FUN_00416924(long lParm1,long lParm2,undefined8 uParm3);
ulong FUN_00416a53(long *plParm1,long lParm2,undefined8 uParm3,undefined8 uParm4);
undefined8 FUN_00416ce1(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_00416e4d(long lParm1,uint uParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_004170cc(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long lParm5,undefined8 uParm6);
ulong FUN_00417199(long *plParm1,long lParm2,undefined8 uParm3,long lParm4,long lParm5,long lParm6);
ulong FUN_00417512(long lParm1,long *plParm2,long lParm3,long lParm4);
ulong FUN_004179e7(long lParm1,long *plParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_00417ac1(int *piParm1,long lParm2,long lParm3);
long FUN_00417c37(int *piParm1,long lParm2,long lParm3);
long FUN_00417ebd(int *piParm1,long lParm2);
ulong FUN_00417f67(long lParm1,long lParm2,undefined8 uParm3);
ulong FUN_00418062(long lParm1,long lParm2);
ulong FUN_004183e4(long lParm1,long lParm2);
ulong FUN_00418950(long lParm1,long lParm2,long lParm3);
ulong FUN_00418ec7(undefined8 uParm1,long *plParm2,undefined8 *puParm3,undefined8 uParm4,long lParm5);
long FUN_00418f9e(long *plParm1,long lParm2,long lParm3,uint uParm4);
ulong FUN_0041902a(long param_1,long *param_2,long param_3,long param_4,undefined8 param_5,long param_6,uint param_7);
ulong FUN_004197b2(long lParm1,long lParm2,long lParm3,undefined8 uParm4);
ulong FUN_00419a75(long lParm1,undefined8 *puParm2,undefined8 uParm3,uint uParm4);
ulong FUN_00419be9(long *plParm1,undefined8 uParm2,long lParm3,long lParm4,uint uParm5);
ulong FUN_00419dae(long lParm1,undefined8 uParm2,long lParm3,undefined8 uParm4,uint uParm5);
undefined8 FUN_0041a18e(long lParm1,long lParm2);
long FUN_0041abb6(long *plParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_0041b45c(long *plParm1,long lParm2,long lParm3,long lParm4);
undefined8 FUN_0041b898(long lParm1,undefined8 *puParm2,long lParm3);
ulong FUN_0041ba73(long lParm1,int iParm2);
undefined8 FUN_0041bbfb(long lParm1,undefined4 uParm2,ulong uParm3);
void FUN_0041bd39(long lParm1);
void FUN_0041be4a(long lParm1);
undefined8 FUN_0041be8b(long lParm1,undefined8 uParm2,long lParm3,long lParm4,long lParm5);
long FUN_0041c1ab(long lParm1,long lParm2);
undefined8 FUN_0041c281(long lParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 * FUN_0041c3f9(long lParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0041c505(undefined8 *puParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
ulong FUN_0041c56d(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0041c68c(byte *pbParm1,byte *pbParm2);
undefined8 FUN_0041c704(undefined8 uParm1);
undefined8 FUN_0041c78f(void);
ulong FUN_0041c79c(uint uParm1);
undefined * FUN_0041c86d(void);
char * FUN_0041cc32(void);
ulong FUN_0041cd00(void);
long FUN_0041cd51(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
int * FUN_0041cf9a(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_0041da7c(ulong uParm1,long lParm2,long lParm3);
long FUN_0041dcef(int *param_1,long *param_2);
long FUN_0041dec6(undefined auParm1 [16],int *piParm2,long *plParm3);
undefined8 FUN_0041e241(int iParm1,long lParm2,uint *puParm3,long lParm4,uint uParm5);
void FUN_0041ea44(uint param_1);
void FUN_0041ea8e(undefined auParm1 [16],uint uParm2);
ulong FUN_0041eadc(void);
ulong FUN_0041ee1e(undefined auParm1 [16],undefined8 uParm2,undefined8 uParm3);
ulong FUN_0041f1a8(char *pcParm1,long lParm2);
undefined8 FUN_0041f201(long param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
ulong FUN_0042695d(uint uParm1);
void FUN_0042697c(undefined auParm1 [16],int *piParm2,undefined8 uParm3);
void FUN_00426a05(int *param_1);
ulong FUN_00426a9a(ulong uParm1,long lParm2);
void FUN_00426ace(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00426b09(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00426b5a(ulong uParm1,ulong uParm2);
undefined8 FUN_00426b75(uint *puParm1,ulong *puParm2);
undefined8 FUN_00427311(char *pcParm1,ulong *puParm2,char **ppcParm3);
ulong FUN_004285e1(void);
ulong FUN_00428618(void);
void FUN_00428650(void);
undefined8 _DT_FINI(void);

