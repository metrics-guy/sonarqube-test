typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00402220(void);
void FUN_004024c0(void);
void entry(void);
void FUN_00402700(void);
void FUN_00402770(void);
void FUN_00402800(void);
ulong FUN_00402835(byte bParm1);
void FUN_00402844(void);
void FUN_00402869(undefined *puParm1);
void FUN_00402a06(undefined8 uParm1,undefined8 uParm2);
undefined1 * FUN_00402a2b(ulong *puParm1);
undefined1 * FUN_00403a5b(undefined8 uParm1);
undefined1 * FUN_00403a86(undefined8 uParm1,undefined8 uParm2);
void FUN_00403b4d(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_00403c25(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00403c6e(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00403cb6(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00403cfe(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00403d47(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00403d90(undefined8 uParm1,undefined8 uParm2);
void FUN_00403dd9(char *pcParm1,char *pcParm2,undefined8 uParm3,long lParm4,long lParm5);
ulong FUN_0040420a(long lParm1,long lParm2,undefined8 uParm3);
undefined8 FUN_004042f8(undefined8 uParm1,undefined8 uParm2,undefined4 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 *puParm6);
char * FUN_00404570(undefined8 uParm1);
ulong FUN_004046bd(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,long lParm4);
void FUN_00404803(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00404834(undefined8 uParm1,long lParm2);
void FUN_00404876(void);
ulong FUN_0040491c(undefined8 uParm1,undefined8 uParm2,undefined4 uParm3,uint uParm4,undefined8 uParm5,undefined8 *puParm6);
void FUN_00404fe7(char cParm1);
ulong FUN_00405080(char *pcParm1,uint uParm2,undefined8 uParm3,code *pcParm4,undefined8 uParm5);
ulong FUN_004054de(undefined8 uParm1,undefined8 uParm2);
ulong FUN_004055ee(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_00405783(char cParm1,char cParm2,char cParm3);
ulong FUN_004058bb(uint uParm1);
undefined8 FUN_00405da0(undefined8 uParm1,long *plParm2);
long FUN_00406247(undefined8 uParm1,ulong uParm2);
long FUN_00406395(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
void FUN_004064c4(undefined8 uParm1);
ulong FUN_004064e3(long *plParm1,undefined8 uParm2,undefined8 uParm3);
char * FUN_0040658a(char *pcParm1,uint uParm2);
void FUN_00406d66(void);
long FUN_00406e4c(undefined8 uParm1);
ulong FUN_00406e7a(char *pcParm1);
undefined * FUN_00406efb(undefined8 uParm1);
char * FUN_00406f92(char *pcParm1);
void FUN_00406ffb(long lParm1,undefined8 uParm2,undefined8 *puParm3);
ulong FUN_00407097(long lParm1,undefined8 uParm2,undefined8 *puParm3);
undefined * FUN_004070f3(long lParm1);
undefined8 FUN_004071d5(uint uParm1);
void FUN_00407280(uint uParm1,undefined *puParm2);
void FUN_00407448(long lParm1,undefined8 uParm2);
long FUN_00407470(long *plParm1,undefined8 uParm2);
long FUN_004074c7(long lParm1,long lParm2);
ulong FUN_0040755a(ulong uParm1);
ulong FUN_004075c5(ulong uParm1);
ulong FUN_0040760c(undefined8 uParm1,ulong uParm2);
ulong FUN_00407643(ulong uParm1,ulong uParm2);
undefined8 FUN_0040765c(long lParm1);
ulong FUN_0040775f(ulong uParm1,long lParm2);
long * FUN_0040786c(undefined8 uParm1,undefined1 *puParm2,code *pcParm3,code *pcParm4,long lParm5);
void FUN_004079d1(long **pplParm1,undefined8 uParm2);
long FUN_00407afc(long lParm1);
void FUN_00407b47(long lParm1,undefined8 *puParm2);
long FUN_00407b7d(long lParm1,long lParm2,long **pplParm3,char cParm4);
undefined8 FUN_00407d12(long lParm1,long **pplParm2,char cParm3);
undefined8 FUN_00407ee2(long *plParm1,undefined8 uParm2);
undefined8 FUN_004080e3(long lParm1,long lParm2,long *plParm3);
undefined8 FUN_004083fd(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00408446(undefined8 *puParm1,ulong uParm2);
ulong FUN_00408492(undefined8 *puParm1,undefined8 *puParm2);
void FUN_0040850a(undefined8 *puParm1);
char * FUN_0040853c(long lParm1,long lParm2);
long FUN_0040866e(long lParm1,long lParm2,long lParm3);
long FUN_004086c4(long lParm1,long lParm2,long lParm3);
ulong FUN_0040871a(int iParm1,int iParm2);
void FUN_0040876e(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,uint uParm6);
char * FUN_004087c4(char *pcParm1,long lParm2,char *pcParm3,undefined8 *puParm4,undefined uParm5,undefined8 uParm6,undefined8 uParm7,uint uParm8);
void FUN_0040a98c(long lParm1);
ulong FUN_0040aa6e(undefined1 *puParm1);
void FUN_0040aa8c(undefined1 *puParm1,undefined4 uParm2);
ulong FUN_0040aab1(undefined1 *puParm1,byte bParm2,uint uParm3);
ulong * FUN_0040ab39(ulong *puParm1,uint uParm2);
char * FUN_0040abd8(char *pcParm1,int iParm2);
ulong FUN_0040ac78(long param_1,ulong param_2,long param_3,ulong param_4,uint param_5,uint param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_0040bb9a(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_0040be0a(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_0040be4b(uint uParm1,undefined8 uParm2);
void FUN_0040be6f(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_0040bf03(undefined8 uParm1,char cParm2);
void FUN_0040bf2d(undefined8 uParm1);
void FUN_0040bf4c(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040bf78(uint uParm1,undefined8 uParm2);
void FUN_0040bfa1(undefined8 uParm1);
undefined8 FUN_0040bfc0(int *piParm1);
void FUN_0040c035(uint *puParm1);
void FUN_0040c06c(uint *puParm1);
void FUN_0040c0a2(void);
undefined8 FUN_0040c0ad(void);
undefined8 FUN_0040c0cf(void);
void FUN_0040c0f1(long lParm1);
void FUN_0040c107(long lParm1);
void FUN_0040c11d(long lParm1);
void FUN_0040c133(void);
ulong FUN_0040c155(uint uParm1);
void FUN_0040c165(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_0040c5d4(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
void FUN_0040c6a7(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_0040c75e(undefined8 uParm1);
long FUN_0040c778(long lParm1);
long FUN_0040c7ad(long lParm1,long lParm2);
void FUN_0040c80e(undefined8 uParm1,undefined8 uParm2);
void FUN_0040c842(undefined8 uParm1);
long FUN_0040c86f(void);
long FUN_0040c899(void);
undefined8 FUN_0040c8d2(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0040c981(ulong uParm1,ulong uParm2);
void FUN_0040ca00(undefined4 *puParm1);
void FUN_0040ca15(uint *puParm1);
void FUN_0040ca30(uint *puParm1);
undefined8 FUN_0040ca82(uint *puParm1,undefined8 uParm2);
long FUN_0040cadc(long lParm1);
ulong FUN_0040cb0a(char *pcParm1);
ulong FUN_0040cdf5(uint uParm1);
void FUN_0040ce1e(void);
void FUN_0040ce53(uint uParm1);
void FUN_0040cec8(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_0040cf4d(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
long FUN_0040d032(long lParm1,undefined *puParm2);
void FUN_0040d5d0(long lParm1,int *piParm2);
ulong FUN_0040d7b3(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_0040dd9c(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_0040de6a(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
ulong FUN_0040e633(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,uint param_6,uint param_7);
void FUN_0040e6c3(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
void FUN_0040e70d(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040e732(long lParm1,long lParm2);
undefined8 FUN_0040e7e9(long lParm1);
ulong FUN_0040e81a(uint *puParm1,byte *pbParm2,long lParm3,undefined8 uParm4);
ulong * FUN_0040e89d(ulong *puParm1,char cParm2,ulong uParm3);
undefined8 FUN_0040e9b0(void);
void FUN_0040e9c1(long lParm1);
char ** FUN_0040eb66(void);
void FUN_0040f49e(undefined8 *puParm1);
void FUN_0040f507(uint uParm1,undefined8 uParm2,uint uParm3,uint uParm4);
ulong FUN_0040f537(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,undefined8 param_2,uint param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0040f6fe(long lParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_0040f7a4(long lParm1,long lParm2);
void FUN_0040f80d(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040f832(long lParm1,long lParm2);
long FUN_0040f8c2(long lParm1,ulong uParm2,long *plParm3);
long FUN_0040faea(long lParm1,long lParm2,long lParm3,ulong uParm4);
long FUN_0040fde6(long lParm1,long lParm2,long lParm3,ulong uParm4);
char * FUN_00410298(char *pcParm1,char *pcParm2);
ulong FUN_004103e6(int iParm1,int iParm2);
ulong FUN_00410421(uint *puParm1,uint *puParm2);
void FUN_004104c9(long lParm1,undefined8 uParm2,long lParm3);
undefined8 * FUN_00410505(long lParm1);
undefined8 FUN_004105b5(long **pplParm1,char *pcParm2);
void FUN_00410782(undefined8 *puParm1);
void FUN_004107c4(void);
void FUN_004107d4(long lParm1);
ulong FUN_0041080b(long lParm1);
long FUN_00410851(long lParm1);
ulong FUN_0041091a(long lParm1);
undefined8 FUN_00410982(long lParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_00410a2e(long lParm1,undefined8 uParm2);
void FUN_00410b03(long lParm1);
void FUN_00410b32(void);
ulong FUN_00410bc4(char *pcParm1);
ulong FUN_00410c36(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00410d55(ulong uParm1,undefined4 uParm2);
ulong FUN_00410d8c(byte *pbParm1,byte *pbParm2);
undefined8 FUN_00410e04(uint uParm1,char cParm2);
undefined8 FUN_00410e7f(undefined8 uParm1);
void FUN_00410f0a(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_00411019(void);
ulong FUN_00411026(uint uParm1);
ulong FUN_004110f7(char *pcParm1,ulong uParm2);
undefined * FUN_00411151(void);
char * FUN_00411516(void);
ulong FUN_004115e4(uint uParm1);
undefined * FUN_00411631(long lParm1,ulong *puParm2);
undefined8 FUN_004117b4(char *pcParm1,undefined8 uParm2);
ulong FUN_00411858(undefined8 uParm1);
ulong FUN_0041190d(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_00411bc6(uint uParm1);
void FUN_00411c29(undefined8 uParm1);
void FUN_00411c4e(void);
ulong FUN_00411c5d(long lParm1);
undefined8 FUN_00411d2d(undefined8 uParm1);
void FUN_00411d4c(undefined8 uParm1,undefined8 uParm2,uint uParm3);
void FUN_00411d77(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
void FUN_00411da4(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
ulong FUN_00411de1(uint uParm1,long lParm2,long lParm3,uint uParm4);
void FUN_00411ef4(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_00411f26(long lParm1,undefined4 uParm2);
ulong FUN_00411f9f(ulong uParm1);
ulong FUN_0041204a(int iParm1,int iParm2);
long FUN_00412085(ulong param_1,long param_2,int param_3,int param_4,int param_5,uint param_6,int param_7,int param_8,int param_9,int param_10);
long FUN_004122f2(undefined8 uParm1,undefined8 uParm2);
long FUN_00412341(undefined8 param_1,undefined8 param_2,uint param_3,uint param_4,uint param_5,long param_6,uint *param_7);
void FUN_0041246a(code *pcParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_0041249c(undefined8 uParm1,long *plParm2,undefined8 uParm3);
long FUN_004125dd(undefined8 *puParm1,undefined8 uParm2,long *plParm3);
void FUN_00412bb7(undefined8 uParm1);
ulong FUN_00412be0(char *pcParm1,char *pcParm2,uint uParm3);
ulong FUN_00412d70(void);
long FUN_00412dc1(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
int * FUN_0041300a(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_00413aec(ulong uParm1,long lParm2,long lParm3);
long FUN_00413d5f(int *param_1,long *param_2);
long FUN_00413f36(undefined auParm1 [16],int *piParm2,long *plParm3);
undefined8 FUN_004142b1(int iParm1,long lParm2,uint *puParm3,long lParm4,uint uParm5);
void FUN_00414ab4(uint param_1);
void FUN_00414afe(undefined auParm1 [16],uint uParm2);
ulong FUN_00414b4c(void);
ulong FUN_00414e8e(undefined auParm1 [16],undefined8 uParm2,undefined8 uParm3);
ulong FUN_00415218(char *pcParm1,long lParm2);
undefined8 FUN_00415271(long param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
char * FUN_00415534(char *pcParm1,char **ppcParm2,long lParm3,undefined8 uParm4);
ulong FUN_0041c9cd(long *plParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0041ca49(uint uParm1);
undefined8 * FUN_0041ca68(ulong uParm1);
void FUN_0041cb2b(ulong uParm1);
void FUN_0041cc04(undefined auParm1 [16],int *piParm2,undefined8 uParm3);
void FUN_0041cc8d(int *param_1);
void FUN_0041cd22(uint uParm1);
ulong FUN_0041cd48(ulong uParm1,long lParm2);
void FUN_0041cd7c(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0041cdb7(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_0041ce08(ulong uParm1,ulong uParm2);
void FUN_0041ce23(void);
long FUN_0041ce2a(long *plParm1,ulong *puParm2,int iParm3,long lParm4);
undefined8 FUN_0041d003(uint *puParm1,ulong *puParm2);
undefined8 FUN_0041d79f(char *pcParm1,ulong *puParm2,char **ppcParm3);
ulong FUN_0041ea6f(void);
ulong FUN_0041eaa6(void);
void FUN_0041eae0(void);
undefined8 _DT_FINI(void);

