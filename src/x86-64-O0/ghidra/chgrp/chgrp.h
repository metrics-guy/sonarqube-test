typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00402600(void);
void entry(void);
void FUN_00402670(void);
void FUN_004026e0(void);
void FUN_00402770(void);
void FUN_004027a5(undefined *puParm1);
ulong FUN_00402942(char *pcParm1);
ulong FUN_004029f5(uint uParm1);
void FUN_00402fd5(undefined4 *puParm1);
void FUN_00403026(uint uParm1);
void FUN_0040306b(uint uParm1);
undefined8 FUN_004030b0(long lParm1,long lParm2);
void FUN_00403166(undefined8 uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4,long lParm5,long lParm6);
ulong FUN_00403330(uint param_1,undefined8 param_2,long *param_3,uint param_4,uint param_5,int param_6,int param_7);
ulong FUN_00403547(long param_1,long param_2,uint param_3,uint param_4,uint param_5,uint param_6,int *param_7);
ulong FUN_00403e87(undefined8 param_1,uint param_2,uint param_3,uint param_4,uint param_5,uint param_6,int *param_7);
void FUN_00403fc9(void);
void FUN_004040af(uint uParm1,undefined8 uParm2,uint uParm3,uint uParm4);
void FUN_004040e0(uint uParm1,undefined8 uParm2,uint uParm3,uint uParm4);
char * FUN_00404111(ulong uParm1,long lParm2);
void FUN_00404196(long lParm1);
ulong FUN_00404278(undefined1 *puParm1,byte bParm2,uint uParm3);
ulong * FUN_00404300(ulong *puParm1,uint uParm2);
char * FUN_0040439f(char *pcParm1,int iParm2);
ulong FUN_0040443f(long param_1,ulong param_2,long param_3,ulong param_4,uint param_5,uint param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_00405361(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_004055d1(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_00405612(uint uParm1,undefined8 uParm2);
void FUN_00405636(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_004056ca(undefined8 uParm1,char cParm2);
void FUN_004056f4(undefined8 uParm1);
void FUN_00405713(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_004057ae(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004057da(uint uParm1,undefined8 uParm2);
void FUN_00405803(undefined8 uParm1);
undefined8 * FUN_00405822(undefined8 *puParm1);
void FUN_0040587f(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_00405cee(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
void FUN_00405dc1(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00405e78(undefined8 uParm1);
long FUN_00405e92(long lParm1);
long FUN_00405ec7(long lParm1,long lParm2);
void FUN_00405f28(undefined8 uParm1,undefined8 uParm2);
void FUN_00405f5c(undefined8 uParm1);
long FUN_00405f89(void);
long FUN_00405fb3(undefined8 uParm1,uint uParm2,undefined8 uParm3);
ulong FUN_0040601b(long lParm1,long lParm2);
undefined8 FUN_0040607d(int iParm1);
ulong FUN_004060a3(ulong *puParm1,int iParm2);
ulong FUN_00406102(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_00406143(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_0040652c(ulong uParm1,ulong uParm2);
ulong FUN_004065ab(uint uParm1);
void FUN_004065d4(void);
void FUN_00406609(uint uParm1);
void FUN_0040667e(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_00406703(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_004067e8(uint uParm1,char *pcParm2,uint uParm3,uint uParm4,uint uParm5);
void FUN_00406aa4(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
void FUN_00406ad1(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
ulong FUN_00406b0e(uint uParm1,long lParm2,long lParm3,uint uParm4);
ulong FUN_00406c21(long *plParm1,long *plParm2);
ulong FUN_00406c76(long lParm1,ulong uParm2);
undefined8 FUN_00406ca0(long lParm1);
undefined8 FUN_00406d34(long lParm1,undefined8 *puParm2);
void FUN_00406e42(long lParm1,long lParm2);
void FUN_00406f50(long lParm1);
void FUN_00406f9e(undefined8 uParm1);
void FUN_00406fe2(long lParm1,char cParm2);
long FUN_00407026(uint uParm1,undefined8 uParm2,uint uParm3,uint *puParm4);
void FUN_004070b4(long lParm1,uint uParm2,char cParm3);
ulong FUN_00407140(long lParm1);
ulong FUN_004071ea(long lParm1,undefined8 uParm2);
long * FUN_00407271(long *plParm1,uint uParm2,long lParm3);
void FUN_00407639(long lParm1,long lParm2);
undefined8 FUN_004076f4(long *plParm1);
ulong FUN_0040787d(ulong *puParm1,ulong uParm2);
ulong FUN_004078ae(ulong *puParm1,ulong *puParm2);
undefined8 FUN_004078e0(long lParm1);
undefined8 FUN_00407a57(undefined8 uParm1);
undefined8 FUN_00407a8d(undefined8 uParm1);
long FUN_00407b09(long *plParm1);
undefined8 FUN_004081fc(undefined8 uParm1,long lParm2,int iParm3);
ulong FUN_00408253(long *plParm1,long *plParm2);
void FUN_004082ae(long lParm1,undefined4 uParm2);
long FUN_0040831f(long *plParm1,int iParm2);
undefined8 FUN_00408c5e(long lParm1,long lParm2,char cParm3);
long FUN_00408e3f(long lParm1,long lParm2,ulong uParm3);
long FUN_00408f86(long lParm1,undefined8 uParm2,long lParm3);
void FUN_00409038(long lParm1);
undefined8 FUN_00409075(long lParm1,long lParm2);
void FUN_00409142(long lParm1,long lParm2);
long FUN_00409251(long *plParm1);
ulong FUN_004092a7(long lParm1,long lParm2,uint uParm3,long lParm4);
void FUN_004094d9(long lParm1,int *piParm2);
ulong FUN_004096bc(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_00409ca5(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_00409d73(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
ulong FUN_0040a53c(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,uint param_6,uint param_7);
void FUN_0040a5cc(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
ulong FUN_0040a616(long lParm1,uint uParm2,uint uParm3);
void FUN_0040a783(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040a7a8(long lParm1,long lParm2);
undefined8 FUN_0040a85f(long lParm1);
ulong FUN_0040a890(uint *puParm1,byte *pbParm2,long lParm3,undefined8 uParm4);
void FUN_0040a913(uint uParm1,undefined8 uParm2,uint uParm3,uint uParm4);
ulong FUN_0040a943(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,undefined8 param_2,uint param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined * FUN_0040ab0a(undefined *puParm1,uint uParm2,char *pcParm3);
undefined8 FUN_0040ac48(long lParm1,long lParm2);
void FUN_0040acb1(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040acd6(long lParm1,long lParm2);
ulong FUN_0040ad66(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040ae85(byte *pbParm1,byte *pbParm2);
undefined8 FUN_0040aefd(uint uParm1,char cParm2);
undefined8 FUN_0040af78(undefined8 uParm1);
ulong FUN_0040b003(ulong uParm1);
void FUN_0040b01f(long lParm1);
undefined8 FUN_0040b041(long *plParm1,long *plParm2);
void FUN_0040b115(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0040b224(void);
ulong FUN_0040b231(uint uParm1);
long FUN_0040b302(long *plParm1,undefined8 uParm2);
long FUN_0040b359(long lParm1,long lParm2);
ulong FUN_0040b3ec(ulong uParm1);
ulong FUN_0040b457(ulong uParm1);
ulong FUN_0040b49e(undefined8 uParm1,ulong uParm2);
ulong FUN_0040b4d5(ulong uParm1,ulong uParm2);
undefined8 FUN_0040b4ee(long lParm1);
ulong FUN_0040b5f1(ulong uParm1,long lParm2);
long * FUN_0040b6fe(undefined8 uParm1,undefined1 *puParm2,code *pcParm3,code *pcParm4,long lParm5);
void FUN_0040b863(long **pplParm1,undefined8 uParm2);
long FUN_0040b98e(long lParm1);
void FUN_0040b9d9(long lParm1,undefined8 *puParm2);
long FUN_0040ba0f(long lParm1,long lParm2,long **pplParm3,char cParm4);
undefined8 FUN_0040bba4(long lParm1,long **pplParm2,char cParm3);
undefined8 FUN_0040bd74(long *plParm1,undefined8 uParm2);
undefined8 FUN_0040bf75(long lParm1,long lParm2,long *plParm3);
undefined8 FUN_0040c28f(undefined8 uParm1,undefined8 uParm2);
long FUN_0040c2d8(long lParm1,undefined8 uParm2);
void FUN_0040c587(long lParm1,undefined4 uParm2);
ulong FUN_0040c5e0(long lParm1);
ulong FUN_0040c5f2(long lParm1,undefined4 uParm2);
ulong FUN_0040c67a(long lParm1);
undefined * FUN_0040c6fc(void);
char * FUN_0040cac1(void);
void FUN_0040cb8f(undefined auParm1 [16],undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint uParm9);
void FUN_0040cbbb(undefined auParm1 [16],undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint uParm9);
void FUN_0040cbe7(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,undefined8 param_2,uint param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0040ccf8(int *piParm1);
void FUN_0040cd6d(uint *puParm1);
void FUN_0040cda4(uint *puParm1);
ulong FUN_0040cdda(uint uParm1);
ulong FUN_0040cdea(uint uParm1);
void FUN_0040ce37(undefined4 *puParm1);
void FUN_0040ce4c(uint *puParm1);
void FUN_0040ce67(uint *puParm1);
undefined8 FUN_0040ceb9(uint *puParm1,undefined8 uParm2);
long FUN_0040cf13(long lParm1);
ulong FUN_0040cf41(char *pcParm1);
ulong FUN_0040d22c(long lParm1,uint uParm2,uint uParm3);
ulong FUN_0040d3d5(undefined8 uParm1);
ulong FUN_0040d48a(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0040d743(uint uParm1);
void FUN_0040d7a6(undefined8 uParm1);
void FUN_0040d7cb(void);
ulong FUN_0040d7da(long lParm1);
undefined8 FUN_0040d8aa(undefined8 uParm1);
void FUN_0040d8c9(undefined8 uParm1,undefined8 uParm2,uint uParm3);
long FUN_0040d8f4(long lParm1,undefined *puParm2);
ulong * FUN_0040de92(ulong *puParm1,char cParm2,ulong uParm3);
ulong FUN_0040dfa5(void);
long FUN_0040dff6(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
int * FUN_0040e23f(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_0040ed21(ulong uParm1,long lParm2,long lParm3);
long FUN_0040ef94(int *param_1,long *param_2);
long FUN_0040f16b(undefined auParm1 [16],int *piParm2,long *plParm3);
undefined8 FUN_0040f4e6(int iParm1,long lParm2,uint *puParm3,long lParm4,uint uParm5);
void FUN_0040fce9(uint param_1);
void FUN_0040fd33(undefined auParm1 [16],uint uParm2);
ulong FUN_0040fd81(void);
ulong FUN_004100c3(undefined auParm1 [16],undefined8 uParm2,undefined8 uParm3);
ulong FUN_0041044d(char *pcParm1,long lParm2);
undefined8 FUN_004104a6(long param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
ulong FUN_00417c02(ulong uParm1,undefined4 uParm2);
ulong FUN_00417c39(uint uParm1);
void FUN_00417c58(undefined auParm1 [16],int *piParm2,undefined8 uParm3);
void FUN_00417ce1(int *param_1);
void FUN_00417d76(uint uParm1);
ulong FUN_00417d9c(ulong uParm1,long lParm2);
void FUN_00417dd0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00417e0b(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00417e5c(ulong uParm1,ulong uParm2);
undefined8 FUN_00417e77(uint *puParm1,ulong *puParm2);
undefined8 FUN_00418613(char *pcParm1,ulong *puParm2,char **ppcParm3);
ulong FUN_004198e3(void);
ulong FUN_0041991a(void);
void FUN_00419950(void);
undefined8 _DT_FINI(void);

