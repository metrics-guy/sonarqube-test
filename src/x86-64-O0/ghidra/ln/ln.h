typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_004021c0(void);
void FUN_00402380(void);
void FUN_00402660(void);
void entry(void);
void FUN_004026d0(void);
void FUN_00402740(void);
void FUN_004027d0(void);
void FUN_00402805(void);
void FUN_0040282a(void);
void FUN_0040286d(undefined *puParm1);
ulong FUN_00402a0a(int iParm1);
ulong FUN_00402a3a(undefined8 uParm1);
long FUN_00402bb3(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00402c94(char *pcParm1,undefined8 uParm2);
ulong FUN_004034aa(uint uParm1);
long FUN_00403c41(long lParm1,long lParm2);
void FUN_00403cd3(undefined8 uParm1,uint *puParm2);
ulong FUN_00403d17(uint uParm1,undefined8 uParm2,uint uParm3,undefined8 uParm4,uint uParm5,char cParm6);
void FUN_00403eb7(undefined8 uParm1,undefined8 *puParm2);
ulong FUN_00403eed(undefined8 uParm1,uint uParm2,undefined8 uParm3,char cParm4);
ulong FUN_0040405d(char *pcParm1,char *pcParm2);
undefined8 FUN_00404141(undefined8 uParm1,long *plParm2,ulong *puParm3);
ulong FUN_004041ec(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_004043aa(char *pcParm1);
void FUN_00404404(long lParm1,long lParm2,undefined uParm3);
ulong FUN_00404517(long *plParm1,undefined8 uParm2,long lParm3,long lParm4,long *plParm5);
long FUN_004048e7(long lParm1,int iParm2,char cParm3);
long FUN_00404ba9(undefined8 uParm1,uint uParm2);
ulong FUN_00404be4(undefined8 uParm1,char *pcParm2);
void FUN_00404c40(undefined8 uParm1,char *pcParm2);
void FUN_00404c90(undefined8 uParm1);
ulong FUN_00404caf(long *plParm1,undefined8 uParm2,undefined8 uParm3);
char * FUN_00404d56(char *pcParm1,uint uParm2);
void FUN_00405532(void);
void FUN_00405637(void);
long FUN_0040571d(undefined8 uParm1);
long FUN_004057e6(undefined8 uParm1);
ulong FUN_00405814(char *pcParm1);
undefined * FUN_00405895(undefined8 uParm1);
char * FUN_0040592c(char *pcParm1);
ulong FUN_00405995(long lParm1);
ulong FUN_004059e3(char *pcParm1);
void FUN_00405a45(long lParm1,undefined8 uParm2,undefined8 *puParm3);
ulong FUN_00405ae1(long lParm1,undefined8 uParm2,undefined8 *puParm3);
long FUN_00405b3d(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
char * FUN_00405b7e(char *pcParm1);
long FUN_00405b9e(long lParm1,char *pcParm2,undefined8 *puParm3);
long FUN_00405ce5(long *plParm1,undefined8 uParm2);
long FUN_00405d3c(long lParm1,long lParm2);
ulong FUN_00405dcf(ulong uParm1);
ulong FUN_00405e3a(ulong uParm1);
ulong FUN_00405e81(undefined8 uParm1,ulong uParm2);
ulong FUN_00405eb8(ulong uParm1,ulong uParm2);
undefined8 FUN_00405ed1(long lParm1);
ulong FUN_00405fd4(ulong uParm1,long lParm2);
long * FUN_004060e1(undefined8 uParm1,undefined1 *puParm2,code *pcParm3,code *pcParm4,long lParm5);
void FUN_00406246(long **pplParm1,undefined8 uParm2);
long FUN_00406371(long lParm1);
void FUN_004063bc(long lParm1,undefined8 *puParm2);
long FUN_004063f2(long lParm1,long lParm2,long **pplParm3,char cParm4);
undefined8 FUN_00406587(long lParm1,long **pplParm2,char cParm3);
undefined8 FUN_00406757(long *plParm1,undefined8 uParm2);
undefined8 FUN_00406958(long lParm1,long lParm2,long *plParm3);
undefined8 FUN_00406c72(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00406cbb(undefined8 *puParm1,ulong uParm2);
ulong FUN_00406d07(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00406d7f(undefined8 *puParm1,undefined8 *puParm2);
void FUN_00406df7(undefined8 *puParm1);
void FUN_00406e29(long lParm1);
ulong FUN_00406f0b(undefined1 *puParm1,byte bParm2,uint uParm3);
ulong * FUN_00406f93(ulong *puParm1,uint uParm2);
char * FUN_00407032(char *pcParm1,int iParm2);
ulong FUN_004070d2(long param_1,ulong param_2,long param_3,ulong param_4,uint param_5,uint param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_00407ff4(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_00408264(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_004082a5(uint uParm1,undefined8 uParm2);
void FUN_004082c9(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_0040835d(undefined8 uParm1,char cParm2);
void FUN_00408387(undefined8 uParm1);
void FUN_004083a6(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_00408441(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040846d(uint uParm1,undefined8 uParm2);
void FUN_00408496(undefined8 uParm1);
undefined8 FUN_004084b5(undefined4 uParm1);
ulong FUN_004084d1(uint uParm1,long lParm2,uint uParm3,long lParm4,uint uParm5);
ulong FUN_00408932(undefined8 uParm1,undefined8 uParm2);
void FUN_00408ad7(uint uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00408b03(undefined8 uParm1,ulong uParm2);
ulong FUN_00408b2d(long lParm1,int iParm2,undefined8 uParm3,code *pcParm4,ulong uParm5);
void FUN_00408ce7(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_00409156(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
void FUN_00409229(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_004092e0(undefined8 uParm1);
long FUN_004092fa(long lParm1);
long FUN_0040932f(long lParm1,long lParm2);
void FUN_00409390(undefined8 uParm1,undefined8 uParm2);
void FUN_004093c4(undefined8 uParm1);
long FUN_004093f1(void);
long FUN_0040941b(void);
ulong FUN_00409454(void);
undefined8 FUN_0040949f(ulong uParm1,ulong uParm2);
ulong FUN_0040951e(uint uParm1);
void FUN_00409547(void);
void FUN_0040957c(uint uParm1);
void FUN_004095f1(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_00409676(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_0040975b(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_00409a14(uint uParm1);
void FUN_00409a77(undefined8 uParm1);
void FUN_00409a9c(void);
ulong FUN_00409aab(long lParm1);
undefined8 FUN_00409b7b(undefined8 uParm1);
void FUN_00409b9a(undefined8 uParm1,undefined8 uParm2,uint uParm3);
void FUN_00409bc5(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
void FUN_00409bf2(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
ulong FUN_00409c2f(uint uParm1,long lParm2,long lParm3,uint uParm4);
long FUN_00409d42(long lParm1,undefined *puParm2);
void FUN_0040a2e0(long lParm1,int *piParm2);
ulong FUN_0040a4c3(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_0040aaac(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_0040ab7a(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
ulong FUN_0040b343(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,uint param_6,uint param_7);
void FUN_0040b3d3(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
ulong FUN_0040b41d(uint uParm1,char *pcParm2,uint uParm3,undefined8 uParm4);
ulong FUN_0040b5ea(uint uParm1,long lParm2,uint uParm3,long lParm4,uint uParm5);
void FUN_0040b7c6(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040b7eb(long lParm1,long lParm2);
undefined8 FUN_0040b8a2(long lParm1);
ulong FUN_0040b8d3(uint *puParm1,byte *pbParm2,long lParm3,undefined8 uParm4);
void FUN_0040b956(uint uParm1,undefined8 uParm2,uint uParm3,uint uParm4);
ulong FUN_0040b986(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,undefined8 param_2,uint param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0040bb4d(long lParm1,long lParm2);
ulong FUN_0040bbb6(long lParm1,long lParm2);
void FUN_0040c0c9(uint uParm1,undefined8 uParm2,uint uParm3,undefined8 uParm4);
ulong FUN_0040c0fc(long lParm1);
void FUN_0040c1b9(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040c1de(long lParm1,long lParm2);
undefined8 FUN_0040c26e(undefined8 uParm1,uint uParm2,long lParm3);
ulong FUN_0040c316(uint uParm1,long lParm2,uint uParm3);
ulong FUN_0040c4d7(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_0040c5f6(undefined8 uParm1,ulong uParm2);
void FUN_0040c744(uint uParm1,undefined8 uParm2);
void FUN_0040c779(void);
long FUN_0040c78a(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
void FUN_0040c8b9(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_0040c93b(long lParm1,long lParm2,long lParm3);
long FUN_0040ca7c(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
ulong FUN_0040cb02(ulong uParm1,undefined4 uParm2);
ulong FUN_0040cb39(byte *pbParm1,byte *pbParm2);
undefined *FUN_0040cbb1(uint uParm1,undefined8 uParm2,undefined *puParm3,ulong uParm4,code **ppcParm5,code *pcParm6);
undefined8 FUN_0040ce53(uint uParm1,char cParm2);
undefined8 FUN_0040cece(undefined8 uParm1);
undefined8 FUN_0040cf59(void);
ulong FUN_0040cf66(uint uParm1);
ulong FUN_0040d037(char *pcParm1,ulong uParm2);
undefined * FUN_0040d091(void);
char * FUN_0040d456(void);
undefined8 * FUN_0040d524(undefined8 uParm1);
undefined8 FUN_0040d56b(undefined8 uParm1,undefined8 uParm2);
long FUN_0040d5ae(long lParm1);
ulong FUN_0040d5c0(undefined8 *puParm1,ulong uParm2);
void FUN_0040d76e(undefined8 uParm1);
ulong FUN_0040d79a(undefined8 *puParm1);
ulong * FUN_0040d7dd(ulong uParm1,ulong uParm2);
undefined8 * FUN_0040d83f(undefined8 uParm1,undefined8 uParm2);
void FUN_0040d886(long lParm1,ulong uParm2,ulong uParm3);
long FUN_0040daec(long lParm1,ulong uParm2);
void FUN_0040dbdb(undefined8 *puParm1,long lParm2,long lParm3);
void FUN_0040dc75(ulong *puParm1,ulong uParm2,ulong uParm3);
void FUN_0040ddba(long *plParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_0040de11(long *plParm1);
undefined8 FUN_0040de61(undefined8 uParm1);
undefined8 FUN_0040de7b(long lParm1,undefined8 uParm2);
void FUN_0040debf(ulong *puParm1,long *plParm2);
void FUN_0040e50e(long lParm1);
ulong FUN_0040ea96(uint uParm1);
void FUN_0040eaa6(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040eace(undefined8 uParm1);
ulong FUN_0040eb83(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_0040ec30(long lParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_0040ecd6(uint uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_0040ed84(void);
long FUN_0040edd5(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
int * FUN_0040f01e(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_0040fb00(ulong uParm1,long lParm2,long lParm3);
long FUN_0040fd73(int *param_1,long *param_2);
long FUN_0040ff4a(undefined auParm1 [16],int *piParm2,long *plParm3);
undefined8 FUN_004102c5(int iParm1,long lParm2,uint *puParm3,long lParm4,uint uParm5);
void FUN_00410ac8(uint param_1);
void FUN_00410b12(undefined auParm1 [16],uint uParm2);
ulong FUN_00410b60(void);
ulong FUN_00410ea2(undefined auParm1 [16],undefined8 uParm2,undefined8 uParm3);
ulong FUN_0041122c(char *pcParm1,long lParm2);
undefined8 FUN_00411285(long param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
ulong FUN_004189e1(uint uParm1);
long FUN_00418a00(undefined8 uParm1,undefined8 uParm2);
void FUN_00418ae6(undefined auParm1 [16],int *piParm2,undefined8 uParm3);
void FUN_00418b6f(int *param_1);
void FUN_00418c04(uint uParm1);
ulong FUN_00418c2a(ulong uParm1,long lParm2);
void FUN_00418c5e(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00418c99(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00418cea(ulong uParm1,ulong uParm2);
undefined8 FUN_00418d05(uint *puParm1,ulong *puParm2);
undefined8 FUN_004194a1(char *pcParm1,ulong *puParm2,char **ppcParm3);
ulong FUN_0041a771(void);
ulong FUN_0041a7a8(void);
void FUN_0041a7e0(void);
undefined8 _DT_FINI(void);

