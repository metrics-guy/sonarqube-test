typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void entry(void);
void FUN_00402600(void);
void FUN_00402670(void);
void FUN_00402700(void);
undefined8 FUN_00402735(int iParm1);
ulong FUN_0040275b(byte bParm1);
long FUN_0040276a(long lParm1,ulong uParm2);
void FUN_004027b0(void);
void FUN_004027d5(void);
void FUN_004027fa(void);
void FUN_0040281f(undefined *puParm1);
undefined8 FUN_004029bc(undefined8 uParm1);
ulong FUN_004029ca(long lParm1);
ulong FUN_00402a09(void);
ulong FUN_00402a6b(int iParm1);
void FUN_00402a95(ulong uParm1,int iParm2);
ulong FUN_00402bf1(uint uParm1,undefined8 uParm2,undefined8 uParm3,ulong uParm4);
ulong FUN_00402d17(uint uParm1,long lParm2,long lParm3,ulong uParm4);
void FUN_00402ed1(void);
ulong FUN_004032d6(undefined8 uParm1);
void FUN_00403753(long lParm1,uint uParm2,uint uParm3,undefined8 uParm4);
undefined8 FUN_00403a25(char cParm1,long lParm2,long lParm3);
void FUN_00403b69(ulong uParm1,long lParm2,ulong uParm3,ulong uParm4,ulong uParm5);
void FUN_00403da2(ulong uParm1,undefined *puParm2,undefined8 uParm3);
void FUN_00403f0e(long lParm1,long lParm2,ulong uParm3);
void FUN_004042ab(ulong uParm1,ulong uParm2,long lParm3,undefined8 uParm4,ulong uParm5,ulong uParm6);
void FUN_0040475e(ulong uParm1,ulong uParm2,long lParm3,undefined8 uParm4,ulong uParm5,ulong uParm6);
ulong FUN_004049c4(long lParm1,long lParm2,long lParm3);
void FUN_00404ce3(ulong uParm1,ulong uParm2,long lParm3,undefined8 uParm4);
void FUN_00405320(undefined8 *puParm1,undefined8 *puParm2,undefined *puParm3);
undefined8 FUN_004053b2(uint uParm1,undefined8 *puParm2);
void FUN_004061be(void);
char * FUN_004062a4(char *pcParm1);
void FUN_0040630d(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_0040641c(uint uParm1,undefined8 uParm2,uint uParm3,uint uParm4);
long FUN_00406495(uint uParm1,long lParm2,long lParm3);
char * FUN_00406516(ulong uParm1,long lParm2);
void FUN_0040659b(long lParm1);
ulong FUN_0040667d(undefined1 *puParm1,byte bParm2,uint uParm3);
ulong * FUN_00406705(ulong *puParm1,uint uParm2);
char * FUN_004067a4(char *pcParm1,int iParm2);
ulong FUN_00406844(long param_1,ulong param_2,long param_3,ulong param_4,uint param_5,uint param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_00407766(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_004079d6(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_00407a17(uint uParm1,undefined8 uParm2);
void FUN_00407a3b(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_00407acf(undefined8 uParm1,char cParm2);
void FUN_00407af9(undefined8 uParm1);
void FUN_00407b18(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_00407bb3(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00407bdf(uint uParm1,undefined8 uParm2);
void FUN_00407c08(undefined8 uParm1);
long FUN_00407c27(uint uParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_00407c97(uint uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00407d07(uint uParm1);
void FUN_00407d54(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_004081c3(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
void FUN_00408296(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_0040834d(ulong uParm1,ulong uParm2);
void FUN_0040839d(long lParm1,ulong *puParm2,ulong uParm3);
void FUN_00408477(undefined8 uParm1);
long FUN_00408491(long lParm1);
long FUN_004084c6(long lParm1,long lParm2);
long FUN_00408527(ulong uParm1,ulong uParm2);
void FUN_00408588(undefined8 uParm1,undefined8 uParm2);
void FUN_004085bc(undefined8 uParm1);
void FUN_004085e9(void);
void FUN_00408613(void);
void FUN_0040861a(uint uParm1,uint uParm2);
ulong FUN_00408643(undefined8 param_1,uint param_2,ulong param_3,ulong param_4,undefined8 param_5,undefined8 param_6,uint param_7);
void FUN_00408755(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,uint uParm6);
undefined8 FUN_004087aa(int iParm1);
ulong FUN_004087d0(ulong *puParm1,int iParm2);
ulong FUN_0040882f(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_00408870(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_00408c59(ulong uParm1,ulong uParm2);
ulong FUN_00408cd8(uint uParm1);
void FUN_00408d01(void);
void FUN_00408d36(uint uParm1);
void FUN_00408dab(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_00408e30(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_00408f15(undefined8 uParm1);
void FUN_00408fca(undefined8 uParm1);
void FUN_00408fef(void);
ulong FUN_00408ffe(long lParm1);
undefined8 FUN_004090ce(undefined8 uParm1);
void FUN_004090ed(undefined8 uParm1,undefined8 uParm2,uint uParm3);
void FUN_00409118(long lParm1,int *piParm2);
ulong FUN_004092fb(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_004098e4(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_004099b2(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
ulong FUN_0040a17b(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,uint param_6,uint param_7);
void FUN_0040a20b(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
undefined8 FUN_0040a255(long lParm1);
ulong FUN_0040a286(uint *puParm1,byte *pbParm2,long lParm3,undefined8 uParm4);
ulong * FUN_0040a309(ulong *puParm1,char cParm2,ulong uParm3);
undefined8 FUN_0040a41c(long lParm1,long lParm2);
ulong FUN_0040a485(char *pcParm1,char *pcParm2,uint uParm3);
undefined8 FUN_0040a615(int iParm1,undefined4 *puParm2);
ulong FUN_0040a72c(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_0040a84b(void);
undefined8 FUN_0040a85c(void);
ulong FUN_0040a86a(uint uParm1,uint uParm2);
ulong FUN_0040a8a1(byte *pbParm1,byte *pbParm2);
undefined8 FUN_0040a919(undefined8 uParm1);
undefined8 FUN_0040a9a4(void);
ulong FUN_0040a9b1(uint uParm1);
undefined * FUN_0040aa82(void);
char * FUN_0040ae47(void);
undefined8 * FUN_0040af15(ulong uParm1);
void FUN_0040afd8(ulong uParm1);
void FUN_0040b0b1(uint uParm1);
ulong FUN_0040b0d7(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_0040b390(void);
long FUN_0040b3e1(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
int * FUN_0040b62a(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_0040c10c(ulong uParm1,long lParm2,long lParm3);
long FUN_0040c37f(int *param_1,long *param_2);
long FUN_0040c556(undefined auParm1 [16],int *piParm2,long *plParm3);
undefined8 FUN_0040c8d1(int iParm1,long lParm2,uint *puParm3,long lParm4,uint uParm5);
void FUN_0040d0d4(uint param_1);
void FUN_0040d11e(undefined auParm1 [16],uint uParm2);
ulong FUN_0040d16c(void);
ulong FUN_0040d4ae(undefined auParm1 [16],undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040d838(char *pcParm1,long lParm2);
undefined8 FUN_0040d891(long param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
char * FUN_0040db54(char *pcParm1,char **ppcParm2,long lParm3,undefined8 uParm4);
ulong FUN_00414fed(uint uParm1);
void FUN_0041500c(undefined auParm1 [16],int *piParm2,undefined8 uParm3);
void FUN_00415095(int *param_1);
ulong FUN_0041512a(ulong uParm1,long lParm2);
void FUN_0041515e(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00415199(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_004151ea(ulong uParm1,ulong uParm2);
undefined8 FUN_00415205(uint *puParm1,ulong *puParm2);
undefined8 FUN_004159a1(char *pcParm1,ulong *puParm2,char **ppcParm3);
ulong FUN_00416c71(void);
ulong FUN_00416ca8(void);
void FUN_00416ce0(void);
undefined8 _DT_FINI(void);

