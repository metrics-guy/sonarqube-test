typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined3;
typedef unsigned int    undefined4;
typedef unsigned long    undefined5;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00402560(void);
void entry(void);
void FUN_004025d0(void);
void FUN_00402640(void);
void FUN_004026d0(void);
undefined8 FUN_00402705(undefined8 uParm1);
void FUN_00402713(undefined *puParm1);
void FUN_004028b0(int iParm1,undefined8 *puParm2);
void FUN_0040299a(uint uParm1);
void FUN_00402b0d(undefined *puParm1);
ulong FUN_00402b78(uint uParm1,undefined8 *puParm2);
ulong FUN_00402fc7(char *pcParm1);
long FUN_00403026(undefined8 uParm1);
ulong FUN_00403066(uint uParm1,undefined8 uParm2);
undefined8 FUN_0040310e(uint uParm1,undefined8 uParm2,long lParm3,uint uParm4);
long FUN_0040319b(long lParm1);
undefined8 FUN_004031b5(uint uParm1,undefined8 uParm2,long lParm3);
undefined8 FUN_00403251(long lParm1,long lParm2,char cParm3,char *pcParm4,int iParm5,undefined4 *puParm6);
undefined8 FUN_00403602(int iParm1);
ulong FUN_0040363b(char *pcParm1,uint uParm2);
void FUN_00403674(undefined8 uParm1,undefined8 uParm2);
void FUN_004036af(long lParm1);
undefined8 FUN_004036fd(long lParm1,long lParm2,long lParm3,char cParm4);
ulong FUN_004038f6(long lParm1,long lParm2,long lParm3);
ulong FUN_00403e5c(long *plParm1,long lParm2);
void FUN_00403fa7(void);
long FUN_00403fb8(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
void FUN_004040e7(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_00404169(long lParm1,long lParm2,long lParm3);
long FUN_004042aa(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
void FUN_00404330(void);
void FUN_00404435(void);
char * FUN_0040451b(char *pcParm1);
undefined * FUN_00404584(long lParm1);
undefined8 FUN_00404666(void);
void FUN_00404671(long lParm1);
ulong FUN_00404753(undefined1 *puParm1,byte bParm2,uint uParm3);
ulong * FUN_004047db(ulong *puParm1,uint uParm2);
char * FUN_0040487a(char *pcParm1,int iParm2);
ulong FUN_0040491a(long param_1,ulong param_2,long param_3,ulong param_4,uint param_5,uint param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_0040583c(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_00405aac(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_00405aed(uint uParm1,undefined8 uParm2);
void FUN_00405b11(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_00405ba5(undefined8 uParm1,char cParm2);
void FUN_00405bcf(undefined8 uParm1);
void FUN_00405bee(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_00405c89(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00405cb5(uint uParm1,undefined8 uParm2);
void FUN_00405cde(undefined8 uParm1);
undefined8 * FUN_00405cfd(undefined8 *puParm1);
void FUN_00405d5a(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00405d86(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_004061f5(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
void FUN_004062c8(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_0040637f(void);
void FUN_004063c0(undefined8 uParm1);
long FUN_004063da(long lParm1);
long FUN_0040640f(long lParm1,long lParm2);
long FUN_00406470(void);
long FUN_0040649a(undefined8 uParm1,uint uParm2,undefined8 uParm3);
ulong FUN_00406502(void);
undefined8 FUN_0040654d(ulong uParm1,ulong uParm2);
ulong FUN_004065cc(uint uParm1);
void FUN_004065f5(void);
void FUN_0040662a(uint uParm1);
void FUN_0040669f(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_00406724(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00406809(uint uParm1,undefined8 uParm2,uint uParm3,uint uParm4);
ulong FUN_00406834(uint uParm1,long lParm2,uint uParm3,uint uParm4);
undefined8 FUN_004068f7(uint uParm1);
void FUN_0040695a(undefined8 uParm1);
void FUN_0040697f(void);
ulong FUN_0040698e(long lParm1);
undefined8 FUN_00406a5e(undefined8 uParm1);
void FUN_00406a7d(undefined8 uParm1,undefined8 uParm2,uint uParm3);
void FUN_00406aa8(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
void FUN_00406ad5(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
ulong FUN_00406b12(uint uParm1,long lParm2,long lParm3,uint uParm4);
ulong FUN_00406c25(long *plParm1,long *plParm2);
ulong FUN_00406c7a(long lParm1,ulong uParm2);
undefined8 FUN_00406ca4(long lParm1);
undefined8 FUN_00406d38(long lParm1,undefined8 *puParm2);
void FUN_00406e46(long lParm1,long lParm2);
void FUN_00406f54(long lParm1);
void FUN_00406fa2(undefined8 uParm1);
void FUN_00406fe6(long lParm1,char cParm2);
long FUN_0040702a(uint uParm1,undefined8 uParm2,uint uParm3,uint *puParm4);
void FUN_004070b8(long lParm1,uint uParm2,char cParm3);
ulong FUN_00407144(long lParm1);
ulong FUN_004071ee(long lParm1,undefined8 uParm2);
long * FUN_00407275(long *plParm1,uint uParm2,long lParm3);
void FUN_0040763d(long lParm1,long lParm2);
undefined8 FUN_004076f8(long *plParm1);
ulong FUN_00407881(ulong *puParm1,ulong uParm2);
ulong FUN_004078b2(ulong *puParm1,ulong *puParm2);
undefined8 FUN_004078e4(long lParm1);
undefined8 FUN_00407a5b(undefined8 uParm1);
undefined8 FUN_00407a91(undefined8 uParm1);
long FUN_00407b0d(long *plParm1);
undefined8 FUN_00408200(undefined8 uParm1,long lParm2,int iParm3);
ulong FUN_00408257(long *plParm1,long *plParm2);
void FUN_004082b2(long lParm1,undefined4 uParm2);
long FUN_00408323(long *plParm1,int iParm2);
undefined8 FUN_00408c62(long lParm1,long lParm2,char cParm3);
long FUN_00408e43(long lParm1,long lParm2,ulong uParm3);
long FUN_00408f8a(long lParm1,undefined8 uParm2,long lParm3);
void FUN_0040903c(long lParm1);
undefined8 FUN_00409079(long lParm1,long lParm2);
void FUN_00409146(long lParm1,long lParm2);
long FUN_00409255(long *plParm1);
ulong FUN_004092ab(long lParm1,long lParm2,uint uParm3,long lParm4);
void FUN_004094dd(long lParm1,int *piParm2);
ulong FUN_004096c0(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_00409ca9(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_00409d77(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
ulong FUN_0040a540(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,uint param_6,uint param_7);
void FUN_0040a5d0(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
void FUN_0040a61a(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040a63f(long lParm1,long lParm2);
undefined8 FUN_0040a6f6(long lParm1);
ulong FUN_0040a727(uint *puParm1,byte *pbParm2,long lParm3,undefined8 uParm4);
void FUN_0040a7aa(uint uParm1,undefined8 uParm2,uint uParm3,uint uParm4);
ulong FUN_0040a7da(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,undefined8 param_2,uint param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0040a9a1(long lParm1,long lParm2);
void FUN_0040aa0a(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040aa2f(long lParm1,long lParm2);
ulong FUN_0040aabf(uint uParm1,long lParm2,uint uParm3);
ulong FUN_0040ac80(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040ad9f(byte *pbParm1,byte *pbParm2);
undefined8 FUN_0040ae17(uint uParm1,char cParm2);
undefined8 FUN_0040ae92(undefined8 uParm1);
ulong FUN_0040af1d(ulong uParm1);
void FUN_0040af39(long lParm1);
undefined8 FUN_0040af5b(long *plParm1,long *plParm2);
void FUN_0040b02f(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0040b13e(void);
ulong FUN_0040b14b(uint uParm1);
long FUN_0040b21c(long *plParm1,undefined8 uParm2);
long FUN_0040b273(long lParm1,long lParm2);
ulong FUN_0040b306(ulong uParm1);
ulong FUN_0040b371(ulong uParm1);
ulong FUN_0040b3b8(undefined8 uParm1,ulong uParm2);
ulong FUN_0040b3ef(ulong uParm1,ulong uParm2);
undefined8 FUN_0040b408(long lParm1);
ulong FUN_0040b50b(ulong uParm1,long lParm2);
long * FUN_0040b618(undefined8 uParm1,undefined1 *puParm2,code *pcParm3,code *pcParm4,long lParm5);
void FUN_0040b77d(long **pplParm1,undefined8 uParm2);
long FUN_0040b8a8(long lParm1);
void FUN_0040b8f3(long lParm1,undefined8 *puParm2);
long FUN_0040b929(long lParm1,long lParm2,long **pplParm3,char cParm4);
undefined8 FUN_0040babe(long lParm1,long **pplParm2,char cParm3);
undefined8 FUN_0040bc8e(long *plParm1,undefined8 uParm2);
undefined8 FUN_0040be8f(long lParm1,long lParm2,long *plParm3);
undefined8 FUN_0040c1a9(undefined8 uParm1,undefined8 uParm2);
long FUN_0040c1f2(long lParm1,undefined8 uParm2);
void FUN_0040c4a1(long lParm1,undefined4 uParm2);
ulong FUN_0040c4fa(long lParm1);
ulong FUN_0040c50c(long lParm1,undefined4 uParm2);
ulong FUN_0040c594(long lParm1);
undefined * FUN_0040c616(void);
char * FUN_0040c9db(void);
void FUN_0040caa9(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,undefined8 param_2,uint param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_0040cbba(uint uParm1);
ulong FUN_0040cbca(uint uParm1);
ulong FUN_0040cc17(undefined8 uParm1);
ulong FUN_0040cccc(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_0040cf85(void);
long FUN_0040cfd6(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
int * FUN_0040d21f(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_0040dd01(ulong uParm1,long lParm2,long lParm3);
long FUN_0040df74(int *param_1,long *param_2);
long FUN_0040e14b(undefined auParm1 [16],int *piParm2,long *plParm3);
undefined8 FUN_0040e4c6(int iParm1,long lParm2,uint *puParm3,long lParm4,uint uParm5);
void FUN_0040ecc9(uint param_1);
void FUN_0040ed13(undefined auParm1 [16],uint uParm2);
ulong FUN_0040ed61(void);
ulong FUN_0040f0a3(undefined auParm1 [16],undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040f42d(char *pcParm1,long lParm2);
undefined8 FUN_0040f486(long param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
char * FUN_0040f749(char *pcParm1,char **ppcParm2,long lParm3,undefined8 uParm4);
ulong FUN_00416be2(ulong uParm1,undefined4 uParm2);
ulong FUN_00416c19(uint uParm1);
void FUN_00416c38(undefined auParm1 [16],int *piParm2,undefined8 uParm3);
void FUN_00416cc1(int *param_1);
void FUN_00416d56(uint uParm1);
ulong FUN_00416d7c(ulong uParm1,long lParm2);
void FUN_00416db0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00416deb(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00416e3c(ulong uParm1,ulong uParm2);
undefined8 FUN_00416e57(uint *puParm1,ulong *puParm2);
undefined8 FUN_004175f3(char *pcParm1,ulong *puParm2,char **ppcParm3);
ulong FUN_004188c3(void);
ulong FUN_004188fa(void);
void FUN_00418930(void);
undefined8 _DT_FINI(void);

