typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined5;
typedef unsigned long    undefined6;
typedef unsigned long    undefined7;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_004032d0(void);
void FUN_00403520(void);
void FUN_004035e0(void);
void FUN_004037e0(void);
void entry(void);
void FUN_00403860(void);
void FUN_004038d0(void);
void FUN_00403960(void);
void FUN_00403995(int iParm1);
ulong FUN_004039ae(byte bParm1);
ulong FUN_004039bd(char *pcParm1);
void FUN_00403a1c(void);
void FUN_00403a41(void);
void FUN_00403a66(undefined *puParm1);
void FUN_00403c03(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00403c28(undefined8 uParm1);
undefined FUN_00403c36(int iParm1);;
undefined8 FUN_00403c46(void);
void FUN_00403c51(undefined8 uParm1,undefined8 uParm2);
void FUN_00403cf9(void);
void FUN_00403d82(undefined8 uParm1,long lParm2);
char * FUN_00403ee2(char *pcParm1);
void FUN_00403f34(void);
undefined8 FUN_00403fa1(long lParm1);
void FUN_00404087(void);
ulong FUN_00404242(ulong *puParm1,ulong uParm2);
ulong FUN_0040426b(long *plParm1,long *plParm2);
void FUN_004042c0(undefined8 uParm1);
ulong FUN_004042db(undefined8 uParm1,undefined8 uParm2);
void FUN_0040435c(undefined8 *puParm1);
ulong FUN_00404396(uint uParm1);
void FUN_00404425(void);
void FUN_00404440(void);
void FUN_0040447e(int iParm1);
void FUN_0040449b(void);
void FUN_004044be(void);
void FUN_00404585(char cParm1);
void FUN_00404787(void);
void FUN_00404798(void);
ulong FUN_004047a9(uint uParm1,undefined8 *puParm2);
undefined8 FUN_00404e0c(undefined8 uParm1);
ulong FUN_00404e71(uint uParm1,undefined8 uParm2);
ulong FUN_00405e13(byte **ppbParm1,byte **ppbParm2,char cParm3,long *plParm4);
undefined8 FUN_00406196(void);
void FUN_0040623d(void);
void FUN_004065f3(void);
void FUN_00406679(char cParm1);
void FUN_004066ab(byte bParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00406707(long lParm1,long lParm2,undefined uParm3);
void FUN_0040679b(long lParm1,long lParm2,byte bParm3);
void FUN_00406d52(undefined8 uParm1);
undefined8 FUN_00406d94(undefined8 *puParm1,undefined8 uParm2);
ulong FUN_00406dee(char *pcParm1);
undefined8 FUN_00406e92(undefined8 uParm1);
undefined8 FUN_00406ea0(void);
void FUN_00406ebe(undefined8 *puParm1);
void FUN_00406f40(void);
ulong FUN_00407009(uint uParm1);
ulong FUN_0040703f(undefined8 uParm1,long lParm2,char cParm3);
ulong FUN_004070ec(undefined8 uParm1,long lParm2);
ulong FUN_00407173(undefined8 uParm1,long lParm2);
ulong FUN_004071ea(char *pcParm1);
undefined8 FUN_00407252(long *plParm1,int iParm2,long lParm3,byte bParm4,char *pcParm5);
ulong FUN_00407e50(long lParm1);
void FUN_00407e87(undefined8 uParm1,long lParm2,byte bParm3);
undefined8 FUN_00407ee2(long lParm1,char *pcParm2);
void FUN_00407fb1(undefined8 uParm1);
void FUN_00407fdb(long lParm1,byte bParm2);
ulong FUN_004081a8(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00408257(undefined8 *puParm1,undefined8 *puParm2,code *pcParm3);
ulong FUN_004082e6(undefined8 *puParm1,undefined8 *puParm2,code *pcParm3);
ulong FUN_00408375(undefined8 *puParm1,undefined8 *puParm2,code *pcParm3);
ulong FUN_00408404(undefined8 *puParm1,undefined8 *puParm2,code *pcParm3);
void FUN_00408477(undefined8 *puParm1,undefined8 *puParm2,code *pcParm3);
ulong FUN_004084a7(undefined8 *puParm1,undefined8 *puParm2,code *pcParm3);
void FUN_00408545(undefined8 uParm1,undefined8 uParm2);
void FUN_0040856f(undefined8 uParm1,undefined8 uParm2);
void FUN_00408599(undefined8 uParm1,undefined8 uParm2);
void FUN_004085c3(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_004085ed(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00408665(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_004086dd(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00408755(undefined8 uParm1,undefined8 uParm2);
void FUN_004087cd(undefined8 uParm1,undefined8 uParm2);
void FUN_004087f7(undefined8 uParm1,undefined8 uParm2);
void FUN_00408821(undefined8 uParm1,undefined8 uParm2);
void FUN_0040884b(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00408875(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_004088ed(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00408965(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_004089dd(undefined8 uParm1,undefined8 uParm2);
void FUN_00408a55(undefined8 uParm1,undefined8 uParm2);
void FUN_00408a7f(undefined8 uParm1,undefined8 uParm2);
void FUN_00408aa9(undefined8 uParm1,undefined8 uParm2);
void FUN_00408ad3(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00408afd(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00408b75(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00408bed(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00408c65(undefined8 uParm1,undefined8 uParm2);
void FUN_00408cdd(undefined8 uParm1,undefined8 uParm2);
void FUN_00408d07(undefined8 uParm1,undefined8 uParm2);
void FUN_00408d31(undefined8 uParm1,undefined8 uParm2);
void FUN_00408d5b(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00408d85(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00408dfd(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00408e75(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00408eed(undefined8 uParm1,undefined8 uParm2);
void FUN_00408f65(undefined8 uParm1,undefined8 uParm2);
void FUN_00408f8f(undefined8 uParm1,undefined8 uParm2);
void FUN_00408fb9(undefined8 uParm1,undefined8 uParm2);
void FUN_00408fe3(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_0040900d(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00409085(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_004090fd(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00409175(undefined8 uParm1,undefined8 uParm2);
void FUN_004091ed(undefined8 uParm1,undefined8 uParm2);
void FUN_00409217(undefined8 uParm1,undefined8 uParm2);
void FUN_00409241(undefined8 uParm1,undefined8 uParm2);
void FUN_0040926b(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00409295(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_0040930d(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00409385(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_004093fd(undefined8 uParm1,undefined8 uParm2);
void FUN_00409475(undefined8 *puParm1,undefined8 *puParm2);
void FUN_004094a0(undefined8 uParm1,undefined8 uParm2);
void FUN_004094c5(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_004094ea(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_0040955d(undefined8 uParm1,undefined8 uParm2);
void FUN_004095d0(void);
void FUN_00409634(void);
void FUN_00409770(void);
void FUN_0040988e(undefined8 uParm1,undefined8 uParm2,byte bParm3,long lParm4,undefined8 uParm5,uint uParm6);
ulong FUN_00409921(void);
void FUN_004099df(long lParm1,undefined8 uParm2,uint uParm3);
void FUN_00409aa9(uint uParm1,uint uParm2,char cParm3);
void FUN_00409b04(uint uParm1,uint uParm2,char cParm3);
ulong FUN_00409b5f(long lParm1,undefined8 uParm2);
void FUN_00409bc1(uint uParm1);
void FUN_00409bfe(uint uParm1);
undefined * FUN_00409c3b(undefined8 uParm1,ulong uParm2,long lParm3);
void FUN_00409cab(long lParm1);
char * FUN_0040a654(char **param_1,char *param_2,char *param_3,undefined8 param_4,int param_5,char **param_6,undefined *param_7);
long FUN_0040ab30(undefined *puParm1,undefined8 uParm2,uint uParm3);
char * FUN_0040abf8(char *pcParm1,char cParm2);
long FUN_0040acf2(char *param_1,undefined8 param_2,uint param_3,long param_4,char param_5,long param_6,long param_7);
long FUN_0040b088(undefined8 *puParm1,byte bParm2,undefined8 uParm3,ulong uParm4);
void FUN_0040b1cc(void);
long FUN_0040b209(long lParm1,undefined8 uParm2);
ulong FUN_0040b3af(char cParm1,uint uParm2,int iParm3);
ulong FUN_0040b4e4(byte bParm1,uint uParm2,uint uParm3);
ulong FUN_0040b539(long lParm1);
ulong * FUN_0040b589(long *plParm1,char cParm2);
void FUN_0040b95d(undefined8 *puParm1);
long FUN_0040b9bf(undefined8 *puParm1);
void FUN_0040bb6d(void);
void FUN_0040bcc4(void);
void FUN_0040be1e(char cParm1);
void FUN_0040bf26(ulong uParm1,ulong uParm2);
void FUN_0040bfcd(char *pcParm1,char *pcParm2,char *pcParm3);
void FUN_0040c085(void);
ulong FUN_0040c2b0(char cParm1);
long FUN_0040c54d(uint uParm1);
long FUN_0040c929(undefined8 uParm1,ulong uParm2);
void FUN_0040ca77(void);
long FUN_0040ca88(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
void FUN_0040cbb7(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_0040cc39(long lParm1,long lParm2,long lParm3);
long FUN_0040cd7a(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
undefined8 FUN_0040ce00(int iParm1);
undefined8 FUN_0040ce3a(int iParm1);
ulong FUN_0040ce68(int iParm1);
ulong FUN_0040ce88(uint uParm1);
ulong FUN_0040cea7(long *plParm1,undefined8 uParm2,undefined8 uParm3);
char * FUN_0040cf4e(char *pcParm1,uint uParm2);
void FUN_0040d72a(void);
ulong FUN_0040d810(char *pcParm1);
char * FUN_0040d891(char *pcParm1);
ulong FUN_0040d8fa(long lParm1);
undefined8 FUN_0040d948(void);
void FUN_0040d95b(long lParm1,undefined8 uParm2,undefined8 *puParm3);
ulong FUN_0040d9f7(long lParm1,undefined8 uParm2,undefined8 *puParm3);
undefined8 FUN_0040da53(uint uParm1);
void FUN_0040dafe(uint uParm1,undefined *puParm2);
void FUN_0040dcc6(long lParm1,undefined8 uParm2);
long FUN_0040dcee(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
char * FUN_0040dd2f(char *pcParm1);
long FUN_0040dd4f(long lParm1,char *pcParm2,undefined8 *puParm3);
char * FUN_0040de96(char **ppcParm1);
ulong FUN_0040df71(byte bParm1);
ulong FUN_0040dfc1(long lParm1,ulong uParm2,long lParm3,ulong uParm4);
ulong FUN_0040e1df(char *pcParm1,char *pcParm2);
void FUN_0040e3f3(undefined8 *puParm1);
ulong FUN_0040e448(uint uParm1);
undefined8 FUN_0040e519(long lParm1);
long FUN_0040e52b(long *plParm1,undefined8 uParm2);
long FUN_0040e582(long lParm1,long lParm2);
ulong FUN_0040e615(ulong uParm1);
ulong FUN_0040e680(ulong uParm1);
ulong FUN_0040e6c7(undefined8 uParm1,ulong uParm2);
ulong FUN_0040e6fe(ulong uParm1,ulong uParm2);
undefined8 FUN_0040e717(long lParm1);
ulong FUN_0040e81a(ulong uParm1,long lParm2);
long * FUN_0040e927(undefined8 uParm1,undefined1 *puParm2,code *pcParm3,code *pcParm4,long lParm5);
void FUN_0040ea8c(long **pplParm1,undefined8 uParm2);
long FUN_0040ebb7(long lParm1);
void FUN_0040ec02(long lParm1,undefined8 *puParm2);
long FUN_0040ec38(long lParm1,long lParm2,long **pplParm3,char cParm4);
undefined8 FUN_0040edcd(long lParm1,long **pplParm2,char cParm3);
undefined8 FUN_0040ef9d(long *plParm1,undefined8 uParm2);
undefined8 FUN_0040f19e(long lParm1,long lParm2,long *plParm3);
undefined8 FUN_0040f4b8(undefined8 uParm1,undefined8 uParm2);
long FUN_0040f501(long lParm1,undefined8 uParm2);
ulong FUN_0040f7b0(undefined8 *puParm1,ulong uParm2);
ulong FUN_0040f7fc(undefined8 *puParm1,undefined8 *puParm2);
void FUN_0040f874(undefined8 *puParm1);
void FUN_0040f8a6(void);
long FUN_0040f98b(long lParm1,ulong uParm2,byte *pbParm3,undefined8 uParm4);
char * FUN_0040fa87(ulong uParm1,long lParm2,uint uParm3,ulong uParm4,ulong uParm5);
undefined8 FUN_004103eb(void);
ulong FUN_0041040c(char *pcParm1,undefined8 *puParm2,uint *puParm3);
ulong FUN_00410573(undefined8 uParm1,undefined8 uParm2,long *plParm3);
uint * FUN_004105c5(uint uParm1);
uint * FUN_004106b6(uint uParm1);
char * FUN_004107a7(long lParm1,long lParm2);
char * FUN_004108d9(ulong uParm1,long lParm2);
ulong FUN_0041095e(uint *puParm1);
long FUN_004109ac(uint *puParm1,ulong uParm2);
undefined * FUN_00410a32(undefined *puParm1,undefined *puParm2,long lParm3);
long FUN_00410a7b(long lParm1,long lParm2,long lParm3,ulong *puParm4,int iParm5,uint uParm6);
void FUN_00410e43(undefined8 uParm1,uint uParm2);
ulong FUN_00410e75(byte *pbParm1,long lParm2,uint uParm3);
void FUN_004110aa(undefined8 *puParm1,ulong uParm2,undefined8 *puParm3,code *pcParm4);
void FUN_0041124c(undefined8 *puParm1,ulong uParm2,undefined8 *puParm3,code *pcParm4);
void FUN_00411492(long lParm1,long lParm2,undefined8 uParm3);
long FUN_004114d7(long lParm1,long lParm2,long lParm3);
long FUN_0041152d(long lParm1,long lParm2,long lParm3);
ulong FUN_00411583(int iParm1,int iParm2);
void FUN_004115d7(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,uint uParm6);
char * FUN_0041162d(char *pcParm1,long lParm2,char *pcParm3,undefined8 *puParm4,undefined uParm5,undefined8 uParm6,undefined8 uParm7,uint uParm8);
void FUN_004137f5(long lParm1);
undefined8 FUN_004138d7(undefined1 *puParm1);
ulong FUN_00413920(undefined1 *puParm1);
void FUN_0041393e(undefined1 *puParm1,undefined4 uParm2);
ulong FUN_00413963(undefined1 *puParm1,byte bParm2,uint uParm3);
ulong * FUN_004139eb(ulong *puParm1,uint uParm2);
char * FUN_00413a8a(char *pcParm1,int iParm2);
ulong FUN_00413b2a(long param_1,ulong param_2,long param_3,ulong param_4,uint param_5,uint param_6,long param_7,char *param_8,char *param_9);
undefined8 FUN_00414a4c(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined1 *puParm5);
undefined1 * FUN_00414aee(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_00414d5e(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_00414d9f(uint uParm1,undefined8 uParm2);
void FUN_00414dc3(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_00414e57(undefined8 uParm1,char cParm2);
void FUN_00414e81(undefined8 uParm1);
void FUN_00414ea0(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_00414f3b(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00414f67(uint uParm1,undefined8 uParm2);
void FUN_00414f90(undefined8 uParm1);
void FUN_00414faf(void);
undefined8 FUN_00414fba(void);
undefined8 FUN_00414fdc(void);
void FUN_00414ffe(long lParm1);
void FUN_00415014(long lParm1);
void FUN_0041502a(long lParm1);
ulong FUN_00415040(uint uParm1);
ulong FUN_00415050(long lParm1,int iParm2,long lParm3,int iParm4);
void FUN_004150d7(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_00415546(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
void FUN_00415619(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_004156d0(ulong uParm1,ulong uParm2);
void FUN_00415720(undefined8 uParm1,ulong uParm2,ulong uParm3);
void FUN_0041577e(long lParm1,ulong *puParm2,ulong uParm3);
void FUN_00415858(undefined8 uParm1);
long FUN_00415872(long lParm1);
long FUN_004158a7(long lParm1,long lParm2);
void FUN_00415908(undefined8 uParm1,undefined8 uParm2);
void FUN_00415932(undefined8 uParm1,undefined8 uParm2);
void FUN_00415966(undefined8 uParm1);
undefined * FUN_00415993(void);
ulong FUN_004159bd(undefined8 param_1,uint param_2,ulong param_3,ulong param_4,undefined8 param_5,undefined8 param_6,uint param_7);
long FUN_00415acf(void);
long FUN_00415b08(void);
undefined8 FUN_00415bf7(int iParm1);
ulong FUN_00415c1d(ulong *puParm1,int iParm2);
ulong FUN_00415c7c(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_00415cbd(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
void FUN_004160a6(uint uParm1,int iParm2,undefined uParm3,long lParm4,undefined8 uParm5,uint uParm6);
undefined8 FUN_00416180(uint uParm1,uint uParm2,char cParm3,undefined8 uParm4,undefined8 uParm5);
undefined8 FUN_004161c6(int iParm1);
ulong FUN_004161ec(ulong *puParm1,int iParm2);
ulong FUN_0041624b(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_0041628c(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_00416675(ulong uParm1,ulong uParm2);
ulong FUN_004166f4(uint uParm1);
void FUN_0041671d(void);
void FUN_00416752(uint uParm1);
void FUN_004167c7(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_0041684c(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_00416931(byte *pbParm1,byte *pbParm2,byte *pbParm3,byte bParm4,uint uParm5);
char * FUN_0041753c(char *pcParm1);
undefined8 FUN_00417682(int iParm1,long lParm2,ulong uParm3,ulong uParm4,byte bParm5,uint uParm6);
undefined8 FUN_00417ed8(int *piParm1);
ulong FUN_00417f74(uint *puParm1,uint *puParm2,uint *puParm3,byte bParm4,uint uParm5);
int * FUN_00418b16(int *piParm1);
undefined8 FUN_00418c5a(int iParm1,long lParm2,ulong uParm3,ulong uParm4,byte bParm5,uint uParm6);
ulong FUN_00419502(undefined8 uParm1,long lParm2,uint uParm3);
long FUN_004197f5(long lParm1,undefined *puParm2);
void FUN_00419d93(long lParm1,int *piParm2);
ulong FUN_00419f76(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_0041a55f(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_0041a62d(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
ulong FUN_0041adf6(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,uint param_6,uint param_7);
void FUN_0041ae86(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
ulong FUN_0041aed0(undefined8 uParm1,undefined8 uParm2);
void FUN_0041af7d(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0041afa2(long lParm1,long lParm2);
undefined8 FUN_0041b059(long lParm1);
ulong FUN_0041b08a(uint *puParm1,byte *pbParm2,long lParm3,undefined8 uParm4);
ulong * FUN_0041b10d(ulong *puParm1,char cParm2,ulong uParm3);
void FUN_0041b220(long lParm1,undefined8 uParm2);
void FUN_0041b270(long lParm1,undefined8 uParm2);
undefined8 FUN_0041b2c1(long *plParm1,long lParm2,long lParm3);
void FUN_0041b3e3(long lParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
void FUN_0041b43e(ulong *puParm1,long lParm2);
void FUN_0041b631(void);
void FUN_0041b660(uint uParm1,undefined8 uParm2,uint uParm3,uint uParm4);
ulong FUN_0041b690(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,undefined8 param_2,uint param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0041b857(long lParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_0041b8fd(long lParm1,long lParm2);
ulong FUN_0041b966(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,long param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_0041baf8(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0041bb1d(long lParm1,long lParm2);
char * FUN_0041bbad(char *pcParm1,char *pcParm2,ulong uParm3);
ulong FUN_0041bd2f(int iParm1,int iParm2);
ulong FUN_0041bd6a(uint *puParm1,uint *puParm2);
void FUN_0041be12(long lParm1,undefined8 uParm2,long lParm3);
undefined8 * FUN_0041be4e(long lParm1);
undefined8 FUN_0041befe(long **pplParm1,char *pcParm2);
void FUN_0041c0cb(undefined8 *puParm1);
void FUN_0041c10d(void);
void FUN_0041c11d(long lParm1);
ulong FUN_0041c154(long lParm1);
long FUN_0041c19a(long lParm1);
ulong FUN_0041c263(long lParm1);
undefined8 FUN_0041c2cb(long lParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_0041c377(long lParm1,undefined8 uParm2);
void FUN_0041c44c(long lParm1);
void FUN_0041c47b(void);
ulong FUN_0041c50d(char *pcParm1);
ulong FUN_0041c57f(void);
long FUN_0041c5d0(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
int * FUN_0041c819(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_0041d2fb(ulong uParm1,long lParm2,long lParm3);
long FUN_0041d56e(int *param_1,long *param_2);
long FUN_0041d745(undefined auParm1 [16],int *piParm2,long *plParm3);
undefined8 FUN_0041dac0(int iParm1,long lParm2,uint *puParm3,long lParm4,uint uParm5);
void FUN_0041e2c3(uint param_1);
void FUN_0041e30d(undefined auParm1 [16],uint uParm2);
ulong FUN_0041e35b(void);
ulong FUN_0041e69d(undefined auParm1 [16],undefined8 uParm2,undefined8 uParm3);
ulong FUN_0041ea27(char *pcParm1,long lParm2);
undefined8 FUN_0041ea80(long param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
ulong FUN_004261dc(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_004262fb(uint *puParm1,long lParm2);
void FUN_00426372(uint uParm1);
ulong FUN_004263ba(ulong uParm1,undefined4 uParm2);
ulong FUN_004263f1(byte *pbParm1,byte *pbParm2);
undefined8 FUN_00426469(uint uParm1,char cParm2);
undefined8 FUN_004264e4(undefined8 uParm1);
undefined8 FUN_0042656f(void);
ulong FUN_0042657c(char *pcParm1,ulong uParm2);
undefined * FUN_004265d6(void);
char * FUN_0042699b(void);
void FUN_00426a69(undefined auParm1 [16],int *piParm2,undefined8 uParm3);
void FUN_00426af2(int *param_1);
undefined8 FUN_00426b87(undefined8 uParm1);
undefined8 FUN_00426c4d(uint uParm1,undefined8 uParm2);
ulong FUN_00426e68(ulong uParm1,long lParm2);
void FUN_00426e9c(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00426ed7(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00426f28(ulong uParm1,ulong uParm2);
ulong FUN_00426f43(undefined8 uParm1);
ulong FUN_00426ff8(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_004272b1(uint uParm1);
void FUN_00427314(undefined8 uParm1);
void FUN_00427339(void);
ulong FUN_00427348(long lParm1);
undefined8 FUN_00427418(undefined8 uParm1);
void FUN_00427437(undefined8 uParm1,undefined8 uParm2,uint uParm3);
void FUN_00427462(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
void FUN_0042748f(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
ulong FUN_004274cc(uint uParm1,long lParm2,long lParm3,uint uParm4);
long FUN_004275df(long lParm1,undefined4 uParm2);
ulong FUN_00427658(ulong uParm1);
ulong FUN_00427703(int iParm1,int iParm2);
long FUN_0042773e(ulong param_1,long param_2,int param_3,int param_4,int param_5,uint param_6,int param_7,int param_8,int param_9,int param_10);
long FUN_004279ab(undefined8 uParm1,undefined8 uParm2);
long FUN_004279fa(undefined8 param_1,undefined8 param_2,uint param_3,uint param_4,uint param_5,long param_6,uint *param_7);
void FUN_00427b23(code *pcParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_00427b55(undefined8 uParm1,long *plParm2,undefined8 uParm3);
long FUN_00427c96(undefined8 *puParm1,undefined8 uParm2,long *plParm3);
void FUN_00428270(undefined8 uParm1);
undefined8 FUN_00428299(uint *puParm1,ulong *puParm2);
undefined8 FUN_00428a35(char *pcParm1,ulong *puParm2,char **ppcParm3);
ulong FUN_00429d05(char *pcParm1,char *pcParm2,uint uParm3);
ulong FUN_00429e95(void);
ulong FUN_00429ecc(void);
undefined8 * FUN_00429ef7(ulong uParm1);
void FUN_00429fba(ulong uParm1);
void FUN_0042a0a0(void);
undefined8 _DT_FINI(void);

