typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined3;
typedef unsigned int    undefined4;
typedef unsigned long    undefined5;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00403870(void);
void entry(void);
void FUN_004038e0(void);
void FUN_00403950(void);
void FUN_004039e0(void);
void FUN_00403a15(int iParm1);
ulong FUN_00403a2e(byte bParm1);
ulong FUN_00403a3d(byte bParm1);
undefined8 FUN_00403a70(undefined8 uParm1);
void FUN_00403a7e(void);
void FUN_00403aa3(void);
void FUN_00403ac8(undefined *puParm1);
void FUN_00403c65(uint uParm1,undefined8 uParm2);
void FUN_00403d11(undefined8 uParm1,undefined *puParm2);
void FUN_00403d70(uint uParm1);
void FUN_00404014(long lParm1);
void FUN_0040404d(char *pcParm1);
ulong FUN_00404081(long lParm1,ulong uParm2);
ulong FUN_004040ac(long lParm1,long lParm2);
ulong FUN_004040dd(uint uParm1);
void FUN_004041cb(long lParm1);
ulong FUN_00404245(undefined4 uParm1);
void FUN_0040428d(uint uParm1);
void FUN_004042b3(void);
void FUN_004042d5(void);
void FUN_004042eb(void);
void FUN_00404309(void);
void FUN_0040434e(void);
undefined8 * FUN_00404390(int *piParm1,char cParm2);
undefined8 FUN_00404552(long lParm1,char *pcParm2);
long FUN_00404677(undefined8 uParm1,undefined8 uParm2);
void FUN_004046bc(undefined8 uParm1,undefined8 uParm2);
void FUN_0040474d(uint uParm1,uint uParm2);
ulong FUN_0040477f(uint *puParm1,long lParm2);
long FUN_00404907(long *plParm1,byte bParm2);
void FUN_00404a48(undefined8 uParm1);
long FUN_00404a67(long lParm1);
void FUN_00404c13(undefined8 uParm1);
void FUN_00404c7d(long lParm1);
void FUN_00404d8d(undefined8 *puParm1,undefined8 *puParm2);
void FUN_00404dc8(void);
void FUN_00404fbb(uint uParm1,char cParm2,undefined8 uParm3);
void FUN_0040519a(uint uParm1,char cParm2,undefined8 uParm3);
long FUN_004053a0(uint uParm1,char cParm2,undefined8 uParm3);
ulong FUN_00405434(void);
long FUN_00405667(long lParm1,ulong uParm2,long lParm3,ulong uParm4,long lParm5);
void FUN_0040587d(long *plParm1,long lParm2,ulong uParm3);
long FUN_0040592e(long *plParm1);
char * FUN_00405952(char **ppcParm1,long *plParm2);
char * FUN_00405ae0(char **ppcParm1,long lParm2);
undefined8 FUN_00405c90(char **ppcParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_004060f0(byte **ppbParm1);
ulong FUN_004061cc(char *pcParm1);
ulong FUN_00406241(char *pcParm1,char *pcParm2);
void FUN_004062f1(char *pcParm1,char *pcParm2);
void FUN_0040636f(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_004063ed(long lParm1,long lParm2);
ulong FUN_00406501(char *pcParm1,char **ppcParm2);
void FUN_00406668(undefined8 uParm1);
undefined8 FUN_004066fd(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_004067b0(undefined *puParm1,ulong uParm2,undefined *puParm3,ulong uParm4);
long FUN_00406ed2(char *pcParm1,char *pcParm2);
void FUN_00406f36(long lParm1,long lParm2);
ulong FUN_00406f99(long lParm1);
void FUN_00406fd6(byte **ppbParm1,long *plParm2);
void FUN_00407243(undefined8 uParm1);
ulong FUN_004072a6(long lParm1);
void FUN_0040733b(long lParm1,undefined *puParm2);
void FUN_0040748f(undefined8 *puParm1,char cParm2);
ulong FUN_00407b5b(char **ppcParm1,char **ppcParm2);
ulong FUN_0040857e(undefined8 *puParm1,undefined8 *puParm2);
void FUN_004086c4(char **ppcParm1,undefined8 uParm2,long lParm3);
ulong FUN_004087da(undefined8 uParm1,char cParm2);
long FUN_00408b08(long lParm1,ulong uParm2,long *plParm3);
void FUN_00408c1e(long lParm1,ulong uParm2,ulong uParm3,undefined8 uParm4,undefined8 uParm5,long lParm6);
ulong FUN_00409853(long lParm1,undefined8 uParm2,ulong uParm3,undefined8 uParm4,undefined8 uParm5);
void FUN_004098e9(undefined8 *puParm1,ulong uParm2,undefined8 *puParm3);
void FUN_00409a08(long lParm1,ulong uParm2,long lParm3,byte bParm4);
undefined8 * FUN_00409c84(long lParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 *FUN_00409d7d(long lParm1,undefined8 *puParm2,long lParm3,ulong uParm4,long lParm5,char cParm6);
ulong FUN_00409f74(long lParm1,long lParm2);
void FUN_00409fec(long lParm1);
void FUN_0040a00b(long lParm1);
void FUN_0040a02a(undefined8 *puParm1,long lParm2);
void FUN_0040a082(undefined8 *puParm1,long lParm2);
long FUN_0040a0e3(undefined8 *puParm1);
void FUN_0040a160(long *plParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040a1eb(long *plParm1,ulong uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_0040a6a2(undefined8 uParm1,long *plParm2);
void FUN_0040a75c(undefined8 uParm1,long lParm2);
void FUN_0040a7e3(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
undefined8 FUN_0040a889(undefined8 *puParm1);
void FUN_0040a8ec(long param_1,ulong param_2,long param_3,long *param_4,undefined8 param_5,undefined8 param_6,undefined8 param_7);
void FUN_0040ab98(long lParm1,ulong uParm2,ulong uParm3,long lParm4);
void FUN_0040adbc(long lParm1,ulong uParm2);
void FUN_0040ae5f(long lParm1);
void FUN_0040aec0(long *plParm1,ulong uParm2,ulong uParm3,undefined8 uParm4);
void FUN_0040b391(undefined8 *puParm1,long lParm2,long lParm3,ulong uParm4);
void FUN_0040b775(undefined8 uParm1);
void FUN_0040b7d5(undefined8 uParm1,undefined8 uParm2);
void FUN_0040b817(undefined8 uParm1);
void FUN_0040b843(void);
undefined8 FUN_0040b91d(undefined8 uParm1,long *plParm2,long lParm3);
void FUN_0040b9d3(uint uParm1);
char * FUN_0040b9ff(char *pcParm1,long lParm2,int iParm3);
long FUN_0040bb07(long lParm1);
ulong FUN_0040bb3b(uint uParm1,undefined8 *puParm2);
void FUN_0040d0f9(void);
long FUN_0040d10a(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
void FUN_0040d239(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_0040d2bb(long lParm1,long lParm2,long lParm3);
long FUN_0040d3fc(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
void FUN_0040d482(void);
void FUN_0040d568(undefined4 *puParm1);
void FUN_0040d5c2(undefined4 *puParm1,undefined4 uParm2);
long FUN_0040d5d9(uint *puParm1,long lParm2);
void FUN_0040d656(long lParm1,undefined8 uParm2);
void FUN_0040d771(ulong uParm1,ulong uParm2,long lParm3);
void FUN_0040d9a1(int *piParm1,ulong uParm2,int *piParm3);
void FUN_0040e695(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
void FUN_0040e6c6(long lParm1,uint uParm2);
char * FUN_0040e701(char **ppcParm1);
ulong FUN_0040e7dc(byte bParm1);
ulong FUN_0040e82c(long lParm1,ulong uParm2,long lParm3,ulong uParm4);
ulong FUN_0040ea4a(char *pcParm1,char *pcParm2);
ulong FUN_0040ec5e(uint uParm1);
long FUN_0040ed2f(long *plParm1,undefined8 uParm2);
ulong FUN_0040ed86(ulong uParm1);
ulong FUN_0040edf1(ulong uParm1);
ulong FUN_0040ee38(undefined8 uParm1,ulong uParm2);
ulong FUN_0040ee6f(ulong uParm1,ulong uParm2);
undefined8 FUN_0040ee88(long lParm1);
ulong FUN_0040ef8b(ulong uParm1,long lParm2);
long * FUN_0040f098(undefined8 uParm1,undefined1 *puParm2,code *pcParm3,code *pcParm4,long lParm5);
long FUN_0040f1fd(long lParm1);
void FUN_0040f248(long lParm1,undefined8 *puParm2);
long FUN_0040f27e(long lParm1,long lParm2,long **pplParm3,char cParm4);
undefined8 FUN_0040f413(long lParm1,long **pplParm2,char cParm3);
undefined8 FUN_0040f5e3(long *plParm1,undefined8 uParm2);
undefined8 FUN_0040f7e4(long lParm1,long lParm2,long *plParm3);
undefined8 FUN_0040fafe(undefined8 uParm1,undefined8 uParm2);
long FUN_0040fb47(long lParm1,undefined8 uParm2);
undefined8 * FUN_0040fdf6(code *pcParm1,long lParm2);
undefined8 FUN_0040fe84(void);
undefined8 FUN_0040fe97(long *plParm1,undefined8 uParm2);
undefined8 FUN_0040ff40(long *plParm1);
ulong FUN_0040ffd0(long lParm1,ulong uParm2,ulong uParm3,code *pcParm4);
void FUN_004100fb(long lParm1,ulong uParm2,code *pcParm3);
char * FUN_004101be(int iParm1,long lParm2);
char * FUN_004102bc(uint uParm1,long lParm2);
char * FUN_00410334(ulong uParm1,long lParm2);
ulong FUN_004103b9(byte *pbParm1,long lParm2,uint uParm3);
long FUN_004105ee(void);
long FUN_00410644(int iParm1);
undefined8 FUN_004106a8(char *pcParm1);
ulong FUN_00410789(uint uParm1);
void FUN_00410817(void);
void FUN_004108ec(void);
ulong FUN_004109fd(uint *puParm1,uint uParm2);
long FUN_00410c64(void);
void FUN_00410cf2(long lParm1);
ulong FUN_00410dd4(undefined1 *puParm1,byte bParm2,uint uParm3);
ulong * FUN_00410e5c(ulong *puParm1,uint uParm2);
char * FUN_00410efb(char *pcParm1,int iParm2);
ulong FUN_00410f9b(long param_1,ulong param_2,long param_3,ulong param_4,uint param_5,uint param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_00411ebd(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_0041212d(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_0041216e(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_004121ad(uint uParm1,undefined8 uParm2);
void FUN_004121d1(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_00412265(undefined8 uParm1,char cParm2);
void FUN_0041228f(undefined8 uParm1);
void FUN_004122ae(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_00412349(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00412375(uint uParm1,undefined8 uParm2);
void FUN_0041239e(undefined8 uParm1);
ulong * FUN_004123bd(ulong uParm1,ulong uParm2);
undefined8 * FUN_0041241f(undefined8 uParm1,undefined8 uParm2);
void FUN_00412466(long lParm1,ulong uParm2,ulong uParm3);
long FUN_004126cc(long lParm1,ulong uParm2);
void FUN_004127bb(undefined8 *puParm1,long lParm2,long lParm3);
void FUN_00412855(ulong *puParm1,ulong uParm2,ulong uParm3);
void FUN_0041299a(long *plParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_004129f1(long *plParm1);
undefined8 FUN_00412a41(undefined8 uParm1);
undefined8 FUN_00412a5b(long lParm1,undefined8 uParm2);
void FUN_00412a9f(ulong *puParm1,long *plParm2);
void FUN_004130ee(long lParm1);
void FUN_00413676(undefined8 *puParm1);
void FUN_00413719(long *plParm1);
ulong FUN_00413916(undefined8 uParm1,long lParm2);
void FUN_00413c86(undefined8 uParm1,uint uParm2);
ulong FUN_00413cb6(char *pcParm1,char *pcParm2,char cParm3);
ulong FUN_00413e1b(byte *pbParm1,byte *pbParm2,uint uParm3,uint uParm4);
void FUN_00414285(undefined8 uParm1,undefined8 uParm2,uint uParm3,uint uParm4);
void FUN_004142b3(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_00414722(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
void FUN_004147f5(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_004148ac(ulong uParm1,ulong uParm2);
void FUN_004148fc(long lParm1,ulong *puParm2,ulong uParm3);
void FUN_004149d6(undefined8 uParm1);
long FUN_004149f0(long lParm1);
long FUN_00414a25(long lParm1,long lParm2);
long FUN_00414a86(ulong uParm1,ulong uParm2);
void FUN_00414ae7(undefined8 uParm1,undefined8 uParm2);
void FUN_00414b1b(void);
void FUN_00414b45(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
ulong FUN_00414bfd(undefined8 uParm1,long lParm2,undefined8 uParm3,long lParm4);
undefined8 FUN_00414c70(undefined auParm1 [16]);
undefined8 FUN_00414cd9(int iParm1);
ulong FUN_00414cff(ulong *puParm1,int iParm2);
ulong FUN_00414d5e(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_00414d9f(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
void FUN_00415188(uint uParm1,int iParm2,undefined uParm3,long lParm4,undefined8 uParm5,uint uParm6);
undefined8 FUN_00415262(uint uParm1,uint uParm2,char cParm3,undefined8 uParm4,undefined8 uParm5);
undefined8 FUN_004152a8(int iParm1);
ulong FUN_004152ce(ulong *puParm1,int iParm2);
ulong FUN_0041532d(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_0041536e(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_00415757(ulong uParm1,ulong uParm2);
ulong FUN_004157d6(uint uParm1);
void FUN_004157ff(void);
void FUN_00415834(uint uParm1);
void FUN_004158a9(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_0041592e(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00415a13(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00415a3b(undefined8 uParm1);
ulong FUN_00415af0(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00415da9(undefined8 uParm1);
void FUN_00415dce(void);
ulong FUN_00415ddd(long lParm1);
undefined8 FUN_00415ead(undefined8 uParm1);
void FUN_00415ecc(undefined8 uParm1,undefined8 uParm2,uint uParm3);
void FUN_00415ef7(long lParm1,int *piParm2);
ulong FUN_004160da(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_004166c3(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_00416791(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
ulong FUN_00416f5a(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,uint param_6,uint param_7);
void FUN_00416fea(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
ulong FUN_00417034(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_004170e1(long lParm1);
ulong FUN_00417112(uint *puParm1,byte *pbParm2,long lParm3,undefined8 uParm4);
ulong * FUN_00417195(ulong *puParm1,char cParm2,ulong uParm3);
ulong FUN_004172a8(long *plParm1,long *plParm2);
void FUN_00417387(long lParm1,undefined8 uParm2);
void FUN_004173d7(long lParm1,undefined8 uParm2);
undefined8 FUN_00417428(long *plParm1,long lParm2,long lParm3);
void FUN_0041754a(long lParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
void FUN_004175a5(ulong *puParm1,long lParm2);
undefined8 FUN_00417798(void);
undefined8 FUN_004177c7(long lParm1,long lParm2);
void FUN_00417830(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00417855(long lParm1,long lParm2);
ulong FUN_004178e5(long lParm1);
ulong FUN_00417a51(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00417b70(uint uParm1);
ulong FUN_00417bb8(ulong uParm1,undefined4 uParm2);
undefined8 FUN_00417bef(int iParm1);
undefined8 FUN_00417c29(int iParm1);
ulong FUN_00417c57(int iParm1);
undefined8 FUN_00417c77(int iParm1);
ulong FUN_00417ca0(uint uParm1);
ulong FUN_00417cbf(byte *pbParm1,byte *pbParm2);
undefined8 FUN_00417d37(undefined8 uParm1);
void FUN_00417dc2(double dParm1);
ulong FUN_00417f10(uint uParm1,uint uParm2);
void FUN_00417f65(uint uParm1,uint uParm2);
long FUN_00417fa3(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00418089(void);
undefined * FUN_00418096(void);
char * FUN_0041845b(void);
ulong FUN_00418529(long lParm1,long lParm2,long lParm3,long lParm4);
undefined8 FUN_004185fa(undefined8 uParm1,long lParm2,undefined8 uParm3,long lParm4);
ulong FUN_00418663(uint uParm1);
void FUN_00418673(undefined8 uParm1,undefined8 uParm2);
void FUN_00418699(uint uParm1);
undefined8 FUN_004186bf(undefined8 uParm1);
undefined8 FUN_00418785(uint uParm1,undefined8 uParm2);
void FUN_004189a0(undefined8 uParm1,undefined8 uParm2);
ulong FUN_004189c5(long lParm1,long lParm2);
ulong FUN_00418a7c(void);
long FUN_00418acd(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
int * FUN_00418d16(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_004197f8(ulong uParm1,long lParm2,long lParm3);
long FUN_00419a6b(int *param_1,long *param_2);
long FUN_00419c42(undefined auParm1 [16],int *piParm2,long *plParm3);
undefined8 FUN_00419fbd(int iParm1,long lParm2,uint *puParm3,long lParm4,uint uParm5);
void FUN_0041a7c0(uint param_1);
void FUN_0041a80a(undefined auParm1 [16],uint uParm2);
ulong FUN_0041a858(void);
ulong FUN_0041ab9a(undefined auParm1 [16],undefined8 uParm2,undefined8 uParm3);
ulong FUN_0041af24(char *pcParm1,long lParm2);
undefined8 FUN_0041af7d(long param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
char * FUN_0041b240(char *pcParm1,char **ppcParm2,long lParm3,undefined8 uParm4);
void FUN_004226d9(undefined auParm1 [16],int *piParm2,undefined8 uParm3);
void FUN_00422762(int *param_1);
ulong FUN_004227f7(ulong uParm1,long lParm2);
void FUN_0042282b(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00422866(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_004228b7(ulong uParm1,ulong uParm2);
undefined8 FUN_004228d2(uint *puParm1,ulong *puParm2);
undefined8 FUN_0042306e(char *pcParm1,ulong *puParm2,char **ppcParm3);
ulong FUN_0042433e(void);
ulong FUN_00424375(void);
void FUN_004243a0(void);
undefined8 _DT_FINI(void);

