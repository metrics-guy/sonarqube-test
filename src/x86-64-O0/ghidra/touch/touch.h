typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_004021b0(void);
void FUN_004023d0(void);
void entry(void);
void FUN_004025b0(void);
void FUN_00402620(void);
void FUN_004026b0(void);
void FUN_004026e5(void);
void FUN_0040270a(undefined *puParm1);
void FUN_004028a7(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_00402904(undefined8 uParm1);
ulong FUN_00402b98(uint uParm1);
void FUN_004033a9(void);
long FUN_004033ba(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
void FUN_004034e9(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_0040356b(long lParm1,long lParm2,long lParm3);
long FUN_004036ac(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
void FUN_00403732(void);
ulong FUN_00403818(uint uParm1,undefined8 uParm2,uint uParm3,uint uParm4);
ulong FUN_00403891(uint uParm1,uint uParm2,long lParm3,undefined8 uParm4,uint uParm5);
void FUN_00403920(undefined8 *puParm1);
undefined8 FUN_00403975(void);
ulong FUN_00403984(byte bParm1);
void FUN_00403993(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00403a5a(long param_1);
ulong FUN_00403cee(long param_1,int param_2);
void FUN_00403ff3(long lParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,int iParm5);
undefined * FUN_00404040(long lParm1,undefined *puParm2,int iParm3);
long FUN_00404174(int iParm1,long lParm2);
void FUN_00404379(undefined8 uParm1,long lParm2);
ulong FUN_004047f1(byte bParm1,long lParm2,undefined8 uParm3);
void FUN_00404849(undefined8 uParm1,long lParm2);
void FUN_00404a07(void);
ulong FUN_00404a2c(long lParm1);
undefined8 FUN_00406a6a(long param_1,long param_2);
ulong FUN_00406c0b(ulong uParm1,int iParm2);
undefined8 FUN_00406c92(int iParm1,undefined8 uParm2);
undefined8 FUN_00406d28(char param_1,int *param_2);
long * FUN_00406e21(long lParm1,undefined8 uParm2);
char ** FUN_00406ef2(undefined8 uParm1,char *pcParm2);
ulong FUN_0040721f(long *plParm1,byte **ppbParm2);
undefined8 FUN_004076c4(void);
ulong FUN_004076d7(undefined8 uParm1,uint *puParm2,uint *puParm3,long lParm4);
long FUN_00407793(undefined8 uParm1,long lParm2,long lParm3,int iParm4);
undefined8 FUN_0040787f(long lParm1,undefined8 uParm2,int iParm3);
undefined8 FUN_004078f1(uint *puParm1,undefined8 uParm2,int iParm3);
void FUN_00407942(int *piParm1,int *piParm2,long lParm3,char cParm4);
ulong FUN_00407c18(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00407c94(long *plParm1,byte *pbParm2,long *plParm3,uint uParm4,long lParm5,byte *pbParm6);
undefined8 FUN_0040976c(long lParm1,int *piParm2,ulong uParm3,uint uParm4);
undefined8 FUN_0040986a(int *piParm1,long lParm2,uint uParm3);
undefined8 FUN_00409b20(long *plParm1,undefined8 uParm2,uint uParm3);
long FUN_00409cd0(void);
void FUN_00409d5e(long lParm1);
ulong FUN_00409e40(undefined1 *puParm1,byte bParm2,uint uParm3);
ulong * FUN_00409ec8(ulong *puParm1,uint uParm2);
char * FUN_00409f67(char *pcParm1,int iParm2);
ulong FUN_0040a007(long param_1,ulong param_2,long param_3,ulong param_4,uint param_5,uint param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_0040af29(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_0040b199(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_0040b1da(uint uParm1,undefined8 uParm2);
void FUN_0040b1fe(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_0040b292(undefined8 uParm1,char cParm2);
void FUN_0040b2bc(undefined8 uParm1);
void FUN_0040b2db(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040b307(uint uParm1,undefined8 uParm2);
void FUN_0040b330(undefined8 uParm1);
void FUN_0040b34f(long lParm1);
void FUN_0040b365(long lParm1);
ulong FUN_0040b37b(uint uParm1);
void FUN_0040b38b(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_0040b7fa(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
void FUN_0040b8cd(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_0040b984(undefined8 uParm1);
long FUN_0040b99e(long lParm1);
long FUN_0040b9d3(long lParm1,long lParm2);
ulong FUN_0040ba34(void);
ulong FUN_0040ba5e(uint uParm1);
void FUN_0040ba87(void);
void FUN_0040babc(uint uParm1);
void FUN_0040bb31(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_0040bbb6(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_0040bc9b(long lParm1,int *piParm2);
ulong FUN_0040be7e(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_0040c467(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_0040c535(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
ulong FUN_0040ccfe(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,uint param_6,uint param_7);
void FUN_0040cd8e(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
ulong FUN_0040cdd8(undefined8 uParm1,undefined8 uParm2);
undefined * FUN_0040ce85(undefined8 uParm1);
void FUN_0040cec1(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040cee6(long lParm1,long lParm2);
undefined8 FUN_0040cf9d(long lParm1);
ulong FUN_0040cfce(uint *puParm1,byte *pbParm2,long lParm3,undefined8 uParm4);
long FUN_0040d051(long lParm1,undefined4 uParm2);
ulong FUN_0040d0ca(ulong uParm1);
ulong FUN_0040d175(int iParm1,int iParm2);
long FUN_0040d1b0(ulong param_1,long param_2,int param_3,int param_4,int param_5,uint param_6,int param_7,int param_8,int param_9,int param_10);
long FUN_0040d41d(undefined8 uParm1,undefined8 uParm2);
long FUN_0040d46c(undefined8 param_1,undefined8 param_2,uint param_3,uint param_4,uint param_5,long param_6,uint *param_7);
void FUN_0040d595(code *pcParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_0040d5c7(undefined8 uParm1,long *plParm2,undefined8 uParm3);
long FUN_0040d708(undefined8 *puParm1,undefined8 uParm2,long *plParm3);
void FUN_0040dce2(undefined8 uParm1);
undefined8 FUN_0040dd0b(long lParm1,long lParm2);
ulong FUN_0040dd74(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,long param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_0040df06(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040df2b(long lParm1,long lParm2);
ulong FUN_0040dfbb(int iParm1,int iParm2);
ulong FUN_0040dff6(uint *puParm1,uint *puParm2);
void FUN_0040e09e(long lParm1,undefined8 uParm2,long lParm3);
undefined8 * FUN_0040e0da(long lParm1);
undefined8 FUN_0040e18a(long **pplParm1,char *pcParm2);
void FUN_0040e357(undefined8 *puParm1);
void FUN_0040e399(void);
void FUN_0040e3a9(long lParm1);
ulong FUN_0040e3e0(long lParm1);
long FUN_0040e426(long lParm1);
ulong FUN_0040e4ef(long lParm1);
undefined8 FUN_0040e557(long lParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_0040e603(long lParm1,undefined8 uParm2);
void FUN_0040e6d8(long lParm1);
void FUN_0040e707(void);
ulong FUN_0040e799(char *pcParm1);
ulong FUN_0040e80b(void);
long FUN_0040e85c(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
int * FUN_0040eaa5(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_0040f587(ulong uParm1,long lParm2,long lParm3);
long FUN_0040f7fa(int *param_1,long *param_2);
long FUN_0040f9d1(undefined auParm1 [16],int *piParm2,long *plParm3);
undefined8 FUN_0040fd4c(int iParm1,long lParm2,uint *puParm3,long lParm4,uint uParm5);
void FUN_0041054f(uint param_1);
void FUN_00410599(undefined auParm1 [16],uint uParm2);
ulong FUN_004105e7(void);
ulong FUN_00410929(undefined auParm1 [16],undefined8 uParm2,undefined8 uParm3);
ulong FUN_00410cb3(char *pcParm1,long lParm2);
undefined8 FUN_00410d0c(long param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
char * FUN_00410fcf(char *pcParm1,char **ppcParm2,long lParm3,undefined8 uParm4);
ulong FUN_00418468(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_00418587(int iParm1);
ulong FUN_004185b5(int iParm1);
undefined8 FUN_004185d5(int iParm1);
ulong FUN_004185fe(uint uParm1);
ulong FUN_0041861d(uint uParm1);
ulong FUN_0041863c(byte *pbParm1,byte *pbParm2);
undefined8 FUN_004186b4(undefined8 uParm1);
undefined8 FUN_0041873f(void);
ulong FUN_0041874c(uint uParm1);
undefined * FUN_0041881d(void);
char * FUN_00418be2(void);
long FUN_00418cb0(long lParm1,long lParm2,long lParm3);
long FUN_00418d06(long lParm1,long lParm2,long lParm3);
ulong FUN_00418d5c(int iParm1,int iParm2);
void FUN_00418db0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,uint uParm6);
char * FUN_00418e06(char *pcParm1,long lParm2,char *pcParm3,undefined8 *puParm4,undefined uParm5,undefined8 uParm6,undefined8 uParm7,uint uParm8);
void FUN_0041afce(undefined auParm1 [16],int *piParm2,undefined8 uParm3);
void FUN_0041b057(int *param_1);
ulong FUN_0041b0ec(ulong uParm1,long lParm2);
void FUN_0041b120(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0041b15b(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_0041b1ac(ulong uParm1,ulong uParm2);
ulong FUN_0041b1c7(undefined8 uParm1);
void FUN_0041b27c(undefined8 uParm1);
void FUN_0041b2a1(void);
ulong FUN_0041b2b0(long lParm1);
undefined8 FUN_0041b380(undefined8 uParm1);
void FUN_0041b39f(undefined8 uParm1,undefined8 uParm2,uint uParm3);
undefined8 FUN_0041b3ca(uint *puParm1,ulong *puParm2);
undefined8 FUN_0041bb66(char *pcParm1,ulong *puParm2,char **ppcParm3);
ulong FUN_0041ce36(char *pcParm1,char *pcParm2,uint uParm3);
ulong FUN_0041cfc6(void);
ulong FUN_0041cffd(void);
undefined8 * FUN_0041d028(ulong uParm1);
void FUN_0041d0eb(ulong uParm1);
void FUN_0041d1d0(void);
undefined8 _DT_FINI(void);

