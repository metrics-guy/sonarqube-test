typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined3;
typedef unsigned int    undefined4;
typedef unsigned long    undefined5;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00403290(void);
void FUN_00403520(void);
void FUN_00403850(void);
void entry(void);
void FUN_004038c0(void);
void FUN_00403930(void);
void FUN_004039c0(void);
void FUN_004039f5(void);
void FUN_00403a1a(void);
void FUN_00403a5d(undefined *puParm1);
void FUN_00403bfa(undefined *puParm1);
void FUN_00403ca2(long lParm1);
ulong FUN_00403df9(undefined8 uParm1);
ulong FUN_00403ea9(long lParm1,undefined8 uParm2,long lParm3);
ulong FUN_00403fa6(undefined8 uParm1,undefined8 uParm2,char cParm3,undefined8 uParm4);
ulong FUN_0040405c(uint uParm1);
ulong FUN_00404720(char *pcParm1);
long FUN_0040477f(undefined8 uParm1);
ulong FUN_004047bf(uint uParm1,undefined8 uParm2);
undefined8 FUN_00404867(uint uParm1,undefined8 uParm2,long lParm3,uint uParm4);
long FUN_004048f4(long lParm1);
undefined8 FUN_0040490e(uint uParm1,undefined8 uParm2,long lParm3);
undefined8 FUN_004049aa(long lParm1,long lParm2,char cParm3,char *pcParm4,int iParm5,undefined4 *puParm6);
undefined8 FUN_00404d5b(int iParm1);
ulong FUN_00404d94(char *pcParm1,uint uParm2);
void FUN_00404dcd(undefined8 uParm1,undefined8 uParm2);
void FUN_00404e08(long lParm1);
undefined8 FUN_00404e56(long lParm1,long lParm2,long lParm3,char cParm4);
ulong FUN_0040504f(long lParm1,long lParm2,long lParm3);
ulong FUN_004055b5(long *plParm1,long lParm2);
ulong FUN_00405700(char *pcParm1);
long FUN_0040575f(long lParm1,ulong uParm2);
ulong FUN_004057a5(char *pcParm1,ulong uParm2);
undefined FUN_0040582a(int iParm1);;
void FUN_0040583a(long lParm1);
ulong FUN_00405871(void);
ulong FUN_004058d3(int iParm1);
undefined8 FUN_004058f7(void);
undefined8 FUN_00405921(void);
ulong FUN_00405942(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00405986(uint uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_004059ea(uint uParm1,undefined8 uParm2,char cParm3,long lParm4);
undefined8 FUN_00405acc(uint param_1,uint param_2,long param_3,ulong param_4,ulong param_5,byte param_6,undefined8 param_7,undefined8 param_8,ulong param_9,long *param_10,char *param_11);
void FUN_00405e2c(uint uParm1,uint uParm2);
undefined8 FUN_00405e53(uint uParm1,ulong uParm2);
undefined8 FUN_00405f06(uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,long param_6,int param_7,undefined8 param_8,undefined8 param_9,undefined *param_10);
undefined8 FUN_00406447(long *plParm1,undefined8 *puParm2);
ulong FUN_0040649e(int iParm1);
undefined8 FUN_004064c2(void);
ulong FUN_004064df(undefined8 param_1,undefined8 param_2,byte param_3,undefined8 param_4,undefined8 param_5,undefined8 *param_6,byte *param_7,byte *param_8);
ulong FUN_00406728(long lParm1,undefined8 uParm2,uint uParm3,long lParm4,char cParm5,long lParm6);
void FUN_00406995(void);
undefined8 FUN_004069a7(undefined8 uParm1,undefined8 uParm2,uint uParm3,char cParm4,long lParm5);
undefined8 FUN_00406bc1(undefined8 uParm1,byte bParm2,byte bParm3,long lParm4);
void FUN_00406cc1(uint uParm1,undefined8 uParm2,uint uParm3);
ulong FUN_00406cfd(long lParm1);
ulong FUN_00406d50(undefined8 param_1,char *param_2,long param_3,uint param_4,uint param_5,byte *param_6,long *param_7);
ulong FUN_00407bca(undefined8 uParm1,ulong *puParm2,undefined8 uParm3,ulong *puParm4,int *piParm5,undefined *puParm6);
ulong FUN_0040835b(undefined8 uParm1,uint uParm2);
void FUN_004083a8(long lParm1,undefined8 uParm2,long lParm3);
void FUN_004084b9(long lParm1);
ulong FUN_004084ef(long lParm1,undefined8 uParm2,long lParm3);
void FUN_004085a3(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_00408638(void);
undefined8 FUN_0040866a(undefined8 uParm1,undefined8 uParm2,byte bParm3,char cParm4,char cParm5);
ulong FUN_00408768(long lParm1,char cParm2);
ulong FUN_004087a4(long lParm1,long *plParm2,undefined8 uParm3);
ulong FUN_00408936(char *param_1,undefined8 param_2,byte param_3,long *param_4,undefined8 param_5,uint *param_6,byte param_7,char *param_8,undefined *param_9,undefined *param_10);
undefined8 FUN_0040ad3f(uint *puParm1);
void FUN_0040ae83(undefined8 uParm1,undefined8 uParm2,byte bParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
void FUN_0040af20(long lParm1);
ulong FUN_0040af65(long lParm1);
ulong FUN_0040afa9(long lParm1);
ulong FUN_0040afed(void);
ulong FUN_0040b021(ulong *puParm1,ulong uParm2);
ulong FUN_0040b04a(long *plParm1,long *plParm2);
void FUN_0040b09f(long lParm1);
void FUN_0040b0d2(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_0040b12a(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_0040b17c(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040b219(void);
undefined8 FUN_0040b257(void);
void FUN_0040b262(undefined4 uParm1,undefined4 *puParm2);
ulong FUN_0040b2c0(uint *puParm1);
long FUN_0040b8ea(long lParm1,long lParm2);
void FUN_0040b97c(undefined8 uParm1,uint *puParm2);
ulong FUN_0040b9c0(uint uParm1,undefined8 uParm2,uint uParm3,undefined8 uParm4,uint uParm5,char cParm6);
void FUN_0040bb60(undefined8 uParm1,undefined8 *puParm2);
ulong FUN_0040bb96(undefined8 uParm1,uint uParm2,undefined8 uParm3,char cParm4);
ulong FUN_0040bd06(undefined8 uParm1,uint uParm2,undefined8 uParm3,uint uParm4,uint uParm5);
ulong FUN_0040bdb9(undefined8 uParm1,uint uParm2,uint uParm3);
long FUN_0040be22(undefined8 uParm1,ulong uParm2);
void FUN_0040bf70(char *pcParm1);
void FUN_0040bfca(long lParm1,long lParm2,undefined uParm3);
ulong FUN_0040c0dd(long *plParm1,undefined8 uParm2,long lParm3,long lParm4,long *plParm5);
long FUN_0040c4ad(long lParm1,int iParm2,char cParm3);
void FUN_0040c76f(undefined8 uParm1,uint uParm2);
ulong FUN_0040c796(undefined8 uParm1,char *pcParm2);
void FUN_0040c7f2(undefined8 uParm1,char *pcParm2);
ulong FUN_0040c842(ulong uParm1,ulong uParm2,ulong uParm3);
void FUN_0040c90d(undefined8 uParm1);
ulong FUN_0040c92c(long *plParm1,undefined8 uParm2,undefined8 uParm3);
char * FUN_0040c9d3(char *pcParm1,uint uParm2);
void FUN_0040d1af(void);
void FUN_0040d2b4(void);
long FUN_0040d39a(undefined8 uParm1);
long FUN_0040d463(undefined8 uParm1);
ulong FUN_0040d491(char *pcParm1);
undefined * FUN_0040d512(undefined8 uParm1);
char * FUN_0040d5a9(char *pcParm1);
ulong FUN_0040d612(long lParm1);
ulong FUN_0040d660(char *pcParm1);
void FUN_0040d6c2(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
void FUN_0040d6f3(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_0040d802(long lParm1,undefined8 uParm2,undefined8 *puParm3);
ulong FUN_0040d89e(long lParm1,undefined8 uParm2,undefined8 *puParm3);
undefined * FUN_0040d8fa(long lParm1);
undefined8 FUN_0040d9dc(uint uParm1);
void FUN_0040da87(uint uParm1,undefined *puParm2);
long FUN_0040dc4f(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
char * FUN_0040dc90(char *pcParm1);
long FUN_0040dcb0(long lParm1,char *pcParm2,undefined8 *puParm3);
long FUN_0040ddf7(uint uParm1,long lParm2,long lParm3);
long FUN_0040de78(long *plParm1,undefined8 uParm2);
long FUN_0040decf(long lParm1,long lParm2);
ulong FUN_0040df62(ulong uParm1);
ulong FUN_0040dfcd(ulong uParm1);
ulong FUN_0040e014(undefined8 uParm1,ulong uParm2);
ulong FUN_0040e04b(ulong uParm1,ulong uParm2);
undefined8 FUN_0040e064(long lParm1);
ulong FUN_0040e167(ulong uParm1,long lParm2);
long * FUN_0040e274(undefined8 uParm1,undefined1 *puParm2,code *pcParm3,code *pcParm4,long lParm5);
void FUN_0040e3d9(long **pplParm1,undefined8 uParm2);
long FUN_0040e504(long lParm1);
void FUN_0040e54f(long lParm1,undefined8 *puParm2);
long FUN_0040e585(long lParm1,long lParm2,long **pplParm3,char cParm4);
undefined8 FUN_0040e71a(long lParm1,long **pplParm2,char cParm3);
undefined8 FUN_0040e8ea(long *plParm1,undefined8 uParm2);
undefined8 FUN_0040eaeb(long lParm1,long lParm2,long *plParm3);
undefined8 FUN_0040ee05(undefined8 uParm1,undefined8 uParm2);
long FUN_0040ee4e(long lParm1,undefined8 uParm2);
ulong FUN_0040f0fd(undefined8 *puParm1,ulong uParm2);
ulong FUN_0040f149(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_0040f1c1(undefined8 *puParm1,undefined8 *puParm2);
void FUN_0040f239(undefined8 *puParm1);
undefined8 FUN_0040f26b(void);
void FUN_0040f276(long lParm1);
ulong FUN_0040f358(undefined8 uParm1,uint uParm2,undefined8 uParm3,uint uParm4,uint uParm5);
ulong FUN_0040f3b6(undefined8 uParm1,uint uParm2,undefined4 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_0040f402(undefined1 *puParm1,byte bParm2,uint uParm3);
ulong * FUN_0040f48a(ulong *puParm1,uint uParm2);
char * FUN_0040f529(char *pcParm1,int iParm2);
ulong FUN_0040f5c9(long param_1,ulong param_2,long param_3,ulong param_4,uint param_5,uint param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_004104eb(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_0041075b(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_0041079c(uint uParm1,undefined8 uParm2);
void FUN_004107c0(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_00410854(undefined8 uParm1,char cParm2);
void FUN_0041087e(undefined8 uParm1);
void FUN_0041089d(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_00410938(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00410964(uint uParm1,undefined8 uParm2);
void FUN_0041098d(undefined8 uParm1);
undefined8 FUN_004109ac(undefined4 uParm1);
ulong FUN_004109c8(uint uParm1,long lParm2,uint uParm3,long lParm4,uint uParm5);
undefined8 * FUN_00410e29(undefined8 *puParm1);
long FUN_00410e86(uint uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00410ef6(undefined8 uParm1,undefined8 uParm2);
void FUN_0041109b(undefined8 *puParm1,undefined8 *puParm2);
long FUN_004110d6(long lParm1,uint uParm2);
undefined8 FUN_00411409(undefined8 uParm1,uint uParm2);
void FUN_00411488(void);
undefined8 FUN_00411493(void);
undefined8 FUN_004114b1(void);
undefined8 FUN_004114d3(long lParm1);
undefined8 FUN_004114e5(long lParm1);
undefined8 FUN_004114f7(long lParm1);
void FUN_00411509(long lParm1);
void FUN_0041151f(long lParm1);
ulong FUN_00411535(uint uParm1);
void FUN_00411545(uint uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00411571(undefined8 uParm1,ulong uParm2);
ulong FUN_0041159b(long lParm1,int iParm2,undefined8 uParm3,code *pcParm4,ulong uParm5);
ulong FUN_00411755(uint uParm1);
ulong FUN_004117a2(ulong *puParm1,ulong uParm2);
ulong FUN_004117cb(ulong *puParm1,ulong *puParm2);
ulong FUN_004117fd(undefined8 uParm1,undefined8 *puParm2,long lParm3,uint uParm4);
ulong FUN_00411fa9(undefined8 *puParm1);
undefined8 FUN_0041210f(undefined8 uParm1,long *plParm2);
ulong FUN_00412238(uint uParm1,long lParm2,undefined8 *puParm3);
void FUN_0041267e(undefined8 uParm1,undefined8 uParm2);
ulong FUN_004126a5(undefined8 uParm1,undefined8 *puParm2);
void FUN_004128d8(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_00412d47(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
void FUN_00412e1a(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_00412ed1(void);
void FUN_00412f12(undefined8 uParm1,ulong uParm2,ulong uParm3);
void FUN_00412f70(long lParm1,ulong *puParm2,ulong uParm3);
void FUN_0041304a(undefined8 uParm1);
long FUN_00413064(long lParm1);
long FUN_00413099(long lParm1,long lParm2);
void FUN_004130fa(undefined8 uParm1,undefined8 uParm2);
void FUN_0041312e(undefined8 uParm1);
long FUN_0041315b(void);
long FUN_00413185(undefined8 uParm1,uint uParm2,undefined8 uParm3);
long FUN_004131ed(void);
ulong FUN_00413226(void);
undefined8 FUN_00413271(ulong uParm1,ulong uParm2);
ulong FUN_004132f0(uint uParm1);
void FUN_00413319(void);
void FUN_0041334e(uint uParm1);
void FUN_004133c3(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_00413448(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_0041352d(uint uParm1,undefined8 uParm2,uint uParm3,uint uParm4);
ulong FUN_00413558(uint uParm1,long lParm2,uint uParm3,uint uParm4);
ulong FUN_0041361b(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_004138d4(uint uParm1);
void FUN_00413937(undefined8 uParm1);
void FUN_0041395c(void);
ulong FUN_0041396b(long lParm1);
undefined8 FUN_00413a3b(undefined8 uParm1);
void FUN_00413a5a(undefined8 uParm1,undefined8 uParm2,uint uParm3);
void FUN_00413a85(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
void FUN_00413ab2(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
ulong FUN_00413aef(uint uParm1,long lParm2,long lParm3,uint uParm4);
ulong FUN_00413c02(long *plParm1,long *plParm2);
ulong FUN_00413c57(long lParm1,ulong uParm2);
undefined8 FUN_00413c81(long lParm1);
undefined8 FUN_00413d15(long lParm1,undefined8 *puParm2);
void FUN_00413e23(long lParm1,long lParm2);
void FUN_00413f31(long lParm1);
void FUN_00413f7f(undefined8 uParm1);
void FUN_00413fc3(long lParm1,char cParm2);
long FUN_00414007(uint uParm1,undefined8 uParm2,uint uParm3,uint *puParm4);
void FUN_00414095(long lParm1,uint uParm2,char cParm3);
ulong FUN_00414121(long lParm1);
ulong FUN_004141cb(long lParm1,undefined8 uParm2);
long * FUN_00414252(long *plParm1,uint uParm2,long lParm3);
void FUN_0041461a(long lParm1,long lParm2);
undefined8 FUN_004146d5(long *plParm1);
ulong FUN_0041485e(ulong *puParm1,ulong uParm2);
ulong FUN_0041488f(ulong *puParm1,ulong *puParm2);
undefined8 FUN_004148c1(long lParm1);
undefined8 FUN_00414a38(undefined8 uParm1);
undefined8 FUN_00414a6e(undefined8 uParm1);
long FUN_00414aea(long *plParm1);
undefined8 FUN_004151dd(undefined8 uParm1,long lParm2,int iParm3);
ulong FUN_00415234(long *plParm1,long *plParm2);
void FUN_0041528f(long lParm1,undefined4 uParm2);
long FUN_00415300(long *plParm1,int iParm2);
undefined8 FUN_00415c3f(long lParm1,long lParm2,char cParm3);
long FUN_00415e20(long lParm1,long lParm2,ulong uParm3);
long FUN_00415f67(long lParm1,undefined8 uParm2,long lParm3);
void FUN_00416019(long lParm1);
undefined8 FUN_00416056(long lParm1,long lParm2);
void FUN_00416123(long lParm1,long lParm2);
long FUN_00416232(long *plParm1);
ulong FUN_00416288(long lParm1,long lParm2,uint uParm3,long lParm4);
long FUN_004164ba(long lParm1,undefined *puParm2);
void FUN_00416a58(long lParm1,int *piParm2);
ulong FUN_00416c3b(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_00417224(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_004172f2(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
ulong FUN_00417abb(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,uint param_6,uint param_7);
void FUN_00417b4b(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
ulong FUN_00417b95(long lParm1,uint uParm2,uint uParm3);
ulong FUN_00417d02(uint uParm1,char *pcParm2,uint uParm3,undefined8 uParm4);
ulong FUN_00417ecf(uint uParm1,long lParm2,uint uParm3,long lParm4,uint uParm5);
void FUN_004180ab(undefined8 uParm1,undefined8 uParm2);
ulong FUN_004180d0(long lParm1,long lParm2);
undefined8 FUN_00418187(long lParm1);
ulong FUN_004181b8(uint *puParm1,byte *pbParm2,long lParm3,undefined8 uParm4);
ulong FUN_0041823b(long lParm1,uint uParm2);
undefined8 FUN_00418377(long lParm1,uint uParm2);
undefined8 FUN_0041840a(long lParm1,uint uParm2,long lParm3);
void FUN_004184f7(uint uParm1,undefined8 uParm2,uint uParm3,uint uParm4);
ulong FUN_00418527(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,undefined8 param_2,uint param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_004186ee(long lParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_00418794(long lParm1,long lParm2);
ulong FUN_004187fd(long lParm1,long lParm2);
void FUN_00418d10(uint uParm1,undefined8 uParm2,uint uParm3,undefined8 uParm4);
ulong FUN_00418d43(long lParm1);
void FUN_00418e00(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00418e25(long lParm1,long lParm2);
undefined8 FUN_00418eb5(undefined8 uParm1,uint uParm2,long lParm3);
ulong FUN_00418f5d(long lParm1);
ulong FUN_004190c9(uint uParm1,long lParm2,uint uParm3);
ulong FUN_0041928a(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_004193a9(undefined8 uParm1,undefined8 uParm2,undefined4 uParm3,undefined4 *puParm4);
void FUN_004193e5(undefined8 uParm1,uint uParm2,uint uParm3);
ulong FUN_00419421(uint *puParm1,undefined8 uParm2,uint uParm3);
void FUN_004194d2(uint uParm1,undefined8 uParm2);
void FUN_00419507(void);
long FUN_00419518(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
void FUN_00419647(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_004196c9(long lParm1,long lParm2,long lParm3);
long FUN_0041980a(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
ulong FUN_00419890(ulong uParm1,undefined4 uParm2);
ulong FUN_004198c7(byte *pbParm1,byte *pbParm2);
undefined *FUN_0041993f(uint uParm1,undefined8 uParm2,undefined *puParm3,ulong uParm4,code **ppcParm5,code *pcParm6);
undefined8 FUN_00419be1(uint uParm1,char cParm2);
undefined8 FUN_00419c5c(undefined8 uParm1);
ulong FUN_00419ce7(ulong uParm1);
void FUN_00419d03(long lParm1);
undefined8 FUN_00419d25(long *plParm1,long *plParm2);
undefined8 FUN_00419df9(void);
void FUN_00419e06(undefined8 *puParm1);
ulong FUN_00419e5b(uint uParm1);
ulong FUN_00419f2c(char *pcParm1,ulong uParm2);
void FUN_00419f86(long lParm1,undefined4 uParm2);
ulong FUN_00419fdf(long lParm1);
ulong FUN_00419ff1(long lParm1,undefined4 uParm2);
ulong FUN_0041a079(long lParm1);
undefined * FUN_0041a0fb(void);
char * FUN_0041a4c0(void);
void FUN_0041a58e(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,undefined8 param_2,uint param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 * FUN_0041a69f(undefined8 uParm1);
undefined8 FUN_0041a6e6(undefined8 uParm1,undefined8 uParm2);
long FUN_0041a729(long lParm1);
ulong FUN_0041a73b(undefined8 *puParm1,ulong uParm2);
void FUN_0041a8e9(undefined8 uParm1);
ulong FUN_0041a915(undefined8 *puParm1);
ulong * FUN_0041a958(ulong uParm1,ulong uParm2);
undefined8 * FUN_0041a9ba(undefined8 uParm1,undefined8 uParm2);
void FUN_0041aa01(long lParm1,ulong uParm2,ulong uParm3);
long FUN_0041ac67(long lParm1,ulong uParm2);
void FUN_0041ad56(undefined8 *puParm1,long lParm2,long lParm3);
void FUN_0041adf0(ulong *puParm1,ulong uParm2,ulong uParm3);
void FUN_0041af35(long *plParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_0041af8c(long *plParm1);
undefined8 FUN_0041afdc(undefined8 uParm1);
undefined8 FUN_0041aff6(long lParm1,undefined8 uParm2);
void FUN_0041b03a(ulong *puParm1,long *plParm2);
void FUN_0041b689(long lParm1);
void FUN_0041bc11(uint uParm1);
ulong FUN_0041bc37(long lParm1,uint uParm2,uint uParm3);
void FUN_0041bde0(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0041be08(undefined8 uParm1);
ulong FUN_0041bebd(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_0041bf6a(uint uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_0041c018(void);
long FUN_0041c069(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
int * FUN_0041c2b2(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_0041cd94(ulong uParm1,long lParm2,long lParm3);
long FUN_0041d007(int *param_1,long *param_2);
long FUN_0041d1de(undefined auParm1 [16],int *piParm2,long *plParm3);
undefined8 FUN_0041d559(int iParm1,long lParm2,uint *puParm3,long lParm4,uint uParm5);
void FUN_0041dd5c(uint param_1);
void FUN_0041dda6(undefined auParm1 [16],uint uParm2);
ulong FUN_0041ddf4(void);
ulong FUN_0041e136(undefined auParm1 [16],undefined8 uParm2,undefined8 uParm3);
ulong FUN_0041e4c0(char *pcParm1,long lParm2);
undefined8 FUN_0041e519(long param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
ulong FUN_00425c75(uint uParm1);
long FUN_00425c94(undefined8 uParm1,undefined8 uParm2);
void FUN_00425d7a(undefined auParm1 [16],int *piParm2,undefined8 uParm3);
void FUN_00425e03(int *param_1);
ulong FUN_00425e98(ulong uParm1,long lParm2);
void FUN_00425ecc(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00425f07(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00425f58(ulong uParm1,ulong uParm2);
undefined8 FUN_00425f73(uint *puParm1,ulong *puParm2);
undefined8 FUN_0042670f(char *pcParm1,ulong *puParm2,char **ppcParm3);
ulong FUN_004279df(void);
ulong FUN_00427a16(void);
void FUN_00427a50(void);
undefined8 _DT_FINI(void);

