typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void entry(void);
void FUN_004026b0(void);
void FUN_00402720(void);
void FUN_004027b0(void);
ulong FUN_004027e5(byte bParm1);
void FUN_004027f4(void);
void FUN_00402819(void);
void FUN_0040283e(undefined8 uParm1);
void FUN_00402863(undefined *puParm1);
char * FUN_00402a00(char *pcParm1);
void FUN_00402a52(void);
void FUN_00402acb(void);
void FUN_00402c18(int iParm1,long lParm2);
void FUN_00402d17(undefined8 uParm1);
void FUN_00402ed1(void);
void FUN_0040310d(void);
undefined8 FUN_0040343a(long lParm1);
undefined8 FUN_004034a8(long lParm1);
ulong FUN_00403516(ulong *puParm1,ulong uParm2);
ulong FUN_0040353f(ulong *puParm1,ulong *puParm2);
undefined8 FUN_00403571(undefined8 uParm1);
void FUN_004035b0(undefined8 uParm1);
void FUN_004035cb(char cParm1);
undefined8 FUN_004039cf(undefined8 uParm1);
undefined FUN_00403a03(ulong uParm1);;
undefined * FUN_00403a15(byte bParm1,long lParm2,long lParm3,undefined8 uParm4,undefined8 uParm5);
void FUN_00403aab(ulong *puParm1,byte *pbParm2,ulong uParm3,byte bParm4);
ulong FUN_00403b78(long lParm1);
void FUN_00403bce(undefined8 *puParm1,undefined8 *puParm2,undefined8 *puParm3);
void FUN_00403da3(long *plParm1,long lParm2);
void FUN_00403ec6(char *param_1,char *param_2,undefined *param_3,char *param_4,undefined *param_5,char param_6,char param_7,undefined8 *param_8,char param_9);
char * FUN_004049c4(undefined8 uParm1);
undefined8 FUN_00404a79(char *pcParm1);
void FUN_00404d30(undefined8 uParm1,long *plParm2);
void FUN_00405114(undefined8 uParm1,long lParm2);
void FUN_00405172(void);
void FUN_00405211(undefined8 uParm1);
void FUN_00405253(undefined8 uParm1);
ulong FUN_00405295(uint uParm1);
undefined8 FUN_00405d73(undefined8 uParm1,long *plParm2);
void FUN_0040621a(undefined8 uParm1);
ulong FUN_00406239(long *plParm1,undefined8 uParm2,undefined8 uParm3);
char * FUN_004062e0(char *pcParm1,uint uParm2);
void FUN_00406abc(void);
long FUN_00406ba2(undefined8 uParm1);
ulong FUN_00406bd0(char *pcParm1);
undefined * FUN_00406c51(undefined8 uParm1);
char * FUN_00406ce8(char *pcParm1);
void FUN_00406d51(long lParm1,undefined8 uParm2,undefined8 *puParm3);
ulong FUN_00406ded(long lParm1,undefined8 uParm2,undefined8 *puParm3);
long FUN_00406e49(long *plParm1,undefined8 uParm2);
long FUN_00406ea0(long lParm1,long lParm2);
ulong FUN_00406f33(ulong uParm1);
ulong FUN_00406f9e(ulong uParm1);
ulong FUN_00406fe5(undefined8 uParm1,ulong uParm2);
ulong FUN_0040701c(ulong uParm1,ulong uParm2);
undefined8 FUN_00407035(long lParm1);
ulong FUN_00407138(ulong uParm1,long lParm2);
long * FUN_00407245(undefined8 uParm1,undefined1 *puParm2,code *pcParm3,code *pcParm4,long lParm5);
void FUN_004073aa(long **pplParm1,undefined8 uParm2);
long FUN_004074d5(long lParm1);
void FUN_00407520(long lParm1,undefined8 *puParm2);
long FUN_00407556(long lParm1,long lParm2,long **pplParm3,char cParm4);
undefined8 FUN_004076eb(long lParm1,long **pplParm2,char cParm3);
undefined8 FUN_004078bb(long *plParm1,undefined8 uParm2);
undefined8 FUN_00407abc(long lParm1,long lParm2,long *plParm3);
undefined8 FUN_00407dd6(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00407e1f(undefined8 *puParm1,ulong uParm2);
ulong FUN_00407e6b(undefined8 *puParm1,undefined8 *puParm2);
void FUN_00407ee3(undefined8 *puParm1);
void FUN_00407f15(void);
long FUN_00407ffa(long lParm1,ulong uParm2,byte *pbParm3,undefined8 uParm4);
char * FUN_004080f6(ulong uParm1,long lParm2,uint uParm3,ulong uParm4,ulong uParm5);
undefined8 FUN_00408a5a(void);
ulong FUN_00408a7b(char *pcParm1,undefined8 *puParm2,uint *puParm3);
ulong FUN_00408be2(undefined8 uParm1,undefined8 uParm2,long *plParm3);
char * FUN_00408c34(ulong uParm1,long lParm2);
ulong FUN_00408cb9(uint *puParm1);
long FUN_00408d07(uint *puParm1,ulong uParm2);
undefined * FUN_00408d8d(undefined *puParm1,undefined *puParm2,long lParm3);
long FUN_00408dd6(long lParm1,long lParm2,long lParm3,ulong *puParm4,int iParm5,uint uParm6);
long FUN_0040919e(undefined8 uParm1,ulong *puParm2,uint uParm3,uint uParm4);
void FUN_0040928c(undefined8 uParm1,uint uParm2);
ulong FUN_004092be(byte *pbParm1,long lParm2,uint uParm3);
void FUN_004094f3(long lParm1);
ulong FUN_004095d5(undefined1 *puParm1,byte bParm2,uint uParm3);
ulong * FUN_0040965d(ulong *puParm1,uint uParm2);
char * FUN_004096fc(char *pcParm1,int iParm2);
ulong FUN_0040979c(long param_1,ulong param_2,long param_3,ulong param_4,uint param_5,uint param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_0040a6be(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_0040a92e(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_0040a96f(uint uParm1,undefined8 uParm2);
void FUN_0040a993(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_0040aa27(undefined8 uParm1,char cParm2);
void FUN_0040aa51(undefined8 uParm1);
void FUN_0040aa70(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_0040ab0b(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040ab37(uint uParm1,undefined8 uParm2);
void FUN_0040ab60(undefined8 uParm1);
undefined8 FUN_0040ab7f(int *piParm1);
void FUN_0040abf4(uint *puParm1);
void FUN_0040ac2b(uint *puParm1);
void FUN_0040ac61(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_0040b0d0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
void FUN_0040b1a3(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_0040b25a(ulong uParm1,ulong uParm2);
void FUN_0040b2aa(undefined8 uParm1,ulong uParm2,ulong uParm3);
void FUN_0040b308(undefined8 uParm1);
long FUN_0040b322(long lParm1);
long FUN_0040b357(long lParm1,long lParm2);
void FUN_0040b3b8(undefined8 uParm1,undefined8 uParm2);
void FUN_0040b3ec(undefined8 uParm1);
long FUN_0040b419(void);
long FUN_0040b443(void);
void FUN_0040b47c(uint uParm1,int iParm2,undefined uParm3,long lParm4,undefined8 uParm5,uint uParm6);
undefined8 FUN_0040b556(uint uParm1,uint uParm2,char cParm3,undefined8 uParm4,undefined8 uParm5);
undefined8 FUN_0040b59c(int iParm1);
ulong FUN_0040b5c2(ulong *puParm1,int iParm2);
ulong FUN_0040b621(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_0040b662(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
ulong FUN_0040ba4b(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0040baff(ulong uParm1,ulong uParm2);
void FUN_0040bb7e(undefined4 *puParm1);
void FUN_0040bb93(uint *puParm1);
void FUN_0040bbae(uint *puParm1);
undefined8 FUN_0040bc00(uint *puParm1,undefined8 uParm2);
long FUN_0040bc5a(long lParm1);
ulong FUN_0040bc88(char *pcParm1);
ulong FUN_0040bf73(uint uParm1);
void FUN_0040bf9c(void);
void FUN_0040bfd1(uint uParm1);
void FUN_0040c046(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_0040c0cb(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0040c1b0(void);
undefined8 FUN_0040c1bb(undefined8 uParm1,undefined8 uParm2,long *plParm3);
long FUN_0040c2a4(long lParm1,undefined *puParm2);
void FUN_0040c842(long lParm1,int *piParm2);
ulong FUN_0040ca25(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_0040d00e(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_0040d0dc(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
ulong FUN_0040d8a5(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,uint param_6,uint param_7);
void FUN_0040d935(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
void FUN_0040d97f(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040d9a4(long lParm1,long lParm2);
undefined8 FUN_0040da5b(long lParm1);
ulong FUN_0040da8c(uint *puParm1,byte *pbParm2,long lParm3,undefined8 uParm4);
ulong * FUN_0040db0f(ulong *puParm1,char cParm2,ulong uParm3);
undefined8 FUN_0040dc22(void);
void FUN_0040dc33(long lParm1);
char ** FUN_0040ddd8(void);
void FUN_0040e710(undefined8 *puParm1);
void FUN_0040e779(uint uParm1,undefined8 uParm2,uint uParm3,uint uParm4);
ulong FUN_0040e7a9(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,undefined8 param_2,uint param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0040e970(long lParm1,long lParm2);
void FUN_0040e9d9(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040e9fe(long lParm1,long lParm2);
long FUN_0040ea8e(long lParm1,ulong uParm2,long *plParm3);
long FUN_0040ecb6(long lParm1,long lParm2,long lParm3,ulong uParm4);
long FUN_0040efb2(long lParm1,long lParm2,long lParm3,ulong uParm4);
char * FUN_0040f464(char *pcParm1,char *pcParm2);
ulong FUN_0040f5b2(long *plParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040f62e(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040f74d(uint *puParm1,long lParm2);
void FUN_0040f7c4(uint uParm1);
long FUN_0040f80c(undefined8 uParm1,ulong uParm2);
long FUN_0040f95a(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_0040fa89(ulong uParm1,undefined4 uParm2);
ulong FUN_0040fac0(byte *pbParm1,byte *pbParm2);
undefined8 FUN_0040fb38(uint uParm1,char cParm2);
undefined8 FUN_0040fbb3(undefined8 uParm1);
void FUN_0040fc3e(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0040fd4d(void);
ulong FUN_0040fd5a(uint uParm1);
ulong FUN_0040fe2b(char *pcParm1,ulong uParm2);
undefined * FUN_0040fe85(void);
char * FUN_0041024a(void);
ulong FUN_00410318(uint uParm1);
ulong FUN_00410328(uint uParm1);
undefined8 FUN_00410375(undefined8 uParm1);
undefined8 FUN_0041043b(uint uParm1,undefined8 uParm2);
ulong FUN_00410656(undefined8 uParm1);
ulong FUN_0041070b(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_004109c4(uint uParm1);
void FUN_00410a27(undefined8 uParm1);
void FUN_00410a4c(void);
ulong FUN_00410a5b(long lParm1);
undefined8 FUN_00410b2b(undefined8 uParm1);
void FUN_00410b4a(undefined8 uParm1,undefined8 uParm2,uint uParm3);
void FUN_00410b75(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
void FUN_00410ba2(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
ulong FUN_00410bdf(uint uParm1,long lParm2,long lParm3,uint uParm4);
void FUN_00410cf2(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_00410d24(long lParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00410dca(void);
long FUN_00410e1b(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
int * FUN_00411064(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_00411b46(ulong uParm1,long lParm2,long lParm3);
long FUN_00411db9(int *param_1,long *param_2);
long FUN_00411f90(undefined auParm1 [16],int *piParm2,long *plParm3);
undefined8 FUN_0041230b(int iParm1,long lParm2,uint *puParm3,long lParm4,uint uParm5);
void FUN_00412b0e(uint param_1);
void FUN_00412b58(undefined auParm1 [16],uint uParm2);
ulong FUN_00412ba6(void);
ulong FUN_00412ee8(undefined auParm1 [16],undefined8 uParm2,undefined8 uParm3);
ulong FUN_00413272(char *pcParm1,long lParm2);
undefined8 FUN_004132cb(long param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
ulong FUN_0041aa27(uint uParm1);
void FUN_0041aa46(undefined auParm1 [16],int *piParm2,undefined8 uParm3);
void FUN_0041aacf(int *param_1);
void FUN_0041ab64(uint uParm1);
ulong FUN_0041ab8a(ulong uParm1,long lParm2);
void FUN_0041abbe(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0041abf9(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_0041ac4a(ulong uParm1,ulong uParm2);
void FUN_0041ac65(void);
long FUN_0041ac6c(long *plParm1,ulong *puParm2,int iParm3,long lParm4);
undefined8 FUN_0041ae45(uint *puParm1,ulong *puParm2);
undefined8 FUN_0041b5e1(char *pcParm1,ulong *puParm2,char **ppcParm3);
ulong FUN_0041c8b1(void);
ulong FUN_0041c8e8(void);
void FUN_0041c920(void);
undefined8 _DT_FINI(void);

