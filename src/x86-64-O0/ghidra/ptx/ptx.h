typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void entry(void);
void FUN_00402670(void);
void FUN_004026e0(void);
void FUN_00402770(void);
undefined8 FUN_004027a5(int iParm1);
ulong FUN_004027cb(byte bParm1);
void FUN_004027da(void);
void FUN_004027ff(void);
void FUN_00402824(undefined *puParm1);
char * FUN_004029c1(void);
char * FUN_004029e2(char *pcParm1);
void FUN_00402cea(undefined8 *puParm1);
void FUN_00402dc1(void);
void FUN_00402efe(char *pcParm1,long *plParm2);
ulong FUN_00402ff8(long *plParm1,long lParm2);
ulong FUN_0040315a(ulong *puParm1,ulong *puParm2);
undefined8 FUN_004031be(undefined8 uParm1,long *plParm2);
void FUN_00403268(void);
void FUN_0040329e(undefined8 uParm1);
void FUN_00403339(undefined8 uParm1,long *plParm2);
void FUN_00403490(int iParm1);
void FUN_00403ae7(long lParm1);
void FUN_00403b17(byte *pbParm1,byte *pbParm2);
void FUN_00403f15(void);
void FUN_00404273(char **ppcParm1);
void FUN_00404e2c(void);
void FUN_00405023(void);
void FUN_0040527d(void);
void FUN_004056a0(void);
undefined8 FUN_0040574e(uint uParm1);
void FUN_004060d6(void);
long FUN_004060e7(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
void FUN_00406216(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_00406298(long lParm1,long lParm2,long lParm3);
long FUN_004063d9(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
void FUN_0040645f(void);
undefined8 FUN_00406545(uint uParm1);
long FUN_00406598(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004066ee(long lParm1);
ulong FUN_004067d0(char *pcParm1,undefined8 uParm2);
long FUN_00406c23(long lParm1,long lParm2);
ulong FUN_00406e29(undefined1 *puParm1,byte bParm2,uint uParm3);
ulong * FUN_00406eb1(ulong *puParm1,uint uParm2);
char * FUN_00406f50(char *pcParm1,int iParm2);
ulong FUN_00406ff0(long param_1,ulong param_2,long param_3,ulong param_4,uint param_5,uint param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_00407f12(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_00408182(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_004081c3(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_00408257(undefined8 uParm1,char cParm2);
void FUN_00408281(undefined8 uParm1);
void FUN_004082a0(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_0040833b(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00408367(uint uParm1,undefined8 uParm2);
void FUN_00408390(undefined8 uParm1);
long FUN_004083af(undefined8 uParm1,ulong *puParm2);
long FUN_004085e8(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00408686(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_004086b0(int iParm1);
byte * FUN_004086d6(undefined8 uParm1,int iParm2);
void FUN_004089ee(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_00408e5d(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
void FUN_00408f30(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00408fe7(ulong uParm1,ulong uParm2);
void FUN_00409037(long lParm1,ulong *puParm2,ulong uParm3);
void FUN_00409111(undefined8 uParm1);
long FUN_0040912b(long lParm1);
long FUN_00409160(long lParm1,long lParm2);
long FUN_004091c1(void);
long FUN_004091eb(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_00409238(long *plParm1,int iParm2);
ulong FUN_004092d6(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_00409317(char *pcParm1,char **ppcParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_004096be(ulong uParm1,ulong uParm2);
ulong FUN_0040973d(uint uParm1);
void FUN_00409766(void);
void FUN_0040979b(uint uParm1);
void FUN_00409810(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_00409895(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_0040997a(undefined8 uParm1);
void FUN_00409a2f(undefined8 uParm1);
void FUN_00409a54(void);
ulong FUN_00409a63(long lParm1);
undefined8 FUN_00409b33(undefined8 uParm1);
void FUN_00409b52(undefined8 uParm1,undefined8 uParm2,uint uParm3);
void FUN_00409b7d(long lParm1,int *piParm2);
ulong FUN_00409d60(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_0040a349(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_0040a417(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
ulong FUN_0040abe0(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,uint param_6,uint param_7);
void FUN_0040ac70(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
undefined8 FUN_0040acba(long lParm1);
ulong FUN_0040aceb(uint *puParm1,byte *pbParm2,long lParm3,undefined8 uParm4);
undefined8 FUN_0040ad6e(long lParm1,long lParm2);
undefined8 FUN_0040add7(int iParm1);
void FUN_0040adfd(long lParm1,long lParm2);
void FUN_0040ae6c(long lParm1,long lParm2);
ulong FUN_0040aee4(long lParm1,long lParm2);
void FUN_0040af3b(undefined8 uParm1);
void FUN_0040af60(undefined8 uParm1);
void FUN_0040af85(undefined8 uParm1,undefined8 uParm2);
void FUN_0040afb0(long lParm1);
void FUN_0040b000(long lParm1,long lParm2);
void FUN_0040b06b(long lParm1,long lParm2);
ulong FUN_0040b0d6(long lParm1,long lParm2);
ulong FUN_0040b14a(long lParm1,long lParm2);
undefined8 FUN_0040b193(void);
ulong FUN_0040b1a6(long param_1,undefined8 param_2,long param_3,long param_4,undefined8 param_5,byte param_6,long param_7);
ulong FUN_0040b2ef(long lParm1,undefined8 uParm2,long lParm3,long lParm4,byte bParm5,long lParm6);
undefined8 FUN_0040b4ca(long lParm1,ulong uParm2);
void FUN_0040b641(undefined8 uParm1,undefined8 uParm2,undefined8 *puParm3,long lParm4,char cParm5,long lParm6);
void FUN_0040b732(long *plParm1);
undefined8 FUN_0040ba5f(long *plParm1);
long FUN_0040c501(long *plParm1,long lParm2,uint *puParm3);
void FUN_0040c634(long *plParm1);
void FUN_0040c703(long *plParm1);
ulong FUN_0040c7a5(byte **ppbParm1,byte *pbParm2,uint uParm3);
ulong FUN_0040d46a(long *plParm1,long lParm2);
ulong FUN_0040d5ec(long *plParm1);
void FUN_0040d776(long lParm1);
ulong FUN_0040d7c4(long lParm1,ulong uParm2,uint uParm3);
undefined8 FUN_0040d94a(long *plParm1,long lParm2);
undefined8 FUN_0040d9b4(undefined8 *puParm1,undefined8 uParm2);
undefined8 FUN_0040da3b(undefined8 *puParm1,long lParm2,long lParm3);
undefined8 FUN_0040db16(long *plParm1,long lParm2);
undefined8 FUN_0040dbf3(long *plParm1,long lParm2,long lParm3);
undefined8 FUN_0040dfdc(long *plParm1,long lParm2,long lParm3);
undefined8 FUN_0040e2e4(long *plParm1,long lParm2);
ulong FUN_0040e6a6(long *plParm1,long lParm2);
undefined8 FUN_0040e893(long *plParm1,undefined8 uParm2);
undefined8 FUN_0040e948(long lParm1,long lParm2);
long FUN_0040e9d7(long lParm1,long lParm2);
void FUN_0040ea8f(long lParm1,long lParm2);
long FUN_0040eb0f(long *plParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_0040eeaa(long lParm1,uint uParm2);
ulong * FUN_0040ef04(undefined4 *puParm1,long lParm2,long lParm3);
ulong * FUN_0040f025(undefined4 *puParm1,long lParm2,long lParm3,uint uParm4);
undefined8 FUN_0040f152(long *plParm1,ulong *puParm2,ulong uParm3);
void FUN_0040f306(long lParm1);
long FUN_0040f3aa(long *plParm1,long lParm2,undefined8 uParm3);
long FUN_0040f57c(long *plParm1,long lParm2,uint uParm3,undefined8 uParm4);
undefined1 * FUN_0040f860(undefined8 uParm1,undefined8 uParm2,long lParm3);
undefined8 FUN_0040f8ec(long *plParm1);
void FUN_0040f9e5(long **pplParm1,long lParm2,long lParm3);
void FUN_004100c7(long *plParm1,undefined8 uParm2);
ulong FUN_0041032a(long *plParm1,undefined8 uParm2,undefined8 uParm3,ulong uParm4);
undefined8 FUN_004106a6(long *plParm1,ulong uParm2);
void FUN_00410a23(long lParm1);
void FUN_00410ba6(long *plParm1);
ulong FUN_00410c36(long *plParm1);
void FUN_00410f7e(long *plParm1);
ulong FUN_004111f7(long *plParm1);
ulong FUN_0041157c(long **pplParm1,code *pcParm2,undefined8 uParm3);
ulong FUN_0041165a(long *plParm1,code *pcParm2,undefined8 uParm3);
undefined8 FUN_00411715(long lParm1,long lParm2);
ulong FUN_0041188d(undefined8 uParm1,long lParm2);
long FUN_00411969(undefined4 *puParm1,long *plParm2,long lParm3);
undefined8 FUN_00411b5a(long *plParm1,long lParm2);
undefined8 FUN_00411c48(undefined8 uParm1,long lParm2);
ulong FUN_00411cf5(long lParm1,long lParm2);
ulong FUN_00411f6a(long *plParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
long FUN_0041249a(long *plParm1,long lParm2,uint uParm3);
long FUN_00412533(long *plParm1,long lParm2,undefined4 uParm3);
undefined8 FUN_00412665(long lParm1);
ulong FUN_004127a3(long lParm1);
ulong FUN_00412886(undefined8 *puParm1,long *plParm2,long lParm3,char cParm4);
void FUN_00412c58(undefined8 uParm1,long lParm2,undefined8 uParm3);
undefined8 FUN_00412c9b(long *plParm1,long lParm2,ulong uParm3);
ulong FUN_0041352c(byte *pbParm1,long lParm2,ulong uParm3);
long FUN_0041374f(undefined8 uParm1,long *plParm2,ulong uParm3,int *piParm4);
long FUN_0041387b(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_00413a7d(undefined8 uParm1,undefined8 *puParm2,long lParm3,undefined8 uParm4,long lParm5,int *piParm6);
long FUN_00413c3c(long lParm1,long *plParm2,undefined8 *puParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_004144e1(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,undefined8 uParm5,int *piParm6);
long FUN_00414675(long lParm1,long lParm2,undefined8 uParm3,undefined8 *puParm4,ulong uParm5,undefined4 *puParm6);
ulong FUN_00414ba6(byte bParm1,long lParm2);
undefined8 FUN_00414bdd(ulong uParm1,undefined8 uParm2,long lParm3,long *plParm4,int *piParm5,int *piParm6);
undefined8 FUN_00414f9e(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,byte *pbParm4);
long FUN_00414ffd(long lParm1,long lParm2,long lParm3,ulong uParm4,int *piParm5);
undefined8 FUN_00415932(undefined4 *param_1,long param_2,undefined *param_3,int param_4,undefined8 param_5,undefined8 param_6,char param_7);
undefined8 FUN_00415a7d(undefined4 *puParm1,long lParm2,char *pcParm3);
undefined8 FUN_00415be9(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,byte *pbParm4);
undefined8 FUN_00415c43(long lParm1,undefined8 uParm2,long lParm3,long *plParm4,undefined *puParm5,ulong uParm6);
long FUN_004165aa(long lParm1,undefined8 uParm2,undefined8 uParm3,char *pcParm4,byte bParm5,int *piParm6);
long FUN_00416842(undefined8 uParm1,byte *pbParm2,undefined8 uParm3);
void FUN_00416929(undefined8 *puParm1);
void FUN_00416963(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined uParm4);
undefined8 * FUN_0041699a(long lParm1,undefined8 *puParm2,undefined8 *puParm3,undefined8 *puParm4);
undefined8 FUN_00416ae6(long lParm1,long lParm2);
void FUN_00416b29(undefined8 *puParm1);
undefined8 FUN_00416b8e(undefined8 uParm1,long lParm2);
long * FUN_00416bb5(long **pplParm1,undefined8 uParm2);
void FUN_00416ce1(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
void FUN_00416d2d(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_00416d7e(long param_1,undefined8 param_2,ulong param_3,ulong param_4,ulong param_5,undefined8 param_6,ulong *param_7,char param_8);
ulong FUN_004170f3(ulong *puParm1,long lParm2,ulong uParm3,int iParm4);
ulong FUN_00417399(long *param_1,long param_2,long param_3,long param_4,long param_5,undefined8 param_6,ulong param_7,undefined8 *param_8,uint param_9);
ulong FUN_0041815a(long lParm1);
long FUN_0041847f(long lParm1,char cParm2,long *plParm3);
undefined8 FUN_0041899b(long *plParm1,long lParm2,uint uParm3);
undefined8 FUN_00418a62(long lParm1,long lParm2,undefined8 uParm3);
long FUN_00418b05(long param_1,undefined8 param_2,long param_3,long *param_4,long param_5,long param_6,long param_7);
ulong FUN_00419044(long *plParm1,undefined8 uParm2,undefined8 uParm3,long lParm4,undefined8 uParm5,undefined8 uParm6);
undefined8 FUN_00419214(long *plParm1,undefined8 *puParm2,long lParm3,undefined8 uParm4,undefined8 *puParm5);
undefined8 FUN_00419362(long *plParm1,long lParm2,ulong uParm3,long *plParm4,char cParm5);
undefined8 FUN_00419772(long *plParm1);
void FUN_00419808(long *plParm1,long lParm2,long lParm3,long lParm4,long lParm5,long lParm6);
ulong FUN_004199ac(long lParm1,long *plParm2);
undefined8 FUN_00419b4d(long lParm1,long *plParm2,long lParm3,undefined8 uParm4);
ulong FUN_00419d58(long lParm1,long lParm2);
ulong FUN_00419e42(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_00419f9c(long lParm1,long *plParm2,long lParm3,long lParm4);
ulong FUN_0041a17b(long lParm1,long lParm2,undefined8 uParm3);
ulong FUN_0041a2aa(long *plParm1,long lParm2,undefined8 uParm3,undefined8 uParm4);
undefined8 FUN_0041a538(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_0041a6a4(long lParm1,uint uParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_0041a923(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long lParm5,undefined8 uParm6);
ulong FUN_0041a9f0(long *plParm1,long lParm2,undefined8 uParm3,long lParm4,long lParm5,long lParm6);
ulong FUN_0041ad69(long lParm1,long *plParm2,long lParm3,long lParm4);
ulong FUN_0041b23e(long lParm1,long *plParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_0041b318(int *piParm1,long lParm2,long lParm3);
long FUN_0041b48e(int *piParm1,long lParm2,long lParm3);
long FUN_0041b714(int *piParm1,long lParm2);
ulong FUN_0041b7be(long lParm1,long lParm2,undefined8 uParm3);
ulong FUN_0041b8b9(long lParm1,long lParm2);
ulong FUN_0041bc3b(long lParm1,long lParm2);
ulong FUN_0041c1a7(long lParm1,long lParm2,long lParm3);
ulong FUN_0041c71e(undefined8 uParm1,long *plParm2,undefined8 *puParm3,undefined8 uParm4,long lParm5);
long FUN_0041c7f5(long *plParm1,long lParm2,long lParm3,uint uParm4);
ulong FUN_0041c881(long param_1,long *param_2,long param_3,long param_4,undefined8 param_5,long param_6,uint param_7);
ulong FUN_0041d009(long lParm1,long lParm2,long lParm3,undefined8 uParm4);
ulong FUN_0041d2cc(long lParm1,undefined8 *puParm2,undefined8 uParm3,uint uParm4);
ulong FUN_0041d440(long *plParm1,undefined8 uParm2,long lParm3,long lParm4,uint uParm5);
ulong FUN_0041d605(long lParm1,undefined8 uParm2,long lParm3,undefined8 uParm4,uint uParm5);
undefined8 FUN_0041d9e5(long lParm1,long lParm2);
long FUN_0041e40d(long *plParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_0041ecb3(long *plParm1,long lParm2,long lParm3,long lParm4);
undefined8 FUN_0041f0ef(long lParm1,undefined8 *puParm2,long lParm3);
ulong FUN_0041f2ca(long lParm1,int iParm2);
undefined8 FUN_0041f452(long lParm1,undefined4 uParm2,ulong uParm3);
void FUN_0041f590(long lParm1);
void FUN_0041f6a1(long lParm1);
undefined8 FUN_0041f6e2(long lParm1,undefined8 uParm2,long lParm3,long lParm4,long lParm5);
long FUN_0041fa02(long lParm1,long lParm2);
undefined8 FUN_0041fad8(long lParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 * FUN_0041fc50(long lParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0041fd5c(undefined8 *puParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
ulong FUN_0041fdc4(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0041fee3(byte *pbParm1,byte *pbParm2);
undefined8 FUN_0041ff5b(undefined8 uParm1);
undefined8 FUN_0041ffe6(void);
ulong FUN_0041fff3(uint uParm1);
undefined * FUN_004200c4(void);
char * FUN_00420489(void);
void FUN_00420557(long *plParm1);
undefined8 FUN_0042073c(char *pcParm1,long lParm2,char *pcParm3,char **ppcParm4);
undefined8 FUN_0042099e(long lParm1,long lParm2,long *plParm3);
char * FUN_0042104c(char *pcParm1,char *pcParm2);
void FUN_0042179a(char *pcParm1);
undefined8 FUN_0042197d(undefined8 uParm1,long lParm2,undefined8 uParm3,long *plParm4,ulong *puParm5);
long FUN_00421cd6(undefined8 uParm1,undefined8 uParm2);
long FUN_00421dad(char *pcParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_00421eb0(long lParm1,long lParm2);
ulong * FUN_00421ef4(ulong *puParm1,char cParm2,ulong uParm3);
ulong FUN_00422007(void);
long FUN_00422058(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
int * FUN_004222a1(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_00422d83(ulong uParm1,long lParm2,long lParm3);
long FUN_00422ff6(int *param_1,long *param_2);
long FUN_004231cd(undefined auParm1 [16],int *piParm2,long *plParm3);
undefined8 FUN_00423548(int iParm1,long lParm2,uint *puParm3,long lParm4,uint uParm5);
void FUN_00423d4b(uint param_1);
void FUN_00423d95(undefined auParm1 [16],uint uParm2);
ulong FUN_00423de3(void);
ulong FUN_00424125(undefined auParm1 [16],undefined8 uParm2,undefined8 uParm3);
ulong FUN_004244af(char *pcParm1,long lParm2);
undefined8 FUN_00424508(long param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
char * FUN_004247cb(char *pcParm1,char **ppcParm2,long lParm3,undefined8 uParm4);
ulong FUN_0042bc64(uint uParm1);
undefined8 * FUN_0042bc83(ulong uParm1);
void FUN_0042bd46(ulong uParm1);
void FUN_0042be1f(long **pplParm1,long **pplParm2);
ulong FUN_0042bec1(byte bParm1);
long FUN_0042bef7(long lParm1);
void FUN_0042bfa0(undefined auParm1 [16],int *piParm2,undefined8 uParm3);
void FUN_0042c029(int *param_1);
ulong FUN_0042c0be(ulong uParm1,long lParm2);
void FUN_0042c0f2(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0042c12d(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_0042c17e(ulong uParm1,ulong uParm2);
undefined8 FUN_0042c199(uint *puParm1,ulong *puParm2);
undefined8 FUN_0042c935(char *pcParm1,ulong *puParm2,char **ppcParm3);
ulong FUN_0042dc05(void);
ulong FUN_0042dc3c(void);
void FUN_0042dc70(void);
undefined8 _DT_FINI(void);

