typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00402550(void);
void entry(void);
void FUN_004025c0(void);
void FUN_00402630(void);
void FUN_004026c0(void);
void FUN_004026f5(void);
void FUN_0040271a(undefined *puParm1);
undefined8 FUN_004028b7(undefined8 uParm1);
undefined8 FUN_004028c5(undefined8 uParm1,uint *puParm2);
ulong FUN_00402ae9(uint uParm1,undefined8 uParm2);
ulong FUN_00402d1f(long lParm1,long lParm2);
ulong FUN_004031c1(undefined8 uParm1,uint uParm2);
ulong FUN_00403291(uint uParm1);
void FUN_00403906(void);
void FUN_004039ec(long lParm1);
ulong FUN_00403ace(undefined1 *puParm1,byte bParm2,uint uParm3);
ulong * FUN_00403b56(ulong *puParm1,uint uParm2);
char * FUN_00403bf5(char *pcParm1,int iParm2);
ulong FUN_00403c95(long param_1,ulong param_2,long param_3,ulong param_4,uint param_5,uint param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_00404bb7(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_00404e27(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_00404e68(uint uParm1,undefined8 uParm2);
void FUN_00404e8c(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_00404f20(undefined8 uParm1,char cParm2);
void FUN_00404f4a(undefined8 uParm1);
void FUN_00404f69(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_00405004(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00405030(uint uParm1,undefined8 uParm2);
void FUN_00405059(undefined8 uParm1);
undefined8 * FUN_00405078(undefined8 *puParm1);
undefined8 FUN_004050d5(void);
undefined8 FUN_004050f3(void);
void FUN_00405110(void);
undefined8 FUN_0040511a(void);
undefined8 FUN_0040513b(void);
undefined8 FUN_0040515c(void);
undefined8 FUN_0040517d(void);
void FUN_0040519e(void);
undefined8 FUN_004051a9(void);
undefined8 FUN_004051cb(void);
undefined8 FUN_004051ed(void);
undefined8 FUN_0040520f(void);
undefined8 FUN_00405231(void);
void FUN_0040524f(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_004056be(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
void FUN_00405791(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00405848(undefined8 uParm1);
long FUN_00405862(long lParm1);
long FUN_00405897(long lParm1,long lParm2);
long FUN_004058f8(void);
long FUN_00405922(undefined8 uParm1,uint uParm2,undefined8 uParm3);
ulong FUN_0040598a(long lParm1,long lParm2);
undefined8 FUN_004059ec(ulong uParm1,ulong uParm2);
ulong FUN_00405a6b(uint uParm1);
void FUN_00405a94(void);
void FUN_00405ac9(uint uParm1);
void FUN_00405b3e(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_00405bc3(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_00405ca8(long *plParm1,long *plParm2);
ulong FUN_00405cfd(long lParm1,ulong uParm2);
undefined8 FUN_00405d27(long lParm1);
undefined8 FUN_00405dbb(long lParm1,undefined8 *puParm2);
void FUN_00405ec9(long lParm1,long lParm2);
void FUN_00405fd7(long lParm1);
void FUN_00406025(undefined8 uParm1);
void FUN_00406069(long lParm1,char cParm2);
long FUN_004060ad(uint uParm1,undefined8 uParm2,uint uParm3,uint *puParm4);
void FUN_0040613b(long lParm1,uint uParm2,char cParm3);
ulong FUN_004061c7(long lParm1);
ulong FUN_00406271(long lParm1,undefined8 uParm2);
long * FUN_004062f8(long *plParm1,uint uParm2,long lParm3);
void FUN_004066c0(long lParm1,long lParm2);
undefined8 FUN_0040677b(long *plParm1);
ulong FUN_00406904(ulong *puParm1,ulong uParm2);
ulong FUN_00406935(ulong *puParm1,ulong *puParm2);
undefined8 FUN_00406967(long lParm1);
undefined8 FUN_00406ade(undefined8 uParm1);
undefined8 FUN_00406b14(undefined8 uParm1);
long FUN_00406b90(long *plParm1);
undefined8 FUN_00407283(undefined8 uParm1,long lParm2,int iParm3);
ulong FUN_004072da(long *plParm1,long *plParm2);
void FUN_00407335(long lParm1,undefined4 uParm2);
long FUN_004073a6(long *plParm1,int iParm2);
undefined8 FUN_00407ce5(long lParm1,long lParm2,char cParm3);
long FUN_00407ec6(long lParm1,long lParm2,ulong uParm3);
long FUN_0040800d(long lParm1,undefined8 uParm2,long lParm3);
void FUN_004080bf(long lParm1);
undefined8 FUN_004080fc(long lParm1,long lParm2);
void FUN_004081c9(long lParm1,long lParm2);
long FUN_004082d8(long *plParm1);
ulong FUN_0040832e(long lParm1,long lParm2,uint uParm3,long lParm4);
void FUN_00408560(long lParm1,int *piParm2);
ulong FUN_00408743(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_00408d2c(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_00408dfa(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
ulong FUN_004095c3(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,uint param_6,uint param_7);
void FUN_00409653(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
void FUN_0040969d(undefined8 uParm1,undefined8 uParm2);
ulong FUN_004096c2(long lParm1,long lParm2);
undefined8 FUN_00409779(long lParm1);
ulong FUN_004097aa(uint *puParm1,byte *pbParm2,long lParm3,undefined8 uParm4);
undefined8 FUN_0040982d(long lParm1,long lParm2);
ulong FUN_00409896(uint uParm1,char *pcParm2,undefined8 uParm3);
ulong FUN_00409a94(uint uParm1,char *pcParm2,undefined8 uParm3);
ulong FUN_00409c92(uint uParm1,char *pcParm2,undefined8 uParm3);
ulong FUN_00409e90(uint uParm1,char *pcParm2,undefined8 uParm3);
void FUN_0040a08e(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040a0b3(long lParm1,long lParm2);
ulong FUN_0040a143(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040a262(byte *pbParm1,byte *pbParm2);
undefined8 FUN_0040a2da(undefined8 uParm1);
ulong FUN_0040a365(ulong uParm1);
void FUN_0040a381(long lParm1);
undefined8 FUN_0040a3a3(long *plParm1,long *plParm2);
void FUN_0040a477(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0040a586(void);
ulong FUN_0040a593(uint uParm1);
long FUN_0040a664(long *plParm1,undefined8 uParm2);
long FUN_0040a6bb(long lParm1,long lParm2);
ulong FUN_0040a74e(ulong uParm1);
ulong FUN_0040a7b9(ulong uParm1);
ulong FUN_0040a800(undefined8 uParm1,ulong uParm2);
ulong FUN_0040a837(ulong uParm1,ulong uParm2);
undefined8 FUN_0040a850(long lParm1);
ulong FUN_0040a953(ulong uParm1,long lParm2);
long * FUN_0040aa60(undefined8 uParm1,undefined1 *puParm2,code *pcParm3,code *pcParm4,long lParm5);
void FUN_0040abc5(long **pplParm1,undefined8 uParm2);
long FUN_0040acf0(long lParm1);
void FUN_0040ad3b(long lParm1,undefined8 *puParm2);
long FUN_0040ad71(long lParm1,long lParm2,long **pplParm3,char cParm4);
undefined8 FUN_0040af06(long lParm1,long **pplParm2,char cParm3);
undefined8 FUN_0040b0d6(long *plParm1,undefined8 uParm2);
undefined8 FUN_0040b2d7(long lParm1,long lParm2,long *plParm3);
undefined8 FUN_0040b5f1(undefined8 uParm1,undefined8 uParm2);
long FUN_0040b63a(long lParm1,undefined8 uParm2);
void FUN_0040b8e9(long lParm1,undefined4 uParm2);
ulong FUN_0040b942(long lParm1);
ulong FUN_0040b954(long lParm1,undefined4 uParm2);
ulong FUN_0040b9dc(long lParm1);
undefined * FUN_0040ba5e(void);
char * FUN_0040be23(void);
void FUN_0040bef1(undefined auParm1 [16],undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint uParm9);
void FUN_0040bf1d(undefined auParm1 [16],undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint uParm9);
void FUN_0040bf49(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,undefined8 param_2,uint param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0040c05a(int *piParm1);
void FUN_0040c0cf(uint *puParm1);
void FUN_0040c106(uint *puParm1);
ulong FUN_0040c13c(uint uParm1);
ulong FUN_0040c14c(uint uParm1);
void FUN_0040c199(undefined4 *puParm1);
void FUN_0040c1ae(uint *puParm1);
void FUN_0040c1c9(uint *puParm1);
undefined8 FUN_0040c21b(uint *puParm1,undefined8 uParm2);
long FUN_0040c275(long lParm1);
ulong FUN_0040c2a3(char *pcParm1);
ulong FUN_0040c58e(undefined8 uParm1);
ulong FUN_0040c643(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0040c8fc(uint uParm1);
void FUN_0040c95f(undefined8 uParm1);
void FUN_0040c984(void);
ulong FUN_0040c993(long lParm1);
undefined8 FUN_0040ca63(undefined8 uParm1);
void FUN_0040ca82(undefined8 uParm1,undefined8 uParm2,uint uParm3);
void FUN_0040caad(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
void FUN_0040cada(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
ulong FUN_0040cb17(uint uParm1,long lParm2,long lParm3,uint uParm4);
long FUN_0040cc2a(long lParm1,undefined *puParm2);
ulong * FUN_0040d1c8(ulong *puParm1,char cParm2,ulong uParm3);
void FUN_0040d2db(uint uParm1,undefined8 uParm2,uint uParm3,uint uParm4);
ulong FUN_0040d30b(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,undefined8 param_2,uint param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined * FUN_0040d4d2(undefined *puParm1,uint uParm2,char *pcParm3);
ulong FUN_0040d610(void);
long FUN_0040d661(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
int * FUN_0040d8aa(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_0040e38c(ulong uParm1,long lParm2,long lParm3);
long FUN_0040e5ff(int *param_1,long *param_2);
long FUN_0040e7d6(undefined auParm1 [16],int *piParm2,long *plParm3);
undefined8 FUN_0040eb51(int iParm1,long lParm2,uint *puParm3,long lParm4,uint uParm5);
void FUN_0040f354(uint param_1);
void FUN_0040f39e(undefined auParm1 [16],uint uParm2);
ulong FUN_0040f3ec(void);
ulong FUN_0040f72e(undefined auParm1 [16],undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040fab8(char *pcParm1,long lParm2);
undefined8 FUN_0040fb11(long param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
ulong FUN_0041726d(ulong uParm1,undefined4 uParm2);
ulong FUN_004172a4(uint uParm1);
undefined8 FUN_004172c3(uint uParm1,char cParm2);
void FUN_0041733e(undefined auParm1 [16],int *piParm2,undefined8 uParm3);
void FUN_004173c7(int *param_1);
void FUN_0041745c(uint uParm1);
ulong FUN_00417482(ulong uParm1,long lParm2);
void FUN_004174b6(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004174f1(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00417542(ulong uParm1,ulong uParm2);
undefined8 FUN_0041755d(uint *puParm1,ulong *puParm2);
undefined8 FUN_00417cf9(char *pcParm1,ulong *puParm2,char **ppcParm3);
ulong FUN_00418fc9(void);
ulong FUN_00419000(void);
void FUN_00419030(void);
undefined8 _DT_FINI(void);

