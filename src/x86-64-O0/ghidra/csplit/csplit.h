typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void entry(void);
void FUN_00402670(void);
void FUN_004026e0(void);
void FUN_00402770(void);
void FUN_004027a5(void);
void FUN_004027ca(undefined *puParm1);
void FUN_00402967(void);
void FUN_004029ad(void);
void FUN_004029c0(void);
void FUN_00402a18(undefined8 uParm1,undefined8 uParm2);
long FUN_00402a50(undefined8 uParm1,long lParm2);
void FUN_00402ac4(undefined8 *puParm1);
long FUN_00402af2(void);
void FUN_00402b29(long lParm1,long lParm2,long lParm3);
long FUN_00402c22(long lParm1);
long * FUN_00402d67(long lParm1);
long FUN_00402dc9(ulong uParm1);
void FUN_00402e64(long lParm1);
void FUN_00402ed3(long lParm1);
ulong FUN_00402f45(void);
undefined8 FUN_004030ee(void);
long * FUN_00403134(void);
long FUN_00403287(ulong uParm1);
ulong FUN_004033a2(void);
void FUN_004033c1(undefined8 uParm1);
void FUN_0040343e(ulong uParm1,char cParm2,int iParm3);
void FUN_00403535(void);
void FUN_0040355f(long lParm1,long lParm2);
void FUN_00403602(long lParm1,long lParm2);
void FUN_004036e4(long lParm1,long lParm2,char cParm3);
void FUN_004037a4(long *plParm1,undefined8 uParm2);
void FUN_00403a78(void);
long FUN_00403be5(uint uParm1);
void FUN_00403c96(void);
void FUN_00403d9c(char cParm1);
void FUN_00403e4a(void);
void FUN_0040403c(long *plParm1);
undefined8 * FUN_004040e4(void);
void FUN_00404185(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004041eb(int iParm1,long lParm2,long lParm3);
long FUN_004042fa(undefined4 uParm1,undefined uParm2,char *pcParm3);
void FUN_00404468(int iParm1,uint uParm2,long lParm3);
long FUN_0040470f(long lParm1,uint *puParm2);
void FUN_0040477e(byte *pbParm1,uint uParm2);
long FUN_0040488a(char *pcParm1);
undefined8 FUN_004049c0(uint uParm1,undefined8 *puParm2);
void FUN_00404eb9(uint uParm1);
ulong FUN_004050f7(uint uParm1,undefined8 uParm2,uint uParm3,uint uParm4);
long FUN_00405170(undefined8 uParm1,undefined8 uParm2);
char * FUN_00405256(ulong uParm1,long lParm2);
void FUN_004052db(long lParm1);
ulong FUN_004053bd(undefined1 *puParm1,byte bParm2,uint uParm3);
ulong * FUN_00405445(ulong *puParm1,uint uParm2);
char * FUN_004054e4(char *pcParm1,int iParm2);
ulong FUN_00405584(long param_1,ulong param_2,long param_3,ulong param_4,uint param_5,uint param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_004064a6(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_00406716(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_00406757(uint uParm1,undefined8 uParm2);
void FUN_0040677b(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_0040680f(undefined8 uParm1,char cParm2);
void FUN_00406839(undefined8 uParm1);
void FUN_00406858(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_004068f3(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040691f(uint uParm1,undefined8 uParm2);
void FUN_00406948(undefined8 uParm1);
long FUN_00406967(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004069d7(uint uParm1);
void FUN_004069fd(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_00406e6c(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
void FUN_00406f3f(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00406ff6(long lParm1,ulong *puParm2,ulong uParm3);
void FUN_004070d0(undefined8 uParm1);
long FUN_004070ea(long lParm1);
long FUN_0040711f(long lParm1,long lParm2);
void FUN_00407180(undefined8 uParm1,undefined8 uParm2);
long FUN_004071b4(undefined8 param_1,uint param_2,long param_3,long param_4,undefined8 param_5,undefined8 param_6,uint param_7);
void FUN_004072df(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,uint uParm6);
undefined8 FUN_00407334(long *plParm1,int iParm2);
ulong FUN_004073d2(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_00407413(char *pcParm1,char **ppcParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_004077ba(int iParm1);
ulong FUN_004077e0(ulong *puParm1,int iParm2);
ulong FUN_0040783f(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_00407880(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_00407c69(ulong uParm1,ulong uParm2);
ulong FUN_00407ce8(uint uParm1);
void FUN_00407d11(void);
void FUN_00407d46(uint uParm1);
void FUN_00407dbb(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_00407e40(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_00407f25(undefined8 uParm1);
ulong FUN_00407fda(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00408293(undefined8 uParm1);
void FUN_004082b8(void);
ulong FUN_004082c7(long lParm1);
undefined8 FUN_00408397(undefined8 uParm1);
void FUN_004083b6(undefined8 uParm1,undefined8 uParm2,uint uParm3);
void FUN_004083e1(long lParm1,int *piParm2);
ulong FUN_004085c4(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_00408bad(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_00408c7b(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
ulong FUN_00409444(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,uint param_6,uint param_7);
void FUN_004094d4(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
undefined8 FUN_0040951e(long lParm1);
ulong FUN_0040954f(uint *puParm1,byte *pbParm2,long lParm3,undefined8 uParm4);
ulong * FUN_004095d2(ulong *puParm1,char cParm2,ulong uParm3);
undefined8 FUN_004096e5(long lParm1,long lParm2);
undefined8 FUN_0040974e(int iParm1);
void FUN_00409774(long lParm1,long lParm2);
void FUN_004097e3(long lParm1,long lParm2);
ulong FUN_0040985b(long lParm1,long lParm2);
void FUN_004098b2(undefined8 uParm1);
void FUN_004098d7(undefined8 uParm1);
void FUN_004098fc(undefined8 uParm1,undefined8 uParm2);
void FUN_00409927(long lParm1);
void FUN_00409977(long lParm1,long lParm2);
void FUN_004099e2(long lParm1,long lParm2);
ulong FUN_00409a4d(long lParm1,long lParm2);
ulong FUN_00409ac1(long lParm1,long lParm2);
undefined8 FUN_00409b0a(void);
ulong FUN_00409b1d(long param_1,undefined8 param_2,long param_3,long param_4,undefined8 param_5,byte param_6,long param_7);
ulong FUN_00409c66(long lParm1,undefined8 uParm2,long lParm3,long lParm4,byte bParm5,long lParm6);
undefined8 FUN_00409e41(long lParm1,ulong uParm2);
void FUN_00409fb8(undefined8 uParm1,undefined8 uParm2,undefined8 *puParm3,long lParm4,char cParm5,long lParm6);
void FUN_0040a0a9(long *plParm1);
undefined8 FUN_0040a3d6(long *plParm1);
long FUN_0040ae78(long *plParm1,long lParm2,uint *puParm3);
void FUN_0040afab(long *plParm1);
void FUN_0040b07a(long *plParm1);
ulong FUN_0040b11c(byte **ppbParm1,byte *pbParm2,uint uParm3);
ulong FUN_0040bde1(long *plParm1,long lParm2);
ulong FUN_0040bf63(long *plParm1);
void FUN_0040c0ed(long lParm1);
ulong FUN_0040c13b(long lParm1,ulong uParm2,uint uParm3);
undefined8 FUN_0040c2c1(long *plParm1,long lParm2);
undefined8 FUN_0040c32b(undefined8 *puParm1,undefined8 uParm2);
undefined8 FUN_0040c3b2(undefined8 *puParm1,long lParm2,long lParm3);
undefined8 FUN_0040c48d(long *plParm1,long lParm2);
undefined8 FUN_0040c56a(long *plParm1,long lParm2,long lParm3);
undefined8 FUN_0040c953(long *plParm1,long lParm2,long lParm3);
undefined8 FUN_0040cc5b(long *plParm1,long lParm2);
ulong FUN_0040d01d(long *plParm1,long lParm2);
undefined8 FUN_0040d20a(long *plParm1,undefined8 uParm2);
undefined8 FUN_0040d2bf(long lParm1,long lParm2);
long FUN_0040d34e(long lParm1,long lParm2);
void FUN_0040d406(long lParm1,long lParm2);
long FUN_0040d486(long *plParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_0040d821(long lParm1,uint uParm2);
ulong * FUN_0040d87b(undefined4 *puParm1,long lParm2,long lParm3);
ulong * FUN_0040d99c(undefined4 *puParm1,long lParm2,long lParm3,uint uParm4);
undefined8 FUN_0040dac9(long *plParm1,ulong *puParm2,ulong uParm3);
void FUN_0040dc7d(long lParm1);
long FUN_0040dd21(long *plParm1,long lParm2,undefined8 uParm3);
long FUN_0040def3(long *plParm1,long lParm2,uint uParm3,undefined8 uParm4);
undefined1 * FUN_0040e1d7(undefined8 uParm1,undefined8 uParm2,long lParm3);
undefined8 FUN_0040e263(long *plParm1);
void FUN_0040e35c(long **pplParm1,long lParm2,long lParm3);
void FUN_0040ea3e(long *plParm1,undefined8 uParm2);
ulong FUN_0040eca1(long *plParm1,undefined8 uParm2,undefined8 uParm3,ulong uParm4);
undefined8 FUN_0040f01d(long *plParm1,ulong uParm2);
void FUN_0040f39a(long lParm1);
void FUN_0040f51d(long *plParm1);
ulong FUN_0040f5ad(long *plParm1);
void FUN_0040f8f5(long *plParm1);
ulong FUN_0040fb6e(long *plParm1);
ulong FUN_0040fef3(long **pplParm1,code *pcParm2,undefined8 uParm3);
ulong FUN_0040ffd1(long *plParm1,code *pcParm2,undefined8 uParm3);
undefined8 FUN_0041008c(long lParm1,long lParm2);
ulong FUN_00410204(undefined8 uParm1,long lParm2);
long FUN_004102e0(undefined4 *puParm1,long *plParm2,long lParm3);
undefined8 FUN_004104d1(long *plParm1,long lParm2);
undefined8 FUN_004105bf(undefined8 uParm1,long lParm2);
ulong FUN_0041066c(long lParm1,long lParm2);
ulong FUN_004108e1(long *plParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
long FUN_00410e11(long *plParm1,long lParm2,uint uParm3);
long FUN_00410eaa(long *plParm1,long lParm2,undefined4 uParm3);
undefined8 FUN_00410fdc(long lParm1);
ulong FUN_0041111a(long lParm1);
ulong FUN_004111fd(undefined8 *puParm1,long *plParm2,long lParm3,char cParm4);
void FUN_004115cf(undefined8 uParm1,long lParm2,undefined8 uParm3);
undefined8 FUN_00411612(long *plParm1,long lParm2,ulong uParm3);
ulong FUN_00411ea3(byte *pbParm1,long lParm2,ulong uParm3);
long FUN_004120c6(undefined8 uParm1,long *plParm2,ulong uParm3,int *piParm4);
long FUN_004121f2(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_004123f4(undefined8 uParm1,undefined8 *puParm2,long lParm3,undefined8 uParm4,long lParm5,int *piParm6);
long FUN_004125b3(long lParm1,long *plParm2,undefined8 *puParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_00412e58(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,undefined8 uParm5,int *piParm6);
long FUN_00412fec(long lParm1,long lParm2,undefined8 uParm3,undefined8 *puParm4,ulong uParm5,undefined4 *puParm6);
ulong FUN_0041351d(byte bParm1,long lParm2);
undefined8 FUN_00413554(ulong uParm1,undefined8 uParm2,long lParm3,long *plParm4,int *piParm5,int *piParm6);
undefined8 FUN_00413915(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,byte *pbParm4);
long FUN_00413974(long lParm1,long lParm2,long lParm3,ulong uParm4,int *piParm5);
undefined8 FUN_004142a9(undefined4 *param_1,long param_2,undefined *param_3,int param_4,undefined8 param_5,undefined8 param_6,char param_7);
undefined8 FUN_004143f4(undefined4 *puParm1,long lParm2,char *pcParm3);
undefined8 FUN_00414560(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,byte *pbParm4);
undefined8 FUN_004145ba(long lParm1,undefined8 uParm2,long lParm3,long *plParm4,undefined *puParm5,ulong uParm6);
long FUN_00414f21(long lParm1,undefined8 uParm2,undefined8 uParm3,char *pcParm4,byte bParm5,int *piParm6);
long FUN_004151b9(undefined8 uParm1,byte *pbParm2,undefined8 uParm3);
void FUN_004152a0(undefined8 *puParm1);
void FUN_004152da(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined uParm4);
undefined8 * FUN_00415311(long lParm1,undefined8 *puParm2,undefined8 *puParm3,undefined8 *puParm4);
undefined8 FUN_0041545d(long lParm1,long lParm2);
void FUN_004154a0(undefined8 *puParm1);
undefined8 FUN_00415505(undefined8 uParm1,long lParm2);
long * FUN_0041552c(long **pplParm1,undefined8 uParm2);
void FUN_00415658(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_004156a9(long param_1,undefined8 param_2,ulong param_3,ulong param_4,ulong param_5,undefined8 param_6,ulong *param_7,char param_8);
ulong FUN_00415a1e(ulong *puParm1,long lParm2,ulong uParm3,int iParm4);
ulong FUN_00415cc4(long *param_1,long param_2,long param_3,long param_4,long param_5,undefined8 param_6,ulong param_7,undefined8 *param_8,uint param_9);
ulong FUN_00416a85(long lParm1);
long FUN_00416daa(long lParm1,char cParm2,long *plParm3);
undefined8 FUN_004172c6(long *plParm1,long lParm2,uint uParm3);
undefined8 FUN_0041738d(long lParm1,long lParm2,undefined8 uParm3);
long FUN_00417430(long param_1,undefined8 param_2,long param_3,long *param_4,long param_5,long param_6,long param_7);
ulong FUN_0041796f(long *plParm1,undefined8 uParm2,undefined8 uParm3,long lParm4,undefined8 uParm5,undefined8 uParm6);
undefined8 FUN_00417b3f(long *plParm1,undefined8 *puParm2,long lParm3,undefined8 uParm4,undefined8 *puParm5);
undefined8 FUN_00417c8d(long *plParm1,long lParm2,ulong uParm3,long *plParm4,char cParm5);
undefined8 FUN_0041809d(long *plParm1);
void FUN_00418133(long *plParm1,long lParm2,long lParm3,long lParm4,long lParm5,long lParm6);
ulong FUN_004182d7(long lParm1,long *plParm2);
undefined8 FUN_00418478(long lParm1,long *plParm2,long lParm3,undefined8 uParm4);
ulong FUN_00418683(long lParm1,long lParm2);
ulong FUN_0041876d(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_004188c7(long lParm1,long *plParm2,long lParm3,long lParm4);
ulong FUN_00418aa6(long lParm1,long lParm2,undefined8 uParm3);
ulong FUN_00418bd5(long *plParm1,long lParm2,undefined8 uParm3,undefined8 uParm4);
undefined8 FUN_00418e63(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_00418fcf(long lParm1,uint uParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_0041924e(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long lParm5,undefined8 uParm6);
ulong FUN_0041931b(long *plParm1,long lParm2,undefined8 uParm3,long lParm4,long lParm5,long lParm6);
ulong FUN_00419694(long lParm1,long *plParm2,long lParm3,long lParm4);
ulong FUN_00419b69(long lParm1,long *plParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_00419c43(int *piParm1,long lParm2,long lParm3);
long FUN_00419db9(int *piParm1,long lParm2,long lParm3);
long FUN_0041a03f(int *piParm1,long lParm2);
ulong FUN_0041a0e9(long lParm1,long lParm2,undefined8 uParm3);
ulong FUN_0041a1e4(long lParm1,long lParm2);
ulong FUN_0041a566(long lParm1,long lParm2);
ulong FUN_0041aad2(long lParm1,long lParm2,long lParm3);
ulong FUN_0041b049(undefined8 uParm1,long *plParm2,undefined8 *puParm3,undefined8 uParm4,long lParm5);
long FUN_0041b120(long *plParm1,long lParm2,long lParm3,uint uParm4);
ulong FUN_0041b1ac(long param_1,long *param_2,long param_3,long param_4,undefined8 param_5,long param_6,uint param_7);
ulong FUN_0041b934(long lParm1,long lParm2,long lParm3,undefined8 uParm4);
ulong FUN_0041bbf7(long lParm1,undefined8 *puParm2,undefined8 uParm3,uint uParm4);
ulong FUN_0041bd6b(long *plParm1,undefined8 uParm2,long lParm3,long lParm4,uint uParm5);
ulong FUN_0041bf30(long lParm1,undefined8 uParm2,long lParm3,undefined8 uParm4,uint uParm5);
undefined8 FUN_0041c310(long lParm1,long lParm2);
long FUN_0041cd38(long *plParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_0041d5de(long *plParm1,long lParm2,long lParm3,long lParm4);
undefined8 FUN_0041da1a(long lParm1,undefined8 *puParm2,long lParm3);
ulong FUN_0041dbf5(long lParm1,int iParm2);
undefined8 FUN_0041dd7d(long lParm1,undefined4 uParm2,ulong uParm3);
void FUN_0041debb(long lParm1);
void FUN_0041dfcc(long lParm1);
undefined8 FUN_0041e00d(long lParm1,undefined8 uParm2,long lParm3,long lParm4,long lParm5);
long FUN_0041e32d(long lParm1,long lParm2);
undefined8 FUN_0041e403(long lParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 * FUN_0041e57b(long lParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0041e687(undefined8 *puParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
ulong FUN_0041e6ef(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,long param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_0041e881(long lParm1);
ulong FUN_0041e9ed(void);
long FUN_0041ea3e(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
int * FUN_0041ec87(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_0041f769(ulong uParm1,long lParm2,long lParm3);
long FUN_0041f9dc(int *param_1,long *param_2);
long FUN_0041fbb3(undefined auParm1 [16],int *piParm2,long *plParm3);
undefined8 FUN_0041ff2e(int iParm1,long lParm2,uint *puParm3,long lParm4,uint uParm5);
void FUN_00420731(uint param_1);
void FUN_0042077b(undefined auParm1 [16],uint uParm2);
ulong FUN_004207c9(void);
ulong FUN_00420b0b(undefined auParm1 [16],undefined8 uParm2,undefined8 uParm3);
ulong FUN_00420e95(char *pcParm1,long lParm2);
undefined8 FUN_00420eee(long param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
ulong FUN_0042864a(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00428769(byte *pbParm1,byte *pbParm2);
undefined8 FUN_004287e1(undefined8 uParm1);
undefined8 FUN_0042886c(void);
ulong FUN_00428879(uint uParm1);
undefined * FUN_0042894a(void);
char * FUN_00428d0f(void);
void FUN_00428ddd(undefined auParm1 [16],int *piParm2,undefined8 uParm3);
void FUN_00428e66(int *param_1);
ulong FUN_00428efb(ulong uParm1,long lParm2);
void FUN_00428f2f(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00428f6a(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00428fbb(ulong uParm1,ulong uParm2);
void FUN_00428fd6(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00428ffb(long lParm1,long lParm2);
undefined8 FUN_004290b2(uint *puParm1,ulong *puParm2);
undefined8 FUN_0042984e(char *pcParm1,ulong *puParm2,char **ppcParm3);
ulong FUN_0042ab1e(void);
ulong FUN_0042ab55(void);
void FUN_0042ab80(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0042aba5(long lParm1,long lParm2);
ulong FUN_0042ac35(uint uParm1);
ulong FUN_0042ac54(uint uParm1);
void FUN_0042ac70(void);
undefined8 _DT_FINI(void);

