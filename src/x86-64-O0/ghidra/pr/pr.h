typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_004021b0(void);
void FUN_004023f0(void);
void entry(void);
void FUN_004025d0(void);
void FUN_00402640(void);
void FUN_004026d0(void);
ulong FUN_00402705(byte bParm1);
void FUN_00402714(void);
void FUN_00402739(void);
void FUN_0040275e(undefined *puParm1);
void FUN_004028fb(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00402920(void);
ulong FUN_0040293d(void);
undefined8 FUN_004029b1(uint uParm1,char cParm2,char *pcParm3);
void FUN_00402ae4(undefined8 uParm1);
void FUN_00402b15(undefined *puParm1);
ulong FUN_00402b59(uint uParm1,undefined8 *puParm2);
void FUN_004033c4(undefined8 uParm1,int iParm2,undefined4 *puParm3,undefined8 uParm4);
void FUN_00403417(char *pcParm1,char cParm2,char *pcParm3,undefined4 *puParm4);
void FUN_00403511(int iParm1);
undefined8 FUN_004037ed(int iParm1,undefined8 *puParm2);
void FUN_00403a08(void);
undefined8 FUN_00403be0(long lParm1,long *plParm2);
void FUN_00403cf0(undefined8 *puParm1);
void FUN_00403e45(long lParm1);
void FUN_00403ecd(void);
void FUN_00403f4f(uint uParm1,undefined8 uParm2);
void FUN_00403ff1(undefined *puParm1,uint uParm2);
void FUN_00404230(void);
void FUN_0040430d(long lParm1,undefined8 uParm2);
undefined8 FUN_00404382(undefined8 uParm1,undefined8 uParm2);
void FUN_004046b6(void);
void FUN_00404801(void);
void FUN_004049a7(int iParm1);
void FUN_00404a21(undefined uParm1);
void FUN_00404a82(long lParm1);
void FUN_00404bf0(int iParm1);
void FUN_00404c46(int iParm1);
void FUN_00404c87(undefined8 *puParm1);
void FUN_00404d19(undefined8 *puParm1,int iParm2);
void FUN_00404ea1(void);
void FUN_00404f3a(void);
void FUN_0040500c(long lParm1,int iParm2,char *pcParm3);
void FUN_00405050(char cParm1);
ulong FUN_004050d7(ulong uParm1);
void FUN_00405240(void);
undefined8 FUN_00405356(undefined8 *puParm1);
undefined8 FUN_00405727(long lParm1,undefined8 uParm2);
ulong FUN_004058e5(byte bParm1);
void FUN_00405b6f(void);
void FUN_00405bd0(uint uParm1);
void FUN_00405f03(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
void FUN_00405f34(long lParm1,uint uParm2);
long FUN_00405f6f(undefined8 uParm1,undefined8 uParm2);
void FUN_00406055(undefined8 *puParm1);
ulong FUN_004060aa(uint uParm1);
char * FUN_0040617b(long lParm1,long lParm2);
void FUN_004062ad(undefined8 uParm1,uint uParm2);
ulong FUN_004062df(byte *pbParm1,long lParm2,uint uParm3);
long FUN_00406514(long lParm1,long lParm2,long lParm3);
long FUN_0040656a(long lParm1,long lParm2,long lParm3);
ulong FUN_004065c0(int iParm1,int iParm2);
void FUN_00406614(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,uint uParm6);
char * FUN_0040666a(char *pcParm1,long lParm2,char *pcParm3,undefined8 *puParm4,undefined uParm5,undefined8 uParm6,undefined8 uParm7,uint uParm8);
void FUN_00408832(long lParm1);
ulong FUN_00408914(undefined1 *puParm1,byte bParm2,uint uParm3);
ulong * FUN_0040899c(ulong *puParm1,uint uParm2);
char * FUN_00408a3b(char *pcParm1,int iParm2);
ulong FUN_00408adb(long param_1,ulong param_2,long param_3,ulong param_4,uint param_5,uint param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_004099fd(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_00409c6d(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_00409d01(undefined8 uParm1,char cParm2);
void FUN_00409d2b(undefined8 uParm1);
void FUN_00409d4a(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_00409de5(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00409e11(uint uParm1,undefined8 uParm2);
void FUN_00409e3a(undefined8 uParm1);
void FUN_00409e59(long lParm1);
void FUN_00409e6f(uint uParm1);
void FUN_00409e95(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_0040a304(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
void FUN_0040a3d7(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_0040a48e(ulong uParm1,ulong uParm2);
void FUN_0040a4de(long lParm1,ulong *puParm2,ulong uParm3);
void FUN_0040a5b8(undefined8 uParm1);
long FUN_0040a5d2(long lParm1);
long FUN_0040a607(long lParm1,long lParm2);
void FUN_0040a668(undefined8 uParm1,undefined8 uParm2);
long FUN_0040a692(void);
long FUN_0040a6bc(undefined8 param_1,uint param_2,long param_3,long param_4,undefined8 param_5,undefined8 param_6,uint param_7);
void FUN_0040a7e7(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,uint uParm6);
undefined8 FUN_0040a83c(long *plParm1,int iParm2);
ulong FUN_0040a8da(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_0040a91b(char *pcParm1,char **ppcParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_0040acc2(long *plParm1,int iParm2);
ulong FUN_0040ad60(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_0040ada1(char *pcParm1,char **ppcParm2,uint uParm3,undefined8 *puParm4,long lParm5);
void FUN_0040b148(uint uParm1,int iParm2,undefined uParm3,long lParm4,undefined8 uParm5,uint uParm6);
undefined8 FUN_0040b222(uint uParm1,uint uParm2,char cParm3,undefined8 uParm4,undefined8 uParm5);
undefined8 FUN_0040b268(int iParm1);
ulong FUN_0040b28e(ulong *puParm1,int iParm2);
ulong FUN_0040b2ed(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_0040b32e(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
ulong FUN_0040b717(uint uParm1);
void FUN_0040b740(void);
void FUN_0040b775(uint uParm1);
void FUN_0040b7ea(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_0040b86f(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_0040b954(undefined8 uParm1);
ulong FUN_0040ba09(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_0040bcc2(undefined8 uParm1);
void FUN_0040bce7(void);
ulong FUN_0040bcf6(long lParm1);
undefined8 FUN_0040bdc6(undefined8 uParm1);
void FUN_0040bde5(undefined8 uParm1,undefined8 uParm2,uint uParm3);
void FUN_0040be10(long lParm1,int *piParm2);
ulong FUN_0040bff3(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_0040c5dc(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_0040c6aa(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
ulong FUN_0040ce73(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,uint param_6,uint param_7);
void FUN_0040cf03(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
ulong FUN_0040cf4d(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_0040cffa(long lParm1);
ulong FUN_0040d02b(uint *puParm1,byte *pbParm2,long lParm3,undefined8 uParm4);
undefined8 FUN_0040d0ae(long lParm1,long lParm2);
ulong FUN_0040d117(int iParm1,int iParm2);
ulong FUN_0040d152(uint *puParm1,uint *puParm2);
void FUN_0040d1fa(long lParm1,undefined8 uParm2,long lParm3);
undefined8 * FUN_0040d236(long lParm1);
undefined8 FUN_0040d2e6(long **pplParm1,char *pcParm2);
void FUN_0040d4b3(undefined8 *puParm1);
void FUN_0040d4f5(void);
void FUN_0040d505(long lParm1);
ulong FUN_0040d53c(long lParm1);
long FUN_0040d582(long lParm1);
ulong FUN_0040d64b(long lParm1);
undefined8 FUN_0040d6b3(long lParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_0040d75f(long lParm1,undefined8 uParm2);
void FUN_0040d834(long lParm1);
void FUN_0040d863(void);
ulong FUN_0040d8f5(char *pcParm1);
ulong FUN_0040d967(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040da86(uint uParm1);
ulong FUN_0040dace(byte *pbParm1,byte *pbParm2);
undefined8 FUN_0040db46(undefined8 uParm1);
undefined8 FUN_0040dbd1(void);
undefined * FUN_0040dbde(void);
char * FUN_0040dfa3(void);
undefined8 FUN_0040e071(undefined8 uParm1);
undefined8 FUN_0040e137(uint uParm1,undefined8 uParm2);
long FUN_0040e352(long lParm1,undefined4 uParm2);
ulong FUN_0040e3cb(ulong uParm1);
ulong FUN_0040e476(int iParm1,int iParm2);
long FUN_0040e4b1(ulong param_1,long param_2,int param_3,int param_4,int param_5,uint param_6,int param_7,int param_8,int param_9,int param_10);
long FUN_0040e71e(undefined8 uParm1,undefined8 uParm2);
long FUN_0040e76d(undefined8 param_1,undefined8 param_2,uint param_3,uint param_4,uint param_5,long param_6,uint *param_7);
void FUN_0040e896(code *pcParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_0040e8c8(undefined8 uParm1,long *plParm2,undefined8 uParm3);
long FUN_0040ea09(undefined8 *puParm1,undefined8 uParm2,long *plParm3);
void FUN_0040efe3(undefined8 uParm1);
ulong FUN_0040f00c(char *pcParm1,char *pcParm2,uint uParm3);
ulong FUN_0040f19c(void);
long FUN_0040f1ed(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
int * FUN_0040f436(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_0040ff18(ulong uParm1,long lParm2,long lParm3);
long FUN_0041018b(int *param_1,long *param_2);
long FUN_00410362(undefined auParm1 [16],int *piParm2,long *plParm3);
undefined8 FUN_004106dd(int iParm1,long lParm2,uint *puParm3,long lParm4,uint uParm5);
void FUN_00410ee0(uint param_1);
void FUN_00410f2a(undefined auParm1 [16],uint uParm2);
ulong FUN_00410f78(void);
ulong FUN_004112ba(undefined auParm1 [16],undefined8 uParm2,undefined8 uParm3);
ulong FUN_00411644(char *pcParm1,long lParm2);
undefined8 FUN_0041169d(long param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
char * FUN_00411960(char *pcParm1,char **ppcParm2,long lParm3,undefined8 uParm4);
ulong FUN_00418df9(uint uParm1);
undefined8 * FUN_00418e18(ulong uParm1);
void FUN_00418edb(ulong uParm1);
void FUN_00418fb4(undefined auParm1 [16],int *piParm2,undefined8 uParm3);
void FUN_0041903d(int *param_1);
ulong FUN_004190d2(ulong uParm1,long lParm2);
void FUN_00419106(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00419141(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00419192(ulong uParm1,ulong uParm2);
undefined8 FUN_004191ad(uint *puParm1,ulong *puParm2);
undefined8 FUN_00419949(char *pcParm1,ulong *puParm2,char **ppcParm3);
ulong FUN_0041ac19(void);
ulong FUN_0041ac50(void);
void FUN_0041ac80(void);
undefined8 _DT_FINI(void);

