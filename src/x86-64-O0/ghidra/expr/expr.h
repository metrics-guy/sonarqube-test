typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void entry(void);
void FUN_004025f0(void);
void FUN_00402660(void);
void FUN_004026f0(void);
void FUN_00402725(int iParm1);
void FUN_0040273e(undefined *puParm1);
void FUN_004028db(void);
void FUN_004028e6(undefined8 *puParm1,undefined8 uParm2);
undefined8 FUN_00402900(undefined8 uParm1,undefined8 uParm2,uint uParm3);
void FUN_00402943(long *plParm1,long *plParm2,long *plParm3);
void FUN_004029ad(long *plParm1,long *plParm2,long *plParm3);
void FUN_00402a14(long *plParm1,long *plParm2,long *plParm3);
void FUN_00402aa8(long *plParm1,long *plParm2,long *plParm3);
void FUN_00402b0a(long *plParm1,long *plParm2,long *plParm3);
void FUN_00402b62(undefined8 uParm1,undefined8 uParm2,undefined8 *puParm3);
ulong FUN_00402b95(long *plParm1);
ulong FUN_00402bc2(ulong *puParm1);
undefined8 FUN_00402bdd(undefined8 *puParm1);
ulong FUN_00402bee(undefined8 uParm1,undefined8 uParm2,undefined8 *puParm3);
long FUN_00402c34(char *pcParm1,char *pcParm2);
undefined * FUN_00402e5d(long lParm1,ulong uParm2,ulong uParm3);
long FUN_0040301d(long lParm1,ulong uParm2);
ulong FUN_004030c6(int iParm1);
ulong FUN_00403266(char cParm1);
undefined4 * FUN_004033e9(undefined8 uParm1);
undefined4 * FUN_0040342a(undefined8 uParm1);
void FUN_0040346b(int *piParm1);
void FUN_004034b3(int *piParm1);
ulong FUN_00403512(int *piParm1);
undefined8 FUN_004035a7(char *pcParm1);
void FUN_004035f2(int *piParm1);
undefined8 FUN_0040365e(int *piParm1);
long FUN_00403712(undefined8 uParm1);
ulong FUN_0040376d(undefined8 uParm1);
ulong FUN_004037cf(void);
void FUN_004037e5(void);
undefined8 FUN_00403831(long lParm1,long lParm2);
undefined8 FUN_00403a9f(byte bParm1);
undefined8 FUN_00403bac(byte bParm1);
undefined8 FUN_00403e6f(byte bParm1);
long FUN_00403eed(byte bParm1);
long FUN_00404031(byte bParm1);
long FUN_00404120(byte bParm1);
undefined8 FUN_004043ba(byte bParm1);
undefined8 FUN_00404484(byte bParm1);
void FUN_0040454f(void);
char * FUN_00404635(long lParm1,long lParm2);
void FUN_00404767(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,int param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,code *param_6);
byte * FUN_004048bc(byte *pbParm1,uint uParm2);
long FUN_0040498c(long lParm1);
void FUN_00404a35(char *pcParm1);
void FUN_00404c18(long lParm1);
ulong FUN_00404cfa(undefined1 *puParm1,byte bParm2,uint uParm3);
ulong * FUN_00404d82(ulong *puParm1,uint uParm2);
char * FUN_00404e21(char *pcParm1,int iParm2);
ulong FUN_00404ec1(long param_1,ulong param_2,long param_3,ulong param_4,uint param_5,uint param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_00405de3(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_00406053(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_00406094(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_00406128(undefined8 uParm1,char cParm2);
void FUN_00406152(undefined8 uParm1);
long FUN_00406171(long lParm1,long lParm2);
ulong FUN_004061b5(char *pcParm1,char *pcParm2,char cParm3);
ulong FUN_0040631a(byte *pbParm1,byte *pbParm2,uint uParm3,uint uParm4);
void FUN_00406784(undefined8 uParm1,undefined8 uParm2);
void FUN_004067b0(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_00406c1f(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
void FUN_00406cf2(undefined8 uParm1);
long FUN_00406d0c(long lParm1);
long FUN_00406d41(long lParm1,long lParm2);
void FUN_00406da2(undefined8 uParm1,undefined8 uParm2);
void FUN_00406dd6(undefined8 uParm1);
undefined8 FUN_00406e03(void);
undefined8 FUN_00406e2d(long *plParm1,int iParm2);
ulong FUN_00406ecb(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_00406f0c(char *pcParm1,char **ppcParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_004072b3(ulong uParm1,ulong uParm2);
ulong FUN_00407332(uint uParm1);
void FUN_0040735b(void);
void FUN_00407390(uint uParm1);
void FUN_00407405(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_0040748a(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_0040756f(long lParm1,int *piParm2);
ulong FUN_00407752(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_00407d3b(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_00407e09(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
ulong FUN_004085d2(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,uint param_6,uint param_7);
void FUN_00408662(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
undefined8 FUN_004086ac(long lParm1);
ulong FUN_004086dd(uint *puParm1,byte *pbParm2,long lParm3,undefined8 uParm4);
ulong * FUN_00408760(ulong *puParm1,char cParm2,ulong uParm3);
undefined8 FUN_00408873(long lParm1,long lParm2);
undefined8 FUN_004088dc(int iParm1);
void FUN_00408902(long lParm1,long lParm2);
void FUN_00408971(long lParm1,long lParm2);
ulong FUN_004089e9(long lParm1,long lParm2);
void FUN_00408a40(undefined8 uParm1);
void FUN_00408a65(undefined8 uParm1);
void FUN_00408a8a(undefined8 uParm1,undefined8 uParm2);
void FUN_00408ab5(long lParm1);
void FUN_00408b05(long lParm1,long lParm2);
void FUN_00408b70(long lParm1,long lParm2);
ulong FUN_00408bdb(long lParm1,long lParm2);
ulong FUN_00408c4f(long lParm1,long lParm2);
undefined8 FUN_00408c98(void);
ulong FUN_00408cab(long param_1,undefined8 param_2,long param_3,long param_4,undefined8 param_5,byte param_6,long param_7);
ulong FUN_00408df4(long lParm1,undefined8 uParm2,long lParm3,long lParm4,byte bParm5,long lParm6);
undefined8 FUN_00408fcf(long lParm1,ulong uParm2);
void FUN_00409146(undefined8 uParm1,undefined8 uParm2,undefined8 *puParm3,long lParm4,char cParm5,long lParm6);
void FUN_00409237(long *plParm1);
undefined8 FUN_00409564(long *plParm1);
long FUN_0040a006(long *plParm1,long lParm2,uint *puParm3);
void FUN_0040a139(long *plParm1);
void FUN_0040a208(long *plParm1);
ulong FUN_0040a2aa(byte **ppbParm1,byte *pbParm2,uint uParm3);
ulong FUN_0040af6f(long *plParm1,long lParm2);
ulong FUN_0040b0f1(long *plParm1);
void FUN_0040b27b(long lParm1);
ulong FUN_0040b2c9(long lParm1,ulong uParm2,uint uParm3);
undefined8 FUN_0040b44f(long *plParm1,long lParm2);
undefined8 FUN_0040b4b9(undefined8 *puParm1,undefined8 uParm2);
undefined8 FUN_0040b540(undefined8 *puParm1,long lParm2,long lParm3);
undefined8 FUN_0040b61b(long *plParm1,long lParm2);
undefined8 FUN_0040b6f8(long *plParm1,long lParm2,long lParm3);
undefined8 FUN_0040bae1(long *plParm1,long lParm2,long lParm3);
undefined8 FUN_0040bde9(long *plParm1,long lParm2);
ulong FUN_0040c1ab(long *plParm1,long lParm2);
undefined8 FUN_0040c398(long *plParm1,undefined8 uParm2);
undefined8 FUN_0040c44d(long lParm1,long lParm2);
long FUN_0040c4dc(long lParm1,long lParm2);
void FUN_0040c594(long lParm1,long lParm2);
long FUN_0040c614(long *plParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_0040c9af(long lParm1,uint uParm2);
ulong * FUN_0040ca09(undefined4 *puParm1,long lParm2,long lParm3);
ulong * FUN_0040cb2a(undefined4 *puParm1,long lParm2,long lParm3,uint uParm4);
undefined8 FUN_0040cc57(long *plParm1,ulong *puParm2,ulong uParm3);
void FUN_0040ce0b(long lParm1);
long FUN_0040ceaf(long *plParm1,long lParm2,undefined8 uParm3);
long FUN_0040d081(long *plParm1,long lParm2,uint uParm3,undefined8 uParm4);
undefined1 * FUN_0040d365(undefined8 uParm1,undefined8 uParm2,long lParm3);
undefined8 FUN_0040d3f1(long *plParm1);
void FUN_0040d4ea(long **pplParm1,long lParm2,long lParm3);
void FUN_0040dbcc(long *plParm1,undefined8 uParm2);
void FUN_0040de2f(long *plParm1);
ulong FUN_0040deb4(long *plParm1,undefined8 uParm2,undefined8 uParm3,ulong uParm4);
undefined8 FUN_0040e230(long *plParm1,ulong uParm2);
void FUN_0040e5ad(long lParm1);
void FUN_0040e730(long *plParm1);
ulong FUN_0040e7c0(long *plParm1);
void FUN_0040eb08(long *plParm1);
ulong FUN_0040ed81(long *plParm1);
ulong FUN_0040f106(long **pplParm1,code *pcParm2,undefined8 uParm3);
ulong FUN_0040f1e4(long *plParm1,code *pcParm2,undefined8 uParm3);
undefined8 FUN_0040f29f(long lParm1,long lParm2);
ulong FUN_0040f417(undefined8 uParm1,long lParm2);
long FUN_0040f4f3(undefined4 *puParm1,long *plParm2,long lParm3);
undefined8 FUN_0040f6e4(long *plParm1,long lParm2);
undefined8 FUN_0040f7d2(undefined8 uParm1,long lParm2);
ulong FUN_0040f87f(long lParm1,long lParm2);
ulong FUN_0040faf4(long *plParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
long FUN_00410024(long *plParm1,long lParm2,uint uParm3);
long FUN_004100bd(long *plParm1,long lParm2,undefined4 uParm3);
undefined8 FUN_004101ef(long lParm1);
ulong FUN_0041032d(long lParm1);
ulong FUN_00410410(undefined8 *puParm1,long *plParm2,long lParm3,char cParm4);
void FUN_004107e2(undefined8 uParm1,long lParm2,undefined8 uParm3);
undefined8 FUN_00410825(long *plParm1,long lParm2,ulong uParm3);
ulong FUN_004110b6(byte *pbParm1,long lParm2,ulong uParm3);
long FUN_004112d9(undefined8 uParm1,long *plParm2,ulong uParm3,int *piParm4);
long FUN_00411405(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_00411607(undefined8 uParm1,undefined8 *puParm2,long lParm3,undefined8 uParm4,long lParm5,int *piParm6);
long FUN_004117c6(long lParm1,long *plParm2,undefined8 *puParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_0041206b(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,undefined8 uParm5,int *piParm6);
long FUN_004121ff(long lParm1,long lParm2,undefined8 uParm3,undefined8 *puParm4,ulong uParm5,undefined4 *puParm6);
ulong FUN_00412730(byte bParm1,long lParm2);
undefined8 FUN_00412767(ulong uParm1,undefined8 uParm2,long lParm3,long *plParm4,int *piParm5,int *piParm6);
undefined8 FUN_00412b28(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,byte *pbParm4);
long FUN_00412b87(long lParm1,long lParm2,long lParm3,ulong uParm4,int *piParm5);
undefined8 FUN_004134bc(undefined4 *param_1,long param_2,undefined *param_3,int param_4,undefined8 param_5,undefined8 param_6,char param_7);
undefined8 FUN_00413607(undefined4 *puParm1,long lParm2,char *pcParm3);
undefined8 FUN_00413773(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,byte *pbParm4);
undefined8 FUN_004137cd(long lParm1,undefined8 uParm2,long lParm3,long *plParm4,undefined *puParm5,ulong uParm6);
long FUN_00414134(long lParm1,undefined8 uParm2,undefined8 uParm3,char *pcParm4,byte bParm5,int *piParm6);
long FUN_004143cc(undefined8 uParm1,byte *pbParm2,undefined8 uParm3);
void FUN_004144b3(undefined8 *puParm1);
void FUN_004144ed(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined uParm4);
undefined8 * FUN_00414524(long lParm1,undefined8 *puParm2,undefined8 *puParm3,undefined8 *puParm4);
undefined8 FUN_00414670(long lParm1,long lParm2);
void FUN_004146b3(undefined8 *puParm1);
undefined8 FUN_00414718(undefined8 uParm1,long lParm2);
long * FUN_0041473f(long **pplParm1,undefined8 uParm2);
void FUN_0041486b(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
ulong FUN_004148b7(long param_1,undefined8 param_2,ulong param_3,ulong param_4,ulong param_5,undefined8 param_6,ulong *param_7,char param_8);
ulong FUN_00414c2c(ulong *puParm1,long lParm2,ulong uParm3,int iParm4);
ulong FUN_00414ed2(long *param_1,long param_2,long param_3,long param_4,long param_5,undefined8 param_6,ulong param_7,undefined8 *param_8,uint param_9);
ulong FUN_00415c93(long lParm1);
long FUN_00415fb8(long lParm1,char cParm2,long *plParm3);
undefined8 FUN_004164d4(long *plParm1,long lParm2,uint uParm3);
undefined8 FUN_0041659b(long lParm1,long lParm2,undefined8 uParm3);
long FUN_0041663e(long param_1,undefined8 param_2,long param_3,long *param_4,long param_5,long param_6,long param_7);
ulong FUN_00416b7d(long *plParm1,undefined8 uParm2,undefined8 uParm3,long lParm4,undefined8 uParm5,undefined8 uParm6);
undefined8 FUN_00416d4d(long *plParm1,undefined8 *puParm2,long lParm3,undefined8 uParm4,undefined8 *puParm5);
undefined8 FUN_00416e9b(long *plParm1,long lParm2,ulong uParm3,long *plParm4,char cParm5);
undefined8 FUN_004172ab(long *plParm1);
void FUN_00417341(long *plParm1,long lParm2,long lParm3,long lParm4,long lParm5,long lParm6);
ulong FUN_004174e5(long lParm1,long *plParm2);
undefined8 FUN_00417686(long lParm1,long *plParm2,long lParm3,undefined8 uParm4);
ulong FUN_00417891(long lParm1,long lParm2);
ulong FUN_0041797b(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_00417ad5(long lParm1,long *plParm2,long lParm3,long lParm4);
ulong FUN_00417cb4(long lParm1,long lParm2,undefined8 uParm3);
ulong FUN_00417de3(long *plParm1,long lParm2,undefined8 uParm3,undefined8 uParm4);
undefined8 FUN_00418071(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_004181dd(long lParm1,uint uParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_0041845c(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long lParm5,undefined8 uParm6);
ulong FUN_00418529(long *plParm1,long lParm2,undefined8 uParm3,long lParm4,long lParm5,long lParm6);
ulong FUN_004188a2(long lParm1,long *plParm2,long lParm3,long lParm4);
ulong FUN_00418d77(long lParm1,long *plParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_00418e51(int *piParm1,long lParm2,long lParm3);
long FUN_00418fc7(int *piParm1,long lParm2,long lParm3);
long FUN_0041924d(int *piParm1,long lParm2);
ulong FUN_004192f7(long lParm1,long lParm2,undefined8 uParm3);
ulong FUN_004193f2(long lParm1,long lParm2);
ulong FUN_00419774(long lParm1,long lParm2);
ulong FUN_00419ce0(long lParm1,long lParm2,long lParm3);
ulong FUN_0041a257(undefined8 uParm1,long *plParm2,undefined8 *puParm3,undefined8 uParm4,long lParm5);
long FUN_0041a32e(long *plParm1,long lParm2,long lParm3,uint uParm4);
ulong FUN_0041a3ba(long param_1,long *param_2,long param_3,long param_4,undefined8 param_5,long param_6,uint param_7);
ulong FUN_0041ab42(long lParm1,long lParm2,long lParm3,undefined8 uParm4);
ulong FUN_0041ae05(long lParm1,undefined8 *puParm2,undefined8 uParm3,uint uParm4);
ulong FUN_0041af79(long *plParm1,undefined8 uParm2,long lParm3,long lParm4,uint uParm5);
ulong FUN_0041b13e(long lParm1,undefined8 uParm2,long lParm3,undefined8 uParm4,uint uParm5);
undefined8 FUN_0041b51e(long lParm1,long lParm2);
long FUN_0041bf46(long *plParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_0041c7ec(long *plParm1,long lParm2,long lParm3,long lParm4);
undefined8 FUN_0041cc28(long lParm1,undefined8 *puParm2,long lParm3);
ulong FUN_0041ce03(long lParm1,int iParm2);
undefined8 FUN_0041cf8b(long lParm1,undefined4 uParm2,ulong uParm3);
void FUN_0041d0c9(long lParm1);
void FUN_0041d1da(long lParm1);
undefined8 FUN_0041d21b(long lParm1,undefined8 uParm2,long lParm3,long lParm4,long lParm5);
long FUN_0041d53b(long lParm1,long lParm2);
undefined8 FUN_0041d611(long lParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 * FUN_0041d789(long lParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0041d895(undefined8 *puParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
ulong FUN_0041d8fd(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0041da1c(byte *pbParm1,byte *pbParm2);
undefined8 FUN_0041da94(undefined8 uParm1);
undefined8 FUN_0041db1f(void);
ulong FUN_0041db2c(uint uParm1);
undefined * FUN_0041dbfd(void);
char * FUN_0041dfc2(void);
ulong FUN_0041e090(byte bParm1);
ulong FUN_0041e0c6(undefined8 uParm1);
void FUN_0041e17b(undefined8 uParm1);
void FUN_0041e1a0(void);
ulong FUN_0041e1af(long lParm1);
undefined8 FUN_0041e27f(undefined8 uParm1);
void FUN_0041e29e(undefined8 uParm1,undefined8 uParm2,uint uParm3);
ulong FUN_0041e2c9(void);
long FUN_0041e31a(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
int * FUN_0041e563(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_0041f045(ulong uParm1,long lParm2,long lParm3);
long FUN_0041f2b8(int *param_1,long *param_2);
long FUN_0041f48f(undefined auParm1 [16],int *piParm2,long *plParm3);
undefined8 FUN_0041f80a(int iParm1,long lParm2,uint *puParm3,long lParm4,uint uParm5);
void FUN_0042000d(uint param_1);
void FUN_00420057(undefined auParm1 [16],uint uParm2);
ulong FUN_004200a5(void);
ulong FUN_004203e7(undefined auParm1 [16],undefined8 uParm2,undefined8 uParm3);
ulong FUN_00420771(char *pcParm1,long lParm2);
undefined8 FUN_004207ca(long param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
ulong FUN_00427f26(uint uParm1);
void FUN_00427f45(undefined auParm1 [16],int *piParm2,undefined8 uParm3);
void FUN_00427fce(int *param_1);
ulong FUN_00428063(ulong uParm1,long lParm2);
void FUN_00428097(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004280d2(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00428123(ulong uParm1,ulong uParm2);
undefined8 FUN_0042813e(uint *puParm1,ulong *puParm2);
undefined8 FUN_004288da(char *pcParm1,ulong *puParm2,char **ppcParm3);
ulong FUN_00429baa(void);
ulong FUN_00429be1(void);
void FUN_00429c10(void);
undefined8 _DT_FINI(void);

