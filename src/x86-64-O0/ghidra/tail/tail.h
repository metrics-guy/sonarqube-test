typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void entry(void);
void FUN_004025f0(void);
void FUN_00402660(void);
void FUN_004026f0(void);
void FUN_00402725(void);
void FUN_0040274a(void);
void FUN_0040276f(undefined *puParm1);
ulong FUN_0040290c(long lParm1);
undefined8 FUN_0040294b(ulong uParm1);
void FUN_00403af6(uint uParm1);
void FUN_00403c9e(void);
ulong FUN_00403d47(long lParm1);
undefined * FUN_00403d77(undefined8 *puParm1);
void FUN_00403dab(long lParm1,undefined4 uParm2,undefined8 uParm3,undefined8 *puParm4,undefined4 uParm5);
void FUN_00403e48(uint uParm1,undefined8 uParm2);
void FUN_00403eb4(undefined8 uParm1);
void FUN_00403efa(undefined8 uParm1,ulong uParm2);
long FUN_00403f7d(char cParm1,undefined8 uParm2,uint uParm3,ulong uParm4);
long FUN_004040bd(uint uParm1,undefined8 uParm2,uint uParm3,undefined8 uParm4);
undefined8 FUN_00404217(undefined8 uParm1,uint uParm2,long lParm3,long lParm4,long lParm5,long *plParm6);
ulong FUN_00404590(undefined8 uParm1,uint uParm2,ulong uParm3,long *plParm4);
ulong FUN_00404a3e(undefined8 uParm1,uint uParm2,ulong uParm3,long *plParm4);
undefined8 FUN_00404d54(undefined8 uParm1,uint uParm2,ulong uParm3,long *plParm4);
undefined8 FUN_00404e86(undefined8 uParm1,uint uParm2,long lParm3,long *plParm4);
ulong FUN_00404ff3(uint uParm1,undefined8 uParm2);
void FUN_004050c9(undefined8 *puParm1,byte bParm2);
undefined8 FUN_00405770(long lParm1,ulong uParm2);
void FUN_0040581a(undefined auParm1 [16],long lParm2,ulong uParm3);
undefined8 FUN_00406106(long lParm1,ulong uParm2);
undefined8 FUN_0040617e(long lParm1,ulong uParm2);
undefined8 FUN_004061f9(long lParm1,ulong uParm2);
undefined8 FUN_00406285(long lParm1,ulong uParm2);
undefined8 FUN_00406335(long lParm1,ulong uParm2);
ulong FUN_004063c1(long lParm1,ulong uParm2);
ulong FUN_004063ec(long lParm1,long lParm2);
void FUN_0040641d(long lParm1,long *plParm2);
undefined8 FUN_00406667(double dParm1,uint uParm2,long lParm3,ulong uParm4,undefined8 uParm5,undefined8 uParm6,undefined8 uParm7);
ulong FUN_0040766f(undefined8 uParm1,uint uParm2,ulong uParm3,long *plParm4);
ulong FUN_0040799f(undefined8 uParm1,uint uParm2,undefined8 uParm3,long *plParm4);
void FUN_00407bcc(undefined8 uParm1,uint uParm2,undefined8 uParm3,undefined8 *puParm4);
ulong FUN_00407c2b(undefined8 *puParm1,undefined8 uParm2);
undefined8 FUN_0040808f(int iParm1,long lParm2,long *plParm3);
void FUN_004082fb(uint uParm1,undefined8 uParm2,undefined8 *puParm3,undefined4 *puParm4,double *pdParm5);
long FUN_0040873d(long lParm1,ulong uParm2);
ulong FUN_004088aa(uint uParm1,undefined8 *puParm2);
void FUN_00408f40(void);
long FUN_00408f51(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
void FUN_00409080(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_00409102(long lParm1,long lParm2,long lParm3);
long FUN_00409243(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
long FUN_004092c9(void);
void FUN_004092fd(undefined8 uParm1,undefined8 *puParm2);
void FUN_0040935c(void);
ulong FUN_00409442(char *pcParm1);
char * FUN_004094c3(char *pcParm1);
void FUN_0040952c(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0040963b(long lParm1);
long FUN_0040964d(long *plParm1,undefined8 uParm2);
long FUN_004096a4(long lParm1,long lParm2);
ulong FUN_00409737(ulong uParm1);
ulong FUN_004097a2(ulong uParm1);
ulong FUN_004097e9(undefined8 uParm1,ulong uParm2);
ulong FUN_00409820(ulong uParm1,ulong uParm2);
undefined8 FUN_00409839(long lParm1);
ulong FUN_0040993c(ulong uParm1,long lParm2);
long * FUN_00409a49(undefined8 uParm1,undefined1 *puParm2,code *pcParm3,code *pcParm4,long lParm5);
void FUN_00409bae(long **pplParm1,undefined8 uParm2);
long FUN_00409cd9(long lParm1);
void FUN_00409d24(long lParm1,undefined8 *puParm2);
long FUN_00409d5a(long lParm1,long lParm2,long **pplParm3,char cParm4);
undefined8 FUN_00409eef(long lParm1,long **pplParm2,char cParm3);
undefined8 FUN_0040a0bf(long *plParm1,undefined8 uParm2);
undefined8 FUN_0040a2c0(long lParm1,long lParm2,long *plParm3);
undefined8 FUN_0040a5da(undefined8 uParm1,undefined8 uParm2);
long FUN_0040a623(long lParm1,undefined8 uParm2);
char * FUN_0040a8d2(long lParm1,long lParm2);
long FUN_0040aa04(void);
void FUN_0040aa92(long lParm1);
ulong FUN_0040ab74(undefined1 *puParm1,byte bParm2,uint uParm3);
ulong * FUN_0040abfc(ulong *puParm1,uint uParm2);
char * FUN_0040ac9b(char *pcParm1,int iParm2);
ulong FUN_0040ad3b(long param_1,ulong param_2,long param_3,ulong param_4,uint param_5,uint param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_0040bc5d(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_0040becd(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_0040bf0e(uint uParm1,undefined8 uParm2);
void FUN_0040bf32(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_0040bfc6(undefined8 uParm1,char cParm2);
void FUN_0040bff0(undefined8 uParm1);
void FUN_0040c00f(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_0040c0aa(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040c0d6(uint uParm1,undefined8 uParm2);
void FUN_0040c0ff(undefined8 uParm1);
long FUN_0040c11e(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040c18e(long lParm1);
ulong FUN_0040c1a4(uint uParm1);
void FUN_0040c1b4(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040c1da(long lParm1,int iParm2,long lParm3,int iParm4);
ulong FUN_0040c261(uint uParm1);
void FUN_0040c2ae(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_0040c71d(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
void FUN_0040c7f0(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_0040c8a7(ulong uParm1,ulong uParm2);
void FUN_0040c8f7(undefined8 uParm1);
long FUN_0040c911(long lParm1);
long FUN_0040c946(long lParm1,long lParm2);
void FUN_0040c9a7(void);
void FUN_0040c9d1(void);
void FUN_0040c9d8(uint uParm1,uint uParm2);
ulong FUN_0040ca01(undefined8 param_1,uint param_2,ulong param_3,ulong param_4,undefined8 param_5,undefined8 param_6,uint param_7);
void FUN_0040cb13(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,uint uParm6);
undefined8 FUN_0040cb68(undefined auParm1 [16]);
ulong FUN_0040cbd1(char *pcParm1,char **ppcParm2,double *pdParm3,code *pcParm4);
undefined8 FUN_0040cc82(int iParm1);
ulong FUN_0040cca8(ulong *puParm1,int iParm2);
ulong FUN_0040cd07(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_0040cd48(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_0040d131(ulong uParm1,ulong uParm2);
ulong FUN_0040d1b0(uint uParm1);
void FUN_0040d1d9(void);
void FUN_0040d20e(uint uParm1);
void FUN_0040d283(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_0040d308(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_0040d3ed(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_0040d6a6(long lParm1,int *piParm2);
ulong FUN_0040d889(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_0040de72(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_0040df40(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
ulong FUN_0040e709(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,uint param_6,uint param_7);
void FUN_0040e799(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
ulong FUN_0040e7e3(uint uParm1);
void FUN_0040e933(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040e958(long lParm1,long lParm2);
undefined8 FUN_0040ea0f(long lParm1);
ulong FUN_0040ea40(uint *puParm1,byte *pbParm2,long lParm3,undefined8 uParm4);
ulong * FUN_0040eac3(ulong *puParm1,char cParm2,ulong uParm3);
ulong FUN_0040ebd6(long *plParm1,long *plParm2);
undefined8 FUN_0040ecb5(long lParm1,long lParm2);
undefined8 FUN_0040ed1e(uint uParm1,long lParm2,long lParm3,long lParm4,undefined8 uParm5);
void FUN_0040ee6b(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040ee90(long lParm1,long lParm2);
ulong FUN_0040ef20(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_0040f03f(void);
undefined8 FUN_0040f050(void);
ulong FUN_0040f05e(uint uParm1,uint uParm2);
ulong FUN_0040f095(ulong uParm1,undefined4 uParm2);
ulong FUN_0040f0cc(byte *pbParm1,byte *pbParm2);
undefined8 FUN_0040f144(undefined8 uParm1);
void FUN_0040f1cf(double dParm1);
undefined8 FUN_0040f31d(void);
ulong FUN_0040f32a(uint uParm1);
undefined * FUN_0040f3fb(void);
char * FUN_0040f7c0(void);
void FUN_0040f88e(uint uParm1);
ulong FUN_0040f8b4(undefined8 uParm1);
void FUN_0040f969(undefined8 uParm1);
void FUN_0040f98e(void);
ulong FUN_0040f99d(long lParm1);
undefined8 FUN_0040fa6d(undefined8 uParm1);
void FUN_0040fa8c(undefined8 uParm1,undefined8 uParm2,uint uParm3);
ulong FUN_0040fab7(void);
long FUN_0040fb08(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
int * FUN_0040fd51(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_00410833(ulong uParm1,long lParm2,long lParm3);
long FUN_00410aa6(int *param_1,long *param_2);
long FUN_00410c7d(undefined auParm1 [16],int *piParm2,long *plParm3);
undefined8 FUN_00410ff8(int iParm1,long lParm2,uint *puParm3,long lParm4,uint uParm5);
void FUN_004117fb(uint param_1);
void FUN_00411845(undefined auParm1 [16],uint uParm2);
ulong FUN_00411893(void);
ulong FUN_00411bd5(undefined auParm1 [16],undefined8 uParm2,undefined8 uParm3);
ulong FUN_00411f5f(char *pcParm1,long lParm2);
undefined8 FUN_00411fb8(long param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
char * FUN_0041227b(char *pcParm1,char **ppcParm2,long lParm3,undefined8 uParm4);
ulong FUN_00419714(uint uParm1);
void FUN_00419733(undefined auParm1 [16],int *piParm2,undefined8 uParm3);
void FUN_004197bc(int *param_1);
ulong FUN_00419851(ulong uParm1,long lParm2);
void FUN_00419885(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004198c0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00419911(ulong uParm1,ulong uParm2);
undefined8 FUN_0041992c(uint *puParm1,ulong *puParm2);
undefined8 FUN_0041a0c8(char *pcParm1,ulong *puParm2,char **ppcParm3);
ulong FUN_0041b398(void);
ulong FUN_0041b3cf(void);
void FUN_0041b400(void);
undefined8 _DT_FINI(void);

