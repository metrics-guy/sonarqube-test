typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined5;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00403270(void);
void FUN_004034e0(void);
void FUN_00403810(void);
void entry(void);
void FUN_00403880(void);
void FUN_004038f0(void);
void FUN_00403980(void);
void FUN_004039b5(void);
void FUN_004039da(void);
void FUN_00403a1d(undefined *puParm1);
undefined8 FUN_00403bba(undefined8 uParm1);
undefined8 FUN_00403bc8(uint uParm1,undefined8 uParm2,undefined8 uParm3,long lParm4);
undefined8 FUN_00403e23(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
undefined8 FUN_004040c6(undefined8 uParm1,long lParm2,long lParm3,long *plParm4,char *pcParm5,long lParm6);
ulong FUN_00404812(undefined8 uParm1,long lParm2,undefined *puParm3);
ulong FUN_004048c3(int iParm1,undefined8 *puParm2,long lParm3,char cParm4,ulong *puParm5);
void FUN_00404fc2(long lParm1);
void FUN_00405113(undefined8 uParm1,long lParm2,char cParm3);
ulong FUN_004052df(uint uParm1,undefined8 *puParm2);
ulong FUN_00405a34(char *pcParm1);
long FUN_00405a93(long lParm1,ulong uParm2);
ulong FUN_00405ad9(char *pcParm1,ulong uParm2);
undefined FUN_00405b5e(int iParm1);;
void FUN_00405b6e(long lParm1);
ulong FUN_00405ba5(void);
ulong FUN_00405c07(int iParm1);
undefined8 FUN_00405c2b(void);
undefined8 FUN_00405c55(void);
ulong FUN_00405c76(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00405cba(uint uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_00405d1e(uint uParm1,undefined8 uParm2,char cParm3,long lParm4);
undefined8 FUN_00405e00(uint param_1,uint param_2,long param_3,ulong param_4,ulong param_5,byte param_6,undefined8 param_7,undefined8 param_8,ulong param_9,long *param_10,char *param_11);
void FUN_00406160(uint uParm1,uint uParm2);
undefined8 FUN_00406187(uint uParm1,ulong uParm2);
undefined8 FUN_0040623a(uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,long param_6,int param_7,undefined8 param_8,undefined8 param_9,undefined *param_10);
undefined8 FUN_0040677b(long *plParm1,undefined8 *puParm2);
ulong FUN_004067d2(int iParm1);
undefined8 FUN_004067f6(void);
ulong FUN_00406813(undefined8 param_1,undefined8 param_2,byte param_3,undefined8 param_4,undefined8 param_5,undefined8 *param_6,byte *param_7,byte *param_8);
ulong FUN_00406a5c(long lParm1,undefined8 uParm2,uint uParm3,long lParm4,char cParm5,long lParm6);
void FUN_00406cc9(void);
undefined8 FUN_00406cdb(undefined8 uParm1,undefined8 uParm2,uint uParm3,char cParm4,long lParm5);
undefined8 FUN_00406ef5(undefined8 uParm1,byte bParm2,byte bParm3,long lParm4);
void FUN_00406ff5(uint uParm1,undefined8 uParm2,uint uParm3);
ulong FUN_00407031(long lParm1);
ulong FUN_00407084(undefined8 param_1,char *param_2,long param_3,uint param_4,uint param_5,byte *param_6,long *param_7);
ulong FUN_00407efe(undefined8 uParm1,ulong *puParm2,undefined8 uParm3,ulong *puParm4,int *piParm5,undefined *puParm6);
ulong FUN_0040868f(undefined8 uParm1,uint uParm2);
void FUN_004086dc(long lParm1,undefined8 uParm2,long lParm3);
void FUN_004087ed(long lParm1);
void FUN_00408823(long lParm1);
ulong FUN_00408859(long lParm1,undefined8 uParm2,long lParm3);
void FUN_0040890d(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_004089a2(void);
undefined8 FUN_004089d4(undefined8 uParm1,undefined8 uParm2,byte bParm3,char cParm4,char cParm5);
ulong FUN_00408ad2(long lParm1,char cParm2);
ulong FUN_00408b0e(long lParm1,long *plParm2,undefined8 uParm3);
ulong FUN_00408ca0(char *param_1,undefined8 param_2,byte param_3,long *param_4,undefined8 param_5,uint *param_6,byte param_7,char *param_8,undefined *param_9,undefined *param_10);
undefined8 FUN_0040b0a9(uint *puParm1);
void FUN_0040b1ed(undefined8 uParm1,undefined8 uParm2,byte bParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
void FUN_0040b28a(long lParm1);
ulong FUN_0040b2cf(long lParm1);
ulong FUN_0040b313(long lParm1);
ulong FUN_0040b357(void);
ulong FUN_0040b38b(ulong *puParm1,ulong uParm2);
ulong FUN_0040b3b4(long *plParm1,long *plParm2);
void FUN_0040b409(long lParm1);
void FUN_0040b43c(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_0040b494(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_0040b4e6(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040b583(void);
undefined8 FUN_0040b5c1(void);
void FUN_0040b5cc(undefined4 uParm1,undefined4 *puParm2);
ulong FUN_0040b62a(uint *puParm1);
long FUN_0040bc54(long lParm1,long lParm2);
void FUN_0040bce6(undefined8 uParm1,uint *puParm2);
ulong FUN_0040bd2a(uint uParm1,undefined8 uParm2,uint uParm3,undefined8 uParm4,uint uParm5,char cParm6);
void FUN_0040beca(undefined8 uParm1,undefined8 *puParm2);
ulong FUN_0040bf00(undefined8 uParm1,uint uParm2,undefined8 uParm3,char cParm4);
ulong FUN_0040c070(undefined8 uParm1,uint uParm2,undefined8 uParm3,uint uParm4,uint uParm5);
ulong FUN_0040c123(undefined8 uParm1,uint uParm2,uint uParm3);
long FUN_0040c18c(undefined8 uParm1,ulong uParm2);
void FUN_0040c2da(void);
long FUN_0040c2eb(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
void FUN_0040c41a(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_0040c49c(long lParm1,long lParm2,long lParm3);
long FUN_0040c5dd(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
void FUN_0040c663(char *pcParm1);
void FUN_0040c6bd(long lParm1,long lParm2,undefined uParm3);
ulong FUN_0040c7d0(long *plParm1,undefined8 uParm2,long lParm3,long lParm4,long *plParm5);
long FUN_0040cba0(long lParm1,int iParm2,char cParm3);
void FUN_0040ce62(undefined8 uParm1,uint uParm2);
long FUN_0040ce89(undefined8 uParm1,uint uParm2);
ulong FUN_0040cec4(undefined8 uParm1,char *pcParm2);
void FUN_0040cf20(undefined8 uParm1,char *pcParm2);
ulong FUN_0040cf70(ulong uParm1,ulong uParm2,ulong uParm3);
void FUN_0040d03b(undefined8 uParm1);
ulong FUN_0040d05a(long *plParm1,undefined8 uParm2,undefined8 uParm3);
char * FUN_0040d101(char *pcParm1,uint uParm2);
void FUN_0040d8dd(void);
void FUN_0040d9e2(void);
long FUN_0040dac8(undefined8 uParm1);
long FUN_0040db91(undefined8 uParm1);
ulong FUN_0040dbbf(char *pcParm1);
undefined * FUN_0040dc40(undefined8 uParm1);
char * FUN_0040dcd7(char *pcParm1);
ulong FUN_0040dd40(long lParm1);
ulong FUN_0040dd8e(char *pcParm1);
void FUN_0040ddf0(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
void FUN_0040de21(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_0040df30(long lParm1,undefined8 uParm2,undefined8 *puParm3);
ulong FUN_0040dfcc(long lParm1,undefined8 uParm2,undefined8 *puParm3);
undefined8 FUN_0040e028(uint uParm1);
void FUN_0040e0d3(uint uParm1,undefined *puParm2);
long FUN_0040e29b(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
char * FUN_0040e2dc(char *pcParm1);
long FUN_0040e2fc(long lParm1,char *pcParm2,undefined8 *puParm3);
long FUN_0040e443(uint uParm1,long lParm2,long lParm3);
long FUN_0040e4c4(long *plParm1,undefined8 uParm2);
long FUN_0040e51b(long lParm1,long lParm2);
ulong FUN_0040e5ae(ulong uParm1);
ulong FUN_0040e619(ulong uParm1);
ulong FUN_0040e660(undefined8 uParm1,ulong uParm2);
ulong FUN_0040e697(ulong uParm1,ulong uParm2);
undefined8 FUN_0040e6b0(long lParm1);
ulong FUN_0040e7b3(ulong uParm1,long lParm2);
long * FUN_0040e8c0(undefined8 uParm1,undefined1 *puParm2,code *pcParm3,code *pcParm4,long lParm5);
void FUN_0040ea25(long **pplParm1,undefined8 uParm2);
long FUN_0040eb50(long lParm1);
void FUN_0040eb9b(long lParm1,undefined8 *puParm2);
long FUN_0040ebd1(long lParm1,long lParm2,long **pplParm3,char cParm4);
undefined8 FUN_0040ed66(long lParm1,long **pplParm2,char cParm3);
undefined8 FUN_0040ef36(long *plParm1,undefined8 uParm2);
undefined8 FUN_0040f137(long lParm1,long lParm2,long *plParm3);
undefined8 FUN_0040f451(undefined8 uParm1,undefined8 uParm2);
long FUN_0040f49a(long lParm1,undefined8 uParm2);
ulong FUN_0040f749(undefined8 *puParm1,ulong uParm2);
ulong FUN_0040f795(long lParm1,ulong uParm2);
ulong FUN_0040f7bf(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_0040f837(undefined8 *puParm1,undefined8 *puParm2);
void FUN_0040f8af(undefined8 *puParm1);
void FUN_0040f8e1(long lParm1);
ulong FUN_0040f9c3(undefined8 uParm1,uint uParm2,undefined8 uParm3,uint uParm4,uint uParm5);
ulong FUN_0040fa21(undefined8 uParm1,uint uParm2,undefined4 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_0040fa6d(undefined1 *puParm1,byte bParm2,uint uParm3);
ulong * FUN_0040faf5(ulong *puParm1,uint uParm2);
char * FUN_0040fb94(char *pcParm1,int iParm2);
ulong FUN_0040fc34(long param_1,ulong param_2,long param_3,ulong param_4,uint param_5,uint param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_00410b56(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_00410dc6(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_00410e07(uint uParm1,undefined8 uParm2);
void FUN_00410e2b(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_00410ebf(undefined8 uParm1,char cParm2);
void FUN_00410ee9(undefined8 uParm1);
void FUN_00410f08(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_00410fa3(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00410fcf(uint uParm1,undefined8 uParm2);
void FUN_00410ff8(undefined8 uParm1);
undefined8 FUN_00411017(undefined4 uParm1);
ulong FUN_00411033(uint uParm1,long lParm2,uint uParm3,long lParm4,uint uParm5);
long FUN_00411494(uint uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00411504(undefined8 uParm1,undefined8 uParm2);
void FUN_004116a9(undefined8 *puParm1,undefined8 *puParm2);
long FUN_004116e4(long lParm1,uint uParm2);
undefined8 FUN_00411a17(undefined8 uParm1,uint uParm2);
void FUN_00411a96(void);
undefined8 FUN_00411aa1(void);
undefined8 FUN_00411abf(void);
undefined8 FUN_00411ae1(long lParm1);
undefined8 FUN_00411af3(long lParm1);
undefined8 FUN_00411b05(long lParm1);
void FUN_00411b17(long lParm1);
void FUN_00411b2d(long lParm1);
ulong FUN_00411b43(uint uParm1);
void FUN_00411b53(uint uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00411b7f(undefined8 uParm1,ulong uParm2);
ulong FUN_00411ba9(long lParm1,int iParm2,undefined8 uParm3,code *pcParm4,ulong uParm5);
ulong FUN_00411d63(uint uParm1);
ulong FUN_00411db0(ulong *puParm1,ulong uParm2);
ulong FUN_00411dd9(ulong *puParm1,ulong *puParm2);
ulong FUN_00411e0b(undefined8 uParm1,undefined8 *puParm2,long lParm3,uint uParm4);
ulong FUN_004125b7(undefined8 *puParm1);
undefined8 FUN_0041271d(undefined8 uParm1,long *plParm2);
ulong FUN_00412846(uint uParm1,long lParm2,undefined8 *puParm3);
void FUN_00412c8c(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00412cb3(undefined8 uParm1,undefined8 *puParm2);
void FUN_00412ee6(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_00413355(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
void FUN_00413428(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_004134df(void);
void FUN_00413520(undefined8 uParm1,ulong uParm2,ulong uParm3);
void FUN_0041357e(long lParm1,ulong *puParm2,ulong uParm3);
void FUN_00413658(undefined8 uParm1);
long FUN_00413672(long lParm1);
long FUN_004136a7(long lParm1,long lParm2);
void FUN_00413708(undefined8 uParm1,undefined8 uParm2);
void FUN_0041373c(undefined8 uParm1);
long FUN_00413769(void);
long FUN_00413793(void);
ulong FUN_004137cc(void);
undefined8 FUN_00413817(ulong uParm1,ulong uParm2);
ulong FUN_00413896(uint uParm1);
void FUN_004138bf(void);
void FUN_004138f4(uint uParm1);
void FUN_00413969(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_004139ee(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_00413ad3(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_00413d8c(uint uParm1);
void FUN_00413def(undefined8 uParm1);
void FUN_00413e14(void);
ulong FUN_00413e23(long lParm1);
undefined8 FUN_00413ef3(undefined8 uParm1);
void FUN_00413f12(undefined8 uParm1,undefined8 uParm2,uint uParm3);
void FUN_00413f3d(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
void FUN_00413f6a(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
ulong FUN_00413fa7(uint uParm1,long lParm2,long lParm3,uint uParm4);
long FUN_004140ba(long lParm1,undefined *puParm2);
void FUN_00414658(long lParm1,int *piParm2);
ulong FUN_0041483b(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_00414e24(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_00414ef2(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
ulong FUN_004156bb(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,uint param_6,uint param_7);
void FUN_0041574b(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
ulong FUN_00415795(long lParm1,uint uParm2,uint uParm3);
ulong FUN_00415902(uint uParm1,char *pcParm2,uint uParm3,undefined8 uParm4);
ulong FUN_00415acf(uint uParm1,long lParm2,uint uParm3,long lParm4,uint uParm5);
void FUN_00415cab(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00415cd0(long lParm1,long lParm2);
undefined8 FUN_00415d87(long lParm1);
ulong FUN_00415db8(uint *puParm1,byte *pbParm2,long lParm3,undefined8 uParm4);
ulong FUN_00415e3b(long lParm1,uint uParm2);
undefined8 FUN_00415f77(long lParm1,uint uParm2);
undefined8 FUN_0041600a(long lParm1,uint uParm2,long lParm3);
void FUN_004160f7(uint uParm1,undefined8 uParm2,uint uParm3,uint uParm4);
ulong FUN_00416127(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,undefined8 param_2,uint param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_004162ee(long lParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_00416394(long lParm1,long lParm2);
ulong FUN_004163fd(long lParm1,long lParm2);
void FUN_00416910(uint uParm1,undefined8 uParm2,uint uParm3,undefined8 uParm4);
ulong FUN_00416943(long lParm1);
void FUN_00416a00(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00416a25(long lParm1,long lParm2);
undefined8 FUN_00416ab5(undefined8 uParm1,uint uParm2,long lParm3);
ulong FUN_00416b5d(long lParm1);
ulong FUN_00416cc9(uint uParm1,long lParm2,uint uParm3);
ulong FUN_00416e8a(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_00416fa9(undefined8 uParm1,undefined8 uParm2,undefined4 uParm3,undefined4 *puParm4);
void FUN_00416fe5(undefined8 uParm1,uint uParm2,uint uParm3);
ulong FUN_00417021(uint *puParm1,undefined8 uParm2,uint uParm3);
void FUN_004170d2(uint uParm1,undefined8 uParm2);
ulong FUN_00417107(ulong uParm1,undefined4 uParm2);
ulong FUN_0041713e(byte *pbParm1,byte *pbParm2);
undefined *FUN_004171b6(uint uParm1,undefined8 uParm2,undefined *puParm3,ulong uParm4,code **ppcParm5,code *pcParm6);
undefined8 FUN_00417458(uint uParm1,char cParm2);
undefined8 FUN_004174d3(undefined8 uParm1);
undefined8 FUN_0041755e(void);
void FUN_0041756b(undefined8 *puParm1);
ulong FUN_004175c0(uint uParm1);
ulong FUN_00417691(char *pcParm1,ulong uParm2);
undefined * FUN_004176eb(void);
char * FUN_00417ab0(void);
undefined8 * FUN_00417b7e(undefined8 uParm1);
undefined8 FUN_00417bc5(undefined8 uParm1,undefined8 uParm2);
long FUN_00417c08(long lParm1);
ulong FUN_00417c1a(undefined8 *puParm1,ulong uParm2);
void FUN_00417dc8(undefined8 uParm1);
ulong FUN_00417df4(undefined8 *puParm1);
ulong * FUN_00417e37(ulong uParm1,ulong uParm2);
undefined8 * FUN_00417e99(undefined8 uParm1,undefined8 uParm2);
void FUN_00417ee0(long lParm1,ulong uParm2,ulong uParm3);
long FUN_00418146(long lParm1,ulong uParm2);
void FUN_00418235(undefined8 *puParm1,long lParm2,long lParm3);
void FUN_004182cf(ulong *puParm1,ulong uParm2,ulong uParm3);
void FUN_00418414(long *plParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_0041846b(long *plParm1);
undefined8 FUN_004184bb(undefined8 uParm1);
undefined8 FUN_004184d5(long lParm1,undefined8 uParm2);
void FUN_00418519(ulong *puParm1,long *plParm2);
void FUN_00418b68(long lParm1);
void FUN_004190f0(uint uParm1);
ulong FUN_00419116(long lParm1,uint uParm2,uint uParm3);
void FUN_004192bf(undefined8 uParm1,undefined8 uParm2);
ulong FUN_004192e7(undefined8 uParm1);
ulong FUN_0041939c(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00419449(uint uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_004194f7(void);
long FUN_00419548(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
int * FUN_00419791(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_0041a273(ulong uParm1,long lParm2,long lParm3);
long FUN_0041a4e6(int *param_1,long *param_2);
long FUN_0041a6bd(undefined auParm1 [16],int *piParm2,long *plParm3);
undefined8 FUN_0041aa38(int iParm1,long lParm2,uint *puParm3,long lParm4,uint uParm5);
void FUN_0041b23b(uint param_1);
void FUN_0041b285(undefined auParm1 [16],uint uParm2);
ulong FUN_0041b2d3(void);
ulong FUN_0041b615(undefined auParm1 [16],undefined8 uParm2,undefined8 uParm3);
ulong FUN_0041b99f(char *pcParm1,long lParm2);
undefined8 FUN_0041b9f8(long param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
ulong FUN_00423154(uint uParm1);
long FUN_00423173(undefined8 uParm1,undefined8 uParm2);
void FUN_00423259(undefined auParm1 [16],int *piParm2,undefined8 uParm3);
void FUN_004232e2(int *param_1);
ulong FUN_00423377(ulong uParm1,long lParm2);
void FUN_004233ab(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004233e6(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00423437(ulong uParm1,ulong uParm2);
undefined8 FUN_00423452(uint *puParm1,ulong *puParm2);
undefined8 FUN_00423bee(char *pcParm1,ulong *puParm2,char **ppcParm3);
ulong FUN_00424ebe(void);
ulong FUN_00424ef5(void);
void FUN_00424f20(void);
undefined8 _DT_FINI(void);

