typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void entry(void);
void FUN_004026a0(void);
void FUN_00402710(void);
void FUN_004027a0(void);
void FUN_004027d5(void);
void FUN_004027fa(void);
void FUN_0040281f(undefined *puParm1);
void FUN_004029bc(uint uParm1);
void FUN_00402aa1(long lParm1,long lParm2);
undefined8 FUN_00402bb5(uint uParm1,undefined8 uParm2,char *pcParm3);
void FUN_004032cd(undefined8 uParm1);
undefined8 FUN_004032ec(long *plParm1,long *plParm2);
long FUN_0040352b(undefined8 *puParm1,undefined8 *puParm2,uint uParm3,undefined8 uParm4);
ulong FUN_004036cc(uint uParm1,undefined8 uParm2);
ulong FUN_00403736(undefined *puParm1);
ulong FUN_004038af(uint uParm1,undefined8 *puParm2);
void FUN_00403cd7(void);
char * FUN_00403dbd(char *pcParm1);
long FUN_00403ddd(long lParm1,char *pcParm2,undefined8 *puParm3);
void FUN_00403f24(long lParm1);
ulong FUN_00404006(undefined1 *puParm1,byte bParm2,uint uParm3);
ulong * FUN_0040408e(ulong *puParm1,uint uParm2);
char * FUN_0040412d(char *pcParm1,int iParm2);
ulong FUN_004041cd(long param_1,ulong param_2,long param_3,ulong param_4,uint param_5,uint param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_004050ef(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_0040535f(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_004053a0(uint uParm1,undefined8 uParm2);
void FUN_004053c4(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_00405458(undefined8 uParm1,char cParm2);
void FUN_00405482(undefined8 uParm1);
void FUN_004054a1(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_0040553c(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00405568(uint uParm1,undefined8 uParm2);
void FUN_00405591(undefined8 uParm1);
long FUN_004055b0(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00405620(undefined8 uParm1);
ulong FUN_00405641(uint uParm1);
void FUN_0040568e(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_00405afd(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
void FUN_00405bd0(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00405c87(undefined8 uParm1);
long FUN_00405ca1(long lParm1);
long FUN_00405cd6(long lParm1,long lParm2);
void FUN_00405d37(void);
void FUN_00405d61(void);
void FUN_00405d68(uint uParm1,uint uParm2);
undefined8 FUN_00405d91(ulong uParm1,ulong uParm2);
ulong FUN_00405e10(uint uParm1);
void FUN_00405e39(void);
void FUN_00405e6e(uint uParm1);
void FUN_00405ee3(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_00405f68(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_0040604d(undefined8 uParm1,undefined8 uParm2,uint uParm3);
void FUN_00406078(long lParm1,int *piParm2);
ulong FUN_0040625b(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_00406844(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_00406912(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
ulong FUN_004070db(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,uint param_6,uint param_7);
void FUN_0040716b(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
undefined8 FUN_004071b5(long lParm1);
ulong FUN_004071e6(uint *puParm1,byte *pbParm2,long lParm3,undefined8 uParm4);
void FUN_00407269(undefined8 uParm1);
undefined8 FUN_00407292(long lParm1,long lParm2);
undefined8 FUN_004072fb(int iParm1);
void FUN_00407321(long lParm1,long lParm2);
void FUN_00407390(long lParm1,long lParm2);
ulong FUN_00407408(long lParm1,long lParm2);
void FUN_0040745f(undefined8 uParm1);
void FUN_00407484(undefined8 uParm1);
void FUN_004074a9(undefined8 uParm1,undefined8 uParm2);
void FUN_004074d4(long lParm1);
void FUN_00407524(long lParm1,long lParm2);
void FUN_0040758f(long lParm1,long lParm2);
ulong FUN_004075fa(long lParm1,long lParm2);
ulong FUN_0040766e(long lParm1,long lParm2);
undefined8 FUN_004076b7(void);
ulong FUN_004076ca(long param_1,undefined8 param_2,long param_3,long param_4,undefined8 param_5,byte param_6,long param_7);
ulong FUN_00407813(long lParm1,undefined8 uParm2,long lParm3,long lParm4,byte bParm5,long lParm6);
undefined8 FUN_004079ee(long lParm1,ulong uParm2);
void FUN_00407b65(undefined8 uParm1,undefined8 uParm2,undefined8 *puParm3,long lParm4,char cParm5,long lParm6);
void FUN_00407c56(long *plParm1);
undefined8 FUN_00407f83(long *plParm1);
long FUN_00408a25(long *plParm1,long lParm2,uint *puParm3);
void FUN_00408b58(long *plParm1);
void FUN_00408c27(long *plParm1);
ulong FUN_00408cc9(byte **ppbParm1,byte *pbParm2,uint uParm3);
ulong FUN_0040998e(long *plParm1,long lParm2);
ulong FUN_00409b10(long *plParm1);
void FUN_00409c9a(long lParm1);
ulong FUN_00409ce8(long lParm1,ulong uParm2,uint uParm3);
undefined8 FUN_00409e6e(long *plParm1,long lParm2);
undefined8 FUN_00409ed8(undefined8 *puParm1,undefined8 uParm2);
undefined8 FUN_00409f5f(undefined8 *puParm1,long lParm2,long lParm3);
undefined8 FUN_0040a03a(long *plParm1,long lParm2);
undefined8 FUN_0040a117(long *plParm1,long lParm2,long lParm3);
undefined8 FUN_0040a500(long *plParm1,long lParm2,long lParm3);
undefined8 FUN_0040a808(long *plParm1,long lParm2);
ulong FUN_0040abca(long *plParm1,long lParm2);
undefined8 FUN_0040adb7(long *plParm1,undefined8 uParm2);
undefined8 FUN_0040ae6c(long lParm1,long lParm2);
long FUN_0040aefb(long lParm1,long lParm2);
void FUN_0040afb3(long lParm1,long lParm2);
long FUN_0040b033(long *plParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_0040b3ce(long lParm1,uint uParm2);
ulong * FUN_0040b428(undefined4 *puParm1,long lParm2,long lParm3);
ulong * FUN_0040b549(undefined4 *puParm1,long lParm2,long lParm3,uint uParm4);
undefined8 FUN_0040b676(long *plParm1,ulong *puParm2,ulong uParm3);
void FUN_0040b82a(long lParm1);
long FUN_0040b8ce(long *plParm1,long lParm2,undefined8 uParm3);
long FUN_0040baa0(long *plParm1,long lParm2,uint uParm3,undefined8 uParm4);
undefined1 * FUN_0040bd84(undefined8 uParm1,undefined8 uParm2,long lParm3);
undefined8 FUN_0040be10(long *plParm1);
void FUN_0040bf09(long **pplParm1,long lParm2,long lParm3);
void FUN_0040c5eb(long *plParm1,undefined8 uParm2);
ulong FUN_0040c84e(long *plParm1,undefined8 uParm2,undefined8 uParm3,ulong uParm4);
undefined8 FUN_0040cbca(long *plParm1,ulong uParm2);
void FUN_0040cf47(long lParm1);
void FUN_0040d0ca(long *plParm1);
ulong FUN_0040d15a(long *plParm1);
void FUN_0040d4a2(long *plParm1);
ulong FUN_0040d71b(long *plParm1);
ulong FUN_0040daa0(long **pplParm1,code *pcParm2,undefined8 uParm3);
ulong FUN_0040db7e(long *plParm1,code *pcParm2,undefined8 uParm3);
undefined8 FUN_0040dc39(long lParm1,long lParm2);
ulong FUN_0040ddb1(undefined8 uParm1,long lParm2);
long FUN_0040de8d(undefined4 *puParm1,long *plParm2,long lParm3);
undefined8 FUN_0040e07e(long *plParm1,long lParm2);
undefined8 FUN_0040e16c(undefined8 uParm1,long lParm2);
ulong FUN_0040e219(long lParm1,long lParm2);
ulong FUN_0040e48e(long *plParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
long FUN_0040e9be(long *plParm1,long lParm2,uint uParm3);
long FUN_0040ea57(long *plParm1,long lParm2,undefined4 uParm3);
undefined8 FUN_0040eb89(long lParm1);
ulong FUN_0040ecc7(long lParm1);
ulong FUN_0040edaa(undefined8 *puParm1,long *plParm2,long lParm3,char cParm4);
void FUN_0040f17c(undefined8 uParm1,long lParm2,undefined8 uParm3);
undefined8 FUN_0040f1bf(long *plParm1,long lParm2,ulong uParm3);
ulong FUN_0040fa50(byte *pbParm1,long lParm2,ulong uParm3);
long FUN_0040fc73(undefined8 uParm1,long *plParm2,ulong uParm3,int *piParm4);
long FUN_0040fd9f(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_0040ffa1(undefined8 uParm1,undefined8 *puParm2,long lParm3,undefined8 uParm4,long lParm5,int *piParm6);
long FUN_00410160(long lParm1,long *plParm2,undefined8 *puParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_00410a05(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,undefined8 uParm5,int *piParm6);
long FUN_00410b99(long lParm1,long lParm2,undefined8 uParm3,undefined8 *puParm4,ulong uParm5,undefined4 *puParm6);
ulong FUN_004110ca(byte bParm1,long lParm2);
undefined8 FUN_00411101(ulong uParm1,undefined8 uParm2,long lParm3,long *plParm4,int *piParm5,int *piParm6);
undefined8 FUN_004114c2(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,byte *pbParm4);
long FUN_00411521(long lParm1,long lParm2,long lParm3,ulong uParm4,int *piParm5);
undefined8 FUN_00411e56(undefined4 *param_1,long param_2,undefined *param_3,int param_4,undefined8 param_5,undefined8 param_6,char param_7);
undefined8 FUN_00411fa1(undefined4 *puParm1,long lParm2,char *pcParm3);
undefined8 FUN_0041210d(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,byte *pbParm4);
undefined8 FUN_00412167(long lParm1,undefined8 uParm2,long lParm3,long *plParm4,undefined *puParm5,ulong uParm6);
long FUN_00412ace(long lParm1,undefined8 uParm2,undefined8 uParm3,char *pcParm4,byte bParm5,int *piParm6);
long FUN_00412d66(undefined8 uParm1,byte *pbParm2,undefined8 uParm3);
void FUN_00412e4d(undefined8 *puParm1);
void FUN_00412e87(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined uParm4);
undefined8 * FUN_00412ebe(long lParm1,undefined8 *puParm2,undefined8 *puParm3,undefined8 *puParm4);
undefined8 FUN_0041300a(long lParm1,long lParm2);
void FUN_0041304d(undefined8 *puParm1);
undefined8 FUN_004130b2(undefined8 uParm1,long lParm2);
long * FUN_004130d9(long **pplParm1,undefined8 uParm2);
void FUN_00413205(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_00413256(long param_1,undefined8 param_2,ulong param_3,ulong param_4,ulong param_5,undefined8 param_6,ulong *param_7,char param_8);
ulong FUN_004135cb(ulong *puParm1,long lParm2,ulong uParm3,int iParm4);
ulong FUN_00413871(long *param_1,long param_2,long param_3,long param_4,long param_5,undefined8 param_6,ulong param_7,undefined8 *param_8,uint param_9);
ulong FUN_00414632(long lParm1);
long FUN_00414957(long lParm1,char cParm2,long *plParm3);
undefined8 FUN_00414e73(long *plParm1,long lParm2,uint uParm3);
undefined8 FUN_00414f3a(long lParm1,long lParm2,undefined8 uParm3);
long FUN_00414fdd(long param_1,undefined8 param_2,long param_3,long *param_4,long param_5,long param_6,long param_7);
ulong FUN_0041551c(long *plParm1,undefined8 uParm2,undefined8 uParm3,long lParm4,undefined8 uParm5,undefined8 uParm6);
undefined8 FUN_004156ec(long *plParm1,undefined8 *puParm2,long lParm3,undefined8 uParm4,undefined8 *puParm5);
undefined8 FUN_0041583a(long *plParm1,long lParm2,ulong uParm3,long *plParm4,char cParm5);
undefined8 FUN_00415c4a(long *plParm1);
void FUN_00415ce0(long *plParm1,long lParm2,long lParm3,long lParm4,long lParm5,long lParm6);
ulong FUN_00415e84(long lParm1,long *plParm2);
undefined8 FUN_00416025(long lParm1,long *plParm2,long lParm3,undefined8 uParm4);
ulong FUN_00416230(long lParm1,long lParm2);
ulong FUN_0041631a(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_00416474(long lParm1,long *plParm2,long lParm3,long lParm4);
ulong FUN_00416653(long lParm1,long lParm2,undefined8 uParm3);
ulong FUN_00416782(long *plParm1,long lParm2,undefined8 uParm3,undefined8 uParm4);
undefined8 FUN_00416a10(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_00416b7c(long lParm1,uint uParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_00416dfb(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long lParm5,undefined8 uParm6);
ulong FUN_00416ec8(long *plParm1,long lParm2,undefined8 uParm3,long lParm4,long lParm5,long lParm6);
ulong FUN_00417241(long lParm1,long *plParm2,long lParm3,long lParm4);
ulong FUN_00417716(long lParm1,long *plParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_004177f0(int *piParm1,long lParm2,long lParm3);
long FUN_00417966(int *piParm1,long lParm2,long lParm3);
long FUN_00417bec(int *piParm1,long lParm2);
ulong FUN_00417c96(long lParm1,long lParm2,undefined8 uParm3);
ulong FUN_00417d91(long lParm1,long lParm2);
ulong FUN_00418113(long lParm1,long lParm2);
ulong FUN_0041867f(long lParm1,long lParm2,long lParm3);
ulong FUN_00418bf6(undefined8 uParm1,long *plParm2,undefined8 *puParm3,undefined8 uParm4,long lParm5);
long FUN_00418ccd(long *plParm1,long lParm2,long lParm3,uint uParm4);
ulong FUN_00418d59(long param_1,long *param_2,long param_3,long param_4,undefined8 param_5,long param_6,uint param_7);
ulong FUN_004194e1(long lParm1,long lParm2,long lParm3,undefined8 uParm4);
ulong FUN_004197a4(long lParm1,undefined8 *puParm2,undefined8 uParm3,uint uParm4);
ulong FUN_00419918(long *plParm1,undefined8 uParm2,long lParm3,long lParm4,uint uParm5);
ulong FUN_00419add(long lParm1,undefined8 uParm2,long lParm3,undefined8 uParm4,uint uParm5);
undefined8 FUN_00419ebd(long lParm1,long lParm2);
long FUN_0041a8e5(long *plParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_0041b18b(long *plParm1,long lParm2,long lParm3,long lParm4);
undefined8 FUN_0041b5c7(long lParm1,undefined8 *puParm2,long lParm3);
ulong FUN_0041b7a2(long lParm1,int iParm2);
undefined8 FUN_0041b92a(long lParm1,undefined4 uParm2,ulong uParm3);
void FUN_0041ba68(long lParm1);
void FUN_0041bb79(long lParm1);
undefined8 FUN_0041bbba(long lParm1,undefined8 uParm2,long lParm3,long lParm4,long lParm5);
long FUN_0041beda(long lParm1,long lParm2);
undefined8 FUN_0041bfb0(long lParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 * FUN_0041c128(long lParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0041c234(undefined8 *puParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
ulong FUN_0041c29c(long lParm1);
ulong FUN_0041c408(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_0041c527(void);
undefined8 FUN_0041c538(void);
ulong FUN_0041c546(uint uParm1,uint uParm2);
ulong FUN_0041c57d(byte *pbParm1,byte *pbParm2);
undefined8 FUN_0041c5f5(undefined8 uParm1);
char * FUN_0041c680(char *pcParm1);
ulong FUN_0041c6e9(long lParm1);
undefined8 FUN_0041c737(void);
ulong FUN_0041c744(uint uParm1);
undefined * FUN_0041c815(void);
char * FUN_0041cbda(void);
ulong FUN_0041cca8(undefined8 uParm1,ulong uParm2);
ulong FUN_0041ccd2(long lParm1,int iParm2,undefined8 uParm3,code *pcParm4,ulong uParm5);
void FUN_0041ce8c(undefined8 uParm1,uint *puParm2);
void FUN_0041cecd(undefined8 uParm1);
undefined8 FUN_0041cef0(undefined8 uParm1);
void FUN_0041cf57(undefined8 uParm1,uint uParm2,undefined4 uParm3,int iParm4,undefined8 uParm5);
void FUN_0041cfe6(undefined8 uParm1,uint uParm2,uint uParm3,uint uParm4);
void FUN_0041d018(uint uParm1);
ulong FUN_0041d03e(undefined8 uParm1);
ulong FUN_0041d0f3(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_0041d3ac(undefined8 uParm1);
void FUN_0041d3d1(void);
ulong FUN_0041d3e0(long lParm1);
undefined8 FUN_0041d4b0(undefined8 uParm1);
void FUN_0041d4cf(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0041d4f4(long lParm1,long lParm2);
ulong FUN_0041d5ab(long lParm1,uint uParm2);
void FUN_0041d6e7(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0041d70c(long lParm1,long lParm2);
ulong FUN_0041d79c(void);
long FUN_0041d7ed(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
int * FUN_0041da36(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_0041e518(ulong uParm1,long lParm2,long lParm3);
long FUN_0041e78b(int *param_1,long *param_2);
long FUN_0041e962(undefined auParm1 [16],int *piParm2,long *plParm3);
undefined8 FUN_0041ecdd(int iParm1,long lParm2,uint *puParm3,long lParm4,uint uParm5);
void FUN_0041f4e0(uint param_1);
void FUN_0041f52a(undefined auParm1 [16],uint uParm2);
ulong FUN_0041f578(void);
ulong FUN_0041f8ba(undefined auParm1 [16],undefined8 uParm2,undefined8 uParm3);
ulong FUN_0041fc44(char *pcParm1,long lParm2);
undefined8 FUN_0041fc9d(long param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
char * FUN_0041ff60(char *pcParm1,char **ppcParm2,long lParm3,undefined8 uParm4);
ulong FUN_004273f9(uint uParm1);
ulong FUN_00427418(char *pcParm1);
void FUN_0042747a(undefined auParm1 [16],int *piParm2,undefined8 uParm3);
void FUN_00427503(int *param_1);
undefined8 * FUN_00427598(undefined8 uParm1);
undefined8 FUN_004275df(undefined8 uParm1,undefined8 uParm2);
long FUN_00427622(long lParm1);
ulong FUN_00427634(undefined8 *puParm1,ulong uParm2);
void FUN_004277e2(undefined8 uParm1);
ulong FUN_0042780e(undefined8 *puParm1);
ulong * FUN_00427851(ulong uParm1,ulong uParm2);
undefined8 * FUN_004278b3(undefined8 uParm1,undefined8 uParm2);
void FUN_004278fa(long lParm1,ulong uParm2,ulong uParm3);
long FUN_00427b60(long lParm1,ulong uParm2);
void FUN_00427c4f(undefined8 *puParm1,long lParm2,long lParm3);
void FUN_00427ce9(ulong *puParm1,ulong uParm2,ulong uParm3);
void FUN_00427e2e(long *plParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_00427e85(long *plParm1);
undefined8 FUN_00427ed5(undefined8 uParm1);
undefined8 FUN_00427eef(long lParm1,undefined8 uParm2);
void FUN_00427f33(ulong *puParm1,long *plParm2);
void FUN_00428582(long lParm1);
ulong FUN_00428b0a(uint uParm1);
ulong FUN_00428b1a(ulong uParm1,long lParm2);
void FUN_00428b4e(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00428b89(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00428bda(ulong uParm1,ulong uParm2);
void FUN_00428bf5(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00428c1d(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00428cca(uint *puParm1,ulong *puParm2);
undefined8 FUN_00429466(char *pcParm1,ulong *puParm2,char **ppcParm3);
ulong FUN_0042a736(void);
ulong FUN_0042a76d(void);
long FUN_0042a798(undefined8 uParm1,undefined8 uParm2);
void FUN_0042a880(void);
undefined8 _DT_FINI(void);

