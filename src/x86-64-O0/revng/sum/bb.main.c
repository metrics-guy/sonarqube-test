typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_9_ret_type;
struct indirect_placeholder_9_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_1(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_5(uint64_t param_0, uint64_t param_1);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_7(void);
extern void indirect_placeholder_6(uint64_t param_0);
extern void indirect_placeholder_2(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_9_ret_type indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
typedef _Bool bool;
void bb_main(uint64_t rsi, uint64_t rdi) {
    struct indirect_placeholder_9_ret_type var_14;
    uint32_t var_20;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint32_t *var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint32_t *var_10;
    uint64_t local_sp_3;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_16;
    uint32_t var_17;
    uint64_t var_18;
    uint64_t local_sp_1;
    unsigned char *var_19;
    uint64_t local_sp_0;
    uint64_t var_21;
    uint64_t var_24;
    uint64_t var_25;
    uint32_t var_26;
    uint64_t local_sp_2;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t *var_29;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint32_t var_15;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = var_0 + (-8L);
    *(uint64_t *)var_3 = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_4 = (uint32_t *)(var_0 + (-60L));
    *var_4 = (uint32_t)rdi;
    var_5 = var_0 + (-72L);
    var_6 = (uint64_t *)var_5;
    *var_6 = rsi;
    var_7 = (uint64_t *)(var_0 + (-40L));
    *var_7 = 4204743UL;
    var_8 = **(uint64_t **)var_5;
    *(uint64_t *)(var_0 + (-80L)) = 4206046UL;
    indirect_placeholder_6(var_8);
    *(uint64_t *)(var_0 + (-88L)) = 4206061UL;
    indirect_placeholder_1();
    *(uint64_t *)(var_0 + (-96L)) = 4206071UL;
    indirect_placeholder_1();
    var_9 = var_0 + (-104L);
    *(uint64_t *)var_9 = 4206101UL;
    indirect_placeholder_1();
    *(unsigned char *)4293520UL = (unsigned char)'\x00';
    var_10 = (uint32_t *)(var_0 + (-44L));
    local_sp_3 = var_9;
    while (1U)
        {
            var_11 = *var_6;
            var_12 = (uint64_t)*var_4;
            var_13 = local_sp_3 + (-8L);
            *(uint64_t *)var_13 = 4206303UL;
            var_14 = indirect_placeholder_9(4276672UL, 4277222UL, var_11, var_12, 0UL);
            var_15 = (uint32_t)var_14.field_0;
            *var_10 = var_15;
            local_sp_0 = var_13;
            local_sp_2 = var_13;
            local_sp_3 = var_13;
            switch_state_var = 0;
            switch (var_15) {
              case 115U:
                {
                    *var_7 = 4205352UL;
                    continue;
                }
                break;
              case 4294967295U:
                {
                    var_16 = var_14.field_1;
                    var_17 = *var_4 - *(uint32_t *)4293368UL;
                    *(uint32_t *)(var_0 + (-48L)) = var_17;
                    if ((int)var_17 <= (int)0U) {
                        var_19 = (unsigned char *)(var_0 + (-25L));
                        *var_19 = (unsigned char)'\x01';
                        var_20 = *(uint32_t *)4293368UL;
                        loop_state_var = 2U;
                        switch_state_var = 1;
                        break;
                    }
                    var_18 = local_sp_3 + (-16L);
                    *(uint64_t *)var_18 = 4206354UL;
                    indirect_placeholder_1();
                    *(unsigned char *)(var_0 + (-25L)) = (unsigned char)var_17;
                    local_sp_1 = var_18;
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              default:
                {
                    if ((int)var_15 <= (int)115U) {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    if (var_15 == 114U) {
                        *var_7 = 4204743UL;
                        continue;
                    }
                    if ((int)var_15 <= (int)114U) {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    switch_state_var = 0;
                    switch (var_15) {
                      case 4294967166U:
                        {
                            *(uint64_t *)(local_sp_3 + (-16L)) = 4206191UL;
                            indirect_placeholder_5(var_3, 0UL);
                            abort();
                        }
                        break;
                      case 4294967165U:
                        {
                            var_27 = *(uint64_t *)4293216UL;
                            var_28 = local_sp_3 + (-24L);
                            var_29 = (uint64_t *)var_28;
                            *var_29 = 0UL;
                            *(uint64_t *)(local_sp_3 + (-32L)) = 4206249UL;
                            indirect_placeholder_8(0UL, var_27, 4276432UL, 4277158UL, 4277188UL, 4277204UL);
                            *var_29 = 4206263UL;
                            indirect_placeholder_1();
                            local_sp_2 = var_28;
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      default:
                        {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 1U:
        {
            *(uint64_t *)(local_sp_2 + (-8L)) = 4206273UL;
            indirect_placeholder_5(var_3, 1UL);
            abort();
        }
        break;
      case 2U:
      case 0U:
        {
            switch (loop_state_var) {
              case 2U:
                {
                    local_sp_1 = local_sp_0;
                    while ((int)var_20 >= (int)*var_4)
                        {
                            var_24 = *(uint64_t *)(*var_6 + ((uint64_t)var_20 << 3UL));
                            var_25 = local_sp_0 + (-8L);
                            *(uint64_t *)var_25 = 4206405UL;
                            indirect_placeholder_1();
                            *var_19 = ((var_24 & (uint64_t)*var_19) != 0UL);
                            var_26 = *(uint32_t *)4293368UL + 1U;
                            *(uint32_t *)4293368UL = var_26;
                            var_20 = var_26;
                            local_sp_0 = var_25;
                            local_sp_1 = local_sp_0;
                        }
                }
                break;
              case 0U:
                {
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4206474UL;
                    var_21 = indirect_placeholder_7();
                    if (*(unsigned char *)4293520UL != '\x00' & (uint64_t)((uint32_t)var_21 + 1U) == 0UL) {
                        *(uint64_t *)(local_sp_1 + (-16L)) = 4206499UL;
                        var_22 = indirect_placeholder_3(4277162UL, 3UL, 0UL);
                        *(uint64_t *)(local_sp_1 + (-24L)) = 4206507UL;
                        indirect_placeholder_1();
                        var_23 = (uint64_t)*(uint32_t *)var_22;
                        *(uint64_t *)(local_sp_1 + (-32L)) = 4206534UL;
                        indirect_placeholder_2(0UL, var_22, 4277166UL, var_23, 1UL, var_16, 0UL);
                    }
                    return;
                }
                break;
            }
        }
        break;
    }
}
