typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_56_ret_type;
struct indirect_placeholder_55_ret_type;
struct indirect_placeholder_57_ret_type;
struct indirect_placeholder_58_ret_type;
struct indirect_placeholder_56_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_55_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_57_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_58_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_3(uint64_t param_0);
extern uint64_t indirect_placeholder_16(uint64_t param_0);
extern uint64_t indirect_placeholder_31(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
extern struct indirect_placeholder_56_ret_type indirect_placeholder_56(uint64_t param_0);
extern struct indirect_placeholder_55_ret_type indirect_placeholder_55(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_57_ret_type indirect_placeholder_57(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_58_ret_type indirect_placeholder_58(uint64_t param_0, uint64_t param_1, uint64_t param_2);
typedef _Bool bool;
void bb_find_occurs_in_text(uint64_t r10, uint64_t rbx, uint64_t rcx, uint64_t rdi, uint64_t r8, uint64_t r9) {
    uint64_t var_112;
    struct indirect_placeholder_55_ret_type var_67;
    uint64_t var_151;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint32_t *var_5;
    uint32_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t *var_10;
    uint64_t **var_11;
    uint64_t var_12;
    uint64_t *var_13;
    uint64_t var_14;
    uint64_t *var_15;
    uint64_t local_sp_4;
    uint64_t var_16;
    uint64_t var_168;
    uint64_t var_160;
    uint64_t var_159;
    uint64_t var_147;
    uint64_t var_139;
    uint64_t local_sp_27;
    uint64_t local_sp_13;
    uint64_t local_sp_18;
    uint64_t rcx3_3_be;
    uint64_t r85_4;
    uint64_t local_sp_8_be;
    uint64_t rbx2_3;
    uint64_t var_121;
    uint64_t var_113;
    uint64_t var_103;
    uint64_t var_97;
    uint64_t var_95;
    uint64_t var_89;
    uint64_t r96_4;
    uint64_t r101_4;
    uint64_t var_80;
    uint64_t var_74;
    uint64_t var_60;
    uint64_t r96_5;
    uint64_t var_73;
    uint64_t r101_0;
    uint64_t rcx3_0;
    uint64_t local_sp_0;
    uint64_t r85_0;
    uint64_t r96_0;
    uint64_t var_72;
    uint64_t var_61;
    struct indirect_placeholder_56_ret_type var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t local_sp_12;
    uint64_t var_33;
    uint64_t var_25;
    uint64_t var_24;
    uint64_t local_sp_1;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t local_sp_2;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t local_sp_3;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_34;
    uint64_t *var_35;
    uint64_t var_36;
    uint64_t *var_37;
    uint64_t *var_38;
    uint64_t *var_39;
    uint64_t *var_40;
    uint64_t *var_41;
    uint64_t *var_42;
    uint64_t var_43;
    uint64_t *var_44;
    uint64_t var_45;
    uint64_t *var_46;
    uint64_t *var_47;
    uint64_t *var_48;
    uint64_t r101_5;
    uint64_t var_49;
    uint64_t rbx2_4;
    uint64_t r101_1;
    uint64_t rcx3_5;
    uint64_t rbx2_0;
    uint64_t local_sp_28;
    uint64_t rcx3_1;
    uint64_t r85_5;
    uint64_t local_sp_5;
    uint64_t r85_1;
    uint64_t r96_1;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    struct indirect_placeholder_57_ret_type var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t r101_2;
    uint64_t rbx2_1;
    uint64_t rcx3_2;
    uint64_t local_sp_6;
    uint64_t r85_2;
    uint64_t r96_2;
    uint64_t local_sp_7;
    uint64_t local_sp_8_ph;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t r101_3;
    uint64_t rcx3_4;
    uint64_t rbx2_2;
    uint64_t rcx3_3;
    uint64_t local_sp_8;
    uint64_t r85_3;
    uint64_t r96_3;
    uint64_t var_81;
    uint64_t var_82;
    struct indirect_placeholder_58_ret_type var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t local_sp_9;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t local_sp_10;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_96;
    uint64_t local_sp_11;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t var_100;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t local_sp_17;
    uint64_t local_sp_15_ph;
    uint64_t var_107;
    uint64_t var_108;
    uint64_t var_109;
    unsigned char **var_110;
    uint64_t local_sp_16;
    uint64_t var_114;
    uint64_t var_115;
    uint64_t var_116;
    uint64_t var_117;
    uint64_t var_118;
    uint64_t var_119;
    uint64_t var_120;
    uint64_t var_111;
    uint64_t local_sp_19;
    uint64_t var_122;
    uint64_t var_123;
    uint64_t local_sp_20;
    uint64_t var_124;
    uint64_t var_125;
    uint64_t var_126;
    uint64_t var_127;
    uint64_t var_130;
    uint64_t var_128;
    uint64_t var_129;
    uint64_t var_131;
    uint64_t local_sp_21;
    uint64_t var_132;
    uint64_t local_sp_22_ph_be;
    uint64_t local_sp_22_ph;
    uint64_t var_133;
    uint64_t var_134;
    uint64_t var_135;
    uint64_t local_sp_24_ph;
    unsigned char **var_136;
    uint64_t var_138;
    uint64_t local_sp_23;
    uint64_t var_140;
    uint64_t var_141;
    uint64_t var_142;
    uint64_t var_143;
    uint64_t var_144;
    uint64_t var_145;
    uint64_t var_146;
    uint64_t var_137;
    uint64_t var_148;
    uint64_t var_149;
    uint64_t var_150;
    uint64_t local_sp_25;
    uint64_t var_152;
    uint64_t var_153;
    uint64_t local_sp_26_ph;
    uint64_t var_154;
    uint64_t var_155;
    uint64_t var_156;
    uint64_t var_157;
    uint64_t var_158;
    uint64_t local_sp_26;
    uint64_t var_161;
    uint64_t var_162;
    uint64_t var_163;
    uint64_t var_164;
    uint64_t var_165;
    uint64_t var_166;
    uint64_t var_167;
    uint64_t var_169;
    uint64_t var_170;
    uint64_t var_171;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = var_0 + (-8L);
    *(uint64_t *)var_3 = var_1;
    var_4 = var_0 + (-152L);
    var_5 = (uint32_t *)(var_0 + (-140L));
    var_6 = (uint32_t)rdi;
    *var_5 = var_6;
    var_7 = *(uint64_t *)4418088UL + ((uint64_t)var_6 << 4UL);
    var_8 = var_0 + (-96L);
    var_9 = (uint64_t *)var_8;
    *var_9 = var_7;
    var_10 = (uint64_t *)(var_0 + (-48L));
    *var_10 = 0UL;
    var_11 = (uint64_t **)var_8;
    var_12 = **var_11;
    var_13 = (uint64_t *)(var_0 + (-32L));
    *var_13 = var_12;
    var_14 = var_0 + (-40L);
    var_15 = (uint64_t *)var_14;
    *var_15 = var_12;
    local_sp_4 = var_4;
    var_16 = var_12;
    rcx3_0 = 0UL;
    local_sp_1 = var_4;
    r101_1 = r10;
    rbx2_0 = rbx;
    rcx3_1 = rcx;
    r85_1 = r8;
    r96_1 = r9;
    if (*(unsigned char *)4416641UL != '\x00') {
        var_17 = *(uint64_t *)(*var_9 + 8UL);
        var_18 = helper_cc_compute_c_wrapper(var_16 - var_17, var_17, var_2, 17U);
        local_sp_2 = local_sp_1;
        while (var_18 != 0UL)
            {
                var_19 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)var_14;
                *(uint64_t *)(local_sp_1 + (-8L)) = 4207880UL;
                var_20 = indirect_placeholder_16(var_19);
                var_21 = (uint64_t)(unsigned char)var_20;
                var_22 = local_sp_1 + (-16L);
                *(uint64_t *)var_22 = 4207890UL;
                var_23 = indirect_placeholder_16(var_21);
                local_sp_1 = var_22;
                local_sp_2 = var_22;
                if ((uint64_t)(uint32_t)var_23 == 0UL) {
                    break;
                }
                var_24 = *var_15 + 1UL;
                *var_15 = var_24;
                var_16 = var_24;
                var_17 = *(uint64_t *)(*var_9 + 8UL);
                var_18 = helper_cc_compute_c_wrapper(var_16 - var_17, var_17, var_2, 17U);
                local_sp_2 = local_sp_1;
            }
        *var_10 = (*var_15 - *var_13);
        var_25 = *var_15;
        local_sp_3 = local_sp_2;
        var_26 = *(uint64_t *)(*var_9 + 8UL);
        var_27 = helper_cc_compute_c_wrapper(var_25 - var_26, var_26, var_2, 17U);
        local_sp_4 = local_sp_3;
        while (var_27 != 0UL)
            {
                var_28 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)var_14;
                *(uint64_t *)(local_sp_3 + (-8L)) = 4207944UL;
                var_29 = indirect_placeholder_16(var_28);
                var_30 = (uint64_t)(unsigned char)var_29;
                var_31 = local_sp_3 + (-16L);
                *(uint64_t *)var_31 = 4207954UL;
                var_32 = indirect_placeholder_16(var_30);
                local_sp_3 = var_31;
                local_sp_4 = var_31;
                if ((uint64_t)(uint32_t)var_32 == 0UL) {
                    break;
                }
                var_33 = *var_15 + 1UL;
                *var_15 = var_33;
                var_25 = var_33;
                var_26 = *(uint64_t *)(*var_9 + 8UL);
                var_27 = helper_cc_compute_c_wrapper(var_25 - var_26, var_26, var_2, 17U);
                local_sp_4 = local_sp_3;
            }
    }
    var_34 = **var_11;
    var_35 = (uint64_t *)(var_0 + (-16L));
    *var_35 = var_34;
    var_36 = var_0 + (-56L);
    var_37 = (uint64_t *)var_36;
    var_38 = (uint64_t *)(var_0 + (-88L));
    var_39 = (uint64_t *)(var_0 + (-64L));
    var_40 = (uint64_t *)(var_0 + (-104L));
    var_41 = (uint64_t *)(var_0 + (-72L));
    var_42 = (uint64_t *)(var_0 + (-80L));
    var_43 = var_0 + (-24L);
    var_44 = (uint64_t *)var_43;
    var_45 = var_0 + (-136L);
    var_46 = (uint64_t *)var_45;
    var_47 = (uint64_t *)(var_0 + (-128L));
    var_48 = (uint64_t *)(var_0 + (-112L));
    var_49 = var_34;
    local_sp_5 = local_sp_4;
    while (1U)
        {
            var_50 = *(uint64_t *)(*var_9 + 8UL);
            var_51 = helper_cc_compute_c_wrapper(var_49 - var_50, var_50, var_2, 17U);
            r101_2 = r101_1;
            rbx2_1 = rbx2_0;
            rcx3_2 = rcx3_1;
            local_sp_6 = local_sp_5;
            r85_2 = r85_1;
            r96_2 = r96_1;
            if (var_51 == 0UL) {
                return;
            }
            *var_37 = *var_35;
            var_52 = *(uint64_t *)(*var_9 + 8UL);
            *var_38 = var_52;
            var_73 = var_52;
            if (*(uint64_t *)4416704UL == 0UL) {
                *var_39 = var_73;
                var_74 = var_73;
                local_sp_7 = local_sp_6;
                r101_3 = r101_2;
                rbx2_2 = rbx2_1;
                rcx3_3 = rcx3_2;
                r85_3 = r85_2;
                r96_3 = r96_2;
                local_sp_8_ph = local_sp_7;
                while (var_74 <= *var_37)
                    {
                        var_75 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(var_74 + (-1L));
                        *(uint64_t *)(local_sp_7 + (-8L)) = 4208219UL;
                        var_76 = indirect_placeholder_16(var_75);
                        var_77 = (uint64_t)(unsigned char)var_76;
                        var_78 = local_sp_7 + (-16L);
                        *(uint64_t *)var_78 = 4208229UL;
                        var_79 = indirect_placeholder_16(var_77);
                        local_sp_7 = var_78;
                        local_sp_8_ph = var_78;
                        if ((uint64_t)(uint32_t)var_79 == 0UL) {
                            break;
                        }
                        var_80 = *var_39 + (-1L);
                        *var_39 = var_80;
                        var_74 = var_80;
                        local_sp_8_ph = local_sp_7;
                    }
                local_sp_8 = local_sp_8_ph;
                while (1U)
                    {
                        r85_4 = r85_3;
                        rbx2_3 = rbx2_2;
                        r96_4 = r96_3;
                        r101_4 = r101_3;
                        r96_5 = r96_3;
                        r101_5 = r101_3;
                        rbx2_4 = rbx2_2;
                        rcx3_5 = rcx3_3;
                        r85_5 = r85_3;
                        rcx3_4 = rcx3_3;
                        local_sp_9 = local_sp_8;
                        if (*(uint64_t *)4417056UL == 0UL) {
                            var_81 = *var_39 - *var_35;
                            var_82 = local_sp_8 + (-8L);
                            *(uint64_t *)var_82 = 4208302UL;
                            var_83 = indirect_placeholder_58(var_81, 4417064UL, 4417696UL);
                            var_84 = var_83.field_0;
                            var_85 = var_83.field_1;
                            var_86 = var_83.field_2;
                            var_87 = var_83.field_3;
                            *var_40 = var_86;
                            local_sp_13 = var_82;
                            r85_4 = var_81;
                            rbx2_3 = var_85;
                            r96_4 = var_87;
                            r101_4 = var_84;
                            r96_5 = var_87;
                            r101_5 = var_84;
                            rbx2_4 = var_85;
                            rcx3_5 = 0UL;
                            local_sp_28 = var_82;
                            r85_5 = var_81;
                            rcx3_4 = 0UL;
                            switch_state_var = 0;
                            switch (var_86) {
                              case 18446744073709551614UL:
                                {
                                    *(uint64_t *)(local_sp_8 + (-16L)) = 4208318UL;
                                    indirect_placeholder_3(var_3);
                                    abort();
                                }
                                break;
                              case 18446744073709551615UL:
                                {
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              default:
                                {
                                    *var_41 = (*var_35 + **(uint64_t **)4417704UL);
                                    *var_42 = (*var_35 + **(uint64_t **)4417712UL);
                                    var_104 = *var_41;
                                    *var_35 = var_104;
                                    local_sp_18 = local_sp_13;
                                    rcx3_3_be = rcx3_4;
                                    local_sp_8_be = local_sp_13;
                                    r101_3 = r101_4;
                                    rbx2_2 = rbx2_3;
                                    r85_3 = r85_4;
                                    r96_3 = r96_4;
                                    local_sp_15_ph = local_sp_13;
                                    if (*var_42 == *var_41) {
                                        *var_35 = (var_104 + 1UL);
                                    } else {
                                        *var_46 = var_104;
                                        var_105 = *var_42 - *var_41;
                                        *var_47 = var_105;
                                        *var_35 = (*var_35 + var_105);
                                        var_106 = *var_47;
                                        if ((long)var_106 > (long)*(uint64_t *)4417984UL) {
                                            *(uint64_t *)4417984UL = var_106;
                                        }
                                        if (*(unsigned char *)4416641UL != '\x00') {
                                            while (1U)
                                                {
                                                    local_sp_8_be = local_sp_15_ph;
                                                    var_107 = *var_15;
                                                    local_sp_16 = local_sp_15_ph;
                                                    local_sp_18 = local_sp_15_ph;
                                                    while (1U)
                                                        {
                                                            var_108 = *var_46;
                                                            var_109 = helper_cc_compute_c_wrapper(var_107 - var_108, var_108, var_2, 17U);
                                                            if (var_109 != 0UL) {
                                                                loop_state_var = 1U;
                                                                break;
                                                            }
                                                            var_110 = (unsigned char **)var_14;
                                                            if (**var_110 != '\n') {
                                                                loop_state_var = 0U;
                                                                break;
                                                            }
                                                            var_111 = *var_15 + 1UL;
                                                            *var_15 = var_111;
                                                            var_107 = var_111;
                                                            continue;
                                                        }
                                                    switch_state_var = 0;
                                                    switch (loop_state_var) {
                                                      case 1U:
                                                        {
                                                            switch_state_var = 1;
                                                            break;
                                                        }
                                                        break;
                                                      case 0U:
                                                        {
                                                            *(uint64_t *)4418064UL = (*(uint64_t *)4418064UL + 1UL);
                                                            var_112 = *var_15 + 1UL;
                                                            *var_15 = var_112;
                                                            *var_13 = var_112;
                                                            var_113 = *var_15;
                                                            var_114 = *(uint64_t *)(*var_9 + 8UL);
                                                            var_115 = helper_cc_compute_c_wrapper(var_113 - var_114, var_114, var_2, 17U);
                                                            local_sp_17 = local_sp_16;
                                                            while (var_115 != 0UL)
                                                                {
                                                                    var_116 = (uint64_t)(uint32_t)(uint64_t)**var_110;
                                                                    *(uint64_t *)(local_sp_16 + (-8L)) = 4208700UL;
                                                                    var_117 = indirect_placeholder_16(var_116);
                                                                    var_118 = (uint64_t)(unsigned char)var_117;
                                                                    var_119 = local_sp_16 + (-16L);
                                                                    *(uint64_t *)var_119 = 4208710UL;
                                                                    var_120 = indirect_placeholder_16(var_118);
                                                                    local_sp_16 = var_119;
                                                                    local_sp_17 = var_119;
                                                                    if ((uint64_t)(uint32_t)var_120 == 0UL) {
                                                                        break;
                                                                    }
                                                                    var_121 = *var_15 + 1UL;
                                                                    *var_15 = var_121;
                                                                    var_113 = var_121;
                                                                    var_114 = *(uint64_t *)(*var_9 + 8UL);
                                                                    var_115 = helper_cc_compute_c_wrapper(var_113 - var_114, var_114, var_2, 17U);
                                                                    local_sp_17 = local_sp_16;
                                                                }
                                                            *var_10 = (*var_15 - *var_13);
                                                            local_sp_15_ph = local_sp_17;
                                                            continue;
                                                        }
                                                        break;
                                                    }
                                                    if (switch_state_var)
                                                        break;
                                                }
                                            if (*var_15 <= *var_46) {
                                                rcx3_3 = rcx3_3_be;
                                                local_sp_8 = local_sp_8_be;
                                                continue;
                                            }
                                        }
                                        local_sp_19 = local_sp_18;
                                        if (*(uint64_t *)4416672UL != 0UL) {
                                            var_122 = local_sp_18 + (-8L);
                                            *(uint64_t *)var_122 = 4208786UL;
                                            var_123 = indirect_placeholder(var_45, 4418000UL);
                                            local_sp_8_be = var_122;
                                            local_sp_19 = var_122;
                                            if ((uint64_t)(unsigned char)var_123 != 0UL) {
                                                rcx3_3 = rcx3_3_be;
                                                local_sp_8 = local_sp_8_be;
                                                continue;
                                            }
                                        }
                                        local_sp_20 = local_sp_19;
                                        if (*(uint64_t *)4416664UL != 0UL) {
                                            var_124 = local_sp_19 + (-8L);
                                            *(uint64_t *)var_124 = 4208823UL;
                                            var_125 = indirect_placeholder(var_45, 4418032UL);
                                            local_sp_8_be = var_124;
                                            local_sp_20 = var_124;
                                            if ((uint64_t)(unsigned char)var_125 != 1UL) {
                                                rcx3_3 = rcx3_3_be;
                                                local_sp_8 = local_sp_8_be;
                                                continue;
                                            }
                                        }
                                        var_126 = *(uint64_t *)4418112UL;
                                        var_130 = var_126;
                                        local_sp_21 = local_sp_20;
                                        if (var_126 == *(uint64_t *)4418104UL) {
                                            var_127 = *(uint64_t *)4418096UL;
                                            var_128 = local_sp_20 + (-8L);
                                            *(uint64_t *)var_128 = 4208878UL;
                                            var_129 = indirect_placeholder_31(r101_4, rbx2_3, rcx3_4, 48UL, var_127, 4418104UL, r85_4, r96_4);
                                            *(uint64_t *)4418096UL = var_129;
                                            var_130 = *(uint64_t *)4418112UL;
                                            var_131 = var_129;
                                            local_sp_21 = var_128;
                                        } else {
                                            var_131 = *(uint64_t *)4418096UL;
                                        }
                                        var_132 = (var_130 * 48UL) + var_131;
                                        *var_48 = var_132;
                                        local_sp_22_ph = local_sp_21;
                                        local_sp_24_ph = local_sp_21;
                                        local_sp_27 = local_sp_21;
                                        if (*(unsigned char *)4416640UL == '\x00') {
                                            if (*(unsigned char *)4416641UL != '\x00') {
                                                var_169 = *var_48;
                                                var_170 = *var_47;
                                                *(uint64_t *)var_169 = *var_46;
                                                *(uint64_t *)(var_169 + 8UL) = var_170;
                                                *(uint64_t *)(*var_48 + 16UL) = (*var_37 - *var_46);
                                                *(uint64_t *)(*var_48 + 24UL) = (*var_39 - *var_46);
                                                *(uint32_t *)(*var_48 + 40UL) = *var_5;
                                                *(uint64_t *)4418112UL = (*(uint64_t *)4418112UL + 1UL);
                                                rcx3_3_be = var_169;
                                                local_sp_8_be = local_sp_27;
                                                rcx3_3 = rcx3_3_be;
                                                local_sp_8 = local_sp_8_be;
                                                continue;
                                            }
                                            *(uint64_t *)(var_132 + 32UL) = (*var_13 - *var_46);
                                            var_148 = *(uint64_t *)4417992UL;
                                            var_149 = *var_10;
                                            if ((long)var_149 > (long)var_148) {
                                                *(uint64_t *)4417992UL = var_149;
                                            }
                                        } else {
                                            while (1U)
                                                {
                                                    var_133 = *var_15;
                                                    local_sp_23 = local_sp_22_ph;
                                                    local_sp_24_ph = local_sp_22_ph;
                                                    while (1U)
                                                        {
                                                            var_134 = *var_46;
                                                            var_135 = helper_cc_compute_c_wrapper(var_133 - var_134, var_134, var_2, 17U);
                                                            if (var_135 != 0UL) {
                                                                loop_state_var = 1U;
                                                                break;
                                                            }
                                                            var_136 = (unsigned char **)var_14;
                                                            if (**var_136 != '\n') {
                                                                loop_state_var = 0U;
                                                                break;
                                                            }
                                                            var_137 = *var_15 + 1UL;
                                                            *var_15 = var_137;
                                                            var_133 = var_137;
                                                            continue;
                                                        }
                                                    switch_state_var = 0;
                                                    switch (loop_state_var) {
                                                      case 1U:
                                                        {
                                                            switch_state_var = 1;
                                                            break;
                                                        }
                                                        break;
                                                      case 0U:
                                                        {
                                                            *(uint64_t *)4418064UL = (*(uint64_t *)4418064UL + 1UL);
                                                            var_138 = *var_15 + 1UL;
                                                            *var_15 = var_138;
                                                            *var_13 = var_138;
                                                            var_139 = *var_15;
                                                            var_140 = *(uint64_t *)(*var_9 + 8UL);
                                                            var_141 = helper_cc_compute_c_wrapper(var_139 - var_140, var_140, var_2, 17U);
                                                            local_sp_22_ph_be = local_sp_23;
                                                            while (var_141 != 0UL)
                                                                {
                                                                    var_142 = (uint64_t)(uint32_t)(uint64_t)**var_136;
                                                                    *(uint64_t *)(local_sp_23 + (-8L)) = 4209019UL;
                                                                    var_143 = indirect_placeholder_16(var_142);
                                                                    var_144 = (uint64_t)(unsigned char)var_143;
                                                                    var_145 = local_sp_23 + (-16L);
                                                                    *(uint64_t *)var_145 = 4209029UL;
                                                                    var_146 = indirect_placeholder_16(var_144);
                                                                    local_sp_23 = var_145;
                                                                    local_sp_22_ph_be = var_145;
                                                                    if ((uint64_t)(uint32_t)var_146 == 0UL) {
                                                                        break;
                                                                    }
                                                                    var_147 = *var_15 + 1UL;
                                                                    *var_15 = var_147;
                                                                    var_139 = var_147;
                                                                    var_140 = *(uint64_t *)(*var_9 + 8UL);
                                                                    var_141 = helper_cc_compute_c_wrapper(var_139 - var_140, var_140, var_2, 17U);
                                                                    local_sp_22_ph_be = local_sp_23;
                                                                }
                                                            local_sp_22_ph = local_sp_22_ph_be;
                                                            continue;
                                                        }
                                                        break;
                                                    }
                                                    if (switch_state_var)
                                                        break;
                                                }
                                            *(uint64_t *)(*var_48 + 32UL) = *(uint64_t *)4418064UL;
                                        }
                                        local_sp_25 = local_sp_24_ph;
                                        local_sp_27 = local_sp_24_ph;
                                        var_150 = *var_13;
                                        var_151 = var_150;
                                        if (*(unsigned char *)4416641UL != '\x00' & var_150 != *var_37) {
                                            var_152 = *var_39;
                                            var_153 = helper_cc_compute_c_wrapper(var_151 - var_152, var_152, var_2, 17U);
                                            local_sp_26_ph = local_sp_25;
                                            while (var_153 != 0UL)
                                                {
                                                    var_154 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)var_36;
                                                    *(uint64_t *)(local_sp_25 + (-8L)) = 4209176UL;
                                                    var_155 = indirect_placeholder_16(var_154);
                                                    var_156 = (uint64_t)(unsigned char)var_155;
                                                    var_157 = local_sp_25 + (-16L);
                                                    *(uint64_t *)var_157 = 4209186UL;
                                                    var_158 = indirect_placeholder_16(var_156);
                                                    local_sp_25 = var_157;
                                                    local_sp_26_ph = var_157;
                                                    if ((uint64_t)(uint32_t)var_158 == 0UL) {
                                                        break;
                                                    }
                                                    var_159 = *var_37 + 1UL;
                                                    *var_37 = var_159;
                                                    var_151 = var_159;
                                                    var_152 = *var_39;
                                                    var_153 = helper_cc_compute_c_wrapper(var_151 - var_152, var_152, var_2, 17U);
                                                    local_sp_26_ph = local_sp_25;
                                                }
                                            var_160 = *var_37;
                                            local_sp_26 = local_sp_26_ph;
                                            var_161 = *var_39;
                                            var_162 = helper_cc_compute_c_wrapper(var_160 - var_161, var_161, var_2, 17U);
                                            local_sp_27 = local_sp_26;
                                            while (var_162 != 0UL)
                                                {
                                                    var_163 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)var_36;
                                                    *(uint64_t *)(local_sp_26 + (-8L)) = 4209224UL;
                                                    var_164 = indirect_placeholder_16(var_163);
                                                    var_165 = (uint64_t)(unsigned char)var_164;
                                                    var_166 = local_sp_26 + (-16L);
                                                    *(uint64_t *)var_166 = 4209234UL;
                                                    var_167 = indirect_placeholder_16(var_165);
                                                    local_sp_26 = var_166;
                                                    local_sp_27 = var_166;
                                                    if ((uint64_t)(uint32_t)var_167 == 0UL) {
                                                        break;
                                                    }
                                                    var_168 = *var_37 + 1UL;
                                                    *var_37 = var_168;
                                                    var_160 = var_168;
                                                    var_161 = *var_39;
                                                    var_162 = helper_cc_compute_c_wrapper(var_160 - var_161, var_161, var_2, 17U);
                                                    local_sp_27 = local_sp_26;
                                                }
                                        }
                                        var_169 = *var_48;
                                        var_170 = *var_47;
                                        *(uint64_t *)var_169 = *var_46;
                                        *(uint64_t *)(var_169 + 8UL) = var_170;
                                        *(uint64_t *)(*var_48 + 16UL) = (*var_37 - *var_46);
                                        *(uint64_t *)(*var_48 + 24UL) = (*var_39 - *var_46);
                                        *(uint32_t *)(*var_48 + 40UL) = *var_5;
                                        *(uint64_t *)4418112UL = (*(uint64_t *)4418112UL + 1UL);
                                        rcx3_3_be = var_169;
                                        local_sp_8_be = local_sp_27;
                                    }
                                }
                                break;
                            }
                            if (switch_state_var)
                                break;
                        } else {
                            var_88 = *var_35;
                            *var_44 = var_88;
                            var_89 = var_88;
                            var_90 = *var_39;
                            var_91 = helper_cc_compute_c_wrapper(var_89 - var_90, var_90, var_2, 17U);
                            local_sp_10 = local_sp_9;
                            while (var_91 != 0UL)
                                {
                                    var_92 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)var_43;
                                    var_93 = local_sp_9 + (-8L);
                                    *(uint64_t *)var_93 = 4208424UL;
                                    var_94 = indirect_placeholder_16(var_92);
                                    local_sp_9 = var_93;
                                    local_sp_10 = var_93;
                                    if (*(unsigned char *)((uint64_t)(unsigned char)var_94 + 4417728UL) == '\x00') {
                                        break;
                                    }
                                    var_95 = *var_44 + 1UL;
                                    *var_44 = var_95;
                                    var_89 = var_95;
                                    var_90 = *var_39;
                                    var_91 = helper_cc_compute_c_wrapper(var_89 - var_90, var_90, var_2, 17U);
                                    local_sp_10 = local_sp_9;
                                }
                            var_96 = *var_44;
                            local_sp_11 = local_sp_10;
                            local_sp_28 = local_sp_10;
                            if (var_96 == *var_39) {
                                break;
                            }
                            *var_41 = var_96;
                            var_97 = *var_44;
                            var_98 = *var_39;
                            var_99 = helper_cc_compute_c_wrapper(var_97 - var_98, var_98, var_2, 17U);
                            local_sp_12 = local_sp_11;
                            while (var_99 != 0UL)
                                {
                                    var_100 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)var_43;
                                    var_101 = local_sp_11 + (-8L);
                                    *(uint64_t *)var_101 = 4208496UL;
                                    var_102 = indirect_placeholder_16(var_100);
                                    local_sp_11 = var_101;
                                    local_sp_12 = var_101;
                                    if (*(unsigned char *)((uint64_t)(unsigned char)var_102 + 4417728UL) == '\x00') {
                                        break;
                                    }
                                    var_103 = *var_44 + 1UL;
                                    *var_44 = var_103;
                                    var_97 = var_103;
                                    var_98 = *var_39;
                                    var_99 = helper_cc_compute_c_wrapper(var_97 - var_98, var_98, var_2, 17U);
                                    local_sp_12 = local_sp_11;
                                }
                            *var_42 = *var_44;
                            local_sp_13 = local_sp_12;
                            var_104 = *var_41;
                            *var_35 = var_104;
                            local_sp_18 = local_sp_13;
                            rcx3_3_be = rcx3_4;
                            local_sp_8_be = local_sp_13;
                            r101_3 = r101_4;
                            rbx2_2 = rbx2_3;
                            r85_3 = r85_4;
                            r96_3 = r96_4;
                            local_sp_15_ph = local_sp_13;
                            if (*var_42 == *var_41) {
                                *var_35 = (var_104 + 1UL);
                            } else {
                                *var_46 = var_104;
                                var_105 = *var_42 - *var_41;
                                *var_47 = var_105;
                                *var_35 = (*var_35 + var_105);
                                var_106 = *var_47;
                                if ((long)var_106 > (long)*(uint64_t *)4417984UL) {
                                    *(uint64_t *)4417984UL = var_106;
                                }
                                if (*(unsigned char *)4416641UL != '\x00') {
                                    while (1U)
                                        {
                                            local_sp_8_be = local_sp_15_ph;
                                            var_107 = *var_15;
                                            local_sp_16 = local_sp_15_ph;
                                            local_sp_18 = local_sp_15_ph;
                                            while (1U)
                                                {
                                                    var_108 = *var_46;
                                                    var_109 = helper_cc_compute_c_wrapper(var_107 - var_108, var_108, var_2, 17U);
                                                    if (var_109 != 0UL) {
                                                        loop_state_var = 1U;
                                                        break;
                                                    }
                                                    var_110 = (unsigned char **)var_14;
                                                    if (**var_110 != '\n') {
                                                        loop_state_var = 0U;
                                                        break;
                                                    }
                                                    var_111 = *var_15 + 1UL;
                                                    *var_15 = var_111;
                                                    var_107 = var_111;
                                                    continue;
                                                }
                                            switch_state_var = 0;
                                            switch (loop_state_var) {
                                              case 1U:
                                                {
                                                    switch_state_var = 1;
                                                    break;
                                                }
                                                break;
                                              case 0U:
                                                {
                                                    *(uint64_t *)4418064UL = (*(uint64_t *)4418064UL + 1UL);
                                                    var_112 = *var_15 + 1UL;
                                                    *var_15 = var_112;
                                                    *var_13 = var_112;
                                                    var_113 = *var_15;
                                                    var_114 = *(uint64_t *)(*var_9 + 8UL);
                                                    var_115 = helper_cc_compute_c_wrapper(var_113 - var_114, var_114, var_2, 17U);
                                                    local_sp_17 = local_sp_16;
                                                    while (var_115 != 0UL)
                                                        {
                                                            var_116 = (uint64_t)(uint32_t)(uint64_t)**var_110;
                                                            *(uint64_t *)(local_sp_16 + (-8L)) = 4208700UL;
                                                            var_117 = indirect_placeholder_16(var_116);
                                                            var_118 = (uint64_t)(unsigned char)var_117;
                                                            var_119 = local_sp_16 + (-16L);
                                                            *(uint64_t *)var_119 = 4208710UL;
                                                            var_120 = indirect_placeholder_16(var_118);
                                                            local_sp_16 = var_119;
                                                            local_sp_17 = var_119;
                                                            if ((uint64_t)(uint32_t)var_120 == 0UL) {
                                                                break;
                                                            }
                                                            var_121 = *var_15 + 1UL;
                                                            *var_15 = var_121;
                                                            var_113 = var_121;
                                                            var_114 = *(uint64_t *)(*var_9 + 8UL);
                                                            var_115 = helper_cc_compute_c_wrapper(var_113 - var_114, var_114, var_2, 17U);
                                                            local_sp_17 = local_sp_16;
                                                        }
                                                    *var_10 = (*var_15 - *var_13);
                                                    local_sp_15_ph = local_sp_17;
                                                    continue;
                                                }
                                                break;
                                            }
                                            if (switch_state_var)
                                                break;
                                        }
                                    if (*var_15 <= *var_46) {
                                        rcx3_3 = rcx3_3_be;
                                        local_sp_8 = local_sp_8_be;
                                        continue;
                                    }
                                }
                                local_sp_19 = local_sp_18;
                                if (*(uint64_t *)4416672UL != 0UL) {
                                    var_122 = local_sp_18 + (-8L);
                                    *(uint64_t *)var_122 = 4208786UL;
                                    var_123 = indirect_placeholder(var_45, 4418000UL);
                                    local_sp_8_be = var_122;
                                    local_sp_19 = var_122;
                                    if ((uint64_t)(unsigned char)var_123 != 0UL) {
                                        rcx3_3 = rcx3_3_be;
                                        local_sp_8 = local_sp_8_be;
                                        continue;
                                    }
                                }
                                local_sp_20 = local_sp_19;
                                if (*(uint64_t *)4416664UL != 0UL) {
                                    var_124 = local_sp_19 + (-8L);
                                    *(uint64_t *)var_124 = 4208823UL;
                                    var_125 = indirect_placeholder(var_45, 4418032UL);
                                    local_sp_8_be = var_124;
                                    local_sp_20 = var_124;
                                    if ((uint64_t)(unsigned char)var_125 != 1UL) {
                                        rcx3_3 = rcx3_3_be;
                                        local_sp_8 = local_sp_8_be;
                                        continue;
                                    }
                                }
                                var_126 = *(uint64_t *)4418112UL;
                                var_130 = var_126;
                                local_sp_21 = local_sp_20;
                                if (var_126 == *(uint64_t *)4418104UL) {
                                    var_127 = *(uint64_t *)4418096UL;
                                    var_128 = local_sp_20 + (-8L);
                                    *(uint64_t *)var_128 = 4208878UL;
                                    var_129 = indirect_placeholder_31(r101_4, rbx2_3, rcx3_4, 48UL, var_127, 4418104UL, r85_4, r96_4);
                                    *(uint64_t *)4418096UL = var_129;
                                    var_130 = *(uint64_t *)4418112UL;
                                    var_131 = var_129;
                                    local_sp_21 = var_128;
                                } else {
                                    var_131 = *(uint64_t *)4418096UL;
                                }
                                var_132 = (var_130 * 48UL) + var_131;
                                *var_48 = var_132;
                                local_sp_22_ph = local_sp_21;
                                local_sp_24_ph = local_sp_21;
                                local_sp_27 = local_sp_21;
                                if (*(unsigned char *)4416640UL == '\x00') {
                                    if (*(unsigned char *)4416641UL != '\x00') {
                                        var_169 = *var_48;
                                        var_170 = *var_47;
                                        *(uint64_t *)var_169 = *var_46;
                                        *(uint64_t *)(var_169 + 8UL) = var_170;
                                        *(uint64_t *)(*var_48 + 16UL) = (*var_37 - *var_46);
                                        *(uint64_t *)(*var_48 + 24UL) = (*var_39 - *var_46);
                                        *(uint32_t *)(*var_48 + 40UL) = *var_5;
                                        *(uint64_t *)4418112UL = (*(uint64_t *)4418112UL + 1UL);
                                        rcx3_3_be = var_169;
                                        local_sp_8_be = local_sp_27;
                                        rcx3_3 = rcx3_3_be;
                                        local_sp_8 = local_sp_8_be;
                                        continue;
                                    }
                                    *(uint64_t *)(var_132 + 32UL) = (*var_13 - *var_46);
                                    var_148 = *(uint64_t *)4417992UL;
                                    var_149 = *var_10;
                                    if ((long)var_149 > (long)var_148) {
                                        *(uint64_t *)4417992UL = var_149;
                                    }
                                } else {
                                    while (1U)
                                        {
                                            var_133 = *var_15;
                                            local_sp_23 = local_sp_22_ph;
                                            local_sp_24_ph = local_sp_22_ph;
                                            while (1U)
                                                {
                                                    var_134 = *var_46;
                                                    var_135 = helper_cc_compute_c_wrapper(var_133 - var_134, var_134, var_2, 17U);
                                                    if (var_135 != 0UL) {
                                                        loop_state_var = 1U;
                                                        break;
                                                    }
                                                    var_136 = (unsigned char **)var_14;
                                                    if (**var_136 != '\n') {
                                                        loop_state_var = 0U;
                                                        break;
                                                    }
                                                    var_137 = *var_15 + 1UL;
                                                    *var_15 = var_137;
                                                    var_133 = var_137;
                                                    continue;
                                                }
                                            switch_state_var = 0;
                                            switch (loop_state_var) {
                                              case 1U:
                                                {
                                                    switch_state_var = 1;
                                                    break;
                                                }
                                                break;
                                              case 0U:
                                                {
                                                    *(uint64_t *)4418064UL = (*(uint64_t *)4418064UL + 1UL);
                                                    var_138 = *var_15 + 1UL;
                                                    *var_15 = var_138;
                                                    *var_13 = var_138;
                                                    var_139 = *var_15;
                                                    var_140 = *(uint64_t *)(*var_9 + 8UL);
                                                    var_141 = helper_cc_compute_c_wrapper(var_139 - var_140, var_140, var_2, 17U);
                                                    local_sp_22_ph_be = local_sp_23;
                                                    while (var_141 != 0UL)
                                                        {
                                                            var_142 = (uint64_t)(uint32_t)(uint64_t)**var_136;
                                                            *(uint64_t *)(local_sp_23 + (-8L)) = 4209019UL;
                                                            var_143 = indirect_placeholder_16(var_142);
                                                            var_144 = (uint64_t)(unsigned char)var_143;
                                                            var_145 = local_sp_23 + (-16L);
                                                            *(uint64_t *)var_145 = 4209029UL;
                                                            var_146 = indirect_placeholder_16(var_144);
                                                            local_sp_23 = var_145;
                                                            local_sp_22_ph_be = var_145;
                                                            if ((uint64_t)(uint32_t)var_146 == 0UL) {
                                                                break;
                                                            }
                                                            var_147 = *var_15 + 1UL;
                                                            *var_15 = var_147;
                                                            var_139 = var_147;
                                                            var_140 = *(uint64_t *)(*var_9 + 8UL);
                                                            var_141 = helper_cc_compute_c_wrapper(var_139 - var_140, var_140, var_2, 17U);
                                                            local_sp_22_ph_be = local_sp_23;
                                                        }
                                                    local_sp_22_ph = local_sp_22_ph_be;
                                                    continue;
                                                }
                                                break;
                                            }
                                            if (switch_state_var)
                                                break;
                                        }
                                    *(uint64_t *)(*var_48 + 32UL) = *(uint64_t *)4418064UL;
                                }
                                local_sp_25 = local_sp_24_ph;
                                local_sp_27 = local_sp_24_ph;
                                var_150 = *var_13;
                                var_151 = var_150;
                                if (*(unsigned char *)4416641UL != '\x00' & var_150 != *var_37) {
                                    var_152 = *var_39;
                                    var_153 = helper_cc_compute_c_wrapper(var_151 - var_152, var_152, var_2, 17U);
                                    local_sp_26_ph = local_sp_25;
                                    while (var_153 != 0UL)
                                        {
                                            var_154 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)var_36;
                                            *(uint64_t *)(local_sp_25 + (-8L)) = 4209176UL;
                                            var_155 = indirect_placeholder_16(var_154);
                                            var_156 = (uint64_t)(unsigned char)var_155;
                                            var_157 = local_sp_25 + (-16L);
                                            *(uint64_t *)var_157 = 4209186UL;
                                            var_158 = indirect_placeholder_16(var_156);
                                            local_sp_25 = var_157;
                                            local_sp_26_ph = var_157;
                                            if ((uint64_t)(uint32_t)var_158 == 0UL) {
                                                break;
                                            }
                                            var_159 = *var_37 + 1UL;
                                            *var_37 = var_159;
                                            var_151 = var_159;
                                            var_152 = *var_39;
                                            var_153 = helper_cc_compute_c_wrapper(var_151 - var_152, var_152, var_2, 17U);
                                            local_sp_26_ph = local_sp_25;
                                        }
                                    var_160 = *var_37;
                                    local_sp_26 = local_sp_26_ph;
                                    var_161 = *var_39;
                                    var_162 = helper_cc_compute_c_wrapper(var_160 - var_161, var_161, var_2, 17U);
                                    local_sp_27 = local_sp_26;
                                    while (var_162 != 0UL)
                                        {
                                            var_163 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)var_36;
                                            *(uint64_t *)(local_sp_26 + (-8L)) = 4209224UL;
                                            var_164 = indirect_placeholder_16(var_163);
                                            var_165 = (uint64_t)(unsigned char)var_164;
                                            var_166 = local_sp_26 + (-16L);
                                            *(uint64_t *)var_166 = 4209234UL;
                                            var_167 = indirect_placeholder_16(var_165);
                                            local_sp_26 = var_166;
                                            local_sp_27 = var_166;
                                            if ((uint64_t)(uint32_t)var_167 == 0UL) {
                                                break;
                                            }
                                            var_168 = *var_37 + 1UL;
                                            *var_37 = var_168;
                                            var_160 = var_168;
                                            var_161 = *var_39;
                                            var_162 = helper_cc_compute_c_wrapper(var_160 - var_161, var_161, var_2, 17U);
                                            local_sp_27 = local_sp_26;
                                        }
                                }
                                var_169 = *var_48;
                                var_170 = *var_47;
                                *(uint64_t *)var_169 = *var_46;
                                *(uint64_t *)(var_169 + 8UL) = var_170;
                                *(uint64_t *)(*var_48 + 16UL) = (*var_37 - *var_46);
                                *(uint64_t *)(*var_48 + 24UL) = (*var_39 - *var_46);
                                *(uint32_t *)(*var_48 + 40UL) = *var_5;
                                *(uint64_t *)4418112UL = (*(uint64_t *)4418112UL + 1UL);
                                rcx3_3_be = var_169;
                                local_sp_8_be = local_sp_27;
                            }
                        }
                    }
                var_171 = *var_38;
                *var_35 = var_171;
                var_49 = var_171;
                r101_1 = r101_5;
                rbx2_0 = rbx2_4;
                rcx3_1 = rcx3_5;
                local_sp_5 = local_sp_28;
                r85_1 = r85_5;
                r96_1 = r96_5;
                continue;
            }
            var_53 = *(uint64_t *)(*var_9 + 8UL) - *var_35;
            var_54 = local_sp_5 + (-8L);
            *(uint64_t *)var_54 = 4208071UL;
            var_55 = indirect_placeholder_57(var_53, 4416712UL, 4417664UL);
            var_56 = var_55.field_0;
            var_57 = var_55.field_1;
            var_58 = var_55.field_2;
            var_59 = var_55.field_3;
            r101_0 = var_56;
            local_sp_0 = var_54;
            r85_0 = var_53;
            r96_0 = var_59;
            r101_2 = var_56;
            rbx2_1 = var_57;
            rcx3_2 = 0UL;
            local_sp_6 = var_54;
            r85_2 = var_53;
            r96_2 = var_59;
            if (var_58 == 0UL) {
                var_61 = *(uint64_t *)4416704UL;
                *(uint64_t *)(local_sp_5 + (-16L)) = 4208115UL;
                var_62 = indirect_placeholder_56(var_61);
                var_63 = var_62.field_0;
                var_64 = var_62.field_1;
                var_65 = var_62.field_2;
                var_66 = local_sp_5 + (-24L);
                *(uint64_t *)var_66 = 4208143UL;
                var_67 = indirect_placeholder_55(0UL, var_63, 4383912UL, 1UL, 0UL, var_64, var_65);
                var_68 = var_67.field_0;
                var_69 = var_67.field_1;
                var_70 = var_67.field_2;
                var_71 = var_67.field_3;
                r101_0 = var_68;
                rcx3_0 = var_69;
                local_sp_0 = var_66;
                r85_0 = var_70;
                r96_0 = var_71;
                var_72 = *var_35 + **(uint64_t **)4417680UL;
                *var_38 = var_72;
                var_73 = var_72;
                r101_2 = r101_0;
                rcx3_2 = rcx3_0;
                local_sp_6 = local_sp_0;
                r85_2 = r85_0;
                r96_2 = r96_0;
            } else {
                var_60 = helper_cc_compute_all_wrapper(var_58, 0UL, 0UL, 25U);
                if ((uint64_t)(((unsigned char)(var_60 >> 4UL) ^ (unsigned char)var_60) & '\xc0') == 0UL) {
                    *(uint64_t *)(local_sp_5 + (-16L)) = 4208100UL;
                    indirect_placeholder_3(var_3);
                    abort();
                }
                var_72 = *var_35 + **(uint64_t **)4417680UL;
                *var_38 = var_72;
                var_73 = var_72;
                r101_2 = r101_0;
                rcx3_2 = rcx3_0;
                local_sp_6 = local_sp_0;
                r85_2 = r85_0;
                r96_2 = r96_0;
            }
        }
}
