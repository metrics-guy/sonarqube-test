typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_1(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_3(uint64_t param_0);
extern uint64_t indirect_placeholder_16(uint64_t param_0);
extern void indirect_placeholder_29(uint64_t param_0, uint64_t param_1, uint64_t param_2);
void bb_define_all_fields(uint64_t rdi) {
    uint64_t local_sp_9;
    uint64_t var_70;
    uint64_t var_65;
    uint64_t storemerge24;
    uint64_t var_64;
    uint64_t local_sp_3;
    uint64_t var_66;
    uint64_t *_pre_phi298;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t storemerge37;
    uint64_t var_47;
    uint64_t var_40;
    uint64_t var_39;
    uint64_t local_sp_5_be;
    uint64_t var_38;
    uint64_t var_33;
    uint64_t var_32;
    uint64_t local_sp_4;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t local_sp_22_be;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t var_10;
    uint64_t *var_11;
    uint64_t var_12;
    uint64_t *var_13;
    uint64_t var_14;
    uint64_t *var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t *var_18;
    uint64_t *var_19;
    uint64_t var_20;
    uint64_t local_sp_25;
    uint64_t storemerge26;
    uint64_t var_186;
    uint64_t var_178;
    uint64_t var_142;
    uint64_t var_159;
    uint64_t var_175;
    uint64_t var_170;
    uint64_t var_169;
    uint64_t local_sp_0;
    uint64_t var_171;
    uint64_t var_172;
    uint64_t var_173;
    uint64_t var_174;
    uint64_t local_sp_20;
    uint64_t var_148;
    uint64_t local_sp_17_be;
    uint64_t var_119;
    uint64_t var_137;
    uint64_t var_132;
    uint64_t var_131;
    uint64_t local_sp_1;
    uint64_t var_133;
    uint64_t var_134;
    uint64_t var_135;
    uint64_t var_136;
    uint64_t storemerge33;
    uint64_t local_sp_15;
    uint64_t var_108;
    uint64_t var_102;
    uint64_t local_sp_6;
    uint64_t var_41;
    uint64_t var_100;
    uint64_t var_94;
    uint64_t local_sp_10_be;
    uint64_t local_sp_10;
    uint64_t var_92;
    uint64_t var_87;
    uint64_t var_86;
    uint64_t local_sp_2;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t storemerge23;
    uint64_t local_sp_5;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t _pre_phi;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    unsigned char **var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    unsigned char storemerge;
    uint64_t var_153;
    uint64_t var_154;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_48;
    uint64_t local_sp_7;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t local_sp_8;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t *var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    unsigned char **var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t *var_53;
    uint64_t var_52;
    uint64_t local_sp_10_ph;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t *var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    unsigned char storemerge273;
    uint64_t var_93;
    uint64_t local_sp_11;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t var_101;
    uint64_t local_sp_12;
    uint64_t local_sp_13_ph;
    uint64_t local_sp_13;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t var_107;
    uint64_t var_109;
    uint64_t local_sp_14;
    uint64_t var_110;
    uint64_t *var_111;
    uint64_t storemerge32;
    uint64_t var_112;
    uint64_t var_113;
    uint64_t var_114;
    uint64_t var_115;
    uint64_t var_116;
    uint64_t var_117;
    uint64_t *var_118;
    uint64_t local_sp_16;
    uint64_t local_sp_17;
    uint64_t var_120;
    uint64_t var_121;
    uint64_t var_122;
    uint64_t var_123;
    uint64_t var_124;
    uint64_t var_125;
    uint64_t var_126;
    unsigned char **var_127;
    uint64_t var_128;
    uint64_t var_129;
    uint64_t var_130;
    uint64_t var_138;
    uint64_t var_139;
    uint64_t var_141;
    uint64_t var_140;
    unsigned char storemerge275;
    uint64_t local_sp_18;
    uint64_t local_sp_19;
    uint64_t var_143;
    uint64_t var_144;
    uint64_t var_145;
    uint64_t var_146;
    uint64_t var_147;
    uint64_t var_149;
    uint64_t *var_150;
    uint64_t storemerge28;
    uint64_t var_151;
    uint64_t local_sp_21;
    uint64_t var_152;
    uint64_t var_155;
    uint64_t var_156;
    uint64_t var_157;
    uint64_t *var_158;
    uint64_t local_sp_22;
    uint64_t var_160;
    uint64_t var_161;
    uint64_t var_162;
    unsigned char storemerge274;
    uint64_t var_176;
    uint64_t var_177;
    uint64_t local_sp_23;
    uint64_t var_179;
    uint64_t var_180;
    uint64_t local_sp_24;
    uint64_t var_181;
    uint64_t var_182;
    uint64_t var_183;
    uint64_t var_184;
    uint64_t var_185;
    uint64_t var_163;
    uint64_t var_164;
    uint64_t var_165;
    uint64_t var_166;
    uint64_t var_167;
    uint64_t var_168;
    uint64_t var_187;
    uint64_t var_188;
    uint64_t *var_189;
    uint64_t var_190;
    uint64_t var_191;
    uint64_t *var_192;
    uint64_t var_193;
    uint64_t var_194;
    uint64_t var_195;
    uint64_t var_196;
    uint64_t var_197;
    uint64_t var_198;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = var_0 + (-8L);
    *(uint64_t *)var_3 = var_1;
    var_4 = var_0 + (-152L);
    var_5 = var_0 + (-144L);
    var_6 = (uint64_t *)var_5;
    *var_6 = rdi;
    var_7 = **(uint64_t **)var_5;
    *(uint64_t *)4418496UL = var_7;
    *(uint64_t *)4418504UL = (*(uint64_t *)(*var_6 + 8UL) + var_7);
    var_8 = *(uint64_t *)(*var_6 + 16UL) + *(uint64_t *)4418496UL;
    var_9 = (uint64_t *)(var_0 + (-48L));
    *var_9 = var_8;
    var_10 = *(uint64_t *)(*var_6 + 24UL) + *(uint64_t *)4418496UL;
    var_11 = (uint64_t *)(var_0 + (-56L));
    *var_11 = var_10;
    var_12 = *(uint64_t *)(((uint64_t)*(uint32_t *)(*var_6 + 40UL) << 4UL) + *(uint64_t *)4418088UL);
    var_13 = (uint64_t *)(var_0 + (-64L));
    *var_13 = var_12;
    var_14 = *(uint64_t *)((((uint64_t)*(uint32_t *)(*var_6 + 40UL) << 4UL) + *(uint64_t *)4418088UL) + 8UL);
    var_15 = (uint64_t *)(var_0 + (-72L));
    *var_15 = var_14;
    var_16 = *(uint64_t *)4418504UL;
    var_17 = var_0 + (-16L);
    var_18 = (uint64_t *)var_17;
    *var_18 = var_16;
    var_19 = (uint64_t *)(var_0 + (-80L));
    storemerge37 = 1UL;
    var_20 = var_16;
    storemerge33 = 1UL;
    storemerge23 = 1UL;
    local_sp_5 = var_4;
    storemerge = (unsigned char)'\x00';
    storemerge273 = (unsigned char)'\x00';
    storemerge275 = (unsigned char)'\x00';
    storemerge274 = (unsigned char)'\x00';
    while (1U)
        {
            var_21 = *var_11;
            var_22 = helper_cc_compute_c_wrapper(var_20 - var_21, var_21, var_2, 17U);
            local_sp_6 = local_sp_5;
            if (var_22 != 0UL) {
                _pre_phi = *(uint64_t *)4418496UL + *(uint64_t *)4418416UL;
                var_39 = *var_18;
                break;
            }
            var_23 = *(uint64_t *)4418496UL + *(uint64_t *)4418416UL;
            var_24 = *var_18;
            _pre_phi = var_23;
            var_39 = var_24;
            if (var_24 <= var_23) {
                break;
            }
            *(uint64_t *)4418504UL = var_24;
            if (*(uint64_t *)4417056UL != 0UL) {
                *(uint64_t *)(local_sp_5 + (-16L)) = 4211589UL;
                indirect_placeholder_3(var_3);
                abort();
            }
            var_28 = (unsigned char **)var_17;
            var_29 = (uint64_t)(uint32_t)(uint64_t)**var_28;
            var_30 = local_sp_5 + (-8L);
            *(uint64_t *)var_30 = 4211630UL;
            var_31 = indirect_placeholder_16(var_29);
            local_sp_5_be = var_30;
            local_sp_4 = var_30;
            if (*(unsigned char *)((uint64_t)(unsigned char)var_31 + 4417728UL) != '\x00') {
                var_32 = *var_18;
                var_33 = *var_11;
                var_34 = helper_cc_compute_c_wrapper(var_32 - var_33, var_33, var_2, 17U);
                local_sp_5_be = local_sp_4;
                while (var_34 != 0UL)
                    {
                        var_35 = (uint64_t)(uint32_t)(uint64_t)**var_28;
                        var_36 = local_sp_4 + (-8L);
                        *(uint64_t *)var_36 = 4211680UL;
                        var_37 = indirect_placeholder_16(var_35);
                        local_sp_5_be = var_36;
                        local_sp_4 = var_36;
                        if (*(unsigned char *)((uint64_t)(unsigned char)var_37 + 4417728UL) == '\x00') {
                            break;
                        }
                        var_38 = *var_18 + 1UL;
                        *var_18 = var_38;
                        var_32 = var_38;
                        var_33 = *var_11;
                        var_34 = helper_cc_compute_c_wrapper(var_32 - var_33, var_33, var_2, 17U);
                        local_sp_5_be = local_sp_4;
                    }
            }
            *var_18 = (*var_18 + 1UL);
        }
    if (var_39 > _pre_phi) {
        *(uint64_t *)4418504UL = var_39;
    }
    if (*(uint64_t *)4416312UL == 0UL) {
    }
    *(unsigned char *)4418512UL = storemerge;
    var_40 = *(uint64_t *)4418504UL;
    while (1U)
        {
            var_41 = *(uint64_t *)4418496UL;
            var_48 = var_41;
            local_sp_7 = local_sp_6;
            if (var_40 <= var_41) {
                break;
            }
            var_42 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(var_40 + (-1L));
            *(uint64_t *)(local_sp_6 + (-8L)) = 4211883UL;
            var_43 = indirect_placeholder_16(var_42);
            var_44 = (uint64_t)(unsigned char)var_43;
            var_45 = local_sp_6 + (-16L);
            *(uint64_t *)var_45 = 4211893UL;
            var_46 = indirect_placeholder_16(var_44);
            local_sp_6 = var_45;
            local_sp_7 = var_45;
            if ((uint64_t)(uint32_t)var_46 != 0UL) {
                var_48 = *(uint64_t *)4418496UL;
                break;
            }
            var_47 = *(uint64_t *)4418504UL + (-1L);
            *(uint64_t *)4418504UL = var_47;
            var_40 = var_47;
            continue;
        }
    var_49 = *(uint64_t *)(*var_6 + 16UL);
    var_50 = 0UL - var_49;
    var_51 = *(uint64_t *)4417984UL + *(uint64_t *)4418400UL;
    local_sp_8 = local_sp_7;
    if ((long)var_51 < (long)var_50) {
        var_54 = var_48 - var_51;
        var_55 = var_0 + (-24L);
        var_56 = (uint64_t *)var_55;
        *var_56 = var_54;
        _pre_phi298 = var_56;
        if (*(uint64_t *)4417056UL != 0UL) {
            *(uint64_t *)(local_sp_7 + (-16L)) = 4212044UL;
            indirect_placeholder_29(var_3, 0UL, var_54);
            abort();
        }
        var_60 = (unsigned char **)var_55;
        var_61 = (uint64_t)(uint32_t)(uint64_t)**var_60;
        var_62 = local_sp_7 + (-8L);
        *(uint64_t *)var_62 = 4212085UL;
        var_63 = indirect_placeholder_16(var_61);
        local_sp_3 = var_62;
        local_sp_8 = var_62;
        if (*(unsigned char *)((uint64_t)(unsigned char)var_63 + 4417728UL) != '\x00') {
            var_64 = *var_56;
            var_65 = *(uint64_t *)4418496UL;
            var_66 = helper_cc_compute_c_wrapper(var_64 - var_65, var_65, var_2, 17U);
            local_sp_8 = local_sp_3;
            while (var_66 != 0UL)
                {
                    var_67 = (uint64_t)(uint32_t)(uint64_t)**var_60;
                    var_68 = local_sp_3 + (-8L);
                    *(uint64_t *)var_68 = 4212138UL;
                    var_69 = indirect_placeholder_16(var_67);
                    local_sp_3 = var_68;
                    local_sp_8 = var_68;
                    if (*(unsigned char *)((uint64_t)(unsigned char)var_69 + 4417728UL) == '\x00') {
                        break;
                    }
                    var_70 = *var_56 + 1UL;
                    *var_56 = var_70;
                    var_64 = var_70;
                    var_65 = *(uint64_t *)4418496UL;
                    var_66 = helper_cc_compute_c_wrapper(var_64 - var_65, var_65, var_2, 17U);
                    local_sp_8 = local_sp_3;
                }
        }
        *var_56 = (*var_56 + 1UL);
    } else {
        var_52 = var_49 + var_48;
        var_53 = (uint64_t *)(var_0 + (-24L));
        *var_53 = var_52;
        _pre_phi298 = var_53;
        *(uint64_t *)4418464UL = *_pre_phi298;
        local_sp_9 = local_sp_8;
        storemerge24 = *(uint64_t *)4418496UL;
        *(uint64_t *)4418472UL = storemerge24;
        local_sp_10_ph = local_sp_9;
        while (storemerge24 <= *(uint64_t *)4418464UL)
            {
                var_71 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(storemerge24 + (-1L));
                *(uint64_t *)(local_sp_9 + (-8L)) = 4212280UL;
                var_72 = indirect_placeholder_16(var_71);
                var_73 = (uint64_t)(unsigned char)var_72;
                var_74 = local_sp_9 + (-16L);
                *(uint64_t *)var_74 = 4212290UL;
                var_75 = indirect_placeholder_16(var_73);
                local_sp_9 = var_74;
                local_sp_10_ph = var_74;
                if ((uint64_t)(uint32_t)var_75 == 0UL) {
                    break;
                }
                storemerge24 = *(uint64_t *)4418472UL + (-1L);
                *(uint64_t *)4418472UL = storemerge24;
                local_sp_10_ph = local_sp_9;
            }
        var_76 = (uint64_t *)(var_0 + (-136L));
        local_sp_10 = local_sp_10_ph;
        while (1U)
            {
                var_77 = *(uint64_t *)4418408UL + *(uint64_t *)4418464UL;
                var_78 = *(uint64_t *)4418472UL;
                var_79 = helper_cc_compute_c_wrapper(var_77 - var_78, var_78, var_2, 17U);
                local_sp_11 = local_sp_10;
                local_sp_13_ph = local_sp_10;
                if (var_79 != 0UL) {
                    if (*(uint64_t *)4416312UL != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_93 = *(uint64_t *)4418464UL;
                    *var_18 = var_93;
                    var_94 = var_93;
                    loop_state_var = 1U;
                    break;
                }
                if (*(uint64_t *)4417056UL == 0UL) {
                    var_83 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)4418464UL;
                    var_84 = local_sp_10 + (-8L);
                    *(uint64_t *)var_84 = 4212435UL;
                    var_85 = indirect_placeholder_16(var_83);
                    local_sp_10_be = var_84;
                    local_sp_2 = var_84;
                    if (*(unsigned char *)((uint64_t)(unsigned char)var_85 + 4417728UL) != '\x00') {
                        var_86 = *(uint64_t *)4418464UL;
                        var_87 = *(uint64_t *)4418472UL;
                        var_88 = helper_cc_compute_c_wrapper(var_86 - var_87, var_87, var_2, 17U);
                        local_sp_10_be = local_sp_2;
                        while (var_88 != 0UL)
                            {
                                var_89 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)4418464UL;
                                var_90 = local_sp_2 + (-8L);
                                *(uint64_t *)var_90 = 4212510UL;
                                var_91 = indirect_placeholder_16(var_89);
                                local_sp_10_be = var_90;
                                local_sp_2 = var_90;
                                if (*(unsigned char *)((uint64_t)(unsigned char)var_91 + 4417728UL) == '\x00') {
                                    break;
                                }
                                var_92 = *(uint64_t *)4418464UL + 1UL;
                                *(uint64_t *)4418464UL = var_92;
                                var_86 = var_92;
                                var_87 = *(uint64_t *)4418472UL;
                                var_88 = helper_cc_compute_c_wrapper(var_86 - var_87, var_87, var_2, 17U);
                                local_sp_10_be = local_sp_2;
                            }
                    }
                    *(uint64_t *)4418464UL = (*(uint64_t *)4418464UL + 1UL);
                } else {
                    var_80 = *(uint64_t *)4418472UL - *(uint64_t *)4418464UL;
                    var_81 = local_sp_10 + (-8L);
                    *(uint64_t *)var_81 = 4212359UL;
                    var_82 = indirect_placeholder_2(var_80, 4417064UL, 0UL);
                    *var_76 = var_82;
                    local_sp_10_be = var_81;
                    if (var_82 != 18446744073709551614UL) {
                        *(uint64_t *)(local_sp_10 + (-16L)) = 4212375UL;
                        indirect_placeholder_3(var_3);
                        abort();
                    }
                    *(uint64_t *)4418464UL = (((var_82 == 18446744073709551615UL) ? 1UL : var_82) + *(uint64_t *)4418464UL);
                }
                local_sp_10 = local_sp_10_be;
                continue;
            }
        switch (loop_state_var) {
          case 0U:
            {
                *(unsigned char *)4418480UL = storemerge273;
                var_102 = *(uint64_t *)4418464UL;
                local_sp_13 = local_sp_13_ph;
                while (1U)
                    {
                        var_109 = var_102;
                        local_sp_14 = local_sp_13;
                        if (*var_15 <= var_102) {
                            break;
                        }
                        var_103 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)4418464UL;
                        *(uint64_t *)(local_sp_13 + (-8L)) = 4212733UL;
                        var_104 = indirect_placeholder_16(var_103);
                        var_105 = (uint64_t)(unsigned char)var_104;
                        var_106 = local_sp_13 + (-16L);
                        *(uint64_t *)var_106 = 4212743UL;
                        var_107 = indirect_placeholder_16(var_105);
                        local_sp_13 = var_106;
                        local_sp_14 = var_106;
                        if ((uint64_t)(uint32_t)var_107 != 0UL) {
                            var_109 = *(uint64_t *)4418464UL;
                            break;
                        }
                        var_108 = *(uint64_t *)4418464UL + 1UL;
                        *(uint64_t *)4418464UL = var_108;
                        var_102 = var_108;
                        continue;
                    }
                var_110 = (*(uint64_t *)4418408UL + (var_109 - *(uint64_t *)4418472UL)) - *(uint64_t *)4416304UL;
                var_111 = (uint64_t *)(var_0 + (-96L));
                *var_111 = var_110;
                local_sp_15 = local_sp_14;
                local_sp_19 = local_sp_14;
                if ((long)var_110 <= (long)0UL) {
                    storemerge32 = *(uint64_t *)4418504UL;
                    while (1U)
                        {
                            *(uint64_t *)4418432UL = storemerge32;
                            var_117 = storemerge32;
                            local_sp_16 = local_sp_15;
                            if (*var_15 <= storemerge32) {
                                break;
                            }
                            var_112 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)4418432UL;
                            *(uint64_t *)(local_sp_15 + (-8L)) = 4212875UL;
                            var_113 = indirect_placeholder_16(var_112);
                            var_114 = (uint64_t)(unsigned char)var_113;
                            var_115 = local_sp_15 + (-16L);
                            *(uint64_t *)var_115 = 4212885UL;
                            var_116 = indirect_placeholder_16(var_114);
                            local_sp_15 = var_115;
                            local_sp_16 = var_115;
                            if ((uint64_t)(uint32_t)var_116 != 0UL) {
                                var_117 = *(uint64_t *)4418432UL;
                                break;
                            }
                            storemerge32 = *(uint64_t *)4418432UL + 1UL;
                            continue;
                        }
                    *(uint64_t *)4418440UL = var_117;
                    *var_18 = var_117;
                    var_118 = (uint64_t *)(var_0 + (-104L));
                    var_119 = var_117;
                    local_sp_17 = local_sp_16;
                    var_120 = *var_11;
                    var_121 = helper_cc_compute_c_wrapper(var_119 - var_120, var_120, var_2, 17U);
                    local_sp_18 = local_sp_17;
                    while (var_121 != 0UL)
                        {
                            var_122 = *var_111 + *(uint64_t *)4418432UL;
                            var_123 = helper_cc_compute_c_wrapper(*var_18 - var_122, var_122, var_2, 17U);
                            if (var_123 == 0UL) {
                                break;
                            }
                            *(uint64_t *)4418440UL = *var_18;
                            if (*(uint64_t *)4417056UL != 0UL) {
                                *(uint64_t *)(local_sp_17 + (-16L)) = 4212997UL;
                                indirect_placeholder_3(var_3);
                                abort();
                            }
                            var_127 = (unsigned char **)var_17;
                            var_128 = (uint64_t)(uint32_t)(uint64_t)**var_127;
                            var_129 = local_sp_17 + (-8L);
                            *(uint64_t *)var_129 = 4213038UL;
                            var_130 = indirect_placeholder_16(var_128);
                            local_sp_17_be = var_129;
                            local_sp_1 = var_129;
                            if (*(unsigned char *)((uint64_t)(unsigned char)var_130 + 4417728UL) != '\x00') {
                                var_131 = *var_18;
                                var_132 = *var_11;
                                var_133 = helper_cc_compute_c_wrapper(var_131 - var_132, var_132, var_2, 17U);
                                local_sp_17_be = local_sp_1;
                                while (var_133 != 0UL)
                                    {
                                        var_134 = (uint64_t)(uint32_t)(uint64_t)**var_127;
                                        var_135 = local_sp_1 + (-8L);
                                        *(uint64_t *)var_135 = 4213088UL;
                                        var_136 = indirect_placeholder_16(var_134);
                                        local_sp_17_be = var_135;
                                        local_sp_1 = var_135;
                                        if (*(unsigned char *)((uint64_t)(unsigned char)var_136 + 4417728UL) == '\x00') {
                                            break;
                                        }
                                        var_137 = *var_18 + 1UL;
                                        *var_18 = var_137;
                                        var_131 = var_137;
                                        var_132 = *var_11;
                                        var_133 = helper_cc_compute_c_wrapper(var_131 - var_132, var_132, var_2, 17U);
                                        local_sp_17_be = local_sp_1;
                                    }
                            }
                            *var_18 = (*var_18 + 1UL);
                        }
                    var_138 = *var_111 + *(uint64_t *)4418432UL;
                    var_139 = helper_cc_compute_c_wrapper(*var_18 - var_138, var_138, var_2, 17U);
                    if (var_139 == 0UL) {
                        var_141 = *(uint64_t *)4418440UL;
                    } else {
                        var_140 = *var_18;
                        *(uint64_t *)4418440UL = var_140;
                        var_141 = var_140;
                    }
                    if (var_141 <= *(uint64_t *)4418432UL) {
                        *(unsigned char *)4418512UL = (unsigned char)'\x00';
                        storemerge275 = (unsigned char)'\x01';
                        if (*(uint64_t *)4416312UL == 0UL) {
                        } else {
                            if (*var_11 > *(uint64_t *)4418440UL) {
                            }
                        }
                    }
                    *(unsigned char *)4418448UL = storemerge275;
                    var_142 = *(uint64_t *)4418440UL;
                    local_sp_19 = local_sp_18;
                    while (var_142 <= *(uint64_t *)4418432UL)
                        {
                            var_143 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(var_142 + (-1L));
                            *(uint64_t *)(local_sp_18 + (-8L)) = 4213320UL;
                            var_144 = indirect_placeholder_16(var_143);
                            var_145 = (uint64_t)(unsigned char)var_144;
                            var_146 = local_sp_18 + (-16L);
                            *(uint64_t *)var_146 = 4213330UL;
                            var_147 = indirect_placeholder_16(var_145);
                            local_sp_18 = var_146;
                            local_sp_19 = var_146;
                            if ((uint64_t)(uint32_t)var_147 == 0UL) {
                                break;
                            }
                            var_148 = *(uint64_t *)4418440UL + (-1L);
                            *(uint64_t *)4418440UL = var_148;
                            var_142 = var_148;
                            local_sp_19 = local_sp_18;
                        }
                }
                *(uint64_t *)4418432UL = 0UL;
                *(uint64_t *)4418440UL = 0UL;
                *(unsigned char *)4418448UL = (unsigned char)'\x00';
                var_149 = (*(uint64_t *)4418416UL + (*(uint64_t *)4418496UL - *(uint64_t *)4418504UL)) - *(uint64_t *)4416304UL;
                var_150 = (uint64_t *)(var_0 + (-112L));
                *var_150 = var_149;
                local_sp_20 = local_sp_19;
                local_sp_24 = local_sp_19;
                if ((long)var_149 <= (long)0UL) {
                    *(uint64_t *)4418528UL = 0UL;
                    *(uint64_t *)4418536UL = 0UL;
                    *(unsigned char *)4418544UL = (unsigned char)'\x00';
                    local_sp_25 = local_sp_24;
                    if (*(unsigned char *)4416640UL == '\x00') {
                        if (*(unsigned char *)4416641UL != '\x00') {
                            var_193 = *(uint64_t *)(*var_6 + 32UL) + *(uint64_t *)4418496UL;
                            *(uint64_t *)4418560UL = var_193;
                            storemerge26 = var_193;
                            *(uint64_t *)4418568UL = storemerge26;
                            while (*var_11 <= storemerge26)
                                {
                                    var_194 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)4418568UL;
                                    *(uint64_t *)(local_sp_25 + (-8L)) = 4214299UL;
                                    var_195 = indirect_placeholder_16(var_194);
                                    var_196 = (uint64_t)(unsigned char)var_195;
                                    var_197 = local_sp_25 + (-16L);
                                    *(uint64_t *)var_197 = 4214309UL;
                                    var_198 = indirect_placeholder_16(var_196);
                                    local_sp_25 = var_197;
                                    if ((uint64_t)(uint32_t)var_198 == 0UL) {
                                        break;
                                    }
                                    storemerge26 = *(uint64_t *)4418568UL + 1UL;
                                    *(uint64_t *)4418568UL = storemerge26;
                                }
                        }
                    }
                    var_187 = *(uint64_t *)(((uint64_t)*(uint32_t *)(*var_6 + 40UL) << 3UL) + *(uint64_t *)4418072UL);
                    *(uint64_t *)(var_0 + (-32L)) = ((var_187 == 0UL) ? 4383163UL : var_187);
                    var_188 = *(uint64_t *)(*var_6 + 32UL) + 1UL;
                    var_189 = (uint64_t *)(var_0 + (-40L));
                    *var_189 = var_188;
                    var_190 = helper_cc_compute_all_wrapper((uint64_t)*(uint32_t *)(*var_6 + 40UL), 0UL, 0UL, 24U);
                    if ((uint64_t)(((unsigned char)(var_190 >> 4UL) ^ (unsigned char)var_190) & '\xc0') == 0UL) {
                        *var_189 = (*var_189 - *(uint64_t *)((((uint64_t)*(uint32_t *)(*var_6 + 40UL) << 3UL) + (-8L)) + *(uint64_t *)4418080UL));
                    }
                    var_191 = *(uint64_t *)4418560UL;
                    *(uint64_t *)(local_sp_24 + (-8L)) = 4214144UL;
                    indirect_placeholder_1();
                    var_192 = (uint64_t *)(var_0 + (-128L));
                    *var_192 = var_191;
                    *(uint64_t *)(local_sp_24 + (-16L)) = 4214174UL;
                    indirect_placeholder_1();
                    *(uint64_t *)4418568UL = *var_192;
                    return;
                }
                storemerge28 = *(uint64_t *)4418464UL;
                *(uint64_t *)4418536UL = storemerge28;
                var_151 = helper_cc_compute_c_wrapper(*var_13 - storemerge28, storemerge28, var_2, 17U);
                local_sp_21 = local_sp_20;
                while (var_151 != 0UL)
                    {
                        var_152 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(*(uint64_t *)4418536UL + (-1L));
                        *(uint64_t *)(local_sp_20 + (-8L)) = 4213497UL;
                        var_153 = indirect_placeholder_16(var_152);
                        var_154 = (uint64_t)(unsigned char)var_153;
                        var_155 = local_sp_20 + (-16L);
                        *(uint64_t *)var_155 = 4213507UL;
                        var_156 = indirect_placeholder_16(var_154);
                        local_sp_20 = var_155;
                        local_sp_21 = var_155;
                        if ((uint64_t)(uint32_t)var_156 == 0UL) {
                            break;
                        }
                        storemerge28 = *(uint64_t *)4418536UL + (-1L);
                        *(uint64_t *)4418536UL = storemerge28;
                        var_151 = helper_cc_compute_c_wrapper(*var_13 - storemerge28, storemerge28, var_2, 17U);
                        local_sp_21 = local_sp_20;
                    }
                var_157 = *_pre_phi298;
                *(uint64_t *)4418528UL = var_157;
                var_158 = (uint64_t *)(var_0 + (-120L));
                var_159 = var_157;
                local_sp_22 = local_sp_21;
                while (1U)
                    {
                        var_160 = var_159 + *var_150;
                        var_161 = *(uint64_t *)4418536UL;
                        var_162 = helper_cc_compute_c_wrapper(var_160 - var_161, var_161, var_2, 17U);
                        local_sp_23 = local_sp_22;
                        if (var_162 != 0UL) {
                            if (*(uint64_t *)4418536UL <= *(uint64_t *)4418528UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            *(unsigned char *)4418480UL = (unsigned char)'\x00';
                            storemerge274 = (unsigned char)'\x01';
                            if (*(uint64_t *)4416312UL != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            var_176 = *(uint64_t *)4418528UL;
                            var_177 = helper_cc_compute_c_wrapper(*var_9 - var_176, var_176, var_2, 17U);
                            if (var_177 != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            loop_state_var = 0U;
                            break;
                        }
                        if (*(uint64_t *)4417056UL == 0UL) {
                            var_166 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)4418528UL;
                            var_167 = local_sp_22 + (-8L);
                            *(uint64_t *)var_167 = 4213663UL;
                            var_168 = indirect_placeholder_16(var_166);
                            local_sp_22_be = var_167;
                            local_sp_0 = var_167;
                            if (*(unsigned char *)((uint64_t)(unsigned char)var_168 + 4417728UL) != '\x00') {
                                var_169 = *(uint64_t *)4418528UL;
                                var_170 = *(uint64_t *)4418536UL;
                                var_171 = helper_cc_compute_c_wrapper(var_169 - var_170, var_170, var_2, 17U);
                                local_sp_22_be = local_sp_0;
                                while (var_171 != 0UL)
                                    {
                                        var_172 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)4418528UL;
                                        var_173 = local_sp_0 + (-8L);
                                        *(uint64_t *)var_173 = 4213738UL;
                                        var_174 = indirect_placeholder_16(var_172);
                                        local_sp_22_be = var_173;
                                        local_sp_0 = var_173;
                                        if (*(unsigned char *)((uint64_t)(unsigned char)var_174 + 4417728UL) == '\x00') {
                                            break;
                                        }
                                        var_175 = *(uint64_t *)4418528UL + 1UL;
                                        *(uint64_t *)4418528UL = var_175;
                                        var_169 = var_175;
                                        var_170 = *(uint64_t *)4418536UL;
                                        var_171 = helper_cc_compute_c_wrapper(var_169 - var_170, var_170, var_2, 17U);
                                        local_sp_22_be = local_sp_0;
                                    }
                            }
                            *(uint64_t *)4418528UL = (*(uint64_t *)4418528UL + 1UL);
                        } else {
                            var_163 = *(uint64_t *)4418536UL - *(uint64_t *)4418528UL;
                            var_164 = local_sp_22 + (-8L);
                            *(uint64_t *)var_164 = 4213587UL;
                            var_165 = indirect_placeholder_2(var_163, 4417064UL, 0UL);
                            *var_158 = var_165;
                            local_sp_22_be = var_164;
                            if (var_165 != 18446744073709551614UL) {
                                *(uint64_t *)(local_sp_22 + (-16L)) = 4213603UL;
                                indirect_placeholder_3(var_3);
                                abort();
                            }
                            *(uint64_t *)4418528UL = (((var_165 == 18446744073709551615UL) ? 1UL : var_165) + *(uint64_t *)4418528UL);
                        }
                        var_159 = *(uint64_t *)4418528UL;
                        local_sp_22 = local_sp_22_be;
                        continue;
                    }
                switch (loop_state_var) {
                  case 1U:
                    {
                    }
                    break;
                  case 0U:
                    {
                        *(unsigned char *)4418544UL = storemerge274;
                        var_178 = *(uint64_t *)4418528UL;
                        var_179 = *(uint64_t *)4418536UL;
                        var_180 = helper_cc_compute_c_wrapper(var_178 - var_179, var_179, var_2, 17U);
                        local_sp_24 = local_sp_23;
                        while (var_180 != 0UL)
                            {
                                var_181 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)4418528UL;
                                *(uint64_t *)(local_sp_23 + (-8L)) = 4213944UL;
                                var_182 = indirect_placeholder_16(var_181);
                                var_183 = (uint64_t)(unsigned char)var_182;
                                var_184 = local_sp_23 + (-16L);
                                *(uint64_t *)var_184 = 4213954UL;
                                var_185 = indirect_placeholder_16(var_183);
                                local_sp_23 = var_184;
                                local_sp_24 = var_184;
                                if ((uint64_t)(uint32_t)var_185 == 0UL) {
                                    break;
                                }
                                var_186 = *(uint64_t *)4418528UL + 1UL;
                                *(uint64_t *)4418528UL = var_186;
                                var_178 = var_186;
                                var_179 = *(uint64_t *)4418536UL;
                                var_180 = helper_cc_compute_c_wrapper(var_178 - var_179, var_179, var_2, 17U);
                                local_sp_24 = local_sp_23;
                            }
                    }
                    break;
                }
            }
            break;
          case 1U:
            {
                while (1U)
                    {
                        var_101 = var_94;
                        local_sp_12 = local_sp_11;
                        if (var_94 <= *var_13) {
                            break;
                        }
                        var_95 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(var_94 + (-1L));
                        *(uint64_t *)(local_sp_11 + (-8L)) = 4212640UL;
                        var_96 = indirect_placeholder_16(var_95);
                        var_97 = (uint64_t)(unsigned char)var_96;
                        var_98 = local_sp_11 + (-16L);
                        *(uint64_t *)var_98 = 4212650UL;
                        var_99 = indirect_placeholder_16(var_97);
                        local_sp_11 = var_98;
                        local_sp_12 = var_98;
                        if ((uint64_t)(uint32_t)var_99 != 0UL) {
                            var_101 = *var_18;
                            break;
                        }
                        var_100 = *var_18 + (-1L);
                        *var_18 = var_100;
                        var_94 = var_100;
                        continue;
                    }
                storemerge273 = (var_101 > *var_9);
                local_sp_13_ph = local_sp_12;
            }
            break;
        }
    }
}
