typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_63_ret_type;
struct indirect_placeholder_63_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_1(void);
extern struct indirect_placeholder_63_ret_type indirect_placeholder_63(uint64_t param_0);
uint64_t bb_fread_file(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    uint64_t var_8;
    uint64_t local_sp_2;
    uint64_t var_44;
    uint32_t var_45;
    uint32_t *var_46;
    uint32_t *_pre_phi87;
    uint64_t var_21;
    uint64_t var_40;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint32_t var_25;
    uint32_t *var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_35;
    uint64_t local_sp_0;
    uint32_t *var_47;
    uint32_t *var_48;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t *var_17;
    uint64_t *var_18;
    uint64_t *var_19;
    uint64_t *var_20;
    uint64_t local_sp_1;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t rax_0;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_13;
    uint64_t var_14;
    struct indirect_placeholder_63_ret_type var_15;
    uint64_t var_16;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_3 = (uint64_t *)(var_0 + (-240L));
    *var_3 = rdi;
    var_4 = var_0 + (-248L);
    *(uint64_t *)var_4 = rsi;
    var_5 = var_0 + (-16L);
    var_6 = (uint64_t *)var_5;
    *var_6 = 0UL;
    var_7 = (uint64_t *)(var_0 + (-24L));
    *var_7 = 1024UL;
    *(uint64_t *)(var_0 + (-256L)) = 4228071UL;
    indirect_placeholder_1();
    var_8 = var_0 + (-264L);
    *(uint64_t *)var_8 = 4228090UL;
    indirect_placeholder_1();
    var_40 = 18446744073709551615UL;
    var_21 = 0UL;
    rax_0 = 0UL;
    local_sp_2 = var_8;
    var_9 = *var_3;
    var_10 = var_0 + (-272L);
    *(uint64_t *)var_10 = 4228127UL;
    indirect_placeholder_1();
    *(uint64_t *)(var_0 + (-48L)) = var_9;
    local_sp_2 = var_10;
    var_11 = *(uint64_t *)(var_0 + (-184L));
    if ((int)((uint32_t)var_0 + (-232)) >= (int)0U & (uint32_t)((uint16_t)*(uint32_t *)(var_0 + (-208L)) & (unsigned short)61440U) != 32768U & (long)var_9 >= (long)0UL & (long)var_9 >= (long)var_11) {
        var_12 = var_11 - var_9;
        *(uint64_t *)(var_0 + (-56L)) = var_12;
        if (var_12 != 18446744073709551615UL) {
            *(uint64_t *)(var_0 + (-280L)) = 4228178UL;
            indirect_placeholder_1();
            *(uint32_t *)18446744073709551615UL = 12U;
            return rax_0;
        }
        *var_7 = (var_12 + 1UL);
    }
    var_13 = *var_7;
    var_14 = local_sp_2 + (-8L);
    *(uint64_t *)var_14 = 4228218UL;
    var_15 = indirect_placeholder_63(var_13);
    var_16 = var_15.field_0;
    *var_6 = var_16;
    local_sp_1 = var_14;
    if (var_16 == 0UL) {
        return rax_0;
    }
    var_17 = (uint64_t *)(var_0 + (-32L));
    *var_17 = 0UL;
    var_18 = (uint64_t *)(var_0 + (-64L));
    var_19 = (uint64_t *)(var_0 + (-72L));
    var_20 = (uint64_t *)(var_0 + (-80L));
    while (1U)
        {
            var_22 = *var_7 - var_21;
            *var_18 = var_22;
            var_23 = local_sp_1 + (-8L);
            *(uint64_t *)var_23 = 4228298UL;
            indirect_placeholder_1();
            *var_19 = var_22;
            *var_17 = (*var_17 + var_22);
            var_24 = *var_19;
            local_sp_0 = var_23;
            if (var_24 != *var_18) {
                *(uint64_t *)(local_sp_1 + (-16L)) = 4228325UL;
                indirect_placeholder_1();
                var_25 = *(uint32_t *)var_24;
                var_26 = (uint32_t *)(var_0 + (-36L));
                *var_26 = var_25;
                var_27 = *var_3;
                var_28 = local_sp_1 + (-24L);
                *(uint64_t *)var_28 = 4228345UL;
                indirect_placeholder_1();
                _pre_phi87 = var_26;
                local_sp_0 = var_28;
                if ((uint64_t)(uint32_t)var_27 != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                var_29 = *var_7 + (-1L);
                var_30 = helper_cc_compute_c_wrapper(*var_17 - var_29, var_29, var_2, 17U);
                if (var_30 != 0UL) {
                    loop_state_var = 1U;
                    break;
                }
                var_31 = *var_17 + 1UL;
                var_32 = *var_6;
                *(uint64_t *)(local_sp_1 + (-32L)) = 4228390UL;
                var_33 = indirect_placeholder(var_32, var_31);
                *(uint64_t *)(var_0 + (-88L)) = var_33;
                if (var_33 != 0UL) {
                    loop_state_var = 1U;
                    break;
                }
                *var_6 = var_33;
                loop_state_var = 1U;
                break;
            }
            var_35 = *var_7;
            if (var_35 != 18446744073709551615UL) {
                var_47 = (uint32_t *)(var_0 + (-36L));
                *var_47 = 12U;
                _pre_phi87 = var_47;
                loop_state_var = 0U;
                break;
            }
            var_36 = (var_35 >> 1UL) ^ (-1L);
            var_37 = helper_cc_compute_c_wrapper(var_35 - var_36, var_36, var_2, 17U);
            if (var_37 == 0UL) {
                *var_7 = 18446744073709551615UL;
            } else {
                var_38 = *var_7;
                var_39 = var_38 + (var_38 >> 1UL);
                *var_7 = var_39;
                var_40 = var_39;
            }
            var_41 = *var_6;
            var_42 = local_sp_1 + (-16L);
            *(uint64_t *)var_42 = 4228518UL;
            var_43 = indirect_placeholder(var_41, var_40);
            *var_20 = var_43;
            local_sp_1 = var_42;
            if (var_43 == 0UL) {
                *var_6 = var_43;
                var_21 = *var_17;
                continue;
            }
            var_44 = local_sp_1 + (-24L);
            *(uint64_t *)var_44 = 4228534UL;
            indirect_placeholder_1();
            var_45 = *(volatile uint32_t *)(uint32_t *)0UL;
            var_46 = (uint32_t *)(var_0 + (-36L));
            *var_46 = var_45;
            _pre_phi87 = var_46;
            local_sp_0 = var_44;
            loop_state_var = 0U;
            break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            var_48 = *(uint32_t **)var_5;
            *(uint64_t *)(local_sp_0 + (-8L)) = 4228567UL;
            indirect_placeholder_1();
            *(uint64_t *)(local_sp_0 + (-16L)) = 4228572UL;
            indirect_placeholder_1();
            *var_48 = *_pre_phi87;
        }
        break;
      case 1U:
        {
            *(unsigned char *)(*var_17 + *var_6) = (unsigned char)'\x00';
            **(uint64_t **)var_4 = *var_17;
            var_34 = *var_6;
            rax_0 = var_34;
        }
        break;
    }
    return rax_0;
}
