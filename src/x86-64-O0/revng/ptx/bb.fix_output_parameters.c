typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_1(void);
extern uint64_t indirect_placeholder_16(uint64_t param_0);
void bb_fix_output_parameters(void) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t local_sp_260;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t local_sp_0;
    uint64_t var_7;
    uint64_t var_17;
    uint64_t *var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    uint64_t local_sp_1;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t local_sp_259;
    uint64_t local_sp_258_ph;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t local_sp_258;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t local_sp_3;
    uint64_t var_28;
    uint64_t storemerge;
    uint32_t *var_29;
    uint32_t var_30;
    uint64_t local_sp_4;
    uint32_t var_35;
    uint64_t var_36;
    uint64_t *var_37;
    uint64_t local_sp_5;
    unsigned char var_38;
    uint32_t var_42;
    uint32_t var_43;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint32_t var_34;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_3 = var_0 + (-72L);
    local_sp_260 = var_3;
    var_16 = 0UL;
    var_7 = 0UL;
    local_sp_1 = var_3;
    var_30 = 0U;
    var_42 = 128U;
    if (*(unsigned char *)4416640UL == '\x00') {
        *(uint64_t *)4417992UL = 0UL;
        var_4 = (uint64_t *)(var_0 + (-16L));
        *var_4 = 0UL;
        var_5 = (uint64_t *)(var_0 + (-24L));
        var_6 = (uint64_t *)(var_0 + (-32L));
        var_8 = (uint64_t)*(uint32_t *)4418056UL;
        var_9 = helper_cc_compute_c_wrapper(var_7 - var_8, var_8, var_2, 17U);
        while (var_9 != 0UL)
            {
                var_10 = *(uint64_t *)(*(uint64_t *)4418080UL + (*var_4 << 3UL)) + 1UL;
                *var_5 = var_10;
                var_11 = *var_4;
                if (var_11 == 0UL) {
                    *var_5 = (var_10 - *(uint64_t *)(*(uint64_t *)4418080UL + ((var_11 << 3UL) + (-8L))));
                }
                var_12 = local_sp_1 + (-8L);
                *(uint64_t *)var_12 = 4210591UL;
                indirect_placeholder_1();
                *var_6 = 0UL;
                var_13 = *(uint64_t *)(*(uint64_t *)4418072UL + (*var_4 << 3UL));
                local_sp_0 = var_12;
                if (var_13 == 0UL) {
                    var_14 = local_sp_1 + (-16L);
                    *(uint64_t *)var_14 = 4210652UL;
                    indirect_placeholder_1();
                    var_15 = var_13 + *var_6;
                    *var_6 = var_15;
                    var_16 = var_15;
                    local_sp_0 = var_14;
                }
                local_sp_1 = local_sp_0;
                if ((long)var_16 > (long)*(uint64_t *)4417992UL) {
                    *(uint64_t *)4417992UL = var_16;
                }
                var_17 = *var_4 + 1UL;
                *var_4 = var_17;
                var_7 = var_17;
                var_8 = (uint64_t)*(uint32_t *)4418056UL;
                var_9 = helper_cc_compute_c_wrapper(var_7 - var_8, var_8, var_2, 17U);
            }
        var_18 = *(uint64_t *)4417992UL;
        *(uint64_t *)4417992UL = (var_18 + 1UL);
        var_19 = var_18 + 2UL;
        var_20 = local_sp_1 + (-8L);
        *(uint64_t *)var_20 = 4210747UL;
        var_21 = indirect_placeholder_16(var_19);
        *(uint64_t *)4418560UL = var_21;
        local_sp_260 = var_20;
        local_sp_259 = var_20;
        if (*(unsigned char *)4416640UL == '\x00') {
            local_sp_259 = local_sp_260;
            local_sp_258_ph = local_sp_260;
            if (*(unsigned char *)4416641UL == '\x00') {
                var_23 = *(uint64_t *)4416296UL;
                local_sp_258 = local_sp_258_ph;
            } else {
                local_sp_258_ph = local_sp_259;
                local_sp_258 = local_sp_259;
                if (*(unsigned char *)4416642UL == '\x01') {
                    var_23 = *(uint64_t *)4416296UL;
                    local_sp_258 = local_sp_258_ph;
                } else {
                    var_22 = *(uint64_t *)4416296UL - (*(uint64_t *)4416304UL + *(uint64_t *)4417992UL);
                    *(uint64_t *)4416296UL = var_22;
                    var_23 = var_22;
                }
            }
        } else {
            local_sp_258_ph = local_sp_259;
            local_sp_258 = local_sp_259;
            if (*(unsigned char *)4416642UL == '\x01') {
                var_22 = *(uint64_t *)4416296UL - (*(uint64_t *)4416304UL + *(uint64_t *)4417992UL);
                *(uint64_t *)4416296UL = var_22;
                var_23 = var_22;
            } else {
                var_23 = *(uint64_t *)4416296UL;
                local_sp_258 = local_sp_258_ph;
            }
        }
    } else {
        local_sp_259 = local_sp_260;
        local_sp_258_ph = local_sp_260;
        if (*(unsigned char *)4416641UL == '\x00') {
            var_23 = *(uint64_t *)4416296UL;
            local_sp_258 = local_sp_258_ph;
        } else {
            local_sp_258_ph = local_sp_259;
            local_sp_258 = local_sp_259;
            if (*(unsigned char *)4416642UL == '\x01') {
                var_22 = *(uint64_t *)4416296UL - (*(uint64_t *)4416304UL + *(uint64_t *)4417992UL);
                *(uint64_t *)4416296UL = var_22;
                var_23 = var_22;
            } else {
                var_23 = *(uint64_t *)4416296UL;
                local_sp_258 = local_sp_258_ph;
            }
        }
    }
    var_24 = var_23;
    local_sp_3 = local_sp_258;
    if ((long)var_23 > (long)18446744073709551615UL) {
        *(uint64_t *)4416296UL = 0UL;
        var_24 = 0UL;
    }
    var_25 = (uint64_t)((long)(var_24 + (var_24 >> 63UL)) >> (long)1UL);
    *(uint64_t *)4418400UL = var_25;
    *(uint64_t *)4418408UL = (var_25 - *(uint64_t *)4416304UL);
    *(uint64_t *)4418416UL = *(uint64_t *)4418400UL;
    var_26 = *(uint64_t *)4416312UL;
    if (var_26 == 0UL) {
        *(uint64_t *)4416312UL = 0UL;
    } else {
        var_27 = local_sp_258 + (-8L);
        *(uint64_t *)var_27 = 4210956UL;
        indirect_placeholder_1();
        *(uint64_t *)4418424UL = var_26;
        local_sp_3 = var_27;
    }
    local_sp_4 = local_sp_3;
    if (*(unsigned char *)4416288UL == '\x00') {
        storemerge = *(uint64_t *)4418416UL - ((*(uint64_t *)4418424UL << 1UL) | 1UL);
    } else {
        var_28 = *(uint64_t *)4418408UL - (*(uint64_t *)4418424UL << 1UL);
        *(uint64_t *)4418408UL = (((long)var_28 > (long)18446744073709551615UL) ? var_28 : 0UL);
        storemerge = *(uint64_t *)4418416UL - (*(uint64_t *)4418424UL << 1UL);
    }
    *(uint64_t *)4418416UL = storemerge;
    var_29 = (uint32_t *)(var_0 + (-36L));
    *var_29 = 0U;
    local_sp_5 = local_sp_4;
    while ((int)var_30 <= (int)255U)
        {
            var_31 = (uint64_t)var_30;
            var_32 = local_sp_4 + (-8L);
            *(uint64_t *)var_32 = 4211116UL;
            var_33 = indirect_placeholder_16(var_31);
            *(unsigned char *)((uint64_t)*var_29 + 4418144UL) = ((uint64_t)(uint32_t)var_33 != 0UL);
            var_34 = *var_29 + 1U;
            *var_29 = var_34;
            var_30 = var_34;
            local_sp_4 = var_32;
            local_sp_5 = local_sp_4;
        }
    *(unsigned char *)4418156UL = (unsigned char)'\x01';
    var_35 = *(uint32_t *)4416644UL;
    if ((uint64_t)(var_35 + (-3)) == 0UL) {
        var_36 = var_0 + (-48L);
        var_37 = (uint64_t *)var_36;
        *var_37 = 4384156UL;
        var_38 = **(unsigned char **)var_36;
        while (var_38 != '\x00')
            {
                var_39 = (uint64_t)(uint32_t)(uint64_t)var_38;
                var_40 = local_sp_5 + (-8L);
                *(uint64_t *)var_40 = 4211226UL;
                var_41 = indirect_placeholder_16(var_39);
                *(unsigned char *)((uint64_t)(unsigned char)var_41 + 4418144UL) = (unsigned char)'\x01';
                *var_37 = (*var_37 + 1UL);
                local_sp_5 = var_40;
                var_38 = **(unsigned char **)var_36;
            }
        *var_29 = 128U;
        while ((int)var_42 <= (int)255U)
            {
                *(unsigned char *)((uint64_t)var_42 + 4418144UL) = (*(unsigned char *)(uint64_t)((uint32_t)(unsigned char)var_42 + 4388032U) != '\x00');
                var_43 = *var_29 + 1U;
                *var_29 = var_43;
                var_42 = var_43;
            }
    }
    if (!(!(((uint64_t)(var_35 & (-4)) != 0UL) || ((uint64_t)(var_35 & (-2)) == 0UL)) && (uint64_t)(var_35 + (-2)) == 0UL)) {
        return;
    }
    *(unsigned char *)4418178UL = (unsigned char)'\x01';
    return;
}
