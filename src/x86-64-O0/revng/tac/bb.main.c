typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_40_ret_type;
struct indirect_placeholder_42_ret_type;
struct indirect_placeholder_41_ret_type;
struct indirect_placeholder_44_ret_type;
struct indirect_placeholder_40_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_42_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_41_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_44_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_1(uint64_t param_0);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern void indirect_placeholder_16(uint64_t param_0);
extern void indirect_placeholder_31(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_32(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_43(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_40_ret_type indirect_placeholder_40(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_42_ret_type indirect_placeholder_42(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_41_ret_type indirect_placeholder_41(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_44_ret_type indirect_placeholder_44(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_40_ret_type var_27;
    struct indirect_placeholder_44_ret_type var_13;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint32_t *var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint32_t *var_9;
    uint64_t local_sp_7;
    uint64_t storemerge6;
    uint64_t var_49;
    uint64_t local_sp_0;
    uint64_t var_50;
    bool var_51;
    uint64_t var_52;
    uint64_t *var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t local_sp_2;
    uint64_t var_43;
    uint64_t local_sp_1;
    uint64_t storemerge5;
    uint32_t var_44;
    uint64_t *var_45;
    uint64_t var_46;
    unsigned char *var_47;
    uint64_t *var_48;
    uint64_t var_26;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t r8_0;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    struct indirect_placeholder_41_ret_type var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t r8_1;
    uint64_t r9_0;
    uint64_t local_sp_3;
    uint64_t rax_0;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t local_sp_4;
    uint64_t r9_1;
    uint64_t local_sp_5;
    uint64_t storemerge;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t *var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t local_sp_6;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t *var_58;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint32_t var_14;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = var_0 + (-8L);
    *(uint64_t *)var_3 = var_1;
    var_4 = (uint32_t *)(var_0 + (-60L));
    *var_4 = (uint32_t)rdi;
    var_5 = var_0 + (-72L);
    var_6 = (uint64_t *)var_5;
    *var_6 = rsi;
    var_7 = **(uint64_t **)var_5;
    *(uint64_t *)(var_0 + (-80L)) = 4208845UL;
    indirect_placeholder_16(var_7);
    *(uint64_t *)(var_0 + (-88L)) = 4208860UL;
    indirect_placeholder();
    var_8 = var_0 + (-96L);
    *(uint64_t *)var_8 = 4208870UL;
    indirect_placeholder();
    *(uint64_t *)4400256UL = 4371934UL;
    *(uint64_t *)4400272UL = 1UL;
    *(unsigned char *)4400265UL = (unsigned char)'\x01';
    var_9 = (uint32_t *)(var_0 + (-28L));
    local_sp_7 = var_8;
    storemerge6 = 4372000UL;
    var_49 = 0UL;
    rax_0 = 1UL;
    storemerge = 8192UL;
    while (1U)
        {
            var_10 = *var_6;
            var_11 = (uint64_t)*var_4;
            var_12 = local_sp_7 + (-8L);
            *(uint64_t *)var_12 = 4209132UL;
            var_13 = indirect_placeholder_44(4370976UL, 4371964UL, var_11, 0UL, var_10);
            var_14 = (uint32_t)var_13.field_0;
            *var_9 = var_14;
            local_sp_2 = var_12;
            local_sp_3 = var_12;
            local_sp_6 = var_12;
            local_sp_7 = var_12;
            switch_state_var = 0;
            switch (var_14) {
              case 115U:
                {
                    *(uint64_t *)4400256UL = *(uint64_t *)4409424UL;
                    continue;
                }
                break;
              case 4294967295U:
                {
                    var_15 = var_13.field_1;
                    var_16 = var_13.field_2;
                    var_17 = var_13.field_3;
                    r8_0 = var_16;
                    r9_0 = var_17;
                    if (*(uint64_t *)4400272UL != 0UL) {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    if (**(unsigned char **)4400256UL != '\x00') {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    var_18 = local_sp_7 + (-16L);
                    *(uint64_t *)var_18 = 4209200UL;
                    indirect_placeholder_42(0UL, var_15, 4371969UL, 1UL, var_16, var_17, 0UL);
                    local_sp_2 = var_18;
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              default:
                {
                    if ((int)var_14 <= (int)115U) {
                        loop_state_var = 2U;
                        switch_state_var = 1;
                        break;
                    }
                    if (var_14 == 114U) {
                        *(uint64_t *)4400272UL = 0UL;
                        continue;
                    }
                    if ((int)var_14 <= (int)114U) {
                        loop_state_var = 2U;
                        switch_state_var = 1;
                        break;
                    }
                    if (var_14 == 98U) {
                        *(unsigned char *)4400265UL = (unsigned char)'\x00';
                        continue;
                    }
                    if ((int)var_14 <= (int)98U) {
                        loop_state_var = 2U;
                        switch_state_var = 1;
                        break;
                    }
                    switch_state_var = 0;
                    switch (var_14) {
                      case 4294967166U:
                        {
                            *(uint64_t *)(local_sp_7 + (-16L)) = 4209020UL;
                            indirect_placeholder_32(var_3, 0UL);
                            abort();
                        }
                        break;
                      case 4294967165U:
                        {
                            var_56 = *(uint64_t *)4399936UL;
                            var_57 = local_sp_7 + (-24L);
                            var_58 = (uint64_t *)var_57;
                            *var_58 = 0UL;
                            *(uint64_t *)(local_sp_7 + (-32L)) = 4209078UL;
                            indirect_placeholder_43(0UL, var_56, 4370712UL, 4371952UL, 4371936UL, 4371630UL);
                            *var_58 = 4209092UL;
                            indirect_placeholder();
                            local_sp_6 = var_57;
                            loop_state_var = 2U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      default:
                        {
                            loop_state_var = 2U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 2U:
        {
            *(uint64_t *)(local_sp_6 + (-8L)) = 4209102UL;
            indirect_placeholder_32(var_3, 1UL);
            abort();
        }
        break;
      case 1U:
      case 0U:
        {
            switch (loop_state_var) {
              case 0U:
                {
                    *(uint64_t *)4400320UL = 0UL;
                    *(uint64_t *)4400328UL = 0UL;
                    *(uint64_t *)4400352UL = 4400384UL;
                    *(uint64_t *)4400360UL = 0UL;
                    var_19 = *(uint64_t *)4400256UL;
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4209259UL;
                    indirect_placeholder();
                    var_20 = *(uint64_t *)4400256UL;
                    var_21 = local_sp_2 + (-16L);
                    *(uint64_t *)var_21 = 4209285UL;
                    var_22 = indirect_placeholder_41(4400320UL, var_20, var_19);
                    var_23 = var_22.field_0;
                    var_24 = var_22.field_1;
                    var_25 = var_22.field_2;
                    *(uint64_t *)(var_0 + (-40L)) = var_23;
                    r8_1 = var_24;
                    r9_1 = var_25;
                    local_sp_5 = var_21;
                    if (var_23 != 0UL) {
                        var_26 = local_sp_2 + (-24L);
                        *(uint64_t *)var_26 = 4209328UL;
                        var_27 = indirect_placeholder_40(0UL, var_23, 4371995UL, 1UL, var_24, var_25, 0UL);
                        var_28 = var_27.field_1;
                        var_29 = var_27.field_2;
                        r8_0 = var_28;
                        r9_0 = var_29;
                        local_sp_3 = var_26;
                        local_sp_4 = local_sp_3;
                        r8_1 = r8_0;
                        r9_1 = r9_0;
                        if (**(unsigned char **)4400256UL != '\x00') {
                            var_30 = *(uint64_t *)4400256UL;
                            var_31 = local_sp_3 + (-8L);
                            *(uint64_t *)var_31 = 4209357UL;
                            indirect_placeholder();
                            rax_0 = var_30;
                            local_sp_4 = var_31;
                        }
                        *(uint64_t *)4400272UL = rax_0;
                        *(uint64_t *)4400280UL = rax_0;
                        local_sp_5 = local_sp_4;
                    }
                }
                break;
              case 1U:
                {
                    local_sp_4 = local_sp_3;
                    r8_1 = r8_0;
                    r9_1 = r9_0;
                    if (**(unsigned char **)4400256UL == '\x00') {
                        var_30 = *(uint64_t *)4400256UL;
                        var_31 = local_sp_3 + (-8L);
                        *(uint64_t *)var_31 = 4209357UL;
                        indirect_placeholder();
                        rax_0 = var_30;
                        local_sp_4 = var_31;
                    }
                    *(uint64_t *)4400272UL = rax_0;
                    *(uint64_t *)4400280UL = rax_0;
                    local_sp_5 = local_sp_4;
                }
                break;
            }
            while (1U)
                {
                    *(uint64_t *)4400296UL = storemerge;
                    var_32 = storemerge >> 1UL;
                    var_33 = *(uint64_t *)4400272UL;
                    if (var_32 > var_33) {
                        if ((long)storemerge <= (long)18446744073709551615UL) {
                            storemerge = storemerge << 1UL;
                            continue;
                        }
                        *(uint64_t *)(local_sp_5 + (-8L)) = 4209415UL;
                        indirect_placeholder_31(var_3, r8_1, r9_1);
                        abort();
                    }
                    var_34 = (var_33 + storemerge) + 1UL;
                    var_35 = (uint64_t *)(var_0 + (-48L));
                    *var_35 = var_34;
                    var_36 = var_34 << 1UL;
                    *(uint64_t *)4400304UL = var_36;
                    var_37 = *(uint64_t *)4400296UL;
                    var_38 = *var_35;
                    if (var_38 <= var_37) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_39 = helper_cc_compute_c_wrapper(var_38 - var_36, var_36, var_2, 17U);
                    if (var_39 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_40 = *(uint64_t *)4400304UL;
                    var_41 = local_sp_5 + (-8L);
                    *(uint64_t *)var_41 = 4209542UL;
                    var_42 = indirect_placeholder_1(var_40);
                    *(uint64_t *)4400288UL = var_42;
                    local_sp_1 = var_41;
                    if (*(uint64_t *)4400272UL == 0UL) {
                        storemerge5 = var_42 + 1UL;
                        loop_state_var = 0U;
                        break;
                    }
                    var_43 = local_sp_5 + (-16L);
                    *(uint64_t *)var_43 = 4209597UL;
                    indirect_placeholder();
                    local_sp_1 = var_43;
                    storemerge5 = *(uint64_t *)4400272UL + *(uint64_t *)4400288UL;
                    loop_state_var = 0U;
                    break;
                }
            switch (loop_state_var) {
              case 1U:
                {
                    *(uint64_t *)(local_sp_5 + (-8L)) = 4209527UL;
                    indirect_placeholder_31(var_3, r8_1, r9_1);
                    abort();
                }
                break;
              case 0U:
                {
                    *(uint64_t *)4400288UL = storemerge5;
                    var_44 = *(uint32_t *)4400088UL;
                    if ((int)var_44 < (int)*var_4) {
                        storemerge6 = *var_6 + ((uint64_t)var_44 << 3UL);
                    }
                    var_45 = (uint64_t *)(var_0 + (-56L));
                    *var_45 = storemerge6;
                    var_46 = local_sp_1 + (-8L);
                    *(uint64_t *)var_46 = 4209701UL;
                    indirect_placeholder_32(1UL, 0UL);
                    var_47 = (unsigned char *)(var_0 + (-9L));
                    *var_47 = (unsigned char)'\x01';
                    var_48 = (uint64_t *)(var_0 + (-24L));
                    *var_48 = 0UL;
                    local_sp_0 = var_46;
                    var_50 = *(uint64_t *)(*var_45 + (var_49 << 3UL));
                    var_51 = (var_50 == 0UL);
                    var_52 = local_sp_0 + (-8L);
                    var_53 = (uint64_t *)var_52;
                    local_sp_0 = var_52;
                    while (!var_51)
                        {
                            *var_53 = 4209745UL;
                            var_54 = indirect_placeholder_1(var_50);
                            *var_47 = ((var_54 & (uint64_t)*var_47) != 0UL);
                            var_55 = *var_48 + 1UL;
                            *var_48 = var_55;
                            var_49 = var_55;
                            var_50 = *(uint64_t *)(*var_45 + (var_49 << 3UL));
                            var_51 = (var_50 == 0UL);
                            var_52 = local_sp_0 + (-8L);
                            var_53 = (uint64_t *)var_52;
                            local_sp_0 = var_52;
                        }
                    *var_53 = 4209809UL;
                    indirect_placeholder_2(0UL, 0UL);
                    if (*(unsigned char *)4400264UL != '\x00') {
                        *(uint64_t *)(local_sp_0 + (-16L)) = 4209830UL;
                        indirect_placeholder();
                    }
                    return;
                }
                break;
            }
        }
        break;
    }
}
