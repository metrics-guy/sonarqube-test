typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_divq_EAX_wrapper_ret_type;
struct type_5;
struct indirect_placeholder_64_ret_type;
struct indirect_placeholder_65_ret_type;
struct indirect_placeholder_67_ret_type;
struct indirect_placeholder_68_ret_type;
struct indirect_placeholder_69_ret_type;
struct indirect_placeholder_70_ret_type;
struct indirect_placeholder_71_ret_type;
struct indirect_placeholder_74_ret_type;
struct helper_divq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint32_t field_3;
    uint64_t field_4;
    uint32_t field_5;
    uint64_t field_6;
    uint32_t field_7;
    uint64_t field_8;
    uint32_t field_9;
    uint32_t field_10;
    unsigned char field_11;
    uint32_t field_12;
};
struct type_5 {
};
struct indirect_placeholder_64_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_65_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_67_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_68_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_69_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_70_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_71_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_74_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern void indirect_placeholder_31(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder_5(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern uint32_t init_state_0x8248(void);
extern struct helper_divq_EAX_wrapper_ret_type helper_divq_EAX_wrapper(struct type_5 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint64_t param_13, uint32_t param_14, uint64_t param_15, uint64_t param_16, uint64_t param_17, uint32_t param_18, uint32_t param_19);
extern struct indirect_placeholder_64_ret_type indirect_placeholder_64(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_65_ret_type indirect_placeholder_65(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_67_ret_type indirect_placeholder_67(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_68_ret_type indirect_placeholder_68(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_69_ret_type indirect_placeholder_69(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_70_ret_type indirect_placeholder_70(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_71_ret_type indirect_placeholder_71(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_74_ret_type indirect_placeholder_74(uint64_t param_0, uint64_t param_1);
uint64_t bb_tac_seekable(uint64_t r10, uint64_t rdx, uint64_t rdi, uint64_t r8, uint64_t r9, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint32_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint32_t var_9;
    uint32_t var_10;
    uint64_t var_11;
    uint32_t *var_12;
    uint64_t *var_13;
    uint64_t *var_14;
    unsigned char *var_15;
    unsigned char var_16;
    unsigned char *var_17;
    uint64_t var_18;
    uint64_t *var_19;
    uint64_t var_20;
    uint64_t *var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_25;
    struct helper_divq_EAX_wrapper_ret_type var_24;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t rcx_7;
    uint64_t r84_5;
    uint64_t rax_0;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t var_97;
    struct indirect_placeholder_65_ret_type var_98;
    uint64_t var_99;
    uint64_t var_100;
    uint64_t var_70;
    struct indirect_placeholder_67_ret_type var_71;
    uint64_t r95_1;
    uint64_t r84_1;
    uint64_t rcx_0;
    uint64_t local_sp_1;
    uint64_t r95_5;
    uint64_t var_43;
    uint64_t var_37;
    uint64_t var_35;
    uint64_t var_34;
    uint64_t local_sp_13;
    uint64_t *var_36;
    uint64_t rcx_1;
    uint64_t local_sp_2;
    uint64_t rcx_2_ph;
    uint64_t local_sp_3_ph;
    uint64_t local_sp_13_ph;
    uint64_t var_44;
    uint64_t rcx_2;
    uint64_t local_sp_3;
    uint64_t var_114;
    uint64_t var_115;
    uint64_t var_116;
    uint64_t var_45;
    uint64_t *var_46;
    uint64_t var_47;
    uint64_t *var_48;
    uint64_t _ph;
    uint64_t r95_2_ph;
    uint64_t r84_2_ph;
    uint64_t local_sp_11;
    uint64_t rcx_3_ph;
    uint64_t local_sp_4_ph;
    uint64_t r95_2_ph249;
    uint64_t r84_2_ph250;
    uint64_t rcx_3_ph251;
    uint64_t local_sp_4_ph252;
    uint64_t var_107;
    uint64_t *var_49;
    uint64_t *var_50;
    uint64_t *var_51;
    uint64_t *var_52;
    uint64_t *var_53;
    uint64_t rcx_9;
    uint64_t var_54;
    uint64_t r95_2;
    uint64_t r84_2;
    uint64_t rcx_3;
    uint64_t local_sp_4;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_63;
    uint64_t var_61;
    struct indirect_placeholder_69_ret_type var_62;
    uint64_t r95_3;
    uint64_t r84_3;
    uint64_t rcx_4;
    uint64_t local_sp_5;
    uint64_t r95_4;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    struct indirect_placeholder_70_ret_type var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t r84_4;
    uint64_t rcx_5;
    uint64_t local_sp_6;
    uint64_t rcx_6_ph;
    uint64_t local_sp_7_ph;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t local_sp_8;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_72;
    uint64_t var_73;
    bool var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_90;
    uint64_t var_79;
    uint64_t spec_select;
    uint64_t *var_80;
    uint64_t var_81;
    uint64_t *var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t *var_88;
    uint64_t var_89;
    uint64_t local_sp_9;
    uint64_t var_91;
    bool var_92;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_104;
    uint64_t var_106;
    uint64_t var_109;
    uint64_t _pre277;
    uint64_t var_105;
    uint64_t rcx_8;
    uint64_t local_sp_10;
    uint64_t var_108;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t var_112;
    uint64_t var_113;
    uint64_t var_38;
    uint64_t var_39;
    struct indirect_placeholder_71_ret_type var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_26;
    uint64_t *var_27;
    uint64_t *var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    struct indirect_placeholder_74_ret_type var_32;
    uint64_t var_33;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_cc_src2();
    var_4 = init_state_0x9018();
    var_5 = init_state_0x9010();
    var_6 = init_state_0x8408();
    var_7 = init_state_0x8328();
    var_8 = init_state_0x82d8();
    var_9 = init_state_0x9080();
    var_10 = init_state_0x8248();
    var_11 = var_0 + (-8L);
    *(uint64_t *)var_11 = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    var_12 = (uint32_t *)(var_0 + (-172L));
    *var_12 = (uint32_t)rdi;
    var_13 = (uint64_t *)(var_0 + (-184L));
    *var_13 = rsi;
    var_14 = (uint64_t *)(var_0 + (-192L));
    *var_14 = rdx;
    var_15 = (unsigned char *)(var_0 + (-49L));
    *var_15 = (unsigned char)'\x01';
    var_16 = **(unsigned char **)4400256UL;
    var_17 = (unsigned char *)(var_0 + (-50L));
    *var_17 = var_16;
    var_18 = *(uint64_t *)4400256UL + 1UL;
    var_19 = (uint64_t *)(var_0 + (-64L));
    *var_19 = var_18;
    var_20 = *(uint64_t *)4400280UL + (-1L);
    var_21 = (uint64_t *)(var_0 + (-72L));
    *var_21 = var_20;
    var_22 = *var_14;
    var_23 = *(uint64_t *)4400296UL;
    var_24 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), var_23, 4205591UL, r10, var_1, var_22, var_11, var_23, 0UL, rdi, r8, r9, rsi, var_4, var_5, var_6, var_7, var_8, var_9, var_10);
    var_25 = var_24.field_4;
    *(uint64_t *)(var_0 + (-80L)) = var_25;
    rax_0 = 0UL;
    _ph = 0UL;
    r95_2_ph = r9;
    r84_2_ph = r8;
    r95_2_ph249 = r9;
    r84_2_ph250 = r8;
    if (var_25 == 0UL) {
        local_sp_13_ph = var_0 + (-200L);
    } else {
        *var_14 = (*var_14 - var_25);
        var_26 = var_0 + (-208L);
        *(uint64_t *)var_26 = 4205655UL;
        indirect_placeholder();
        local_sp_13_ph = var_26;
    }
    var_27 = (uint64_t *)(var_0 + (-48L));
    var_28 = (uint64_t *)(var_0 + (-88L));
    local_sp_13 = local_sp_13_ph;
    while (1U)
        {
            var_29 = *(uint64_t *)4400296UL;
            var_30 = (uint64_t)*var_12;
            var_31 = local_sp_13 + (-8L);
            *(uint64_t *)var_31 = 4205893UL;
            var_32 = indirect_placeholder_74(var_29, var_30);
            var_33 = var_32.field_0;
            *var_27 = var_33;
            var_37 = var_33;
            local_sp_2 = var_31;
            if (var_33 != 0UL) {
                var_35 = var_32.field_1;
                break;
            }
            if (*var_14 != 0UL) {
                var_35 = var_32.field_1;
                break;
            }
            *var_28 = *(uint64_t *)4400296UL;
            var_34 = local_sp_13 + (-16L);
            *(uint64_t *)var_34 = 4205771UL;
            indirect_placeholder();
            *var_14 = (*var_14 - *(uint64_t *)4400296UL);
            local_sp_13 = var_34;
            continue;
        }
    var_36 = (uint64_t *)(var_0 + (-96L));
    rcx_1 = var_35;
    while (1U)
        {
            var_43 = var_37;
            rcx_2_ph = rcx_1;
            local_sp_3_ph = local_sp_2;
            if (var_37 != *(uint64_t *)4400296UL) {
                loop_state_var = 0U;
                break;
            }
            var_38 = (uint64_t)*var_12;
            var_39 = local_sp_2 + (-8L);
            *(uint64_t *)var_39 = 4205950UL;
            var_40 = indirect_placeholder_71(var_37, var_38);
            var_41 = var_40.field_0;
            var_42 = var_40.field_1;
            *var_36 = var_41;
            var_43 = 18446744073709551615UL;
            rcx_1 = var_42;
            local_sp_2 = var_39;
            rcx_2_ph = var_42;
            local_sp_3_ph = var_39;
            rcx_2 = var_42;
            local_sp_3 = var_39;
            if (var_41 != 0UL) {
                var_44 = *var_27;
                loop_state_var = 1U;
                break;
            }
            *var_27 = var_41;
            if (var_41 != 18446744073709551615UL) {
                loop_state_var = 0U;
                break;
            }
            *var_14 = (*var_36 + *var_14);
            var_37 = *var_27;
            continue;
        }
    switch (loop_state_var) {
      case 0U:
        {
            var_44 = var_43;
            rcx_2 = rcx_2_ph;
            local_sp_3 = local_sp_3_ph;
        }
        break;
      case 1U:
        {
            rcx_3_ph = rcx_2;
            local_sp_4_ph = local_sp_3;
            rcx_3_ph251 = rcx_2;
            local_sp_4_ph252 = local_sp_3;
            if (var_44 == 18446744073709551615UL) {
                var_114 = *var_13;
                *(uint64_t *)(local_sp_3 + (-8L)) = 4206048UL;
                var_115 = indirect_placeholder_5(var_114, 0UL, 3UL);
                *(uint64_t *)(local_sp_3 + (-16L)) = 4206056UL;
                indirect_placeholder();
                var_116 = (uint64_t)*(uint32_t *)var_115;
                *(uint64_t *)(local_sp_3 + (-24L)) = 4206083UL;
                indirect_placeholder_68(0UL, var_115, 4371650UL, 0UL, r8, r9, var_116);
            } else {
                var_45 = var_44 + *(uint64_t *)4400288UL;
                var_46 = (uint64_t *)(var_0 + (-40L));
                *var_46 = var_45;
                var_47 = var_0 + (-32L);
                var_48 = (uint64_t *)var_47;
                *var_48 = var_45;
                rax_0 = 1UL;
                if (*(uint64_t *)4400272UL != 0UL) {
                    *var_48 = (var_45 - *var_21);
                    _ph = *(uint64_t *)4400272UL;
                    r95_2_ph249 = r95_2_ph;
                    r84_2_ph250 = r84_2_ph;
                    rcx_3_ph251 = rcx_3_ph;
                    local_sp_4_ph252 = local_sp_4_ph;
                }
                while (1U)
                    {
                        var_49 = (uint64_t *)(var_0 + (-104L));
                        var_50 = (uint64_t *)(var_0 + (-112L));
                        var_51 = (uint64_t *)(var_0 + (-120L));
                        var_52 = (uint64_t *)(var_0 + (-128L));
                        var_53 = (uint64_t *)(var_0 + (-136L));
                        var_54 = _ph;
                        r95_2 = r95_2_ph249;
                        r84_2 = r84_2_ph250;
                        rcx_3 = rcx_3_ph251;
                        local_sp_4 = local_sp_4_ph252;
                        while (1U)
                            {
                                r84_5 = r84_2;
                                r95_5 = r95_2;
                                var_54 = 0UL;
                                r95_3 = r95_2;
                                r84_3 = r84_2;
                                rcx_4 = rcx_3;
                                local_sp_5 = local_sp_4;
                                rcx_6_ph = rcx_3;
                                local_sp_7_ph = local_sp_4;
                                if (var_54 != 0UL) {
                                    while (1U)
                                        {
                                            var_55 = *var_48;
                                            rcx_7 = rcx_6_ph;
                                            local_sp_8 = local_sp_7_ph;
                                            var_56 = var_55 + (-1L);
                                            *var_48 = var_56;
                                            rcx_6_ph = var_55;
                                            var_55 = var_56;
                                            do {
                                                var_56 = var_55 + (-1L);
                                                *var_48 = var_56;
                                                rcx_6_ph = var_55;
                                                var_55 = var_56;
                                            } while ((uint64_t)(*var_17 - **(unsigned char **)var_47) != 0UL);
                                            if (*var_21 == 0UL) {
                                                break;
                                            }
                                            var_57 = *var_19;
                                            var_58 = local_sp_7_ph + (-8L);
                                            *(uint64_t *)var_58 = 4206463UL;
                                            indirect_placeholder();
                                            local_sp_7_ph = var_58;
                                            rcx_7 = var_55;
                                            local_sp_8 = var_58;
                                            if ((uint64_t)(uint32_t)var_57 != 0UL) {
                                                continue;
                                            }
                                            break;
                                        }
                                    var_72 = *(uint64_t *)4400288UL;
                                    var_73 = helper_cc_compute_c_wrapper(*var_48 - var_72, var_72, var_3, 17U);
                                    r95_2_ph = r95_5;
                                    r84_2_ph = r84_5;
                                    r95_2 = r95_5;
                                    r84_2 = r84_5;
                                    local_sp_9 = local_sp_8;
                                    rcx_8 = rcx_7;
                                    local_sp_10 = local_sp_8;
                                    if (var_73 != 0UL) {
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    if (*(unsigned char *)4400265UL == '\x00') {
                                        var_110 = *var_46;
                                        var_111 = *var_48;
                                        var_112 = local_sp_8 + (-8L);
                                        *(uint64_t *)var_112 = 4207248UL;
                                        var_113 = indirect_placeholder_2(var_111, var_110);
                                        *var_46 = *var_48;
                                        rcx_9 = var_113;
                                        local_sp_11 = var_112;
                                    } else {
                                        var_104 = *var_48 + *(uint64_t *)4400280UL;
                                        *var_53 = var_104;
                                        var_109 = var_104;
                                        if (*var_15 == '\x01') {
                                            var_105 = *var_46;
                                            var_106 = var_105;
                                            if (var_104 == var_105) {
                                                var_107 = local_sp_8 + (-8L);
                                                *(uint64_t *)var_107 = 4207215UL;
                                                var_108 = indirect_placeholder_2(var_104, var_106);
                                                var_109 = *var_53;
                                                rcx_8 = var_108;
                                                local_sp_10 = var_107;
                                            }
                                        } else {
                                            _pre277 = *var_46;
                                            var_106 = _pre277;
                                            var_107 = local_sp_8 + (-8L);
                                            *(uint64_t *)var_107 = 4207215UL;
                                            var_108 = indirect_placeholder_2(var_104, var_106);
                                            var_109 = *var_53;
                                            rcx_8 = var_108;
                                            local_sp_10 = var_107;
                                        }
                                        *var_46 = var_109;
                                        *var_15 = (unsigned char)'\x00';
                                        rcx_9 = rcx_8;
                                        local_sp_11 = local_sp_10;
                                    }
                                    rcx_3_ph = rcx_9;
                                    local_sp_4_ph = local_sp_11;
                                    rcx_3 = rcx_9;
                                    local_sp_4 = local_sp_11;
                                    if (*(uint64_t *)4400272UL == 0UL) {
                                        continue;
                                    }
                                    loop_state_var = 0U;
                                    break;
                                }
                                var_59 = *var_48 - *(uint64_t *)4400288UL;
                                *var_49 = var_59;
                                *var_50 = var_59;
                                var_60 = 1UL - var_59;
                                *var_51 = var_60;
                                var_63 = var_60;
                                if ((long)var_60 > (long)1UL) {
                                    var_61 = local_sp_4 + (-8L);
                                    *(uint64_t *)var_61 = 4206232UL;
                                    var_62 = indirect_placeholder_69(0UL, rcx_3, 4371665UL, 1UL, r84_2, r95_2, 0UL);
                                    var_63 = *var_51;
                                    r95_3 = var_62.field_2;
                                    r84_3 = var_62.field_1;
                                    rcx_4 = var_62.field_0;
                                    local_sp_5 = var_61;
                                }
                                r84_1 = var_63;
                                r95_4 = r95_3;
                                r84_4 = r84_3;
                                rcx_5 = rcx_4;
                                local_sp_6 = local_sp_5;
                                if (var_63 == 1UL) {
                                    *var_48 = (*(uint64_t *)4400288UL + (-1L));
                                    r95_5 = r95_4;
                                    r84_5 = r84_4;
                                    rcx_7 = rcx_5;
                                    local_sp_8 = local_sp_6;
                                } else {
                                    *var_48 = (*(uint64_t *)4400288UL + (-1L));
                                    r95_5 = r95_4;
                                    r84_5 = r84_4;
                                    rcx_7 = rcx_5;
                                    local_sp_8 = local_sp_6;
                                }
                            }
                        switch_state_var = 0;
                        switch (loop_state_var) {
                          case 0U:
                            {
                                *var_48 = (*var_48 + (1UL - *(uint64_t *)4400280UL));
                            }
                            break;
                          case 1U:
                            {
                                var_74 = (*var_14 == 0UL);
                                var_75 = *(uint64_t *)4400288UL;
                                var_76 = *var_46;
                                if (!var_74) {
                                    *(uint64_t *)(local_sp_8 + (-8L)) = 4206516UL;
                                    indirect_placeholder_2(var_75, var_76);
                                    switch_state_var = 1;
                                    break;
                                }
                                var_77 = var_76 - var_75;
                                *var_27 = var_77;
                                var_78 = *(uint64_t *)4400296UL;
                                var_90 = var_78;
                                if (var_77 <= var_78) {
                                    var_79 = *(uint64_t *)4400272UL;
                                    spec_select = (var_79 == 0UL) ? 1UL : var_79;
                                    var_80 = (uint64_t *)(var_0 + (-144L));
                                    *var_80 = spec_select;
                                    var_81 = *(uint64_t *)4400304UL;
                                    var_82 = (uint64_t *)(var_0 + (-152L));
                                    *var_82 = var_81;
                                    var_83 = *(uint64_t *)4400296UL;
                                    *(uint64_t *)4400296UL = (var_83 << 1UL);
                                    var_84 = (*(uint64_t *)4400272UL + (var_83 << 2UL)) + 2UL;
                                    *(uint64_t *)4400304UL = var_84;
                                    if (*var_82 <= var_84) {
                                        *(uint64_t *)(local_sp_8 + (-8L)) = 4206681UL;
                                        indirect_placeholder_31(var_11, r84_5, r95_5);
                                        abort();
                                    }
                                    var_85 = *(uint64_t *)4400288UL - *var_80;
                                    var_86 = local_sp_8 + (-8L);
                                    *(uint64_t *)var_86 = 4206719UL;
                                    var_87 = indirect_placeholder_2(var_85, var_84);
                                    var_88 = (uint64_t *)(var_0 + (-160L));
                                    *var_88 = var_87;
                                    var_89 = var_87 + *var_80;
                                    *var_88 = var_89;
                                    *(uint64_t *)4400288UL = var_89;
                                    var_90 = *(uint64_t *)4400296UL;
                                    local_sp_9 = var_86;
                                }
                                var_91 = helper_cc_compute_c_wrapper(*var_14 - var_90, var_90, var_3, 17U);
                                var_92 = (var_91 == 0UL);
                                var_93 = *var_14;
                                if (var_92) {
                                    *var_14 = (var_93 - *(uint64_t *)4400296UL);
                                } else {
                                    *(uint64_t *)4400296UL = var_93;
                                    *var_14 = 0UL;
                                }
                                *(uint64_t *)(local_sp_9 + (-8L)) = 4206855UL;
                                indirect_placeholder();
                                *(uint64_t *)(local_sp_9 + (-16L)) = 4206959UL;
                                indirect_placeholder();
                                var_94 = *(uint64_t *)4400288UL + (*var_27 + *(uint64_t *)4400296UL);
                                *var_46 = var_94;
                                if (*(uint64_t *)4400272UL == 0UL) {
                                    *var_48 = var_94;
                                } else {
                                    *var_48 = (*(uint64_t *)4400296UL + *(uint64_t *)4400288UL);
                                }
                                var_95 = *(uint64_t *)4400296UL;
                                var_96 = (uint64_t)*var_12;
                                var_97 = local_sp_9 + (-24L);
                                *(uint64_t *)var_97 = 4207060UL;
                                var_98 = indirect_placeholder_65(var_95, var_96);
                                var_99 = var_98.field_0;
                                var_100 = var_98.field_1;
                                rcx_3_ph = var_100;
                                local_sp_4_ph = var_97;
                                if (var_99 != *(uint64_t *)4400296UL) {
                                    var_101 = *var_13;
                                    *(uint64_t *)(local_sp_9 + (-32L)) = 4207101UL;
                                    var_102 = indirect_placeholder_5(var_101, 0UL, 3UL);
                                    *(uint64_t *)(local_sp_9 + (-40L)) = 4207109UL;
                                    indirect_placeholder();
                                    var_103 = (uint64_t)*(uint32_t *)var_102;
                                    *(uint64_t *)(local_sp_9 + (-48L)) = 4207136UL;
                                    indirect_placeholder_64(0UL, var_102, 4371650UL, 0UL, r84_5, r95_5, var_103);
                                    switch_state_var = 1;
                                    break;
                                }
                            }
                            break;
                        }
                        if (switch_state_var)
                            break;
                    }
            }
            return rax_0;
        }
        break;
    }
}
