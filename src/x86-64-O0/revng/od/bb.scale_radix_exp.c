typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct helper_movq_mm_T0_xmm_wrapper_ret_type;
struct type_4;
struct helper_comisd_wrapper_133_ret_type;
struct type_7;
struct helper_cvtsi2sd_wrapper_134_ret_type;
struct helper_comisd_wrapper_ret_type;
struct helper_pxor_xmm_wrapper_ret_type;
struct helper_ucomisd_wrapper_ret_type;
struct helper_mulsd_wrapper_ret_type;
struct helper_cvtsi2sd_wrapper_ret_type;
struct helper_divsd_wrapper_ret_type;
struct helper_movq_mm_T0_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_4 {
};
struct helper_comisd_wrapper_133_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct type_7 {
};
struct helper_cvtsi2sd_wrapper_134_ret_type {
    uint64_t field_0;
};
struct helper_comisd_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_pxor_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_ucomisd_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_mulsd_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvtsi2sd_wrapper_ret_type {
    uint64_t field_0;
};
struct helper_divsd_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_1(void);
extern uint64_t init_state_0x8558(void);
extern unsigned char init_state_0x8549(void);
extern unsigned char init_state_0x854c(void);
extern unsigned char init_state_0x8548(void);
extern unsigned char init_state_0x854b(void);
extern unsigned char init_state_0x8547(void);
extern unsigned char init_state_0x854d(void);
extern struct helper_movq_mm_T0_xmm_wrapper_ret_type helper_movq_mm_T0_xmm_wrapper(struct type_4 *param_0, uint64_t param_1);
extern struct helper_comisd_wrapper_133_ret_type helper_comisd_wrapper_133(struct type_7 *param_0, struct type_4 *param_1, struct type_4 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_cvtsi2sd_wrapper_134_ret_type helper_cvtsi2sd_wrapper_134(struct type_7 *param_0, struct type_4 *param_1, uint32_t param_2);
extern struct helper_comisd_wrapper_ret_type helper_comisd_wrapper(struct type_7 *param_0, struct type_4 *param_1, struct type_4 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_pxor_xmm_wrapper_ret_type helper_pxor_xmm_wrapper(struct type_7 *param_0, struct type_4 *param_1, struct type_4 *param_2, uint64_t param_3, uint64_t param_4);
extern struct helper_ucomisd_wrapper_ret_type helper_ucomisd_wrapper(struct type_7 *param_0, struct type_4 *param_1, struct type_4 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_mulsd_wrapper_ret_type helper_mulsd_wrapper(struct type_7 *param_0, struct type_4 *param_1, struct type_4 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_cvtsi2sd_wrapper_ret_type helper_cvtsi2sd_wrapper(struct type_7 *param_0, struct type_4 *param_1, uint32_t param_2);
extern struct helper_divsd_wrapper_ret_type helper_divsd_wrapper(struct type_7 *param_0, struct type_4 *param_1, struct type_4 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
void bb_scale_radix_exp(uint64_t rdi, uint64_t rsi) {
    struct helper_pxor_xmm_wrapper_ret_type var_14;
    struct helper_pxor_xmm_wrapper_ret_type var_19;
    struct helper_pxor_xmm_wrapper_ret_type var_27;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    unsigned char var_3;
    unsigned char var_4;
    unsigned char var_5;
    unsigned char var_6;
    unsigned char var_7;
    unsigned char var_8;
    uint64_t *var_9;
    uint32_t *var_10;
    struct helper_ucomisd_wrapper_ret_type var_16;
    uint64_t *var_11;
    struct helper_movq_mm_T0_xmm_wrapper_ret_type var_57;
    uint64_t var_58;
    uint64_t var_12;
    uint64_t *var_13;
    uint64_t var_15;
    uint64_t var_17;
    unsigned char var_18;
    unsigned char state_0x8549_0;
    struct helper_ucomisd_wrapper_ret_type var_20;
    uint64_t var_21;
    uint64_t var_22;
    unsigned char state_0x8549_1;
    struct helper_cvtsi2sd_wrapper_ret_type var_23;
    struct helper_divsd_wrapper_ret_type var_24;
    uint64_t var_25;
    unsigned char var_26;
    uint64_t var_28;
    struct helper_ucomisd_wrapper_ret_type var_29;
    uint64_t var_30;
    unsigned char var_31;
    struct helper_pxor_xmm_wrapper_ret_type var_32;
    uint64_t var_33;
    struct helper_ucomisd_wrapper_ret_type var_34;
    uint64_t var_35;
    unsigned char var_36;
    unsigned char state_0x8549_1_be;
    struct helper_pxor_xmm_wrapper_ret_type var_37;
    uint64_t var_38;
    struct helper_ucomisd_wrapper_ret_type var_39;
    unsigned char var_40;
    struct helper_pxor_xmm_wrapper_ret_type var_41;
    struct helper_ucomisd_wrapper_ret_type var_42;
    uint64_t var_43;
    unsigned char state_0x8549_2;
    uint64_t rax_0;
    struct helper_cvtsi2sd_wrapper_ret_type var_44;
    struct helper_divsd_wrapper_ret_type var_45;
    struct helper_comisd_wrapper_ret_type var_46;
    uint64_t var_56;
    unsigned char var_47;
    struct helper_cvtsi2sd_wrapper_ret_type var_48;
    struct helper_divsd_wrapper_ret_type var_49;
    struct helper_comisd_wrapper_133_ret_type var_50;
    uint64_t var_55;
    unsigned char var_51;
    struct helper_cvtsi2sd_wrapper_134_ret_type var_52;
    struct helper_mulsd_wrapper_ret_type var_53;
    unsigned char var_54;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_state_0x8558();
    var_3 = init_state_0x8549();
    var_4 = init_state_0x854c();
    var_5 = init_state_0x8548();
    var_6 = init_state_0x854b();
    var_7 = init_state_0x8547();
    var_8 = init_state_0x854d();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_9 = (uint64_t *)(var_0 + (-32L));
    *var_9 = var_2;
    var_10 = (uint32_t *)(var_0 + (-36L));
    *var_10 = (uint32_t)rdi;
    *(uint64_t *)(var_0 + (-48L)) = rsi;
    var_11 = (uint64_t *)(var_0 + (-16L));
    *var_11 = rsi;
    if (*var_10 == 2U) {
        var_57 = helper_movq_mm_T0_xmm_wrapper((struct type_4 *)(776UL), *var_9);
        var_58 = var_57.field_0;
        *(uint64_t *)(var_0 + (-64L)) = 4237720UL;
        indirect_placeholder_1();
        rax_0 = var_58;
    } else {
        var_12 = *var_9;
        var_13 = (uint64_t *)(var_0 + (-24L));
        *var_13 = var_12;
        var_14 = helper_pxor_xmm_wrapper((struct type_7 *)(0UL), (struct type_4 *)(776UL), (struct type_4 *)(776UL), var_12, 0UL);
        var_15 = var_14.field_0;
        var_16 = helper_ucomisd_wrapper((struct type_7 *)(0UL), (struct type_4 *)(776UL), (struct type_4 *)(2824UL), var_15, *var_13, var_3, var_4);
        var_17 = var_16.field_0;
        var_18 = var_16.field_1;
        state_0x8549_0 = var_18;
        if ((var_17 & 4UL) != 0UL) {
            var_19 = helper_pxor_xmm_wrapper((struct type_7 *)(0UL), (struct type_4 *)(776UL), (struct type_4 *)(776UL), var_15, var_14.field_1);
            var_20 = helper_ucomisd_wrapper((struct type_7 *)(0UL), (struct type_4 *)(776UL), (struct type_4 *)(2824UL), var_19.field_0, *var_13, var_18, var_4);
            state_0x8549_0 = var_20.field_1;
            if ((var_20.field_0 & 64UL) != 0UL) {
                rax_0 = *var_13;
                helper_movq_mm_T0_xmm_wrapper((struct type_4 *)(776UL), rax_0);
                return;
            }
        }
        var_21 = *var_11;
        var_22 = var_21;
        state_0x8549_1 = state_0x8549_0;
        var_43 = var_21;
        state_0x8549_2 = state_0x8549_0;
        if ((long)var_21 > (long)18446744073709551615UL) {
            while (1U)
                {
                    *var_11 = (var_22 + 1UL);
                    if (var_22 != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_23 = helper_cvtsi2sd_wrapper((struct type_7 *)(0UL), (struct type_4 *)(840UL), *var_10);
                    var_24 = helper_divsd_wrapper((struct type_7 *)(0UL), (struct type_4 *)(776UL), (struct type_4 *)(840UL), *var_13, var_23.field_0, state_0x8549_1, var_4, var_5, var_6, var_7, var_8);
                    var_25 = var_24.field_0;
                    var_26 = var_24.field_1;
                    *var_13 = var_25;
                    var_27 = helper_pxor_xmm_wrapper((struct type_7 *)(0UL), (struct type_4 *)(776UL), (struct type_4 *)(776UL), var_25, 0UL);
                    var_28 = var_27.field_0;
                    var_29 = helper_ucomisd_wrapper((struct type_7 *)(0UL), (struct type_4 *)(776UL), (struct type_4 *)(2824UL), var_28, *var_13, var_26, var_4);
                    var_30 = var_29.field_0;
                    var_31 = var_29.field_1;
                    state_0x8549_1_be = var_31;
                    var_32 = helper_pxor_xmm_wrapper((struct type_7 *)(0UL), (struct type_4 *)(776UL), (struct type_4 *)(776UL), var_28, var_27.field_1);
                    var_33 = var_32.field_0;
                    var_34 = helper_ucomisd_wrapper((struct type_7 *)(0UL), (struct type_4 *)(776UL), (struct type_4 *)(2824UL), var_33, *var_13, var_31, var_4);
                    var_35 = var_34.field_0;
                    var_36 = var_34.field_1;
                    state_0x8549_1_be = var_36;
                    if ((var_30 & 4UL) != 0UL & (var_35 & 64UL) != 0UL) {
                        var_37 = helper_pxor_xmm_wrapper((struct type_7 *)(0UL), (struct type_4 *)(776UL), (struct type_4 *)(776UL), var_33, var_32.field_1);
                        var_38 = var_37.field_0;
                        var_39 = helper_ucomisd_wrapper((struct type_7 *)(0UL), (struct type_4 *)(776UL), (struct type_4 *)(2824UL), var_38, *var_9, var_36, var_4);
                        if ((var_39.field_0 & 4UL) != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_40 = var_39.field_1;
                        var_41 = helper_pxor_xmm_wrapper((struct type_7 *)(0UL), (struct type_4 *)(776UL), (struct type_4 *)(776UL), var_38, var_37.field_1);
                        var_42 = helper_ucomisd_wrapper((struct type_7 *)(0UL), (struct type_4 *)(776UL), (struct type_4 *)(2824UL), var_41.field_0, *var_9, var_40, var_4);
                        if ((var_42.field_0 & 64UL) != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        state_0x8549_1_be = var_42.field_1;
                    }
                    var_22 = *var_11;
                    state_0x8549_1 = state_0x8549_1_be;
                    continue;
                }
            switch (loop_state_var) {
              case 1U:
                {
                    *(uint64_t *)(var_0 + (-64L)) = 4237851UL;
                    indirect_placeholder_1();
                    *(uint32_t *)var_22 = 34U;
                }
                break;
              case 0U:
                {
                    rax_0 = *var_13;
                }
                break;
            }
        } else {
            while (1U)
                {
                    *var_11 = (var_43 + (-1L));
                    if (var_43 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_44 = helper_cvtsi2sd_wrapper((struct type_7 *)(0UL), (struct type_4 *)(840UL), *var_10);
                    var_45 = helper_divsd_wrapper((struct type_7 *)(0UL), (struct type_4 *)(776UL), (struct type_4 *)(840UL), *(uint64_t *)4303792UL, var_44.field_0, state_0x8549_2, var_4, var_5, var_6, var_7, var_8);
                    var_46 = helper_comisd_wrapper((struct type_7 *)(0UL), (struct type_4 *)(776UL), (struct type_4 *)(2824UL), var_45.field_0, *var_13, var_45.field_1, var_4);
                    if ((var_46.field_0 & 65UL) != 0UL) {
                        *(uint64_t *)(var_0 + (-64L)) = 4237913UL;
                        indirect_placeholder_1();
                        *(uint32_t *)var_43 = 34U;
                        var_56 = *(uint64_t *)4303800UL;
                        rax_0 = var_56;
                        loop_state_var = 0U;
                        break;
                    }
                    var_47 = var_46.field_1;
                    var_48 = helper_cvtsi2sd_wrapper((struct type_7 *)(0UL), (struct type_4 *)(840UL), *var_10);
                    var_49 = helper_divsd_wrapper((struct type_7 *)(0UL), (struct type_4 *)(776UL), (struct type_4 *)(840UL), *(uint64_t *)4303808UL, var_48.field_0, var_47, var_4, var_5, var_6, var_7, var_8);
                    var_50 = helper_comisd_wrapper_133((struct type_7 *)(0UL), (struct type_4 *)(776UL), (struct type_4 *)(840UL), *var_13, var_49.field_0, var_49.field_1, var_4);
                    if ((var_50.field_0 & 65UL) != 0UL) {
                        *(uint64_t *)(var_0 + (-64L)) = 4237965UL;
                        indirect_placeholder_1();
                        *(uint32_t *)var_43 = 34U;
                        var_55 = *(uint64_t *)4303816UL;
                        rax_0 = var_55;
                        loop_state_var = 0U;
                        break;
                    }
                    var_51 = var_50.field_1;
                    var_52 = helper_cvtsi2sd_wrapper_134((struct type_7 *)(0UL), (struct type_4 *)(776UL), *var_10);
                    var_53 = helper_mulsd_wrapper((struct type_7 *)(0UL), (struct type_4 *)(776UL), (struct type_4 *)(840UL), var_52.field_0, *var_13, var_51, var_4, var_5, var_6, var_7, var_8);
                    var_54 = var_53.field_1;
                    *var_13 = var_53.field_0;
                    var_43 = *var_11;
                    state_0x8549_2 = var_54;
                    continue;
                }
            rax_0 = *var_13;
        }
    }
}
