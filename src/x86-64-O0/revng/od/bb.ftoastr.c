typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_ftoastr_ret_type;
struct helper_movq_mm_T0_xmm_wrapper_ret_type;
struct type_5;
struct helper_pxor_xmm_wrapper_ret_type;
struct type_8;
struct helper_comiss_wrapper_ret_type;
struct helper_pxor_xmm_wrapper_132_ret_type;
struct helper_cvtss2sd_wrapper_ret_type;
struct helper_ucomiss_wrapper_ret_type;
struct bb_ftoastr_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct helper_movq_mm_T0_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_5 {
};
struct helper_pxor_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_8 {
};
struct helper_comiss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_pxor_xmm_wrapper_132_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_cvtss2sd_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_ucomiss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_1(void);
extern uint64_t init_state_0x8558(void);
extern unsigned char init_state_0x8549(void);
extern unsigned char init_state_0x854c(void);
extern unsigned char init_state_0x854d(void);
extern struct helper_movq_mm_T0_xmm_wrapper_ret_type helper_movq_mm_T0_xmm_wrapper(struct type_5 *param_0, uint64_t param_1);
extern struct helper_pxor_xmm_wrapper_ret_type helper_pxor_xmm_wrapper(struct type_8 *param_0, struct type_5 *param_1, struct type_5 *param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_state_0x8560(void);
extern uint64_t init_state_0x8d58(void);
extern struct helper_comiss_wrapper_ret_type helper_comiss_wrapper(struct type_8 *param_0, struct type_5 *param_1, struct type_5 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_pxor_xmm_wrapper_132_ret_type helper_pxor_xmm_wrapper_132(struct type_8 *param_0, struct type_5 *param_1, struct type_5 *param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern uint64_t indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct helper_cvtss2sd_wrapper_ret_type helper_cvtss2sd_wrapper(struct type_8 *param_0, struct type_5 *param_1, struct type_5 *param_2, uint64_t param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_ucomiss_wrapper_ret_type helper_ucomiss_wrapper(struct type_8 *param_0, struct type_5 *param_1, struct type_5 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
struct bb_ftoastr_ret_type bb_ftoastr(uint64_t rcx, uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t r9) {
    struct helper_pxor_xmm_wrapper_ret_type var_22;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    unsigned char var_5;
    unsigned char var_6;
    unsigned char var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t *var_10;
    uint32_t *var_11;
    struct helper_comiss_wrapper_ret_type var_23;
    uint32_t *var_12;
    uint64_t var_13;
    uint32_t var_14;
    uint32_t *var_15;
    uint64_t var_16;
    uint64_t var_17;
    struct helper_cvtss2sd_wrapper_ret_type var_18;
    uint64_t var_19;
    unsigned char var_20;
    uint64_t *var_21;
    unsigned char var_24;
    bool var_25;
    uint64_t var_26;
    uint64_t state_0x8558_0;
    uint64_t var_62;
    struct helper_ucomiss_wrapper_ret_type var_63;
    uint64_t state_0x8d58_0;
    uint32_t var_65;
    uint64_t var_55;
    uint64_t var_56;
    unsigned char state_0x8549_1;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t local_sp_1;
    struct helper_ucomiss_wrapper_ret_type var_59;
    uint64_t var_60;
    unsigned char var_61;
    unsigned char state_0x8549_0;
    uint64_t local_sp_0;
    uint32_t var_64;
    uint32_t var_44;
    struct bb_ftoastr_ret_type mrv;
    struct bb_ftoastr_ret_type mrv1;
    struct bb_ftoastr_ret_type mrv2;
    struct bb_ftoastr_ret_type mrv3;
    struct helper_pxor_xmm_wrapper_132_ret_type var_27;
    uint32_t var_28;
    uint32_t *var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t *var_32;
    unsigned char **var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    unsigned char spec_select;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    struct helper_comiss_wrapper_ret_type var_40;
    unsigned char var_41;
    uint32_t storemerge;
    uint32_t *var_42;
    uint32_t *var_43;
    uint64_t state_0x8d58_1;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    struct helper_movq_mm_T0_xmm_wrapper_ret_type var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint32_t var_54;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_state_0x8558();
    var_3 = init_state_0x8560();
    var_4 = init_state_0x8d58();
    var_5 = init_state_0x8549();
    var_6 = init_state_0x854c();
    var_7 = init_state_0x854d();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_8 = var_0 + (-88L);
    var_9 = (uint64_t *)(var_0 + (-64L));
    *var_9 = rdi;
    var_10 = (uint64_t *)(var_0 + (-72L));
    *var_10 = rsi;
    var_11 = (uint32_t *)(var_0 + (-76L));
    *var_11 = (uint32_t)rdx;
    var_12 = (uint32_t *)(var_0 + (-80L));
    *var_12 = (uint32_t)rcx;
    var_13 = var_0 + (-84L);
    var_14 = (uint32_t)var_2;
    var_15 = (uint32_t *)var_13;
    *var_15 = var_14;
    var_16 = (uint64_t)var_14;
    var_17 = var_4 & (-4294967296L);
    var_18 = helper_cvtss2sd_wrapper((struct type_8 *)(0UL), (struct type_5 *)(776UL), (struct type_5 *)(2824UL), var_17 | var_16, var_5, var_6, var_7);
    var_19 = var_18.field_0;
    var_20 = var_18.field_1;
    var_21 = (uint64_t *)(var_0 + (-24L));
    *var_21 = var_19;
    var_22 = helper_pxor_xmm_wrapper((struct type_8 *)(0UL), (struct type_5 *)(776UL), (struct type_5 *)(776UL), var_19, var_3);
    var_23 = helper_comiss_wrapper((struct type_8 *)(0UL), (struct type_5 *)(776UL), (struct type_5 *)(2824UL), var_22.field_0, var_17 | (uint64_t)*var_15, var_20, var_6);
    var_24 = var_23.field_1;
    var_25 = ((var_23.field_0 & 65UL) == 0UL);
    var_26 = (uint64_t)*var_15;
    state_0x8558_0 = var_26;
    local_sp_1 = var_8;
    if (var_25) {
        var_27 = helper_pxor_xmm_wrapper_132((struct type_8 *)(0UL), (struct type_5 *)(776UL), (struct type_5 *)(840UL), var_26, 0UL, (uint64_t)*(uint32_t *)4299680UL, 0UL);
        state_0x8558_0 = var_27.field_0;
    }
    var_28 = (uint32_t)state_0x8558_0;
    var_29 = (uint32_t *)(var_0 + (-28L));
    *var_29 = var_28;
    var_30 = var_0 + (-55L);
    var_31 = var_0 + (-40L);
    var_32 = (uint64_t *)var_31;
    *var_32 = (var_30 + 1UL);
    *(unsigned char *)var_30 = (unsigned char)'%';
    var_33 = (unsigned char **)var_31;
    **var_33 = (unsigned char)'-';
    *var_32 = (*var_32 + (uint64_t)(*var_11 & 1U));
    **var_33 = (unsigned char)'+';
    *var_32 = (*var_32 + (uint64_t)((*var_11 >> 1U) & 1U));
    **var_33 = (unsigned char)' ';
    *var_32 = (*var_32 + (uint64_t)((*var_11 >> 2U) & 1U));
    **var_33 = (unsigned char)'0';
    var_34 = *var_32 + (uint64_t)((*var_11 >> 3U) & 1U);
    *var_32 = (var_34 + 1UL);
    *(unsigned char *)var_34 = (unsigned char)'*';
    var_35 = *var_32;
    *var_32 = (var_35 + 1UL);
    *(unsigned char *)var_35 = (unsigned char)'.';
    var_36 = *var_32;
    *var_32 = (var_36 + 1UL);
    *(unsigned char *)var_36 = (unsigned char)'*';
    **var_33 = (unsigned char)'L';
    spec_select = ((*var_11 & 16U) == 0U) ? 'g' : 'G';
    var_37 = *var_32;
    *var_32 = (var_37 + 1UL);
    *(unsigned char *)var_37 = spec_select;
    **var_33 = (unsigned char)'\x00';
    var_38 = (uint64_t)*(uint32_t *)4299696UL;
    var_39 = var_17 | (uint64_t)*var_29;
    var_40 = helper_comiss_wrapper((struct type_8 *)(0UL), (struct type_5 *)(776UL), (struct type_5 *)(2824UL), var_38, var_39, var_24, var_6);
    var_41 = var_40.field_1;
    storemerge = ((var_40.field_0 & 65UL) == 0UL) ? 1U : 6U;
    var_42 = (uint32_t *)(var_0 + (-12L));
    *var_42 = storemerge;
    var_43 = (uint32_t *)(var_0 + (-44L));
    var_44 = storemerge;
    state_0x8d58_1 = var_39;
    state_0x8549_1 = var_41;
    while (1U)
        {
            var_45 = *var_21;
            var_46 = (uint64_t)var_44;
            var_47 = (uint64_t)*var_12;
            var_48 = *var_10;
            var_49 = *var_9;
            var_50 = helper_movq_mm_T0_xmm_wrapper((struct type_5 *)(776UL), var_45);
            var_51 = var_50.field_0;
            var_52 = local_sp_1 + (-8L);
            *(uint64_t *)var_52 = 4221237UL;
            var_53 = indirect_placeholder_7(1UL, var_47, var_30, var_49, var_48, r9, var_46);
            var_54 = (uint32_t)var_53;
            *var_43 = var_54;
            state_0x8d58_0 = state_0x8d58_1;
            state_0x8549_0 = state_0x8549_1;
            local_sp_0 = var_52;
            var_65 = var_54;
            if (!((int)var_54 < (int)0U && (int)*var_42 > (int)8U)) {
                loop_state_var = 1U;
                break;
            }
            if (*var_10 <= (uint64_t)var_54) {
                var_55 = local_sp_1 + (-16L);
                *(uint64_t *)var_55 = 4221280UL;
                indirect_placeholder_1();
                var_56 = (uint64_t)*var_15;
                var_57 = state_0x8d58_1 & (-4294967296L);
                var_58 = var_57 | var_56;
                var_59 = helper_ucomiss_wrapper((struct type_8 *)(0UL), (struct type_5 *)(776UL), (struct type_5 *)(2824UL), var_51, var_58, state_0x8549_1, var_6);
                var_60 = var_59.field_0;
                var_61 = var_59.field_1;
                state_0x8d58_0 = var_58;
                state_0x8549_0 = var_61;
                local_sp_0 = var_55;
                var_62 = var_57 | (uint64_t)*var_15;
                var_63 = helper_ucomiss_wrapper((struct type_8 *)(0UL), (struct type_5 *)(776UL), (struct type_5 *)(2824UL), var_51, var_62, var_61, var_6);
                state_0x8d58_0 = var_62;
                state_0x8549_0 = var_63.field_1;
                if ((var_60 & 4UL) != 0UL & (var_63.field_0 & 64UL) != 0UL) {
                    var_65 = *var_43;
                    loop_state_var = 0U;
                    break;
                }
            }
            var_64 = *var_42 + 1U;
            *var_42 = var_64;
            var_44 = var_64;
            state_0x8d58_1 = state_0x8d58_0;
            state_0x8549_1 = state_0x8549_0;
            local_sp_1 = local_sp_0;
            continue;
        }
    switch (loop_state_var) {
      case 0U:
        {
            mrv.field_0 = (uint64_t)var_65;
            mrv1 = mrv;
            mrv1.field_1 = var_47;
            mrv2 = mrv1;
            mrv2.field_2 = r9;
            mrv3 = mrv2;
            mrv3.field_3 = var_46;
            return mrv3;
        }
        break;
      case 1U:
        {
        }
        break;
    }
}
