typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_45_ret_type;
struct indirect_placeholder_46_ret_type;
struct indirect_placeholder_45_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_46_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_12(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_1(void);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern uint64_t init_rcx(void);
extern struct indirect_placeholder_45_ret_type indirect_placeholder_45(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_46_ret_type indirect_placeholder_46(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
uint64_t bb_read_input(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_45_ret_type var_16;
    uint64_t r8_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t var_7;
    unsigned char *var_8;
    uint64_t *var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t local_sp_1;
    uint64_t var_41;
    uint64_t local_sp_0;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t r9_0;
    uint64_t *var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t *var_21;
    uint64_t *var_22;
    uint64_t var_23;
    uint64_t *var_24;
    uint64_t var_25;
    uint64_t local_sp_2;
    uint64_t var_26;
    uint64_t var_27;
    bool var_28;
    uint64_t var_29;
    uint64_t var_34;
    struct indirect_placeholder_46_ret_type var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t *var_38;
    uint64_t var_39;
    uint64_t *var_40;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_cc_src2();
    var_2 = init_rbp();
    var_3 = init_rcx();
    var_4 = init_r9();
    var_5 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    var_6 = (uint64_t *)(var_0 + (-80L));
    *var_6 = rdi;
    var_7 = var_0 + (-96L);
    *(uint64_t *)var_7 = rdx;
    var_8 = (unsigned char *)(var_0 + (-84L));
    *var_8 = (unsigned char)rsi;
    var_9 = (uint64_t *)(var_0 + (-40L));
    *var_9 = 0UL;
    var_10 = var_0 + (-64L);
    var_11 = *var_6;
    var_12 = var_0 + (-112L);
    *(uint64_t *)var_12 = 4206325UL;
    var_13 = indirect_placeholder_12(var_11, var_10);
    *var_9 = var_13;
    r8_0 = var_5;
    local_sp_1 = var_12;
    var_41 = 1UL;
    r9_0 = var_4;
    var_19 = 0UL;
    if (var_13 == 0UL) {
        *(uint64_t *)(var_0 + (-120L)) = 4206341UL;
        indirect_placeholder_1();
        var_14 = (uint64_t)*(volatile uint32_t *)(uint32_t *)0UL;
        var_15 = var_0 + (-128L);
        *(uint64_t *)var_15 = 4206365UL;
        var_16 = indirect_placeholder_45(0UL, var_3, 4294325UL, 1UL, var_14, var_4, var_5);
        local_sp_1 = var_15;
        r9_0 = var_16.field_2;
        r8_0 = var_16.field_3;
    }
    var_17 = (uint64_t *)var_10;
    var_18 = *var_17;
    local_sp_2 = local_sp_1;
    var_19 = var_18;
    if (var_18 != 0UL & (uint64_t)(*var_8 - *(unsigned char *)(*var_9 + (var_18 + (-1L)))) == 0UL) {
        *var_17 = (var_18 + 1UL);
        *(unsigned char *)(*var_9 + var_18) = *var_8;
        var_19 = *var_17;
    }
    var_20 = *var_9 + var_19;
    var_21 = (uint64_t *)(var_0 + (-48L));
    *var_21 = var_20;
    var_22 = (uint64_t *)(var_0 + (-24L));
    *var_22 = 0UL;
    var_23 = *var_9;
    var_24 = (uint64_t *)(var_0 + (-16L));
    *var_24 = var_23;
    var_25 = var_23;
    var_26 = *var_21;
    var_27 = helper_cc_compute_c_wrapper(var_25 - var_26, var_26, var_1, 17U);
    var_28 = (var_27 == 0UL);
    var_29 = *var_22 + 1UL;
    while (!var_28)
        {
            *var_22 = var_29;
            var_30 = *var_21 - *var_24;
            var_31 = (uint64_t)(uint32_t)(uint64_t)*var_8;
            var_32 = local_sp_2 + (-8L);
            *(uint64_t *)var_32 = 4206489UL;
            var_33 = indirect_placeholder_12(var_30, var_31);
            *var_24 = var_33;
            var_25 = var_33;
            local_sp_2 = var_32;
            var_26 = *var_21;
            var_27 = helper_cc_compute_c_wrapper(var_25 - var_26, var_26, var_1, 17U);
            var_28 = (var_27 == 0UL);
            var_29 = *var_22 + 1UL;
        }
    var_34 = local_sp_2 + (-8L);
    *(uint64_t *)var_34 = 4206524UL;
    var_35 = indirect_placeholder_46(var_29, 8UL, r9_0, r8_0);
    var_36 = var_35.field_0;
    var_37 = var_0 + (-56L);
    var_38 = (uint64_t *)var_37;
    *var_38 = var_36;
    **(uint64_t **)var_7 = var_36;
    var_39 = *var_9;
    *var_24 = var_39;
    **(uint64_t **)var_37 = var_39;
    var_40 = (uint64_t *)(var_0 + (-32L));
    *var_40 = 1UL;
    local_sp_0 = var_34;
    var_42 = *var_22;
    while (var_41 <= var_42)
        {
            var_43 = *var_21 - *var_24;
            var_44 = (uint64_t)(uint32_t)(uint64_t)*var_8;
            var_45 = local_sp_0 + (-8L);
            *(uint64_t *)var_45 = 4206597UL;
            var_46 = indirect_placeholder_12(var_43, var_44);
            *var_24 = var_46;
            *(uint64_t *)((*var_40 << 3UL) + *var_38) = var_46;
            var_47 = *var_40 + 1UL;
            *var_40 = var_47;
            var_41 = var_47;
            local_sp_0 = var_45;
            var_42 = *var_22;
        }
    return var_42;
}
