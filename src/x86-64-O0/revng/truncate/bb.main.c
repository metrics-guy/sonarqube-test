typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_20_ret_type;
struct indirect_placeholder_4_ret_type;
struct indirect_placeholder_7_ret_type;
struct indirect_placeholder_8_ret_type;
struct indirect_placeholder_9_ret_type;
struct indirect_placeholder_14_ret_type;
struct indirect_placeholder_13_ret_type;
struct indirect_placeholder_15_ret_type;
struct indirect_placeholder_12_ret_type;
struct indirect_placeholder_16_ret_type;
struct indirect_placeholder_11_ret_type;
struct indirect_placeholder_18_ret_type;
struct indirect_placeholder_20_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_4_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_7_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_8_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_9_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_14_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_13_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_15_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_12_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_16_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_11_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_18_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_6(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t indirect_placeholder_17(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_10(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_2(uint64_t param_0);
extern void indirect_placeholder_3(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_19(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_20_ret_type indirect_placeholder_20(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_4_ret_type indirect_placeholder_4(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_7_ret_type indirect_placeholder_7(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_8_ret_type indirect_placeholder_8(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_9_ret_type indirect_placeholder_9(uint64_t param_0);
extern struct indirect_placeholder_14_ret_type indirect_placeholder_14(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_13_ret_type indirect_placeholder_13(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_15_ret_type indirect_placeholder_15(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_12_ret_type indirect_placeholder_12(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_16_ret_type indirect_placeholder_16(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_11_ret_type indirect_placeholder_11(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_18_ret_type indirect_placeholder_18(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
void bb_main(uint64_t rsi, uint64_t rdi) {
    uint64_t local_sp_5_be;
    uint64_t local_sp_5;
    uint64_t var_89;
    uint64_t var_87;
    uint64_t var_88;
    struct indirect_placeholder_20_ret_type var_19;
    uint64_t var_105;
    unsigned char var_26;
    uint32_t var_27;
    uint64_t local_sp_9;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t local_sp_8;
    uint64_t var_115;
    uint64_t var_116;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint32_t var_20;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint32_t *var_4;
    uint64_t var_5;
    uint64_t *var_6;
    unsigned char *var_7;
    unsigned char *var_8;
    uint64_t *var_9;
    uint32_t *var_10;
    uint32_t *var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint32_t *var_14;
    uint64_t *var_15;
    uint64_t local_sp_10;
    uint64_t var_108;
    struct indirect_placeholder_4_ret_type var_109;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t var_112;
    uint64_t var_113;
    uint64_t var_114;
    uint64_t local_sp_1;
    uint64_t var_79;
    uint64_t var_97;
    uint64_t var_90;
    struct indirect_placeholder_7_ret_type var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t local_sp_2;
    uint64_t local_sp_4;
    uint32_t *var_80;
    uint64_t *var_81;
    uint32_t var_82;
    uint32_t *var_83;
    uint32_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_69;
    struct indirect_placeholder_8_ret_type var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t local_sp_3;
    uint64_t var_76;
    uint64_t var_77;
    struct indirect_placeholder_9_ret_type var_78;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint32_t var_47;
    bool var_48;
    unsigned char var_49;
    struct indirect_placeholder_14_ret_type var_55;
    uint64_t var_56;
    struct indirect_placeholder_13_ret_type var_57;
    uint64_t var_58;
    uint64_t var_59;
    struct indirect_placeholder_15_ret_type var_50;
    uint64_t var_51;
    struct indirect_placeholder_12_ret_type var_52;
    uint64_t var_53;
    uint64_t var_54;
    struct indirect_placeholder_16_ret_type var_60;
    uint64_t var_61;
    struct indirect_placeholder_11_ret_type var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t *var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t *var_98;
    uint64_t var_99;
    uint64_t var_100;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t var_104;
    uint32_t var_106;
    uint64_t var_107;
    uint64_t var_43;
    uint64_t local_sp_6;
    uint64_t local_sp_10_be;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    struct indirect_placeholder_18_ret_type var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t local_sp_7;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = var_0 + (-8L);
    *(uint64_t *)var_3 = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_4 = (uint32_t *)(var_0 + (-252L));
    *var_4 = (uint32_t)rdi;
    var_5 = var_0 + (-264L);
    var_6 = (uint64_t *)var_5;
    *var_6 = rsi;
    var_7 = (unsigned char *)(var_0 + (-25L));
    *var_7 = (unsigned char)'\x00';
    var_8 = (unsigned char *)(var_0 + (-26L));
    *var_8 = (unsigned char)'\x00';
    var_9 = (uint64_t *)(var_0 + (-48L));
    *var_9 = 18446744073709551615UL;
    var_10 = (uint32_t *)(var_0 + (-52L));
    *var_10 = 0U;
    var_11 = (uint32_t *)(var_0 + (-68L));
    *var_11 = 4294967295U;
    var_12 = **(uint64_t **)var_5;
    *(uint64_t *)(var_0 + (-272L)) = 4206164UL;
    indirect_placeholder_2(var_12);
    *(uint64_t *)(var_0 + (-280L)) = 4206179UL;
    indirect_placeholder();
    var_13 = var_0 + (-288L);
    *(uint64_t *)var_13 = 4206189UL;
    indirect_placeholder();
    var_14 = (uint32_t *)(var_0 + (-72L));
    var_15 = (uint64_t *)(var_0 + (-40L));
    local_sp_10 = var_13;
    while (1U)
        {
            var_16 = *var_6;
            var_17 = (uint64_t)*var_4;
            var_18 = local_sp_10 + (-8L);
            *(uint64_t *)var_18 = 4206902UL;
            var_19 = indirect_placeholder_20(4276896UL, 4278370UL, var_16, var_17, 0UL);
            var_20 = (uint32_t)var_19.field_0;
            *var_14 = var_20;
            local_sp_4 = var_18;
            local_sp_10_be = var_18;
            local_sp_8 = var_18;
            local_sp_9 = var_18;
            switch_state_var = 0;
            switch (var_20) {
              case 115U:
                {
                    var_21 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)4298072UL;
                    *(uint64_t *)(local_sp_9 + (-8L)) = 4206374UL;
                    var_22 = indirect_placeholder_17(var_21);
                    var_23 = (uint64_t)(unsigned char)var_22;
                    var_24 = local_sp_9 + (-16L);
                    *(uint64_t *)var_24 = 4206384UL;
                    var_25 = indirect_placeholder_17(var_23);
                    local_sp_7 = var_24;
                    local_sp_9 = var_24;
                    while ((uint64_t)(uint32_t)var_25 != 0UL)
                        {
                            *(uint64_t *)4298072UL = (*(uint64_t *)4298072UL + 1UL);
                            var_21 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)4298072UL;
                            *(uint64_t *)(local_sp_9 + (-8L)) = 4206374UL;
                            var_22 = indirect_placeholder_17(var_21);
                            var_23 = (uint64_t)(unsigned char)var_22;
                            var_24 = local_sp_9 + (-16L);
                            *(uint64_t *)var_24 = 4206384UL;
                            var_25 = indirect_placeholder_17(var_23);
                            local_sp_7 = var_24;
                            local_sp_9 = var_24;
                        }
                    var_26 = **(unsigned char **)4298072UL;
                    var_27 = (uint32_t)(uint64_t)var_26;
                    if ((uint64_t)(var_27 + (-62)) == 0UL) {
                        *var_10 = 2U;
                        *(uint64_t *)4298072UL = (*(uint64_t *)4298072UL + 1UL);
                    } else {
                        if ((signed char)var_26 <= '>') {
                            if ((uint64_t)(var_27 + (-60)) == 0UL) {
                                *var_10 = 3U;
                                *(uint64_t *)4298072UL = (*(uint64_t *)4298072UL + 1UL);
                            } else {
                                if ((signed char)var_26 <= '<') {
                                    if ((uint64_t)(var_27 + (-37)) == 0UL) {
                                        *var_10 = 5U;
                                        *(uint64_t *)4298072UL = (*(uint64_t *)4298072UL + 1UL);
                                    } else {
                                        if ((uint64_t)(var_27 + (-47)) == 0UL) {
                                            *var_10 = 4U;
                                            *(uint64_t *)4298072UL = (*(uint64_t *)4298072UL + 1UL);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    var_28 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)4298072UL;
                    *(uint64_t *)(local_sp_7 + (-8L)) = 4206588UL;
                    var_29 = indirect_placeholder_17(var_28);
                    var_30 = (uint64_t)(unsigned char)var_29;
                    var_31 = local_sp_7 + (-16L);
                    *(uint64_t *)var_31 = 4206598UL;
                    var_32 = indirect_placeholder_17(var_30);
                    local_sp_7 = var_31;
                    while ((uint64_t)(uint32_t)var_32 != 0UL)
                        {
                            *(uint64_t *)4298072UL = (*(uint64_t *)4298072UL + 1UL);
                            var_28 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)4298072UL;
                            *(uint64_t *)(local_sp_7 + (-8L)) = 4206588UL;
                            var_29 = indirect_placeholder_17(var_28);
                            var_30 = (uint64_t)(unsigned char)var_29;
                            var_31 = local_sp_7 + (-16L);
                            *(uint64_t *)var_31 = 4206598UL;
                            var_32 = indirect_placeholder_17(var_30);
                            local_sp_7 = var_31;
                        }
                    if (*var_10 != 0U) {
                        var_33 = var_19.field_1;
                        var_34 = var_19.field_2;
                        var_35 = var_19.field_3;
                        *(uint64_t *)(local_sp_7 + (-24L)) = 4206661UL;
                        indirect_placeholder_3(0UL, var_33, 4278272UL, 0UL, 0UL, var_34, var_35);
                        *(uint64_t *)(local_sp_7 + (-32L)) = 4206671UL;
                        indirect_placeholder_10(var_3, 1UL);
                        abort();
                    }
                    *var_10 = 1U;
                }
                break;
              case 4294967295U:
                {
                    var_44 = var_19.field_1;
                    var_45 = var_19.field_2;
                    var_46 = var_19.field_3;
                    *var_6 = (*var_6 + ((uint64_t)*(uint32_t *)4297464UL << 3UL));
                    var_47 = *var_4 - *(uint32_t *)4297464UL;
                    *var_4 = var_47;
                    var_48 = (*(uint64_t *)4297624UL == 0UL);
                    var_49 = *var_7;
                    if (var_48) {
                        if (var_49 != '\x01') {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        *(uint64_t *)(local_sp_10 + (-16L)) = 4206984UL;
                        var_55 = indirect_placeholder_14(4278377UL, 1UL);
                        var_56 = var_55.field_0;
                        *(uint64_t *)(local_sp_10 + (-24L)) = 4207002UL;
                        var_57 = indirect_placeholder_13(4278389UL, 0UL);
                        var_58 = var_57.field_0;
                        var_59 = var_57.field_1;
                        *(uint64_t *)(local_sp_10 + (-32L)) = 4207033UL;
                        indirect_placeholder_3(0UL, var_58, 4278400UL, 0UL, 0UL, var_59, var_56);
                        *(uint64_t *)(local_sp_10 + (-40L)) = 4207043UL;
                        indirect_placeholder_10(var_3, 1UL);
                        abort();
                    }
                    if (var_49 != '\x00') {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    if (*var_10 != 0U) {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    *(uint64_t *)(local_sp_10 + (-16L)) = 4207082UL;
                    var_50 = indirect_placeholder_15(4278377UL, 1UL);
                    var_51 = var_50.field_0;
                    *(uint64_t *)(local_sp_10 + (-24L)) = 4207100UL;
                    var_52 = indirect_placeholder_12(4278389UL, 0UL);
                    var_53 = var_52.field_0;
                    var_54 = var_52.field_1;
                    *(uint64_t *)(local_sp_10 + (-32L)) = 4207131UL;
                    indirect_placeholder_3(0UL, var_53, 4278440UL, 0UL, 0UL, var_54, var_51);
                    *(uint64_t *)(local_sp_10 + (-40L)) = 4207141UL;
                    indirect_placeholder_10(var_3, 1UL);
                    abort();
                }
                break;
              default:
                {
                    if ((int)var_20 <= (int)115U) {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    if (var_20 == 114U) {
                        *(uint64_t *)4297624UL = *(uint64_t *)4298072UL;
                    } else {
                        if ((int)var_20 <= (int)114U) {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        if (var_20 == 111U) {
                            *(unsigned char *)4297617UL = (unsigned char)'\x01';
                        } else {
                            if ((int)var_20 <= (int)111U) {
                                loop_state_var = 0U;
                                switch_state_var = 1;
                                break;
                            }
                            if (var_20 != 99U) {
                                if ((int)var_20 <= (int)99U) {
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                switch_state_var = 0;
                                switch (var_20) {
                                  case 4294967166U:
                                    {
                                        *(uint64_t *)(local_sp_10 + (-16L)) = 4206794UL;
                                        indirect_placeholder_10(var_3, 0UL);
                                        abort();
                                    }
                                    break;
                                  case 4294967165U:
                                    {
                                        var_115 = *(uint64_t *)4297312UL;
                                        *(uint64_t *)(local_sp_10 + (-16L)) = 4206846UL;
                                        indirect_placeholder_19(0UL, var_115, 4276608UL, 4278007UL, 0UL, 4278356UL);
                                        var_116 = local_sp_10 + (-24L);
                                        *(uint64_t *)var_116 = 4206856UL;
                                        indirect_placeholder();
                                        local_sp_8 = var_116;
                                        loop_state_var = 0U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    break;
                                  default:
                                    {
                                        loop_state_var = 0U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    break;
                                }
                                if (switch_state_var)
                                    break;
                            }
                            *(unsigned char *)4297616UL = (unsigned char)'\x01';
                        }
                    }
                    local_sp_10 = local_sp_10_be;
                    continue;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(uint64_t *)(local_sp_8 + (-8L)) = 4206866UL;
            indirect_placeholder_10(var_3, 1UL);
            abort();
        }
        break;
      case 1U:
        {
            if (*(unsigned char *)4297617UL != '\x00') {
                if (var_49 == '\x01') {
                    *(uint64_t *)(local_sp_10 + (-16L)) = 4207178UL;
                    var_60 = indirect_placeholder_16(4278389UL, 1UL);
                    var_61 = var_60.field_0;
                    *(uint64_t *)(local_sp_10 + (-24L)) = 4207196UL;
                    var_62 = indirect_placeholder_11(4278479UL, 0UL);
                    var_63 = var_62.field_0;
                    var_64 = var_62.field_1;
                    *(uint64_t *)(local_sp_10 + (-32L)) = 4207227UL;
                    indirect_placeholder_3(0UL, var_63, 4278496UL, 0UL, 0UL, var_64, var_61);
                    *(uint64_t *)(local_sp_10 + (-40L)) = 4207237UL;
                    indirect_placeholder_10(var_3, 1UL);
                    abort();
                }
            }
            if ((int)var_47 > (int)0U) {
                *(uint64_t *)(local_sp_10 + (-16L)) = 4207271UL;
                indirect_placeholder_3(0UL, var_44, 4278528UL, 0UL, 0UL, var_45, var_46);
                *(uint64_t *)(local_sp_10 + (-24L)) = 4207281UL;
                indirect_placeholder_10(var_3, 1UL);
                abort();
            }
            if (!var_48) {
                var_65 = (uint64_t *)(var_0 + (-64L));
                *var_65 = 18446744073709551615UL;
                var_66 = *(uint64_t *)4297624UL;
                var_67 = local_sp_10 + (-16L);
                *(uint64_t *)var_67 = 4207330UL;
                var_68 = indirect_placeholder_17(var_66);
                local_sp_3 = var_67;
                if ((uint64_t)(uint32_t)var_68 == 0UL) {
                    var_69 = *(uint64_t *)4297624UL;
                    *(uint64_t *)(local_sp_10 + (-24L)) = 4207354UL;
                    var_70 = indirect_placeholder_8(var_69, 4UL);
                    var_71 = var_70.field_0;
                    var_72 = var_70.field_1;
                    var_73 = var_70.field_2;
                    *(uint64_t *)(local_sp_10 + (-32L)) = 4207362UL;
                    indirect_placeholder();
                    var_74 = (uint64_t)*(uint32_t *)var_71;
                    var_75 = local_sp_10 + (-40L);
                    *(uint64_t *)var_75 = 4207389UL;
                    indirect_placeholder_3(0UL, var_71, 4278549UL, var_74, 1UL, var_72, var_73);
                    local_sp_3 = var_75;
                }
                var_76 = var_0 + (-248L);
                var_77 = local_sp_3 + (-8L);
                *(uint64_t *)var_77 = 4207404UL;
                var_78 = indirect_placeholder_9(var_76);
                local_sp_1 = var_77;
                if ((uint64_t)(unsigned char)var_78.field_0 == 0UL) {
                    var_79 = *(uint64_t *)(var_0 + (-200L));
                    *var_65 = var_79;
                    var_89 = var_79;
                } else {
                    *(uint64_t *)(local_sp_3 + (-16L)) = 4207446UL;
                    indirect_placeholder();
                    var_80 = (uint32_t *)(var_0 + (-76L));
                    *var_80 = 0U;
                    *(uint64_t *)(local_sp_3 + (-24L)) = 4207475UL;
                    indirect_placeholder();
                    var_81 = (uint64_t *)(var_0 + (-88L));
                    *var_81 = 0UL;
                    *(uint64_t *)(local_sp_3 + (-32L)) = 4207484UL;
                    indirect_placeholder();
                    var_82 = *(volatile uint32_t *)(uint32_t *)0UL;
                    var_83 = (uint32_t *)(var_0 + (-92L));
                    *var_83 = var_82;
                    var_84 = *var_80;
                    var_85 = local_sp_3 + (-40L);
                    *(uint64_t *)var_85 = 4207499UL;
                    indirect_placeholder();
                    var_86 = *var_81;
                    var_89 = var_86;
                    local_sp_1 = var_85;
                    if ((long)var_86 < (long)0UL) {
                        var_87 = (uint64_t)var_84;
                        var_88 = local_sp_3 + (-48L);
                        *(uint64_t *)var_88 = 4207521UL;
                        indirect_placeholder();
                        *(uint32_t *)var_87 = *var_83;
                        var_89 = *var_65;
                        local_sp_1 = var_88;
                    } else {
                        *var_65 = var_86;
                    }
                }
                var_97 = var_89;
                local_sp_2 = local_sp_1;
                if ((long)var_89 > (long)18446744073709551615UL) {
                    var_90 = *(uint64_t *)4297624UL;
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4207553UL;
                    var_91 = indirect_placeholder_7(var_90, 4UL);
                    var_92 = var_91.field_0;
                    var_93 = var_91.field_1;
                    var_94 = var_91.field_2;
                    *(uint64_t *)(local_sp_1 + (-16L)) = 4207561UL;
                    indirect_placeholder();
                    var_95 = (uint64_t)*(uint32_t *)var_92;
                    var_96 = local_sp_1 + (-24L);
                    *(uint64_t *)var_96 = 4207588UL;
                    indirect_placeholder_3(0UL, var_92, 4278122UL, var_95, 1UL, var_93, var_94);
                    var_97 = *var_65;
                    local_sp_2 = var_96;
                }
                local_sp_4 = local_sp_2;
                if (*var_7 == '\x01') {
                    *var_9 = var_97;
                } else {
                    *var_15 = var_97;
                }
            }
            *(uint32_t *)(var_0 + (-96L)) = ((*(unsigned char *)4297616UL == '\x00') ? 2113U : 2049U);
            var_98 = (uint64_t *)(var_0 + (-104L));
            local_sp_5 = local_sp_4;
            var_99 = *var_6;
            *var_6 = (var_99 + 8UL);
            var_100 = *(uint64_t *)var_99;
            *var_98 = var_100;
            while (var_100 != 0UL)
                {
                    *(uint64_t *)(local_sp_5 + (-8L)) = 4207675UL;
                    indirect_placeholder();
                    *var_11 = 0U;
                    var_101 = (uint64_t)*var_10;
                    var_102 = *var_9;
                    var_103 = *var_15;
                    var_104 = *var_98;
                    *(uint64_t *)(local_sp_5 + (-16L)) = 4207806UL;
                    var_105 = indirect_placeholder_6(var_102, var_103, var_104, 0UL, var_101);
                    *var_8 = ((((uint64_t)(unsigned char)var_105 ^ 1UL) | (uint64_t)*var_8) != 0UL);
                    var_106 = *var_11;
                    var_107 = local_sp_5 + (-24L);
                    *(uint64_t *)var_107 = 4207836UL;
                    indirect_placeholder();
                    local_sp_5_be = var_107;
                    if (var_106 == 0U) {
                        var_108 = *var_98;
                        *(uint64_t *)(local_sp_5 + (-32L)) = 4207857UL;
                        var_109 = indirect_placeholder_4(var_108, 4UL);
                        var_110 = var_109.field_0;
                        var_111 = var_109.field_1;
                        var_112 = var_109.field_2;
                        *(uint64_t *)(local_sp_5 + (-40L)) = 4207865UL;
                        indirect_placeholder();
                        var_113 = (uint64_t)*(uint32_t *)var_110;
                        var_114 = local_sp_5 + (-48L);
                        *(uint64_t *)var_114 = 4207892UL;
                        indirect_placeholder_3(0UL, var_110, 4278591UL, var_113, 0UL, var_111, var_112);
                        *var_8 = (unsigned char)'\x01';
                        local_sp_5_be = var_114;
                    }
                    local_sp_5 = local_sp_5_be;
                    var_99 = *var_6;
                    *var_6 = (var_99 + 8UL);
                    var_100 = *(uint64_t *)var_99;
                    *var_98 = var_100;
                }
            return;
        }
        break;
    }
}
