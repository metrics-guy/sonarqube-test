typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_avoid_trashing_input_ret_type;
struct indirect_placeholder_101_ret_type;
struct indirect_placeholder_100_ret_type;
struct indirect_placeholder_102_ret_type;
struct bb_avoid_trashing_input_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_101_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_100_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_102_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern struct indirect_placeholder_101_ret_type indirect_placeholder_101(uint64_t param_0);
extern struct indirect_placeholder_100_ret_type indirect_placeholder_100(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_102_ret_type indirect_placeholder_102(uint64_t param_0);
struct bb_avoid_trashing_input_ret_type bb_avoid_trashing_input(uint64_t rcx, uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    uint64_t *var_9;
    unsigned char *var_10;
    uint64_t *var_11;
    uint64_t var_12;
    uint64_t *var_13;
    unsigned char *var_14;
    unsigned char *var_15;
    uint64_t *var_16;
    uint64_t *var_17;
    uint64_t *var_18;
    uint64_t *var_19;
    uint64_t var_20;
    uint64_t *var_21;
    uint64_t var_22;
    uint64_t local_sp_2;
    uint64_t local_sp_1;
    unsigned char storemerge;
    unsigned char var_33;
    unsigned char var_34;
    uint64_t local_sp_3;
    uint64_t r9_1;
    uint64_t var_35;
    uint64_t var_44;
    struct indirect_placeholder_101_ret_type var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    struct indirect_placeholder_100_ret_type var_43;
    uint64_t r8_2;
    uint64_t r9_0;
    uint64_t local_sp_8;
    uint64_t r8_0;
    uint64_t local_sp_4;
    uint64_t r8_1;
    uint64_t local_sp_5;
    uint64_t var_45;
    uint64_t var_28;
    uint64_t local_sp_6;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    struct indirect_placeholder_102_ret_type var_32;
    uint64_t r9_2;
    uint64_t var_23;
    uint64_t var_24;
    struct bb_avoid_trashing_input_ret_type mrv;
    struct bb_avoid_trashing_input_ret_type mrv1;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r9();
    var_2 = init_r8();
    var_3 = init_rbp();
    var_4 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    var_5 = var_0 + (-376L);
    var_6 = (uint64_t *)(var_0 + (-352L));
    *var_6 = rdi;
    var_7 = (uint64_t *)(var_0 + (-360L));
    *var_7 = rsi;
    var_8 = (uint64_t *)(var_0 + (-368L));
    *var_8 = rdx;
    var_9 = (uint64_t *)var_5;
    *var_9 = rcx;
    var_10 = (unsigned char *)(var_0 + (-9L));
    *var_10 = (unsigned char)'\x00';
    var_11 = (uint64_t *)(var_0 + (-24L));
    *var_11 = 0UL;
    var_12 = *var_7;
    var_13 = (uint64_t *)(var_0 + (-32L));
    *var_13 = var_12;
    var_14 = (unsigned char *)(var_0 + (-34L));
    var_15 = (unsigned char *)(var_0 + (-33L));
    var_16 = (uint64_t *)(var_0 + (-336L));
    var_17 = (uint64_t *)(var_0 + (-176L));
    var_18 = (uint64_t *)(var_0 + (-344L));
    var_19 = (uint64_t *)(var_0 + (-184L));
    var_20 = var_0 + (-192L);
    var_21 = (uint64_t *)var_20;
    var_22 = var_12;
    storemerge = (unsigned char)'\x00';
    var_34 = (unsigned char)'\x01';
    r8_2 = var_2;
    local_sp_8 = var_5;
    r9_2 = var_1;
    var_23 = *var_8;
    var_24 = helper_cc_compute_c_wrapper(var_22 - var_23, var_23, var_4, 17U);
    r9_0 = r9_2;
    r8_0 = r8_2;
    r9_1 = r9_2;
    r8_1 = r8_2;
    while (var_24 != 0UL)
        {
            var_25 = *(uint64_t *)(*var_6 + (*var_13 << 4UL));
            var_26 = local_sp_8 + (-8L);
            *(uint64_t *)var_26 = 4238336UL;
            indirect_placeholder();
            *var_14 = ((uint64_t)(uint32_t)var_25 == 0UL);
            var_27 = *var_9;
            local_sp_6 = var_26;
            if (var_27 == 0UL) {
                var_28 = local_sp_8 + (-16L);
                *(uint64_t *)var_28 = 4238396UL;
                indirect_placeholder();
                local_sp_3 = var_28;
                local_sp_6 = var_28;
                if ((uint64_t)(uint32_t)var_27 == 0UL) {
                    if (*var_14 == '\x01') {
                        *var_15 = (unsigned char)'\x01';
                    } else {
                        if (*var_10 != '\x01') {
                            *(uint64_t *)(local_sp_6 + (-8L)) = 4238451UL;
                            indirect_placeholder();
                            break;
                        }
                        if (*var_14 == '\x00') {
                            var_30 = *(uint64_t *)(*var_6 + (*var_13 << 4UL));
                            var_31 = local_sp_6 + (-8L);
                            *(uint64_t *)var_31 = 4238539UL;
                            var_32 = indirect_placeholder_102(var_30);
                            local_sp_1 = var_31;
                            local_sp_2 = var_31;
                            storemerge = (unsigned char)'\x01';
                            if ((uint64_t)(uint32_t)var_32.field_0 == 0UL) {
                                local_sp_2 = local_sp_1;
                            } else {
                                if (*var_16 == *var_17) {
                                    local_sp_2 = local_sp_1;
                                } else {
                                    if (*var_18 == *var_19) {
                                        local_sp_2 = local_sp_1;
                                    }
                                }
                            }
                        } else {
                            var_29 = local_sp_6 + (-8L);
                            *(uint64_t *)var_29 = 4238491UL;
                            indirect_placeholder();
                            local_sp_1 = var_29;
                            local_sp_2 = local_sp_1;
                        }
                        var_33 = storemerge & '\x01';
                        *var_15 = var_33;
                        var_34 = var_33;
                        local_sp_3 = local_sp_2;
                    }
                } else {
                    if (*var_10 == '\x01') {
                        *(uint64_t *)(local_sp_6 + (-8L)) = 4238451UL;
                        indirect_placeholder();
                        break;
                    }
                    if (*var_14 == '\x00') {
                        var_29 = local_sp_6 + (-8L);
                        *(uint64_t *)var_29 = 4238491UL;
                        indirect_placeholder();
                        local_sp_1 = var_29;
                        local_sp_2 = local_sp_1;
                    } else {
                        var_30 = *(uint64_t *)(*var_6 + (*var_13 << 4UL));
                        var_31 = local_sp_6 + (-8L);
                        *(uint64_t *)var_31 = 4238539UL;
                        var_32 = indirect_placeholder_102(var_30);
                        local_sp_1 = var_31;
                        local_sp_2 = var_31;
                        storemerge = (unsigned char)'\x01';
                        if ((uint64_t)(uint32_t)var_32.field_0 == 0UL) {
                            local_sp_2 = local_sp_1;
                        } else {
                            if (*var_16 == *var_17) {
                                local_sp_2 = local_sp_1;
                            } else {
                                if (*var_18 == *var_19) {
                                    local_sp_2 = local_sp_1;
                                }
                            }
                        }
                    }
                    var_33 = storemerge & '\x01';
                    *var_15 = var_33;
                    var_34 = var_33;
                    local_sp_3 = local_sp_2;
                }
            } else {
                if (*var_10 == '\x01') {
                    *(uint64_t *)(local_sp_6 + (-8L)) = 4238451UL;
                    indirect_placeholder();
                    break;
                }
                if (*var_14 == '\x00') {
                    var_29 = local_sp_6 + (-8L);
                    *(uint64_t *)var_29 = 4238491UL;
                    indirect_placeholder();
                    local_sp_1 = var_29;
                    local_sp_2 = local_sp_1;
                } else {
                    var_30 = *(uint64_t *)(*var_6 + (*var_13 << 4UL));
                    var_31 = local_sp_6 + (-8L);
                    *(uint64_t *)var_31 = 4238539UL;
                    var_32 = indirect_placeholder_102(var_30);
                    local_sp_1 = var_31;
                    local_sp_2 = var_31;
                    storemerge = (unsigned char)'\x01';
                    if ((uint64_t)(uint32_t)var_32.field_0 == 0UL) {
                        local_sp_2 = local_sp_1;
                    } else {
                        if (*var_16 == *var_17) {
                            local_sp_2 = local_sp_1;
                        } else {
                            if (*var_18 == *var_19) {
                                local_sp_2 = local_sp_1;
                            }
                        }
                    }
                }
                var_33 = storemerge & '\x01';
                *var_15 = var_33;
                var_34 = var_33;
                local_sp_3 = local_sp_2;
            }
            local_sp_4 = local_sp_3;
            local_sp_5 = local_sp_3;
            if (var_34 != '\x00') {
                var_35 = *var_11;
                var_44 = var_35;
                if (var_35 == 0UL) {
                    *(uint64_t *)(local_sp_3 + (-8L)) = 4238632UL;
                    var_36 = indirect_placeholder_101(var_20);
                    var_37 = var_36.field_0;
                    var_38 = var_36.field_1;
                    *var_11 = var_38;
                    var_39 = var_38 + 13UL;
                    var_40 = *var_21;
                    var_41 = *var_6 + (*var_13 << 4UL);
                    var_42 = local_sp_3 + (-16L);
                    *(uint64_t *)var_42 = 4238694UL;
                    var_43 = indirect_placeholder_100(var_37, var_39, var_40, 1UL, var_41, 0UL);
                    var_44 = *var_11;
                    r9_0 = var_43.field_1;
                    r8_0 = var_43.field_2;
                    local_sp_4 = var_42;
                }
                *(uint64_t *)(*var_6 + (*var_13 << 4UL)) = (var_44 + 13UL);
                *(uint64_t *)(((*var_13 << 4UL) + *var_6) + 8UL) = *var_11;
                r9_1 = r9_0;
                r8_1 = r8_0;
                local_sp_5 = local_sp_4;
            }
            var_45 = *var_13 + 1UL;
            *var_13 = var_45;
            var_22 = var_45;
            r9_2 = r9_1;
            r8_2 = r8_1;
            local_sp_8 = local_sp_5;
            var_23 = *var_8;
            var_24 = helper_cc_compute_c_wrapper(var_22 - var_23, var_23, var_4, 17U);
            r9_0 = r9_2;
            r8_0 = r8_2;
            r9_1 = r9_2;
            r8_1 = r8_2;
        }
    mrv.field_0 = r9_2;
    mrv1 = mrv;
    mrv1.field_1 = r8_2;
    return mrv1;
}
