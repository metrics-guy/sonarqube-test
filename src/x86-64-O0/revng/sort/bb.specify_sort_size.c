typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_specify_sort_size_ret_type;
struct helper_addsd_wrapper_ret_type;
struct type_6;
struct type_8;
struct helper_comisd_wrapper_225_ret_type;
struct helper_mulsd_wrapper_224_ret_type;
struct helper_subsd_wrapper_ret_type;
struct helper_cvtsq2sd_wrapper_ret_type;
struct helper_divsd_wrapper_ret_type;
struct helper_cvttsd2sq_wrapper_ret_type;
struct bb_specify_sort_size_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct helper_addsd_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct type_6 {
};
struct type_8 {
};
struct helper_comisd_wrapper_225_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_mulsd_wrapper_224_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_subsd_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvtsq2sd_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_divsd_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvttsd2sq_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_state_0x8558(void);
extern unsigned char init_state_0x8549(void);
extern unsigned char init_state_0x854c(void);
extern unsigned char init_state_0x8548(void);
extern unsigned char init_state_0x854b(void);
extern unsigned char init_state_0x8547(void);
extern unsigned char init_state_0x854d(void);
extern void indirect_placeholder(void);
extern struct helper_addsd_wrapper_ret_type helper_addsd_wrapper(struct type_6 *param_0, struct type_8 *param_1, struct type_8 *param_2, uint64_t param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9);
extern struct helper_comisd_wrapper_225_ret_type helper_comisd_wrapper_225(struct type_6 *param_0, struct type_8 *param_1, struct type_8 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_mulsd_wrapper_224_ret_type helper_mulsd_wrapper_224(struct type_6 *param_0, struct type_8 *param_1, struct type_8 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_subsd_wrapper_ret_type helper_subsd_wrapper(struct type_6 *param_0, struct type_8 *param_1, struct type_8 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern uint64_t indirect_placeholder_26(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_r10(void);
extern struct helper_cvtsq2sd_wrapper_ret_type helper_cvtsq2sd_wrapper(struct type_6 *param_0, struct type_8 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6);
extern uint64_t init_r9(void);
extern void indirect_placeholder_19(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct helper_divsd_wrapper_ret_type helper_divsd_wrapper(struct type_6 *param_0, struct type_8 *param_1, struct type_8 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_cvttsd2sq_wrapper_ret_type helper_cvttsd2sq_wrapper(struct type_6 *param_0, struct type_8 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4);
struct bb_specify_sort_size_ret_type bb_specify_sort_size(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_20;
    struct helper_cvttsd2sq_wrapper_ret_type var_47;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    unsigned char var_6;
    unsigned char var_7;
    unsigned char var_8;
    unsigned char var_9;
    unsigned char var_10;
    unsigned char var_11;
    uint64_t var_12;
    uint32_t *var_13;
    struct helper_cvtsq2sd_wrapper_ret_type var_33;
    uint64_t *var_14;
    unsigned char *var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint32_t *var_21;
    uint32_t var_22;
    uint32_t var_25;
    struct helper_cvtsq2sd_wrapper_ret_type var_31;
    uint64_t state_0x8558_0;
    uint64_t var_32;
    struct helper_addsd_wrapper_ret_type var_34;
    uint64_t cc_dst_0;
    unsigned char storemerge;
    struct helper_mulsd_wrapper_224_ret_type var_35;
    struct helper_divsd_wrapper_ret_type var_36;
    uint64_t var_37;
    unsigned char var_38;
    uint64_t *var_39;
    struct helper_comisd_wrapper_225_ret_type var_40;
    struct helper_comisd_wrapper_225_ret_type var_41;
    uint64_t var_42;
    unsigned char var_43;
    uint64_t var_44;
    bool var_45;
    uint64_t var_46;
    uint64_t rax_0;
    struct helper_subsd_wrapper_ret_type var_48;
    struct helper_cvttsd2sq_wrapper_ret_type var_49;
    uint32_t var_50;
    uint64_t *var_23;
    uint64_t var_24;
    uint64_t var_26;
    uint32_t var_27;
    uint64_t local_sp_0;
    uint64_t var_28;
    uint64_t *var_29;
    uint64_t var_30;
    uint32_t var_56;
    uint64_t *var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_61;
    uint64_t var_62;
    struct bb_specify_sort_size_ret_type mrv;
    struct bb_specify_sort_size_ret_type mrv1;
    struct bb_specify_sort_size_ret_type mrv2;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r10();
    var_2 = init_r9();
    var_3 = init_rbp();
    var_4 = init_cc_src2();
    var_5 = init_state_0x8558();
    var_6 = init_state_0x8549();
    var_7 = init_state_0x854c();
    var_8 = init_state_0x8548();
    var_9 = init_state_0x854b();
    var_10 = init_state_0x8547();
    var_11 = init_state_0x854d();
    var_12 = var_0 + (-8L);
    *(uint64_t *)var_12 = var_3;
    var_13 = (uint32_t *)(var_0 + (-44L));
    *var_13 = (uint32_t)rdi;
    var_14 = (uint64_t *)(var_0 + (-56L));
    *var_14 = rdx;
    var_15 = (unsigned char *)(var_0 + (-48L));
    *var_15 = (unsigned char)rsi;
    var_16 = var_0 + (-32L);
    var_17 = var_0 + (-40L);
    var_18 = *var_14;
    var_19 = var_0 + (-64L);
    *(uint64_t *)var_19 = 4215248UL;
    var_20 = indirect_placeholder_26(4352424UL, var_16, 10UL, var_18, var_17);
    var_21 = (uint32_t *)(var_0 + (-12L));
    var_22 = (uint32_t)var_20;
    *var_21 = var_22;
    var_25 = var_22;
    local_sp_0 = var_19;
    var_25 = 0U;
    if (var_22 != 0U & (uint64_t)(((uint32_t)(uint64_t)*(unsigned char *)(*(uint64_t *)var_17 + (-1L)) + (-48)) & (-2)) <= 9UL) {
        var_23 = (uint64_t *)var_16;
        var_24 = *var_23;
        var_25 = 1U;
        if (var_24 > 18014398509481983UL) {
            *var_21 = 1U;
        } else {
            *var_23 = (var_24 << 10UL);
            var_25 = *var_21;
        }
    }
    var_50 = var_25;
    var_26 = *(uint64_t *)var_17;
    var_50 = 2U;
    if (var_25 != 2U & (uint64_t)(((uint32_t)(uint64_t)*(unsigned char *)(var_26 + (-1L)) + (-48)) & (-2)) <= 9UL & *(unsigned char *)(var_26 + 1UL) != '\x00') {
        var_27 = (uint32_t)(uint64_t)**(unsigned char **)var_17;
        var_50 = 0U;
        if ((uint64_t)(var_27 + (-37)) == 0UL) {
            var_28 = var_0 + (-72L);
            *(uint64_t *)var_28 = 4215415UL;
            indirect_placeholder();
            var_29 = (uint64_t *)var_16;
            var_30 = *var_29;
            cc_dst_0 = var_30;
            local_sp_0 = var_28;
            if ((long)var_30 < (long)0UL) {
                var_32 = (var_30 >> 1UL) | (var_30 & 1UL);
                var_33 = helper_cvtsq2sd_wrapper((struct type_6 *)(0UL), (struct type_8 *)(776UL), var_32, var_6, var_8, var_9, var_10);
                var_34 = helper_addsd_wrapper((struct type_6 *)(0UL), (struct type_8 *)(776UL), (struct type_8 *)(776UL), var_33.field_0, var_33.field_1, var_7, var_8, var_9, var_10, var_11);
                state_0x8558_0 = var_34.field_0;
                cc_dst_0 = var_32;
                storemerge = var_34.field_1;
            } else {
                var_31 = helper_cvtsq2sd_wrapper((struct type_6 *)(0UL), (struct type_8 *)(776UL), var_30, var_6, var_8, var_9, var_10);
                state_0x8558_0 = var_31.field_0;
                storemerge = var_31.field_1;
            }
            var_35 = helper_mulsd_wrapper_224((struct type_6 *)(0UL), (struct type_8 *)(776UL), (struct type_8 *)(840UL), state_0x8558_0, var_5, storemerge, var_7, var_8, var_9, var_10, var_11);
            var_36 = helper_divsd_wrapper((struct type_6 *)(0UL), (struct type_8 *)(776UL), (struct type_8 *)(840UL), var_35.field_0, *(uint64_t *)4355712UL, var_35.field_1, var_7, var_8, var_9, var_10, var_11);
            var_37 = var_36.field_0;
            var_38 = var_36.field_1;
            var_39 = (uint64_t *)(var_0 + (-24L));
            *var_39 = var_37;
            var_40 = helper_comisd_wrapper_225((struct type_6 *)(0UL), (struct type_8 *)(776UL), (struct type_8 *)(2824UL), *(uint64_t *)4355720UL, var_37, var_38, var_7);
            if ((var_40.field_0 & 65UL) == 0UL) {
                *var_21 = 1U;
                var_50 = 1U;
            } else {
                var_41 = helper_comisd_wrapper_225((struct type_6 *)(0UL), (struct type_8 *)(776UL), (struct type_8 *)(2824UL), *var_39, *(uint64_t *)4355728UL, var_40.field_1, var_7);
                var_42 = var_41.field_0;
                var_43 = var_41.field_1;
                var_44 = helper_cc_compute_c_wrapper(cc_dst_0, var_42, var_4, 1U);
                var_45 = (var_44 == 0UL);
                var_46 = *var_39;
                if (var_45) {
                    var_48 = helper_subsd_wrapper((struct type_6 *)(0UL), (struct type_8 *)(776UL), (struct type_8 *)(840UL), var_46, *(uint64_t *)4355728UL, var_43, var_7, var_8, var_9, var_10, var_11);
                    var_49 = helper_cvttsd2sq_wrapper((struct type_6 *)(0UL), (struct type_8 *)(776UL), var_48.field_0, var_48.field_1, var_7);
                    rax_0 = var_49.field_0 ^ (-9223372036854775808L);
                } else {
                    var_47 = helper_cvttsd2sq_wrapper((struct type_6 *)(0UL), (struct type_8 *)(776UL), var_46, var_43, var_7);
                    rax_0 = var_47.field_0;
                }
                *var_29 = rax_0;
                *var_21 = 0U;
            }
        } else {
            if ((uint64_t)(var_27 + (-98)) == 0UL) {
                *var_21 = 0U;
                var_50 = 0U;
            }
        }
    }
    var_56 = var_50;
    if (var_50 == 0U) {
        var_57 = (uint64_t)(uint32_t)(uint64_t)*var_15;
        var_58 = *var_14;
        var_59 = (uint64_t)*var_13;
        var_60 = (uint64_t)var_56;
        *(uint64_t *)(local_sp_0 + (-8L)) = 4215709UL;
        indirect_placeholder_19(var_58, var_12, 4350720UL, var_57, var_60, var_59);
        abort();
    }
    var_51 = (uint64_t *)var_16;
    var_52 = *var_51;
    var_53 = *(uint64_t *)4389600UL;
    var_54 = helper_cc_compute_c_wrapper(var_52 - var_53, var_53, var_4, 17U);
    var_56 = 1U;
    if (var_54 == 0UL) {
        mrv.field_0 = var_1;
        mrv1 = mrv;
        mrv1.field_1 = var_2;
        mrv2 = mrv1;
        mrv2.field_2 = 4352424UL;
        return mrv2;
    }
    var_55 = *var_51;
    *(uint64_t *)4389600UL = var_55;
    if (var_55 != *var_51) {
        var_61 = (uint64_t)*(uint32_t *)4388172UL * 34UL;
        var_62 = helper_cc_compute_c_wrapper(var_61 - var_55, var_55, var_4, 17U);
        *(uint64_t *)4389600UL = ((var_62 == 0UL) ? var_61 : var_55);
        mrv.field_0 = var_1;
        mrv1 = mrv;
        mrv1.field_1 = var_2;
        mrv2 = mrv1;
        mrv2.field_2 = 4352424UL;
        return mrv2;
    }
    *var_21 = 1U;
    var_57 = (uint64_t)(uint32_t)(uint64_t)*var_15;
    var_58 = *var_14;
    var_59 = (uint64_t)*var_13;
    var_60 = (uint64_t)var_56;
    *(uint64_t *)(local_sp_0 + (-8L)) = 4215709UL;
    indirect_placeholder_19(var_58, var_12, 4350720UL, var_57, var_60, var_59);
    abort();
}
