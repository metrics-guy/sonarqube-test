typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_86_ret_type;
struct indirect_placeholder_87_ret_type;
struct indirect_placeholder_86_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_87_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_8(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
extern struct indirect_placeholder_86_ret_type indirect_placeholder_86(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_87_ret_type indirect_placeholder_87(uint64_t param_0);
uint64_t bb_keycompare(uint64_t rdi, uint64_t rsi) {
    uint64_t var_230;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t *var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t *var_16;
    uint64_t var_17;
    uint64_t *var_18;
    uint64_t var_19;
    uint64_t *var_20;
    uint64_t *var_21;
    uint64_t *var_22;
    uint64_t *var_23;
    uint64_t *var_24;
    uint64_t *var_25;
    uint64_t var_26;
    uint64_t *var_27;
    uint64_t *var_28;
    uint64_t *var_29;
    uint64_t *var_30;
    uint64_t *var_31;
    uint64_t *var_32;
    unsigned char *var_33;
    unsigned char *var_34;
    uint32_t *var_35;
    uint64_t local_sp_8;
    uint64_t var_237;
    uint64_t local_sp_8_be;
    uint64_t var_243;
    uint64_t var_236;
    uint64_t local_sp_0;
    uint64_t local_sp_3_be;
    uint64_t local_sp_3;
    uint64_t var_166;
    uint64_t var_160;
    uint64_t local_sp_0_be;
    uint64_t var_129;
    uint64_t var_123;
    uint64_t var_45;
    uint64_t local_sp_9;
    uint64_t var_217;
    uint64_t var_117;
    uint64_t var_118;
    uint64_t var_119;
    uint64_t local_sp_1_ph;
    uint64_t var_120;
    uint64_t var_121;
    uint64_t var_122;
    uint64_t local_sp_1;
    uint64_t var_124;
    uint64_t var_125;
    uint64_t var_167;
    uint64_t local_sp_2;
    uint64_t var_126;
    uint64_t var_127;
    uint64_t var_128;
    uint64_t var_130;
    uint64_t var_131;
    uint64_t var_132;
    uint64_t var_133;
    uint64_t var_134;
    uint64_t var_135;
    uint64_t var_136;
    uint64_t var_137;
    uint64_t var_50;
    uint64_t local_sp_10;
    uint64_t var_138;
    uint64_t var_139;
    uint64_t var_140;
    uint64_t var_141;
    uint64_t var_142;
    uint64_t var_143;
    uint64_t var_144;
    uint64_t var_145;
    uint32_t var_146;
    uint32_t var_215;
    uint64_t var_147;
    uint64_t var_148;
    uint64_t var_149;
    uint64_t var_150;
    uint64_t var_151;
    uint64_t var_152;
    uint64_t var_153;
    uint64_t local_sp_15;
    uint64_t var_154;
    uint64_t var_155;
    uint64_t var_156;
    uint64_t local_sp_4_ph;
    uint64_t var_157;
    uint64_t var_158;
    uint64_t var_159;
    uint64_t local_sp_4;
    uint64_t var_161;
    uint64_t var_162;
    uint64_t local_sp_5;
    uint64_t var_163;
    uint64_t var_164;
    uint64_t var_165;
    uint64_t var_96;
    uint64_t var_168;
    uint64_t var_169;
    uint64_t var_170;
    uint64_t var_171;
    uint64_t var_172;
    uint64_t var_173;
    uint64_t var_174;
    uint64_t var_175;
    uint64_t var_176;
    uint64_t var_177;
    uint64_t var_178;
    uint32_t var_179;
    uint64_t var_180;
    uint64_t var_181;
    uint64_t var_182;
    uint64_t var_183;
    uint64_t var_184;
    uint64_t var_185;
    uint64_t var_186;
    bool var_187;
    uint64_t var_188;
    uint64_t local_sp_6;
    uint64_t var_189;
    uint64_t var_190;
    uint64_t var_191;
    uint64_t local_sp_7;
    uint64_t var_192;
    uint64_t var_193;
    uint64_t var_194;
    uint64_t var_195;
    uint64_t var_196;
    uint64_t var_197;
    uint64_t var_198;
    uint64_t var_199;
    uint64_t var_200;
    uint64_t var_201;
    uint64_t var_202;
    uint64_t var_203;
    uint64_t var_204;
    uint64_t var_205;
    uint64_t var_206;
    uint32_t var_207;
    uint64_t var_211;
    uint64_t var_212;
    uint64_t var_213;
    uint32_t storemerge42;
    uint64_t var_208;
    uint64_t var_209;
    uint32_t var_210;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t local_sp_13;
    bool var_57;
    uint64_t var_58;
    uint64_t local_sp_23;
    uint64_t var_46;
    uint64_t local_sp_24;
    uint64_t var_51;
    uint64_t local_sp_11;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t local_sp_21;
    uint64_t var_64;
    uint64_t local_sp_22;
    uint64_t local_sp_12;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t var_82;
    struct indirect_placeholder_86_ret_type var_83;
    uint64_t local_sp_14;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t var_97;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t var_100;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t var_107;
    bool var_108;
    uint64_t var_109;
    uint32_t var_214;
    uint64_t var_216;
    uint32_t rax_0_shrunk;
    uint64_t var_218;
    uint64_t var_219;
    uint64_t var_220;
    uint64_t var_221;
    uint64_t var_222;
    uint64_t local_sp_16;
    uint64_t var_223;
    uint64_t var_224;
    uint64_t var_225;
    uint64_t var_226;
    uint64_t var_227;
    uint64_t var_228;
    uint64_t var_229;
    uint64_t local_sp_17;
    uint64_t var_231;
    uint64_t var_232;
    uint64_t local_sp_18_ph;
    uint64_t var_233;
    uint64_t var_234;
    uint64_t var_235;
    uint64_t local_sp_18;
    uint64_t var_238;
    uint64_t var_239;
    uint64_t var_240;
    uint64_t var_241;
    uint64_t var_242;
    uint64_t var_116;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t var_112;
    uint64_t var_113;
    uint64_t var_114;
    uint64_t var_115;
    uint64_t local_sp_19;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    bool var_70;
    uint64_t var_71;
    uint64_t local_sp_20;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t storemerge41_in_in_in;
    unsigned char storemerge41_in402;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t storemerge_in_in_in;
    unsigned char storemerge_in401;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_47;
    struct indirect_placeholder_87_ret_type var_48;
    uint64_t var_49;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    var_4 = var_0 + (-4184L);
    var_5 = var_0 + (-4176L);
    var_6 = (uint64_t *)var_5;
    *var_6 = rdi;
    var_7 = (uint64_t *)var_4;
    *var_7 = rsi;
    var_8 = *(uint64_t *)4389640UL;
    var_9 = var_0 + (-32L);
    var_10 = (uint64_t *)var_9;
    *var_10 = var_8;
    var_11 = *(uint64_t *)(*var_6 + 16UL);
    var_12 = var_0 + (-40L);
    var_13 = (uint64_t *)var_12;
    *var_13 = var_11;
    var_14 = *(uint64_t *)(*var_7 + 16UL);
    var_15 = var_0 + (-48L);
    var_16 = (uint64_t *)var_15;
    *var_16 = var_14;
    var_17 = *(uint64_t *)(*var_6 + 24UL);
    var_18 = (uint64_t *)(var_0 + (-56L));
    *var_18 = var_17;
    var_19 = *(uint64_t *)(*var_7 + 24UL);
    var_20 = (uint64_t *)(var_0 + (-64L));
    *var_20 = var_19;
    var_21 = (uint64_t *)(var_0 + (-136L));
    var_22 = (uint64_t *)(var_0 + (-144L));
    var_23 = (uint64_t *)(var_0 + (-152L));
    var_24 = (uint64_t *)(var_0 + (-160L));
    var_25 = (uint64_t *)(var_0 + (-168L));
    var_26 = var_0 + (-4168L);
    var_27 = (uint64_t *)(var_0 + (-80L));
    var_28 = (uint64_t *)(var_0 + (-120L));
    var_29 = (uint64_t *)(var_0 + (-88L));
    var_30 = (uint64_t *)(var_0 + (-128L));
    var_31 = (uint64_t *)(var_0 + (-96L));
    var_32 = (uint64_t *)(var_0 + (-104L));
    var_33 = (unsigned char *)(var_0 + (-105L));
    var_34 = (unsigned char *)(var_0 + (-106L));
    var_35 = (uint32_t *)(var_0 + (-68L));
    storemerge42 = 4294967295U;
    local_sp_8 = var_4;
    var_215 = 1U;
    rax_0_shrunk = 0U;
    while (1U)
        {
            *var_21 = *(uint64_t *)(*var_10 + 40UL);
            *var_22 = *(uint64_t *)(*var_10 + 32UL);
            var_36 = *var_13;
            var_37 = *var_18;
            var_38 = helper_cc_compute_c_wrapper(var_37 - var_36, var_36, var_2, 17U);
            *var_18 = ((var_38 == 0UL) ? var_37 : var_36);
            var_39 = *var_16;
            var_40 = *var_20;
            var_41 = helper_cc_compute_c_wrapper(var_40 - var_39, var_39, var_2, 17U);
            *var_20 = ((var_41 == 0UL) ? var_40 : var_39);
            *var_23 = (*var_18 - *var_13);
            *var_24 = (*var_20 - *var_16);
            local_sp_9 = local_sp_8;
            if (*(unsigned char *)4388520UL == '\x00') {
                local_sp_10 = local_sp_9;
                local_sp_13 = local_sp_9;
                if (*var_22 == 0UL) {
                    if (*var_21 == 0UL) {
                        *var_27 = *var_13;
                        var_77 = *var_23;
                        *var_31 = var_77;
                        *var_33 = *(unsigned char *)(var_77 + *var_27);
                        *(unsigned char *)(*var_31 + *var_27) = (unsigned char)'\x00';
                        *var_29 = *var_16;
                        var_78 = *var_24;
                        *var_32 = var_78;
                        *var_34 = *(unsigned char *)(var_78 + *var_29);
                        *(unsigned char *)(*var_32 + *var_29) = (unsigned char)'\x00';
                    } else {
                        var_46 = (*var_24 + *var_23) + 2UL;
                        *var_25 = var_46;
                        if (var_46 > 4000UL) {
                            var_47 = local_sp_9 + (-8L);
                            *(uint64_t *)var_47 = 4226236UL;
                            var_48 = indirect_placeholder_87(var_46);
                            var_49 = var_48.field_3;
                            *var_28 = var_49;
                            *var_27 = var_49;
                            var_50 = var_49;
                            local_sp_10 = var_47;
                        } else {
                            *var_27 = var_26;
                            *var_28 = 0UL;
                            var_50 = *var_27;
                        }
                        *var_29 = (var_50 + (*var_23 + 1UL));
                        *var_30 = 0UL;
                        *var_31 = 0UL;
                        var_51 = *var_30;
                        local_sp_11 = local_sp_10;
                        var_52 = *var_23;
                        var_53 = helper_cc_compute_c_wrapper(var_51 - var_52, var_52, var_2, 17U);
                        local_sp_12 = local_sp_11;
                        local_sp_22 = local_sp_11;
                        while (var_53 != 0UL)
                            {
                                if (*var_22 != 0UL) {
                                    var_54 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(*var_30 + *var_13);
                                    var_55 = local_sp_11 + (-8L);
                                    *(uint64_t *)var_55 = 4226325UL;
                                    var_56 = indirect_placeholder_2(var_54);
                                    local_sp_22 = var_55;
                                    local_sp_24 = var_55;
                                    if (*(unsigned char *)(*var_22 + (uint64_t)(unsigned char)var_56) != '\x01') {
                                        var_63 = *var_30 + 1UL;
                                        *var_30 = var_63;
                                        var_51 = var_63;
                                        local_sp_11 = local_sp_24;
                                        var_52 = *var_23;
                                        var_53 = helper_cc_compute_c_wrapper(var_51 - var_52, var_52, var_2, 17U);
                                        local_sp_12 = local_sp_11;
                                        local_sp_22 = local_sp_11;
                                        continue;
                                    }
                                }
                                var_57 = (*var_21 == 0UL);
                                var_58 = *var_30 + *var_13;
                                local_sp_23 = local_sp_22;
                                storemerge_in_in_in = var_58;
                                if (var_57) {
                                    var_59 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)var_58;
                                    var_60 = local_sp_22 + (-8L);
                                    *(uint64_t *)var_60 = 4226379UL;
                                    var_61 = indirect_placeholder_2(var_59);
                                    local_sp_23 = var_60;
                                    storemerge_in_in_in = *var_21 + (uint64_t)(unsigned char)var_61;
                                }
                                storemerge_in401 = *(unsigned char *)storemerge_in_in_in;
                                var_62 = *var_31;
                                *var_31 = (var_62 + 1UL);
                                *(unsigned char *)(var_62 + *var_27) = storemerge_in401;
                                local_sp_24 = local_sp_23;
                                var_63 = *var_30 + 1UL;
                                *var_30 = var_63;
                                var_51 = var_63;
                                local_sp_11 = local_sp_24;
                                var_52 = *var_23;
                                var_53 = helper_cc_compute_c_wrapper(var_51 - var_52, var_52, var_2, 17U);
                                local_sp_12 = local_sp_11;
                                local_sp_22 = local_sp_11;
                            }
                        *(unsigned char *)(*var_31 + *var_27) = (unsigned char)'\x00';
                        *var_30 = 0UL;
                        *var_32 = 0UL;
                        var_64 = *var_30;
                        var_65 = *var_24;
                        var_66 = helper_cc_compute_c_wrapper(var_64 - var_65, var_65, var_2, 17U);
                        local_sp_13 = local_sp_12;
                        local_sp_19 = local_sp_12;
                        while (var_66 != 0UL)
                            {
                                if (*var_22 != 0UL) {
                                    var_67 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(*var_30 + *var_16);
                                    var_68 = local_sp_12 + (-8L);
                                    *(uint64_t *)var_68 = 4226520UL;
                                    var_69 = indirect_placeholder_2(var_67);
                                    local_sp_19 = var_68;
                                    local_sp_21 = var_68;
                                    if (*(unsigned char *)(*var_22 + (uint64_t)(unsigned char)var_69) != '\x01') {
                                        var_76 = *var_30 + 1UL;
                                        *var_30 = var_76;
                                        var_64 = var_76;
                                        local_sp_12 = local_sp_21;
                                        var_65 = *var_24;
                                        var_66 = helper_cc_compute_c_wrapper(var_64 - var_65, var_65, var_2, 17U);
                                        local_sp_13 = local_sp_12;
                                        local_sp_19 = local_sp_12;
                                        continue;
                                    }
                                }
                                var_70 = (*var_21 == 0UL);
                                var_71 = *var_30 + *var_16;
                                local_sp_20 = local_sp_19;
                                storemerge41_in_in_in = var_71;
                                if (var_70) {
                                    var_72 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)var_71;
                                    var_73 = local_sp_19 + (-8L);
                                    *(uint64_t *)var_73 = 4226574UL;
                                    var_74 = indirect_placeholder_2(var_72);
                                    local_sp_20 = var_73;
                                    storemerge41_in_in_in = *var_21 + (uint64_t)(unsigned char)var_74;
                                }
                                storemerge41_in402 = *(unsigned char *)storemerge41_in_in_in;
                                var_75 = *var_32;
                                *var_32 = (var_75 + 1UL);
                                *(unsigned char *)(var_75 + *var_29) = storemerge41_in402;
                                local_sp_21 = local_sp_20;
                                var_76 = *var_30 + 1UL;
                                *var_30 = var_76;
                                var_64 = var_76;
                                local_sp_12 = local_sp_21;
                                var_65 = *var_24;
                                var_66 = helper_cc_compute_c_wrapper(var_64 - var_65, var_65, var_2, 17U);
                                local_sp_13 = local_sp_12;
                                local_sp_19 = local_sp_12;
                            }
                        *(unsigned char *)(*var_32 + *var_29) = (unsigned char)'\x00';
                    }
                } else {
                    var_46 = (*var_24 + *var_23) + 2UL;
                    *var_25 = var_46;
                    if (var_46 > 4000UL) {
                        *var_27 = var_26;
                        *var_28 = 0UL;
                        var_50 = *var_27;
                    } else {
                        var_47 = local_sp_9 + (-8L);
                        *(uint64_t *)var_47 = 4226236UL;
                        var_48 = indirect_placeholder_87(var_46);
                        var_49 = var_48.field_3;
                        *var_28 = var_49;
                        *var_27 = var_49;
                        var_50 = var_49;
                        local_sp_10 = var_47;
                    }
                    *var_29 = (var_50 + (*var_23 + 1UL));
                    *var_30 = 0UL;
                    *var_31 = 0UL;
                    var_51 = *var_30;
                    local_sp_11 = local_sp_10;
                    var_52 = *var_23;
                    var_53 = helper_cc_compute_c_wrapper(var_51 - var_52, var_52, var_2, 17U);
                    local_sp_12 = local_sp_11;
                    local_sp_22 = local_sp_11;
                    while (var_53 != 0UL)
                        {
                            if (*var_22 != 0UL) {
                                var_54 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(*var_30 + *var_13);
                                var_55 = local_sp_11 + (-8L);
                                *(uint64_t *)var_55 = 4226325UL;
                                var_56 = indirect_placeholder_2(var_54);
                                local_sp_22 = var_55;
                                local_sp_24 = var_55;
                                if (*(unsigned char *)(*var_22 + (uint64_t)(unsigned char)var_56) != '\x01') {
                                    var_63 = *var_30 + 1UL;
                                    *var_30 = var_63;
                                    var_51 = var_63;
                                    local_sp_11 = local_sp_24;
                                    var_52 = *var_23;
                                    var_53 = helper_cc_compute_c_wrapper(var_51 - var_52, var_52, var_2, 17U);
                                    local_sp_12 = local_sp_11;
                                    local_sp_22 = local_sp_11;
                                    continue;
                                }
                            }
                            var_57 = (*var_21 == 0UL);
                            var_58 = *var_30 + *var_13;
                            local_sp_23 = local_sp_22;
                            storemerge_in_in_in = var_58;
                            if (var_57) {
                                var_59 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)var_58;
                                var_60 = local_sp_22 + (-8L);
                                *(uint64_t *)var_60 = 4226379UL;
                                var_61 = indirect_placeholder_2(var_59);
                                local_sp_23 = var_60;
                                storemerge_in_in_in = *var_21 + (uint64_t)(unsigned char)var_61;
                            }
                            storemerge_in401 = *(unsigned char *)storemerge_in_in_in;
                            var_62 = *var_31;
                            *var_31 = (var_62 + 1UL);
                            *(unsigned char *)(var_62 + *var_27) = storemerge_in401;
                            local_sp_24 = local_sp_23;
                            var_63 = *var_30 + 1UL;
                            *var_30 = var_63;
                            var_51 = var_63;
                            local_sp_11 = local_sp_24;
                            var_52 = *var_23;
                            var_53 = helper_cc_compute_c_wrapper(var_51 - var_52, var_52, var_2, 17U);
                            local_sp_12 = local_sp_11;
                            local_sp_22 = local_sp_11;
                        }
                    *(unsigned char *)(*var_31 + *var_27) = (unsigned char)'\x00';
                    *var_30 = 0UL;
                    *var_32 = 0UL;
                    var_64 = *var_30;
                    var_65 = *var_24;
                    var_66 = helper_cc_compute_c_wrapper(var_64 - var_65, var_65, var_2, 17U);
                    local_sp_13 = local_sp_12;
                    local_sp_19 = local_sp_12;
                    while (var_66 != 0UL)
                        {
                            if (*var_22 != 0UL) {
                                var_67 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(*var_30 + *var_16);
                                var_68 = local_sp_12 + (-8L);
                                *(uint64_t *)var_68 = 4226520UL;
                                var_69 = indirect_placeholder_2(var_67);
                                local_sp_19 = var_68;
                                local_sp_21 = var_68;
                                if (*(unsigned char *)(*var_22 + (uint64_t)(unsigned char)var_69) != '\x01') {
                                    var_76 = *var_30 + 1UL;
                                    *var_30 = var_76;
                                    var_64 = var_76;
                                    local_sp_12 = local_sp_21;
                                    var_65 = *var_24;
                                    var_66 = helper_cc_compute_c_wrapper(var_64 - var_65, var_65, var_2, 17U);
                                    local_sp_13 = local_sp_12;
                                    local_sp_19 = local_sp_12;
                                    continue;
                                }
                            }
                            var_70 = (*var_21 == 0UL);
                            var_71 = *var_30 + *var_16;
                            local_sp_20 = local_sp_19;
                            storemerge41_in_in_in = var_71;
                            if (var_70) {
                                var_72 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)var_71;
                                var_73 = local_sp_19 + (-8L);
                                *(uint64_t *)var_73 = 4226574UL;
                                var_74 = indirect_placeholder_2(var_72);
                                local_sp_20 = var_73;
                                storemerge41_in_in_in = *var_21 + (uint64_t)(unsigned char)var_74;
                            }
                            storemerge41_in402 = *(unsigned char *)storemerge41_in_in_in;
                            var_75 = *var_32;
                            *var_32 = (var_75 + 1UL);
                            *(unsigned char *)(var_75 + *var_29) = storemerge41_in402;
                            local_sp_21 = local_sp_20;
                            var_76 = *var_30 + 1UL;
                            *var_30 = var_76;
                            var_64 = var_76;
                            local_sp_12 = local_sp_21;
                            var_65 = *var_24;
                            var_66 = helper_cc_compute_c_wrapper(var_64 - var_65, var_65, var_2, 17U);
                            local_sp_13 = local_sp_12;
                            local_sp_19 = local_sp_12;
                        }
                    *(unsigned char *)(*var_32 + *var_29) = (unsigned char)'\x00';
                }
                var_79 = *var_10;
                local_sp_14 = local_sp_13;
                if (*(unsigned char *)(var_79 + 50UL) == '\x00') {
                    var_80 = *var_29;
                    var_81 = *var_27;
                    var_82 = local_sp_13 + (-8L);
                    *(uint64_t *)var_82 = 4226793UL;
                    var_83 = indirect_placeholder_86(var_81, var_80);
                    *var_35 = (uint32_t)var_83.field_0;
                    local_sp_14 = var_82;
                } else {
                    if (*(unsigned char *)(var_79 + 52UL) == '\x00') {
                        var_84 = *var_29;
                        var_85 = *var_27;
                        var_86 = local_sp_13 + (-8L);
                        *(uint64_t *)var_86 = 4226832UL;
                        var_87 = indirect_placeholder_8(var_85, var_84);
                        *var_35 = (uint32_t)var_87;
                        local_sp_14 = var_86;
                    } else {
                        if (*(unsigned char *)(var_79 + 53UL) == '\x00') {
                            var_88 = *var_29;
                            var_89 = *var_27;
                            var_90 = local_sp_13 + (-8L);
                            *(uint64_t *)var_90 = 4226871UL;
                            var_91 = indirect_placeholder_8(var_89, var_88);
                            *var_35 = (uint32_t)var_91;
                            local_sp_14 = var_90;
                        } else {
                            if (*(unsigned char *)(var_79 + 54UL) == '\x00') {
                                var_92 = *var_27;
                                *(uint64_t *)(local_sp_13 + (-8L)) = 4226908UL;
                                var_93 = indirect_placeholder_8(var_92, 0UL);
                                var_94 = *var_29;
                                var_95 = local_sp_13 + (-16L);
                                *(uint64_t *)var_95 = 4226927UL;
                                var_96 = indirect_placeholder_8(var_94, 0UL);
                                *var_35 = ((uint32_t)var_93 - (uint32_t)var_96);
                                local_sp_14 = var_95;
                            } else {
                                if (*(unsigned char *)(var_79 + 51UL) == '\x00') {
                                    var_97 = *var_32;
                                    var_98 = *var_29;
                                    var_99 = *var_31;
                                    var_100 = *var_27;
                                    var_101 = local_sp_13 + (-8L);
                                    *(uint64_t *)var_101 = 4226975UL;
                                    var_102 = indirect_placeholder_5(var_97, var_98, var_100, var_99);
                                    *var_35 = (uint32_t)var_102;
                                    local_sp_14 = var_101;
                                } else {
                                    if (*(unsigned char *)(var_79 + 56UL) == '\x00') {
                                        var_103 = *var_29;
                                        var_104 = *var_27;
                                        var_105 = local_sp_13 + (-8L);
                                        *(uint64_t *)var_105 = 4227011UL;
                                        var_106 = indirect_placeholder_8(var_104, var_103);
                                        *var_35 = (uint32_t)var_106;
                                        local_sp_14 = var_105;
                                    } else {
                                        var_107 = *var_31;
                                        var_108 = (var_107 == 0UL);
                                        var_109 = *var_32;
                                        if (var_108) {
                                            *var_35 = (var_109 != 0UL);
                                        } else {
                                            if (var_109 == 0UL) {
                                                *var_35 = 1U;
                                            } else {
                                                var_110 = var_109 + 1UL;
                                                var_111 = var_107 + 1UL;
                                                var_112 = *var_29;
                                                var_113 = *var_27;
                                                var_114 = local_sp_13 + (-8L);
                                                *(uint64_t *)var_114 = 4227089UL;
                                                var_115 = indirect_placeholder_5(var_110, var_112, var_113, var_111);
                                                *var_35 = (uint32_t)var_115;
                                                local_sp_14 = var_114;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                local_sp_15 = local_sp_14;
                if (*var_22 == 0UL) {
                    var_116 = local_sp_14 + (-8L);
                    *(uint64_t *)var_116 = 4227121UL;
                    indirect_placeholder();
                    local_sp_15 = var_116;
                } else {
                    if (*var_21 == 0UL) {
                        *(unsigned char *)(*var_27 + *var_31) = *var_33;
                        *(unsigned char *)(*var_29 + *var_32) = *var_34;
                    } else {
                        var_116 = local_sp_14 + (-8L);
                        *(uint64_t *)var_116 = 4227121UL;
                        indirect_placeholder();
                        local_sp_15 = var_116;
                    }
                }
                var_214 = *var_35;
                local_sp_16 = local_sp_15;
                var_215 = var_214;
                if (var_214 != 0U) {
                    loop_state_var = 1U;
                    break;
                }
                var_216 = *(uint64_t *)(*var_10 + 64UL);
                *var_10 = var_216;
                if (var_216 != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                if (*(uint64_t *)(var_216 + 16UL) == 18446744073709551615UL) {
                    *var_18 = ((*(uint64_t *)(*var_6 + 8UL) + (-1L)) + **(uint64_t **)var_5);
                    *var_20 = ((*(uint64_t *)(*var_7 + 8UL) + (-1L)) + **(uint64_t **)var_4);
                } else {
                    var_217 = *var_6;
                    *(uint64_t *)(local_sp_15 + (-8L)) = 4228096UL;
                    var_218 = indirect_placeholder_8(var_217, var_216);
                    *var_18 = var_218;
                    var_219 = *var_10;
                    var_220 = *var_7;
                    var_221 = local_sp_15 + (-16L);
                    *(uint64_t *)var_221 = 4228122UL;
                    var_222 = indirect_placeholder_8(var_220, var_219);
                    *var_20 = var_222;
                    local_sp_16 = var_221;
                }
                local_sp_8_be = local_sp_16;
                local_sp_17 = local_sp_16;
                if (**(uint64_t **)var_9 != 18446744073709551615UL) {
                    *var_13 = **(uint64_t **)var_5;
                    *var_16 = **(uint64_t **)var_4;
                    if (*(unsigned char *)(*var_10 + 48UL) != '\x00') {
                        var_230 = *var_13;
                        var_231 = *var_18;
                        var_232 = helper_cc_compute_c_wrapper(var_230 - var_231, var_231, var_2, 17U);
                        local_sp_18_ph = local_sp_17;
                        while (var_232 != 0UL)
                            {
                                var_233 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)var_12;
                                var_234 = local_sp_17 + (-8L);
                                *(uint64_t *)var_234 = 4228340UL;
                                var_235 = indirect_placeholder_2(var_233);
                                local_sp_17 = var_234;
                                local_sp_18_ph = var_234;
                                if (*(unsigned char *)((uint64_t)(unsigned char)var_235 + 4388576UL) == '\x00') {
                                    break;
                                }
                                var_236 = *var_13 + 1UL;
                                *var_13 = var_236;
                                var_230 = var_236;
                                var_231 = *var_18;
                                var_232 = helper_cc_compute_c_wrapper(var_230 - var_231, var_231, var_2, 17U);
                                local_sp_18_ph = local_sp_17;
                            }
                        var_237 = *var_16;
                        local_sp_18 = local_sp_18_ph;
                        var_238 = *var_20;
                        var_239 = helper_cc_compute_c_wrapper(var_237 - var_238, var_238, var_2, 17U);
                        local_sp_8_be = local_sp_18;
                        while (var_239 != 0UL)
                            {
                                var_240 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)var_15;
                                var_241 = local_sp_18 + (-8L);
                                *(uint64_t *)var_241 = 4228394UL;
                                var_242 = indirect_placeholder_2(var_240);
                                local_sp_8_be = var_241;
                                local_sp_18 = var_241;
                                if (*(unsigned char *)((uint64_t)(unsigned char)var_242 + 4388576UL) == '\x00') {
                                    break;
                                }
                                var_243 = *var_16 + 1UL;
                                *var_16 = var_243;
                                var_237 = var_243;
                                var_238 = *var_20;
                                var_239 = helper_cc_compute_c_wrapper(var_237 - var_238, var_238, var_2, 17U);
                                local_sp_8_be = local_sp_18;
                            }
                    }
                }
                var_223 = *var_10;
                var_224 = *var_6;
                *(uint64_t *)(local_sp_16 + (-8L)) = 4228227UL;
                var_225 = indirect_placeholder_8(var_224, var_223);
                *var_13 = var_225;
                var_226 = *var_10;
                var_227 = *var_7;
                var_228 = local_sp_16 + (-16L);
                *(uint64_t *)var_228 = 4228253UL;
                var_229 = indirect_placeholder_8(var_227, var_226);
                *var_16 = var_229;
                local_sp_8_be = var_228;
                local_sp_8 = local_sp_8_be;
                continue;
            }
            var_42 = *var_10;
            var_43 = local_sp_8 + (-8L);
            *(uint64_t *)var_43 = 4226094UL;
            var_44 = indirect_placeholder_2(var_42);
            local_sp_0 = var_43;
            local_sp_3 = var_43;
            local_sp_9 = var_43;
            local_sp_15 = var_43;
            local_sp_6 = var_43;
            var_45 = *var_10;
            if ((uint64_t)(unsigned char)var_44 != 0UL & *(unsigned char *)(var_45 + 54UL) != '\x00' & *(unsigned char *)(var_45 + 51UL) != '\x00' & *(unsigned char *)(var_45 + 56UL) != '\x00') {
                if (*var_22 == 0UL) {
                    var_187 = (*var_23 == 0UL);
                    var_188 = *var_24;
                    if (var_187) {
                        *var_35 = (var_188 != 0UL);
                    } else {
                        if (var_188 != 0UL) {
                            *var_35 = 1U;
                            loop_state_var = 1U;
                            break;
                        }
                        if (*var_21 == 0UL) {
                            var_208 = *var_13;
                            var_209 = local_sp_8 + (-16L);
                            *(uint64_t *)var_209 = 4227968UL;
                            indirect_placeholder();
                            var_210 = (uint32_t)var_208;
                            *var_35 = var_210;
                            local_sp_7 = var_209;
                            var_215 = var_210;
                            if (var_210 != 0U) {
                                loop_state_var = 1U;
                                break;
                            }
                        }
                        while (1U)
                            {
                                var_189 = *var_13;
                                var_190 = *var_18;
                                var_191 = helper_cc_compute_c_wrapper(var_189 - var_190, var_190, var_2, 17U);
                                local_sp_7 = local_sp_6;
                                if (var_191 != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                var_192 = *var_16;
                                var_193 = *var_20;
                                var_194 = helper_cc_compute_c_wrapper(var_192 - var_193, var_193, var_2, 17U);
                                if (var_194 != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                var_195 = *var_13;
                                *var_13 = (var_195 + 1UL);
                                var_196 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)var_195;
                                *(uint64_t *)(local_sp_6 + (-8L)) = 4227804UL;
                                var_197 = indirect_placeholder_2(var_196);
                                var_198 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(*var_21 + (uint64_t)(unsigned char)var_197);
                                *(uint64_t *)(local_sp_6 + (-16L)) = 4227827UL;
                                var_199 = indirect_placeholder_2(var_198);
                                var_200 = (uint64_t)(unsigned char)var_199;
                                var_201 = *var_16;
                                *var_16 = (var_201 + 1UL);
                                var_202 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)var_201;
                                *(uint64_t *)(local_sp_6 + (-24L)) = 4227855UL;
                                var_203 = indirect_placeholder_2(var_202);
                                var_204 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(*var_21 + (uint64_t)(unsigned char)var_203);
                                var_205 = local_sp_6 + (-32L);
                                *(uint64_t *)var_205 = 4227878UL;
                                var_206 = indirect_placeholder_2(var_204);
                                var_207 = (uint32_t)var_200 - (uint32_t)(uint64_t)(unsigned char)var_206;
                                *var_35 = var_207;
                                local_sp_6 = var_205;
                                var_215 = var_207;
                                if (var_207 == 0U) {
                                    continue;
                                }
                                loop_state_var = 0U;
                                break;
                            }
                        switch_state_var = 0;
                        switch (loop_state_var) {
                          case 1U:
                            {
                                var_211 = *var_23;
                                var_212 = *var_24;
                                var_213 = helper_cc_compute_c_wrapper(var_211 - var_212, var_212, var_2, 17U);
                                local_sp_15 = local_sp_7;
                                if (var_213 == 0UL) {
                                    storemerge42 = (*var_23 != *var_24);
                                }
                                *var_35 = storemerge42;
                            }
                            break;
                          case 0U:
                            {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            break;
                        }
                        if (switch_state_var)
                            break;
                    }
                } else {
                    if (*var_21 != 0UL) {
                        while (1U)
                            {
                                var_154 = *var_13;
                                var_155 = *var_18;
                                var_156 = helper_cc_compute_c_wrapper(var_154 - var_155, var_155, var_2, 17U);
                                local_sp_4_ph = local_sp_3;
                                if (var_156 != 0UL) {
                                    var_157 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)var_12;
                                    var_158 = local_sp_3 + (-8L);
                                    *(uint64_t *)var_158 = 4227512UL;
                                    var_159 = indirect_placeholder_2(var_157);
                                    local_sp_3_be = var_158;
                                    local_sp_4_ph = var_158;
                                    if (*(unsigned char *)(*var_22 + (uint64_t)(unsigned char)var_159) != '\x00') {
                                        *var_13 = (*var_13 + 1UL);
                                        local_sp_3 = local_sp_3_be;
                                        continue;
                                    }
                                }
                                var_160 = *var_16;
                                local_sp_4 = local_sp_4_ph;
                                var_161 = *var_20;
                                var_162 = helper_cc_compute_c_wrapper(var_160 - var_161, var_161, var_2, 17U);
                                local_sp_5 = local_sp_4;
                                while (var_162 != 0UL)
                                    {
                                        var_163 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)var_15;
                                        var_164 = local_sp_4 + (-8L);
                                        *(uint64_t *)var_164 = 4227566UL;
                                        var_165 = indirect_placeholder_2(var_163);
                                        local_sp_4 = var_164;
                                        local_sp_5 = var_164;
                                        if (*(unsigned char *)(*var_22 + (uint64_t)(unsigned char)var_165) == '\x00') {
                                            break;
                                        }
                                        var_166 = *var_16 + 1UL;
                                        *var_16 = var_166;
                                        var_160 = var_166;
                                        var_161 = *var_20;
                                        var_162 = helper_cc_compute_c_wrapper(var_160 - var_161, var_161, var_2, 17U);
                                        local_sp_5 = local_sp_4;
                                    }
                                var_167 = *var_13;
                                var_168 = *var_18;
                                var_169 = helper_cc_compute_c_wrapper(var_167 - var_168, var_168, var_2, 17U);
                                local_sp_15 = local_sp_5;
                                if (var_169 != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                var_170 = *var_16;
                                var_171 = *var_20;
                                var_172 = helper_cc_compute_c_wrapper(var_170 - var_171, var_171, var_2, 17U);
                                if (var_172 != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                var_173 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)var_12;
                                *(uint64_t *)(local_sp_5 + (-8L)) = 4227623UL;
                                var_174 = indirect_placeholder_2(var_173);
                                var_175 = (uint64_t)(unsigned char)var_174;
                                var_176 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)var_15;
                                var_177 = local_sp_5 + (-16L);
                                *(uint64_t *)var_177 = 4227643UL;
                                var_178 = indirect_placeholder_2(var_176);
                                var_179 = (uint32_t)var_175 - (uint32_t)(uint64_t)(unsigned char)var_178;
                                *var_35 = var_179;
                                local_sp_3_be = var_177;
                                var_215 = var_179;
                                if (var_179 != 0U) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                *var_13 = (*var_13 + 1UL);
                                *var_16 = (*var_16 + 1UL);
                                local_sp_3 = local_sp_3_be;
                                continue;
                            }
                        switch_state_var = 0;
                        switch (loop_state_var) {
                          case 1U:
                            {
                                var_180 = *var_13;
                                var_181 = *var_18;
                                var_182 = helper_cc_compute_c_wrapper(var_180 - var_181, var_181, var_2, 17U);
                                var_183 = (uint64_t)(unsigned char)var_182;
                                var_184 = *var_16;
                                var_185 = *var_20;
                                var_186 = helper_cc_compute_c_wrapper(var_184 - var_185, var_185, var_2, 17U);
                                *var_35 = ((uint32_t)var_183 - (uint32_t)(uint64_t)(unsigned char)var_186);
                                var_214 = *var_35;
                                local_sp_16 = local_sp_15;
                                var_215 = var_214;
                                if (var_214 != 0U) {
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                                var_216 = *(uint64_t *)(*var_10 + 64UL);
                                *var_10 = var_216;
                                if (var_216 != 0UL) {
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                if (*(uint64_t *)(var_216 + 16UL) == 18446744073709551615UL) {
                                    *var_18 = ((*(uint64_t *)(*var_6 + 8UL) + (-1L)) + **(uint64_t **)var_5);
                                    *var_20 = ((*(uint64_t *)(*var_7 + 8UL) + (-1L)) + **(uint64_t **)var_4);
                                } else {
                                    var_217 = *var_6;
                                    *(uint64_t *)(local_sp_15 + (-8L)) = 4228096UL;
                                    var_218 = indirect_placeholder_8(var_217, var_216);
                                    *var_18 = var_218;
                                    var_219 = *var_10;
                                    var_220 = *var_7;
                                    var_221 = local_sp_15 + (-16L);
                                    *(uint64_t *)var_221 = 4228122UL;
                                    var_222 = indirect_placeholder_8(var_220, var_219);
                                    *var_20 = var_222;
                                    local_sp_16 = var_221;
                                }
                                local_sp_8_be = local_sp_16;
                                local_sp_17 = local_sp_16;
                                if (**(uint64_t **)var_9 != 18446744073709551615UL) {
                                    *var_13 = **(uint64_t **)var_5;
                                    *var_16 = **(uint64_t **)var_4;
                                    if (*(unsigned char *)(*var_10 + 48UL) != '\x00') {
                                        var_230 = *var_13;
                                        var_231 = *var_18;
                                        var_232 = helper_cc_compute_c_wrapper(var_230 - var_231, var_231, var_2, 17U);
                                        local_sp_18_ph = local_sp_17;
                                        while (var_232 != 0UL)
                                            {
                                                var_233 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)var_12;
                                                var_234 = local_sp_17 + (-8L);
                                                *(uint64_t *)var_234 = 4228340UL;
                                                var_235 = indirect_placeholder_2(var_233);
                                                local_sp_17 = var_234;
                                                local_sp_18_ph = var_234;
                                                if (*(unsigned char *)((uint64_t)(unsigned char)var_235 + 4388576UL) == '\x00') {
                                                    break;
                                                }
                                                var_236 = *var_13 + 1UL;
                                                *var_13 = var_236;
                                                var_230 = var_236;
                                                var_231 = *var_18;
                                                var_232 = helper_cc_compute_c_wrapper(var_230 - var_231, var_231, var_2, 17U);
                                                local_sp_18_ph = local_sp_17;
                                            }
                                        var_237 = *var_16;
                                        local_sp_18 = local_sp_18_ph;
                                        var_238 = *var_20;
                                        var_239 = helper_cc_compute_c_wrapper(var_237 - var_238, var_238, var_2, 17U);
                                        local_sp_8_be = local_sp_18;
                                        while (var_239 != 0UL)
                                            {
                                                var_240 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)var_15;
                                                var_241 = local_sp_18 + (-8L);
                                                *(uint64_t *)var_241 = 4228394UL;
                                                var_242 = indirect_placeholder_2(var_240);
                                                local_sp_8_be = var_241;
                                                local_sp_18 = var_241;
                                                if (*(unsigned char *)((uint64_t)(unsigned char)var_242 + 4388576UL) == '\x00') {
                                                    break;
                                                }
                                                var_243 = *var_16 + 1UL;
                                                *var_16 = var_243;
                                                var_237 = var_243;
                                                var_238 = *var_20;
                                                var_239 = helper_cc_compute_c_wrapper(var_237 - var_238, var_238, var_2, 17U);
                                                local_sp_8_be = local_sp_18;
                                            }
                                    }
                                }
                                var_223 = *var_10;
                                var_224 = *var_6;
                                *(uint64_t *)(local_sp_16 + (-8L)) = 4228227UL;
                                var_225 = indirect_placeholder_8(var_224, var_223);
                                *var_13 = var_225;
                                var_226 = *var_10;
                                var_227 = *var_7;
                                var_228 = local_sp_16 + (-16L);
                                *(uint64_t *)var_228 = 4228253UL;
                                var_229 = indirect_placeholder_8(var_227, var_226);
                                *var_16 = var_229;
                                local_sp_8_be = var_228;
                                local_sp_8 = local_sp_8_be;
                                continue;
                            }
                            break;
                          case 0U:
                            {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            break;
                        }
                        if (switch_state_var)
                            break;
                    }
                    while (1U)
                        {
                            var_117 = *var_13;
                            var_118 = *var_18;
                            var_119 = helper_cc_compute_c_wrapper(var_117 - var_118, var_118, var_2, 17U);
                            local_sp_1_ph = local_sp_0;
                            if (var_119 != 0UL) {
                                var_120 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)var_12;
                                var_121 = local_sp_0 + (-8L);
                                *(uint64_t *)var_121 = 4227224UL;
                                var_122 = indirect_placeholder_2(var_120);
                                local_sp_0_be = var_121;
                                local_sp_1_ph = var_121;
                                if (*(unsigned char *)(*var_22 + (uint64_t)(unsigned char)var_122) != '\x00') {
                                    *var_13 = (*var_13 + 1UL);
                                    local_sp_0 = local_sp_0_be;
                                    continue;
                                }
                            }
                            var_123 = *var_16;
                            local_sp_1 = local_sp_1_ph;
                            var_124 = *var_20;
                            var_125 = helper_cc_compute_c_wrapper(var_123 - var_124, var_124, var_2, 17U);
                            local_sp_2 = local_sp_1;
                            while (var_125 != 0UL)
                                {
                                    var_126 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)var_15;
                                    var_127 = local_sp_1 + (-8L);
                                    *(uint64_t *)var_127 = 4227278UL;
                                    var_128 = indirect_placeholder_2(var_126);
                                    local_sp_1 = var_127;
                                    local_sp_2 = var_127;
                                    if (*(unsigned char *)(*var_22 + (uint64_t)(unsigned char)var_128) == '\x00') {
                                        break;
                                    }
                                    var_129 = *var_16 + 1UL;
                                    *var_16 = var_129;
                                    var_123 = var_129;
                                    var_124 = *var_20;
                                    var_125 = helper_cc_compute_c_wrapper(var_123 - var_124, var_124, var_2, 17U);
                                    local_sp_2 = local_sp_1;
                                }
                            var_130 = *var_13;
                            var_131 = *var_18;
                            var_132 = helper_cc_compute_c_wrapper(var_130 - var_131, var_131, var_2, 17U);
                            local_sp_15 = local_sp_2;
                            if (var_132 != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            var_133 = *var_16;
                            var_134 = *var_20;
                            var_135 = helper_cc_compute_c_wrapper(var_133 - var_134, var_134, var_2, 17U);
                            if (var_135 != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            var_136 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)var_12;
                            *(uint64_t *)(local_sp_2 + (-8L)) = 4227339UL;
                            var_137 = indirect_placeholder_2(var_136);
                            var_138 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(*var_21 + (uint64_t)(unsigned char)var_137);
                            *(uint64_t *)(local_sp_2 + (-16L)) = 4227362UL;
                            var_139 = indirect_placeholder_2(var_138);
                            var_140 = (uint64_t)(unsigned char)var_139;
                            var_141 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)var_15;
                            *(uint64_t *)(local_sp_2 + (-24L)) = 4227382UL;
                            var_142 = indirect_placeholder_2(var_141);
                            var_143 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(*var_21 + (uint64_t)(unsigned char)var_142);
                            var_144 = local_sp_2 + (-32L);
                            *(uint64_t *)var_144 = 4227405UL;
                            var_145 = indirect_placeholder_2(var_143);
                            var_146 = (uint32_t)var_140 - (uint32_t)(uint64_t)(unsigned char)var_145;
                            *var_35 = var_146;
                            local_sp_0_be = var_144;
                            var_215 = var_146;
                            if (var_146 != 0U) {
                                loop_state_var = 1U;
                                break;
                            }
                            *var_13 = (*var_13 + 1UL);
                            *var_16 = (*var_16 + 1UL);
                            local_sp_0 = local_sp_0_be;
                            continue;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 0U:
                        {
                            var_147 = *var_13;
                            var_148 = *var_18;
                            var_149 = helper_cc_compute_c_wrapper(var_147 - var_148, var_148, var_2, 17U);
                            var_150 = (uint64_t)(unsigned char)var_149;
                            var_151 = *var_16;
                            var_152 = *var_20;
                            var_153 = helper_cc_compute_c_wrapper(var_151 - var_152, var_152, var_2, 17U);
                            *var_35 = ((uint32_t)var_150 - (uint32_t)(uint64_t)(unsigned char)var_153);
                        }
                        break;
                      case 1U:
                        {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            }
        }
    switch (loop_state_var) {
      case 0U:
        {
            return (uint64_t)rax_0_shrunk;
        }
        break;
      case 1U:
        {
            rax_0_shrunk = (*(unsigned char *)(*var_10 + 55UL) == '\x00') ? var_215 : (0U - var_215);
        }
        break;
    }
}
