typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_key_warnings_ret_type;
struct indirect_placeholder_76_ret_type;
struct indirect_placeholder_75_ret_type;
struct indirect_placeholder_77_ret_type;
struct indirect_placeholder_78_ret_type;
struct indirect_placeholder_81_ret_type;
struct indirect_placeholder_80_ret_type;
struct indirect_placeholder_79_ret_type;
struct indirect_placeholder_82_ret_type;
struct bb_key_warnings_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_76_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_75_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_77_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_78_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_81_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_80_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_79_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_82_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_8(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_r10(void);
extern uint64_t indirect_placeholder_4(void);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
extern struct indirect_placeholder_76_ret_type indirect_placeholder_76(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_75_ret_type indirect_placeholder_75(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_77_ret_type indirect_placeholder_77(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_78_ret_type indirect_placeholder_78(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_81_ret_type indirect_placeholder_81(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_80_ret_type indirect_placeholder_80(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_79_ret_type indirect_placeholder_79(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_82_ret_type indirect_placeholder_82(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
struct bb_key_warnings_ret_type bb_key_warnings(uint64_t r9, uint64_t r8, uint64_t rdi, uint64_t rsi) {
    unsigned char var_69;
    uint64_t r10_6;
    uint64_t var_70;
    uint64_t var_71;
    struct indirect_placeholder_76_ret_type var_111;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t *var_6;
    unsigned char *var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t *var_15;
    uint64_t *var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t *var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t *var_24;
    uint64_t *var_25;
    uint64_t *var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t *var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t *var_32;
    uint64_t var_33;
    unsigned char *var_34;
    unsigned char *var_35;
    unsigned char *var_36;
    uint64_t *var_37;
    uint64_t *var_38;
    unsigned char *var_39;
    unsigned char *var_40;
    unsigned char *var_41;
    unsigned char *var_42;
    unsigned char *var_43;
    unsigned char *var_44;
    unsigned char *var_45;
    unsigned char *var_46;
    unsigned char *var_47;
    uint64_t var_48;
    unsigned char var_98;
    uint64_t r93_1;
    uint64_t r84_1;
    unsigned char var_110;
    uint64_t r93_4;
    uint64_t r10_0;
    uint64_t r84_4;
    uint64_t r93_0;
    uint64_t rcx_3;
    uint64_t r84_0;
    uint64_t local_sp_4;
    uint64_t rcx_0;
    uint64_t local_sp_0;
    uint64_t r10_1;
    unsigned char var_77;
    struct bb_key_warnings_ret_type mrv;
    struct bb_key_warnings_ret_type mrv1;
    struct bb_key_warnings_ret_type mrv2;
    unsigned char var_99;
    unsigned char *var_100;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t storemerge;
    uint64_t var_103;
    struct indirect_placeholder_75_ret_type var_104;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t var_107;
    uint64_t var_108;
    unsigned char var_109;
    uint64_t r10_5;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t r10_3;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t r84_2;
    uint64_t local_sp_6;
    struct indirect_placeholder_77_ret_type var_93;
    unsigned char storemerge10;
    uint64_t var_76;
    unsigned char storemerge11;
    uint64_t r93_5;
    uint64_t r84_5;
    uint64_t rcx_4;
    uint64_t local_sp_5;
    uint64_t var_67;
    uint64_t r93_6;
    uint64_t var_78;
    bool var_79;
    uint64_t r84_6;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t rcx_5;
    struct indirect_placeholder_78_ret_type var_82;
    uint64_t r10_2;
    uint64_t r93_2;
    uint64_t rcx_1;
    uint64_t local_sp_1;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t r93_3;
    uint64_t r84_3;
    uint64_t rcx_2;
    uint64_t local_sp_2;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_51;
    uint64_t local_sp_3;
    struct indirect_placeholder_81_ret_type var_60;
    uint64_t var_61;
    struct indirect_placeholder_80_ret_type var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    struct indirect_placeholder_79_ret_type var_66;
    uint64_t **_pre_phi;
    uint64_t r10_4;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t **var_49;
    uint64_t var_50;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_68;
    unsigned char storemerge9;
    struct indirect_placeholder_82_ret_type var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r10();
    var_2 = init_rbp();
    var_3 = init_cc_src2();
    var_4 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    var_5 = var_0 + (-360L);
    var_6 = (uint64_t *)(var_0 + (-352L));
    *var_6 = rdi;
    var_7 = (unsigned char *)(var_0 + (-356L));
    *var_7 = (unsigned char)rsi;
    var_8 = *var_6;
    var_9 = *(uint64_t *)var_8;
    var_10 = *(uint64_t *)(var_8 + 8UL);
    var_11 = var_0 + (-184L);
    *(uint64_t *)var_11 = var_9;
    *(uint64_t *)(var_0 + (-176L)) = var_10;
    var_12 = *(uint64_t *)(var_8 + 24UL);
    *(uint64_t *)(var_0 + (-168L)) = *(uint64_t *)(var_8 + 16UL);
    *(uint64_t *)(var_0 + (-160L)) = var_12;
    var_13 = *(uint64_t *)(var_8 + 32UL);
    var_14 = *(uint64_t *)(var_8 + 40UL);
    var_15 = (uint64_t *)(var_0 + (-152L));
    *var_15 = var_13;
    var_16 = (uint64_t *)(var_0 + (-144L));
    *var_16 = var_14;
    var_17 = *(uint64_t *)(var_8 + 48UL);
    var_18 = *(uint64_t *)(var_8 + 56UL);
    var_19 = var_0 + (-136L);
    *(uint64_t *)var_19 = var_17;
    var_20 = var_0 + (-128L);
    *(uint64_t *)var_20 = var_18;
    *(uint64_t *)(var_0 + (-120L)) = *(uint64_t *)(var_8 + 64UL);
    var_21 = (uint64_t *)(var_0 + (-40L));
    *var_21 = 1UL;
    var_22 = *(uint64_t *)4389640UL;
    var_23 = var_0 + (-32L);
    var_24 = (uint64_t *)var_23;
    *var_24 = var_22;
    var_25 = (uint64_t *)(var_0 + (-48L));
    var_26 = (uint64_t *)(var_0 + (-72L));
    var_27 = var_0 + (-344L);
    var_28 = var_0 + (-80L);
    var_29 = (uint64_t *)var_28;
    var_30 = var_0 + (-296L);
    var_31 = var_0 + (-88L);
    var_32 = (uint64_t *)var_31;
    var_33 = var_0 + (-216L);
    var_34 = (unsigned char *)(var_0 + (-89L));
    var_35 = (unsigned char *)(var_0 + (-90L));
    var_36 = (unsigned char *)(var_0 + (-91L));
    var_37 = (uint64_t *)(var_0 + (-56L));
    var_38 = (uint64_t *)(var_0 + (-104L));
    var_39 = (unsigned char *)var_19;
    var_40 = (unsigned char *)(var_0 + (-135L));
    var_41 = (unsigned char *)(var_0 + (-130L));
    var_42 = (unsigned char *)(var_0 + (-134L));
    var_43 = (unsigned char *)(var_0 + (-132L));
    var_44 = (unsigned char *)(var_0 + (-131L));
    var_45 = (unsigned char *)(var_0 + (-133L));
    var_46 = (unsigned char *)var_20;
    var_47 = (unsigned char *)(var_0 + (-129L));
    var_48 = var_22;
    var_110 = (unsigned char)'\x00';
    r93_4 = r9;
    r84_4 = r8;
    rcx_3 = var_17;
    local_sp_4 = var_5;
    storemerge10 = (unsigned char)'\x01';
    storemerge11 = (unsigned char)'\x00';
    r10_4 = var_1;
    storemerge9 = (unsigned char)'\x00';
    r10_0 = r10_4;
    r93_0 = r93_4;
    r84_0 = r84_4;
    rcx_0 = rcx_3;
    r10_5 = r10_4;
    r93_5 = r93_4;
    r84_5 = r84_4;
    rcx_4 = rcx_3;
    local_sp_5 = local_sp_4;
    while (var_48 != 0UL)
        {
            if (*(unsigned char *)(var_48 + 57UL) == '\x00') {
                _pre_phi = (uint64_t **)var_23;
            } else {
                var_49 = (uint64_t **)var_23;
                *var_25 = **var_49;
                *var_26 = *(uint64_t *)(*var_24 + 16UL);
                *var_29 = var_27;
                *var_32 = var_30;
                var_50 = *var_25;
                var_51 = var_50;
                _pre_phi = var_49;
                if (var_50 == 18446744073709551615UL) {
                    *var_25 = 0UL;
                    var_51 = 0UL;
                }
                *(uint64_t *)(local_sp_4 + (-8L)) = 4224385UL;
                indirect_placeholder_8(var_51, var_33);
                **(uint16_t **)var_28 = (uint16_t)(unsigned short)43U;
                var_52 = *var_29 + 1UL;
                *(uint64_t *)(local_sp_4 + (-16L)) = 4224416UL;
                indirect_placeholder();
                *var_29 = var_52;
                var_53 = *var_25 + 1UL;
                *(uint64_t *)(local_sp_4 + (-24L)) = 4224446UL;
                indirect_placeholder_8(var_53, var_33);
                **(uint32_t **)var_31 = 2124589U;
                var_54 = *var_32 + 3UL;
                var_55 = local_sp_4 + (-32L);
                *(uint64_t *)var_55 = 4224478UL;
                indirect_placeholder();
                *var_32 = var_54;
                local_sp_3 = var_55;
                if (*(uint64_t *)(*var_24 + 16UL) != 18446744073709551615UL) {
                    var_56 = *var_26 + 1UL;
                    *(uint64_t *)(local_sp_4 + (-40L)) = 4224526UL;
                    indirect_placeholder_8(var_56, var_33);
                    var_57 = *var_29;
                    *(uint16_t *)var_57 = (uint16_t)(unsigned short)11552U;
                    *(unsigned char *)(var_57 + 2UL) = (unsigned char)'\x00';
                    *(uint64_t *)(local_sp_4 + (-48L)) = 4224561UL;
                    indirect_placeholder();
                    var_58 = (*var_26 + (*(uint64_t *)(*var_24 + 24UL) == 18446744073709551615UL)) + 1UL;
                    *(uint64_t *)(local_sp_4 + (-56L)) = 4224608UL;
                    indirect_placeholder_8(var_58, var_33);
                    **(uint16_t **)var_31 = (uint16_t)(unsigned short)44U;
                    var_59 = local_sp_4 + (-64L);
                    *(uint64_t *)var_59 = 4224639UL;
                    indirect_placeholder();
                    local_sp_3 = var_59;
                }
                *(uint64_t *)(local_sp_3 + (-8L)) = 4224659UL;
                var_60 = indirect_placeholder_81(1UL, var_30);
                var_61 = var_60.field_1;
                *(uint64_t *)(local_sp_3 + (-16L)) = 4224682UL;
                var_62 = indirect_placeholder_80(0UL, var_27);
                var_63 = var_62.field_0;
                var_64 = var_62.field_1;
                var_65 = local_sp_3 + (-24L);
                *(uint64_t *)var_65 = 4224713UL;
                var_66 = indirect_placeholder_79(var_63, var_61, 0UL, var_64, 4352904UL, 0UL, 0UL);
                r10_5 = var_66.field_0;
                r93_5 = var_66.field_1;
                r84_5 = var_66.field_2;
                rcx_4 = var_66.field_3;
                local_sp_5 = var_65;
            }
            var_67 = **_pre_phi;
            r10_6 = r10_5;
            local_sp_6 = local_sp_5;
            r93_6 = r93_5;
            r84_6 = r84_5;
            rcx_5 = rcx_4;
            if (var_67 == 18446744073709551615UL) {
            } else {
                var_68 = helper_cc_compute_c_wrapper(*(uint64_t *)(*var_24 + 16UL) - var_67, var_67, var_3, 17U);
                storemerge9 = (unsigned char)'\x01';
                if (var_68 == 0UL) {
                }
            }
            var_69 = storemerge9 & '\x01';
            *var_34 = var_69;
            if (var_69 == '\x00') {
                var_70 = *var_21;
                var_71 = local_sp_5 + (-8L);
                *(uint64_t *)var_71 = 4224803UL;
                var_72 = indirect_placeholder_82(r93_5, r84_5, 0UL, var_70, 4352952UL, 0UL, 0UL);
                r10_6 = var_72.field_0;
                local_sp_6 = var_71;
                r93_6 = var_72.field_1;
                r84_6 = var_72.field_2;
                rcx_5 = var_72.field_3;
            }
            var_73 = *var_24;
            var_74 = local_sp_6 + (-8L);
            *(uint64_t *)var_74 = 4224815UL;
            var_75 = indirect_placeholder_2(var_73);
            r84_2 = r84_6;
            r10_2 = r10_6;
            r93_2 = r93_6;
            rcx_1 = rcx_5;
            local_sp_1 = var_74;
            if ((uint64_t)(unsigned char)var_75 == 0UL) {
            } else {
                storemerge10 = (unsigned char)'\x00';
                if (*(unsigned char *)(*var_24 + 54UL) == '\x00') {
                }
            }
            *var_35 = (storemerge10 & '\x01');
            var_76 = *var_24;
            if (*(uint64_t *)(var_76 + 16UL) == 0UL) {
            } else {
                storemerge11 = (unsigned char)'\x01';
                if (*(uint64_t *)(var_76 + 24UL) == 0UL) {
                }
            }
            var_77 = storemerge11 & '\x01';
            *var_36 = var_77;
            if (*var_34 != '\x01' & *var_7 != '\x01' & !((*(uint32_t *)4388168UL != 128U) || (var_77 == '\x01'))) {
                var_78 = *var_24;
                var_79 = (*(unsigned char *)(var_78 + 48UL) == '\x01');
                if (var_79) {
                    if (*var_35 == '\x01') {
                        var_80 = *var_21;
                        var_81 = local_sp_6 + (-16L);
                        *(uint64_t *)var_81 = 4225066UL;
                        var_82 = indirect_placeholder_78(r93_6, r84_6, 0UL, var_80, 4353000UL, 0UL, 0UL);
                        r84_2 = var_82.field_2;
                        r10_2 = var_82.field_0;
                        r93_2 = var_82.field_1;
                        rcx_1 = var_82.field_3;
                        local_sp_1 = var_81;
                    } else {
                        if (var_79) {
                            if (*(uint64_t *)(var_78 + 8UL) == 0UL) {
                                var_80 = *var_21;
                                var_81 = local_sp_6 + (-16L);
                                *(uint64_t *)var_81 = 4225066UL;
                                var_82 = indirect_placeholder_78(r93_6, r84_6, 0UL, var_80, 4353000UL, 0UL, 0UL);
                                r84_2 = var_82.field_2;
                                r10_2 = var_82.field_0;
                                r93_2 = var_82.field_1;
                                rcx_1 = var_82.field_3;
                                local_sp_1 = var_81;
                            } else {
                                if (*(unsigned char *)(var_78 + 49UL) != '\x01' & *(uint64_t *)(var_78 + 24UL) == 0UL) {
                                    var_80 = *var_21;
                                    var_81 = local_sp_6 + (-16L);
                                    *(uint64_t *)var_81 = 4225066UL;
                                    var_82 = indirect_placeholder_78(r93_6, r84_6, 0UL, var_80, 4353000UL, 0UL, 0UL);
                                    r84_2 = var_82.field_2;
                                    r10_2 = var_82.field_0;
                                    r93_2 = var_82.field_1;
                                    rcx_1 = var_82.field_3;
                                    local_sp_1 = var_81;
                                }
                            }
                        } else {
                            if (*(unsigned char *)(var_78 + 49UL) != '\x01' & *(uint64_t *)(var_78 + 24UL) == 0UL) {
                                var_80 = *var_21;
                                var_81 = local_sp_6 + (-16L);
                                *(uint64_t *)var_81 = 4225066UL;
                                var_82 = indirect_placeholder_78(r93_6, r84_6, 0UL, var_80, 4353000UL, 0UL, 0UL);
                                r84_2 = var_82.field_2;
                                r10_2 = var_82.field_0;
                                r93_2 = var_82.field_1;
                                rcx_1 = var_82.field_3;
                                local_sp_1 = var_81;
                            }
                        }
                    }
                } else {
                    if (var_79) {
                        if (*(uint64_t *)(var_78 + 8UL) == 0UL) {
                            var_80 = *var_21;
                            var_81 = local_sp_6 + (-16L);
                            *(uint64_t *)var_81 = 4225066UL;
                            var_82 = indirect_placeholder_78(r93_6, r84_6, 0UL, var_80, 4353000UL, 0UL, 0UL);
                            r84_2 = var_82.field_2;
                            r10_2 = var_82.field_0;
                            r93_2 = var_82.field_1;
                            rcx_1 = var_82.field_3;
                            local_sp_1 = var_81;
                        } else {
                            if (*(unsigned char *)(var_78 + 49UL) != '\x01' & *(uint64_t *)(var_78 + 24UL) == 0UL) {
                                var_80 = *var_21;
                                var_81 = local_sp_6 + (-16L);
                                *(uint64_t *)var_81 = 4225066UL;
                                var_82 = indirect_placeholder_78(r93_6, r84_6, 0UL, var_80, 4353000UL, 0UL, 0UL);
                                r84_2 = var_82.field_2;
                                r10_2 = var_82.field_0;
                                r93_2 = var_82.field_1;
                                rcx_1 = var_82.field_3;
                                local_sp_1 = var_81;
                            }
                        }
                    } else {
                        if (*(unsigned char *)(var_78 + 49UL) != '\x01' & *(uint64_t *)(var_78 + 24UL) == 0UL) {
                            var_80 = *var_21;
                            var_81 = local_sp_6 + (-16L);
                            *(uint64_t *)var_81 = 4225066UL;
                            var_82 = indirect_placeholder_78(r93_6, r84_6, 0UL, var_80, 4353000UL, 0UL, 0UL);
                            r84_2 = var_82.field_2;
                            r10_2 = var_82.field_0;
                            r93_2 = var_82.field_1;
                            rcx_1 = var_82.field_3;
                            local_sp_1 = var_81;
                        }
                    }
                }
            }
            r10_3 = r10_2;
            r93_3 = r93_2;
            r84_3 = r84_2;
            rcx_2 = rcx_1;
            local_sp_2 = local_sp_1;
            var_83 = *var_24;
            var_84 = local_sp_1 + (-8L);
            *(uint64_t *)var_84 = 4225092UL;
            var_85 = indirect_placeholder_2(var_83);
            local_sp_2 = var_84;
            if (*var_7 != '\x01' & (uint64_t)(unsigned char)var_85 != 0UL) {
                *var_37 = (**_pre_phi + 1UL);
                var_86 = *(uint64_t *)(*var_24 + 16UL) + 1UL;
                *var_38 = var_86;
                var_87 = *var_37;
                var_88 = var_87;
                var_89 = var_86;
                if (var_87 == 0UL) {
                    *var_37 = 1UL;
                    var_88 = 1UL;
                    var_89 = *var_38;
                }
                if (var_89 == 0UL) {
                    var_91 = *var_21;
                    var_92 = local_sp_1 + (-16L);
                    *(uint64_t *)var_92 = 4225188UL;
                    var_93 = indirect_placeholder_77(r93_2, r84_2, 0UL, var_91, 4353072UL, 0UL, 0UL);
                    r10_3 = var_93.field_0;
                    r93_3 = var_93.field_1;
                    r84_3 = var_93.field_2;
                    rcx_2 = var_93.field_3;
                    local_sp_2 = var_92;
                } else {
                    var_90 = helper_cc_compute_c_wrapper(var_88 - var_89, var_89, var_3, 17U);
                    if (var_90 == 0UL) {
                        var_91 = *var_21;
                        var_92 = local_sp_1 + (-16L);
                        *(uint64_t *)var_92 = 4225188UL;
                        var_93 = indirect_placeholder_77(r93_2, r84_2, 0UL, var_91, 4353072UL, 0UL, 0UL);
                        r10_3 = var_93.field_0;
                        r93_3 = var_93.field_1;
                        r84_3 = var_93.field_2;
                        rcx_2 = var_93.field_3;
                        local_sp_2 = var_92;
                    }
                }
            }
            var_94 = *var_15;
            r93_4 = r93_3;
            r84_4 = r84_3;
            rcx_3 = rcx_2;
            local_sp_4 = local_sp_2;
            r10_4 = r10_3;
            if (var_94 != 0UL & var_94 == *(uint64_t *)(*var_24 + 32UL)) {
                *var_15 = 0UL;
            }
            var_95 = *var_16;
            if (var_95 != 0UL & var_95 == *(uint64_t *)(*var_24 + 40UL)) {
                *var_16 = 0UL;
            }
            *var_39 = ((*var_39 & (*(unsigned char *)(*var_24 + 48UL) ^ '\x01')) != '\x00');
            *var_40 = ((*var_40 & (*(unsigned char *)(*var_24 + 49UL) ^ '\x01')) != '\x00');
            *var_41 = ((*var_41 & (*(unsigned char *)(*var_24 + 54UL) ^ '\x01')) != '\x00');
            *var_42 = ((*var_42 & (*(unsigned char *)(*var_24 + 50UL) ^ '\x01')) != '\x00');
            *var_43 = ((*var_43 & (*(unsigned char *)(*var_24 + 52UL) ^ '\x01')) != '\x00');
            *var_44 = ((*var_44 & (*(unsigned char *)(*var_24 + 53UL) ^ '\x01')) != '\x00');
            *var_45 = ((*var_45 & (*(unsigned char *)(*var_24 + 51UL) ^ '\x01')) != '\x00');
            *var_46 = ((*var_46 & (*(unsigned char *)(*var_24 + 56UL) ^ '\x01')) != '\x00');
            *var_47 = ((*var_47 & (*(unsigned char *)(*var_24 + 55UL) ^ '\x01')) != '\x00');
            *var_24 = *(uint64_t *)(*var_24 + 64UL);
            *var_21 = (*var_21 + 1UL);
            var_48 = *var_24;
            r10_0 = r10_4;
            r93_0 = r93_4;
            r84_0 = r84_4;
            rcx_0 = rcx_3;
            r10_5 = r10_4;
            r93_5 = r93_4;
            r84_5 = r84_4;
            rcx_4 = rcx_3;
            local_sp_5 = local_sp_4;
        }
    var_96 = local_sp_4 + (-8L);
    *(uint64_t *)var_96 = 4225596UL;
    var_97 = indirect_placeholder_4();
    local_sp_0 = var_96;
    if ((uint64_t)(unsigned char)var_97 != 1UL) {
        var_99 = *var_47;
        var_100 = (unsigned char *)(var_0 + (-57L));
        *var_100 = var_99;
        if (*(unsigned char *)4389633UL != '\x01' & *(unsigned char *)4389634UL == '\x01') {
            *var_47 = (unsigned char)'\x00';
        }
        var_101 = var_0 + (-248L);
        *(uint64_t *)(local_sp_4 + (-16L)) = 4225721UL;
        indirect_placeholder_1(var_11, var_101);
        *(uint64_t *)(local_sp_4 + (-24L)) = 4225736UL;
        indirect_placeholder();
        *(uint64_t *)(local_sp_4 + (-32L)) = 4225744UL;
        var_102 = indirect_placeholder_2(var_101);
        storemerge = (var_102 == 1UL) ? 4353117UL : 4353141UL;
        var_103 = local_sp_4 + (-40L);
        *(uint64_t *)var_103 = 4225792UL;
        var_104 = indirect_placeholder_75(r93_4, r84_4, 0UL, var_101, storemerge, 0UL, 0UL);
        var_105 = var_104.field_0;
        var_106 = var_104.field_1;
        var_107 = var_104.field_2;
        var_108 = var_104.field_3;
        var_109 = *var_100;
        *var_47 = var_109;
        var_110 = var_109;
        r10_0 = var_105;
        r93_0 = var_106;
        r84_0 = var_107;
        rcx_0 = var_108;
        local_sp_0 = var_103;
        r10_1 = r10_0;
        r93_1 = r93_0;
        r84_1 = r84_0;
        if (var_110 != '\x00' & *(unsigned char *)4389633UL != '\x01' & *(unsigned char *)4389634UL != '\x01' & *(uint64_t *)4389640UL == 0UL) {
            *(uint64_t *)(local_sp_0 + (-8L)) = 4225872UL;
            var_111 = indirect_placeholder_76(r93_0, r84_0, 0UL, rcx_0, 4353168UL, 0UL, 0UL);
            r10_1 = var_111.field_0;
            r93_1 = var_111.field_1;
            r84_1 = var_111.field_2;
        }
        mrv.field_0 = r10_1;
        mrv1 = mrv;
        mrv1.field_1 = r93_1;
        mrv2 = mrv1;
        mrv2.field_2 = r84_1;
        return mrv2;
    }
    var_98 = *var_47;
    var_99 = var_98;
    if (var_98 != '\x00') {
        r10_1 = r10_0;
        r93_1 = r93_0;
        r84_1 = r84_0;
        if (var_110 != '\x00' & *(unsigned char *)4389633UL != '\x01' & *(unsigned char *)4389634UL != '\x01' & *(uint64_t *)4389640UL == 0UL) {
            *(uint64_t *)(local_sp_0 + (-8L)) = 4225872UL;
            var_111 = indirect_placeholder_76(r93_0, r84_0, 0UL, rcx_0, 4353168UL, 0UL, 0UL);
            r10_1 = var_111.field_0;
            r93_1 = var_111.field_1;
            r84_1 = var_111.field_2;
        }
        mrv.field_0 = r10_1;
        mrv1 = mrv;
        mrv1.field_1 = r93_1;
        mrv2 = mrv1;
        mrv2.field_2 = r84_1;
        return mrv2;
    }
    var_110 = var_98;
    if (*(unsigned char *)4389633UL != '\x00') {
        if (*(unsigned char *)4389634UL != '\x00') {
            r10_1 = r10_0;
            r93_1 = r93_0;
            r84_1 = r84_0;
            if (var_110 != '\x00' & *(unsigned char *)4389633UL != '\x01' & *(unsigned char *)4389634UL != '\x01' & *(uint64_t *)4389640UL == 0UL) {
                *(uint64_t *)(local_sp_0 + (-8L)) = 4225872UL;
                var_111 = indirect_placeholder_76(r93_0, r84_0, 0UL, rcx_0, 4353168UL, 0UL, 0UL);
                r10_1 = var_111.field_0;
                r93_1 = var_111.field_1;
                r84_1 = var_111.field_2;
            }
            mrv.field_0 = r10_1;
            mrv1 = mrv;
            mrv1.field_1 = r93_1;
            mrv2 = mrv1;
            mrv2.field_2 = r84_1;
            return mrv2;
        }
    }
    if (*(uint64_t *)4389640UL == 0UL) {
        return;
    }
    r10_1 = r10_0;
    r93_1 = r93_0;
    r84_1 = r84_0;
    if (var_110 != '\x00' & *(unsigned char *)4389633UL != '\x01' & *(unsigned char *)4389634UL != '\x01' & *(uint64_t *)4389640UL == 0UL) {
        *(uint64_t *)(local_sp_0 + (-8L)) = 4225872UL;
        var_111 = indirect_placeholder_76(r93_0, r84_0, 0UL, rcx_0, 4353168UL, 0UL, 0UL);
        r10_1 = var_111.field_0;
        r93_1 = var_111.field_1;
        r84_1 = var_111.field_2;
    }
    mrv.field_0 = r10_1;
    mrv1 = mrv;
    mrv1.field_1 = r93_1;
    mrv2 = mrv1;
    mrv2.field_2 = r84_1;
    return mrv2;
}
