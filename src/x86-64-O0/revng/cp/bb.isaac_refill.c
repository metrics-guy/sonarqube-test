typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder(uint64_t param_0);
void bb_isaac_refill(uint64_t rsi, uint64_t rdi) {
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t var_100;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t var_107;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_118;
    uint64_t var_119;
    uint64_t var_120;
    uint64_t var_121;
    uint64_t var_122;
    uint64_t var_123;
    uint64_t var_124;
    uint64_t var_125;
    uint64_t var_126;
    uint64_t var_127;
    uint64_t var_128;
    uint64_t var_129;
    uint64_t var_130;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_108;
    uint64_t var_109;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t var_112;
    uint64_t var_113;
    uint64_t var_114;
    uint64_t var_115;
    uint64_t var_116;
    uint64_t var_117;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t *var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t *var_18;
    uint64_t *var_19;
    uint64_t *var_20;
    uint64_t *var_21;
    uint64_t *var_22;
    uint64_t *var_23;
    uint64_t *var_24;
    uint64_t *var_25;
    uint64_t *var_26;
    uint64_t local_sp_1;
    uint64_t *var_76;
    uint64_t *var_77;
    uint64_t *var_78;
    uint64_t *var_79;
    uint64_t *var_80;
    uint64_t *var_81;
    uint64_t *var_82;
    uint64_t *var_83;
    uint64_t local_sp_0;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t **var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t **var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_3 = var_0 + (-184L);
    var_4 = (uint64_t *)(var_0 + (-176L));
    *var_4 = rdi;
    var_5 = (uint64_t *)var_3;
    *var_5 = rsi;
    var_6 = *(uint64_t *)(*var_4 + 2048UL);
    var_7 = (uint64_t *)(var_0 + (-16L));
    *var_7 = var_6;
    var_8 = *var_4;
    var_9 = *(uint64_t *)(var_8 + 2056UL);
    var_10 = (uint64_t *)(var_8 + 2064UL);
    *var_10 = (*var_10 + 1UL);
    var_11 = *(uint64_t *)(*var_4 + 2064UL) + var_9;
    var_12 = (uint64_t *)(var_0 + (-24L));
    *var_12 = var_11;
    var_13 = *var_4;
    var_14 = var_0 + (-32L);
    var_15 = (uint64_t *)var_14;
    *var_15 = var_13;
    var_16 = *var_5;
    var_17 = var_0 + (-40L);
    var_18 = (uint64_t *)var_17;
    *var_18 = var_16;
    var_19 = (uint64_t *)(var_0 + (-48L));
    var_20 = (uint64_t *)(var_0 + (-56L));
    var_21 = (uint64_t *)(var_0 + (-64L));
    var_22 = (uint64_t *)(var_0 + (-72L));
    var_23 = (uint64_t *)(var_0 + (-80L));
    var_24 = (uint64_t *)(var_0 + (-88L));
    var_25 = (uint64_t *)(var_0 + (-96L));
    var_26 = (uint64_t *)(var_0 + (-104L));
    local_sp_1 = var_3;
    var_27 = *(uint64_t *)(*var_15 + 1024UL);
    var_28 = *var_7;
    *var_7 = (var_27 + (((var_28 << 21UL) ^ var_28) ^ (-1L)));
    var_29 = (uint64_t **)var_14;
    var_30 = **var_29;
    *var_19 = var_30;
    var_31 = *var_4;
    *(uint64_t *)(local_sp_1 + (-8L)) = 4294117UL;
    var_32 = indirect_placeholder_3(var_30, var_31);
    var_33 = *var_12 + (*var_7 + var_32);
    *var_20 = var_33;
    **var_29 = var_33;
    var_34 = *var_20 >> 8UL;
    var_35 = *var_4;
    *(uint64_t *)(local_sp_1 + (-16L)) = 4294175UL;
    var_36 = indirect_placeholder_3(var_34, var_35);
    var_37 = var_36 + *var_19;
    *(uint64_t *)(local_sp_1 + (-24L)) = 4294190UL;
    var_38 = indirect_placeholder(var_37);
    *var_12 = var_38;
    var_39 = (uint64_t **)var_17;
    **var_39 = var_38;
    var_40 = *var_7;
    *(uint64_t *)(local_sp_1 + (-32L)) = 4294217UL;
    var_41 = indirect_placeholder(var_40);
    *var_7 = (*(uint64_t *)(*var_15 + 1032UL) + ((var_41 >> 5UL) ^ *var_7));
    var_42 = *(uint64_t *)(*var_15 + 8UL);
    *var_21 = var_42;
    var_43 = *var_4;
    *(uint64_t *)(local_sp_1 + (-40L)) = 4294282UL;
    var_44 = indirect_placeholder_3(var_42, var_43);
    var_45 = *var_12 + (*var_7 + var_44);
    *var_22 = var_45;
    *(uint64_t *)(*var_15 + 8UL) = var_45;
    var_46 = *var_22 >> 8UL;
    var_47 = *var_4;
    *(uint64_t *)(local_sp_1 + (-48L)) = 4294344UL;
    var_48 = indirect_placeholder_3(var_46, var_47);
    var_49 = var_48 + *var_21;
    *(uint64_t *)(local_sp_1 + (-56L)) = 4294359UL;
    var_50 = indirect_placeholder(var_49);
    *var_12 = var_50;
    *(uint64_t *)(*var_18 + 8UL) = var_50;
    var_51 = *var_7;
    *var_7 = (*(uint64_t *)(*var_15 + 1040UL) + ((var_51 << 12UL) ^ var_51));
    var_52 = *(uint64_t *)(*var_15 + 16UL);
    *var_23 = var_52;
    var_53 = *var_4;
    *(uint64_t *)(local_sp_1 + (-64L)) = 4294447UL;
    var_54 = indirect_placeholder_3(var_52, var_53);
    var_55 = *var_12 + (*var_7 + var_54);
    *var_24 = var_55;
    *(uint64_t *)(*var_15 + 16UL) = var_55;
    var_56 = *var_24 >> 8UL;
    var_57 = *var_4;
    *(uint64_t *)(local_sp_1 + (-72L)) = 4294509UL;
    var_58 = indirect_placeholder_3(var_56, var_57);
    var_59 = var_58 + *var_23;
    *(uint64_t *)(local_sp_1 + (-80L)) = 4294524UL;
    var_60 = indirect_placeholder(var_59);
    *var_12 = var_60;
    *(uint64_t *)(*var_18 + 16UL) = var_60;
    var_61 = *var_7;
    *(uint64_t *)(local_sp_1 + (-88L)) = 4294555UL;
    var_62 = indirect_placeholder(var_61);
    *var_7 = (*(uint64_t *)(*var_15 + 1048UL) + ((var_62 >> 33UL) ^ *var_7));
    var_63 = *(uint64_t *)(*var_15 + 24UL);
    *var_25 = var_63;
    var_64 = *var_4;
    *(uint64_t *)(local_sp_1 + (-96L)) = 4294620UL;
    var_65 = indirect_placeholder_3(var_63, var_64);
    var_66 = *var_12 + (*var_7 + var_65);
    *var_26 = var_66;
    *(uint64_t *)(*var_15 + 24UL) = var_66;
    var_67 = *var_26 >> 8UL;
    var_68 = *var_4;
    *(uint64_t *)(local_sp_1 + (-104L)) = 4294682UL;
    var_69 = indirect_placeholder_3(var_67, var_68);
    var_70 = var_69 + *var_25;
    var_71 = local_sp_1 + (-112L);
    *(uint64_t *)var_71 = 4294697UL;
    var_72 = indirect_placeholder(var_70);
    *var_12 = var_72;
    *(uint64_t *)(*var_18 + 24UL) = var_72;
    *var_18 = (*var_18 + 32UL);
    var_73 = *var_15 + 32UL;
    *var_15 = var_73;
    var_74 = *var_4 + 1024UL;
    var_75 = helper_cc_compute_c_wrapper(var_73 - var_74, var_74, var_2, 17U);
    local_sp_0 = var_71;
    local_sp_1 = var_71;
    do {
        var_27 = *(uint64_t *)(*var_15 + 1024UL);
        var_28 = *var_7;
        *var_7 = (var_27 + (((var_28 << 21UL) ^ var_28) ^ (-1L)));
        var_29 = (uint64_t **)var_14;
        var_30 = **var_29;
        *var_19 = var_30;
        var_31 = *var_4;
        *(uint64_t *)(local_sp_1 + (-8L)) = 4294117UL;
        var_32 = indirect_placeholder_3(var_30, var_31);
        var_33 = *var_12 + (*var_7 + var_32);
        *var_20 = var_33;
        **var_29 = var_33;
        var_34 = *var_20 >> 8UL;
        var_35 = *var_4;
        *(uint64_t *)(local_sp_1 + (-16L)) = 4294175UL;
        var_36 = indirect_placeholder_3(var_34, var_35);
        var_37 = var_36 + *var_19;
        *(uint64_t *)(local_sp_1 + (-24L)) = 4294190UL;
        var_38 = indirect_placeholder(var_37);
        *var_12 = var_38;
        var_39 = (uint64_t **)var_17;
        **var_39 = var_38;
        var_40 = *var_7;
        *(uint64_t *)(local_sp_1 + (-32L)) = 4294217UL;
        var_41 = indirect_placeholder(var_40);
        *var_7 = (*(uint64_t *)(*var_15 + 1032UL) + ((var_41 >> 5UL) ^ *var_7));
        var_42 = *(uint64_t *)(*var_15 + 8UL);
        *var_21 = var_42;
        var_43 = *var_4;
        *(uint64_t *)(local_sp_1 + (-40L)) = 4294282UL;
        var_44 = indirect_placeholder_3(var_42, var_43);
        var_45 = *var_12 + (*var_7 + var_44);
        *var_22 = var_45;
        *(uint64_t *)(*var_15 + 8UL) = var_45;
        var_46 = *var_22 >> 8UL;
        var_47 = *var_4;
        *(uint64_t *)(local_sp_1 + (-48L)) = 4294344UL;
        var_48 = indirect_placeholder_3(var_46, var_47);
        var_49 = var_48 + *var_21;
        *(uint64_t *)(local_sp_1 + (-56L)) = 4294359UL;
        var_50 = indirect_placeholder(var_49);
        *var_12 = var_50;
        *(uint64_t *)(*var_18 + 8UL) = var_50;
        var_51 = *var_7;
        *var_7 = (*(uint64_t *)(*var_15 + 1040UL) + ((var_51 << 12UL) ^ var_51));
        var_52 = *(uint64_t *)(*var_15 + 16UL);
        *var_23 = var_52;
        var_53 = *var_4;
        *(uint64_t *)(local_sp_1 + (-64L)) = 4294447UL;
        var_54 = indirect_placeholder_3(var_52, var_53);
        var_55 = *var_12 + (*var_7 + var_54);
        *var_24 = var_55;
        *(uint64_t *)(*var_15 + 16UL) = var_55;
        var_56 = *var_24 >> 8UL;
        var_57 = *var_4;
        *(uint64_t *)(local_sp_1 + (-72L)) = 4294509UL;
        var_58 = indirect_placeholder_3(var_56, var_57);
        var_59 = var_58 + *var_23;
        *(uint64_t *)(local_sp_1 + (-80L)) = 4294524UL;
        var_60 = indirect_placeholder(var_59);
        *var_12 = var_60;
        *(uint64_t *)(*var_18 + 16UL) = var_60;
        var_61 = *var_7;
        *(uint64_t *)(local_sp_1 + (-88L)) = 4294555UL;
        var_62 = indirect_placeholder(var_61);
        *var_7 = (*(uint64_t *)(*var_15 + 1048UL) + ((var_62 >> 33UL) ^ *var_7));
        var_63 = *(uint64_t *)(*var_15 + 24UL);
        *var_25 = var_63;
        var_64 = *var_4;
        *(uint64_t *)(local_sp_1 + (-96L)) = 4294620UL;
        var_65 = indirect_placeholder_3(var_63, var_64);
        var_66 = *var_12 + (*var_7 + var_65);
        *var_26 = var_66;
        *(uint64_t *)(*var_15 + 24UL) = var_66;
        var_67 = *var_26 >> 8UL;
        var_68 = *var_4;
        *(uint64_t *)(local_sp_1 + (-104L)) = 4294682UL;
        var_69 = indirect_placeholder_3(var_67, var_68);
        var_70 = var_69 + *var_25;
        var_71 = local_sp_1 + (-112L);
        *(uint64_t *)var_71 = 4294697UL;
        var_72 = indirect_placeholder(var_70);
        *var_12 = var_72;
        *(uint64_t *)(*var_18 + 24UL) = var_72;
        *var_18 = (*var_18 + 32UL);
        var_73 = *var_15 + 32UL;
        *var_15 = var_73;
        var_74 = *var_4 + 1024UL;
        var_75 = helper_cc_compute_c_wrapper(var_73 - var_74, var_74, var_2, 17U);
        local_sp_0 = var_71;
        local_sp_1 = var_71;
    } while (var_75 != 0UL);
    var_76 = (uint64_t *)(var_0 + (-112L));
    var_77 = (uint64_t *)(var_0 + (-120L));
    var_78 = (uint64_t *)(var_0 + (-128L));
    var_79 = (uint64_t *)(var_0 + (-136L));
    var_80 = (uint64_t *)(var_0 + (-144L));
    var_81 = (uint64_t *)(var_0 + (-152L));
    var_82 = (uint64_t *)(var_0 + (-160L));
    var_83 = (uint64_t *)(var_0 + (-168L));
    var_84 = *(uint64_t *)(*var_15 + (-1024L));
    var_85 = *var_7;
    *var_7 = (var_84 + (((var_85 << 21UL) ^ var_85) ^ (-1L)));
    var_86 = **var_29;
    *var_76 = var_86;
    var_87 = *var_4;
    *(uint64_t *)(local_sp_0 + (-8L)) = 4294821UL;
    var_88 = indirect_placeholder_3(var_86, var_87);
    var_89 = *var_12 + (*var_7 + var_88);
    *var_77 = var_89;
    **var_29 = var_89;
    var_90 = *var_77 >> 8UL;
    var_91 = *var_4;
    *(uint64_t *)(local_sp_0 + (-16L)) = 4294879UL;
    var_92 = indirect_placeholder_3(var_90, var_91);
    var_93 = var_92 + *var_76;
    *(uint64_t *)(local_sp_0 + (-24L)) = 4294894UL;
    var_94 = indirect_placeholder(var_93);
    *var_12 = var_94;
    **var_39 = var_94;
    var_95 = *var_7;
    *(uint64_t *)(local_sp_0 + (-32L)) = 4294921UL;
    var_96 = indirect_placeholder(var_95);
    *var_7 = (*(uint64_t *)(*var_15 + (-1016L)) + ((var_96 >> 5UL) ^ *var_7));
    var_97 = *(uint64_t *)(*var_15 + 8UL);
    *var_78 = var_97;
    var_98 = *var_4;
    *(uint64_t *)(local_sp_0 + (-40L)) = 4294986UL;
    var_99 = indirect_placeholder_3(var_97, var_98);
    var_100 = *var_12 + (*var_7 + var_99);
    *var_79 = var_100;
    *(uint64_t *)(*var_15 + 8UL) = var_100;
    var_101 = *var_79 >> 8UL;
    var_102 = *var_4;
    *(uint64_t *)(local_sp_0 + (-48L)) = 4295048UL;
    var_103 = indirect_placeholder_3(var_101, var_102);
    var_104 = var_103 + *var_78;
    *(uint64_t *)(local_sp_0 + (-56L)) = 4295063UL;
    var_105 = indirect_placeholder(var_104);
    *var_12 = var_105;
    *(uint64_t *)(*var_18 + 8UL) = var_105;
    var_106 = *var_7;
    *var_7 = (*(uint64_t *)(*var_15 + (-1008L)) + ((var_106 << 12UL) ^ var_106));
    var_107 = *(uint64_t *)(*var_15 + 16UL);
    *var_80 = var_107;
    var_108 = *var_4;
    *(uint64_t *)(local_sp_0 + (-64L)) = 4295157UL;
    var_109 = indirect_placeholder_3(var_107, var_108);
    var_110 = *var_12 + (*var_7 + var_109);
    *var_81 = var_110;
    *(uint64_t *)(*var_15 + 16UL) = var_110;
    var_111 = *var_81 >> 8UL;
    var_112 = *var_4;
    *(uint64_t *)(local_sp_0 + (-72L)) = 4295228UL;
    var_113 = indirect_placeholder_3(var_111, var_112);
    var_114 = var_113 + *var_80;
    *(uint64_t *)(local_sp_0 + (-80L)) = 4295246UL;
    var_115 = indirect_placeholder(var_114);
    *var_12 = var_115;
    *(uint64_t *)(*var_18 + 16UL) = var_115;
    var_116 = *var_7;
    *(uint64_t *)(local_sp_0 + (-88L)) = 4295277UL;
    var_117 = indirect_placeholder(var_116);
    *var_7 = (*(uint64_t *)(*var_15 + (-1000L)) + ((var_117 >> 33UL) ^ *var_7));
    var_118 = *(uint64_t *)(*var_15 + 24UL);
    *var_82 = var_118;
    var_119 = *var_4;
    *(uint64_t *)(local_sp_0 + (-96L)) = 4295348UL;
    var_120 = indirect_placeholder_3(var_118, var_119);
    var_121 = *var_12 + (*var_7 + var_120);
    *var_83 = var_121;
    *(uint64_t *)(*var_15 + 24UL) = var_121;
    var_122 = *var_83 >> 8UL;
    var_123 = *var_4;
    *(uint64_t *)(local_sp_0 + (-104L)) = 4295419UL;
    var_124 = indirect_placeholder_3(var_122, var_123);
    var_125 = var_124 + *var_82;
    var_126 = local_sp_0 + (-112L);
    *(uint64_t *)var_126 = 4295437UL;
    var_127 = indirect_placeholder(var_125);
    *var_12 = var_127;
    *(uint64_t *)(*var_18 + 24UL) = var_127;
    *var_18 = (*var_18 + 32UL);
    var_128 = *var_15 + 32UL;
    *var_15 = var_128;
    var_129 = *var_4 + 2048UL;
    var_130 = helper_cc_compute_c_wrapper(var_128 - var_129, var_129, var_2, 17U);
    local_sp_0 = var_126;
    do {
        var_84 = *(uint64_t *)(*var_15 + (-1024L));
        var_85 = *var_7;
        *var_7 = (var_84 + (((var_85 << 21UL) ^ var_85) ^ (-1L)));
        var_86 = **var_29;
        *var_76 = var_86;
        var_87 = *var_4;
        *(uint64_t *)(local_sp_0 + (-8L)) = 4294821UL;
        var_88 = indirect_placeholder_3(var_86, var_87);
        var_89 = *var_12 + (*var_7 + var_88);
        *var_77 = var_89;
        **var_29 = var_89;
        var_90 = *var_77 >> 8UL;
        var_91 = *var_4;
        *(uint64_t *)(local_sp_0 + (-16L)) = 4294879UL;
        var_92 = indirect_placeholder_3(var_90, var_91);
        var_93 = var_92 + *var_76;
        *(uint64_t *)(local_sp_0 + (-24L)) = 4294894UL;
        var_94 = indirect_placeholder(var_93);
        *var_12 = var_94;
        **var_39 = var_94;
        var_95 = *var_7;
        *(uint64_t *)(local_sp_0 + (-32L)) = 4294921UL;
        var_96 = indirect_placeholder(var_95);
        *var_7 = (*(uint64_t *)(*var_15 + (-1016L)) + ((var_96 >> 5UL) ^ *var_7));
        var_97 = *(uint64_t *)(*var_15 + 8UL);
        *var_78 = var_97;
        var_98 = *var_4;
        *(uint64_t *)(local_sp_0 + (-40L)) = 4294986UL;
        var_99 = indirect_placeholder_3(var_97, var_98);
        var_100 = *var_12 + (*var_7 + var_99);
        *var_79 = var_100;
        *(uint64_t *)(*var_15 + 8UL) = var_100;
        var_101 = *var_79 >> 8UL;
        var_102 = *var_4;
        *(uint64_t *)(local_sp_0 + (-48L)) = 4295048UL;
        var_103 = indirect_placeholder_3(var_101, var_102);
        var_104 = var_103 + *var_78;
        *(uint64_t *)(local_sp_0 + (-56L)) = 4295063UL;
        var_105 = indirect_placeholder(var_104);
        *var_12 = var_105;
        *(uint64_t *)(*var_18 + 8UL) = var_105;
        var_106 = *var_7;
        *var_7 = (*(uint64_t *)(*var_15 + (-1008L)) + ((var_106 << 12UL) ^ var_106));
        var_107 = *(uint64_t *)(*var_15 + 16UL);
        *var_80 = var_107;
        var_108 = *var_4;
        *(uint64_t *)(local_sp_0 + (-64L)) = 4295157UL;
        var_109 = indirect_placeholder_3(var_107, var_108);
        var_110 = *var_12 + (*var_7 + var_109);
        *var_81 = var_110;
        *(uint64_t *)(*var_15 + 16UL) = var_110;
        var_111 = *var_81 >> 8UL;
        var_112 = *var_4;
        *(uint64_t *)(local_sp_0 + (-72L)) = 4295228UL;
        var_113 = indirect_placeholder_3(var_111, var_112);
        var_114 = var_113 + *var_80;
        *(uint64_t *)(local_sp_0 + (-80L)) = 4295246UL;
        var_115 = indirect_placeholder(var_114);
        *var_12 = var_115;
        *(uint64_t *)(*var_18 + 16UL) = var_115;
        var_116 = *var_7;
        *(uint64_t *)(local_sp_0 + (-88L)) = 4295277UL;
        var_117 = indirect_placeholder(var_116);
        *var_7 = (*(uint64_t *)(*var_15 + (-1000L)) + ((var_117 >> 33UL) ^ *var_7));
        var_118 = *(uint64_t *)(*var_15 + 24UL);
        *var_82 = var_118;
        var_119 = *var_4;
        *(uint64_t *)(local_sp_0 + (-96L)) = 4295348UL;
        var_120 = indirect_placeholder_3(var_118, var_119);
        var_121 = *var_12 + (*var_7 + var_120);
        *var_83 = var_121;
        *(uint64_t *)(*var_15 + 24UL) = var_121;
        var_122 = *var_83 >> 8UL;
        var_123 = *var_4;
        *(uint64_t *)(local_sp_0 + (-104L)) = 4295419UL;
        var_124 = indirect_placeholder_3(var_122, var_123);
        var_125 = var_124 + *var_82;
        var_126 = local_sp_0 + (-112L);
        *(uint64_t *)var_126 = 4295437UL;
        var_127 = indirect_placeholder(var_125);
        *var_12 = var_127;
        *(uint64_t *)(*var_18 + 24UL) = var_127;
        *var_18 = (*var_18 + 32UL);
        var_128 = *var_15 + 32UL;
        *var_15 = var_128;
        var_129 = *var_4 + 2048UL;
        var_130 = helper_cc_compute_c_wrapper(var_128 - var_129, var_129, var_2, 17U);
        local_sp_0 = var_126;
    } while (var_130 != 0UL);
    *(uint64_t *)(*var_4 + 2048UL) = *var_7;
    *(uint64_t *)(*var_4 + 2056UL) = *var_12;
    return;
}
