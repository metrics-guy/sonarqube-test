typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_1(void);
uint64_t bb_careadlinkat(uint64_t rcx, uint64_t rdx, uint64_t rsi, uint64_t rdi, uint64_t r9, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint32_t *var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    uint64_t var_19;
    bool var_20;
    uint64_t var_21;
    uint64_t var_25;
    uint64_t *var_26;
    uint64_t local_sp_2;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t rax_1;
    uint64_t rax_0;
    uint64_t local_sp_1;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_35;
    uint64_t var_32;
    bool var_33;
    uint64_t var_34;
    uint64_t var_36;
    uint64_t local_sp_4;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t local_sp_3;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t *var_13;
    uint64_t *var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_3 = var_0 + (-1144L);
    var_4 = (uint32_t *)(var_0 + (-1100L));
    *var_4 = (uint32_t)rdi;
    *(uint64_t *)(var_0 + (-1112L)) = rsi;
    var_5 = (uint64_t *)(var_0 + (-1120L));
    *var_5 = rdx;
    var_6 = (uint64_t *)(var_0 + (-1128L));
    *var_6 = rcx;
    var_7 = (uint64_t *)(var_0 + (-1136L));
    *var_7 = r8;
    *(uint64_t *)var_3 = r9;
    var_8 = (uint64_t *)(var_0 + (-32L));
    *var_8 = 9223372036854775808UL;
    rax_0 = 0UL;
    rax_1 = 0UL;
    local_sp_4 = var_3;
    if (*var_7 == 0UL) {
        *var_7 = 4362432UL;
    }
    if (*var_6 == 0UL) {
        *var_5 = (var_0 + (-1096L));
        *var_6 = 1024UL;
    }
    var_9 = *var_5;
    var_10 = (uint64_t *)(var_0 + (-16L));
    *var_10 = var_9;
    var_11 = *var_6;
    var_12 = (uint64_t *)(var_0 + (-24L));
    *var_12 = var_11;
    var_13 = (uint64_t *)(var_0 + (-40L));
    var_14 = (uint64_t *)(var_0 + (-56L));
    while (1U)
        {
            var_15 = (uint64_t)*var_4;
            var_16 = local_sp_4 + (-8L);
            *(uint64_t *)var_16 = 4289127UL;
            indirect_placeholder_1();
            *var_13 = var_15;
            *var_14 = var_15;
            var_17 = *var_12;
            var_18 = helper_cc_compute_c_wrapper(var_15 - var_17, var_17, var_2, 17U);
            local_sp_1 = var_16;
            if (var_18 == 0UL) {
                if (*var_10 == *var_5) {
                    var_27 = var_16 + (-8L);
                    *(uint64_t *)var_27 = 4289466UL;
                    indirect_placeholder_1();
                    local_sp_1 = var_27;
                }
                var_28 = *var_8;
                var_29 = var_28 >> 1UL;
                var_30 = *var_12;
                local_sp_2 = local_sp_1;
                if (var_30 > var_29) {
                    var_31 = var_30 << 1UL;
                    *var_12 = var_31;
                    var_35 = var_31;
                } else {
                    var_32 = helper_cc_compute_c_wrapper(var_30 - var_28, var_28, var_2, 17U);
                    var_33 = (var_32 == 0UL);
                    var_34 = *var_8;
                    var_35 = var_34;
                    if (!var_33) {
                        if (var_34 != 18446744073709551615UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        *(uint64_t *)(local_sp_1 + (-8L)) = 4289517UL;
                        indirect_placeholder_1();
                        *(uint32_t *)var_30 = 36U;
                        loop_state_var = 0U;
                        break;
                    }
                    *var_12 = var_34;
                }
                var_36 = local_sp_1 + (-8L);
                *(uint64_t *)var_36 = 4289549UL;
                indirect_placeholder_1();
                *var_10 = var_35;
                local_sp_2 = var_36;
                local_sp_4 = var_36;
                if (var_35 == 0UL) {
                    continue;
                }
                loop_state_var = 1U;
                break;
            }
            var_19 = *var_14;
            *var_14 = (var_19 + 1UL);
            *(unsigned char *)(var_19 + *var_10) = (unsigned char)'\x00';
            var_20 = (*var_10 == (var_0 + (-1096L)));
            var_21 = *var_14;
            if (var_20) {
                var_25 = var_16 + (-8L);
                *(uint64_t *)var_25 = 4289283UL;
                indirect_placeholder_1();
                var_26 = (uint64_t *)(var_0 + (-72L));
                *var_26 = var_21;
                *var_12 = *var_14;
                local_sp_2 = var_25;
                if (*var_26 != 0UL) {
                    loop_state_var = 1U;
                    break;
                }
                *(uint64_t *)(var_16 + (-16L)) = 4289329UL;
                indirect_placeholder_1();
                *var_10 = *var_26;
                loop_state_var = 2U;
                break;
            }
            var_22 = *var_12;
            var_23 = helper_cc_compute_c_wrapper(var_21 - var_22, var_22, var_2, 17U);
            if (var_23 != 0UL) {
                loop_state_var = 2U;
                break;
            }
            var_24 = *var_10;
            if (var_24 != *var_5) {
                loop_state_var = 2U;
                break;
            }
            if (*(uint64_t *)(*var_7 + 8UL) != 0UL) {
                loop_state_var = 2U;
                break;
            }
            *(uint64_t *)(var_16 + (-8L)) = 4289405UL;
            indirect_placeholder_1();
            *(uint64_t *)(var_0 + (-64L)) = var_24;
            if (var_24 != 0UL) {
                loop_state_var = 2U;
                break;
            }
            *var_10 = var_24;
            loop_state_var = 2U;
            break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            return rax_0;
        }
        break;
      case 2U:
        {
            rax_0 = *var_10;
        }
        break;
      case 1U:
        {
            local_sp_3 = local_sp_2;
            if (*(uint64_t *)(*var_7 + 24UL) != 0UL) {
                var_37 = *var_12;
                var_38 = local_sp_2 + (-8L);
                *(uint64_t *)var_38 = 4289606UL;
                indirect_placeholder_1();
                rax_1 = var_37;
                local_sp_3 = var_38;
            }
            *(uint64_t *)(local_sp_3 + (-8L)) = 4289611UL;
            indirect_placeholder_1();
            *(uint32_t *)rax_1 = 12U;
        }
        break;
    }
}
