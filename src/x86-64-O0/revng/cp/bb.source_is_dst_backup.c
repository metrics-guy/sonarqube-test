typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_57_ret_type;
struct indirect_placeholder_57_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder(uint64_t param_0);
extern struct indirect_placeholder_57_ret_type indirect_placeholder_57(uint64_t param_0);
uint64_t bb_source_is_dst_backup(uint64_t rdx, uint64_t rsi, uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    uint64_t var_7;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t *var_11;
    uint64_t var_12;
    uint64_t *var_13;
    uint64_t storemerge1;
    uint64_t var_16;
    uint64_t var_17;
    struct indirect_placeholder_57_ret_type var_18;
    uint64_t var_19;
    uint64_t *var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint32_t *var_23;
    uint64_t var_15;
    uint64_t var_14;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_3 = (uint64_t *)(var_0 + (-240L));
    *var_3 = rdi;
    var_4 = var_0 + (-248L);
    var_5 = (uint64_t *)var_4;
    *var_5 = rsi;
    var_6 = (uint64_t *)(var_0 + (-256L));
    *var_6 = rdx;
    var_7 = *var_3;
    *(uint64_t *)(var_0 + (-272L)) = 4229950UL;
    indirect_placeholder_1();
    var_8 = (uint64_t *)(var_0 + (-32L));
    *var_8 = var_7;
    var_9 = *var_6;
    *(uint64_t *)(var_0 + (-280L)) = 4229969UL;
    var_10 = indirect_placeholder(var_9);
    *(uint64_t *)(var_0 + (-40L)) = var_10;
    *(uint64_t *)(var_0 + (-288L)) = 4229985UL;
    indirect_placeholder_1();
    var_11 = (uint64_t *)(var_0 + (-48L));
    *var_11 = var_10;
    var_12 = *(uint64_t *)4385320UL;
    *(uint64_t *)(var_0 + (-296L)) = 4230004UL;
    indirect_placeholder_1();
    var_13 = (uint64_t *)(var_0 + (-56L));
    *var_13 = var_12;
    storemerge1 = 0UL;
    var_14 = *var_3;
    *(uint64_t *)(var_0 + (-304L)) = 4230051UL;
    indirect_placeholder_1();
    var_15 = *(uint64_t *)4385320UL;
    *(uint64_t *)(var_0 + (-312L)) = 4230087UL;
    indirect_placeholder_1();
    if (~(*var_8 != (var_12 + *var_11) & (uint64_t)(uint32_t)var_14 != 0UL & (uint64_t)(uint32_t)var_15 != 0UL)) {
        return storemerge1;
    }
    var_16 = *var_6;
    *(uint64_t *)(var_0 + (-320L)) = 4230116UL;
    indirect_placeholder_1();
    *(uint64_t *)(var_0 + (-64L)) = var_16;
    var_17 = (*var_13 + var_16) + 1UL;
    *(uint64_t *)(var_0 + (-328L)) = 4230143UL;
    var_18 = indirect_placeholder_57(var_17);
    var_19 = var_18.field_0;
    var_20 = (uint64_t *)(var_0 + (-72L));
    *var_20 = var_19;
    *(uint64_t *)(var_0 + (-336L)) = 4230180UL;
    indirect_placeholder_1();
    *(uint64_t *)(var_0 + (-344L)) = 4230191UL;
    indirect_placeholder_1();
    var_21 = *var_20;
    *(uint64_t *)(var_0 + (-352L)) = 4230213UL;
    var_22 = indirect_placeholder(var_21);
    var_23 = (uint32_t *)(var_0 + (-76L));
    *var_23 = (uint32_t)var_22;
    *(uint64_t *)(var_0 + (-360L)) = 4230228UL;
    indirect_placeholder_1();
    storemerge1 = 1UL;
    if (*var_23 == 0U) {
    } else {
        if (*(uint64_t *)(*var_5 + 8UL) == *(uint64_t *)(var_0 + (-224L))) {
        } else {
            if (**(uint64_t **)var_4 == *(uint64_t *)(var_0 + (-232L))) {
            }
        }
    }
    return storemerge1;
}
