typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_291_ret_type;
struct indirect_placeholder_292_ret_type;
struct indirect_placeholder_291_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_292_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder(uint64_t param_0);
extern struct indirect_placeholder_291_ret_type indirect_placeholder_291(uint64_t param_0);
extern struct indirect_placeholder_292_ret_type indirect_placeholder_292(uint64_t param_0);
uint64_t bb_fdutimens(uint64_t rdx, uint64_t rsi, uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t spec_select;
    uint32_t *var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    bool var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint32_t *var_11;
    uint32_t var_20;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t local_sp_0;
    uint64_t storemerge7;
    uint64_t local_sp_9;
    uint64_t var_52;
    uint32_t var_53;
    uint64_t local_sp_1;
    uint64_t var_54;
    uint64_t local_sp_8;
    uint64_t var_55;
    uint64_t var_44;
    uint32_t var_45;
    uint64_t local_sp_2;
    uint64_t var_46;
    uint32_t var_48;
    uint64_t var_47;
    uint64_t local_sp_3;
    uint64_t storemerge8;
    uint64_t var_27;
    uint64_t *var_34;
    uint64_t var_35;
    uint64_t var_36;
    struct indirect_placeholder_291_ret_type var_37;
    uint64_t var_38;
    uint64_t local_sp_4;
    uint64_t *var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    struct indirect_placeholder_292_ret_type var_32;
    uint64_t var_33;
    uint32_t var_39;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint32_t var_19;
    uint64_t local_sp_5;
    uint64_t rax_0;
    uint64_t rax_1;
    uint32_t var_21;
    bool var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t local_sp_6;
    uint64_t var_40;
    uint64_t var_41;
    uint32_t *var_42;
    uint32_t var_43;
    uint64_t local_sp_7;
    uint64_t var_49;
    uint64_t var_50;
    uint32_t *var_51;
    uint32_t var_56;
    uint64_t local_sp_10;
    uint32_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint32_t var_69;
    uint64_t var_70;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_3 = var_0 + (-328L);
    var_4 = (uint32_t *)(var_0 + (-300L));
    *var_4 = (uint32_t)rdi;
    var_5 = (uint64_t *)(var_0 + (-312L));
    *var_5 = rsi;
    var_6 = (uint64_t *)(var_0 + (-320L));
    *var_6 = rdx;
    var_7 = (rdx == 0UL);
    var_8 = var_0 + (-88L);
    spec_select = var_7 ? 0UL : var_8;
    var_9 = var_0 + (-96L);
    var_10 = (uint64_t *)var_9;
    *var_10 = spec_select;
    var_11 = (uint32_t *)(var_0 + (-28L));
    *var_11 = 0U;
    var_20 = 0U;
    local_sp_5 = var_3;
    rax_0 = 0UL;
    rax_1 = 4294967295UL;
    if (*var_10 == 0UL) {
        var_12 = *var_6;
        var_13 = *(uint64_t *)(var_12 + 8UL);
        *(uint64_t *)var_8 = *(uint64_t *)var_12;
        *(uint64_t *)(var_0 + (-80L)) = var_13;
        var_14 = *var_6;
        var_15 = *(uint64_t *)(var_14 + 24UL);
        *(uint64_t *)(var_0 + (-72L)) = *(uint64_t *)(var_14 + 16UL);
        *(uint64_t *)(var_0 + (-64L)) = var_15;
        var_16 = *var_10;
        var_17 = var_0 + (-336L);
        *(uint64_t *)var_17 = 4270280UL;
        var_18 = indirect_placeholder(var_16);
        var_19 = (uint32_t)var_18;
        *var_11 = var_19;
        var_20 = var_19;
        local_sp_5 = var_17;
        rax_0 = var_18;
    }
    local_sp_6 = local_sp_5;
    local_sp_8 = local_sp_5;
    if ((int)var_20 > (int)4294967295U) {
        return rax_1;
    }
    var_21 = *var_4;
    var_22 = ((int)var_21 > (int)4294967295U);
    rax_1 = 0UL;
    var_39 = var_21;
    if (!var_22) {
        if (*var_5 != 0UL) {
            *(uint64_t *)(local_sp_5 + (-8L)) = 4270323UL;
            indirect_placeholder_1();
            *(uint32_t *)rax_0 = 9U;
            return rax_1;
        }
    }
    if ((int)*(uint32_t *)4385712UL >= (int)0U) {
        if (var_20 != 2U) {
            if (var_22) {
                var_26 = local_sp_5 + (-8L);
                *(uint64_t *)var_26 = 4270427UL;
                indirect_placeholder_1();
                local_sp_3 = var_26;
                storemerge8 = (uint64_t)(var_21 & (-256)) | (var_21 != 0U);
            } else {
                var_23 = *var_5;
                var_24 = local_sp_5 + (-8L);
                *(uint64_t *)var_24 = 4270397UL;
                var_25 = indirect_placeholder(var_23);
                local_sp_3 = var_24;
                storemerge8 = (var_25 & (-256L)) | ((uint64_t)(uint32_t)var_25 != 0UL);
            }
            local_sp_4 = local_sp_3;
            if ((uint64_t)(unsigned char)storemerge8 == 0UL) {
                return rax_1;
            }
            var_27 = *var_10;
            if (*(uint64_t *)(var_27 + 8UL) == 1073741822UL) {
                var_34 = *(uint64_t **)var_9;
                var_35 = var_0 + (-248L);
                var_36 = local_sp_3 + (-8L);
                *(uint64_t *)var_36 = 4270481UL;
                var_37 = indirect_placeholder_291(var_35);
                var_38 = var_37.field_1;
                *var_34 = var_37.field_0;
                *(uint64_t *)((uint64_t)var_34 + 8UL) = var_38;
                local_sp_4 = var_36;
            } else {
                var_28 = (uint64_t *)(var_27 + 24UL);
                if (*var_28 == 1073741822UL) {
                    var_29 = var_27 + 16UL;
                    var_30 = var_0 + (-248L);
                    var_31 = local_sp_3 + (-8L);
                    *(uint64_t *)var_31 = 4270533UL;
                    var_32 = indirect_placeholder_292(var_30);
                    var_33 = var_32.field_1;
                    *(uint64_t *)var_29 = var_32.field_0;
                    *var_28 = var_33;
                    local_sp_4 = var_31;
                }
            }
            *var_11 = (*var_11 + 1U);
            var_39 = *var_4;
            local_sp_6 = local_sp_4;
        }
        var_48 = var_39;
        local_sp_7 = local_sp_6;
        if ((int)var_39 <= (int)4294967295U) {
            var_40 = *var_5;
            var_41 = local_sp_6 + (-8L);
            *(uint64_t *)var_41 = 4270582UL;
            indirect_placeholder_1();
            var_42 = (uint32_t *)(var_0 + (-52L));
            var_43 = (uint32_t)var_40;
            *var_42 = var_43;
            var_45 = var_43;
            local_sp_2 = var_41;
            if ((int)var_43 > (int)0U) {
                var_44 = local_sp_6 + (-16L);
                *(uint64_t *)var_44 = 4270596UL;
                indirect_placeholder_1();
                *(uint32_t *)var_40 = 38U;
                var_45 = *var_42;
                local_sp_2 = var_44;
            }
            if (var_45 != 0U) {
                *(uint32_t *)4385712UL = 1U;
                var_47 = (uint64_t)*var_42;
                rax_1 = var_47;
                return rax_1;
            }
            var_46 = local_sp_2 + (-8L);
            *(uint64_t *)var_46 = 4270613UL;
            indirect_placeholder_1();
            local_sp_7 = var_46;
            if (*(uint32_t *)var_40 != 38U) {
                *(uint32_t *)4385712UL = 1U;
                var_47 = (uint64_t)*var_42;
                rax_1 = var_47;
                return rax_1;
            }
            var_48 = *var_4;
        }
        var_53 = var_48;
        local_sp_8 = local_sp_7;
        if ((int)var_48 >= (int)0U) {
            var_49 = (uint64_t)var_48;
            var_50 = local_sp_7 + (-8L);
            *(uint64_t *)var_50 = 4270667UL;
            indirect_placeholder_1();
            var_51 = (uint32_t *)(var_0 + (-52L));
            *var_51 = var_48;
            local_sp_1 = var_50;
            if ((int)var_48 > (int)0U) {
                var_52 = local_sp_7 + (-16L);
                *(uint64_t *)var_52 = 4270681UL;
                indirect_placeholder_1();
                *(uint32_t *)var_49 = 38U;
                var_53 = *var_51;
                local_sp_1 = var_52;
            }
            if (var_53 != 0U) {
                *(uint32_t *)4385712UL = 1U;
                var_55 = (uint64_t)*var_51;
                rax_1 = var_55;
                return rax_1;
            }
            var_54 = local_sp_1 + (-8L);
            *(uint64_t *)var_54 = 4270698UL;
            indirect_placeholder_1();
            local_sp_8 = var_54;
            if (*(uint32_t *)var_49 != 38U) {
                *(uint32_t *)4385712UL = 1U;
                var_55 = (uint64_t)*var_51;
                rax_1 = var_55;
                return rax_1;
            }
        }
    }
    *(uint32_t *)4385712UL = 4294967295U;
    *(uint32_t *)4385716UL = 4294967295U;
    var_56 = *var_11;
    local_sp_9 = local_sp_8;
    local_sp_10 = local_sp_8;
    if (var_56 != 0U) {
        rax_1 = 4294967295UL;
        if (var_56 != 3U) {
            var_57 = *var_4;
            if ((int)var_57 > (int)4294967295U) {
                var_58 = *var_5;
                var_59 = local_sp_8 + (-8L);
                *(uint64_t *)var_59 = 4270802UL;
                var_60 = indirect_placeholder(var_58);
                local_sp_0 = var_59;
                storemerge7 = (var_60 & (-256L)) | ((uint64_t)(uint32_t)var_60 != 0UL);
            } else {
                var_61 = local_sp_8 + (-8L);
                *(uint64_t *)var_61 = 4270832UL;
                indirect_placeholder_1();
                local_sp_0 = var_61;
                storemerge7 = (uint64_t)(var_57 & (-256)) | (var_57 != 0U);
            }
            local_sp_9 = local_sp_0;
            if ((uint64_t)(unsigned char)storemerge7 == 0UL) {
                return rax_1;
            }
        }
        local_sp_10 = local_sp_9;
        if (*var_10 != 0UL) {
            var_62 = var_0 + (-248L);
            var_63 = local_sp_9 + (-8L);
            *(uint64_t *)var_63 = 4270882UL;
            var_64 = indirect_placeholder_3(var_9, var_62);
            local_sp_10 = var_63;
            if ((uint64_t)(unsigned char)var_64 == 0UL) {
                return rax_1;
            }
        }
        if (*var_10 == 0UL) {
            *(uint64_t *)(var_0 + (-40L)) = 0UL;
        } else {
            var_65 = **(uint64_t **)var_9;
            var_66 = var_0 + (-296L);
            *(uint64_t *)var_66 = var_65;
            var_67 = *(uint64_t *)(*var_10 + 8UL);
            *(uint64_t *)(var_0 + (-288L)) = ((uint64_t)((long)(uint64_t)(((unsigned __int128)var_67 * 2361183241434822607ULL) >> 64ULL) >> (long)7UL) - (uint64_t)((long)var_67 >> (long)63UL));
            *(uint64_t *)(var_0 + (-280L)) = *(uint64_t *)(*var_10 + 16UL);
            var_68 = *(uint64_t *)(*var_10 + 24UL);
            *(uint64_t *)(var_0 + (-272L)) = ((uint64_t)((long)(uint64_t)(((unsigned __int128)var_68 * 2361183241434822607ULL) >> 64ULL) >> (long)7UL) - (uint64_t)((long)var_68 >> (long)63UL));
            *(uint64_t *)(var_0 + (-40L)) = var_66;
        }
        var_69 = *var_4;
        if ((int)var_69 > (int)4294967295U) {
            var_70 = *var_5;
            *(uint64_t *)(local_sp_10 + (-8L)) = 4271095UL;
            indirect_placeholder_1();
            rax_1 = var_70;
        } else {
            *(uint64_t *)(local_sp_10 + (-8L)) = 4271122UL;
            indirect_placeholder_1();
            rax_1 = 4294967295UL;
            if (var_69 != 0U & *var_5 != 0UL) {
                if (*var_10 == 0UL) {
                    *(uint64_t *)(var_0 + (-48L)) = 0UL;
                } else {
                    var_71 = **(uint64_t **)var_9;
                    var_72 = var_0 + (-264L);
                    *(uint64_t *)var_72 = var_71;
                    *(uint64_t *)(var_0 + (-256L)) = *(uint64_t *)(*var_10 + 16UL);
                    *(uint64_t *)(var_0 + (-48L)) = var_72;
                }
                var_73 = *var_5;
                *(uint64_t *)(local_sp_10 + (-16L)) = 4271234UL;
                indirect_placeholder_1();
                rax_1 = var_73;
            }
        }
        return rax_1;
    }
    if ((int)*var_4 <= (int)4294967295U) {
        rax_1 = 4294967295UL;
        if (var_56 == 3U) {
            local_sp_10 = local_sp_9;
            var_62 = var_0 + (-248L);
            var_63 = local_sp_9 + (-8L);
            *(uint64_t *)var_63 = 4270882UL;
            var_64 = indirect_placeholder_3(var_9, var_62);
            local_sp_10 = var_63;
            if (*var_10 != 0UL & (uint64_t)(unsigned char)var_64 == 0UL) {
                return rax_1;
            }
        }
        var_57 = *var_4;
        if ((int)var_57 > (int)4294967295U) {
            var_61 = local_sp_8 + (-8L);
            *(uint64_t *)var_61 = 4270832UL;
            indirect_placeholder_1();
            local_sp_0 = var_61;
            storemerge7 = (uint64_t)(var_57 & (-256)) | (var_57 != 0U);
        } else {
            var_58 = *var_5;
            var_59 = local_sp_8 + (-8L);
            *(uint64_t *)var_59 = 4270802UL;
            var_60 = indirect_placeholder(var_58);
            local_sp_0 = var_59;
            storemerge7 = (var_60 & (-256L)) | ((uint64_t)(uint32_t)var_60 != 0UL);
        }
        local_sp_9 = local_sp_0;
        if ((uint64_t)(unsigned char)storemerge7 == 0UL) {
            return rax_1;
        }
    }
    if (*var_10 == 0UL) {
        var_65 = **(uint64_t **)var_9;
        var_66 = var_0 + (-296L);
        *(uint64_t *)var_66 = var_65;
        var_67 = *(uint64_t *)(*var_10 + 8UL);
        *(uint64_t *)(var_0 + (-288L)) = ((uint64_t)((long)(uint64_t)(((unsigned __int128)var_67 * 2361183241434822607ULL) >> 64ULL) >> (long)7UL) - (uint64_t)((long)var_67 >> (long)63UL));
        *(uint64_t *)(var_0 + (-280L)) = *(uint64_t *)(*var_10 + 16UL);
        var_68 = *(uint64_t *)(*var_10 + 24UL);
        *(uint64_t *)(var_0 + (-272L)) = ((uint64_t)((long)(uint64_t)(((unsigned __int128)var_68 * 2361183241434822607ULL) >> 64ULL) >> (long)7UL) - (uint64_t)((long)var_68 >> (long)63UL));
        *(uint64_t *)(var_0 + (-40L)) = var_66;
    } else {
        *(uint64_t *)(var_0 + (-40L)) = 0UL;
    }
    var_69 = *var_4;
    if ((int)var_69 > (int)4294967295U) {
        var_70 = *var_5;
        *(uint64_t *)(local_sp_10 + (-8L)) = 4271095UL;
        indirect_placeholder_1();
        rax_1 = var_70;
    } else {
        *(uint64_t *)(local_sp_10 + (-8L)) = 4271122UL;
        indirect_placeholder_1();
        rax_1 = 4294967295UL;
        if (var_69 != 0U & *var_5 != 0UL) {
            if (*var_10 == 0UL) {
                var_71 = **(uint64_t **)var_9;
                var_72 = var_0 + (-264L);
                *(uint64_t *)var_72 = var_71;
                *(uint64_t *)(var_0 + (-256L)) = *(uint64_t *)(*var_10 + 16UL);
                *(uint64_t *)(var_0 + (-48L)) = var_72;
            } else {
                *(uint64_t *)(var_0 + (-48L)) = 0UL;
            }
            var_73 = *var_5;
            *(uint64_t *)(local_sp_10 + (-16L)) = 4271234UL;
            indirect_placeholder_1();
            rax_1 = var_73;
        }
    }
}
