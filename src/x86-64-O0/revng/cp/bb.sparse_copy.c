typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_217_ret_type;
struct indirect_placeholder_216_ret_type;
struct indirect_placeholder_220_ret_type;
struct indirect_placeholder_215_ret_type;
struct indirect_placeholder_217_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_216_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_220_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_215_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_1(void);
extern uint64_t indirect_placeholder_6(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder(uint64_t param_0);
extern uint64_t indirect_placeholder_25(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_217_ret_type indirect_placeholder_217(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_216_ret_type indirect_placeholder_216(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_220_ret_type indirect_placeholder_220(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_215_ret_type indirect_placeholder_215(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
typedef _Bool bool;
uint64_t bb_sparse_copy(uint64_t rcx, uint64_t rdx, uint64_t rsi, uint64_t rdi, uint64_t r9, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint32_t *var_5;
    uint32_t *var_6;
    uint64_t *var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t *var_10;
    unsigned char *var_11;
    unsigned char **var_12;
    uint64_t **var_13;
    unsigned char *var_14;
    uint64_t *var_15;
    uint64_t *var_16;
    uint64_t *var_17;
    uint64_t *var_18;
    uint64_t *var_19;
    uint64_t *var_20;
    unsigned char *var_21;
    unsigned char *var_22;
    unsigned char *var_23;
    uint64_t *var_24;
    uint64_t local_sp_4;
    uint64_t var_43;
    struct indirect_placeholder_217_ret_type var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t rax_0;
    uint64_t local_sp_0;
    uint64_t local_sp_3;
    uint64_t *var_27;
    uint64_t local_sp_1;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    unsigned char var_34;
    uint64_t local_sp_2;
    uint64_t var_31;
    uint64_t var_32;
    unsigned char var_33;
    unsigned char storemerge4;
    uint64_t var_35;
    unsigned char storemerge5;
    unsigned char var_36;
    unsigned char var_37;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    struct indirect_placeholder_220_ret_type var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t local_sp_5;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_25;
    uint64_t var_26;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    var_4 = var_0 + (-136L);
    var_5 = (uint32_t *)(var_0 + (-92L));
    *var_5 = (uint32_t)rdi;
    var_6 = (uint32_t *)(var_0 + (-96L));
    *var_6 = (uint32_t)rsi;
    var_7 = (uint64_t *)(var_0 + (-104L));
    *var_7 = rdx;
    var_8 = var_0 + (-112L);
    *(uint64_t *)var_8 = rcx;
    var_9 = var_0 + (-120L);
    var_10 = (uint64_t *)var_9;
    *var_10 = r8;
    var_11 = (unsigned char *)(var_0 + (-124L));
    *var_11 = (unsigned char)r9;
    var_12 = (unsigned char **)(var_0 | 40UL);
    **var_12 = (unsigned char)'\x00';
    var_13 = (uint64_t **)(var_0 | 32UL);
    **var_13 = 0UL;
    var_14 = (unsigned char *)(var_0 + (-25L));
    *var_14 = (unsigned char)'\x00';
    var_15 = (uint64_t *)(var_0 + (-40L));
    *var_15 = 0UL;
    var_16 = (uint64_t *)(var_0 | 24UL);
    var_17 = (uint64_t *)(var_0 + (-48L));
    var_18 = (uint64_t *)(var_0 + (-56L));
    var_19 = (uint64_t *)(var_0 + (-64L));
    var_20 = (uint64_t *)(var_0 + (-72L));
    var_21 = (unsigned char *)(var_0 + (-73L));
    var_22 = (unsigned char *)(var_0 + (-74L));
    var_23 = (unsigned char *)(var_0 + (-75L));
    var_24 = (uint64_t *)(var_0 | 16UL);
    storemerge4 = (unsigned char)'\x00';
    storemerge5 = (unsigned char)'\x01';
    local_sp_4 = var_4;
    rax_0 = 0UL;
    while (1U)
        {
            local_sp_5 = local_sp_4;
            var_25 = (uint64_t)*var_5;
            var_26 = local_sp_4 + (-8L);
            *(uint64_t *)var_26 = 4218469UL;
            indirect_placeholder_1();
            *var_17 = var_25;
            local_sp_1 = var_26;
            local_sp_5 = var_26;
            if (!(*var_16 == 0UL && var_25 == 0UL)) {
                loop_state_var = 0U;
                break;
            }
            *var_16 = (*var_16 - var_25);
            var_27 = *var_13;
            *var_27 = (*var_27 + *var_17);
            *var_18 = *(uint64_t *)((*var_10 == 0UL) ? var_8 : var_9);
            *var_19 = *var_7;
            *var_20 = *var_7;
            while (1U)
                {
                    local_sp_2 = local_sp_1;
                    local_sp_4 = local_sp_1;
                    if (*var_17 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    *var_21 = *var_14;
                    var_28 = *var_17;
                    var_29 = *var_18;
                    var_30 = (var_29 > var_28) ? var_28 : var_29;
                    *var_18 = var_30;
                    if ((*var_10 == 0UL) || (var_30 == 0UL)) {
                        var_34 = *var_14;
                    } else {
                        var_31 = local_sp_1 + (-8L);
                        *(uint64_t *)var_31 = 4218695UL;
                        var_32 = indirect_placeholder(var_30);
                        var_33 = (unsigned char)var_32;
                        *var_14 = var_33;
                        var_34 = var_33;
                        local_sp_2 = var_31;
                    }
                    local_sp_3 = local_sp_2;
                    if ((uint64_t)(var_34 - *var_21) == 0UL) {
                    } else {
                        storemerge4 = (unsigned char)'\x01';
                        if (*var_15 == 0UL) {
                        }
                    }
                    *var_22 = (storemerge4 & '\x01');
                    var_35 = *var_18;
                    if (*var_17 == var_35) {
                        storemerge5 = (var_35 == 0UL) ? '\x01' : '\x00';
                    } else {
                        if (*var_14 == '\x01') {
                            storemerge5 = (var_35 == 0UL) ? '\x01' : '\x00';
                        }
                    }
                    var_36 = storemerge5 & '\x01';
                    *var_23 = var_36;
                    var_37 = *var_22;
                    if ((var_37 == '\x00') && (var_36 == '\x00')) {
                        var_55 = 9223372036854775807UL - *var_18;
                        var_56 = *var_15;
                        var_57 = helper_cc_compute_c_wrapper(var_55 - var_56, var_56, var_2, 17U);
                        if (var_57 != 0UL) {
                            var_58 = *(uint64_t *)(var_0 | 8UL);
                            *(uint64_t *)(local_sp_2 + (-8L)) = 4219088UL;
                            var_59 = indirect_placeholder_220(var_58, 4UL);
                            var_60 = var_59.field_0;
                            var_61 = var_59.field_1;
                            var_62 = var_59.field_2;
                            *(uint64_t *)(local_sp_2 + (-16L)) = 4219116UL;
                            indirect_placeholder_215(0UL, var_60, 4354344UL, 0UL, 0UL, var_61, var_62);
                            loop_state_var = 0U;
                            break;
                        }
                        *var_15 = (*var_18 + *var_15);
                    } else {
                        if (var_37 == '\x01') {
                            *var_15 = (*var_18 + *var_15);
                        }
                        if (*var_21 == '\x01') {
                            var_49 = (uint64_t)*var_11;
                            var_50 = *var_15;
                            var_51 = *var_24;
                            var_52 = (uint64_t)*var_6;
                            var_53 = local_sp_2 + (-8L);
                            *(uint64_t *)var_53 = 4218947UL;
                            var_54 = indirect_placeholder_25(var_50, var_49, var_51, var_52);
                            local_sp_0 = var_53;
                            if ((uint64_t)(unsigned char)var_54 != 1UL) {
                                loop_state_var = 0U;
                                break;
                            }
                        }
                        var_38 = *var_15;
                        var_39 = *var_20;
                        var_40 = (uint64_t)*var_6;
                        var_41 = local_sp_2 + (-8L);
                        *(uint64_t *)var_41 = 4218854UL;
                        var_42 = indirect_placeholder_6(var_38, var_39, var_40);
                        local_sp_0 = var_41;
                        if (var_42 != *var_15) {
                            var_43 = *var_24;
                            *(uint64_t *)(local_sp_2 + (-16L)) = 4218880UL;
                            var_44 = indirect_placeholder_217(var_43, 4UL);
                            var_45 = var_44.field_0;
                            var_46 = var_44.field_1;
                            var_47 = var_44.field_2;
                            *(uint64_t *)(local_sp_2 + (-24L)) = 4218888UL;
                            indirect_placeholder_1();
                            var_48 = (uint64_t)*(uint32_t *)var_45;
                            *(uint64_t *)(local_sp_2 + (-32L)) = 4218915UL;
                            indirect_placeholder_216(0UL, var_45, 4354327UL, var_48, 0UL, var_46, var_47);
                            loop_state_var = 0U;
                            break;
                        }
                        *var_20 = *var_19;
                        *var_15 = *var_18;
                        local_sp_3 = local_sp_0;
                        if (*var_23 != '\x00') {
                            if (*var_18 == 0UL) {
                                *var_17 = 0UL;
                            }
                            if (*var_22 == '\x00') {
                                *var_15 = 0UL;
                            } else {
                                *var_18 = 0UL;
                            }
                        }
                    }
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    **var_12 = *var_14;
                    continue;
                }
                break;
              case 0U:
                {
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 1U:
        {
            return rax_0;
        }
        break;
      case 0U:
        {
            rax_0 = 1UL;
            if (*var_14 == '\x00') {
            }
        }
        break;
    }
}
