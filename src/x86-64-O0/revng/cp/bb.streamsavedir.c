typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_309_ret_type;
struct indirect_placeholder_309_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_1(void);
extern uint64_t init_r10(void);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder(uint64_t param_0);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern void indirect_placeholder_17(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_309_ret_type indirect_placeholder_309(uint64_t param_0);
extern uint64_t indirect_placeholder_310(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
typedef _Bool bool;
uint64_t bb_streamsavedir(uint64_t rsi, uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t *var_8;
    uint32_t *var_9;
    uint64_t var_10;
    uint64_t *var_11;
    uint64_t *var_12;
    uint64_t *var_13;
    uint64_t *var_14;
    uint64_t *var_15;
    uint64_t *var_16;
    uint64_t var_17;
    uint64_t *var_18;
    uint64_t rax_1;
    uint64_t local_sp_1;
    uint64_t var_62;
    uint64_t local_sp_0;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint32_t *var_55;
    uint64_t rax_2_ph;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    struct indirect_placeholder_309_ret_type var_59;
    uint64_t *var_60;
    uint64_t *var_61;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t local_sp_4;
    uint64_t var_39;
    uint64_t local_sp_2;
    uint64_t rbx_0;
    uint64_t var_51;
    uint64_t local_sp_5;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t local_sp_3;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_30;
    uint64_t rax_0;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t rax_2;
    uint64_t var_52;
    uint32_t var_53;
    uint32_t *var_54;
    uint64_t var_19;
    uint64_t *var_20;
    uint64_t var_21;
    uint64_t *var_22;
    uint64_t *var_23;
    uint64_t var_24;
    uint64_t *var_25;
    uint64_t var_26;
    uint64_t *var_27;
    uint64_t local_sp_5_ph;
    uint64_t rbx_1_ph;
    uint64_t var_28;
    uint64_t var_29;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = init_r10();
    var_4 = init_r9();
    var_5 = init_r8();
    var_6 = init_rbx();
    var_7 = var_0 + (-8L);
    *(uint64_t *)var_7 = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    var_8 = (uint64_t *)(var_0 + (-160L));
    *var_8 = rdi;
    var_9 = (uint32_t *)(var_0 + (-164L));
    *var_9 = (uint32_t)rsi;
    var_10 = var_0 + (-32L);
    var_11 = (uint64_t *)var_10;
    *var_11 = 0UL;
    var_12 = (uint64_t *)(var_0 + (-40L));
    *var_12 = 0UL;
    var_13 = (uint64_t *)(var_0 + (-48L));
    *var_13 = 0UL;
    var_14 = (uint64_t *)(var_0 + (-56L));
    *var_14 = 0UL;
    var_15 = (uint64_t *)(var_0 + (-64L));
    *var_15 = 0UL;
    var_16 = (uint64_t *)(var_0 + (-72L));
    *var_16 = 0UL;
    var_17 = *(uint64_t *)(((uint64_t)*var_9 << 3UL) + 4359424UL);
    var_18 = (uint64_t *)(var_0 + (-88L));
    *var_18 = var_17;
    rax_1 = 0UL;
    var_62 = 0UL;
    rax_2_ph = var_17;
    rax_0 = 0UL;
    rbx_1_ph = var_6;
    if (*var_8 == 0UL) {
        return rax_1;
    }
    var_19 = var_0 + (-168L);
    var_20 = (uint64_t *)(var_0 + (-96L));
    var_21 = var_0 + (-104L);
    var_22 = (uint64_t *)var_21;
    var_23 = (uint64_t *)(var_0 + (-112L));
    var_24 = var_0 + (-136L);
    var_25 = (uint64_t *)var_24;
    var_26 = var_0 + (-144L);
    var_27 = (uint64_t *)var_26;
    local_sp_5_ph = var_19;
    while (1U)
        {
            rbx_0 = rbx_1_ph;
            rax_2 = rax_2_ph;
            local_sp_5 = local_sp_5_ph;
            while (1U)
                {
                    *(uint64_t *)(local_sp_5 + (-8L)) = 4265816UL;
                    indirect_placeholder_1();
                    *(uint32_t *)rax_2 = 0U;
                    var_28 = *var_8;
                    var_29 = local_sp_5 + (-16L);
                    *(uint64_t *)var_29 = 4265837UL;
                    indirect_placeholder_1();
                    *var_20 = var_28;
                    rax_2 = 0UL;
                    local_sp_5 = var_29;
                    if (var_28 == 0UL) {
                        var_30 = var_28 + 19UL;
                        *var_22 = var_30;
                        if (**(unsigned char **)var_21 == '.') {
                            rax_0 = (*(unsigned char *)(var_30 + 1UL) == '.') ? 2UL : 1UL;
                        }
                        if (*(unsigned char *)(rax_0 + var_30) == '\x00') {
                            continue;
                        }
                        loop_state_var = 0U;
                        break;
                    }
                    var_52 = local_sp_5 + (-24L);
                    *(uint64_t *)var_52 = 4266192UL;
                    indirect_placeholder_1();
                    var_53 = *(volatile uint32_t *)(uint32_t *)0UL;
                    var_54 = (uint32_t *)(var_0 + (-116L));
                    *var_54 = var_53;
                    local_sp_1 = var_52;
                    if (var_53 == 0U) {
                        *(uint64_t *)(local_sp_5 + (-32L)) = 4266215UL;
                        indirect_placeholder_1();
                        var_55 = *(uint32_t **)var_10;
                        *(uint64_t *)(local_sp_5 + (-40L)) = 4266227UL;
                        indirect_placeholder_1();
                        *(uint64_t *)(local_sp_5 + (-48L)) = 4266232UL;
                        indirect_placeholder_1();
                        *var_55 = *var_54;
                        loop_state_var = 1U;
                        break;
                    }
                    if (*var_18 == 0UL) {
                        if (*var_15 != 0UL) {
                            loop_state_var = 3U;
                            break;
                        }
                        var_56 = local_sp_5 + (-32L);
                        *(uint64_t *)var_56 = 4266293UL;
                        indirect_placeholder_1();
                        local_sp_1 = var_56;
                        loop_state_var = 3U;
                        break;
                    }
                    var_68 = *var_16;
                    if (var_68 != *var_12) {
                        loop_state_var = 2U;
                        break;
                    }
                    var_69 = var_68 + 1UL;
                    var_70 = *var_11;
                    *(uint64_t *)(local_sp_5 + (-32L)) = 4266487UL;
                    var_71 = indirect_placeholder_3(var_69, var_70);
                    *var_11 = var_71;
                    loop_state_var = 2U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    var_31 = *var_20;
                    var_32 = local_sp_5 + (-24L);
                    *(uint64_t *)var_32 = 4265939UL;
                    indirect_placeholder_1();
                    var_33 = var_31 + 20UL;
                    *var_23 = var_33;
                    local_sp_3 = var_32;
                    local_sp_4 = var_32;
                    if (*var_18 == 0UL) {
                        var_44 = *var_12 - *var_16;
                        var_45 = helper_cc_compute_c_wrapper(var_33 - var_44, var_44, var_2, 17U);
                        if (var_45 != 0UL) {
                            var_46 = *var_23 + *var_16;
                            *var_27 = var_46;
                            if (*var_16 <= var_46) {
                                *(uint64_t *)(local_sp_5 + (-32L)) = 4266100UL;
                                indirect_placeholder_17(var_7, var_4, var_5);
                                abort();
                            }
                            var_47 = *var_11;
                            var_48 = local_sp_5 + (-32L);
                            *(uint64_t *)var_48 = 4266127UL;
                            var_49 = indirect_placeholder_310(var_26, 1UL, var_26, var_47, var_3, var_4, var_5, rbx_1_ph);
                            *var_11 = var_49;
                            *var_12 = *var_27;
                            local_sp_4 = var_48;
                        }
                        var_50 = local_sp_4 + (-8L);
                        *(uint64_t *)var_50 = 4266173UL;
                        indirect_placeholder_1();
                        local_sp_2 = var_50;
                    } else {
                        var_34 = *var_14;
                        var_35 = *var_15;
                        var_39 = var_35;
                        if (var_34 == var_35) {
                            *var_25 = var_34;
                            var_36 = *var_13;
                            var_37 = local_sp_5 + (-32L);
                            *(uint64_t *)var_37 = 4265996UL;
                            var_38 = indirect_placeholder_310(var_24, 8UL, var_24, var_36, var_3, var_4, var_5, rbx_1_ph);
                            *var_13 = var_38;
                            *var_14 = *var_25;
                            var_39 = *var_15;
                            local_sp_3 = var_37;
                        }
                        var_40 = *var_13 + (var_39 << 3UL);
                        var_41 = *var_22;
                        var_42 = local_sp_3 + (-8L);
                        *(uint64_t *)var_42 = 4266040UL;
                        var_43 = indirect_placeholder(var_41);
                        *(uint64_t *)var_40 = var_43;
                        *var_15 = (*var_15 + 1UL);
                        local_sp_2 = var_42;
                        rbx_0 = var_40;
                    }
                    var_51 = *var_23;
                    *var_16 = (*var_16 + var_51);
                    rax_2_ph = var_51;
                    local_sp_5_ph = local_sp_2;
                    rbx_1_ph = rbx_0;
                    continue;
                }
                break;
              case 3U:
                {
                    var_57 = *var_16 + 1UL;
                    var_58 = local_sp_1 + (-8L);
                    *(uint64_t *)var_58 = 4266309UL;
                    var_59 = indirect_placeholder_309(var_57);
                    *var_11 = var_59.field_0;
                    *var_16 = 0UL;
                    var_60 = (uint64_t *)(var_0 + (-80L));
                    *var_60 = 0UL;
                    var_61 = (uint64_t *)(var_0 + (-128L));
                    local_sp_0 = var_58;
                    loop_state_var = 2U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 1U:
                {
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 2U:
                {
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 1U:
        {
            *(unsigned char *)(*var_16 + *var_11) = (unsigned char)'\x00';
            rax_1 = *var_11;
        }
        break;
      case 2U:
        {
            var_63 = *var_15;
            var_64 = helper_cc_compute_c_wrapper(var_62 - var_63, var_63, var_2, 17U);
            while (var_64 != 0UL)
                {
                    var_65 = *var_16 + *var_11;
                    *var_61 = var_65;
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4266383UL;
                    indirect_placeholder_1();
                    *var_16 = (*var_16 + ((var_65 - *var_61) + 1UL));
                    var_66 = local_sp_0 + (-16L);
                    *(uint64_t *)var_66 = 4266425UL;
                    indirect_placeholder_1();
                    var_67 = *var_60 + 1UL;
                    *var_60 = var_67;
                    var_62 = var_67;
                    local_sp_0 = var_66;
                    var_63 = *var_15;
                    var_64 = helper_cc_compute_c_wrapper(var_62 - var_63, var_63, var_2, 17U);
                }
            *(uint64_t *)(local_sp_0 + (-8L)) = 4266452UL;
            indirect_placeholder_1();
        }
        break;
    }
}
