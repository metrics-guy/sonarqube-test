typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_backupfile_internal_ret_type;
struct indirect_placeholder_243_ret_type;
struct bb_backupfile_internal_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_243_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_1(void);
extern uint64_t indirect_placeholder_19(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t indirect_placeholder(uint64_t param_0);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern void indirect_placeholder_79(uint64_t param_0);
extern void indirect_placeholder_17(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_243_ret_type indirect_placeholder_243(uint64_t param_0);
struct bb_backupfile_internal_ret_type bb_backupfile_internal(uint64_t rdx, uint64_t rsi, uint64_t rdi) {
    uint64_t var_16;
    uint64_t local_sp_6;
    uint64_t var_18;
    uint64_t *var_19;
    uint64_t spec_store_select;
    uint64_t var_20;
    uint64_t *var_21;
    uint64_t var_22;
    struct indirect_placeholder_243_ret_type var_23;
    uint64_t var_36;
    uint64_t var_57;
    uint64_t local_sp_5;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint32_t *var_5;
    unsigned char *var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t *var_14;
    uint64_t var_15;
    uint64_t var_17;
    uint64_t local_sp_0;
    uint64_t var_60;
    uint32_t *var_61;
    uint64_t rax_1;
    uint64_t var_58;
    uint32_t var_59;
    uint32_t *var_46;
    uint64_t var_38;
    uint64_t r8_3;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t local_sp_1;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t local_sp_2;
    uint64_t r8_0;
    uint64_t local_sp_4;
    uint64_t var_48;
    uint64_t rax_0;
    uint64_t var_49;
    uint64_t local_sp_3;
    uint32_t var_50;
    uint32_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t r8_1;
    uint64_t var_47;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint32_t var_37;
    uint64_t r8_2;
    struct bb_backupfile_internal_ret_type mrv;
    struct bb_backupfile_internal_ret_type mrv1;
    struct bb_backupfile_internal_ret_type mrv2;
    uint64_t var_27;
    uint64_t *var_28;
    uint32_t *var_29;
    uint32_t *var_30;
    uint32_t *var_31;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t *var_26;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r9();
    var_3 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_4 = (uint64_t *)(var_0 + (-96L));
    *var_4 = rdi;
    var_5 = (uint32_t *)(var_0 + (-100L));
    *var_5 = (uint32_t)rsi;
    var_6 = (unsigned char *)(var_0 + (-104L));
    *var_6 = (unsigned char)rdx;
    var_7 = *var_4;
    *(uint64_t *)(var_0 + (-112L)) = 4246464UL;
    var_8 = indirect_placeholder(var_7);
    var_9 = var_8 - *var_4;
    var_10 = (uint64_t *)(var_0 + (-16L));
    *var_10 = var_9;
    var_11 = *var_4 + var_9;
    var_12 = var_0 + (-120L);
    *(uint64_t *)var_12 = 4246491UL;
    indirect_placeholder_1();
    var_13 = var_11 + *var_10;
    var_14 = (uint64_t *)(var_0 + (-40L));
    *var_14 = var_13;
    var_15 = *(uint64_t *)4385320UL;
    local_sp_6 = var_12;
    var_17 = var_15;
    rax_1 = 0UL;
    r8_3 = var_3;
    rax_0 = 4294967295UL;
    r8_2 = var_3;
    if (var_15 == 0UL) {
        var_16 = var_0 + (-128L);
        *(uint64_t *)var_16 = 4246524UL;
        indirect_placeholder_79(0UL);
        var_17 = *(uint64_t *)4385320UL;
        local_sp_6 = var_16;
    }
    *(uint64_t *)(local_sp_6 + (-8L)) = 4246539UL;
    indirect_placeholder_1();
    var_18 = var_17 + 1UL;
    *(uint64_t *)(var_0 + (-48L)) = var_18;
    var_19 = (uint64_t *)(var_0 + (-24L));
    spec_store_select = (var_18 > 8UL) ? var_18 : 9UL;
    *var_19 = spec_store_select;
    var_20 = (spec_store_select + *var_14) + 1UL;
    var_21 = (uint64_t *)(var_0 + (-56L));
    *var_21 = var_20;
    var_22 = local_sp_6 + (-16L);
    *(uint64_t *)var_22 = 4246601UL;
    var_23 = indirect_placeholder_243(var_20);
    var_24 = var_23.field_0;
    var_25 = var_0 + (-72L);
    var_26 = (uint64_t *)var_25;
    *var_26 = var_24;
    local_sp_5 = var_22;
    if (var_24 == 0UL) {
        mrv.field_0 = rax_1;
        mrv1 = mrv;
        mrv1.field_1 = var_2;
        mrv2 = mrv1;
        mrv2.field_2 = r8_2;
        return mrv2;
    }
    var_27 = var_0 + (-80L);
    var_28 = (uint64_t *)var_27;
    *var_28 = 0UL;
    var_29 = (uint32_t *)(var_0 + (-28L));
    var_30 = (uint32_t *)(var_0 + (-60L));
    var_31 = (uint32_t *)(var_0 + (-64L));
    r8_0 = var_27;
    r8_2 = var_27;
    while (1U)
        {
            *(uint64_t *)(local_sp_5 + (-8L)) = 4246658UL;
            indirect_placeholder_1();
            if (*var_5 == 1U) {
                var_47 = local_sp_5 + (-16L);
                *(uint64_t *)var_47 = 4246697UL;
                indirect_placeholder_1();
                local_sp_2 = var_47;
                r8_0 = r8_3;
            } else {
                var_32 = *var_21;
                var_33 = *var_10;
                var_34 = *var_14;
                var_35 = local_sp_5 + (-16L);
                *(uint64_t *)var_35 = 4246733UL;
                var_36 = indirect_placeholder_19(var_33, var_34, var_32, var_25, var_27);
                var_37 = (uint32_t)var_36;
                local_sp_1 = var_35;
                local_sp_2 = var_35;
                if ((uint64_t)(var_37 + (-3)) != 0UL) {
                    var_46 = *(uint32_t **)var_25;
                    *(uint64_t *)(local_sp_5 + (-24L)) = 4246893UL;
                    indirect_placeholder_1();
                    *(uint64_t *)(local_sp_5 + (-32L)) = 4246898UL;
                    indirect_placeholder_1();
                    *var_46 = 12U;
                    loop_state_var = 2U;
                    break;
                }
                if ((uint64_t)(var_37 & (-4)) != 0UL) {
                    if ((uint64_t)(var_37 + (-2)) == 0UL) {
                        if (*var_5 == 2U) {
                            *var_5 = 1U;
                            var_42 = local_sp_5 + (-24L);
                            *(uint64_t *)var_42 = 4246829UL;
                            indirect_placeholder_1();
                            local_sp_1 = var_42;
                        }
                        var_43 = *var_26;
                        var_44 = *var_14;
                        var_45 = local_sp_1 + (-8L);
                        *(uint64_t *)var_45 = 4246853UL;
                        indirect_placeholder_17(126UL, var_44, var_43);
                        local_sp_2 = var_45;
                    } else {
                        var_38 = (uint64_t)var_37;
                        if (!((var_38 > 2UL) || (var_38 == 0UL)) && (uint64_t)(var_37 + (-1)) == 0UL) {
                            var_39 = *var_26;
                            var_40 = *var_14;
                            var_41 = local_sp_5 + (-24L);
                            *(uint64_t *)var_41 = 4246879UL;
                            indirect_placeholder_17(126UL, var_40, var_39);
                            local_sp_2 = var_41;
                        }
                    }
                }
            }
            local_sp_3 = local_sp_2;
            local_sp_4 = local_sp_2;
            r8_1 = r8_0;
            if (*var_6 != '\x01') {
                loop_state_var = 1U;
                break;
            }
            var_48 = *var_28;
            if (var_48 == 0UL) {
                var_49 = local_sp_2 + (-8L);
                *(uint64_t *)var_49 = 4246951UL;
                indirect_placeholder_1();
                rax_0 = var_48;
                local_sp_3 = var_49;
            }
            var_50 = (uint32_t)rax_0;
            *var_29 = var_50;
            if ((int)var_50 > (int)4294967295U) {
                *var_29 = 4294967196U;
                *var_10 = 0UL;
            }
            var_51 = (*var_5 != 1U);
            *var_30 = var_51;
            var_52 = *var_10 + *var_26;
            var_53 = (uint64_t)var_51;
            var_54 = (uint64_t)*var_29;
            var_55 = *var_4;
            var_56 = local_sp_3 + (-8L);
            *(uint64_t *)var_56 = 4247033UL;
            var_57 = indirect_placeholder_19(var_52, var_54, var_55, 4294967196UL, var_53);
            local_sp_4 = var_56;
            r8_1 = var_53;
            r8_2 = var_53;
            r8_3 = var_53;
            if ((uint64_t)(uint32_t)var_57 != 0UL) {
                loop_state_var = 1U;
                break;
            }
            var_58 = local_sp_3 + (-16L);
            *(uint64_t *)var_58 = 4247042UL;
            indirect_placeholder_1();
            var_59 = *(uint32_t *)var_57;
            *var_31 = var_59;
            local_sp_0 = var_58;
            local_sp_5 = var_58;
            if (var_59 == 17U) {
                continue;
            }
            if (*var_28 != 0UL) {
                loop_state_var = 0U;
                break;
            }
            var_60 = local_sp_3 + (-24L);
            *(uint64_t *)var_60 = 4247078UL;
            indirect_placeholder_1();
            local_sp_0 = var_60;
            loop_state_var = 0U;
            break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            var_61 = *(uint32_t **)var_25;
            *(uint64_t *)(local_sp_0 + (-8L)) = 4247090UL;
            indirect_placeholder_1();
            *(uint64_t *)(local_sp_0 + (-16L)) = 4247095UL;
            indirect_placeholder_1();
            *var_61 = *var_31;
        }
        break;
      case 2U:
        {
            break;
        }
        break;
      case 1U:
        {
            r8_2 = r8_1;
            if (*var_28 != 0UL) {
                *(uint64_t *)(local_sp_4 + (-8L)) = 4247132UL;
                indirect_placeholder_1();
            }
            rax_1 = *var_26;
        }
        break;
    }
}
