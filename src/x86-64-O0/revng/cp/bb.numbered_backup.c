typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_1(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_10(void);
uint64_t bb_numbered_backup(uint64_t rcx, uint64_t rdx, uint64_t rsi, uint64_t rdi, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    uint64_t var_7;
    uint32_t *var_8;
    uint64_t **var_9;
    uint64_t var_10;
    uint64_t *var_11;
    uint64_t **var_12;
    uint64_t var_13;
    uint64_t *var_14;
    uint64_t *var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t *var_18;
    uint64_t var_19;
    uint64_t *var_20;
    uint32_t var_71;
    uint32_t rax_2_shrunk;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t local_sp_1;
    uint64_t var_68;
    unsigned char *var_69;
    unsigned char var_70;
    uint64_t local_sp_3_be;
    uint64_t local_sp_3;
    uint64_t local_sp_3_ph;
    unsigned char var_46;
    uint64_t var_47;
    uint64_t var_48;
    unsigned char var_49;
    uint64_t var_51;
    uint64_t local_sp_0;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_59;
    uint64_t var_58;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    unsigned char **var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_50;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t local_sp_2;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint32_t var_72;
    uint64_t *var_30;
    uint64_t var_31;
    uint64_t *var_32;
    unsigned char *var_33;
    uint64_t *var_34;
    uint64_t *var_35;
    uint64_t *var_36;
    uint64_t var_37;
    uint64_t *var_38;
    uint64_t var_39;
    uint64_t var_21;
    uint16_t **var_22;
    uint16_t var_23;
    uint16_t *var_24;
    uint64_t var_25;
    uint64_t var_26;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_3 = var_0 + (-128L);
    *(uint64_t *)var_3 = rdi;
    var_4 = (uint64_t *)(var_0 + (-136L));
    *var_4 = rsi;
    var_5 = (uint64_t *)(var_0 + (-144L));
    *var_5 = rdx;
    var_6 = (uint64_t *)(var_0 + (-152L));
    *var_6 = rcx;
    var_7 = var_0 + (-160L);
    *(uint64_t *)var_7 = r8;
    var_8 = (uint32_t *)(var_0 + (-12L));
    *var_8 = 2U;
    var_9 = (uint64_t **)var_7;
    var_10 = **var_9;
    var_11 = (uint64_t *)(var_0 + (-24L));
    *var_11 = var_10;
    var_12 = (uint64_t **)var_3;
    var_13 = **var_12;
    var_14 = (uint64_t *)(var_0 + (-32L));
    *var_14 = var_13;
    var_15 = (uint64_t *)(var_0 + (-40L));
    *var_15 = 1UL;
    var_16 = *var_14 + *var_6;
    var_17 = var_0 + (-80L);
    var_18 = (uint64_t *)var_17;
    *var_18 = var_16;
    *(uint64_t *)(var_0 + (-176L)) = 4245566UL;
    var_19 = indirect_placeholder_10();
    var_20 = (uint64_t *)(var_0 + (-88L));
    *var_20 = var_19;
    var_47 = 1UL;
    rax_2_shrunk = 3U;
    if (*var_11 == 0UL) {
        var_21 = var_0 + (-184L);
        *(uint64_t *)var_21 = 4245589UL;
        indirect_placeholder_1();
        local_sp_3_ph = var_21;
    } else {
        var_22 = (uint16_t **)var_17;
        var_23 = **var_22;
        var_24 = (uint16_t *)(var_0 + (-114L));
        *var_24 = var_23;
        **var_22 = (uint16_t)(unsigned short)46U;
        var_25 = var_0 + (-184L);
        *(uint64_t *)var_25 = 4245626UL;
        var_26 = indirect_placeholder_10();
        *var_11 = var_26;
        local_sp_2 = var_25;
        var_27 = var_0 + (-192L);
        *(uint64_t *)var_27 = 4245642UL;
        indirect_placeholder_1();
        local_sp_2 = var_27;
        if (var_26 != 0UL & *(volatile uint32_t *)(uint32_t *)0UL == 12U) {
            *var_8 = 3U;
        }
        **var_22 = *var_24;
        var_28 = *var_20 + *var_18;
        *(uint32_t *)var_28 = 2117172782U;
        *(unsigned char *)(var_28 + 4UL) = (unsigned char)'\x00';
        var_29 = *var_11;
        local_sp_3_ph = local_sp_2;
        if (var_29 != 0UL) {
            var_72 = *var_8;
            rax_2_shrunk = var_72;
            return (uint64_t)rax_2_shrunk;
        }
        **var_9 = var_29;
    }
    var_30 = (uint64_t *)(var_0 + (-96L));
    var_31 = var_0 + (-104L);
    var_32 = (uint64_t *)var_31;
    var_33 = (unsigned char *)(var_0 + (-49L));
    var_34 = (uint64_t *)(var_0 + (-64L));
    var_35 = (uint64_t *)(var_0 + (-72L));
    var_36 = (uint64_t *)(var_0 + (-112L));
    var_37 = var_0 + (-48L);
    var_38 = (uint64_t *)var_37;
    local_sp_3 = local_sp_3_ph;
    while (1U)
        {
            var_39 = *var_11;
            *(uint64_t *)(local_sp_3 + (-8L)) = 4246401UL;
            indirect_placeholder_1();
            *var_30 = var_39;
            if (var_39 != 0UL) {
                **var_12 = *var_14;
                var_71 = *var_8;
                rax_2_shrunk = var_71;
                break;
            }
            var_40 = var_39 + 19UL;
            var_41 = local_sp_3 + (-16L);
            *(uint64_t *)var_41 = 4245738UL;
            indirect_placeholder_1();
            var_42 = *var_20 + 4UL;
            var_43 = helper_cc_compute_c_wrapper(var_40 - var_42, var_42, var_2, 17U);
            local_sp_3_be = var_41;
            if (var_43 == 0UL) {
                local_sp_3 = local_sp_3_be;
                continue;
            }
            var_44 = (uint64_t)((uint32_t)*var_14 + (uint32_t)*var_6);
            var_45 = local_sp_3 + (-24L);
            *(uint64_t *)var_45 = 4245797UL;
            indirect_placeholder_1();
            local_sp_3_be = var_45;
            local_sp_0 = var_45;
            *var_32 = ((*var_20 + *var_30) + 21UL);
            var_46 = **(unsigned char **)var_31;
            if (var_44 != 0UL & !(((signed char)var_46 <= '0') || ((signed char)var_46 > '9'))) {
                *var_33 = (var_46 == '9');
                *var_34 = 1UL;
                var_48 = *var_32;
                var_49 = *(unsigned char *)(var_47 + var_48);
                while ((uint64_t)(((uint32_t)(uint64_t)var_49 + (-48)) & (-2)) <= 9UL)
                    {
                        *var_33 = (*var_33 & (var_49 == '9'));
                        var_50 = *var_34 + 1UL;
                        *var_34 = var_50;
                        var_47 = var_50;
                        var_48 = *var_32;
                        var_49 = *(unsigned char *)(var_47 + var_48);
                    }
                if (var_49 != '~' & *(unsigned char *)(var_48 + (var_47 + 1UL)) != '\x00') {
                    var_51 = helper_cc_compute_c_wrapper(*var_15 - var_47, var_47, var_2, 17U);
                    if (var_51 != 0UL) {
                        if (*var_15 != *var_34) {
                            local_sp_3 = local_sp_3_be;
                            continue;
                        }
                        var_52 = *var_32;
                        var_53 = local_sp_3 + (-32L);
                        *(uint64_t *)var_53 = 4246061UL;
                        indirect_placeholder_1();
                        var_54 = helper_cc_compute_all_wrapper(var_52, 0UL, 0UL, 24U);
                        local_sp_3_be = var_53;
                        local_sp_0 = var_53;
                        if ((uint64_t)(((unsigned char)(var_54 >> 4UL) ^ (unsigned char)var_54) & '\xc0') != 0UL) {
                            local_sp_3 = local_sp_3_be;
                            continue;
                        }
                    }
                    *var_15 = (*var_34 + (uint64_t)*var_33);
                    *var_8 = (uint32_t)*var_33;
                    var_55 = (*var_15 + *var_5) + 4UL;
                    *var_35 = var_55;
                    var_56 = helper_cc_compute_c_wrapper(*var_4 - var_55, var_55, var_2, 17U);
                    local_sp_1 = local_sp_0;
                    if (var_56 != 0UL) {
                        var_57 = *var_35;
                        var_59 = var_57;
                        if (((var_57 & 4611686018427387904UL) != 0UL) || ((((long)var_57 > (long)18446744073709551615UL) ? 1UL : 0UL) == 0UL)) {
                            var_58 = var_57 << 1UL;
                            *var_35 = var_58;
                            var_59 = var_58;
                        }
                        var_60 = *var_14;
                        var_61 = local_sp_0 + (-8L);
                        *(uint64_t *)var_61 = 4246200UL;
                        var_62 = indirect_placeholder_3(var_59, var_60);
                        *var_36 = var_62;
                        local_sp_1 = var_61;
                        if (var_62 != 0UL) {
                            **var_12 = *var_14;
                            break;
                        }
                        *var_14 = var_62;
                        *var_4 = *var_35;
                    }
                    var_63 = *var_5 + *var_14;
                    *var_38 = (var_63 + 1UL);
                    *(unsigned char *)var_63 = (unsigned char)'.';
                    var_64 = *var_38;
                    *var_38 = (var_64 + 1UL);
                    *(unsigned char *)var_64 = (unsigned char)'~';
                    var_65 = (unsigned char **)var_37;
                    **var_65 = (unsigned char)'0';
                    *var_38 = (*var_38 + (uint64_t)*var_33);
                    var_66 = local_sp_1 + (-8L);
                    *(uint64_t *)var_66 = 4246338UL;
                    indirect_placeholder_1();
                    var_67 = *var_38 + *var_34;
                    *var_38 = var_67;
                    var_68 = var_67;
                    local_sp_3_be = var_66;
                    *var_38 = (var_68 + (-1L));
                    var_69 = *var_65;
                    var_70 = *var_69;
                    while (var_70 != '9')
                        {
                            *var_69 = (unsigned char)'0';
                            var_68 = *var_38;
                            *var_38 = (var_68 + (-1L));
                            var_69 = *var_65;
                            var_70 = *var_69;
                        }
                    *var_69 = (var_70 + '\x01');
                }
            }
        }
    return (uint64_t)rax_2_shrunk;
}
