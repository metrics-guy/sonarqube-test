typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_process_file_ret_type;
struct indirect_placeholder_85_ret_type;
struct indirect_placeholder_86_ret_type;
struct indirect_placeholder_88_ret_type;
struct indirect_placeholder_87_ret_type;
struct indirect_placeholder_92_ret_type;
struct indirect_placeholder_93_ret_type;
struct indirect_placeholder_91_ret_type;
struct indirect_placeholder_94_ret_type;
struct indirect_placeholder_89_ret_type;
struct indirect_placeholder_90_ret_type;
struct indirect_placeholder_96_ret_type;
struct indirect_placeholder_95_ret_type;
struct indirect_placeholder_97_ret_type;
struct indirect_placeholder_98_ret_type;
struct indirect_placeholder_99_ret_type;
struct indirect_placeholder_100_ret_type;
struct bb_process_file_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_85_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_86_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_88_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_87_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_92_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_93_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_91_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_94_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_89_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_90_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_96_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_95_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_97_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_98_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_99_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_100_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_26(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_r10(void);
extern uint64_t indirect_placeholder_6(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_rbx(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_15(uint64_t param_0);
extern uint64_t indirect_placeholder_5(uint64_t param_0);
extern void indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_85_ret_type indirect_placeholder_85(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_86_ret_type indirect_placeholder_86(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_88_ret_type indirect_placeholder_88(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_87_ret_type indirect_placeholder_87(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_92_ret_type indirect_placeholder_92(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_93_ret_type indirect_placeholder_93(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_91_ret_type indirect_placeholder_91(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_94_ret_type indirect_placeholder_94(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_89_ret_type indirect_placeholder_89(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_90_ret_type indirect_placeholder_90(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_96_ret_type indirect_placeholder_96(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_95_ret_type indirect_placeholder_95(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_97_ret_type indirect_placeholder_97(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_98_ret_type indirect_placeholder_98(uint64_t param_0);
extern struct indirect_placeholder_99_ret_type indirect_placeholder_99(uint64_t param_0);
extern struct indirect_placeholder_100_ret_type indirect_placeholder_100(uint64_t param_0);
struct bb_process_file_ret_type bb_process_file(uint64_t rsi, uint64_t rdi, uint64_t r8, uint64_t r9) {
    uint64_t r96_3;
    uint64_t local_sp_5;
    uint64_t var_66;
    struct indirect_placeholder_87_ret_type var_85;
    struct indirect_placeholder_86_ret_type var_142;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t var_7;
    uint64_t *var_8;
    unsigned char *var_9;
    uint64_t var_10;
    uint64_t *var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t *var_14;
    uint16_t var_15;
    uint32_t *var_16;
    uint32_t var_17;
    uint64_t r85_4;
    uint64_t var_161;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t _pre263;
    uint64_t var_159;
    uint64_t var_160;
    uint64_t storemerge2;
    uint64_t var_162;
    uint64_t var_163;
    uint64_t storemerge3;
    uint64_t var_164;
    uint64_t var_165;
    uint64_t r85_3;
    uint64_t var_126;
    uint64_t var_127;
    uint64_t var_128;
    uint64_t var_122;
    uint64_t r96_0;
    uint64_t local_sp_0;
    uint64_t var_129;
    uint64_t var_130;
    uint64_t var_131;
    uint64_t var_132;
    uint64_t var_133;
    uint64_t var_134;
    uint64_t var_135;
    uint64_t var_136;
    uint64_t var_137;
    uint64_t r96_2;
    uint64_t var_151;
    uint64_t var_152;
    uint64_t r96_8;
    uint64_t local_sp_12;
    struct indirect_placeholder_85_ret_type var_153;
    uint64_t var_154;
    uint64_t var_119;
    uint64_t var_120;
    uint64_t var_138;
    uint64_t r96_1;
    uint64_t var_139;
    uint64_t var_140;
    uint64_t var_141;
    uint64_t var_143;
    uint64_t local_sp_1;
    uint64_t var_144;
    uint64_t *var_145;
    uint64_t var_146;
    uint64_t local_sp_2;
    uint64_t var_147;
    uint64_t r85_0;
    uint64_t r96_4;
    uint64_t local_sp_8;
    uint64_t var_148;
    uint64_t var_149;
    uint64_t var_150;
    uint64_t var_121;
    uint64_t local_sp_3;
    uint64_t var_123;
    uint64_t var_124;
    uint64_t var_125;
    uint64_t local_sp_4;
    uint32_t var_74;
    uint64_t var_158;
    uint64_t var_155;
    uint64_t var_156;
    uint64_t var_157;
    uint64_t rdx_0;
    uint64_t rax_0;
    uint64_t local_sp_6;
    bool var_107;
    uint64_t var_108;
    uint64_t var_109;
    uint64_t storemerge;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t var_112;
    uint64_t *var_113;
    uint64_t var_114;
    uint64_t var_115;
    uint64_t var_116;
    uint64_t var_117;
    uint64_t var_118;
    uint64_t var_80;
    struct indirect_placeholder_88_ret_type var_81;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t local_sp_7;
    uint32_t var_48;
    uint32_t var_49;
    uint32_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    struct indirect_placeholder_92_ret_type var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t r85_1;
    uint64_t var_59;
    uint64_t var_50;
    struct indirect_placeholder_93_ret_type var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    struct indirect_placeholder_91_ret_type var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t r96_5;
    uint64_t local_sp_9;
    uint64_t local_sp_11;
    uint64_t local_sp_10;
    uint64_t var_61;
    uint64_t _pre261;
    uint64_t var_60;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_88;
    struct indirect_placeholder_94_ret_type var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t var_94;
    struct indirect_placeholder_89_ret_type var_95;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    struct indirect_placeholder_90_ret_type var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t r96_7;
    uint64_t rax_1;
    struct bb_process_file_ret_type mrv;
    struct bb_process_file_ret_type mrv1;
    struct bb_process_file_ret_type mrv2;
    uint64_t var_18;
    struct indirect_placeholder_96_ret_type var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    struct indirect_placeholder_95_ret_type var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_30;
    struct indirect_placeholder_97_ret_type var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    unsigned char *var_35;
    unsigned char var_36;
    uint64_t var_101;
    uint64_t var_102;
    struct indirect_placeholder_98_ret_type var_103;
    uint64_t var_98;
    uint64_t var_99;
    struct indirect_placeholder_99_ret_type var_100;
    uint64_t var_104;
    uint64_t var_105;
    struct indirect_placeholder_100_ret_type var_106;
    switch (var_17) {
      case 4U:
        {
            var_18 = *var_11;
            *(uint64_t *)(var_0 + (-176L)) = 4211181UL;
            var_19 = indirect_placeholder_96(var_18, 4UL);
            var_20 = var_19.field_0;
            var_21 = var_19.field_1;
            var_22 = var_19.field_2;
            var_23 = (uint64_t)*(uint32_t *)(*var_8 + 64UL);
            var_24 = var_0 + (-184L);
            *(uint64_t *)var_24 = 4211219UL;
            var_25 = indirect_placeholder_95(0UL, var_20, 4441283UL, var_23, 0UL, var_21, var_22);
            var_26 = var_25.field_1;
            var_27 = var_25.field_2;
            *var_9 = (unsigned char)'\x00';
            r85_4 = var_26;
            r96_8 = var_27;
            local_sp_12 = var_24;
        }
        break;
      case 6U:
        {
            r96_1 = r96_8;
            r96_2 = r96_8;
            r85_3 = r85_4;
            switch (*(uint32_t *)4486480UL) {
              case 0U:
                {
                    var_101 = *var_14;
                    var_102 = local_sp_12 + (-8L);
                    *(uint64_t *)var_102 = 4211997UL;
                    var_103 = indirect_placeholder_98(var_101);
                    rdx_0 = var_103.field_1;
                    rax_0 = var_103.field_0;
                    local_sp_6 = var_102;
                }
                break;
              case 2U:
                {
                    var_98 = *var_14;
                    var_99 = local_sp_12 + (-8L);
                    *(uint64_t *)var_99 = 4212022UL;
                    var_100 = indirect_placeholder_99(var_98);
                    rdx_0 = var_100.field_1;
                    rax_0 = var_100.field_0;
                    local_sp_6 = var_99;
                }
                break;
              default:
                {
                    var_104 = *var_14;
                    var_105 = local_sp_12 + (-8L);
                    *(uint64_t *)var_105 = 4212036UL;
                    var_106 = indirect_placeholder_100(var_104);
                    rdx_0 = var_106.field_1;
                    rax_0 = var_106.field_0;
                    local_sp_6 = var_105;
                }
                break;
            }
            var_107 = (*(unsigned char *)4486457UL == '\x00');
            var_108 = *var_14;
            if (var_107) {
                storemerge = *(uint64_t *)(var_108 + 64UL) << 9UL;
            } else {
                var_109 = *(uint64_t *)(var_108 + 48UL);
                storemerge = ((long)var_109 > (long)0UL) ? var_109 : 0UL;
            }
            var_110 = var_0 + (-120L);
            var_111 = local_sp_6 + (-8L);
            *(uint64_t *)var_111 = 4212102UL;
            indirect_placeholder_7(rdx_0, rax_0, storemerge, var_110);
            var_112 = *(uint64_t *)(*var_8 + 88UL);
            var_113 = (uint64_t *)(var_0 + (-72L));
            *var_113 = var_112;
            var_114 = *(uint64_t *)var_110;
            var_115 = *(uint64_t *)(var_0 + (-112L));
            var_116 = var_0 + (-152L);
            *(uint64_t *)var_116 = var_114;
            *(uint64_t *)(var_0 + (-144L)) = var_115;
            var_117 = *(uint64_t *)(var_0 + (-96L));
            *(uint64_t *)(var_0 + (-136L)) = *(uint64_t *)(var_0 + (-104L));
            *(uint64_t *)(var_0 + (-128L)) = var_117;
            var_118 = *(uint64_t *)4486560UL;
            local_sp_1 = var_111;
            local_sp_3 = var_111;
            local_sp_4 = var_111;
            if (var_118 == 0UL) {
                var_151 = *var_113 + 10UL;
                *(uint64_t *)4486560UL = var_151;
                var_152 = local_sp_6 + (-16L);
                *(uint64_t *)var_152 = 4212202UL;
                var_153 = indirect_placeholder_85(var_1, 64UL, var_151, r85_4, r96_8, var_3);
                var_154 = var_153.field_1;
                *(uint64_t *)4486568UL = var_153.field_0;
                r96_2 = var_154;
                local_sp_4 = var_152;
            } else {
                var_119 = *(uint64_t *)4486448UL;
                var_120 = *var_113;
                var_122 = var_119;
                if (var_120 != var_119) {
                    if (var_120 > var_119) {
                        var_138 = helper_cc_compute_c_wrapper(var_120 - var_118, var_118, var_4, 17U);
                        if (var_138 == 0UL) {
                            var_139 = *(uint64_t *)4486568UL;
                            var_140 = *var_113;
                            var_141 = local_sp_6 + (-16L);
                            *(uint64_t *)var_141 = 4212288UL;
                            var_142 = indirect_placeholder_86(128UL, var_140, var_139, r85_4, r96_8);
                            var_143 = var_142.field_1;
                            *(uint64_t *)4486568UL = var_142.field_0;
                            *(uint64_t *)4486560UL = (*var_113 << 1UL);
                            r96_1 = var_143;
                            local_sp_1 = var_141;
                        }
                        var_144 = *(uint64_t *)4486448UL + 1UL;
                        var_145 = (uint64_t *)(var_0 + (-32L));
                        *var_145 = var_144;
                        var_146 = var_144;
                        local_sp_2 = local_sp_1;
                        r96_2 = r96_1;
                        local_sp_4 = local_sp_2;
                        while (var_146 <= *var_113)
                            {
                                var_147 = *(uint64_t *)4486568UL + (var_146 << 6UL);
                                *(uint64_t *)(local_sp_2 + (-8L)) = 4212352UL;
                                indirect_placeholder_15(var_147);
                                var_148 = (*(uint64_t *)4486568UL + (*var_145 << 6UL)) + 32UL;
                                var_149 = local_sp_2 + (-16L);
                                *(uint64_t *)var_149 = 4212382UL;
                                indirect_placeholder_15(var_148);
                                var_150 = *var_145 + 1UL;
                                *var_145 = var_150;
                                var_146 = var_150;
                                local_sp_2 = var_149;
                                local_sp_4 = local_sp_2;
                            }
                    }
                    if (var_120 == (var_119 + (-1L))) {
                        var_121 = local_sp_6 + (-16L);
                        *(uint64_t *)var_121 = 4212444UL;
                        indirect_placeholder();
                        var_122 = *(uint64_t *)4486448UL;
                        local_sp_3 = var_121;
                    }
                    var_123 = *(uint64_t *)4486568UL + (var_122 << 6UL);
                    var_124 = local_sp_3 + (-8L);
                    *(uint64_t *)var_124 = 4212486UL;
                    var_125 = indirect_placeholder_6(var_123, var_116, r96_8);
                    r96_0 = var_125;
                    local_sp_0 = var_124;
                    if (*(unsigned char *)4486462UL != '\x01') {
                        var_126 = (*(uint64_t *)4486568UL + (*(uint64_t *)4486448UL << 6UL)) + 32UL;
                        var_127 = local_sp_3 + (-16L);
                        *(uint64_t *)var_127 = 4212543UL;
                        var_128 = indirect_placeholder_6(var_126, var_116, var_125);
                        r96_0 = var_128;
                        local_sp_0 = var_127;
                    }
                    var_129 = *(uint64_t *)4486568UL;
                    var_130 = var_129 + (*(uint64_t *)4486448UL << 6UL);
                    var_131 = (var_129 + (*var_113 << 6UL)) + 32UL;
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4212600UL;
                    var_132 = indirect_placeholder_6(var_130, var_131, r96_0);
                    var_133 = *(uint64_t *)4486568UL;
                    var_134 = (var_133 + (*(uint64_t *)4486448UL << 6UL)) + 32UL;
                    var_135 = (var_133 + (*var_113 << 6UL)) + 32UL;
                    var_136 = local_sp_0 + (-16L);
                    *(uint64_t *)var_136 = 4212658UL;
                    var_137 = indirect_placeholder_6(var_134, var_135, var_132);
                    r96_2 = var_137;
                    local_sp_4 = var_136;
                }
            }
            *(uint64_t *)4486448UL = *var_113;
            r96_3 = r96_2;
            local_sp_5 = local_sp_4;
            if (*(unsigned char *)4486462UL != '\x01') {
                var_155 = *(uint64_t *)4486568UL + (*var_113 << 6UL);
                var_156 = local_sp_4 + (-8L);
                *(uint64_t *)var_156 = 4212731UL;
                var_157 = indirect_placeholder_6(var_110, var_155, r96_2);
                r96_3 = var_157;
                local_sp_5 = var_156;
                if (*var_113 <= *(uint64_t *)4486112UL) {
                    if (*(unsigned char *)4486456UL != '\x00') {
                        var_159 = *(uint64_t *)4486112UL;
                        var_160 = *var_113;
                        var_161 = var_160;
                        if (var_160 <= var_159 & var_161 == 0UL) {
                            var_165 = (uint64_t)*var_9;
                            rax_1 = var_165;
                            mrv.field_0 = rax_1;
                            mrv1 = mrv;
                            mrv1.field_1 = r85_3;
                            mrv2 = mrv1;
                            mrv2.field_2 = r96_7;
                            return mrv2;
                        }
                    }
                    _pre263 = *var_113;
                    var_161 = _pre263;
                    if (var_161 != 0UL) {
                        var_165 = (uint64_t)*var_9;
                        rax_1 = var_165;
                        mrv.field_0 = rax_1;
                        mrv1 = mrv;
                        mrv1.field_1 = r85_3;
                        mrv2 = mrv1;
                        mrv2.field_2 = r96_7;
                        return mrv2;
                    }
                }
                storemerge2 = *(uint64_t *)(((*(unsigned char *)4486476UL == '\x00') ? var_5 : var_0) + (-144L));
                *(uint64_t *)(var_0 + (-80L)) = storemerge2;
                var_162 = *(uint64_t *)4486464UL;
                if ((long)var_162 > (long)18446744073709551615UL) {
                    var_163 = 0UL - var_162;
                    storemerge3 = (var_163 & (-256L)) | (storemerge2 <= var_163);
                } else {
                    var_164 = helper_cc_compute_c_wrapper(storemerge2 - var_162, var_162, var_4, 17U);
                    storemerge3 = ((uint64_t)(unsigned char)var_164 | (var_162 & (-256L))) ^ 1UL;
                }
                if ((uint64_t)(unsigned char)storemerge3 == 0UL) {
                    *(uint64_t *)(local_sp_5 + (-16L)) = 4212906UL;
                    indirect_placeholder_15(var_116);
                }
                var_165 = (uint64_t)*var_9;
                rax_1 = var_165;
                mrv.field_0 = rax_1;
                mrv1 = mrv;
                mrv1.field_1 = r85_3;
                mrv2 = mrv1;
                mrv2.field_2 = r96_7;
                return mrv2;
            }
            break;
        }
        break;
    }
}
