typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_xcalloc_ret_type;
struct indirect_placeholder_60_ret_type;
struct bb_xcalloc_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_60_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_60_ret_type indirect_placeholder_60(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
struct bb_xcalloc_ret_type bb_xcalloc(uint64_t rbx, uint64_t rsi, uint64_t rdi, uint64_t r8, uint64_t r9, uint64_t r10) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    unsigned __int128 var_6;
    uint64_t var_7;
    uint64_t var_8;
    bool var_9;
    uint64_t spec_select;
    uint64_t local_sp_0;
    struct bb_xcalloc_ret_type mrv;
    struct bb_xcalloc_ret_type mrv1;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    struct indirect_placeholder_60_ret_type var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t r85_0;
    uint64_t r96_0;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = var_0 + (-8L);
    *(uint64_t *)var_2 = var_1;
    var_3 = var_0 + (-40L);
    var_4 = (uint64_t *)(var_0 + (-32L));
    *var_4 = rdi;
    var_5 = (uint64_t *)var_3;
    *var_5 = rsi;
    var_6 = (unsigned __int128)rsi * (unsigned __int128)*var_4;
    var_7 = (uint64_t)var_6;
    var_8 = helper_cc_compute_all_wrapper(var_7, (uint64_t)(var_6 >> 64ULL), 0UL, 5U);
    var_9 = ((uint64_t)((uint16_t)var_8 & (unsigned short)2048U) == 0UL);
    spec_select = var_9 ? 0UL : 1UL;
    local_sp_0 = var_3;
    r85_0 = r8;
    r96_0 = r9;
    if (!(((long)var_7 > (long)18446744073709551615UL) && var_9)) {
        var_10 = *var_5;
        var_11 = *var_4;
        var_12 = var_0 + (-48L);
        *(uint64_t *)var_12 = 4251608UL;
        var_13 = indirect_placeholder_60(rbx, spec_select, var_10, var_11, r8, r9, r10);
        var_14 = var_13.field_1;
        var_15 = var_13.field_3;
        var_16 = var_13.field_4;
        *(uint64_t *)(var_0 + (-16L)) = var_14;
        local_sp_0 = var_12;
        r85_0 = var_15;
        r96_0 = var_16;
        if (var_14 != 0UL) {
            mrv.field_0 = var_14;
            mrv1 = mrv;
            mrv1.field_1 = var_16;
            return mrv1;
        }
    }
    *(uint64_t *)(local_sp_0 + (-8L)) = 4251624UL;
    indirect_placeholder_8(var_2, r85_0, r96_0);
    abort();
    *(uint64_t *)(local_sp_0 + (-8L)) = 4251624UL;
    indirect_placeholder_8(var_2, r85_0, r96_0);
    abort();
}
