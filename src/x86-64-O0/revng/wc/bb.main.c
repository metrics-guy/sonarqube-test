typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_24_ret_type;
struct indirect_placeholder_27_ret_type;
struct indirect_placeholder_26_ret_type;
struct indirect_placeholder_28_ret_type;
struct indirect_placeholder_29_ret_type;
struct indirect_placeholder_25_ret_type;
struct indirect_placeholder_33_ret_type;
struct indirect_placeholder_32_ret_type;
struct indirect_placeholder_35_ret_type;
struct indirect_placeholder_34_ret_type;
struct indirect_placeholder_36_ret_type;
struct indirect_placeholder_24_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_27_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_26_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_28_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_29_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_25_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_33_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_32_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_35_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_34_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_36_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_6(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_15(uint64_t param_0);
extern uint64_t indirect_placeholder_5(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint64_t indirect_placeholder_9(uint64_t param_0);
extern void indirect_placeholder_20(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_24_ret_type indirect_placeholder_24(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_16(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_27_ret_type indirect_placeholder_27(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_26_ret_type indirect_placeholder_26(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_28_ret_type indirect_placeholder_28(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_29_ret_type indirect_placeholder_29(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_25_ret_type indirect_placeholder_25(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_33_ret_type indirect_placeholder_33(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_32_ret_type indirect_placeholder_32(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_35_ret_type indirect_placeholder_35(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_34_ret_type indirect_placeholder_34(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_36_ret_type indirect_placeholder_36(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_36_ret_type var_14;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint32_t *var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint32_t *var_10;
    uint64_t local_sp_17;
    uint64_t var_132;
    uint64_t local_sp_0;
    uint64_t local_sp_9;
    uint64_t local_sp_2;
    uint64_t var_124;
    uint64_t var_125;
    uint64_t var_126;
    uint64_t var_127;
    uint64_t var_128;
    uint64_t var_129;
    uint64_t r8_2;
    uint64_t local_sp_4;
    uint64_t r9_0;
    uint64_t r8_0;
    uint64_t rcx_0;
    uint64_t r9_3;
    unsigned char var_130;
    uint64_t var_131;
    uint64_t local_sp_16;
    uint64_t var_116;
    uint64_t var_117;
    uint64_t var_118;
    uint64_t local_sp_3;
    uint64_t r9_1;
    uint64_t r8_1;
    uint64_t rcx_1;
    uint64_t local_sp_1;
    unsigned char var_100;
    uint64_t var_82;
    struct indirect_placeholder_27_ret_type var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    struct indirect_placeholder_26_ret_type var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_78;
    uint64_t var_80;
    uint64_t var_81;
    uint32_t var_76;
    unsigned char var_112;
    uint64_t local_sp_15;
    uint64_t local_sp_10;
    uint64_t r9_2;
    uint64_t rcx_2;
    uint64_t r8_8;
    uint64_t var_113;
    uint64_t var_114;
    uint64_t var_115;
    uint64_t var_119;
    uint64_t var_120;
    uint64_t var_121;
    uint64_t var_122;
    uint64_t var_123;
    uint64_t var_77;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t var_107;
    uint64_t var_108;
    struct indirect_placeholder_28_ret_type var_109;
    uint64_t var_110;
    uint64_t var_27;
    uint64_t var_111;
    uint64_t rcx_9;
    uint64_t rcx_3;
    uint64_t local_sp_5;
    uint64_t var_79;
    uint64_t r8_3;
    uint64_t rcx_4;
    uint64_t local_sp_6;
    uint64_t var_98;
    struct indirect_placeholder_29_ret_type var_99;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t var_96;
    struct indirect_placeholder_25_ret_type var_97;
    uint64_t r9_4;
    uint64_t r8_4;
    uint64_t rcx_5;
    uint64_t local_sp_7;
    uint64_t storemerge4;
    uint64_t local_sp_8;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t r9_5;
    uint64_t r8_5;
    uint64_t rcx_6;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t rax_0;
    uint64_t *_pre_phi343;
    uint64_t r9_6;
    uint64_t r8_6;
    uint64_t rcx_7;
    uint64_t var_56;
    uint64_t *var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t *var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    unsigned char *var_65;
    uint32_t *var_66;
    unsigned char *var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t *var_70;
    uint32_t *var_71;
    uint64_t *var_72;
    uint64_t var_30;
    struct indirect_placeholder_33_ret_type var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    struct indirect_placeholder_32_ret_type var_37;
    uint64_t var_38;
    uint64_t *var_39;
    uint64_t *_pre_phi345;
    uint64_t r9_8;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t *var_43;
    uint64_t var_28;
    uint64_t *var_29;
    uint64_t var_20;
    unsigned char *var_21;
    uint64_t var_22;
    bool var_23;
    uint32_t var_24;
    bool var_25;
    uint64_t var_44;
    struct indirect_placeholder_35_ret_type var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_26;
    uint64_t *var_49;
    uint32_t var_50;
    uint32_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t *var_55;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t *var_19;
    uint32_t var_16;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint32_t var_15;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = var_0 + (-8L);
    *(uint64_t *)var_3 = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_4 = (uint32_t *)(var_0 + (-572L));
    *var_4 = (uint32_t)rdi;
    var_5 = var_0 + (-584L);
    var_6 = (uint64_t *)var_5;
    *var_6 = rsi;
    var_7 = (uint64_t *)(var_0 + (-56L));
    *var_7 = 0UL;
    var_8 = **(uint64_t **)var_5;
    *(uint64_t *)(var_0 + (-608L)) = 4209306UL;
    indirect_placeholder_15(var_8);
    *(uint64_t *)(var_0 + (-616L)) = 4209321UL;
    indirect_placeholder();
    *(uint64_t *)(var_0 + (-624L)) = 4209331UL;
    indirect_placeholder();
    *(uint64_t *)(var_0 + (-632L)) = 4209336UL;
    indirect_placeholder();
    *(uint64_t *)4310056UL = (uint64_t)((long)(var_8 << 32UL) >> (long)32UL);
    var_9 = var_0 + (-640L);
    *(uint64_t *)var_9 = 4209375UL;
    indirect_placeholder();
    *(unsigned char *)4310043UL = (unsigned char)'\x00';
    *(unsigned char *)4310042UL = (unsigned char)'\x00';
    *(unsigned char *)4310041UL = (unsigned char)'\x00';
    *(unsigned char *)4310040UL = (unsigned char)'\x00';
    *(unsigned char *)4310044UL = (unsigned char)'\x00';
    *(uint64_t *)4310032UL = 0UL;
    *(uint64_t *)4310024UL = 0UL;
    *(uint64_t *)4310016UL = 0UL;
    *(uint64_t *)4310008UL = 0UL;
    *(uint64_t *)4310000UL = 0UL;
    var_10 = (uint32_t *)(var_0 + (-92L));
    local_sp_17 = var_9;
    rcx_1 = 4286935UL;
    var_100 = (unsigned char)'\x01';
    var_112 = (unsigned char)'\x00';
    r8_8 = 0UL;
    rcx_9 = 4284928UL;
    storemerge4 = 0UL;
    rax_0 = 4310064UL;
    r8_6 = 0UL;
    rcx_7 = 4284928UL;
    while (1U)
        {
            var_11 = *var_6;
            var_12 = (uint64_t)*var_4;
            var_13 = local_sp_17 + (-8L);
            *(uint64_t *)var_13 = 4209777UL;
            var_14 = indirect_placeholder_36(4284928UL, 4286661UL, var_12, var_11, 0UL);
            var_15 = (uint32_t)var_14.field_0;
            *var_10 = var_15;
            local_sp_16 = var_13;
            local_sp_17 = var_13;
            if (var_15 != 4294967295U) {
                var_20 = var_14.field_1;
                r9_6 = var_20;
                r9_8 = var_20;
                if (*(unsigned char *)4310040UL != '\x01') {
                    loop_state_var = 1U;
                    break;
                }
                if (*(unsigned char *)4310041UL != '\x01') {
                    loop_state_var = 1U;
                    break;
                }
                if (*(unsigned char *)4310042UL != '\x01') {
                    loop_state_var = 1U;
                    break;
                }
                if (*(unsigned char *)4310043UL != '\x01') {
                    loop_state_var = 1U;
                    break;
                }
                if (*(unsigned char *)4310044UL != '\x01') {
                    loop_state_var = 1U;
                    break;
                }
                *(unsigned char *)4310043UL = (unsigned char)'\x01';
                *(unsigned char *)4310041UL = (unsigned char)'\x01';
                *(unsigned char *)4310040UL = (unsigned char)'\x01';
                loop_state_var = 1U;
                break;
            }
            if ((int)var_15 <= (int)128U) {
                loop_state_var = 0U;
                break;
            }
            if ((int)var_15 >= (int)99U) {
                var_16 = var_15 + (-99);
                if (var_16 <= 29U) {
                    loop_state_var = 0U;
                    break;
                }
                switch_state_var = 0;
                switch (*(uint64_t *)(((uint64_t)var_16 << 3UL) + 4286984UL)) {
                  case 4209731UL:
                    {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 4209636UL:
                  case 4209618UL:
                  case 4209609UL:
                  case 4209597UL:
                  case 4209585UL:
                    {
                        switch (*(uint64_t *)(((uint64_t)var_16 << 3UL) + 4286984UL)) {
                          case 4209597UL:
                            {
                                *(unsigned char *)4310042UL = (unsigned char)'\x01';
                            }
                            break;
                          case 4209585UL:
                            {
                                *(unsigned char *)4310043UL = (unsigned char)'\x01';
                            }
                            break;
                          case 4209609UL:
                            {
                                *(unsigned char *)4310040UL = (unsigned char)'\x01';
                            }
                            break;
                          case 4209618UL:
                            {
                                *(unsigned char *)4310041UL = (unsigned char)'\x01';
                            }
                            break;
                          case 4209636UL:
                            {
                                *var_7 = *(uint64_t *)4310520UL;
                            }
                            break;
                        }
                        continue;
                    }
                    break;
                  default:
                    {
                        abort();
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
            if (var_15 == 76U) {
                *(unsigned char *)4310044UL = (unsigned char)'\x01';
                continue;
            }
            if ((int)var_15 <= (int)76U) {
                loop_state_var = 0U;
                break;
            }
            switch_state_var = 0;
            switch (var_15) {
              case 4294967166U:
                {
                    *(uint64_t *)(local_sp_17 + (-16L)) = 4209659UL;
                    indirect_placeholder_6(var_3, 0UL);
                    abort();
                }
                break;
              case 4294967165U:
                {
                    var_17 = *(uint64_t *)4309696UL;
                    var_18 = local_sp_17 + (-24L);
                    var_19 = (uint64_t *)var_18;
                    *var_19 = 0UL;
                    *(uint64_t *)(local_sp_17 + (-32L)) = 4209717UL;
                    indirect_placeholder_16(0UL, var_17, 4284624UL, 4286222UL, 4286634UL, 4286650UL);
                    *var_19 = 4209731UL;
                    indirect_placeholder();
                    local_sp_16 = var_18;
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              default:
                {
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(uint64_t *)(local_sp_16 + (-8L)) = 4209741UL;
            indirect_placeholder_6(var_3, 1UL);
            abort();
        }
        break;
      case 1U:
        {
            var_21 = (unsigned char *)(var_0 + (-57L));
            *var_21 = (unsigned char)'\x00';
            var_22 = *var_7;
            var_23 = (var_22 == 0UL);
            var_24 = *(uint32_t *)4309848UL;
            var_25 = ((int)var_24 < (int)*var_4);
            if (var_23) {
                if (var_25) {
                    rax_0 = *var_6 + ((uint64_t)var_24 << 3UL);
                }
                var_49 = (uint64_t *)(var_0 + (-48L));
                *var_49 = rax_0;
                var_50 = *(uint32_t *)4309848UL;
                var_51 = *var_4;
                *(uint64_t *)(var_0 + (-40L)) = (((int)var_50 < (int)var_51) ? (uint64_t)((long)(((uint64_t)var_51 - (uint64_t)var_50) << 32UL) >> (long)32UL) : 1UL);
                var_52 = *var_49;
                var_53 = local_sp_17 + (-16L);
                *(uint64_t *)var_53 = 4210587UL;
                var_54 = indirect_placeholder_9(var_52);
                var_55 = (uint64_t *)(var_0 + (-72L));
                *var_55 = var_54;
                _pre_phi343 = var_55;
                local_sp_10 = var_53;
            } else {
                if (var_25) {
                    var_44 = *(uint64_t *)(*var_6 + ((uint64_t)var_24 << 3UL));
                    *(uint64_t *)(local_sp_17 + (-16L)) = 4209964UL;
                    var_45 = indirect_placeholder_35(4UL, var_44);
                    var_46 = var_45.field_0;
                    var_47 = var_45.field_1;
                    var_48 = var_45.field_2;
                    *(uint64_t *)(local_sp_17 + (-24L)) = 4209992UL;
                    indirect_placeholder_34(0UL, var_46, 4286667UL, 0UL, 0UL, var_47, var_48);
                    *(uint64_t *)(local_sp_17 + (-32L)) = 4210022UL;
                    indirect_placeholder();
                    *(uint64_t *)(local_sp_17 + (-40L)) = 4210032UL;
                    indirect_placeholder_6(var_3, 1UL);
                    abort();
                }
                var_26 = local_sp_17 + (-16L);
                *(uint64_t *)var_26 = 4210049UL;
                indirect_placeholder();
                local_sp_15 = var_26;
                if ((uint64_t)(uint32_t)var_22 == 0UL) {
                    var_38 = *(uint64_t *)4308552UL;
                    var_39 = (uint64_t *)(var_0 + (-80L));
                    *var_39 = var_38;
                    _pre_phi345 = var_39;
                } else {
                    var_27 = *var_7;
                    var_28 = local_sp_17 + (-24L);
                    *(uint64_t *)var_28 = 4210083UL;
                    indirect_placeholder();
                    var_29 = (uint64_t *)(var_0 + (-80L));
                    *var_29 = var_27;
                    _pre_phi345 = var_29;
                    local_sp_15 = var_28;
                    if (var_27 == 0UL) {
                        var_30 = *var_7;
                        *(uint64_t *)(local_sp_17 + (-32L)) = 4210111UL;
                        var_31 = indirect_placeholder_33(4UL, var_30);
                        var_32 = var_31.field_0;
                        var_33 = var_31.field_1;
                        var_34 = var_31.field_2;
                        *(uint64_t *)(local_sp_17 + (-40L)) = 4210119UL;
                        indirect_placeholder();
                        var_35 = (uint64_t)*(uint32_t *)var_32;
                        var_36 = local_sp_17 + (-48L);
                        *(uint64_t *)var_36 = 4210146UL;
                        var_37 = indirect_placeholder_32(0UL, var_32, 4286746UL, 1UL, var_35, var_33, var_34);
                        r9_8 = var_37.field_0;
                        r8_8 = var_37.field_1;
                        rcx_9 = var_32;
                        local_sp_15 = var_36;
                    }
                }
                *(uint64_t *)(local_sp_15 + (-8L)) = 4210158UL;
                indirect_placeholder();
                *(uint64_t *)(local_sp_15 + (-16L)) = 4210177UL;
                indirect_placeholder();
                *(uint64_t *)(var_0 + (-48L)) = 0UL;
                *(uint64_t *)(var_0 + (-40L)) = 0UL;
                var_40 = *_pre_phi345;
                var_41 = local_sp_15 + (-24L);
                *(uint64_t *)var_41 = 4210475UL;
                var_42 = indirect_placeholder_9(var_40);
                var_43 = (uint64_t *)(var_0 + (-72L));
                *var_43 = var_42;
                local_sp_10 = var_41;
                _pre_phi343 = var_43;
                r9_6 = r9_8;
                r8_6 = r8_8;
                rcx_7 = rcx_9;
            }
            r9_5 = r9_6;
            r8_5 = r8_6;
            rcx_6 = rcx_7;
            if (*_pre_phi343 == 0UL) {
                *(uint64_t *)(local_sp_10 + (-8L)) = 4210603UL;
                indirect_placeholder_20(var_3, r9_6, r8_6);
                abort();
            }
            var_56 = *(uint64_t *)(var_0 + (-48L));
            var_57 = (uint64_t *)(var_0 + (-40L));
            var_58 = *var_57;
            *(uint64_t *)(local_sp_10 + (-8L)) = 4210622UL;
            var_59 = indirect_placeholder_5(var_58, var_56, r9_6, r8_6);
            var_60 = var_0 + (-104L);
            var_61 = (uint64_t *)var_60;
            *var_61 = var_59;
            var_62 = *var_57;
            var_63 = local_sp_10 + (-16L);
            *(uint64_t *)var_63 = 4210645UL;
            var_64 = indirect_placeholder_1(var_62, var_59);
            *(uint32_t *)4310048UL = (uint32_t)var_64;
            var_65 = (unsigned char *)(var_0 + (-25L));
            *var_65 = (unsigned char)'\x01';
            var_66 = (uint32_t *)(var_0 + (-84L));
            *var_66 = 0U;
            var_67 = (unsigned char *)(var_0 + (-85L));
            var_68 = var_0 + (-412L);
            var_69 = var_0 + (-112L);
            var_70 = (uint64_t *)var_69;
            var_71 = (uint32_t *)var_68;
            var_72 = (uint64_t *)(var_0 + (-120L));
            local_sp_9 = var_63;
            while (1U)
                {
                    *var_67 = (unsigned char)'\x00';
                    var_73 = *_pre_phi343;
                    var_74 = local_sp_9 + (-8L);
                    *(uint64_t *)var_74 = 4210688UL;
                    var_75 = indirect_placeholder_1(var_73, var_68);
                    *var_70 = var_75;
                    local_sp_2 = var_74;
                    r8_2 = r8_5;
                    r9_3 = r9_5;
                    r9_2 = r9_5;
                    rcx_2 = rcx_6;
                    rcx_3 = rcx_6;
                    local_sp_5 = var_74;
                    r8_3 = r8_5;
                    if (var_75 == 0UL) {
                        var_78 = *var_7;
                        rcx_4 = rcx_3;
                        local_sp_6 = local_sp_5;
                        var_79 = local_sp_5 + (-8L);
                        *(uint64_t *)var_79 = 4210855UL;
                        indirect_placeholder();
                        local_sp_6 = var_79;
                        var_80 = *var_70;
                        var_81 = local_sp_5 + (-16L);
                        *(uint64_t *)var_81 = 4210876UL;
                        indirect_placeholder();
                        local_sp_6 = var_81;
                        if (var_78 != 0UL & (uint64_t)(uint32_t)var_78 != 0UL & (uint64_t)(uint32_t)var_80 == 0UL) {
                            var_82 = *var_70;
                            *(uint64_t *)(local_sp_5 + (-24L)) = 4210897UL;
                            var_83 = indirect_placeholder_27(4UL, var_82);
                            var_84 = var_83.field_0;
                            var_85 = var_83.field_1;
                            var_86 = var_83.field_2;
                            var_87 = local_sp_5 + (-32L);
                            *(uint64_t *)var_87 = 4210925UL;
                            var_88 = indirect_placeholder_26(0UL, var_84, 4286872UL, 0UL, 0UL, var_85, var_86);
                            var_89 = var_88.field_0;
                            var_90 = var_88.field_1;
                            *var_67 = (unsigned char)'\x01';
                            r9_3 = var_89;
                            r8_3 = var_90;
                            rcx_4 = var_84;
                            local_sp_6 = var_87;
                        }
                        r9_4 = r9_3;
                        r8_4 = r8_3;
                        rcx_5 = rcx_4;
                        local_sp_7 = local_sp_6;
                        if (**(unsigned char **)var_69 == '\x00') {
                            var_100 = *var_67;
                        } else {
                            if (*var_7 == 0UL) {
                                var_98 = local_sp_6 + (-8L);
                                *(uint64_t *)var_98 = 4210977UL;
                                var_99 = indirect_placeholder_29(0UL, 4286935UL, 4286244UL, 0UL, 0UL, r9_3, r8_3);
                                r9_1 = var_99.field_0;
                                r8_1 = var_99.field_1;
                                local_sp_1 = var_98;
                            } else {
                                var_91 = *_pre_phi343;
                                *(uint64_t *)(local_sp_6 + (-8L)) = 4210991UL;
                                var_92 = indirect_placeholder_9(var_91);
                                *var_72 = var_92;
                                var_93 = *var_7;
                                *(uint64_t *)(local_sp_6 + (-16L)) = 4211017UL;
                                var_94 = indirect_placeholder_3(var_93, 0UL, 3UL);
                                var_95 = *var_72;
                                var_96 = local_sp_6 + (-24L);
                                *(uint64_t *)var_96 = 4211058UL;
                                var_97 = indirect_placeholder_25(0UL, var_94, 4286965UL, 0UL, 0UL, 4286935UL, var_95);
                                r9_1 = var_97.field_0;
                                r8_1 = var_97.field_1;
                                rcx_1 = var_94;
                                local_sp_1 = var_96;
                            }
                            *var_67 = (unsigned char)'\x01';
                            r9_4 = r9_1;
                            r8_4 = r8_1;
                            rcx_5 = rcx_1;
                            local_sp_7 = local_sp_1;
                        }
                        local_sp_8 = local_sp_7;
                        r9_5 = r9_4;
                        r8_5 = r8_4;
                        rcx_6 = rcx_5;
                        if (var_100 == '\x00') {
                            *var_65 = (unsigned char)'\x00';
                        } else {
                            if (*var_57 != 0UL) {
                                storemerge4 = (uint64_t)*var_66 * 152UL;
                            }
                            var_101 = storemerge4 + *var_61;
                            var_102 = *var_70;
                            var_103 = local_sp_7 + (-8L);
                            *(uint64_t *)var_103 = 4211139UL;
                            var_104 = indirect_placeholder_1(var_102, var_101);
                            *var_65 = ((var_104 & (uint64_t)*var_65) != 0UL);
                            local_sp_8 = var_103;
                        }
                        local_sp_9 = local_sp_8;
                        if (*var_57 == 0UL) {
                            **(uint32_t **)var_60 = 1U;
                        }
                        *var_66 = (*var_66 + 1U);
                        continue;
                    }
                    var_76 = *var_71;
                    rcx_3 = 4287229UL;
                    if ((uint64_t)(var_76 + (-4)) != 0UL) {
                        var_105 = *var_7;
                        *(uint64_t *)(local_sp_9 + (-16L)) = 4210757UL;
                        var_106 = indirect_placeholder_3(var_105, 0UL, 3UL);
                        *(uint64_t *)(local_sp_9 + (-24L)) = 4210765UL;
                        indirect_placeholder();
                        var_107 = (uint64_t)*(uint32_t *)var_106;
                        var_108 = local_sp_9 + (-32L);
                        *(uint64_t *)var_108 = 4210792UL;
                        var_109 = indirect_placeholder_28(0UL, var_106, 4286807UL, 0UL, var_107, r9_5, r8_5);
                        var_110 = var_109.field_0;
                        var_111 = var_109.field_1;
                        *var_65 = (unsigned char)'\x00';
                        r9_2 = var_110;
                        r8_2 = var_111;
                        rcx_2 = var_106;
                        local_sp_2 = var_108;
                        break;
                    }
                    if (var_76 <= 4U) {
                        if ((uint64_t)(var_76 + (-2)) != 0UL) {
                            var_112 = *var_65;
                            break;
                        }
                        if ((uint64_t)(var_76 + (-3)) != 0UL) {
                            *(uint64_t *)(local_sp_9 + (-16L)) = 4210806UL;
                            indirect_placeholder_20(var_3, r9_5, r8_5);
                            abort();
                        }
                    }
                    var_77 = local_sp_9 + (-16L);
                    *(uint64_t *)var_77 = 4210831UL;
                    indirect_placeholder();
                    local_sp_5 = var_77;
                }
            r9_0 = r9_2;
            r8_0 = r8_2;
            rcx_0 = rcx_2;
            local_sp_3 = local_sp_2;
            var_113 = *_pre_phi343;
            var_114 = local_sp_2 + (-8L);
            *(uint64_t *)var_114 = 4211208UL;
            var_115 = indirect_placeholder_9(var_113);
            local_sp_3 = var_114;
            if (var_112 != '\x00' & *var_7 != 0UL & var_115 == 0UL) {
                var_116 = *var_61;
                var_117 = local_sp_2 + (-16L);
                *(uint64_t *)var_117 = 4211230UL;
                var_118 = indirect_placeholder_1(0UL, var_116);
                *var_65 = ((var_118 & (uint64_t)*var_65) != 0UL);
                local_sp_3 = var_117;
            }
            local_sp_4 = local_sp_3;
            if (*var_21 == '\x00') {
                var_119 = var_0 + (-408L);
                var_120 = local_sp_3 + (-8L);
                *(uint64_t *)var_120 = 4211268UL;
                indirect_placeholder_15(var_119);
                local_sp_4 = var_120;
            }
            var_121 = *_pre_phi343;
            var_122 = local_sp_4 + (-8L);
            *(uint64_t *)var_122 = 4211280UL;
            var_123 = indirect_placeholder_9(var_121);
            local_sp_0 = var_122;
            if (var_123 > 1UL) {
                var_124 = *(uint64_t *)4310032UL;
                var_125 = *(uint64_t *)4310024UL;
                var_126 = *(uint64_t *)4310016UL;
                var_127 = *(uint64_t *)4310008UL;
                var_128 = *(uint64_t *)4310000UL;
                var_129 = local_sp_4 + (-16L);
                *(uint64_t *)var_129 = 4211338UL;
                indirect_placeholder_16(var_125, var_126, var_128, var_127, 4286976UL, var_124);
                r9_0 = 4286976UL;
                r8_0 = var_124;
                rcx_0 = var_125;
                local_sp_0 = var_129;
            }
            *(uint64_t *)(local_sp_0 + (-8L)) = 4211350UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_0 + (-16L)) = 4211362UL;
            indirect_placeholder();
            var_130 = *(unsigned char *)4310052UL;
            var_131 = (uint64_t)var_130;
            if (var_130 != '\x00') {
                *(uint64_t *)(local_sp_0 + (-24L)) = 4211383UL;
                indirect_placeholder();
                *(uint64_t *)(local_sp_0 + (-32L)) = 4211392UL;
                indirect_placeholder();
                var_132 = (uint64_t)*(uint32_t *)var_131;
                *(uint64_t *)(local_sp_0 + (-40L)) = 4211416UL;
                indirect_placeholder_24(0UL, rcx_0, 4286632UL, 1UL, var_132, r9_0, r8_0);
            }
            return;
        }
        break;
    }
}
