typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_18_ret_type;
struct indirect_placeholder_18_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_9(uint64_t param_0);
extern struct indirect_placeholder_18_ret_type indirect_placeholder_18(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
uint64_t bb_get_input_fstatus(uint64_t rdi, uint64_t rsi, uint64_t r9, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t *var_4;
    uint64_t var_5;
    uint64_t spec_select;
    uint64_t var_6;
    struct indirect_placeholder_18_ret_type var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t local_sp_1;
    uint64_t var_21;
    uint64_t local_sp_0;
    uint64_t storemerge_in;
    uint64_t var_24;
    uint64_t var_12;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t *var_11;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t _pre;
    uint64_t local_sp_2;
    uint64_t var_22;
    uint64_t var_23;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_cc_src2();
    var_2 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    var_3 = (uint64_t *)(var_0 + (-32L));
    *var_3 = rdi;
    var_4 = (uint64_t *)(var_0 + (-40L));
    *var_4 = rsi;
    var_5 = *var_3;
    spec_select = (var_5 == 0UL) ? 1UL : var_5;
    var_6 = var_0 + (-48L);
    *(uint64_t *)var_6 = 4208600UL;
    var_7 = indirect_placeholder_18(spec_select, 152UL, r9, r8);
    var_8 = var_7.field_0;
    var_9 = var_0 + (-24L);
    var_10 = (uint64_t *)var_9;
    *var_10 = var_8;
    var_12 = 0UL;
    local_sp_1 = var_6;
    switch (*var_3) {
      case 0UL:
        {
            **(uint32_t **)var_9 = 1U;
        }
        break;
      case 1UL:
        {
            if ((uint64_t)((((((uint32_t)(uint64_t)*(unsigned char *)4310040UL + (uint32_t)(uint64_t)*(unsigned char *)4310041UL) + (uint32_t)(uint64_t)*(unsigned char *)4310042UL) + (uint32_t)(uint64_t)*(unsigned char *)4310043UL) + (uint32_t)(uint64_t)*(unsigned char *)4310044UL) + (-1)) != 0UL) {
                **(uint32_t **)var_9 = 1U;
                return *var_10;
            }
            var_11 = (uint64_t *)(var_0 + (-16L));
            *var_11 = 0UL;
            var_13 = *var_3;
            var_14 = helper_cc_compute_c_wrapper(var_12 - var_13, var_13, var_1, 17U);
            local_sp_2 = local_sp_1;
            while (var_14 != 0UL)
                {
                    var_15 = *var_11;
                    var_16 = *(uint64_t *)(*var_4 + (var_15 << 3UL));
                    var_21 = var_15;
                    if (var_16 == 0UL) {
                        var_22 = (*var_10 + (var_21 * 152UL)) + 8UL;
                        var_23 = local_sp_2 + (-8L);
                        *(uint64_t *)var_23 = 4208826UL;
                        indirect_placeholder();
                        local_sp_0 = var_23;
                        storemerge_in = var_22;
                    } else {
                        var_17 = local_sp_1 + (-8L);
                        *(uint64_t *)var_17 = 4208771UL;
                        indirect_placeholder();
                        local_sp_2 = var_17;
                        if ((uint64_t)(uint32_t)var_16 == 0UL) {
                            var_18 = *(uint64_t *)(*var_4 + (*var_11 << 3UL));
                            var_19 = local_sp_1 + (-16L);
                            *(uint64_t *)var_19 = 4208901UL;
                            var_20 = indirect_placeholder_9(var_18);
                            local_sp_0 = var_19;
                            storemerge_in = var_20;
                        } else {
                            _pre = *var_11;
                            var_21 = _pre;
                            var_22 = (*var_10 + (var_21 * 152UL)) + 8UL;
                            var_23 = local_sp_2 + (-8L);
                            *(uint64_t *)var_23 = 4208826UL;
                            indirect_placeholder();
                            local_sp_0 = var_23;
                            storemerge_in = var_22;
                        }
                    }
                    *(uint32_t *)(*var_10 + (*var_11 * 152UL)) = (uint32_t)storemerge_in;
                    var_24 = *var_11 + 1UL;
                    *var_11 = var_24;
                    var_12 = var_24;
                    local_sp_1 = local_sp_0;
                    var_13 = *var_3;
                    var_14 = helper_cc_compute_c_wrapper(var_12 - var_13, var_13, var_1, 17U);
                    local_sp_2 = local_sp_1;
                }
        }
        break;
      default:
        {
            var_11 = (uint64_t *)(var_0 + (-16L));
            *var_11 = 0UL;
            var_13 = *var_3;
            var_14 = helper_cc_compute_c_wrapper(var_12 - var_13, var_13, var_1, 17U);
            local_sp_2 = local_sp_1;
            while (var_14 != 0UL)
                {
                    var_15 = *var_11;
                    var_16 = *(uint64_t *)(*var_4 + (var_15 << 3UL));
                    var_21 = var_15;
                    if (var_16 == 0UL) {
                        var_22 = (*var_10 + (var_21 * 152UL)) + 8UL;
                        var_23 = local_sp_2 + (-8L);
                        *(uint64_t *)var_23 = 4208826UL;
                        indirect_placeholder();
                        local_sp_0 = var_23;
                        storemerge_in = var_22;
                    } else {
                        var_17 = local_sp_1 + (-8L);
                        *(uint64_t *)var_17 = 4208771UL;
                        indirect_placeholder();
                        local_sp_2 = var_17;
                        if ((uint64_t)(uint32_t)var_16 == 0UL) {
                            var_18 = *(uint64_t *)(*var_4 + (*var_11 << 3UL));
                            var_19 = local_sp_1 + (-16L);
                            *(uint64_t *)var_19 = 4208901UL;
                            var_20 = indirect_placeholder_9(var_18);
                            local_sp_0 = var_19;
                            storemerge_in = var_20;
                        } else {
                            _pre = *var_11;
                            var_21 = _pre;
                            var_22 = (*var_10 + (var_21 * 152UL)) + 8UL;
                            var_23 = local_sp_2 + (-8L);
                            *(uint64_t *)var_23 = 4208826UL;
                            indirect_placeholder();
                            local_sp_0 = var_23;
                            storemerge_in = var_22;
                        }
                    }
                    *(uint32_t *)(*var_10 + (*var_11 * 152UL)) = (uint32_t)storemerge_in;
                    var_24 = *var_11 + 1UL;
                    *var_11 = var_24;
                    var_12 = var_24;
                    local_sp_1 = local_sp_0;
                    var_13 = *var_3;
                    var_14 = helper_cc_compute_c_wrapper(var_12 - var_13, var_13, var_1, 17U);
                    local_sp_2 = local_sp_1;
                }
        }
        break;
    }
    return *var_10;
}
