typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct helper_divq_EAX_wrapper_ret_type;
struct type_6;
struct indirect_placeholder_10_ret_type;
struct indirect_placeholder_12_ret_type;
struct indirect_placeholder_13_ret_type;
struct indirect_placeholder_14_ret_type;
struct helper_divq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint32_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint32_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint32_t field_7;
    uint64_t field_8;
    uint32_t field_9;
    uint32_t field_10;
    unsigned char field_11;
    uint32_t field_12;
};
struct type_6 {
};
struct indirect_placeholder_10_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_12_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_13_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_14_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern void function_dispatcher(unsigned char *param_0);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_r10(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_rbx(void);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern uint32_t init_state_0x8248(void);
extern struct helper_divq_EAX_wrapper_ret_type helper_divq_EAX_wrapper(struct type_6 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint64_t param_13, uint32_t param_14, uint64_t param_15, uint64_t param_16, uint64_t param_17, uint32_t param_18, uint32_t param_19);
extern void indirect_placeholder_15(uint64_t param_0);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern uint64_t indirect_placeholder_9(uint64_t param_0);
extern void indirect_placeholder_16(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct indirect_placeholder_10_ret_type indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_12_ret_type indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_13_ret_type indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_14_ret_type indirect_placeholder_14(uint64_t param_0);
typedef _Bool bool;
uint64_t bb_wc(uint64_t rcx, uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint32_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint32_t var_12;
    uint32_t var_13;
    uint64_t var_14;
    uint32_t *var_15;
    uint64_t *var_16;
    uint64_t var_17;
    uint64_t *var_18;
    uint64_t *var_19;
    unsigned char *var_20;
    uint64_t var_21;
    uint64_t spec_select;
    uint64_t *var_22;
    uint64_t *var_23;
    uint64_t *var_24;
    uint64_t *var_25;
    uint64_t *var_26;
    uint64_t *var_27;
    uint64_t var_28;
    uint64_t var_107;
    uint64_t local_sp_20;
    uint64_t var_115;
    uint64_t var_116;
    uint64_t var_117;
    uint64_t var_118;
    uint64_t var_119;
    uint64_t local_sp_0;
    uint64_t var_120;
    uint64_t local_sp_12;
    uint64_t local_sp_2;
    uint64_t local_sp_1;
    uint64_t var_125;
    uint64_t var_71;
    uint64_t var_121;
    uint64_t var_122;
    uint64_t var_123;
    uint64_t var_124;
    uint64_t var_108;
    uint32_t var_109;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t var_112;
    uint64_t var_113;
    uint64_t var_114;
    uint64_t var_98;
    unsigned char *var_99;
    uint64_t *var_100;
    uint64_t *var_101;
    uint64_t var_64;
    uint64_t var_102;
    uint64_t *var_103;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t local_sp_14;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t local_sp_13;
    uint64_t local_sp_21;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t local_sp_15;
    uint64_t var_129;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_47;
    uint64_t local_sp_19;
    uint64_t *var_48;
    uint64_t var_51;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t rdi3_0;
    uint64_t local_sp_18;
    uint64_t rsi4_0;
    uint64_t local_sp_16;
    uint64_t var_52;
    struct helper_divq_EAX_wrapper_ret_type var_53;
    uint64_t var_54;
    uint32_t var_55;
    uint64_t var_56;
    uint32_t var_57;
    uint32_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t storemerge9;
    uint64_t var_61;
    uint64_t storemerge10;
    uint64_t var_62;
    struct helper_divq_EAX_wrapper_ret_type var_63;
    uint64_t *var_65;
    uint64_t var_66;
    uint64_t var_67;
    unsigned char storemerge7;
    unsigned char *var_29;
    unsigned char *var_30;
    unsigned char storemerge;
    unsigned char *var_31;
    unsigned char var_32;
    unsigned char var_35;
    uint64_t rsi4_1;
    uint64_t local_sp_17;
    unsigned char var_78;
    unsigned char var_36;
    unsigned char *var_37;
    uint32_t **var_38;
    uint64_t var_39;
    uint64_t rsi4_2;
    uint64_t var_40;
    uint32_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    struct indirect_placeholder_14_ret_type var_45;
    uint64_t var_46;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t *var_70;
    uint64_t var_126;
    uint64_t var_127;
    uint64_t var_128;
    uint64_t var_130;
    uint64_t var_131;
    uint64_t var_132;
    uint64_t var_133;
    uint64_t var_134;
    uint64_t var_135;
    uint64_t var_136;
    unsigned char *var_79;
    uint64_t *var_80;
    uint64_t var_81;
    uint64_t *var_82;
    uint64_t *var_83;
    uint64_t *var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_33;
    uint64_t var_34;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_cc_src2();
    var_2 = init_rbp();
    var_3 = init_r10();
    var_4 = init_r9();
    var_5 = init_r8();
    var_6 = init_rbx();
    var_7 = init_state_0x9018();
    var_8 = init_state_0x9010();
    var_9 = init_state_0x8408();
    var_10 = init_state_0x8328();
    var_11 = init_state_0x82d8();
    var_12 = init_state_0x9080();
    var_13 = init_state_0x8248();
    var_14 = var_0 + (-8L);
    *(uint64_t *)var_14 = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    var_15 = (uint32_t *)(var_0 + (-16652L));
    *var_15 = (uint32_t)rdi;
    var_16 = (uint64_t *)(var_0 + (-16664L));
    *var_16 = rsi;
    var_17 = var_0 + (-16672L);
    var_18 = (uint64_t *)var_17;
    *var_18 = rdx;
    var_19 = (uint64_t *)(var_0 + (-16680L));
    *var_19 = rcx;
    var_20 = (unsigned char *)(var_0 + (-25L));
    *var_20 = (unsigned char)'\x01';
    var_21 = *var_16;
    spec_select = (var_21 == 0UL) ? 4286229UL : var_21;
    var_22 = (uint64_t *)(var_0 + (-176L));
    *var_22 = spec_select;
    var_23 = (uint64_t *)(var_0 + (-80L));
    *var_23 = 0UL;
    var_24 = (uint64_t *)(var_0 + (-72L));
    *var_24 = 0UL;
    var_25 = (uint64_t *)(var_0 + (-64L));
    *var_25 = 0UL;
    var_26 = (uint64_t *)(var_0 + (-56L));
    *var_26 = 0UL;
    var_27 = (uint64_t *)(var_0 + (-48L));
    *var_27 = 0UL;
    var_28 = var_0 + (-16688L);
    *(uint64_t *)var_28 = 4205625UL;
    indirect_placeholder();
    storemerge9 = 0UL;
    storemerge10 = 513UL;
    storemerge7 = (unsigned char)'\x01';
    storemerge = (unsigned char)'\x01';
    var_35 = (unsigned char)'\x01';
    rsi4_1 = 0UL;
    local_sp_17 = var_28;
    var_78 = (unsigned char)'\x01';
    if (*(unsigned char *)4310043UL == '\x00') {
    }
    var_29 = (unsigned char *)(var_0 + (-81L));
    *var_29 = (storemerge7 & '\x01');
    var_30 = (unsigned char *)(var_0 + (-82L));
    *var_30 = (unsigned char)'\x00';
    if (*(unsigned char *)4310041UL == '\x00') {
    }
    var_31 = (unsigned char *)(var_0 + (-177L));
    var_32 = storemerge & '\x01';
    *var_31 = var_32;
    if (*var_29 == '\x01') {
        var_33 = (uint64_t)*var_15;
        var_34 = var_0 + (-16696L);
        *(uint64_t *)var_34 = 4205810UL;
        indirect_placeholder_15(var_33);
        var_35 = *var_29;
        local_sp_17 = var_34;
    }
    rsi4_2 = rsi4_1;
    local_sp_18 = local_sp_17;
    local_sp_21 = local_sp_17;
    if (var_35 != '\x00') {
        var_36 = *var_30;
        var_78 = var_36;
        if (var_36 != '\x01' & *(unsigned char *)4310040UL != '\x01' & *var_31 != '\x01') {
            var_37 = (unsigned char *)(var_0 + (-83L));
            *var_37 = (unsigned char)'\x00';
            var_38 = (uint32_t **)var_17;
            var_39 = helper_cc_compute_all_wrapper((uint64_t)**var_38, 0UL, 0UL, 24U);
            if ((uint64_t)(((unsigned char)(var_39 >> 4UL) ^ (unsigned char)var_39) & '\xc0') == 0UL) {
                var_40 = *var_18 + 8UL;
                var_41 = *var_15;
                var_42 = local_sp_17 + (-8L);
                *(uint64_t *)var_42 = 4205915UL;
                indirect_placeholder();
                **var_38 = var_41;
                rsi4_2 = var_40;
                local_sp_18 = var_42;
            }
            rsi4_0 = rsi4_2;
            local_sp_19 = local_sp_18;
            var_43 = *var_18 + 8UL;
            var_44 = local_sp_18 + (-8L);
            *(uint64_t *)var_44 = 4205960UL;
            var_45 = indirect_placeholder_14(var_43);
            var_46 = var_45.field_1;
            rdi3_0 = var_46;
            local_sp_16 = var_44;
            local_sp_19 = var_44;
            var_47 = *(uint64_t *)(*var_18 + 56UL);
            var_51 = var_47;
            if (**var_38 != 0U & (uint64_t)(unsigned char)var_45.field_0 != 0UL & (long)var_47 >= (long)0UL) {
                var_48 = (uint64_t *)(var_0 + (-192L));
                *var_48 = var_47;
                if ((long)*var_19 > (long)18446744073709551615UL) {
                    var_49 = (uint64_t)*var_15;
                    var_50 = local_sp_18 + (-16L);
                    *(uint64_t *)var_50 = 4206039UL;
                    indirect_placeholder();
                    *var_19 = var_49;
                    var_51 = *var_48;
                    rdi3_0 = var_49;
                    rsi4_0 = 0UL;
                    local_sp_16 = var_50;
                }
                var_52 = *(uint64_t *)4310056UL;
                var_53 = helper_divq_EAX_wrapper((struct type_6 *)(0UL), var_52, 4206065UL, var_51, var_14, var_52, 0UL, rdi3_0, rsi4_0, var_3, var_4, var_5, var_6, var_7, var_8, var_9, var_10, var_11, var_12, var_13);
                var_54 = var_53.field_6;
                var_55 = var_53.field_7;
                var_56 = var_53.field_8;
                var_57 = var_53.field_9;
                var_58 = var_53.field_10;
                local_sp_19 = local_sp_16;
                if (var_53.field_5 == 0UL) {
                    var_61 = helper_cc_compute_all_wrapper(*(uint64_t *)(*var_18 + 64UL), 0UL, 0UL, 25U);
                    if ((uint64_t)(((unsigned char)(var_61 >> 4UL) ^ (unsigned char)var_61) & '\xc0') == 0UL) {
                        var_62 = *(uint64_t *)(*var_18 + 64UL);
                        storemerge10 = (var_62 > 2305843009213693952UL) ? 513UL : (var_62 + 1UL);
                    }
                    var_63 = helper_divq_EAX_wrapper((struct type_6 *)(0UL), storemerge10, 4206214UL, *var_48, var_14, storemerge10, 0UL, rdi3_0, rsi4_0, var_3, var_4, var_5, var_6, var_54, var_55, var_9, var_10, var_56, var_57, var_58);
                    var_64 = *var_48 - var_63.field_5;
                    var_65 = (uint64_t *)(var_0 + (-200L));
                    *var_65 = var_64;
                    var_66 = *var_19;
                    if (((long)var_66 >= (long)0UL) && ((long)var_66 < (long)var_64)) {
                        var_67 = local_sp_16 + (-8L);
                        *(uint64_t *)var_67 = 4206288UL;
                        indirect_placeholder();
                        *var_24 = (*var_65 - *var_19);
                        local_sp_19 = var_67;
                    }
                } else {
                    var_59 = *var_19;
                    var_60 = helper_cc_compute_c_wrapper(*var_48 - var_59, var_59, var_1, 17U);
                    if (var_60 == 0UL) {
                        storemerge9 = *var_48 - *var_19;
                    }
                    *var_24 = storemerge9;
                    *var_37 = (unsigned char)'\x01';
                }
            }
            local_sp_20 = local_sp_19;
            if (*var_37 != '\x01') {
                var_68 = (uint64_t)*var_15;
                var_69 = local_sp_19 + (-8L);
                *(uint64_t *)var_69 = 4206354UL;
                indirect_placeholder_15(var_68);
                var_70 = (uint64_t *)(var_0 + (-40L));
                local_sp_15 = var_69;
                while (1U)
                    {
                        var_71 = (uint64_t)*var_15;
                        var_72 = local_sp_15 + (-8L);
                        *(uint64_t *)var_72 = 4206469UL;
                        var_73 = indirect_placeholder_1(16384UL, var_71);
                        *var_70 = var_73;
                        local_sp_15 = var_72;
                        local_sp_20 = var_72;
                        switch_state_var = 0;
                        switch (var_73) {
                          case 0UL:
                            {
                                switch_state_var = 1;
                                break;
                            }
                            break;
                          case 18446744073709551615UL:
                            {
                                var_74 = *var_22;
                                *(uint64_t *)(local_sp_15 + (-16L)) = 4206388UL;
                                var_75 = indirect_placeholder_3(var_74, 0UL, 3UL);
                                *(uint64_t *)(local_sp_15 + (-24L)) = 4206396UL;
                                indirect_placeholder();
                                var_76 = (uint64_t)*(uint32_t *)var_75;
                                var_77 = local_sp_15 + (-32L);
                                *(uint64_t *)var_77 = 4206423UL;
                                indirect_placeholder_13(0UL, var_75, 4286244UL, 0UL, var_76, var_4, var_5);
                                *var_20 = (unsigned char)'\x00';
                                local_sp_20 = var_77;
                                switch_state_var = 1;
                                break;
                            }
                            break;
                          default:
                            {
                                *var_24 = (*var_24 + var_73);
                                continue;
                            }
                            break;
                        }
                        if (switch_state_var)
                            break;
                    }
            }
            var_126 = (uint64_t)*(unsigned char *)4310042UL;
            var_127 = helper_cc_compute_c_wrapper((uint64_t)*var_30 - var_126, var_126, var_1, 14U);
            if (var_127 == 0UL) {
                var_129 = *var_25;
            } else {
                var_128 = *var_24;
                *var_25 = var_128;
                var_129 = var_128;
            }
            var_130 = *var_16;
            var_131 = *var_23;
            var_132 = *var_24;
            var_133 = *var_26;
            var_134 = *var_27;
            *(uint64_t *)(local_sp_20 + (-8L)) = 4208121UL;
            indirect_placeholder_16(var_132, var_129, var_134, var_133, var_130, var_131);
            *(uint64_t *)4310000UL = (*var_27 + *(uint64_t *)4310000UL);
            *(uint64_t *)4310008UL = (*var_26 + *(uint64_t *)4310008UL);
            *(uint64_t *)4310016UL = (*var_25 + *(uint64_t *)4310016UL);
            *(uint64_t *)4310024UL = (*var_24 + *(uint64_t *)4310024UL);
            var_135 = *(uint64_t *)4310032UL;
            var_136 = *var_23;
            if (var_136 > var_135) {
                *(uint64_t *)4310032UL = var_136;
            }
            return (uint64_t)*var_20;
        }
    }
    var_78 = *var_30;
}
