typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_16_ret_type;
struct indirect_placeholder_16_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_8(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern void indirect_placeholder_2(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_6(void);
extern void indirect_placeholder_10(uint64_t param_0);
extern void indirect_placeholder_15(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern void indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_16_ret_type indirect_placeholder_16(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
typedef _Bool bool;
void bb_main(uint64_t rsi, uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint32_t *var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint32_t *var_9;
    uint64_t var_10;
    unsigned char *var_11;
    unsigned char *var_12;
    uint64_t local_sp_3;
    uint64_t var_32;
    uint64_t var_18;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t local_sp_1;
    unsigned char *var_19;
    uint32_t var_20;
    uint32_t *var_21;
    uint32_t var_22;
    uint64_t local_sp_0;
    uint64_t var_31;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint32_t var_27;
    uint64_t local_sp_2;
    uint64_t var_38;
    struct indirect_placeholder_16_ret_type var_16;
    uint64_t var_39;
    uint64_t var_33;
    uint64_t local_sp_3_be;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint32_t var_17;
    uint64_t _pre;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = var_0 + (-8L);
    *(uint64_t *)var_2 = var_1;
    var_3 = (uint32_t *)(var_0 + (-44L));
    *var_3 = (uint32_t)rdi;
    var_4 = var_0 + (-56L);
    var_5 = (uint64_t *)var_4;
    *var_5 = rsi;
    var_6 = (uint64_t *)(var_0 + (-16L));
    *var_6 = 80UL;
    var_7 = **(uint64_t **)var_4;
    *(uint64_t *)(var_0 + (-64L)) = 4206012UL;
    indirect_placeholder_10(var_7);
    *(uint64_t *)(var_0 + (-72L)) = 4206027UL;
    indirect_placeholder();
    var_8 = var_0 + (-80L);
    *(uint64_t *)var_8 = 4206037UL;
    indirect_placeholder();
    *(unsigned char *)4289426UL = (unsigned char)'\x00';
    *(unsigned char *)4289425UL = (unsigned char)'\x00';
    *(unsigned char *)4289424UL = (unsigned char)'\x00';
    var_9 = (uint32_t *)(var_0 + (-28L));
    var_10 = var_0 + (-30L);
    var_11 = (unsigned char *)var_10;
    var_12 = (unsigned char *)(var_0 + (-29L));
    var_35 = var_10;
    local_sp_3 = var_8;
    while (1U)
        {
            var_13 = *var_5;
            var_14 = (uint64_t)*var_3;
            var_15 = local_sp_3 + (-8L);
            *(uint64_t *)var_15 = 4206413UL;
            var_16 = indirect_placeholder_16(4272736UL, 4272640UL, var_13, var_14, 0UL);
            var_17 = (uint32_t)var_16.field_0;
            *var_9 = var_17;
            local_sp_0 = var_15;
            local_sp_2 = var_15;
            local_sp_3_be = var_15;
            switch_state_var = 0;
            switch (var_17) {
              case 119U:
                {
                    _pre = *(uint64_t *)4289880UL;
                    var_35 = _pre;
                    var_36 = local_sp_3 + (-16L);
                    *(uint64_t *)var_36 = 4206295UL;
                    var_37 = indirect_placeholder_13(4272571UL, 18446744073709551606UL, 1UL, var_35, 0UL, 4273338UL);
                    *var_6 = var_37;
                    local_sp_3_be = var_36;
                    local_sp_3 = local_sp_3_be;
                    continue;
                }
                break;
              case 4294967295U:
                {
                    var_18 = var_16.field_1;
                    if ((uint64_t)(*var_3 - *(uint32_t *)4289272UL) != 0UL) {
                        var_19 = (unsigned char *)(var_0 + (-21L));
                        *var_19 = (unsigned char)'\x01';
                        var_20 = *(uint32_t *)4289272UL;
                        var_21 = (uint32_t *)(var_0 + (-20L));
                        *var_21 = var_20;
                        var_22 = var_20;
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    var_28 = *var_6;
                    var_29 = local_sp_3 + (-16L);
                    *(uint64_t *)var_29 = 4206454UL;
                    var_30 = indirect_placeholder_8(var_28, 4273331UL);
                    *(unsigned char *)(var_0 + (-21L)) = (unsigned char)var_30;
                    local_sp_1 = var_29;
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
              default:
                {
                    if ((int)var_17 <= (int)119U) {
                        loop_state_var = 2U;
                        switch_state_var = 1;
                        break;
                    }
                    if (var_17 != 115U) {
                        *(unsigned char *)4289424UL = (unsigned char)'\x01';
                        local_sp_3 = local_sp_3_be;
                        continue;
                    }
                    if ((int)var_17 <= (int)115U) {
                        loop_state_var = 2U;
                        switch_state_var = 1;
                        break;
                    }
                    if (var_17 != 98U) {
                        *(unsigned char *)4289425UL = (unsigned char)'\x01';
                        local_sp_3 = local_sp_3_be;
                        continue;
                    }
                    if ((int)var_17 <= (int)57U) {
                        loop_state_var = 2U;
                        switch_state_var = 1;
                        break;
                    }
                    if ((int)var_17 < (int)48U) {
                        var_33 = *(uint64_t *)4289880UL;
                        if (var_33 == 0UL) {
                            *var_11 = (unsigned char)var_17;
                            *var_12 = (unsigned char)'\x00';
                            *(uint64_t *)4289880UL = var_10;
                        } else {
                            var_34 = var_33 + (-1L);
                            *(uint64_t *)4289880UL = var_34;
                            var_35 = var_34;
                        }
                        var_36 = local_sp_3 + (-16L);
                        *(uint64_t *)var_36 = 4206295UL;
                        var_37 = indirect_placeholder_13(4272571UL, 18446744073709551606UL, 1UL, var_35, 0UL, 4273338UL);
                        *var_6 = var_37;
                        local_sp_3_be = var_36;
                        local_sp_3 = local_sp_3_be;
                        continue;
                    }
                    switch_state_var = 0;
                    switch (var_17) {
                      case 4294967166U:
                        {
                            *(uint64_t *)(local_sp_3 + (-16L)) = 4206311UL;
                            indirect_placeholder_2(var_2, 0UL);
                            abort();
                        }
                        break;
                      case 4294967165U:
                        {
                            var_38 = *(uint64_t *)4289120UL;
                            *(uint64_t *)(local_sp_3 + (-16L)) = 4206363UL;
                            indirect_placeholder_15(0UL, var_38, 4272408UL, 4273326UL, 0UL, 4273364UL);
                            var_39 = local_sp_3 + (-24L);
                            *(uint64_t *)var_39 = 4206373UL;
                            indirect_placeholder();
                            local_sp_2 = var_39;
                            loop_state_var = 2U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      default:
                        {
                            loop_state_var = 2U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 2U:
        {
            *(uint64_t *)(local_sp_2 + (-8L)) = 4206383UL;
            indirect_placeholder_2(var_2, 1UL);
            abort();
        }
        break;
      case 1U:
      case 0U:
        {
            switch (loop_state_var) {
              case 0U:
                {
                    local_sp_1 = local_sp_0;
                    while ((long)((uint64_t)var_22 << 32UL) >= (long)((uint64_t)*var_3 << 32UL))
                        {
                            var_23 = *(uint64_t *)(*var_5 + ((uint64_t)var_22 << 3UL));
                            var_24 = *var_6;
                            var_25 = local_sp_0 + (-8L);
                            *(uint64_t *)var_25 = 4206512UL;
                            var_26 = indirect_placeholder_8(var_24, var_23);
                            *var_19 = ((var_26 & (uint64_t)*var_19) != 0UL);
                            var_27 = *var_21 + 1U;
                            *var_21 = var_27;
                            var_22 = var_27;
                            local_sp_0 = var_25;
                            local_sp_1 = local_sp_0;
                        }
                }
                break;
              case 1U:
                {
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4206567UL;
                    var_31 = indirect_placeholder_6();
                    if (*(unsigned char *)4289426UL != '\x00' & (uint64_t)((uint32_t)var_31 + 1U) == 0UL) {
                        *(uint64_t *)(local_sp_1 + (-16L)) = 4206577UL;
                        indirect_placeholder();
                        var_32 = (uint64_t)*(uint32_t *)var_31;
                        *(uint64_t *)(local_sp_1 + (-24L)) = 4206601UL;
                        indirect_placeholder_9(0UL, 4272736UL, 4273331UL, var_32, 1UL, var_18, 0UL);
                    }
                    return;
                }
                break;
            }
        }
        break;
    }
}
