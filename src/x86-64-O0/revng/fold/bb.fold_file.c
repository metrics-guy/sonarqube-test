typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_11_ret_type;
struct indirect_placeholder_12_ret_type;
struct indirect_placeholder_14_ret_type;
struct indirect_placeholder_11_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_12_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_14_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_8(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_r10(void);
extern uint64_t indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern void indirect_placeholder_2(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_6(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0);
extern void indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_11_ret_type indirect_placeholder_11(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_12_ret_type indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_14_ret_type indirect_placeholder_14(uint64_t param_0, uint64_t param_1, uint64_t param_2);
uint64_t bb_fold_file(uint64_t rsi, uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    uint64_t *var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t local_sp_8;
    uint64_t var_81;
    struct indirect_placeholder_11_ret_type var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_80;
    struct indirect_placeholder_12_ret_type var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_69;
    uint64_t local_sp_0;
    uint64_t var_70;
    bool var_71;
    uint64_t var_72;
    uint64_t *var_73;
    uint64_t var_66;
    uint32_t var_67;
    uint32_t *var_68;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t local_sp_5;
    uint64_t var_50;
    uint64_t local_sp_1;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t local_sp_6_be;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint32_t var_34;
    unsigned char var_48;
    uint64_t local_sp_4;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t local_sp_2;
    uint64_t local_sp_3;
    uint64_t var_49;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_58;
    bool var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t local_sp_7_be;
    uint64_t var_62;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint32_t var_33;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t local_sp_6;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t local_sp_7;
    uint64_t var_24;
    uint64_t var_25;
    uint32_t var_26;
    uint64_t rax_0;
    uint64_t var_16;
    uint64_t *var_17;
    uint64_t *_pre_phi177;
    uint64_t var_18;
    uint64_t var_87;
    struct indirect_placeholder_14_ret_type var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_19;
    uint32_t *var_20;
    unsigned char *var_21;
    uint64_t *var_22;
    uint64_t *var_23;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t *var_15;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = init_cc_src2();
    var_4 = init_r10();
    var_5 = init_r9();
    var_6 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_7 = (uint64_t *)(var_0 + (-96L));
    *var_7 = rdi;
    var_8 = (uint64_t *)(var_0 + (-104L));
    *var_8 = rsi;
    var_9 = (uint64_t *)(var_0 + (-40L));
    *var_9 = 0UL;
    var_10 = (uint64_t *)(var_0 + (-48L));
    *var_10 = 0UL;
    var_11 = *var_7;
    var_12 = var_0 + (-112L);
    *(uint64_t *)var_12 = 4204955UL;
    indirect_placeholder();
    var_48 = (unsigned char)'\x01';
    rax_0 = 0UL;
    local_sp_8 = var_12;
    if ((uint64_t)(uint32_t)var_11 == 0UL) {
        var_16 = *(uint64_t *)4288072UL;
        var_17 = (uint64_t *)(var_0 + (-32L));
        *var_17 = var_16;
        *(unsigned char *)4289426UL = (unsigned char)'\x01';
        _pre_phi177 = var_17;
    } else {
        var_13 = *var_7;
        var_14 = var_0 + (-120L);
        *(uint64_t *)var_14 = 4204996UL;
        indirect_placeholder();
        var_15 = (uint64_t *)(var_0 + (-32L));
        *var_15 = var_13;
        _pre_phi177 = var_15;
        local_sp_8 = var_14;
    }
    var_18 = *_pre_phi177;
    if (var_18 == 0UL) {
        var_87 = *var_7;
        *(uint64_t *)(local_sp_8 + (-8L)) = 4205029UL;
        var_88 = indirect_placeholder_14(var_87, 3UL, 0UL);
        var_89 = var_88.field_0;
        var_90 = var_88.field_1;
        var_91 = var_88.field_2;
        *(uint64_t *)(local_sp_8 + (-16L)) = 4205037UL;
        indirect_placeholder();
        var_92 = (uint64_t)*(uint32_t *)var_89;
        *(uint64_t *)(local_sp_8 + (-24L)) = 4205064UL;
        indirect_placeholder_9(0UL, var_89, 4273335UL, var_92, 0UL, var_90, var_91);
    } else {
        var_19 = local_sp_8 + (-8L);
        *(uint64_t *)var_19 = 4205091UL;
        indirect_placeholder_2(2UL, var_18);
        var_20 = (uint32_t *)(var_0 + (-76L));
        var_21 = (unsigned char *)(var_0 + (-49L));
        var_22 = (uint64_t *)(var_0 + (-64L));
        var_23 = (uint64_t *)(var_0 + (-72L));
        local_sp_7 = var_19;
        rax_0 = 1UL;
        var_24 = *_pre_phi177;
        var_25 = local_sp_7 + (-8L);
        *(uint64_t *)var_25 = 4205690UL;
        indirect_placeholder();
        var_26 = (uint32_t)var_24;
        *var_20 = var_26;
        local_sp_5 = var_25;
        while (var_26 != 4294967295U)
            {
                var_27 = *var_10 + 1UL;
                var_28 = *(uint64_t *)4289432UL;
                var_29 = helper_cc_compute_c_wrapper(var_27 - var_28, var_28, var_3, 17U);
                if (var_29 == 0UL) {
                    var_30 = *(uint64_t *)4289440UL;
                    var_31 = local_sp_7 + (-16L);
                    *(uint64_t *)var_31 = 4205136UL;
                    var_32 = indirect_placeholder_13(var_2, 4289432UL, var_30, var_4, var_5, var_6);
                    *(uint64_t *)4289440UL = var_32;
                    local_sp_5 = var_31;
                }
                var_33 = *var_20;
                var_34 = var_33;
                local_sp_6 = local_sp_5;
                if (var_33 == 10U) {
                    var_63 = *(uint64_t *)4289440UL;
                    var_64 = *var_10;
                    *var_10 = (var_64 + 1UL);
                    *(unsigned char *)(var_64 + var_63) = (unsigned char)*var_20;
                    var_65 = local_sp_5 + (-8L);
                    *(uint64_t *)var_65 = 4205207UL;
                    indirect_placeholder();
                    *var_10 = 0UL;
                    *var_9 = 0UL;
                    local_sp_7_be = var_65;
                } else {
                    while (1U)
                        {
                            var_35 = (uint64_t)(uint32_t)((int)(var_34 << 24U) >> (int)24U);
                            var_36 = *var_9;
                            var_37 = local_sp_6 + (-8L);
                            *(uint64_t *)var_37 = 4205249UL;
                            var_38 = indirect_placeholder_8(var_35, var_36);
                            *var_9 = var_38;
                            local_sp_2 = var_37;
                            local_sp_4 = var_37;
                            local_sp_7_be = var_37;
                            if (var_38 <= *var_8) {
                                loop_state_var = 0U;
                                break;
                            }
                            if (*(unsigned char *)4289424UL == '\x00') {
                                var_58 = *var_10;
                                var_59 = (var_58 == 0UL);
                                var_60 = *(uint64_t *)4289440UL;
                                *var_10 = (var_58 + 1UL);
                                var_61 = var_58 + var_60;
                                local_sp_7_be = local_sp_4;
                                if (!var_59) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                *(unsigned char *)var_61 = (unsigned char)'\n';
                                var_62 = local_sp_4 + (-8L);
                                *(uint64_t *)var_62 = 4205630UL;
                                indirect_placeholder();
                                *var_10 = 0UL;
                                *var_9 = 0UL;
                                local_sp_6_be = var_62;
                                var_34 = *var_20;
                                local_sp_6 = local_sp_6_be;
                                continue;
                            }
                            *var_21 = (unsigned char)'\x00';
                            var_41 = *var_10;
                            *var_22 = var_41;
                            var_42 = var_41;
                            while (1U)
                                {
                                    local_sp_3 = local_sp_2;
                                    if (var_42 != 0UL) {
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    var_43 = var_42 + (-1L);
                                    *var_22 = var_43;
                                    var_44 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(var_43 + *(uint64_t *)4289440UL);
                                    *(uint64_t *)(local_sp_2 + (-8L)) = 4205328UL;
                                    var_45 = indirect_placeholder_5(var_44);
                                    var_46 = (uint64_t)(unsigned char)var_45;
                                    var_47 = local_sp_2 + (-16L);
                                    *(uint64_t *)var_47 = 4205338UL;
                                    indirect_placeholder();
                                    local_sp_2 = var_47;
                                    local_sp_3 = var_47;
                                    if (var_46 != 0UL) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    var_42 = *var_22;
                                    continue;
                                }
                            switch (loop_state_var) {
                              case 0U:
                                {
                                    *var_21 = (unsigned char)'\x01';
                                }
                                break;
                              case 1U:
                                {
                                    var_48 = *var_21;
                                }
                                break;
                            }
                            local_sp_4 = local_sp_3;
                            if (var_48 != '\x00') {
                                *var_22 = (*var_22 + 1UL);
                                *(uint64_t *)(local_sp_3 + (-8L)) = 4205401UL;
                                indirect_placeholder();
                                *(uint64_t *)(local_sp_3 + (-16L)) = 4205411UL;
                                indirect_placeholder();
                                var_49 = local_sp_3 + (-24L);
                                *(uint64_t *)var_49 = 4205454UL;
                                indirect_placeholder();
                                *var_10 = (*var_10 - *var_22);
                                *var_23 = 0UL;
                                *var_9 = 0UL;
                                var_50 = *var_23;
                                local_sp_1 = var_49;
                                var_51 = *var_10;
                                var_52 = helper_cc_compute_c_wrapper(var_50 - var_51, var_51, var_3, 17U);
                                local_sp_6_be = local_sp_1;
                                while (var_52 != 0UL)
                                    {
                                        var_53 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(*var_23 + *(uint64_t *)4289440UL);
                                        var_54 = *var_9;
                                        var_55 = local_sp_1 + (-8L);
                                        *(uint64_t *)var_55 = 4205514UL;
                                        var_56 = indirect_placeholder_8(var_53, var_54);
                                        *var_9 = var_56;
                                        var_57 = *var_23 + 1UL;
                                        *var_23 = var_57;
                                        var_50 = var_57;
                                        local_sp_1 = var_55;
                                        var_51 = *var_10;
                                        var_52 = helper_cc_compute_c_wrapper(var_50 - var_51, var_51, var_3, 17U);
                                        local_sp_6_be = local_sp_1;
                                    }
                                var_34 = *var_20;
                                local_sp_6 = local_sp_6_be;
                                continue;
                            }
                        }
                    switch (loop_state_var) {
                      case 0U:
                        {
                            var_39 = *(uint64_t *)4289440UL;
                            var_40 = *var_10;
                            *var_10 = (var_40 + 1UL);
                            *(unsigned char *)(var_40 + var_39) = (unsigned char)*var_20;
                        }
                        break;
                      case 1U:
                        {
                            *(unsigned char *)var_61 = (unsigned char)*var_20;
                        }
                        break;
                    }
                }
                local_sp_7 = local_sp_7_be;
                var_24 = *_pre_phi177;
                var_25 = local_sp_7 + (-8L);
                *(uint64_t *)var_25 = 4205690UL;
                indirect_placeholder();
                var_26 = (uint32_t)var_24;
                *var_20 = var_26;
                local_sp_5 = var_25;
            }
        var_66 = local_sp_7 + (-16L);
        *(uint64_t *)var_66 = 4205708UL;
        indirect_placeholder();
        var_67 = *(uint32_t *)var_24;
        var_68 = (uint32_t *)(var_0 + (-80L));
        *var_68 = var_67;
        local_sp_0 = var_66;
        if (*var_10 == 0UL) {
            var_69 = local_sp_7 + (-24L);
            *(uint64_t *)var_69 = 4205751UL;
            indirect_placeholder();
            local_sp_0 = var_69;
        }
        var_70 = *_pre_phi177;
        *(uint64_t *)(local_sp_0 + (-8L)) = 4205763UL;
        indirect_placeholder();
        var_71 = ((uint64_t)(uint32_t)var_70 == 0UL);
        var_72 = *var_7;
        var_73 = (uint64_t *)(local_sp_0 + (-16L));
        if (var_71) {
            *var_73 = 4205877UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_0 + (-24L)) = 4205893UL;
            var_80 = indirect_placeholder_6();
            if ((uint64_t)(uint32_t)var_72 != 0UL & (uint64_t)((uint32_t)var_80 + 1U) == 0UL) {
                var_81 = *var_7;
                *(uint64_t *)(local_sp_0 + (-32L)) = 4205920UL;
                var_82 = indirect_placeholder_11(var_81, 3UL, 0UL);
                var_83 = var_82.field_0;
                var_84 = var_82.field_1;
                var_85 = var_82.field_2;
                *(uint64_t *)(local_sp_0 + (-40L)) = 4205928UL;
                indirect_placeholder();
                var_86 = (uint64_t)*(uint32_t *)var_83;
                *(uint64_t *)(local_sp_0 + (-48L)) = 4205955UL;
                indirect_placeholder_9(0UL, var_83, 4273335UL, var_86, 0UL, var_84, var_85);
                rax_0 = 0UL;
            }
        } else {
            *var_73 = 4205789UL;
            var_74 = indirect_placeholder_12(var_72, 3UL, 0UL);
            var_75 = var_74.field_0;
            var_76 = var_74.field_1;
            var_77 = var_74.field_2;
            var_78 = (uint64_t)*var_68;
            *(uint64_t *)(local_sp_0 + (-24L)) = 4205820UL;
            indirect_placeholder_9(0UL, var_75, 4273335UL, var_78, 0UL, var_76, var_77);
            var_79 = *var_7;
            *(uint64_t *)(local_sp_0 + (-32L)) = 4205837UL;
            indirect_placeholder();
            if ((uint64_t)(uint32_t)var_79 == 0UL) {
                *(uint64_t *)(local_sp_0 + (-40L)) = 4205853UL;
                indirect_placeholder_6();
            }
        }
    }
    return rax_0;
}
