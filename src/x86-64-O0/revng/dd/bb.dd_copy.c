typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_134_ret_type;
struct indirect_placeholder_133_ret_type;
struct indirect_placeholder_124_ret_type;
struct indirect_placeholder_123_ret_type;
struct indirect_placeholder_126_ret_type;
struct indirect_placeholder_125_ret_type;
struct indirect_placeholder_132_ret_type;
struct indirect_placeholder_131_ret_type;
struct indirect_placeholder_138_ret_type;
struct indirect_placeholder_139_ret_type;
struct indirect_placeholder_140_ret_type;
struct indirect_placeholder_141_ret_type;
struct indirect_placeholder_142_ret_type;
struct indirect_placeholder_136_ret_type;
struct indirect_placeholder_135_ret_type;
struct indirect_placeholder_143_ret_type;
struct indirect_placeholder_137_ret_type;
struct indirect_placeholder_144_ret_type;
struct indirect_placeholder_145_ret_type;
struct indirect_placeholder_146_ret_type;
struct indirect_placeholder_147_ret_type;
struct indirect_placeholder_148_ret_type;
struct indirect_placeholder_149_ret_type;
struct indirect_placeholder_150_ret_type;
struct indirect_placeholder_130_ret_type;
struct indirect_placeholder_129_ret_type;
struct indirect_placeholder_153_ret_type;
struct indirect_placeholder_152_ret_type;
struct indirect_placeholder_154_ret_type;
struct indirect_placeholder_156_ret_type;
struct indirect_placeholder_155_ret_type;
struct indirect_placeholder_157_ret_type;
struct indirect_placeholder_151_ret_type;
struct indirect_placeholder_158_ret_type;
struct indirect_placeholder_159_ret_type;
struct indirect_placeholder_134_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_133_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_124_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_123_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_126_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_125_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_132_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_131_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_138_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_139_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_140_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_141_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_142_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_136_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_135_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_143_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_137_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_144_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_145_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_146_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_147_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_148_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_149_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_150_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_130_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_129_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_153_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_152_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_154_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_156_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_155_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_157_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_151_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_158_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_159_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern struct indirect_placeholder_134_ret_type indirect_placeholder_134(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_133_ret_type indirect_placeholder_133(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_6(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_3(void);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_41(uint64_t param_0);
extern struct indirect_placeholder_124_ret_type indirect_placeholder_124(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_123_ret_type indirect_placeholder_123(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_126_ret_type indirect_placeholder_126(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_125_ret_type indirect_placeholder_125(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_132_ret_type indirect_placeholder_132(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_131_ret_type indirect_placeholder_131(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_138_ret_type indirect_placeholder_138(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_139_ret_type indirect_placeholder_139(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_140_ret_type indirect_placeholder_140(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_141_ret_type indirect_placeholder_141(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_142_ret_type indirect_placeholder_142(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_136_ret_type indirect_placeholder_136(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_135_ret_type indirect_placeholder_135(uint64_t param_0);
extern struct indirect_placeholder_143_ret_type indirect_placeholder_143(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_137_ret_type indirect_placeholder_137(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_144_ret_type indirect_placeholder_144(uint64_t param_0);
extern struct indirect_placeholder_145_ret_type indirect_placeholder_145(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_146_ret_type indirect_placeholder_146(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_147_ret_type indirect_placeholder_147(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_148_ret_type indirect_placeholder_148(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_149_ret_type indirect_placeholder_149(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_150_ret_type indirect_placeholder_150(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_130_ret_type indirect_placeholder_130(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_129_ret_type indirect_placeholder_129(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_153_ret_type indirect_placeholder_153(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_152_ret_type indirect_placeholder_152(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_154_ret_type indirect_placeholder_154(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_156_ret_type indirect_placeholder_156(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_155_ret_type indirect_placeholder_155(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_157_ret_type indirect_placeholder_157(void);
extern struct indirect_placeholder_151_ret_type indirect_placeholder_151(void);
extern struct indirect_placeholder_158_ret_type indirect_placeholder_158(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_159_ret_type indirect_placeholder_159(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
uint64_t bb_dd_copy(uint64_t r9, uint64_t r8) {
    struct indirect_placeholder_134_ret_type var_147;
    uint64_t var_148;
    uint64_t var_149;
    uint64_t var_150;
    uint64_t var_151;
    uint64_t local_sp_9_be;
    struct indirect_placeholder_159_ret_type var_17;
    struct indirect_placeholder_158_ret_type var_35;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint32_t *var_6;
    uint64_t var_7;
    uint64_t _pre;
    uint64_t var_10;
    uint64_t var_220;
    uint64_t local_sp_21;
    uint32_t var_221;
    uint64_t rax_0;
    uint64_t local_sp_9;
    uint64_t r82_3;
    uint64_t var_222;
    struct indirect_placeholder_124_ret_type var_223;
    uint64_t var_224;
    uint64_t var_225;
    uint64_t var_226;
    uint64_t var_227;
    uint32_t rax_1_shrunk;
    uint64_t var_210;
    struct indirect_placeholder_126_ret_type var_211;
    uint64_t var_212;
    uint64_t var_213;
    uint64_t var_214;
    uint64_t var_215;
    uint64_t var_216;
    uint64_t var_146;
    uint64_t var_209;
    uint64_t local_sp_0;
    uint32_t var_217;
    uint32_t var_218;
    uint16_t _pre_phi;
    uint64_t var_190;
    uint64_t local_sp_18;
    uint64_t r91_10;
    uint64_t var_191;
    struct indirect_placeholder_132_ret_type var_192;
    uint64_t var_193;
    uint64_t var_194;
    uint64_t var_195;
    uint64_t var_196;
    uint64_t var_116;
    uint64_t var_107;
    uint64_t var_108;
    struct indirect_placeholder_138_ret_type var_109;
    uint64_t rcx_0;
    uint64_t local_sp_2;
    uint64_t r82_4;
    uint64_t r91_1;
    uint64_t r82_1;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t var_112;
    uint64_t local_sp_4;
    uint16_t var_113;
    uint64_t r91_3;
    uint64_t var_72;
    uint64_t rcx_2;
    uint64_t rcx_1;
    uint64_t var_114;
    uint64_t var_115;
    uint64_t local_sp_3;
    uint64_t var_70;
    uint64_t var_139;
    uint64_t var_140;
    struct indirect_placeholder_139_ret_type var_141;
    uint64_t var_142;
    uint64_t var_143;
    uint64_t var_144;
    uint64_t var_145;
    uint64_t var_119;
    uint64_t var_117;
    uint64_t var_118;
    uint64_t local_sp_5;
    uint64_t var_120;
    uint64_t var_121;
    uint64_t var_122;
    uint64_t local_sp_6;
    uint32_t var_123;
    uint64_t var_124;
    uint64_t var_125;
    struct indirect_placeholder_140_ret_type var_126;
    uint64_t var_127;
    uint64_t var_128;
    bool var_129;
    uint64_t var_130;
    uint64_t var_131;
    uint64_t *var_132;
    struct indirect_placeholder_141_ret_type var_133;
    uint64_t var_134;
    uint64_t var_135;
    struct indirect_placeholder_142_ret_type var_136;
    uint64_t var_137;
    uint64_t var_138;
    uint64_t r91_3_be;
    uint64_t r82_3_be;
    uint64_t var_106;
    uint64_t storemerge6;
    uint64_t rcx_5;
    uint16_t var_89;
    uint16_t _pre_phi380;
    uint64_t rcx_4;
    uint64_t local_sp_8;
    uint64_t r91_2;
    uint64_t r82_2;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t var_100;
    struct indirect_placeholder_135_ret_type var_101;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t var_90;
    struct indirect_placeholder_143_ret_type var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t var_96;
    struct indirect_placeholder_137_ret_type var_97;
    struct indirect_placeholder_144_ret_type var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t local_sp_10;
    uint32_t var_229;
    uint64_t var_71;
    uint64_t r91_4;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint16_t var_79;
    uint64_t local_sp_11;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t var_83;
    bool var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t *var_88;
    uint64_t local_sp_12;
    uint64_t r91_5;
    uint64_t r82_5;
    uint64_t rcx_6;
    uint32_t var_152;
    uint64_t var_153;
    struct indirect_placeholder_145_ret_type var_154;
    uint64_t var_155;
    struct indirect_placeholder_146_ret_type var_156;
    uint64_t var_157;
    uint64_t var_158;
    uint64_t var_159;
    uint64_t var_160;
    uint64_t var_161;
    uint64_t var_162;
    struct indirect_placeholder_147_ret_type var_163;
    uint64_t local_sp_13;
    uint64_t r91_6;
    uint64_t r82_6;
    uint64_t rcx_9_ph;
    uint64_t var_164;
    uint64_t rcx_10;
    uint64_t *var_165;
    uint64_t rcx_8;
    uint64_t var_166;
    uint64_t local_sp_15;
    uint64_t rcx_7;
    uint64_t r91_8;
    uint64_t local_sp_14;
    uint64_t r82_8;
    uint64_t r91_7;
    uint64_t r82_7;
    uint64_t var_167;
    uint64_t var_168;
    uint64_t var_169;
    uint64_t var_170;
    uint64_t var_171;
    uint64_t var_172;
    uint64_t var_173;
    uint64_t var_174;
    struct indirect_placeholder_148_ret_type var_175;
    uint64_t var_176;
    uint64_t local_sp_16_ph;
    uint64_t r91_9_ph;
    uint64_t r82_9_ph;
    uint64_t var_177;
    uint64_t var_178;
    uint64_t var_179;
    uint64_t var_180;
    uint64_t var_181;
    uint64_t var_182;
    struct indirect_placeholder_149_ret_type var_183;
    uint64_t local_sp_17;
    uint64_t r82_10;
    uint64_t var_184;
    uint64_t var_185;
    uint64_t var_186;
    struct indirect_placeholder_150_ret_type var_187;
    uint64_t var_188;
    uint64_t *var_189;
    uint64_t var_197;
    struct indirect_placeholder_130_ret_type var_198;
    uint64_t var_199;
    uint64_t var_200;
    uint64_t var_201;
    uint64_t var_202;
    uint16_t var_203;
    uint32_t var_204;
    uint64_t var_205;
    uint64_t var_206;
    uint32_t var_207;
    uint64_t var_208;
    uint64_t local_sp_20;
    uint32_t var_219;
    uint32_t var_228;
    uint64_t r91_11;
    uint64_t var_50;
    struct indirect_placeholder_153_ret_type var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t local_sp_24;
    uint64_t var_43;
    uint64_t local_sp_22;
    uint64_t r82_11;
    uint64_t storemerge;
    uint64_t var_44;
    uint64_t var_45;
    struct indirect_placeholder_154_ret_type var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t *var_42;
    uint64_t local_sp_23;
    uint64_t var_21;
    struct indirect_placeholder_156_ret_type var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t _pre370;
    uint64_t var_28;
    uint64_t var_27;
    uint64_t var_59;
    struct indirect_placeholder_151_ret_type var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t *var_63;
    uint64_t *var_64;
    uint64_t *var_65;
    uint64_t var_66;
    uint64_t *var_67;
    uint64_t *var_68;
    uint64_t *var_69;
    uint64_t var_29;
    uint64_t *var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t *var_39;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_18;
    uint64_t *var_19;
    uint64_t var_20;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = init_rbx();
    var_4 = var_0 + (-8L);
    *(uint64_t *)var_4 = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    var_5 = (uint64_t *)(var_0 + (-48L));
    *var_5 = 0UL;
    var_6 = (uint32_t *)(var_0 + (-52L));
    *var_6 = 0U;
    var_7 = *(uint64_t *)4318352UL;
    var_190 = 0UL;
    storemerge6 = 0UL;
    rax_1_shrunk = 1U;
    if (var_7 == 0UL) {
        var_8 = var_0 + (-296L);
        var_9 = *(uint64_t *)4318360UL;
        local_sp_23 = var_8;
        var_10 = var_9;
        if (var_9 != 0UL) {
            var_11 = var_10 + ((*(uint64_t *)4318328UL * var_7) + *(uint64_t *)4318488UL);
            var_12 = (uint64_t *)(var_0 + (-80L));
            *var_12 = var_11;
            var_13 = *(uint64_t *)4318328UL;
            var_14 = *(uint64_t *)4318352UL;
            var_15 = *(uint64_t *)4318304UL;
            var_16 = var_0 + (-304L);
            *(uint64_t *)var_16 = 4217251UL;
            var_17 = indirect_placeholder_159(var_13, var_14, var_15, 0UL, 4318360UL);
            var_18 = var_17.field_0;
            var_19 = (uint64_t *)(var_0 + (-88L));
            *var_19 = var_18;
            var_20 = *var_12 - *(uint64_t *)4318488UL;
            *var_12 = var_20;
            local_sp_23 = var_16;
            if (*var_19 == 0UL) {
                if (!((*(unsigned char *)4318496UL == '\x01') || (var_20 == 0UL)) && *(uint32_t *)4317960UL == 1U) {
                    var_21 = *(uint64_t *)4318304UL;
                    *(uint64_t *)(var_0 + (-312L)) = 4217330UL;
                    var_22 = indirect_placeholder_156(var_21, 3UL, 0UL);
                    var_23 = var_22.field_0;
                    var_24 = var_22.field_1;
                    var_25 = var_22.field_2;
                    var_26 = var_0 + (-320L);
                    *(uint64_t *)var_26 = 4217358UL;
                    indirect_placeholder_155(0UL, var_23, 4299208UL, 0UL, 0UL, var_24, var_25);
                    local_sp_23 = var_26;
                }
            } else {
                if (*(uint32_t *)4317960UL == 1U) {
                    var_21 = *(uint64_t *)4318304UL;
                    *(uint64_t *)(var_0 + (-312L)) = 4217330UL;
                    var_22 = indirect_placeholder_156(var_21, 3UL, 0UL);
                    var_23 = var_22.field_0;
                    var_24 = var_22.field_1;
                    var_25 = var_22.field_2;
                    var_26 = var_0 + (-320L);
                    *(uint64_t *)var_26 = 4217358UL;
                    indirect_placeholder_155(0UL, var_23, 4299208UL, 0UL, 0UL, var_24, var_25);
                    local_sp_23 = var_26;
                }
            }
        }
    } else {
        _pre = *(uint64_t *)4318360UL;
        var_10 = _pre;
        var_11 = var_10 + ((*(uint64_t *)4318328UL * var_7) + *(uint64_t *)4318488UL);
        var_12 = (uint64_t *)(var_0 + (-80L));
        *var_12 = var_11;
        var_13 = *(uint64_t *)4318328UL;
        var_14 = *(uint64_t *)4318352UL;
        var_15 = *(uint64_t *)4318304UL;
        var_16 = var_0 + (-304L);
        *(uint64_t *)var_16 = 4217251UL;
        var_17 = indirect_placeholder_159(var_13, var_14, var_15, 0UL, 4318360UL);
        var_18 = var_17.field_0;
        var_19 = (uint64_t *)(var_0 + (-88L));
        *var_19 = var_18;
        var_20 = *var_12 - *(uint64_t *)4318488UL;
        *var_12 = var_20;
        local_sp_23 = var_16;
        if (*var_19 == 0UL) {
            if (!((*(unsigned char *)4318496UL == '\x01') || (var_20 == 0UL)) && *(uint32_t *)4317960UL == 1U) {
                var_21 = *(uint64_t *)4318304UL;
                *(uint64_t *)(var_0 + (-312L)) = 4217330UL;
                var_22 = indirect_placeholder_156(var_21, 3UL, 0UL);
                var_23 = var_22.field_0;
                var_24 = var_22.field_1;
                var_25 = var_22.field_2;
                var_26 = var_0 + (-320L);
                *(uint64_t *)var_26 = 4217358UL;
                indirect_placeholder_155(0UL, var_23, 4299208UL, 0UL, 0UL, var_24, var_25);
                local_sp_23 = var_26;
            }
        } else {
            if (*(uint32_t *)4317960UL == 1U) {
                var_21 = *(uint64_t *)4318304UL;
                *(uint64_t *)(var_0 + (-312L)) = 4217330UL;
                var_22 = indirect_placeholder_156(var_21, 3UL, 0UL);
                var_23 = var_22.field_0;
                var_24 = var_22.field_1;
                var_25 = var_22.field_2;
                var_26 = var_0 + (-320L);
                *(uint64_t *)var_26 = 4217358UL;
                indirect_placeholder_155(0UL, var_23, 4299208UL, 0UL, 0UL, var_24, var_25);
                local_sp_23 = var_26;
            }
        }
    }
    local_sp_24 = local_sp_23;
    if (*(uint64_t *)4318368UL == 0UL) {
        var_27 = *(uint64_t *)4318376UL;
        var_28 = var_27;
        if (var_27 != 0UL) {
            var_29 = var_0 + (-152L);
            var_30 = (uint64_t *)var_29;
            *var_30 = var_28;
            var_31 = *(uint64_t *)4318336UL;
            var_32 = *(uint64_t *)4318368UL;
            var_33 = *(uint64_t *)4318312UL;
            var_34 = local_sp_23 + (-8L);
            *(uint64_t *)var_34 = 4217444UL;
            var_35 = indirect_placeholder_158(var_31, var_32, var_33, 1UL, var_29);
            var_36 = var_35.field_0;
            var_37 = var_35.field_1;
            var_38 = var_35.field_2;
            var_39 = (uint64_t *)(var_0 + (-64L));
            *var_39 = var_36;
            r91_11 = var_37;
            r82_11 = var_38;
            local_sp_24 = var_34;
            if (var_36 != 0UL) {
                var_40 = *(uint64_t *)4318520UL;
                var_41 = local_sp_23 + (-16L);
                *(uint64_t *)var_41 = 4217517UL;
                indirect_placeholder();
                var_42 = (uint64_t *)(var_0 + (-96L));
                var_43 = *var_39;
                local_sp_22 = var_41;
                while (1U)
                    {
                        storemerge = *((var_43 == 0UL) ? var_30 : 4318336UL);
                        *var_42 = storemerge;
                        var_44 = *(uint64_t *)4318520UL;
                        var_45 = local_sp_22 + (-8L);
                        *(uint64_t *)var_45 = 4217568UL;
                        var_46 = indirect_placeholder_154(var_40, storemerge, var_44, 1UL, r91_11, r82_11);
                        var_47 = var_46.field_0;
                        var_48 = var_46.field_1;
                        var_49 = var_46.field_2;
                        local_sp_22 = var_45;
                        r91_11 = var_48;
                        r82_11 = var_49;
                        local_sp_24 = var_45;
                        if (*var_42 != var_47) {
                            var_56 = *var_39;
                            if (var_56 == 0UL) {
                                *var_30 = 0UL;
                                var_58 = *var_39;
                            } else {
                                var_57 = var_56 + (-1L);
                                *var_39 = var_57;
                                var_58 = var_57;
                            }
                            var_43 = var_58;
                            if (var_58 != 0UL) {
                                continue;
                            }
                            if (*var_30 != 0UL) {
                                continue;
                            }
                            break;
                        }
                        var_50 = *(uint64_t *)4318312UL;
                        *(uint64_t *)(local_sp_22 + (-16L)) = 4217594UL;
                        var_51 = indirect_placeholder_153(var_50, 4UL);
                        var_52 = var_51.field_0;
                        var_53 = var_51.field_1;
                        var_54 = var_51.field_2;
                        *(uint64_t *)(local_sp_22 + (-24L)) = 4217602UL;
                        indirect_placeholder();
                        var_55 = (uint64_t)*(uint32_t *)var_52;
                        *(uint64_t *)(local_sp_22 + (-32L)) = 4217629UL;
                        indirect_placeholder_152(0UL, var_52, 4298344UL, var_55, 0UL, var_53, var_54);
                        *(uint64_t *)(local_sp_22 + (-40L)) = 4217639UL;
                        indirect_placeholder_6(var_4, 1UL);
                        abort();
                    }
            }
            if (*var_30 != 0UL) {
                var_40 = *(uint64_t *)4318520UL;
                var_41 = local_sp_23 + (-16L);
                *(uint64_t *)var_41 = 4217517UL;
                indirect_placeholder();
                var_42 = (uint64_t *)(var_0 + (-96L));
                var_43 = *var_39;
                local_sp_22 = var_41;
                while (1U)
                    {
                        storemerge = *((var_43 == 0UL) ? var_30 : 4318336UL);
                        *var_42 = storemerge;
                        var_44 = *(uint64_t *)4318520UL;
                        var_45 = local_sp_22 + (-8L);
                        *(uint64_t *)var_45 = 4217568UL;
                        var_46 = indirect_placeholder_154(var_40, storemerge, var_44, 1UL, r91_11, r82_11);
                        var_47 = var_46.field_0;
                        var_48 = var_46.field_1;
                        var_49 = var_46.field_2;
                        local_sp_22 = var_45;
                        r91_11 = var_48;
                        r82_11 = var_49;
                        local_sp_24 = var_45;
                        if (*var_42 != var_47) {
                            var_56 = *var_39;
                            if (var_56 == 0UL) {
                                *var_30 = 0UL;
                                var_58 = *var_39;
                            } else {
                                var_57 = var_56 + (-1L);
                                *var_39 = var_57;
                                var_58 = var_57;
                            }
                            var_43 = var_58;
                            if (var_58 != 0UL) {
                                continue;
                            }
                            if (*var_30 != 0UL) {
                                continue;
                            }
                            break;
                        }
                        var_50 = *(uint64_t *)4318312UL;
                        *(uint64_t *)(local_sp_22 + (-16L)) = 4217594UL;
                        var_51 = indirect_placeholder_153(var_50, 4UL);
                        var_52 = var_51.field_0;
                        var_53 = var_51.field_1;
                        var_54 = var_51.field_2;
                        *(uint64_t *)(local_sp_22 + (-24L)) = 4217602UL;
                        indirect_placeholder();
                        var_55 = (uint64_t)*(uint32_t *)var_52;
                        *(uint64_t *)(local_sp_22 + (-32L)) = 4217629UL;
                        indirect_placeholder_152(0UL, var_52, 4298344UL, var_55, 0UL, var_53, var_54);
                        *(uint64_t *)(local_sp_22 + (-40L)) = 4217639UL;
                        indirect_placeholder_6(var_4, 1UL);
                        abort();
                    }
            }
        }
    }
    _pre370 = *(uint64_t *)4318376UL;
    var_28 = _pre370;
    var_29 = var_0 + (-152L);
    var_30 = (uint64_t *)var_29;
    *var_30 = var_28;
    var_31 = *(uint64_t *)4318336UL;
    var_32 = *(uint64_t *)4318368UL;
    var_33 = *(uint64_t *)4318312UL;
    var_34 = local_sp_23 + (-8L);
    *(uint64_t *)var_34 = 4217444UL;
    var_35 = indirect_placeholder_158(var_31, var_32, var_33, 1UL, var_29);
    var_36 = var_35.field_0;
    var_37 = var_35.field_1;
    var_38 = var_35.field_2;
    var_39 = (uint64_t *)(var_0 + (-64L));
    *var_39 = var_36;
    r91_11 = var_37;
    r82_11 = var_38;
    local_sp_24 = var_34;
    if (var_36 != 0UL) {
        var_40 = *(uint64_t *)4318520UL;
        var_41 = local_sp_23 + (-16L);
        *(uint64_t *)var_41 = 4217517UL;
        indirect_placeholder();
        var_42 = (uint64_t *)(var_0 + (-96L));
        var_43 = *var_39;
        local_sp_22 = var_41;
        while (1U)
            {
                storemerge = *((var_43 == 0UL) ? var_30 : 4318336UL);
                *var_42 = storemerge;
                var_44 = *(uint64_t *)4318520UL;
                var_45 = local_sp_22 + (-8L);
                *(uint64_t *)var_45 = 4217568UL;
                var_46 = indirect_placeholder_154(var_40, storemerge, var_44, 1UL, r91_11, r82_11);
                var_47 = var_46.field_0;
                var_48 = var_46.field_1;
                var_49 = var_46.field_2;
                local_sp_22 = var_45;
                r91_11 = var_48;
                r82_11 = var_49;
                local_sp_24 = var_45;
                if (*var_42 != var_47) {
                    var_56 = *var_39;
                    if (var_56 == 0UL) {
                        *var_30 = 0UL;
                        var_58 = *var_39;
                    } else {
                        var_57 = var_56 + (-1L);
                        *var_39 = var_57;
                        var_58 = var_57;
                    }
                    var_43 = var_58;
                    if (var_58 != 0UL) {
                        continue;
                    }
                    if (*var_30 != 0UL) {
                        continue;
                    }
                    break;
                }
                var_50 = *(uint64_t *)4318312UL;
                *(uint64_t *)(local_sp_22 + (-16L)) = 4217594UL;
                var_51 = indirect_placeholder_153(var_50, 4UL);
                var_52 = var_51.field_0;
                var_53 = var_51.field_1;
                var_54 = var_51.field_2;
                *(uint64_t *)(local_sp_22 + (-24L)) = 4217602UL;
                indirect_placeholder();
                var_55 = (uint64_t)*(uint32_t *)var_52;
                *(uint64_t *)(local_sp_22 + (-32L)) = 4217629UL;
                indirect_placeholder_152(0UL, var_52, 4298344UL, var_55, 0UL, var_53, var_54);
                *(uint64_t *)(local_sp_22 + (-40L)) = 4217639UL;
                indirect_placeholder_6(var_4, 1UL);
                abort();
            }
    }
    if (*var_30 != 0UL) {
        var_40 = *(uint64_t *)4318520UL;
        var_41 = local_sp_23 + (-16L);
        *(uint64_t *)var_41 = 4217517UL;
        indirect_placeholder();
        var_42 = (uint64_t *)(var_0 + (-96L));
        var_43 = *var_39;
        local_sp_22 = var_41;
        while (1U)
            {
                storemerge = *((var_43 == 0UL) ? var_30 : 4318336UL);
                *var_42 = storemerge;
                var_44 = *(uint64_t *)4318520UL;
                var_45 = local_sp_22 + (-8L);
                *(uint64_t *)var_45 = 4217568UL;
                var_46 = indirect_placeholder_154(var_40, storemerge, var_44, 1UL, r91_11, r82_11);
                var_47 = var_46.field_0;
                var_48 = var_46.field_1;
                var_49 = var_46.field_2;
                local_sp_22 = var_45;
                r91_11 = var_48;
                r82_11 = var_49;
                local_sp_24 = var_45;
                if (*var_42 != var_47) {
                    var_56 = *var_39;
                    if (var_56 == 0UL) {
                        *var_30 = 0UL;
                        var_58 = *var_39;
                    } else {
                        var_57 = var_56 + (-1L);
                        *var_39 = var_57;
                        var_58 = var_57;
                    }
                    var_43 = var_58;
                    if (var_58 != 0UL) {
                        continue;
                    }
                    if (*var_30 != 0UL) {
                        continue;
                    }
                    break;
                }
                var_50 = *(uint64_t *)4318312UL;
                *(uint64_t *)(local_sp_22 + (-16L)) = 4217594UL;
                var_51 = indirect_placeholder_153(var_50, 4UL);
                var_52 = var_51.field_0;
                var_53 = var_51.field_1;
                var_54 = var_51.field_2;
                *(uint64_t *)(local_sp_22 + (-24L)) = 4217602UL;
                indirect_placeholder();
                var_55 = (uint64_t)*(uint32_t *)var_52;
                *(uint64_t *)(local_sp_22 + (-32L)) = 4217629UL;
                indirect_placeholder_152(0UL, var_52, 4298344UL, var_55, 0UL, var_53, var_54);
                *(uint64_t *)(local_sp_22 + (-40L)) = 4217639UL;
                indirect_placeholder_6(var_4, 1UL);
                abort();
            }
    }
    if (*(uint64_t *)4317952UL != 0UL) {
        if (*(uint64_t *)4318392UL != 0UL) {
            var_229 = *var_6;
            rax_1_shrunk = var_229;
            return (uint64_t)rax_1_shrunk;
        }
    }
    *(uint64_t *)(local_sp_24 + (-8L)) = 4217728UL;
    indirect_placeholder_157();
    var_59 = local_sp_24 + (-16L);
    *(uint64_t *)var_59 = 4217733UL;
    var_60 = indirect_placeholder_151();
    var_61 = var_60.field_0;
    var_62 = var_60.field_1;
    var_63 = (uint64_t *)(var_0 + (-104L));
    var_64 = (uint64_t *)(var_0 + (-40L));
    var_65 = (uint64_t *)(var_0 + (-112L));
    var_66 = var_0 + (-144L);
    var_67 = (uint64_t *)var_66;
    var_68 = (uint64_t *)(var_0 + (-32L));
    var_69 = (uint64_t *)(var_0 + (-120L));
    local_sp_9 = var_59;
    r91_3 = var_61;
    r82_3 = var_62;
    while (1U)
        {
            local_sp_10 = local_sp_9;
            r91_4 = r91_3;
            r82_4 = r82_3;
            var_70 = local_sp_9 + (-8L);
            *(uint64_t *)var_70 = 4217749UL;
            var_71 = indirect_placeholder_3();
            *var_63 = var_71;
            local_sp_10 = var_70;
            if (*(uint32_t *)4317960UL != 4U & (long)var_71 < (long)*(uint64_t *)4318464UL) {
                var_72 = local_sp_9 + (-16L);
                *(uint64_t *)var_72 = 4217778UL;
                var_73 = indirect_placeholder_144(var_71);
                var_74 = var_73.field_0;
                var_75 = var_73.field_1;
                *(uint64_t *)4318464UL = (*(uint64_t *)4318464UL + 1000000000UL);
                local_sp_10 = var_72;
                r91_4 = var_74;
                r82_4 = var_75;
            }
            var_76 = *(uint64_t *)4318440UL + *(uint64_t *)4318432UL;
            var_77 = *(uint64_t *)4317952UL + (*(uint64_t *)4318392UL != 0UL);
            var_78 = helper_cc_compute_c_wrapper(var_76 - var_77, var_77, var_2, 17U);
            r91_1 = r91_4;
            r82_1 = r82_4;
            rcx_5 = var_76;
            r91_2 = r91_4;
            r82_2 = r82_4;
            local_sp_11 = local_sp_10;
            local_sp_12 = local_sp_10;
            r91_5 = r91_4;
            r82_5 = r82_4;
            if (var_78 != 0UL) {
                loop_state_var = 1U;
                break;
            }
            var_79 = (uint16_t)*(uint32_t *)4318400UL;
            if (((uint32_t)(var_79 & (unsigned short)1024U) == 0U) || ((uint32_t)(var_79 & (unsigned short)256U) == 0U)) {
                var_80 = local_sp_10 + (-8L);
                *(uint64_t *)var_80 = 4217930UL;
                indirect_placeholder();
                local_sp_11 = var_80;
            }
            var_81 = *(uint64_t *)4318432UL + *(uint64_t *)4318440UL;
            var_82 = *(uint64_t *)4317952UL;
            var_83 = helper_cc_compute_c_wrapper(var_81 - var_82, var_82, var_2, 17U);
            var_84 = (var_83 == 0UL);
            var_85 = *(uint64_t *)4318704UL;
            var_86 = *(uint64_t *)4318512UL;
            var_87 = local_sp_11 + (-8L);
            var_88 = (uint64_t *)var_87;
            rcx_0 = var_85;
            rcx_5 = var_85;
            rcx_4 = var_85;
            local_sp_8 = var_87;
            local_sp_12 = var_87;
            if (var_84) {
                *var_88 = 4217990UL;
                indirect_placeholder();
                *var_64 = var_86;
            } else {
                *var_88 = 4218027UL;
                indirect_placeholder();
                *var_64 = var_86;
            }
            if ((long)var_86 > (long)0UL) {
                var_106 = var_87 + (-8L);
                *(uint64_t *)var_106 = 4218050UL;
                indirect_placeholder_41(var_86);
                local_sp_2 = var_106;
                if (*(unsigned char *)4318696UL == '\x00') {
                    var_107 = *var_64;
                    var_108 = var_87 + (-16L);
                    *(uint64_t *)var_108 = 4218082UL;
                    var_109 = indirect_placeholder_138(var_107, 0UL);
                    rcx_0 = var_109.field_1;
                    local_sp_2 = var_108;
                    r91_1 = var_109.field_2;
                    r82_1 = var_109.field_3;
                }
            } else {
                if (var_86 != 0UL) {
                    *(unsigned char *)4318698UL = ((*(unsigned char *)4318696UL | *(unsigned char *)4318698UL) != '\x00');
                    if (*(unsigned char *)4318697UL != '\x00') {
                        loop_state_var = 3U;
                        break;
                    }
                    storemerge6 = 1UL;
                    if ((uint32_t)((uint16_t)*(uint32_t *)4318400UL & (unsigned short)512U) != 0U) {
                        loop_state_var = 3U;
                        break;
                    }
                    loop_state_var = 2U;
                    break;
                }
                var_89 = (uint16_t)*(uint32_t *)4318400UL & (unsigned short)256U;
                _pre_phi380 = var_89;
                if ((uint32_t)var_89 == 0U) {
                    var_90 = *(uint64_t *)4318304UL;
                    *(uint64_t *)(var_87 + (-8L)) = 4218236UL;
                    var_91 = indirect_placeholder_143(var_90, 4UL);
                    var_92 = var_91.field_0;
                    var_93 = var_91.field_1;
                    var_94 = var_91.field_2;
                    *(uint64_t *)(var_87 + (-16L)) = 4218244UL;
                    indirect_placeholder();
                    var_95 = (uint64_t)*(uint32_t *)var_92;
                    var_96 = var_87 + (-24L);
                    *(uint64_t *)var_96 = 4218271UL;
                    var_97 = indirect_placeholder_137(0UL, var_92, 4299037UL, var_95, 0UL, var_93, var_94);
                    _pre_phi380 = (uint16_t)*(uint32_t *)4318400UL & (unsigned short)256U;
                    rcx_4 = var_97.field_0;
                    local_sp_8 = var_96;
                    r91_2 = var_97.field_4;
                    r82_2 = var_97.field_5;
                } else {
                    if (*(uint32_t *)4317960UL == 1U) {
                        var_90 = *(uint64_t *)4318304UL;
                        *(uint64_t *)(var_87 + (-8L)) = 4218236UL;
                        var_91 = indirect_placeholder_143(var_90, 4UL);
                        var_92 = var_91.field_0;
                        var_93 = var_91.field_1;
                        var_94 = var_91.field_2;
                        *(uint64_t *)(var_87 + (-16L)) = 4218244UL;
                        indirect_placeholder();
                        var_95 = (uint64_t)*(uint32_t *)var_92;
                        var_96 = var_87 + (-24L);
                        *(uint64_t *)var_96 = 4218271UL;
                        var_97 = indirect_placeholder_137(0UL, var_92, 4299037UL, var_95, 0UL, var_93, var_94);
                        _pre_phi380 = (uint16_t)*(uint32_t *)4318400UL & (unsigned short)256U;
                        rcx_4 = var_97.field_0;
                        local_sp_8 = var_96;
                        r91_2 = var_97.field_4;
                        r82_2 = var_97.field_5;
                    }
                }
                rcx_5 = rcx_4;
                local_sp_12 = local_sp_8;
                r91_5 = r91_2;
                r82_5 = r82_2;
                if ((uint32_t)_pre_phi380 != 0U) {
                    *var_6 = 1U;
                    loop_state_var = 1U;
                    break;
                }
                *(uint64_t *)(local_sp_8 + (-8L)) = 4218291UL;
                indirect_placeholder();
                var_98 = *(uint64_t *)4318328UL - *var_5;
                *var_65 = var_98;
                *(uint64_t *)(local_sp_8 + (-16L)) = 4218323UL;
                indirect_placeholder_136(var_98, 0UL);
                var_99 = *var_65;
                var_100 = local_sp_8 + (-24L);
                *(uint64_t *)var_100 = 4218335UL;
                var_101 = indirect_placeholder_135(var_99);
                var_102 = var_101.field_0;
                var_103 = var_101.field_1;
                var_104 = var_101.field_2;
                var_105 = var_101.field_3;
                local_sp_9_be = var_100;
                rcx_0 = var_103;
                local_sp_2 = var_100;
                r91_1 = var_104;
                r82_1 = var_105;
                r91_3_be = var_104;
                r82_3_be = var_105;
                if ((uint64_t)(unsigned char)var_102 == 1UL) {
                    *var_6 = 1U;
                    *(unsigned char *)4318476UL = (unsigned char)'\x00';
                    *(uint32_t *)4318480UL = 29U;
                }
                if ((uint32_t)((uint16_t)*(uint32_t *)4318400UL & (unsigned short)1024U) != 0U) {
                    local_sp_9 = local_sp_9_be;
                    r91_3 = r91_3_be;
                    r82_3 = r82_3_be;
                    continue;
                }
                if (*var_5 != 0UL) {
                    local_sp_9 = local_sp_9_be;
                    r91_3 = r91_3_be;
                    r82_3 = r82_3_be;
                    continue;
                }
                *var_64 = 0UL;
            }
            var_110 = *var_64;
            *var_67 = var_110;
            var_111 = *(uint64_t *)4318328UL;
            var_112 = helper_cc_compute_c_wrapper(var_110 - var_111, var_111, var_2, 17U);
            rcx_1 = rcx_0;
            local_sp_3 = local_sp_2;
            rcx_2 = rcx_0;
            local_sp_4 = local_sp_2;
            if (var_112 == 0UL) {
                *(uint64_t *)4318440UL = (*(uint64_t *)4318440UL + 1UL);
                *var_5 = 0UL;
            } else {
                *(uint64_t *)4318432UL = (*(uint64_t *)4318432UL + 1UL);
                *var_5 = *var_67;
                var_113 = (uint16_t)*(uint32_t *)4318400UL;
                if ((uint32_t)(var_113 & (unsigned short)1024U) != 0U) {
                    if ((uint32_t)(var_113 & (unsigned short)256U) == 0U) {
                        var_114 = *(uint64_t *)4318512UL;
                        var_115 = local_sp_2 + (-8L);
                        *(uint64_t *)var_115 = 4218578UL;
                        indirect_placeholder();
                        rcx_1 = var_114;
                        local_sp_3 = var_115;
                    }
                    *var_67 = *(uint64_t *)4318328UL;
                    rcx_2 = rcx_1;
                    local_sp_4 = local_sp_3;
                }
            }
            var_116 = *(uint64_t *)4318512UL;
            var_119 = var_116;
            local_sp_5 = local_sp_4;
            if (var_116 == *(uint64_t *)4318520UL) {
                var_139 = *var_67;
                var_140 = local_sp_4 + (-8L);
                *(uint64_t *)var_140 = 4218670UL;
                var_141 = indirect_placeholder_139(rcx_2, var_139, var_116, 1UL, r91_1, r82_1);
                var_142 = var_141.field_0;
                var_143 = var_141.field_1;
                var_144 = var_141.field_2;
                *var_69 = var_142;
                *(uint64_t *)4318448UL = (var_142 + *(uint64_t *)4318448UL);
                var_145 = *var_67;
                local_sp_9_be = var_140;
                r91_3_be = var_143;
                r82_3_be = var_144;
                if (*var_69 != var_145) {
                    var_146 = *(uint64_t *)4318312UL;
                    *(uint64_t *)(local_sp_4 + (-16L)) = 4218728UL;
                    var_147 = indirect_placeholder_134(var_146, 4UL);
                    var_148 = var_147.field_0;
                    var_149 = var_147.field_1;
                    var_150 = var_147.field_2;
                    *(uint64_t *)(local_sp_4 + (-24L)) = 4218736UL;
                    indirect_placeholder();
                    var_151 = (uint64_t)*(uint32_t *)var_148;
                    *(uint64_t *)(local_sp_4 + (-32L)) = 4218763UL;
                    indirect_placeholder_133(0UL, var_148, 4299244UL, var_151, 0UL, var_149, var_150);
                    loop_state_var = 0U;
                    break;
                }
                if (var_145 == *(uint64_t *)4318328UL) {
                    *(uint64_t *)4318424UL = (*(uint64_t *)4318424UL + 1UL);
                } else {
                    *(uint64_t *)4318416UL = (*(uint64_t *)4318416UL + 1UL);
                }
            } else {
                if (*(unsigned char *)4318412UL == '\x00') {
                    var_117 = *var_67;
                    var_118 = local_sp_4 + (-8L);
                    *(uint64_t *)var_118 = 4218874UL;
                    indirect_placeholder_6(var_117, var_116);
                    var_119 = *(uint64_t *)4318512UL;
                    local_sp_5 = var_118;
                }
                var_122 = var_119;
                local_sp_6 = local_sp_5;
                if ((signed char)(unsigned char)*(uint32_t *)4318400UL > '\xff') {
                    *var_68 = var_119;
                } else {
                    var_120 = local_sp_5 + (-8L);
                    *(uint64_t *)var_120 = 4218914UL;
                    var_121 = indirect_placeholder_2(var_66, var_119);
                    *var_68 = var_121;
                    var_122 = var_121;
                    local_sp_6 = var_120;
                }
                var_123 = *(uint32_t *)4318400UL;
                if ((var_123 & 8U) == 0U) {
                    var_124 = *var_67;
                    var_125 = local_sp_6 + (-8L);
                    *(uint64_t *)var_125 = 4218966UL;
                    var_126 = indirect_placeholder_140(var_124, var_122, r91_1, r82_1);
                    var_127 = var_126.field_1;
                    var_128 = var_126.field_2;
                    local_sp_9_be = var_125;
                    r91_3_be = var_127;
                    r82_3_be = var_128;
                } else {
                    var_129 = ((var_123 & 16U) == 0U);
                    var_130 = *var_67;
                    var_131 = local_sp_6 + (-8L);
                    var_132 = (uint64_t *)var_131;
                    local_sp_9_be = var_131;
                    if (var_129) {
                        *var_132 = 4219033UL;
                        var_136 = indirect_placeholder_142(var_130, var_122);
                        var_137 = var_136.field_0;
                        var_138 = var_136.field_1;
                        r91_3_be = var_137;
                        r82_3_be = var_138;
                    } else {
                        *var_132 = 4219006UL;
                        var_133 = indirect_placeholder_141(var_130, var_122, r91_1, r82_1);
                        var_134 = var_133.field_1;
                        var_135 = var_133.field_2;
                        r91_3_be = var_134;
                        r82_3_be = var_135;
                    }
                }
            }
            local_sp_9 = local_sp_9_be;
            r91_3 = r91_3_be;
            r82_3 = r82_3_be;
            continue;
        }
    switch (loop_state_var) {
      case 0U:
        {
            return (uint64_t)rax_1_shrunk;
        }
        break;
      case 3U:
      case 2U:
      case 1U:
        {
            switch (loop_state_var) {
              case 1U:
                {
                    rcx_6 = rcx_5;
                    local_sp_13 = local_sp_12;
                    r91_6 = r91_5;
                    r82_6 = r82_5;
                    if (*(unsigned char *)4318976UL != '\x00') {
                        var_152 = *(uint32_t *)4318400UL;
                        if ((var_152 & 8U) == 0U) {
                            var_153 = local_sp_12 + (-8L);
                            *(uint64_t *)var_153 = 4219084UL;
                            var_154 = indirect_placeholder_145(1UL, 4318977UL, r91_5, r82_5);
                            rcx_6 = var_154.field_0;
                            local_sp_13 = var_153;
                            r91_6 = var_154.field_1;
                            r82_6 = var_154.field_2;
                        } else {
                            if ((var_152 & 16U) == 0U) {
                                var_155 = local_sp_12 + (-8L);
                                *(uint64_t *)var_155 = 4219114UL;
                                var_156 = indirect_placeholder_146(1UL, 4318977UL, r91_5, r82_5);
                                rcx_6 = var_156.field_0;
                                local_sp_13 = var_155;
                                r91_6 = var_156.field_1;
                                r82_6 = var_156.field_2;
                            } else {
                                var_157 = *(uint64_t *)4318520UL;
                                var_158 = *(uint64_t *)4318528UL;
                                *(uint64_t *)4318528UL = (var_158 + 1UL);
                                *(unsigned char *)(var_158 + var_157) = *(unsigned char *)4318977UL;
                                var_159 = *(uint64_t *)4318528UL;
                                var_160 = *(uint64_t *)4318336UL;
                                var_161 = helper_cc_compute_c_wrapper(var_159 - var_160, var_160, var_2, 17U);
                                rcx_6 = var_157;
                                if (var_161 == 0UL) {
                                    var_162 = local_sp_12 + (-8L);
                                    *(uint64_t *)var_162 = 4219178UL;
                                    var_163 = indirect_placeholder_147(var_157, r91_5, r82_5);
                                    rcx_6 = var_163.field_0;
                                    local_sp_13 = var_162;
                                    r91_6 = var_163.field_1;
                                    r82_6 = var_163.field_2;
                                }
                            }
                        }
                    }
                    r91_10 = r91_6;
                    rcx_9_ph = rcx_6;
                    rcx_10 = rcx_6;
                    rcx_7 = rcx_6;
                    local_sp_14 = local_sp_13;
                    r91_7 = r91_6;
                    r82_7 = r82_6;
                    local_sp_16_ph = local_sp_13;
                    r91_9_ph = r91_6;
                    r82_9_ph = r82_6;
                    local_sp_17 = local_sp_13;
                    r82_10 = r82_6;
                    if ((*(uint32_t *)4318400UL & 8U) == 0U) {
                        var_164 = *(uint64_t *)4318536UL;
                        var_166 = var_164;
                        if (var_164 != 0UL) {
                            var_165 = (uint64_t *)(var_0 + (-72L));
                            *var_165 = var_164;
                            var_167 = *(uint64_t *)4318344UL;
                            var_168 = helper_cc_compute_c_wrapper(var_166 - var_167, var_167, var_2, 17U);
                            rcx_9_ph = rcx_7;
                            local_sp_15 = local_sp_14;
                            r91_8 = r91_7;
                            r82_8 = r82_7;
                            local_sp_16_ph = local_sp_14;
                            r91_9_ph = r91_7;
                            r82_9_ph = r82_7;
                            while (var_168 != 0UL)
                                {
                                    var_169 = *(uint64_t *)4318520UL;
                                    var_170 = *(uint64_t *)4318528UL;
                                    *(uint64_t *)4318528UL = (var_170 + 1UL);
                                    *(unsigned char *)(var_170 + var_169) = *(unsigned char *)4317965UL;
                                    var_171 = *(uint64_t *)4318528UL;
                                    var_172 = *(uint64_t *)4318336UL;
                                    var_173 = helper_cc_compute_c_wrapper(var_171 - var_172, var_172, var_2, 17U);
                                    rcx_8 = var_169;
                                    if (var_173 == 0UL) {
                                        var_174 = local_sp_14 + (-8L);
                                        *(uint64_t *)var_174 = 4219278UL;
                                        var_175 = indirect_placeholder_148(var_169, r91_7, r82_7);
                                        rcx_8 = var_175.field_0;
                                        local_sp_15 = var_174;
                                        r91_8 = var_175.field_1;
                                        r82_8 = var_175.field_2;
                                    }
                                    var_176 = *var_165 + 1UL;
                                    *var_165 = var_176;
                                    var_166 = var_176;
                                    rcx_7 = rcx_8;
                                    local_sp_14 = local_sp_15;
                                    r91_7 = r91_8;
                                    r82_7 = r82_8;
                                    var_167 = *(uint64_t *)4318344UL;
                                    var_168 = helper_cc_compute_c_wrapper(var_166 - var_167, var_167, var_2, 17U);
                                    rcx_9_ph = rcx_7;
                                    local_sp_15 = local_sp_14;
                                    r91_8 = r91_7;
                                    r82_8 = r82_7;
                                    local_sp_16_ph = local_sp_14;
                                    r91_9_ph = r91_7;
                                    r82_9_ph = r82_7;
                                }
                            rcx_10 = rcx_9_ph;
                            local_sp_17 = local_sp_16_ph;
                            r91_10 = r91_9_ph;
                            r82_10 = r82_9_ph;
                            var_177 = *(uint64_t *)4318520UL;
                            var_178 = *(uint64_t *)4318528UL;
                            *(uint64_t *)4318528UL = (var_178 + 1UL);
                            *(unsigned char *)(var_178 + var_177) = *(unsigned char *)4317964UL;
                            var_179 = *(uint64_t *)4318528UL;
                            var_180 = *(uint64_t *)4318336UL;
                            var_181 = helper_cc_compute_c_wrapper(var_179 - var_180, var_180, var_2, 17U);
                            rcx_10 = var_177;
                            if (*(uint64_t *)4318536UL != 0UL & (*(uint32_t *)4318400UL & 16U) != 0U & var_181 == 0UL) {
                                var_182 = local_sp_16_ph + (-8L);
                                *(uint64_t *)var_182 = 4219383UL;
                                var_183 = indirect_placeholder_149(var_177, r91_9_ph, r82_9_ph);
                                rcx_10 = var_183.field_0;
                                local_sp_17 = var_182;
                                r91_10 = var_183.field_1;
                                r82_10 = var_183.field_2;
                            }
                        }
                    } else {
                        rcx_10 = rcx_9_ph;
                        local_sp_17 = local_sp_16_ph;
                        r91_10 = r91_9_ph;
                        r82_10 = r82_9_ph;
                        var_177 = *(uint64_t *)4318520UL;
                        var_178 = *(uint64_t *)4318528UL;
                        *(uint64_t *)4318528UL = (var_178 + 1UL);
                        *(unsigned char *)(var_178 + var_177) = *(unsigned char *)4317964UL;
                        var_179 = *(uint64_t *)4318528UL;
                        var_180 = *(uint64_t *)4318336UL;
                        var_181 = helper_cc_compute_c_wrapper(var_179 - var_180, var_180, var_2, 17U);
                        rcx_10 = var_177;
                        if (*(uint64_t *)4318536UL != 0UL & (*(uint32_t *)4318400UL & 16U) != 0U & var_181 == 0UL) {
                            var_182 = local_sp_16_ph + (-8L);
                            *(uint64_t *)var_182 = 4219383UL;
                            var_183 = indirect_placeholder_149(var_177, r91_9_ph, r82_9_ph);
                            rcx_10 = var_183.field_0;
                            local_sp_17 = var_182;
                            r91_10 = var_183.field_1;
                            r82_10 = var_183.field_2;
                        }
                    }
                    var_184 = *(uint64_t *)4318528UL;
                    local_sp_18 = local_sp_17;
                    if (var_184 != 0UL) {
                        var_185 = *(uint64_t *)4318520UL;
                        var_186 = local_sp_17 + (-8L);
                        *(uint64_t *)var_186 = 4219426UL;
                        var_187 = indirect_placeholder_150(rcx_10, var_184, var_185, 1UL, r91_10, r82_10);
                        var_188 = var_187.field_0;
                        var_189 = (uint64_t *)(var_0 + (-128L));
                        *var_189 = var_188;
                        *(uint64_t *)4318448UL = (var_188 + *(uint64_t *)4318448UL);
                        local_sp_18 = var_186;
                        if (*var_189 == 0UL) {
                            *(uint64_t *)4318416UL = (*(uint64_t *)4318416UL + 1UL);
                            var_190 = *var_189;
                        }
                        if (var_190 != *(uint64_t *)4318528UL) {
                            var_191 = *(uint64_t *)4318312UL;
                            *(uint64_t *)(local_sp_17 + (-16L)) = 4219509UL;
                            var_192 = indirect_placeholder_132(var_191, 4UL);
                            var_193 = var_192.field_0;
                            var_194 = var_192.field_1;
                            var_195 = var_192.field_2;
                            *(uint64_t *)(local_sp_17 + (-24L)) = 4219517UL;
                            indirect_placeholder();
                            var_196 = (uint64_t)*(uint32_t *)var_193;
                            *(uint64_t *)(local_sp_17 + (-32L)) = 4219544UL;
                            indirect_placeholder_131(0UL, var_193, 4299244UL, var_196, 0UL, var_194, var_195);
                            return (uint64_t)rax_1_shrunk;
                        }
                    }
                    local_sp_20 = local_sp_18;
                    if (*(unsigned char *)4318384UL == '\x00') {
                        var_203 = (uint16_t)*(uint32_t *)4318400UL;
                        var_204 = (uint32_t)(var_203 & (unsigned short)16384U);
                        var_205 = (uint64_t)var_204;
                        _pre_phi = var_203;
                        if (var_204 != 0U) {
                            *(uint64_t *)(local_sp_18 + (-8L)) = 4219842UL;
                            indirect_placeholder();
                            var_206 = local_sp_18 + (-16L);
                            *(uint64_t *)var_206 = 4219851UL;
                            indirect_placeholder();
                            var_207 = *(uint32_t *)var_205;
                            var_208 = (uint64_t)var_207;
                            local_sp_0 = var_206;
                            var_209 = local_sp_18 + (-24L);
                            *(uint64_t *)var_209 = 4219863UL;
                            indirect_placeholder();
                            local_sp_0 = var_209;
                            if ((uint64_t)(var_207 + (-38)) != 0UL & *(uint32_t *)var_208 == 22U) {
                                var_210 = *(uint64_t *)4318312UL;
                                *(uint64_t *)(local_sp_18 + (-32L)) = 4219890UL;
                                var_211 = indirect_placeholder_126(var_210, 4UL);
                                var_212 = var_211.field_0;
                                var_213 = var_211.field_1;
                                var_214 = var_211.field_2;
                                *(uint64_t *)(local_sp_18 + (-40L)) = 4219898UL;
                                indirect_placeholder();
                                var_215 = (uint64_t)*(uint32_t *)var_212;
                                var_216 = local_sp_18 + (-48L);
                                *(uint64_t *)var_216 = 4219925UL;
                                indirect_placeholder_125(0UL, var_212, 4299314UL, var_215, 0UL, var_213, var_214);
                                *var_6 = 1U;
                                local_sp_0 = var_216;
                            }
                            var_217 = *(uint32_t *)4318400UL;
                            var_218 = (var_217 & (-65281)) | ((uint32_t)((uint16_t)var_217 & (unsigned short)32512U) | 32768U);
                            *(uint32_t *)4318400UL = var_218;
                            _pre_phi = (uint16_t)var_218;
                            local_sp_20 = local_sp_0;
                        }
                        var_219 = (uint32_t)(_pre_phi & (unsigned short)32768U);
                        local_sp_21 = local_sp_20;
                        if (var_219 == 0U) {
                            rax_0 = (uint64_t)var_219;
                            while (1U)
                                {
                                    *(uint64_t *)(local_sp_21 + (-8L)) = 4220048UL;
                                    indirect_placeholder();
                                    if (rax_0 != 0UL) {
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    var_220 = local_sp_21 + (-16L);
                                    *(uint64_t *)var_220 = 4219969UL;
                                    indirect_placeholder();
                                    var_221 = *(uint32_t *)rax_0;
                                    rax_0 = (uint64_t)var_221;
                                    local_sp_21 = var_220;
                                    if ((uint64_t)(var_221 + (-4)) == 0UL) {
                                        continue;
                                    }
                                    var_222 = *(uint64_t *)4318312UL;
                                    *(uint64_t *)(local_sp_21 + (-24L)) = 4219996UL;
                                    var_223 = indirect_placeholder_124(var_222, 4UL);
                                    var_224 = var_223.field_0;
                                    var_225 = var_223.field_1;
                                    var_226 = var_223.field_2;
                                    *(uint64_t *)(local_sp_21 + (-32L)) = 4220004UL;
                                    indirect_placeholder();
                                    var_227 = (uint64_t)*(uint32_t *)var_224;
                                    *(uint64_t *)(local_sp_21 + (-40L)) = 4220031UL;
                                    indirect_placeholder_123(0UL, var_224, 4299338UL, var_227, 0UL, var_225, var_226);
                                    loop_state_var = 0U;
                                    break;
                                }
                            var_228 = *var_6;
                            rax_1_shrunk = var_228;
                        } else {
                            var_228 = *var_6;
                            rax_1_shrunk = var_228;
                        }
                    } else {
                        *(uint64_t *)(local_sp_18 + (-8L)) = 4219589UL;
                        indirect_placeholder();
                        var_197 = *(uint64_t *)4318312UL;
                        *(uint64_t *)(local_sp_18 + (-16L)) = 4219613UL;
                        var_198 = indirect_placeholder_130(var_197, 4UL);
                        var_199 = var_198.field_0;
                        var_200 = var_198.field_1;
                        var_201 = var_198.field_2;
                        *(uint64_t *)(local_sp_18 + (-24L)) = 4219621UL;
                        indirect_placeholder();
                        var_202 = (uint64_t)*(uint32_t *)var_199;
                        *(uint64_t *)(local_sp_18 + (-32L)) = 4219648UL;
                        indirect_placeholder_129(0UL, var_199, 4298989UL, var_202, 0UL, var_200, var_201);
                    }
                }
                break;
              case 3U:
              case 2U:
                {
                    switch (loop_state_var) {
                      case 2U:
                        {
                            *(unsigned char *)4318699UL = ((storemerge6 | (uint64_t)*(unsigned char *)4318699UL) != 0UL);
                        }
                        break;
                      case 3U:
                        {
                        }
                        break;
                    }
                }
                break;
            }
        }
        break;
    }
}
