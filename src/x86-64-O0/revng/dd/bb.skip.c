typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_skip_ret_type;
struct helper_divq_EAX_wrapper_ret_type;
struct type_6;
struct indirect_placeholder_101_ret_type;
struct indirect_placeholder_100_ret_type;
struct indirect_placeholder_102_ret_type;
struct indirect_placeholder_99_ret_type;
struct indirect_placeholder_105_ret_type;
struct indirect_placeholder_104_ret_type;
struct indirect_placeholder_106_ret_type;
struct indirect_placeholder_103_ret_type;
struct indirect_placeholder_107_ret_type;
struct indirect_placeholder_108_ret_type;
struct indirect_placeholder_110_ret_type;
struct indirect_placeholder_109_ret_type;
struct indirect_placeholder_111_ret_type;
struct bb_skip_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct helper_divq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint32_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint32_t field_6;
    uint64_t field_7;
    uint32_t field_8;
    uint32_t field_9;
    unsigned char field_10;
    uint32_t field_11;
    uint32_t field_12;
};
struct type_6 {
};
struct indirect_placeholder_101_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_100_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_102_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_99_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_105_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_104_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_106_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_103_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_107_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_108_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_110_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_109_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_111_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r10(void);
extern void indirect_placeholder_6(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_41(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern uint32_t init_state_0x8248(void);
extern struct helper_divq_EAX_wrapper_ret_type helper_divq_EAX_wrapper(struct type_6 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint64_t param_13, uint32_t param_14, uint64_t param_15, uint64_t param_16, uint64_t param_17, uint32_t param_18, uint32_t param_19);
extern uint64_t indirect_placeholder_34(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint64_t init_r9(void);
extern struct indirect_placeholder_101_ret_type indirect_placeholder_101(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_100_ret_type indirect_placeholder_100(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_102_ret_type indirect_placeholder_102(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_99_ret_type indirect_placeholder_99(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_105_ret_type indirect_placeholder_105(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_104_ret_type indirect_placeholder_104(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_106_ret_type indirect_placeholder_106(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_103_ret_type indirect_placeholder_103(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_107_ret_type indirect_placeholder_107(void);
extern struct indirect_placeholder_108_ret_type indirect_placeholder_108(void);
extern struct indirect_placeholder_110_ret_type indirect_placeholder_110(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_109_ret_type indirect_placeholder_109(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_111_ret_type indirect_placeholder_111(uint64_t param_0);
struct bb_skip_ret_type bb_skip(uint64_t rcx, uint64_t rdx, uint64_t rsi, uint64_t rdi, uint64_t r8) {
    uint64_t *var_52;
    uint64_t var_53;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint32_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint32_t var_11;
    uint32_t var_12;
    uint64_t var_13;
    uint32_t *var_14;
    uint64_t *var_15;
    uint64_t *var_16;
    uint64_t *var_17;
    uint64_t var_18;
    uint64_t *var_19;
    uint64_t var_20;
    uint64_t **var_21;
    uint64_t var_22;
    uint64_t *var_23;
    uint64_t var_24;
    uint64_t var_26;
    uint64_t var_27;
    uint32_t var_28;
    uint64_t var_29;
    struct helper_divq_EAX_wrapper_ret_type var_25;
    uint32_t var_30;
    uint32_t var_31;
    uint64_t rax_0;
    uint64_t var_105;
    uint64_t local_sp_0;
    bool var_90;
    uint64_t var_91;
    uint64_t *var_92;
    struct indirect_placeholder_101_ret_type var_99;
    uint64_t var_100;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t var_104;
    struct indirect_placeholder_102_ret_type var_93;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t var_98;
    uint64_t r9_3;
    uint64_t local_sp_1;
    uint64_t var_106;
    uint64_t var_107;
    uint64_t var_108;
    uint64_t var_109;
    uint64_t r9_0_ph;
    uint64_t var_87;
    uint64_t local_sp_2;
    uint64_t var_88;
    uint64_t *storemerge5_in_pre_phi;
    uint64_t storemerge5;
    uint64_t var_89;
    uint64_t local_sp_3;
    bool var_64;
    uint64_t var_65;
    uint64_t *var_66;
    struct indirect_placeholder_105_ret_type var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    struct indirect_placeholder_106_ret_type var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    bool var_79;
    uint64_t var_80;
    uint64_t *var_81;
    struct indirect_placeholder_107_ret_type var_84;
    uint64_t var_85;
    uint64_t *var_86;
    struct indirect_placeholder_108_ret_type var_82;
    uint64_t var_83;
    uint64_t var_54;
    uint64_t var_55;
    struct indirect_placeholder_109_ret_type var_45;
    struct helper_divq_EAX_wrapper_ret_type var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t r87_2;
    uint64_t var_37;
    uint64_t var_38;
    struct indirect_placeholder_110_ret_type var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    struct indirect_placeholder_111_ret_type var_50;
    uint64_t var_51;
    struct bb_skip_ret_type mrv;
    struct bb_skip_ret_type mrv1;
    struct bb_skip_ret_type mrv2;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t local_sp_5;
    uint32_t var_59;
    uint32_t *var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = init_r10();
    var_4 = init_r9();
    var_5 = init_rbx();
    var_6 = init_state_0x9018();
    var_7 = init_state_0x9010();
    var_8 = init_state_0x8408();
    var_9 = init_state_0x8328();
    var_10 = init_state_0x82d8();
    var_11 = init_state_0x9080();
    var_12 = init_state_0x8248();
    var_13 = var_0 + (-8L);
    *(uint64_t *)var_13 = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    var_14 = (uint32_t *)(var_0 + (-204L));
    *var_14 = (uint32_t)rdi;
    var_15 = (uint64_t *)(var_0 + (-216L));
    *var_15 = rsi;
    var_16 = (uint64_t *)(var_0 + (-224L));
    *var_16 = rdx;
    var_17 = (uint64_t *)(var_0 + (-232L));
    *var_17 = rcx;
    var_18 = var_0 + (-240L);
    var_19 = (uint64_t *)var_18;
    *var_19 = r8;
    var_20 = *var_17 * *var_16;
    var_21 = (uint64_t **)var_18;
    var_22 = **var_21 + var_20;
    var_23 = (uint64_t *)(var_0 + (-32L));
    *var_23 = var_22;
    var_24 = var_0 + (-256L);
    *(uint64_t *)var_24 = 4214483UL;
    indirect_placeholder();
    *(uint32_t *)var_22 = 0U;
    var_25 = helper_divq_EAX_wrapper((struct type_6 *)(0UL), *var_17, 4214504UL, 9223372036854775807UL, var_13, rcx, 0UL, rsi, rdi, var_3, var_4, r8, var_5, var_6, var_7, var_8, var_9, var_10, var_11, var_12);
    var_26 = var_25.field_1;
    var_27 = var_25.field_5;
    var_28 = var_25.field_6;
    var_29 = var_25.field_7;
    var_30 = var_25.field_8;
    var_31 = var_25.field_9;
    rax_0 = var_26;
    r9_3 = var_4;
    storemerge5_in_pre_phi = var_17;
    r87_2 = r8;
    local_sp_5 = var_24;
    if (*var_16 <= var_26) {
        var_32 = *var_23;
        var_33 = (uint64_t)*var_14;
        var_34 = *var_15;
        var_35 = var_0 + (-264L);
        *(uint64_t *)var_35 = 4214554UL;
        var_36 = indirect_placeholder_34(1UL, var_32, var_33, var_34);
        rax_0 = var_36;
        local_sp_5 = var_35;
        if ((long)var_36 >= (long)0UL) {
            if (*var_14 == 0U) {
                *var_16 = 0UL;
                **var_21 = 0UL;
            } else {
                var_37 = var_0 + (-200L);
                *(uint64_t *)(var_0 + (-272L)) = 4214596UL;
                indirect_placeholder();
                var_38 = *var_15;
                *(uint64_t *)(var_0 + (-280L)) = 4214620UL;
                var_39 = indirect_placeholder_110(var_38, 4UL);
                var_40 = var_39.field_0;
                var_41 = var_39.field_1;
                var_42 = var_39.field_2;
                *(uint64_t *)(var_0 + (-288L)) = 4214628UL;
                indirect_placeholder();
                var_43 = (uint64_t)*(uint32_t *)var_40;
                var_44 = var_0 + (-296L);
                *(uint64_t *)var_44 = 4214655UL;
                var_45 = indirect_placeholder_109(0UL, var_40, 4298989UL, var_43, 1UL, var_41, var_42);
                var_46 = var_45.field_1;
                var_47 = var_45.field_3;
                var_48 = var_45.field_4;
                var_49 = var_45.field_5;
                *(uint64_t *)(var_44 + (-8L)) = 4214670UL;
                var_50 = indirect_placeholder_111(var_37);
                var_51 = var_50.field_1;
                r9_3 = var_48;
                r87_2 = var_49;
                if ((uint64_t)(unsigned char)var_50.field_0 == 0UL) {
                    *var_16 = 0UL;
                    var_58 = *var_23;
                } else {
                    var_52 = (uint64_t *)(var_0 + (-152L));
                    var_53 = *var_52;
                    var_54 = *var_23 + *(uint64_t *)4318488UL;
                    var_55 = helper_cc_compute_c_wrapper(var_53 - var_54, var_54, var_2, 17U);
                    if (var_55 == 0UL) {
                        *var_16 = 0UL;
                        var_58 = *var_23;
                    } else {
                        var_56 = helper_divq_EAX_wrapper((struct type_6 *)(0UL), *var_17, 4214725UL, *var_23 - *var_52, var_13, var_53, 0UL, var_46, var_51, var_47, var_48, var_49, var_40, var_27, var_28, var_8, var_9, var_29, var_30, var_31);
                        *var_16 = var_56.field_1;
                        var_57 = *var_52 - *(uint64_t *)4318488UL;
                        *var_23 = var_57;
                        var_58 = var_57;
                    }
                }
                *(uint64_t *)(var_44 + (-16L)) = 4214791UL;
                indirect_placeholder_41(var_58);
            }
            mrv.field_0 = *var_16;
            mrv1 = mrv;
            mrv1.field_1 = r9_3;
            mrv2 = mrv1;
            mrv2.field_2 = r87_2;
            return mrv2;
        }
    }
    *(uint64_t *)(local_sp_5 + (-8L)) = 4214835UL;
    indirect_placeholder();
    var_59 = *(uint32_t *)rax_0;
    var_60 = (uint32_t *)(var_0 + (-36L));
    *var_60 = var_59;
    var_61 = (uint64_t)*var_14;
    var_62 = *var_15;
    *(uint64_t *)(local_sp_5 + (-16L)) = 4214871UL;
    var_63 = indirect_placeholder_34(2UL, 0UL, var_61, var_62);
    if ((long)var_63 >= (long)0UL) {
        if (*var_60 == 0U) {
            *var_60 = 75U;
        }
        var_64 = (*var_14 == 0U);
        var_65 = *var_15;
        var_66 = (uint64_t *)(local_sp_5 + (-24L));
        if (var_64) {
            *var_66 = 4214927UL;
            var_73 = indirect_placeholder_105(var_65, 3UL, 0UL);
            var_74 = var_73.field_0;
            var_75 = var_73.field_1;
            var_76 = var_73.field_2;
            var_77 = (uint64_t)*var_60;
            var_78 = local_sp_5 + (-32L);
            *(uint64_t *)var_78 = 4214958UL;
            indirect_placeholder_104(0UL, var_74, 4299005UL, var_77, 0UL, var_75, var_76);
            local_sp_3 = var_78;
        } else {
            *var_66 = 4214985UL;
            var_67 = indirect_placeholder_106(var_65, 3UL, 0UL);
            var_68 = var_67.field_0;
            var_69 = var_67.field_1;
            var_70 = var_67.field_2;
            var_71 = (uint64_t)*var_60;
            var_72 = local_sp_5 + (-32L);
            *(uint64_t *)var_72 = 4215016UL;
            indirect_placeholder_103(0UL, var_68, 4299021UL, var_71, 0UL, var_69, var_70);
            local_sp_3 = var_72;
        }
        *(uint64_t *)(local_sp_3 + (-8L)) = 4215026UL;
        indirect_placeholder_6(var_13, 1UL);
        abort();
    }
    var_79 = (*var_14 == 0U);
    var_80 = local_sp_5 + (-24L);
    var_81 = (uint64_t *)var_80;
    local_sp_2 = var_80;
    if (var_79) {
        *var_81 = 4215040UL;
        var_84 = indirect_placeholder_107();
        var_85 = var_84.field_0;
        *(uint64_t *)(var_0 + (-48L)) = *(uint64_t *)4318512UL;
        r9_0_ph = var_85;
    } else {
        *var_81 = 4215058UL;
        var_82 = indirect_placeholder_108();
        var_83 = var_82.field_0;
        *(uint64_t *)(var_0 + (-48L)) = *(uint64_t *)4318520UL;
        r9_0_ph = var_83;
    }
    var_86 = (uint64_t *)(var_0 + (-56L));
    var_87 = *var_16;
    r9_3 = r9_0_ph;
    while (1U)
        {
            var_88 = *(uint64_t *)4318704UL;
            r87_2 = var_88;
            if (var_87 == 0UL) {
                storemerge5_in_pre_phi = (uint64_t *)*var_19;
            }
            storemerge5 = *storemerge5_in_pre_phi;
            var_89 = local_sp_2 + (-8L);
            *(uint64_t *)var_89 = 4215123UL;
            indirect_placeholder();
            *var_86 = storemerge5;
            local_sp_1 = var_89;
            if ((long)storemerge5 > (long)18446744073709551615UL) {
                if (storemerge5 != 0UL) {
                    loop_state_var = 1U;
                    break;
                }
                if (*var_14 == 0U) {
                    var_106 = local_sp_2 + (-16L);
                    *(uint64_t *)var_106 = 4215318UL;
                    indirect_placeholder_41(storemerge5);
                    local_sp_1 = var_106;
                }
                var_107 = *var_16;
                local_sp_2 = local_sp_1;
                if (var_107 == 0UL) {
                    **var_21 = 0UL;
                    var_109 = *var_16;
                } else {
                    var_108 = var_107 + (-1L);
                    *var_16 = var_108;
                    var_109 = var_108;
                }
                var_87 = var_109;
                if (var_109 != 0UL) {
                    continue;
                }
                if (**var_21 == 0UL) {
                    continue;
                }
                loop_state_var = 1U;
                break;
            }
            var_90 = (*var_14 == 0U);
            var_91 = *var_15;
            var_92 = (uint64_t *)(local_sp_2 + (-16L));
            if (var_90) {
                *var_92 = 4215167UL;
                var_99 = indirect_placeholder_101(var_91, 4UL);
                var_100 = var_99.field_0;
                var_101 = var_99.field_1;
                var_102 = var_99.field_2;
                *(uint64_t *)(local_sp_2 + (-24L)) = 4215175UL;
                indirect_placeholder();
                var_103 = (uint64_t)*(uint32_t *)var_100;
                var_104 = local_sp_2 + (-32L);
                *(uint64_t *)var_104 = 4215202UL;
                indirect_placeholder_100(0UL, var_100, 4299037UL, var_103, 0UL, var_101, var_102);
                local_sp_0 = var_104;
                if ((uint32_t)((uint16_t)*(uint32_t *)4318400UL & (unsigned short)256U) != 0U) {
                    loop_state_var = 0U;
                    break;
                }
                var_105 = local_sp_2 + (-40L);
                *(uint64_t *)var_105 = 4215222UL;
                indirect_placeholder();
                local_sp_0 = var_105;
                loop_state_var = 0U;
                break;
            }
            *var_92 = 4215249UL;
            var_93 = indirect_placeholder_102(var_91, 3UL, 0UL);
            var_94 = var_93.field_0;
            var_95 = var_93.field_1;
            var_96 = var_93.field_2;
            var_97 = (uint64_t)*var_60;
            var_98 = local_sp_2 + (-24L);
            *(uint64_t *)var_98 = 4215280UL;
            indirect_placeholder_99(0UL, var_94, 4299021UL, var_97, 0UL, var_95, var_96);
            local_sp_0 = var_98;
            loop_state_var = 0U;
            break;
        }
    switch (loop_state_var) {
      case 1U:
        {
            mrv.field_0 = *var_16;
            mrv1 = mrv;
            mrv1.field_1 = r9_3;
            mrv2 = mrv1;
            mrv2.field_2 = r87_2;
            return mrv2;
        }
        break;
      case 0U:
        {
            *(uint64_t *)(local_sp_0 + (-8L)) = 4215290UL;
            indirect_placeholder_6(var_13, 1UL);
            abort();
        }
        break;
    }
}
