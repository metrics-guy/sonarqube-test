typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct helper_divq_EAX_wrapper_ret_type;
struct type_5;
struct indirect_placeholder_96_ret_type;
struct indirect_placeholder_77_ret_type;
struct indirect_placeholder_78_ret_type;
struct indirect_placeholder_79_ret_type;
struct indirect_placeholder_80_ret_type;
struct indirect_placeholder_81_ret_type;
struct indirect_placeholder_87_ret_type;
struct indirect_placeholder_86_ret_type;
struct indirect_placeholder_88_ret_type;
struct indirect_placeholder_85_ret_type;
struct indirect_placeholder_89_ret_type;
struct indirect_placeholder_90_ret_type;
struct indirect_placeholder_91_ret_type;
struct indirect_placeholder_92_ret_type;
struct indirect_placeholder_93_ret_type;
struct indirect_placeholder_95_ret_type;
struct indirect_placeholder_94_ret_type;
struct indirect_placeholder_84_ret_type;
struct indirect_placeholder_97_ret_type;
struct indirect_placeholder_83_ret_type;
struct indirect_placeholder_98_ret_type;
struct indirect_placeholder_82_ret_type;
struct helper_divq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint32_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint32_t field_6;
    uint64_t field_7;
    uint32_t field_8;
    uint32_t field_9;
    unsigned char field_10;
    uint32_t field_11;
    uint32_t field_12;
};
struct type_5 {
};
struct indirect_placeholder_96_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_77_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_78_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_79_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_80_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_81_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_87_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_86_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_88_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_85_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_89_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_90_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_91_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_92_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_93_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_95_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_94_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_84_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_97_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_83_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_98_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_82_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_6(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_3(void);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern uint32_t init_state_0x8248(void);
extern struct helper_divq_EAX_wrapper_ret_type helper_divq_EAX_wrapper(struct type_5 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint64_t param_13, uint32_t param_14, uint64_t param_15, uint64_t param_16, uint64_t param_17, uint32_t param_18, uint32_t param_19);
extern uint64_t indirect_placeholder_12(uint64_t param_0);
extern uint64_t init_rcx(void);
extern struct indirect_placeholder_96_ret_type indirect_placeholder_96(uint64_t param_0);
extern struct indirect_placeholder_77_ret_type indirect_placeholder_77(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_78_ret_type indirect_placeholder_78(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_79_ret_type indirect_placeholder_79(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_80_ret_type indirect_placeholder_80(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_81_ret_type indirect_placeholder_81(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_87_ret_type indirect_placeholder_87(uint64_t param_0);
extern struct indirect_placeholder_86_ret_type indirect_placeholder_86(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_88_ret_type indirect_placeholder_88(uint64_t param_0);
extern struct indirect_placeholder_85_ret_type indirect_placeholder_85(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_89_ret_type indirect_placeholder_89(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_90_ret_type indirect_placeholder_90(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_91_ret_type indirect_placeholder_91(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_92_ret_type indirect_placeholder_92(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_93_ret_type indirect_placeholder_93(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_95_ret_type indirect_placeholder_95(uint64_t param_0);
extern struct indirect_placeholder_94_ret_type indirect_placeholder_94(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_84_ret_type indirect_placeholder_84(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_97_ret_type indirect_placeholder_97(uint64_t param_0);
extern struct indirect_placeholder_83_ret_type indirect_placeholder_83(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_98_ret_type indirect_placeholder_98(uint64_t param_0);
extern struct indirect_placeholder_82_ret_type indirect_placeholder_82(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
void bb_scanargs(uint64_t rsi, uint64_t rdi, uint64_t r10, uint64_t r9, uint64_t r8, uint64_t rbx) {
    uint32_t var_32;
    uint32_t var_33;
    uint32_t var_35;
    struct indirect_placeholder_96_ret_type var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint32_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint32_t var_9;
    uint32_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint32_t *var_13;
    uint64_t *var_14;
    uint64_t *var_15;
    uint64_t *var_16;
    uint64_t *var_17;
    uint64_t *var_18;
    uint32_t var_19;
    uint32_t *var_20;
    uint64_t *var_21;
    uint64_t *var_22;
    uint64_t var_23;
    uint32_t *var_24;
    uint64_t *var_25;
    uint64_t *var_26;
    uint64_t *var_27;
    uint32_t var_28;
    uint64_t var_208;
    unsigned char storemerge3;
    unsigned char storemerge4;
    uint64_t var_112;
    uint64_t var_113;
    uint64_t var_114;
    uint64_t local_sp_0;
    uint64_t var_94;
    uint64_t r94_0;
    uint64_t r85_0;
    uint64_t local_sp_1;
    uint64_t var_107;
    uint64_t r94_1;
    uint64_t r85_1;
    uint64_t local_sp_2;
    struct indirect_placeholder_78_ret_type var_108;
    uint64_t rcx_0;
    uint64_t var_109;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t var_102;
    uint64_t r94_2;
    uint64_t r85_2;
    uint64_t local_sp_3;
    struct indirect_placeholder_79_ret_type var_103;
    uint64_t rcx_1;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t var_97;
    uint64_t r94_3;
    uint64_t r85_3;
    uint64_t local_sp_4;
    struct indirect_placeholder_80_ret_type var_98;
    uint64_t rcx_2;
    uint64_t var_99;
    uint64_t var_100;
    uint64_t var_101;
    uint64_t var_92;
    uint64_t r85_5;
    uint64_t local_sp_7;
    struct indirect_placeholder_81_ret_type var_93;
    unsigned char storemerge1;
    uint64_t rcx_3;
    uint64_t var_161;
    uint64_t var_162;
    uint64_t var_163;
    uint64_t var_164;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t rcx_4;
    uint64_t var_203;
    struct indirect_placeholder_87_ret_type var_204;
    uint64_t var_205;
    uint64_t var_206;
    uint64_t var_207;
    uint64_t var_186;
    uint64_t var_187;
    uint64_t var_188;
    uint64_t var_183;
    uint64_t var_184;
    uint64_t var_185;
    uint64_t var_180;
    uint64_t var_181;
    uint64_t var_182;
    uint64_t var_176;
    uint64_t var_177;
    uint64_t var_178;
    uint64_t var_179;
    uint64_t var_172;
    uint64_t var_173;
    uint64_t var_174;
    uint64_t var_175;
    uint64_t rsi1_0;
    uint64_t var_168;
    uint64_t rdi2_0;
    uint64_t local_sp_5;
    uint64_t var_189;
    uint64_t var_190;
    uint64_t var_191;
    uint32_t var_195;
    uint64_t var_192;
    uint64_t var_193;
    uint64_t var_194;
    uint64_t rsi1_2;
    uint64_t var_196;
    struct indirect_placeholder_88_ret_type var_197;
    uint64_t var_198;
    uint64_t var_199;
    uint64_t storemerge5;
    uint64_t var_200;
    struct indirect_placeholder_85_ret_type var_201;
    uint64_t var_169;
    uint64_t var_170;
    uint64_t var_171;
    uint64_t var_150;
    uint64_t var_151;
    struct indirect_placeholder_89_ret_type var_152;
    uint64_t var_153;
    uint64_t var_154;
    uint64_t var_155;
    uint64_t var_156;
    uint64_t var_157;
    uint64_t var_158;
    struct indirect_placeholder_90_ret_type var_159;
    uint64_t var_160;
    uint64_t var_165;
    uint64_t var_166;
    uint64_t var_167;
    uint64_t var_141;
    uint64_t var_142;
    struct indirect_placeholder_91_ret_type var_143;
    uint64_t var_144;
    uint64_t var_145;
    uint64_t var_146;
    uint64_t var_147;
    uint64_t var_148;
    uint64_t var_149;
    uint64_t var_132;
    uint64_t var_133;
    struct indirect_placeholder_92_ret_type var_134;
    uint64_t var_135;
    uint64_t var_136;
    uint64_t var_137;
    uint64_t var_138;
    uint64_t var_139;
    uint64_t var_140;
    uint64_t var_123;
    uint64_t var_124;
    struct indirect_placeholder_93_ret_type var_125;
    uint64_t var_126;
    uint64_t var_127;
    uint64_t var_128;
    uint64_t var_129;
    uint64_t var_130;
    uint64_t var_131;
    uint64_t var_122;
    uint64_t rcx_5;
    uint64_t rsi1_1;
    uint64_t rdi2_1;
    uint64_t r94_5;
    uint64_t r103_0;
    uint64_t r94_4;
    uint64_t r85_4;
    uint64_t local_sp_6;
    uint32_t var_202;
    uint64_t var_119;
    uint64_t var_120;
    uint64_t var_121;
    struct indirect_placeholder_95_ret_type var_209;
    uint64_t var_210;
    uint64_t var_211;
    uint64_t var_212;
    uint64_t var_116;
    uint64_t var_117;
    uint64_t var_118;
    uint64_t rcx_6;
    uint64_t rdi2_2;
    uint64_t r103_1;
    uint64_t var_29;
    uint64_t var_31;
    uint32_t var_30;
    uint32_t var_34;
    struct indirect_placeholder_97_ret_type var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t spec_select;
    struct indirect_placeholder_98_ret_type var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_60;
    uint64_t _pre;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_48;
    uint32_t var_49;
    uint64_t var_50;
    struct helper_divq_EAX_wrapper_ret_type var_47;
    uint32_t var_51;
    uint32_t var_52;
    uint64_t var_53;
    uint64_t var_55;
    uint32_t var_56;
    uint64_t var_57;
    struct helper_divq_EAX_wrapper_ret_type var_54;
    uint32_t var_58;
    uint32_t var_59;
    uint64_t rcx_7;
    uint64_t state_0x9018_0;
    uint32_t state_0x9010_0;
    uint64_t state_0x82d8_0;
    uint32_t state_0x9080_0;
    uint32_t state_0x8248_0;
    uint64_t _pre374;
    uint64_t var_76;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_64;
    uint32_t var_65;
    uint64_t var_66;
    struct helper_divq_EAX_wrapper_ret_type var_63;
    uint32_t var_67;
    uint32_t var_68;
    uint64_t var_69;
    uint64_t var_71;
    uint32_t var_72;
    uint64_t var_73;
    struct helper_divq_EAX_wrapper_ret_type var_70;
    uint32_t var_74;
    uint32_t var_75;
    uint64_t rcx_8;
    uint64_t state_0x9018_1;
    uint32_t state_0x9010_1;
    uint64_t state_0x82d8_1;
    uint32_t state_0x9080_1;
    uint32_t state_0x8248_1;
    uint64_t _pre375;
    uint64_t var_87;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_80;
    uint32_t var_81;
    uint64_t var_82;
    struct helper_divq_EAX_wrapper_ret_type var_79;
    uint32_t var_83;
    uint32_t var_84;
    uint64_t var_85;
    struct helper_divq_EAX_wrapper_ret_type var_86;
    uint64_t rcx_9;
    uint32_t var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_115;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rcx();
    var_3 = init_cc_src2();
    var_4 = init_state_0x9018();
    var_5 = init_state_0x9010();
    var_6 = init_state_0x8408();
    var_7 = init_state_0x8328();
    var_8 = init_state_0x82d8();
    var_9 = init_state_0x9080();
    var_10 = init_state_0x8248();
    var_11 = var_0 + (-8L);
    *(uint64_t *)var_11 = var_1;
    var_12 = var_0 + (-120L);
    var_13 = (uint32_t *)(var_0 + (-108L));
    *var_13 = (uint32_t)rdi;
    var_14 = (uint64_t *)var_12;
    *var_14 = rsi;
    var_15 = (uint64_t *)(var_0 + (-16L));
    *var_15 = 0UL;
    var_16 = (uint64_t *)(var_0 + (-24L));
    *var_16 = 18446744073709551615UL;
    var_17 = (uint64_t *)(var_0 + (-32L));
    *var_17 = 0UL;
    var_18 = (uint64_t *)(var_0 + (-40L));
    *var_18 = 0UL;
    var_19 = *(uint32_t *)4318136UL;
    var_20 = (uint32_t *)(var_0 + (-44L));
    *var_20 = var_19;
    var_21 = (uint64_t *)(var_0 + (-72L));
    var_22 = (uint64_t *)(var_0 + (-80L));
    var_23 = var_0 + (-92L);
    var_24 = (uint32_t *)var_23;
    var_25 = (uint64_t *)(var_0 + (-88L));
    var_26 = (uint64_t *)(var_0 + (-56L));
    var_27 = (uint64_t *)(var_0 + (-64L));
    var_28 = var_19;
    storemerge3 = (unsigned char)'\x00';
    storemerge4 = (unsigned char)'\x00';
    r85_5 = r8;
    local_sp_7 = var_12;
    storemerge1 = (unsigned char)'\x01';
    rcx_4 = 9223372036854775807UL;
    rsi1_0 = 4298586UL;
    var_195 = 1U;
    rsi1_2 = rsi;
    rsi1_1 = 4298480UL;
    r94_5 = r9;
    rcx_6 = var_2;
    rdi2_2 = rdi;
    r103_1 = r10;
    var_60 = 0UL;
    state_0x9018_0 = var_4;
    state_0x9010_0 = var_5;
    state_0x82d8_0 = var_8;
    state_0x9080_0 = var_9;
    state_0x8248_0 = var_10;
    var_76 = 18446744073709551615UL;
    var_87 = 0UL;
    while (1U)
        {
            r94_3 = r94_5;
            r85_3 = r85_5;
            rcx_5 = rcx_6;
            r103_0 = r103_1;
            r94_4 = r94_5;
            r85_4 = r85_5;
            rcx_7 = rcx_6;
            if ((long)((uint64_t)var_28 << 32UL) >= (long)((uint64_t)*var_13 << 32UL)) {
                var_29 = *var_15;
                var_31 = var_29;
                if (var_29 != 0UL) {
                    *(uint64_t *)4318336UL = var_29;
                    *(uint64_t *)4318328UL = var_29;
                    break;
                }
                var_30 = *(uint32_t *)4318400UL;
                *(uint32_t *)4318400UL = ((var_30 & (-65281)) | ((uint32_t)((uint16_t)var_30 & (unsigned short)63232U) | 2048U));
                var_31 = *(uint64_t *)4318328UL;
                break;
            }
            var_115 = *(uint64_t *)(*var_14 + ((uint64_t)var_28 << 3UL));
            *var_21 = var_115;
            *(uint64_t *)(local_sp_7 + (-8L)) = 4211251UL;
            indirect_placeholder();
            *var_22 = var_115;
            if (var_115 == 0UL) {
                var_208 = *var_21;
                *(uint64_t *)(local_sp_7 + (-16L)) = 4211274UL;
                var_209 = indirect_placeholder_95(var_208);
                var_210 = var_209.field_0;
                var_211 = var_209.field_1;
                var_212 = var_209.field_2;
                *(uint64_t *)(local_sp_7 + (-24L)) = 4211302UL;
                indirect_placeholder_94(0UL, var_210, 4298453UL, 0UL, 0UL, var_211, var_212);
                *(uint64_t *)(local_sp_7 + (-32L)) = 4211312UL;
                indirect_placeholder_6(var_11, 1UL);
                abort();
            }
            *var_22 = (var_115 + 1UL);
            var_116 = *var_21;
            var_117 = local_sp_7 + (-16L);
            *(uint64_t *)var_117 = 4211334UL;
            var_118 = indirect_placeholder_3();
            rdi2_1 = var_116;
            local_sp_6 = var_117;
            if ((uint64_t)(unsigned char)var_118 == 0UL) {
                *(uint64_t *)4318304UL = *var_22;
                rsi1_1 = 4298477UL;
            } else {
                var_119 = *var_21;
                var_120 = local_sp_7 + (-24L);
                *(uint64_t *)var_120 = 4211371UL;
                var_121 = indirect_placeholder_3();
                rcx_5 = 4298488UL;
                rdi2_1 = var_119;
                local_sp_6 = var_120;
                if ((uint64_t)(unsigned char)var_121 == 0UL) {
                    *(uint64_t *)4318312UL = *var_22;
                } else {
                    *(uint64_t *)(local_sp_7 + (-32L)) = 4211408UL;
                    var_122 = indirect_placeholder_3();
                    if ((uint64_t)(unsigned char)var_122 == 0UL) {
                        var_123 = *var_22;
                        var_124 = local_sp_7 + (-40L);
                        *(uint64_t *)var_124 = 4211439UL;
                        var_125 = indirect_placeholder_93(4298488UL, 0UL, 4293024UL, var_123);
                        var_126 = var_125.field_1;
                        var_127 = var_125.field_2;
                        var_128 = var_125.field_3;
                        var_129 = var_125.field_4;
                        var_130 = var_125.field_5;
                        *(uint32_t *)4318400UL = (*(uint32_t *)4318400UL | (uint32_t)var_125.field_0);
                        rsi1_1 = var_126;
                        rdi2_1 = var_127;
                        r103_0 = var_128;
                        r94_4 = var_129;
                        r85_4 = var_130;
                        local_sp_6 = var_124;
                    } else {
                        *(uint64_t *)(local_sp_7 + (-40L)) = 4211475UL;
                        var_131 = indirect_placeholder_3();
                        rcx_5 = 4298513UL;
                        if ((uint64_t)(unsigned char)var_131 == 0UL) {
                            var_132 = *var_22;
                            var_133 = local_sp_7 + (-48L);
                            *(uint64_t *)var_133 = 4211506UL;
                            var_134 = indirect_placeholder_92(4298513UL, 0UL, 4293312UL, var_132);
                            var_135 = var_134.field_1;
                            var_136 = var_134.field_2;
                            var_137 = var_134.field_3;
                            var_138 = var_134.field_4;
                            var_139 = var_134.field_5;
                            *(uint32_t *)4318404UL = (*(uint32_t *)4318404UL | (uint32_t)var_134.field_0);
                            rsi1_1 = var_135;
                            rdi2_1 = var_136;
                            r103_0 = var_137;
                            r94_4 = var_138;
                            r85_4 = var_139;
                            local_sp_6 = var_133;
                        } else {
                            *(uint64_t *)(local_sp_7 + (-48L)) = 4211542UL;
                            var_140 = indirect_placeholder_3();
                            rcx_5 = 4298538UL;
                            if ((uint64_t)(unsigned char)var_140 == 0UL) {
                                var_141 = *var_22;
                                var_142 = local_sp_7 + (-56L);
                                *(uint64_t *)var_142 = 4211573UL;
                                var_143 = indirect_placeholder_91(4298538UL, 0UL, 4293312UL, var_141);
                                var_144 = var_143.field_1;
                                var_145 = var_143.field_2;
                                var_146 = var_143.field_3;
                                var_147 = var_143.field_4;
                                var_148 = var_143.field_5;
                                *(uint32_t *)4318408UL = (*(uint32_t *)4318408UL | (uint32_t)var_143.field_0);
                                rsi1_1 = var_144;
                                rdi2_1 = var_145;
                                r103_0 = var_146;
                                r94_4 = var_147;
                                r85_4 = var_148;
                                local_sp_6 = var_142;
                            } else {
                                *(uint64_t *)(local_sp_7 + (-56L)) = 4211609UL;
                                var_149 = indirect_placeholder_3();
                                rcx_5 = 4298565UL;
                                if ((uint64_t)(unsigned char)var_149 == 0UL) {
                                    var_150 = *var_22;
                                    var_151 = local_sp_7 + (-64L);
                                    *(uint64_t *)var_151 = 4211640UL;
                                    var_152 = indirect_placeholder_89(4298565UL, 1UL, 4293632UL, var_150);
                                    var_153 = var_152.field_1;
                                    var_154 = var_152.field_2;
                                    var_155 = var_152.field_3;
                                    var_156 = var_152.field_4;
                                    var_157 = var_152.field_5;
                                    *(uint32_t *)4317960UL = (uint32_t)var_152.field_0;
                                    rsi1_1 = var_153;
                                    rdi2_1 = var_154;
                                    r103_0 = var_155;
                                    r94_4 = var_156;
                                    r85_4 = var_157;
                                    local_sp_6 = var_151;
                                } else {
                                    *var_24 = 0U;
                                    var_158 = *var_22;
                                    *(uint64_t *)(local_sp_7 + (-64L)) = 4211677UL;
                                    var_159 = indirect_placeholder_90(var_23, var_158);
                                    var_160 = var_159.field_0;
                                    var_161 = var_159.field_1;
                                    var_162 = var_159.field_4;
                                    var_163 = var_159.field_5;
                                    var_164 = var_159.field_6;
                                    *var_25 = var_160;
                                    *var_26 = 0UL;
                                    *var_27 = 18446744073709551615UL;
                                    var_165 = *var_21;
                                    var_166 = local_sp_7 + (-72L);
                                    *(uint64_t *)var_166 = 4211714UL;
                                    var_167 = indirect_placeholder_3();
                                    rdi2_0 = var_165;
                                    local_sp_5 = var_166;
                                    r103_0 = var_162;
                                    r94_4 = var_163;
                                    r85_4 = var_164;
                                    if ((uint64_t)(unsigned char)var_167 == 0UL) {
                                        *var_26 = 1UL;
                                        var_168 = 18446744073709551612UL - (-4L);
                                        *var_27 = ((var_168 < 9223372036854775807UL) ? var_168 : 9223372036854775807UL);
                                        *(uint64_t *)4318328UL = *var_25;
                                    } else {
                                        var_169 = *var_21;
                                        var_170 = local_sp_7 + (-80L);
                                        *(uint64_t *)var_170 = 4211807UL;
                                        var_171 = indirect_placeholder_3();
                                        rcx_4 = var_161;
                                        rsi1_0 = 4298590UL;
                                        rdi2_0 = var_169;
                                        local_sp_5 = var_170;
                                        if ((uint64_t)(unsigned char)var_171 == 0UL) {
                                            *var_26 = 1UL;
                                            var_172 = 0UL - *(uint64_t *)4318320UL;
                                            *var_27 = ((var_172 < 9223372036854775807UL) ? var_172 : 9223372036854775807UL);
                                            *(uint64_t *)4318336UL = *var_25;
                                            rcx_4 = 9223372036854775807UL;
                                        } else {
                                            var_173 = *var_21;
                                            var_174 = local_sp_7 + (-88L);
                                            *(uint64_t *)var_174 = 4211893UL;
                                            var_175 = indirect_placeholder_3();
                                            rsi1_0 = 4298594UL;
                                            rdi2_0 = var_173;
                                            local_sp_5 = var_174;
                                            if ((uint64_t)(unsigned char)var_175 == 0UL) {
                                                *var_26 = 1UL;
                                                var_176 = 18446744073709551612UL - (-4L);
                                                *var_27 = ((var_176 < 9223372036854775807UL) ? var_176 : 9223372036854775807UL);
                                                *var_15 = *var_25;
                                                rcx_4 = 9223372036854775807UL;
                                            } else {
                                                var_177 = *var_21;
                                                var_178 = local_sp_7 + (-96L);
                                                *(uint64_t *)var_178 = 4211983UL;
                                                var_179 = indirect_placeholder_3();
                                                rsi1_0 = 4298597UL;
                                                rdi2_0 = var_177;
                                                local_sp_5 = var_178;
                                                if ((uint64_t)(unsigned char)var_179 == 0UL) {
                                                    *var_26 = 1UL;
                                                    *var_27 = 18446744073709551615UL;
                                                    *(uint64_t *)4318344UL = *var_25;
                                                } else {
                                                    var_180 = *var_21;
                                                    var_181 = local_sp_7 + (-104L);
                                                    *(uint64_t *)var_181 = 4212036UL;
                                                    var_182 = indirect_placeholder_3();
                                                    rsi1_0 = 4298601UL;
                                                    rdi2_0 = var_180;
                                                    local_sp_5 = var_181;
                                                    if ((uint64_t)(unsigned char)var_182 == 0UL) {
                                                        *var_17 = *var_25;
                                                    } else {
                                                        var_183 = *var_21;
                                                        var_184 = local_sp_7 + (-112L);
                                                        *(uint64_t *)var_184 = 4212067UL;
                                                        var_185 = indirect_placeholder_3();
                                                        rsi1_0 = 4298606UL;
                                                        rdi2_0 = var_183;
                                                        local_sp_5 = var_184;
                                                        if ((uint64_t)(unsigned char)var_185 == 0UL) {
                                                            *var_18 = *var_25;
                                                        } else {
                                                            var_186 = *var_21;
                                                            var_187 = local_sp_7 + (-120L);
                                                            *(uint64_t *)var_187 = 4212098UL;
                                                            var_188 = indirect_placeholder_3();
                                                            rsi1_0 = 4298611UL;
                                                            rdi2_0 = var_186;
                                                            local_sp_5 = var_187;
                                                            if ((uint64_t)(unsigned char)var_188 != 0UL) {
                                                                var_203 = *var_21;
                                                                *(uint64_t *)(local_sp_7 + (-128L)) = 4212124UL;
                                                                var_204 = indirect_placeholder_87(var_203);
                                                                var_205 = var_204.field_0;
                                                                var_206 = var_204.field_1;
                                                                var_207 = var_204.field_2;
                                                                *(uint64_t *)(local_sp_7 + (-136L)) = 4212152UL;
                                                                indirect_placeholder_86(0UL, var_205, 4298453UL, 0UL, 0UL, var_206, var_207);
                                                                *(uint64_t *)(local_sp_7 + (-144L)) = 4212162UL;
                                                                indirect_placeholder_6(var_11, 1UL);
                                                                abort();
                                                            }
                                                            *var_16 = *var_25;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    var_189 = *var_25;
                                    var_190 = *var_26;
                                    var_191 = helper_cc_compute_c_wrapper(var_189 - var_190, var_190, var_3, 17U);
                                    rcx_5 = rcx_4;
                                    rsi1_1 = rsi1_0;
                                    rdi2_1 = rdi2_0;
                                    local_sp_6 = local_sp_5;
                                    if (var_191 == 0UL) {
                                        *var_24 = 4U;
                                        var_195 = 4U;
                                    } else {
                                        var_192 = *var_27;
                                        var_193 = *var_25;
                                        var_194 = helper_cc_compute_c_wrapper(var_192 - var_193, var_193, var_3, 17U);
                                        if (var_194 == 0UL) {
                                            var_195 = *var_24;
                                        } else {
                                            *var_24 = 1U;
                                        }
                                    }
                                    if (var_195 == 0U) {
                                        var_196 = *var_22;
                                        *(uint64_t *)(local_sp_5 + (-8L)) = 4212217UL;
                                        var_197 = indirect_placeholder_88(var_196);
                                        var_198 = var_197.field_0;
                                        var_199 = var_197.field_1;
                                        storemerge5 = (*var_24 == 1U) ? 75UL : 0UL;
                                        var_200 = local_sp_5 + (-16L);
                                        *(uint64_t *)var_200 = 4212265UL;
                                        var_201 = indirect_placeholder_85(0UL, 4298617UL, 4298358UL, storemerge5, 1UL, var_199, var_198);
                                        rcx_5 = var_201.field_0;
                                        rsi1_1 = var_201.field_1;
                                        rdi2_1 = var_201.field_2;
                                        r103_0 = var_201.field_3;
                                        r94_4 = var_201.field_4;
                                        r85_4 = var_201.field_5;
                                        local_sp_6 = var_200;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            var_202 = *var_20 + 1U;
            *var_20 = var_202;
            var_28 = var_202;
            r85_5 = r85_4;
            local_sp_7 = local_sp_6;
            rsi1_2 = rsi1_1;
            r94_5 = r94_4;
            rcx_6 = rcx_5;
            rdi2_2 = rdi2_1;
            r103_1 = r103_0;
            continue;
        }
    if (var_31 == 0UL) {
        *(uint64_t *)4318328UL = 512UL;
    }
    if (*(uint64_t *)4318336UL == 0UL) {
        *(uint64_t *)4318336UL = 512UL;
    }
    if (*(uint64_t *)4318344UL == 0UL) {
        *(uint32_t *)4318400UL = (*(uint32_t *)4318400UL & (-25));
    }
    var_32 = *(uint32_t *)4318404UL;
    var_34 = var_32;
    if ((var_32 & 1052672U) == 0U) {
        var_33 = var_32 | 1052672U;
        *(uint32_t *)4318404UL = var_33;
        var_34 = var_33;
    }
    var_35 = *(uint32_t *)4318408UL;
    if ((var_35 & 1U) == 0U) {
        *(uint64_t *)(local_sp_7 + (-8L)) = 4212458UL;
        var_36 = indirect_placeholder_96(4298632UL);
        var_37 = var_36.field_0;
        var_38 = var_36.field_1;
        *(uint64_t *)(local_sp_7 + (-16L)) = 4212491UL;
        indirect_placeholder_84(0UL, 4298538UL, 4298358UL, 0UL, 0UL, var_38, var_37);
        *(uint64_t *)(local_sp_7 + (-24L)) = 4212501UL;
        indirect_placeholder_6(var_11, 1UL);
        abort();
    }
    if ((var_34 & 16U) == 0U) {
        *(uint64_t *)(local_sp_7 + (-8L)) = 4212524UL;
        var_39 = indirect_placeholder_97(4298642UL);
        var_40 = var_39.field_0;
        var_41 = var_39.field_1;
        *(uint64_t *)(local_sp_7 + (-16L)) = 4212557UL;
        indirect_placeholder_83(0UL, 4298513UL, 4298358UL, 0UL, 0UL, var_41, var_40);
        *(uint64_t *)(local_sp_7 + (-24L)) = 4212567UL;
        indirect_placeholder_6(var_11, 1UL);
        abort();
    }
    if ((var_35 & 12U) == 0U) {
        spec_select = ((var_35 & 4U) == 0U) ? 4298665UL : 4298653UL;
        *(uint64_t *)(local_sp_7 + (-8L)) = 4212613UL;
        var_42 = indirect_placeholder_98(spec_select);
        var_43 = var_42.field_0;
        var_44 = var_42.field_1;
        *(uint64_t *)(local_sp_7 + (-16L)) = 4212646UL;
        indirect_placeholder_82(0UL, 4298538UL, 4298358UL, 0UL, 0UL, var_44, var_43);
        *(uint64_t *)(local_sp_7 + (-24L)) = 4212656UL;
        indirect_placeholder_6(var_11, 1UL);
        abort();
    }
    if ((var_34 & 8U) == 0U) {
        _pre = *var_17;
        var_60 = _pre;
        if (var_60 == 0UL) {
            *(uint64_t *)4318352UL = var_60;
        }
    } else {
        var_45 = *var_17;
        if (var_45 == 0UL) {
            var_46 = *(uint64_t *)4318328UL;
            var_47 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), var_46, 4212692UL, var_45, var_11, var_46, 0UL, rsi1_2, rdi2_2, r103_1, r94_5, r85_5, rbx, var_4, var_5, var_6, var_7, var_8, var_9, var_10);
            var_48 = var_47.field_5;
            var_49 = var_47.field_6;
            var_50 = var_47.field_7;
            var_51 = var_47.field_8;
            var_52 = var_47.field_9;
            *(uint64_t *)4318352UL = var_47.field_1;
            var_53 = *(uint64_t *)4318328UL;
            var_54 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), var_53, 4212718UL, *var_17, var_11, var_53, 0UL, rsi1_2, rdi2_2, r103_1, r94_5, r85_5, rbx, var_48, var_49, var_6, var_7, var_50, var_51, var_52);
            var_55 = var_54.field_5;
            var_56 = var_54.field_6;
            var_57 = var_54.field_7;
            var_58 = var_54.field_8;
            var_59 = var_54.field_9;
            *(uint64_t *)4318360UL = var_54.field_4;
            rcx_7 = var_53;
            state_0x9018_0 = var_55;
            state_0x9010_0 = var_56;
            state_0x82d8_0 = var_57;
            state_0x9080_0 = var_58;
            state_0x8248_0 = var_59;
        } else {
            if (var_60 == 0UL) {
                *(uint64_t *)4318352UL = var_60;
            }
        }
    }
    rcx_8 = rcx_7;
    state_0x9018_1 = state_0x9018_0;
    state_0x9010_1 = state_0x9010_0;
    state_0x82d8_1 = state_0x82d8_0;
    state_0x9080_1 = state_0x9080_0;
    state_0x8248_1 = state_0x8248_0;
    if ((*(uint32_t *)4318404UL & 4U) == 0U) {
        _pre374 = *var_16;
        var_76 = _pre374;
        if (var_76 == 18446744073709551615UL) {
            *(uint64_t *)4317952UL = var_76;
        }
    } else {
        var_61 = *var_16;
        if (var_61 == 18446744073709551615UL) {
            var_62 = *(uint64_t *)4318328UL;
            var_63 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), var_62, 4212787UL, var_61, var_11, var_62, 0UL, rsi1_2, rdi2_2, r103_1, r94_5, r85_5, rbx, state_0x9018_0, state_0x9010_0, var_6, var_7, state_0x82d8_0, state_0x9080_0, state_0x8248_0);
            var_64 = var_63.field_5;
            var_65 = var_63.field_6;
            var_66 = var_63.field_7;
            var_67 = var_63.field_8;
            var_68 = var_63.field_9;
            *(uint64_t *)4317952UL = var_63.field_1;
            var_69 = *(uint64_t *)4318328UL;
            var_70 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), var_69, 4212813UL, *var_16, var_11, var_69, 0UL, rsi1_2, rdi2_2, r103_1, r94_5, r85_5, rbx, var_64, var_65, var_6, var_7, var_66, var_67, var_68);
            var_71 = var_70.field_5;
            var_72 = var_70.field_6;
            var_73 = var_70.field_7;
            var_74 = var_70.field_8;
            var_75 = var_70.field_9;
            *(uint64_t *)4318392UL = var_70.field_4;
            rcx_8 = var_69;
            state_0x9018_1 = var_71;
            state_0x9010_1 = var_72;
            state_0x82d8_1 = var_73;
            state_0x9080_1 = var_74;
            state_0x8248_1 = var_75;
        } else {
            if (var_76 == 18446744073709551615UL) {
                *(uint64_t *)4317952UL = var_76;
            }
        }
    }
    rcx_9 = rcx_8;
    if ((*(uint32_t *)4318408UL & 16U) == 0U) {
        _pre375 = *var_18;
        var_87 = _pre375;
        if (var_87 == 0UL) {
            *(uint64_t *)4318368UL = var_87;
        }
    } else {
        var_77 = *var_18;
        if (var_77 == 0UL) {
            var_78 = *(uint64_t *)4318336UL;
            var_79 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), var_78, 4212882UL, var_77, var_11, var_78, 0UL, rsi1_2, rdi2_2, r103_1, r94_5, r85_5, rbx, state_0x9018_1, state_0x9010_1, var_6, var_7, state_0x82d8_1, state_0x9080_1, state_0x8248_1);
            var_80 = var_79.field_5;
            var_81 = var_79.field_6;
            var_82 = var_79.field_7;
            var_83 = var_79.field_8;
            var_84 = var_79.field_9;
            *(uint64_t *)4318368UL = var_79.field_1;
            var_85 = *(uint64_t *)4318336UL;
            var_86 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), var_85, 4212908UL, *var_18, var_11, var_85, 0UL, rsi1_2, rdi2_2, r103_1, r94_5, r85_5, rbx, var_80, var_81, var_6, var_7, var_82, var_83, var_84);
            *(uint64_t *)4318376UL = var_86.field_4;
            rcx_9 = var_85;
        } else {
            if (var_87 == 0UL) {
                *(uint64_t *)4318368UL = var_87;
            }
        }
    }
    rcx_3 = rcx_9;
    if ((uint32_t)((uint16_t)*(uint32_t *)4318400UL & (unsigned short)2048U) != 0U) {
        storemerge1 = (unsigned char)'\x00';
        *(unsigned char *)4318497UL = storemerge1;
        *(uint64_t *)4318704UL = (((*(uint32_t *)4318404UL & 1U) == 0U) ? 4209116UL : 4209430UL);
        *(uint32_t *)4318404UL = (*(uint32_t *)4318404UL & (-2));
        var_89 = (uint64_t)(*(uint32_t *)4318400UL & 7U);
        var_90 = local_sp_7 + (-8L);
        *(uint64_t *)var_90 = 4213113UL;
        var_91 = indirect_placeholder_12(var_89);
        local_sp_4 = var_90;
        if ((uint64_t)(unsigned char)var_91 == 0UL) {
            var_92 = local_sp_7 + (-16L);
            *(uint64_t *)var_92 = 4213142UL;
            var_93 = indirect_placeholder_81(0UL, rcx_9, 4298680UL, 0UL, 1UL, r94_5, r85_5);
            rcx_3 = var_93.field_0;
            r94_3 = var_93.field_4;
            r85_3 = var_93.field_5;
            local_sp_4 = var_92;
        }
        var_94 = (uint64_t)(*(uint32_t *)4318400UL & 24U);
        var_95 = local_sp_4 + (-8L);
        *(uint64_t *)var_95 = 4213158UL;
        var_96 = indirect_placeholder_12(var_94);
        rcx_2 = rcx_3;
        r94_2 = r94_3;
        r85_2 = r85_3;
        local_sp_3 = var_95;
        if ((uint64_t)(unsigned char)var_96 == 0UL) {
            var_97 = local_sp_4 + (-16L);
            *(uint64_t *)var_97 = 4213187UL;
            var_98 = indirect_placeholder_80(0UL, rcx_3, 4298728UL, 0UL, 1UL, r94_3, r85_3);
            rcx_2 = var_98.field_0;
            r94_2 = var_98.field_4;
            r85_2 = var_98.field_5;
            local_sp_3 = var_97;
        }
        var_99 = (uint64_t)(*(uint32_t *)4318400UL & 96U);
        var_100 = local_sp_3 + (-8L);
        *(uint64_t *)var_100 = 4213203UL;
        var_101 = indirect_placeholder_12(var_99);
        rcx_1 = rcx_2;
        r94_1 = r94_2;
        r85_1 = r85_2;
        local_sp_2 = var_100;
        if ((uint64_t)(unsigned char)var_101 == 0UL) {
            var_102 = local_sp_3 + (-16L);
            *(uint64_t *)var_102 = 4213232UL;
            var_103 = indirect_placeholder_79(0UL, rcx_2, 4298768UL, 0UL, 1UL, r94_2, r85_2);
            rcx_1 = var_103.field_0;
            r94_1 = var_103.field_4;
            r85_1 = var_103.field_5;
            local_sp_2 = var_102;
        }
        var_104 = (uint64_t)(uint32_t)((uint16_t)*(uint32_t *)4318400UL & (unsigned short)12288U);
        var_105 = local_sp_2 + (-8L);
        *(uint64_t *)var_105 = 4213250UL;
        var_106 = indirect_placeholder_12(var_104);
        rcx_0 = rcx_1;
        r94_0 = r94_1;
        r85_0 = r85_1;
        local_sp_1 = var_105;
        if ((uint64_t)(unsigned char)var_106 == 0UL) {
            var_107 = local_sp_2 + (-16L);
            *(uint64_t *)var_107 = 4213279UL;
            var_108 = indirect_placeholder_78(0UL, rcx_1, 4298800UL, 0UL, 1UL, r94_1, r85_1);
            rcx_0 = var_108.field_0;
            r94_0 = var_108.field_4;
            r85_0 = var_108.field_5;
            local_sp_1 = var_107;
        }
        var_109 = (uint64_t)(uint32_t)((uint16_t)*(uint32_t *)4318404UL & (unsigned short)16386U);
        var_110 = local_sp_1 + (-8L);
        *(uint64_t *)var_110 = 4213297UL;
        var_111 = indirect_placeholder_12(var_109);
        local_sp_0 = var_110;
        if ((uint64_t)(unsigned char)var_111 == 0UL) {
            *(uint64_t *)(local_sp_0 + (-8L)) = 4213348UL;
            indirect_placeholder_77(0UL, rcx_0, 4298832UL, 0UL, 1UL, r94_0, r85_0);
        } else {
            var_112 = (uint64_t)(uint32_t)((uint16_t)*(uint32_t *)4318408UL & (unsigned short)16386U);
            var_113 = local_sp_1 + (-16L);
            *(uint64_t *)var_113 = 4213319UL;
            var_114 = indirect_placeholder_12(var_112);
            local_sp_0 = var_113;
            if ((uint64_t)(unsigned char)var_114 == 0UL) {
                *(uint64_t *)(local_sp_0 + (-8L)) = 4213348UL;
                indirect_placeholder_77(0UL, rcx_0, 4298832UL, 0UL, 1UL, r94_0, r85_0);
            }
        }
        if ((*(uint32_t *)4318404UL & 2U) != 0U) {
            *(unsigned char *)4318696UL = (unsigned char)'\x01';
            if (*(uint64_t *)4317952UL == 0UL) {
            } else {
                storemerge3 = (unsigned char)'\x01';
                if (*(uint64_t *)4318392UL == 0UL) {
                }
            }
            *(unsigned char *)4318698UL = storemerge3;
            *(uint32_t *)4318404UL = (*(uint32_t *)4318404UL & (-3));
        }
        if ((*(uint32_t *)4318408UL & 2U) != 0U) {
            *(unsigned char *)4318697UL = (unsigned char)'\x01';
            if (*(uint64_t *)4317952UL == 0UL) {
            } else {
                storemerge4 = (unsigned char)'\x01';
                if (*(uint64_t *)4318392UL == 0UL) {
                }
            }
            *(unsigned char *)4318699UL = storemerge4;
            *(uint32_t *)4318408UL = (*(uint32_t *)4318408UL & (-3));
        }
        return;
    }
    var_88 = *(uint32_t *)4318404UL;
    if ((var_88 & 1U) == 0U) {
        if (*(uint64_t *)4318352UL != 0UL) {
            if ((uint32_t)(((uint16_t)*(uint32_t *)4318408UL | (uint16_t)var_88) & (unsigned short)16384U) == 0U) {
                storemerge1 = (unsigned char)'\x00';
            }
        }
    } else {
        storemerge1 = (unsigned char)'\x00';
    }
}
