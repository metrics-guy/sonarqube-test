typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_rax(void);
void bb_install_signal_handlers(void) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    bool var_3;
    unsigned char *var_4;
    uint64_t var_5;
    uint64_t local_sp_2;
    uint64_t var_22;
    uint64_t local_sp_1;
    uint64_t _pre_phi44;
    uint64_t local_sp_0;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t *var_9;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-176L)) = 4207745UL;
    indirect_placeholder();
    var_3 = (var_1 == 0UL);
    var_4 = (unsigned char *)(var_0 + (-9L));
    *var_4 = var_3;
    var_5 = var_0 + (-184L);
    *(uint64_t *)var_5 = 4207764UL;
    indirect_placeholder();
    local_sp_2 = var_5;
    if (*var_4 == '\x00') {
        var_6 = var_0 + (-192L);
        *(uint64_t *)var_6 = 4207785UL;
        indirect_placeholder();
        local_sp_2 = var_6;
    }
    var_7 = local_sp_2 + (-8L);
    *(uint64_t *)var_7 = 4207810UL;
    indirect_placeholder();
    var_8 = var_0 + (-168L);
    var_9 = (uint64_t *)var_8;
    local_sp_1 = var_7;
    if (*var_9 == 1UL) {
        var_10 = local_sp_2 + (-16L);
        *(uint64_t *)var_10 = 4207838UL;
        indirect_placeholder();
        local_sp_1 = var_10;
    }
    var_11 = *(uint64_t *)4318568UL;
    *(uint64_t *)(var_0 + (-160L)) = *(uint64_t *)4318560UL;
    *(uint64_t *)(var_0 + (-152L)) = var_11;
    var_12 = *(uint64_t *)4318584UL;
    *(uint64_t *)(var_0 + (-144L)) = *(uint64_t *)4318576UL;
    *(uint64_t *)(var_0 + (-136L)) = var_12;
    var_13 = *(uint64_t *)4318600UL;
    *(uint64_t *)(var_0 + (-128L)) = *(uint64_t *)4318592UL;
    *(uint64_t *)(var_0 + (-120L)) = var_13;
    var_14 = *(uint64_t *)4318616UL;
    *(uint64_t *)(var_0 + (-112L)) = *(uint64_t *)4318608UL;
    *(uint64_t *)(var_0 + (-104L)) = var_14;
    var_15 = *(uint64_t *)4318632UL;
    *(uint64_t *)(var_0 + (-96L)) = *(uint64_t *)4318624UL;
    *(uint64_t *)(var_0 + (-88L)) = var_15;
    var_16 = *(uint64_t *)4318648UL;
    *(uint64_t *)(var_0 + (-80L)) = *(uint64_t *)4318640UL;
    *(uint64_t *)(var_0 + (-72L)) = var_16;
    var_17 = *(uint64_t *)4318664UL;
    *(uint64_t *)(var_0 + (-64L)) = *(uint64_t *)4318656UL;
    *(uint64_t *)(var_0 + (-56L)) = var_17;
    var_18 = *(uint64_t *)4318672UL;
    var_19 = *(uint64_t *)4318680UL;
    *(uint64_t *)(var_0 + (-48L)) = var_18;
    *(uint64_t *)(var_0 + (-40L)) = var_19;
    var_20 = local_sp_1 + (-8L);
    *(uint64_t *)var_20 = 4208038UL;
    indirect_placeholder();
    var_21 = (uint64_t)(uint32_t)var_18;
    _pre_phi44 = var_21;
    local_sp_0 = var_20;
    if (var_21 == 0UL) {
        *var_9 = 4207699UL;
        *(uint32_t *)(var_0 + (-32L)) = 0U;
        var_22 = local_sp_1 + (-16L);
        *(uint64_t *)var_22 = 4208085UL;
        indirect_placeholder();
        _pre_phi44 = (uint64_t)(uint32_t)var_8;
        local_sp_0 = var_22;
    }
    *(uint64_t *)(local_sp_0 + (-8L)) = 4208100UL;
    indirect_placeholder();
    if (_pre_phi44 == 0UL) {
        return;
    }
    *var_9 = 4207680UL;
    *(uint32_t *)(var_0 + (-32L)) = 3221225472U;
    *(uint64_t *)(local_sp_0 + (-16L)) = 4208147UL;
    indirect_placeholder();
    return;
}
