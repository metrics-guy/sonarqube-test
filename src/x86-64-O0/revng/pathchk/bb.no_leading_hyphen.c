typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_no_leading_hyphen_ret_type;
struct indirect_placeholder_1_ret_type;
struct bb_no_leading_hyphen_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_1_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_2(void);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern void indirect_placeholder(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern uint64_t init_rcx(void);
extern struct indirect_placeholder_1_ret_type indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
struct bb_no_leading_hyphen_ret_type bb_no_leading_hyphen(uint64_t rdi) {
    struct bb_no_leading_hyphen_ret_type mrv2;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    uint64_t var_8;
    uint64_t var_10;
    uint64_t rcx_0;
    uint64_t r9_0;
    uint64_t r8_0;
    uint64_t storemerge;
    struct bb_no_leading_hyphen_ret_type mrv;
    struct bb_no_leading_hyphen_ret_type mrv1;
    struct bb_no_leading_hyphen_ret_type mrv3;
    uint64_t var_11;
    struct indirect_placeholder_1_ret_type var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t local_sp_0;
    uint64_t var_9;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rcx();
    var_3 = init_r9();
    var_4 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_5 = var_0 + (-40L);
    var_6 = (uint64_t *)(var_0 + (-32L));
    *var_6 = rdi;
    var_7 = (uint64_t *)(var_0 + (-16L));
    *var_7 = rdi;
    var_8 = rdi;
    rcx_0 = var_2;
    r9_0 = var_3;
    r8_0 = var_4;
    storemerge = 1UL;
    local_sp_0 = var_5;
    while (1U)
        {
            var_9 = local_sp_0 + (-8L);
            *(uint64_t *)var_9 = 4205186UL;
            indirect_placeholder_2();
            *var_7 = var_8;
            local_sp_0 = var_9;
            if (var_8 != 0UL) {
                loop_state_var = 1U;
                break;
            }
            var_10 = *var_6;
            storemerge = 0UL;
            if (var_8 != var_10) {
                loop_state_var = 0U;
                break;
            }
            if (*(unsigned char *)(var_8 + (-1L)) != '/') {
                loop_state_var = 0U;
                break;
            }
            var_11 = var_8 + 1UL;
            *var_7 = var_11;
            var_8 = var_11;
            continue;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(uint64_t *)(local_sp_0 + (-16L)) = 4205129UL;
            var_12 = indirect_placeholder_1(4UL, var_10);
            var_13 = var_12.field_0;
            var_14 = var_12.field_1;
            var_15 = var_12.field_2;
            *(uint64_t *)(local_sp_0 + (-24L)) = 4205157UL;
            indirect_placeholder(0UL, var_13, 4273152UL, 0UL, 0UL, var_14, var_15);
            rcx_0 = var_13;
            r9_0 = var_14;
            r8_0 = var_15;
        }
        break;
      case 1U:
        {
            mrv.field_0 = storemerge;
            mrv1 = mrv;
            mrv1.field_1 = rcx_0;
            mrv2 = mrv1;
            mrv2.field_2 = r9_0;
            mrv3 = mrv2;
            mrv3.field_3 = r8_0;
            return mrv3;
        }
        break;
    }
}
