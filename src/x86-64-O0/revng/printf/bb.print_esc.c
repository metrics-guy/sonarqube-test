typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_print_esc_ret_type;
struct indirect_placeholder_7_ret_type;
struct indirect_placeholder_10_ret_type;
struct indirect_placeholder_11_ret_type;
struct bb_print_esc_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_7_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_10_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_11_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern void indirect_placeholder_1(void);
extern uint64_t indirect_placeholder_9(uint64_t param_0);
extern void indirect_placeholder_8(uint64_t param_0);
extern void indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_7_ret_type indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_10_ret_type indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct indirect_placeholder_11_ret_type indirect_placeholder_11(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
struct bb_print_esc_ret_type bb_print_esc(uint64_t rcx, uint64_t rdi, uint64_t rsi, uint64_t r9, uint64_t r8) {
    struct indirect_placeholder_10_ret_type var_50;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    unsigned char *var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t *var_8;
    uint32_t *var_9;
    unsigned char **var_10;
    unsigned char var_11;
    uint64_t r97_3;
    uint64_t var_36;
    uint64_t r88_3;
    struct indirect_placeholder_7_ret_type var_37;
    uint64_t local_sp_0;
    uint64_t r97_0;
    uint64_t r88_0;
    uint32_t var_38;
    unsigned char var_39;
    uint32_t rax_0_in;
    uint64_t local_sp_5;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t local_sp_3;
    uint64_t local_sp_6;
    uint64_t rcx4_0;
    uint64_t r88_2;
    uint64_t r88_4;
    uint64_t r97_1;
    uint64_t r88_1;
    struct bb_print_esc_ret_type mrv;
    struct bb_print_esc_ret_type mrv1;
    struct bb_print_esc_ret_type mrv2;
    struct bb_print_esc_ret_type mrv3;
    uint32_t var_46;
    unsigned char var_47;
    uint32_t rax_1_in;
    uint64_t local_sp_1;
    uint32_t *var_40;
    uint32_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t local_sp_2;
    uint32_t var_48;
    uint64_t var_49;
    uint64_t r97_2;
    uint32_t *var_12;
    uint64_t storemerge31;
    unsigned char var_13;
    unsigned char var_18;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t local_sp_4;
    unsigned char *var_19;
    uint32_t *var_20;
    uint32_t spec_select;
    uint32_t *var_21;
    uint32_t var_22;
    uint32_t var_24;
    uint64_t var_23;
    uint64_t r97_4;
    uint32_t _pre167;
    unsigned char var_25;
    uint64_t spec_select165;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    struct indirect_placeholder_11_ret_type var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_cc_src2();
    var_2 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    var_3 = var_0 + (-56L);
    var_4 = (uint64_t *)(var_0 + (-48L));
    *var_4 = rdi;
    var_5 = (unsigned char *)(var_0 + (-52L));
    *var_5 = (unsigned char)rsi;
    var_6 = *var_4 + 1UL;
    var_7 = var_0 + (-16L);
    var_8 = (uint64_t *)var_7;
    *var_8 = var_6;
    var_9 = (uint32_t *)(var_0 + (-20L));
    *var_9 = 0U;
    var_10 = (unsigned char **)var_7;
    var_11 = **var_10;
    r97_3 = r9;
    r88_3 = r8;
    rcx4_0 = rcx;
    r88_2 = r8;
    r97_1 = r9;
    r88_1 = r8;
    local_sp_1 = var_3;
    r97_2 = r9;
    storemerge31 = 0UL;
    var_18 = (unsigned char)'\x00';
    local_sp_4 = var_3;
    if (var_11 == 'x') {
        var_40 = (uint32_t *)(var_0 + (-24L));
        *var_40 = 0U;
        *var_8 = (*var_8 + 1UL);
        while (1U)
            {
                var_41 = *var_40;
                var_48 = var_41;
                local_sp_2 = local_sp_1;
                if ((int)var_41 <= (int)1U) {
                    break;
                }
                var_42 = (uint64_t)(uint32_t)(uint64_t)**var_10;
                *(uint64_t *)(local_sp_1 + (-8L)) = 4206020UL;
                var_43 = indirect_placeholder_9(var_42);
                var_44 = (uint64_t)(unsigned char)var_43;
                var_45 = local_sp_1 + (-16L);
                *(uint64_t *)var_45 = 4206030UL;
                indirect_placeholder_1();
                local_sp_1 = var_45;
                local_sp_2 = var_45;
                if (var_44 != 0UL) {
                    var_48 = *var_40;
                    break;
                }
                var_46 = *var_9 << 4U;
                var_47 = **var_10;
                if (((signed char)var_47 <= '`') || ((signed char)var_47 > 'f')) {
                    rax_1_in = (uint32_t)var_47 + (-87);
                } else {
                    if (((signed char)var_47 <= '@') || ((signed char)var_47 > 'F')) {
                        rax_1_in = (uint32_t)var_47 + (-48);
                    } else {
                        rax_1_in = (uint32_t)var_47 + (-55);
                    }
                }
                *var_9 = (rax_1_in + var_46);
                *var_40 = (*var_40 + 1U);
                *var_8 = (*var_8 + 1UL);
                continue;
            }
        local_sp_3 = local_sp_2;
        if (var_48 == 0U) {
            var_49 = local_sp_2 + (-8L);
            *(uint64_t *)var_49 = 4206069UL;
            var_50 = indirect_placeholder_10(0UL, rcx, 4274360UL, 1UL, 0UL, r9, r8);
            local_sp_3 = var_49;
            r97_2 = var_50.field_0;
            r88_2 = var_50.field_1;
        }
        *(uint64_t *)(local_sp_3 + (-8L)) = 4206079UL;
        indirect_placeholder_1();
        r97_1 = r97_2;
        r88_1 = r88_2;
    } else {
        if (((signed char)var_11 <= '/') || ((signed char)var_11 > '7')) {
            if (var_11 != '\x00') {
                var_14 = (uint64_t)(uint32_t)(uint64_t)var_11;
                var_15 = var_0 + (-64L);
                *(uint64_t *)var_15 = 4206267UL;
                indirect_placeholder_1();
                local_sp_4 = var_15;
                if (var_14 != 0UL) {
                    var_16 = *var_8;
                    *var_8 = (var_16 + 1UL);
                    var_17 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)var_16;
                    *(uint64_t *)(var_0 + (-72L)) = 4206297UL;
                    indirect_placeholder_8(var_17);
                    mrv.field_0 = (uint64_t)(((uint32_t)*var_8 - (uint32_t)*var_4) + (-1));
                    mrv1 = mrv;
                    mrv1.field_1 = rcx4_0;
                    mrv2 = mrv1;
                    mrv2.field_2 = r97_1;
                    mrv3 = mrv2;
                    mrv3.field_3 = r88_1;
                    return mrv3;
                }
                var_18 = **var_10;
            }
            var_19 = (unsigned char *)(var_0 + (-29L));
            *var_19 = var_18;
            var_20 = (uint32_t *)(var_0 + (-28L));
            *var_20 = 0U;
            spec_select = (*var_19 == 'u') ? 4U : 8U;
            var_21 = (uint32_t *)(var_0 + (-24L));
            *var_21 = spec_select;
            *var_8 = (*var_8 + 1UL);
            r97_0 = r97_3;
            r88_0 = r88_3;
            local_sp_6 = local_sp_5;
            r88_4 = r88_3;
            r97_4 = r97_3;
            while ((int)*var_21 <= (int)0U)
                {
                    var_32 = (uint64_t)(uint32_t)(uint64_t)**var_10;
                    *(uint64_t *)(local_sp_5 + (-8L)) = 4206393UL;
                    var_33 = indirect_placeholder_9(var_32);
                    var_34 = (uint64_t)(unsigned char)var_33;
                    var_35 = local_sp_5 + (-16L);
                    *(uint64_t *)var_35 = 4206403UL;
                    indirect_placeholder_1();
                    local_sp_0 = var_35;
                    if (var_34 == 0UL) {
                        var_36 = local_sp_5 + (-24L);
                        *(uint64_t *)var_36 = 4206432UL;
                        var_37 = indirect_placeholder_7(0UL, rcx, 4274360UL, 1UL, 0UL, r97_3, r88_3);
                        local_sp_0 = var_36;
                        r97_0 = var_37.field_0;
                        r88_0 = var_37.field_1;
                    }
                    var_38 = *var_20 << 4U;
                    var_39 = **var_10;
                    local_sp_5 = local_sp_0;
                    r97_3 = r97_0;
                    r88_3 = r88_0;
                    if (((signed char)var_39 <= '`') || ((signed char)var_39 > 'f')) {
                        rax_0_in = (uint32_t)var_39 + (-87);
                    } else {
                        if (((signed char)var_39 <= '@') || ((signed char)var_39 > 'F')) {
                            rax_0_in = (uint32_t)var_39 + (-48);
                        } else {
                            rax_0_in = (uint32_t)var_39 + (-55);
                        }
                    }
                    *var_20 = (rax_0_in + var_38);
                    *var_21 = (*var_21 + (-1));
                    *var_8 = (*var_8 + 1UL);
                    r97_0 = r97_3;
                    r88_0 = r88_3;
                    local_sp_6 = local_sp_5;
                    r88_4 = r88_3;
                    r97_4 = r97_3;
                }
            var_22 = *var_20;
            var_24 = var_22;
            if (var_22 > 159U) {
                var_23 = helper_cc_compute_all_wrapper((uint64_t)var_22 + (-57343L), 57343UL, var_1, 16U);
                if (var_22 <= 55295U & (var_23 & 65UL) != 0UL) {
                    _pre167 = *var_20;
                    var_24 = _pre167;
                    var_25 = *var_19;
                    spec_select165 = (var_25 == 'u') ? 4UL : 8UL;
                    var_26 = (uint64_t)(uint32_t)(uint64_t)var_25;
                    var_27 = (uint64_t)var_24;
                    var_28 = local_sp_5 + (-8L);
                    *(uint64_t *)var_28 = 4206654UL;
                    var_29 = indirect_placeholder_11(0UL, var_26, 4274416UL, 1UL, 0UL, var_27, spec_select165);
                    local_sp_6 = var_28;
                    r97_4 = var_29.field_0;
                    r88_4 = var_29.field_1;
                }
            } else {
                var_23 = helper_cc_compute_all_wrapper((uint64_t)var_22 + (-57343L), 57343UL, var_1, 16U);
                if (var_22 <= 55295U & (var_23 & 65UL) != 0UL) {
                    _pre167 = *var_20;
                    var_24 = _pre167;
                    var_25 = *var_19;
                    spec_select165 = (var_25 == 'u') ? 4UL : 8UL;
                    var_26 = (uint64_t)(uint32_t)(uint64_t)var_25;
                    var_27 = (uint64_t)var_24;
                    var_28 = local_sp_5 + (-8L);
                    *(uint64_t *)var_28 = 4206654UL;
                    var_29 = indirect_placeholder_11(0UL, var_26, 4274416UL, 1UL, 0UL, var_27, spec_select165);
                    local_sp_6 = var_28;
                    r97_4 = var_29.field_0;
                    r88_4 = var_29.field_1;
                }
            }
            var_30 = *(uint64_t *)4292160UL;
            var_31 = (uint64_t)*var_20;
            *(uint64_t *)(local_sp_6 + (-8L)) = 4206679UL;
            indirect_placeholder_12(0UL, var_30, var_31);
            rcx4_0 = var_31;
            r97_1 = r97_4;
            r88_1 = r88_4;
        } else {
            var_12 = (uint32_t *)(var_0 + (-24L));
            *var_12 = 0U;
            if (*var_5 == '\x00') {
            } else {
                storemerge31 = 1UL;
                if (**var_10 == '0') {
                }
            }
            *var_8 = (*var_8 + storemerge31);
            while ((int)*var_12 <= (int)2U)
                {
                    var_13 = **var_10;
                    if (((signed char)var_13 <= '/') || ((signed char)var_13 > '7')) {
                        break;
                    }
                    *var_9 = (((uint32_t)var_13 + (-48)) + (*var_9 << 3U));
                    *var_12 = (*var_12 + 1U);
                    *var_8 = (*var_8 + 1UL);
                }
            *(uint64_t *)(var_0 + (-64L)) = 4206229UL;
            indirect_placeholder_1();
        }
    }
    mrv.field_0 = (uint64_t)(((uint32_t)*var_8 - (uint32_t)*var_4) + (-1));
    mrv1 = mrv;
    mrv1.field_1 = rcx4_0;
    mrv2 = mrv1;
    mrv2.field_2 = r97_1;
    mrv3 = mrv2;
    mrv3.field_3 = r88_1;
    return mrv3;
}
