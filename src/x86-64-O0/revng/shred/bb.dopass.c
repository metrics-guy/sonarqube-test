typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_divq_EAX_wrapper_ret_type;
struct type_5;
struct indirect_placeholder_18_ret_type;
struct indirect_placeholder_17_ret_type;
struct indirect_placeholder_5_ret_type;
struct indirect_placeholder_7_ret_type;
struct indirect_placeholder_8_ret_type;
struct indirect_placeholder_9_ret_type;
struct indirect_placeholder_10_ret_type;
struct indirect_placeholder_11_ret_type;
struct indirect_placeholder_13_ret_type;
struct indirect_placeholder_15_ret_type;
struct indirect_placeholder_16_ret_type;
struct helper_divq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint32_t field_3;
    uint64_t field_4;
    uint32_t field_5;
    uint64_t field_6;
    uint32_t field_7;
    uint64_t field_8;
    uint32_t field_9;
    uint32_t field_10;
    unsigned char field_11;
    uint32_t field_12;
};
struct type_5 {
};
struct indirect_placeholder_18_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_17_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_5_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_7_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_8_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_9_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_10_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_11_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_13_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_15_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_16_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_6(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t indirect_placeholder_19(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern uint32_t init_state_0x8248(void);
extern struct helper_divq_EAX_wrapper_ret_type helper_divq_EAX_wrapper(struct type_5 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint64_t param_13, uint32_t param_14, uint64_t param_15, uint64_t param_16, uint64_t param_17, uint32_t param_18, uint32_t param_19);
extern void indirect_placeholder_14(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_3(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_18_ret_type indirect_placeholder_18(uint64_t param_0);
extern struct indirect_placeholder_17_ret_type indirect_placeholder_17(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_5_ret_type indirect_placeholder_5(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_7_ret_type indirect_placeholder_7(uint64_t param_0);
extern struct indirect_placeholder_8_ret_type indirect_placeholder_8(uint64_t param_0);
extern struct indirect_placeholder_9_ret_type indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_10_ret_type indirect_placeholder_10(uint64_t param_0);
extern struct indirect_placeholder_11_ret_type indirect_placeholder_11(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_13_ret_type indirect_placeholder_13(uint64_t param_0);
extern struct indirect_placeholder_15_ret_type indirect_placeholder_15(uint64_t param_0);
extern struct indirect_placeholder_16_ret_type indirect_placeholder_16(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
uint64_t bb_dopass(uint64_t rcx, uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t r9, uint64_t r8) {
    uint64_t var_165;
    uint64_t var_184;
    struct indirect_placeholder_17_ret_type var_37;
    uint64_t storemerge4;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint32_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint32_t var_9;
    uint32_t var_10;
    uint64_t var_11;
    uint32_t *var_12;
    uint64_t *var_13;
    uint64_t *var_14;
    uint64_t var_15;
    uint32_t *var_16;
    uint64_t *var_17;
    uint64_t **var_18;
    uint64_t var_19;
    uint64_t *var_20;
    uint64_t *var_21;
    uint64_t var_22;
    uint64_t *var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t spec_select;
    uint64_t *var_26;
    unsigned __int128 var_27;
    uint64_t var_28;
    struct indirect_placeholder_18_ret_type var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t *var_43;
    unsigned char *var_44;
    unsigned char *var_45;
    uint64_t var_46;
    uint64_t var_117;
    uint64_t var_118;
    uint64_t var_98;
    uint64_t var_109;
    uint64_t var_196;
    uint64_t local_sp_12_ph_be;
    uint64_t r95_3_ph;
    uint64_t r86_4_be;
    uint64_t local_sp_12_be;
    uint64_t var_185;
    uint64_t var_186;
    uint64_t var_187;
    uint64_t var_188;
    uint64_t *_pre_phi399;
    uint64_t _pre_phi401;
    uint64_t var_189;
    uint64_t r86_0;
    uint32_t state_0x9010_1;
    uint32_t state_0x9010_2_ph;
    uint64_t state_0x9018_0;
    uint64_t rax_0;
    uint64_t local_sp_12_ph;
    uint32_t state_0x9010_0;
    uint32_t state_0x9080_1;
    uint32_t state_0x9080_2_ph;
    uint64_t state_0x82d8_0;
    uint32_t state_0x8248_1;
    uint32_t state_0x8248_2_ph;
    uint32_t state_0x9080_0;
    uint32_t state_0x8248_0;
    uint64_t storemerge6;
    uint64_t *_pre_phi397;
    uint64_t _pre_phi393;
    uint64_t var_190;
    uint64_t var_191;
    uint64_t var_192;
    struct indirect_placeholder_5_ret_type var_193;
    uint64_t var_194;
    uint64_t var_195;
    uint64_t var_172;
    uint64_t var_173;
    uint64_t var_174;
    uint64_t var_175;
    uint64_t var_176;
    uint64_t var_177;
    uint64_t var_178;
    uint64_t state_0x9018_1;
    uint64_t local_sp_12;
    struct helper_divq_EAX_wrapper_ret_type var_179;
    uint64_t var_180;
    struct helper_divq_EAX_wrapper_ret_type var_181;
    uint64_t state_0x82d8_1;
    uint64_t var_182;
    uint64_t var_183;
    uint64_t local_sp_7;
    uint64_t rax_2;
    uint64_t var_162;
    uint64_t r95_3;
    uint64_t var_166;
    uint64_t var_167;
    uint64_t local_sp_9;
    uint64_t local_sp_0;
    uint64_t var_168;
    struct indirect_placeholder_7_ret_type var_169;
    uint64_t var_170;
    uint64_t var_171;
    uint64_t var_150;
    uint64_t var_151;
    uint64_t var_147;
    uint64_t var_148;
    struct indirect_placeholder_8_ret_type var_149;
    uint64_t local_sp_1_ph;
    uint64_t var_137;
    uint64_t var_138;
    struct indirect_placeholder_9_ret_type var_139;
    uint64_t var_140;
    uint64_t var_141;
    uint64_t var_135;
    uint64_t var_136;
    uint64_t local_sp_2;
    uint64_t var_142;
    uint64_t var_143;
    uint64_t var_144;
    uint64_t var_145;
    uint64_t var_146;
    uint64_t local_sp_3;
    uint64_t var_127;
    uint32_t var_128;
    uint64_t var_129;
    uint64_t var_130;
    uint64_t var_131;
    uint64_t local_sp_8;
    uint64_t var_132;
    uint64_t rax_1;
    uint64_t local_sp_4;
    uint64_t var_133;
    unsigned char var_134;
    uint64_t r95_0;
    uint64_t r86_1;
    uint64_t local_sp_5;
    uint64_t var_152;
    uint64_t var_114;
    uint64_t var_123;
    uint64_t var_124;
    struct indirect_placeholder_10_ret_type var_125;
    uint64_t var_126;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t var_106;
    struct indirect_placeholder_11_ret_type var_107;
    uint64_t var_108;
    uint64_t local_sp_6;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t var_112;
    uint64_t var_113;
    uint64_t r86_4;
    uint64_t r95_1;
    uint64_t r86_2;
    uint64_t var_115;
    uint64_t var_116;
    uint64_t var_153;
    uint64_t var_154;
    uint64_t var_155;
    uint64_t var_197;
    uint64_t var_198;
    uint64_t var_156;
    bool var_157;
    unsigned char var_158;
    uint64_t var_159;
    uint64_t r86_5;
    unsigned char var_160;
    uint64_t var_161;
    uint64_t var_163;
    uint64_t var_164;
    uint64_t var_119;
    uint64_t var_120;
    uint64_t var_121;
    uint64_t var_122;
    uint64_t storemerge5;
    uint64_t local_sp_10;
    uint64_t *var_71;
    uint64_t var_72;
    uint64_t _pre392;
    uint64_t *_pre_phi405;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t *var_76;
    uint64_t *var_77;
    uint64_t var_78;
    uint64_t *var_79;
    uint64_t var_80;
    uint64_t *var_81;
    uint64_t r95_2;
    uint64_t local_sp_14;
    uint64_t r86_3;
    uint64_t local_sp_11;
    uint64_t *var_82;
    uint64_t *var_83;
    uint64_t *var_84;
    uint64_t *var_85;
    uint32_t *var_86;
    uint64_t var_87;
    uint64_t *var_88;
    unsigned char *var_89;
    uint32_t *var_90;
    uint64_t var_91;
    uint64_t *var_92;
    uint64_t *var_93;
    uint32_t *var_94;
    uint64_t var_95;
    uint64_t *var_96;
    uint64_t var_97;
    uint64_t r86_4_ph;
    uint64_t state_0x9018_2_ph;
    uint64_t state_0x82d8_2_ph;
    uint64_t var_99;
    struct indirect_placeholder_13_ret_type var_100;
    unsigned __int128 var_61;
    uint64_t var_62;
    uint64_t var_63;
    unsigned char storemerge;
    uint64_t _pre_phi391;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t local_sp_13;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    struct indirect_placeholder_15_ret_type var_60;
    uint64_t r95_4;
    unsigned __int128 _pre382;
    uint64_t _pre390;
    uint64_t var_69;
    uint64_t var_70;
    unsigned char *var_47;
    unsigned char var_48;
    uint64_t var_49;
    uint64_t var_50;
    struct indirect_placeholder_16_ret_type var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = init_cc_src2();
    var_4 = init_state_0x9018();
    var_5 = init_state_0x9010();
    var_6 = init_state_0x8408();
    var_7 = init_state_0x8328();
    var_8 = init_state_0x82d8();
    var_9 = init_state_0x9080();
    var_10 = init_state_0x8248();
    var_11 = var_0 + (-8L);
    *(uint64_t *)var_11 = var_1;
    var_12 = (uint32_t *)(var_0 + (-844L));
    *var_12 = (uint32_t)rdi;
    var_13 = (uint64_t *)(var_0 + (-856L));
    *var_13 = rsi;
    var_14 = (uint64_t *)(var_0 + (-864L));
    *var_14 = rdx;
    var_15 = var_0 + (-872L);
    *(uint64_t *)var_15 = rcx;
    var_16 = (uint32_t *)(var_0 + (-848L));
    *var_16 = (uint32_t)r8;
    var_17 = (uint64_t *)(var_0 + (-880L));
    *var_17 = r9;
    var_18 = (uint64_t **)var_15;
    var_19 = **var_18;
    var_20 = (uint64_t *)(var_0 + (-16L));
    *var_20 = var_19;
    var_21 = (uint64_t *)(var_0 + (-40L));
    *var_21 = 0UL;
    *(uint64_t *)(var_0 + (-896L)) = 4206536UL;
    indirect_placeholder();
    var_22 = (uint64_t)((long)(var_19 << 32UL) >> (long)32UL);
    var_23 = (uint64_t *)(var_0 + (-104L));
    *var_23 = var_22;
    var_24 = (uint64_t)*var_16;
    *(uint64_t *)(var_0 + (-904L)) = 4206555UL;
    var_25 = indirect_placeholder_19(var_24);
    spec_select = ((uint64_t)(unsigned char)var_25 == 0UL) ? 65536UL : 61440UL;
    var_26 = (uint64_t *)(var_0 + (-112L));
    *var_26 = spec_select;
    var_27 = ((unsigned __int128)(spec_select + 2UL) * 12297829382473034411ULL) >> 65ULL;
    var_28 = (*var_23 + ((uint64_t)var_27 + (uint64_t)(var_27 << 1ULL))) + (-1L);
    *(uint64_t *)(var_0 + (-912L)) = 4206627UL;
    var_29 = indirect_placeholder_18(var_28);
    var_30 = var_29.field_0;
    var_31 = var_29.field_1;
    var_32 = var_29.field_2;
    var_33 = var_29.field_3;
    var_34 = var_29.field_4;
    *(uint64_t *)(var_0 + (-120L)) = var_30;
    var_35 = *var_23;
    var_36 = var_0 + (-920L);
    *(uint64_t *)var_36 = 4206650UL;
    var_37 = indirect_placeholder_17(var_31, var_30, var_35, var_32, var_33, var_34, var_2);
    var_38 = var_37.field_0;
    var_39 = var_37.field_1;
    var_40 = var_37.field_2;
    var_41 = var_37.field_3;
    var_42 = var_37.field_4;
    var_43 = (uint64_t *)(var_0 + (-128L));
    *var_43 = var_38;
    var_44 = (unsigned char *)(var_0 + (-65L));
    *var_44 = (unsigned char)'\x00';
    var_45 = (unsigned char *)(var_0 + (-66L));
    *var_45 = (unsigned char)'\x00';
    var_46 = *var_20;
    storemerge4 = 4294967295UL;
    state_0x9010_2_ph = var_5;
    rax_0 = 100UL;
    state_0x9080_2_ph = var_9;
    state_0x8248_2_ph = var_10;
    var_114 = 0UL;
    r86_5 = var_42;
    local_sp_14 = var_36;
    state_0x9018_2_ph = var_4;
    state_0x82d8_2_ph = var_8;
    storemerge = (unsigned char)'\x00';
    r95_4 = var_41;
    if ((long)var_46 > (long)0UL) {
    }
    var_47 = (unsigned char *)(var_0 + (-81L));
    var_48 = storemerge & '\x01';
    *var_47 = var_48;
    if (var_48 == '\x01') {
        var_49 = (uint64_t)*var_12;
        var_50 = var_0 + (-928L);
        *(uint64_t *)var_50 = 4206727UL;
        var_51 = indirect_placeholder_16(var_39, var_40, var_49, 1UL, var_41, var_42);
        r95_4 = var_51.field_0;
        r86_5 = var_51.field_1;
        local_sp_14 = var_50;
    }
    var_52 = *var_13;
    var_53 = (uint64_t)*var_12;
    *(uint64_t *)(local_sp_14 + (-8L)) = 4206750UL;
    var_54 = indirect_placeholder_1(var_53, var_52);
    var_55 = (uint64_t)(uint32_t)var_54 ^ 1UL;
    r86_3 = r86_5;
    if ((uint64_t)(unsigned char)var_55 != 0UL) {
        if ((int)*var_16 < (int)0U) {
            var_69 = var_0 + (-183L);
            var_70 = local_sp_14 + (-16L);
            *(uint64_t *)var_70 = 4206987UL;
            indirect_placeholder_14(0UL, var_69);
            local_sp_10 = var_70;
        } else {
            var_59 = *var_20;
            *(uint64_t *)(local_sp_14 + (-16L)) = 4206830UL;
            var_60 = indirect_placeholder_15(var_59);
            if ((uint64_t)(unsigned char)var_60.field_0 == 0UL) {
                _pre382 = ((unsigned __int128)(*var_26 + 2UL) * 12297829382473034411ULL) >> 65ULL;
                _pre390 = (uint64_t)(_pre382 << 1ULL) + (uint64_t)_pre382;
                _pre_phi391 = _pre390;
                storemerge5 = _pre_phi391;
            } else {
                var_61 = ((unsigned __int128)(*var_26 + 2UL) * 12297829382473034411ULL) >> 65ULL;
                var_62 = (uint64_t)var_61 + (uint64_t)(var_61 << 1ULL);
                var_63 = *var_20;
                _pre_phi391 = var_62;
                storemerge5 = var_63;
                if (var_62 > var_63) {
                    storemerge5 = _pre_phi391;
                }
            }
            *(uint64_t *)(var_0 + (-48L)) = storemerge5;
            var_64 = *var_43;
            var_65 = (uint64_t)*var_16;
            *(uint64_t *)(local_sp_14 + (-24L)) = 4206943UL;
            indirect_placeholder_12(storemerge5, var_65, var_64);
            var_66 = var_0 + (-183L);
            var_67 = *var_43;
            var_68 = local_sp_14 + (-32L);
            *(uint64_t *)var_68 = 4206965UL;
            indirect_placeholder_14(var_67, var_66);
            local_sp_10 = var_68;
        }
        var_71 = (uint64_t *)(var_0 | 16UL);
        var_72 = *var_71;
        r95_2 = var_72;
        local_sp_11 = local_sp_10;
        if (var_72 == 0UL) {
            _pre392 = var_0 + (-80L);
            _pre_phi399 = (uint64_t *)_pre392;
            _pre_phi401 = var_0 + (-183L);
            _pre_phi397 = (uint64_t *)(var_0 + (-32L));
            _pre_phi393 = _pre392;
            _pre_phi405 = (uint64_t *)(var_0 | 8UL);
            r95_2 = r95_4;
        } else {
            var_73 = *var_14;
            var_74 = var_0 + (-183L);
            var_75 = local_sp_10 + (-16L);
            var_76 = (uint64_t *)var_75;
            *var_76 = var_74;
            var_77 = (uint64_t *)(var_0 | 8UL);
            var_78 = *var_77;
            *(uint64_t *)(local_sp_10 + (-24L)) = 4207052UL;
            indirect_placeholder_3(0UL, var_73, 4296553UL, 0UL, 0UL, var_72, var_78);
            *var_76 = 4207066UL;
            indirect_placeholder();
            var_79 = (uint64_t *)(var_0 + (-32L));
            *var_79 = 5UL;
            var_80 = var_0 + (-80L);
            var_81 = (uint64_t *)var_80;
            *var_81 = 4292995UL;
            _pre_phi399 = var_81;
            _pre_phi401 = var_74;
            _pre_phi397 = var_79;
            _pre_phi393 = var_80;
            _pre_phi405 = var_77;
            r86_3 = var_78;
            local_sp_11 = var_75;
        }
        var_82 = (uint64_t *)(var_0 + (-24L));
        *var_82 = 0UL;
        var_83 = (uint64_t *)(var_0 + (-48L));
        var_84 = (uint64_t *)(var_0 + (-56L));
        var_85 = (uint64_t *)(var_0 + (-64L));
        var_86 = (uint32_t *)(var_0 + (-132L));
        var_87 = var_0 + (-424L);
        var_88 = (uint64_t *)(var_0 + (-144L));
        var_89 = (unsigned char *)(var_0 + (-145L));
        var_90 = (uint32_t *)(var_0 + (-152L));
        var_91 = var_0 + (-840L);
        var_92 = (uint64_t *)(var_0 + (-96L));
        var_93 = (uint64_t *)(var_0 + (-160L));
        var_94 = (uint32_t *)(var_0 + (-164L));
        var_95 = var_0 + (-632L);
        var_96 = (uint64_t *)(var_0 + (-176L));
        var_97 = var_0 + (-392L);
        r95_3_ph = r95_2;
        r86_4_ph = r86_3;
        local_sp_12_ph = local_sp_11;
        while (1U)
            {
                state_0x9010_1 = state_0x9010_2_ph;
                state_0x9018_0 = state_0x9018_2_ph;
                state_0x9010_0 = state_0x9010_2_ph;
                state_0x9080_1 = state_0x9080_2_ph;
                state_0x82d8_0 = state_0x82d8_2_ph;
                state_0x8248_1 = state_0x8248_2_ph;
                state_0x9080_0 = state_0x9080_2_ph;
                state_0x8248_0 = state_0x8248_2_ph;
                state_0x9018_1 = state_0x9018_2_ph;
                local_sp_12 = local_sp_12_ph;
                state_0x82d8_1 = state_0x82d8_2_ph;
                r95_3 = r95_3_ph;
                r86_4 = r86_4_ph;
                while (1U)
                    {
                        *var_83 = *var_26;
                        var_98 = *var_20;
                        var_99 = local_sp_12 + (-8L);
                        *(uint64_t *)var_99 = 4207110UL;
                        var_100 = indirect_placeholder_13(var_98);
                        local_sp_6 = var_99;
                        r95_1 = r95_3;
                        r86_2 = r86_4;
                        if ((uint64_t)(unsigned char)var_100.field_0 != 0UL) {
                            var_101 = *var_20;
                            var_102 = *var_82;
                            var_103 = var_101 - var_102;
                            if (*var_26 <= var_103) {
                                if ((long)var_101 >= (long)var_102) {
                                    loop_state_var = 2U;
                                    break;
                                }
                                *var_83 = var_103;
                                if (var_103 != 0UL) {
                                    loop_state_var = 2U;
                                    break;
                                }
                            }
                        }
                        if ((int)*var_16 > (int)4294967295U) {
                            var_110 = *var_83;
                            var_111 = *var_43;
                            var_112 = *var_17;
                            var_113 = local_sp_12 + (-16L);
                            *(uint64_t *)var_113 = 4207200UL;
                            indirect_placeholder_12(var_110, var_112, var_111);
                            local_sp_6 = var_113;
                        }
                        *var_84 = 0UL;
                        local_sp_7 = local_sp_6;
                        while (1U)
                            {
                                var_115 = *var_83;
                                var_116 = helper_cc_compute_c_wrapper(var_114 - var_115, var_115, var_3, 17U);
                                r86_4_be = r86_2;
                                r95_3 = r95_1;
                                local_sp_8 = local_sp_7;
                                r95_0 = r95_1;
                                r86_1 = r86_2;
                                if (var_116 != 0UL) {
                                    loop_state_var = 2U;
                                    break;
                                }
                                var_117 = *var_83;
                                var_118 = *var_84;
                                var_119 = var_117 - var_118;
                                var_120 = *var_43 + var_118;
                                var_121 = (uint64_t)*var_12;
                                var_122 = local_sp_7 + (-8L);
                                *(uint64_t *)var_122 = 4207251UL;
                                indirect_placeholder();
                                *var_85 = var_121;
                                local_sp_5 = var_122;
                                if ((long)var_121 > (long)0UL) {
                                    var_152 = *var_84 + *var_85;
                                    *var_84 = var_152;
                                    var_114 = var_152;
                                    r95_1 = r95_0;
                                    r86_2 = r86_1;
                                    local_sp_7 = local_sp_5;
                                    continue;
                                }
                                var_123 = *var_20;
                                var_124 = local_sp_7 + (-16L);
                                *(uint64_t *)var_124 = 4207297UL;
                                var_125 = indirect_placeholder_10(var_123);
                                var_126 = (uint64_t)(uint32_t)var_125.field_0 ^ 1UL;
                                local_sp_3 = var_124;
                                rax_1 = var_126;
                                local_sp_4 = var_124;
                                if ((uint64_t)(unsigned char)var_126 != 0UL) {
                                    if (*var_85 != 0UL) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    var_127 = local_sp_7 + (-24L);
                                    *(uint64_t *)var_127 = 4207316UL;
                                    indirect_placeholder();
                                    var_128 = *(uint32_t *)var_126;
                                    local_sp_3 = var_127;
                                    rax_1 = (uint64_t)var_128;
                                    local_sp_4 = var_127;
                                    if ((uint64_t)(var_128 + (-28)) != 0UL) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                }
                                var_133 = local_sp_4 + (-8L);
                                *(uint64_t *)var_133 = 4207386UL;
                                indirect_placeholder();
                                *var_86 = *(uint32_t *)rax_1;
                                var_134 = *var_47 ^ '\x01';
                                local_sp_2 = var_133;
                                if (var_134 != '\x00') {
                                    var_135 = (uint64_t)var_134;
                                    var_136 = local_sp_4 + (-16L);
                                    *(uint64_t *)var_136 = 4207407UL;
                                    indirect_placeholder();
                                    local_sp_2 = var_136;
                                    if (*(uint32_t *)var_135 != 22U) {
                                        var_137 = (uint64_t)*var_12;
                                        var_138 = local_sp_4 + (-24L);
                                        *(uint64_t *)var_138 = 4207432UL;
                                        var_139 = indirect_placeholder_9(var_120, var_119, var_137, 0UL, r95_1, r86_2);
                                        var_140 = var_139.field_0;
                                        var_141 = var_139.field_1;
                                        *var_85 = 0UL;
                                        *var_47 = (unsigned char)'\x01';
                                        r95_0 = var_140;
                                        r86_1 = var_141;
                                        local_sp_5 = var_138;
                                        var_152 = *var_84 + *var_85;
                                        *var_84 = var_152;
                                        var_114 = var_152;
                                        r95_1 = r95_0;
                                        r86_2 = r86_1;
                                        local_sp_7 = local_sp_5;
                                        continue;
                                    }
                                }
                                var_142 = *var_82 + *var_84;
                                *(uint64_t *)(local_sp_2 + (-8L)) = 4207478UL;
                                var_143 = indirect_placeholder_1(var_142, var_87);
                                var_144 = *var_14;
                                var_145 = (uint64_t)*var_86;
                                var_146 = local_sp_2 + (-16L);
                                *(uint64_t *)var_146 = 4207519UL;
                                indirect_placeholder_3(0UL, var_144, 4296584UL, 0UL, var_145, r95_1, var_143);
                                local_sp_1_ph = var_146;
                                r86_1 = var_143;
                                if (*var_86 != 5U) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                var_147 = *var_20;
                                var_148 = local_sp_2 + (-24L);
                                *(uint64_t *)var_148 = 4207541UL;
                                var_149 = indirect_placeholder_8(var_147);
                                local_sp_1_ph = var_148;
                                if ((uint64_t)(unsigned char)var_149.field_0 != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                var_150 = *var_84 | 511UL;
                                if (*var_83 <= var_150) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                *var_88 = (var_150 + 1UL);
                                var_151 = local_sp_2 + (-32L);
                                *(uint64_t *)var_151 = 4207624UL;
                                indirect_placeholder();
                                *var_85 = (*var_88 - *var_84);
                                *var_44 = (unsigned char)'\x01';
                                local_sp_5 = var_151;
                            }
                        switch_state_var = 0;
                        switch (loop_state_var) {
                          case 1U:
                            {
                                *var_45 = (unsigned char)'\x01';
                                local_sp_13 = local_sp_1_ph;
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            break;
                          case 2U:
                          case 0U:
                            {
                                switch_state_var = 0;
                                switch (loop_state_var) {
                                  case 2U:
                                    {
                                        var_153 = *var_82;
                                        var_154 = 9223372036854775807UL - var_153;
                                        var_155 = *var_84;
                                        local_sp_12_be = local_sp_8;
                                        local_sp_9 = local_sp_8;
                                        if (var_155 <= var_154) {
                                            var_197 = *var_14;
                                            var_198 = local_sp_8 + (-8L);
                                            *(uint64_t *)var_198 = 4207779UL;
                                            indirect_placeholder_3(0UL, var_197, 4296632UL, 0UL, 0UL, r95_1, r86_2);
                                            *var_45 = (unsigned char)'\x01';
                                            local_sp_13 = var_198;
                                            loop_state_var = 1U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                        var_156 = var_155 + var_153;
                                        *var_82 = var_156;
                                        var_157 = (var_156 == *var_20);
                                        var_158 = var_157;
                                        *var_89 = var_158;
                                        if (*var_71 == 0UL) {
                                            r86_4 = r86_4_be;
                                            local_sp_12 = local_sp_12_be;
                                            continue;
                                        }
                                        var_159 = (var_156 & (-256L)) | var_157;
                                        r86_4_be = 1UL;
                                        rax_2 = var_159;
                                        if (var_158 == '\x00') {
                                            var_160 = **(unsigned char **)_pre_phi393;
                                            var_161 = (uint64_t)var_160;
                                            rax_2 = var_161;
                                            var_162 = local_sp_8 + (-8L);
                                            *(uint64_t *)var_162 = 4207861UL;
                                            indirect_placeholder();
                                            *var_21 = rax_2;
                                            r86_4_be = r86_2;
                                            local_sp_12_be = var_162;
                                            local_sp_9 = var_162;
                                            if (var_160 != '\x00' & (long)rax_2 >= (long)*_pre_phi397) {
                                                r86_4 = r86_4_be;
                                                local_sp_12 = local_sp_12_be;
                                                continue;
                                            }
                                        }
                                        var_162 = local_sp_8 + (-8L);
                                        *(uint64_t *)var_162 = 4207861UL;
                                        indirect_placeholder();
                                        *var_21 = rax_2;
                                        r86_4_be = r86_2;
                                        local_sp_12_be = var_162;
                                        local_sp_9 = var_162;
                                        if ((long)rax_2 >= (long)*_pre_phi397) {
                                            r86_4 = r86_4_be;
                                            local_sp_12 = local_sp_12_be;
                                            continue;
                                        }
                                        *var_90 = 432U;
                                        var_163 = *var_82;
                                        var_164 = local_sp_9 + (-8L);
                                        *(uint64_t *)var_164 = 4207930UL;
                                        var_165 = indirect_placeholder_6(1UL, 434UL, var_163, var_91, 1UL);
                                        *var_92 = var_165;
                                        local_sp_0 = var_164;
                                        if (*var_89 != '\x00') {
                                            loop_state_var = 0U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                        var_166 = *_pre_phi399;
                                        var_167 = local_sp_9 + (-16L);
                                        *(uint64_t *)var_167 = 4207962UL;
                                        indirect_placeholder();
                                        local_sp_12_be = var_167;
                                        local_sp_0 = var_167;
                                        if ((uint64_t)(uint32_t)var_166 != 0UL) {
                                            loop_state_var = 0U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                    }
                                    break;
                                  case 0U:
                                    {
                                        var_129 = *var_82;
                                        var_130 = 9223372036854775807UL - var_129;
                                        var_131 = *var_84;
                                        local_sp_8 = local_sp_3;
                                        if (var_131 > var_130) {
                                            var_132 = var_131 + var_129;
                                            *var_20 = var_132;
                                            **var_18 = var_132;
                                        }
                                    }
                                    break;
                                }
                                if (switch_state_var)
                                    break;
                            }
                            break;
                        }
                        if (switch_state_var)
                            break;
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 1U:
                    {
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 0U:
                    {
                        var_168 = *var_20;
                        *(uint64_t *)(local_sp_0 + (-8L)) = 4207982UL;
                        var_169 = indirect_placeholder_7(var_168);
                        var_170 = var_169.field_0;
                        var_171 = var_169.field_1;
                        if ((uint64_t)(unsigned char)var_170 == 1UL) {
                            var_172 = *var_71;
                            var_173 = *var_14;
                            var_174 = *var_92;
                            var_175 = local_sp_0 + (-16L);
                            *(uint64_t *)var_175 = var_174;
                            *(uint64_t *)(local_sp_0 + (-24L)) = _pre_phi401;
                            var_176 = *_pre_phi405;
                            *(uint64_t *)(local_sp_0 + (-32L)) = 4208046UL;
                            indirect_placeholder_3(0UL, var_173, 4296651UL, 0UL, 0UL, var_172, var_176);
                            r86_0 = var_176;
                            storemerge6 = var_175;
                        } else {
                            var_177 = *var_82;
                            *var_93 = var_177;
                            var_178 = *var_20;
                            if (var_178 != 0UL) {
                                if (var_177 > 184467440737095516UL) {
                                    var_180 = (uint64_t)((long)(var_178 + (uint64_t)(((unsigned __int128)var_178 * 18446744073709551615ULL) >> 64ULL)) >> (long)6UL) - (uint64_t)((long)var_178 >> (long)63UL);
                                    var_181 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), var_180, 4208201UL, var_177, var_11, var_178, 0UL, var_171, var_180, var_32, r95_1, 1UL, var_2, state_0x9018_2_ph, state_0x9010_2_ph, var_6, var_7, state_0x82d8_2_ph, state_0x9080_2_ph, state_0x8248_2_ph);
                                    state_0x9010_1 = var_181.field_7;
                                    rax_0 = var_181.field_1;
                                    state_0x9080_1 = var_181.field_9;
                                    state_0x8248_1 = var_181.field_10;
                                    state_0x9018_1 = var_181.field_6;
                                    state_0x82d8_1 = var_181.field_8;
                                } else {
                                    var_179 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), var_178, 4208137UL, var_177 * 100UL, var_11, 1UL, 0UL, var_171, var_178, var_32, r95_1, 1UL, var_2, state_0x9018_2_ph, state_0x9010_2_ph, var_6, var_7, state_0x82d8_2_ph, state_0x9080_2_ph, state_0x8248_2_ph);
                                    state_0x9010_1 = var_179.field_7;
                                    rax_0 = var_179.field_1;
                                    state_0x9080_1 = var_179.field_9;
                                    state_0x8248_1 = var_179.field_10;
                                    state_0x9018_1 = var_179.field_6;
                                    state_0x82d8_1 = var_179.field_8;
                                }
                            }
                            *var_94 = (uint32_t)rax_0;
                            var_182 = *var_20;
                            var_183 = (uint64_t)*var_90;
                            *(uint64_t *)(local_sp_0 + (-16L)) = 4208253UL;
                            var_184 = indirect_placeholder_6(1UL, var_183, var_182, var_95, 1UL);
                            *var_96 = var_184;
                            state_0x9018_0 = state_0x9018_1;
                            state_0x9010_0 = state_0x9010_1;
                            state_0x82d8_0 = state_0x82d8_1;
                            state_0x9080_0 = state_0x9080_1;
                            state_0x8248_0 = state_0x8248_1;
                            if (*var_89 != '\x00') {
                                *var_92 = var_184;
                            }
                            var_185 = *var_71;
                            var_186 = *var_14;
                            var_187 = (uint64_t)*var_94;
                            var_188 = local_sp_0 + (-24L);
                            *(uint64_t *)var_188 = var_187;
                            *(uint64_t *)(local_sp_0 + (-32L)) = *var_96;
                            *(uint64_t *)(local_sp_0 + (-40L)) = *var_92;
                            *(uint64_t *)(local_sp_0 + (-48L)) = _pre_phi401;
                            var_189 = *_pre_phi405;
                            *(uint64_t *)(local_sp_0 + (-56L)) = 4208350UL;
                            indirect_placeholder_3(0UL, var_186, 4296680UL, 0UL, 0UL, var_185, var_189);
                            r86_0 = var_189;
                            storemerge6 = var_188;
                        }
                        *(uint64_t *)(storemerge6 + (-8L)) = 4208376UL;
                        indirect_placeholder();
                        *_pre_phi399 = var_97;
                        *_pre_phi397 = (*var_21 + 5UL);
                        var_190 = *var_14;
                        var_191 = (uint64_t)*var_12;
                        var_192 = storemerge6 + (-16L);
                        *(uint64_t *)var_192 = 4208422UL;
                        var_193 = indirect_placeholder_5(var_191, var_190);
                        var_194 = var_193.field_0;
                        var_195 = var_193.field_1;
                        local_sp_12_ph_be = var_192;
                        r95_3_ph = var_195;
                        state_0x9010_2_ph = state_0x9010_0;
                        state_0x9080_2_ph = state_0x9080_0;
                        state_0x8248_2_ph = state_0x8248_0;
                        r86_4_ph = r86_0;
                        state_0x9018_2_ph = state_0x9018_0;
                        state_0x82d8_2_ph = state_0x82d8_0;
                        if ((uint64_t)(uint32_t)var_194 != 0UL) {
                            var_196 = storemerge6 + (-24L);
                            *(uint64_t *)var_196 = 4208435UL;
                            indirect_placeholder();
                            local_sp_12_ph_be = var_196;
                            local_sp_13 = var_196;
                            if (*(uint32_t *)var_194 != 5U) {
                                *var_45 = (unsigned char)'\x01';
                                switch_state_var = 1;
                                break;
                            }
                            *var_44 = (unsigned char)'\x01';
                        }
                        local_sp_12_ph = local_sp_12_ph_be;
                        continue;
                    }
                    break;
                  case 2U:
                    {
                        var_104 = *var_14;
                        var_105 = (uint64_t)*var_12;
                        var_106 = local_sp_12 + (-16L);
                        *(uint64_t *)var_106 = 4208484UL;
                        var_107 = indirect_placeholder_11(var_105, var_104);
                        var_108 = var_107.field_0;
                        local_sp_13 = var_106;
                        if ((uint64_t)(uint32_t)var_108 == 0UL) {
                            switch_state_var = 1;
                            break;
                        }
                        var_109 = local_sp_12 + (-24L);
                        *(uint64_t *)var_109 = 4208493UL;
                        indirect_placeholder();
                        local_sp_13 = var_109;
                        if (*(uint32_t *)var_108 != 5U) {
                            *var_44 = (unsigned char)'\x01';
                            switch_state_var = 1;
                            break;
                        }
                        *var_45 = (unsigned char)'\x01';
                        switch_state_var = 1;
                        break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
    }
    *(uint64_t *)(local_sp_14 + (-16L)) = 4206762UL;
    indirect_placeholder();
    var_56 = (uint64_t)*(uint32_t *)var_55;
    var_57 = *var_14;
    var_58 = local_sp_14 + (-24L);
    *(uint64_t *)var_58 = 4206796UL;
    indirect_placeholder_3(0UL, var_57, 4296535UL, 0UL, var_56, r95_4, r86_5);
    *var_45 = (unsigned char)'\x01';
    local_sp_13 = var_58;
    *(uint64_t *)(local_sp_13 + (-8L)) = 4208525UL;
    indirect_placeholder();
    if (*var_45 != '\x00') {
        return storemerge4;
    }
    storemerge4 = (uint64_t)*var_44;
    return storemerge4;
}
