typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_1(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder_12(void);
extern uint64_t indirect_placeholder_4(uint64_t param_0);
extern uint32_t init_state_0x82fc(void);
uint64_t bb_canonicalize_filename_mode(uint64_t rdi, uint64_t rsi) {
    uint64_t var_134;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint32_t var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint32_t *var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint32_t var_11;
    uint32_t var_12;
    unsigned char *var_13;
    uint32_t var_14;
    uint32_t var_15;
    uint64_t var_16;
    uint64_t local_sp_7;
    uint64_t local_sp_0;
    uint64_t *_pre_phi290;
    uint64_t *_pre_phi288;
    uint64_t _pre_phi;
    uint64_t local_sp_10_ph;
    uint64_t var_54;
    uint64_t *_pre_phi294;
    uint64_t var_150;
    uint64_t _pre_phi289;
    uint64_t var_128;
    uint64_t var_123;
    uint64_t var_124;
    uint64_t var_125;
    uint64_t var_126;
    uint64_t local_sp_11;
    uint64_t var_127;
    uint64_t var_129;
    uint64_t var_117;
    uint64_t var_118;
    uint64_t var_119;
    uint64_t var_120;
    uint64_t local_sp_3;
    uint64_t var_112;
    unsigned char var_68;
    uint64_t var_113;
    uint64_t var_114;
    uint64_t var_115;
    uint64_t var_116;
    uint64_t var_121;
    uint64_t var_122;
    uint64_t rax_1;
    uint64_t var_130;
    uint64_t local_sp_4;
    uint64_t var_131;
    uint64_t var_106;
    uint64_t var_107;
    uint64_t var_108;
    bool var_109;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t var_96;
    uint64_t var_97;
    uint32_t var_98;
    uint64_t storemerge;
    uint64_t var_95;
    uint32_t _pre287;
    uint64_t var_75;
    uint64_t local_sp_6;
    uint64_t var_99;
    uint64_t var_100;
    uint64_t var_101;
    uint64_t var_132;
    uint64_t var_133;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t *var_92;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t local_sp_8;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t *var_53;
    uint64_t *_pre_phi296;
    uint64_t var_24;
    uint64_t rcx_0;
    uint64_t rdi1_0;
    uint64_t rcx_1;
    unsigned char var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t *var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t *var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t *var_36;
    uint64_t *_pre293_pre_phi;
    uint64_t *var_37;
    uint64_t local_sp_9;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t *var_40;
    uint64_t *var_55;
    uint64_t *var_56;
    uint64_t *var_57;
    uint32_t *var_58;
    uint32_t *var_59;
    uint64_t var_60;
    uint64_t *var_61;
    uint64_t var_62;
    uint64_t *var_63;
    uint64_t *var_64;
    uint64_t *var_65;
    uint64_t *var_66;
    uint64_t _pre_phi298;
    uint64_t local_sp_10;
    unsigned char **var_67;
    uint64_t var_140;
    uint64_t var_69;
    unsigned char var_70;
    uint64_t var_71;
    unsigned char **var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_135;
    uint64_t var_136;
    uint64_t var_137;
    uint64_t var_138;
    uint64_t var_139;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_87;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t var_80;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t local_sp_12;
    uint64_t var_88;
    bool var_89;
    uint64_t var_142;
    uint64_t var_143;
    uint64_t var_144;
    uint64_t local_sp_13;
    uint64_t var_145;
    uint64_t var_146;
    uint64_t var_147;
    uint64_t var_148;
    uint64_t var_149;
    uint64_t var_141;
    uint64_t rax_2;
    unsigned char **var_17;
    uint64_t *var_18;
    bool var_19;
    uint64_t var_20;
    uint64_t *var_21;
    uint64_t var_22;
    uint64_t *var_23;
    uint64_t var_41;
    uint64_t *var_42;
    uint64_t var_43;
    uint64_t *var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t *var_47;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = init_state_0x82fc();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_4 = var_0 + (-304L);
    var_5 = (uint64_t *)var_4;
    *var_5 = rdi;
    var_6 = (uint32_t *)(var_0 + (-308L));
    *var_6 = (uint32_t)rsi;
    var_7 = (uint64_t *)(var_0 + (-32L));
    *var_7 = 0UL;
    var_8 = (uint64_t *)(var_0 + (-64L));
    *var_8 = 0UL;
    var_9 = var_0 + (-152L);
    var_10 = (uint64_t *)var_9;
    *var_10 = 0UL;
    var_11 = *var_6 & (-4);
    *(uint32_t *)(var_0 + (-92L)) = var_11;
    var_12 = var_11 >> 2U;
    var_13 = (unsigned char *)(var_0 + (-93L));
    *var_13 = ((unsigned char)var_12 & '\x01');
    var_14 = *var_6 & 3U;
    *var_6 = var_14;
    var_15 = (var_14 + (-1)) & var_14;
    var_16 = (uint64_t)var_15;
    rax_1 = 0UL;
    var_98 = 0U;
    rcx_0 = 18446744073709551615UL;
    rcx_1 = 0UL;
    rax_2 = 0UL;
    if (var_15 != 0U) {
        *(uint64_t *)(var_0 + (-320L)) = 4208457UL;
        indirect_placeholder_1();
        *(uint32_t *)var_16 = 22U;
        return rax_2;
    }
    if (*var_5 == 0UL) {
        *(uint64_t *)(var_0 + (-320L)) = 4208488UL;
        indirect_placeholder_1();
        *(uint32_t *)var_16 = 22U;
    } else {
        var_17 = (unsigned char **)var_4;
        if (**var_17 == '\x00') {
            *(uint64_t *)(var_0 + (-320L)) = 4208523UL;
            indirect_placeholder_1();
            *(volatile uint32_t *)(uint32_t *)0UL = 2U;
        } else {
            var_18 = (uint64_t *)(var_0 + (-80L));
            *var_18 = 0UL;
            var_19 = (**var_17 == '/');
            var_20 = var_0 + (-320L);
            var_21 = (uint64_t *)var_20;
            local_sp_8 = var_20;
            local_sp_9 = var_20;
            if (var_19) {
                *var_21 = 4208761UL;
                var_41 = indirect_placeholder_4(4096UL);
                var_42 = (uint64_t *)(var_0 + (-16L));
                *var_42 = var_41;
                var_43 = var_41 + 4096UL;
                var_44 = (uint64_t *)(var_0 + (-56L));
                *var_44 = var_43;
                var_45 = *var_42;
                var_46 = var_0 + (-24L);
                var_47 = (uint64_t *)var_46;
                *var_47 = var_45;
                _pre_phi290 = var_47;
                _pre_phi294 = var_44;
                _pre_phi289 = var_46;
                var_50 = var_45;
                _pre_phi296 = var_42;
                if (*var_18 != 0UL) {
                    var_48 = var_0 + (-328L);
                    *(uint64_t *)var_48 = 4208820UL;
                    indirect_placeholder_1();
                    var_49 = *var_47 + *var_18;
                    *var_47 = var_49;
                    var_50 = var_49;
                    local_sp_8 = var_48;
                }
                *var_47 = (var_50 + 1UL);
                *(unsigned char *)var_50 = (unsigned char)'/';
                var_51 = *var_18 + *var_5;
                var_52 = var_0 + (-40L);
                var_53 = (uint64_t *)var_52;
                *var_53 = var_51;
                _pre_phi288 = var_53;
                _pre_phi = var_52;
                local_sp_10_ph = local_sp_8;
            } else {
                *var_21 = 4208570UL;
                var_22 = indirect_placeholder_12();
                var_23 = (uint64_t *)(var_0 + (-16L));
                *var_23 = var_22;
                rdi1_0 = var_22;
                _pre_phi296 = var_23;
                if (var_22 == 0UL) {
                    return rax_2;
                }
                var_24 = (uint64_t)var_3;
                while (rcx_0 != 0UL)
                    {
                        var_25 = *(unsigned char *)rdi1_0;
                        var_26 = rcx_0 + (-1L);
                        rcx_0 = var_26;
                        rcx_1 = var_26;
                        if (var_25 == '\x00') {
                            break;
                        }
                        rdi1_0 = rdi1_0 + var_24;
                    }
                var_27 = var_22 + (18446744073709551614UL - (-2L));
                var_28 = var_0 + (-24L);
                var_29 = (uint64_t *)var_28;
                *var_29 = var_27;
                var_30 = *var_23;
                _pre_phi290 = var_29;
                _pre_phi289 = var_28;
                if ((long)(var_27 - var_30) > (long)4095UL) {
                    var_37 = (uint64_t *)(var_0 + (-56L));
                    *var_37 = var_27;
                    _pre293_pre_phi = var_37;
                } else {
                    var_31 = var_0 + (-328L);
                    *(uint64_t *)var_31 = 4208669UL;
                    var_32 = indirect_placeholder(var_30, 4096UL);
                    var_33 = (uint64_t *)(var_0 + (-104L));
                    *var_33 = var_32;
                    *var_29 = (var_32 + (*var_29 - *var_23));
                    var_34 = *var_33;
                    *var_23 = var_34;
                    var_35 = var_34 + 4096UL;
                    var_36 = (uint64_t *)(var_0 + (-56L));
                    *var_36 = var_35;
                    _pre293_pre_phi = var_36;
                    local_sp_9 = var_31;
                }
                var_38 = *var_5;
                var_39 = var_0 + (-40L);
                var_40 = (uint64_t *)var_39;
                *var_40 = var_38;
                *var_18 = 0UL;
                _pre_phi294 = _pre293_pre_phi;
                _pre_phi288 = var_40;
                _pre_phi = var_39;
                local_sp_10_ph = local_sp_9;
            }
            var_54 = var_0 + (-48L);
            var_55 = (uint64_t *)var_54;
            var_56 = (uint64_t *)(var_0 + (-112L));
            var_57 = (uint64_t *)(var_0 + (-88L));
            var_58 = (uint32_t *)(var_0 + (-272L));
            var_59 = (uint32_t *)(var_0 + (-68L));
            var_60 = var_0 + (-296L);
            var_61 = (uint64_t *)(var_0 + (-248L));
            var_62 = var_0 + (-120L);
            var_63 = (uint64_t *)var_62;
            var_64 = (uint64_t *)(var_0 + (-128L));
            var_65 = (uint64_t *)(var_0 + (-136L));
            var_66 = (uint64_t *)(var_0 + (-144L));
            local_sp_10 = local_sp_10_ph;
            while (1U)
                {
                    var_67 = (unsigned char **)_pre_phi;
                    var_68 = **var_67;
                    var_70 = var_68;
                    local_sp_11 = local_sp_10;
                    local_sp_12 = local_sp_10;
                    local_sp_13 = local_sp_10;
                    if (var_68 != '\x00') {
                        loop_state_var = 1U;
                        break;
                    }
                    var_69 = *_pre_phi288;
                    var_71 = var_69;
                    while (var_70 != '/')
                        {
                            var_141 = var_69 + 1UL;
                            *_pre_phi288 = var_141;
                            var_69 = var_141;
                            var_70 = **var_67;
                            var_71 = var_69;
                        }
                    *var_55 = var_69;
                    while (1U)
                        {
                            switch_state_var = 1;
                            break;
                        }
                    var_73 = *_pre_phi288;
                    if (var_71 != var_73) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_74 = var_73 + 1UL;
                    if (var_71 != var_74) {
                        if (**var_67 != '.') {
                            *_pre_phi288 = *var_55;
                            local_sp_10 = local_sp_11;
                            continue;
                        }
                    }
                    var_75 = var_71 - var_73;
                    _pre_phi298 = var_75;
                    if (var_75 != 2UL) {
                        if (**var_67 != '.' & *(unsigned char *)var_74 != '.') {
                            var_135 = *_pre_phi296 + (*var_18 + 1UL);
                            var_136 = *_pre_phi290;
                            if (var_136 <= var_135) {
                                var_137 = var_136 + (-1L);
                                *_pre_phi290 = var_137;
                                var_138 = var_137;
                                while (var_138 <= *_pre_phi296)
                                    {
                                        var_139 = var_138 + (-1L);
                                        var_138 = var_139;
                                        if (*(unsigned char *)var_139 == '/') {
                                            break;
                                        }
                                        *_pre_phi290 = var_139;
                                    }
                            }
                            *_pre_phi288 = *var_55;
                            local_sp_10 = local_sp_11;
                            continue;
                        }
                    }
                    var_76 = *_pre_phi290;
                    var_77 = var_76;
                    if (*(unsigned char *)(var_76 + (-1L)) == '/') {
                        *_pre_phi290 = (var_76 + 1UL);
                        *(unsigned char *)var_76 = (unsigned char)'/';
                        _pre_phi298 = *var_55 - *_pre_phi288;
                        var_77 = *_pre_phi290;
                    }
                    var_87 = var_77;
                    if (*_pre_phi294 <= (var_77 + _pre_phi298)) {
                        *var_56 = (var_77 - *_pre_phi296);
                        var_78 = *_pre_phi294 - *_pre_phi296;
                        *var_57 = var_78;
                        var_79 = *var_55 - *_pre_phi288;
                        if ((long)var_79 > (long)4095UL) {
                            var_81 = (var_78 + var_79) + 1UL;
                            *var_57 = var_81;
                            var_82 = var_81;
                        } else {
                            var_80 = var_78 + 4096UL;
                            *var_57 = var_80;
                            var_82 = var_80;
                        }
                        var_83 = *_pre_phi296;
                        var_84 = local_sp_10 + (-8L);
                        *(uint64_t *)var_84 = 4209226UL;
                        var_85 = indirect_placeholder(var_83, var_82);
                        *_pre_phi296 = var_85;
                        *_pre_phi294 = (*var_57 + var_85);
                        var_86 = *_pre_phi296 + *var_56;
                        *_pre_phi290 = var_86;
                        var_87 = var_86;
                        local_sp_12 = var_84;
                    }
                    var_88 = local_sp_12 + (-8L);
                    *(uint64_t *)var_88 = 4209290UL;
                    indirect_placeholder_1();
                    *_pre_phi290 = var_87;
                    *_pre_phi290 = (var_87 + (*var_55 - *_pre_phi288));
                    **(unsigned char **)_pre_phi289 = (unsigned char)'\x00';
                    var_89 = (*var_13 == '\x00');
                    local_sp_6 = var_88;
                    if (var_89) {
                        var_90 = *_pre_phi296;
                        var_91 = local_sp_12 + (-16L);
                        var_92 = (uint64_t *)var_91;
                        local_sp_6 = var_91;
                        if (var_89) {
                            *var_92 = 4209400UL;
                            var_94 = indirect_placeholder_4(var_90);
                            storemerge = (var_94 & (-256L)) | ((uint64_t)(uint32_t)var_94 != 0UL);
                        } else {
                            *var_92 = 4209371UL;
                            var_93 = indirect_placeholder_4(var_90);
                            storemerge = (var_93 & (-256L)) | ((uint64_t)(uint32_t)var_93 != 0UL);
                        }
                        if ((uint64_t)(unsigned char)storemerge == 0UL) {
                            var_95 = var_91 + (-8L);
                            *(uint64_t *)var_95 = 4209414UL;
                            indirect_placeholder_1();
                            *var_59 = *(uint32_t *)storemerge;
                            local_sp_6 = var_95;
                            local_sp_7 = var_95;
                            switch_state_var = 0;
                            switch (*var_6) {
                              case 1U:
                                {
                                    var_96 = *var_55;
                                    var_97 = var_91 + (-16L);
                                    *(uint64_t *)var_97 = 4209458UL;
                                    indirect_placeholder_1();
                                    local_sp_7 = var_97;
                                    local_sp_11 = var_97;
                                    if (*(unsigned char *)(var_96 + *var_55) != '\x00') {
                                        loop_state_var = 2U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    if (*var_59 != 2U) {
                                        loop_state_var = 2U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    *_pre_phi288 = *var_55;
                                    local_sp_10 = local_sp_11;
                                    continue;
                                }
                                break;
                              case 0U:
                                {
                                    loop_state_var = 2U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              default:
                                {
                                    *var_58 = 0U;
                                    local_sp_7 = local_sp_6;
                                    local_sp_11 = local_sp_6;
                                    switch_state_var = 0;
                                    switch ((uint32_t)((uint16_t)var_98 & (unsigned short)61440U)) {
                                      case 16384U:
                                        {
                                            *_pre_phi288 = *var_55;
                                            local_sp_10 = local_sp_11;
                                            continue;
                                        }
                                        break;
                                      case 40960U:
                                        {
                                            var_99 = *var_5;
                                            var_100 = local_sp_6 + (-8L);
                                            *(uint64_t *)var_100 = 4209555UL;
                                            var_101 = indirect_placeholder_5(var_60, var_9, var_99);
                                            local_sp_7 = var_100;
                                            local_sp_11 = var_100;
                                            if ((uint64_t)(unsigned char)var_101 != 0UL) {
                                                if (*var_6 != 2U) {
                                                    *var_59 = 40U;
                                                    loop_state_var = 2U;
                                                    switch_state_var = 1;
                                                    break;
                                                }
                                            }
                                            var_102 = *var_61;
                                            var_103 = *_pre_phi296;
                                            var_104 = local_sp_6 + (-16L);
                                            *(uint64_t *)var_104 = 4209609UL;
                                            var_105 = indirect_placeholder(var_103, var_102);
                                            *var_63 = var_105;
                                            local_sp_4 = var_104;
                                            if (var_105 == 0UL) {
                                                if (*var_6 != 2U) {
                                                    loop_state_var = 0U;
                                                    switch_state_var = 1;
                                                    break;
                                                }
                                                var_130 = local_sp_6 + (-24L);
                                                *(uint64_t *)var_130 = 4209634UL;
                                                indirect_placeholder_1();
                                                rax_1 = 12UL;
                                                local_sp_4 = var_130;
                                                local_sp_11 = var_130;
                                                if (*(volatile uint32_t *)(uint32_t *)0UL != 12U) {
                                                    loop_state_var = 0U;
                                                    switch_state_var = 1;
                                                    break;
                                                }
                                            }
                                            *(uint64_t *)(local_sp_6 + (-24L)) = 4209672UL;
                                            indirect_placeholder_1();
                                            *var_64 = var_105;
                                            var_106 = *var_55;
                                            var_107 = local_sp_6 + (-32L);
                                            *(uint64_t *)var_107 = 4209688UL;
                                            indirect_placeholder_1();
                                            *var_65 = var_106;
                                            var_108 = *var_8;
                                            var_109 = (var_108 == 0UL);
                                            var_110 = var_106 + *var_64;
                                            var_111 = var_110 + 1UL;
                                            local_sp_3 = var_107;
                                            if (var_109) {
                                                var_117 = helper_cc_compute_c_wrapper(var_110 + (-4095L), 4096UL, var_2, 17U);
                                                var_118 = (var_117 == 0UL) ? var_111 : 4096UL;
                                                *var_8 = var_118;
                                                var_119 = local_sp_6 + (-40L);
                                                *(uint64_t *)var_119 = 4209745UL;
                                                var_120 = indirect_placeholder_4(var_118);
                                                *var_7 = var_120;
                                                local_sp_3 = var_119;
                                            } else {
                                                var_112 = helper_cc_compute_c_wrapper(var_108 - var_111, var_111, var_2, 17U);
                                                if (var_112 == 0UL) {
                                                    var_113 = (*var_65 + *var_64) + 1UL;
                                                    *var_8 = var_113;
                                                    var_114 = *var_7;
                                                    var_115 = local_sp_6 + (-40L);
                                                    *(uint64_t *)var_115 = 4209810UL;
                                                    var_116 = indirect_placeholder(var_114, var_113);
                                                    *var_7 = var_116;
                                                    local_sp_3 = var_115;
                                                }
                                            }
                                            *(uint64_t *)(local_sp_3 + (-8L)) = 4209848UL;
                                            indirect_placeholder_1();
                                            var_121 = *var_7;
                                            var_122 = local_sp_3 + (-16L);
                                            *(uint64_t *)var_122 = 4209871UL;
                                            indirect_placeholder_1();
                                            *var_55 = var_121;
                                            *var_5 = var_121;
                                            if (**(unsigned char **)var_62 != '/') {
                                                var_123 = *_pre_phi296 + (*var_18 + 1UL);
                                                var_124 = *_pre_phi290;
                                                if (var_124 <= var_123) {
                                                    var_125 = var_124 + (-1L);
                                                    *_pre_phi290 = var_125;
                                                    var_126 = var_125;
                                                    while (var_126 <= *_pre_phi296)
                                                        {
                                                            var_127 = var_126 + (-1L);
                                                            var_126 = var_127;
                                                            if (*(unsigned char *)var_127 == '/') {
                                                                break;
                                                            }
                                                            *_pre_phi290 = var_127;
                                                        }
                                                }
                                            }
                                            *var_66 = 0UL;
                                            var_128 = *_pre_phi296;
                                            *_pre_phi290 = (var_128 + 1UL);
                                            *(unsigned char *)var_128 = (unsigned char)'/';
                                            *var_18 = *var_66;
                                            var_129 = var_122 + (-8L);
                                            *(uint64_t *)var_129 = 4210060UL;
                                            indirect_placeholder_1();
                                            local_sp_11 = var_129;
                                        }
                                        break;
                                      default:
                                        {
                                            if (**var_72 != '\x00' & *var_6 != 2U) {
                                                *var_59 = 20U;
                                                loop_state_var = 2U;
                                                switch_state_var = 1;
                                                break;
                                            }
                                        }
                                        break;
                                    }
                                    if (switch_state_var)
                                        break;
                                }
                                break;
                            }
                            if (switch_state_var)
                                break;
                        } else {
                            _pre287 = *var_58;
                            var_98 = _pre287;
                            local_sp_7 = local_sp_6;
                            local_sp_11 = local_sp_6;
                            switch_state_var = 0;
                            switch ((uint32_t)((uint16_t)var_98 & (unsigned short)61440U)) {
                              case 16384U:
                                {
                                    *_pre_phi288 = *var_55;
                                    local_sp_10 = local_sp_11;
                                    continue;
                                }
                                break;
                              case 40960U:
                                {
                                    var_99 = *var_5;
                                    var_100 = local_sp_6 + (-8L);
                                    *(uint64_t *)var_100 = 4209555UL;
                                    var_101 = indirect_placeholder_5(var_60, var_9, var_99);
                                    local_sp_7 = var_100;
                                    local_sp_11 = var_100;
                                    if ((uint64_t)(unsigned char)var_101 != 0UL) {
                                        if (*var_6 != 2U) {
                                            *var_59 = 40U;
                                            loop_state_var = 2U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                    }
                                    var_102 = *var_61;
                                    var_103 = *_pre_phi296;
                                    var_104 = local_sp_6 + (-16L);
                                    *(uint64_t *)var_104 = 4209609UL;
                                    var_105 = indirect_placeholder(var_103, var_102);
                                    *var_63 = var_105;
                                    local_sp_4 = var_104;
                                    if (var_105 == 0UL) {
                                        if (*var_6 != 2U) {
                                            loop_state_var = 0U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                        var_130 = local_sp_6 + (-24L);
                                        *(uint64_t *)var_130 = 4209634UL;
                                        indirect_placeholder_1();
                                        rax_1 = 12UL;
                                        local_sp_4 = var_130;
                                        local_sp_11 = var_130;
                                        if (*(volatile uint32_t *)(uint32_t *)0UL != 12U) {
                                            loop_state_var = 0U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                    }
                                    *(uint64_t *)(local_sp_6 + (-24L)) = 4209672UL;
                                    indirect_placeholder_1();
                                    *var_64 = var_105;
                                    var_106 = *var_55;
                                    var_107 = local_sp_6 + (-32L);
                                    *(uint64_t *)var_107 = 4209688UL;
                                    indirect_placeholder_1();
                                    *var_65 = var_106;
                                    var_108 = *var_8;
                                    var_109 = (var_108 == 0UL);
                                    var_110 = var_106 + *var_64;
                                    var_111 = var_110 + 1UL;
                                    local_sp_3 = var_107;
                                    if (var_109) {
                                        var_117 = helper_cc_compute_c_wrapper(var_110 + (-4095L), 4096UL, var_2, 17U);
                                        var_118 = (var_117 == 0UL) ? var_111 : 4096UL;
                                        *var_8 = var_118;
                                        var_119 = local_sp_6 + (-40L);
                                        *(uint64_t *)var_119 = 4209745UL;
                                        var_120 = indirect_placeholder_4(var_118);
                                        *var_7 = var_120;
                                        local_sp_3 = var_119;
                                    } else {
                                        var_112 = helper_cc_compute_c_wrapper(var_108 - var_111, var_111, var_2, 17U);
                                        if (var_112 == 0UL) {
                                            var_113 = (*var_65 + *var_64) + 1UL;
                                            *var_8 = var_113;
                                            var_114 = *var_7;
                                            var_115 = local_sp_6 + (-40L);
                                            *(uint64_t *)var_115 = 4209810UL;
                                            var_116 = indirect_placeholder(var_114, var_113);
                                            *var_7 = var_116;
                                            local_sp_3 = var_115;
                                        }
                                    }
                                    *(uint64_t *)(local_sp_3 + (-8L)) = 4209848UL;
                                    indirect_placeholder_1();
                                    var_121 = *var_7;
                                    var_122 = local_sp_3 + (-16L);
                                    *(uint64_t *)var_122 = 4209871UL;
                                    indirect_placeholder_1();
                                    *var_55 = var_121;
                                    *var_5 = var_121;
                                    if (**(unsigned char **)var_62 != '/') {
                                        var_123 = *_pre_phi296 + (*var_18 + 1UL);
                                        var_124 = *_pre_phi290;
                                        if (var_124 <= var_123) {
                                            var_125 = var_124 + (-1L);
                                            *_pre_phi290 = var_125;
                                            var_126 = var_125;
                                            while (var_126 <= *_pre_phi296)
                                                {
                                                    var_127 = var_126 + (-1L);
                                                    var_126 = var_127;
                                                    if (*(unsigned char *)var_127 == '/') {
                                                        break;
                                                    }
                                                    *_pre_phi290 = var_127;
                                                }
                                        }
                                    }
                                    *var_66 = 0UL;
                                    var_128 = *_pre_phi296;
                                    *_pre_phi290 = (var_128 + 1UL);
                                    *(unsigned char *)var_128 = (unsigned char)'/';
                                    *var_18 = *var_66;
                                    var_129 = var_122 + (-8L);
                                    *(uint64_t *)var_129 = 4210060UL;
                                    indirect_placeholder_1();
                                    local_sp_11 = var_129;
                                }
                                break;
                              default:
                                {
                                    if (**var_72 != '\x00' & *var_6 != 2U) {
                                        *var_59 = 20U;
                                        loop_state_var = 2U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                }
                                break;
                            }
                            if (switch_state_var)
                                break;
                        }
                    } else {
                        if (*var_6 != 2U) {
                            *var_58 = 0U;
                            local_sp_7 = local_sp_6;
                            local_sp_11 = local_sp_6;
                            switch_state_var = 0;
                            switch ((uint32_t)((uint16_t)var_98 & (unsigned short)61440U)) {
                              case 16384U:
                                {
                                    *_pre_phi288 = *var_55;
                                    local_sp_10 = local_sp_11;
                                    continue;
                                }
                                break;
                              case 40960U:
                                {
                                    var_99 = *var_5;
                                    var_100 = local_sp_6 + (-8L);
                                    *(uint64_t *)var_100 = 4209555UL;
                                    var_101 = indirect_placeholder_5(var_60, var_9, var_99);
                                    local_sp_7 = var_100;
                                    local_sp_11 = var_100;
                                    if ((uint64_t)(unsigned char)var_101 != 0UL) {
                                        if (*var_6 != 2U) {
                                            *var_59 = 40U;
                                            loop_state_var = 2U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                    }
                                    var_102 = *var_61;
                                    var_103 = *_pre_phi296;
                                    var_104 = local_sp_6 + (-16L);
                                    *(uint64_t *)var_104 = 4209609UL;
                                    var_105 = indirect_placeholder(var_103, var_102);
                                    *var_63 = var_105;
                                    local_sp_4 = var_104;
                                    if (var_105 == 0UL) {
                                        if (*var_6 != 2U) {
                                            loop_state_var = 0U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                        var_130 = local_sp_6 + (-24L);
                                        *(uint64_t *)var_130 = 4209634UL;
                                        indirect_placeholder_1();
                                        rax_1 = 12UL;
                                        local_sp_4 = var_130;
                                        local_sp_11 = var_130;
                                        if (*(volatile uint32_t *)(uint32_t *)0UL != 12U) {
                                            loop_state_var = 0U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                    }
                                    *(uint64_t *)(local_sp_6 + (-24L)) = 4209672UL;
                                    indirect_placeholder_1();
                                    *var_64 = var_105;
                                    var_106 = *var_55;
                                    var_107 = local_sp_6 + (-32L);
                                    *(uint64_t *)var_107 = 4209688UL;
                                    indirect_placeholder_1();
                                    *var_65 = var_106;
                                    var_108 = *var_8;
                                    var_109 = (var_108 == 0UL);
                                    var_110 = var_106 + *var_64;
                                    var_111 = var_110 + 1UL;
                                    local_sp_3 = var_107;
                                    if (var_109) {
                                        var_117 = helper_cc_compute_c_wrapper(var_110 + (-4095L), 4096UL, var_2, 17U);
                                        var_118 = (var_117 == 0UL) ? var_111 : 4096UL;
                                        *var_8 = var_118;
                                        var_119 = local_sp_6 + (-40L);
                                        *(uint64_t *)var_119 = 4209745UL;
                                        var_120 = indirect_placeholder_4(var_118);
                                        *var_7 = var_120;
                                        local_sp_3 = var_119;
                                    } else {
                                        var_112 = helper_cc_compute_c_wrapper(var_108 - var_111, var_111, var_2, 17U);
                                        if (var_112 == 0UL) {
                                            var_113 = (*var_65 + *var_64) + 1UL;
                                            *var_8 = var_113;
                                            var_114 = *var_7;
                                            var_115 = local_sp_6 + (-40L);
                                            *(uint64_t *)var_115 = 4209810UL;
                                            var_116 = indirect_placeholder(var_114, var_113);
                                            *var_7 = var_116;
                                            local_sp_3 = var_115;
                                        }
                                    }
                                    *(uint64_t *)(local_sp_3 + (-8L)) = 4209848UL;
                                    indirect_placeholder_1();
                                    var_121 = *var_7;
                                    var_122 = local_sp_3 + (-16L);
                                    *(uint64_t *)var_122 = 4209871UL;
                                    indirect_placeholder_1();
                                    *var_55 = var_121;
                                    *var_5 = var_121;
                                    if (**(unsigned char **)var_62 != '/') {
                                        var_123 = *_pre_phi296 + (*var_18 + 1UL);
                                        var_124 = *_pre_phi290;
                                        if (var_124 <= var_123) {
                                            var_125 = var_124 + (-1L);
                                            *_pre_phi290 = var_125;
                                            var_126 = var_125;
                                            while (var_126 <= *_pre_phi296)
                                                {
                                                    var_127 = var_126 + (-1L);
                                                    var_126 = var_127;
                                                    if (*(unsigned char *)var_127 == '/') {
                                                        break;
                                                    }
                                                    *_pre_phi290 = var_127;
                                                }
                                        }
                                    }
                                    *var_66 = 0UL;
                                    var_128 = *_pre_phi296;
                                    *_pre_phi290 = (var_128 + 1UL);
                                    *(unsigned char *)var_128 = (unsigned char)'/';
                                    *var_18 = *var_66;
                                    var_129 = var_122 + (-8L);
                                    *(uint64_t *)var_129 = 4210060UL;
                                    indirect_placeholder_1();
                                    local_sp_11 = var_129;
                                }
                                break;
                              default:
                                {
                                    if (**var_72 != '\x00' & *var_6 != 2U) {
                                        *var_59 = 20U;
                                        loop_state_var = 2U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                }
                                break;
                            }
                            if (switch_state_var)
                                break;
                        }
                    }
                }
            switch (loop_state_var) {
              case 1U:
                {
                    var_142 = *_pre_phi296 + (*var_18 + 1UL);
                    var_143 = *_pre_phi290;
                    var_144 = var_143 + (-1L);
                    if (var_143 <= var_142 & *(unsigned char *)var_144 == '/') {
                        *_pre_phi290 = var_144;
                    }
                    **(unsigned char **)_pre_phi289 = (unsigned char)'\x00';
                    var_145 = *_pre_phi290;
                    if (*_pre_phi294 == (var_145 + 1UL)) {
                        var_146 = *_pre_phi296;
                        var_147 = (var_145 - var_146) + 1UL;
                        var_148 = local_sp_10 + (-8L);
                        *(uint64_t *)var_148 = 4210240UL;
                        var_149 = indirect_placeholder(var_146, var_147);
                        *_pre_phi296 = var_149;
                        local_sp_13 = var_148;
                    }
                    *(uint64_t *)(local_sp_13 + (-8L)) = 4210256UL;
                    indirect_placeholder_1();
                    if (*var_10 != 0UL) {
                        *(uint64_t *)(local_sp_13 + (-16L)) = 4210283UL;
                        indirect_placeholder_1();
                    }
                    var_150 = *_pre_phi296;
                    rax_2 = var_150;
                }
                break;
              case 2U:
              case 0U:
                {
                    switch (loop_state_var) {
                      case 0U:
                        {
                            var_131 = local_sp_4 + (-8L);
                            *(uint64_t *)var_131 = 4209650UL;
                            indirect_placeholder_1();
                            *var_59 = *(uint32_t *)rax_1;
                            local_sp_7 = var_131;
                        }
                        break;
                      case 2U:
                        {
                            *(uint64_t *)(local_sp_7 + (-8L)) = 4210305UL;
                            indirect_placeholder_1();
                            var_132 = local_sp_7 + (-16L);
                            *(uint64_t *)var_132 = 4210317UL;
                            indirect_placeholder_1();
                            var_133 = *var_10;
                            local_sp_0 = var_132;
                            if (var_133 != 0UL) {
                                var_134 = local_sp_7 + (-24L);
                                *(uint64_t *)var_134 = 4210344UL;
                                indirect_placeholder_1();
                                local_sp_0 = var_134;
                            }
                            *(uint64_t *)(local_sp_0 + (-8L)) = 4210349UL;
                            indirect_placeholder_1();
                            *(uint32_t *)var_133 = *var_59;
                        }
                        break;
                    }
                }
                break;
            }
        }
    }
}
