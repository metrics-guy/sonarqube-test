typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_1(void);
extern uint64_t init_rax(void);
typedef _Bool bool;
uint64_t bb_getugroups(uint64_t rcx, uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t rax_1;
    uint64_t var_32;
    uint32_t var_33;
    uint64_t local_sp_2;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_34;
    uint32_t var_35;
    uint64_t var_36;
    uint64_t rax_2;
    uint32_t *var_16;
    uint64_t var_17;
    uint32_t var_10;
    uint64_t var_9;
    uint64_t rax_3;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t *var_14;
    uint32_t *var_15;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint32_t *var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint32_t *var_6;
    uint32_t *var_7;
    uint32_t var_8;
    uint64_t rax_4;
    uint64_t local_sp_1;
    uint32_t var_20;
    uint64_t var_21;
    uint32_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t local_sp_0;
    uint64_t var_26;
    uint32_t var_27;
    uint32_t var_31;
    uint64_t var_28;
    uint32_t var_29;
    uint64_t var_30;
    uint64_t rax_0;
    uint32_t var_37;
    uint64_t var_38;
    uint32_t *var_39;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    var_3 = (uint32_t *)(var_0 + (-60L));
    *var_3 = (uint32_t)rdi;
    var_4 = (uint64_t *)(var_0 + (-72L));
    *var_4 = rsi;
    var_5 = (uint64_t *)(var_0 + (-80L));
    *var_5 = rdx;
    var_6 = (uint32_t *)(var_0 + (-64L));
    *var_6 = (uint32_t)rcx;
    var_7 = (uint32_t *)(var_0 + (-12L));
    *var_7 = 0U;
    var_8 = *var_6;
    var_20 = 0U;
    var_10 = 0U;
    rax_3 = var_1;
    rax_4 = var_1;
    if (var_8 != 4294967295U) {
        if (*var_3 != 0U) {
            var_9 = (uint64_t)var_8;
            *(uint32_t *)*var_4 = var_8;
            var_10 = *var_7;
            rax_3 = var_9;
        }
        *var_7 = (var_10 + 1U);
        rax_4 = rax_3;
    }
    var_11 = var_0 + (-96L);
    *(uint64_t *)var_11 = 4278617UL;
    indirect_placeholder_1();
    var_12 = (uint64_t *)(var_0 + (-40L));
    var_13 = var_0 + (-24L);
    var_14 = (uint64_t *)var_13;
    var_15 = (uint32_t *)(var_0 + (-28L));
    rax_2 = rax_4;
    local_sp_2 = var_11;
    while (1U)
        {
            *(uint64_t *)(local_sp_2 + (-8L)) = 4278622UL;
            indirect_placeholder_1();
            var_16 = (uint32_t *)rax_2;
            *var_16 = 0U;
            var_17 = local_sp_2 + (-16L);
            *(uint64_t *)var_17 = 4278633UL;
            indirect_placeholder_1();
            *var_12 = rax_2;
            local_sp_1 = var_17;
            rax_2 = 0UL;
            if (rax_2 != 0UL) {
                var_34 = local_sp_2 + (-24L);
                *(uint64_t *)var_34 = 4278871UL;
                indirect_placeholder_1();
                var_35 = *var_16;
                var_36 = (uint64_t)var_35;
                rax_0 = var_36;
                local_sp_0 = var_34;
                if (var_35 == 0U) {
                    break;
                }
                *var_7 = 4294967295U;
                break;
            }
            *var_14 = *(uint64_t *)(rax_2 + 24UL);
            while (1U)
                {
                    local_sp_2 = local_sp_1;
                    if (**(uint64_t **)var_13 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_18 = *var_5;
                    var_19 = local_sp_1 + (-8L);
                    *(uint64_t *)var_19 = 4278687UL;
                    indirect_placeholder_1();
                    local_sp_0 = var_19;
                    local_sp_1 = var_19;
                    if ((uint64_t)(uint32_t)var_18 != 0UL) {
                        *var_15 = 0U;
                        var_21 = (uint64_t)var_20;
                        var_22 = *var_7;
                        var_23 = (uint64_t)var_22;
                        var_24 = var_21 << 32UL;
                        var_25 = var_23 << 32UL;
                        rax_0 = var_23;
                        var_31 = var_22;
                        rax_1 = var_21;
                        while ((long)var_24 >= (long)var_25)
                            {
                                var_26 = *var_4;
                                if (var_26 != 0UL) {
                                    if ((uint64_t)(*(uint32_t *)(var_26 + ((uint64_t)var_20 << 2UL)) - *(uint32_t *)(*var_12 + 16UL)) == 0UL) {
                                        break;
                                    }
                                }
                                var_33 = var_20 + 1U;
                                *var_15 = var_33;
                                var_20 = var_33;
                                var_21 = (uint64_t)var_20;
                                var_22 = *var_7;
                                var_23 = (uint64_t)var_22;
                                var_24 = var_21 << 32UL;
                                var_25 = var_23 << 32UL;
                                rax_0 = var_23;
                                var_31 = var_22;
                                rax_1 = var_21;
                            }
                        if ((uint64_t)(var_20 - var_22) != 0UL) {
                            var_27 = *var_3;
                            if (var_27 != 0U) {
                                if ((long)var_25 >= (long)((uint64_t)var_27 << 32UL)) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                var_28 = ((uint64_t)var_22 << 2UL) + *var_4;
                                var_29 = *(uint32_t *)(*var_12 + 16UL);
                                var_30 = (uint64_t)var_29;
                                *(uint32_t *)var_28 = var_29;
                                var_31 = *var_7;
                                rax_1 = var_30;
                            }
                            rax_0 = rax_1;
                            if (var_31 != 2147483647U) {
                                var_32 = local_sp_1 + (-16L);
                                *(uint64_t *)var_32 = 4278824UL;
                                indirect_placeholder_1();
                                *(uint32_t *)rax_1 = 75U;
                                local_sp_0 = var_32;
                                loop_state_var = 0U;
                                break;
                            }
                            *var_7 = (var_31 + 1U);
                        }
                    }
                    *var_14 = (*var_14 + 8UL);
                    continue;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 1U:
                {
                    continue;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    *(uint64_t *)(local_sp_0 + (-8L)) = 4278895UL;
    indirect_placeholder_1();
    var_37 = *(uint32_t *)rax_0;
    var_38 = (uint64_t)var_37;
    var_39 = (uint32_t *)(var_0 + (-44L));
    *var_39 = var_37;
    *(uint64_t *)(local_sp_0 + (-16L)) = 4278905UL;
    indirect_placeholder_1();
    *(uint64_t *)(local_sp_0 + (-24L)) = 4278910UL;
    indirect_placeholder_1();
    *(uint32_t *)var_38 = *var_39;
    return (uint64_t)*var_7;
}
