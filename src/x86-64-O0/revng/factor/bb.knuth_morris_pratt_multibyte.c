typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_divq_EAX_wrapper_ret_type;
struct type_5;
struct indirect_placeholder_72_ret_type;
struct helper_divq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint32_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint32_t field_6;
    uint64_t field_7;
    uint32_t field_8;
    uint32_t field_9;
    unsigned char field_10;
    uint32_t field_11;
    uint32_t field_12;
};
struct type_5 {
};
struct indirect_placeholder_72_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_r10(void);
extern uint64_t init_rbx(void);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern uint32_t init_state_0x8248(void);
extern struct helper_divq_EAX_wrapper_ret_type helper_divq_EAX_wrapper(struct type_5 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint64_t param_13, uint32_t param_14, uint64_t param_15, uint64_t param_16, uint64_t param_17, uint32_t param_18, uint32_t param_19);
extern void indirect_placeholder_13(uint64_t param_0, uint64_t param_1);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern struct indirect_placeholder_72_ret_type indirect_placeholder_72(uint64_t param_0);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_44(uint64_t param_0);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
uint64_t bb_knuth_morris_pratt_multibyte(uint64_t rdx, uint64_t rsi, uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint32_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint32_t var_12;
    uint32_t var_13;
    uint64_t var_14;
    uint64_t *var_15;
    uint64_t *var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    struct indirect_placeholder_72_ret_type var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t *var_23;
    unsigned __int128 var_24;
    uint64_t var_25;
    uint64_t var_26;
    bool var_27;
    uint64_t spec_select;
    uint64_t rax_0;
    uint64_t local_sp_5;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_95;
    uint64_t local_sp_0;
    uint64_t var_96;
    uint64_t var_90;
    uint64_t local_sp_7_be;
    uint64_t var_92;
    uint64_t local_sp_1;
    uint64_t var_93;
    uint64_t var_88;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t local_sp_4180;
    uint64_t _pre199;
    uint64_t var_97;
    uint64_t storemerge12;
    uint64_t local_sp_4176;
    uint64_t var_89;
    uint64_t local_sp_6;
    uint64_t var_91;
    uint64_t var_94;
    uint64_t local_sp_7;
    uint64_t var_82;
    unsigned char var_83;
    uint64_t *var_33;
    uint64_t *var_51;
    uint64_t *var_52;
    uint64_t var_53;
    uint64_t *var_54;
    uint64_t local_sp_12184;
    uint64_t var_55;
    uint64_t local_sp_8;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t **var_71;
    uint64_t *var_72;
    uint64_t var_73;
    uint64_t *var_74;
    uint64_t var_75;
    unsigned char *var_76;
    uint64_t var_77;
    uint64_t *var_78;
    unsigned char *var_79;
    uint32_t *var_80;
    uint64_t *var_81;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t local_sp_9;
    uint64_t _pre193;
    uint64_t _pre_phi202;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_65;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_67;
    uint64_t _pre195;
    uint64_t local_sp_12189;
    uint64_t local_sp_12185;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_66;
    uint64_t var_70;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t local_sp_13;
    uint64_t var_48;
    uint64_t var_28;
    uint64_t var_30;
    struct helper_divq_EAX_wrapper_ret_type var_29;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t local_sp_14;
    uint64_t *var_34;
    uint64_t var_35;
    uint64_t *var_36;
    uint64_t *var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t *var_40;
    uint64_t var_41;
    unsigned char *var_42;
    uint64_t var_43;
    unsigned char *var_44;
    uint32_t *var_45;
    unsigned char *var_46;
    uint64_t *var_47;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_cc_src2();
    var_2 = init_rbp();
    var_3 = init_r10();
    var_4 = init_r9();
    var_5 = init_r8();
    var_6 = init_rbx();
    var_7 = init_state_0x9018();
    var_8 = init_state_0x9010();
    var_9 = init_state_0x8408();
    var_10 = init_state_0x8328();
    var_11 = init_state_0x82d8();
    var_12 = init_state_0x9080();
    var_13 = init_state_0x8248();
    var_14 = var_0 + (-8L);
    *(uint64_t *)var_14 = var_2;
    var_15 = (uint64_t *)(var_0 + (-240L));
    *var_15 = rdi;
    var_16 = (uint64_t *)(var_0 + (-248L));
    *var_16 = rsi;
    var_17 = var_0 + (-256L);
    *(uint64_t *)var_17 = rdx;
    var_18 = *var_16;
    var_19 = var_0 + (-272L);
    *(uint64_t *)var_19 = 4239066UL;
    var_20 = indirect_placeholder_72(var_18);
    var_21 = var_20.field_0;
    var_22 = var_20.field_1;
    var_23 = (uint64_t *)(var_0 + (-56L));
    *var_23 = var_21;
    var_24 = (unsigned __int128)var_21 * 56ULL;
    var_25 = (uint64_t)var_24;
    var_26 = helper_cc_compute_all_wrapper(var_25, (uint64_t)(var_24 >> 64ULL), 0UL, 5U);
    var_27 = ((uint64_t)((uint16_t)var_26 & (unsigned short)2048U) == 0UL);
    spec_select = var_27 ? 0UL : 1UL;
    var_55 = 2UL;
    rax_0 = 0UL;
    local_sp_14 = var_19;
    storemerge12 = 0UL;
    if (!(((long)var_25 > (long)18446744073709551615UL) && var_27)) {
        var_28 = *var_23 * 56UL;
        if (var_28 > 4015UL) {
            var_31 = var_0 + (-280L);
            *(uint64_t *)var_31 = 4239249UL;
            var_32 = indirect_placeholder_2(var_28);
            rax_0 = var_32;
            local_sp_14 = var_31;
        } else {
            var_29 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), 16UL, 4239192UL, var_28 + 39UL, var_14, spec_select, 0UL, 16UL, var_22, var_3, var_4, var_5, var_6, var_7, var_8, var_9, var_10, var_11, var_12, var_13);
            var_30 = var_19 - (var_29.field_1 << 4UL);
            rax_0 = (var_30 + 31UL) & (-16L);
            local_sp_14 = var_30;
        }
    }
    var_33 = (uint64_t *)(var_0 + (-64L));
    *var_33 = rax_0;
    if (rax_0 == 0UL) {
        return storemerge12;
    }
    var_34 = (uint64_t *)(var_0 + (-72L));
    *var_34 = rax_0;
    var_35 = rax_0 + (*var_23 * 48UL);
    *(uint64_t *)(var_0 + (-80L)) = var_35;
    var_36 = (uint64_t *)(var_0 + (-88L));
    *var_36 = var_35;
    var_37 = (uint64_t *)(var_0 + (-16L));
    *var_37 = 0UL;
    var_38 = *var_16;
    var_39 = var_0 + (-152L);
    var_40 = (uint64_t *)var_39;
    *var_40 = var_38;
    var_41 = var_0 + (-168L);
    var_42 = (unsigned char *)var_41;
    *var_42 = (unsigned char)'\x00';
    var_43 = local_sp_14 + (-8L);
    *(uint64_t *)var_43 = 4239382UL;
    indirect_placeholder();
    var_44 = (unsigned char *)(var_0 + (-156L));
    *var_44 = (unsigned char)'\x00';
    var_45 = (uint32_t *)(var_0 + (-132L));
    var_46 = (unsigned char *)(var_0 + (-136L));
    var_47 = (uint64_t *)(var_0 + (-144L));
    local_sp_13 = var_43;
    storemerge12 = 1UL;
    while (1U)
        {
            var_48 = local_sp_13 + (-8L);
            *(uint64_t *)var_48 = 4239491UL;
            indirect_placeholder_44(var_41);
            local_sp_8 = var_48;
            if (*var_46 != '\x01') {
                if (*var_45 == 0U) {
                    break;
                }
            }
            var_49 = *var_34 + (*var_37 * 48UL);
            var_50 = local_sp_13 + (-16L);
            *(uint64_t *)var_50 = 4239440UL;
            indirect_placeholder_13(var_39, var_49);
            *var_40 = (*var_47 + *var_40);
            *var_44 = (unsigned char)'\x00';
            *var_37 = (*var_37 + 1UL);
            local_sp_13 = var_50;
            continue;
        }
    *(uint64_t *)(*var_36 + 8UL) = 1UL;
    var_51 = (uint64_t *)(var_0 + (-32L));
    *var_51 = 0UL;
    var_52 = (uint64_t *)(var_0 + (-24L));
    *var_52 = 2UL;
    var_53 = var_0 + (-96L);
    var_54 = (uint64_t *)var_53;
    var_56 = *var_23;
    var_57 = helper_cc_compute_c_wrapper(var_55 - var_56, var_56, var_1, 17U);
    local_sp_9 = local_sp_8;
    while (var_57 != 0UL)
        {
            var_58 = *var_34 + ((*var_52 * 48UL) + (-48L));
            *var_54 = var_58;
            var_59 = var_58;
            while (1U)
                {
                    local_sp_12189 = local_sp_9;
                    local_sp_12185 = local_sp_9;
                    if (*(unsigned char *)(var_59 + 16UL) != '\x00') {
                        var_60 = *var_51;
                        var_61 = *var_34 + (var_60 * 48UL);
                        _pre_phi202 = var_61;
                        var_62 = var_60;
                        var_67 = var_60;
                        var_65 = var_60;
                        if (*(unsigned char *)(var_61 + 16UL) != '\x00') {
                            if ((uint64_t)(*(uint32_t *)(var_59 + 20UL) - *(uint32_t *)(var_61 + 20UL)) != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            local_sp_9 = local_sp_12185;
                            local_sp_12184 = local_sp_12185;
                            if (var_65 != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            *var_51 = (var_65 - *(uint64_t *)(*var_36 + (var_65 << 3UL)));
                            var_59 = *var_54;
                            continue;
                        }
                    }
                    _pre193 = *var_51;
                    _pre_phi202 = *var_34 + (_pre193 * 48UL);
                    var_62 = _pre193;
                }
            switch (loop_state_var) {
              case 1U:
                {
                    var_66 = *var_52;
                    *(uint64_t *)((var_66 << 3UL) + *var_36) = var_66;
                }
                break;
              case 2U:
              case 0U:
                {
                    switch (loop_state_var) {
                      case 0U:
                        {
                        }
                        break;
                      case 2U:
                        {
                            var_67 = *var_51;
                        }
                        break;
                    }
                    var_68 = var_67 + 1UL;
                    *var_51 = var_68;
                    var_69 = *var_52;
                    *(uint64_t *)((var_69 << 3UL) + *var_36) = (var_69 - var_68);
                    local_sp_12184 = local_sp_12189;
                }
                break;
            }
            var_70 = *var_52 + 1UL;
            *var_52 = var_70;
            var_55 = var_70;
            local_sp_8 = local_sp_12184;
            var_56 = *var_23;
            var_57 = helper_cc_compute_c_wrapper(var_55 - var_56, var_56, var_1, 17U);
            local_sp_9 = local_sp_8;
        }
    var_71 = (uint64_t **)var_17;
    **var_71 = 0UL;
    var_72 = (uint64_t *)(var_0 + (-40L));
    *var_72 = 0UL;
    var_73 = *var_15;
    var_74 = (uint64_t *)(var_0 + (-216L));
    *var_74 = var_73;
    var_75 = var_0 + (-232L);
    *(unsigned char *)var_75 = (unsigned char)'\x00';
    *(uint64_t *)(local_sp_8 + (-8L)) = 4240008UL;
    indirect_placeholder();
    var_76 = (unsigned char *)(var_0 + (-220L));
    *var_76 = (unsigned char)'\x00';
    *var_40 = *var_15;
    *var_42 = (unsigned char)'\x00';
    var_77 = local_sp_8 + (-16L);
    *(uint64_t *)var_77 = 4240065UL;
    indirect_placeholder();
    *var_44 = (unsigned char)'\x00';
    var_78 = (uint64_t *)(var_0 + (-48L));
    var_79 = (unsigned char *)(var_0 + (-200L));
    var_80 = (uint32_t *)(var_0 + (-196L));
    var_81 = (uint64_t *)(var_0 + (-208L));
    local_sp_7 = var_77;
    while (1U)
        {
            var_82 = local_sp_7 + (-8L);
            *(uint64_t *)var_82 = 4240672UL;
            indirect_placeholder_44(var_41);
            var_83 = *var_46;
            local_sp_4180 = var_82;
            local_sp_5 = var_82;
            local_sp_4176 = var_82;
            if (var_83 != '\x01') {
                if (*var_45 == 0U) {
                    break;
                }
            }
            var_84 = *var_72;
            var_85 = *var_34 + (var_84 * 48UL);
            var_96 = var_84;
            var_88 = var_84;
            if (*(unsigned char *)(var_85 + 16UL) != '\x00') {
                if (*(uint64_t *)(var_85 + 8UL) != *var_47) {
                    var_86 = *(uint64_t *)var_85;
                    var_87 = local_sp_7 + (-16L);
                    *(uint64_t *)var_87 = 4240288UL;
                    indirect_placeholder();
                    local_sp_4180 = var_87;
                    local_sp_4176 = var_87;
                    if ((uint64_t)(uint32_t)var_86 != 0UL) {
                        _pre199 = *var_72;
                        var_96 = _pre199;
                        *var_72 = (var_96 + 1UL);
                        *var_40 = (*var_47 + *var_40);
                        *var_44 = (unsigned char)'\x00';
                        local_sp_7_be = local_sp_4180;
                        local_sp_5 = local_sp_4180;
                        if (*var_72 != *var_23) {
                            local_sp_7 = local_sp_7_be;
                            continue;
                        }
                        **var_71 = *var_74;
                        break;
                    }
                    var_88 = *var_72;
                }
                local_sp_6 = local_sp_4176;
                if (var_88 != 0UL) {
                    var_89 = *(uint64_t *)(*var_36 + (var_88 << 3UL));
                    *var_78 = var_89;
                    *var_72 = (*var_72 - var_89);
                    var_90 = *var_78;
                    local_sp_7_be = local_sp_6;
                    while (var_90 != 0UL)
                        {
                            var_91 = local_sp_6 + (-8L);
                            *(uint64_t *)var_91 = 4240445UL;
                            indirect_placeholder_44(var_75);
                            local_sp_1 = var_91;
                            if (*var_79 != '\x01' & *var_80 == 0U) {
                                var_92 = local_sp_6 + (-16L);
                                *(uint64_t *)var_92 = 4240490UL;
                                indirect_placeholder();
                                local_sp_1 = var_92;
                            }
                            *var_74 = (*var_81 + *var_74);
                            *var_76 = (unsigned char)'\x00';
                            var_93 = *var_78 + (-1L);
                            *var_78 = var_93;
                            var_90 = var_93;
                            local_sp_6 = local_sp_1;
                            local_sp_7_be = local_sp_6;
                        }
                }
                var_94 = local_sp_4176 + (-8L);
                *(uint64_t *)var_94 = 4240550UL;
                indirect_placeholder_44(var_75);
                local_sp_0 = var_94;
                if (*var_79 != '\x01' & *var_80 == 0U) {
                    var_95 = local_sp_4176 + (-16L);
                    *(uint64_t *)var_95 = 4240595UL;
                    indirect_placeholder();
                    local_sp_0 = var_95;
                }
                *var_74 = (*var_81 + *var_74);
                *var_76 = (unsigned char)'\x00';
                *var_40 = (*var_47 + *var_40);
                *var_44 = (unsigned char)'\x00';
                local_sp_7_be = local_sp_0;
                local_sp_7 = local_sp_7_be;
                continue;
            }
            if (var_83 != '\x00') {
                if ((uint64_t)(*(uint32_t *)(var_85 + 20UL) - *var_45) != 0UL) {
                    *var_72 = (var_96 + 1UL);
                    *var_40 = (*var_47 + *var_40);
                    *var_44 = (unsigned char)'\x00';
                    local_sp_7_be = local_sp_4180;
                    local_sp_5 = local_sp_4180;
                    if (*var_72 == *var_23) {
                        local_sp_7 = local_sp_7_be;
                        continue;
                    }
                    **var_71 = *var_74;
                    break;
                }
            }
            if (*(uint64_t *)(var_85 + 8UL) != *var_47) {
                var_86 = *(uint64_t *)var_85;
                var_87 = local_sp_7 + (-16L);
                *(uint64_t *)var_87 = 4240288UL;
                indirect_placeholder();
                local_sp_4180 = var_87;
                local_sp_4176 = var_87;
                if ((uint64_t)(uint32_t)var_86 != 0UL) {
                    _pre199 = *var_72;
                    var_96 = _pre199;
                    *var_72 = (var_96 + 1UL);
                    *var_40 = (*var_47 + *var_40);
                    *var_44 = (unsigned char)'\x00';
                    local_sp_7_be = local_sp_4180;
                    local_sp_5 = local_sp_4180;
                    if (*var_72 == *var_23) {
                        local_sp_7 = local_sp_7_be;
                        continue;
                    }
                    **var_71 = *var_74;
                    break;
                }
                var_88 = *var_72;
            }
        }
    var_97 = *var_33;
    *(uint64_t *)(local_sp_5 + (-8L)) = 4240722UL;
    indirect_placeholder_44(var_97);
    return storemerge12;
}
