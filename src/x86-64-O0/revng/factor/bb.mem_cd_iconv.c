typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_75_ret_type;
struct indirect_placeholder_75_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern struct indirect_placeholder_75_ret_type indirect_placeholder_75(uint64_t param_0);
typedef _Bool bool;
uint64_t bb_mem_cd_iconv(uint64_t rcx, uint64_t rdx, uint64_t rsi, uint64_t rdi, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t *var_2;
    uint64_t *var_3;
    uint64_t *var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t *var_13;
    uint64_t *var_14;
    uint64_t var_15;
    uint64_t _pre_phi;
    uint64_t local_sp_2;
    uint64_t local_sp_0;
    uint64_t var_48;
    uint32_t var_49;
    uint32_t *var_50;
    uint64_t local_sp_3;
    uint32_t *var_51;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t *_pre_phi117;
    uint64_t local_sp_1;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t local_sp_6;
    uint64_t rax_0;
    uint64_t var_20;
    uint64_t var_25;
    uint64_t *var_26;
    uint64_t **var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t *var_30;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t *var_38;
    uint64_t var_39;
    uint64_t *var_40;
    uint64_t *var_41;
    uint64_t var_31;
    struct indirect_placeholder_75_ret_type var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t *var_35;
    uint64_t var_21;
    uint64_t var_18;
    uint32_t var_19;
    uint64_t local_sp_4;
    uint64_t local_sp_5;
    uint64_t *var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_16;
    uint64_t var_17;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = (uint64_t *)(var_0 + (-4256L));
    *var_2 = rdi;
    var_3 = (uint64_t *)(var_0 + (-4264L));
    *var_3 = rsi;
    var_4 = (uint64_t *)(var_0 + (-4272L));
    *var_4 = rdx;
    var_5 = var_0 + (-4280L);
    *(uint64_t *)var_5 = rcx;
    var_6 = var_0 + (-4288L);
    *(uint64_t *)var_6 = r8;
    var_7 = var_0 + (-4304L);
    *(uint64_t *)var_7 = 4243164UL;
    indirect_placeholder();
    var_8 = (uint64_t *)(var_0 + (-24L));
    *var_8 = 0UL;
    *(uint64_t *)(var_0 + (-80L)) = *var_2;
    var_9 = *var_3;
    var_10 = (uint64_t *)(var_0 + (-88L));
    *var_10 = var_9;
    var_11 = var_0 + (-4248L);
    var_12 = (uint64_t *)(var_0 + (-96L));
    var_13 = (uint64_t *)(var_0 + (-104L));
    var_14 = (uint64_t *)(var_0 + (-32L));
    rax_0 = 4294967295UL;
    var_15 = var_9;
    local_sp_5 = var_7;
    while (1U)
        {
            local_sp_6 = local_sp_5;
            if (var_15 != 0UL) {
                loop_state_var = 1U;
                break;
            }
            *var_12 = var_11;
            *var_13 = 4096UL;
            var_16 = *var_4;
            var_17 = local_sp_5 + (-8L);
            *(uint64_t *)var_17 = 4243252UL;
            indirect_placeholder();
            *var_14 = var_16;
            local_sp_4 = var_17;
            switch_state_var = 0;
            switch (var_16) {
              case 18446744073709551615UL:
                {
                    var_18 = local_sp_5 + (-16L);
                    *(uint64_t *)var_18 = 4243268UL;
                    indirect_placeholder();
                    var_19 = *(uint32_t *)18446744073709551615UL;
                    local_sp_4 = var_18;
                    if (var_19 != 7U) {
                        var_20 = (uint64_t)var_19;
                        var_21 = local_sp_5 + (-24L);
                        *(uint64_t *)var_21 = 4243280UL;
                        indirect_placeholder();
                        local_sp_6 = var_21;
                        if (*(uint32_t *)var_20 != 22U) {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    *var_8 = (*var_8 + (*var_12 - var_11));
                    var_15 = *var_10;
                    local_sp_5 = local_sp_4;
                    continue;
                }
                break;
              case 0UL:
                {
                    *var_8 = (*var_8 + (*var_12 - var_11));
                    var_15 = *var_10;
                    local_sp_5 = local_sp_4;
                    continue;
                }
                break;
              default:
                {
                    *(uint64_t *)(local_sp_5 + (-16L)) = 4243309UL;
                    indirect_placeholder();
                    *(uint32_t *)var_16 = 84U;
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            return rax_0;
        }
        break;
      case 1U:
        {
            var_22 = (uint64_t *)(var_0 + (-112L));
            *var_22 = var_11;
            *(uint64_t *)(var_0 + (-120L)) = 4096UL;
            var_23 = *var_4;
            var_24 = local_sp_6 + (-8L);
            *(uint64_t *)var_24 = 4243420UL;
            indirect_placeholder();
            *(uint64_t *)(var_0 + (-40L)) = var_23;
            local_sp_3 = var_24;
            if (var_23 != 18446744073709551615UL) {
                var_25 = *var_8 + (*var_22 - var_11);
                *var_8 = var_25;
                var_26 = (uint64_t *)(var_0 + (-48L));
                *var_26 = var_25;
                rax_0 = 0UL;
                if (var_25 == 0UL) {
                    **(uint64_t **)var_6 = 0UL;
                } else {
                    var_27 = (uint64_t **)var_5;
                    var_28 = **var_27;
                    rax_0 = 4294967295UL;
                    if (var_28 == 0UL) {
                        if (var_25 > **(uint64_t **)var_6) {
                            var_31 = local_sp_6 + (-16L);
                            *(uint64_t *)var_31 = 4243560UL;
                            var_32 = indirect_placeholder_75(var_25);
                            var_33 = var_32.field_0;
                            var_34 = var_0 + (-16L);
                            var_35 = (uint64_t *)var_34;
                            *var_35 = var_33;
                            _pre_phi117 = var_35;
                            _pre_phi = var_34;
                            local_sp_3 = var_31;
                            if (var_33 != 0UL) {
                                *(uint64_t *)(local_sp_6 + (-24L)) = 4243576UL;
                                indirect_placeholder();
                                *(volatile uint32_t *)(uint32_t *)0UL = 12U;
                                return rax_0;
                            }
                        }
                        var_29 = var_0 + (-16L);
                        var_30 = (uint64_t *)var_29;
                        *var_30 = var_28;
                        _pre_phi117 = var_30;
                        _pre_phi = var_29;
                    } else {
                        var_31 = local_sp_6 + (-16L);
                        *(uint64_t *)var_31 = 4243560UL;
                        var_32 = indirect_placeholder_75(var_25);
                        var_33 = var_32.field_0;
                        var_34 = var_0 + (-16L);
                        var_35 = (uint64_t *)var_34;
                        *var_35 = var_33;
                        _pre_phi117 = var_35;
                        _pre_phi = var_34;
                        local_sp_3 = var_31;
                        if (var_33 != 0UL) {
                            *(uint64_t *)(local_sp_6 + (-24L)) = 4243576UL;
                            indirect_placeholder();
                            *(volatile uint32_t *)(uint32_t *)0UL = 12U;
                            return rax_0;
                        }
                        var_36 = local_sp_3 + (-8L);
                        *(uint64_t *)var_36 = 4243628UL;
                        indirect_placeholder();
                        *(uint64_t *)(var_0 + (-128L)) = *var_2;
                        var_37 = *var_3;
                        var_38 = (uint64_t *)(var_0 + (-136L));
                        *var_38 = var_37;
                        *(uint64_t *)(var_0 + (-144L)) = *_pre_phi117;
                        var_39 = *var_26;
                        var_40 = (uint64_t *)(var_0 + (-152L));
                        *var_40 = var_39;
                        var_41 = (uint64_t *)(var_0 + (-56L));
                        local_sp_1 = var_36;
                        rax_0 = 0UL;
                        while (1U)
                            {
                                local_sp_2 = local_sp_1;
                                if (*var_38 != 0UL) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                var_42 = *var_4;
                                var_43 = local_sp_1 + (-8L);
                                *(uint64_t *)var_43 = 4243714UL;
                                indirect_placeholder();
                                *var_41 = var_42;
                                local_sp_1 = var_43;
                                switch_state_var = 0;
                                switch (var_42) {
                                  case 0UL:
                                    {
                                        continue;
                                    }
                                    break;
                                  case 18446744073709551615UL:
                                    {
                                        var_44 = local_sp_1 + (-16L);
                                        *(uint64_t *)var_44 = 4243730UL;
                                        indirect_placeholder();
                                        local_sp_0 = var_44;
                                        local_sp_2 = var_44;
                                        if (*(uint32_t *)18446744073709551615UL != 22U) {
                                            loop_state_var = 1U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                        loop_state_var = 0U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    break;
                                  default:
                                    {
                                        var_45 = local_sp_1 + (-16L);
                                        *(uint64_t *)var_45 = 4243755UL;
                                        indirect_placeholder();
                                        *(uint32_t *)var_42 = 84U;
                                        local_sp_0 = var_45;
                                        loop_state_var = 1U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    break;
                                }
                                if (switch_state_var)
                                    break;
                            }
                        switch (loop_state_var) {
                          case 1U:
                            {
                                var_48 = **var_27;
                                if (*_pre_phi117 == var_48) {
                                    *(uint64_t *)(local_sp_0 + (-8L)) = 4243905UL;
                                    indirect_placeholder();
                                    var_49 = *(uint32_t *)var_48;
                                    var_50 = (uint32_t *)(var_0 + (-68L));
                                    *var_50 = var_49;
                                    var_51 = *(uint32_t **)_pre_phi;
                                    *(uint64_t *)(local_sp_0 + (-16L)) = 4243922UL;
                                    indirect_placeholder();
                                    *(uint64_t *)(local_sp_0 + (-24L)) = 4243927UL;
                                    indirect_placeholder();
                                    *var_51 = *var_50;
                                }
                            }
                            break;
                          case 0U:
                            {
                                var_46 = *var_4;
                                var_47 = local_sp_2 + (-8L);
                                *(uint64_t *)var_47 = 4243817UL;
                                indirect_placeholder();
                                *(uint64_t *)(var_0 + (-64L)) = var_46;
                                local_sp_0 = var_47;
                                if (var_46 != 18446744073709551615UL) {
                                    if (*var_40 == 0UL) {
                                        *(uint64_t *)(local_sp_2 + (-16L)) = 4243845UL;
                                        indirect_placeholder();
                                    }
                                    **var_27 = *_pre_phi117;
                                    **(uint64_t **)var_6 = *var_26;
                                    return rax_0;
                                }
                            }
                            break;
                        }
                    }
                }
            }
        }
        break;
    }
}
