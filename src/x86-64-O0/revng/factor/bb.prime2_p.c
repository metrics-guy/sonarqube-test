typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_26_ret_type;
struct indirect_placeholder_23_ret_type;
struct indirect_placeholder_25_ret_type;
struct indirect_placeholder_27_ret_type;
struct indirect_placeholder_26_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
};
struct indirect_placeholder_23_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_25_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_27_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_26_ret_type indirect_placeholder_26(uint64_t param_0);
extern void indirect_placeholder_24(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_23_ret_type indirect_placeholder_23(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_25_ret_type indirect_placeholder_25(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern uint64_t helper_ctz_wrapper(uint64_t param_0);
extern struct indirect_placeholder_27_ret_type indirect_placeholder_27(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
uint64_t bb_prime2_p(uint64_t rsi, uint64_t rdi) {
    uint64_t var_147;
    uint64_t var_144;
    uint64_t var_145;
    uint64_t var_149;
    uint64_t var_150;
    uint64_t var_151;
    uint64_t cc_src2_3;
    uint64_t rax_0;
    struct indirect_placeholder_26_ret_type var_160;
    uint64_t var_7;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t var_131;
    uint64_t var_114;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint64_t var_6;
    uint64_t var_156;
    uint64_t var_157;
    uint64_t var_158;
    uint32_t var_159;
    uint32_t var_106;
    unsigned char storemerge1;
    uint32_t var_132;
    uint32_t var_115;
    uint64_t r9_0;
    uint64_t r8_0;
    unsigned char storemerge;
    uint64_t local_sp_2;
    uint64_t _pre_phi331;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_81;
    struct indirect_placeholder_23_ret_type var_82;
    uint64_t local_sp_0;
    uint64_t rcx_0;
    uint32_t *var_83;
    unsigned char *var_84;
    uint64_t *var_85;
    uint64_t *var_86;
    uint64_t *var_87;
    uint64_t *var_88;
    uint64_t *var_89;
    uint64_t var_90;
    uint64_t *var_91;
    uint64_t *var_92;
    uint64_t *var_93;
    uint64_t *var_94;
    uint32_t *var_95;
    unsigned char *var_96;
    uint64_t *var_97;
    uint64_t *var_98;
    uint64_t *var_99;
    uint64_t *var_100;
    uint64_t *var_101;
    uint64_t *var_102;
    uint64_t *var_103;
    uint64_t *var_104;
    uint64_t *var_105;
    uint64_t local_sp_1;
    uint64_t cc_src2_0;
    uint64_t rcx_1;
    uint64_t r9_1;
    uint64_t r8_1;
    uint64_t local_sp_4;
    uint64_t var_107;
    uint64_t var_108;
    uint64_t var_109;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t var_112;
    uint64_t var_113;
    uint64_t local_sp_3;
    uint64_t var_116;
    uint64_t var_117;
    uint64_t var_148;
    unsigned char var_134;
    uint64_t var_118;
    uint64_t var_119;
    uint64_t var_120;
    uint64_t var_121;
    uint64_t var_122;
    uint64_t var_123;
    uint64_t var_124;
    uint64_t var_125;
    uint64_t var_43;
    unsigned __int128 var_126;
    uint64_t var_127;
    uint64_t var_128;
    uint64_t var_129;
    uint64_t var_130;
    unsigned char var_133;
    uint64_t var_135;
    uint64_t var_136;
    uint64_t var_137;
    uint64_t var_146;
    uint64_t cc_src2_1_ph;
    uint64_t var_138;
    bool var_139;
    uint64_t var_140;
    uint64_t var_152;
    uint64_t var_153;
    uint64_t var_154;
    struct indirect_placeholder_25_ret_type var_155;
    uint64_t var_141;
    uint64_t var_142;
    uint64_t var_143;
    uint64_t *var_10;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint32_t *var_17;
    uint32_t var_18;
    uint64_t var_11;
    uint32_t *var_12;
    uint32_t var_13;
    uint64_t *var_19;
    uint64_t var_20;
    uint64_t *var_21;
    uint64_t var_22;
    uint64_t *var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t *var_27;
    bool var_28;
    uint64_t *var_29;
    uint64_t *var_32;
    uint64_t *var_33;
    uint64_t *_pre_phi329;
    uint64_t *var_30;
    uint64_t *var_31;
    uint64_t *_pre_phi327;
    uint64_t var_42;
    uint64_t cc_src2_2_ph;
    uint64_t var_34;
    bool var_35;
    uint64_t var_36;
    uint64_t *var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t *var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t *var_56;
    uint64_t var_57;
    uint64_t *var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint32_t *var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    struct indirect_placeholder_27_ret_type var_77;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_cc_src2();
    var_2 = init_rbp();
    var_3 = var_0 + (-8L);
    *(uint64_t *)var_3 = var_2;
    var_4 = (uint64_t *)(var_0 + (-560L));
    *var_4 = rdi;
    var_5 = (uint64_t *)(var_0 + (-568L));
    *var_5 = rsi;
    var_6 = *var_4;
    rax_0 = 0UL;
    var_106 = 0U;
    storemerge1 = (unsigned char)'\x01';
    var_115 = 0U;
    storemerge = (unsigned char)'\x01';
    var_134 = (unsigned char)'\x00';
    cc_src2_2_ph = var_1;
    if (var_6 == 0UL) {
        *(uint64_t *)(var_0 + (-576L)) = 4210414UL;
        var_160 = indirect_placeholder_26(rsi);
        rax_0 = var_160.field_0;
    } else {
        var_7 = var_6 + (rsi == 0UL);
        var_8 = (uint64_t *)(var_0 + (-208L));
        *var_8 = var_7;
        var_9 = *var_5 + (-1L);
        var_10 = (uint64_t *)(var_0 + (-216L));
        *var_10 = var_9;
        if (var_9 == 0UL) {
            var_14 = *var_8;
            var_15 = helper_ctz_wrapper(var_14);
            var_16 = (var_14 == 0UL) ? 0UL : var_15;
            var_17 = (uint32_t *)(var_0 + (-44L));
            var_18 = (uint32_t)var_16;
            *var_17 = var_18;
            *(uint64_t *)(var_0 + (-200L)) = (*var_8 >> (uint64_t)(var_18 & 63U));
            *(uint64_t *)(var_0 + (-192L)) = 0UL;
            *var_17 = (*var_17 + 64U);
        } else {
            var_11 = helper_ctz_wrapper(var_9);
            var_12 = (uint32_t *)(var_0 + (-44L));
            var_13 = (uint32_t)var_11;
            *var_12 = var_13;
            *(uint64_t *)(var_0 + (-200L)) = ((*var_10 >> (uint64_t)(var_13 & 63U)) | (*var_8 << (uint64_t)((0U - var_13) & 63U)));
            *(uint64_t *)(var_0 + (-192L)) = (*var_8 >> (uint64_t)(*var_12 & 63U));
        }
        var_19 = (uint64_t *)(var_0 + (-56L));
        *var_19 = 2UL;
        var_20 = *var_5;
        var_21 = (uint64_t *)(var_0 + (-96L));
        *var_21 = var_20;
        var_22 = (uint64_t)*(unsigned char *)(((var_20 >> 1UL) & 127UL) + 4309536UL);
        var_23 = (uint64_t *)(var_0 + (-104L));
        *var_23 = var_22;
        var_24 = (var_22 << 1UL) - (*var_21 * (var_22 * var_22));
        *var_23 = var_24;
        var_25 = (var_24 << 1UL) - (*var_21 * (var_24 * var_24));
        *var_23 = var_25;
        var_26 = (var_25 << 1UL) - (*var_21 * (var_25 * var_25));
        *var_23 = var_26;
        var_27 = (uint64_t *)(var_0 + (-112L));
        *var_27 = var_26;
        var_28 = (*var_4 > 1UL);
        var_29 = (uint64_t *)(var_0 + (-32L));
        if (var_28) {
            *var_29 = 1UL;
            var_32 = (uint64_t *)(var_0 + (-40L));
            *var_32 = 0UL;
            var_33 = (uint64_t *)(var_0 + (-16L));
            *var_33 = 64UL;
            _pre_phi329 = var_32;
            _pre_phi327 = var_33;
        } else {
            *var_29 = 0UL;
            var_30 = (uint64_t *)(var_0 + (-40L));
            *var_30 = 1UL;
            var_31 = (uint64_t *)(var_0 + (-16L));
            *var_31 = 128UL;
            _pre_phi329 = var_30;
            _pre_phi327 = var_31;
        }
        while (1U)
            {
                while (1U)
                    {
                        var_34 = *_pre_phi327;
                        *_pre_phi327 = (var_34 + (-1L));
                        var_35 = (var_34 == 0UL);
                        var_36 = *var_29;
                        if (var_35) {
                            *var_29 = ((*_pre_phi329 >> 63UL) | (var_36 << 1UL));
                            var_37 = *_pre_phi329 << 1UL;
                            *_pre_phi329 = var_37;
                            var_38 = *var_29;
                            var_39 = *var_4;
                            var_42 = var_39;
                            var_43 = var_37;
                            var_44 = var_38;
                            if (var_38 <= var_39) {
                                loop_state_var = 0U;
                                break;
                            }
                            if (var_38 != var_39) {
                                continue;
                            }
                            var_40 = *var_5;
                            var_41 = helper_cc_compute_c_wrapper(var_37 - var_40, var_40, cc_src2_2_ph, 17U);
                            if (var_41 == 0UL) {
                                continue;
                            }
                            loop_state_var = 1U;
                            break;
                        }
                        var_48 = (uint64_t *)(var_0 + (-240L));
                        *var_48 = var_36;
                        var_49 = *_pre_phi329;
                        var_50 = var_0 + (-248L);
                        var_51 = (uint64_t *)var_50;
                        *var_51 = var_49;
                        var_52 = *var_48;
                        var_53 = var_49 + var_49;
                        var_54 = helper_cc_compute_c_wrapper(var_53, var_49, cc_src2_2_ph, 9U);
                        var_55 = (var_52 + var_52) + var_54;
                        var_56 = (uint64_t *)(var_0 + (-224L));
                        *var_56 = var_55;
                        var_57 = var_0 + (-232L);
                        var_58 = (uint64_t *)var_57;
                        *var_58 = var_53;
                        var_59 = *var_56;
                        var_60 = helper_cc_compute_c_wrapper(*var_4 - var_59, var_59, var_54, 17U);
                        cc_src2_3 = var_54;
                        if (var_60 != 0UL) {
                            var_64 = *var_4;
                            var_65 = *var_5;
                            var_66 = *var_58;
                            var_67 = *var_56;
                            loop_state_var = 3U;
                            break;
                        }
                        var_61 = *var_56;
                        var_64 = var_61;
                        var_67 = var_61;
                        if (*var_4 != var_61) {
                            loop_state_var = 2U;
                            break;
                        }
                        var_62 = *var_58;
                        var_63 = *var_5;
                        var_65 = var_63;
                        var_66 = var_62;
                        if (var_63 <= var_62) {
                            loop_state_var = 3U;
                            break;
                        }
                        loop_state_var = 2U;
                        break;
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 2U:
                    {
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 3U:
                    {
                        var_68 = var_66 - var_65;
                        var_69 = helper_cc_compute_c_wrapper(var_68, var_65, var_54, 17U);
                        *var_56 = ((var_67 - var_64) - var_69);
                        *var_58 = var_68;
                        cc_src2_3 = var_69;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 1U:
                  case 0U:
                    {
                        switch (loop_state_var) {
                          case 1U:
                            {
                                var_42 = *var_4;
                                var_43 = *_pre_phi329;
                                var_44 = *var_29;
                            }
                            break;
                          case 0U:
                            {
                            }
                            break;
                        }
                        var_45 = *var_5;
                        var_46 = var_43 - var_45;
                        var_47 = helper_cc_compute_c_wrapper(var_46, var_45, cc_src2_2_ph, 17U);
                        *var_29 = ((var_44 - var_42) - var_47);
                        *_pre_phi329 = var_46;
                        cc_src2_2_ph = var_47;
                        continue;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
        var_70 = *var_5;
        var_71 = var_0 + (-264L);
        *(uint64_t *)var_71 = var_70;
        *(uint64_t *)(var_0 + (-256L)) = *var_4;
        var_72 = (uint32_t *)(var_0 + (-44L));
        var_73 = (uint64_t)*var_72;
        var_74 = var_0 + (-200L);
        var_75 = *var_27;
        var_76 = var_0 + (-576L);
        *(uint64_t *)var_76 = 4211185UL;
        var_77 = indirect_placeholder_27(var_74, var_57, var_75, var_71, var_50, var_73);
        local_sp_0 = var_76;
        cc_src2_0 = cc_src2_3;
        if ((uint64_t)(unsigned char)var_77.field_0 != 1UL) {
            rcx_0 = var_77.field_1;
            r9_0 = var_77.field_2;
            r8_0 = var_77.field_3;
            rax_0 = 1UL;
            if (*(unsigned char *)4330112UL == '\x00') {
                _pre_phi331 = var_0 + (-520L);
            } else {
                var_78 = *var_10;
                var_79 = *var_8;
                var_80 = var_0 + (-520L);
                var_81 = var_0 + (-584L);
                *(uint64_t *)var_81 = 4211245UL;
                var_82 = indirect_placeholder_23(var_80, var_78, var_79);
                r9_0 = var_82.field_4;
                r8_0 = var_82.field_5;
                _pre_phi331 = var_80;
                local_sp_0 = var_81;
                rcx_0 = var_82.field_0;
            }
            var_83 = (uint32_t *)(var_0 + (-20L));
            *var_83 = 0U;
            var_84 = (unsigned char *)(var_0 + (-57L));
            var_85 = (uint64_t *)(var_0 + (-512L));
            var_86 = (uint64_t *)_pre_phi331;
            var_87 = (uint64_t *)(var_0 + (-120L));
            var_88 = (uint64_t *)(var_0 + (-128L));
            var_89 = (uint64_t *)(var_0 + (-136L));
            var_90 = var_0 + (-536L);
            var_91 = (uint64_t *)var_90;
            var_92 = (uint64_t *)(var_0 + (-528L));
            var_93 = (uint64_t *)(var_0 + (-552L));
            var_94 = (uint64_t *)(var_0 + (-544L));
            var_95 = (uint32_t *)(var_0 + (-64L));
            var_96 = (unsigned char *)(var_0 + (-270L));
            var_97 = (uint64_t *)(var_0 + (-144L));
            var_98 = (uint64_t *)(var_0 + (-152L));
            var_99 = (uint64_t *)(var_0 + (-160L));
            var_100 = (uint64_t *)(var_0 + (-168L));
            var_101 = (uint64_t *)(var_0 + (-176L));
            var_102 = (uint64_t *)(var_0 + (-184L));
            var_103 = (uint64_t *)(var_0 + (-72L));
            var_104 = (uint64_t *)(var_0 + (-80L));
            var_105 = (uint64_t *)(var_0 + (-88L));
            local_sp_1 = local_sp_0;
            rcx_1 = rcx_0;
            r9_1 = r9_0;
            r8_1 = r8_0;
            while (1U)
                {
                    local_sp_2 = local_sp_1;
                    local_sp_4 = local_sp_1;
                    cc_src2_1_ph = cc_src2_0;
                    if (var_106 <= 667U) {
                        *(uint64_t *)(local_sp_1 + (-8L)) = 4212512UL;
                        indirect_placeholder_24(0UL, rcx_1, 4309784UL, 0UL, 0UL, r9_1, r8_1);
                        *(uint64_t *)(local_sp_1 + (-16L)) = 4212517UL;
                        indirect_placeholder();
                        break;
                    }
                    if (*(unsigned char *)4330112UL == '\x00') {
                        var_133 = (var_106 == 24U);
                        *var_84 = var_133;
                        var_134 = var_133;
                    } else {
                        *var_84 = (unsigned char)'\x01';
                        if (*var_85 != 0UL) {
                            var_107 = *var_86;
                            *var_87 = var_107;
                            var_108 = (uint64_t)*(unsigned char *)(((var_107 >> 1UL) & 127UL) + 4309536UL);
                            *var_88 = var_108;
                            var_109 = (var_108 << 1UL) - (*var_87 * (var_108 * var_108));
                            *var_88 = var_109;
                            var_110 = (var_109 << 1UL) - (*var_87 * (var_109 * var_109));
                            *var_88 = var_110;
                            var_111 = (var_110 << 1UL) - (*var_87 * (var_110 * var_110));
                            *var_88 = var_111;
                            *var_89 = var_111;
                            *var_91 = (var_111 * *var_10);
                            *var_92 = 0UL;
                            var_112 = *var_27;
                            var_113 = local_sp_1 + (-8L);
                            *(uint64_t *)var_113 = 4211509UL;
                            var_114 = indirect_placeholder_12(var_71, var_90, var_57, var_50, var_112);
                            *var_93 = var_114;
                            local_sp_2 = var_113;
                            if (var_114 == *var_51) {
                            } else {
                                storemerge = (unsigned char)'\x00';
                                if (*var_94 == *var_48) {
                                }
                            }
                            *var_84 = (storemerge & '\x01');
                        }
                        *var_95 = 0U;
                        local_sp_3 = local_sp_2;
                        while (1U)
                            {
                                var_116 = (uint64_t)*var_96;
                                var_117 = helper_cc_compute_c_wrapper((uint64_t)var_115 - var_116, var_116, cc_src2_0, 16U);
                                local_sp_4 = local_sp_3;
                                if (var_117 != 0UL) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                if (*var_84 != '\x00') {
                                    loop_state_var = 1U;
                                    break;
                                }
                                var_118 = *(uint64_t *)(((((uint64_t)*var_95 << 3UL) + 16UL) + var_3) + (-512L));
                                if (var_118 == 2UL) {
                                    *var_91 = ((*var_10 >> 1UL) | (*var_8 << 63UL));
                                    *var_92 = (*var_8 >> 1UL);
                                } else {
                                    *var_97 = var_118;
                                    var_119 = (uint64_t)*(unsigned char *)(((var_118 >> 1UL) & 127UL) + 4309536UL);
                                    *var_98 = var_119;
                                    var_120 = (var_119 << 1UL) - (*var_97 * (var_119 * var_119));
                                    *var_98 = var_120;
                                    var_121 = (var_120 << 1UL) - (*var_97 * (var_120 * var_120));
                                    *var_98 = var_121;
                                    var_122 = (var_121 << 1UL) - (*var_97 * (var_121 * var_121));
                                    *var_98 = var_122;
                                    *var_99 = var_122;
                                    *var_100 = (var_122 * *var_10);
                                    var_123 = *var_8;
                                    var_124 = *(uint64_t *)(((((uint64_t)*var_95 << 3UL) + 16UL) + var_3) + (-512L));
                                    var_125 = helper_cc_compute_c_wrapper(var_123 - var_124, var_124, cc_src2_0, 17U);
                                    if (var_125 == 0UL) {
                                        var_126 = (unsigned __int128)*(uint64_t *)(((((uint64_t)*var_95 << 3UL) + 16UL) + var_3) + (-512L)) * (unsigned __int128)*var_100;
                                        var_127 = (uint64_t)var_126;
                                        var_128 = (uint64_t)(var_126 >> 64ULL);
                                        *var_101 = var_127;
                                        *var_102 = var_128;
                                        *var_92 = (*var_99 * (*var_8 - var_128));
                                        *var_91 = *var_100;
                                    } else {
                                        *var_91 = *var_100;
                                        *var_92 = 0UL;
                                    }
                                }
                                var_129 = *var_27;
                                var_130 = local_sp_3 + (-8L);
                                *(uint64_t *)var_130 = 4212069UL;
                                var_131 = indirect_placeholder_12(var_71, var_90, var_57, var_50, var_129);
                                *var_93 = var_131;
                                local_sp_3 = var_130;
                                if (var_131 == *var_51) {
                                } else {
                                    storemerge1 = (unsigned char)'\x00';
                                    if (*var_94 == *var_48) {
                                    }
                                }
                                *var_84 = (storemerge1 & '\x01');
                                var_132 = *var_95 + 1U;
                                *var_95 = var_132;
                                var_115 = var_132;
                                continue;
                            }
                        var_134 = *var_84;
                    }
                }
        }
    }
    return rax_0;
}
