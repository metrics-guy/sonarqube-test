typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_48_ret_type;
struct indirect_placeholder_48_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_13(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_32(void);
extern void indirect_placeholder_44(uint64_t param_0);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
extern void indirect_placeholder_47(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_48_ret_type indirect_placeholder_48(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
typedef _Bool bool;
void bb_main(uint64_t rsi, uint64_t rdi) {
    struct indirect_placeholder_48_ret_type var_13;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint32_t *var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint32_t *var_9;
    uint64_t local_sp_2;
    uint32_t var_23;
    uint32_t var_19;
    uint64_t var_15;
    unsigned char *var_16;
    uint32_t var_17;
    uint32_t *var_18;
    uint64_t local_sp_0;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t local_sp_1;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t *var_28;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint32_t var_14;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = var_0 + (-8L);
    *(uint64_t *)var_3 = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_4 = (uint32_t *)(var_0 + (-44L));
    *var_4 = (uint32_t)rdi;
    var_5 = var_0 + (-56L);
    var_6 = (uint64_t *)var_5;
    *var_6 = rsi;
    var_7 = **(uint64_t **)var_5;
    *(uint64_t *)(var_0 + (-64L)) = 4219306UL;
    indirect_placeholder_44(var_7);
    *(uint64_t *)(var_0 + (-72L)) = 4219321UL;
    indirect_placeholder();
    *(uint64_t *)(var_0 + (-80L)) = 4219326UL;
    indirect_placeholder();
    *(uint64_t *)(var_0 + (-88L)) = 4219336UL;
    indirect_placeholder();
    var_8 = var_0 + (-96L);
    *(uint64_t *)var_8 = 4219346UL;
    indirect_placeholder();
    var_9 = (uint32_t *)(var_0 + (-36L));
    local_sp_2 = var_8;
    while (1U)
        {
            var_10 = *var_6;
            var_11 = (uint64_t)*var_4;
            var_12 = local_sp_2 + (-8L);
            *(uint64_t *)var_12 = 4219554UL;
            var_13 = indirect_placeholder_48(4297120UL, 4297019UL, var_10, var_11, 0UL);
            var_14 = (uint32_t)var_13.field_0;
            *var_9 = var_14;
            local_sp_0 = var_12;
            local_sp_1 = var_12;
            local_sp_2 = var_12;
            switch_state_var = 0;
            switch (var_14) {
              case 128U:
                {
                    *(unsigned char *)4330416UL = (unsigned char)'\x01';
                    continue;
                }
                break;
              case 4294967295U:
                {
                    if ((int)*(uint32_t *)4330264UL < (int)*var_4) {
                        *(uint64_t *)(local_sp_2 + (-16L)) = 4219583UL;
                        var_15 = indirect_placeholder_32();
                        *(unsigned char *)(var_0 + (-25L)) = (unsigned char)var_15;
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    var_16 = (unsigned char *)(var_0 + (-25L));
                    *var_16 = (unsigned char)'\x01';
                    var_17 = *(uint32_t *)4330264UL;
                    var_18 = (uint32_t *)(var_0 + (-32L));
                    *var_18 = var_17;
                    var_19 = var_17;
                    while ((long)((uint64_t)var_19 << 32UL) >= (long)((uint64_t)*var_4 << 32UL))
                        {
                            var_20 = *(uint64_t *)(*var_6 + ((uint64_t)var_19 << 3UL));
                            var_21 = local_sp_0 + (-8L);
                            *(uint64_t *)var_21 = 4219634UL;
                            var_22 = indirect_placeholder_2(var_20);
                            local_sp_0 = var_21;
                            if ((uint64_t)(unsigned char)var_22 == 1UL) {
                                *var_16 = (unsigned char)'\x00';
                            }
                            var_23 = *var_18 + 1U;
                            *var_18 = var_23;
                            var_19 = var_23;
                        }
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
              default:
                {
                    if ((int)var_14 <= (int)128U) {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    switch_state_var = 0;
                    switch (var_14) {
                      case 4294967166U:
                        {
                            *(uint64_t *)(local_sp_2 + (-16L)) = 4219412UL;
                            indirect_placeholder_13(var_3, 0UL);
                            abort();
                        }
                        break;
                      case 4294967165U:
                        {
                            *(uint64_t *)(local_sp_2 + (-16L)) = 4219427UL;
                            var_24 = indirect_placeholder_1(4310305UL, 4310319UL);
                            *(uint64_t *)(local_sp_2 + (-24L)) = 4219445UL;
                            var_25 = indirect_placeholder_1(4310332UL, 4310351UL);
                            var_26 = *(uint64_t *)4330120UL;
                            *(uint64_t *)(local_sp_2 + (-32L)) = 0UL;
                            var_27 = local_sp_2 + (-40L);
                            var_28 = (uint64_t *)var_27;
                            *var_28 = var_24;
                            *(uint64_t *)(local_sp_2 + (-48L)) = 4219500UL;
                            indirect_placeholder_47(0UL, var_26, 4296856UL, 4310294UL, var_25, 4310369UL);
                            *var_28 = 4219514UL;
                            indirect_placeholder();
                            local_sp_1 = var_27;
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      default:
                        {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(uint64_t *)(local_sp_1 + (-8L)) = 4219524UL;
            indirect_placeholder_13(var_3, 1UL);
            abort();
        }
        break;
      case 1U:
        {
            return;
        }
        break;
    }
}
