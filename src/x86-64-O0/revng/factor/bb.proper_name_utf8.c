typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_41(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_32(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
uint64_t bb_proper_name_utf8(uint64_t rsi, uint64_t rdi) {
    uint64_t local_sp_1;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t *var_10;
    uint64_t *var_11;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t *var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_42;
    uint64_t local_sp_2;
    uint64_t local_sp_0;
    uint64_t *rax_0_in_pre_phi;
    uint64_t var_29;
    uint64_t var_43;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t *var_40;
    uint64_t var_41;
    uint64_t local_sp_3;
    uint64_t local_sp_4;
    uint64_t var_35;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t local_sp_5;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t local_sp_6;
    uint64_t rax_1;
    uint64_t *var_23;
    uint64_t var_24;
    uint64_t var_25;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_3 = var_0 + (-96L);
    var_4 = (uint64_t *)var_3;
    *var_4 = rdi;
    var_5 = (uint64_t *)(var_0 + (-104L));
    *var_5 = rsi;
    var_6 = *var_4;
    var_7 = (uint64_t *)(var_0 + (-56L));
    *var_7 = var_6;
    *(uint64_t *)(var_0 + (-112L)) = 4221528UL;
    var_8 = indirect_placeholder_32();
    var_9 = (uint64_t *)(var_0 + (-64L));
    *var_9 = var_8;
    var_10 = (uint64_t *)(var_0 + (-32L));
    *var_10 = 0UL;
    var_11 = (uint64_t *)(var_0 + (-72L));
    *var_11 = 0UL;
    var_12 = (uint64_t *)(var_0 + (-40L));
    *var_12 = 0UL;
    var_13 = var_0 + (-48L);
    var_14 = (uint64_t *)var_13;
    *var_14 = 0UL;
    var_15 = *var_9;
    var_16 = var_0 + (-120L);
    *(uint64_t *)var_16 = 4221581UL;
    var_17 = indirect_placeholder_1(4310620UL, var_15);
    local_sp_6 = var_16;
    if ((uint64_t)(uint32_t)var_17 == 0UL) {
        *var_12 = *var_5;
        *var_14 = *var_5;
        var_22 = *var_12;
    } else {
        var_18 = *var_9;
        var_19 = *var_5;
        var_20 = var_0 + (-128L);
        *(uint64_t *)var_20 = 4221606UL;
        var_21 = indirect_placeholder_41(var_18, 4310620UL, var_19);
        *var_10 = var_21;
        *var_12 = var_21;
        var_22 = var_21;
        local_sp_6 = var_20;
    }
    rax_1 = var_22;
    if (var_22 == 0UL) {
        rax_1 = *(uint64_t *)((*var_14 == 0UL) ? var_3 : var_13);
    }
    var_23 = (uint64_t *)(var_0 + (-80L));
    *var_23 = rax_1;
    var_24 = *var_7;
    var_25 = local_sp_6 + (-8L);
    *(uint64_t *)var_25 = 4221689UL;
    indirect_placeholder();
    local_sp_5 = var_25;
    rax_0_in_pre_phi = var_23;
    if ((uint64_t)(uint32_t)var_24 == 0UL) {
        var_43 = *var_10;
        if (var_43 != 0UL & var_43 == *var_23) {
            var_44 = local_sp_6 + (-16L);
            *(uint64_t *)var_44 = 4221976UL;
            indirect_placeholder();
            local_sp_5 = var_44;
        }
        var_45 = *var_11;
        if (var_45 != 0UL & var_45 == *var_23) {
            *(uint64_t *)(local_sp_5 + (-8L)) = 4222005UL;
            indirect_placeholder();
        }
    } else {
        var_26 = *var_4;
        var_27 = local_sp_6 + (-16L);
        *(uint64_t *)var_27 = 4221716UL;
        var_28 = indirect_placeholder_2(var_26);
        local_sp_1 = var_27;
        local_sp_3 = var_27;
        rax_0_in_pre_phi = var_7;
        if ((uint64_t)(unsigned char)var_28 == 0UL) {
            var_29 = *var_12;
            if (var_29 != 0UL) {
                var_30 = local_sp_6 + (-24L);
                *(uint64_t *)var_30 = 4221746UL;
                var_31 = indirect_placeholder_2(var_29);
                local_sp_1 = var_30;
                local_sp_3 = var_30;
                if ((uint64_t)(unsigned char)var_31 != 0UL) {
                    local_sp_4 = local_sp_3;
                    if (*var_10 == 0UL) {
                        var_35 = local_sp_3 + (-8L);
                        *(uint64_t *)var_35 = 4221799UL;
                        indirect_placeholder();
                        local_sp_4 = var_35;
                    }
                    if (*var_11 != 0UL) {
                        *(uint64_t *)(local_sp_4 + (-8L)) = 4221818UL;
                        indirect_placeholder();
                    }
                    return *rax_0_in_pre_phi;
                }
            }
            var_32 = *var_14;
            local_sp_2 = local_sp_1;
            if (var_32 != 0UL) {
                var_33 = local_sp_1 + (-8L);
                *(uint64_t *)var_33 = 4221776UL;
                var_34 = indirect_placeholder_2(var_32);
                local_sp_2 = var_33;
                local_sp_3 = var_33;
                if ((uint64_t)(unsigned char)var_34 != 0UL) {
                    local_sp_4 = local_sp_3;
                    if (*var_10 == 0UL) {
                        var_35 = local_sp_3 + (-8L);
                        *(uint64_t *)var_35 = 4221799UL;
                        indirect_placeholder();
                        local_sp_4 = var_35;
                    }
                    if (*var_11 != 0UL) {
                        *(uint64_t *)(local_sp_4 + (-8L)) = 4221818UL;
                        indirect_placeholder();
                    }
                    return *rax_0_in_pre_phi;
                }
            }
            var_36 = *var_7;
            *(uint64_t *)(local_sp_2 + (-8L)) = 4221839UL;
            indirect_placeholder();
            var_37 = *var_23;
            *(uint64_t *)(local_sp_2 + (-16L)) = 4221854UL;
            indirect_placeholder();
            var_38 = (var_37 + var_36) + 4UL;
            *(uint64_t *)(local_sp_2 + (-24L)) = 4221869UL;
            var_39 = indirect_placeholder_2(var_38);
            var_40 = (uint64_t *)(var_0 + (-88L));
            *var_40 = var_39;
            var_41 = local_sp_2 + (-32L);
            *(uint64_t *)var_41 = 4221903UL;
            indirect_placeholder();
            local_sp_0 = var_41;
            rax_0_in_pre_phi = var_40;
            if (*var_10 == 0UL) {
                var_42 = local_sp_2 + (-40L);
                *(uint64_t *)var_42 = 4221922UL;
                indirect_placeholder();
                local_sp_0 = var_42;
            }
            if (*var_11 == 0UL) {
                *(uint64_t *)(local_sp_0 + (-8L)) = 4221941UL;
                indirect_placeholder();
            }
        } else {
            local_sp_4 = local_sp_3;
            if (*var_10 == 0UL) {
                var_35 = local_sp_3 + (-8L);
                *(uint64_t *)var_35 = 4221799UL;
                indirect_placeholder();
                local_sp_4 = var_35;
            }
            if (*var_11 == 0UL) {
                *(uint64_t *)(local_sp_4 + (-8L)) = 4221818UL;
                indirect_placeholder();
            }
        }
    }
}
