typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct helper_divq_EAX_wrapper_ret_type;
struct type_4;
struct helper_divq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint32_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint32_t field_6;
    uint64_t field_7;
    uint32_t field_8;
    uint32_t field_9;
    unsigned char field_10;
    uint32_t field_11;
    uint32_t field_12;
};
struct type_4 {
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern uint32_t init_state_0x8248(void);
extern struct helper_divq_EAX_wrapper_ret_type helper_divq_EAX_wrapper(struct type_4 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint64_t param_13, uint32_t param_14, uint64_t param_15, uint64_t param_16, uint64_t param_17, uint32_t param_18, uint32_t param_19);
extern void indirect_placeholder_44(uint64_t param_0);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
uint64_t bb_knuth_morris_pratt(uint64_t rcx, uint64_t rdx, uint64_t rsi, uint64_t rdi, uint64_t r10, uint64_t r9, uint64_t r8, uint64_t rbx) {
    uint64_t rax_2;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint32_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint32_t var_8;
    uint32_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t *var_13;
    uint64_t *var_14;
    uint64_t var_15;
    uint64_t *var_16;
    uint64_t var_17;
    uint64_t var_27;
    uint64_t var_18;
    uint64_t var_20;
    struct helper_divq_EAX_wrapper_ret_type var_19;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t local_sp_0;
    uint64_t *var_23;
    uint64_t storemerge;
    uint64_t *var_24;
    uint64_t *var_25;
    unsigned char *var_26;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t **var_38;
    uint64_t *var_39;
    uint64_t var_40;
    uint64_t *var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t *var_44;
    unsigned char var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_49;
    uint64_t var_48;
    unsigned char var_30;
    unsigned char var_31;
    uint64_t var_32;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_34;
    uint64_t var_37;
    uint64_t var_33;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_cc_src2();
    var_2 = init_rbp();
    var_3 = init_state_0x9018();
    var_4 = init_state_0x9010();
    var_5 = init_state_0x8408();
    var_6 = init_state_0x8328();
    var_7 = init_state_0x82d8();
    var_8 = init_state_0x9080();
    var_9 = init_state_0x8248();
    var_10 = var_0 + (-8L);
    *(uint64_t *)var_10 = var_2;
    var_11 = var_0 + (-104L);
    var_12 = (uint64_t *)(var_0 + (-80L));
    *var_12 = rdi;
    var_13 = (uint64_t *)(var_0 + (-88L));
    *var_13 = rsi;
    var_14 = (uint64_t *)(var_0 + (-96L));
    *var_14 = rdx;
    *(uint64_t *)var_11 = rcx;
    var_15 = *var_14;
    var_16 = (uint64_t *)(var_0 + (-56L));
    *var_16 = var_15;
    var_17 = var_15 >> 61UL;
    rax_2 = 0UL;
    local_sp_0 = var_11;
    storemerge = 0UL;
    var_27 = 2UL;
    if (!(((var_15 & 1152921504606846976UL) != 0UL) || (((var_17 == 0UL) ? 1UL : 0UL) == 0UL))) {
        var_18 = var_15 << 3UL;
        if (var_18 > 4015UL) {
            var_21 = var_0 + (-112L);
            *(uint64_t *)var_21 = 4238593UL;
            var_22 = indirect_placeholder_2(var_18);
            rax_2 = var_22;
            local_sp_0 = var_21;
        } else {
            var_19 = helper_divq_EAX_wrapper((struct type_4 *)(0UL), 16UL, 4238546UL, var_18 + 39UL, var_10, var_17, 0UL, 16UL, rdi, r10, r9, r8, rbx, var_3, var_4, var_5, var_6, var_7, var_8, var_9);
            var_20 = var_11 - (var_19.field_1 << 4UL);
            rax_2 = (var_20 + 31UL) & (-16L);
            local_sp_0 = var_20;
        }
    }
    var_23 = (uint64_t *)(var_0 + (-64L));
    *var_23 = rax_2;
    if (rax_2 == 0UL) {
        return storemerge;
    }
    *(uint64_t *)(rax_2 + 8UL) = 1UL;
    var_24 = (uint64_t *)(var_0 + (-24L));
    *var_24 = 0UL;
    var_25 = (uint64_t *)(var_0 + (-16L));
    *var_25 = 2UL;
    var_26 = (unsigned char *)(var_0 + (-65L));
    storemerge = 1UL;
    var_28 = *var_16;
    var_29 = helper_cc_compute_c_wrapper(var_27 - var_28, var_28, var_1, 17U);
    while (var_29 != 0UL)
        {
            var_30 = *(unsigned char *)(*var_13 + (*var_25 + (-1L)));
            *var_26 = var_30;
            var_31 = var_30;
            var_32 = *var_24;
            while (1U)
                {
                    if ((uint64_t)(var_31 - *(unsigned char *)(var_32 + *var_13)) != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    if (var_32 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_33 = var_32 - *(uint64_t *)(*var_23 + (var_32 << 3UL));
                    *var_24 = var_33;
                    var_31 = *var_26;
                    var_32 = var_33;
                    continue;
                }
            switch (loop_state_var) {
              case 0U:
                {
                    var_35 = var_32 + 1UL;
                    *var_24 = var_35;
                    var_36 = *var_25;
                    *(uint64_t *)((var_36 << 3UL) + *var_23) = (var_36 - var_35);
                }
                break;
              case 1U:
                {
                    var_34 = *var_25;
                    *(uint64_t *)((var_34 << 3UL) + *var_23) = var_34;
                }
                break;
            }
            var_37 = *var_25 + 1UL;
            *var_25 = var_37;
            var_27 = var_37;
            var_28 = *var_16;
            var_29 = helper_cc_compute_c_wrapper(var_27 - var_28, var_28, var_1, 17U);
        }
    var_38 = (uint64_t **)var_11;
    **var_38 = 0UL;
    var_39 = (uint64_t *)(var_0 + (-32L));
    *var_39 = 0UL;
    var_40 = *var_12;
    var_41 = (uint64_t *)(var_0 + (-40L));
    *var_41 = var_40;
    var_42 = *var_12;
    var_43 = var_0 + (-48L);
    var_44 = (uint64_t *)var_43;
    *var_44 = var_42;
    var_45 = **(unsigned char **)var_43;
    while (var_45 != '\x00')
        {
            var_46 = *var_13;
            var_47 = *var_39;
            if ((uint64_t)(*(unsigned char *)(var_47 + var_46) - var_45) == 0UL) {
                *var_39 = (var_47 + 1UL);
                *var_44 = (*var_44 + 1UL);
                if (*var_39 != *var_16) {
                    **var_38 = *var_41;
                    break;
                }
                var_45 = **(unsigned char **)var_43;
                continue;
            }
            if (var_47 == 0UL) {
                *var_41 = (*var_41 + 1UL);
                *var_44 = (*var_44 + 1UL);
            } else {
                *var_41 = (*var_41 + *(uint64_t *)(*var_23 + (var_47 << 3UL)));
                var_48 = *var_39;
                *var_39 = (var_48 - *(uint64_t *)(*var_23 + (var_48 << 3UL)));
            }
            var_45 = **(unsigned char **)var_43;
        }
    var_49 = *var_23;
    *(uint64_t *)(local_sp_0 + (-8L)) = 4239012UL;
    indirect_placeholder_44(var_49);
    return storemerge;
}
