typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_36_ret_type;
struct indirect_placeholder_37_ret_type;
struct indirect_placeholder_36_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_37_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_38(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t helper_ctz_wrapper(uint64_t param_0);
extern struct indirect_placeholder_36_ret_type indirect_placeholder_36(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_37_ret_type indirect_placeholder_37(uint64_t param_0, uint64_t param_1, uint64_t param_2);
uint64_t bb_factor_using_division(uint64_t rcx, uint64_t rdx, uint64_t rsi, uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t local_sp_0;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint32_t *var_16;
    uint32_t var_17;
    uint32_t *_pre_phi202;
    uint64_t var_10;
    uint32_t *var_11;
    uint32_t var_12;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t *var_21;
    uint32_t *var_22;
    uint64_t *var_23;
    uint64_t *var_24;
    uint64_t *var_25;
    uint64_t *var_26;
    uint32_t var_41;
    uint32_t var_27;
    uint64_t local_sp_1;
    uint64_t var_28;
    uint32_t var_29;
    uint64_t local_sp_2;
    uint64_t var_30;
    unsigned __int128 var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint32_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint32_t var_42;
    uint32_t _pre193;
    uint64_t var_43;
    uint64_t *var_44;
    uint64_t *var_45;
    uint32_t var_46;
    uint64_t local_sp_3;
    uint64_t var_61;
    uint64_t local_sp_4;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_52;
    uint64_t local_sp_5;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_58;
    uint64_t local_sp_6;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_64;
    uint64_t local_sp_7;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_70;
    uint64_t local_sp_8;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_76;
    uint64_t local_sp_9;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_82;
    uint64_t local_sp_10;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_88;
    uint64_t local_sp_11;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_95;
    uint32_t var_96;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_cc_src2();
    var_2 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    var_3 = var_0 + (-120L);
    var_4 = var_0 + (-96L);
    var_5 = (uint64_t *)var_4;
    *var_5 = rdi;
    var_6 = (uint64_t *)(var_0 + (-104L));
    *var_6 = rsi;
    var_7 = (uint64_t *)(var_0 + (-112L));
    *var_7 = rdx;
    var_8 = (uint64_t *)var_3;
    *var_8 = rcx;
    var_9 = *var_7;
    local_sp_0 = var_3;
    var_27 = 0U;
    if ((var_9 & 1UL) != 0UL) {
        if (var_9 == 0UL) {
            var_13 = *var_6;
            var_14 = helper_ctz_wrapper(var_13);
            var_15 = (var_13 == 0UL) ? 0UL : var_14;
            var_16 = (uint32_t *)(var_0 + (-12L));
            var_17 = (uint32_t)var_15;
            *var_16 = var_17;
            *var_7 = (*var_6 >> (uint64_t)(var_17 & 63U));
            *var_6 = 0UL;
            *var_16 = (*var_16 + 64U);
            _pre_phi202 = var_16;
        } else {
            var_10 = helper_ctz_wrapper(var_9);
            var_11 = (uint32_t *)(var_0 + (-12L));
            var_12 = (uint32_t)var_10;
            *var_11 = var_12;
            *var_7 = ((*var_7 >> (uint64_t)(var_12 & 63U)) | (*var_6 << (uint64_t)((0U - var_12) & 63U)));
            *var_6 = (*var_6 >> (uint64_t)(*var_11 & 63U));
            _pre_phi202 = var_11;
        }
        var_18 = (uint64_t)*_pre_phi202;
        var_19 = *var_8;
        var_20 = var_0 + (-128L);
        *(uint64_t *)var_20 = 4206145UL;
        indirect_placeholder_36(var_18, 2UL, var_19);
        local_sp_0 = var_20;
    }
    var_21 = (uint64_t *)(var_0 + (-24L));
    *var_21 = 3UL;
    var_22 = (uint32_t *)(var_0 + (-28L));
    *var_22 = 0U;
    var_23 = (uint64_t *)(var_0 + (-40L));
    var_24 = (uint64_t *)(var_0 + (-48L));
    var_25 = (uint64_t *)(var_0 + (-56L));
    var_26 = (uint64_t *)(var_0 + (-64L));
    local_sp_1 = local_sp_0;
    var_28 = *var_6;
    var_29 = var_27;
    local_sp_2 = local_sp_1;
    _pre193 = var_27;
    local_sp_3 = local_sp_1;
    while (var_28 != 0UL)
        {
            while (1U)
                {
                    var_30 = *var_7 * *(uint64_t *)(((uint64_t)var_29 << 4UL) + 4298720UL);
                    *var_23 = var_30;
                    var_31 = (unsigned __int128)*var_21 * (unsigned __int128)var_30;
                    var_32 = (uint64_t)var_31;
                    var_33 = (uint64_t)(var_31 >> 64ULL);
                    *var_24 = var_32;
                    *var_25 = var_33;
                    var_34 = *var_6;
                    local_sp_1 = local_sp_2;
                    if (var_33 <= var_34) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_35 = var_34 - var_33;
                    *var_25 = var_35;
                    var_36 = var_35 * *(uint64_t *)(((uint64_t)*var_22 << 4UL) + 4298720UL);
                    *var_26 = var_36;
                    var_37 = *var_22;
                    var_41 = var_37;
                    if (var_36 <= *(uint64_t *)(((uint64_t)var_37 << 4UL) + 4298728UL)) {
                        loop_state_var = 1U;
                        break;
                    }
                    *var_6 = var_36;
                    *var_7 = *var_23;
                    var_38 = *var_21;
                    var_39 = *var_8;
                    var_40 = local_sp_2 + (-8L);
                    *(uint64_t *)var_40 = 4206330UL;
                    indirect_placeholder_37(1UL, var_38, var_39);
                    var_29 = *var_22;
                    local_sp_2 = var_40;
                    continue;
                }
            switch (loop_state_var) {
              case 0U:
                {
                    var_41 = *var_22;
                }
                break;
              case 1U:
                {
                }
                break;
            }
            *var_21 = (*var_21 + (uint64_t)*(unsigned char *)((uint64_t)(var_41 + 1U) + 4297312UL));
            var_42 = *var_22 + 1U;
            *var_22 = var_42;
            var_27 = var_42;
            var_28 = *var_6;
            var_29 = var_27;
            local_sp_2 = local_sp_1;
            _pre193 = var_27;
            local_sp_3 = local_sp_1;
        }
    if (*var_5 == 0UL) {
        **(uint64_t **)var_4 = var_28;
        _pre193 = *var_22;
    }
    var_43 = var_0 + (-72L);
    var_44 = (uint64_t *)var_43;
    var_45 = (uint64_t *)(var_0 + (-80L));
    var_46 = _pre193;
    local_sp_4 = local_sp_3;
    while (var_46 <= 667U)
        {
            *var_44 = (((uint64_t)var_46 << 4UL) + 4298720UL);
            var_47 = *var_7 * **(uint64_t **)var_43;
            *var_45 = var_47;
            var_48 = *var_44;
            var_52 = var_48;
            local_sp_5 = local_sp_4;
            while (var_47 <= *(uint64_t *)(var_48 + 8UL))
                {
                    *var_7 = var_47;
                    var_49 = (uint64_t)(*var_22 + 1U);
                    var_50 = *var_21;
                    var_51 = local_sp_4 + (-8L);
                    *(uint64_t *)var_51 = 4206508UL;
                    indirect_placeholder_38(0UL, var_49, var_50);
                    local_sp_4 = var_51;
                    var_47 = *var_7 * **(uint64_t **)var_43;
                    *var_45 = var_47;
                    var_48 = *var_44;
                    var_52 = var_48;
                    local_sp_5 = local_sp_4;
                }
            var_53 = *var_7 * *(uint64_t *)(var_52 + 16UL);
            *var_45 = var_53;
            var_54 = *var_44;
            var_58 = var_54;
            local_sp_6 = local_sp_5;
            while (var_53 <= *(uint64_t *)(var_54 + 24UL))
                {
                    *var_7 = var_53;
                    var_55 = (uint64_t)(*var_22 + 1U);
                    var_56 = *var_21;
                    var_57 = local_sp_5 + (-8L);
                    *(uint64_t *)var_57 = 4206597UL;
                    indirect_placeholder_38(1UL, var_55, var_56);
                    var_52 = *var_44;
                    local_sp_5 = var_57;
                    var_53 = *var_7 * *(uint64_t *)(var_52 + 16UL);
                    *var_45 = var_53;
                    var_54 = *var_44;
                    var_58 = var_54;
                    local_sp_6 = local_sp_5;
                }
            var_59 = *var_7 * *(uint64_t *)(var_58 + 32UL);
            *var_45 = var_59;
            var_60 = *var_44;
            var_64 = var_60;
            local_sp_7 = local_sp_6;
            while (var_59 <= *(uint64_t *)(var_60 + 40UL))
                {
                    *var_7 = var_59;
                    var_61 = (uint64_t)(*var_22 + 1U);
                    var_62 = *var_21;
                    var_63 = local_sp_6 + (-8L);
                    *(uint64_t *)var_63 = 4206686UL;
                    indirect_placeholder_38(2UL, var_61, var_62);
                    var_58 = *var_44;
                    local_sp_6 = var_63;
                    var_59 = *var_7 * *(uint64_t *)(var_58 + 32UL);
                    *var_45 = var_59;
                    var_60 = *var_44;
                    var_64 = var_60;
                    local_sp_7 = local_sp_6;
                }
            var_65 = *var_7 * *(uint64_t *)(var_64 + 48UL);
            *var_45 = var_65;
            var_66 = *var_44;
            var_70 = var_66;
            local_sp_8 = local_sp_7;
            while (var_65 <= *(uint64_t *)(var_66 + 56UL))
                {
                    *var_7 = var_65;
                    var_67 = (uint64_t)(*var_22 + 1U);
                    var_68 = *var_21;
                    var_69 = local_sp_7 + (-8L);
                    *(uint64_t *)var_69 = 4206775UL;
                    indirect_placeholder_38(3UL, var_67, var_68);
                    var_64 = *var_44;
                    local_sp_7 = var_69;
                    var_65 = *var_7 * *(uint64_t *)(var_64 + 48UL);
                    *var_45 = var_65;
                    var_66 = *var_44;
                    var_70 = var_66;
                    local_sp_8 = local_sp_7;
                }
            var_71 = *var_7 * *(uint64_t *)(var_70 + 64UL);
            *var_45 = var_71;
            var_72 = *var_44;
            var_76 = var_72;
            local_sp_9 = local_sp_8;
            while (var_71 <= *(uint64_t *)(var_72 + 72UL))
                {
                    *var_7 = var_71;
                    var_73 = (uint64_t)(*var_22 + 1U);
                    var_74 = *var_21;
                    var_75 = local_sp_8 + (-8L);
                    *(uint64_t *)var_75 = 4206864UL;
                    indirect_placeholder_38(4UL, var_73, var_74);
                    var_70 = *var_44;
                    local_sp_8 = var_75;
                    var_71 = *var_7 * *(uint64_t *)(var_70 + 64UL);
                    *var_45 = var_71;
                    var_72 = *var_44;
                    var_76 = var_72;
                    local_sp_9 = local_sp_8;
                }
            var_77 = *var_7 * *(uint64_t *)(var_76 + 80UL);
            *var_45 = var_77;
            var_78 = *var_44;
            var_82 = var_78;
            local_sp_10 = local_sp_9;
            while (var_77 <= *(uint64_t *)(var_78 + 88UL))
                {
                    *var_7 = var_77;
                    var_79 = (uint64_t)(*var_22 + 1U);
                    var_80 = *var_21;
                    var_81 = local_sp_9 + (-8L);
                    *(uint64_t *)var_81 = 4206953UL;
                    indirect_placeholder_38(5UL, var_79, var_80);
                    var_76 = *var_44;
                    local_sp_9 = var_81;
                    var_77 = *var_7 * *(uint64_t *)(var_76 + 80UL);
                    *var_45 = var_77;
                    var_78 = *var_44;
                    var_82 = var_78;
                    local_sp_10 = local_sp_9;
                }
            var_83 = *var_7 * *(uint64_t *)(var_82 + 96UL);
            *var_45 = var_83;
            var_84 = *var_44;
            var_88 = var_84;
            local_sp_11 = local_sp_10;
            while (var_83 <= *(uint64_t *)(var_84 + 104UL))
                {
                    *var_7 = var_83;
                    var_85 = (uint64_t)(*var_22 + 1U);
                    var_86 = *var_21;
                    var_87 = local_sp_10 + (-8L);
                    *(uint64_t *)var_87 = 4207042UL;
                    indirect_placeholder_38(6UL, var_85, var_86);
                    var_82 = *var_44;
                    local_sp_10 = var_87;
                    var_83 = *var_7 * *(uint64_t *)(var_82 + 96UL);
                    *var_45 = var_83;
                    var_84 = *var_44;
                    var_88 = var_84;
                    local_sp_11 = local_sp_10;
                }
            var_89 = *var_7 * *(uint64_t *)(var_88 + 112UL);
            *var_45 = var_89;
            local_sp_3 = local_sp_11;
            while (var_89 <= *(uint64_t *)(*var_44 + 120UL))
                {
                    *var_7 = var_89;
                    var_90 = (uint64_t)(*var_22 + 1U);
                    var_91 = *var_21;
                    var_92 = local_sp_11 + (-8L);
                    *(uint64_t *)var_92 = 4207129UL;
                    indirect_placeholder_38(7UL, var_90, var_91);
                    var_88 = *var_44;
                    local_sp_11 = var_92;
                    var_89 = *var_7 * *(uint64_t *)(var_88 + 112UL);
                    *var_45 = var_89;
                    local_sp_3 = local_sp_11;
                }
            var_93 = *var_21 + (uint64_t)*(unsigned char *)((uint64_t)*var_22 + 4298016UL);
            *var_21 = var_93;
            var_94 = var_93 * var_93;
            var_95 = helper_cc_compute_c_wrapper(*var_7 - var_94, var_94, var_1, 17U);
            if (var_95 == 0UL) {
                break;
            }
            var_96 = *var_22 + 8U;
            *var_22 = var_96;
            var_46 = var_96;
            local_sp_4 = local_sp_3;
        }
    return *var_7;
}
