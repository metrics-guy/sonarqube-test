typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_18_ret_type;
struct indirect_placeholder_19_ret_type;
struct indirect_placeholder_17_ret_type;
struct indirect_placeholder_20_ret_type;
struct indirect_placeholder_16_ret_type;
struct indirect_placeholder_15_ret_type;
struct indirect_placeholder_22_ret_type;
struct indirect_placeholder_6_ret_type;
struct indirect_placeholder_7_ret_type;
struct indirect_placeholder_10_ret_type;
struct indirect_placeholder_11_ret_type;
struct indirect_placeholder_12_ret_type;
struct indirect_placeholder_8_ret_type;
struct indirect_placeholder_13_ret_type;
struct indirect_placeholder_18_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_19_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_17_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_20_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_16_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_15_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_22_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_6_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_7_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_10_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_11_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_12_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_8_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_13_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_1(void);
extern void indirect_placeholder_9(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_5(uint64_t param_0);
extern struct indirect_placeholder_18_ret_type indirect_placeholder_18(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_19_ret_type indirect_placeholder_19(uint64_t param_0);
extern struct indirect_placeholder_17_ret_type indirect_placeholder_17(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_20_ret_type indirect_placeholder_20(uint64_t param_0);
extern struct indirect_placeholder_16_ret_type indirect_placeholder_16(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_15_ret_type indirect_placeholder_15(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_21(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_22_ret_type indirect_placeholder_22(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern void indirect_placeholder_14(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_6_ret_type indirect_placeholder_6(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_7_ret_type indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_10_ret_type indirect_placeholder_10(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_11_ret_type indirect_placeholder_11(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_12_ret_type indirect_placeholder_12(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_8_ret_type indirect_placeholder_8(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_13_ret_type indirect_placeholder_13(uint64_t param_0, uint64_t param_1);
void bb_main(uint64_t rdi, uint64_t rsi) {
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    unsigned char storemerge7;
    uint32_t spec_select;
    uint32_t *var_23;
    uint32_t storemerge;
    uint32_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_145;
    uint64_t var_139;
    struct indirect_placeholder_19_ret_type var_140;
    uint64_t var_141;
    uint64_t var_142;
    uint64_t var_143;
    uint64_t var_144;
    uint64_t var_132;
    struct indirect_placeholder_20_ret_type var_133;
    uint64_t var_134;
    uint64_t var_135;
    uint64_t var_136;
    uint64_t var_137;
    uint64_t var_30;
    struct indirect_placeholder_15_ret_type var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t local_sp_17;
    uint64_t var_146;
    uint64_t var_147;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint32_t var_17;
    struct indirect_placeholder_22_ret_type var_16;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint32_t *var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint32_t *var_12;
    uint64_t local_sp_18;
    uint64_t rax_1;
    uint64_t rcx_7;
    uint64_t var_131;
    uint64_t r9_1;
    uint64_t r8_1;
    uint64_t rcx_3;
    uint64_t var_114;
    uint64_t var_115;
    uint64_t r8_2;
    struct indirect_placeholder_7_ret_type var_116;
    uint64_t var_117;
    uint64_t var_118;
    uint64_t var_119;
    uint64_t local_sp_13;
    uint64_t var_112;
    uint64_t var_113;
    uint64_t local_sp_8;
    uint64_t var_62;
    uint32_t var_126;
    uint32_t var_124;
    uint64_t local_sp_0;
    uint64_t local_sp_4;
    uint64_t var_125;
    uint64_t local_sp_1;
    uint32_t var_127;
    uint32_t var_122;
    uint32_t var_120;
    uint64_t local_sp_2;
    uint64_t var_121;
    uint64_t local_sp_3;
    uint32_t var_123;
    uint32_t var_103;
    bool var_104;
    uint64_t local_sp_12_be;
    uint64_t var_128;
    uint64_t var_129;
    uint64_t var_130;
    uint64_t local_sp_12;
    uint64_t var_105;
    uint64_t local_sp_7;
    uint64_t var_106;
    uint64_t r9_2;
    uint64_t var_81;
    uint32_t var_82;
    uint64_t local_sp_5;
    uint64_t rcx_0;
    uint64_t var_35;
    uint32_t var_83;
    uint32_t var_70;
    uint32_t *var_72;
    uint32_t *var_73;
    uint32_t var_74;
    uint64_t rcx_4;
    uint64_t local_sp_6;
    uint64_t rcx_6;
    uint64_t rcx_1;
    uint64_t rcx_2;
    uint64_t var_107;
    uint64_t var_108;
    uint64_t *var_109;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t var_75;
    uint64_t var_76;
    struct indirect_placeholder_10_ret_type var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint32_t var_80;
    uint64_t local_sp_9;
    uint32_t var_84;
    uint32_t var_71;
    uint64_t var_56;
    uint64_t var_57;
    struct indirect_placeholder_11_ret_type var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t local_sp_11;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t rcx_8;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    bool var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t *var_51;
    uint64_t var_63;
    uint64_t var_64;
    unsigned char var_65;
    uint64_t var_66;
    uint64_t *var_67;
    uint64_t var_68;
    uint32_t *var_69;
    uint32_t *var_85;
    uint32_t var_86;
    uint32_t *var_88;
    uint64_t var_89;
    uint32_t *var_90;
    uint64_t var_91;
    uint32_t *var_92;
    uint32_t *var_93;
    uint32_t *var_94;
    uint32_t *var_95;
    uint64_t var_96;
    struct indirect_placeholder_12_ret_type var_97;
    uint64_t var_98;
    uint64_t var_99;
    struct indirect_placeholder_8_ret_type var_100;
    uint32_t var_101;
    uint32_t var_102;
    uint32_t var_87;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    struct indirect_placeholder_13_ret_type var_39;
    uint64_t var_40;
    uint64_t local_sp_14;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    bool var_44;
    uint64_t var_138;
    uint64_t local_sp_15;
    uint64_t local_sp_16;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint32_t var_21;
    uint32_t *var_22;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = var_0 + (-8L);
    *(uint64_t *)var_2 = var_1;
    var_3 = (uint32_t *)(var_0 + (-252L));
    *var_3 = (uint32_t)rdi;
    var_4 = var_0 + (-264L);
    var_5 = (uint64_t *)var_4;
    *var_5 = rsi;
    var_6 = var_0 + (-168L);
    var_7 = (uint64_t *)(var_0 + (-48L));
    *var_7 = var_6;
    var_8 = var_0 + (-232L);
    var_9 = (uint64_t *)(var_0 + (-16L));
    *var_9 = var_8;
    var_10 = **(uint64_t **)var_4;
    *(uint64_t *)(var_0 + (-272L)) = 4214035UL;
    indirect_placeholder_5(var_10);
    *(uint64_t *)(var_0 + (-280L)) = 4214050UL;
    indirect_placeholder_1();
    var_11 = var_0 + (-288L);
    *(uint64_t *)var_11 = 4214060UL;
    indirect_placeholder_1();
    var_12 = (uint32_t *)(var_0 + (-52L));
    storemerge7 = (unsigned char)'\x00';
    local_sp_18 = var_11;
    rax_1 = 0UL;
    var_124 = 0U;
    var_120 = 0U;
    var_70 = 0U;
    var_74 = 0U;
    var_86 = 0U;
    var_40 = 0UL;
    while (1U)
        {
            var_13 = *var_5;
            var_14 = (uint64_t)*var_3;
            var_15 = local_sp_18 + (-8L);
            *(uint64_t *)var_15 = 4214319UL;
            var_16 = indirect_placeholder_22(4285024UL, 4289979UL, var_14, var_13, 0UL);
            var_17 = (uint32_t)var_16.field_0;
            *var_12 = var_17;
            local_sp_17 = var_15;
            local_sp_18 = var_15;
            switch_state_var = 0;
            switch (var_17) {
              case 116U:
                {
                    *(unsigned char *)4309987UL = (unsigned char)'\x01';
                    continue;
                }
                break;
              case 4294967295U:
                {
                    var_18 = var_16.field_1;
                    var_19 = var_16.field_2;
                    var_20 = var_16.field_3;
                    var_21 = *var_3 - *(uint32_t *)4309816UL;
                    var_22 = (uint32_t *)(var_0 + (-56L));
                    *var_22 = var_21;
                    storemerge7 = (unsigned char)'\x01';
                    if (!(var_21 == 2U && *(unsigned char *)4309985UL == '\x01')) {
                        loop_state_var = 2U;
                        switch_state_var = 1;
                        break;
                    }
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
              default:
                {
                    if ((int)var_17 <= (int)116U) {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    if (var_17 == 115U) {
                        *(unsigned char *)4309984UL = (unsigned char)'\x01';
                        continue;
                    }
                    if ((int)var_17 <= (int)115U) {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    if (var_17 == 100U) {
                        *(unsigned char *)4309985UL = (unsigned char)'\x01';
                        continue;
                    }
                    if ((int)var_17 <= (int)100U) {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    if (var_17 == 99U) {
                        *(unsigned char *)4309986UL = (unsigned char)'\x01';
                        continue;
                    }
                    if ((int)var_17 <= (int)99U) {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    if (var_17 != 67U) {
                        if ((int)var_17 <= (int)67U) {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        switch_state_var = 0;
                        switch (var_17) {
                          case 4294967166U:
                            {
                                *(uint64_t *)(local_sp_18 + (-16L)) = 4214211UL;
                                indirect_placeholder_9(var_2, 0UL);
                                abort();
                            }
                            break;
                          case 4294967165U:
                            {
                                var_146 = *(uint64_t *)4309664UL;
                                *(uint64_t *)(local_sp_18 + (-16L)) = 4214263UL;
                                indirect_placeholder_21(0UL, var_146, 4284568UL, 4287624UL, 0UL, 4289966UL);
                                var_147 = local_sp_18 + (-24L);
                                *(uint64_t *)var_147 = 4214273UL;
                                indirect_placeholder_1();
                                local_sp_17 = var_147;
                                loop_state_var = 0U;
                                switch_state_var = 1;
                                break;
                            }
                            break;
                          default:
                            {
                                loop_state_var = 0U;
                                switch_state_var = 1;
                                break;
                            }
                            break;
                        }
                        if (switch_state_var)
                            break;
                    }
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(uint64_t *)(local_sp_17 + (-8L)) = 4214283UL;
            indirect_placeholder_9(var_2, 1UL);
            abort();
        }
        break;
      case 2U:
      case 1U:
        {
            switch (loop_state_var) {
              case 2U:
                {
                }
                break;
              case 1U:
                {
                    *(unsigned char *)4309988UL = storemerge7;
                    spec_select = ((uint64_t)(*(unsigned char *)4309985UL - *(unsigned char *)4309984UL) == 0UL) ? 2U : 1U;
                    var_23 = (uint32_t *)(var_0 + (-60L));
                    *var_23 = spec_select;
                    storemerge = (*(unsigned char *)4309985UL > *(unsigned char *)4309984UL) ? 1U : 2U;
                    *(uint32_t *)(var_0 + (-64L)) = storemerge;
                    var_24 = *var_22;
                    var_25 = (uint64_t)*var_23;
                    var_26 = (uint64_t)var_24 << 32UL;
                    if ((long)var_26 < (long)(var_25 << 32UL)) {
                        if (var_24 == 0U) {
                            var_145 = local_sp_18 + (-16L);
                            *(uint64_t *)var_145 = 4214501UL;
                            indirect_placeholder_18(0UL, var_18, 4289986UL, 0UL, 0UL, var_19, var_20);
                            local_sp_16 = var_145;
                        } else {
                            var_139 = *(uint64_t *)(*var_5 + (((uint64_t)*var_3 << 3UL) + (-8L)));
                            *(uint64_t *)(local_sp_18 + (-16L)) = 4214540UL;
                            var_140 = indirect_placeholder_19(var_139);
                            var_141 = var_140.field_0;
                            var_142 = var_140.field_1;
                            var_143 = var_140.field_2;
                            *(uint64_t *)(local_sp_18 + (-24L)) = 4214568UL;
                            indirect_placeholder_17(0UL, var_141, 4290002UL, 0UL, 0UL, var_142, var_143);
                            var_144 = local_sp_18 + (-32L);
                            *(uint64_t *)var_144 = 4214616UL;
                            indirect_placeholder_1();
                            local_sp_16 = var_144;
                        }
                        *(uint64_t *)(local_sp_16 + (-8L)) = 4214626UL;
                        indirect_placeholder_9(var_2, 1UL);
                        abort();
                    }
                    if ((long)((uint64_t)storemerge << 32UL) < (long)var_26) {
                        var_132 = *(uint64_t *)(*var_5 + (uint64_t)((long)((uint64_t)(*(uint32_t *)4309816UL + storemerge) << 32UL) >> (long)29UL));
                        *(uint64_t *)(local_sp_18 + (-16L)) = 4214676UL;
                        var_133 = indirect_placeholder_20(var_132);
                        var_134 = var_133.field_0;
                        var_135 = var_133.field_1;
                        var_136 = var_133.field_2;
                        var_137 = local_sp_18 + (-24L);
                        *(uint64_t *)var_137 = 4214704UL;
                        indirect_placeholder_16(0UL, var_134, 4290152UL, 0UL, 0UL, var_135, var_136);
                        local_sp_15 = var_137;
                        if (*var_22 == 2U) {
                            var_138 = local_sp_18 + (-32L);
                            *(uint64_t *)var_138 = 4214740UL;
                            indirect_placeholder_1();
                            local_sp_15 = var_138;
                        }
                        *(uint64_t *)(local_sp_15 + (-8L)) = 4214750UL;
                        indirect_placeholder_9(var_2, 1UL);
                        abort();
                    }
                    var_27 = *var_7;
                    *(uint64_t *)(local_sp_18 + (-16L)) = 4214762UL;
                    indirect_placeholder_5(var_27);
                    var_28 = *(uint64_t *)(*var_5 + ((uint64_t)*(uint32_t *)4309816UL << 3UL));
                    var_29 = *var_7;
                    var_30 = local_sp_18 + (-24L);
                    *(uint64_t *)var_30 = 4214806UL;
                    var_31 = indirect_placeholder_15(var_28, var_29);
                    var_32 = var_31.field_1;
                    var_33 = var_31.field_2;
                    var_34 = var_31.field_3;
                    local_sp_14 = var_30;
                    rcx_8 = var_32;
                    r9_2 = var_33;
                    r8_2 = var_34;
                    if ((uint64_t)(unsigned char)var_31.field_0 == 1UL) {
                        return;
                    }
                    if (*var_22 == 2U) {
                        *var_9 = 0UL;
                    } else {
                        var_35 = *var_9;
                        *(uint64_t *)(local_sp_18 + (-32L)) = 4214841UL;
                        indirect_placeholder_5(var_35);
                        var_36 = *(uint64_t *)(*var_5 + (((uint64_t)*(uint32_t *)4309816UL << 3UL) + 8UL));
                        var_37 = *var_9;
                        var_38 = local_sp_18 + (-40L);
                        *(uint64_t *)var_38 = 4214889UL;
                        var_39 = indirect_placeholder_13(var_36, var_37);
                        local_sp_14 = var_38;
                        rcx_8 = var_39.field_1;
                        r9_2 = var_39.field_2;
                        r8_2 = var_39.field_3;
                        if ((uint64_t)(unsigned char)var_39.field_0 != 1UL) {
                            return;
                        }
                        var_40 = *var_9;
                    }
                    var_41 = *var_7;
                    *(uint64_t *)(local_sp_14 + (-8L)) = 4214933UL;
                    indirect_placeholder_14(rcx_8, var_41, var_40, r9_2, r8_2);
                    *(uint64_t *)(local_sp_14 + (-16L)) = 4214948UL;
                    indirect_placeholder_9(0UL, 0UL);
                    *(uint64_t *)(local_sp_14 + (-24L)) = 4214963UL;
                    indirect_placeholder_9(1UL, 0UL);
                    var_42 = *(uint64_t *)4308552UL;
                    var_43 = local_sp_14 + (-32L);
                    *(uint64_t *)var_43 = 4214983UL;
                    indirect_placeholder_9(var_42, 2UL);
                    var_44 = (*(unsigned char *)4309984UL == '\x00');
                    rcx_7 = rcx_8;
                    r9_1 = r9_2;
                    r8_1 = r8_2;
                    local_sp_13 = var_43;
                    local_sp_12 = var_43;
                    rcx_1 = rcx_8;
                    if (!var_44) {
                        if (*var_22 != 1U) {
                            var_45 = (uint64_t)*(unsigned char *)4309986UL;
                            var_46 = *var_7;
                            *(uint64_t *)(local_sp_14 + (-40L)) = 4215029UL;
                            indirect_placeholder_9(4311040UL, var_45);
                            var_47 = local_sp_14 + (-48L);
                            *(uint64_t *)var_47 = 4215049UL;
                            indirect_placeholder_5(4213362UL);
                            local_sp_13 = var_47;
                            rax_1 = var_46;
                            rcx_7 = var_45;
                            *(uint64_t *)(local_sp_13 + (-8L)) = 4216110UL;
                            indirect_placeholder_1();
                            if ((uint64_t)(uint32_t)rax_1 != 0UL) {
                                *(uint64_t *)(local_sp_13 + (-16L)) = 4216119UL;
                                indirect_placeholder_1();
                                var_131 = (uint64_t)*(uint32_t *)rax_1;
                                *(uint64_t *)(local_sp_13 + (-24L)) = 4216143UL;
                                indirect_placeholder_6(0UL, rcx_7, 4290298UL, 1UL, var_131, r9_1, r8_1);
                            }
                            return;
                        }
                    }
                    var_48 = (*(unsigned char *)4309985UL == '\x00');
                    if (!var_48) {
                        if (*var_22 != 1U) {
                            var_49 = (uint64_t)*(unsigned char *)4309986UL;
                            var_50 = local_sp_14 + (-40L);
                            *(uint64_t *)var_50 = 4215108UL;
                            indirect_placeholder_9(4311296UL, var_49);
                            var_51 = (uint64_t *)(var_0 + (-72L));
                            local_sp_11 = var_50;
                            rcx_6 = var_49;
                            var_52 = local_sp_11 + (-8L);
                            *(uint64_t *)var_52 = 4215123UL;
                            var_53 = indirect_placeholder(4310016UL, 1024UL);
                            *var_51 = var_53;
                            local_sp_13 = var_52;
                            rcx_7 = rcx_6;
                            while (var_53 != 0UL)
                                {
                                    var_54 = *(uint64_t *)4308544UL;
                                    var_55 = local_sp_11 + (-16L);
                                    *(uint64_t *)var_55 = 4215166UL;
                                    indirect_placeholder_1();
                                    local_sp_11 = var_55;
                                    rcx_6 = var_54;
                                    if (*var_51 == var_53) {
                                        var_52 = local_sp_11 + (-8L);
                                        *(uint64_t *)var_52 = 4215123UL;
                                        var_53 = indirect_placeholder(4310016UL, 1024UL);
                                        *var_51 = var_53;
                                        local_sp_13 = var_52;
                                        rcx_7 = rcx_6;
                                        continue;
                                    }
                                    *(uint64_t *)(local_sp_11 + (-24L)) = 4215177UL;
                                    indirect_placeholder_1();
                                    var_56 = (uint64_t)*(uint32_t *)var_53;
                                    var_57 = local_sp_11 + (-32L);
                                    *(uint64_t *)var_57 = 4215201UL;
                                    var_58 = indirect_placeholder_11(0UL, var_54, 4289943UL, 1UL, var_56, r9_2, r8_2);
                                    var_59 = var_58.field_0;
                                    var_60 = var_58.field_1;
                                    var_61 = var_58.field_2;
                                    local_sp_13 = var_57;
                                    rcx_7 = var_59;
                                    r9_1 = var_60;
                                    r8_1 = var_61;
                                    break;
                                }
                            *(uint64_t *)(local_sp_13 + (-8L)) = 4216110UL;
                            indirect_placeholder_1();
                            if ((uint64_t)(uint32_t)rax_1 != 0UL) {
                                *(uint64_t *)(local_sp_13 + (-16L)) = 4216119UL;
                                indirect_placeholder_1();
                                var_131 = (uint64_t)*(uint32_t *)rax_1;
                                *(uint64_t *)(local_sp_13 + (-24L)) = 4216143UL;
                                indirect_placeholder_6(0UL, rcx_7, 4290298UL, 1UL, var_131, r9_1, r8_1);
                            }
                            return;
                        }
                    }
                    if (!var_44) {
                        if (!var_48 & *var_22 != 2U) {
                            var_62 = (uint64_t)*(unsigned char *)4309986UL;
                            *(uint64_t *)(local_sp_14 + (-40L)) = 4215264UL;
                            indirect_placeholder_9(4311296UL, var_62);
                            var_63 = *var_9;
                            *(uint64_t *)(local_sp_14 + (-48L)) = 4215286UL;
                            indirect_placeholder_9(4311040UL, 0UL);
                            var_64 = local_sp_14 + (-56L);
                            *(uint64_t *)var_64 = 4215306UL;
                            indirect_placeholder_5(4213445UL);
                            local_sp_13 = var_64;
                            rax_1 = var_63;
                            rcx_7 = var_62;
                            *(uint64_t *)(local_sp_13 + (-8L)) = 4216110UL;
                            indirect_placeholder_1();
                            if ((uint64_t)(uint32_t)rax_1 != 0UL) {
                                *(uint64_t *)(local_sp_13 + (-16L)) = 4216119UL;
                                indirect_placeholder_1();
                                var_131 = (uint64_t)*(uint32_t *)rax_1;
                                *(uint64_t *)(local_sp_13 + (-24L)) = 4216143UL;
                                indirect_placeholder_6(0UL, rcx_7, 4290298UL, 1UL, var_131, r9_1, r8_1);
                            }
                            return;
                        }
                    }
                    var_65 = *(unsigned char *)4309988UL;
                    var_66 = (uint64_t)var_65;
                    rax_1 = var_66;
                    if (var_65 != '\x00') {
                        rax_1 = 0UL;
                        if (*(unsigned char *)4309986UL != '\x00') {
                            var_67 = (uint64_t *)(var_0 + (-88L));
                            *var_67 = 4311296UL;
                            var_68 = local_sp_14 + (-40L);
                            *(uint64_t *)var_68 = 4215370UL;
                            indirect_placeholder_9(4311296UL, 0UL);
                            *(uint64_t *)(*var_9 + 16UL) = 18446744073709551614UL;
                            var_69 = (uint32_t *)(var_0 + (-20L));
                            *var_69 = 0U;
                            local_sp_6 = var_68;
                            while ((int)var_70 <= (int)255U)
                                {
                                    *(unsigned char *)((uint64_t)var_70 + 4311552UL) = (unsigned char)var_70;
                                    var_71 = *var_69 + 1U;
                                    *var_69 = var_71;
                                    var_70 = var_71;
                                }
                            var_72 = (uint32_t *)(var_0 + (-24L));
                            *var_72 = 0U;
                            var_73 = (uint32_t *)(var_0 + (-92L));
                            local_sp_7 = local_sp_6;
                            var_83 = var_74;
                            rcx_4 = rcx_1;
                            rcx_2 = rcx_1;
                            local_sp_9 = local_sp_6;
                            while ((int)var_74 <= (int)255U)
                                {
                                    if (*(unsigned char *)(*var_67 + (uint64_t)var_74) != '\x01') {
                                        var_75 = *var_9;
                                        var_76 = local_sp_6 + (-8L);
                                        *(uint64_t *)var_76 = 4215469UL;
                                        var_77 = indirect_placeholder_10(var_75, 0UL);
                                        var_78 = var_77.field_0;
                                        var_79 = var_77.field_1;
                                        var_80 = (uint32_t)var_78;
                                        *var_73 = var_80;
                                        var_82 = var_80;
                                        local_sp_5 = var_76;
                                        rcx_0 = var_79;
                                        var_82 = 4294967295U;
                                        if (var_80 != 4294967295U & *(unsigned char *)4309987UL == '\x00') {
                                            var_81 = local_sp_6 + (-16L);
                                            *(uint64_t *)var_81 = 4215514UL;
                                            indirect_placeholder_1();
                                            var_82 = *var_73;
                                            local_sp_5 = var_81;
                                            rcx_0 = 4290567UL;
                                        }
                                        local_sp_7 = local_sp_5;
                                        rcx_2 = rcx_0;
                                        local_sp_9 = local_sp_5;
                                        rcx_4 = rcx_0;
                                        if (var_82 != 4294967295U) {
                                            break;
                                        }
                                        *(unsigned char *)((uint64_t)*var_72 + 4311552UL) = (unsigned char)var_82;
                                        var_83 = *var_72;
                                    }
                                    var_84 = var_83 + 1U;
                                    *var_72 = var_84;
                                    var_74 = var_84;
                                    local_sp_6 = local_sp_9;
                                    rcx_1 = rcx_4;
                                    local_sp_7 = local_sp_6;
                                    var_83 = var_74;
                                    rcx_4 = rcx_1;
                                    rcx_2 = rcx_1;
                                    local_sp_9 = local_sp_6;
                                }
                        }
                        var_85 = (uint32_t *)(var_0 + (-28L));
                        *var_85 = 0U;
                        while ((int)var_86 <= (int)255U)
                            {
                                *(unsigned char *)((uint64_t)var_86 + 4311552UL) = (unsigned char)var_86;
                                var_87 = *var_85 + 1U;
                                *var_85 = var_87;
                                var_86 = var_87;
                            }
                        *(uint64_t *)(*var_7 + 16UL) = 18446744073709551614UL;
                        *(uint64_t *)(*var_9 + 16UL) = 18446744073709551614UL;
                        var_88 = (uint32_t *)(var_0 + (-80L));
                        var_89 = var_0 + (-236L);
                        var_90 = (uint32_t *)(var_0 + (-76L));
                        var_91 = var_0 + (-240L);
                        var_92 = (uint32_t *)var_89;
                        var_93 = (uint32_t *)var_91;
                        var_94 = (uint32_t *)(var_0 + (-36L));
                        var_95 = (uint32_t *)(var_0 + (-32L));
                        while (1U)
                            {
                                var_96 = *var_7;
                                *(uint64_t *)(local_sp_12 + (-8L)) = 4215645UL;
                                var_97 = indirect_placeholder_12(var_96, var_89);
                                *var_90 = (uint32_t)var_97.field_0;
                                var_98 = *var_9;
                                var_99 = local_sp_12 + (-16L);
                                *(uint64_t *)var_99 = 4215670UL;
                                var_100 = indirect_placeholder_8(var_98, var_91);
                                var_101 = (uint32_t)var_100.field_0;
                                *var_88 = var_101;
                                var_102 = *var_92;
                                local_sp_0 = var_99;
                                local_sp_2 = var_99;
                                local_sp_4 = var_99;
                                local_sp_7 = var_99;
                                if (var_102 != 0U) {
                                    if (*var_93 != 1U) {
                                        *var_95 = 0U;
                                        var_126 = var_124;
                                        local_sp_1 = local_sp_0;
                                        local_sp_4 = local_sp_0;
                                        while ((int)var_124 <= (int)255U)
                                            {
                                                if ((var_124 + (-97)) > 25U) {
                                                    var_125 = local_sp_0 + (-8L);
                                                    *(uint64_t *)var_125 = 4215724UL;
                                                    indirect_placeholder_1();
                                                    *(unsigned char *)((uint64_t)*var_95 + 4311552UL) = (unsigned char)var_124;
                                                    var_126 = *var_95;
                                                    local_sp_1 = var_125;
                                                }
                                                var_127 = var_126 + 1U;
                                                *var_95 = var_127;
                                                var_124 = var_127;
                                                local_sp_0 = local_sp_1;
                                                var_126 = var_124;
                                                local_sp_1 = local_sp_0;
                                                local_sp_4 = local_sp_0;
                                            }
                                        local_sp_12_be = local_sp_4;
                                        if (*var_93 == 2U) {
                                            var_128 = *var_7;
                                            *(uint64_t *)(local_sp_4 + (-8L)) = 4215886UL;
                                            indirect_placeholder_5(var_128);
                                            var_129 = *var_9;
                                            var_130 = local_sp_4 + (-16L);
                                            *(uint64_t *)var_130 = 4215898UL;
                                            indirect_placeholder_5(var_129);
                                            local_sp_12_be = var_130;
                                        }
                                        local_sp_12 = local_sp_12_be;
                                        continue;
                                    }
                                }
                                if (var_102 == 1U) {
                                    if (*var_93 == 0U) {
                                        *var_94 = 0U;
                                        var_122 = var_120;
                                        local_sp_3 = local_sp_2;
                                        local_sp_4 = local_sp_2;
                                        while ((int)var_120 <= (int)255U)
                                            {
                                                if ((var_120 + (-65)) > 25U) {
                                                    var_121 = local_sp_2 + (-8L);
                                                    *(uint64_t *)var_121 = 4215803UL;
                                                    indirect_placeholder_1();
                                                    *(unsigned char *)((uint64_t)*var_94 + 4311552UL) = (unsigned char)var_120;
                                                    var_122 = *var_94;
                                                    local_sp_3 = var_121;
                                                }
                                                var_123 = var_122 + 1U;
                                                *var_94 = var_123;
                                                var_120 = var_123;
                                                local_sp_2 = local_sp_3;
                                                var_122 = var_120;
                                                local_sp_3 = local_sp_2;
                                                local_sp_4 = local_sp_2;
                                            }
                                    }
                                    var_103 = *var_90;
                                    var_104 = (var_103 == 4294967295U);
                                    if (var_104) {
                                        break;
                                    }
                                    if (var_101 != 4294967295U) {
                                        break;
                                    }
                                    *(unsigned char *)((uint64_t)var_103 + 4311552UL) = (unsigned char)var_101;
                                } else {
                                    var_103 = *var_90;
                                    var_104 = (var_103 == 4294967295U);
                                    if (var_104) {
                                        break;
                                    }
                                    if (var_101 == 4294967295U) {
                                        break;
                                    }
                                    *(unsigned char *)((uint64_t)var_103 + 4311552UL) = (unsigned char)var_101;
                                }
                                local_sp_12_be = local_sp_4;
                                if (*var_93 == 2U) {
                                    var_128 = *var_7;
                                    *(uint64_t *)(local_sp_4 + (-8L)) = 4215886UL;
                                    indirect_placeholder_5(var_128);
                                    var_129 = *var_9;
                                    var_130 = local_sp_4 + (-16L);
                                    *(uint64_t *)var_130 = 4215898UL;
                                    indirect_placeholder_5(var_129);
                                    local_sp_12_be = var_130;
                                }
                                local_sp_12 = local_sp_12_be;
                                continue;
                            }
                        var_105 = var_100.field_1;
                        rcx_2 = var_105;
                        if (!var_104 && *(unsigned char *)4309987UL == '\x00') {
                            var_106 = local_sp_12 + (-24L);
                            *(uint64_t *)var_106 = 4215945UL;
                            indirect_placeholder_1();
                            local_sp_7 = var_106;
                            rcx_2 = 4290567UL;
                        }
                        local_sp_8 = local_sp_7;
                        rcx_3 = rcx_2;
                        rcx_7 = rcx_2;
                        if (*(unsigned char *)4309984UL != '\x00') {
                            var_109 = (uint64_t *)(var_0 + (-104L));
                            var_110 = local_sp_8 + (-8L);
                            *(uint64_t *)var_110 = 4216021UL;
                            var_111 = indirect_placeholder(4310016UL, 1024UL);
                            *var_109 = var_111;
                            local_sp_13 = var_110;
                            rcx_7 = rcx_3;
                            while (var_111 != 0UL)
                                {
                                    var_112 = *(uint64_t *)4308544UL;
                                    var_113 = local_sp_8 + (-16L);
                                    *(uint64_t *)var_113 = 4216064UL;
                                    indirect_placeholder_1();
                                    local_sp_8 = var_113;
                                    rcx_3 = var_112;
                                    if (*var_109 == var_111) {
                                        var_110 = local_sp_8 + (-8L);
                                        *(uint64_t *)var_110 = 4216021UL;
                                        var_111 = indirect_placeholder(4310016UL, 1024UL);
                                        *var_109 = var_111;
                                        local_sp_13 = var_110;
                                        rcx_7 = rcx_3;
                                        continue;
                                    }
                                    *(uint64_t *)(local_sp_8 + (-24L)) = 4216075UL;
                                    indirect_placeholder_1();
                                    var_114 = (uint64_t)*(uint32_t *)var_111;
                                    var_115 = local_sp_8 + (-32L);
                                    *(uint64_t *)var_115 = 4216099UL;
                                    var_116 = indirect_placeholder_7(0UL, var_112, 4289943UL, 1UL, var_114, r9_2, r8_2);
                                    var_117 = var_116.field_0;
                                    var_118 = var_116.field_1;
                                    var_119 = var_116.field_2;
                                    local_sp_13 = var_115;
                                    rcx_7 = var_117;
                                    r9_1 = var_118;
                                    r8_1 = var_119;
                                    break;
                                }
                        }
                        var_107 = *var_9;
                        *(uint64_t *)(local_sp_7 + (-8L)) = 4215984UL;
                        indirect_placeholder_9(4311040UL, 0UL);
                        var_108 = local_sp_7 + (-16L);
                        *(uint64_t *)var_108 = 4216004UL;
                        indirect_placeholder_5(4213695UL);
                        local_sp_13 = var_108;
                        rax_1 = var_107;
                    }
                    *(uint64_t *)(local_sp_13 + (-8L)) = 4216110UL;
                    indirect_placeholder_1();
                    if ((uint64_t)(uint32_t)rax_1 == 0UL) {
                        *(uint64_t *)(local_sp_13 + (-16L)) = 4216119UL;
                        indirect_placeholder_1();
                        var_131 = (uint64_t)*(uint32_t *)rax_1;
                        *(uint64_t *)(local_sp_13 + (-24L)) = 4216143UL;
                        indirect_placeholder_6(0UL, rcx_7, 4290298UL, 1UL, var_131, r9_1, r8_1);
                    }
                }
                break;
            }
        }
        break;
    }
}
