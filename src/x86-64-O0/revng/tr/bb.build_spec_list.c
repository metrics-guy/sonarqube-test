typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_55_ret_type;
struct indirect_placeholder_54_ret_type;
struct indirect_placeholder_58_ret_type;
struct indirect_placeholder_57_ret_type;
struct indirect_placeholder_56_ret_type;
struct indirect_placeholder_59_ret_type;
struct indirect_placeholder_60_ret_type;
struct indirect_placeholder_61_ret_type;
struct indirect_placeholder_55_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_54_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_58_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_57_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_56_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_59_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_60_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_61_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_1(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t indirect_placeholder_53(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_9(uint64_t param_0, uint64_t param_1);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern void indirect_placeholder_43(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_55_ret_type indirect_placeholder_55(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_54_ret_type indirect_placeholder_54(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_58_ret_type indirect_placeholder_58(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_57_ret_type indirect_placeholder_57(uint64_t param_0);
extern struct indirect_placeholder_56_ret_type indirect_placeholder_56(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_59_ret_type indirect_placeholder_59(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_60_ret_type indirect_placeholder_60(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_61_ret_type indirect_placeholder_61(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
uint64_t bb_build_spec_list(uint64_t rdi, uint64_t rsi) {
    uint64_t var_83;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t *var_11;
    unsigned char *var_12;
    uint64_t var_13;
    uint64_t *var_14;
    uint64_t *var_15;
    uint64_t *var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint32_t *var_20;
    uint64_t *var_21;
    unsigned char *var_22;
    uint64_t *var_23;
    uint64_t _pre;
    uint64_t local_sp_5_ph;
    uint64_t r8_1_ph;
    uint64_t var_24;
    uint64_t var_101;
    uint64_t r8_0;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t var_100;
    uint64_t rax_0;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t _pre_pre_pre;
    uint64_t _pre_pre;
    uint64_t var_85;
    uint64_t local_sp_5_ph_be;
    uint64_t r8_1_ph_be;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    unsigned char var_89;
    uint64_t local_sp_0;
    uint64_t local_sp_4;
    uint64_t var_57;
    uint64_t var_58;
    struct indirect_placeholder_55_ret_type var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_68;
    uint64_t var_69;
    struct indirect_placeholder_58_ret_type var_70;
    uint64_t var_71;
    struct indirect_placeholder_57_ret_type var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_76;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_77;
    bool var_78;
    uint64_t *var_79;
    bool var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t *var_51;
    uint64_t var_63;
    uint64_t var_52;
    uint64_t local_sp_2;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t var_82;
    uint32_t var_84;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t local_sp_3;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    struct indirect_placeholder_61_ret_type var_45;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t local_sp_5;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_106;
    uint64_t local_sp_6;
    uint64_t var_107;
    uint64_t var_108;
    uint64_t var_109;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t var_112;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_cc_src2();
    var_2 = init_rbp();
    var_3 = init_r9();
    var_4 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    var_5 = var_0 + (-120L);
    var_6 = var_0 + (-112L);
    var_7 = (uint64_t *)var_6;
    *var_7 = rdi;
    var_8 = (uint64_t *)var_5;
    *var_8 = rsi;
    var_9 = **(uint64_t **)var_6;
    var_10 = (uint64_t *)(var_0 + (-32L));
    *var_10 = var_9;
    var_11 = (uint64_t *)(var_0 + (-16L));
    *var_11 = 0UL;
    var_12 = (unsigned char *)(var_0 + (-17L));
    var_13 = var_0 + (-104L);
    var_14 = (uint64_t *)var_13;
    var_15 = (uint64_t *)(var_0 + (-40L));
    var_16 = (uint64_t *)(var_0 + (-48L));
    var_17 = var_0 + (-80L);
    var_18 = var_0 + (-96L);
    var_19 = var_0 + (-81L);
    var_20 = (uint32_t *)(var_0 + (-68L));
    var_21 = (uint64_t *)var_18;
    var_22 = (unsigned char *)var_19;
    var_23 = (uint64_t *)var_17;
    _pre = 0UL;
    local_sp_5_ph = var_5;
    r8_1_ph = var_4;
    rax_0 = 0UL;
    r8_1_ph_be = var_17;
    var_89 = (unsigned char)'\x00';
    while (1U)
        {
            r8_0 = r8_1_ph;
            var_24 = _pre;
            local_sp_5 = local_sp_5_ph;
            while (1U)
                {
                    var_25 = var_24 + 2UL;
                    var_26 = *(uint64_t *)(*var_7 + 16UL);
                    var_27 = helper_cc_compute_c_wrapper(var_25 - var_26, var_26, var_1, 17U);
                    local_sp_6 = local_sp_5;
                    if (var_27 != 0UL) {
                        var_106 = *var_11;
                        rax_0 = 1UL;
                        loop_state_var = 3U;
                        break;
                    }
                    var_28 = *var_11;
                    var_29 = *var_7;
                    var_30 = local_sp_5 + (-8L);
                    *(uint64_t *)var_30 = 4208540UL;
                    var_31 = indirect_placeholder_53(91UL, var_29, var_28);
                    local_sp_4 = var_30;
                    if ((uint64_t)(unsigned char)var_31 != 0UL) {
                        loop_state_var = 4U;
                        break;
                    }
                    *var_12 = (unsigned char)'\x01';
                    var_32 = *var_11 + 1UL;
                    var_33 = *var_7;
                    var_34 = local_sp_5 + (-16L);
                    *(uint64_t *)var_34 = 4208580UL;
                    var_35 = indirect_placeholder_53(58UL, var_33, var_32);
                    local_sp_3 = var_34;
                    r8_0 = var_17;
                    if ((uint64_t)(unsigned char)var_35 != 0UL) {
                        var_36 = *var_11 + 1UL;
                        var_37 = *var_7;
                        var_38 = local_sp_5 + (-24L);
                        *(uint64_t *)var_38 = 4208612UL;
                        var_39 = indirect_placeholder_53(61UL, var_37, var_36);
                        local_sp_2 = var_38;
                        local_sp_3 = var_38;
                        if ((uint64_t)(unsigned char)var_39 != 0UL) {
                            loop_state_var = 5U;
                            break;
                        }
                    }
                    var_40 = *var_11;
                    var_41 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(*var_10 + (var_40 + 1UL));
                    var_42 = var_40 + 2UL;
                    var_43 = *var_7;
                    var_44 = local_sp_3 + (-8L);
                    *(uint64_t *)var_44 = 4208665UL;
                    var_45 = indirect_placeholder_61(var_13, var_41, var_43, var_42);
                    local_sp_2 = var_44;
                    if ((uint64_t)(unsigned char)var_45.field_0 != 0UL) {
                        loop_state_var = 5U;
                        break;
                    }
                    *var_15 = ((*var_14 - *var_11) + (-2L));
                    var_46 = *var_10 + (*var_11 + 2UL);
                    *var_16 = var_46;
                    var_47 = *var_15;
                    if (var_47 != 0UL) {
                        var_77 = var_45.field_1;
                        var_78 = (*(unsigned char *)(*var_10 + (*var_11 + 1UL)) == ':');
                        var_79 = (uint64_t *)(local_sp_3 + (-16L));
                        if (!var_78) {
                            *var_79 = 4208762UL;
                            indirect_placeholder_59(0UL, var_77, 4288728UL, 0UL, 0UL, var_3, r8_1_ph);
                            loop_state_var = 2U;
                            break;
                        }
                        *var_79 = 4208789UL;
                        indirect_placeholder_60(0UL, var_77, 4288768UL, 0UL, 0UL, var_3, r8_1_ph);
                        loop_state_var = 2U;
                        break;
                    }
                    var_48 = (*(unsigned char *)(*var_10 + (*var_11 + 1UL)) == ':');
                    var_49 = *var_8;
                    var_50 = local_sp_3 + (-16L);
                    var_51 = (uint64_t *)var_50;
                    local_sp_5 = var_50;
                    if (var_48) {
                        *var_51 = 4208848UL;
                        var_63 = indirect_placeholder_53(var_47, var_49, var_46);
                        if ((uint64_t)(unsigned char)var_63 != 1UL) {
                            loop_state_var = 0U;
                            break;
                        }
                    }
                    *var_51 = 4208998UL;
                    var_52 = indirect_placeholder_53(var_47, var_49, var_46);
                    if ((uint64_t)(unsigned char)var_52 != 1UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_76 = *var_14 + 2UL;
                    *var_11 = var_76;
                    var_24 = var_76;
                    continue;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    var_64 = *var_11 + 2UL;
                    var_65 = *var_7;
                    var_66 = local_sp_3 + (-24L);
                    *(uint64_t *)var_66 = 4208882UL;
                    var_67 = indirect_placeholder(var_65, var_64);
                    local_sp_2 = var_66;
                    if ((uint64_t)(unsigned char)var_67 != 0UL) {
                        var_68 = *var_15;
                        var_69 = *var_16;
                        *(uint64_t *)(local_sp_3 + (-32L)) = 4208909UL;
                        var_70 = indirect_placeholder_58(var_69, var_68, var_3, r8_1_ph);
                        var_71 = var_70.field_0;
                        *(uint64_t *)(var_0 + (-64L)) = var_71;
                        *(uint64_t *)(local_sp_3 + (-40L)) = 4208925UL;
                        var_72 = indirect_placeholder_57(var_71);
                        var_73 = var_72.field_0;
                        var_74 = var_72.field_1;
                        var_75 = var_72.field_2;
                        *(uint64_t *)(local_sp_3 + (-48L)) = 4208953UL;
                        indirect_placeholder_56(0UL, var_73, 4288811UL, 0UL, 0UL, var_74, var_75);
                        *(uint64_t *)(local_sp_3 + (-56L)) = 4208965UL;
                        indirect_placeholder_1();
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    switch (var_84) {
                      case 0U:
                        {
                            var_85 = *var_21;
                            var_86 = (uint64_t)*var_22;
                            var_87 = *var_8;
                            var_88 = local_sp_2 + (-16L);
                            *(uint64_t *)var_88 = 4209202UL;
                            indirect_placeholder_43(var_85, var_87, var_86);
                            *var_11 = (*var_23 + 1UL);
                            var_89 = *var_12;
                            local_sp_0 = var_88;
                        }
                        break;
                      case 4294967295U:
                        {
                            *var_12 = (unsigned char)'\x00';
                        }
                        break;
                    }
                    local_sp_5_ph_be = local_sp_0;
                    local_sp_4 = local_sp_0;
                    if (var_89 != '\x00') {
                        _pre_pre_pre = *var_11;
                        _pre_pre = _pre_pre_pre;
                        _pre = _pre_pre;
                        local_sp_5_ph = local_sp_5_ph_be;
                        r8_1_ph = r8_1_ph_be;
                        continue;
                    }
                    var_90 = *var_11 + 1UL;
                    var_91 = *var_7;
                    *(uint64_t *)(local_sp_4 + (-8L)) = 4209278UL;
                    var_92 = indirect_placeholder_53(45UL, var_91, var_90);
                    r8_1_ph_be = r8_0;
                    if ((uint64_t)(unsigned char)var_92 == 0UL) {
                        var_102 = (uint64_t)*(unsigned char *)(*var_11 + *var_10);
                        var_103 = *var_8;
                        var_104 = local_sp_4 + (-16L);
                        *(uint64_t *)var_104 = 4209386UL;
                        indirect_placeholder_9(var_103, var_102);
                        var_105 = *var_11 + 1UL;
                        *var_11 = var_105;
                        _pre_pre = var_105;
                        local_sp_5_ph_be = var_104;
                    } else {
                        var_93 = *var_11;
                        var_94 = var_93 + 2UL;
                        var_95 = *var_10;
                        var_96 = (uint64_t)*(unsigned char *)(var_95 + var_94);
                        var_97 = (uint64_t)*(unsigned char *)(var_93 + var_95);
                        var_98 = *var_8;
                        var_99 = local_sp_4 + (-16L);
                        *(uint64_t *)var_99 = 4209334UL;
                        var_100 = indirect_placeholder_53(var_96, var_98, var_97);
                        local_sp_5_ph_be = var_99;
                        if ((uint64_t)(unsigned char)var_100 != 1UL) {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        var_101 = *var_11 + 3UL;
                        *var_11 = var_101;
                        _pre_pre = var_101;
                    }
                    _pre = _pre_pre;
                    local_sp_5_ph = local_sp_5_ph_be;
                    r8_1_ph = r8_1_ph_be;
                    continue;
                }
                break;
              case 1U:
                {
                    var_53 = *var_11 + 2UL;
                    var_54 = *var_7;
                    var_55 = local_sp_3 + (-24L);
                    *(uint64_t *)var_55 = 4209028UL;
                    var_56 = indirect_placeholder(var_54, var_53);
                    local_sp_2 = var_55;
                    if ((uint64_t)(unsigned char)var_56 == 0UL) {
                        var_57 = *var_15;
                        var_58 = *var_16;
                        *(uint64_t *)(local_sp_3 + (-32L)) = 4209051UL;
                        var_59 = indirect_placeholder_55(var_58, var_57, var_3, r8_1_ph);
                        var_60 = var_59.field_0;
                        var_61 = var_59.field_1;
                        var_62 = var_59.field_2;
                        *(uint64_t *)(var_0 + (-56L)) = var_60;
                        *(uint64_t *)(local_sp_3 + (-40L)) = 4209087UL;
                        indirect_placeholder_54(0UL, var_60, 4288840UL, 0UL, 0UL, var_61, var_62);
                        *(uint64_t *)(local_sp_3 + (-48L)) = 4209099UL;
                        indirect_placeholder_1();
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    switch (var_84) {
                      case 4294967295U:
                        {
                            *var_12 = (unsigned char)'\x00';
                        }
                        break;
                      case 0U:
                        {
                            var_85 = *var_21;
                            var_86 = (uint64_t)*var_22;
                            var_87 = *var_8;
                            var_88 = local_sp_2 + (-16L);
                            *(uint64_t *)var_88 = 4209202UL;
                            indirect_placeholder_43(var_85, var_87, var_86);
                            *var_11 = (*var_23 + 1UL);
                            var_89 = *var_12;
                            local_sp_0 = var_88;
                        }
                        break;
                    }
                    local_sp_5_ph_be = local_sp_0;
                    local_sp_4 = local_sp_0;
                    if (var_89 == '\x00') {
                        _pre_pre_pre = *var_11;
                        _pre_pre = _pre_pre_pre;
                        _pre = _pre_pre;
                        local_sp_5_ph = local_sp_5_ph_be;
                        r8_1_ph = r8_1_ph_be;
                        continue;
                    }
                    var_90 = *var_11 + 1UL;
                    var_91 = *var_7;
                    *(uint64_t *)(local_sp_4 + (-8L)) = 4209278UL;
                    var_92 = indirect_placeholder_53(45UL, var_91, var_90);
                    r8_1_ph_be = r8_0;
                    if ((uint64_t)(unsigned char)var_92 == 0UL) {
                        var_102 = (uint64_t)*(unsigned char *)(*var_11 + *var_10);
                        var_103 = *var_8;
                        var_104 = local_sp_4 + (-16L);
                        *(uint64_t *)var_104 = 4209386UL;
                        indirect_placeholder_9(var_103, var_102);
                        var_105 = *var_11 + 1UL;
                        *var_11 = var_105;
                        _pre_pre = var_105;
                        local_sp_5_ph_be = var_104;
                    } else {
                        var_93 = *var_11;
                        var_94 = var_93 + 2UL;
                        var_95 = *var_10;
                        var_96 = (uint64_t)*(unsigned char *)(var_95 + var_94);
                        var_97 = (uint64_t)*(unsigned char *)(var_93 + var_95);
                        var_98 = *var_8;
                        var_99 = local_sp_4 + (-16L);
                        *(uint64_t *)var_99 = 4209334UL;
                        var_100 = indirect_placeholder_53(var_96, var_98, var_97);
                        local_sp_5_ph_be = var_99;
                        if ((uint64_t)(unsigned char)var_100 != 1UL) {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        var_101 = *var_11 + 3UL;
                        *var_11 = var_101;
                        _pre_pre = var_101;
                    }
                    _pre = _pre_pre;
                    local_sp_5_ph = local_sp_5_ph_be;
                    r8_1_ph = r8_1_ph_be;
                    continue;
                }
                break;
              case 4U:
                {
                    var_90 = *var_11 + 1UL;
                    var_91 = *var_7;
                    *(uint64_t *)(local_sp_4 + (-8L)) = 4209278UL;
                    var_92 = indirect_placeholder_53(45UL, var_91, var_90);
                    r8_1_ph_be = r8_0;
                    if ((uint64_t)(unsigned char)var_92 == 0UL) {
                        var_102 = (uint64_t)*(unsigned char *)(*var_11 + *var_10);
                        var_103 = *var_8;
                        var_104 = local_sp_4 + (-16L);
                        *(uint64_t *)var_104 = 4209386UL;
                        indirect_placeholder_9(var_103, var_102);
                        var_105 = *var_11 + 1UL;
                        *var_11 = var_105;
                        _pre_pre = var_105;
                        local_sp_5_ph_be = var_104;
                    } else {
                        var_93 = *var_11;
                        var_94 = var_93 + 2UL;
                        var_95 = *var_10;
                        var_96 = (uint64_t)*(unsigned char *)(var_95 + var_94);
                        var_97 = (uint64_t)*(unsigned char *)(var_93 + var_95);
                        var_98 = *var_8;
                        var_99 = local_sp_4 + (-16L);
                        *(uint64_t *)var_99 = 4209334UL;
                        var_100 = indirect_placeholder_53(var_96, var_98, var_97);
                        local_sp_5_ph_be = var_99;
                        if ((uint64_t)(unsigned char)var_100 != 1UL) {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        var_101 = *var_11 + 3UL;
                        *var_11 = var_101;
                        _pre_pre = var_101;
                    }
                    _pre = _pre_pre;
                    local_sp_5_ph = local_sp_5_ph_be;
                    r8_1_ph = r8_1_ph_be;
                    continue;
                }
                break;
              case 5U:
                {
                    switch (var_84) {
                      case 4294967295U:
                        {
                            *var_12 = (unsigned char)'\x00';
                        }
                        break;
                      case 0U:
                        {
                            var_85 = *var_21;
                            var_86 = (uint64_t)*var_22;
                            var_87 = *var_8;
                            var_88 = local_sp_2 + (-16L);
                            *(uint64_t *)var_88 = 4209202UL;
                            indirect_placeholder_43(var_85, var_87, var_86);
                            *var_11 = (*var_23 + 1UL);
                            var_89 = *var_12;
                            local_sp_0 = var_88;
                        }
                        break;
                    }
                    local_sp_5_ph_be = local_sp_0;
                    local_sp_4 = local_sp_0;
                    if (var_89 == '\x00') {
                        _pre_pre_pre = *var_11;
                        _pre_pre = _pre_pre_pre;
                        _pre = _pre_pre;
                        local_sp_5_ph = local_sp_5_ph_be;
                        r8_1_ph = r8_1_ph_be;
                        continue;
                    }
                    var_90 = *var_11 + 1UL;
                    var_91 = *var_7;
                    *(uint64_t *)(local_sp_4 + (-8L)) = 4209278UL;
                    var_92 = indirect_placeholder_53(45UL, var_91, var_90);
                    r8_1_ph_be = r8_0;
                    if ((uint64_t)(unsigned char)var_92 == 0UL) {
                        var_102 = (uint64_t)*(unsigned char *)(*var_11 + *var_10);
                        var_103 = *var_8;
                        var_104 = local_sp_4 + (-16L);
                        *(uint64_t *)var_104 = 4209386UL;
                        indirect_placeholder_9(var_103, var_102);
                        var_105 = *var_11 + 1UL;
                        *var_11 = var_105;
                        _pre_pre = var_105;
                        local_sp_5_ph_be = var_104;
                    } else {
                        var_93 = *var_11;
                        var_94 = var_93 + 2UL;
                        var_95 = *var_10;
                        var_96 = (uint64_t)*(unsigned char *)(var_95 + var_94);
                        var_97 = (uint64_t)*(unsigned char *)(var_93 + var_95);
                        var_98 = *var_8;
                        var_99 = local_sp_4 + (-16L);
                        *(uint64_t *)var_99 = 4209334UL;
                        var_100 = indirect_placeholder_53(var_96, var_98, var_97);
                        local_sp_5_ph_be = var_99;
                        if ((uint64_t)(unsigned char)var_100 != 1UL) {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        var_101 = *var_11 + 3UL;
                        *var_11 = var_101;
                        _pre_pre = var_101;
                    }
                    _pre = _pre_pre;
                    local_sp_5_ph = local_sp_5_ph_be;
                    r8_1_ph = r8_1_ph_be;
                    continue;
                }
                break;
              case 2U:
                {
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 3U:
                {
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            return rax_0;
        }
        break;
      case 1U:
        {
            var_107 = *(uint64_t *)(*var_7 + 16UL);
            var_108 = helper_cc_compute_c_wrapper(var_106 - var_107, var_107, var_1, 17U);
            while (var_108 != 0UL)
                {
                    var_109 = (uint64_t)*(unsigned char *)(*var_11 + *var_10);
                    var_110 = *var_8;
                    var_111 = local_sp_6 + (-8L);
                    *(uint64_t *)var_111 = 4209449UL;
                    indirect_placeholder_9(var_110, var_109);
                    var_112 = *var_11 + 1UL;
                    *var_11 = var_112;
                    var_106 = var_112;
                    local_sp_6 = var_111;
                    var_107 = *(uint64_t *)(*var_7 + 16UL);
                    var_108 = helper_cc_compute_c_wrapper(var_106 - var_107, var_107, var_1, 17U);
                }
        }
        break;
    }
}
