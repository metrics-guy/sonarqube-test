typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_1(void);
extern uint64_t indirect_placeholder_8(uint64_t param_0);
uint64_t bb_freopen_safer(uint64_t rdx, uint64_t rsi, uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t *var_2;
    uint64_t *var_3;
    unsigned char *var_4;
    unsigned char *var_5;
    unsigned char *var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint32_t var_9;
    uint64_t local_sp_9;
    uint64_t rax_0;
    uint64_t var_28;
    uint64_t local_sp_0;
    uint64_t local_sp_1;
    uint64_t var_29;
    uint64_t var_24;
    uint64_t local_sp_2;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t local_sp_8;
    uint64_t local_sp_3;
    uint32_t var_25;
    uint64_t var_26;
    uint32_t *var_27;
    uint64_t local_sp_5;
    uint64_t local_sp_4;
    uint64_t var_12;
    uint64_t local_sp_6;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t local_sp_7;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_10;
    uint64_t var_11;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = (uint64_t *)(var_0 + (-32L));
    *var_2 = rdi;
    *(uint64_t *)(var_0 + (-40L)) = rsi;
    var_3 = (uint64_t *)(var_0 + (-48L));
    *var_3 = rdx;
    var_4 = (unsigned char *)(var_0 + (-9L));
    *var_4 = (unsigned char)'\x00';
    var_5 = (unsigned char *)(var_0 + (-10L));
    *var_5 = (unsigned char)'\x00';
    var_6 = (unsigned char *)(var_0 + (-11L));
    *var_6 = (unsigned char)'\x00';
    var_7 = *var_3;
    var_8 = var_0 + (-64L);
    *(uint64_t *)var_8 = 4209774UL;
    indirect_placeholder_1();
    var_9 = (uint32_t)var_7;
    local_sp_4 = var_8;
    local_sp_5 = var_8;
    local_sp_9 = var_8;
    if ((uint64_t)(var_9 + (-2)) == 0UL) {
        if ((int)var_9 > (int)2U) {
            if ((uint64_t)var_9 != 0UL) {
                if ((uint64_t)(var_9 + (-1)) != 0UL) {
                    var_10 = var_0 + (-72L);
                    *(uint64_t *)var_10 = 4209808UL;
                    indirect_placeholder_1();
                    *var_6 = (unsigned char)'\x01';
                    local_sp_9 = var_10;
                    var_11 = local_sp_9 + (-8L);
                    *(uint64_t *)var_11 = 4209832UL;
                    indirect_placeholder_1();
                    local_sp_4 = var_11;
                    if ((uint64_t)(var_9 + (-1)) == 0UL) {
                        *var_5 = (unsigned char)'\x01';
                    }
                }
                var_12 = local_sp_4 + (-8L);
                *(uint64_t *)var_12 = 4209856UL;
                indirect_placeholder_1();
                local_sp_5 = var_12;
                if ((uint64_t)var_9 == 0UL) {
                    *var_4 = (unsigned char)'\x01';
                }
            }
        } else {
            var_10 = var_0 + (-72L);
            *(uint64_t *)var_10 = 4209808UL;
            indirect_placeholder_1();
            *var_6 = (unsigned char)'\x01';
            local_sp_9 = var_10;
            var_11 = local_sp_9 + (-8L);
            *(uint64_t *)var_11 = 4209832UL;
            indirect_placeholder_1();
            local_sp_4 = var_11;
            if ((uint64_t)(var_9 + (-1)) == 0UL) {
                *var_5 = (unsigned char)'\x01';
            }
            var_12 = local_sp_4 + (-8L);
            *(uint64_t *)var_12 = 4209856UL;
            indirect_placeholder_1();
            local_sp_5 = var_12;
            if ((uint64_t)var_9 == 0UL) {
                *var_4 = (unsigned char)'\x01';
            }
        }
    } else {
        var_11 = local_sp_9 + (-8L);
        *(uint64_t *)var_11 = 4209832UL;
        indirect_placeholder_1();
        local_sp_4 = var_11;
        if ((uint64_t)(var_9 + (-1)) == 0UL) {
            *var_5 = (unsigned char)'\x01';
        }
        var_12 = local_sp_4 + (-8L);
        *(uint64_t *)var_12 = 4209856UL;
        indirect_placeholder_1();
        local_sp_5 = var_12;
        if ((uint64_t)var_9 == 0UL) {
            *var_4 = (unsigned char)'\x01';
        }
    }
    local_sp_6 = local_sp_5;
    var_13 = local_sp_5 + (-8L);
    *(uint64_t *)var_13 = 4209881UL;
    var_14 = indirect_placeholder_8(0UL);
    var_15 = (uint64_t)(uint32_t)var_14 ^ 1UL;
    local_sp_3 = var_13;
    rax_0 = var_15;
    local_sp_6 = var_13;
    if (*var_4 == '\x00' && (uint64_t)(unsigned char)var_15 == 0UL) {
        *var_3 = 0UL;
    } else {
        local_sp_7 = local_sp_6;
        if (*var_5 == '\x00') {
            var_16 = local_sp_6 + (-8L);
            *(uint64_t *)var_16 = 4209914UL;
            var_17 = indirect_placeholder_8(1UL);
            var_18 = (uint64_t)(uint32_t)var_17 ^ 1UL;
            local_sp_3 = var_16;
            rax_0 = var_18;
            local_sp_7 = var_16;
            if ((uint64_t)(unsigned char)var_18 == 0UL) {
                *var_3 = 0UL;
            } else {
                local_sp_8 = local_sp_7;
                if (*var_6 == '\x00') {
                    var_22 = *var_2;
                    var_23 = local_sp_8 + (-8L);
                    *(uint64_t *)var_23 = 4209987UL;
                    indirect_placeholder_1();
                    *var_3 = var_22;
                    local_sp_3 = var_23;
                    rax_0 = var_22;
                } else {
                    var_19 = local_sp_7 + (-8L);
                    *(uint64_t *)var_19 = 4209947UL;
                    var_20 = indirect_placeholder_8(2UL);
                    var_21 = (uint64_t)(uint32_t)var_20 ^ 1UL;
                    local_sp_3 = var_19;
                    rax_0 = var_21;
                    local_sp_8 = var_19;
                    if ((uint64_t)(unsigned char)var_21 == 0UL) {
                        var_22 = *var_2;
                        var_23 = local_sp_8 + (-8L);
                        *(uint64_t *)var_23 = 4209987UL;
                        indirect_placeholder_1();
                        *var_3 = var_22;
                        local_sp_3 = var_23;
                        rax_0 = var_22;
                    } else {
                        *var_3 = 0UL;
                    }
                }
            }
        } else {
            local_sp_8 = local_sp_7;
            if (*var_6 == '\x00') {
                var_22 = *var_2;
                var_23 = local_sp_8 + (-8L);
                *(uint64_t *)var_23 = 4209987UL;
                indirect_placeholder_1();
                *var_3 = var_22;
                local_sp_3 = var_23;
                rax_0 = var_22;
            } else {
                var_19 = local_sp_7 + (-8L);
                *(uint64_t *)var_19 = 4209947UL;
                var_20 = indirect_placeholder_8(2UL);
                var_21 = (uint64_t)(uint32_t)var_20 ^ 1UL;
                local_sp_3 = var_19;
                rax_0 = var_21;
                local_sp_8 = var_19;
                if ((uint64_t)(unsigned char)var_21 == 0UL) {
                    *var_3 = 0UL;
                } else {
                    var_22 = *var_2;
                    var_23 = local_sp_8 + (-8L);
                    *(uint64_t *)var_23 = 4209987UL;
                    indirect_placeholder_1();
                    *var_3 = var_22;
                    local_sp_3 = var_23;
                    rax_0 = var_22;
                }
            }
        }
    }
    var_24 = local_sp_3 + (-8L);
    *(uint64_t *)var_24 = 4209996UL;
    indirect_placeholder_1();
    var_25 = *(uint32_t *)rax_0;
    var_26 = (uint64_t)var_25;
    var_27 = (uint32_t *)(var_0 + (-16L));
    *var_27 = var_25;
    local_sp_0 = var_24;
    if (*var_6 == '\x00') {
        var_28 = local_sp_3 + (-16L);
        *(uint64_t *)var_28 = 4210017UL;
        indirect_placeholder_1();
        local_sp_0 = var_28;
    }
    local_sp_1 = local_sp_0;
    if (*var_5 == '\x00') {
        var_29 = local_sp_0 + (-8L);
        *(uint64_t *)var_29 = 4210033UL;
        indirect_placeholder_1();
        local_sp_1 = var_29;
    }
    local_sp_2 = local_sp_1;
    if (*var_4 == '\x00') {
        var_30 = local_sp_1 + (-8L);
        *(uint64_t *)var_30 = 4210049UL;
        indirect_placeholder_1();
        local_sp_2 = var_30;
    }
    var_31 = *var_3;
    var_32 = var_31;
    if (var_31 != 0UL) {
        return var_32;
    }
    *(uint64_t *)(local_sp_2 + (-8L)) = 4210061UL;
    indirect_placeholder_1();
    *(uint32_t *)var_26 = *var_27;
    var_32 = *var_3;
    return var_32;
}
