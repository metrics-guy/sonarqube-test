typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_15_ret_type;
struct indirect_placeholder_18_ret_type;
struct indirect_placeholder_15_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_18_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_7(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_1(void);
extern uint64_t indirect_placeholder_16(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t indirect_placeholder_8(uint64_t param_0);
extern void indirect_placeholder_9(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_4(void);
extern void indirect_placeholder_12(uint64_t param_0);
extern void indirect_placeholder_2(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_15_ret_type indirect_placeholder_15(uint64_t param_0);
extern void indirect_placeholder_17(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_18_ret_type indirect_placeholder_18(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rsi, uint64_t rdi) {
    uint64_t local_sp_4;
    struct indirect_placeholder_18_ret_type var_16;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint32_t *var_3;
    uint64_t var_4;
    uint64_t *var_5;
    unsigned char *var_6;
    uint32_t *var_7;
    unsigned char *var_8;
    uint64_t **var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint32_t *var_12;
    uint64_t local_sp_5;
    unsigned char var_40;
    uint64_t local_sp_0;
    uint64_t *var_41;
    uint64_t var_42;
    uint64_t *var_43;
    uint64_t var_44;
    uint64_t *var_45;
    uint64_t *_pre_phi174;
    unsigned char *var_46;
    uint64_t _pre;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t *var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t *_pre_phi178;
    uint64_t _pre171;
    uint64_t var_56;
    bool var_57;
    uint64_t *var_58;
    uint64_t var_37;
    uint64_t var_38;
    unsigned char var_39;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    unsigned char var_36;
    uint64_t var_32;
    uint64_t local_sp_3;
    uint64_t var_65;
    uint64_t local_sp_1;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    unsigned char var_22;
    bool var_23;
    uint64_t var_59;
    struct indirect_placeholder_15_ret_type var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t *var_24;
    uint64_t var_25;
    uint64_t local_sp_2;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint32_t var_31;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint32_t var_17;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = var_0 + (-8L);
    *(uint64_t *)var_2 = var_1;
    var_3 = (uint32_t *)(var_0 + (-108L));
    *var_3 = (uint32_t)rdi;
    var_4 = var_0 + (-120L);
    var_5 = (uint64_t *)var_4;
    *var_5 = rsi;
    var_6 = (unsigned char *)(var_0 + (-9L));
    *var_6 = (unsigned char)'\x01';
    var_7 = (uint32_t *)(var_0 + (-16L));
    *var_7 = 2U;
    var_8 = (unsigned char *)(var_0 + (-17L));
    *var_8 = (unsigned char)'\x00';
    var_9 = (uint64_t **)var_4;
    var_10 = **var_9;
    *(uint64_t *)(var_0 + (-128L)) = 4208201UL;
    indirect_placeholder_12(var_10);
    *(uint64_t *)(var_0 + (-136L)) = 4208216UL;
    indirect_placeholder_1();
    var_11 = var_0 + (-144L);
    *(uint64_t *)var_11 = 4208226UL;
    indirect_placeholder_1();
    var_12 = (uint32_t *)(var_0 + (-52L));
    var_25 = 4288896UL;
    local_sp_5 = var_11;
    while (1U)
        {
            var_13 = *var_5;
            var_14 = (uint64_t)*var_3;
            var_15 = local_sp_5 + (-8L);
            *(uint64_t *)var_15 = 4208431UL;
            var_16 = indirect_placeholder_18(4294112UL, 4295214UL, 0UL, var_13, var_14);
            var_17 = (uint32_t)var_16.field_0;
            *var_12 = var_17;
            local_sp_2 = var_15;
            local_sp_3 = var_15;
            local_sp_4 = var_15;
            local_sp_5 = var_15;
            switch_state_var = 0;
            switch (var_17) {
              case 112U:
                {
                    *var_8 = (unsigned char)'\x01';
                    continue;
                }
                break;
              case 4294967295U:
                {
                    var_18 = var_16.field_1;
                    var_19 = var_16.field_2;
                    var_20 = var_16.field_3;
                    *var_3 = (*var_3 - *(uint32_t *)4313944UL);
                    var_21 = *var_5 + ((uint64_t)*(uint32_t *)4313944UL << 3UL);
                    *var_5 = var_21;
                    var_22 = *var_8;
                    var_23 = (var_22 == '\x00');
                    if (!(var_23 && *var_7 == 2U)) {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    *(uint64_t *)(local_sp_5 + (-16L)) = 4208506UL;
                    indirect_placeholder_2(0UL, var_18, 4295224UL, var_19, 0UL, 0UL, var_20);
                    *(uint64_t *)(local_sp_5 + (-24L)) = 4208516UL;
                    indirect_placeholder_9(var_2, 1UL);
                    abort();
                }
                break;
              default:
                {
                    if ((int)var_17 <= (int)112U) {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    if (var_17 == 99U) {
                        *var_7 = 1U;
                        continue;
                    }
                    if ((int)var_17 <= (int)99U) {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    if (var_17 == 98U) {
                        *var_7 = 0U;
                        continue;
                    }
                    if ((int)var_17 <= (int)98U) {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    switch_state_var = 0;
                    switch (var_17) {
                      case 4294967166U:
                        {
                            *(uint64_t *)(local_sp_5 + (-16L)) = 4208329UL;
                            indirect_placeholder_9(var_2, 0UL);
                            abort();
                        }
                        break;
                      case 4294967165U:
                        {
                            var_66 = *(uint64_t *)4313792UL;
                            *(uint64_t *)(local_sp_5 + (-16L)) = 4208381UL;
                            indirect_placeholder_17(0UL, var_66, 4288664UL, 4295199UL, 4295046UL, 0UL);
                            var_67 = local_sp_5 + (-24L);
                            *(uint64_t *)var_67 = 4208391UL;
                            indirect_placeholder_1();
                            local_sp_4 = var_67;
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      default:
                        {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 1U:
        {
            *(uint64_t *)(local_sp_4 + (-8L)) = 4208401UL;
            indirect_placeholder_9(var_2, 1UL);
            abort();
        }
        break;
      case 0U:
        {
            if ((long)(uint64_t)(var_22 ^ '\x01') < (long)(uint64_t)*var_3) {
                var_59 = *(uint64_t *)(var_21 + ((var_22 == '\x01') ? 0UL : 8UL));
                *(uint64_t *)(local_sp_5 + (-16L)) = 4208572UL;
                var_60 = indirect_placeholder_15(var_59);
                var_61 = var_60.field_0;
                var_62 = var_60.field_1;
                var_63 = var_60.field_2;
                var_64 = local_sp_5 + (-24L);
                *(uint64_t *)var_64 = 4208600UL;
                indirect_placeholder_2(0UL, var_61, 4295327UL, var_62, 0UL, 0UL, var_63);
                local_sp_1 = var_64;
                if (*var_8 != '\x00') {
                    var_65 = local_sp_5 + (-32L);
                    *(uint64_t *)var_65 = 4208636UL;
                    indirect_placeholder_1();
                    local_sp_1 = var_65;
                }
                *(uint64_t *)(local_sp_1 + (-8L)) = 4208646UL;
                indirect_placeholder_9(var_2, 1UL);
                abort();
            }
            if (!var_23) {
                var_24 = (uint64_t *)(var_0 + (-32L));
                *var_24 = 4288896UL;
                while ((var_25 + (-4288896L)) <= 4172UL)
                    {
                        *(uint64_t *)(local_sp_2 + (-8L)) = 4208674UL;
                        indirect_placeholder_1();
                        var_26 = *var_24;
                        var_27 = local_sp_2 + (-16L);
                        *(uint64_t *)var_27 = 4208686UL;
                        indirect_placeholder_1();
                        var_28 = *var_24 + (var_26 + 1UL);
                        *var_24 = var_28;
                        var_25 = var_28;
                        local_sp_2 = var_27;
                    }
            }
            var_29 = local_sp_5 + (-16L);
            *(uint64_t *)var_29 = 4208728UL;
            var_30 = indirect_placeholder_4();
            var_31 = (uint32_t)var_30;
            *var_7 = var_31;
            local_sp_3 = var_29;
            if (*var_7 != 2U & var_31 == 2U) {
                var_32 = local_sp_5 + (-24L);
                *(uint64_t *)var_32 = 4208762UL;
                indirect_placeholder_2(0UL, var_18, 4295408UL, var_19, 0UL, 1UL, var_20);
                local_sp_3 = var_32;
            }
            *(uint64_t *)(local_sp_3 + (-8L)) = 4208793UL;
            indirect_placeholder_16(4235070UL, 0UL, 4203808UL, 0UL, 4314112UL);
            if (*var_3 == 0U) {
                var_37 = local_sp_3 + (-16L);
                *(uint64_t *)var_37 = 4208814UL;
                var_38 = indirect_placeholder_7(0UL, 0UL);
                var_39 = (unsigned char)var_38;
                *var_6 = var_39;
                var_40 = var_39;
                local_sp_0 = var_37;
            } else {
                var_33 = **var_9;
                var_34 = local_sp_3 + (-16L);
                *(uint64_t *)var_34 = 4208834UL;
                var_35 = indirect_placeholder_8(var_33);
                var_36 = (unsigned char)var_35;
                *var_6 = var_36;
                var_40 = var_36;
                local_sp_0 = var_34;
            }
            if (var_40 != '\x00') {
                *(uint64_t *)(var_0 + (-64L)) = 4314112UL;
                *(uint64_t *)(var_0 + (-72L)) = (*(uint64_t *)4314136UL - *(uint64_t *)4314128UL);
                var_41 = (uint64_t *)(var_0 + (-80L));
                *var_41 = 4314112UL;
                var_42 = *(uint64_t *)4314128UL;
                var_43 = (uint64_t *)(var_0 + (-88L));
                *var_43 = var_42;
                var_44 = *var_41;
                var_45 = (uint64_t *)(var_44 + 24UL);
                _pre_phi174 = var_45;
                var_47 = var_44;
                if (var_42 == *var_45) {
                    var_46 = (unsigned char *)(var_44 + 80UL);
                    *var_46 = (*var_46 | '\x02');
                    _pre = *var_41;
                    _pre_phi174 = (uint64_t *)(_pre + 24UL);
                    var_47 = _pre;
                }
                var_48 = *_pre_phi174;
                var_49 = *(uint64_t *)(var_47 + 48UL);
                *_pre_phi174 = ((var_48 + var_49) & (var_49 ^ (-1L)));
                var_50 = *var_41;
                var_51 = (uint64_t *)(var_50 + 24UL);
                var_52 = *var_51;
                var_53 = *(uint64_t *)(var_50 + 8UL);
                var_54 = var_52 - var_53;
                var_55 = *(uint64_t *)(var_50 + 32UL);
                _pre_phi178 = var_51;
                var_56 = var_50;
                if (var_54 > (var_55 - var_53)) {
                    *var_51 = var_55;
                    _pre171 = *var_41;
                    _pre_phi178 = (uint64_t *)(_pre171 + 24UL);
                    var_56 = _pre171;
                }
                *(uint64_t *)(var_56 + 16UL) = *_pre_phi178;
                *(uint64_t *)(var_0 + (-96L)) = *var_43;
                var_57 = (*var_7 == 0U);
                var_58 = (uint64_t *)(var_0 + (-40L));
                if (var_57) {
                    *var_58 = 4295470UL;
                    *(uint64_t *)(var_0 + (-48L)) = 4295482UL;
                } else {
                    *var_58 = 4295503UL;
                    *(uint64_t *)(var_0 + (-48L)) = 4295522UL;
                }
                *(uint64_t *)(local_sp_0 + (-8L)) = 4209130UL;
                indirect_placeholder_1();
                *(uint64_t *)(local_sp_0 + (-16L)) = 4209158UL;
                indirect_placeholder_1();
                *(uint64_t *)(local_sp_0 + (-24L)) = 4209180UL;
                indirect_placeholder_1();
            }
            return;
        }
        break;
    }
}
