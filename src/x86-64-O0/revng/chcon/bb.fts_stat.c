typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_fts_stat_ret_type;
struct bb_fts_stat_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rcx(void);
extern uint64_t indirect_placeholder_35(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint64_t indirect_placeholder_4(uint64_t param_0);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
struct bb_fts_stat_ret_type bb_fts_stat(uint64_t rdx, uint64_t rsi, uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    unsigned char *var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    uint64_t var_23;
    uint64_t local_sp_0;
    uint64_t rcx_0;
    uint64_t rax_1;
    uint64_t storemerge;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t local_sp_1;
    uint64_t rax_0;
    uint64_t var_18;
    uint64_t var_13;
    uint32_t var_14;
    uint64_t rcx_1;
    uint64_t var_24;
    uint64_t rcx_2;
    uint64_t var_25;
    uint64_t spec_select55;
    struct bb_fts_stat_ret_type mrv;
    struct bb_fts_stat_ret_type mrv1;
    uint64_t var_9;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t var_12;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rcx();
    var_3 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    var_4 = (uint64_t *)(var_0 + (-48L));
    *var_4 = rdi;
    var_5 = (unsigned char *)(var_0 + (-52L));
    *var_5 = (unsigned char)rdx;
    var_6 = rsi + 120UL;
    var_7 = (uint64_t *)(var_0 + (-32L));
    *var_7 = var_6;
    var_8 = (uint64_t *)(rsi + 88UL);
    rcx_0 = 256UL;
    rax_1 = 10UL;
    storemerge = 18446744073709551615UL;
    rcx_1 = 256UL;
    rcx_2 = var_2;
    if (*var_8 != 0UL & (*(uint32_t *)(*var_4 + 72UL) & 1U) == 0U) {
        *var_5 = (unsigned char)'\x01';
    }
    var_9 = *var_4;
    if ((*(uint32_t *)(var_9 + 72UL) & 2U) == 0U) {
        if (*var_5 == '\x00') {
            var_19 = *(uint64_t *)(rsi + 48UL);
            var_20 = (uint64_t)*(uint32_t *)(var_9 + 44UL);
            var_21 = *var_7;
            *(uint64_t *)(var_0 + (-64L)) = 4226475UL;
            var_22 = indirect_placeholder_35(256UL, var_21, var_19, var_20);
            if ((uint64_t)(uint32_t)var_22 != 0UL) {
                var_23 = var_0 + (-72L);
                *(uint64_t *)var_23 = 4226484UL;
                indirect_placeholder();
                *(uint32_t *)(rsi + 64UL) = *(uint32_t *)var_22;
                local_sp_0 = var_23;
                *(uint64_t *)(local_sp_0 + (-8L)) = 4226511UL;
                indirect_placeholder();
                rcx_2 = rcx_0;
                mrv.field_0 = rax_1;
                mrv1 = mrv;
                mrv1.field_1 = rcx_2;
                return mrv1;
            }
        }
        var_10 = (uint64_t *)(rsi + 48UL);
        var_11 = *var_10;
        *(uint64_t *)(var_0 + (-64L)) = 4226372UL;
        var_12 = indirect_placeholder_4(var_11);
        rcx_0 = var_2;
        rcx_1 = var_2;
        rax_1 = 13UL;
        if ((uint64_t)(uint32_t)var_12 != 0UL) {
            var_13 = var_0 + (-72L);
            *(uint64_t *)var_13 = 4226385UL;
            indirect_placeholder();
            var_14 = *(uint32_t *)var_12;
            local_sp_1 = var_13;
            rax_0 = (uint64_t)var_14;
            if ((uint64_t)(var_14 + (-2)) != 0UL) {
                var_15 = *var_10;
                var_16 = var_0 + (-80L);
                *(uint64_t *)var_16 = 4226411UL;
                var_17 = indirect_placeholder_4(var_15);
                local_sp_1 = var_16;
                rax_0 = var_17;
                if ((uint64_t)(uint32_t)var_17 != 0UL) {
                    *(uint64_t *)(var_0 + (-88L)) = 4226420UL;
                    indirect_placeholder();
                    *(uint32_t *)var_17 = 0U;
                    mrv.field_0 = rax_1;
                    mrv1 = mrv;
                    mrv1.field_1 = rcx_2;
                    return mrv1;
                }
            }
            var_18 = local_sp_1 + (-8L);
            *(uint64_t *)var_18 = 4226441UL;
            indirect_placeholder();
            *(uint32_t *)(rsi + 64UL) = *(uint32_t *)rax_0;
            local_sp_0 = var_18;
            *(uint64_t *)(local_sp_0 + (-8L)) = 4226511UL;
            indirect_placeholder();
            rcx_2 = rcx_0;
            mrv.field_0 = rax_1;
            mrv1 = mrv;
            mrv1.field_1 = rcx_2;
            return mrv1;
        }
    }
    var_10 = (uint64_t *)(rsi + 48UL);
    var_11 = *var_10;
    *(uint64_t *)(var_0 + (-64L)) = 4226372UL;
    var_12 = indirect_placeholder_4(var_11);
    rcx_0 = var_2;
    rcx_1 = var_2;
    rax_1 = 13UL;
    if ((uint64_t)(uint32_t)var_12 != 0UL) {
        var_13 = var_0 + (-72L);
        *(uint64_t *)var_13 = 4226385UL;
        indirect_placeholder();
        var_14 = *(uint32_t *)var_12;
        local_sp_1 = var_13;
        rax_0 = (uint64_t)var_14;
        if ((uint64_t)(var_14 + (-2)) != 0UL) {
            var_15 = *var_10;
            var_16 = var_0 + (-80L);
            *(uint64_t *)var_16 = 4226411UL;
            var_17 = indirect_placeholder_4(var_15);
            local_sp_1 = var_16;
            rax_0 = var_17;
            if ((uint64_t)(uint32_t)var_17 == 0UL) {
                *(uint64_t *)(var_0 + (-88L)) = 4226420UL;
                indirect_placeholder();
                *(uint32_t *)var_17 = 0U;
                mrv.field_0 = rax_1;
                mrv1 = mrv;
                mrv1.field_1 = rcx_2;
                return mrv1;
            }
        }
        var_18 = local_sp_1 + (-8L);
        *(uint64_t *)var_18 = 4226441UL;
        indirect_placeholder();
        *(uint32_t *)(rsi + 64UL) = *(uint32_t *)rax_0;
        local_sp_0 = var_18;
        *(uint64_t *)(local_sp_0 + (-8L)) = 4226511UL;
        indirect_placeholder();
        rcx_2 = rcx_0;
        mrv.field_0 = rax_1;
        mrv1 = mrv;
        mrv1.field_1 = rcx_2;
        return mrv1;
    }
    var_24 = *var_7;
    rax_1 = 12UL;
    rcx_2 = rcx_1;
    switch ((uint32_t)((uint16_t)*(uint32_t *)(var_24 + 24UL) & (unsigned short)61440U)) {
      case 40960U:
        {
            mrv.field_0 = rax_1;
            mrv1 = mrv;
            mrv1.field_1 = rcx_2;
            return mrv1;
        }
        break;
      case 32768U:
        {
            rax_1 = 8UL;
        }
        break;
      case 16384U:
        {
            rax_1 = 1UL;
            var_25 = helper_cc_compute_all_wrapper(*var_8, 0UL, 0UL, 25U);
            if (*(uint64_t *)(var_24 + 16UL) <= 1UL & (uint64_t)(((unsigned char)(var_25 >> 4UL) ^ (unsigned char)var_25) & '\xc0') == 0UL) {
                storemerge = *(uint64_t *)(*var_7 + 16UL) - (((*(uint32_t *)(*var_4 + 72UL) & 32U) == 0U) ? 2UL : 0UL);
            }
            *(uint64_t *)(rsi + 104UL) = storemerge;
            if (*(unsigned char *)(rsi + 264UL) != '.') {
                switch (*(unsigned char *)(rsi + 265UL)) {
                  case '\x00':
                    {
                        spec_select55 = (*var_8 == 0UL) ? 1UL : 5UL;
                        rax_1 = spec_select55;
                    }
                    break;
                  case '.':
                    {
                        if (*(unsigned char *)(rsi + 266UL) == '\x00') {
                            spec_select55 = (*var_8 == 0UL) ? 1UL : 5UL;
                            rax_1 = spec_select55;
                        }
                    }
                    break;
                  default:
                    {
                        break;
                    }
                    break;
                }
            }
        }
        break;
      default:
        {
            rax_1 = 3UL;
        }
        break;
    }
}
