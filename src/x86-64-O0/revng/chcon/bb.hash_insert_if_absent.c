typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_cvtsq2ss_wrapper_ret_type;
struct type_5;
struct type_7;
struct helper_mulss_wrapper_163_ret_type;
struct helper_addss_wrapper_ret_type;
struct helper_comiss_wrapper_164_ret_type;
struct helper_cvttss2sq_wrapper_ret_type;
struct helper_subss_wrapper_ret_type;
struct helper_comiss_wrapper_ret_type;
struct helper_cvtsq2ss_wrapper_161_ret_type;
struct helper_addss_wrapper_162_ret_type;
struct helper_mulss_wrapper_ret_type;
struct helper_mulss_wrapper_165_ret_type;
struct helper_cvtsq2ss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct type_5 {
};
struct type_7 {
};
struct helper_mulss_wrapper_163_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_addss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_comiss_wrapper_164_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvttss2sq_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_subss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_comiss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvtsq2ss_wrapper_161_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_addss_wrapper_162_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_mulss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_mulss_wrapper_165_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern unsigned char init_state_0x8549(void);
extern unsigned char init_state_0x854c(void);
extern unsigned char init_state_0x8548(void);
extern unsigned char init_state_0x854b(void);
extern unsigned char init_state_0x8547(void);
extern unsigned char init_state_0x854d(void);
extern void indirect_placeholder(void);
extern uint64_t init_r10(void);
extern struct helper_cvtsq2ss_wrapper_ret_type helper_cvtsq2ss_wrapper(struct type_5 *param_0, struct type_7 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_mulss_wrapper_163_ret_type helper_mulss_wrapper_163(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern uint64_t init_rbx(void);
extern uint64_t init_state_0x8d58(void);
extern struct helper_addss_wrapper_ret_type helper_addss_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9);
extern struct helper_comiss_wrapper_164_ret_type helper_comiss_wrapper_164(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_cvttss2sq_wrapper_ret_type helper_cvttss2sq_wrapper(struct type_5 *param_0, struct type_7 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4);
extern struct helper_subss_wrapper_ret_type helper_subss_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern uint64_t indirect_placeholder_35(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint64_t indirect_placeholder_4(uint64_t param_0);
extern uint64_t indirect_placeholder_45(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct helper_comiss_wrapper_ret_type helper_comiss_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_cvtsq2ss_wrapper_161_ret_type helper_cvtsq2ss_wrapper_161(struct type_5 *param_0, struct type_7 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_addss_wrapper_162_ret_type helper_addss_wrapper_162(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9);
extern struct helper_mulss_wrapper_ret_type helper_mulss_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_mulss_wrapper_165_ret_type helper_mulss_wrapper_165(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
uint64_t bb_hash_insert_if_absent(uint64_t rdx, uint64_t rsi, uint64_t rdi) {
    struct helper_cvtsq2ss_wrapper_ret_type var_66;
    uint64_t state_0x8558_2;
    uint64_t var_67;
    struct helper_cvtsq2ss_wrapper_ret_type var_68;
    struct helper_addss_wrapper_ret_type var_69;
    uint64_t cc_dst_1;
    unsigned char storemerge9;
    struct helper_cvttss2sq_wrapper_ret_type var_88;
    struct helper_cvtsq2ss_wrapper_ret_type var_43;
    struct helper_cvttss2sq_wrapper_ret_type var_86;
    struct helper_cvtsq2ss_wrapper_ret_type var_44;
    uint64_t state_0x8558_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    unsigned char var_8;
    unsigned char var_9;
    unsigned char var_10;
    unsigned char var_11;
    unsigned char var_12;
    unsigned char var_13;
    uint64_t var_14;
    uint64_t *var_15;
    uint64_t *var_16;
    uint64_t var_17;
    uint64_t *var_18;
    uint64_t var_19;
    uint64_t var_21;
    uint64_t *var_101;
    uint64_t *var_102;
    uint64_t rax_1;
    uint64_t var_95;
    uint64_t local_sp_0;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t var_94;
    struct helper_addss_wrapper_ret_type var_45;
    unsigned char storemerge5;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    struct helper_cvtsq2ss_wrapper_161_ret_type var_49;
    uint64_t state_0x8598_0;
    struct helper_cvtsq2ss_wrapper_161_ret_type var_50;
    struct helper_addss_wrapper_162_ret_type var_51;
    unsigned char storemerge6;
    struct helper_mulss_wrapper_ret_type var_52;
    struct helper_comiss_wrapper_ret_type var_53;
    uint64_t var_54;
    unsigned char var_55;
    uint64_t var_56;
    uint64_t *var_57;
    bool var_58;
    uint64_t var_59;
    bool var_60;
    struct helper_cvtsq2ss_wrapper_ret_type var_61;
    uint64_t state_0x8558_1;
    uint64_t var_62;
    struct helper_cvtsq2ss_wrapper_ret_type var_63;
    struct helper_addss_wrapper_ret_type var_64;
    uint64_t cc_dst_0;
    unsigned char storemerge7;
    struct helper_mulss_wrapper_163_ret_type var_65;
    uint64_t state_0x8558_3;
    struct helper_mulss_wrapper_165_ret_type var_70;
    struct helper_mulss_wrapper_163_ret_type var_71;
    uint64_t cc_dst_2;
    unsigned char storemerge8;
    uint64_t var_72;
    uint32_t var_73;
    uint32_t *var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    struct helper_comiss_wrapper_164_ret_type var_78;
    uint64_t var_79;
    struct helper_comiss_wrapper_164_ret_type var_80;
    uint64_t var_81;
    unsigned char var_82;
    uint64_t var_83;
    bool var_84;
    uint64_t var_85;
    uint64_t rax_0;
    struct helper_subss_wrapper_ret_type var_87;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_26;
    struct helper_cvtsq2ss_wrapper_ret_type var_27;
    uint64_t state_0x8558_4;
    struct helper_cvtsq2ss_wrapper_ret_type var_28;
    struct helper_addss_wrapper_ret_type var_29;
    unsigned char storemerge;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    struct helper_cvtsq2ss_wrapper_161_ret_type var_33;
    uint64_t state_0x8598_1;
    struct helper_cvtsq2ss_wrapper_161_ret_type var_34;
    struct helper_addss_wrapper_162_ret_type var_35;
    unsigned char storemerge4;
    struct helper_mulss_wrapper_ret_type var_36;
    struct helper_comiss_wrapper_ret_type var_37;
    uint64_t var_38;
    unsigned char var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t *var_96;
    uint64_t var_97;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t *var_100;
    uint64_t *var_103;
    uint64_t *var_104;
    uint64_t var_20;
    uint64_t local_sp_1;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_cc_src2();
    var_2 = init_rbp();
    var_3 = init_r10();
    var_4 = init_r9();
    var_5 = init_r8();
    var_6 = init_rbx();
    var_7 = init_state_0x8d58();
    var_8 = init_state_0x8549();
    var_9 = init_state_0x854c();
    var_10 = init_state_0x8548();
    var_11 = init_state_0x854b();
    var_12 = init_state_0x8547();
    var_13 = init_state_0x854d();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    var_14 = var_0 + (-88L);
    var_15 = (uint64_t *)(var_0 + (-64L));
    *var_15 = rdi;
    var_16 = (uint64_t *)(var_0 + (-72L));
    *var_16 = rsi;
    var_17 = var_0 + (-80L);
    var_18 = (uint64_t *)var_17;
    *var_18 = rdx;
    var_19 = *var_16;
    rax_1 = 0UL;
    var_21 = var_19;
    local_sp_1 = var_14;
    if (var_19 == 0UL) {
        var_20 = var_0 + (-96L);
        *(uint64_t *)var_20 = 4240119UL;
        indirect_placeholder();
        var_21 = *var_16;
        local_sp_1 = var_20;
    }
    var_22 = var_0 + (-48L);
    var_23 = *var_15;
    var_24 = local_sp_1 + (-8L);
    *(uint64_t *)var_24 = 4240144UL;
    var_25 = indirect_placeholder_35(0UL, var_22, var_21, var_23);
    *(uint64_t *)(var_0 + (-16L)) = var_25;
    local_sp_0 = var_24;
    if (var_25 == 0UL) {
        var_26 = *(uint64_t *)(*var_15 + 24UL);
        rax_1 = 4294967295UL;
        if ((long)var_26 < (long)0UL) {
            var_28 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (var_26 >> 1UL) | (var_26 & 1UL), var_8, var_10, var_11, var_12);
            var_29 = helper_addss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_28.field_0, var_28.field_1, var_9, var_10, var_11, var_12, var_13);
            state_0x8558_4 = var_29.field_0;
            storemerge = var_29.field_1;
        } else {
            var_27 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_26, var_8, var_10, var_11, var_12);
            state_0x8558_4 = var_27.field_0;
            storemerge = var_27.field_1;
        }
        var_30 = *var_15;
        var_31 = (uint64_t)*(uint32_t *)(*(uint64_t *)(var_30 + 40UL) + 8UL);
        var_32 = *(uint64_t *)(var_30 + 16UL);
        if ((long)var_32 < (long)0UL) {
            var_34 = helper_cvtsq2ss_wrapper_161((struct type_5 *)(0UL), (struct type_7 *)(840UL), (var_32 >> 1UL) | (var_32 & 1UL), storemerge, var_10, var_11, var_12);
            var_35 = helper_addss_wrapper_162((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(840UL), var_34.field_0, var_34.field_1, var_9, var_10, var_11, var_12, var_13);
            state_0x8598_1 = var_35.field_0;
            storemerge4 = var_35.field_1;
        } else {
            var_33 = helper_cvtsq2ss_wrapper_161((struct type_5 *)(0UL), (struct type_7 *)(840UL), var_32, storemerge, var_10, var_11, var_12);
            state_0x8598_1 = var_33.field_0;
            storemerge4 = var_33.field_1;
        }
        var_36 = helper_mulss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(904UL), state_0x8598_1, var_31, storemerge4, var_9, var_10, var_11, var_12, var_13);
        var_37 = helper_comiss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(840UL), var_36.field_0, state_0x8558_4, var_36.field_1, var_9);
        var_38 = var_37.field_0;
        var_39 = var_37.field_1;
        if ((var_38 & 65UL) != 0UL) {
            var_40 = *var_15;
            var_41 = local_sp_1 + (-16L);
            *(uint64_t *)var_41 = 4240303UL;
            indirect_placeholder_4(var_40);
            var_42 = *(uint64_t *)(*var_15 + 24UL);
            local_sp_0 = var_41;
            if ((long)var_42 < (long)0UL) {
                var_44 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (var_42 >> 1UL) | (var_42 & 1UL), var_39, var_10, var_11, var_12);
                var_45 = helper_addss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_44.field_0, var_44.field_1, var_9, var_10, var_11, var_12, var_13);
                state_0x8558_0 = var_45.field_0;
                storemerge5 = var_45.field_1;
            } else {
                var_43 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_42, var_39, var_10, var_11, var_12);
                state_0x8558_0 = var_43.field_0;
                storemerge5 = var_43.field_1;
            }
            var_46 = *var_15;
            var_47 = (uint64_t)*(uint32_t *)(*(uint64_t *)(var_46 + 40UL) + 8UL);
            var_48 = *(uint64_t *)(var_46 + 16UL);
            if ((long)var_48 < (long)0UL) {
                var_50 = helper_cvtsq2ss_wrapper_161((struct type_5 *)(0UL), (struct type_7 *)(840UL), (var_48 >> 1UL) | (var_48 & 1UL), storemerge5, var_10, var_11, var_12);
                var_51 = helper_addss_wrapper_162((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(840UL), var_50.field_0, var_50.field_1, var_9, var_10, var_11, var_12, var_13);
                state_0x8598_0 = var_51.field_0;
                storemerge6 = var_51.field_1;
            } else {
                var_49 = helper_cvtsq2ss_wrapper_161((struct type_5 *)(0UL), (struct type_7 *)(840UL), var_48, storemerge5, var_10, var_11, var_12);
                state_0x8598_0 = var_49.field_0;
                storemerge6 = var_49.field_1;
            }
            var_52 = helper_mulss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(904UL), state_0x8598_0, var_47, storemerge6, var_9, var_10, var_11, var_12, var_13);
            var_53 = helper_comiss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(840UL), var_52.field_0, state_0x8558_0, var_52.field_1, var_9);
            var_54 = var_53.field_0;
            var_55 = var_53.field_1;
            if ((var_54 & 65UL) != 0UL) {
                var_56 = *(uint64_t *)(*var_15 + 40UL);
                var_57 = (uint64_t *)(var_0 + (-24L));
                *var_57 = var_56;
                var_58 = (*(unsigned char *)(var_56 + 16UL) == '\x00');
                var_59 = *(uint64_t *)(*var_15 + 16UL);
                var_60 = ((long)var_59 < (long)0UL);
                cc_dst_0 = var_59;
                cc_dst_1 = var_59;
                if (var_58) {
                    if (var_60) {
                        var_67 = (var_59 >> 1UL) | (var_59 & 1UL);
                        var_68 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_67, var_55, var_10, var_11, var_12);
                        var_69 = helper_addss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_68.field_0, var_68.field_1, var_9, var_10, var_11, var_12, var_13);
                        state_0x8558_2 = var_69.field_0;
                        cc_dst_1 = var_67;
                        storemerge9 = var_69.field_1;
                    } else {
                        var_66 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_59, var_55, var_10, var_11, var_12);
                        state_0x8558_2 = var_66.field_0;
                        storemerge9 = var_66.field_1;
                    }
                    var_70 = helper_mulss_wrapper_165((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(776UL), (uint64_t)*(uint32_t *)(*var_57 + 12UL), state_0x8558_2, storemerge9, var_9, var_10, var_11, var_12, var_13);
                    var_71 = helper_mulss_wrapper_163((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(840UL), var_70.field_0, (uint64_t)*(uint32_t *)(*var_57 + 8UL), var_70.field_1, var_9, var_10, var_11, var_12, var_13);
                    state_0x8558_3 = var_71.field_0;
                    cc_dst_2 = cc_dst_1;
                    storemerge8 = var_71.field_1;
                } else {
                    if (var_60) {
                        var_62 = (var_59 >> 1UL) | (var_59 & 1UL);
                        var_63 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_62, var_55, var_10, var_11, var_12);
                        var_64 = helper_addss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_63.field_0, var_63.field_1, var_9, var_10, var_11, var_12, var_13);
                        state_0x8558_1 = var_64.field_0;
                        cc_dst_0 = var_62;
                        storemerge7 = var_64.field_1;
                    } else {
                        var_61 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_59, var_55, var_10, var_11, var_12);
                        state_0x8558_1 = var_61.field_0;
                        storemerge7 = var_61.field_1;
                    }
                    var_65 = helper_mulss_wrapper_163((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(840UL), (uint64_t)*(uint32_t *)(*var_57 + 12UL), state_0x8558_1, storemerge7, var_9, var_10, var_11, var_12, var_13);
                    state_0x8558_3 = var_65.field_0;
                    cc_dst_2 = cc_dst_0;
                    storemerge8 = var_65.field_1;
                }
                var_72 = var_0 + (-28L);
                var_73 = (uint32_t)state_0x8558_3;
                var_74 = (uint32_t *)var_72;
                *var_74 = var_73;
                var_75 = (uint64_t)var_73;
                var_76 = (uint64_t)*(uint32_t *)4308656UL;
                var_77 = var_7 & (-4294967296L);
                var_78 = helper_comiss_wrapper_164((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), var_75, var_77 | var_76, storemerge8, var_9);
                var_79 = helper_cc_compute_c_wrapper(cc_dst_2, var_78.field_0, var_1, 1U);
                if (var_79 == 0UL) {
                    return rax_1;
                }
                var_80 = helper_comiss_wrapper_164((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), (uint64_t)*var_74, var_77 | (uint64_t)*(uint32_t *)4308660UL, var_78.field_1, var_9);
                var_81 = var_80.field_0;
                var_82 = var_80.field_1;
                var_83 = helper_cc_compute_c_wrapper(cc_dst_2, var_81, var_1, 1U);
                var_84 = (var_83 == 0UL);
                var_85 = (uint64_t)*var_74;
                if (var_84) {
                    var_87 = helper_subss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(840UL), (uint64_t)*(uint32_t *)4308660UL, var_85, var_82, var_9, var_10, var_11, var_12, var_13);
                    var_88 = helper_cvttss2sq_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_87.field_0, var_87.field_1, var_9);
                    rax_0 = var_88.field_0 ^ (-9223372036854775808L);
                } else {
                    var_86 = helper_cvttss2sq_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_85, var_82, var_9);
                    rax_0 = var_86.field_0;
                }
                var_89 = *var_15;
                *(uint64_t *)(local_sp_1 + (-24L)) = 4240663UL;
                var_90 = indirect_placeholder_45(0UL, rax_0, var_89, var_3, var_4, var_5, var_6);
                if ((uint64_t)(unsigned char)var_90 == 1UL) {
                    return rax_1;
                }
                var_91 = *var_16;
                var_92 = *var_15;
                var_93 = local_sp_1 + (-32L);
                *(uint64_t *)var_93 = 4240705UL;
                var_94 = indirect_placeholder_35(0UL, var_22, var_91, var_92);
                local_sp_0 = var_93;
                if (var_94 == 0UL) {
                    var_95 = local_sp_1 + (-40L);
                    *(uint64_t *)var_95 = 4240715UL;
                    indirect_placeholder();
                    local_sp_0 = var_95;
                }
            }
        }
        var_96 = *(uint64_t **)var_22;
        rax_1 = 1UL;
        if (*var_96 == 0UL) {
            *var_96 = *var_16;
            var_103 = (uint64_t *)(*var_15 + 32UL);
            *var_103 = (*var_103 + 1UL);
            var_104 = (uint64_t *)(*var_15 + 24UL);
            *var_104 = (*var_104 + 1UL);
        } else {
            var_97 = *var_15;
            *(uint64_t *)(local_sp_0 + (-8L)) = 4240739UL;
            var_98 = indirect_placeholder_4(var_97);
            var_99 = var_0 + (-40L);
            var_100 = (uint64_t *)var_99;
            *var_100 = var_98;
            if (var_98 == 0UL) {
                **(uint64_t **)var_99 = *var_16;
                var_101 = (uint64_t *)var_22;
                *(uint64_t *)(*var_100 + 8UL) = *(uint64_t *)(*var_101 + 8UL);
                *(uint64_t *)(*var_101 + 8UL) = *var_100;
                var_102 = (uint64_t *)(*var_15 + 32UL);
                *var_102 = (*var_102 + 1UL);
                rax_1 = 1UL;
            }
        }
    } else {
        if (*var_18 == 0UL) {
            **(uint64_t **)var_17 = var_25;
        }
    }
    return rax_1;
}
