typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_cvtsq2ss_wrapper_ret_type;
struct type_5;
struct type_7;
struct helper_mulss_wrapper_163_ret_type;
struct helper_addss_wrapper_ret_type;
struct helper_comiss_wrapper_164_ret_type;
struct helper_cvttss2sq_wrapper_ret_type;
struct helper_subss_wrapper_ret_type;
struct helper_comiss_wrapper_ret_type;
struct helper_cvtsq2ss_wrapper_161_ret_type;
struct helper_addss_wrapper_162_ret_type;
struct helper_mulss_wrapper_ret_type;
struct helper_mulss_wrapper_165_ret_type;
struct helper_cvtsq2ss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct type_5 {
};
struct type_7 {
};
struct helper_mulss_wrapper_163_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_addss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_comiss_wrapper_164_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvttss2sq_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_subss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_comiss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvtsq2ss_wrapper_161_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_addss_wrapper_162_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_mulss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_mulss_wrapper_165_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern unsigned char init_state_0x8549(void);
extern unsigned char init_state_0x854c(void);
extern unsigned char init_state_0x8548(void);
extern unsigned char init_state_0x854b(void);
extern unsigned char init_state_0x8547(void);
extern unsigned char init_state_0x854d(void);
extern void indirect_placeholder(void);
extern uint64_t init_r10(void);
extern struct helper_cvtsq2ss_wrapper_ret_type helper_cvtsq2ss_wrapper(struct type_5 *param_0, struct type_7 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_mulss_wrapper_163_ret_type helper_mulss_wrapper_163(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern uint64_t init_rbx(void);
extern uint64_t init_state_0x8d58(void);
extern struct helper_addss_wrapper_ret_type helper_addss_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9);
extern struct helper_comiss_wrapper_164_ret_type helper_comiss_wrapper_164(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_cvttss2sq_wrapper_ret_type helper_cvttss2sq_wrapper(struct type_5 *param_0, struct type_7 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4);
extern struct helper_subss_wrapper_ret_type helper_subss_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern uint64_t indirect_placeholder_35(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint64_t indirect_placeholder_4(uint64_t param_0);
extern uint64_t indirect_placeholder_45(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct helper_comiss_wrapper_ret_type helper_comiss_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_cvtsq2ss_wrapper_161_ret_type helper_cvtsq2ss_wrapper_161(struct type_5 *param_0, struct type_7 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_addss_wrapper_162_ret_type helper_addss_wrapper_162(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9);
extern struct helper_mulss_wrapper_ret_type helper_mulss_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_mulss_wrapper_165_ret_type helper_mulss_wrapper_165(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
uint64_t bb_hash_delete(uint64_t rsi, uint64_t rdi) {
    struct helper_cvttss2sq_wrapper_ret_type var_81;
    struct helper_cvttss2sq_wrapper_ret_type var_79;
    struct helper_cvttss2sq_wrapper_ret_type var_67;
    struct helper_cvttss2sq_wrapper_ret_type var_65;
    struct helper_cvtsq2ss_wrapper_ret_type var_37;
    uint64_t state_0x8558_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    unsigned char var_8;
    unsigned char var_9;
    unsigned char var_10;
    unsigned char var_11;
    unsigned char var_12;
    unsigned char var_13;
    uint64_t *var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t *var_18;
    uint64_t storemerge8;
    uint64_t var_85;
    uint64_t *var_86;
    uint64_t *var_87;
    uint64_t var_88;
    uint64_t local_sp_0;
    uint64_t var_89;
    uint64_t var_90;
    struct helper_cvtsq2ss_wrapper_ret_type var_38;
    struct helper_addss_wrapper_ret_type var_39;
    unsigned char storemerge5;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    struct helper_cvtsq2ss_wrapper_161_ret_type var_43;
    uint64_t state_0x8598_0;
    struct helper_cvtsq2ss_wrapper_161_ret_type var_44;
    struct helper_addss_wrapper_162_ret_type var_45;
    unsigned char storemerge4;
    struct helper_mulss_wrapper_ret_type var_46;
    struct helper_comiss_wrapper_ret_type var_47;
    uint64_t var_48;
    unsigned char var_49;
    uint64_t var_50;
    uint64_t *var_51;
    bool var_52;
    uint64_t var_53;
    bool var_54;
    struct helper_cvtsq2ss_wrapper_ret_type var_55;
    uint64_t state_0x8558_1;
    uint64_t var_56;
    struct helper_cvtsq2ss_wrapper_ret_type var_57;
    struct helper_addss_wrapper_ret_type var_58;
    uint64_t cc_dst_0;
    unsigned char storemerge;
    struct helper_mulss_wrapper_163_ret_type var_59;
    uint64_t var_60;
    struct helper_comiss_wrapper_164_ret_type var_61;
    unsigned char var_34;
    uint64_t var_62;
    unsigned char var_63;
    uint64_t var_64;
    uint64_t rax_0;
    struct helper_subss_wrapper_ret_type var_66;
    struct helper_cvtsq2ss_wrapper_ret_type var_68;
    uint64_t state_0x8558_2;
    uint64_t var_69;
    struct helper_cvtsq2ss_wrapper_ret_type var_70;
    struct helper_addss_wrapper_ret_type var_71;
    uint64_t cc_dst_1;
    unsigned char storemerge3;
    struct helper_mulss_wrapper_165_ret_type var_72;
    struct helper_mulss_wrapper_163_ret_type var_73;
    uint64_t var_74;
    struct helper_comiss_wrapper_164_ret_type var_75;
    uint64_t var_76;
    unsigned char var_77;
    uint64_t var_78;
    struct helper_subss_wrapper_ret_type var_80;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t *var_19;
    uint64_t *var_20;
    uint64_t var_21;
    struct helper_cvtsq2ss_wrapper_ret_type var_22;
    uint64_t state_0x8558_3;
    struct helper_cvtsq2ss_wrapper_ret_type var_23;
    struct helper_addss_wrapper_ret_type var_24;
    unsigned char storemerge7;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    struct helper_cvtsq2ss_wrapper_161_ret_type var_28;
    uint64_t state_0x8598_1;
    struct helper_cvtsq2ss_wrapper_161_ret_type var_29;
    struct helper_addss_wrapper_162_ret_type var_30;
    unsigned char storemerge6;
    struct helper_mulss_wrapper_ret_type var_31;
    struct helper_comiss_wrapper_ret_type var_32;
    uint64_t var_33;
    uint64_t var_35;
    uint64_t var_36;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_cc_src2();
    var_2 = init_rbp();
    var_3 = init_r10();
    var_4 = init_r9();
    var_5 = init_r8();
    var_6 = init_rbx();
    var_7 = init_state_0x8d58();
    var_8 = init_state_0x8549();
    var_9 = init_state_0x854c();
    var_10 = init_state_0x8548();
    var_11 = init_state_0x854b();
    var_12 = init_state_0x8547();
    var_13 = init_state_0x854d();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    var_14 = (uint64_t *)(var_0 + (-64L));
    *var_14 = rdi;
    *(uint64_t *)(var_0 + (-72L)) = rsi;
    var_15 = var_0 + (-56L);
    var_16 = *var_14;
    *(uint64_t *)(var_0 + (-80L)) = 4240995UL;
    var_17 = indirect_placeholder_35(1UL, var_15, rsi, var_16);
    var_18 = (uint64_t *)(var_0 + (-24L));
    *var_18 = var_17;
    storemerge8 = 0UL;
    if (var_17 == 0UL) {
        return storemerge8;
    }
    var_19 = (uint64_t *)(*var_14 + 32UL);
    *var_19 = (*var_19 + (-1L));
    if (**(uint64_t **)var_15 != 0UL) {
        var_20 = (uint64_t *)(*var_14 + 24UL);
        *var_20 = (*var_20 + (-1L));
        var_21 = *(uint64_t *)(*var_14 + 24UL);
        if ((long)var_21 < (long)0UL) {
            var_23 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (var_21 >> 1UL) | (var_21 & 1UL), var_8, var_10, var_11, var_12);
            var_24 = helper_addss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_23.field_0, var_23.field_1, var_9, var_10, var_11, var_12, var_13);
            state_0x8558_3 = var_24.field_0;
            storemerge7 = var_24.field_1;
        } else {
            var_22 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_21, var_8, var_10, var_11, var_12);
            state_0x8558_3 = var_22.field_0;
            storemerge7 = var_22.field_1;
        }
        var_25 = *var_14;
        var_26 = (uint64_t)**(uint32_t **)(var_25 + 40UL);
        var_27 = *(uint64_t *)(var_25 + 16UL);
        if ((long)var_27 < (long)0UL) {
            var_29 = helper_cvtsq2ss_wrapper_161((struct type_5 *)(0UL), (struct type_7 *)(840UL), (var_27 >> 1UL) | (var_27 & 1UL), storemerge7, var_10, var_11, var_12);
            var_30 = helper_addss_wrapper_162((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(840UL), var_29.field_0, var_29.field_1, var_9, var_10, var_11, var_12, var_13);
            state_0x8598_1 = var_30.field_0;
            storemerge6 = var_30.field_1;
        } else {
            var_28 = helper_cvtsq2ss_wrapper_161((struct type_5 *)(0UL), (struct type_7 *)(840UL), var_27, storemerge7, var_10, var_11, var_12);
            state_0x8598_1 = var_28.field_0;
            storemerge6 = var_28.field_1;
        }
        var_31 = helper_mulss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(904UL), state_0x8598_1, var_26, storemerge6, var_9, var_10, var_11, var_12, var_13);
        var_32 = helper_comiss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(776UL), var_31.field_0, state_0x8558_3, var_31.field_1, var_9);
        var_33 = var_32.field_0;
        var_34 = var_32.field_1;
        if ((var_33 & 65UL) != 0UL) {
            var_35 = *var_14;
            *(uint64_t *)(var_0 + (-88L)) = 4241191UL;
            indirect_placeholder_4(var_35);
            var_36 = *(uint64_t *)(*var_14 + 24UL);
            if ((long)var_36 < (long)0UL) {
                var_38 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (var_36 >> 1UL) | (var_36 & 1UL), var_34, var_10, var_11, var_12);
                var_39 = helper_addss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_38.field_0, var_38.field_1, var_9, var_10, var_11, var_12, var_13);
                state_0x8558_0 = var_39.field_0;
                storemerge5 = var_39.field_1;
            } else {
                var_37 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_36, var_34, var_10, var_11, var_12);
                state_0x8558_0 = var_37.field_0;
                storemerge5 = var_37.field_1;
            }
            var_40 = *var_14;
            var_41 = (uint64_t)**(uint32_t **)(var_40 + 40UL);
            var_42 = *(uint64_t *)(var_40 + 16UL);
            if ((long)var_42 < (long)0UL) {
                var_44 = helper_cvtsq2ss_wrapper_161((struct type_5 *)(0UL), (struct type_7 *)(840UL), (var_42 >> 1UL) | (var_42 & 1UL), storemerge5, var_10, var_11, var_12);
                var_45 = helper_addss_wrapper_162((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(840UL), var_44.field_0, var_44.field_1, var_9, var_10, var_11, var_12, var_13);
                state_0x8598_0 = var_45.field_0;
                storemerge4 = var_45.field_1;
            } else {
                var_43 = helper_cvtsq2ss_wrapper_161((struct type_5 *)(0UL), (struct type_7 *)(840UL), var_42, storemerge5, var_10, var_11, var_12);
                state_0x8598_0 = var_43.field_0;
                storemerge4 = var_43.field_1;
            }
            var_46 = helper_mulss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(904UL), state_0x8598_0, var_41, storemerge4, var_9, var_10, var_11, var_12, var_13);
            var_47 = helper_comiss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(776UL), var_46.field_0, state_0x8558_0, var_46.field_1, var_9);
            var_48 = var_47.field_0;
            var_49 = var_47.field_1;
            if ((var_48 & 65UL) != 0UL) {
                var_50 = *(uint64_t *)(*var_14 + 40UL);
                var_51 = (uint64_t *)(var_0 + (-32L));
                *var_51 = var_50;
                var_52 = (*(unsigned char *)(var_50 + 16UL) == '\x00');
                var_53 = *(uint64_t *)(*var_14 + 16UL);
                var_54 = ((long)var_53 < (long)0UL);
                cc_dst_0 = var_53;
                cc_dst_1 = var_53;
                if (var_52) {
                    if (var_54) {
                        var_69 = (var_53 >> 1UL) | (var_53 & 1UL);
                        var_70 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_69, var_49, var_10, var_11, var_12);
                        var_71 = helper_addss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_70.field_0, var_70.field_1, var_9, var_10, var_11, var_12, var_13);
                        state_0x8558_2 = var_71.field_0;
                        cc_dst_1 = var_69;
                        storemerge3 = var_71.field_1;
                    } else {
                        var_68 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_53, var_49, var_10, var_11, var_12);
                        state_0x8558_2 = var_68.field_0;
                        storemerge3 = var_68.field_1;
                    }
                    var_72 = helper_mulss_wrapper_165((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(776UL), (uint64_t)*(uint32_t *)(*var_51 + 4UL), state_0x8558_2, storemerge3, var_9, var_10, var_11, var_12, var_13);
                    var_73 = helper_mulss_wrapper_163((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(840UL), var_72.field_0, (uint64_t)*(uint32_t *)(*var_51 + 8UL), var_72.field_1, var_9, var_10, var_11, var_12, var_13);
                    var_74 = var_73.field_0;
                    var_75 = helper_comiss_wrapper_164((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), var_74, (var_7 & (-4294967296L)) | (uint64_t)*(uint32_t *)4308660UL, var_73.field_1, var_9);
                    var_76 = var_75.field_0;
                    var_77 = var_75.field_1;
                    var_78 = helper_cc_compute_c_wrapper(cc_dst_1, var_76, var_1, 1U);
                    if (var_78 == 0UL) {
                        var_80 = helper_subss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(840UL), (uint64_t)*(uint32_t *)4308660UL, var_74, var_77, var_9, var_10, var_11, var_12, var_13);
                        var_81 = helper_cvttss2sq_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_80.field_0, var_80.field_1, var_9);
                        rax_0 = var_81.field_0 ^ (-9223372036854775808L);
                    } else {
                        var_79 = helper_cvttss2sq_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_74, var_77, var_9);
                        rax_0 = var_79.field_0;
                    }
                } else {
                    if (var_54) {
                        var_56 = (var_53 >> 1UL) | (var_53 & 1UL);
                        var_57 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_56, var_49, var_10, var_11, var_12);
                        var_58 = helper_addss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_57.field_0, var_57.field_1, var_9, var_10, var_11, var_12, var_13);
                        state_0x8558_1 = var_58.field_0;
                        cc_dst_0 = var_56;
                        storemerge = var_58.field_1;
                    } else {
                        var_55 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_53, var_49, var_10, var_11, var_12);
                        state_0x8558_1 = var_55.field_0;
                        storemerge = var_55.field_1;
                    }
                    var_59 = helper_mulss_wrapper_163((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(840UL), (uint64_t)*(uint32_t *)(*var_51 + 4UL), state_0x8558_1, storemerge, var_9, var_10, var_11, var_12, var_13);
                    var_60 = var_59.field_0;
                    var_61 = helper_comiss_wrapper_164((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), var_60, (var_7 & (-4294967296L)) | (uint64_t)*(uint32_t *)4308660UL, var_59.field_1, var_9);
                    var_62 = var_61.field_0;
                    var_63 = var_61.field_1;
                    var_64 = helper_cc_compute_c_wrapper(cc_dst_0, var_62, var_1, 1U);
                    if (var_64 == 0UL) {
                        var_66 = helper_subss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(840UL), (uint64_t)*(uint32_t *)4308660UL, var_60, var_63, var_9, var_10, var_11, var_12, var_13);
                        var_67 = helper_cvttss2sq_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_66.field_0, var_66.field_1, var_9);
                        rax_0 = var_67.field_0 ^ (-9223372036854775808L);
                    } else {
                        var_65 = helper_cvttss2sq_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_60, var_63, var_9);
                        rax_0 = var_65.field_0;
                    }
                }
                *(uint64_t *)(var_0 + (-40L)) = rax_0;
                var_82 = *var_14;
                var_83 = var_0 + (-96L);
                *(uint64_t *)var_83 = 4241563UL;
                var_84 = indirect_placeholder_45(1UL, rax_0, var_82, var_3, var_4, var_5, var_6);
                local_sp_0 = var_83;
                if ((uint64_t)(unsigned char)var_84 != 1UL) {
                    var_85 = *(uint64_t *)(*var_14 + 72UL);
                    var_86 = (uint64_t *)(var_0 + (-16L));
                    *var_86 = var_85;
                    var_87 = (uint64_t *)(var_0 + (-48L));
                    var_88 = var_85;
                    while (var_88 != 0UL)
                        {
                            *var_87 = *(uint64_t *)(var_88 + 8UL);
                            var_89 = local_sp_0 + (-8L);
                            *(uint64_t *)var_89 = 4241608UL;
                            indirect_placeholder();
                            var_90 = *var_87;
                            *var_86 = var_90;
                            var_88 = var_90;
                            local_sp_0 = var_89;
                        }
                    *(uint64_t *)(*var_14 + 72UL) = 0UL;
                }
            }
        }
    }
    storemerge8 = *var_18;
    return storemerge8;
}
