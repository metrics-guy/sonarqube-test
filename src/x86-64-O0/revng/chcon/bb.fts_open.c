typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_26_ret_type;
struct indirect_placeholder_27_ret_type;
struct indirect_placeholder_29_ret_type;
struct indirect_placeholder_26_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_27_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_29_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_22(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t indirect_placeholder_28(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_5(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_4(uint64_t param_0);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t init_r14(void);
extern struct indirect_placeholder_26_ret_type indirect_placeholder_26(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_27_ret_type indirect_placeholder_27(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_29_ret_type indirect_placeholder_29(uint64_t param_0);
uint64_t bb_fts_open(uint64_t rdx, uint64_t rsi, uint64_t rdi) {
    uint64_t var_64;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t *var_10;
    uint64_t *var_11;
    uint64_t *var_12;
    uint32_t var_13;
    uint64_t var_14;
    uint64_t local_sp_0;
    uint32_t *var_61;
    uint32_t var_62;
    uint64_t r13_3;
    uint64_t var_63;
    uint32_t *var_65;
    uint32_t var_66;
    uint64_t var_67;
    uint64_t var_54;
    struct indirect_placeholder_26_ret_type var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t r15_0;
    uint64_t local_sp_1;
    bool var_46;
    uint64_t *var_47;
    uint64_t r13_0;
    uint64_t r13_2;
    uint64_t local_sp_5;
    bool var_41;
    bool var_42;
    uint64_t var_43;
    uint64_t var_44;
    struct indirect_placeholder_27_ret_type var_45;
    uint64_t local_sp_6;
    uint64_t local_sp_2;
    uint64_t r13_1;
    uint64_t var_68;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_48;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t local_sp_4;
    uint64_t local_sp_3;
    uint64_t var_69;
    uint64_t **var_27;
    uint64_t var_28;
    uint64_t var_29;
    unsigned char storemerge;
    unsigned char *var_30;
    uint64_t *var_31;
    bool var_32;
    uint64_t *var_33;
    uint64_t var_34;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t *var_53;
    uint64_t local_sp_7;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint32_t *var_20;
    uint64_t rax_0;
    uint64_t var_15;
    bool var_16;
    uint64_t *var_17;
    struct indirect_placeholder_29_ret_type var_18;
    uint64_t var_19;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_cc_src2();
    var_2 = init_rbp();
    var_3 = init_rbx();
    var_4 = init_r15();
    var_5 = init_r12();
    var_6 = init_r13();
    var_7 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_7;
    *(uint64_t *)(var_0 + (-32L)) = var_6;
    *(uint64_t *)(var_0 + (-40L)) = var_5;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_8 = var_0 + (-112L);
    var_9 = (uint64_t *)var_8;
    *var_9 = rdi;
    var_10 = (uint64_t *)(var_0 + (-120L));
    *var_10 = rdx;
    var_11 = (uint64_t *)(var_0 + (-64L));
    *var_11 = 0UL;
    var_12 = (uint64_t *)(var_0 + (-72L));
    *var_12 = 0UL;
    var_13 = (uint32_t)rsi;
    var_14 = (uint64_t)(var_13 & (-8192));
    storemerge = (unsigned char)'\x01';
    r15_0 = 0UL;
    r13_2 = 0UL;
    rax_0 = 0UL;
    if (var_14 != 0UL) {
        *(uint64_t *)(var_0 + (-128L)) = 4219701UL;
        indirect_placeholder();
        *(uint32_t *)var_14 = 22U;
        return rax_0;
    }
    if ((rsi & 4UL) != 0UL) {
        var_15 = (uint64_t)((uint16_t)rsi & (unsigned short)512U);
        if (var_15 != 0UL) {
            *(uint64_t *)(var_0 + (-128L)) = 4219744UL;
            indirect_placeholder();
            *(uint32_t *)var_15 = 22U;
            return rax_0;
        }
    }
    var_16 = ((rsi & 18UL) == 0UL);
    var_17 = (uint64_t *)(var_0 + (-128L));
    if (var_16) {
        *var_17 = 4219775UL;
        indirect_placeholder();
        *(volatile uint32_t *)(uint32_t *)0UL = 22U;
    } else {
        *var_17 = 4219801UL;
        var_18 = indirect_placeholder_29(128UL);
        var_19 = var_18.field_0;
        if (var_19 != 0UL) {
            *(uint64_t *)(var_0 + (-136L)) = 4219837UL;
            indirect_placeholder();
            *(uint64_t *)(var_19 + 64UL) = *var_10;
            var_20 = (uint32_t *)(var_19 + 72UL);
            *var_20 = var_13;
            if ((var_13 & 2U) == 0U) {
                *var_20 = ((var_13 | 4U) & (-513));
            }
            *(uint32_t *)(var_19 + 44UL) = 4294967196U;
            var_21 = *var_9;
            *(uint64_t *)(var_0 + (-144L)) = 4219896UL;
            var_22 = indirect_placeholder_4(var_21);
            *(uint64_t *)(var_0 + (-88L)) = var_22;
            var_23 = helper_cc_compute_c_wrapper(var_22 + (-4096L), 4096UL, var_1, 17U);
            var_24 = (var_23 == 0UL) ? var_22 : 4096UL;
            var_25 = var_0 + (-152L);
            *(uint64_t *)var_25 = 4219929UL;
            var_26 = indirect_placeholder_3(var_24, var_19);
            local_sp_4 = var_25;
            local_sp_7 = var_25;
            if ((uint64_t)(unsigned char)var_26 == 1UL) {
                *(uint64_t *)(local_sp_7 + (-8L)) = 4220588UL;
                indirect_placeholder();
            } else {
                var_27 = (uint64_t **)var_8;
                if (**var_27 != 0UL) {
                    var_28 = var_0 + (-160L);
                    *(uint64_t *)var_28 = 4219970UL;
                    var_29 = indirect_placeholder_3(0UL, var_19);
                    *var_11 = var_29;
                    local_sp_3 = var_28;
                    local_sp_4 = var_28;
                    if (var_29 != 0UL) {
                        var_69 = local_sp_3 + (-8L);
                        *(uint64_t *)var_69 = 4220577UL;
                        indirect_placeholder();
                        local_sp_7 = var_69;
                        *(uint64_t *)(local_sp_7 + (-8L)) = 4220588UL;
                        indirect_placeholder();
                        return rax_0;
                    }
                    *(uint64_t *)(var_29 + 88UL) = 18446744073709551615UL;
                    *(uint64_t *)(*var_11 + 104UL) = 18446744073709551615UL;
                }
                local_sp_5 = local_sp_4;
                if (*var_10 == 0UL) {
                } else {
                    storemerge = (unsigned char)'\x00';
                    if ((uint32_t)((uint16_t)*var_20 & (unsigned short)1024U) == 0U) {
                    }
                }
                var_30 = (unsigned char *)(var_0 + (-89L));
                *var_30 = (storemerge & '\x01');
                var_31 = (uint64_t *)(var_0 + (-80L));
                var_32 = ((uint64_t)((uint16_t)rsi & (unsigned short)4096U) == 0UL);
                var_33 = (uint64_t *)(var_0 + (-104L));
                while (1U)
                    {
                        var_34 = **var_27;
                        r13_1 = r13_2;
                        var_38 = var_34;
                        local_sp_6 = local_sp_5;
                        r13_3 = r13_2;
                        if (var_34 != 0UL) {
                            if (!((*var_10 != 0UL) && (r15_0 > 1UL))) {
                                loop_state_var = 0U;
                                break;
                            }
                            var_49 = local_sp_5 + (-8L);
                            *(uint64_t *)var_49 = 4220396UL;
                            var_50 = indirect_placeholder_28(r15_0, r13_2, var_19);
                            local_sp_6 = var_49;
                            r13_3 = var_50;
                            loop_state_var = 0U;
                            break;
                        }
                        *(uint64_t *)(local_sp_5 + (-8L)) = 4220079UL;
                        indirect_placeholder();
                        *var_31 = var_34;
                        var_35 = **var_27;
                        *var_33 = var_35;
                        var_36 = *var_31;
                        var_37 = var_36;
                        var_38 = var_36;
                        if (!var_32 & var_36 <= 2UL && *(unsigned char *)(var_35 + (var_36 + (-1L))) == '/') {
                            var_38 = var_37;
                            while (var_37 <= 1UL)
                                {
                                    if (*(unsigned char *)(*var_33 + (var_37 + (-2L))) == '/') {
                                        break;
                                    }
                                    var_48 = var_37 + (-1L);
                                    *var_31 = var_48;
                                    var_37 = var_48;
                                    var_38 = var_37;
                                }
                        }
                        var_39 = local_sp_5 + (-16L);
                        *(uint64_t *)var_39 = 4220193UL;
                        var_40 = indirect_placeholder_3(var_38, var_19);
                        r13_0 = var_40;
                        local_sp_2 = var_39;
                        if (var_40 != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        *(uint64_t *)(var_40 + 88UL) = 0UL;
                        *(uint64_t *)(var_40 + 8UL) = *var_11;
                        *(uint64_t *)(var_40 + 48UL) = (var_40 + 264UL);
                        var_41 = (*var_30 == '\x00');
                        var_42 = (r13_2 == 0UL);
                        if (var_41 || var_42) {
                            var_44 = local_sp_5 + (-24L);
                            *(uint64_t *)var_44 = 4220286UL;
                            var_45 = indirect_placeholder_27(0UL, var_40, var_19);
                            *(uint16_t *)(var_40 + 112UL) = (uint16_t)var_45.field_0;
                            local_sp_1 = var_44;
                        } else {
                            *(uint16_t *)(var_40 + 112UL) = (uint16_t)(unsigned short)11U;
                            var_43 = local_sp_5 + (-24L);
                            *(uint64_t *)var_43 = 4220268UL;
                            indirect_placeholder_1(1UL, var_40);
                            local_sp_1 = var_43;
                        }
                        var_46 = (*var_10 == 0UL);
                        var_47 = (uint64_t *)(var_40 + 16UL);
                        local_sp_5 = local_sp_1;
                        if (var_46) {
                            *var_47 = r13_2;
                        } else {
                            *var_47 = 0UL;
                            r13_0 = r13_2;
                            if (var_42) {
                                *var_12 = var_40;
                            } else {
                                *(uint64_t *)(*var_12 + 16UL) = var_40;
                                *var_12 = var_40;
                            }
                        }
                        *var_9 = (*var_9 + 8UL);
                        r15_0 = r15_0 + 1UL;
                        r13_2 = r13_0;
                        continue;
                    }
                switch (loop_state_var) {
                  case 1U:
                    {
                        *(uint64_t *)(local_sp_2 + (-8L)) = 4220550UL;
                        indirect_placeholder_5(r13_1);
                        var_68 = local_sp_2 + (-16L);
                        *(uint64_t *)var_68 = 4220562UL;
                        indirect_placeholder();
                        local_sp_3 = var_68;
                        var_69 = local_sp_3 + (-8L);
                        *(uint64_t *)var_69 = 4220577UL;
                        indirect_placeholder();
                        local_sp_7 = var_69;
                    }
                    break;
                  case 0U:
                    {
                        var_51 = local_sp_6 + (-8L);
                        *(uint64_t *)var_51 = 4220417UL;
                        var_52 = indirect_placeholder_3(0UL, var_19);
                        var_53 = (uint64_t *)var_19;
                        *var_53 = var_52;
                        local_sp_2 = var_51;
                        r13_1 = r13_3;
                        *(uint64_t *)(var_52 + 16UL) = r13_3;
                        *(uint16_t *)(*var_53 + 112UL) = (uint16_t)(unsigned short)9U;
                        var_54 = local_sp_6 + (-16L);
                        *(uint64_t *)var_54 = 4220452UL;
                        var_55 = indirect_placeholder_26(4308000UL, var_19);
                        var_56 = var_55.field_0;
                        var_57 = var_55.field_1;
                        var_58 = var_55.field_2;
                        var_59 = var_55.field_3;
                        var_60 = var_55.field_4;
                        local_sp_0 = var_54;
                        local_sp_2 = var_54;
                        rax_0 = var_60;
                        if (var_52 != 0UL & (uint64_t)(unsigned char)var_56 != 1UL) {
                            var_61 = (uint32_t *)(var_60 + 72UL);
                            var_62 = *var_61;
                            var_63 = local_sp_6 + (-24L);
                            *(uint64_t *)var_63 = 4220494UL;
                            var_64 = indirect_placeholder_22(var_57, 4308001UL, var_60, var_58, var_59);
                            var_65 = (uint32_t *)(var_60 + 40UL);
                            var_66 = (uint32_t)var_64;
                            *var_65 = var_66;
                            local_sp_0 = var_63;
                            if (!(((var_62 & 4U) == 0U) && ((uint32_t)((uint16_t)var_62 & (unsigned short)512U) == 0U)) && (int)var_66 > (int)4294967295U) {
                                *var_61 = (*var_61 | 4U);
                            }
                            var_67 = var_60 + 96UL;
                            *(uint64_t *)(local_sp_0 + (-8L)) = 4220530UL;
                            indirect_placeholder_1(4294967295UL, var_67);
                            return rax_0;
                        }
                    }
                    break;
                }
            }
        }
    }
}
