typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_135_ret_type;
struct indirect_placeholder_136_ret_type;
struct indirect_placeholder_137_ret_type;
struct indirect_placeholder_138_ret_type;
struct indirect_placeholder_135_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_136_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_137_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_138_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_28(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_5(uint64_t param_0);
extern uint64_t indirect_placeholder_2(void);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern uint64_t indirect_placeholder_4(uint64_t param_0);
extern struct indirect_placeholder_135_ret_type indirect_placeholder_135(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_136_ret_type indirect_placeholder_136(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_137_ret_type indirect_placeholder_137(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_138_ret_type indirect_placeholder_138(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
uint64_t bb_chdir_long(uint64_t rdi) {
    uint64_t r9_1;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint32_t *var_7;
    uint32_t var_8;
    uint32_t var_71;
    uint64_t rax_1;
    uint64_t r8_1;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_48;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_61;
    uint64_t var_60;
    uint64_t local_sp_0;
    uint64_t var_62;
    struct indirect_placeholder_135_ret_type var_63;
    uint64_t var_64;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t local_sp_6;
    uint64_t local_sp_1;
    uint64_t rax_0;
    uint64_t var_17;
    uint64_t local_sp_7;
    uint32_t var_65;
    uint32_t *var_66;
    uint64_t var_27;
    uint64_t var_28;
    struct indirect_placeholder_136_ret_type var_29;
    uint64_t var_30;
    uint32_t *var_31;
    uint64_t var_32;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t *var_26;
    uint64_t var_37;
    struct indirect_placeholder_137_ret_type var_38;
    uint64_t var_39;
    uint64_t local_sp_8;
    uint64_t local_sp_2;
    uint64_t r9_0;
    uint64_t r8_0;
    uint64_t local_sp_3;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t _pre122;
    uint64_t var_44;
    uint64_t local_sp_4;
    uint64_t var_45;
    uint64_t *var_46;
    uint32_t *var_47;
    uint64_t local_sp_5;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    struct indirect_placeholder_138_ret_type var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_16;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t *var_21;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_72;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_cc_src2();
    var_2 = init_rbp();
    var_3 = init_r9();
    var_4 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    var_5 = var_0 + (-96L);
    var_6 = (uint64_t *)var_5;
    *var_6 = rdi;
    *(uint64_t *)(var_0 + (-112L)) = 4244155UL;
    indirect_placeholder();
    var_7 = (uint32_t *)(var_0 + (-12L));
    var_8 = (uint32_t)rdi;
    *var_7 = var_8;
    r9_0 = var_3;
    r8_0 = var_4;
    var_71 = 0U;
    rax_1 = 4294967295UL;
    if (var_8 != 0U) {
        *(uint64_t *)(var_0 + (-120L)) = 4244169UL;
        indirect_placeholder();
        if (*(uint32_t *)rdi != 36U) {
            var_9 = *var_6;
            *(uint64_t *)(var_0 + (-128L)) = 4244196UL;
            indirect_placeholder();
            var_10 = (uint64_t *)(var_0 + (-24L));
            *var_10 = var_9;
            var_11 = var_9 + *var_6;
            var_12 = (uint64_t *)(var_0 + (-32L));
            *var_12 = var_11;
            var_13 = var_0 + (-76L);
            var_14 = var_0 + (-136L);
            *(uint64_t *)var_14 = 4244227UL;
            indirect_placeholder_5(var_13);
            var_15 = *var_10;
            var_17 = var_15;
            local_sp_7 = var_14;
            if (var_15 == 0UL) {
                var_16 = var_0 + (-144L);
                *(uint64_t *)var_16 = 4244259UL;
                indirect_placeholder();
                var_17 = *var_10;
                local_sp_7 = var_16;
            }
            local_sp_8 = local_sp_7;
            if (var_17 > 4095UL) {
                var_18 = local_sp_7 + (-8L);
                *(uint64_t *)var_18 = 4244294UL;
                indirect_placeholder();
                local_sp_8 = var_18;
            }
            var_19 = *var_6;
            var_20 = local_sp_8 + (-8L);
            *(uint64_t *)var_20 = 4244311UL;
            indirect_placeholder();
            var_21 = (uint64_t *)(var_0 + (-40L));
            *var_21 = var_19;
            local_sp_2 = var_20;
            switch (var_19) {
              case 0UL:
                {
                    local_sp_3 = local_sp_2;
                    r9_1 = r9_0;
                    r8_1 = r8_0;
                    rax_1 = 0UL;
                    if (**(unsigned char **)var_5 == '/') {
                        var_42 = local_sp_2 + (-8L);
                        *(uint64_t *)var_42 = 4244541UL;
                        indirect_placeholder();
                        local_sp_3 = var_42;
                    }
                    var_43 = *var_6;
                    _pre122 = var_43;
                    local_sp_4 = local_sp_3;
                    if (var_43 > *var_12) {
                        var_44 = local_sp_3 + (-8L);
                        *(uint64_t *)var_44 = 4244576UL;
                        indirect_placeholder();
                        _pre122 = *var_6;
                        local_sp_4 = var_44;
                    }
                    var_45 = var_0 + (-64L);
                    var_46 = (uint64_t *)var_45;
                    var_47 = (uint32_t *)(var_0 + (-68L));
                    var_48 = _pre122;
                    local_sp_5 = local_sp_4;
                    while (1U)
                        {
                            var_49 = *var_12;
                            local_sp_6 = local_sp_5;
                            if ((long)(var_49 - var_48) <= (long)4095UL) {
                                var_50 = helper_cc_compute_c_wrapper(var_48 - var_49, var_49, var_1, 17U);
                                if (var_50 != 0UL) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                var_51 = *var_6;
                                var_52 = local_sp_5 + (-8L);
                                *(uint64_t *)var_52 = 4244787UL;
                                var_53 = indirect_placeholder_138(var_51, var_13, r9_1, r8_1);
                                var_54 = var_53.field_0;
                                local_sp_1 = var_52;
                                rax_0 = var_54;
                                local_sp_6 = var_52;
                                if ((uint64_t)(uint32_t)var_54 != 0UL) {
                                    loop_state_var = 2U;
                                    break;
                                }
                                loop_state_var = 0U;
                                break;
                            }
                            var_57 = local_sp_5 + (-8L);
                            *(uint64_t *)var_57 = 4244603UL;
                            indirect_placeholder();
                            *var_46 = var_48;
                            local_sp_0 = var_57;
                            if (var_48 != 0UL) {
                                *(uint64_t *)(local_sp_5 + (-16L)) = 4244619UL;
                                indirect_placeholder();
                                *(volatile uint32_t *)(uint32_t *)0UL = 36U;
                                loop_state_var = 1U;
                                break;
                            }
                            **(unsigned char **)var_45 = (unsigned char)'\x00';
                            var_58 = *var_46;
                            var_59 = *var_6;
                            var_61 = var_59;
                            if ((long)(var_58 - var_59) > (long)4095UL) {
                                var_60 = local_sp_5 + (-16L);
                                *(uint64_t *)var_60 = 4244683UL;
                                indirect_placeholder();
                                var_61 = *var_6;
                                local_sp_0 = var_60;
                            }
                            var_62 = local_sp_0 + (-8L);
                            *(uint64_t *)var_62 = 4244702UL;
                            var_63 = indirect_placeholder_135(var_61, var_13, r9_1, r8_1);
                            *var_47 = (uint32_t)var_63.field_0;
                            var_64 = *var_46;
                            *(unsigned char *)var_64 = (unsigned char)'/';
                            local_sp_1 = var_62;
                            rax_0 = var_64;
                            if (*var_47 != 0U) {
                                loop_state_var = 2U;
                                break;
                            }
                            var_67 = var_63.field_2;
                            var_68 = var_63.field_1;
                            var_69 = local_sp_0 + (-16L);
                            *(uint64_t *)var_69 = 4244734UL;
                            var_70 = indirect_placeholder_2();
                            *var_6 = var_70;
                            var_48 = var_70;
                            local_sp_5 = var_69;
                            r9_1 = var_68;
                            r8_1 = var_67;
                            continue;
                        }
                    switch (loop_state_var) {
                      case 1U:
                        {
                            return rax_1;
                        }
                        break;
                      case 2U:
                        {
                            *(uint64_t *)(local_sp_1 + (-8L)) = 4244844UL;
                            indirect_placeholder();
                            var_65 = *(uint32_t *)rax_0;
                            var_66 = (uint32_t *)(var_0 + (-72L));
                            *var_66 = var_65;
                            *(uint64_t *)(local_sp_1 + (-16L)) = 4244861UL;
                            indirect_placeholder_5(var_13);
                            *(uint64_t *)(local_sp_1 + (-24L)) = 4244866UL;
                            indirect_placeholder();
                            *(uint32_t *)var_13 = *var_66;
                        }
                        break;
                      case 0U:
                        {
                            var_55 = local_sp_6 + (-8L);
                            *(uint64_t *)var_55 = 4244803UL;
                            var_56 = indirect_placeholder_4(var_13);
                            local_sp_1 = var_55;
                            rax_0 = var_56;
                            if ((uint64_t)(uint32_t)var_56 != 0UL) {
                                *(uint64_t *)(local_sp_6 + (-16L)) = 4244819UL;
                                indirect_placeholder_5(var_13);
                                return rax_1;
                            }
                            *(uint64_t *)(local_sp_1 + (-8L)) = 4244844UL;
                            indirect_placeholder();
                            var_65 = *(uint32_t *)rax_0;
                            var_66 = (uint32_t *)(var_0 + (-72L));
                            *var_66 = var_65;
                            *(uint64_t *)(local_sp_1 + (-16L)) = 4244861UL;
                            indirect_placeholder_5(var_13);
                            *(uint64_t *)(local_sp_1 + (-24L)) = 4244866UL;
                            indirect_placeholder();
                            *(uint32_t *)var_13 = *var_66;
                        }
                        break;
                    }
                }
                break;
              case 2UL:
                {
                    var_22 = *var_6 + 3UL;
                    var_23 = *var_12 - var_22;
                    *(uint64_t *)(local_sp_8 + (-16L)) = 4244365UL;
                    var_24 = indirect_placeholder_28(var_23, 47UL, var_22);
                    var_25 = var_0 + (-48L);
                    var_26 = (uint64_t *)var_25;
                    *var_26 = var_24;
                    if (var_24 != 0UL) {
                        *(uint64_t *)(local_sp_8 + (-24L)) = 4244381UL;
                        indirect_placeholder();
                        *(volatile uint32_t *)(uint32_t *)0UL = 36U;
                        return rax_1;
                    }
                    **(unsigned char **)var_25 = (unsigned char)'\x00';
                    var_27 = *var_6;
                    var_28 = local_sp_8 + (-24L);
                    *(uint64_t *)var_28 = 4244423UL;
                    var_29 = indirect_placeholder_136(var_27, var_13, var_3, var_4);
                    var_30 = var_29.field_0;
                    var_31 = (uint32_t *)(var_0 + (-52L));
                    *var_31 = (uint32_t)var_30;
                    var_32 = *var_26;
                    *(unsigned char *)var_32 = (unsigned char)'/';
                    local_sp_1 = var_28;
                    rax_0 = var_32;
                    if (*var_31 != 0U) {
                        *(uint64_t *)(local_sp_1 + (-8L)) = 4244844UL;
                        indirect_placeholder();
                        var_65 = *(uint32_t *)rax_0;
                        var_66 = (uint32_t *)(var_0 + (-72L));
                        *var_66 = var_65;
                        *(uint64_t *)(local_sp_1 + (-16L)) = 4244861UL;
                        indirect_placeholder_5(var_13);
                        *(uint64_t *)(local_sp_1 + (-24L)) = 4244866UL;
                        indirect_placeholder();
                        *(uint32_t *)var_13 = *var_66;
                        return rax_1;
                    }
                    var_33 = var_29.field_2;
                    var_34 = var_29.field_1;
                    var_35 = local_sp_8 + (-32L);
                    *(uint64_t *)var_35 = 4244459UL;
                    var_36 = indirect_placeholder_2();
                    *var_6 = var_36;
                    local_sp_2 = var_35;
                    r9_0 = var_34;
                    r8_0 = var_33;
                }
                break;
              default:
                {
                    var_37 = local_sp_8 + (-16L);
                    *(uint64_t *)var_37 = 4244489UL;
                    var_38 = indirect_placeholder_137(4308942UL, var_13, var_3, var_4);
                    var_39 = var_38.field_0;
                    local_sp_1 = var_37;
                    rax_0 = var_39;
                    local_sp_2 = var_37;
                    if ((uint64_t)(uint32_t)var_39 != 0UL) {
                        *(uint64_t *)(local_sp_1 + (-8L)) = 4244844UL;
                        indirect_placeholder();
                        var_65 = *(uint32_t *)rax_0;
                        var_66 = (uint32_t *)(var_0 + (-72L));
                        *var_66 = var_65;
                        *(uint64_t *)(local_sp_1 + (-16L)) = 4244861UL;
                        indirect_placeholder_5(var_13);
                        *(uint64_t *)(local_sp_1 + (-24L)) = 4244866UL;
                        indirect_placeholder();
                        *(uint32_t *)var_13 = *var_66;
                        return rax_1;
                    }
                    var_40 = var_38.field_2;
                    var_41 = var_38.field_1;
                    *var_6 = (*var_6 + *var_21);
                    r9_0 = var_41;
                    r8_0 = var_40;
                }
                break;
            }
        }
        var_71 = *var_7;
    }
    var_72 = (uint64_t)var_71;
    rax_1 = var_72;
    return rax_1;
}
