typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_47_ret_type;
struct indirect_placeholder_50_ret_type;
struct indirect_placeholder_49_ret_type;
struct indirect_placeholder_48_ret_type;
struct indirect_placeholder_51_ret_type;
struct indirect_placeholder_52_ret_type;
struct indirect_placeholder_53_ret_type;
struct indirect_placeholder_54_ret_type;
struct indirect_placeholder_55_ret_type;
struct indirect_placeholder_47_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_50_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_49_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_48_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_51_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_52_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_53_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_54_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_55_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_28(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_5(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t init_rcx(void);
extern uint64_t indirect_placeholder_35(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern struct indirect_placeholder_47_ret_type indirect_placeholder_47(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_50_ret_type indirect_placeholder_50(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_49_ret_type indirect_placeholder_49(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_48_ret_type indirect_placeholder_48(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_51_ret_type indirect_placeholder_51(uint64_t param_0);
extern struct indirect_placeholder_52_ret_type indirect_placeholder_52(uint64_t param_0);
extern struct indirect_placeholder_53_ret_type indirect_placeholder_53(uint64_t param_0);
extern struct indirect_placeholder_54_ret_type indirect_placeholder_54(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_55_ret_type indirect_placeholder_55(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
uint64_t bb_fts_build(uint64_t rsi, uint64_t rdi, uint64_t r9, uint64_t r8) {
    struct indirect_placeholder_54_ret_type var_60;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint32_t *var_8;
    uint64_t var_9;
    uint64_t *var_10;
    bool var_11;
    unsigned char *var_12;
    unsigned char var_13;
    uint64_t *var_148;
    uint64_t var_149;
    uint64_t r14_1;
    uint64_t var_150;
    uint64_t *_pre_phi408;
    uint64_t local_sp_0;
    uint64_t var_124;
    uint64_t storemerge6;
    uint64_t local_sp_15;
    uint32_t var_142;
    uint64_t rax_1;
    unsigned char storemerge5;
    uint64_t local_sp_1;
    uint64_t r14_0_ph;
    uint64_t r13_0;
    uint64_t local_sp_11_ph;
    uint64_t local_sp_13;
    uint64_t _pre_phi399;
    uint64_t local_sp_2;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t var_107;
    uint64_t var_108;
    uint64_t var_109;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t var_112;
    uint32_t var_113;
    uint64_t var_114;
    uint64_t local_sp_17;
    uint64_t var_115;
    uint64_t local_sp_3;
    uint32_t var_119;
    uint32_t var_116;
    uint64_t var_117;
    struct indirect_placeholder_47_ret_type var_118;
    uint64_t local_sp_9;
    uint64_t var_120;
    uint64_t var_121;
    uint64_t var_122;
    uint64_t var_123;
    uint64_t _pre_phi410;
    uint64_t local_sp_4;
    uint64_t rax_0;
    uint32_t var_125;
    uint32_t *var_126;
    uint32_t var_127;
    uint64_t var_128;
    uint64_t var_131;
    uint64_t var_132;
    uint64_t local_sp_12;
    uint64_t var_129;
    uint32_t var_130;
    uint64_t var_100;
    uint64_t local_sp_11;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t var_69;
    uint64_t local_sp_19;
    uint64_t local_sp_5;
    uint64_t local_sp_10;
    uint64_t var_41;
    struct indirect_placeholder_50_ret_type var_42;
    uint64_t var_43;
    uint64_t local_sp_6;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    struct indirect_placeholder_48_ret_type var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_18;
    uint64_t rcx_0;
    uint64_t r93_0;
    uint64_t r84_0;
    uint64_t rbx_0;
    uint64_t spec_select;
    uint64_t *var_44;
    uint64_t local_sp_8;
    uint32_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    struct indirect_placeholder_51_ret_type var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t rcx_2;
    uint64_t r93_2;
    uint64_t r84_2;
    uint64_t rbx_2;
    uint64_t rcx_3;
    uint64_t r93_3;
    uint64_t r84_3;
    uint64_t rbx_3;
    unsigned char storemerge8;
    unsigned char *var_54;
    unsigned char var_55;
    uint64_t rbx_4;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t _;
    uint64_t *var_74;
    uint32_t *var_75;
    uint64_t *var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t *var_79;
    uint64_t *_pre_phi400;
    uint64_t var_80;
    uint64_t *var_81;
    uint64_t *_pre_phi398;
    uint64_t var_82;
    uint64_t *var_83;
    uint64_t var_84;
    uint64_t *var_85;
    uint64_t var_86;
    uint64_t *var_87;
    unsigned char *var_88;
    uint64_t *var_89;
    uint64_t *var_90;
    uint64_t *var_91;
    uint64_t *var_92;
    uint64_t *var_93;
    uint64_t *var_94;
    uint64_t *var_95;
    unsigned char *var_96;
    uint64_t r13_1_ph;
    uint64_t var_97;
    uint64_t var_133;
    uint64_t r13_2;
    uint64_t local_sp_14;
    uint64_t var_134;
    uint64_t local_sp_15365;
    uint64_t var_135;
    uint64_t var_139;
    struct indirect_placeholder_52_ret_type var_140;
    uint64_t var_141;
    uint64_t var_136;
    uint64_t var_137;
    uint64_t var_138;
    uint16_t *var_152;
    uint64_t var_143;
    uint64_t var_144;
    struct indirect_placeholder_53_ret_type var_145;
    uint64_t var_146;
    uint64_t var_147;
    uint64_t local_sp_16;
    uint64_t rbx_5;
    uint64_t r13_3;
    uint64_t var_151;
    uint64_t var_98;
    uint64_t var_99;
    uint32_t *var_56;
    uint32_t *_pre_phi404;
    uint32_t *var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint32_t var_61;
    uint64_t var_62;
    uint64_t local_sp_18;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t rax_2;
    uint64_t var_66;
    uint16_t *var_67;
    uint64_t var_68;
    uint64_t var_14;
    uint64_t var_15;
    uint32_t *var_16;
    uint32_t var_17;
    uint32_t *var_19;
    uint32_t var_20;
    uint64_t storemerge2;
    uint64_t storemerge;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    struct indirect_placeholder_55_ret_type var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_cc_src2();
    var_2 = init_rbp();
    var_3 = init_rcx();
    var_4 = init_rbx();
    var_5 = init_r12();
    var_6 = init_r13();
    var_7 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_7;
    *(uint64_t *)(var_0 + (-24L)) = var_6;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    var_8 = (uint32_t *)(var_0 + (-172L));
    *var_8 = (uint32_t)rsi;
    var_9 = *(uint64_t *)rdi;
    var_10 = (uint64_t *)(var_0 + (-80L));
    *var_10 = var_9;
    var_11 = (*(uint64_t *)(var_9 + 24UL) != 0UL);
    var_12 = (unsigned char *)(var_0 + (-81L));
    var_13 = var_11;
    *var_12 = var_13;
    rax_1 = 0UL;
    storemerge5 = (unsigned char)'\x00';
    r14_0_ph = 0UL;
    rax_0 = 0UL;
    rcx_0 = var_3;
    r93_0 = r9;
    r84_0 = r8;
    rbx_0 = rdi;
    storemerge8 = (unsigned char)'\x01';
    r13_1_ph = 0UL;
    storemerge2 = 131072UL;
    storemerge = 4294967196UL;
    if (var_13 == '\x00') {
        var_19 = (uint32_t *)(rdi + 72UL);
        var_20 = *var_19;
        if ((var_20 & 16U) == 0U) {
            storemerge2 = 0UL;
        } else {
            if ((var_20 & 1U) != 0U & *(uint64_t *)(*var_10 + 88UL) == 0UL) {
                storemerge2 = 0UL;
            }
        }
        var_21 = storemerge2 | (uint64_t)((uint32_t)((uint64_t)var_20 << 7UL) & 262144U);
        var_22 = *(uint64_t *)(*var_10 + 48UL);
        if (((var_20 & 4U) != 0U) || ((uint32_t)((uint16_t)var_20 & (unsigned short)512U) == 0U)) {
            storemerge = (uint64_t)*(uint32_t *)(rdi + 44UL);
        }
        var_23 = var_0 + (-164L);
        var_24 = var_0 + (-192L);
        *(uint64_t *)var_24 = 4224201UL;
        var_25 = indirect_placeholder_55(var_23, var_21, var_22, storemerge, r9, r8);
        var_26 = var_25.field_0;
        var_27 = var_25.field_1;
        var_28 = var_25.field_2;
        var_29 = var_25.field_3;
        *(uint64_t *)(*var_10 + 24UL) = var_26;
        var_30 = *var_10;
        local_sp_6 = var_24;
        rcx_0 = var_27;
        r93_0 = var_28;
        r84_0 = var_29;
        if (*(uint64_t *)(var_30 + 24UL) != 0UL) {
            if (*var_8 == 3U) {
                *(uint16_t *)(var_30 + 112UL) = (uint16_t)(unsigned short)4U;
                *(uint64_t *)(var_0 + (-200L)) = 4224246UL;
                indirect_placeholder();
                *(uint32_t *)(*var_10 + 64UL) = *(uint32_t *)var_30;
            }
            return rax_1;
        }
        if (*(uint16_t *)(var_30 + 112UL) != (unsigned short)11U) {
            *(uint64_t *)(var_0 + (-200L)) = 4224336UL;
            var_31 = indirect_placeholder_3(var_30, rdi);
            var_32 = *var_10;
            *(uint64_t *)(var_0 + (-208L)) = 4224356UL;
            indirect_placeholder_49(0UL, var_32, var_31);
            var_33 = *var_10;
            var_34 = var_0 + (-216L);
            *(uint64_t *)var_34 = 4224371UL;
            var_35 = indirect_placeholder_48(var_33, var_31);
            var_36 = var_35.field_1;
            var_37 = var_35.field_2;
            var_38 = var_35.field_3;
            var_39 = var_35.field_4;
            var_40 = (uint64_t)(uint32_t)var_35.field_0 ^ 1UL;
            local_sp_6 = var_34;
            rcx_0 = var_36;
            r93_0 = var_37;
            r84_0 = var_38;
            rbx_0 = var_39;
            if ((uint32_t)((uint16_t)*var_19 & (unsigned short)256U) != 0U & (uint64_t)(unsigned char)var_40 != 0UL) {
                *(uint64_t *)(var_0 + (-224L)) = 4224383UL;
                indirect_placeholder();
                *(uint32_t *)var_40 = 12U;
                return rax_1;
            }
        }
        var_41 = var_0 + (-200L);
        *(uint64_t *)var_41 = 4224299UL;
        var_42 = indirect_placeholder_50(0UL, var_30, rdi);
        var_43 = var_42.field_1;
        *(uint16_t *)(*var_10 + 112UL) = (uint16_t)var_42.field_0;
        local_sp_6 = var_41;
        rcx_0 = var_43;
    } else {
        var_14 = *(uint64_t *)(*var_10 + 24UL);
        *(uint64_t *)(var_0 + (-96L)) = var_14;
        var_15 = var_0 + (-192L);
        *(uint64_t *)var_15 = 4223991UL;
        indirect_placeholder();
        var_16 = (uint32_t *)(var_0 + (-164L));
        var_17 = (uint32_t)var_14;
        *var_16 = var_17;
        local_sp_6 = var_15;
        if ((int)var_17 <= (int)4294967295U) {
            *(uint64_t *)(var_0 + (-200L)) = 4224027UL;
            indirect_placeholder();
            *(uint64_t *)(*var_10 + 24UL) = 0UL;
            if (*var_8 == 3U) {
                var_18 = *var_10;
                *(uint16_t *)(var_18 + 112UL) = (uint16_t)(unsigned short)4U;
                *(uint64_t *)(var_0 + (-208L)) = 4224063UL;
                indirect_placeholder();
                *(uint32_t *)(*var_10 + 64UL) = *(uint32_t *)var_18;
            }
            return rax_1;
        }
        spec_select = (*(uint64_t *)(rbx_0 + 64UL) == 0UL) ? 100000UL : 18446744073709551615UL;
        var_44 = (uint64_t *)(var_0 + (-104L));
        *var_44 = spec_select;
        local_sp_9 = local_sp_6;
        local_sp_10 = local_sp_6;
        local_sp_8 = local_sp_6;
        rcx_2 = rcx_0;
        r93_2 = r93_0;
        r84_2 = r84_0;
        rbx_2 = rbx_0;
        rcx_3 = rcx_0;
        r93_3 = r93_0;
        r84_3 = r84_0;
        rbx_3 = rbx_0;
        rbx_4 = rbx_0;
        if (*var_12 == '\x00') {
            *(unsigned char *)(var_0 + (-49L)) = (unsigned char)'\x01';
        } else {
            if (*var_8 == 2U) {
                local_sp_9 = local_sp_8;
                rcx_3 = rcx_2;
                r93_3 = r93_2;
                r84_3 = r84_2;
                rbx_3 = rbx_2;
                storemerge8 = (unsigned char)'\x00';
            } else {
                var_45 = *(uint32_t *)(rbx_0 + 72UL);
                var_46 = *var_10;
                var_47 = local_sp_6 + (-8L);
                *(uint64_t *)var_47 = 4224509UL;
                var_48 = indirect_placeholder_51(var_46);
                var_49 = var_48.field_0;
                var_50 = var_48.field_1;
                var_51 = var_48.field_2;
                var_52 = var_48.field_3;
                var_53 = var_48.field_4;
                local_sp_9 = var_47;
                local_sp_8 = var_47;
                rcx_2 = var_50;
                r93_2 = var_51;
                r84_2 = var_52;
                rbx_2 = var_53;
                rcx_3 = var_50;
                r93_3 = var_51;
                r84_3 = var_52;
                rbx_3 = var_53;
                if (!(((((var_45 & 8U) == 0U) || ((var_45 & 16U) == 0U)) ^ 1) && ((var_45 & 32U) == 0U)) & *(uint64_t *)(var_46 + 136UL) != 2UL && (uint64_t)(uint32_t)var_49 == 0UL) {
                    local_sp_9 = local_sp_8;
                    rcx_3 = rcx_2;
                    r93_3 = r93_2;
                    r84_3 = r84_2;
                    rbx_3 = rbx_2;
                    storemerge8 = (unsigned char)'\x00';
                }
            }
            var_54 = (unsigned char *)(var_0 + (-49L));
            var_55 = storemerge8 & '\x01';
            *var_54 = var_55;
            local_sp_10 = local_sp_9;
            rbx_4 = rbx_3;
            local_sp_17 = local_sp_9;
            if (var_55 == '\x00') {
                if (*var_8 != 3U) {
                    var_56 = (uint32_t *)(rbx_3 + 72UL);
                    if ((uint32_t)((uint16_t)*var_56 & (unsigned short)512U) == 0U) {
                        _pre_phi404 = (uint32_t *)(var_0 + (-164L));
                    } else {
                        var_57 = (uint32_t *)(var_0 + (-164L));
                        var_58 = (uint64_t)*var_57;
                        var_59 = local_sp_9 + (-8L);
                        *(uint64_t *)var_59 = 4224591UL;
                        var_60 = indirect_placeholder_54(0UL, rcx_3, 3UL, 1030UL, var_58, r93_3, r84_3);
                        *var_57 = (uint32_t)var_60.field_0;
                        _pre_phi404 = var_57;
                        local_sp_17 = var_59;
                    }
                    var_61 = *_pre_phi404;
                    var_62 = (uint64_t)var_61;
                    local_sp_18 = local_sp_17;
                    rax_2 = var_62;
                    if ((int)var_61 < (int)0U) {
                        var_63 = *var_10;
                        var_64 = local_sp_17 + (-8L);
                        *(uint64_t *)var_64 = 4224633UL;
                        var_65 = indirect_placeholder_35(0UL, var_62, var_63, rbx_3);
                        local_sp_10 = var_64;
                        local_sp_18 = var_64;
                        rax_2 = var_65;
                        if ((uint64_t)(uint32_t)var_65 == 0UL) {
                            *var_54 = (unsigned char)'\x01';
                        } else {
                            local_sp_19 = local_sp_18;
                            if (*var_54 != '\x00' & *var_8 == 3U) {
                                var_66 = local_sp_18 + (-8L);
                                *(uint64_t *)var_66 = 4224661UL;
                                indirect_placeholder();
                                *(uint32_t *)(*var_10 + 64UL) = *(uint32_t *)rax_2;
                                local_sp_19 = var_66;
                            }
                            var_67 = (uint16_t *)(*var_10 + 114UL);
                            *var_67 = (*var_67 | (unsigned short)1U);
                            *var_54 = (unsigned char)'\x00';
                            var_68 = local_sp_19 + (-8L);
                            *(uint64_t *)var_68 = 4224711UL;
                            indirect_placeholder();
                            *(uint64_t *)(*var_10 + 24UL) = 0UL;
                            local_sp_5 = var_68;
                            if ((uint32_t)((uint16_t)*var_56 & (unsigned short)512U) != 0U & (int)*_pre_phi404 < (int)0U) {
                                var_69 = local_sp_19 + (-16L);
                                *(uint64_t *)var_69 = 4224758UL;
                                indirect_placeholder();
                                local_sp_5 = var_69;
                            }
                            *(uint64_t *)(*var_10 + 24UL) = 0UL;
                            local_sp_10 = local_sp_5;
                        }
                    } else {
                        local_sp_19 = local_sp_18;
                        if (*var_54 != '\x00' & *var_8 == 3U) {
                            var_66 = local_sp_18 + (-8L);
                            *(uint64_t *)var_66 = 4224661UL;
                            indirect_placeholder();
                            *(uint32_t *)(*var_10 + 64UL) = *(uint32_t *)rax_2;
                            local_sp_19 = var_66;
                        }
                        var_67 = (uint16_t *)(*var_10 + 114UL);
                        *var_67 = (*var_67 | (unsigned short)1U);
                        *var_54 = (unsigned char)'\x00';
                        var_68 = local_sp_19 + (-8L);
                        *(uint64_t *)var_68 = 4224711UL;
                        indirect_placeholder();
                        *(uint64_t *)(*var_10 + 24UL) = 0UL;
                        local_sp_5 = var_68;
                        if ((uint32_t)((uint16_t)*var_56 & (unsigned short)512U) != 0U & (int)*_pre_phi404 < (int)0U) {
                            var_69 = local_sp_19 + (-16L);
                            *(uint64_t *)var_69 = 4224758UL;
                            indirect_placeholder();
                            local_sp_5 = var_69;
                        }
                        *(uint64_t *)(*var_10 + 24UL) = 0UL;
                        local_sp_10 = local_sp_5;
                    }
                }
            } else {
                var_56 = (uint32_t *)(rbx_3 + 72UL);
                if ((uint32_t)((uint16_t)*var_56 & (unsigned short)512U) == 0U) {
                    var_57 = (uint32_t *)(var_0 + (-164L));
                    var_58 = (uint64_t)*var_57;
                    var_59 = local_sp_9 + (-8L);
                    *(uint64_t *)var_59 = 4224591UL;
                    var_60 = indirect_placeholder_54(0UL, rcx_3, 3UL, 1030UL, var_58, r93_3, r84_3);
                    *var_57 = (uint32_t)var_60.field_0;
                    _pre_phi404 = var_57;
                    local_sp_17 = var_59;
                } else {
                    _pre_phi404 = (uint32_t *)(var_0 + (-164L));
                }
                var_61 = *_pre_phi404;
                var_62 = (uint64_t)var_61;
                local_sp_18 = local_sp_17;
                rax_2 = var_62;
                if ((int)var_61 < (int)0U) {
                    var_63 = *var_10;
                    var_64 = local_sp_17 + (-8L);
                    *(uint64_t *)var_64 = 4224633UL;
                    var_65 = indirect_placeholder_35(0UL, var_62, var_63, rbx_3);
                    local_sp_10 = var_64;
                    local_sp_18 = var_64;
                    rax_2 = var_65;
                    if ((uint64_t)(uint32_t)var_65 == 0UL) {
                        *var_54 = (unsigned char)'\x01';
                    } else {
                        local_sp_19 = local_sp_18;
                        if (*var_54 != '\x00' & *var_8 == 3U) {
                            var_66 = local_sp_18 + (-8L);
                            *(uint64_t *)var_66 = 4224661UL;
                            indirect_placeholder();
                            *(uint32_t *)(*var_10 + 64UL) = *(uint32_t *)rax_2;
                            local_sp_19 = var_66;
                        }
                        var_67 = (uint16_t *)(*var_10 + 114UL);
                        *var_67 = (*var_67 | (unsigned short)1U);
                        *var_54 = (unsigned char)'\x00';
                        var_68 = local_sp_19 + (-8L);
                        *(uint64_t *)var_68 = 4224711UL;
                        indirect_placeholder();
                        *(uint64_t *)(*var_10 + 24UL) = 0UL;
                        local_sp_5 = var_68;
                        if ((uint32_t)((uint16_t)*var_56 & (unsigned short)512U) != 0U & (int)*_pre_phi404 < (int)0U) {
                            var_69 = local_sp_19 + (-16L);
                            *(uint64_t *)var_69 = 4224758UL;
                            indirect_placeholder();
                            local_sp_5 = var_69;
                        }
                        *(uint64_t *)(*var_10 + 24UL) = 0UL;
                        local_sp_10 = local_sp_5;
                    }
                } else {
                    local_sp_19 = local_sp_18;
                    if (*var_54 != '\x00' & *var_8 == 3U) {
                        var_66 = local_sp_18 + (-8L);
                        *(uint64_t *)var_66 = 4224661UL;
                        indirect_placeholder();
                        *(uint32_t *)(*var_10 + 64UL) = *(uint32_t *)rax_2;
                        local_sp_19 = var_66;
                    }
                    var_67 = (uint16_t *)(*var_10 + 114UL);
                    *var_67 = (*var_67 | (unsigned short)1U);
                    *var_54 = (unsigned char)'\x00';
                    var_68 = local_sp_19 + (-8L);
                    *(uint64_t *)var_68 = 4224711UL;
                    indirect_placeholder();
                    *(uint64_t *)(*var_10 + 24UL) = 0UL;
                    local_sp_5 = var_68;
                    if ((uint32_t)((uint16_t)*var_56 & (unsigned short)512U) != 0U & (int)*_pre_phi404 < (int)0U) {
                        var_69 = local_sp_19 + (-16L);
                        *(uint64_t *)var_69 = 4224758UL;
                        indirect_placeholder();
                        local_sp_5 = var_69;
                    }
                    *(uint64_t *)(*var_10 + 24UL) = 0UL;
                    local_sp_10 = local_sp_5;
                }
            }
        }
        var_70 = *var_10;
        var_71 = *(uint64_t *)(var_70 + 56UL);
        var_72 = *(uint64_t *)(var_70 + 72UL);
        var_73 = var_72 + (-1L);
        _ = (*(unsigned char *)(var_73 + var_71) == '/') ? var_73 : var_72;
        var_74 = (uint64_t *)(var_0 + (-112L));
        *var_74 = _;
        var_75 = (uint32_t *)(rbx_4 + 72UL);
        local_sp_11_ph = local_sp_10;
        rbx_5 = rbx_4;
        if ((*var_75 & 4U) == 0U) {
            var_80 = var_0 + (-72L);
            var_81 = (uint64_t *)var_80;
            *var_81 = 0UL;
            _pre_phi400 = var_81;
            _pre_phi399 = var_80;
            _pre_phi398 = (uint64_t *)(rbx_4 + 32UL);
        } else {
            var_76 = (uint64_t *)(rbx_4 + 32UL);
            var_77 = _ + *var_76;
            var_78 = var_0 + (-72L);
            var_79 = (uint64_t *)var_78;
            *var_79 = (var_77 + 1UL);
            *(unsigned char *)var_77 = (unsigned char)'/';
            _pre_phi400 = var_79;
            _pre_phi399 = var_78;
            _pre_phi398 = var_76;
        }
        var_82 = *var_74 + 1UL;
        *var_74 = var_82;
        var_83 = (uint64_t *)(rbx_4 + 48UL);
        var_84 = *var_83 - var_82;
        var_85 = (uint64_t *)(var_0 + (-64L));
        *var_85 = var_84;
        var_86 = *(uint64_t *)(*var_10 + 88UL) + 1UL;
        var_87 = (uint64_t *)(var_0 + (-120L));
        *var_87 = var_86;
        var_88 = (unsigned char *)(var_0 + (-50L));
        *var_88 = (unsigned char)'\x00';
        var_89 = (uint64_t *)(var_0 + (-48L));
        *var_89 = 0UL;
        var_90 = (uint64_t *)(var_0 + (-128L));
        var_91 = (uint64_t *)(var_0 + (-136L));
        var_92 = (uint64_t *)(var_0 + (-144L));
        var_93 = (uint64_t *)(var_0 + (-152L));
        var_94 = (uint64_t *)rbx_4;
        var_95 = (uint64_t *)(rbx_4 + 64UL);
        var_96 = (unsigned char *)(var_0 + (-153L));
        _pre_phi408 = var_95;
        while (1U)
            {
                local_sp_11 = local_sp_11_ph;
                r13_2 = r13_1_ph;
                r14_1 = r14_0_ph;
                while (1U)
                    {
                        var_97 = *(uint64_t *)(*var_10 + 24UL);
                        local_sp_12 = local_sp_11;
                        if (var_97 != 0UL) {
                            loop_state_var = 2U;
                            break;
                        }
                        *(uint64_t *)(local_sp_11 + (-8L)) = 4224949UL;
                        indirect_placeholder();
                        *(uint32_t *)var_97 = 0U;
                        var_98 = *(uint64_t *)(*var_10 + 24UL);
                        var_99 = local_sp_11 + (-16L);
                        *(uint64_t *)var_99 = 4224971UL;
                        indirect_placeholder();
                        *var_90 = var_98;
                        local_sp_11 = var_99;
                        if (var_98 == 0UL) {
                            if ((*var_75 & 32U) != 0U) {
                                loop_state_var = 1U;
                                break;
                            }
                            var_100 = var_98 + 19UL;
                            _pre_phi410 = var_100;
                            if (*(unsigned char *)var_100 != '.') {
                                loop_state_var = 0U;
                                break;
                            }
                            switch_state_var = 0;
                            switch (*(unsigned char *)(var_98 + 20UL)) {
                              case '\x00':
                                {
                                    continue;
                                }
                                break;
                              case '.':
                                {
                                    if (*(unsigned char *)(var_98 + 21UL) != '\x00') {
                                        continue;
                                    }
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              default:
                                {
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                            }
                            if (switch_state_var)
                                break;
                        }
                        var_129 = local_sp_11 + (-24L);
                        *(uint64_t *)var_129 = 4224987UL;
                        indirect_placeholder();
                        var_130 = *(volatile uint32_t *)(uint32_t *)0UL;
                        local_sp_12 = var_129;
                        if (var_130 != 0U) {
                            loop_state_var = 2U;
                            break;
                        }
                        var_131 = (uint64_t)var_130;
                        var_132 = local_sp_11 + (-32L);
                        *(uint64_t *)var_132 = 4225002UL;
                        indirect_placeholder();
                        *(uint32_t *)(*var_10 + 64UL) = *(uint32_t *)var_131;
                        *(uint16_t *)(*var_10 + 112UL) = (((*var_12 == '\x00') && (r14_0_ph == 0UL)) ? (unsigned short)4U : (unsigned short)7U);
                        local_sp_12 = var_132;
                        loop_state_var = 2U;
                        break;
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 2U:
                    {
                        local_sp_13 = local_sp_12;
                        if (*(uint64_t *)(*var_10 + 24UL) != 0UL) {
                            loop_state_var = 2U;
                            switch_state_var = 1;
                            break;
                        }
                        var_133 = local_sp_12 + (-8L);
                        *(uint64_t *)var_133 = 4225886UL;
                        indirect_placeholder();
                        *(uint64_t *)(*var_10 + 24UL) = 0UL;
                        local_sp_13 = var_133;
                        loop_state_var = 2U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 1U:
                  case 0U:
                    {
                        switch (loop_state_var) {
                          case 0U:
                            {
                            }
                            break;
                          case 1U:
                            {
                                _pre_phi410 = var_98 + 19UL;
                            }
                            break;
                        }
                        *(uint64_t *)(local_sp_11 + (-24L)) = 4225130UL;
                        indirect_placeholder();
                        *var_91 = _pre_phi410;
                        var_101 = local_sp_11 + (-32L);
                        *(uint64_t *)var_101 = 4225160UL;
                        var_102 = indirect_placeholder_3(_pre_phi410, rbx_4);
                        r13_0 = var_102;
                        local_sp_2 = var_101;
                        local_sp_4 = var_101;
                        if (var_102 != 0UL) {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        var_103 = *var_91;
                        var_104 = *var_85;
                        var_105 = helper_cc_compute_c_wrapper(var_103 - var_104, var_104, var_1, 17U);
                        if (var_105 != 0UL) {
                            *var_92 = *_pre_phi398;
                            var_106 = (*var_74 + *var_91) + 1UL;
                            var_107 = local_sp_11 + (-40L);
                            *(uint64_t *)var_107 = 4225219UL;
                            var_108 = indirect_placeholder_3(var_106, rbx_4);
                            var_109 = (uint64_t)(uint32_t)var_108 ^ 1UL;
                            local_sp_2 = var_107;
                            local_sp_4 = var_107;
                            rax_0 = var_109;
                            if ((uint64_t)(unsigned char)var_109 != 0UL) {
                                loop_state_var = 0U;
                                switch_state_var = 1;
                                break;
                            }
                            *var_88 = (unsigned char)'\x01';
                            if (*var_92 != *_pre_phi398 & (*var_75 & 4U) == 0U) {
                                *_pre_phi400 = (*var_74 + *_pre_phi398);
                            }
                            *var_85 = (*var_83 - *var_74);
                        }
                        var_110 = *var_91 + *var_74;
                        *var_93 = var_110;
                        var_111 = *var_74;
                        var_112 = helper_cc_compute_c_wrapper(var_110 - var_111, var_111, var_1, 17U);
                        local_sp_3 = local_sp_2;
                        if (var_112 != 0UL) {
                            *(uint64_t *)(local_sp_2 + (-8L)) = 4225421UL;
                            indirect_placeholder();
                            *(uint64_t *)(local_sp_2 + (-16L)) = 4225429UL;
                            indirect_placeholder_5(r13_1_ph);
                            *(uint64_t *)(local_sp_2 + (-24L)) = 4225445UL;
                            indirect_placeholder();
                            *(uint64_t *)(*var_10 + 24UL) = 0UL;
                            *(uint16_t *)(*var_10 + 112UL) = (uint16_t)(unsigned short)7U;
                            var_113 = *var_75;
                            var_114 = (uint64_t)(var_113 & (-65281)) | ((uint64_t)((uint16_t)(uint64_t)var_113 & (unsigned short)48896U) | 16384UL);
                            *var_75 = (uint32_t)var_114;
                            *(uint64_t *)(local_sp_2 + (-32L)) = 4225481UL;
                            indirect_placeholder();
                            *(uint32_t *)var_114 = 36U;
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        *(uint64_t *)(var_102 + 88UL) = *var_87;
                        *(uint64_t *)(var_102 + 8UL) = *var_94;
                        *(uint64_t *)(var_102 + 72UL) = *var_93;
                        *(uint64_t *)(var_102 + 128UL) = 0UL;
                        if ((*var_75 & 4U) == 0U) {
                            *(uint64_t *)(var_102 + 48UL) = (var_102 + 264UL);
                        } else {
                            *(uint64_t *)(var_102 + 48UL) = *(uint64_t *)(var_102 + 56UL);
                            var_115 = local_sp_2 + (-8L);
                            *(uint64_t *)var_115 = 4225590UL;
                            indirect_placeholder();
                            local_sp_3 = var_115;
                        }
                        if (*var_95 != 0UL) {
                            var_116 = *var_75;
                            var_119 = var_116;
                            if ((uint32_t)((uint16_t)var_116 & (unsigned short)1024U) != 0U) {
                                var_117 = local_sp_3 + (-8L);
                                *(uint64_t *)var_117 = 4225785UL;
                                var_118 = indirect_placeholder_47(0UL, var_102, rbx_4);
                                *(uint16_t *)(var_102 + 112UL) = (uint16_t)var_118.field_0;
                                local_sp_1 = var_117;
                                *(uint64_t *)(var_102 + 16UL) = 0UL;
                                local_sp_11_ph = local_sp_1;
                                local_sp_13 = local_sp_1;
                                if (r13_1_ph == 0UL) {
                                    *var_89 = var_102;
                                } else {
                                    *(uint64_t *)(*var_89 + 16UL) = var_102;
                                    *var_89 = var_102;
                                    r13_0 = r13_1_ph;
                                }
                                var_124 = r14_0_ph + 1UL;
                                r13_1_ph = r13_0;
                                r14_0_ph = var_124;
                                r13_2 = r13_0;
                                r14_1 = var_124;
                                if (*var_44 > var_124) {
                                    continue;
                                }
                                loop_state_var = 2U;
                                switch_state_var = 1;
                                break;
                            }
                        }
                        var_119 = *var_75;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
        switch (loop_state_var) {
          case 0U:
            {
                *(uint64_t *)(local_sp_4 + (-8L)) = 4225234UL;
                indirect_placeholder();
                var_125 = *(uint32_t *)rax_0;
                var_126 = (uint32_t *)(var_0 + (-160L));
                *var_126 = var_125;
                *(uint64_t *)(local_sp_4 + (-16L)) = 4225250UL;
                indirect_placeholder();
                *(uint64_t *)(local_sp_4 + (-24L)) = 4225258UL;
                indirect_placeholder_5(r13_1_ph);
                *(uint64_t *)(local_sp_4 + (-32L)) = 4225274UL;
                indirect_placeholder();
                *(uint64_t *)(*var_10 + 24UL) = 0UL;
                *(uint16_t *)(*var_10 + 112UL) = (uint16_t)(unsigned short)7U;
                var_127 = *var_75;
                var_128 = (uint64_t)(var_127 & (-65281)) | ((uint64_t)((uint16_t)(uint64_t)var_127 & (unsigned short)48896U) | 16384UL);
                *var_75 = (uint32_t)var_128;
                *(uint64_t *)(local_sp_4 + (-40L)) = 4225310UL;
                indirect_placeholder();
                *(uint32_t *)var_128 = *var_126;
            }
            break;
          case 1U:
            {
                return rax_1;
            }
            break;
          case 2U:
            {
                local_sp_14 = local_sp_13;
                r13_3 = r13_2;
                if (*var_88 == '\x00') {
                    var_134 = local_sp_13 + (-8L);
                    *(uint64_t *)var_134 = 4225921UL;
                    indirect_placeholder_1(r13_2, rbx_4);
                    local_sp_14 = var_134;
                }
                local_sp_15 = local_sp_14;
                local_sp_15365 = local_sp_14;
                if ((*var_75 & 4U) != 0U) {
                    if ((*var_74 == *var_83) || (r14_1 == 0UL)) {
                        *_pre_phi400 = (*_pre_phi400 + (-1L));
                    }
                    **(unsigned char **)_pre_phi399 = (unsigned char)'\x00';
                }
                if (*var_12 == '\x01') {
                    if (*(unsigned char *)(var_0 + (-49L)) != '\x00') {
                        local_sp_15365 = local_sp_15;
                        if (r14_1 != 0UL) {
                            if (*var_8 == 3U) {
                                *(uint64_t *)(local_sp_15 + (-8L)) = 4226152UL;
                                indirect_placeholder_5(r13_2);
                                return rax_1;
                            }
                            break;
                        }
                    }
                    if (!((*var_8 == 1U) || (r14_1 == 0UL))) {
                        var_135 = *var_10;
                        if (*(uint64_t *)(var_135 + 88UL) == 0UL) {
                            var_139 = local_sp_14 + (-8L);
                            *(uint64_t *)var_139 = 4226010UL;
                            var_140 = indirect_placeholder_52(rbx_4);
                            var_141 = var_140.field_0;
                            local_sp_0 = var_139;
                            storemerge6 = (var_141 & (-256L)) | ((uint64_t)(uint32_t)var_141 != 0UL);
                        } else {
                            var_136 = *(uint64_t *)(var_135 + 8UL);
                            var_137 = local_sp_14 + (-8L);
                            *(uint64_t *)var_137 = 4226046UL;
                            var_138 = indirect_placeholder_35(4308003UL, 4294967295UL, var_136, rbx_4);
                            local_sp_0 = var_137;
                            storemerge6 = (var_138 & (-256L)) | ((uint64_t)(uint32_t)var_138 != 0UL);
                        }
                        local_sp_15 = local_sp_0;
                        if ((uint64_t)(unsigned char)storemerge6 != 0UL) {
                            *(uint16_t *)(*var_10 + 112UL) = (uint16_t)(unsigned short)7U;
                            var_142 = *var_75;
                            *var_75 = ((var_142 & (-65281)) | ((uint32_t)((uint16_t)var_142 & (unsigned short)48896U) | 16384U));
                            *(uint64_t *)(local_sp_0 + (-8L)) = 4226082UL;
                            indirect_placeholder_5(r13_2);
                            return rax_1;
                        }
                        local_sp_15365 = local_sp_15;
                        if (r14_1 != 0UL) {
                            if (*var_8 == 3U) {
                                *(uint64_t *)(local_sp_15 + (-8L)) = 4226152UL;
                                indirect_placeholder_5(r13_2);
                                return rax_1;
                            }
                            break;
                        }
                    }
                }
                local_sp_15365 = local_sp_15;
                if (r14_1 != 0UL) {
                    if (*var_8 == 3U) {
                        *(uint64_t *)(local_sp_15 + (-8L)) = 4226152UL;
                        indirect_placeholder_5(r13_2);
                        return rax_1;
                    }
                    break;
                }
                local_sp_16 = local_sp_15365;
                if (r14_1 <= 10000UL & *var_95 != 0UL) {
                    var_143 = *var_10;
                    var_144 = local_sp_15365 + (-8L);
                    *(uint64_t *)var_144 = 4226189UL;
                    var_145 = indirect_placeholder_53(var_143);
                    var_146 = var_145.field_0;
                    var_147 = var_145.field_1;
                    local_sp_16 = var_144;
                    rbx_5 = var_147;
                    if ((uint64_t)(unsigned char)var_146 == 0UL) {
                        _pre_phi408 = (uint64_t *)(var_147 + 64UL);
                    } else {
                        var_148 = (uint64_t *)(var_147 + 64UL);
                        *var_148 = 4223706UL;
                        var_149 = local_sp_15365 + (-16L);
                        *(uint64_t *)var_149 = 4226215UL;
                        var_150 = indirect_placeholder_28(r14_1, r13_2, var_147);
                        *var_148 = 0UL;
                        _pre_phi408 = var_148;
                        local_sp_16 = var_149;
                        r13_3 = var_150;
                    }
                }
                rax_1 = r13_3;
                if ((*_pre_phi408 != 0UL) && (r14_1 > 1UL)) {
                    *(uint64_t *)(local_sp_16 + (-8L)) = 4226255UL;
                    var_151 = indirect_placeholder_28(r14_1, r13_3, rbx_5);
                    rax_1 = var_151;
                }
            }
            break;
        }
    }
}
