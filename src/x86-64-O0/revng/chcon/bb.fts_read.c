typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_36_ret_type;
struct indirect_placeholder_37_ret_type;
struct indirect_placeholder_38_ret_type;
struct indirect_placeholder_39_ret_type;
struct indirect_placeholder_40_ret_type;
struct indirect_placeholder_41_ret_type;
struct indirect_placeholder_42_ret_type;
struct indirect_placeholder_43_ret_type;
struct indirect_placeholder_44_ret_type;
struct indirect_placeholder_36_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_37_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_38_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_39_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_40_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_41_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_42_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_43_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_44_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_22(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_5(uint64_t param_0);
extern uint64_t indirect_placeholder_35(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern void indirect_placeholder_30(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_36_ret_type indirect_placeholder_36(uint64_t param_0);
extern struct indirect_placeholder_37_ret_type indirect_placeholder_37(uint64_t param_0);
extern struct indirect_placeholder_38_ret_type indirect_placeholder_38(uint64_t param_0);
extern struct indirect_placeholder_39_ret_type indirect_placeholder_39(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_40_ret_type indirect_placeholder_40(uint64_t param_0);
extern struct indirect_placeholder_41_ret_type indirect_placeholder_41(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_42_ret_type indirect_placeholder_42(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_43_ret_type indirect_placeholder_43(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_44_ret_type indirect_placeholder_44(uint64_t param_0, uint64_t param_1, uint64_t param_2);
uint64_t bb_fts_read(uint64_t rdi, uint64_t r9, uint64_t r8) {
    uint64_t var_86;
    uint64_t var_101;
    uint64_t var_102;
    uint16_t *var_103;
    uint64_t var_97;
    uint32_t *var_99;
    uint32_t var_100;
    uint64_t local_sp_7;
    uint64_t rbx_2;
    uint16_t *var_104;
    uint64_t var_98;
    uint64_t var_27;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t *_cast;
    uint64_t var_6;
    uint64_t rax_0;
    uint64_t var_57;
    uint32_t var_58;
    uint64_t local_sp_3;
    uint32_t var_107;
    uint64_t var_49;
    uint64_t local_sp_2;
    uint64_t var_91;
    uint64_t var_50;
    uint64_t var_51;
    uint32_t var_52;
    uint64_t var_62;
    uint32_t var_63;
    uint16_t *var_41;
    uint64_t local_sp_1;
    uint64_t var_42;
    uint64_t var_59;
    struct indirect_placeholder_36_ret_type var_60;
    uint64_t var_61;
    uint16_t var_43;
    uint32_t var_44;
    bool var_45;
    uint32_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t rbx_0;
    uint32_t *var_64;
    uint64_t var_65;
    uint64_t rbx_4;
    uint16_t *_pre_phi307;
    uint64_t *var_114;
    uint64_t var_115;
    uint64_t rbx_6;
    uint64_t var_30;
    uint64_t var_31;
    uint16_t *var_32;
    uint64_t var_26;
    uint32_t *var_28;
    uint32_t var_29;
    uint64_t local_sp_4;
    uint64_t local_sp_6;
    uint32_t var_34;
    uint64_t var_112;
    uint64_t var_35;
    struct indirect_placeholder_37_ret_type var_36;
    uint64_t var_37;
    struct indirect_placeholder_38_ret_type var_33;
    uint16_t *var_19;
    uint64_t local_sp_13;
    uint64_t var_20;
    struct indirect_placeholder_39_ret_type var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint16_t *var_24;
    uint16_t var_25;
    uint64_t *_pre_phi319;
    uint64_t var_17;
    uint64_t local_sp_11;
    uint64_t var_87;
    uint64_t storemerge8_in_in;
    uint64_t storemerge8;
    uint64_t var_76;
    uint16_t *var_77;
    uint64_t local_sp_5;
    uint64_t var_80;
    uint64_t rbx_1;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_105;
    uint64_t *var_106;
    uint16_t *_pre_phi311;
    uint64_t var_108;
    struct indirect_placeholder_40_ret_type var_109;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t local_sp_8;
    uint64_t local_sp_9;
    uint64_t rbx_3;
    uint16_t *_pre310;
    struct indirect_placeholder_41_ret_type var_113;
    uint64_t var_116;
    struct indirect_placeholder_42_ret_type var_117;
    uint64_t var_118;
    uint64_t var_119;
    uint64_t var_120;
    uint32_t *var_7;
    uint16_t *var_8;
    uint16_t var_9;
    struct indirect_placeholder_43_ret_type var_121;
    uint16_t *_pre_phi315;
    uint16_t *var_10;
    uint64_t var_93;
    struct indirect_placeholder_44_ret_type var_94;
    uint64_t var_95;
    uint16_t var_96;
    uint32_t var_66;
    uint64_t var_72;
    uint64_t var_67;
    uint64_t *var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t local_sp_10;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_88;
    uint64_t *var_89;
    uint64_t var_90;
    uint64_t local_sp_12;
    uint64_t var_92;
    uint64_t var_11;
    bool var_12;
    uint64_t *var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_18;
    uint64_t local_sp_14;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t *var_40;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = init_r12();
    var_4 = init_r13();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_2;
    var_5 = var_0 + (-56L);
    _cast = (uint64_t *)rdi;
    var_6 = *_cast;
    rbx_2 = var_6;
    rax_0 = 0UL;
    rbx_6 = var_6;
    local_sp_13 = var_5;
    local_sp_11 = var_5;
    var_72 = 0UL;
    local_sp_10 = var_5;
    if (var_6 == 0UL) {
        return rax_0;
    }
    var_7 = (uint32_t *)(rdi + 72UL);
    if ((uint32_t)((uint16_t)*var_7 & (unsigned short)16384U) == 0U) {
        return;
    }
    var_8 = (uint16_t *)(var_6 + 116UL);
    var_9 = *var_8;
    *var_8 = (uint16_t)(unsigned short)3U;
    rax_0 = var_6;
    if ((uint64_t)(var_9 + (unsigned short)65535U) == 0UL) {
        *(uint64_t *)(var_0 + (-64L)) = 4221927UL;
        var_121 = indirect_placeholder_43(0UL, var_6, rdi);
        *(uint16_t *)(var_6 + 112UL) = (uint16_t)var_121.field_0;
    } else {
        if ((uint64_t)(var_9 + (unsigned short)65534U) == 0UL) {
            var_10 = (uint16_t *)(var_6 + 112UL);
            _pre_phi315 = var_10;
            if ((*var_10 + (unsigned short)65524U) >= (unsigned short)2U) {
                var_93 = var_0 + (-64L);
                *(uint64_t *)var_93 = 4221986UL;
                var_94 = indirect_placeholder_44(1UL, var_6, rdi);
                var_95 = var_94.field_1;
                var_96 = (uint16_t)var_94.field_0;
                *var_10 = var_96;
                local_sp_7 = var_93;
                if (var_96 != (unsigned short)1U & (*var_7 & 4U) != 0U) {
                    var_97 = var_0 + (-72L);
                    *(uint64_t *)var_97 = 4222033UL;
                    var_98 = indirect_placeholder_22(var_95, 4308001UL, rdi, r9, r8);
                    var_99 = (uint32_t *)(var_6 + 68UL);
                    var_100 = (uint32_t)var_98;
                    *var_99 = var_100;
                    local_sp_7 = var_97;
                    if ((int)var_100 > (int)4294967295U) {
                        var_103 = (uint16_t *)(var_6 + 114UL);
                        *var_103 = (*var_103 | (unsigned short)2U);
                    } else {
                        var_101 = (uint64_t)var_100;
                        var_102 = var_0 + (-80L);
                        *(uint64_t *)var_102 = 4222048UL;
                        indirect_placeholder();
                        *(uint32_t *)(var_6 + 64UL) = *(uint32_t *)var_101;
                        *var_10 = (uint16_t)(unsigned short)7U;
                        local_sp_7 = var_102;
                    }
                }
                *_cast = rbx_2;
                var_104 = (uint16_t *)(rbx_2 + 112UL);
                rbx_4 = rbx_2;
                _pre_phi307 = var_104;
                _pre_phi311 = var_104;
                local_sp_8 = local_sp_7;
                local_sp_9 = local_sp_7;
                rbx_3 = rbx_2;
                if (*var_104 == (unsigned short)11U) {
                    rax_0 = rbx_4;
                    if (*_pre_phi307 != (unsigned short)1U) {
                        if (*(uint64_t *)(rbx_4 + 88UL) == 0UL) {
                            *(uint64_t *)(rdi + 24UL) = *(uint64_t *)(rbx_4 + 120UL);
                        }
                        *(uint64_t *)(local_sp_9 + (-8L)) = 4223162UL;
                        var_117 = indirect_placeholder_42(rbx_4, rdi);
                        var_118 = var_117.field_0;
                        var_119 = var_117.field_4;
                        var_120 = (uint64_t)(uint32_t)var_118 ^ 1UL;
                        rax_0 = var_119;
                        if ((uint64_t)(unsigned char)var_120 == 0UL) {
                            *(uint64_t *)(local_sp_9 + (-16L)) = 4223174UL;
                            indirect_placeholder();
                            *(uint32_t *)var_120 = 12U;
                            rax_0 = 0UL;
                        }
                    }
                    return rax_0;
                }
                switch (*(uint64_t *)(rbx_2 + 168UL)) {
                  case 1UL:
                    {
                        break;
                    }
                    break;
                  case 2UL:
                    {
                        var_105 = *(uint64_t *)(rbx_2 + 8UL);
                        var_106 = (uint64_t *)(var_0 + (-48L));
                        *var_106 = var_105;
                        if (*(uint64_t *)(var_105 + 104UL) != 0UL) {
                            var_112 = local_sp_8 + (-8L);
                            *(uint64_t *)var_112 = 4223025UL;
                            var_113 = indirect_placeholder_41(0UL, rbx_3, rdi);
                            *_pre_phi311 = (uint16_t)var_113.field_0;
                            _pre_phi307 = _pre_phi311;
                            local_sp_9 = var_112;
                            rbx_4 = rbx_3;
                            if ((uint32_t)((uint16_t)*(uint32_t *)(rbx_3 + 144UL) & (unsigned short)61440U) != 16384U & *(uint64_t *)(rbx_3 + 88UL) != 0UL) {
                                break;
                            }
                        }
                        var_107 = *var_7;
                        if (!(((var_107 & 8U) == 0U) || ((var_107 & 16U) == 0U))) {
                            var_112 = local_sp_8 + (-8L);
                            *(uint64_t *)var_112 = 4223025UL;
                            var_113 = indirect_placeholder_41(0UL, rbx_3, rdi);
                            *_pre_phi311 = (uint16_t)var_113.field_0;
                            _pre_phi307 = _pre_phi311;
                            local_sp_9 = var_112;
                            rbx_4 = rbx_3;
                            if ((uint32_t)((uint16_t)*(uint32_t *)(rbx_3 + 144UL) & (unsigned short)61440U) != 16384U & *(uint64_t *)(rbx_3 + 88UL) != 0UL) {
                                break;
                            }
                        }
                        var_108 = local_sp_7 + (-8L);
                        *(uint64_t *)var_108 = 4223004UL;
                        var_109 = indirect_placeholder_40(var_105);
                        var_110 = var_109.field_0;
                        var_111 = var_109.field_4;
                        local_sp_8 = var_108;
                        rbx_3 = var_111;
                        local_sp_9 = var_108;
                        rbx_4 = var_111;
                        if ((uint64_t)((uint32_t)var_110 + (-2)) != 0UL) {
                            _pre310 = (uint16_t *)(var_111 + 112UL);
                            _pre_phi311 = _pre310;
                            var_112 = local_sp_8 + (-8L);
                            *(uint64_t *)var_112 = 4223025UL;
                            var_113 = indirect_placeholder_41(0UL, rbx_3, rdi);
                            *_pre_phi311 = (uint16_t)var_113.field_0;
                            _pre_phi307 = _pre_phi311;
                            local_sp_9 = var_112;
                            rbx_4 = rbx_3;
                            if ((uint32_t)((uint16_t)*(uint32_t *)(rbx_3 + 144UL) & (unsigned short)61440U) != 16384U & *(uint64_t *)(rbx_3 + 88UL) != 0UL) {
                                break;
                            }
                        }
                        _pre_phi307 = (uint16_t *)(var_111 + 112UL);
                    }
                    break;
                  default:
                    {
                        var_116 = local_sp_7 + (-8L);
                        *(uint64_t *)var_116 = 4223123UL;
                        indirect_placeholder();
                        local_sp_9 = var_116;
                    }
                    break;
                }
            }
        } else {
            _pre_phi315 = (uint16_t *)(var_6 + 112UL);
            if (*_pre_phi315 == (unsigned short)1U) {
                if ((uint64_t)(var_9 + (unsigned short)65532U) != 0UL) {
                    if ((*(uint16_t *)(var_6 + 114UL) & (unsigned short)2U) == (unsigned short)0U) {
                        var_88 = var_0 + (-64L);
                        *(uint64_t *)var_88 = 4222151UL;
                        indirect_placeholder();
                        local_sp_11 = var_88;
                    }
                    var_89 = (uint64_t *)(rdi + 8UL);
                    var_90 = *var_89;
                    local_sp_12 = local_sp_11;
                    if (var_90 == 0UL) {
                        var_91 = local_sp_11 + (-8L);
                        *(uint64_t *)var_91 = 4222174UL;
                        indirect_placeholder_5(var_90);
                        *var_89 = 0UL;
                        local_sp_12 = var_91;
                    }
                    *_pre_phi315 = (uint16_t)(unsigned short)6U;
                    *(uint64_t *)(local_sp_12 + (-8L)) = 4222200UL;
                    var_92 = indirect_placeholder_3(var_6, rdi);
                    rax_0 = var_92;
                    return rax_0;
                }
                var_66 = *var_7;
                if ((var_66 & 64U) != 0U) {
                    if (*(uint64_t *)(var_6 + 120UL) != *(uint64_t *)(rdi + 24UL)) {
                        if ((*(uint16_t *)(var_6 + 114UL) & (unsigned short)2U) == (unsigned short)0U) {
                            var_88 = var_0 + (-64L);
                            *(uint64_t *)var_88 = 4222151UL;
                            indirect_placeholder();
                            local_sp_11 = var_88;
                        }
                        var_89 = (uint64_t *)(rdi + 8UL);
                        var_90 = *var_89;
                        local_sp_12 = local_sp_11;
                        if (var_90 == 0UL) {
                            var_91 = local_sp_11 + (-8L);
                            *(uint64_t *)var_91 = 4222174UL;
                            indirect_placeholder_5(var_90);
                            *var_89 = 0UL;
                            local_sp_12 = var_91;
                        }
                        *_pre_phi315 = (uint16_t)(unsigned short)6U;
                        *(uint64_t *)(local_sp_12 + (-8L)) = 4222200UL;
                        var_92 = indirect_placeholder_3(var_6, rdi);
                        rax_0 = var_92;
                        return rax_0;
                    }
                }
                var_67 = rdi + 8UL;
                var_68 = (uint64_t *)var_67;
                var_69 = *var_68;
                storemerge8_in_in = var_67;
                var_72 = var_69;
                if (var_69 != 0UL & (uint32_t)((uint16_t)var_66 & (unsigned short)8192U) == 0U) {
                    *var_7 = (var_66 & (-8193));
                    var_70 = *var_68;
                    var_71 = var_0 + (-64L);
                    *(uint64_t *)var_71 = 4222258UL;
                    indirect_placeholder_5(var_70);
                    *var_68 = 0UL;
                    var_72 = 0UL;
                    local_sp_10 = var_71;
                }
                if (var_72 == 0UL) {
                    var_78 = local_sp_10 + (-8L);
                    *(uint64_t *)var_78 = 4222372UL;
                    var_79 = indirect_placeholder_35(3UL, rdi, r9, r8);
                    *var_68 = var_79;
                    local_sp_5 = var_78;
                    if (var_79 != 0UL) {
                        if ((uint32_t)((uint16_t)*var_7 & (unsigned short)16384U) != 0U) {
                            if (*(uint32_t *)(var_6 + 64UL) != 0U & *_pre_phi315 == (unsigned short)4U) {
                                *_pre_phi315 = (uint16_t)(unsigned short)7U;
                            }
                            *(uint64_t *)(local_sp_10 + (-16L)) = 4222445UL;
                            var_87 = indirect_placeholder_3(var_6, rdi);
                            rax_0 = var_87;
                        }
                        return rax_0;
                    }
                }
                var_73 = *(uint64_t *)(var_6 + 48UL);
                var_74 = local_sp_10 + (-8L);
                *(uint64_t *)var_74 = 4222300UL;
                var_75 = indirect_placeholder_35(var_73, 4294967295UL, var_6, rdi);
                local_sp_5 = var_74;
                if ((uint64_t)(uint32_t)var_75 != 0UL) {
                    var_76 = local_sp_10 + (-16L);
                    *(uint64_t *)var_76 = 4222313UL;
                    indirect_placeholder();
                    *(uint32_t *)(var_6 + 64UL) = *(uint32_t *)var_75;
                    var_77 = (uint16_t *)(var_6 + 114UL);
                    *var_77 = (*var_77 | (unsigned short)1U);
                    local_sp_5 = var_76;
                    storemerge8 = *(uint64_t *)storemerge8_in_in;
                    while (storemerge8 != 0UL)
                        {
                            *(uint64_t *)(storemerge8 + 48UL) = *(uint64_t *)(*(uint64_t *)(storemerge8 + 8UL) + 48UL);
                            storemerge8_in_in = storemerge8 + 16UL;
                            storemerge8 = *(uint64_t *)storemerge8_in_in;
                        }
                }
                var_80 = *var_68;
                *var_68 = 0UL;
                local_sp_6 = local_sp_5;
                rbx_1 = var_80;
                var_81 = *(uint64_t *)(rdi + 32UL);
                var_82 = *(uint64_t *)(rbx_1 + 8UL);
                var_83 = *(uint64_t *)(var_82 + 56UL);
                var_84 = *(uint64_t *)(var_82 + 72UL);
                var_85 = var_84 + (-1L);
                *(unsigned char *)(((*(unsigned char *)(var_85 + var_83) == '/') ? var_85 : var_84) + var_81) = (unsigned char)'/';
                var_86 = local_sp_6 + (-8L);
                *(uint64_t *)var_86 = 4222909UL;
                indirect_placeholder();
                local_sp_7 = var_86;
                rbx_2 = rbx_1;
            } else {
                while (1U)
                    {
                        var_11 = *(uint64_t *)(rbx_6 + 16UL);
                        var_12 = (var_11 == 0UL);
                        rbx_1 = var_11;
                        rbx_6 = var_11;
                        local_sp_14 = local_sp_13;
                        if (!var_12) {
                            var_13 = (uint64_t *)(rbx_6 + 8UL);
                            var_14 = *var_13;
                            _pre_phi319 = var_13;
                            if (*(uint64_t *)(var_14 + 24UL) != 0UL) {
                                *_cast = var_14;
                                *(unsigned char *)(*(uint64_t *)(var_14 + 72UL) + *(uint64_t *)(rdi + 32UL)) = (unsigned char)'\x00';
                                var_15 = local_sp_13 + (-8L);
                                *(uint64_t *)var_15 = 4222534UL;
                                var_16 = indirect_placeholder_35(3UL, rdi, r9, r8);
                                rbx_1 = var_16;
                                local_sp_14 = var_15;
                                if (var_16 != 0UL) {
                                    if ((uint32_t)((uint16_t)*var_7 & (unsigned short)16384U) != 0U) {
                                        loop_state_var = 2U;
                                        break;
                                    }
                                    loop_state_var = 1U;
                                    break;
                                }
                                var_17 = local_sp_13 + (-16L);
                                *(uint64_t *)var_17 = 4222578UL;
                                indirect_placeholder();
                                local_sp_6 = var_17;
                                loop_state_var = 4U;
                                break;
                            }
                        }
                        if (!var_12) {
                            _pre_phi319 = (uint64_t *)(rbx_6 + 8UL);
                            loop_state_var = 2U;
                            break;
                        }
                        *_cast = var_11;
                        var_18 = local_sp_13 + (-8L);
                        *(uint64_t *)var_18 = 4222608UL;
                        indirect_placeholder();
                        local_sp_6 = var_18;
                        local_sp_13 = var_18;
                        if (*(uint64_t *)(var_11 + 88UL) != 0UL) {
                            *(uint64_t *)(local_sp_13 + (-16L)) = 4222625UL;
                            var_33 = indirect_placeholder_38(rdi);
                            if ((uint64_t)(uint32_t)var_33.field_0 != 0UL) {
                                var_34 = *var_7;
                                *var_7 = ((var_34 & (-65281)) | ((uint32_t)((uint16_t)var_34 & (unsigned short)48896U) | 16384U));
                                loop_state_var = 1U;
                                break;
                            }
                            *(uint64_t *)(local_sp_13 + (-24L)) = 4222660UL;
                            indirect_placeholder_5(rdi);
                            *(uint64_t *)(local_sp_13 + (-32L)) = 4222671UL;
                            indirect_placeholder_1(var_11, rdi);
                            var_35 = local_sp_13 + (-40L);
                            *(uint64_t *)var_35 = 4222679UL;
                            var_36 = indirect_placeholder_37(rdi);
                            var_37 = var_36.field_4;
                            local_sp_7 = var_35;
                            rbx_2 = var_37;
                            loop_state_var = 3U;
                            break;
                        }
                        var_19 = (uint16_t *)(var_11 + 116UL);
                        switch_state_var = 0;
                        switch (*var_19) {
                          case (unsigned short)4U:
                            {
                                continue;
                            }
                            break;
                          case (unsigned short)2U:
                            {
                                var_20 = local_sp_13 + (-16L);
                                *(uint64_t *)var_20 = 4222725UL;
                                var_21 = indirect_placeholder_39(1UL, var_11, rdi);
                                var_22 = var_21.field_0;
                                var_23 = var_21.field_1;
                                var_24 = (uint16_t *)(var_11 + 112UL);
                                var_25 = (uint16_t)var_22;
                                *var_24 = var_25;
                                local_sp_4 = var_20;
                                if (var_25 != (unsigned short)1U) {
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                if ((*var_7 & 4U) != 0U) {
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                var_26 = local_sp_13 + (-24L);
                                *(uint64_t *)var_26 = 4222764UL;
                                var_27 = indirect_placeholder_22(var_23, 4308001UL, rdi, r9, r8);
                                var_28 = (uint32_t *)(var_11 + 68UL);
                                var_29 = (uint32_t)var_27;
                                *var_28 = var_29;
                                local_sp_4 = var_26;
                                if ((int)var_29 <= (int)4294967295U) {
                                    var_32 = (uint16_t *)(var_11 + 114UL);
                                    *var_32 = (*var_32 | (unsigned short)2U);
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                var_30 = (uint64_t)var_29;
                                var_31 = local_sp_13 + (-32L);
                                *(uint64_t *)var_31 = 4222779UL;
                                indirect_placeholder();
                                *(uint32_t *)(var_11 + 64UL) = *(uint32_t *)var_30;
                                *var_24 = (uint16_t)(unsigned short)7U;
                                local_sp_4 = var_31;
                                loop_state_var = 0U;
                                switch_state_var = 1;
                                break;
                            }
                            break;
                          default:
                            {
                                loop_state_var = 4U;
                                switch_state_var = 1;
                                break;
                            }
                            break;
                        }
                        if (switch_state_var)
                            break;
                    }
                switch (loop_state_var) {
                  case 4U:
                  case 3U:
                  case 1U:
                  case 0U:
                    {
                        switch (loop_state_var) {
                          case 1U:
                            {
                                return rax_0;
                            }
                            break;
                          case 4U:
                          case 3U:
                          case 0U:
                            {
                                switch (loop_state_var) {
                                  case 3U:
                                    {
                                        *_cast = rbx_2;
                                        var_104 = (uint16_t *)(rbx_2 + 112UL);
                                        rbx_4 = rbx_2;
                                        _pre_phi307 = var_104;
                                        _pre_phi311 = var_104;
                                        local_sp_8 = local_sp_7;
                                        local_sp_9 = local_sp_7;
                                        rbx_3 = rbx_2;
                                        if (*var_104 == (unsigned short)11U) {
                                            rax_0 = rbx_4;
                                            if (*_pre_phi307 != (unsigned short)1U) {
                                                if (*(uint64_t *)(rbx_4 + 88UL) == 0UL) {
                                                    *(uint64_t *)(rdi + 24UL) = *(uint64_t *)(rbx_4 + 120UL);
                                                }
                                                *(uint64_t *)(local_sp_9 + (-8L)) = 4223162UL;
                                                var_117 = indirect_placeholder_42(rbx_4, rdi);
                                                var_118 = var_117.field_0;
                                                var_119 = var_117.field_4;
                                                var_120 = (uint64_t)(uint32_t)var_118 ^ 1UL;
                                                rax_0 = var_119;
                                                if ((uint64_t)(unsigned char)var_120 == 0UL) {
                                                    *(uint64_t *)(local_sp_9 + (-16L)) = 4223174UL;
                                                    indirect_placeholder();
                                                    *(uint32_t *)var_120 = 12U;
                                                    rax_0 = 0UL;
                                                }
                                            }
                                        } else {
                                            switch (*(uint64_t *)(rbx_2 + 168UL)) {
                                              case 1UL:
                                                {
                                                    break;
                                                }
                                                break;
                                              case 2UL:
                                                {
                                                    var_105 = *(uint64_t *)(rbx_2 + 8UL);
                                                    var_106 = (uint64_t *)(var_0 + (-48L));
                                                    *var_106 = var_105;
                                                    if (*(uint64_t *)(var_105 + 104UL) != 0UL) {
                                                        var_112 = local_sp_8 + (-8L);
                                                        *(uint64_t *)var_112 = 4223025UL;
                                                        var_113 = indirect_placeholder_41(0UL, rbx_3, rdi);
                                                        *_pre_phi311 = (uint16_t)var_113.field_0;
                                                        _pre_phi307 = _pre_phi311;
                                                        local_sp_9 = var_112;
                                                        rbx_4 = rbx_3;
                                                        if ((uint32_t)((uint16_t)*(uint32_t *)(rbx_3 + 144UL) & (unsigned short)61440U) != 16384U & *(uint64_t *)(rbx_3 + 88UL) != 0UL) {
                                                            break;
                                                        }
                                                    }
                                                    var_107 = *var_7;
                                                    if (!(((var_107 & 8U) == 0U) || ((var_107 & 16U) == 0U))) {
                                                        var_112 = local_sp_8 + (-8L);
                                                        *(uint64_t *)var_112 = 4223025UL;
                                                        var_113 = indirect_placeholder_41(0UL, rbx_3, rdi);
                                                        *_pre_phi311 = (uint16_t)var_113.field_0;
                                                        _pre_phi307 = _pre_phi311;
                                                        local_sp_9 = var_112;
                                                        rbx_4 = rbx_3;
                                                        if ((uint32_t)((uint16_t)*(uint32_t *)(rbx_3 + 144UL) & (unsigned short)61440U) != 16384U & *(uint64_t *)(rbx_3 + 88UL) != 0UL) {
                                                            break;
                                                        }
                                                    }
                                                    var_108 = local_sp_7 + (-8L);
                                                    *(uint64_t *)var_108 = 4223004UL;
                                                    var_109 = indirect_placeholder_40(var_105);
                                                    var_110 = var_109.field_0;
                                                    var_111 = var_109.field_4;
                                                    local_sp_8 = var_108;
                                                    rbx_3 = var_111;
                                                    local_sp_9 = var_108;
                                                    rbx_4 = var_111;
                                                    if ((uint64_t)((uint32_t)var_110 + (-2)) != 0UL) {
                                                        _pre310 = (uint16_t *)(var_111 + 112UL);
                                                        _pre_phi311 = _pre310;
                                                        var_112 = local_sp_8 + (-8L);
                                                        *(uint64_t *)var_112 = 4223025UL;
                                                        var_113 = indirect_placeholder_41(0UL, rbx_3, rdi);
                                                        *_pre_phi311 = (uint16_t)var_113.field_0;
                                                        _pre_phi307 = _pre_phi311;
                                                        local_sp_9 = var_112;
                                                        rbx_4 = rbx_3;
                                                        if ((uint32_t)((uint16_t)*(uint32_t *)(rbx_3 + 144UL) & (unsigned short)61440U) != 16384U & *(uint64_t *)(rbx_3 + 88UL) != 0UL) {
                                                            break;
                                                        }
                                                    }
                                                    _pre_phi307 = (uint16_t *)(var_111 + 112UL);
                                                }
                                                break;
                                              default:
                                                {
                                                    var_116 = local_sp_7 + (-8L);
                                                    *(uint64_t *)var_116 = 4223123UL;
                                                    indirect_placeholder();
                                                    local_sp_9 = var_116;
                                                }
                                                break;
                                            }
                                        }
                                    }
                                    break;
                                  case 4U:
                                  case 0U:
                                    {
                                        switch (loop_state_var) {
                                          case 0U:
                                            {
                                                *var_19 = (uint16_t)(unsigned short)3U;
                                                local_sp_6 = local_sp_4;
                                            }
                                            break;
                                          case 4U:
                                            {
                                                var_81 = *(uint64_t *)(rdi + 32UL);
                                                var_82 = *(uint64_t *)(rbx_1 + 8UL);
                                                var_83 = *(uint64_t *)(var_82 + 56UL);
                                                var_84 = *(uint64_t *)(var_82 + 72UL);
                                                var_85 = var_84 + (-1L);
                                                *(unsigned char *)(((*(unsigned char *)(var_85 + var_83) == '/') ? var_85 : var_84) + var_81) = (unsigned char)'/';
                                                var_86 = local_sp_6 + (-8L);
                                                *(uint64_t *)var_86 = 4222909UL;
                                                indirect_placeholder();
                                                local_sp_7 = var_86;
                                                rbx_2 = rbx_1;
                                            }
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }
                            break;
                        }
                    }
                    break;
                  case 2U:
                    {
                        var_38 = *_pre_phi319;
                        *_cast = var_38;
                        var_39 = local_sp_14 + (-8L);
                        *(uint64_t *)var_39 = 4223218UL;
                        indirect_placeholder();
                        var_40 = (uint64_t *)(var_38 + 88UL);
                        local_sp_1 = var_39;
                        rbx_0 = var_38;
                        if (*var_40 == 18446744073709551615UL) {
                            *(uint64_t *)(local_sp_14 + (-16L)) = 4223236UL;
                            indirect_placeholder();
                            *(uint64_t *)(local_sp_14 + (-24L)) = 4223241UL;
                            indirect_placeholder();
                            *(uint32_t *)18446744073709551615UL = 0U;
                            *_cast = 0UL;
                            return rax_0;
                        }
                        var_41 = (uint16_t *)(var_38 + 112UL);
                        if (*var_41 == (unsigned short)11U) {
                            var_42 = local_sp_14 + (-16L);
                            *(uint64_t *)var_42 = 4223280UL;
                            indirect_placeholder();
                            local_sp_1 = var_42;
                        }
                        *(unsigned char *)(*(uint64_t *)(var_38 + 72UL) + *(uint64_t *)(rdi + 32UL)) = (unsigned char)'\x00';
                        local_sp_2 = local_sp_1;
                        local_sp_3 = local_sp_1;
                        if (*var_40 == 0UL) {
                            var_59 = local_sp_1 + (-8L);
                            *(uint64_t *)var_59 = 4223312UL;
                            var_60 = indirect_placeholder_36(rdi);
                            var_61 = var_60.field_0;
                            local_sp_3 = var_59;
                            if ((uint64_t)(uint32_t)var_61 == 0UL) {
                                var_62 = local_sp_1 + (-16L);
                                *(uint64_t *)var_62 = 4223325UL;
                                indirect_placeholder();
                                *(uint32_t *)(var_38 + 64UL) = *(uint32_t *)var_61;
                                var_63 = *var_7;
                                *var_7 = ((var_63 & (-65281)) | ((uint32_t)((uint16_t)var_63 & (unsigned short)48896U) | 16384U));
                                local_sp_3 = var_62;
                            }
                        } else {
                            var_43 = *(uint16_t *)(var_38 + 114UL);
                            if ((var_43 & (unsigned short)2U) == (unsigned short)0U) {
                                var_54 = *(uint64_t *)(var_38 + 8UL);
                                var_55 = local_sp_1 + (-8L);
                                *(uint64_t *)var_55 = 4223506UL;
                                var_56 = indirect_placeholder_35(4308003UL, 4294967295UL, var_54, rdi);
                                local_sp_3 = var_55;
                                if ((var_43 & (unsigned short)1U) != (unsigned short)0U & (uint64_t)(uint32_t)var_56 == 0UL) {
                                    var_57 = local_sp_1 + (-16L);
                                    *(uint64_t *)var_57 = 4223515UL;
                                    indirect_placeholder();
                                    *(uint32_t *)(var_38 + 64UL) = *(uint32_t *)var_56;
                                    var_58 = *var_7;
                                    *var_7 = ((var_58 & (-65281)) | ((uint32_t)((uint16_t)var_58 & (unsigned short)48896U) | 16384U));
                                    local_sp_3 = var_57;
                                }
                            } else {
                                var_44 = *var_7;
                                if ((var_44 & 4U) != 0U) {
                                    var_45 = ((uint32_t)((uint16_t)var_44 & (unsigned short)512U) == 0U);
                                    var_46 = *(uint32_t *)(var_38 + 68UL);
                                    if (var_45) {
                                        var_47 = (uint64_t)var_46;
                                        var_48 = local_sp_1 + (-8L);
                                        *(uint64_t *)var_48 = 4223406UL;
                                        indirect_placeholder_30(1UL, var_47, rdi);
                                        local_sp_2 = var_48;
                                    } else {
                                        var_49 = local_sp_1 + (-8L);
                                        *(uint64_t *)var_49 = 4223423UL;
                                        indirect_placeholder();
                                        local_sp_2 = var_49;
                                        if (var_46 != 0U) {
                                            var_50 = (uint64_t)(var_46 & (-256)) | 1UL;
                                            var_51 = local_sp_1 + (-16L);
                                            *(uint64_t *)var_51 = 4223437UL;
                                            indirect_placeholder();
                                            *(uint32_t *)(var_38 + 64UL) = *(uint32_t *)var_50;
                                            var_52 = *var_7;
                                            *var_7 = ((var_52 & (-65281)) | ((uint32_t)((uint16_t)var_52 & (unsigned short)48896U) | 16384U));
                                            local_sp_2 = var_51;
                                        }
                                    }
                                }
                                var_53 = local_sp_2 + (-8L);
                                *(uint64_t *)var_53 = 4223465UL;
                                indirect_placeholder();
                                local_sp_3 = var_53;
                            }
                        }
                        var_64 = (uint32_t *)(var_38 + 64UL);
                        *var_41 = ((*var_64 == 0U) ? (unsigned short)6U : (unsigned short)7U);
                        if (*var_41 != (unsigned short)2U & *var_64 == 0U) {
                            *(uint64_t *)(local_sp_3 + (-8L)) = 4223584UL;
                            var_65 = indirect_placeholder_3(var_38, rdi);
                            rbx_0 = var_65;
                        }
                        return ((uint32_t)((uint16_t)*var_7 & (unsigned short)16384U) == 0U) ? rbx_0 : 0UL;
                    }
                    break;
                }
            }
        }
    }
}
