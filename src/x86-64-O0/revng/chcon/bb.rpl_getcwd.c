typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_141_ret_type;
struct indirect_placeholder_142_ret_type;
struct indirect_placeholder_141_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_142_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rcx(void);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern uint64_t indirect_placeholder_35(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint64_t indirect_placeholder_4(uint64_t param_0);
extern struct indirect_placeholder_141_ret_type indirect_placeholder_141(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_142_ret_type indirect_placeholder_142(uint64_t param_0);
typedef _Bool bool;
uint64_t bb_rpl_getcwd(uint64_t rsi, uint64_t rdi) {
    uint64_t var_99;
    uint64_t local_sp_4;
    uint64_t rbx_0;
    struct indirect_placeholder_141_ret_type var_53;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_67;
    uint64_t var_55;
    uint64_t var_56;
    uint32_t var_57;
    uint32_t var_68;
    uint64_t var_69;
    uint64_t var_72;
    uint64_t var_103;
    uint32_t var_104;
    uint64_t var_105;
    uint64_t var_73;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t *var_8;
    uint64_t *var_9;
    uint32_t *var_10;
    unsigned char *var_11;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t *var_14;
    uint64_t var_15;
    uint64_t rax_7;
    uint64_t var_128;
    uint64_t local_sp_0;
    uint64_t rax_0;
    uint64_t local_sp_1;
    uint64_t var_25;
    uint64_t *var_26;
    uint64_t var_129;
    uint64_t var_130;
    uint64_t rax_1;
    uint64_t local_sp_11;
    uint64_t local_sp_2;
    uint64_t var_131;
    uint64_t var_132;
    uint64_t rax_2;
    uint64_t var_117;
    uint64_t var_118;
    uint64_t var_119;
    uint64_t var_120;
    uint64_t var_122;
    uint64_t var_121;
    uint64_t local_sp_6;
    uint64_t rax_4;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t local_sp_3;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t rax_3;
    uint64_t var_98;
    uint64_t var_100;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t local_sp_8;
    uint32_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t _pre_phi224;
    uint64_t local_sp_6_be;
    uint64_t rax_6_be;
    uint64_t var_123;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_106;
    uint64_t local_sp_5;
    uint64_t rax_6;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint32_t var_77;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_60;
    uint64_t local_sp_7;
    uint64_t rcx_0;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint32_t var_58;
    uint64_t var_59;
    uint64_t *_pre_phi222;
    uint64_t var_34;
    uint64_t *var_35;
    uint64_t var_36;
    uint64_t *var_37;
    unsigned char *var_38;
    uint32_t *var_39;
    uint64_t *var_40;
    uint64_t *var_41;
    unsigned char *var_42;
    uint64_t *var_43;
    unsigned char *var_44;
    uint32_t *var_45;
    uint32_t *var_46;
    uint64_t *var_47;
    uint64_t *var_48;
    uint64_t *var_49;
    uint64_t *var_50;
    uint64_t r9_0;
    uint64_t r8_0;
    uint64_t rbx_1;
    uint64_t var_107;
    uint64_t local_sp_9;
    uint64_t var_108;
    uint64_t var_109;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t var_113;
    uint64_t var_112;
    uint64_t var_114;
    uint64_t rbx_2;
    uint64_t var_115;
    uint64_t *var_116;
    uint64_t local_sp_10;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_54;
    uint64_t var_27;
    uint64_t *var_28;
    uint64_t *var_29;
    uint64_t var_30;
    uint64_t *var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint32_t var_124;
    uint64_t var_125;
    uint32_t *var_126;
    uint64_t var_127;
    uint64_t rax_8;
    uint64_t var_16;
    uint64_t var_18;
    struct indirect_placeholder_142_ret_type var_19;
    uint64_t var_20;
    uint64_t *var_21;
    uint64_t *var_17;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_cc_src2();
    var_2 = init_rbp();
    var_3 = init_rcx();
    var_4 = init_r9();
    var_5 = init_r8();
    var_6 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    var_7 = var_0 + (-344L);
    var_8 = (uint64_t *)(var_0 + (-336L));
    *var_8 = rdi;
    var_9 = (uint64_t *)var_7;
    *var_9 = rsi;
    var_10 = (uint32_t *)(var_0 + (-28L));
    *var_10 = 4294967196U;
    var_11 = (unsigned char *)(var_0 + (-29L));
    *var_11 = (unsigned char)'\x00';
    var_12 = (uint64_t *)(var_0 + (-40L));
    *var_12 = 0UL;
    var_13 = *var_9;
    var_14 = (uint64_t *)(var_0 + (-72L));
    *var_14 = var_13;
    var_15 = var_13;
    local_sp_11 = var_7;
    rax_6_be = 0UL;
    rcx_0 = var_3;
    r9_0 = var_4;
    r8_0 = var_5;
    rax_8 = 0UL;
    if (*var_9 != 0UL) {
        var_15 = 4096UL;
        if (*var_8 != 0UL) {
            *(uint64_t *)(var_0 + (-352L)) = 4246651UL;
            indirect_placeholder();
            *(uint32_t *)var_13 = 22U;
            return rax_8;
        }
        *var_14 = 4096UL;
    }
    var_16 = *var_8;
    if (var_16 == 0UL) {
        var_18 = var_0 + (-352L);
        *(uint64_t *)var_18 = 4246697UL;
        var_19 = indirect_placeholder_142(var_15);
        var_20 = var_19.field_0;
        var_21 = (uint64_t *)(var_0 + (-64L));
        *var_21 = var_20;
        _pre_phi222 = var_21;
        local_sp_11 = var_18;
        if (var_20 == 0UL) {
            return rax_8;
        }
    }
    var_17 = (uint64_t *)(var_0 + (-64L));
    *var_17 = var_16;
    _pre_phi222 = var_17;
    var_22 = (*var_14 + *_pre_phi222) + (-1L);
    *(unsigned char *)var_22 = (unsigned char)'\x00';
    var_23 = local_sp_11 + (-8L);
    *(uint64_t *)var_23 = 4246768UL;
    var_24 = indirect_placeholder_4(4309043UL);
    rbx_1 = var_22;
    local_sp_10 = var_23;
    rax_7 = var_24;
    if ((int)(uint32_t)var_24 < (int)0U) {
        var_123 = local_sp_10 + (-8L);
        *(uint64_t *)var_123 = 4247915UL;
        indirect_placeholder();
        var_124 = *(uint32_t *)rax_7;
        var_125 = (uint64_t)var_124;
        var_126 = (uint32_t *)(var_0 + (-180L));
        *var_126 = var_124;
        var_127 = *var_12;
        local_sp_0 = var_123;
        rax_0 = var_125;
        if (var_127 == 0UL) {
            var_128 = local_sp_10 + (-16L);
            *(uint64_t *)var_128 = 4247942UL;
            indirect_placeholder();
            local_sp_0 = var_128;
            rax_0 = var_127;
        }
        local_sp_1 = local_sp_0;
        rax_1 = rax_0;
        if (*var_11 == '\x00') {
            var_129 = (uint64_t)*var_10;
            var_130 = local_sp_0 + (-8L);
            *(uint64_t *)var_130 = 4247958UL;
            indirect_placeholder();
            local_sp_1 = var_130;
            rax_1 = var_129;
        }
        local_sp_2 = local_sp_1;
        rax_2 = rax_1;
        if (*var_8 == 0UL) {
            var_131 = *_pre_phi222;
            var_132 = local_sp_1 + (-8L);
            *(uint64_t *)var_132 = 4247980UL;
            indirect_placeholder();
            local_sp_2 = var_132;
            rax_2 = var_131;
        }
        *(uint64_t *)(local_sp_2 + (-8L)) = 4247985UL;
        indirect_placeholder();
        *(uint32_t *)rax_2 = *var_126;
        return rax_8;
    }
    var_25 = var_0 + (-328L);
    var_26 = (uint64_t *)var_25;
    var_27 = *var_26;
    var_28 = (uint64_t *)(var_0 + (-48L));
    *var_28 = var_27;
    var_29 = (uint64_t *)(var_0 + (-320L));
    var_30 = *var_29;
    var_31 = (uint64_t *)(var_0 + (-56L));
    *var_31 = var_30;
    var_32 = local_sp_11 + (-16L);
    *(uint64_t *)var_32 = 4246818UL;
    var_33 = indirect_placeholder_4(4309045UL);
    local_sp_8 = var_32;
    local_sp_10 = var_32;
    rax_7 = var_33;
    if ((int)(uint32_t)var_33 < (int)0U) {
        return;
    }
    var_34 = *var_26;
    var_35 = (uint64_t *)(var_0 + (-96L));
    *var_35 = var_34;
    var_36 = *var_29;
    var_37 = (uint64_t *)(var_0 + (-104L));
    *var_37 = var_36;
    var_38 = (unsigned char *)(var_0 + (-81L));
    var_39 = (uint32_t *)(var_0 + (-116L));
    var_40 = (uint64_t *)(var_0 + (-128L));
    var_41 = (uint64_t *)(var_0 + (-136L));
    var_42 = (unsigned char *)(var_0 + (-137L));
    var_43 = (uint64_t *)(var_0 + (-80L));
    var_44 = (unsigned char *)(var_0 + (-138L));
    var_45 = (uint32_t *)(var_0 + (-144L));
    var_46 = (uint32_t *)(var_0 + (-304L));
    var_47 = (uint64_t *)(var_0 + (-152L));
    var_48 = (uint64_t *)(var_0 + (-160L));
    var_49 = (uint64_t *)(var_0 + (-168L));
    var_50 = (uint64_t *)(var_0 + (-176L));
    while (1U)
        {
            rbx_0 = rbx_1;
            local_sp_9 = local_sp_8;
            rbx_2 = rbx_1;
            if (*var_28 != *var_35) {
                if (*var_31 != *var_37) {
                    var_107 = *var_12;
                    rax_7 = var_107;
                    if (var_107 != 0UL) {
                        loop_state_var = 2U;
                        break;
                    }
                    var_108 = local_sp_8 + (-8L);
                    *(uint64_t *)var_108 = 4247733UL;
                    indirect_placeholder();
                    local_sp_9 = var_108;
                    local_sp_10 = var_108;
                    if ((uint64_t)(uint32_t)var_107 != 0UL) {
                        loop_state_var = 2U;
                        break;
                    }
                    *var_12 = 0UL;
                    loop_state_var = 1U;
                    break;
                }
            }
            *var_38 = (unsigned char)'\x01';
            var_51 = (uint64_t)*var_10;
            var_52 = local_sp_8 + (-8L);
            *(uint64_t *)var_52 = 4246882UL;
            var_53 = indirect_placeholder_141(0UL, rcx_0, 0UL, 4309047UL, var_51, r9_0, r8_0);
            var_54 = var_53.field_0;
            var_55 = var_53.field_2;
            var_56 = var_53.field_3;
            var_57 = (uint32_t)var_54;
            *var_10 = var_57;
            r9_0 = var_55;
            r8_0 = var_56;
            local_sp_10 = var_52;
            rax_7 = var_54;
            if ((int)var_57 >= (int)0U) {
                loop_state_var = 1U;
                break;
            }
            *var_11 = (unsigned char)'\x01';
            var_58 = *var_10;
            var_59 = local_sp_8 + (-16L);
            *(uint64_t *)var_59 = 4246919UL;
            indirect_placeholder();
            *var_39 = var_58;
            local_sp_7 = var_59;
            local_sp_10 = var_59;
            if (var_58 != 0U) {
                rax_7 = (uint64_t)var_58;
                loop_state_var = 1U;
                break;
            }
            var_60 = *var_12;
            rax_7 = var_60;
            if (var_60 != 0UL) {
                var_61 = local_sp_8 + (-24L);
                *(uint64_t *)var_61 = 4246951UL;
                indirect_placeholder();
                local_sp_7 = var_61;
                local_sp_10 = var_61;
                if ((uint64_t)(uint32_t)var_60 != 0UL) {
                    *var_12 = 0UL;
                    loop_state_var = 1U;
                    break;
                }
            }
            *var_40 = *var_26;
            *var_41 = *var_29;
            *var_42 = (*var_40 != *var_28);
            var_62 = (uint64_t)*var_10;
            var_63 = local_sp_7 + (-8L);
            *(uint64_t *)var_63 = 4247017UL;
            var_64 = indirect_placeholder_4(var_62);
            *var_12 = var_64;
            local_sp_6 = var_63;
            rax_6 = var_64;
            local_sp_10 = var_63;
            rax_7 = 0UL;
            if (var_64 != 0UL) {
                loop_state_var = 1U;
                break;
            }
            *var_11 = (unsigned char)'\x00';
            while (1U)
                {
                    *(uint64_t *)(local_sp_6 + (-8L)) = 4247041UL;
                    indirect_placeholder();
                    *(uint32_t *)rax_6 = 0U;
                    var_65 = *var_12;
                    var_66 = local_sp_6 + (-16L);
                    *(uint64_t *)var_66 = 4247059UL;
                    indirect_placeholder();
                    *var_43 = var_65;
                    local_sp_5 = var_66;
                    rax_4 = var_65;
                    var_67 = local_sp_6 + (-24L);
                    *(uint64_t *)var_67 = 4247075UL;
                    indirect_placeholder();
                    var_68 = *(volatile uint32_t *)(uint32_t *)0UL;
                    var_69 = (uint64_t)var_68;
                    local_sp_5 = var_67;
                    rax_4 = var_69;
                    if (var_65 != 0UL & var_68 != 0U & *var_38 == '\x00') {
                        *var_38 = (unsigned char)'\x00';
                        *(uint64_t *)(local_sp_6 + (-32L)) = 4247103UL;
                        indirect_placeholder();
                        var_70 = *var_12;
                        var_71 = local_sp_6 + (-40L);
                        *(uint64_t *)var_71 = 4247115UL;
                        indirect_placeholder();
                        *var_43 = var_70;
                        local_sp_5 = var_71;
                        rax_4 = var_70;
                    }
                    var_72 = *var_43;
                    local_sp_6_be = local_sp_5;
                    if (var_72 != 0UL) {
                        var_103 = local_sp_5 + (-8L);
                        *(uint64_t *)var_103 = 4247131UL;
                        indirect_placeholder();
                        var_104 = *(uint32_t *)rax_4;
                        var_105 = (uint64_t)var_104;
                        local_sp_10 = var_103;
                        rax_7 = var_105;
                        if (var_104 != 0U) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_106 = local_sp_5 + (-16L);
                        *(uint64_t *)var_106 = 4247146UL;
                        indirect_placeholder();
                        *(uint32_t *)var_105 = 2U;
                        local_sp_10 = var_106;
                        loop_state_var = 1U;
                        break;
                    }
                    var_73 = var_72 + 19UL;
                    _pre_phi224 = var_73;
                    if (*(unsigned char *)var_73 == '.') {
                        if (*var_38 == '\x00') {
                            *var_44 = (unsigned char)'\x01';
                            _pre_phi224 = *var_43 + 19UL;
                        }
                        var_74 = (uint64_t)*var_10;
                        var_75 = local_sp_5 + (-8L);
                        *(uint64_t *)var_75 = 4247270UL;
                        var_76 = indirect_placeholder_35(256UL, var_25, _pre_phi224, var_74);
                        var_77 = (uint32_t)var_76;
                        *var_45 = var_77;
                        local_sp_6_be = var_75;
                        rax_6_be = var_76;
                        if (var_77 != 0U) {
                            var_78 = (uint32_t)((uint16_t)*var_46 & (unsigned short)61440U);
                            var_79 = (uint64_t)var_78;
                            rax_6_be = var_79;
                            var_80 = *var_26;
                            rax_6_be = var_80;
                            var_81 = *var_29;
                            rax_6_be = var_81;
                            if ((uint64_t)((var_78 + (-16384)) & (-4096)) != 0UL & *var_28 != var_80 & *var_31 != var_81) {
                                loop_state_var = 0U;
                                break;
                            }
                        }
                        local_sp_6 = local_sp_6_be;
                        rax_6 = rax_6_be;
                        continue;
                    }
                    switch_state_var = 0;
                    switch (*(unsigned char *)(var_72 + 20UL)) {
                      case '\x00':
                        {
                        }
                        break;
                      case '.':
                        {
                            if (*(unsigned char *)(var_72 + 21UL) != '\x00') {
                                local_sp_6 = local_sp_6_be;
                                rax_6 = rax_6_be;
                                continue;
                            }
                            if (*var_38 == '\x00') {
                                *var_44 = (unsigned char)'\x01';
                                _pre_phi224 = *var_43 + 19UL;
                            }
                            var_74 = (uint64_t)*var_10;
                            var_75 = local_sp_5 + (-8L);
                            *(uint64_t *)var_75 = 4247270UL;
                            var_76 = indirect_placeholder_35(256UL, var_25, _pre_phi224, var_74);
                            var_77 = (uint32_t)var_76;
                            *var_45 = var_77;
                            local_sp_6_be = var_75;
                            rax_6_be = var_76;
                            var_78 = (uint32_t)((uint16_t)*var_46 & (unsigned short)61440U);
                            var_79 = (uint64_t)var_78;
                            rax_6_be = var_79;
                            var_80 = *var_26;
                            rax_6_be = var_80;
                            var_81 = *var_29;
                            rax_6_be = var_81;
                            if (var_77 != 0U & (uint64_t)((var_78 + (-16384)) & (-4096)) != 0UL & *var_28 != var_80 & *var_31 != var_81) {
                                loop_state_var = 0U;
                                switch_state_var = 1;
                                break;
                            }
                        }
                        break;
                      default:
                        {
                            if (*var_38 == '\x00') {
                                *var_44 = (unsigned char)'\x01';
                                _pre_phi224 = *var_43 + 19UL;
                            }
                            var_74 = (uint64_t)*var_10;
                            var_75 = local_sp_5 + (-8L);
                            *(uint64_t *)var_75 = 4247270UL;
                            var_76 = indirect_placeholder_35(256UL, var_25, _pre_phi224, var_74);
                            var_77 = (uint32_t)var_76;
                            *var_45 = var_77;
                            local_sp_6_be = var_75;
                            rax_6_be = var_76;
                            if (var_77 != 0U) {
                                local_sp_6 = local_sp_6_be;
                                rax_6 = rax_6_be;
                                continue;
                            }
                            var_78 = (uint32_t)((uint16_t)*var_46 & (unsigned short)61440U);
                            var_79 = (uint64_t)var_78;
                            rax_6_be = var_79;
                            if ((uint64_t)((var_78 + (-16384)) & (-4096)) != 0UL) {
                                local_sp_6 = local_sp_6_be;
                                rax_6 = rax_6_be;
                                continue;
                            }
                            var_80 = *var_26;
                            rax_6_be = var_80;
                            if (*var_28 != var_80) {
                                var_81 = *var_29;
                                rax_6_be = var_81;
                                if (*var_31 == var_81) {
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                            }
                            local_sp_6 = local_sp_6_be;
                            rax_6 = rax_6_be;
                            continue;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    *var_47 = (rbx_1 - *_pre_phi222);
                    var_82 = *var_43 + 19UL;
                    var_83 = local_sp_5 + (-16L);
                    *(uint64_t *)var_83 = 4247389UL;
                    indirect_placeholder();
                    *var_48 = var_82;
                    var_84 = *var_47;
                    local_sp_3 = var_83;
                    var_99 = var_82;
                    local_sp_4 = var_83;
                    rax_7 = var_84;
                    if (var_84 <= var_82) {
                        if (*var_9 != 0UL) {
                            var_85 = local_sp_5 + (-24L);
                            *(uint64_t *)var_85 = 4247431UL;
                            indirect_placeholder();
                            *(uint32_t *)var_84 = 34U;
                            local_sp_10 = var_85;
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        *var_49 = *var_14;
                        var_86 = *var_48;
                        var_87 = *var_14;
                        var_88 = helper_cc_compute_c_wrapper(var_87 - var_86, var_86, var_1, 17U);
                        var_89 = *var_14 + ((var_88 == 0UL) ? var_87 : var_86);
                        *var_14 = var_89;
                        var_90 = *var_49;
                        var_91 = helper_cc_compute_c_wrapper(var_89 - var_90, var_90, var_1, 17U);
                        rax_3 = var_89;
                        if (var_91 != 0UL) {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        var_92 = *var_14;
                        var_93 = *_pre_phi222;
                        var_94 = local_sp_5 + (-24L);
                        *(uint64_t *)var_94 = 4247509UL;
                        var_95 = indirect_placeholder_3(var_92, var_93);
                        *var_50 = var_95;
                        local_sp_3 = var_94;
                        rax_3 = 0UL;
                        if (var_95 != 0UL) {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        var_96 = *var_49 - *var_47;
                        var_97 = local_sp_5 + (-32L);
                        *(uint64_t *)var_97 = 4247607UL;
                        indirect_placeholder();
                        *_pre_phi222 = *var_50;
                        var_99 = *var_48;
                        local_sp_4 = var_97;
                        rbx_0 = var_96;
                    }
                    var_100 = *var_43 + 19UL;
                    var_101 = local_sp_4 + (-8L);
                    *(uint64_t *)var_101 = 4247663UL;
                    indirect_placeholder();
                    var_102 = rbx_0 + (var_99 ^ (-1L));
                    *(unsigned char *)var_102 = (unsigned char)'/';
                    *var_28 = *var_40;
                    *var_31 = *var_41;
                    local_sp_8 = var_101;
                    rcx_0 = var_100;
                    rbx_1 = var_102;
                    continue;
                }
                break;
              case 1U:
                {
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 1U:
      case 0U:
        {
            var_98 = local_sp_3 + (-8L);
            *(uint64_t *)var_98 = 4247886UL;
            indirect_placeholder();
            *(uint32_t *)rax_3 = 12U;
            local_sp_10 = var_98;
            rax_7 = rax_3;
        }
        break;
      case 2U:
        {
            var_109 = *var_14;
            var_110 = var_109 + (-1L);
            var_111 = *_pre_phi222;
            var_113 = var_109;
            var_114 = var_111;
            if (rbx_1 == (var_111 + var_110)) {
                var_112 = rbx_1 + (-1L);
                *(unsigned char *)var_112 = (unsigned char)'/';
                var_113 = *var_14;
                var_114 = *_pre_phi222;
                rbx_2 = var_112;
            }
            var_115 = (var_113 + var_114) - rbx_2;
            var_116 = (uint64_t *)(var_0 + (-112L));
            *var_116 = var_115;
            *(uint64_t *)(local_sp_9 + (-8L)) = 4247814UL;
            indirect_placeholder();
            if (*var_9 == 0UL) {
                var_117 = *var_116;
                var_118 = *_pre_phi222;
                *(uint64_t *)(local_sp_9 + (-16L)) = 4247843UL;
                var_119 = indirect_placeholder_3(var_117, var_118);
                *var_8 = var_119;
                var_120 = var_119;
            } else {
                var_120 = *var_8;
            }
            var_122 = var_120;
            if (var_120 == 0UL) {
                var_121 = *_pre_phi222;
                *var_8 = var_121;
                var_122 = var_121;
            }
            rax_8 = var_122;
            return rax_8;
        }
        break;
    }
}
