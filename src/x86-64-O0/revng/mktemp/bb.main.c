typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_15_ret_type;
struct indirect_placeholder_16_ret_type;
struct indirect_placeholder_17_ret_type;
struct indirect_placeholder_18_ret_type;
struct indirect_placeholder_19_ret_type;
struct indirect_placeholder_20_ret_type;
struct indirect_placeholder_22_ret_type;
struct indirect_placeholder_23_ret_type;
struct indirect_placeholder_25_ret_type;
struct indirect_placeholder_15_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_16_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_17_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_18_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_19_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_20_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_22_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_23_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_25_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_8(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_21(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0);
extern void indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_14(uint64_t param_0);
extern void indirect_placeholder_11(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_5(void);
extern uint64_t indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint32_t init_state_0x82fc(void);
extern struct indirect_placeholder_15_ret_type indirect_placeholder_15(uint64_t param_0);
extern struct indirect_placeholder_16_ret_type indirect_placeholder_16(uint64_t param_0);
extern struct indirect_placeholder_17_ret_type indirect_placeholder_17(uint64_t param_0);
extern struct indirect_placeholder_18_ret_type indirect_placeholder_18(uint64_t param_0);
extern struct indirect_placeholder_19_ret_type indirect_placeholder_19(uint64_t param_0);
extern struct indirect_placeholder_20_ret_type indirect_placeholder_20(uint64_t param_0);
extern struct indirect_placeholder_22_ret_type indirect_placeholder_22(uint64_t param_0);
extern struct indirect_placeholder_23_ret_type indirect_placeholder_23(uint64_t param_0);
extern void indirect_placeholder_24(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_25_ret_type indirect_placeholder_25(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_25_ret_type var_23;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint32_t var_3;
    uint64_t var_4;
    uint32_t *var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t var_8;
    uint64_t *var_9;
    unsigned char *var_10;
    uint64_t *var_11;
    unsigned char *var_12;
    unsigned char *var_13;
    unsigned char *var_14;
    unsigned char *var_15;
    uint32_t *var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint32_t *var_19;
    uint64_t local_sp_13;
    uint64_t var_150;
    uint64_t var_151;
    uint64_t r8_2;
    uint64_t local_sp_3;
    uint32_t var_146;
    uint32_t *var_147;
    uint64_t var_116;
    uint64_t var_148;
    struct indirect_placeholder_15_ret_type var_149;
    uint64_t var_145;
    uint64_t var_137;
    uint64_t local_sp_2;
    uint64_t local_sp_0;
    uint64_t r8_6;
    uint64_t _pre_phi;
    uint64_t var_138;
    struct indirect_placeholder_16_ret_type var_139;
    uint64_t var_140;
    uint64_t var_141;
    uint64_t var_142;
    uint64_t var_143;
    uint64_t var_144;
    uint64_t r9_0;
    uint64_t local_sp_8;
    uint64_t r8_0;
    uint64_t local_sp_1;
    uint64_t r9_1;
    uint64_t var_127;
    struct indirect_placeholder_17_ret_type var_128;
    uint64_t var_129;
    uint64_t var_130;
    uint64_t var_131;
    uint64_t var_132;
    uint64_t var_133;
    uint64_t r8_1;
    uint64_t r9_8;
    uint64_t rax_0;
    uint64_t r9_3;
    uint64_t r9_2;
    uint64_t var_124;
    uint32_t *var_125;
    uint32_t var_126;
    uint64_t var_134;
    uint32_t *var_135;
    uint32_t var_136;
    uint64_t rax_2;
    uint64_t storemerge;
    uint64_t local_sp_6;
    struct indirect_placeholder_18_ret_type var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t var_98;
    uint64_t var_80;
    uint64_t rax_1;
    uint64_t r8_8;
    uint64_t var_89;
    uint64_t r9_5;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t var_82;
    struct indirect_placeholder_19_ret_type var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t local_sp_10;
    uint64_t r8_3;
    uint64_t local_sp_4;
    uint64_t *_pre_phi346;
    uint64_t var_88;
    uint64_t r9_4;
    uint64_t r8_4;
    uint64_t local_sp_5;
    uint64_t var_99;
    unsigned char var_100;
    uint64_t var_101;
    uint64_t var_104;
    struct indirect_placeholder_20_ret_type var_105;
    uint64_t var_106;
    uint64_t var_107;
    uint64_t var_108;
    uint64_t var_109;
    uint64_t r8_5;
    uint64_t local_sp_7;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t var_112;
    uint64_t *var_113;
    uint64_t var_114;
    uint64_t var_115;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t r9_6;
    uint64_t var_117;
    bool var_118;
    uint64_t var_119;
    uint64_t var_120;
    uint64_t r8_9;
    uint64_t var_121;
    uint64_t var_122;
    uint64_t *var_123;
    struct indirect_placeholder_22_ret_type var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t rcx_0;
    uint64_t rdi1_0;
    uint64_t rcx_1;
    unsigned char var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_58;
    uint64_t var_65;
    uint64_t *var_66;
    uint64_t r9_9;
    uint64_t *_pre_phi342;
    uint64_t r9_7;
    uint64_t local_sp_11;
    uint64_t r8_7;
    uint64_t local_sp_9;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t *var_81;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_51;
    uint64_t *var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t *var_55;
    uint64_t var_56;
    uint64_t var_45;
    struct indirect_placeholder_23_ret_type var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_29;
    uint64_t var_30;
    uint32_t var_31;
    uint64_t var_152;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t *var_37;
    uint64_t *_pre_phi340;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t *var_34;
    bool var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t *var_41;
    uint64_t *var_42;
    uint64_t _pre;
    uint64_t var_57;
    uint64_t local_sp_12;
    uint32_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t *var_28;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint32_t var_24;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = init_state_0x82fc();
    var_4 = var_0 + (-8L);
    *(uint64_t *)var_4 = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_5 = (uint32_t *)(var_0 + (-156L));
    *var_5 = (uint32_t)rdi;
    var_6 = var_0 + (-168L);
    var_7 = (uint64_t *)var_6;
    *var_7 = rsi;
    var_8 = var_0 + (-40L);
    var_9 = (uint64_t *)var_8;
    *var_9 = 0UL;
    var_10 = (unsigned char *)(var_0 + (-41L));
    *var_10 = (unsigned char)'\x00';
    var_11 = (uint64_t *)(var_0 + (-64L));
    *var_11 = 0UL;
    var_12 = (unsigned char *)(var_0 + (-65L));
    *var_12 = (unsigned char)'\x00';
    var_13 = (unsigned char *)(var_0 + (-66L));
    *var_13 = (unsigned char)'\x00';
    var_14 = (unsigned char *)(var_0 + (-67L));
    *var_14 = (unsigned char)'\x00';
    var_15 = (unsigned char *)(var_0 + (-68L));
    *var_15 = (unsigned char)'\x00';
    var_16 = (uint32_t *)(var_0 + (-72L));
    *var_16 = 0U;
    var_17 = **(uint64_t **)var_6;
    *(uint64_t *)(var_0 + (-176L)) = 4205393UL;
    indirect_placeholder_14(var_17);
    *(uint64_t *)(var_0 + (-184L)) = 4205408UL;
    indirect_placeholder();
    var_18 = var_0 + (-192L);
    *(uint64_t *)var_18 = 4205418UL;
    indirect_placeholder();
    var_19 = (uint32_t *)(var_0 + (-84L));
    storemerge = 4282658UL;
    rcx_0 = 18446744073709551615UL;
    rcx_1 = 0UL;
    local_sp_13 = var_18;
    while (1U)
        {
            var_20 = *var_7;
            var_21 = (uint64_t)*var_5;
            var_22 = local_sp_13 + (-8L);
            *(uint64_t *)var_22 = 4205681UL;
            var_23 = indirect_placeholder_25(4280768UL, 4282501UL, var_21, var_20, 0UL);
            var_24 = (uint32_t)var_23.field_0;
            *var_19 = var_24;
            local_sp_12 = var_22;
            local_sp_13 = var_22;
            if (var_24 != 4294967295U) {
                var_29 = var_23.field_2;
                var_30 = var_23.field_3;
                var_31 = *var_5 - *(uint32_t *)4301656UL;
                *(uint32_t *)(var_0 + (-88L)) = var_31;
                r9_7 = var_29;
                r8_7 = var_30;
                r9_9 = var_29;
                r8_9 = var_30;
                if (var_31 > 1U) {
                    var_152 = var_23.field_1;
                    *(uint64_t *)(local_sp_13 + (-16L)) = 4205744UL;
                    indirect_placeholder_13(0UL, var_152, 4282509UL, 0UL, 0UL, var_29, var_30);
                    *(uint64_t *)(local_sp_13 + (-24L)) = 4205754UL;
                    indirect_placeholder_11(var_4, 1UL);
                    abort();
                }
                if (var_31 != 0U) {
                    *var_12 = (unsigned char)'\x01';
                    var_35 = *(uint64_t *)4301504UL;
                    var_36 = var_0 + (-56L);
                    var_37 = (uint64_t *)var_36;
                    *var_37 = var_35;
                    _pre_phi340 = var_37;
                    _pre_phi = var_36;
                    loop_state_var = 2U;
                    break;
                }
                var_32 = *(uint64_t *)(*var_7 + ((uint64_t)*(uint32_t *)4301656UL << 3UL));
                var_33 = var_0 + (-56L);
                var_34 = (uint64_t *)var_33;
                *var_34 = var_32;
                _pre_phi340 = var_34;
                _pre_phi = var_33;
                loop_state_var = 2U;
                break;
            }
            if ((int)var_24 <= (int)128U) {
                loop_state_var = 0U;
                break;
            }
            if ((int)var_24 >= (int)86U) {
                switch_state_var = 0;
                switch (var_24) {
                  case 4294967166U:
                    {
                        *(uint64_t *)(local_sp_13 + (-16L)) = 4205563UL;
                        indirect_placeholder_11(var_4, 0UL);
                        abort();
                    }
                    break;
                  case 4294967165U:
                    {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  default:
                    {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
            var_25 = var_24 + (-86);
            if (var_25 <= 42U) {
                loop_state_var = 0U;
                break;
            }
            switch_state_var = 0;
            switch (*(uint64_t *)(((uint64_t)var_25 << 3UL) + 4282888UL)) {
              case 4205635UL:
                {
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 4205563UL:
                {
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 4205540UL:
              case 4205534UL:
              case 4205524UL:
              case 4205518UL:
              case 4205501UL:
              case 4205492UL:
                {
                    switch (*(uint64_t *)(((uint64_t)var_25 << 3UL) + 4282888UL)) {
                      case 4205524UL:
                        {
                            *var_12 = (unsigned char)'\x01';
                            *var_13 = (unsigned char)'\x01';
                        }
                        break;
                      case 4205540UL:
                        {
                            *var_11 = *(uint64_t *)4302328UL;
                        }
                        break;
                      case 4205534UL:
                        {
                            *var_15 = (unsigned char)'\x01';
                        }
                        break;
                      case 4205501UL:
                        {
                            *var_9 = *(uint64_t *)4302328UL;
                            *var_12 = (unsigned char)'\x01';
                        }
                        break;
                      case 4205518UL:
                        {
                            *var_10 = (unsigned char)'\x01';
                        }
                        break;
                      case 4205492UL:
                        {
                            *var_14 = (unsigned char)'\x01';
                        }
                        break;
                    }
                    continue;
                }
                break;
              default:
                {
                    abort();
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 2U:
        {
            var_38 = (*var_11 == 0UL);
            var_39 = *_pre_phi340;
            var_40 = local_sp_13 + (-16L);
            var_41 = (uint64_t *)var_40;
            local_sp_11 = var_40;
            if (var_38) {
                *var_41 = 4206043UL;
                var_57 = indirect_placeholder_3(var_39);
                *_pre_phi340 = var_57;
                *(uint64_t *)(local_sp_13 + (-24L)) = 4206064UL;
                indirect_placeholder();
                *var_11 = var_57;
                if (var_57 == 0UL) {
                    var_58 = var_57 + 1UL;
                    *var_11 = var_58;
                    var_64 = var_58;
                } else {
                    var_59 = *_pre_phi340;
                    var_60 = (uint64_t)var_3;
                    rdi1_0 = var_59;
                    while (rcx_0 != 0UL)
                        {
                            var_61 = *(unsigned char *)rdi1_0;
                            var_62 = rcx_0 + (-1L);
                            rcx_0 = var_62;
                            rcx_1 = var_62;
                            if (var_61 == '\x00') {
                                break;
                            }
                            rdi1_0 = rdi1_0 + var_60;
                        }
                    var_63 = var_59 + (18446744073709551614UL - (-2L));
                    *var_11 = var_63;
                    var_64 = var_63;
                }
                var_65 = local_sp_13 + (-32L);
                *(uint64_t *)var_65 = 4206139UL;
                indirect_placeholder();
                var_66 = (uint64_t *)(var_0 + (-80L));
                *var_66 = var_64;
                _pre_phi342 = var_66;
                local_sp_9 = var_65;
            } else {
                *var_41 = 4205833UL;
                indirect_placeholder();
                var_42 = (uint64_t *)(var_0 + (-96L));
                *var_42 = var_39;
                if (var_39 == 0UL) {
                    _pre = *_pre_phi340;
                    var_45 = _pre;
                    *(uint64_t *)(local_sp_13 + (-24L)) = 4205878UL;
                    var_46 = indirect_placeholder_23(var_45);
                    var_47 = var_46.field_0;
                    var_48 = var_46.field_1;
                    var_49 = var_46.field_2;
                    var_50 = local_sp_13 + (-32L);
                    *(uint64_t *)var_50 = 4205906UL;
                    indirect_placeholder_13(0UL, var_47, 4282528UL, 1UL, 0UL, var_48, var_49);
                    r9_9 = var_48;
                    r8_9 = var_49;
                    local_sp_11 = var_50;
                } else {
                    var_43 = var_39 + (-1L);
                    var_44 = *_pre_phi340;
                    var_45 = var_44;
                    if (*(unsigned char *)(var_44 + var_43) == 'X') {
                        *(uint64_t *)(local_sp_13 + (-24L)) = 4205878UL;
                        var_46 = indirect_placeholder_23(var_45);
                        var_47 = var_46.field_0;
                        var_48 = var_46.field_1;
                        var_49 = var_46.field_2;
                        var_50 = local_sp_13 + (-32L);
                        *(uint64_t *)var_50 = 4205906UL;
                        indirect_placeholder_13(0UL, var_47, 4282528UL, 1UL, 0UL, var_48, var_49);
                        r9_9 = var_48;
                        r8_9 = var_49;
                        local_sp_11 = var_50;
                    }
                }
                var_51 = *var_11;
                *(uint64_t *)(local_sp_11 + (-8L)) = 4205918UL;
                indirect_placeholder();
                var_52 = (uint64_t *)(var_0 + (-80L));
                *var_52 = var_51;
                var_53 = (var_51 + *var_42) + 1UL;
                *(uint64_t *)(local_sp_11 + (-16L)) = 4205945UL;
                var_54 = indirect_placeholder_3(var_53);
                var_55 = (uint64_t *)(var_0 + (-104L));
                *var_55 = var_54;
                *(uint64_t *)(local_sp_11 + (-24L)) = 4205972UL;
                indirect_placeholder();
                var_56 = local_sp_11 + (-32L);
                *(uint64_t *)var_56 = 4206006UL;
                indirect_placeholder();
                *_pre_phi340 = *var_55;
                *var_11 = (*var_42 + *var_55);
                _pre_phi342 = var_52;
                r9_7 = r9_9;
                r8_7 = r8_9;
                local_sp_9 = var_56;
            }
            r9_8 = r9_7;
            r8_8 = r8_7;
            local_sp_10 = local_sp_9;
            var_67 = *var_11;
            var_68 = local_sp_9 + (-8L);
            *(uint64_t *)var_68 = 4206162UL;
            var_69 = indirect_placeholder_3(var_67);
            var_70 = *var_11;
            local_sp_10 = var_68;
            if (*_pre_phi342 != 0UL & var_70 == var_69) {
                *(uint64_t *)(local_sp_9 + (-16L)) = 4206180UL;
                var_71 = indirect_placeholder_22(var_70);
                var_72 = var_71.field_0;
                var_73 = var_71.field_1;
                var_74 = var_71.field_2;
                var_75 = local_sp_9 + (-24L);
                *(uint64_t *)var_75 = 4206208UL;
                indirect_placeholder_13(0UL, var_72, 4282576UL, 1UL, 0UL, var_73, var_74);
                r9_8 = var_73;
                r8_8 = var_74;
                local_sp_10 = var_75;
            }
            var_76 = *var_11;
            var_77 = *_pre_phi340;
            var_78 = var_76 - var_77;
            var_79 = local_sp_10 + (-8L);
            *(uint64_t *)var_79 = 4206234UL;
            var_80 = indirect_placeholder_8(var_77, var_78);
            var_81 = (uint64_t *)(var_0 + (-112L));
            *var_81 = var_80;
            rax_0 = var_80;
            r9_3 = r9_8;
            r8_3 = r8_8;
            local_sp_4 = var_79;
            if (var_80 > 2UL) {
                var_82 = *_pre_phi340;
                *(uint64_t *)(local_sp_10 + (-16L)) = 4206257UL;
                var_83 = indirect_placeholder_19(var_82);
                var_84 = var_83.field_0;
                var_85 = var_83.field_1;
                var_86 = var_83.field_2;
                var_87 = local_sp_10 + (-24L);
                *(uint64_t *)var_87 = 4206285UL;
                indirect_placeholder_13(0UL, var_84, 4282624UL, 1UL, 0UL, var_85, var_86);
                rax_0 = 0UL;
                r9_3 = var_85;
                r8_3 = var_86;
                local_sp_4 = var_87;
            }
            r8_6 = r8_3;
            local_sp_8 = local_sp_4;
            rax_1 = rax_0;
            r9_5 = r9_3;
            r9_4 = r9_3;
            r8_4 = r8_3;
            local_sp_5 = local_sp_4;
            r8_5 = r8_3;
            r9_6 = r9_3;
            if (*var_12 == '\x00') {
                _pre_phi346 = (uint64_t *)(var_0 + (-104L));
                var_116 = *_pre_phi340;
            } else {
                if (*var_13 == '\x00') {
                    *(uint64_t *)(local_sp_4 + (-8L)) = 4206315UL;
                    indirect_placeholder();
                    var_88 = var_0 + (-128L);
                    *(uint64_t *)var_88 = rax_0;
                    rax_1 = 0UL;
                    if (rax_0 == 0UL) {
                        if (**(unsigned char **)var_88 == '\x00') {
                            *(uint64_t *)(var_0 + (-32L)) = rax_0;
                        } else {
                            var_89 = *var_9;
                            if (var_89 == 0UL) {
                                *(uint64_t *)(var_0 + (-32L)) = 4282658UL;
                            } else {
                                if (**(unsigned char **)var_8 == '\x00') {
                                    *(uint64_t *)(var_0 + (-32L)) = 4282658UL;
                                } else {
                                    *(uint64_t *)(var_0 + (-32L)) = var_89;
                                }
                            }
                        }
                    } else {
                        var_89 = *var_9;
                        if (var_89 == 0UL) {
                            *(uint64_t *)(var_0 + (-32L)) = 4282658UL;
                        } else {
                            if (**(unsigned char **)var_8 == '\x00') {
                                *(uint64_t *)(var_0 + (-32L)) = var_89;
                            } else {
                                *(uint64_t *)(var_0 + (-32L)) = 4282658UL;
                            }
                        }
                    }
                    var_90 = *_pre_phi340;
                    var_91 = local_sp_4 + (-16L);
                    *(uint64_t *)var_91 = 4206395UL;
                    var_92 = indirect_placeholder_3(var_90);
                    var_93 = *_pre_phi340;
                    local_sp_7 = var_91;
                    if (var_93 != var_92) {
                        *(uint64_t *)(local_sp_4 + (-24L)) = 4206417UL;
                        var_94 = indirect_placeholder_18(var_93);
                        var_95 = var_94.field_0;
                        var_96 = var_94.field_1;
                        var_97 = var_94.field_2;
                        var_98 = local_sp_4 + (-32L);
                        *(uint64_t *)var_98 = 4206445UL;
                        indirect_placeholder_13(0UL, var_95, 4282664UL, 1UL, 0UL, var_96, var_97);
                        r9_4 = var_96;
                        r8_4 = var_97;
                        local_sp_5 = var_98;
                        var_99 = *var_9;
                        local_sp_6 = local_sp_5;
                        r9_5 = r9_4;
                        r8_5 = r8_4;
                        rax_2 = rax_1;
                        if (var_99 == 0UL) {
                            var_100 = **(unsigned char **)var_8;
                            var_101 = (uint64_t)var_100;
                            rax_2 = var_101;
                            if (var_100 == '\x00') {
                                *(uint64_t *)(var_0 + (-32L)) = var_99;
                            } else {
                                var_102 = local_sp_5 + (-8L);
                                *(uint64_t *)var_102 = 4206483UL;
                                indirect_placeholder();
                                var_103 = var_0 + (-120L);
                                *(uint64_t *)var_103 = rax_2;
                                local_sp_6 = var_102;
                                if (rax_2 != 0UL) {
                                    storemerge = (**(unsigned char **)var_103 == '\x00') ? 4282658UL : rax_2;
                                }
                                *(uint64_t *)(var_0 + (-32L)) = storemerge;
                            }
                        } else {
                            var_102 = local_sp_5 + (-8L);
                            *(uint64_t *)var_102 = 4206483UL;
                            indirect_placeholder();
                            var_103 = var_0 + (-120L);
                            *(uint64_t *)var_103 = rax_2;
                            local_sp_6 = var_102;
                            if (rax_2 == 0UL) {
                                storemerge = (**(unsigned char **)var_103 == '\x00') ? 4282658UL : rax_2;
                            }
                            *(uint64_t *)(var_0 + (-32L)) = storemerge;
                        }
                        local_sp_7 = local_sp_6;
                        if (**(unsigned char **)_pre_phi == '/') {
                            var_104 = *_pre_phi340;
                            *(uint64_t *)(local_sp_6 + (-8L)) = 4206543UL;
                            var_105 = indirect_placeholder_20(var_104);
                            var_106 = var_105.field_0;
                            var_107 = var_105.field_1;
                            var_108 = var_105.field_2;
                            var_109 = local_sp_6 + (-16L);
                            *(uint64_t *)var_109 = 4206571UL;
                            indirect_placeholder_13(0UL, var_106, 4282720UL, 1UL, 0UL, var_107, var_108);
                            r9_5 = var_107;
                            r8_5 = var_108;
                            local_sp_7 = var_109;
                        }
                    }
                } else {
                    var_99 = *var_9;
                    local_sp_6 = local_sp_5;
                    r9_5 = r9_4;
                    r8_5 = r8_4;
                    rax_2 = rax_1;
                    if (var_99 == 0UL) {
                        var_100 = **(unsigned char **)var_8;
                        var_101 = (uint64_t)var_100;
                        rax_2 = var_101;
                        if (var_100 == '\x00') {
                            *(uint64_t *)(var_0 + (-32L)) = var_99;
                        } else {
                            var_102 = local_sp_5 + (-8L);
                            *(uint64_t *)var_102 = 4206483UL;
                            indirect_placeholder();
                            var_103 = var_0 + (-120L);
                            *(uint64_t *)var_103 = rax_2;
                            local_sp_6 = var_102;
                            if (rax_2 == 0UL) {
                                storemerge = (**(unsigned char **)var_103 == '\x00') ? 4282658UL : rax_2;
                            }
                            *(uint64_t *)(var_0 + (-32L)) = storemerge;
                        }
                    } else {
                        var_102 = local_sp_5 + (-8L);
                        *(uint64_t *)var_102 = 4206483UL;
                        indirect_placeholder();
                        var_103 = var_0 + (-120L);
                        *(uint64_t *)var_103 = rax_2;
                        local_sp_6 = var_102;
                        if (rax_2 == 0UL) {
                            storemerge = (**(unsigned char **)var_103 == '\x00') ? 4282658UL : rax_2;
                        }
                        *(uint64_t *)(var_0 + (-32L)) = storemerge;
                    }
                    local_sp_7 = local_sp_6;
                    if (**(unsigned char **)_pre_phi == '/') {
                        var_104 = *_pre_phi340;
                        *(uint64_t *)(local_sp_6 + (-8L)) = 4206543UL;
                        var_105 = indirect_placeholder_20(var_104);
                        var_106 = var_105.field_0;
                        var_107 = var_105.field_1;
                        var_108 = var_105.field_2;
                        var_109 = local_sp_6 + (-16L);
                        *(uint64_t *)var_109 = 4206571UL;
                        indirect_placeholder_13(0UL, var_106, 4282720UL, 1UL, 0UL, var_107, var_108);
                        r9_5 = var_107;
                        r8_5 = var_108;
                        local_sp_7 = var_109;
                    }
                }
                var_110 = *_pre_phi340;
                var_111 = *(uint64_t *)(var_0 + (-32L));
                *(uint64_t *)(local_sp_7 + (-8L)) = 4206595UL;
                var_112 = indirect_placeholder_21(0UL, var_111, var_110);
                var_113 = (uint64_t *)(var_0 + (-104L));
                *var_113 = var_112;
                var_114 = local_sp_7 + (-16L);
                *(uint64_t *)var_114 = 4206611UL;
                indirect_placeholder();
                var_115 = *var_113;
                *_pre_phi340 = var_115;
                var_116 = var_115;
                r8_6 = r8_5;
                local_sp_8 = var_114;
                _pre_phi346 = var_113;
                r9_6 = r9_5;
            }
            *(uint64_t *)(local_sp_8 + (-8L)) = 4206631UL;
            var_117 = indirect_placeholder_3(var_116);
            *_pre_phi346 = var_117;
            var_118 = (*var_14 == '\x00');
            var_119 = (uint64_t)*var_15;
            var_120 = *var_81;
            var_121 = *_pre_phi342;
            var_122 = local_sp_8 + (-16L);
            var_123 = (uint64_t *)var_122;
            r8_2 = r8_6;
            local_sp_3 = var_122;
            local_sp_2 = var_122;
            local_sp_0 = var_122;
            r9_0 = r9_6;
            r8_0 = r8_6;
            r9_1 = r9_6;
            r8_1 = r8_6;
            r9_2 = r9_6;
            if (var_118) {
                *var_123 = 4206769UL;
                var_134 = indirect_placeholder_12(var_119, var_120, var_117, var_121);
                var_135 = (uint32_t *)(var_0 + (-132L));
                var_136 = (uint32_t)var_134;
                *var_135 = var_136;
                if ((int)var_136 < (int)0U) {
                    var_137 = local_sp_8 + (-24L);
                    *(uint64_t *)var_137 = 4206799UL;
                    indirect_placeholder();
                    local_sp_0 = var_137;
                    local_sp_3 = var_137;
                    if (*var_15 != '\x01' & var_136 != 0U) {
                        local_sp_1 = local_sp_0;
                        if (*var_10 != '\x01') {
                            var_138 = *_pre_phi340;
                            *(uint64_t *)(local_sp_0 + (-8L)) = 4206826UL;
                            var_139 = indirect_placeholder_16(var_138);
                            var_140 = var_139.field_0;
                            var_141 = var_139.field_1;
                            var_142 = var_139.field_2;
                            *(uint64_t *)(local_sp_0 + (-16L)) = 4206834UL;
                            indirect_placeholder();
                            var_143 = (uint64_t)*(uint32_t *)var_140;
                            var_144 = local_sp_0 + (-24L);
                            *(uint64_t *)var_144 = 4206861UL;
                            indirect_placeholder_13(0UL, var_140, 4282832UL, 0UL, var_143, var_141, var_142);
                            r9_0 = var_141;
                            r8_0 = var_142;
                            local_sp_1 = var_144;
                        }
                        *var_16 = 1U;
                        r9_2 = r9_0;
                        r8_2 = r8_0;
                        local_sp_3 = local_sp_1;
                    }
                } else {
                    local_sp_1 = local_sp_0;
                    if (*var_10 == '\x01') {
                        var_138 = *_pre_phi340;
                        *(uint64_t *)(local_sp_0 + (-8L)) = 4206826UL;
                        var_139 = indirect_placeholder_16(var_138);
                        var_140 = var_139.field_0;
                        var_141 = var_139.field_1;
                        var_142 = var_139.field_2;
                        *(uint64_t *)(local_sp_0 + (-16L)) = 4206834UL;
                        indirect_placeholder();
                        var_143 = (uint64_t)*(uint32_t *)var_140;
                        var_144 = local_sp_0 + (-24L);
                        *(uint64_t *)var_144 = 4206861UL;
                        indirect_placeholder_13(0UL, var_140, 4282832UL, 0UL, var_143, var_141, var_142);
                        r9_0 = var_141;
                        r8_0 = var_142;
                        local_sp_1 = var_144;
                    }
                    *var_16 = 1U;
                    r9_2 = r9_0;
                    r8_2 = r8_0;
                    local_sp_3 = local_sp_1;
                }
            } else {
                *var_123 = 4206665UL;
                var_124 = indirect_placeholder_12(var_119, var_120, var_117, var_121);
                var_125 = (uint32_t *)(var_0 + (-136L));
                var_126 = (uint32_t)var_124;
                *var_125 = var_126;
                if (var_126 != 0U) {
                    if (*var_10 != '\x01') {
                        var_127 = *_pre_phi340;
                        *(uint64_t *)(local_sp_8 + (-24L)) = 4206701UL;
                        var_128 = indirect_placeholder_17(var_127);
                        var_129 = var_128.field_0;
                        var_130 = var_128.field_1;
                        var_131 = var_128.field_2;
                        *(uint64_t *)(local_sp_8 + (-32L)) = 4206709UL;
                        indirect_placeholder();
                        var_132 = (uint64_t)*(uint32_t *)var_129;
                        var_133 = local_sp_8 + (-40L);
                        *(uint64_t *)var_133 = 4206736UL;
                        indirect_placeholder_13(0UL, var_129, 4282784UL, 0UL, var_132, var_130, var_131);
                        r9_1 = var_130;
                        r8_1 = var_131;
                        local_sp_2 = var_133;
                    }
                    *var_16 = 1U;
                    r9_2 = r9_1;
                    r8_2 = r8_1;
                    local_sp_3 = local_sp_2;
                }
            }
            *(uint64_t *)(local_sp_3 + (-8L)) = 4206886UL;
            indirect_placeholder();
            *(unsigned char *)4301808UL = (unsigned char)'\x01';
            *(uint64_t *)(local_sp_3 + (-16L)) = 4206919UL;
            var_145 = indirect_placeholder_5();
            if (*var_16 != 0U & *var_15 != '\x01' & (uint64_t)(uint32_t)var_145 != 0UL) {
                *(uint64_t *)(local_sp_3 + (-24L)) = 4206928UL;
                indirect_placeholder();
                var_146 = *(uint32_t *)var_145;
                var_147 = (uint32_t *)(var_0 + (-140L));
                *var_147 = var_146;
                var_148 = *_pre_phi346;
                *(uint64_t *)(local_sp_3 + (-32L)) = 4206948UL;
                var_149 = indirect_placeholder_15(var_148);
                if (*var_10 != '\x01') {
                    var_150 = var_149.field_1;
                    var_151 = (uint64_t)*var_147;
                    *(uint64_t *)(local_sp_3 + (-40L)) = 4206987UL;
                    indirect_placeholder_13(0UL, var_150, 4282870UL, 0UL, var_151, r9_2, r8_2);
                }
                *var_16 = 1U;
            }
            return;
        }
        break;
      case 1U:
      case 0U:
        {
            switch (loop_state_var) {
              case 0U:
                {
                    *(uint64_t *)(local_sp_12 + (-8L)) = 4205645UL;
                    indirect_placeholder_11(var_4, 1UL);
                    abort();
                }
                break;
              case 1U:
                {
                    var_26 = *(uint64_t *)4301512UL;
                    var_27 = local_sp_13 + (-24L);
                    var_28 = (uint64_t *)var_27;
                    *var_28 = 0UL;
                    *(uint64_t *)(local_sp_13 + (-32L)) = 4205621UL;
                    indirect_placeholder_24(0UL, var_26, 4280472UL, 4282470UL, 4282477UL, 4282488UL);
                    *var_28 = 4205635UL;
                    indirect_placeholder();
                    local_sp_12 = var_27;
                }
                break;
            }
        }
        break;
    }
}
