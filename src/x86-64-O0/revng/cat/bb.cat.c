typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_8_ret_type;
struct indirect_placeholder_8_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_4(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_6(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_8_ret_type indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2);
uint64_t bb_cat(uint64_t rcx, uint64_t rdx, uint64_t rsi, uint64_t rdi, uint64_t r9, uint64_t r8) {
    uint64_t rcx1_6;
    uint64_t var_68;
    uint64_t var_76;
    uint64_t var_49;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    uint64_t var_8;
    uint32_t var_9;
    uint32_t var_10;
    uint64_t var_11;
    uint32_t var_12;
    uint32_t var_13;
    unsigned char *var_14;
    unsigned char *var_15;
    unsigned char *var_16;
    unsigned char *var_17;
    unsigned char *var_18;
    unsigned char *var_19;
    uint32_t var_20;
    uint32_t *var_21;
    unsigned char *var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t *var_25;
    uint64_t var_26;
    uint64_t *var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t *var_30;
    uint64_t *var_31;
    uint64_t *var_32;
    unsigned char *var_33;
    unsigned char *var_34;
    uint32_t *var_35;
    uint64_t *var_36;
    uint64_t var_69;
    struct indirect_placeholder_8_ret_type var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t rax_0;
    uint64_t local_sp_2_be;
    uint64_t var_37;
    uint64_t local_sp_8;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_39;
    uint64_t var_45;
    uint64_t local_sp_1;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_50;
    uint64_t local_sp_4;
    uint64_t rcx1_0_be;
    uint64_t local_sp_2;
    uint64_t rcx1_0;
    uint64_t var_38;
    uint64_t var_40;
    uint64_t local_sp_3;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t rcx1_1;
    uint32_t var_59;
    uint64_t var_58;
    uint64_t local_sp_5;
    unsigned char var_60;
    uint64_t local_sp_6;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint32_t var_51;
    uint64_t local_sp_7;
    uint64_t var_108;
    uint64_t var_52;
    unsigned char var_53;
    unsigned char var_79;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t rcx1_2;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_77;
    unsigned char var_78;
    uint64_t local_sp_9;
    uint64_t rcx1_3;
    uint64_t local_sp_10;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t rcx1_5;
    unsigned char var_82;
    uint64_t rcx1_7;
    uint64_t rcx1_4;
    uint64_t var_87;
    bool var_88;
    uint64_t var_89;
    unsigned char *var_90;
    uint64_t var_100;
    uint64_t var_91;
    unsigned char var_92;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t var_83;
    unsigned char var_103;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_101;
    unsigned char var_102;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t var_107;
    unsigned char var_109;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_3 = var_0 + (-168L);
    var_4 = (uint64_t *)(var_0 + (-112L));
    *var_4 = rdi;
    var_5 = (uint64_t *)(var_0 + (-120L));
    *var_5 = rsi;
    var_6 = (uint64_t *)(var_0 + (-128L));
    *var_6 = rdx;
    var_7 = (uint64_t *)(var_0 + (-136L));
    *var_7 = rcx;
    var_8 = (uint64_t)(uint32_t)r9;
    var_9 = *(uint32_t *)(var_0 | 8UL);
    var_10 = *(uint32_t *)(var_0 | 16UL);
    var_11 = (uint64_t)var_10;
    var_12 = *(uint32_t *)(var_0 | 24UL);
    var_13 = *(uint32_t *)(var_0 | 32UL);
    var_14 = (unsigned char *)(var_0 + (-140L));
    *var_14 = (unsigned char)r8;
    var_15 = (unsigned char *)(var_0 + (-144L));
    *var_15 = (unsigned char)r9;
    var_16 = (unsigned char *)(var_0 + (-148L));
    *var_16 = (unsigned char)var_9;
    var_17 = (unsigned char *)(var_0 + (-152L));
    *var_17 = (unsigned char)var_10;
    var_18 = (unsigned char *)(var_0 + (-156L));
    *var_18 = (unsigned char)var_12;
    var_19 = (unsigned char *)(var_0 + (-160L));
    *var_19 = (unsigned char)var_13;
    var_20 = *(uint32_t *)4293564UL;
    var_21 = (uint32_t *)(var_0 + (-52L));
    *var_21 = var_20;
    var_22 = (unsigned char *)(var_0 + (-53L));
    *var_22 = (unsigned char)'\x01';
    var_23 = *var_4;
    var_24 = var_0 + (-48L);
    var_25 = (uint64_t *)var_24;
    *var_25 = var_23;
    var_26 = var_23 + 1UL;
    var_27 = (uint64_t *)(var_0 + (-40L));
    *var_27 = var_26;
    var_28 = *var_6;
    var_29 = var_0 + (-96L);
    var_30 = (uint64_t *)var_29;
    *var_30 = var_28;
    var_31 = (uint64_t *)(var_0 + (-64L));
    var_32 = (uint64_t *)(var_0 + (-80L));
    var_33 = (unsigned char *)(var_0 + (-25L));
    var_34 = (unsigned char *)(var_0 + (-65L));
    var_35 = (uint32_t *)(var_0 + (-100L));
    var_36 = (uint64_t *)(var_0 + (-88L));
    rax_0 = 1UL;
    var_37 = var_28;
    local_sp_2 = var_3;
    rcx1_0 = var_11;
    var_59 = 0U;
    var_60 = (unsigned char)'\x01';
    while (1U)
        {
            var_38 = *var_6;
            var_39 = var_38;
            local_sp_3 = local_sp_2;
            local_sp_4 = local_sp_2;
            rcx1_1 = rcx1_0;
            if ((var_38 + *var_7) <= var_37) {
                *var_31 = var_38;
                var_40 = *var_7;
                while (1U)
                    {
                        var_41 = local_sp_3 + (-8L);
                        *(uint64_t *)var_41 = 4205648UL;
                        var_42 = indirect_placeholder_7(var_40, var_39, 1UL);
                        var_45 = var_42;
                        local_sp_1 = var_41;
                        if (*var_7 == var_42) {
                            *(uint64_t *)(local_sp_3 + (-16L)) = 4205659UL;
                            indirect_placeholder();
                            var_43 = (uint64_t)*(uint32_t *)var_42;
                            var_44 = local_sp_3 + (-24L);
                            *(uint64_t *)var_44 = 4205683UL;
                            indirect_placeholder_6(0UL, rcx1_0, 4277584UL, var_43, 1UL, r9, var_8);
                            var_45 = *var_7;
                            local_sp_1 = var_44;
                        }
                        var_46 = *var_31 + var_45;
                        *var_31 = var_46;
                        var_47 = *var_30 - var_46;
                        *var_32 = var_47;
                        var_48 = *var_7;
                        var_40 = var_48;
                        local_sp_3 = local_sp_1;
                        if (var_48 > var_47) {
                            break;
                        }
                        var_39 = *var_31;
                        continue;
                    }
                var_49 = *var_31;
                var_50 = local_sp_1 + (-8L);
                *(uint64_t *)var_50 = 4205736UL;
                indirect_placeholder();
                *var_30 = (*var_32 + *var_6);
                local_sp_4 = var_50;
                rcx1_1 = var_49;
            }
            local_sp_5 = local_sp_4;
            local_sp_7 = local_sp_4;
            rcx1_2 = rcx1_1;
            local_sp_9 = local_sp_4;
            rcx1_3 = rcx1_1;
            if (*var_27 <= *var_25) {
                *var_34 = (unsigned char)'\x00';
                *var_35 = 0U;
                if (*var_22 == '\x00') {
                    var_58 = local_sp_4 + (-8L);
                    *(uint64_t *)var_58 = 4205813UL;
                    indirect_placeholder();
                    var_59 = *var_35;
                    local_sp_5 = var_58;
                }
                local_sp_6 = local_sp_5;
                if (var_59 == 0U) {
                    var_60 = *var_34;
                } else {
                    *var_34 = (unsigned char)'\x01';
                }
                if (var_60 == '\x01') {
                    var_61 = *var_6;
                    var_62 = local_sp_5 + (-8L);
                    *(uint64_t *)var_62 = 4206002UL;
                    indirect_placeholder_4(var_29, var_61);
                    local_sp_6 = var_62;
                }
                var_63 = (uint64_t)*(uint32_t *)4293560UL;
                var_64 = *var_5;
                var_65 = *var_4;
                var_66 = local_sp_6 + (-8L);
                *(uint64_t *)var_66 = 4206026UL;
                var_67 = indirect_placeholder_1(var_64, var_63);
                *var_36 = var_67;
                local_sp_8 = var_66;
                rcx1_2 = var_65;
                switch_state_var = 0;
                switch (var_67) {
                  case 18446744073709551615UL:
                    {
                        var_69 = *(uint64_t *)4293552UL;
                        *(uint64_t *)(local_sp_6 + (-16L)) = 4206062UL;
                        var_70 = indirect_placeholder_8(var_69, 3UL, 0UL);
                        var_71 = var_70.field_0;
                        var_72 = var_70.field_1;
                        var_73 = var_70.field_2;
                        *(uint64_t *)(local_sp_6 + (-24L)) = 4206070UL;
                        indirect_placeholder();
                        var_74 = (uint64_t)*(uint32_t *)var_71;
                        *(uint64_t *)(local_sp_6 + (-32L)) = 4206097UL;
                        indirect_placeholder_6(0UL, var_71, 4277581UL, var_74, 0UL, var_72, var_73);
                        var_75 = *var_6;
                        *(uint64_t *)(local_sp_6 + (-40L)) = 4206116UL;
                        indirect_placeholder_4(var_29, var_75);
                        rax_0 = 0UL;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 0UL:
                    {
                        var_68 = *var_6;
                        *(uint64_t *)(local_sp_6 + (-16L)) = 4206161UL;
                        indirect_placeholder_4(var_29, var_68);
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  default:
                    {
                        var_76 = *var_4;
                        *var_27 = var_76;
                        *var_25 = (*var_36 + var_76);
                        **(unsigned char **)var_24 = (unsigned char)'\n';
                        var_77 = *var_27;
                        *var_27 = (var_77 + 1UL);
                        var_78 = *(unsigned char *)var_77;
                        *var_33 = var_78;
                        var_79 = var_78;
                        local_sp_9 = local_sp_8;
                        rcx1_3 = rcx1_2;
                        rcx1_6 = rcx1_3;
                        local_sp_2_be = local_sp_9;
                        rcx1_0_be = rcx1_3;
                        local_sp_10 = local_sp_9;
                        rcx1_4 = rcx1_3;
                        if (var_79 != '\n') {
                            if ((int)*var_21 >= (int)0U & *var_16 == '\x00') {
                                *(uint64_t *)(local_sp_9 + (-8L)) = 4206408UL;
                                indirect_placeholder();
                                var_80 = *var_30;
                                var_81 = local_sp_9 + (-16L);
                                *(uint64_t *)var_81 = 4206430UL;
                                indirect_placeholder();
                                *var_30 = var_80;
                                local_sp_10 = var_81;
                            }
                            local_sp_2_be = local_sp_10;
                            if (*var_14 == '\x00') {
                                var_103 = *var_33;
                                while (1U)
                                    {
                                        rcx1_7 = rcx1_6;
                                        rcx1_0_be = rcx1_6;
                                        if (var_103 == '\t') {
                                            if (*var_15 == '\x00') {
                                                var_104 = *var_30;
                                                *var_30 = (var_104 + 1UL);
                                                *(unsigned char *)var_104 = (unsigned char)'^';
                                                var_105 = (uint64_t)*var_33 + 64UL;
                                                var_106 = *var_30;
                                                *var_30 = (var_106 + 1UL);
                                                *(unsigned char *)var_106 = (unsigned char)var_105;
                                                rcx1_7 = var_105;
                                            } else {
                                                if (var_103 != '\n') {
                                                    break;
                                                }
                                                var_107 = *var_30;
                                                *var_30 = (var_107 + 1UL);
                                                *(unsigned char *)var_107 = *var_33;
                                            }
                                        } else {
                                            if (var_103 == '\n') {
                                                break;
                                            }
                                            var_107 = *var_30;
                                            *var_30 = (var_107 + 1UL);
                                            *(unsigned char *)var_107 = *var_33;
                                        }
                                        var_108 = *var_27;
                                        *var_27 = (var_108 + 1UL);
                                        var_109 = *(unsigned char *)var_108;
                                        *var_33 = var_109;
                                        var_103 = var_109;
                                        rcx1_6 = rcx1_7;
                                        continue;
                                    }
                                *var_21 = 4294967295U;
                            } else {
                                var_82 = *var_33;
                                while (1U)
                                    {
                                        rcx1_5 = rcx1_4;
                                        rcx1_0_be = rcx1_4;
                                        if (var_82 > '\x1f') {
                                            if (var_82 > '~') {
                                                var_87 = *var_30;
                                                *var_30 = (var_87 + 1UL);
                                                *(unsigned char *)var_87 = *var_33;
                                            } else {
                                                var_88 = (var_82 == '\x7f');
                                                var_89 = *var_30;
                                                *var_30 = (var_89 + 1UL);
                                                var_90 = (unsigned char *)var_89;
                                                if (var_88) {
                                                    *var_90 = (unsigned char)'^';
                                                    var_100 = *var_30;
                                                    *var_30 = (var_100 + 1UL);
                                                    *(unsigned char *)var_100 = (unsigned char)'?';
                                                } else {
                                                    *var_90 = (unsigned char)'M';
                                                    var_91 = *var_30;
                                                    *var_30 = (var_91 + 1UL);
                                                    *(unsigned char *)var_91 = (unsigned char)'-';
                                                    var_92 = *var_33;
                                                    if (var_92 > '\x9f') {
                                                        var_93 = *var_30;
                                                        *var_30 = (var_93 + 1UL);
                                                        *(unsigned char *)var_93 = (unsigned char)'^';
                                                        var_94 = (uint64_t)((uint32_t)(uint64_t)*var_33 + (-64));
                                                        var_95 = *var_30;
                                                        *var_30 = (var_95 + 1UL);
                                                        *(unsigned char *)var_95 = (unsigned char)var_94;
                                                        rcx1_5 = var_94;
                                                    } else {
                                                        if (var_92 == '\xff') {
                                                            var_98 = *var_30;
                                                            *var_30 = (var_98 + 1UL);
                                                            *(unsigned char *)var_98 = (unsigned char)'^';
                                                            var_99 = *var_30;
                                                            *var_30 = (var_99 + 1UL);
                                                            *(unsigned char *)var_99 = (unsigned char)'?';
                                                        } else {
                                                            var_96 = (uint64_t)((uint32_t)(uint64_t)var_92 + (-128));
                                                            var_97 = *var_30;
                                                            *var_30 = (var_97 + 1UL);
                                                            *(unsigned char *)var_97 = (unsigned char)var_96;
                                                            rcx1_5 = var_96;
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            if (var_82 != '\t') {
                                                if (*var_15 != '\x01') {
                                                    var_83 = *var_30;
                                                    *var_30 = (var_83 + 1UL);
                                                    *(unsigned char *)var_83 = (unsigned char)'\t';
                                                    var_101 = *var_27;
                                                    *var_27 = (var_101 + 1UL);
                                                    var_102 = *(unsigned char *)var_101;
                                                    *var_33 = var_102;
                                                    var_82 = var_102;
                                                    rcx1_4 = rcx1_5;
                                                    continue;
                                                }
                                            }
                                            if (var_82 != '\n') {
                                                break;
                                            }
                                            var_84 = *var_30;
                                            *var_30 = (var_84 + 1UL);
                                            *(unsigned char *)var_84 = (unsigned char)'^';
                                            var_85 = (uint64_t)*var_33 + 64UL;
                                            var_86 = *var_30;
                                            *var_30 = (var_86 + 1UL);
                                            *(unsigned char *)var_86 = (unsigned char)var_85;
                                            rcx1_5 = var_85;
                                        }
                                    }
                                *var_21 = 4294967295U;
                            }
                        }
                        var_37 = *var_30;
                        local_sp_2 = local_sp_2_be;
                        rcx1_0 = rcx1_0_be;
                        continue;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
            var_51 = *var_21 + 1U;
            *var_21 = var_51;
            if ((int)var_51 > (int)0U) {
                if ((int)var_51 > (int)1U) {
                    *var_21 = 2U;
                    if (*var_19 == '\x00') {
                        var_52 = *var_27;
                        *var_27 = (var_52 + 1UL);
                        var_53 = *(unsigned char *)var_52;
                        *var_33 = var_53;
                        var_79 = var_53;
                    } else {
                        if (*var_16 != '\x00' & *var_17 == '\x01') {
                            *(uint64_t *)(local_sp_4 + (-8L)) = 4206295UL;
                            indirect_placeholder();
                            var_54 = *var_30;
                            var_55 = local_sp_4 + (-16L);
                            *(uint64_t *)var_55 = 4206317UL;
                            indirect_placeholder();
                            *var_30 = var_54;
                            local_sp_7 = var_55;
                        }
                        local_sp_8 = local_sp_7;
                        if (*var_18 == '\x00') {
                            var_56 = *var_30;
                            *var_30 = (var_56 + 1UL);
                            *(unsigned char *)var_56 = (unsigned char)'$';
                        }
                        var_57 = *var_30;
                        *var_30 = (var_57 + 1UL);
                        *(unsigned char *)var_57 = (unsigned char)'\n';
                        var_77 = *var_27;
                        *var_27 = (var_77 + 1UL);
                        var_78 = *(unsigned char *)var_77;
                        *var_33 = var_78;
                        var_79 = var_78;
                        local_sp_9 = local_sp_8;
                        rcx1_3 = rcx1_2;
                    }
                } else {
                    if (*var_16 != '\x00' & *var_17 == '\x01') {
                        *(uint64_t *)(local_sp_4 + (-8L)) = 4206295UL;
                        indirect_placeholder();
                        var_54 = *var_30;
                        var_55 = local_sp_4 + (-16L);
                        *(uint64_t *)var_55 = 4206317UL;
                        indirect_placeholder();
                        *var_30 = var_54;
                        local_sp_7 = var_55;
                    }
                    local_sp_8 = local_sp_7;
                    if (*var_18 == '\x00') {
                        var_56 = *var_30;
                        *var_30 = (var_56 + 1UL);
                        *(unsigned char *)var_56 = (unsigned char)'$';
                    }
                    var_57 = *var_30;
                    *var_30 = (var_57 + 1UL);
                    *(unsigned char *)var_57 = (unsigned char)'\n';
                    var_77 = *var_27;
                    *var_27 = (var_77 + 1UL);
                    var_78 = *(unsigned char *)var_77;
                    *var_33 = var_78;
                    var_79 = var_78;
                    local_sp_9 = local_sp_8;
                    rcx1_3 = rcx1_2;
                }
            } else {
                local_sp_8 = local_sp_7;
                if (*var_18 == '\x00') {
                    var_56 = *var_30;
                    *var_30 = (var_56 + 1UL);
                    *(unsigned char *)var_56 = (unsigned char)'$';
                }
                var_57 = *var_30;
                *var_30 = (var_57 + 1UL);
                *(unsigned char *)var_57 = (unsigned char)'\n';
                var_77 = *var_27;
                *var_27 = (var_77 + 1UL);
                var_78 = *(unsigned char *)var_77;
                *var_33 = var_78;
                var_79 = var_78;
                local_sp_9 = local_sp_8;
                rcx1_3 = rcx1_2;
            }
            rcx1_6 = rcx1_3;
            local_sp_2_be = local_sp_9;
            rcx1_0_be = rcx1_3;
            local_sp_10 = local_sp_9;
            rcx1_4 = rcx1_3;
            if (var_79 != '\n') {
                if ((int)*var_21 >= (int)0U & *var_16 == '\x00') {
                    *(uint64_t *)(local_sp_9 + (-8L)) = 4206408UL;
                    indirect_placeholder();
                    var_80 = *var_30;
                    var_81 = local_sp_9 + (-16L);
                    *(uint64_t *)var_81 = 4206430UL;
                    indirect_placeholder();
                    *var_30 = var_80;
                    local_sp_10 = var_81;
                }
                local_sp_2_be = local_sp_10;
                if (*var_14 == '\x00') {
                    var_103 = *var_33;
                    while (1U)
                        {
                            rcx1_7 = rcx1_6;
                            rcx1_0_be = rcx1_6;
                            if (var_103 == '\t') {
                                if (*var_15 == '\x00') {
                                    var_104 = *var_30;
                                    *var_30 = (var_104 + 1UL);
                                    *(unsigned char *)var_104 = (unsigned char)'^';
                                    var_105 = (uint64_t)*var_33 + 64UL;
                                    var_106 = *var_30;
                                    *var_30 = (var_106 + 1UL);
                                    *(unsigned char *)var_106 = (unsigned char)var_105;
                                    rcx1_7 = var_105;
                                } else {
                                    if (var_103 != '\n') {
                                        break;
                                    }
                                    var_107 = *var_30;
                                    *var_30 = (var_107 + 1UL);
                                    *(unsigned char *)var_107 = *var_33;
                                }
                            } else {
                                if (var_103 == '\n') {
                                    break;
                                }
                                var_107 = *var_30;
                                *var_30 = (var_107 + 1UL);
                                *(unsigned char *)var_107 = *var_33;
                            }
                            var_108 = *var_27;
                            *var_27 = (var_108 + 1UL);
                            var_109 = *(unsigned char *)var_108;
                            *var_33 = var_109;
                            var_103 = var_109;
                            rcx1_6 = rcx1_7;
                            continue;
                        }
                    *var_21 = 4294967295U;
                } else {
                    var_82 = *var_33;
                    while (1U)
                        {
                            rcx1_5 = rcx1_4;
                            rcx1_0_be = rcx1_4;
                            if (var_82 > '\x1f') {
                                if (var_82 > '~') {
                                    var_87 = *var_30;
                                    *var_30 = (var_87 + 1UL);
                                    *(unsigned char *)var_87 = *var_33;
                                } else {
                                    var_88 = (var_82 == '\x7f');
                                    var_89 = *var_30;
                                    *var_30 = (var_89 + 1UL);
                                    var_90 = (unsigned char *)var_89;
                                    if (var_88) {
                                        *var_90 = (unsigned char)'^';
                                        var_100 = *var_30;
                                        *var_30 = (var_100 + 1UL);
                                        *(unsigned char *)var_100 = (unsigned char)'?';
                                    } else {
                                        *var_90 = (unsigned char)'M';
                                        var_91 = *var_30;
                                        *var_30 = (var_91 + 1UL);
                                        *(unsigned char *)var_91 = (unsigned char)'-';
                                        var_92 = *var_33;
                                        if (var_92 > '\x9f') {
                                            var_93 = *var_30;
                                            *var_30 = (var_93 + 1UL);
                                            *(unsigned char *)var_93 = (unsigned char)'^';
                                            var_94 = (uint64_t)((uint32_t)(uint64_t)*var_33 + (-64));
                                            var_95 = *var_30;
                                            *var_30 = (var_95 + 1UL);
                                            *(unsigned char *)var_95 = (unsigned char)var_94;
                                            rcx1_5 = var_94;
                                        } else {
                                            if (var_92 == '\xff') {
                                                var_98 = *var_30;
                                                *var_30 = (var_98 + 1UL);
                                                *(unsigned char *)var_98 = (unsigned char)'^';
                                                var_99 = *var_30;
                                                *var_30 = (var_99 + 1UL);
                                                *(unsigned char *)var_99 = (unsigned char)'?';
                                            } else {
                                                var_96 = (uint64_t)((uint32_t)(uint64_t)var_92 + (-128));
                                                var_97 = *var_30;
                                                *var_30 = (var_97 + 1UL);
                                                *(unsigned char *)var_97 = (unsigned char)var_96;
                                                rcx1_5 = var_96;
                                            }
                                        }
                                    }
                                }
                            } else {
                                if (var_82 != '\t') {
                                    if (*var_15 != '\x01') {
                                        var_83 = *var_30;
                                        *var_30 = (var_83 + 1UL);
                                        *(unsigned char *)var_83 = (unsigned char)'\t';
                                        var_101 = *var_27;
                                        *var_27 = (var_101 + 1UL);
                                        var_102 = *(unsigned char *)var_101;
                                        *var_33 = var_102;
                                        var_82 = var_102;
                                        rcx1_4 = rcx1_5;
                                        continue;
                                    }
                                }
                                if (var_82 != '\n') {
                                    break;
                                }
                                var_84 = *var_30;
                                *var_30 = (var_84 + 1UL);
                                *(unsigned char *)var_84 = (unsigned char)'^';
                                var_85 = (uint64_t)*var_33 + 64UL;
                                var_86 = *var_30;
                                *var_30 = (var_86 + 1UL);
                                *(unsigned char *)var_86 = (unsigned char)var_85;
                                rcx1_5 = var_85;
                            }
                        }
                    *var_21 = 4294967295U;
                }
            }
            var_37 = *var_30;
            local_sp_2 = local_sp_2_be;
            rcx1_0 = rcx1_0_be;
            continue;
        }
    *(uint32_t *)4293564UL = *var_21;
    return rax_0;
}
