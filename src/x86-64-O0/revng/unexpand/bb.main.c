typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_7_ret_type;
struct indirect_placeholder_10_ret_type;
struct indirect_placeholder_11_ret_type;
struct indirect_placeholder_12_ret_type;
struct indirect_placeholder_7_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_10_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_11_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_12_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_r10(void);
extern void indirect_placeholder_8(uint64_t param_0, uint64_t param_1);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_6(uint64_t param_0);
extern void indirect_placeholder_5(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_7_ret_type indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern void indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_10_ret_type indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_11_ret_type indirect_placeholder_11(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_12_ret_type indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
typedef _Bool bool;
void bb_main(uint64_t rsi, uint64_t rdi) {
    struct indirect_placeholder_11_ret_type var_38;
    struct indirect_placeholder_12_ret_type var_18;
    uint32_t *var_13;
    uint64_t *var_14;
    uint64_t local_sp_1;
    uint64_t rbx_0;
    uint64_t rbx_1_ph;
    uint64_t r10_0;
    uint64_t local_sp_4_ph;
    uint64_t r10_2_ph;
    uint64_t local_sp_4_ph99;
    uint64_t rcx_0;
    uint64_t storemerge;
    uint64_t var_24;
    uint64_t var_25;
    struct indirect_placeholder_7_ret_type var_26;
    uint64_t local_sp_0;
    uint64_t r8_0;
    uint32_t var_27;
    uint64_t rcx_1;
    uint64_t r8_1;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    struct indirect_placeholder_10_ret_type var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t local_sp_4_be;
    uint64_t local_sp_4;
    uint64_t local_sp_2;
    uint64_t r9_1;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_39;
    uint64_t local_sp_3;
    uint64_t r10_1;
    uint64_t r10_2_ph100;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint32_t var_23;
    uint64_t r9_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint32_t *var_6;
    uint64_t var_7;
    uint64_t *var_8;
    unsigned char *var_9;
    unsigned char *var_10;
    uint64_t var_11;
    uint64_t var_12;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = init_cc_src2();
    var_4 = init_r10();
    var_5 = var_0 + (-8L);
    *(uint64_t *)var_5 = var_1;
    var_6 = (uint32_t *)(var_0 + (-28L));
    *var_6 = (uint32_t)rdi;
    var_7 = var_0 + (-40L);
    var_8 = (uint64_t *)var_7;
    *var_8 = rsi;
    var_9 = (unsigned char *)(var_0 + (-9L));
    *var_9 = (unsigned char)'\x00';
    var_10 = (unsigned char *)(var_0 + (-10L));
    *var_10 = (unsigned char)'\x00';
    var_11 = **(uint64_t **)var_7;
    *(uint64_t *)(var_0 + (-48L)) = 4205564UL;
    indirect_placeholder_6(var_11);
    *(uint64_t *)(var_0 + (-56L)) = 4205579UL;
    indirect_placeholder();
    var_12 = var_0 + (-64L);
    *(uint64_t *)var_12 = 4205589UL;
    indirect_placeholder();
    var_13 = (uint32_t *)(var_0 + (-16L));
    var_14 = (uint64_t *)(var_0 + (-24L));
    rbx_1_ph = var_2;
    local_sp_4_ph = var_12;
    r10_2_ph = var_4;
    storemerge = 0UL;
    while (1U)
        {
            rbx_0 = rbx_1_ph;
            local_sp_4_ph99 = local_sp_4_ph;
            r10_2_ph100 = r10_2_ph;
            while (1U)
                {
                    r10_0 = r10_2_ph100;
                    r10_1 = r10_2_ph100;
                    local_sp_4 = local_sp_4_ph99;
                    while (1U)
                        {
                            var_15 = *var_8;
                            var_16 = (uint64_t)*var_6;
                            var_17 = local_sp_4 + (-8L);
                            *(uint64_t *)var_17 = 4206053UL;
                            var_18 = indirect_placeholder_12(4272672UL, 4273430UL, var_15, var_16, 0UL);
                            var_19 = var_18.field_0;
                            var_20 = var_18.field_1;
                            var_21 = var_18.field_2;
                            var_22 = var_18.field_3;
                            var_23 = (uint32_t)var_19;
                            *var_13 = var_23;
                            local_sp_1 = var_17;
                            rcx_0 = var_20;
                            local_sp_0 = var_17;
                            r8_0 = var_22;
                            rcx_1 = var_20;
                            r8_1 = var_22;
                            local_sp_4_be = var_17;
                            local_sp_2 = var_17;
                            r9_1 = var_21;
                            r9_0 = var_21;
                            switch_state_var = 0;
                            switch (var_23) {
                              case 128U:
                                {
                                    *var_10 = (unsigned char)'\x01';
                                    local_sp_4 = local_sp_4_be;
                                    continue;
                                }
                                break;
                              case 4294967295U:
                                {
                                    if (*var_10 != '\x00') {
                                        loop_state_var = 0U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    *(unsigned char *)4289424UL = (unsigned char)'\x00';
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              default:
                                {
                                    if ((int)var_23 <= (int)128U) {
                                        loop_state_var = 2U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    if (var_23 == 116U) {
                                        *(unsigned char *)4289424UL = (unsigned char)'\x01';
                                        var_33 = *(uint64_t *)4289952UL;
                                        var_34 = local_sp_4 + (-16L);
                                        *(uint64_t *)var_34 = 4205751UL;
                                        indirect_placeholder_9(var_20, rbx_1_ph, var_33, r10_2_ph100, var_21, var_22);
                                        local_sp_4_be = var_34;
                                    } else {
                                        if ((int)var_23 <= (int)116U) {
                                            loop_state_var = 2U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                        if (var_23 != 97U) {
                                            loop_state_var = 1U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                        *(unsigned char *)4289424UL = (unsigned char)'\x01';
                                    }
                                    local_sp_4 = local_sp_4_be;
                                    continue;
                                }
                                break;
                            }
                            if (switch_state_var)
                                break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 1U:
                        {
                            if ((int)var_23 > (int)97U) {
                                local_sp_3 = local_sp_2;
                                if (*var_9 == '\x01') {
                                    *var_14 = 0UL;
                                    *var_9 = (unsigned char)'\x01';
                                }
                                var_35 = *var_14;
                                if (var_35 > 1844674407370955161UL) {
                                    var_37 = local_sp_2 + (-8L);
                                    *(uint64_t *)var_37 = 4206022UL;
                                    var_38 = indirect_placeholder_11(0UL, rcx_1, 4273402UL, 0UL, 1UL, r9_1, r8_1);
                                    var_39 = var_38.field_1;
                                    local_sp_3 = var_37;
                                    r10_1 = var_39;
                                    local_sp_4_ph99 = local_sp_3;
                                    r10_2_ph100 = r10_1;
                                    continue;
                                }
                                var_36 = helper_cc_compute_c_wrapper(((var_35 * 10UL) + (uint64_t)((long)(((uint64_t)*var_13 << 32UL) + (-206158430208L)) >> (long)32UL)) - var_35, var_35, var_3, 17U);
                                if (var_36 != 0UL) {
                                    *var_14 = ((uint64_t)((long)(((uint64_t)*var_13 << 32UL) + (-206158430208L)) >> (long)32UL) + (*var_14 * 10UL));
                                    local_sp_4_ph99 = local_sp_3;
                                    r10_2_ph100 = r10_1;
                                    continue;
                                }
                            }
                            if (var_23 == 63U) {
                                *(uint64_t *)(local_sp_4 + (-16L)) = 4205717UL;
                                indirect_placeholder_8(var_5, 1UL);
                                abort();
                            }
                            if ((int)var_23 <= (int)63U) {
                                if (var_23 != 44U) {
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                                if ((int)var_23 <= (int)44U) {
                                    *(uint64_t *)(local_sp_4 + (-16L)) = 4205802UL;
                                    indirect_placeholder_8(var_5, 0UL);
                                    abort();
                                }
                            }
                        }
                        break;
                      case 2U:
                        {
                            local_sp_3 = local_sp_2;
                            if (*var_9 == '\x01') {
                                *var_14 = 0UL;
                                *var_9 = (unsigned char)'\x01';
                            }
                            var_35 = *var_14;
                            if (var_35 > 1844674407370955161UL) {
                                var_37 = local_sp_2 + (-8L);
                                *(uint64_t *)var_37 = 4206022UL;
                                var_38 = indirect_placeholder_11(0UL, rcx_1, 4273402UL, 0UL, 1UL, r9_1, r8_1);
                                var_39 = var_38.field_1;
                                local_sp_3 = var_37;
                                r10_1 = var_39;
                                local_sp_4_ph99 = local_sp_3;
                                r10_2_ph100 = r10_1;
                                continue;
                            }
                            var_36 = helper_cc_compute_c_wrapper(((var_35 * 10UL) + (uint64_t)((long)(((uint64_t)*var_13 << 32UL) + (-206158430208L)) >> (long)32UL)) - var_35, var_35, var_3, 17U);
                            if (var_36 == 0UL) {
                                *var_14 = ((uint64_t)((long)(((uint64_t)*var_13 << 32UL) + (-206158430208L)) >> (long)32UL) + (*var_14 * 10UL));
                                local_sp_4_ph99 = local_sp_3;
                                r10_2_ph100 = r10_1;
                                continue;
                            }
                        }
                        break;
                      case 0U:
                        {
                            if (*var_9 != '\x00') {
                                loop_state_var = 0U;
                                switch_state_var = 1;
                                break;
                            }
                            var_24 = *var_14;
                            var_25 = local_sp_4 + (-16L);
                            *(uint64_t *)var_25 = 4206097UL;
                            var_26 = indirect_placeholder_7(var_20, rbx_1_ph, var_24, r10_2_ph100, var_21, var_22);
                            rcx_0 = var_26.field_0;
                            local_sp_0 = var_25;
                            r9_0 = var_26.field_3;
                            r8_0 = var_26.field_4;
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 1U:
                {
                    if (*var_9 == '\x00') {
                        var_30 = *var_14;
                        var_31 = local_sp_4 + (-16L);
                        *(uint64_t *)var_31 = 4205783UL;
                        var_32 = indirect_placeholder_10(var_20, rbx_1_ph, var_30, r10_2_ph100, var_21, var_22);
                        rbx_0 = var_32.field_1;
                        local_sp_1 = var_31;
                        r10_0 = var_32.field_2;
                    }
                    *var_9 = (unsigned char)'\x00';
                    rbx_1_ph = rbx_0;
                    local_sp_4_ph = local_sp_1;
                    r10_2_ph = r10_0;
                    continue;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    *(uint64_t *)(local_sp_0 + (-8L)) = 4206102UL;
    indirect_placeholder_5(rcx_0, r9_0, r8_0);
    var_27 = *(uint32_t *)4289272UL;
    if ((int)var_27 >= (int)*var_6) {
        *(uint64_t *)(local_sp_0 + (-16L)) = 4206151UL;
        indirect_placeholder_6(storemerge);
        *(uint64_t *)(local_sp_0 + (-24L)) = 4206156UL;
        indirect_placeholder();
        *(uint64_t *)(local_sp_0 + (-32L)) = 4206161UL;
        indirect_placeholder();
        return;
    }
    storemerge = *var_8 + ((uint64_t)var_27 << 3UL);
    *(uint64_t *)(local_sp_0 + (-16L)) = 4206151UL;
    indirect_placeholder_6(storemerge);
    *(uint64_t *)(local_sp_0 + (-24L)) = 4206156UL;
    indirect_placeholder();
    *(uint64_t *)(local_sp_0 + (-32L)) = 4206161UL;
    indirect_placeholder();
    return;
}
