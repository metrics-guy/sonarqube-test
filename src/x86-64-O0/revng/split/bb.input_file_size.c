typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_input_file_size_ret_type;
struct bb_input_file_size_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_1(void);
extern uint64_t indirect_placeholder_9(uint64_t param_0);
typedef _Bool bool;
struct bb_input_file_size_ret_type bb_input_file_size(uint64_t rcx, uint64_t rdx, uint64_t rsi, uint64_t rdi) {
    uint64_t local_sp_0;
    uint64_t var_0;
    uint64_t var_1;
    uint32_t *var_2;
    uint64_t *var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t *var_8;
    uint64_t *var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t *_pre_phi73;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_24;
    uint64_t rcx3_1;
    uint64_t *var_25;
    uint64_t rcx3_0;
    uint64_t rdi6_0;
    uint64_t var_34;
    uint64_t rax_0;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t *var_28;
    uint64_t var_29;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t rdi6_1;
    struct bb_input_file_size_ret_type mrv;
    struct bb_input_file_size_ret_type mrv1;
    struct bb_input_file_size_ret_type mrv2;
    uint64_t var_12;
    uint64_t local_sp_1;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = (uint32_t *)(var_0 + (-44L));
    *var_2 = (uint32_t)rdi;
    var_3 = (uint64_t *)(var_0 + (-56L));
    *var_3 = rsi;
    var_4 = (uint64_t *)(var_0 + (-64L));
    *var_4 = rdx;
    var_5 = (uint64_t *)(var_0 + (-72L));
    *var_5 = rcx;
    var_6 = (uint64_t)*var_2;
    var_7 = var_0 + (-80L);
    *(uint64_t *)var_7 = 4205890UL;
    indirect_placeholder_1();
    var_8 = (uint64_t *)(var_0 + (-32L));
    *var_8 = var_6;
    var_9 = (uint64_t *)(var_0 + (-16L));
    *var_9 = 0UL;
    var_10 = (uint64_t *)(var_0 + (-40L));
    rax_0 = 18446744073709551615UL;
    var_11 = *var_5;
    var_12 = 0UL;
    local_sp_1 = var_7;
    while (1U)
        {
            var_13 = var_11 - var_12;
            var_14 = *var_4;
            var_15 = (uint64_t)*var_2;
            var_16 = local_sp_1 + (-8L);
            *(uint64_t *)var_16 = 4205979UL;
            var_17 = indirect_placeholder(var_13, var_15);
            *var_10 = var_17;
            rdi6_1 = var_15;
            local_sp_1 = var_16;
            switch_state_var = 0;
            switch (var_17) {
              case 0UL:
                {
                    rax_0 = *var_9;
                    rcx3_1 = var_12 + var_14;
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 18446744073709551615UL:
                {
                    rcx3_1 = var_12 + var_14;
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              default:
                {
                    var_18 = var_17 + *var_9;
                    *var_9 = var_18;
                    var_19 = *var_5;
                    var_11 = var_19;
                    var_12 = var_18;
                    if (var_19 > var_18) {
                        continue;
                    }
                    var_20 = var_12 + var_14;
                    rcx3_0 = var_20;
                    rcx3_1 = var_20;
                    if (*(uint64_t *)(*var_3 + 48UL) != 0UL) {
                        *(uint64_t *)(local_sp_1 + (-16L)) = 4206061UL;
                        indirect_placeholder_1();
                        *(volatile uint32_t *)(uint32_t *)0UL = 75U;
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    *var_8 = (*var_8 + var_18);
                    var_21 = *var_3;
                    var_22 = local_sp_1 + (-16L);
                    *(uint64_t *)var_22 = 4206099UL;
                    var_23 = indirect_placeholder_9(var_21);
                    local_sp_0 = var_22;
                    rdi6_0 = var_21;
                    if ((uint64_t)(unsigned char)var_23 != 0UL) {
                        loop_state_var = 2U;
                        switch_state_var = 1;
                        break;
                    }
                    var_24 = *(uint64_t *)(*var_3 + 48UL);
                    if ((long)*var_8 <= (long)var_24) {
                        loop_state_var = 2U;
                        switch_state_var = 1;
                        break;
                    }
                    var_25 = (uint64_t *)(var_0 + (-24L));
                    *var_25 = var_24;
                    _pre_phi73 = var_25;
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            mrv.field_0 = rax_0;
            mrv1 = mrv;
            mrv1.field_1 = rcx3_1;
            mrv2 = mrv1;
            mrv2.field_2 = rdi6_1;
            return mrv2;
        }
        break;
      case 2U:
      case 1U:
        {
            switch (loop_state_var) {
              case 1U:
                {
                    var_34 = *var_9 + (*_pre_phi73 - *var_8);
                    *var_9 = var_34;
                    rax_0 = var_34;
                    rcx3_1 = rcx3_0;
                    rdi6_1 = rdi6_0;
                    if (var_34 == 9223372036854775807UL) {
                        *(uint64_t *)(local_sp_0 + (-8L)) = 4206268UL;
                        indirect_placeholder_1();
                        *(uint32_t *)9223372036854775807UL = 75U;
                        rax_0 = 18446744073709551615UL;
                    }
                }
                break;
              case 2U:
                {
                    var_26 = (uint64_t)*var_2;
                    var_27 = local_sp_1 + (-24L);
                    *(uint64_t *)var_27 = 4206151UL;
                    indirect_placeholder_1();
                    var_28 = (uint64_t *)(var_0 + (-24L));
                    *var_28 = var_26;
                    var_29 = *var_8;
                    _pre_phi73 = var_28;
                    local_sp_0 = var_27;
                    rdi6_0 = var_26;
                    var_30 = (uint64_t)*var_2;
                    var_31 = local_sp_1 + (-32L);
                    *(uint64_t *)var_31 = 4206203UL;
                    indirect_placeholder_1();
                    var_32 = *var_28;
                    var_33 = *var_8;
                    local_sp_0 = var_31;
                    rcx3_0 = var_29;
                    rdi6_0 = var_30;
                    if (var_26 != var_29 & (long)var_32 < (long)var_33) {
                        *var_28 = var_33;
                    }
                }
                break;
            }
        }
        break;
    }
}
