typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_next_file_name_ret_type;
struct indirect_placeholder_33_ret_type;
struct indirect_placeholder_35_ret_type;
struct bb_next_file_name_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_33_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_35_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_1(void);
extern uint64_t init_r10(void);
extern uint64_t indirect_placeholder_32(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern uint64_t init_rbx(void);
extern struct indirect_placeholder_33_ret_type indirect_placeholder_33(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_34(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_35_ret_type indirect_placeholder_35(uint64_t param_0, uint64_t param_1);
struct bb_next_file_name_ret_type bb_next_file_name(uint64_t rcx, uint64_t r9, uint64_t r8) {
    uint64_t local_sp_3;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_7;
    uint64_t *var_8;
    uint64_t r94_0;
    uint64_t r85_0;
    uint64_t var_9;
    uint64_t rcx3_1;
    uint64_t var_10;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t *_cast;
    uint64_t var_12;
    uint64_t local_sp_4;
    uint64_t var_11;
    struct indirect_placeholder_33_ret_type var_22;
    uint64_t var_56;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t rcx3_2;
    uint64_t var_64;
    uint64_t var_55;
    uint64_t local_sp_0;
    uint64_t *var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t *var_61;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_46;
    uint64_t rcx3_0;
    uint64_t local_sp_1;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t local_sp_2;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_31;
    uint64_t rax_0;
    bool var_26;
    unsigned char *var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    struct indirect_placeholder_35_ret_type var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    struct bb_next_file_name_ret_type mrv;
    struct bb_next_file_name_ret_type mrv1;
    struct bb_next_file_name_ret_type mrv2;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = init_r10();
    var_4 = init_rbx();
    var_5 = var_0 + (-8L);
    *(uint64_t *)var_5 = var_1;
    var_6 = var_0 + (-40L);
    r94_0 = r9;
    r85_0 = r8;
    rcx3_1 = rcx;
    local_sp_4 = var_6;
    rax_0 = 0UL;
    if (*(uint64_t *)4314920UL != 0UL) {
        var_7 = *(uint64_t *)4314936UL;
        var_8 = (uint64_t *)(var_0 + (-32L));
        *var_8 = var_7;
        var_9 = var_7;
        while (1U)
            {
                var_10 = var_9 + (-1L);
                *var_8 = var_10;
                if (var_9 != 0UL) {
                    *(uint64_t *)(var_0 + (-48L)) = 4207315UL;
                    var_22 = indirect_placeholder_33(0UL, rcx3_1, 4291848UL, 0UL, 1UL, r9, r8);
                    var_23 = var_22.field_0;
                    var_24 = var_22.field_3;
                    var_25 = var_22.field_4;
                    rcx3_2 = var_23;
                    r94_0 = var_24;
                    r85_0 = var_25;
                    loop_state_var = 1U;
                    break;
                }
                _cast = (uint64_t *)(*(uint64_t *)4315152UL + (var_10 << 3UL));
                *_cast = (*_cast + 1UL);
                if (*(unsigned char *)4313824UL != '\x00') {
                    var_11 = *var_8;
                    var_12 = var_11;
                    var_12 = 0UL;
                    if (var_11 != 0UL & *(unsigned char *)((**(uint64_t **)4315152UL + 1UL) + *(uint64_t *)4313832UL) != '\x00') {
                        loop_state_var = 0U;
                        break;
                    }
                }
                var_12 = *var_8;
            }
        switch (loop_state_var) {
          case 1U:
            {
                mrv.field_0 = rcx3_2;
                mrv1 = mrv;
                mrv1.field_1 = r94_0;
                mrv2 = mrv1;
                mrv2.field_2 = r85_0;
                return mrv2;
            }
            break;
          case 0U:
            {
                break;
            }
            break;
        }
    }
    var_26 = (*(uint64_t *)4315128UL != 0UL);
    var_27 = (unsigned char *)(var_0 + (-33L));
    *var_27 = var_26;
    if (var_26) {
        *(uint64_t *)4315128UL = (*(uint64_t *)4315128UL + 2UL);
        *(uint64_t *)4314936UL = (*(uint64_t *)4314936UL + 1UL);
        var_33 = *(uint64_t *)4315128UL;
    } else {
        var_28 = *(uint64_t *)4314912UL;
        var_29 = var_0 + (-48L);
        *(uint64_t *)var_29 = 4206359UL;
        indirect_placeholder_1();
        *(uint64_t *)4315136UL = var_28;
        var_30 = *(uint64_t *)4314952UL;
        local_sp_3 = var_29;
        if (var_30 != 0UL) {
            var_31 = var_0 + (-56L);
            *(uint64_t *)var_31 = 4206393UL;
            indirect_placeholder_1();
            rax_0 = var_30;
            local_sp_3 = var_31;
        }
        *(uint64_t *)4315144UL = rax_0;
        var_32 = rax_0 + (*(uint64_t *)4315136UL + *(uint64_t *)4314936UL);
        *(uint64_t *)4315128UL = var_32;
        var_33 = var_32;
        local_sp_4 = local_sp_3;
    }
    var_34 = var_33 + 1UL;
    var_35 = *(uint64_t *)4315136UL;
    var_36 = helper_cc_compute_c_wrapper(var_34 - var_35, var_35, var_2, 17U);
    if (var_36 == 0UL) {
        *(uint64_t *)(local_sp_4 + (-8L)) = 4206507UL;
        indirect_placeholder_34(var_5, r9, r8);
        abort();
    }
    var_37 = *(uint64_t *)4315128UL + 1UL;
    var_38 = *(uint64_t *)4314920UL;
    var_39 = local_sp_4 + (-8L);
    *(uint64_t *)var_39 = 4206536UL;
    var_40 = indirect_placeholder_35(var_37, var_38);
    var_41 = var_40.field_0;
    var_42 = var_40.field_1;
    var_43 = var_40.field_2;
    *(uint64_t *)4314920UL = var_41;
    local_sp_2 = var_39;
    r94_0 = var_42;
    r85_0 = var_43;
    if (*var_27 == '\x01') {
        *(unsigned char *)(*(uint64_t *)4315136UL + var_41) = *(unsigned char *)(**(uint64_t **)4315152UL + *(uint64_t *)4313832UL);
        var_45 = *(uint64_t *)4315136UL + 1UL;
        *(uint64_t *)4315136UL = var_45;
        var_46 = var_45;
    } else {
        var_44 = local_sp_4 + (-16L);
        *(uint64_t *)var_44 = 4206586UL;
        indirect_placeholder_1();
        var_46 = *(uint64_t *)4315136UL;
        local_sp_2 = var_44;
    }
    *(uint64_t *)4314928UL = (var_46 + *(uint64_t *)4314920UL);
    var_47 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)4313832UL;
    var_48 = local_sp_2 + (-8L);
    *(uint64_t *)var_48 = 4206709UL;
    indirect_placeholder_1();
    rcx3_0 = var_47;
    local_sp_1 = var_48;
    if (*(uint64_t *)4314952UL == 0UL) {
        var_49 = *(uint64_t *)4314936UL + *(uint64_t *)4314928UL;
        var_50 = local_sp_2 + (-16L);
        *(uint64_t *)var_50 = 4206763UL;
        indirect_placeholder_1();
        rcx3_0 = var_49;
        local_sp_1 = var_50;
    }
    *(unsigned char *)(*(uint64_t *)4315128UL + *(uint64_t *)4314920UL) = (unsigned char)'\x00';
    *(uint64_t *)(local_sp_1 + (-8L)) = 4206798UL;
    indirect_placeholder_1();
    var_51 = *(uint64_t *)4314936UL;
    var_52 = local_sp_1 + (-16L);
    *(uint64_t *)var_52 = 4206818UL;
    var_53 = indirect_placeholder_32(8UL, var_51, var_3, var_42, var_43, var_4);
    *(uint64_t *)4315152UL = var_53;
    var_54 = *(uint64_t *)4314944UL;
    var_56 = var_54;
    local_sp_0 = var_52;
    rcx3_2 = rcx3_0;
    if (var_54 == 0UL) {
        mrv.field_0 = rcx3_2;
        mrv1 = mrv;
        mrv1.field_1 = r94_0;
        mrv2 = mrv1;
        mrv2.field_2 = r85_0;
        return mrv2;
    }
    if (*var_27 == '\x01') {
        var_55 = local_sp_1 + (-24L);
        *(uint64_t *)var_55 = 4206877UL;
        indirect_placeholder_1();
        var_56 = *(uint64_t *)4314944UL;
        local_sp_0 = var_55;
    }
    *(uint64_t *)(local_sp_0 + (-8L)) = 4206892UL;
    indirect_placeholder_1();
    var_57 = (uint64_t *)(var_0 + (-16L));
    *var_57 = var_56;
    var_58 = *(uint64_t *)4314928UL + (*(uint64_t *)4314936UL - var_56);
    *(uint64_t *)(local_sp_0 + (-16L)) = 4206939UL;
    indirect_placeholder_1();
    var_59 = *(uint64_t *)4315152UL + (*(uint64_t *)4314936UL << 3UL);
    var_60 = var_0 + (-24L);
    var_61 = (uint64_t *)var_60;
    *var_61 = var_59;
    rcx3_2 = var_58;
    var_62 = *var_57;
    var_63 = var_62 + (-1L);
    *var_57 = var_63;
    while (var_62 != 0UL)
        {
            var_64 = (uint64_t)*(unsigned char *)(var_63 + *(uint64_t *)4314944UL);
            *var_61 = (*var_61 + (-8L));
            **(uint64_t **)var_60 = (uint64_t)((long)((var_64 << 32UL) + (-206158430208L)) >> (long)32UL);
            var_62 = *var_57;
            var_63 = var_62 + (-1L);
            *var_57 = var_63;
        }
    mrv.field_0 = rcx3_2;
    mrv1 = mrv;
    mrv1.field_1 = r94_0;
    mrv2 = mrv1;
    mrv2.field_2 = r85_0;
    return mrv2;
}
