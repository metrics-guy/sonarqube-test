typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_46_ret_type;
struct indirect_placeholder_47_ret_type;
struct indirect_placeholder_48_ret_type;
struct indirect_placeholder_49_ret_type;
struct indirect_placeholder_46_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_47_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_48_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_49_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_1(void);
extern uint64_t indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_rbx(void);
extern struct indirect_placeholder_46_ret_type indirect_placeholder_46(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_47_ret_type indirect_placeholder_47(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_48_ret_type indirect_placeholder_48(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_49_ret_type indirect_placeholder_49(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
typedef _Bool bool;
void bb_bytes_split(uint64_t rcx, uint64_t rdx, uint64_t rsi, uint64_t rdi, uint64_t r9, uint64_t r8) {
    struct indirect_placeholder_48_ret_type var_61;
    struct indirect_placeholder_47_ret_type var_55;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    struct indirect_placeholder_46_ret_type var_31;
    uint64_t *var_9;
    unsigned char *var_10;
    unsigned char *var_11;
    uint64_t var_12;
    uint64_t *var_13;
    uint64_t *var_14;
    uint64_t *var_15;
    unsigned char *var_16;
    uint64_t *var_17;
    uint64_t r95_7;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t local_sp_9;
    uint64_t var_32;
    uint64_t local_sp_0;
    uint64_t r86_2;
    uint64_t r95_0;
    uint64_t r86_0;
    uint64_t local_sp_2;
    uint64_t local_sp_7;
    uint64_t local_sp_1;
    uint64_t r86_7;
    uint64_t r95_1;
    uint64_t r86_1;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t r95_2;
    uint64_t local_sp_5;
    uint64_t r95_5;
    uint64_t var_33;
    uint64_t r95_4;
    uint64_t local_sp_3;
    uint64_t r86_4;
    uint64_t r95_3;
    uint64_t r86_3;
    uint64_t var_34;
    uint64_t var_49;
    unsigned char var_36;
    unsigned char var_43;
    unsigned char _pre105;
    unsigned char var_35;
    uint64_t local_sp_4;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    unsigned char storemerge;
    unsigned char var_47;
    uint64_t r86_5;
    unsigned char var_51;
    unsigned char var_58;
    unsigned char _pre108;
    unsigned char var_50;
    uint64_t local_sp_6;
    uint64_t r95_6;
    uint64_t r86_6;
    uint64_t local_sp_8_ph;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t r95_8_ph;
    uint64_t r86_8_ph;
    uint64_t local_sp_8;
    uint64_t r95_8;
    uint64_t r86_8;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_48;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    struct indirect_placeholder_49_ret_type var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    var_4 = var_0 + (-120L);
    var_5 = (uint64_t *)(var_0 + (-80L));
    *var_5 = rdi;
    var_6 = (uint64_t *)(var_0 + (-88L));
    *var_6 = rsi;
    var_7 = (uint64_t *)(var_0 + (-96L));
    *var_7 = rdx;
    var_8 = (uint64_t *)(var_0 + (-104L));
    *var_8 = rcx;
    var_9 = (uint64_t *)(var_0 + (-112L));
    *var_9 = r8;
    var_10 = (unsigned char *)(var_0 + (-33L));
    *var_10 = (unsigned char)'\x01';
    var_11 = (unsigned char *)(var_0 + (-34L));
    *var_11 = (unsigned char)'\x01';
    var_12 = *var_5;
    var_13 = (uint64_t *)(var_0 + (-48L));
    *var_13 = var_12;
    var_14 = (uint64_t *)(var_0 + (-56L));
    *var_14 = 0UL;
    var_15 = (uint64_t *)(var_0 + (-32L));
    var_16 = (unsigned char *)(var_0 + (-57L));
    var_17 = (uint64_t *)(var_0 + (-72L));
    local_sp_1 = var_4;
    r95_1 = r9;
    r86_1 = r8;
    var_43 = (unsigned char)'\x00';
    storemerge = (unsigned char)'\x01';
    var_58 = (unsigned char)'\x00';
    while (1U)
        {
            var_18 = *var_8;
            local_sp_9 = local_sp_1;
            r86_2 = r86_1;
            r95_0 = r95_1;
            r86_0 = r86_1;
            local_sp_2 = local_sp_1;
            r95_2 = r95_1;
            if (var_18 == 18446744073709551615UL) {
                *var_15 = var_18;
                *var_8 = 18446744073709551615UL;
                var_19 = *var_15;
                var_20 = *var_7;
                var_21 = helper_cc_compute_c_wrapper(var_19 - var_20, var_20, var_2, 17U);
                *var_16 = (unsigned char)var_21;
            } else {
                var_22 = *var_13;
                var_23 = local_sp_1 + (-8L);
                *(uint64_t *)var_23 = 4209641UL;
                indirect_placeholder_1();
                local_sp_9 = var_23;
                if (*var_11 != '\x01' & var_22 == 18446744073709551615UL) {
                    *var_13 = *var_5;
                    *var_10 = (unsigned char)'\x01';
                }
                var_24 = *var_7;
                var_25 = local_sp_9 + (-8L);
                *(uint64_t *)var_25 = 4209680UL;
                var_26 = indirect_placeholder(var_24, 0UL);
                *var_15 = var_26;
                var_32 = var_26;
                local_sp_0 = var_25;
                if (var_26 == 18446744073709551615UL) {
                    var_27 = *(uint64_t *)4314960UL;
                    *(uint64_t *)(local_sp_9 + (-16L)) = 4209716UL;
                    var_28 = indirect_placeholder_4(var_27, 3UL, 0UL);
                    *(uint64_t *)(local_sp_9 + (-24L)) = 4209724UL;
                    indirect_placeholder_1();
                    var_29 = (uint64_t)*(uint32_t *)var_28;
                    var_30 = local_sp_9 + (-32L);
                    *(uint64_t *)var_30 = 4209751UL;
                    var_31 = indirect_placeholder_46(0UL, var_28, 4292253UL, var_29, 1UL, r95_1, r86_1);
                    var_32 = *var_15;
                    local_sp_0 = var_30;
                    r95_0 = var_31.field_3;
                    r86_0 = var_31.field_4;
                }
                *var_16 = (var_32 == 0UL);
                local_sp_2 = local_sp_0;
                r95_2 = r95_0;
                r86_2 = r86_0;
            }
            *var_17 = *var_6;
            var_33 = *var_13;
            local_sp_3 = local_sp_2;
            r95_3 = r95_2;
            r86_3 = r86_2;
            while (1U)
                {
                    var_34 = *var_15;
                    local_sp_5 = local_sp_3;
                    r95_5 = r95_3;
                    r95_4 = r95_3;
                    r86_4 = r86_3;
                    var_49 = var_34;
                    local_sp_4 = local_sp_3;
                    r86_5 = r86_3;
                    if (var_33 <= var_34) {
                        loop_state_var = 1U;
                        break;
                    }
                    if (*var_11 == '\x00') {
                        var_35 = *var_10;
                        var_36 = var_35;
                        if (var_35 == '\x00') {
                            var_37 = (uint64_t)var_36;
                            var_38 = *var_17;
                            var_39 = local_sp_3 + (-8L);
                            *(uint64_t *)var_39 = 4209809UL;
                            var_40 = indirect_placeholder_49(var_33, var_38, var_37, r95_3, r86_3);
                            var_41 = var_40.field_1;
                            var_42 = var_40.field_2;
                            *var_11 = (unsigned char)var_40.field_0;
                            var_43 = *var_10;
                            local_sp_4 = var_39;
                            r95_4 = var_41;
                            r86_4 = var_42;
                        }
                    } else {
                        _pre105 = *var_10;
                        var_36 = _pre105;
                        var_37 = (uint64_t)var_36;
                        var_38 = *var_17;
                        var_39 = local_sp_3 + (-8L);
                        *(uint64_t *)var_39 = 4209809UL;
                        var_40 = indirect_placeholder_49(var_33, var_38, var_37, r95_3, r86_3);
                        var_41 = var_40.field_1;
                        var_42 = var_40.field_2;
                        *var_11 = (unsigned char)var_40.field_0;
                        var_43 = *var_10;
                        local_sp_4 = var_39;
                        r95_4 = var_41;
                        r86_4 = var_42;
                    }
                    var_44 = *var_14 + (uint64_t)var_43;
                    *var_14 = var_44;
                    var_45 = *var_9;
                    local_sp_5 = local_sp_4;
                    r95_5 = r95_4;
                    local_sp_3 = local_sp_4;
                    r95_3 = r95_4;
                    r86_3 = r86_4;
                    r86_5 = r86_4;
                    if (var_45 == 0UL) {
                    } else {
                        var_46 = helper_cc_compute_c_wrapper(var_44 - var_45, var_45, var_2, 17U);
                        storemerge = (unsigned char)'\x00';
                        if (var_46 == 0UL) {
                        }
                    }
                    var_47 = storemerge & '\x01';
                    *var_10 = var_47;
                    if (!((*var_11 == '\x01') || (var_47 == '\x01'))) {
                        loop_state_var = 0U;
                        break;
                    }
                    *var_17 = (*var_17 + *var_13);
                    *var_15 = (*var_15 - *var_13);
                    var_48 = *var_5;
                    *var_13 = var_48;
                    var_33 = var_48;
                    continue;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    *var_15 = 0UL;
                    *var_16 = (unsigned char)'\x01';
                    var_49 = *var_15;
                }
                break;
              case 1U:
                {
                    r95_7 = r95_5;
                    local_sp_7 = local_sp_5;
                    r86_7 = r86_5;
                    local_sp_6 = local_sp_5;
                    r95_6 = r95_5;
                    r86_6 = r86_5;
                    if (var_49 == 0UL) {
                        local_sp_1 = local_sp_7;
                        r95_1 = r95_7;
                        r86_1 = r86_7;
                        local_sp_8_ph = local_sp_7;
                        r95_8_ph = r95_7;
                        r86_8_ph = r86_7;
                        if (*var_16 != '\x01') {
                            continue;
                        }
                        switch_state_var = 1;
                        break;
                    }
                    if (*var_11 == '\x00') {
                        var_50 = *var_10;
                        var_51 = var_50;
                        if (var_50 == '\x00') {
                            var_52 = (uint64_t)var_51;
                            var_53 = *var_17;
                            var_54 = local_sp_5 + (-8L);
                            *(uint64_t *)var_54 = 4209971UL;
                            var_55 = indirect_placeholder_47(var_49, var_53, var_52, r95_5, r86_5);
                            var_56 = var_55.field_1;
                            var_57 = var_55.field_2;
                            *var_11 = (unsigned char)var_55.field_0;
                            var_58 = *var_10;
                            local_sp_6 = var_54;
                            r95_6 = var_56;
                            r86_6 = var_57;
                        }
                    } else {
                        _pre108 = *var_10;
                        var_51 = _pre108;
                        var_52 = (uint64_t)var_51;
                        var_53 = *var_17;
                        var_54 = local_sp_5 + (-8L);
                        *(uint64_t *)var_54 = 4209971UL;
                        var_55 = indirect_placeholder_47(var_49, var_53, var_52, r95_5, r86_5);
                        var_56 = var_55.field_1;
                        var_57 = var_55.field_2;
                        *var_11 = (unsigned char)var_55.field_0;
                        var_58 = *var_10;
                        local_sp_6 = var_54;
                        r95_6 = var_56;
                        r86_6 = var_57;
                    }
                    *var_14 = (*var_14 + (uint64_t)var_58);
                    *var_10 = (unsigned char)'\x00';
                    r95_7 = r95_6;
                    local_sp_7 = local_sp_6;
                    r86_7 = r86_6;
                    local_sp_8_ph = local_sp_6;
                    r95_8_ph = r95_6;
                    r86_8_ph = r86_6;
                    if (*var_11 != '\x01') {
                        if (*var_14 != *var_9) {
                            switch_state_var = 1;
                            break;
                        }
                    }
                    *var_13 = (*var_13 - *var_15);
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    local_sp_8 = local_sp_8_ph;
    r95_8 = r95_8_ph;
    r86_8 = r86_8_ph;
    var_59 = *var_14;
    *var_14 = (var_59 + 1UL);
    while (*var_9 <= var_59)
        {
            var_60 = local_sp_8 + (-8L);
            *(uint64_t *)var_60 = 4210055UL;
            var_61 = indirect_placeholder_48(0UL, 0UL, 1UL, r95_8, r86_8);
            local_sp_8 = var_60;
            r95_8 = var_61.field_1;
            r86_8 = var_61.field_2;
            var_59 = *var_14;
            *var_14 = (var_59 + 1UL);
        }
    return;
}
