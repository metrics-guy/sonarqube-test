typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_75_ret_type;
struct helper_divq_EAX_wrapper_ret_type;
struct type_6;
struct indirect_placeholder_76_ret_type;
struct indirect_placeholder_75_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_divq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint32_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint32_t field_6;
    uint64_t field_7;
    uint32_t field_8;
    uint32_t field_9;
    unsigned char field_10;
    uint32_t field_11;
    uint32_t field_12;
};
struct type_6 {
};
struct indirect_placeholder_76_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_1(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_rbx(void);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern uint32_t init_state_0x8248(void);
extern struct indirect_placeholder_75_ret_type indirect_placeholder_75(uint64_t param_0);
extern struct helper_divq_EAX_wrapper_ret_type helper_divq_EAX_wrapper(struct type_6 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint64_t param_13, uint32_t param_14, uint64_t param_15, uint64_t param_16, uint64_t param_17, uint32_t param_18, uint32_t param_19);
extern uint64_t helper_clz_wrapper(uint64_t param_0);
extern struct indirect_placeholder_76_ret_type indirect_placeholder_76(uint64_t param_0);
uint64_t bb_divide(uint64_t rcx, uint64_t rdx, uint64_t rsi, uint64_t rdi, uint64_t r10, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint32_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint32_t var_9;
    uint32_t var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t *var_13;
    uint64_t *var_14;
    uint64_t *var_15;
    uint64_t var_16;
    uint64_t *var_17;
    uint64_t var_18;
    uint64_t *var_19;
    uint64_t var_20;
    uint64_t *var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t *var_24;
    uint64_t var_25;
    uint64_t *var_26;
    uint64_t *var_27;
    uint64_t var_28;
    uint64_t var_29;
    struct indirect_placeholder_75_ret_type var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t *var_33;
    uint64_t rax_1;
    uint64_t var_52;
    uint64_t *var_53;
    uint64_t var_54;
    uint64_t *var_55;
    uint64_t *var_56;
    uint64_t var_57;
    uint64_t *var_58;
    uint64_t var_59;
    uint64_t local_sp_0;
    uint64_t var_63;
    uint64_t rdi4_0;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t local_sp_1;
    uint64_t var_38;
    uint64_t var_37;
    uint64_t local_sp_2;
    uint64_t var_235;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t local_sp_5;
    uint64_t var_41;
    uint64_t var_68;
    uint64_t var_167;
    uint64_t var_168;
    uint64_t var_169;
    uint64_t *var_170;
    uint32_t var_171;
    uint32_t *var_172;
    uint32_t *var_173;
    uint64_t var_174;
    uint64_t var_175;
    uint64_t *var_176;
    uint64_t var_177;
    uint64_t var_178;
    uint64_t *var_179;
    uint64_t var_180;
    uint64_t *var_181;
    uint64_t *var_182;
    uint64_t var_183;
    uint64_t rcx1_0;
    uint64_t state_0x9018_0;
    uint32_t state_0x9010_0;
    uint64_t state_0x82d8_0;
    uint32_t state_0x9080_0;
    uint32_t state_0x8248_0;
    bool var_184;
    uint32_t var_185;
    uint32_t state_0x9010_3;
    uint64_t var_204;
    uint64_t *var_205;
    uint64_t var_186;
    uint64_t var_187;
    uint64_t var_188;
    uint64_t var_190;
    uint64_t var_191;
    uint32_t var_192;
    uint64_t var_193;
    struct helper_divq_EAX_wrapper_ret_type var_189;
    uint32_t var_194;
    uint32_t var_195;
    uint64_t var_196;
    uint64_t var_198;
    uint32_t var_199;
    uint64_t var_200;
    struct helper_divq_EAX_wrapper_ret_type var_197;
    uint32_t var_201;
    uint32_t var_202;
    uint64_t var_203;
    uint32_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t *var_46;
    uint64_t var_47;
    uint64_t var_48;
    struct indirect_placeholder_76_ret_type var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t local_sp_3;
    uint64_t var_64;
    uint64_t *var_65;
    uint64_t var_78;
    uint64_t rdi4_1;
    uint64_t var_66;
    uint64_t *var_67;
    uint64_t *var_163;
    uint64_t var_164;
    uint64_t *var_69;
    uint64_t *var_70;
    uint64_t var_71;
    uint64_t *var_72;
    uint64_t var_73;
    uint64_t var_77;
    uint64_t local_sp_4;
    uint64_t var_79;
    uint64_t *var_80;
    uint64_t var_81;
    uint64_t *var_82;
    uint64_t var_83;
    uint64_t *var_84;
    uint32_t var_85;
    uint32_t *var_86;
    uint32_t var_87;
    uint32_t *var_88;
    uint64_t var_89;
    uint64_t *var_90;
    uint32_t *var_91;
    uint32_t *var_92;
    uint64_t *var_93;
    uint64_t *var_94;
    uint64_t *var_95;
    uint64_t *var_96;
    uint64_t var_97;
    uint64_t *var_98;
    uint64_t *var_99;
    uint64_t *var_100;
    uint32_t *var_101;
    uint64_t *var_102;
    uint64_t var_103;
    uint64_t *var_104;
    uint32_t *var_105;
    uint64_t *var_106;
    uint32_t *var_107;
    uint32_t *var_108;
    uint64_t var_109;
    uint32_t state_0x9010_4;
    uint64_t state_0x9018_1;
    uint64_t state_0x82d8_4;
    uint32_t state_0x9010_1;
    uint32_t state_0x9080_4;
    uint64_t state_0x82d8_1;
    uint32_t state_0x8248_4;
    uint32_t state_0x9080_1;
    uint32_t state_0x8248_1;
    uint64_t var_110;
    uint64_t var_111;
    uint32_t var_112;
    uint64_t var_119;
    uint64_t var_120;
    uint64_t var_121;
    uint64_t var_123;
    uint32_t var_124;
    uint64_t var_125;
    struct helper_divq_EAX_wrapper_ret_type var_122;
    uint32_t var_126;
    uint32_t var_127;
    uint64_t var_128;
    uint64_t var_130;
    uint64_t var_131;
    uint32_t var_132;
    uint64_t var_133;
    struct helper_divq_EAX_wrapper_ret_type var_129;
    uint32_t var_134;
    uint32_t var_135;
    uint32_t var_136;
    uint32_t var_137;
    uint64_t var_113;
    uint64_t var_114;
    uint64_t state_0x9018_3;
    uint32_t var_115;
    uint64_t var_116;
    uint64_t var_117;
    uint64_t var_118;
    uint64_t state_0x9018_2;
    uint32_t _pre393;
    uint32_t state_0x9010_2;
    uint64_t state_0x82d8_2;
    uint32_t state_0x9080_2;
    uint32_t state_0x8248_2;
    uint64_t var_138;
    uint64_t var_139;
    uint64_t var_140;
    uint64_t state_0x9018_4;
    uint64_t state_0x82d8_3;
    uint32_t state_0x9080_3;
    uint32_t state_0x8248_3;
    uint64_t var_141;
    uint64_t var_142;
    uint32_t var_148;
    uint64_t var_149;
    uint64_t storemerge29;
    uint64_t var_150;
    uint64_t var_151;
    uint32_t var_152;
    uint32_t var_153;
    uint64_t var_154;
    bool var_155;
    uint32_t var_156;
    uint64_t var_157;
    uint64_t var_158;
    uint64_t var_159;
    uint64_t var_160;
    uint64_t var_161;
    uint64_t var_162;
    uint64_t _pre394;
    uint64_t var_165;
    uint64_t var_166;
    uint64_t *var_206;
    uint64_t var_207;
    uint64_t var_208;
    uint64_t *var_209;
    uint64_t *var_210;
    uint32_t *var_211;
    uint32_t *var_212;
    uint64_t var_213;
    uint64_t var_214;
    uint32_t storemerge;
    uint64_t var_215;
    uint32_t rax_0;
    uint64_t var_216;
    uint64_t var_217;
    uint64_t var_218;
    uint32_t storemerge28;
    uint32_t var_219;
    uint64_t var_220;
    uint64_t var_221;
    uint64_t var_222;
    uint64_t var_223;
    uint64_t var_224;
    uint64_t *var_225;
    uint64_t *var_226;
    uint64_t *var_227;
    uint64_t var_228;
    uint64_t var_229;
    uint64_t var_230;
    uint64_t var_233;
    uint32_t *_cast7;
    uint32_t var_231;
    uint64_t var_232;
    uint64_t var_234;
    uint32_t var_143;
    uint64_t var_144;
    uint64_t var_145;
    uint64_t var_146;
    uint64_t var_147;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = init_rbx();
    var_4 = init_state_0x9018();
    var_5 = init_state_0x9010();
    var_6 = init_state_0x8408();
    var_7 = init_state_0x8328();
    var_8 = init_state_0x82d8();
    var_9 = init_state_0x9080();
    var_10 = init_state_0x8248();
    var_11 = var_0 + (-8L);
    *(uint64_t *)var_11 = var_1;
    var_12 = (uint64_t *)(var_0 + (-392L));
    *var_12 = rdi;
    var_13 = (uint64_t *)(var_0 + (-384L));
    *var_13 = rsi;
    var_14 = (uint64_t *)(var_0 + (-408L));
    *var_14 = rdx;
    var_15 = (uint64_t *)(var_0 + (-400L));
    *var_15 = rcx;
    var_16 = var_0 + (-416L);
    var_17 = (uint64_t *)var_16;
    *var_17 = r8;
    var_18 = *var_13;
    var_19 = (uint64_t *)(var_0 + (-272L));
    *var_19 = var_18;
    var_20 = *var_12;
    var_21 = (uint64_t *)(var_0 + (-16L));
    *var_21 = var_20;
    var_22 = *var_15;
    var_23 = var_0 + (-24L);
    var_24 = (uint64_t *)var_23;
    *var_24 = var_22;
    var_25 = *var_14;
    var_26 = (uint64_t *)(var_0 + (-32L));
    *var_26 = var_25;
    var_27 = (uint64_t *)(var_0 + (-40L));
    *var_27 = 0UL;
    var_28 = (*var_21 << 2UL) + 8UL;
    var_29 = var_0 + (-432L);
    *(uint64_t *)var_29 = 4228402UL;
    var_30 = indirect_placeholder_75(var_28);
    var_31 = var_30.field_0;
    var_32 = var_30.field_1;
    var_33 = (uint64_t *)(var_0 + (-280L));
    *var_33 = var_31;
    rax_1 = 0UL;
    rdi4_0 = var_32;
    local_sp_1 = var_29;
    rcx1_0 = rcx;
    state_0x9018_0 = var_4;
    state_0x9010_0 = var_5;
    state_0x82d8_0 = var_8;
    state_0x9080_0 = var_9;
    state_0x8248_0 = var_10;
    state_0x9018_1 = var_4;
    state_0x9010_1 = var_5;
    state_0x82d8_1 = var_8;
    state_0x9080_1 = var_9;
    state_0x8248_1 = var_10;
    storemerge = 0U;
    rax_0 = 0U;
    storemerge28 = 0U;
    var_228 = 0UL;
    if (var_31 == 0UL) {
        return rax_1;
    }
    var_34 = *var_21;
    while (var_34 != 0UL)
        {
            if (*(uint32_t *)(*var_19 + ((var_34 << 2UL) + (-4L))) == 0U) {
                break;
            }
            var_35 = var_34 + (-1L);
            *var_21 = var_35;
            var_34 = var_35;
        }
    var_36 = *var_26;
    while (1U)
        {
            var_38 = var_36;
            local_sp_2 = local_sp_1;
            if (var_36 == 0UL) {
                var_37 = local_sp_1 + (-8L);
                *(uint64_t *)var_37 = 4228481UL;
                indirect_placeholder_1();
                var_38 = *var_26;
                local_sp_2 = var_37;
            }
            local_sp_1 = local_sp_2;
            local_sp_3 = local_sp_2;
            local_sp_5 = local_sp_2;
            if (*(uint32_t *)(*var_24 + ((var_38 << 2UL) + (-4L))) == 0U) {
                break;
            }
            var_235 = var_38 + (-1L);
            *var_26 = var_235;
            var_36 = var_235;
            continue;
        }
    var_39 = helper_cc_compute_c_wrapper(*var_21 - var_38, var_38, var_2, 17U);
    if (var_39 == 0UL) {
        *(uint64_t *)(var_0 + (-64L)) = *var_33;
        *(uint64_t *)(var_0 + (-72L)) = *var_21;
        var_40 = local_sp_2 + (-8L);
        *(uint64_t *)var_40 = 4228577UL;
        indirect_placeholder_1();
        *(uint64_t *)(var_0 + (-48L)) = (*var_33 + (*var_21 << 2UL));
        *(uint64_t *)(var_0 + (-56L)) = 0UL;
        local_sp_5 = var_40;
    } else {
        var_41 = *var_26;
        if (var_41 != 1UL) {
            var_42 = *(uint32_t *)(*var_24 + ((var_41 << 2UL) + (-4L)));
            var_43 = (uint64_t)var_42;
            *(uint32_t *)(var_0 + (-284L)) = var_42;
            var_44 = helper_clz_wrapper(var_43);
            var_45 = (uint64_t)((long)(((var_42 == 0U) ? var_43 : (var_44 ^ 63UL)) << 32UL) >> (long)32UL) ^ 31UL;
            var_46 = (uint64_t *)(var_0 + (-296L));
            *var_46 = var_45;
            if (var_45 != 0UL) {
                var_47 = *var_26 << 2UL;
                var_48 = local_sp_2 + (-8L);
                *(uint64_t *)var_48 = 4228993UL;
                var_49 = indirect_placeholder_76(var_47);
                var_50 = var_49.field_0;
                var_51 = var_49.field_1;
                *var_27 = var_50;
                local_sp_0 = var_48;
                rdi4_0 = var_51;
                if (var_50 != 0UL) {
                    *(uint64_t *)(local_sp_2 + (-16L)) = 4229019UL;
                    indirect_placeholder_1();
                    return rax_1;
                }
                var_52 = *var_24;
                var_53 = (uint64_t *)(var_0 + (-112L));
                *var_53 = var_52;
                var_54 = *var_27;
                var_55 = (uint64_t *)(var_0 + (-120L));
                *var_55 = var_54;
                var_56 = (uint64_t *)(var_0 + (-128L));
                *var_56 = 0UL;
                var_57 = *var_26;
                var_58 = (uint64_t *)(var_0 + (-136L));
                *var_58 = var_57;
                var_59 = var_57;
                while (var_59 != 0UL)
                    {
                        var_60 = *var_53;
                        *var_53 = (var_60 + 4UL);
                        *var_56 = (*var_56 + ((uint64_t)*(uint32_t *)var_60 << (*var_46 & 63UL)));
                        var_61 = *var_55;
                        *var_55 = (var_61 + 4UL);
                        *(uint32_t *)var_61 = (uint32_t)*var_56;
                        *var_56 = (*var_56 >> 32UL);
                        var_62 = *var_58 + (-1L);
                        *var_58 = var_62;
                        var_59 = var_62;
                    }
                if (*var_56 != 0UL) {
                    var_63 = local_sp_2 + (-16L);
                    *(uint64_t *)var_63 = 4229145UL;
                    indirect_placeholder_1();
                    local_sp_0 = var_63;
                }
                *var_24 = *var_27;
                local_sp_3 = local_sp_0;
            }
            var_64 = *var_33;
            var_65 = (uint64_t *)(var_0 + (-64L));
            *var_65 = var_64;
            rdi4_1 = var_64;
            local_sp_4 = local_sp_3;
            if (*var_46 == 0UL) {
                var_78 = local_sp_3 + (-8L);
                *(uint64_t *)var_78 = 4229208UL;
                indirect_placeholder_1();
                *(uint32_t *)(*var_65 + (*var_21 << 2UL)) = 0U;
                local_sp_4 = var_78;
            } else {
                var_66 = *var_19;
                var_67 = (uint64_t *)(var_0 + (-144L));
                *var_67 = var_66;
                var_68 = *var_65;
                var_69 = (uint64_t *)(var_0 + (-152L));
                *var_69 = var_68;
                var_70 = (uint64_t *)(var_0 + (-160L));
                *var_70 = 0UL;
                var_71 = *var_21;
                var_72 = (uint64_t *)(var_0 + (-168L));
                *var_72 = var_71;
                var_73 = var_71;
                rdi4_1 = rdi4_0;
                while (var_73 != 0UL)
                    {
                        var_74 = *var_67;
                        *var_67 = (var_74 + 4UL);
                        *var_70 = (*var_70 + ((uint64_t)*(uint32_t *)var_74 << (*var_46 & 63UL)));
                        var_75 = *var_69;
                        *var_69 = (var_75 + 4UL);
                        *(uint32_t *)var_75 = (uint32_t)*var_70;
                        *var_70 = (*var_70 >> 32UL);
                        var_76 = *var_72 + (-1L);
                        *var_72 = var_76;
                        var_73 = var_76;
                    }
                var_77 = *var_69;
                *var_69 = (var_77 + 4UL);
                *(uint32_t *)var_77 = (uint32_t)*var_70;
            }
            var_79 = *var_33 + (*var_26 << 2UL);
            var_80 = (uint64_t *)(var_0 + (-48L));
            *var_80 = var_79;
            var_81 = (*var_21 - *var_26) + 1UL;
            var_82 = (uint64_t *)(var_0 + (-56L));
            *var_82 = var_81;
            var_83 = *var_21 - *var_26;
            var_84 = (uint64_t *)(var_0 + (-176L));
            *var_84 = var_83;
            var_85 = *(uint32_t *)(*var_24 + ((*var_26 << 2UL) + (-4L)));
            var_86 = (uint32_t *)(var_0 + (-300L));
            *var_86 = var_85;
            var_87 = *(uint32_t *)(*var_24 + ((*var_26 << 2UL) + (-8L)));
            var_88 = (uint32_t *)(var_0 + (-304L));
            *var_88 = var_87;
            var_89 = ((uint64_t)*var_86 << 32UL) | (uint64_t)var_87;
            var_90 = (uint64_t *)(var_0 + (-312L));
            *var_90 = var_89;
            var_91 = (uint32_t *)(var_0 + (-180L));
            var_92 = (uint32_t *)(var_0 + (-184L));
            var_93 = (uint64_t *)(var_0 + (-320L));
            var_94 = (uint64_t *)(var_0 + (-328L));
            var_95 = (uint64_t *)(var_0 + (-336L));
            var_96 = (uint64_t *)(var_0 + (-192L));
            var_97 = var_0 + (-200L);
            var_98 = (uint64_t *)var_97;
            var_99 = (uint64_t *)(var_0 + (-208L));
            var_100 = (uint64_t *)(var_0 + (-216L));
            var_101 = (uint32_t *)(var_0 + (-340L));
            var_102 = (uint64_t *)(var_0 + (-224L));
            var_103 = var_0 + (-232L);
            var_104 = (uint64_t *)var_103;
            var_105 = (uint32_t *)(var_0 + (-236L));
            var_106 = (uint64_t *)(var_0 + (-248L));
            var_107 = (uint32_t *)(var_0 + (-344L));
            var_108 = (uint32_t *)(var_0 + (-348L));
            var_109 = *var_84;
            local_sp_5 = local_sp_4;
            while (1U)
                {
                    var_110 = (*var_26 + var_109) << 2UL;
                    var_111 = *var_65;
                    var_112 = *(uint32_t *)(var_111 + var_110);
                    state_0x9010_3 = state_0x9010_1;
                    state_0x9018_3 = state_0x9018_1;
                    state_0x9018_2 = state_0x9018_1;
                    state_0x9010_2 = state_0x9010_1;
                    state_0x82d8_2 = state_0x82d8_1;
                    state_0x9080_2 = state_0x9080_1;
                    state_0x8248_2 = state_0x8248_1;
                    state_0x82d8_3 = state_0x82d8_1;
                    state_0x9080_3 = state_0x9080_1;
                    state_0x8248_3 = state_0x8248_1;
                    if (var_112 < *var_86) {
                        var_119 = (uint64_t)var_112 << 32UL;
                        var_120 = var_119 | (uint64_t)*(uint32_t *)(var_111 + (var_110 + (-4L)));
                        *var_93 = var_120;
                        var_121 = (uint64_t)*var_86;
                        var_122 = helper_divq_EAX_wrapper((struct type_6 *)(0UL), var_121, var_120, var_11, 4229695UL, var_119, 0UL, var_121, rdi4_1, rdi, r10, r8, var_3, state_0x9018_1, state_0x9010_1, var_6, var_7, state_0x82d8_1, state_0x9080_1, state_0x8248_1);
                        var_123 = var_122.field_5;
                        var_124 = var_122.field_6;
                        var_125 = var_122.field_7;
                        var_126 = var_122.field_8;
                        var_127 = var_122.field_9;
                        *var_91 = (uint32_t)var_122.field_0;
                        var_128 = (uint64_t)*var_86;
                        var_129 = helper_divq_EAX_wrapper((struct type_6 *)(0UL), var_128, *var_93, var_11, 4229722UL, var_128, 0UL, var_121, rdi4_1, rdi, r10, r8, var_3, var_123, var_124, var_6, var_7, var_125, var_126, var_127);
                        var_130 = var_129.field_4;
                        var_131 = var_129.field_5;
                        var_132 = var_129.field_6;
                        var_133 = var_129.field_7;
                        var_134 = var_129.field_8;
                        var_135 = var_129.field_9;
                        var_136 = (uint32_t)var_130;
                        *var_92 = var_136;
                        var_137 = var_136;
                        state_0x9018_2 = var_131;
                        state_0x9010_2 = var_132;
                        state_0x82d8_2 = var_133;
                        state_0x9080_2 = var_134;
                        state_0x8248_2 = var_135;
                        *var_94 = (((uint64_t)var_137 << 32UL) | (uint64_t)*(uint32_t *)(*var_65 + (((*var_26 + *var_84) << 2UL) + (-8L))));
                        var_138 = (uint64_t)*var_88 * (uint64_t)*var_91;
                        *var_95 = var_138;
                        state_0x9010_3 = state_0x9010_2;
                        state_0x9010_4 = state_0x9010_2;
                        state_0x82d8_4 = state_0x82d8_2;
                        state_0x9080_4 = state_0x9080_2;
                        state_0x8248_4 = state_0x8248_2;
                        state_0x9018_3 = state_0x9018_2;
                        state_0x9018_4 = state_0x9018_2;
                        state_0x82d8_3 = state_0x82d8_2;
                        state_0x9080_3 = state_0x9080_2;
                        state_0x8248_3 = state_0x8248_2;
                        *var_91 = (*var_91 + (-1));
                        var_139 = *var_95 - *var_94;
                        var_140 = helper_cc_compute_c_wrapper(*var_90 - var_139, var_139, var_2, 17U);
                        if (var_138 <= *var_94 & var_140 == 0UL) {
                            *var_91 = (*var_91 + (-1));
                        }
                        if (*var_91 != 0U) {
                            *(uint32_t *)((*var_84 << 2UL) + *var_80) = *var_91;
                            var_160 = *var_84;
                            state_0x9018_1 = state_0x9018_4;
                            state_0x9010_1 = state_0x9010_4;
                            state_0x82d8_1 = state_0x82d8_4;
                            state_0x9080_1 = state_0x9080_4;
                            state_0x8248_1 = state_0x8248_4;
                            if (var_160 == 0UL) {
                                break;
                            }
                            var_161 = var_160 + (-1L);
                            *var_84 = var_161;
                            var_109 = var_161;
                            continue;
                        }
                    }
                    *var_91 = 4294967295U;
                    var_113 = (uint64_t)*(uint32_t *)(*var_65 + ((*var_26 + *var_84) << 2UL));
                    var_114 = helper_cc_compute_c_wrapper((uint64_t)*var_86 - var_113, var_113, var_2, 16U);
                    if (var_114 != 0UL) {
                        var_115 = *(uint32_t *)(*var_65 + (((*var_26 + *var_84) << 2UL) + (-4L))) + *var_86;
                        *var_92 = var_115;
                        var_116 = (uint64_t)var_115;
                        var_117 = (uint64_t)*var_86;
                        var_118 = helper_cc_compute_c_wrapper(var_116 - var_117, var_117, var_2, 16U);
                        if (var_118 != 0UL) {
                            _pre393 = *var_92;
                            var_137 = _pre393;
                            *var_94 = (((uint64_t)var_137 << 32UL) | (uint64_t)*(uint32_t *)(*var_65 + (((*var_26 + *var_84) << 2UL) + (-8L))));
                            var_138 = (uint64_t)*var_88 * (uint64_t)*var_91;
                            *var_95 = var_138;
                            state_0x9010_3 = state_0x9010_2;
                            state_0x9010_4 = state_0x9010_2;
                            state_0x82d8_4 = state_0x82d8_2;
                            state_0x9080_4 = state_0x9080_2;
                            state_0x8248_4 = state_0x8248_2;
                            state_0x9018_3 = state_0x9018_2;
                            state_0x9018_4 = state_0x9018_2;
                            state_0x82d8_3 = state_0x82d8_2;
                            state_0x9080_3 = state_0x9080_2;
                            state_0x8248_3 = state_0x8248_2;
                            *var_91 = (*var_91 + (-1));
                            var_139 = *var_95 - *var_94;
                            var_140 = helper_cc_compute_c_wrapper(*var_90 - var_139, var_139, var_2, 17U);
                            if (var_138 <= *var_94 & var_140 == 0UL) {
                                *var_91 = (*var_91 + (-1));
                            }
                            if (*var_91 != 0U) {
                                *(uint32_t *)((*var_84 << 2UL) + *var_80) = *var_91;
                                var_160 = *var_84;
                                state_0x9018_1 = state_0x9018_4;
                                state_0x9010_1 = state_0x9010_4;
                                state_0x82d8_1 = state_0x82d8_4;
                                state_0x9080_1 = state_0x9080_4;
                                state_0x8248_1 = state_0x8248_4;
                                if (var_160 == 0UL) {
                                    break;
                                }
                                var_161 = var_160 + (-1L);
                                *var_84 = var_161;
                                var_109 = var_161;
                                continue;
                            }
                        }
                    }
                    *var_96 = *var_24;
                    *var_98 = (*var_65 + (*var_84 << 2UL));
                    *var_99 = 0UL;
                    var_141 = *var_26;
                    *var_100 = var_141;
                    state_0x9010_4 = state_0x9010_3;
                    state_0x82d8_4 = state_0x82d8_3;
                    state_0x9080_4 = state_0x9080_3;
                    state_0x8248_4 = state_0x8248_3;
                    state_0x9018_4 = state_0x9018_3;
                    var_142 = var_141;
                    while (var_142 != 0UL)
                        {
                            var_143 = *var_91;
                            var_144 = *var_96;
                            *var_96 = (var_144 + 4UL);
                            var_145 = (*var_99 + ((uint64_t)*(uint32_t *)var_144 * (uint64_t)var_143)) + (uint64_t)(**(uint32_t **)var_97 ^ (-1));
                            *var_99 = var_145;
                            var_146 = *var_98;
                            *var_98 = (var_146 + 4UL);
                            *(uint32_t *)var_146 = ((uint32_t)var_145 ^ (-1));
                            *var_99 = (*var_99 >> 32UL);
                            var_147 = *var_100 + (-1L);
                            *var_100 = var_147;
                            var_142 = var_147;
                        }
                    var_148 = (uint32_t)*var_99;
                    *var_101 = var_148;
                    if (*(uint32_t *)(*var_65 + ((*var_26 + *var_84) << 2UL)) >= var_148) {
                        *var_91 = (*var_91 + (-1));
                        *var_102 = *var_24;
                        *var_104 = (*var_65 + (*var_84 << 2UL));
                        *var_105 = 0U;
                        var_149 = *var_26;
                        *var_106 = var_149;
                        var_150 = var_149;
                        while (var_150 != 0UL)
                            {
                                var_151 = *var_102;
                                *var_102 = (var_151 + 4UL);
                                *var_107 = *(uint32_t *)var_151;
                                var_152 = **(uint32_t **)var_103;
                                *var_108 = var_152;
                                var_153 = var_152 + *var_107;
                                var_154 = *var_104;
                                *var_104 = (var_154 + 4UL);
                                *(uint32_t *)var_154 = (var_153 + *var_105);
                                var_155 = (*var_105 == 0U);
                                var_156 = *var_108 ^ (-1);
                                if (var_155) {
                                    storemerge29 = (*var_107 > var_156);
                                } else {
                                    var_157 = (uint64_t)var_156;
                                    var_158 = helper_cc_compute_c_wrapper((uint64_t)*var_107 - var_157, var_157, var_2, 16U);
                                    storemerge29 = (uint64_t)(unsigned char)var_158 ^ 1UL;
                                }
                                *var_105 = (uint32_t)storemerge29;
                                var_159 = *var_106 + (-1L);
                                *var_106 = var_159;
                                var_150 = var_159;
                            }
                    }
                    *(uint32_t *)((*var_84 << 2UL) + *var_80) = *var_91;
                    var_160 = *var_84;
                    state_0x9018_1 = state_0x9018_4;
                    state_0x9010_1 = state_0x9010_4;
                    state_0x82d8_1 = state_0x82d8_4;
                    state_0x9080_1 = state_0x9080_4;
                    state_0x8248_1 = state_0x8248_4;
                    if (var_160 == 0UL) {
                        break;
                    }
                    var_161 = var_160 + (-1L);
                    *var_84 = var_161;
                    var_109 = var_161;
                    continue;
                }
            var_162 = *var_26;
            var_163 = (uint64_t *)(var_0 + (-72L));
            *var_163 = var_162;
            var_164 = *var_82;
            _pre394 = var_162;
            if (*(uint32_t *)(*var_80 + ((var_164 << 2UL) + (-4L))) == 0U) {
                *var_82 = (var_164 + (-1L));
                _pre394 = *var_163;
            }
            var_165 = _pre394;
            while (var_165 != 0UL)
                {
                    if (*(uint32_t *)(*var_65 + ((var_165 << 2UL) + (-4L))) == 0U) {
                        break;
                    }
                    var_166 = var_165 + (-1L);
                    *var_163 = var_166;
                    var_165 = var_166;
                }
        }
        var_167 = *var_33;
        var_168 = var_0 + (-64L);
        *(uint64_t *)var_168 = var_167;
        var_169 = *var_33 + 4UL;
        var_170 = (uint64_t *)(var_0 + (-48L));
        *var_170 = var_169;
        var_171 = **(uint32_t **)var_23;
        var_172 = (uint32_t *)(var_0 + (-352L));
        *var_172 = var_171;
        var_173 = (uint32_t *)(var_0 + (-76L));
        *var_173 = 0U;
        var_174 = *var_19 + (*var_21 << 2UL);
        var_175 = var_0 + (-88L);
        var_176 = (uint64_t *)var_175;
        *var_176 = var_174;
        var_177 = *var_170 + (*var_21 << 2UL);
        var_178 = var_0 + (-96L);
        var_179 = (uint64_t *)var_178;
        *var_179 = var_177;
        var_180 = *var_21;
        var_181 = (uint64_t *)(var_0 + (-104L));
        *var_181 = var_180;
        var_182 = (uint64_t *)(var_0 + (-360L));
        var_183 = var_180;
        var_184 = (var_183 == 0UL);
        var_185 = *var_173;
        while (!var_184)
            {
                var_186 = (uint64_t)var_185 << 32UL;
                *var_176 = (*var_176 + (-4L));
                var_187 = var_186 | (uint64_t)**(uint32_t **)var_175;
                *var_182 = var_187;
                var_188 = (uint64_t)*var_172;
                var_189 = helper_divq_EAX_wrapper((struct type_6 *)(0UL), var_188, var_187, var_11, 4228782UL, rcx1_0, 0UL, var_188, var_32, rdi, r10, r8, var_3, state_0x9018_0, state_0x9010_0, var_6, var_7, state_0x82d8_0, state_0x9080_0, state_0x8248_0);
                var_190 = var_189.field_0;
                var_191 = var_189.field_5;
                var_192 = var_189.field_6;
                var_193 = var_189.field_7;
                var_194 = var_189.field_8;
                var_195 = var_189.field_9;
                *var_179 = (*var_179 + (-4L));
                **(uint32_t **)var_178 = (uint32_t)var_190;
                var_196 = (uint64_t)*var_172;
                var_197 = helper_divq_EAX_wrapper((struct type_6 *)(0UL), var_196, *var_182, var_11, 4228816UL, var_196, 0UL, var_188, var_32, rdi, r10, r8, var_3, var_191, var_192, var_6, var_7, var_193, var_194, var_195);
                var_198 = var_197.field_5;
                var_199 = var_197.field_6;
                var_200 = var_197.field_7;
                var_201 = var_197.field_8;
                var_202 = var_197.field_9;
                *var_173 = (uint32_t)var_197.field_4;
                var_203 = *var_181 + (-1L);
                *var_181 = var_203;
                var_183 = var_203;
                rcx1_0 = var_196;
                state_0x9018_0 = var_198;
                state_0x9010_0 = var_199;
                state_0x82d8_0 = var_200;
                state_0x9080_0 = var_201;
                state_0x8248_0 = var_202;
                var_184 = (var_183 == 0UL);
                var_185 = *var_173;
            }
        if (var_185 == 0U) {
            *(uint64_t *)(var_0 + (-72L)) = 0UL;
        } else {
            **(uint32_t **)var_168 = var_185;
            *(uint64_t *)(var_0 + (-72L)) = 1UL;
        }
        var_204 = *var_21;
        var_205 = (uint64_t *)(var_0 + (-56L));
        *var_205 = var_204;
        if (*(uint32_t *)(*var_170 + ((var_204 << 2UL) + (-4L))) == 0U) {
            *var_205 = (var_204 + (-1L));
        }
    }
    var_206 = (uint64_t *)(var_0 + (-72L));
    var_207 = *var_206;
    var_208 = *var_26;
    var_213 = var_208;
    if (var_207 <= var_208) {
        var_209 = (uint64_t *)(var_0 + (-256L));
        *var_209 = var_208;
        var_210 = (uint64_t *)(var_0 + (-64L));
        var_211 = (uint32_t *)(var_0 + (-364L));
        var_212 = (uint32_t *)(var_0 + (-368L));
        while (1U)
            {
                var_214 = *var_206;
                if ((var_213 > var_214) || (var_213 == 0UL)) {
                    storemerge = *(uint32_t *)(*var_210 + ((var_213 << 2UL) + (-4L))) >> 31U;
                }
                var_215 = helper_cc_compute_c_wrapper(var_213 - var_214, var_214, var_2, 17U);
                if (var_215 == 0UL) {
                    rax_0 = *(uint32_t *)(*var_210 + (*var_209 << 2UL)) << 1U;
                }
                *var_211 = (rax_0 | storemerge);
                var_216 = *var_209;
                var_217 = *var_26;
                var_218 = helper_cc_compute_c_wrapper(var_216 - var_217, var_217, var_2, 17U);
                if (var_218 == 0UL) {
                    storemerge28 = *(uint32_t *)(*var_24 + (*var_209 << 2UL));
                }
                *var_212 = storemerge28;
                var_219 = *var_211;
                if (var_219 <= storemerge28) {
                    loop_state_var = 1U;
                    break;
                }
                var_220 = (uint64_t)var_219;
                var_221 = (uint64_t)storemerge28;
                var_222 = helper_cc_compute_c_wrapper(var_220 - var_221, var_221, var_2, 16U);
                if (var_222 != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                var_223 = *var_209;
                if (var_223 == 0UL) {
                    var_224 = var_223 + (-1L);
                    *var_209 = var_224;
                    var_213 = var_224;
                    continue;
                }
                if (*(uint64_t *)(var_0 + (-56L)) != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                if ((**(uint32_t **)(var_0 + (-48L)) & 1U) != 0U) {
                    loop_state_var = 1U;
                    break;
                }
                loop_state_var = 0U;
                break;
            }
        switch (loop_state_var) {
          case 1U:
            {
                break;
            }
            break;
          case 0U:
            {
                if (*var_27 == 0UL) {
                    *(uint64_t *)(local_sp_5 + (-8L)) = 4231014UL;
                    indirect_placeholder_1();
                }
                *(uint64_t *)(*var_17 + 8UL) = *(uint64_t *)(var_0 + (-48L));
                **(uint64_t **)var_16 = *(uint64_t *)(var_0 + (-56L));
                var_234 = *var_33;
                rax_1 = var_234;
                return rax_1;
            }
            break;
        }
    }
    var_225 = (uint64_t *)(var_0 + (-264L));
    *var_225 = 0UL;
    var_226 = (uint64_t *)(var_0 + (-56L));
    var_227 = (uint64_t *)(var_0 + (-48L));
    while (1U)
        {
            var_229 = *var_226;
            var_230 = helper_cc_compute_c_wrapper(var_228 - var_229, var_229, var_2, 17U);
            if (var_230 == 0UL) {
                _cast7 = (uint32_t *)(*var_227 + (*var_225 << 2UL));
                var_231 = *_cast7 + 1U;
                *_cast7 = var_231;
                if (var_231 == 0U) {
                    break;
                }
                var_232 = *var_225 + 1UL;
                *var_225 = var_232;
                var_228 = var_232;
                continue;
            }
            var_233 = *var_226;
            *var_226 = (var_233 + 1UL);
            *(uint32_t *)(*var_227 + (var_233 << 2UL)) = 1U;
            break;
        }
    if (*var_27 != 0UL) {
        *(uint64_t *)(local_sp_5 + (-8L)) = 4231014UL;
        indirect_placeholder_1();
    }
    *(uint64_t *)(*var_17 + 8UL) = *(uint64_t *)(var_0 + (-48L));
    **(uint64_t **)var_16 = *(uint64_t *)(var_0 + (-56L));
    var_234 = *var_33;
    rax_1 = var_234;
}
