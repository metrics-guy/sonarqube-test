typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_59_ret_type;
struct indirect_placeholder_60_ret_type;
struct indirect_placeholder_61_ret_type;
struct indirect_placeholder_58_ret_type;
struct indirect_placeholder_62_ret_type;
struct indirect_placeholder_57_ret_type;
struct indirect_placeholder_63_ret_type;
struct indirect_placeholder_59_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_60_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_61_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_58_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_62_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_57_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_63_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_1(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_56(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t indirect_placeholder_20(void);
extern uint64_t indirect_placeholder_13(uint64_t param_0);
extern void indirect_placeholder_3(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct indirect_placeholder_59_ret_type indirect_placeholder_59(uint64_t param_0);
extern struct indirect_placeholder_60_ret_type indirect_placeholder_60(uint64_t param_0);
extern struct indirect_placeholder_61_ret_type indirect_placeholder_61(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_58_ret_type indirect_placeholder_58(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_62_ret_type indirect_placeholder_62(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_57_ret_type indirect_placeholder_57(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_63_ret_type indirect_placeholder_63(uint64_t param_0);
uint64_t bb_binary_operator(uint64_t rcx, uint64_t rdi, uint64_t r9, uint64_t r8) {
    uint64_t var_44;
    uint64_t var_70;
    uint64_t r84_1;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    unsigned char *var_4;
    unsigned char var_5;
    uint64_t local_sp_2;
    uint64_t rcx1_1;
    uint64_t var_98;
    bool var_99;
    unsigned char *var_100;
    uint64_t var_101;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_45;
    uint64_t var_95;
    uint64_t var_93;
    uint64_t var_94;
    uint32_t var_8;
    uint64_t var_96;
    uint64_t rax_0;
    uint64_t local_sp_0;
    uint64_t rax_1;
    uint64_t var_89;
    uint32_t *var_90;
    bool var_91;
    unsigned char *var_92;
    uint64_t local_sp_1;
    uint64_t rax_2;
    bool var_79;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t rcx1_0;
    uint64_t var_14;
    struct indirect_placeholder_59_ret_type var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t local_sp_4;
    uint64_t var_6;
    struct indirect_placeholder_60_ret_type var_7;
    uint64_t r93_0;
    uint64_t r84_0;
    uint32_t *var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t local_sp_3;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t r93_1;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t *_cast4_pre_phi;
    uint64_t *_pre;
    uint64_t var_27;
    unsigned char var_28;
    bool var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t *var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint32_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    struct indirect_placeholder_61_ret_type var_32;
    uint64_t var_33;
    unsigned char *var_34;
    uint64_t var_35;
    uint64_t var_36;
    struct indirect_placeholder_58_ret_type var_37;
    unsigned char *var_38;
    unsigned char var_39;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_56;
    uint64_t var_57;
    struct indirect_placeholder_62_ret_type var_58;
    uint64_t var_59;
    unsigned char *var_60;
    uint64_t var_61;
    uint64_t var_62;
    struct indirect_placeholder_57_ret_type var_63;
    unsigned char *var_64;
    unsigned char var_65;
    struct indirect_placeholder_63_ret_type var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t *_cast7;
    uint64_t var_22;
    uint64_t var_23;
    bool var_24;
    unsigned char *var_25;
    uint64_t var_26;
    uint64_t var_97;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = var_0 + (-8L);
    *(uint64_t *)var_2 = var_1;
    var_3 = var_0 + (-488L);
    var_4 = (unsigned char *)(var_0 + (-476L));
    var_5 = (unsigned char)rdi;
    *var_4 = var_5;
    local_sp_2 = var_3;
    rax_0 = 1UL;
    rcx1_0 = rcx;
    r93_0 = r9;
    r84_0 = r8;
    if (var_5 == '\x00') {
        var_6 = var_0 + (-496L);
        *(uint64_t *)var_6 = 4206565UL;
        var_7 = indirect_placeholder_60(0UL);
        local_sp_2 = var_6;
        rcx1_0 = var_7.field_0;
        r93_0 = var_7.field_1;
        r84_0 = var_7.field_2;
    }
    var_8 = *(uint32_t *)4297552UL + 1U;
    var_9 = (uint32_t *)(var_0 + (-16L));
    *var_9 = var_8;
    var_10 = (uint64_t)*(uint32_t *)4297556UL;
    var_11 = (uint64_t)var_8;
    local_sp_3 = local_sp_2;
    rcx1_1 = rcx1_0;
    r93_1 = r93_0;
    r84_1 = r84_0;
    if ((long)(uint64_t)((long)((var_10 << 32UL) + (-8589934592L)) >> (long)32UL) > (long)var_11) {
        *(unsigned char *)(var_0 + (-9L)) = (unsigned char)'\x00';
        local_sp_4 = local_sp_3;
    } else {
        *(unsigned char *)(var_0 + (-9L)) = (unsigned char)'\x01';
        var_14 = local_sp_2 + (-16L);
        *(uint64_t *)var_14 = 4206649UL;
        var_15 = indirect_placeholder_59(0UL);
        var_16 = var_15.field_0;
        var_17 = var_15.field_1;
        var_18 = var_15.field_2;
        local_sp_4 = var_14;
        rcx1_1 = var_16;
        r93_1 = var_17;
        r84_1 = var_18;
    }
    switch (*(unsigned char *)(var_27 + 2UL)) {
      case 't':
      case 'e':
        {
            if (*(unsigned char *)(var_27 + 3UL) != '\x00') {
                var_71 = (*var_4 == '\x00');
                var_72 = *(uint64_t *)(var_19 + (var_20 + (-8L)));
                var_73 = local_sp_4 + (-8L);
                var_74 = (uint64_t *)var_73;
                local_sp_1 = var_73;
                if (var_71) {
                    *var_74 = 4207059UL;
                    indirect_placeholder_1();
                    var_75 = var_0 + (-376L);
                    var_76 = local_sp_4 + (-16L);
                    *(uint64_t *)var_76 = 4207080UL;
                    var_77 = indirect_placeholder(var_75, var_72);
                    local_sp_1 = var_76;
                    rax_2 = var_77;
                } else {
                    *var_74 = 4207117UL;
                    var_78 = indirect_placeholder_13(var_72);
                    rax_2 = var_78;
                }
                *(uint64_t *)(var_0 + (-32L)) = rax_2;
                var_79 = (*(unsigned char *)(var_0 + (-9L)) == '\x00');
                var_80 = *(uint64_t *)4297560UL;
                var_81 = (uint64_t)*var_9 << 3UL;
                if (var_79) {
                    var_82 = *(uint64_t *)(var_80 + (var_81 + 16UL));
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4207162UL;
                    indirect_placeholder_1();
                    var_83 = var_0 + (-408L);
                    var_84 = local_sp_1 + (-16L);
                    *(uint64_t *)var_84 = 4207183UL;
                    var_85 = indirect_placeholder(var_83, var_82);
                    local_sp_0 = var_84;
                    rax_1 = var_85;
                } else {
                    var_86 = *(uint64_t *)(var_80 + (var_81 + 8UL));
                    var_87 = local_sp_1 + (-8L);
                    *(uint64_t *)var_87 = 4207220UL;
                    var_88 = indirect_placeholder_13(var_86);
                    local_sp_0 = var_87;
                    rax_1 = var_88;
                }
                *(uint64_t *)(var_0 + (-40L)) = rax_1;
                *(uint64_t *)(local_sp_0 + (-8L)) = 4207243UL;
                var_89 = indirect_placeholder_20();
                var_90 = (uint32_t *)(var_0 + (-44L));
                *var_90 = (uint32_t)var_89;
                var_91 = (*(unsigned char *)(*(uint64_t *)(*(uint64_t *)4297560UL + ((uint64_t)*var_9 << 3UL)) + 2UL) == 'e');
                var_92 = (unsigned char *)(var_0 + (-45L));
                *var_92 = var_91;
                *(uint32_t *)4297552UL = (*(uint32_t *)4297552UL + 3U);
                switch (*(unsigned char *)(*(uint64_t *)(*(uint64_t *)4297560UL + ((uint64_t)*var_9 << 3UL)) + 1UL)) {
                  case 'g':
                    {
                        var_93 = 0UL - (uint64_t)*var_92;
                        var_94 = (uint64_t)((uint32_t)var_93 & (-256)) | ((long)var_93 < (long)(uint64_t)*var_90);
                        rax_0 = var_94;
                    }
                    break;
                  case 'l':
                    {
                        var_95 = ((long)(uint64_t)*var_92 > (long)(uint64_t)*var_90);
                        rax_0 = var_95;
                    }
                    break;
                  default:
                    {
                        var_96 = ((uint64_t)((uint32_t)(*var_90 != 0U) - (uint32_t)(uint64_t)*var_92) == 0UL);
                        rax_0 = var_96;
                    }
                    break;
                }
                return rax_0;
            }
            var_29 = (uint32_t)(uint64_t)var_28;
            if ((uint64_t)(var_29 + (-111)) != 0UL) {
                if (*(unsigned char *)(var_27 + 2UL) != 't') {
                    if (*(unsigned char *)(var_27 + 3UL) != '\x00') {
                        *(uint32_t *)4297552UL = (*(uint32_t *)4297552UL + 3U);
                        if (*var_4 == '\x00') {
                            *(uint64_t *)(local_sp_4 + (-8L)) = 4208202UL;
                            indirect_placeholder_3(0UL, var_2, rcx1_1, var_20, 0UL, 4276797UL, r93_1, r84_1);
                            abort();
                        }
                        if (*(unsigned char *)(var_0 + (-9L)) != '\x00') {
                            var_56 = *(uint64_t *)(*(uint64_t *)4297560UL + (((uint64_t)*var_9 << 3UL) + (-8L)));
                            var_57 = var_0 + (-456L);
                            *(uint64_t *)(local_sp_4 + (-8L)) = 4208247UL;
                            var_58 = indirect_placeholder_62(var_57, var_56);
                            var_59 = var_58.field_0;
                            var_60 = (unsigned char *)(var_0 + (-46L));
                            *var_60 = (unsigned char)var_59;
                            var_61 = *(uint64_t *)(*(uint64_t *)4297560UL + (((uint64_t)*var_9 << 3UL) + 8UL));
                            var_62 = var_0 + (-472L);
                            *(uint64_t *)(local_sp_4 + (-16L)) = 4208295UL;
                            var_63 = indirect_placeholder_57(var_62, var_61);
                            var_64 = (unsigned char *)(var_0 + (-47L));
                            var_65 = (unsigned char)var_63.field_0;
                            *var_64 = var_65;
                            if (var_65 == '\x00') {
                                rax_0 = 0UL;
                            } else {
                                var_66 = *(uint64_t *)var_62;
                                var_67 = *(uint64_t *)(var_0 + (-464L));
                                var_68 = *(uint64_t *)var_57;
                                var_69 = *(uint64_t *)(var_0 + (-448L));
                                *(uint64_t *)(local_sp_4 + (-24L)) = 4208354UL;
                                var_70 = indirect_placeholder_56(var_67, var_66, var_69, var_68, r93_1);
                                if (*var_60 != '\x01' & (int)(uint32_t)var_70 > (int)4294967295U) {
                                    rax_0 = 0UL;
                                }
                            }
                            return rax_0;
                        }
                    }
                }
                *(uint64_t *)(local_sp_4 + (-8L)) = 4208419UL;
                var_50 = indirect_placeholder_63(var_27);
                var_51 = var_50.field_0;
                var_52 = var_50.field_1;
                var_53 = var_50.field_2;
                var_54 = var_50.field_3;
                var_55 = var_50.field_4;
                *(uint64_t *)(local_sp_4 + (-16L)) = 4208437UL;
                indirect_placeholder_3(0UL, var_2, var_52, var_53, var_51, 4276820UL, var_54, var_55);
                abort();
            }
            rax_0 = 0UL;
            if ((signed char)var_28 > 'o') {
                *(uint64_t *)(local_sp_4 + (-8L)) = 4208419UL;
                var_50 = indirect_placeholder_63(var_27);
                var_51 = var_50.field_0;
                var_52 = var_50.field_1;
                var_53 = var_50.field_2;
                var_54 = var_50.field_3;
                var_55 = var_50.field_4;
                *(uint64_t *)(local_sp_4 + (-16L)) = 4208437UL;
                indirect_placeholder_3(0UL, var_2, var_52, var_53, var_51, 4276820UL, var_54, var_55);
                abort();
            }
            if ((uint64_t)(var_29 + (-101)) != 0UL) {
                if (*(unsigned char *)(var_27 + 2UL) == 'f') {
                    *(uint64_t *)(local_sp_4 + (-8L)) = 4208419UL;
                    var_50 = indirect_placeholder_63(var_27);
                    var_51 = var_50.field_0;
                    var_52 = var_50.field_1;
                    var_53 = var_50.field_2;
                    var_54 = var_50.field_3;
                    var_55 = var_50.field_4;
                    *(uint64_t *)(local_sp_4 + (-16L)) = 4208437UL;
                    indirect_placeholder_3(0UL, var_2, var_52, var_53, var_51, 4276820UL, var_54, var_55);
                    abort();
                }
                if (*(unsigned char *)(var_27 + 3UL) != '\x00') {
                    *(uint32_t *)4297552UL = (*(uint32_t *)4297552UL + 3U);
                    if (*var_4 == '\x00') {
                        *(uint64_t *)(local_sp_4 + (-8L)) = 4207920UL;
                        indirect_placeholder_3(0UL, var_2, rcx1_1, var_20, 0UL, 4276774UL, r93_1, r84_1);
                        abort();
                    }
                    if (*(unsigned char *)(var_0 + (-9L)) != '\x00') {
                        var_46 = *(uint64_t *)(*(uint64_t *)4297560UL + (((uint64_t)*var_9 << 3UL) + (-8L)));
                        *(uint64_t *)(local_sp_4 + (-8L)) = 4207965UL;
                        var_47 = indirect_placeholder_13(var_46);
                        if ((uint64_t)(uint32_t)var_47 == 0UL) {
                        } else {
                            var_48 = *(uint64_t *)(*(uint64_t *)4297560UL + (((uint64_t)*var_9 << 3UL) + 8UL));
                            *(uint64_t *)(local_sp_4 + (-16L)) = 4208014UL;
                            var_49 = indirect_placeholder_13(var_48);
                            if ((uint64_t)(uint32_t)var_49 == 0UL) {
                            } else {
                                if (*(uint64_t *)(var_0 + (-200L)) == *(uint64_t *)(var_0 + (-344L))) {
                                } else {
                                    if (*(uint64_t *)(var_0 + (-192L)) == *(uint64_t *)(var_0 + (-336L))) {
                                    }
                                }
                            }
                        }
                        return rax_0;
                    }
                }
            }
            if ((uint64_t)(var_29 + (-110)) == 0UL) {
                *(uint64_t *)(local_sp_4 + (-8L)) = 4208419UL;
                var_50 = indirect_placeholder_63(var_27);
                var_51 = var_50.field_0;
                var_52 = var_50.field_1;
                var_53 = var_50.field_2;
                var_54 = var_50.field_3;
                var_55 = var_50.field_4;
                *(uint64_t *)(local_sp_4 + (-16L)) = 4208437UL;
                indirect_placeholder_3(0UL, var_2, var_52, var_53, var_51, 4276820UL, var_54, var_55);
                abort();
            }
            if (*(unsigned char *)(var_27 + 2UL) != 't') {
                if (*(unsigned char *)(var_27 + 3UL) != '\x00') {
                    *(uint32_t *)4297552UL = (*(uint32_t *)4297552UL + 3U);
                    if (*var_4 == '\x00') {
                        *(uint64_t *)(local_sp_4 + (-8L)) = 4207618UL;
                        indirect_placeholder_3(0UL, var_2, rcx1_1, var_20, 0UL, 4276751UL, r93_1, r84_1);
                        abort();
                    }
                    if (*(unsigned char *)(var_0 + (-9L)) != '\x00') {
                        var_30 = *(uint64_t *)(*(uint64_t *)4297560UL + (((uint64_t)*var_9 << 3UL) + (-8L)));
                        var_31 = var_0 + (-424L);
                        *(uint64_t *)(local_sp_4 + (-8L)) = 4207663UL;
                        var_32 = indirect_placeholder_61(var_31, var_30);
                        var_33 = var_32.field_0;
                        var_34 = (unsigned char *)(var_0 + (-48L));
                        *var_34 = (unsigned char)var_33;
                        var_35 = *(uint64_t *)(*(uint64_t *)4297560UL + (((uint64_t)*var_9 << 3UL) + 8UL));
                        var_36 = var_0 + (-440L);
                        *(uint64_t *)(local_sp_4 + (-16L)) = 4207711UL;
                        var_37 = indirect_placeholder_58(var_36, var_35);
                        var_38 = (unsigned char *)(var_0 + (-49L));
                        var_39 = (unsigned char)var_37.field_0;
                        *var_38 = var_39;
                        if (*var_34 == '\x00') {
                        } else {
                            var_40 = *(uint64_t *)var_36;
                            var_41 = *(uint64_t *)(var_0 + (-432L));
                            var_42 = *(uint64_t *)var_31;
                            var_43 = *(uint64_t *)(var_0 + (-416L));
                            *(uint64_t *)(local_sp_4 + (-24L)) = 4207770UL;
                            var_44 = indirect_placeholder_56(var_41, var_40, var_43, var_42, r93_1);
                            var_45 = helper_cc_compute_all_wrapper(var_44, 0UL, 0UL, 24U);
                            if (var_39 != '\x01' & (uint64_t)(((unsigned char)(var_45 >> 4UL) ^ (unsigned char)var_45) & '\xc0') == 0UL) {
                            }
                        }
                        return rax_0;
                    }
                }
            }
            *(uint64_t *)(local_sp_4 + (-8L)) = 4208419UL;
            var_50 = indirect_placeholder_63(var_27);
            var_51 = var_50.field_0;
            var_52 = var_50.field_1;
            var_53 = var_50.field_2;
            var_54 = var_50.field_3;
            var_55 = var_50.field_4;
            *(uint64_t *)(local_sp_4 + (-16L)) = 4208437UL;
            indirect_placeholder_3(0UL, var_2, var_52, var_53, var_51, 4276820UL, var_54, var_55);
            abort();
        }
        break;
      default:
        {
            if (var_28 == 'e') {
                if (*(unsigned char *)(var_27 + 2UL) != 'q') {
                    if (*(unsigned char *)(var_27 + 3UL) != '\x00') {
                        var_71 = (*var_4 == '\x00');
                        var_72 = *(uint64_t *)(var_19 + (var_20 + (-8L)));
                        var_73 = local_sp_4 + (-8L);
                        var_74 = (uint64_t *)var_73;
                        local_sp_1 = var_73;
                        if (var_71) {
                            *var_74 = 4207059UL;
                            indirect_placeholder_1();
                            var_75 = var_0 + (-376L);
                            var_76 = local_sp_4 + (-16L);
                            *(uint64_t *)var_76 = 4207080UL;
                            var_77 = indirect_placeholder(var_75, var_72);
                            local_sp_1 = var_76;
                            rax_2 = var_77;
                        } else {
                            *var_74 = 4207117UL;
                            var_78 = indirect_placeholder_13(var_72);
                            rax_2 = var_78;
                        }
                        *(uint64_t *)(var_0 + (-32L)) = rax_2;
                        var_79 = (*(unsigned char *)(var_0 + (-9L)) == '\x00');
                        var_80 = *(uint64_t *)4297560UL;
                        var_81 = (uint64_t)*var_9 << 3UL;
                        if (var_79) {
                            var_82 = *(uint64_t *)(var_80 + (var_81 + 16UL));
                            *(uint64_t *)(local_sp_1 + (-8L)) = 4207162UL;
                            indirect_placeholder_1();
                            var_83 = var_0 + (-408L);
                            var_84 = local_sp_1 + (-16L);
                            *(uint64_t *)var_84 = 4207183UL;
                            var_85 = indirect_placeholder(var_83, var_82);
                            local_sp_0 = var_84;
                            rax_1 = var_85;
                        } else {
                            var_86 = *(uint64_t *)(var_80 + (var_81 + 8UL));
                            var_87 = local_sp_1 + (-8L);
                            *(uint64_t *)var_87 = 4207220UL;
                            var_88 = indirect_placeholder_13(var_86);
                            local_sp_0 = var_87;
                            rax_1 = var_88;
                        }
                        *(uint64_t *)(var_0 + (-40L)) = rax_1;
                        *(uint64_t *)(local_sp_0 + (-8L)) = 4207243UL;
                        var_89 = indirect_placeholder_20();
                        var_90 = (uint32_t *)(var_0 + (-44L));
                        *var_90 = (uint32_t)var_89;
                        var_91 = (*(unsigned char *)(*(uint64_t *)(*(uint64_t *)4297560UL + ((uint64_t)*var_9 << 3UL)) + 2UL) == 'e');
                        var_92 = (unsigned char *)(var_0 + (-45L));
                        *var_92 = var_91;
                        *(uint32_t *)4297552UL = (*(uint32_t *)4297552UL + 3U);
                        switch (*(unsigned char *)(*(uint64_t *)(*(uint64_t *)4297560UL + ((uint64_t)*var_9 << 3UL)) + 1UL)) {
                          case 'g':
                            {
                                var_93 = 0UL - (uint64_t)*var_92;
                                var_94 = (uint64_t)((uint32_t)var_93 & (-256)) | ((long)var_93 < (long)(uint64_t)*var_90);
                                rax_0 = var_94;
                            }
                            break;
                          case 'l':
                            {
                                var_95 = ((long)(uint64_t)*var_92 > (long)(uint64_t)*var_90);
                                rax_0 = var_95;
                            }
                            break;
                          default:
                            {
                                var_96 = ((uint64_t)((uint32_t)(*var_90 != 0U) - (uint32_t)(uint64_t)*var_92) == 0UL);
                                rax_0 = var_96;
                            }
                            break;
                        }
                        return rax_0;
                    }
                }
                if (var_28 != 'n' & *(unsigned char *)(var_27 + 2UL) != 'e' & *(unsigned char *)(var_27 + 3UL) != '\x00') {
                    var_71 = (*var_4 == '\x00');
                    var_72 = *(uint64_t *)(var_19 + (var_20 + (-8L)));
                    var_73 = local_sp_4 + (-8L);
                    var_74 = (uint64_t *)var_73;
                    local_sp_1 = var_73;
                    if (var_71) {
                        *var_74 = 4207117UL;
                        var_78 = indirect_placeholder_13(var_72);
                        rax_2 = var_78;
                    } else {
                        *var_74 = 4207059UL;
                        indirect_placeholder_1();
                        var_75 = var_0 + (-376L);
                        var_76 = local_sp_4 + (-16L);
                        *(uint64_t *)var_76 = 4207080UL;
                        var_77 = indirect_placeholder(var_75, var_72);
                        local_sp_1 = var_76;
                        rax_2 = var_77;
                    }
                    *(uint64_t *)(var_0 + (-32L)) = rax_2;
                    var_79 = (*(unsigned char *)(var_0 + (-9L)) == '\x00');
                    var_80 = *(uint64_t *)4297560UL;
                    var_81 = (uint64_t)*var_9 << 3UL;
                    if (var_79) {
                        var_86 = *(uint64_t *)(var_80 + (var_81 + 8UL));
                        var_87 = local_sp_1 + (-8L);
                        *(uint64_t *)var_87 = 4207220UL;
                        var_88 = indirect_placeholder_13(var_86);
                        local_sp_0 = var_87;
                        rax_1 = var_88;
                    } else {
                        var_82 = *(uint64_t *)(var_80 + (var_81 + 16UL));
                        *(uint64_t *)(local_sp_1 + (-8L)) = 4207162UL;
                        indirect_placeholder_1();
                        var_83 = var_0 + (-408L);
                        var_84 = local_sp_1 + (-16L);
                        *(uint64_t *)var_84 = 4207183UL;
                        var_85 = indirect_placeholder(var_83, var_82);
                        local_sp_0 = var_84;
                        rax_1 = var_85;
                    }
                    *(uint64_t *)(var_0 + (-40L)) = rax_1;
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4207243UL;
                    var_89 = indirect_placeholder_20();
                    var_90 = (uint32_t *)(var_0 + (-44L));
                    *var_90 = (uint32_t)var_89;
                    var_91 = (*(unsigned char *)(*(uint64_t *)(*(uint64_t *)4297560UL + ((uint64_t)*var_9 << 3UL)) + 2UL) == 'e');
                    var_92 = (unsigned char *)(var_0 + (-45L));
                    *var_92 = var_91;
                    *(uint32_t *)4297552UL = (*(uint32_t *)4297552UL + 3U);
                    switch (*(unsigned char *)(*(uint64_t *)(*(uint64_t *)4297560UL + ((uint64_t)*var_9 << 3UL)) + 1UL)) {
                      case 'g':
                        {
                            var_93 = 0UL - (uint64_t)*var_92;
                            var_94 = (uint64_t)((uint32_t)var_93 & (-256)) | ((long)var_93 < (long)(uint64_t)*var_90);
                            rax_0 = var_94;
                        }
                        break;
                      case 'l':
                        {
                            var_95 = ((long)(uint64_t)*var_92 > (long)(uint64_t)*var_90);
                            rax_0 = var_95;
                        }
                        break;
                      default:
                        {
                            var_96 = ((uint64_t)((uint32_t)(*var_90 != 0U) - (uint32_t)(uint64_t)*var_92) == 0UL);
                            rax_0 = var_96;
                        }
                        break;
                    }
                    return rax_0;
                }
            }
            if (var_28 != 'n') {
                var_29 = (uint32_t)(uint64_t)var_28;
                if ((uint64_t)(var_29 + (-111)) != 0UL) {
                    if (*(unsigned char *)(var_27 + 2UL) != 't') {
                        if (*(unsigned char *)(var_27 + 3UL) != '\x00') {
                            *(uint32_t *)4297552UL = (*(uint32_t *)4297552UL + 3U);
                            if (*var_4 == '\x00') {
                                *(uint64_t *)(local_sp_4 + (-8L)) = 4208202UL;
                                indirect_placeholder_3(0UL, var_2, rcx1_1, var_20, 0UL, 4276797UL, r93_1, r84_1);
                                abort();
                            }
                            if (*(unsigned char *)(var_0 + (-9L)) != '\x00') {
                                var_56 = *(uint64_t *)(*(uint64_t *)4297560UL + (((uint64_t)*var_9 << 3UL) + (-8L)));
                                var_57 = var_0 + (-456L);
                                *(uint64_t *)(local_sp_4 + (-8L)) = 4208247UL;
                                var_58 = indirect_placeholder_62(var_57, var_56);
                                var_59 = var_58.field_0;
                                var_60 = (unsigned char *)(var_0 + (-46L));
                                *var_60 = (unsigned char)var_59;
                                var_61 = *(uint64_t *)(*(uint64_t *)4297560UL + (((uint64_t)*var_9 << 3UL) + 8UL));
                                var_62 = var_0 + (-472L);
                                *(uint64_t *)(local_sp_4 + (-16L)) = 4208295UL;
                                var_63 = indirect_placeholder_57(var_62, var_61);
                                var_64 = (unsigned char *)(var_0 + (-47L));
                                var_65 = (unsigned char)var_63.field_0;
                                *var_64 = var_65;
                                if (var_65 == '\x00') {
                                    rax_0 = 0UL;
                                } else {
                                    var_66 = *(uint64_t *)var_62;
                                    var_67 = *(uint64_t *)(var_0 + (-464L));
                                    var_68 = *(uint64_t *)var_57;
                                    var_69 = *(uint64_t *)(var_0 + (-448L));
                                    *(uint64_t *)(local_sp_4 + (-24L)) = 4208354UL;
                                    var_70 = indirect_placeholder_56(var_67, var_66, var_69, var_68, r93_1);
                                    if (*var_60 != '\x01' & (int)(uint32_t)var_70 > (int)4294967295U) {
                                        rax_0 = 0UL;
                                    }
                                }
                                return rax_0;
                            }
                        }
                    }
                    *(uint64_t *)(local_sp_4 + (-8L)) = 4208419UL;
                    var_50 = indirect_placeholder_63(var_27);
                    var_51 = var_50.field_0;
                    var_52 = var_50.field_1;
                    var_53 = var_50.field_2;
                    var_54 = var_50.field_3;
                    var_55 = var_50.field_4;
                    *(uint64_t *)(local_sp_4 + (-16L)) = 4208437UL;
                    indirect_placeholder_3(0UL, var_2, var_52, var_53, var_51, 4276820UL, var_54, var_55);
                    abort();
                }
                rax_0 = 0UL;
                if ((signed char)var_28 > 'o') {
                    *(uint64_t *)(local_sp_4 + (-8L)) = 4208419UL;
                    var_50 = indirect_placeholder_63(var_27);
                    var_51 = var_50.field_0;
                    var_52 = var_50.field_1;
                    var_53 = var_50.field_2;
                    var_54 = var_50.field_3;
                    var_55 = var_50.field_4;
                    *(uint64_t *)(local_sp_4 + (-16L)) = 4208437UL;
                    indirect_placeholder_3(0UL, var_2, var_52, var_53, var_51, 4276820UL, var_54, var_55);
                    abort();
                }
                if ((uint64_t)(var_29 + (-101)) != 0UL) {
                    if (*(unsigned char *)(var_27 + 2UL) == 'f') {
                        *(uint64_t *)(local_sp_4 + (-8L)) = 4208419UL;
                        var_50 = indirect_placeholder_63(var_27);
                        var_51 = var_50.field_0;
                        var_52 = var_50.field_1;
                        var_53 = var_50.field_2;
                        var_54 = var_50.field_3;
                        var_55 = var_50.field_4;
                        *(uint64_t *)(local_sp_4 + (-16L)) = 4208437UL;
                        indirect_placeholder_3(0UL, var_2, var_52, var_53, var_51, 4276820UL, var_54, var_55);
                        abort();
                    }
                    if (*(unsigned char *)(var_27 + 3UL) != '\x00') {
                        *(uint32_t *)4297552UL = (*(uint32_t *)4297552UL + 3U);
                        if (*var_4 == '\x00') {
                            *(uint64_t *)(local_sp_4 + (-8L)) = 4207920UL;
                            indirect_placeholder_3(0UL, var_2, rcx1_1, var_20, 0UL, 4276774UL, r93_1, r84_1);
                            abort();
                        }
                        if (*(unsigned char *)(var_0 + (-9L)) != '\x00') {
                            var_46 = *(uint64_t *)(*(uint64_t *)4297560UL + (((uint64_t)*var_9 << 3UL) + (-8L)));
                            *(uint64_t *)(local_sp_4 + (-8L)) = 4207965UL;
                            var_47 = indirect_placeholder_13(var_46);
                            if ((uint64_t)(uint32_t)var_47 == 0UL) {
                            } else {
                                var_48 = *(uint64_t *)(*(uint64_t *)4297560UL + (((uint64_t)*var_9 << 3UL) + 8UL));
                                *(uint64_t *)(local_sp_4 + (-16L)) = 4208014UL;
                                var_49 = indirect_placeholder_13(var_48);
                                if ((uint64_t)(uint32_t)var_49 == 0UL) {
                                } else {
                                    if (*(uint64_t *)(var_0 + (-200L)) == *(uint64_t *)(var_0 + (-344L))) {
                                    } else {
                                        if (*(uint64_t *)(var_0 + (-192L)) == *(uint64_t *)(var_0 + (-336L))) {
                                        }
                                    }
                                }
                            }
                            return rax_0;
                        }
                    }
                }
                if ((uint64_t)(var_29 + (-110)) == 0UL) {
                    *(uint64_t *)(local_sp_4 + (-8L)) = 4208419UL;
                    var_50 = indirect_placeholder_63(var_27);
                    var_51 = var_50.field_0;
                    var_52 = var_50.field_1;
                    var_53 = var_50.field_2;
                    var_54 = var_50.field_3;
                    var_55 = var_50.field_4;
                    *(uint64_t *)(local_sp_4 + (-16L)) = 4208437UL;
                    indirect_placeholder_3(0UL, var_2, var_52, var_53, var_51, 4276820UL, var_54, var_55);
                    abort();
                }
                if (*(unsigned char *)(var_27 + 2UL) != 't') {
                    if (*(unsigned char *)(var_27 + 3UL) != '\x00') {
                        *(uint32_t *)4297552UL = (*(uint32_t *)4297552UL + 3U);
                        if (*var_4 == '\x00') {
                            *(uint64_t *)(local_sp_4 + (-8L)) = 4207618UL;
                            indirect_placeholder_3(0UL, var_2, rcx1_1, var_20, 0UL, 4276751UL, r93_1, r84_1);
                            abort();
                        }
                        if (*(unsigned char *)(var_0 + (-9L)) != '\x00') {
                            var_30 = *(uint64_t *)(*(uint64_t *)4297560UL + (((uint64_t)*var_9 << 3UL) + (-8L)));
                            var_31 = var_0 + (-424L);
                            *(uint64_t *)(local_sp_4 + (-8L)) = 4207663UL;
                            var_32 = indirect_placeholder_61(var_31, var_30);
                            var_33 = var_32.field_0;
                            var_34 = (unsigned char *)(var_0 + (-48L));
                            *var_34 = (unsigned char)var_33;
                            var_35 = *(uint64_t *)(*(uint64_t *)4297560UL + (((uint64_t)*var_9 << 3UL) + 8UL));
                            var_36 = var_0 + (-440L);
                            *(uint64_t *)(local_sp_4 + (-16L)) = 4207711UL;
                            var_37 = indirect_placeholder_58(var_36, var_35);
                            var_38 = (unsigned char *)(var_0 + (-49L));
                            var_39 = (unsigned char)var_37.field_0;
                            *var_38 = var_39;
                            if (*var_34 == '\x00') {
                            } else {
                                var_40 = *(uint64_t *)var_36;
                                var_41 = *(uint64_t *)(var_0 + (-432L));
                                var_42 = *(uint64_t *)var_31;
                                var_43 = *(uint64_t *)(var_0 + (-416L));
                                *(uint64_t *)(local_sp_4 + (-24L)) = 4207770UL;
                                var_44 = indirect_placeholder_56(var_41, var_40, var_43, var_42, r93_1);
                                var_45 = helper_cc_compute_all_wrapper(var_44, 0UL, 0UL, 24U);
                                if (var_39 != '\x01' & (uint64_t)(((unsigned char)(var_45 >> 4UL) ^ (unsigned char)var_45) & '\xc0') == 0UL) {
                                }
                            }
                            return rax_0;
                        }
                    }
                }
                *(uint64_t *)(local_sp_4 + (-8L)) = 4208419UL;
                var_50 = indirect_placeholder_63(var_27);
                var_51 = var_50.field_0;
                var_52 = var_50.field_1;
                var_53 = var_50.field_2;
                var_54 = var_50.field_3;
                var_55 = var_50.field_4;
                *(uint64_t *)(local_sp_4 + (-16L)) = 4208437UL;
                indirect_placeholder_3(0UL, var_2, var_52, var_53, var_51, 4276820UL, var_54, var_55);
                abort();
            }
            if (*(unsigned char *)(var_27 + 2UL) == 'e') {
                var_29 = (uint32_t)(uint64_t)var_28;
                if ((uint64_t)(var_29 + (-111)) == 0UL) {
                    if (*(unsigned char *)(var_27 + 2UL) != 't' & *(unsigned char *)(var_27 + 3UL) != '\x00') {
                        *(uint32_t *)4297552UL = (*(uint32_t *)4297552UL + 3U);
                        if (*var_4 == '\x00') {
                            *(uint64_t *)(local_sp_4 + (-8L)) = 4208202UL;
                            indirect_placeholder_3(0UL, var_2, rcx1_1, var_20, 0UL, 4276797UL, r93_1, r84_1);
                            abort();
                        }
                        if (*(unsigned char *)(var_0 + (-9L)) != '\x00') {
                            var_56 = *(uint64_t *)(*(uint64_t *)4297560UL + (((uint64_t)*var_9 << 3UL) + (-8L)));
                            var_57 = var_0 + (-456L);
                            *(uint64_t *)(local_sp_4 + (-8L)) = 4208247UL;
                            var_58 = indirect_placeholder_62(var_57, var_56);
                            var_59 = var_58.field_0;
                            var_60 = (unsigned char *)(var_0 + (-46L));
                            *var_60 = (unsigned char)var_59;
                            var_61 = *(uint64_t *)(*(uint64_t *)4297560UL + (((uint64_t)*var_9 << 3UL) + 8UL));
                            var_62 = var_0 + (-472L);
                            *(uint64_t *)(local_sp_4 + (-16L)) = 4208295UL;
                            var_63 = indirect_placeholder_57(var_62, var_61);
                            var_64 = (unsigned char *)(var_0 + (-47L));
                            var_65 = (unsigned char)var_63.field_0;
                            *var_64 = var_65;
                            if (var_65 == '\x00') {
                                rax_0 = 0UL;
                            } else {
                                var_66 = *(uint64_t *)var_62;
                                var_67 = *(uint64_t *)(var_0 + (-464L));
                                var_68 = *(uint64_t *)var_57;
                                var_69 = *(uint64_t *)(var_0 + (-448L));
                                *(uint64_t *)(local_sp_4 + (-24L)) = 4208354UL;
                                var_70 = indirect_placeholder_56(var_67, var_66, var_69, var_68, r93_1);
                                if (*var_60 != '\x01' & (int)(uint32_t)var_70 > (int)4294967295U) {
                                    rax_0 = 0UL;
                                }
                            }
                            return rax_0;
                        }
                    }
                }
                rax_0 = 0UL;
                if ((signed char)var_28 > 'o') {
                    *(uint64_t *)(local_sp_4 + (-8L)) = 4208419UL;
                    var_50 = indirect_placeholder_63(var_27);
                    var_51 = var_50.field_0;
                    var_52 = var_50.field_1;
                    var_53 = var_50.field_2;
                    var_54 = var_50.field_3;
                    var_55 = var_50.field_4;
                    *(uint64_t *)(local_sp_4 + (-16L)) = 4208437UL;
                    indirect_placeholder_3(0UL, var_2, var_52, var_53, var_51, 4276820UL, var_54, var_55);
                    abort();
                }
                if ((uint64_t)(var_29 + (-101)) != 0UL) {
                    if ((uint64_t)(var_29 + (-110)) != 0UL & *(unsigned char *)(var_27 + 2UL) != 't' & *(unsigned char *)(var_27 + 3UL) != '\x00') {
                        *(uint32_t *)4297552UL = (*(uint32_t *)4297552UL + 3U);
                        if (*var_4 == '\x00') {
                            *(uint64_t *)(local_sp_4 + (-8L)) = 4207618UL;
                            indirect_placeholder_3(0UL, var_2, rcx1_1, var_20, 0UL, 4276751UL, r93_1, r84_1);
                            abort();
                        }
                        if (*(unsigned char *)(var_0 + (-9L)) != '\x00') {
                            var_30 = *(uint64_t *)(*(uint64_t *)4297560UL + (((uint64_t)*var_9 << 3UL) + (-8L)));
                            var_31 = var_0 + (-424L);
                            *(uint64_t *)(local_sp_4 + (-8L)) = 4207663UL;
                            var_32 = indirect_placeholder_61(var_31, var_30);
                            var_33 = var_32.field_0;
                            var_34 = (unsigned char *)(var_0 + (-48L));
                            *var_34 = (unsigned char)var_33;
                            var_35 = *(uint64_t *)(*(uint64_t *)4297560UL + (((uint64_t)*var_9 << 3UL) + 8UL));
                            var_36 = var_0 + (-440L);
                            *(uint64_t *)(local_sp_4 + (-16L)) = 4207711UL;
                            var_37 = indirect_placeholder_58(var_36, var_35);
                            var_38 = (unsigned char *)(var_0 + (-49L));
                            var_39 = (unsigned char)var_37.field_0;
                            *var_38 = var_39;
                            if (*var_34 == '\x00') {
                            } else {
                                var_40 = *(uint64_t *)var_36;
                                var_41 = *(uint64_t *)(var_0 + (-432L));
                                var_42 = *(uint64_t *)var_31;
                                var_43 = *(uint64_t *)(var_0 + (-416L));
                                *(uint64_t *)(local_sp_4 + (-24L)) = 4207770UL;
                                var_44 = indirect_placeholder_56(var_41, var_40, var_43, var_42, r93_1);
                                var_45 = helper_cc_compute_all_wrapper(var_44, 0UL, 0UL, 24U);
                                if (var_39 != '\x01' & (uint64_t)(((unsigned char)(var_45 >> 4UL) ^ (unsigned char)var_45) & '\xc0') == 0UL) {
                                }
                            }
                            return rax_0;
                        }
                    }
                }
                if (*(unsigned char *)(var_27 + 2UL) != 'f' & *(unsigned char *)(var_27 + 3UL) != '\x00') {
                    *(uint32_t *)4297552UL = (*(uint32_t *)4297552UL + 3U);
                    if (*var_4 == '\x00') {
                        *(uint64_t *)(local_sp_4 + (-8L)) = 4207920UL;
                        indirect_placeholder_3(0UL, var_2, rcx1_1, var_20, 0UL, 4276774UL, r93_1, r84_1);
                        abort();
                    }
                    if (*(unsigned char *)(var_0 + (-9L)) != '\x00') {
                        var_46 = *(uint64_t *)(*(uint64_t *)4297560UL + (((uint64_t)*var_9 << 3UL) + (-8L)));
                        *(uint64_t *)(local_sp_4 + (-8L)) = 4207965UL;
                        var_47 = indirect_placeholder_13(var_46);
                        if ((uint64_t)(uint32_t)var_47 == 0UL) {
                        } else {
                            var_48 = *(uint64_t *)(*(uint64_t *)4297560UL + (((uint64_t)*var_9 << 3UL) + 8UL));
                            *(uint64_t *)(local_sp_4 + (-16L)) = 4208014UL;
                            var_49 = indirect_placeholder_13(var_48);
                            if ((uint64_t)(uint32_t)var_49 == 0UL) {
                            } else {
                                if (*(uint64_t *)(var_0 + (-200L)) == *(uint64_t *)(var_0 + (-344L))) {
                                } else {
                                    if (*(uint64_t *)(var_0 + (-192L)) == *(uint64_t *)(var_0 + (-336L))) {
                                    }
                                }
                            }
                        }
                        return rax_0;
                    }
                }
            }
            if (*(unsigned char *)(var_27 + 3UL) == '\x00') {
                var_71 = (*var_4 == '\x00');
                var_72 = *(uint64_t *)(var_19 + (var_20 + (-8L)));
                var_73 = local_sp_4 + (-8L);
                var_74 = (uint64_t *)var_73;
                local_sp_1 = var_73;
                if (var_71) {
                    *var_74 = 4207059UL;
                    indirect_placeholder_1();
                    var_75 = var_0 + (-376L);
                    var_76 = local_sp_4 + (-16L);
                    *(uint64_t *)var_76 = 4207080UL;
                    var_77 = indirect_placeholder(var_75, var_72);
                    local_sp_1 = var_76;
                    rax_2 = var_77;
                } else {
                    *var_74 = 4207117UL;
                    var_78 = indirect_placeholder_13(var_72);
                    rax_2 = var_78;
                }
                *(uint64_t *)(var_0 + (-32L)) = rax_2;
                var_79 = (*(unsigned char *)(var_0 + (-9L)) == '\x00');
                var_80 = *(uint64_t *)4297560UL;
                var_81 = (uint64_t)*var_9 << 3UL;
                if (var_79) {
                    var_82 = *(uint64_t *)(var_80 + (var_81 + 16UL));
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4207162UL;
                    indirect_placeholder_1();
                    var_83 = var_0 + (-408L);
                    var_84 = local_sp_1 + (-16L);
                    *(uint64_t *)var_84 = 4207183UL;
                    var_85 = indirect_placeholder(var_83, var_82);
                    local_sp_0 = var_84;
                    rax_1 = var_85;
                } else {
                    var_86 = *(uint64_t *)(var_80 + (var_81 + 8UL));
                    var_87 = local_sp_1 + (-8L);
                    *(uint64_t *)var_87 = 4207220UL;
                    var_88 = indirect_placeholder_13(var_86);
                    local_sp_0 = var_87;
                    rax_1 = var_88;
                }
                *(uint64_t *)(var_0 + (-40L)) = rax_1;
                *(uint64_t *)(local_sp_0 + (-8L)) = 4207243UL;
                var_89 = indirect_placeholder_20();
                var_90 = (uint32_t *)(var_0 + (-44L));
                *var_90 = (uint32_t)var_89;
                var_91 = (*(unsigned char *)(*(uint64_t *)(*(uint64_t *)4297560UL + ((uint64_t)*var_9 << 3UL)) + 2UL) == 'e');
                var_92 = (unsigned char *)(var_0 + (-45L));
                *var_92 = var_91;
                *(uint32_t *)4297552UL = (*(uint32_t *)4297552UL + 3U);
                switch (*(unsigned char *)(*(uint64_t *)(*(uint64_t *)4297560UL + ((uint64_t)*var_9 << 3UL)) + 1UL)) {
                  case 'g':
                    {
                        var_93 = 0UL - (uint64_t)*var_92;
                        var_94 = (uint64_t)((uint32_t)var_93 & (-256)) | ((long)var_93 < (long)(uint64_t)*var_90);
                        rax_0 = var_94;
                    }
                    break;
                  case 'l':
                    {
                        var_95 = ((long)(uint64_t)*var_92 > (long)(uint64_t)*var_90);
                        rax_0 = var_95;
                    }
                    break;
                  default:
                    {
                        var_96 = ((uint64_t)((uint32_t)(*var_90 != 0U) - (uint32_t)(uint64_t)*var_92) == 0UL);
                        rax_0 = var_96;
                    }
                    break;
                }
                return rax_0;
            }
        }
        break;
    }
}
