typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_54_ret_type;
struct indirect_placeholder_53_ret_type;
struct indirect_placeholder_55_ret_type;
struct indirect_placeholder_54_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_53_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_55_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_1(void);
extern uint64_t indirect_placeholder_20(void);
extern struct indirect_placeholder_54_ret_type indirect_placeholder_54(uint64_t param_0);
extern struct indirect_placeholder_53_ret_type indirect_placeholder_53(uint64_t param_0);
extern struct indirect_placeholder_55_ret_type indirect_placeholder_55(uint64_t param_0);
uint64_t bb_posixtest(uint64_t rdi) {
    uint32_t *var_3;
    uint32_t var_4;
    uint64_t var_19;
    unsigned char *var_20;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t local_sp_0;
    uint64_t var_21;
    bool var_22;
    unsigned char *var_23;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t local_sp_1;
    unsigned char *_pre_phi35;
    uint64_t var_5;
    unsigned char *var_6;
    uint64_t var_7;
    unsigned char *var_8;
    uint64_t var_9;
    unsigned char *var_10;
    uint64_t var_16;
    uint64_t var_17;
    unsigned char *var_18;
    uint64_t var_11;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-40L);
    var_3 = (uint32_t *)(var_0 + (-28L));
    var_4 = (uint32_t)rdi;
    *var_3 = var_4;
    local_sp_0 = var_2;
    if (var_4 != 4U) {
        var_11 = *(uint64_t *)(*(uint64_t *)4297560UL + ((uint64_t)*(uint32_t *)4297552UL << 3UL));
        *(uint64_t *)(var_0 + (-48L)) = 4211956UL;
        indirect_placeholder_1();
        if ((uint64_t)(uint32_t)var_11 != 0UL) {
            *(uint64_t *)(var_0 + (-56L)) = 4211970UL;
            indirect_placeholder_55(1UL);
            *(uint64_t *)(var_0 + (-64L)) = 4211975UL;
            var_21 = indirect_placeholder_20();
            var_22 = ((uint64_t)(unsigned char)var_21 == 0UL);
            var_23 = (unsigned char *)(var_0 + (-9L));
            *var_23 = (var_22 & '\x01');
            _pre_phi35 = var_23;
            return (uint64_t)*_pre_phi35;
        }
        var_12 = *(uint64_t *)(*(uint64_t *)4297560UL + ((uint64_t)*(uint32_t *)4297552UL << 3UL));
        var_13 = var_0 + (-56L);
        *(uint64_t *)var_13 = 4212040UL;
        indirect_placeholder_1();
        local_sp_0 = var_13;
        if ((uint64_t)(uint32_t)var_12 == 0UL) {
            local_sp_1 = local_sp_0;
            if ((int)*var_3 > (int)0U) {
                var_16 = local_sp_0 + (-8L);
                *(uint64_t *)var_16 = 4212132UL;
                indirect_placeholder_1();
                local_sp_1 = var_16;
            }
            *(uint64_t *)(local_sp_1 + (-8L)) = 4212137UL;
            var_17 = indirect_placeholder_20();
            var_18 = (unsigned char *)(var_0 + (-9L));
            *var_18 = (unsigned char)var_17;
            _pre_phi35 = var_18;
            return (uint64_t)*_pre_phi35;
        }
        var_14 = *(uint64_t *)(*(uint64_t *)4297560UL + (((uint64_t)*(uint32_t *)4297552UL << 3UL) + 24UL));
        var_15 = var_0 + (-64L);
        *(uint64_t *)var_15 = 4212087UL;
        indirect_placeholder_1();
        local_sp_0 = var_15;
        if ((uint64_t)(uint32_t)var_14 != 0UL) {
            *(uint64_t *)(var_0 + (-72L)) = 4212101UL;
            indirect_placeholder_54(0UL);
            *(uint64_t *)(var_0 + (-80L)) = 4212106UL;
            var_19 = indirect_placeholder_20();
            var_20 = (unsigned char *)(var_0 + (-9L));
            *var_20 = (unsigned char)var_19;
            *(uint64_t *)(var_0 + (-88L)) = 4212119UL;
            indirect_placeholder_53(0UL);
            _pre_phi35 = var_20;
            return (uint64_t)*_pre_phi35;
        }
    }
    if ((int)var_4 <= (int)4U) {
        local_sp_1 = local_sp_0;
        if ((int)*var_3 > (int)0U) {
            var_16 = local_sp_0 + (-8L);
            *(uint64_t *)var_16 = 4212132UL;
            indirect_placeholder_1();
            local_sp_1 = var_16;
        }
        *(uint64_t *)(local_sp_1 + (-8L)) = 4212137UL;
        var_17 = indirect_placeholder_20();
        var_18 = (unsigned char *)(var_0 + (-9L));
        *var_18 = (unsigned char)var_17;
        _pre_phi35 = var_18;
        return (uint64_t)*_pre_phi35;
    }
    if (var_4 != 3U) {
        *(uint64_t *)(var_0 + (-48L)) = 4211909UL;
        var_9 = indirect_placeholder_20();
        var_10 = (unsigned char *)(var_0 + (-9L));
        *var_10 = (unsigned char)var_9;
        _pre_phi35 = var_10;
        return (uint64_t)*_pre_phi35;
    }
    if ((int)var_4 <= (int)3U) {
        local_sp_1 = local_sp_0;
        if ((int)*var_3 > (int)0U) {
            var_16 = local_sp_0 + (-8L);
            *(uint64_t *)var_16 = 4212132UL;
            indirect_placeholder_1();
            local_sp_1 = var_16;
        }
        *(uint64_t *)(local_sp_1 + (-8L)) = 4212137UL;
        var_17 = indirect_placeholder_20();
        var_18 = (unsigned char *)(var_0 + (-9L));
        *var_18 = (unsigned char)var_17;
        _pre_phi35 = var_18;
        return (uint64_t)*_pre_phi35;
    }
    switch (var_4) {
      case 1U:
        {
            *(uint64_t *)(var_0 + (-48L)) = 4211883UL;
            var_7 = indirect_placeholder_20();
            var_8 = (unsigned char *)(var_0 + (-9L));
            *var_8 = (unsigned char)var_7;
            _pre_phi35 = var_8;
        }
        break;
      case 2U:
        {
            *(uint64_t *)(var_0 + (-48L)) = 4211896UL;
            var_5 = indirect_placeholder_20();
            var_6 = (unsigned char *)(var_0 + (-9L));
            *var_6 = (unsigned char)var_5;
            _pre_phi35 = var_6;
        }
        break;
      default:
        {
            local_sp_1 = local_sp_0;
            if ((int)*var_3 <= (int)0U) {
                var_16 = local_sp_0 + (-8L);
                *(uint64_t *)var_16 = 4212132UL;
                indirect_placeholder_1();
                local_sp_1 = var_16;
            }
            *(uint64_t *)(local_sp_1 + (-8L)) = 4212137UL;
            var_17 = indirect_placeholder_20();
            var_18 = (unsigned char *)(var_0 + (-9L));
            *var_18 = (unsigned char)var_17;
            _pre_phi35 = var_18;
        }
        break;
    }
    return (uint64_t)*_pre_phi35;
}
