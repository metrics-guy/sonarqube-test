typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_81_ret_type;
struct indirect_placeholder_82_ret_type;
struct helper_cvtsq2ss_wrapper_ret_type;
struct type_7;
struct type_9;
struct helper_mulss_wrapper_ret_type;
struct helper_cvttss2si_wrapper_ret_type;
struct indirect_placeholder_80_ret_type;
struct indirect_placeholder_83_ret_type;
struct indirect_placeholder_81_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_82_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_cvtsq2ss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct type_7 {
};
struct type_9 {
};
struct helper_mulss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvttss2si_wrapper_ret_type {
    uint32_t field_0;
    unsigned char field_1;
};
struct indirect_placeholder_80_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_83_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern unsigned char init_state_0x8549(void);
extern unsigned char init_state_0x854c(void);
extern unsigned char init_state_0x8548(void);
extern unsigned char init_state_0x854b(void);
extern unsigned char init_state_0x8547(void);
extern unsigned char init_state_0x854d(void);
extern void indirect_placeholder_1(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct indirect_placeholder_81_ret_type indirect_placeholder_81(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_82_ret_type indirect_placeholder_82(uint64_t param_0);
extern struct helper_cvtsq2ss_wrapper_ret_type helper_cvtsq2ss_wrapper(struct type_7 *param_0, struct type_9 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_mulss_wrapper_ret_type helper_mulss_wrapper(struct type_7 *param_0, struct type_9 *param_1, struct type_9 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_cvttss2si_wrapper_ret_type helper_cvttss2si_wrapper(struct type_7 *param_0, struct type_9 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4);
extern uint64_t init_r10(void);
extern uint64_t indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_80_ret_type indirect_placeholder_80(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_83_ret_type indirect_placeholder_83(uint64_t param_0);
uint64_t bb_scale10_round_decimal_decoded(uint64_t rcx, uint64_t rdx, uint64_t rsi, uint64_t rdi, uint64_t r8) {
    uint64_t *var_99;
    uint64_t var_100;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t var_103;
    struct indirect_placeholder_81_ret_type var_104;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t var_124;
    struct indirect_placeholder_82_ret_type var_125;
    uint64_t var_126;
    uint64_t *var_127;
    uint32_t var_16;
    uint32_t *var_17;
    uint64_t *var_18;
    uint32_t var_19;
    uint32_t var_22;
    uint32_t var_20;
    uint32_t _v;
    uint32_t var_21;
    uint32_t var_23;
    uint32_t var_24;
    uint32_t *var_25;
    uint32_t var_26;
    uint32_t var_27;
    uint32_t var_28;
    uint32_t *var_29;
    struct helper_mulss_wrapper_ret_type var_31;
    struct helper_cvtsq2ss_wrapper_ret_type var_30;
    struct helper_cvttss2si_wrapper_ret_type var_32;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    unsigned char var_4;
    unsigned char var_5;
    unsigned char var_6;
    unsigned char var_7;
    unsigned char var_8;
    unsigned char var_9;
    uint32_t *var_10;
    uint64_t *var_11;
    uint64_t *var_12;
    uint64_t *var_13;
    uint32_t *var_14;
    uint32_t var_15;
    uint64_t rax_2;
    uint64_t var_163;
    uint64_t var_164;
    uint64_t var_165;
    uint64_t var_166;
    uint64_t *var_167;
    uint64_t var_168;
    uint64_t var_80;
    uint64_t *var_128;
    uint64_t *var_129;
    uint64_t var_130;
    uint64_t var_131;
    uint64_t var_132;
    bool var_135;
    uint64_t var_136;
    uint64_t *var_137;
    uint64_t *var_138;
    uint64_t var_139;
    uint64_t *var_140;
    uint64_t var_141;
    uint64_t var_145;
    uint64_t var_142;
    uint64_t var_143;
    uint64_t var_144;
    uint64_t *var_146;
    uint64_t var_147;
    uint64_t *var_148;
    uint64_t var_149;
    uint64_t var_153;
    uint64_t *var_154;
    uint64_t var_155;
    uint64_t var_156;
    uint64_t var_157;
    uint64_t var_158;
    uint64_t var_159;
    uint64_t var_160;
    uint64_t *var_161;
    uint64_t var_162;
    uint64_t var_150;
    uint64_t var_151;
    uint64_t var_152;
    uint64_t var_133;
    uint64_t var_134;
    uint32_t var_51;
    uint64_t var_107;
    uint64_t *var_108;
    uint64_t *var_109;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t var_112;
    uint64_t var_114;
    uint64_t *var_115;
    uint64_t var_116;
    uint64_t var_117;
    uint64_t var_118;
    uint64_t var_119;
    uint64_t var_120;
    uint64_t var_121;
    uint64_t *var_122;
    uint64_t var_123;
    uint64_t var_113;
    uint64_t *_pre_phi305;
    uint64_t local_sp_0;
    uint64_t *var_38;
    uint32_t *var_39;
    uint32_t *var_40;
    uint64_t *var_41;
    uint64_t *var_42;
    uint32_t *var_43;
    uint32_t var_44;
    uint32_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t rax_0;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_54;
    uint32_t var_55;
    uint64_t var_52;
    uint64_t var_53;
    uint32_t var_56;
    uint32_t *var_57;
    uint32_t var_58;
    uint32_t *var_59;
    bool var_60;
    uint32_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t *var_64;
    uint64_t *var_65;
    uint64_t var_66;
    uint64_t *var_67;
    uint64_t var_68;
    uint64_t var_71;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_72;
    uint64_t *var_73;
    uint64_t var_74;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_81;
    uint64_t *var_82;
    uint64_t var_83;
    bool var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t *var_90;
    struct indirect_placeholder_80_ret_type var_91;
    uint64_t var_92;
    uint64_t *var_93;
    uint64_t var_94;
    uint64_t *var_95;
    uint64_t var_96;
    uint64_t *var_97;
    uint64_t var_98;
    uint64_t var_33;
    struct indirect_placeholder_83_ret_type var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t *var_37;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = init_r10();
    var_4 = init_state_0x8549();
    var_5 = init_state_0x854c();
    var_6 = init_state_0x8548();
    var_7 = init_state_0x854b();
    var_8 = init_state_0x8547();
    var_9 = init_state_0x854d();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_10 = (uint32_t *)(var_0 + (-316L));
    *var_10 = (uint32_t)rdi;
    var_11 = (uint64_t *)(var_0 + (-344L));
    *var_11 = rsi;
    var_12 = (uint64_t *)(var_0 + (-336L));
    *var_12 = rdx;
    var_13 = (uint64_t *)(var_0 + (-328L));
    *var_13 = rcx;
    var_14 = (uint32_t *)(var_0 + (-320L));
    var_15 = (uint32_t)r8;
    *var_14 = var_15;
    rax_2 = 0UL;
    var_130 = 0UL;
    var_110 = 0UL;
    var_44 = 0U;
    rax_0 = 13UL;
    var_48 = 0UL;
    if (*var_13 == 0UL) {
        return rax_2;
    }
    var_16 = var_15 + *var_10;
    var_17 = (uint32_t *)(var_0 + (-12L));
    *var_17 = var_16;
    var_18 = (uint64_t *)(var_0 + (-24L));
    *var_18 = 0UL;
    var_19 = *var_17;
    if ((int)var_19 > (int)0U) {
        var_22 = *var_14;
    } else {
        var_20 = *var_14;
        var_22 = var_20;
        if ((int)var_20 > (int)0U) {
            _v = ((int)var_19 < (int)var_20) ? var_19 : var_20;
            *var_18 = (uint64_t)_v;
            *var_17 = (*var_17 - _v);
            var_21 = *var_14 - (uint32_t)*var_18;
            *var_14 = var_21;
            var_22 = var_21;
        }
    }
    var_23 = (uint32_t)(uint64_t)((long)((uint64_t)var_22 << 32UL) >> (long)63UL);
    var_24 = (var_23 ^ var_22) - var_23;
    var_25 = (uint32_t *)(var_0 + (-164L));
    *var_25 = var_24;
    var_26 = *var_17;
    var_27 = (uint32_t)(uint64_t)((long)((uint64_t)var_26 << 32UL) >> (long)63UL);
    var_28 = (var_27 ^ var_26) - var_27;
    var_29 = (uint32_t *)(var_0 + (-168L));
    *var_29 = var_28;
    var_30 = helper_cvtsq2ss_wrapper((struct type_7 *)(0UL), (struct type_9 *)(776UL), (uint64_t)*var_25, var_4, var_6, var_7, var_8);
    var_31 = helper_mulss_wrapper((struct type_7 *)(0UL), (struct type_9 *)(776UL), (struct type_9 *)(840UL), (uint64_t)*(uint32_t *)4284864UL, var_30.field_0, var_30.field_1, var_5, var_6, var_7, var_8, var_9);
    var_32 = helper_cvttss2si_wrapper((struct type_7 *)(0UL), (struct type_9 *)(776UL), var_31.field_0, var_31.field_1, var_5);
    var_33 = (((uint64_t)(var_32.field_0 + (*var_29 >> 5U)) << 2UL) + 8UL) & 17179869180UL;
    *(uint64_t *)(var_0 + (-352L)) = 4233332UL;
    var_34 = indirect_placeholder_83(var_33);
    var_35 = var_34.field_0;
    var_36 = var_0 + (-176L);
    var_37 = (uint64_t *)var_36;
    *var_37 = var_35;
    if (var_35 == 0UL) {
        *(uint64_t *)(var_0 + (-360L)) = 4233364UL;
        indirect_placeholder_1();
    } else {
        **(uint32_t **)var_36 = 1U;
        var_38 = (uint64_t *)(var_0 + (-32L));
        *var_38 = 1UL;
        if (*var_25 != 0U) {
            var_39 = (uint32_t *)(var_0 + (-44L));
            *var_39 = 0U;
            var_40 = (uint32_t *)(var_0 + (-180L));
            var_41 = (uint64_t *)(var_0 + (-64L));
            var_42 = (uint64_t *)(var_0 + (-56L));
            var_43 = (uint32_t *)(var_0 + (-184L));
            var_45 = *var_25;
            while (var_44 <= var_45)
                {
                    var_46 = (uint64_t)(var_44 + 13U);
                    var_47 = helper_cc_compute_c_wrapper((uint64_t)var_45 - var_46, var_46, var_2, 16U);
                    if (var_47 == 0UL) {
                        rax_0 = (uint64_t)(*var_25 - *var_39);
                    }
                    *var_40 = *(uint32_t *)((rax_0 << 2UL) + 4284736UL);
                    *var_41 = 0UL;
                    *var_42 = 0UL;
                    var_49 = *var_38;
                    var_50 = helper_cc_compute_c_wrapper(var_48 - var_49, var_49, var_2, 17U);
                    while (var_50 != 0UL)
                        {
                            var_51 = *(uint32_t *)(*var_37 + (*var_42 << 2UL));
                            *var_43 = var_51;
                            var_52 = *var_41 + ((uint64_t)*var_40 * (uint64_t)var_51);
                            *var_41 = var_52;
                            *(uint32_t *)(*var_37 + (*var_42 << 2UL)) = (uint32_t)var_52;
                            *var_41 = (*var_41 >> 32UL);
                            var_53 = *var_42 + 1UL;
                            *var_42 = var_53;
                            var_48 = var_53;
                            var_49 = *var_38;
                            var_50 = helper_cc_compute_c_wrapper(var_48 - var_49, var_49, var_2, 17U);
                        }
                    if (*var_41 == 0UL) {
                        var_54 = *var_38;
                        *var_38 = (var_54 + 1UL);
                        *(uint32_t *)(*var_37 + (var_54 << 2UL)) = (uint32_t)*var_41;
                    }
                    var_55 = *var_39 + 13U;
                    *var_39 = var_55;
                    var_44 = var_55;
                    var_45 = *var_25;
                }
        }
        var_56 = *var_29 >> 5U;
        var_57 = (uint32_t *)(var_0 + (-188L));
        *var_57 = var_56;
        var_58 = *var_29 & 31U;
        var_59 = (uint32_t *)(var_0 + (-192L));
        *var_59 = var_58;
        var_60 = ((int)*var_14 < (int)0U);
        var_61 = *var_17;
        if ((uint64_t)(unsigned char)(var_60 ? ((int)var_61 < (int)1U) : (uint64_t)((var_61 >> 31U) ^ 1U)) == 0UL) {
            var_96 = *var_37;
            var_97 = (uint64_t *)(var_0 + (-240L));
            *var_97 = var_96;
            var_98 = *var_38;
            var_99 = (uint64_t *)(var_0 + (-248L));
            *var_99 = var_98;
            if ((int)*var_14 < (int)0U) {
                var_124 = ((*var_11 + (uint64_t)*var_57) << 2UL) + 4UL;
                *(uint64_t *)(var_0 + (-360L)) = 4234542UL;
                var_125 = indirect_placeholder_82(var_124);
                var_126 = var_125.field_0;
                var_127 = (uint64_t *)(var_0 + (-200L));
                *var_127 = var_126;
                if (var_126 != 0UL) {
                    *(uint64_t *)(var_0 + (-368L)) = 4234574UL;
                    indirect_placeholder_1();
                    *(uint64_t *)(var_0 + (-376L)) = 4234589UL;
                    indirect_placeholder_1();
                    return rax_2;
                }
                var_128 = (uint64_t *)(var_0 + (-112L));
                *var_128 = var_126;
                var_129 = (uint64_t *)(var_0 + (-120L));
                *var_129 = 0UL;
                var_131 = (uint64_t)*var_57;
                var_132 = helper_cc_compute_c_wrapper(var_130 - var_131, var_131, var_2, 17U);
                while (var_132 != 0UL)
                    {
                        var_133 = *var_128;
                        *var_128 = (var_133 + 4UL);
                        *(uint32_t *)var_133 = 0U;
                        var_134 = *var_129 + 1UL;
                        *var_129 = var_134;
                        var_130 = var_134;
                        var_131 = (uint64_t)*var_57;
                        var_132 = helper_cc_compute_c_wrapper(var_130 - var_131, var_131, var_2, 17U);
                    }
                var_135 = (*var_59 == 0U);
                var_136 = *var_12;
                if (var_135) {
                    var_146 = (uint64_t *)(var_0 + (-152L));
                    *var_146 = var_136;
                    var_147 = *var_11;
                    var_148 = (uint64_t *)(var_0 + (-160L));
                    *var_148 = var_147;
                    var_149 = var_147;
                    while (var_149 != 0UL)
                        {
                            var_150 = *var_146;
                            *var_146 = (var_150 + 4UL);
                            var_151 = *var_128;
                            *var_128 = (var_151 + 4UL);
                            *(uint32_t *)var_151 = *(uint32_t *)var_150;
                            var_152 = *var_148 + (-1L);
                            *var_148 = var_152;
                            var_149 = var_152;
                        }
                }
                var_137 = (uint64_t *)(var_0 + (-128L));
                *var_137 = var_136;
                var_138 = (uint64_t *)(var_0 + (-136L));
                *var_138 = 0UL;
                var_139 = *var_11;
                var_140 = (uint64_t *)(var_0 + (-144L));
                *var_140 = var_139;
                var_141 = var_139;
                while (var_141 != 0UL)
                    {
                        var_142 = *var_137;
                        *var_137 = (var_142 + 4UL);
                        *var_138 = (*var_138 + ((uint64_t)*(uint32_t *)var_142 << (uint64_t)(*var_59 & 63U)));
                        var_143 = *var_128;
                        *var_128 = (var_143 + 4UL);
                        *(uint32_t *)var_143 = (uint32_t)*var_138;
                        *var_138 = (*var_138 >> 32UL);
                        var_144 = *var_140 + (-1L);
                        *var_140 = var_144;
                        var_141 = var_144;
                    }
                if (*var_138 == 0UL) {
                    var_145 = *var_128;
                    *var_128 = (var_145 + 4UL);
                    *(uint32_t *)var_145 = (uint32_t)*var_138;
                }
                var_153 = *var_127;
                var_154 = (uint64_t *)(var_0 + (-304L));
                *var_154 = var_153;
                var_155 = (uint64_t)((long)(*var_128 - *var_127) >> (long)2UL);
                *(uint64_t *)(var_0 + (-312L)) = var_155;
                var_156 = var_0 + (-264L);
                var_157 = *var_99;
                var_158 = *var_97;
                var_159 = *var_154;
                *(uint64_t *)(var_0 + (-368L)) = 4234972UL;
                var_160 = indirect_placeholder_12(var_158, var_157, var_159, var_155, var_3, var_156);
                var_161 = (uint64_t *)(var_0 + (-40L));
                *var_161 = var_160;
                var_162 = var_0 + (-376L);
                *(uint64_t *)var_162 = 4234991UL;
                indirect_placeholder_1();
                _pre_phi305 = var_161;
                local_sp_0 = var_162;
            } else {
                var_100 = var_0 + (-280L);
                var_101 = *var_97;
                var_102 = *var_11;
                var_103 = *var_12;
                *(uint64_t *)(var_0 + (-360L)) = 4234221UL;
                var_104 = indirect_placeholder_81(var_101, var_98, var_103, var_102, var_3, var_100);
                var_105 = var_104.field_0;
                var_106 = var_104.field_1;
                *(uint64_t *)(var_0 + (-208L)) = var_105;
                if (var_105 != 0UL) {
                    *(uint64_t *)(var_0 + (-368L)) = 4234253UL;
                    indirect_placeholder_1();
                    *(uint64_t *)(var_0 + (-376L)) = 4234268UL;
                    indirect_placeholder_1();
                    return rax_2;
                }
                var_107 = *var_37 + (*var_38 << 2UL);
                var_108 = (uint64_t *)(var_0 + (-216L));
                *var_108 = var_107;
                var_109 = (uint64_t *)(var_0 + (-104L));
                *var_109 = 0UL;
                var_111 = (uint64_t)*var_57;
                var_112 = helper_cc_compute_c_wrapper(var_110 - var_111, var_111, var_2, 17U);
                while (var_112 != 0UL)
                    {
                        *(uint32_t *)(*var_108 + (*var_109 << 2UL)) = 0U;
                        var_113 = *var_109 + 1UL;
                        *var_109 = var_113;
                        var_110 = var_113;
                        var_111 = (uint64_t)*var_57;
                        var_112 = helper_cc_compute_c_wrapper(var_110 - var_111, var_111, var_2, 17U);
                    }
                *(uint32_t *)(((uint64_t)*var_57 << 2UL) + *var_108) = (uint32_t)(1UL << (uint64_t)(*var_59 & 31U));
                var_114 = *var_108;
                var_115 = (uint64_t *)(var_0 + (-288L));
                *var_115 = var_114;
                var_116 = (uint64_t)(*var_57 + 1U);
                *(uint64_t *)(var_0 + (-296L)) = var_116;
                var_117 = var_0 + (-264L);
                var_118 = *var_115;
                var_119 = *(uint64_t *)var_100;
                var_120 = *(uint64_t *)(var_0 + (-272L));
                *(uint64_t *)(var_0 + (-368L)) = 4234486UL;
                var_121 = indirect_placeholder_12(var_118, var_116, var_120, var_119, var_106, var_117);
                var_122 = (uint64_t *)(var_0 + (-40L));
                *var_122 = var_121;
                var_123 = var_0 + (-376L);
                *(uint64_t *)var_123 = 4234505UL;
                indirect_placeholder_1();
                _pre_phi305 = var_122;
                local_sp_0 = var_123;
            }
        } else {
            if (var_58 != 0U) {
                var_62 = *var_37;
                var_63 = var_0 + (-72L);
                var_64 = (uint64_t *)var_63;
                *var_64 = var_62;
                var_65 = (uint64_t *)(var_0 + (-80L));
                *var_65 = 0UL;
                var_66 = *var_38;
                var_67 = (uint64_t *)(var_0 + (-88L));
                *var_67 = var_66;
                var_68 = var_66;
                while (var_68 != 0UL)
                    {
                        *var_65 = (*var_65 + ((uint64_t)**(uint32_t **)var_63 << (uint64_t)(*var_59 & 63U)));
                        var_69 = *var_64;
                        *var_64 = (var_69 + 4UL);
                        *(uint32_t *)var_69 = (uint32_t)*var_65;
                        *var_65 = (*var_65 >> 32UL);
                        var_70 = *var_67 + (-1L);
                        *var_67 = var_70;
                        var_68 = var_70;
                    }
                var_71 = *var_65;
                if (var_71 == 0UL) {
                    **(uint32_t **)var_63 = (uint32_t)var_71;
                    *var_38 = (*var_38 + 1UL);
                }
            }
            if (*var_57 != 0U) {
                var_72 = *var_38;
                var_73 = (uint64_t *)(var_0 + (-96L));
                *var_73 = var_72;
                var_74 = var_72;
                while (var_74 != 0UL)
                    {
                        var_75 = var_74 + (-1L);
                        *var_73 = var_75;
                        var_76 = var_75 << 2UL;
                        var_77 = *var_37;
                        *(uint32_t *)(var_77 + ((var_75 + (uint64_t)*var_57) << 2UL)) = *(uint32_t *)(var_77 + var_76);
                        var_74 = *var_73;
                    }
                var_78 = (uint64_t)*var_57;
                *var_73 = var_78;
                var_79 = var_78;
                while (var_79 != 0UL)
                    {
                        var_80 = var_79 + (-1L);
                        *var_73 = var_80;
                        *(uint32_t *)(*var_37 + (var_80 << 2UL)) = 0U;
                        var_79 = *var_73;
                    }
                *var_38 = (*var_38 + (uint64_t)*var_57);
            }
            var_81 = *var_37;
            var_82 = (uint64_t *)(var_0 + (-240L));
            *var_82 = var_81;
            var_83 = *var_38;
            *(uint64_t *)(var_0 + (-248L)) = var_83;
            var_84 = ((int)*var_14 < (int)0U);
            var_85 = var_0 + (-264L);
            var_86 = *var_82;
            var_87 = *var_11;
            var_88 = *var_12;
            var_89 = var_0 + (-360L);
            var_90 = (uint64_t *)var_89;
            local_sp_0 = var_89;
            if (var_84) {
                *var_90 = 4234125UL;
                var_94 = indirect_placeholder_12(var_86, var_83, var_88, var_87, var_3, var_85);
                var_95 = (uint64_t *)(var_0 + (-40L));
                *var_95 = var_94;
                _pre_phi305 = var_95;
            } else {
                *var_90 = 4234067UL;
                var_91 = indirect_placeholder_80(var_86, var_83, var_88, var_87, var_3, var_85);
                var_92 = var_91.field_0;
                var_93 = (uint64_t *)(var_0 + (-40L));
                *var_93 = var_92;
                _pre_phi305 = var_93;
            }
        }
        *(uint64_t *)(local_sp_0 + (-8L)) = 4235006UL;
        indirect_placeholder_1();
        *(uint64_t *)(local_sp_0 + (-16L)) = 4235021UL;
        indirect_placeholder_1();
        if (*_pre_phi305 == 0UL) {
            var_163 = *var_18;
            var_164 = *(uint64_t *)(var_0 + (-264L));
            var_165 = *(uint64_t *)(var_0 + (-256L));
            *(uint64_t *)(local_sp_0 + (-24L)) = 4235064UL;
            var_166 = indirect_placeholder_7(var_163, var_165, var_164);
            var_167 = (uint64_t *)(var_0 + (-224L));
            *var_167 = var_166;
            *(uint64_t *)(local_sp_0 + (-32L)) = 4235083UL;
            indirect_placeholder_1();
            var_168 = *var_167;
            rax_2 = var_168;
        }
    }
    return rax_2;
}
