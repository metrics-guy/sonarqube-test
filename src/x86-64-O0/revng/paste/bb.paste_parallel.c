typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_5_ret_type;
struct indirect_placeholder_13_ret_type;
struct indirect_placeholder_8_ret_type;
struct indirect_placeholder_9_ret_type;
struct indirect_placeholder_12_ret_type;
struct indirect_placeholder_5_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_13_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_8_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_9_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_12_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_11(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_10(void);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_6(uint64_t param_0);
extern void indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_5_ret_type indirect_placeholder_5(uint64_t param_0);
extern struct indirect_placeholder_13_ret_type indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_8_ret_type indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_9_ret_type indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_12_ret_type indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2);
void bb_paste_parallel(uint64_t rsi, uint64_t rdi) {
    uint64_t local_sp_9;
    uint64_t var_70;
    uint64_t local_sp_14;
    uint64_t var_110;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    unsigned char *var_7;
    uint64_t var_8;
    struct indirect_placeholder_5_ret_type var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t *var_13;
    uint64_t var_14;
    uint64_t var_15;
    struct indirect_placeholder_13_ret_type var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t *var_21;
    unsigned char *var_22;
    uint64_t *var_23;
    uint64_t var_24;
    uint64_t local_sp_11;
    uint64_t var_84;
    struct indirect_placeholder_8_ret_type var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t local_sp_0;
    uint64_t var_91;
    uint64_t var_73;
    struct indirect_placeholder_9_ret_type var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t local_sp_1;
    bool var_80;
    uint64_t var_81;
    uint64_t *var_82;
    uint64_t var_83;
    uint64_t *_cast3_pre_phi;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t local_sp_2_ph;
    uint64_t local_sp_2;
    unsigned char var_69;
    uint64_t var_64;
    uint32_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t r9_2;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t local_sp_4;
    uint64_t rcx_1;
    uint64_t var_34;
    struct indirect_placeholder_12_ret_type var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t local_sp_12;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t local_sp_3;
    uint64_t r8_0;
    uint64_t r9_0;
    uint64_t var_41;
    uint64_t r8_2;
    uint64_t rcx_0;
    uint64_t r8_1;
    uint64_t r9_1;
    uint64_t var_45;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t *_cast2;
    uint64_t local_sp_5;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_46;
    uint64_t local_sp_7;
    uint64_t local_sp_6;
    unsigned char *var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t *var_50;
    uint64_t *var_51;
    uint64_t *var_52;
    unsigned char *var_53;
    uint32_t *var_54;
    uint32_t *var_55;
    unsigned char *var_56;
    uint64_t var_57;
    uint64_t local_sp_8;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t local_sp_10;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t var_98;
    unsigned char **var_92;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_99;
    uint32_t var_100;
    unsigned char var_103;
    uint64_t local_sp_13;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t var_106;
    unsigned char storemerge;
    uint32_t var_107;
    uint64_t var_108;
    uint64_t var_109;
    uint64_t var_27;
    uint64_t var_28;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = init_rbx();
    var_4 = var_0 + (-8L);
    *(uint64_t *)var_4 = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    var_5 = (uint64_t *)(var_0 + (-128L));
    *var_5 = rdi;
    var_6 = (uint64_t *)(var_0 + (-136L));
    *var_6 = rsi;
    var_7 = (unsigned char *)(var_0 + (-25L));
    *var_7 = (unsigned char)'\x01';
    var_8 = *var_5 + 2UL;
    *(uint64_t *)(var_0 + (-144L)) = 4205003UL;
    var_9 = indirect_placeholder_5(var_8);
    var_10 = var_9.field_0;
    var_11 = var_9.field_1;
    var_12 = var_9.field_2;
    var_13 = (uint64_t *)(var_0 + (-96L));
    *var_13 = var_10;
    var_14 = *var_5 + 1UL;
    var_15 = var_0 + (-152L);
    *(uint64_t *)var_15 = 4205028UL;
    var_16 = indirect_placeholder_13(var_11, 8UL, var_14, var_12);
    var_17 = var_16.field_0;
    var_18 = var_16.field_1;
    var_19 = var_16.field_3;
    var_20 = var_16.field_4;
    var_21 = (uint64_t *)(var_0 + (-104L));
    *var_21 = var_17;
    var_22 = (unsigned char *)(var_0 + (-41L));
    *var_22 = (unsigned char)'\x00';
    var_23 = (uint64_t *)(var_0 + (-40L));
    *var_23 = 0UL;
    var_24 = 0UL;
    var_69 = (unsigned char)'\x00';
    r9_2 = var_20;
    rcx_1 = var_18;
    r8_2 = var_19;
    local_sp_5 = var_15;
    var_57 = 0UL;
    var_25 = *var_5;
    var_26 = helper_cc_compute_c_wrapper(var_24 - var_25, var_25, var_2, 17U);
    r8_0 = r8_2;
    r9_0 = r9_2;
    rcx_0 = rcx_1;
    r8_1 = r8_2;
    r9_1 = r9_2;
    local_sp_6 = local_sp_5;
    while (var_26 != 0UL)
        {
            var_27 = *(uint64_t *)(*var_6 + (*var_23 << 3UL));
            var_28 = local_sp_5 + (-8L);
            *(uint64_t *)var_28 = 4205084UL;
            indirect_placeholder();
            local_sp_4 = var_28;
            if ((uint64_t)(uint32_t)var_27 == 0UL) {
                *(unsigned char *)4289392UL = (unsigned char)'\x01';
                *(uint64_t *)((*var_23 << 3UL) + *var_21) = *(uint64_t *)4288072UL;
            } else {
                var_29 = *var_23 << 3UL;
                var_30 = *(uint64_t *)(*var_6 + var_29);
                var_31 = *var_21 + var_29;
                var_32 = local_sp_5 + (-16L);
                *(uint64_t *)var_32 = 4205184UL;
                indirect_placeholder();
                *(uint64_t *)var_31 = var_30;
                var_33 = *var_23 << 3UL;
                _cast2 = (uint64_t *)(*var_21 + var_33);
                _cast3_pre_phi = _cast2;
                local_sp_3 = var_32;
                if (*_cast2 == 0UL) {
                    var_34 = *(uint64_t *)(*var_6 + var_33);
                    *(uint64_t *)(local_sp_5 + (-24L)) = 4205254UL;
                    var_35 = indirect_placeholder_12(var_34, 3UL, 0UL);
                    var_36 = var_35.field_0;
                    var_37 = var_35.field_1;
                    var_38 = var_35.field_2;
                    *(uint64_t *)(local_sp_5 + (-32L)) = 4205262UL;
                    indirect_placeholder();
                    var_39 = (uint64_t)*(uint32_t *)var_36;
                    var_40 = local_sp_5 + (-40L);
                    *(uint64_t *)var_40 = 4205289UL;
                    indirect_placeholder_7(0UL, var_36, 4273128UL, var_37, var_39, 1UL, var_38);
                    _cast3_pre_phi = (uint64_t *)(*var_21 + (*var_23 << 3UL));
                    local_sp_3 = var_40;
                    r8_0 = var_37;
                    r9_0 = var_38;
                }
                var_41 = *_cast3_pre_phi;
                *(uint64_t *)(local_sp_3 + (-8L)) = 4205319UL;
                indirect_placeholder();
                r8_1 = r8_0;
                r9_1 = r9_0;
                if ((uint64_t)(uint32_t)var_41 == 0UL) {
                    *var_22 = (unsigned char)'\x01';
                }
                var_42 = *(uint64_t *)(*var_21 + (*var_23 << 3UL));
                var_43 = local_sp_3 + (-16L);
                *(uint64_t *)var_43 = 4205362UL;
                var_44 = indirect_placeholder_11(2UL, var_42);
                local_sp_4 = var_43;
                rcx_0 = var_44;
            }
            var_45 = *var_23 + 1UL;
            *var_23 = var_45;
            var_24 = var_45;
            r9_2 = r9_1;
            rcx_1 = rcx_0;
            r8_2 = r8_1;
            local_sp_5 = local_sp_4;
            var_25 = *var_5;
            var_26 = helper_cc_compute_c_wrapper(var_24 - var_25, var_25, var_2, 17U);
            r8_0 = r8_2;
            r9_0 = r9_2;
            rcx_0 = rcx_1;
            r8_1 = r8_2;
            r9_1 = r9_2;
            local_sp_6 = local_sp_5;
        }
    if (*var_22 != '\x00' & *(unsigned char *)4289392UL != '\x00') {
        var_46 = local_sp_5 + (-8L);
        *(uint64_t *)var_46 = 4205431UL;
        indirect_placeholder_7(0UL, rcx_1, 4273131UL, r8_2, 0UL, 1UL, r9_2);
        local_sp_7 = var_46;
        var_47 = (unsigned char *)(var_0 + (-42L));
        *var_47 = (unsigned char)'\x00';
        var_48 = *(uint64_t *)4289400UL;
        var_49 = var_0 + (-56L);
        var_50 = (uint64_t *)var_49;
        *var_50 = var_48;
        var_51 = (uint64_t *)(var_0 + (-64L));
        *var_51 = 0UL;
        var_52 = (uint64_t *)(var_0 + (-72L));
        *var_52 = 0UL;
        var_53 = (unsigned char *)(var_0 + (-81L));
        var_54 = (uint32_t *)(var_0 + (-76L));
        var_55 = (uint32_t *)(var_0 + (-80L));
        var_56 = (unsigned char *)(var_0 + (-105L));
        local_sp_8 = local_sp_7;
        var_58 = *var_5;
        var_59 = helper_cc_compute_c_wrapper(var_57 - var_58, var_58, var_2, 17U);
        local_sp_6 = local_sp_8;
        local_sp_9 = local_sp_8;
        while (var_59 != 0UL)
            {
                if (*var_23 == 0UL) {
                    break;
                }
                *var_53 = (unsigned char)'\x00';
                var_60 = *(uint64_t *)(*var_21 + (*var_52 << 3UL));
                if (var_60 == 0UL) {
                    local_sp_10 = local_sp_9;
                    local_sp_12 = local_sp_9;
                    if (var_69 == '\x01') {
                        *var_47 = (unsigned char)'\x01';
                        if (*var_5 == (*var_52 + 1UL)) {
                            var_107 = *var_54;
                            if (var_107 == 4294967295U) {
                                storemerge = *(unsigned char *)4289088UL;
                            } else {
                                storemerge = (unsigned char)var_107;
                            }
                            *var_56 = storemerge;
                            var_108 = (uint64_t)(uint32_t)(uint64_t)storemerge;
                            var_109 = local_sp_9 + (-8L);
                            *(uint64_t *)var_109 = 4206381UL;
                            indirect_placeholder_6(var_108);
                            local_sp_14 = var_109;
                        } else {
                            var_99 = (uint64_t)*(unsigned char *)4289088UL;
                            var_100 = *var_54;
                            if (((uint64_t)(var_100 - (uint32_t)var_99) == 0UL) || (var_100 == 4294967295U)) {
                                var_101 = (uint64_t)(uint32_t)((int)(var_100 << 24U) >> (int)24U);
                                var_102 = local_sp_9 + (-8L);
                                *(uint64_t *)var_102 = 4206290UL;
                                indirect_placeholder_6(var_101);
                                local_sp_12 = var_102;
                            }
                            var_103 = **(unsigned char **)var_49;
                            local_sp_13 = local_sp_12;
                            if (var_103 == '\x00') {
                                var_104 = (uint64_t)(uint32_t)(uint64_t)var_103;
                                var_105 = local_sp_12 + (-8L);
                                *(uint64_t *)var_105 = 4206318UL;
                                indirect_placeholder_6(var_104);
                                local_sp_13 = var_105;
                            }
                            var_106 = *var_50 + 1UL;
                            *var_50 = var_106;
                            local_sp_14 = local_sp_13;
                            if (var_106 == *(uint64_t *)4289408UL) {
                                *var_50 = *(uint64_t *)4289400UL;
                            }
                        }
                    } else {
                        var_70 = *var_52;
                        var_71 = *(uint64_t *)(*var_21 + (var_70 << 3UL));
                        var_91 = var_70;
                        if (var_71 != 0UL) {
                            var_72 = local_sp_9 + (-8L);
                            *(uint64_t *)var_72 = 4205771UL;
                            indirect_placeholder();
                            local_sp_1 = var_72;
                            if ((uint64_t)(uint32_t)var_71 == 0UL) {
                                var_73 = *(uint64_t *)(*var_6 + (*var_52 << 3UL));
                                *(uint64_t *)(local_sp_9 + (-16L)) = 4205815UL;
                                var_74 = indirect_placeholder_9(var_73, 3UL, 0UL);
                                var_75 = var_74.field_0;
                                var_76 = var_74.field_1;
                                var_77 = var_74.field_2;
                                var_78 = (uint64_t)*var_55;
                                var_79 = local_sp_9 + (-24L);
                                *(uint64_t *)var_79 = 4205846UL;
                                indirect_placeholder_7(0UL, var_75, 4273128UL, var_76, var_78, 0UL, var_77);
                                *var_7 = (unsigned char)'\x00';
                                local_sp_1 = var_79;
                            }
                            var_80 = (*(uint64_t *)(*var_21 + (*var_52 << 3UL)) == *(uint64_t *)4288072UL);
                            var_81 = local_sp_1 + (-8L);
                            var_82 = (uint64_t *)var_81;
                            local_sp_0 = var_81;
                            if (var_80) {
                                *var_82 = 4205914UL;
                                indirect_placeholder();
                            } else {
                                *var_82 = 4205946UL;
                                var_83 = indirect_placeholder_10();
                                if ((uint64_t)((uint32_t)var_83 + 1U) == 0UL) {
                                    var_84 = *(uint64_t *)(*var_6 + (*var_52 << 3UL));
                                    *(uint64_t *)(local_sp_1 + (-16L)) = 4205991UL;
                                    var_85 = indirect_placeholder_8(var_84, 3UL, 0UL);
                                    var_86 = var_85.field_0;
                                    var_87 = var_85.field_1;
                                    var_88 = var_85.field_2;
                                    *(uint64_t *)(local_sp_1 + (-24L)) = 4205999UL;
                                    indirect_placeholder();
                                    var_89 = (uint64_t)*(uint32_t *)var_86;
                                    var_90 = local_sp_1 + (-32L);
                                    *(uint64_t *)var_90 = 4206026UL;
                                    indirect_placeholder_7(0UL, var_86, 4273128UL, var_87, var_89, 0UL, var_88);
                                    *var_7 = (unsigned char)'\x00';
                                    local_sp_0 = var_90;
                                }
                            }
                            *(uint64_t *)(*var_21 + (*var_52 << 3UL)) = 0UL;
                            *var_23 = (*var_23 + (-1L));
                            var_91 = *var_52;
                            local_sp_10 = local_sp_0;
                        }
                        local_sp_11 = local_sp_10;
                        local_sp_14 = local_sp_10;
                        if (*var_5 == (var_91 + 1UL)) {
                            if (*var_47 != '\x00') {
                                if (*var_51 != 0UL) {
                                    var_95 = *var_13;
                                    var_96 = local_sp_10 + (-8L);
                                    *(uint64_t *)var_96 = 4206120UL;
                                    indirect_placeholder();
                                    local_sp_11 = var_96;
                                    if (*var_51 != var_95) {
                                        *(uint64_t *)(local_sp_10 + (-16L)) = 4206131UL;
                                        indirect_placeholder_6(var_4);
                                        abort();
                                    }
                                    *var_51 = 0UL;
                                }
                                var_97 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)4289088UL;
                                var_98 = local_sp_11 + (-8L);
                                *(uint64_t *)var_98 = 4206156UL;
                                indirect_placeholder_6(var_97);
                                local_sp_14 = var_98;
                            }
                        } else {
                            var_92 = (unsigned char **)var_49;
                            if (**var_92 == '\x00') {
                                var_93 = *var_51;
                                *var_51 = (var_93 + 1UL);
                                *(unsigned char *)(*var_13 + var_93) = **var_92;
                            }
                            var_94 = *var_50 + 1UL;
                            *var_50 = var_94;
                            if (var_94 == *(uint64_t *)4289408UL) {
                                *var_50 = *(uint64_t *)4289400UL;
                            }
                        }
                    }
                    var_110 = *var_52 + 1UL;
                    *var_52 = var_110;
                    var_57 = var_110;
                    local_sp_8 = local_sp_14;
                    var_58 = *var_5;
                    var_59 = helper_cc_compute_c_wrapper(var_57 - var_58, var_58, var_2, 17U);
                    local_sp_6 = local_sp_8;
                    local_sp_9 = local_sp_8;
                    continue;
                }
                *(uint64_t *)(local_sp_8 + (-8L)) = 4205532UL;
                indirect_placeholder();
                *var_54 = (uint32_t)var_60;
                var_61 = local_sp_8 + (-16L);
                *(uint64_t *)var_61 = 4205540UL;
                indirect_placeholder();
                *var_55 = *(uint32_t *)var_60;
                local_sp_2_ph = var_61;
                var_69 = (unsigned char)'\x01';
                if (*var_54 != 4294967295U & *var_51 != 0UL) {
                    var_62 = *var_13;
                    var_63 = local_sp_8 + (-24L);
                    *(uint64_t *)var_63 = 4205590UL;
                    indirect_placeholder();
                    local_sp_2_ph = var_63;
                    if (*var_51 != var_62) {
                        *(uint64_t *)(local_sp_8 + (-32L)) = 4205601UL;
                        indirect_placeholder_6(var_4);
                        abort();
                    }
                    *var_51 = 0UL;
                }
                local_sp_2 = local_sp_2_ph;
                while (1U)
                    {
                        local_sp_9 = local_sp_2;
                        if (*var_54 != 4294967295U) {
                            loop_state_var = 0U;
                            break;
                        }
                        *var_53 = (unsigned char)'\x01';
                        var_64 = (uint64_t)*(unsigned char *)4289088UL;
                        var_65 = *var_54;
                        if ((uint64_t)(var_65 - (uint32_t)var_64) != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_66 = (uint64_t)(uint32_t)((int)(var_65 << 24U) >> (int)24U);
                        *(uint64_t *)(local_sp_2 + (-8L)) = 4205643UL;
                        indirect_placeholder_6(var_66);
                        var_67 = *(uint64_t *)(*var_21 + (*var_52 << 3UL));
                        *(uint64_t *)(local_sp_2 + (-16L)) = 4205673UL;
                        indirect_placeholder();
                        *var_54 = (uint32_t)var_67;
                        var_68 = local_sp_2 + (-24L);
                        *(uint64_t *)var_68 = 4205681UL;
                        indirect_placeholder();
                        *var_55 = *(uint32_t *)var_67;
                        local_sp_2 = var_68;
                        continue;
                    }
                var_69 = *var_53;
            }
    }
    local_sp_7 = local_sp_6;
    while (*var_23 != 0UL)
        {
            var_47 = (unsigned char *)(var_0 + (-42L));
            *var_47 = (unsigned char)'\x00';
            var_48 = *(uint64_t *)4289400UL;
            var_49 = var_0 + (-56L);
            var_50 = (uint64_t *)var_49;
            *var_50 = var_48;
            var_51 = (uint64_t *)(var_0 + (-64L));
            *var_51 = 0UL;
            var_52 = (uint64_t *)(var_0 + (-72L));
            *var_52 = 0UL;
            var_53 = (unsigned char *)(var_0 + (-81L));
            var_54 = (uint32_t *)(var_0 + (-76L));
            var_55 = (uint32_t *)(var_0 + (-80L));
            var_56 = (unsigned char *)(var_0 + (-105L));
            local_sp_8 = local_sp_7;
            var_58 = *var_5;
            var_59 = helper_cc_compute_c_wrapper(var_57 - var_58, var_58, var_2, 17U);
            local_sp_6 = local_sp_8;
            local_sp_9 = local_sp_8;
            while (var_59 != 0UL)
                {
                    if (*var_23 == 0UL) {
                        break;
                    }
                    *var_53 = (unsigned char)'\x00';
                    var_60 = *(uint64_t *)(*var_21 + (*var_52 << 3UL));
                    if (var_60 == 0UL) {
                        local_sp_10 = local_sp_9;
                        local_sp_12 = local_sp_9;
                        if (var_69 == '\x01') {
                            *var_47 = (unsigned char)'\x01';
                            if (*var_5 == (*var_52 + 1UL)) {
                                var_107 = *var_54;
                                if (var_107 == 4294967295U) {
                                    storemerge = *(unsigned char *)4289088UL;
                                } else {
                                    storemerge = (unsigned char)var_107;
                                }
                                *var_56 = storemerge;
                                var_108 = (uint64_t)(uint32_t)(uint64_t)storemerge;
                                var_109 = local_sp_9 + (-8L);
                                *(uint64_t *)var_109 = 4206381UL;
                                indirect_placeholder_6(var_108);
                                local_sp_14 = var_109;
                            } else {
                                var_99 = (uint64_t)*(unsigned char *)4289088UL;
                                var_100 = *var_54;
                                if (((uint64_t)(var_100 - (uint32_t)var_99) == 0UL) || (var_100 == 4294967295U)) {
                                    var_101 = (uint64_t)(uint32_t)((int)(var_100 << 24U) >> (int)24U);
                                    var_102 = local_sp_9 + (-8L);
                                    *(uint64_t *)var_102 = 4206290UL;
                                    indirect_placeholder_6(var_101);
                                    local_sp_12 = var_102;
                                }
                                var_103 = **(unsigned char **)var_49;
                                local_sp_13 = local_sp_12;
                                if (var_103 == '\x00') {
                                    var_104 = (uint64_t)(uint32_t)(uint64_t)var_103;
                                    var_105 = local_sp_12 + (-8L);
                                    *(uint64_t *)var_105 = 4206318UL;
                                    indirect_placeholder_6(var_104);
                                    local_sp_13 = var_105;
                                }
                                var_106 = *var_50 + 1UL;
                                *var_50 = var_106;
                                local_sp_14 = local_sp_13;
                                if (var_106 == *(uint64_t *)4289408UL) {
                                    *var_50 = *(uint64_t *)4289400UL;
                                }
                            }
                        } else {
                            var_70 = *var_52;
                            var_71 = *(uint64_t *)(*var_21 + (var_70 << 3UL));
                            var_91 = var_70;
                            if (var_71 != 0UL) {
                                var_72 = local_sp_9 + (-8L);
                                *(uint64_t *)var_72 = 4205771UL;
                                indirect_placeholder();
                                local_sp_1 = var_72;
                                if ((uint64_t)(uint32_t)var_71 == 0UL) {
                                    var_73 = *(uint64_t *)(*var_6 + (*var_52 << 3UL));
                                    *(uint64_t *)(local_sp_9 + (-16L)) = 4205815UL;
                                    var_74 = indirect_placeholder_9(var_73, 3UL, 0UL);
                                    var_75 = var_74.field_0;
                                    var_76 = var_74.field_1;
                                    var_77 = var_74.field_2;
                                    var_78 = (uint64_t)*var_55;
                                    var_79 = local_sp_9 + (-24L);
                                    *(uint64_t *)var_79 = 4205846UL;
                                    indirect_placeholder_7(0UL, var_75, 4273128UL, var_76, var_78, 0UL, var_77);
                                    *var_7 = (unsigned char)'\x00';
                                    local_sp_1 = var_79;
                                }
                                var_80 = (*(uint64_t *)(*var_21 + (*var_52 << 3UL)) == *(uint64_t *)4288072UL);
                                var_81 = local_sp_1 + (-8L);
                                var_82 = (uint64_t *)var_81;
                                local_sp_0 = var_81;
                                if (var_80) {
                                    *var_82 = 4205914UL;
                                    indirect_placeholder();
                                } else {
                                    *var_82 = 4205946UL;
                                    var_83 = indirect_placeholder_10();
                                    if ((uint64_t)((uint32_t)var_83 + 1U) == 0UL) {
                                        var_84 = *(uint64_t *)(*var_6 + (*var_52 << 3UL));
                                        *(uint64_t *)(local_sp_1 + (-16L)) = 4205991UL;
                                        var_85 = indirect_placeholder_8(var_84, 3UL, 0UL);
                                        var_86 = var_85.field_0;
                                        var_87 = var_85.field_1;
                                        var_88 = var_85.field_2;
                                        *(uint64_t *)(local_sp_1 + (-24L)) = 4205999UL;
                                        indirect_placeholder();
                                        var_89 = (uint64_t)*(uint32_t *)var_86;
                                        var_90 = local_sp_1 + (-32L);
                                        *(uint64_t *)var_90 = 4206026UL;
                                        indirect_placeholder_7(0UL, var_86, 4273128UL, var_87, var_89, 0UL, var_88);
                                        *var_7 = (unsigned char)'\x00';
                                        local_sp_0 = var_90;
                                    }
                                }
                                *(uint64_t *)(*var_21 + (*var_52 << 3UL)) = 0UL;
                                *var_23 = (*var_23 + (-1L));
                                var_91 = *var_52;
                                local_sp_10 = local_sp_0;
                            }
                            local_sp_11 = local_sp_10;
                            local_sp_14 = local_sp_10;
                            if (*var_5 == (var_91 + 1UL)) {
                                if (*var_47 != '\x00') {
                                    if (*var_51 != 0UL) {
                                        var_95 = *var_13;
                                        var_96 = local_sp_10 + (-8L);
                                        *(uint64_t *)var_96 = 4206120UL;
                                        indirect_placeholder();
                                        local_sp_11 = var_96;
                                        if (*var_51 != var_95) {
                                            *(uint64_t *)(local_sp_10 + (-16L)) = 4206131UL;
                                            indirect_placeholder_6(var_4);
                                            abort();
                                        }
                                        *var_51 = 0UL;
                                    }
                                    var_97 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)4289088UL;
                                    var_98 = local_sp_11 + (-8L);
                                    *(uint64_t *)var_98 = 4206156UL;
                                    indirect_placeholder_6(var_97);
                                    local_sp_14 = var_98;
                                }
                            } else {
                                var_92 = (unsigned char **)var_49;
                                if (**var_92 == '\x00') {
                                    var_93 = *var_51;
                                    *var_51 = (var_93 + 1UL);
                                    *(unsigned char *)(*var_13 + var_93) = **var_92;
                                }
                                var_94 = *var_50 + 1UL;
                                *var_50 = var_94;
                                if (var_94 == *(uint64_t *)4289408UL) {
                                    *var_50 = *(uint64_t *)4289400UL;
                                }
                            }
                        }
                        var_110 = *var_52 + 1UL;
                        *var_52 = var_110;
                        var_57 = var_110;
                        local_sp_8 = local_sp_14;
                        var_58 = *var_5;
                        var_59 = helper_cc_compute_c_wrapper(var_57 - var_58, var_58, var_2, 17U);
                        local_sp_6 = local_sp_8;
                        local_sp_9 = local_sp_8;
                        continue;
                    }
                    *(uint64_t *)(local_sp_8 + (-8L)) = 4205532UL;
                    indirect_placeholder();
                    *var_54 = (uint32_t)var_60;
                    var_61 = local_sp_8 + (-16L);
                    *(uint64_t *)var_61 = 4205540UL;
                    indirect_placeholder();
                    *var_55 = *(uint32_t *)var_60;
                    local_sp_2_ph = var_61;
                    var_69 = (unsigned char)'\x01';
                    if (*var_54 != 4294967295U & *var_51 != 0UL) {
                        var_62 = *var_13;
                        var_63 = local_sp_8 + (-24L);
                        *(uint64_t *)var_63 = 4205590UL;
                        indirect_placeholder();
                        local_sp_2_ph = var_63;
                        if (*var_51 != var_62) {
                            *(uint64_t *)(local_sp_8 + (-32L)) = 4205601UL;
                            indirect_placeholder_6(var_4);
                            abort();
                        }
                        *var_51 = 0UL;
                    }
                    local_sp_2 = local_sp_2_ph;
                    while (1U)
                        {
                            local_sp_9 = local_sp_2;
                            if (*var_54 != 4294967295U) {
                                loop_state_var = 0U;
                                break;
                            }
                            *var_53 = (unsigned char)'\x01';
                            var_64 = (uint64_t)*(unsigned char *)4289088UL;
                            var_65 = *var_54;
                            if ((uint64_t)(var_65 - (uint32_t)var_64) != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            var_66 = (uint64_t)(uint32_t)((int)(var_65 << 24U) >> (int)24U);
                            *(uint64_t *)(local_sp_2 + (-8L)) = 4205643UL;
                            indirect_placeholder_6(var_66);
                            var_67 = *(uint64_t *)(*var_21 + (*var_52 << 3UL));
                            *(uint64_t *)(local_sp_2 + (-16L)) = 4205673UL;
                            indirect_placeholder();
                            *var_54 = (uint32_t)var_67;
                            var_68 = local_sp_2 + (-24L);
                            *(uint64_t *)var_68 = 4205681UL;
                            indirect_placeholder();
                            *var_55 = *(uint32_t *)var_67;
                            local_sp_2 = var_68;
                            continue;
                        }
                    var_69 = *var_53;
                }
            local_sp_7 = local_sp_6;
        }
    *(uint64_t *)(local_sp_6 + (-8L)) = 4206433UL;
    indirect_placeholder();
    *(uint64_t *)(local_sp_6 + (-16L)) = 4206445UL;
    indirect_placeholder();
    return;
}
