typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
uint64_t bb_freadseek(uint64_t rsi, uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t *var_2;
    uint64_t *var_3;
    uint64_t rax_0;
    uint64_t var_31;
    uint64_t var_28;
    uint64_t var_25;
    uint64_t *var_26;
    uint64_t var_27;
    uint64_t local_sp_0;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_9;
    uint64_t local_sp_1;
    uint64_t var_32;
    uint64_t spec_select;
    uint64_t var_16;
    uint64_t local_sp_4;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t local_sp_2;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t local_sp_3;
    uint64_t var_21;
    uint64_t var_22;
    uint32_t *var_23;
    uint32_t var_24;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = (uint64_t *)(var_0 + (-4160L));
    *var_2 = rdi;
    var_3 = (uint64_t *)(var_0 + (-4168L));
    *var_3 = rsi;
    rax_0 = 0UL;
    if (rsi == 0UL) {
        return rax_0;
    }
    var_4 = *var_2;
    var_5 = var_0 + (-4176L);
    *(uint64_t *)var_5 = 4226634UL;
    indirect_placeholder();
    var_6 = (uint64_t *)(var_0 + (-16L));
    *var_6 = var_4;
    var_7 = (uint64_t *)(var_0 + (-48L));
    var_8 = (uint64_t *)(var_0 + (-24L));
    var_9 = var_4;
    local_sp_3 = var_5;
    while (1U)
        {
            local_sp_4 = local_sp_3;
            if (var_9 != 0UL) {
                loop_state_var = 1U;
                break;
            }
            var_10 = *var_2;
            var_11 = local_sp_3 + (-8L);
            *(uint64_t *)var_11 = 4226665UL;
            indirect_placeholder();
            local_sp_2 = var_11;
            if (var_10 != 0UL) {
                var_12 = *var_7;
                if (var_12 != 0UL) {
                    var_13 = *var_3;
                    *var_8 = ((var_13 > var_12) ? var_12 : var_13);
                    var_14 = local_sp_3 + (-16L);
                    *(uint64_t *)var_14 = 4226724UL;
                    indirect_placeholder();
                    var_15 = *var_3 - *var_8;
                    *var_3 = var_15;
                    local_sp_2 = var_14;
                    local_sp_4 = var_14;
                    if (var_15 != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_16 = *var_6 - *var_8;
                    *var_6 = var_16;
                    if (var_16 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                }
            }
            var_17 = *var_2;
            var_18 = local_sp_2 + (-8L);
            *(uint64_t *)var_18 = 4226785UL;
            indirect_placeholder();
            local_sp_1 = var_18;
            local_sp_3 = var_18;
            if ((uint64_t)((uint32_t)var_17 + 1U) != 0UL) {
                loop_state_var = 2U;
                break;
            }
            var_19 = *var_3 + (-1L);
            *var_3 = var_19;
            if (var_19 != 0UL) {
                loop_state_var = 0U;
                break;
            }
            var_20 = *var_6 + (-1L);
            *var_6 = var_20;
            var_9 = var_20;
            continue;
        }
    switch (loop_state_var) {
      case 0U:
        {
            break;
        }
        break;
      case 2U:
        {
            var_32 = *var_2;
            *(uint64_t *)(local_sp_1 + (-8L)) = 4227037UL;
            indirect_placeholder();
            spec_select = ((uint64_t)(uint32_t)var_32 == 0UL) ? 0UL : 4294967295UL;
            rax_0 = spec_select;
        }
        break;
      case 1U:
        {
            var_21 = *var_2;
            var_22 = local_sp_4 + (-8L);
            *(uint64_t *)var_22 = 4226856UL;
            indirect_placeholder();
            var_23 = (uint32_t *)(var_0 + (-28L));
            var_24 = (uint32_t)var_21;
            *var_23 = var_24;
            local_sp_0 = var_22;
            if ((int)var_24 >= (int)0U) {
                *(uint64_t *)(local_sp_4 + (-16L)) = 4226885UL;
                indirect_placeholder();
                var_25 = *var_2;
                *(uint64_t *)(local_sp_4 + (-24L)) = 4226920UL;
                indirect_placeholder();
                rax_0 = var_25;
                return rax_0;
            }
            var_26 = (uint64_t *)(var_0 + (-40L));
            var_27 = var_0 + (-4152L);
            var_28 = *var_3;
            while (1U)
                {
                    *var_26 = ((var_28 > 4096UL) ? 4096UL : var_28);
                    var_29 = local_sp_0 + (-8L);
                    *(uint64_t *)var_29 = 4226984UL;
                    indirect_placeholder();
                    var_30 = *var_26;
                    local_sp_0 = var_29;
                    local_sp_1 = var_29;
                    if (var_30 <= var_27) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_31 = *var_3 - var_30;
                    *var_3 = var_31;
                    var_28 = var_31;
                    if (var_31 == 0UL) {
                        continue;
                    }
                    loop_state_var = 0U;
                    break;
                }
            switch (loop_state_var) {
              case 1U:
                {
                    var_32 = *var_2;
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4227037UL;
                    indirect_placeholder();
                    spec_select = ((uint64_t)(uint32_t)var_32 == 0UL) ? 0UL : 4294967295UL;
                    rax_0 = spec_select;
                }
                break;
              case 0U:
                {
                    return rax_0;
                }
                break;
            }
        }
        break;
    }
}
