typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_17_ret_type;
struct indirect_placeholder_18_ret_type;
struct indirect_placeholder_19_ret_type;
struct indirect_placeholder_20_ret_type;
struct indirect_placeholder_21_ret_type;
struct indirect_placeholder_17_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_18_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_19_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_20_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_21_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_r10(void);
extern void indirect_placeholder_6(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_5(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
extern uint64_t init_rcx(void);
extern void indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_16(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_17_ret_type indirect_placeholder_17(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_18_ret_type indirect_placeholder_18(uint64_t param_0);
extern struct indirect_placeholder_19_ret_type indirect_placeholder_19(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_20_ret_type indirect_placeholder_20(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_21_ret_type indirect_placeholder_21(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
void bb_set_fields(uint64_t rsi, uint64_t rdi) {
    uint64_t local_sp_1;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint32_t *var_11;
    uint64_t *var_12;
    uint64_t *var_13;
    unsigned char *var_14;
    unsigned char *var_15;
    unsigned char *var_16;
    unsigned char *var_17;
    uint64_t var_81;
    uint64_t var_62;
    uint64_t rcx_0;
    uint64_t local_sp_0;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t r9_1;
    uint64_t var_83;
    uint64_t r8_1;
    uint64_t rbx_1;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t rcx_1;
    uint64_t local_sp_2;
    uint64_t var_68;
    uint64_t rcx_2;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t *var_74;
    uint64_t var_75;
    unsigned char var_27;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_82;
    uint64_t rcx_3;
    uint64_t local_sp_3;
    uint64_t r10_0;
    uint64_t r9_0;
    uint64_t r8_0;
    uint64_t rbx_0;
    uint64_t rcx_4;
    uint64_t local_sp_4;
    uint64_t r10_1;
    uint64_t rcx_5_ph;
    uint64_t spec_select;
    uint64_t var_59;
    uint64_t *var_60;
    uint64_t *var_61;
    uint64_t local_sp_6;
    uint64_t var_42;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t local_sp_5_ph260;
    uint64_t var_31;
    uint64_t var_32;
    struct indirect_placeholder_17_ret_type var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t storemerge11;
    uint64_t var_37;
    struct indirect_placeholder_18_ret_type var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t storemerge12;
    uint64_t local_sp_5_ph;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t local_sp_5_ph258;
    uint64_t r10_2_ph;
    uint64_t r9_2_ph;
    uint64_t r8_2_ph;
    uint64_t rbx_2_ph;
    unsigned char **var_20;
    unsigned char var_21;
    uint64_t spec_select294;
    bool var_22;
    uint64_t spec_select295;
    uint64_t storemerge14;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_43;
    struct indirect_placeholder_19_ret_type var_44;
    uint64_t var_45;
    uint64_t var_46;
    bool var_47;
    uint64_t var_48;
    uint64_t *var_49;
    struct indirect_placeholder_20_ret_type var_50;
    uint64_t var_51;
    uint64_t spec_select296;
    uint64_t var_52;
    struct indirect_placeholder_21_ret_type var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rcx();
    var_3 = init_cc_src2();
    var_4 = init_r10();
    var_5 = init_r9();
    var_6 = init_r8();
    var_7 = init_rbx();
    var_8 = var_0 + (-8L);
    *(uint64_t *)var_8 = var_1;
    var_9 = var_0 + (-80L);
    var_10 = (uint64_t *)var_9;
    *var_10 = rdi;
    var_11 = (uint32_t *)(var_0 + (-84L));
    *var_11 = (uint32_t)rsi;
    var_12 = (uint64_t *)(var_0 + (-16L));
    *var_12 = 1UL;
    var_13 = (uint64_t *)(var_0 + (-64L));
    *var_13 = 0UL;
    var_14 = (unsigned char *)(var_0 + (-17L));
    *var_14 = (unsigned char)'\x00';
    var_15 = (unsigned char *)(var_0 + (-18L));
    *var_15 = (unsigned char)'\x00';
    var_16 = (unsigned char *)(var_0 + (-19L));
    *var_16 = (unsigned char)'\x00';
    var_17 = (unsigned char *)(var_0 + (-20L));
    *var_17 = (unsigned char)'\x00';
    var_62 = 0UL;
    rcx_0 = 4208353UL;
    rcx_5_ph = var_2;
    r10_2_ph = var_4;
    r9_2_ph = var_5;
    r8_2_ph = var_6;
    rbx_2_ph = var_7;
    storemerge14 = 1UL;
    if ((*var_11 & 1U) == 0U) {
        local_sp_5_ph = var_0 + (-88L);
    } else {
        var_18 = *var_10;
        var_19 = var_0 + (-96L);
        *(uint64_t *)var_19 = 4208822UL;
        indirect_placeholder();
        local_sp_5_ph = var_19;
        if ((uint64_t)(uint32_t)var_18 == 0UL) {
            *var_13 = 1UL;
            *var_14 = (unsigned char)'\x01';
            *var_16 = (unsigned char)'\x01';
            *var_10 = (*var_10 + 1UL);
        }
    }
    local_sp_5_ph258 = local_sp_5_ph;
    while (1U)
        {
            local_sp_5_ph260 = local_sp_5_ph258;
            while (1U)
                {
                    local_sp_6 = local_sp_5_ph260;
                    while (1U)
                        {
                            var_20 = (unsigned char **)var_9;
                            var_21 = **var_20;
                            switch_state_var = 0;
                            switch (var_21) {
                              case '-':
                                {
                                    *var_17 = (unsigned char)'\x00';
                                    if (*var_16 == '\x00') {
                                        spec_select294 = ((*var_11 & 4U) == 0U) ? 4279424UL : 4279392UL;
                                        *(uint64_t *)(local_sp_5_ph260 + (-8L)) = 4208917UL;
                                        indirect_placeholder_4(0UL, rcx_5_ph, spec_select294, 0UL, 0UL, r9_2_ph, r8_2_ph);
                                        *(uint64_t *)(local_sp_5_ph260 + (-16L)) = 4208927UL;
                                        indirect_placeholder_6(var_8, 1UL);
                                        abort();
                                    }
                                    *var_16 = (unsigned char)'\x01';
                                    *var_10 = (*var_10 + 1UL);
                                    var_22 = (*var_14 == '\x00');
                                    if (var_22) {
                                        if (var_22) {
                                            storemerge14 = *var_13;
                                        }
                                        *var_12 = storemerge14;
                                        *var_13 = 0UL;
                                        continue;
                                    }
                                    if (*var_13 == 0UL) {
                                        spec_select295 = ((*var_11 & 4U) == 0U) ? 4279493UL : 4279448UL;
                                        *(uint64_t *)(local_sp_5_ph260 + (-8L)) = 4208996UL;
                                        indirect_placeholder_4(0UL, rcx_5_ph, spec_select295, 0UL, 0UL, r9_2_ph, r8_2_ph);
                                        *(uint64_t *)(local_sp_5_ph260 + (-16L)) = 4209006UL;
                                        indirect_placeholder_6(var_8, 1UL);
                                        abort();
                                    }
                                }
                                break;
                              case ',':
                                {
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              default:
                                {
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                            }
                            if (switch_state_var)
                                break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 0U:
                        {
                            var_23 = (uint64_t)(uint32_t)(uint64_t)var_21;
                            *(uint64_t *)(local_sp_5_ph260 + (-8L)) = 4209068UL;
                            var_24 = indirect_placeholder_2(var_23);
                            var_25 = (uint64_t)(unsigned char)var_24;
                            var_26 = local_sp_5_ph260 + (-16L);
                            *(uint64_t *)var_26 = 4209078UL;
                            indirect_placeholder();
                            local_sp_5_ph260 = var_26;
                            local_sp_6 = var_26;
                            var_27 = **var_20;
                            if (!(var_25 == 0UL && var_27 == '\x00')) {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            if ((uint64_t)(((uint32_t)(uint64_t)var_27 + (-48)) & (-2)) > 9UL) {
                                var_37 = *var_10;
                                *(uint64_t *)(local_sp_5_ph260 + (-24L)) = 4209772UL;
                                var_38 = indirect_placeholder_18(var_37);
                                var_39 = var_38.field_0;
                                var_40 = var_38.field_1;
                                var_41 = var_38.field_2;
                                storemerge12 = ((*var_11 & 4U) == 0U) ? 4279699UL : 4279664UL;
                                *(uint64_t *)(local_sp_5_ph260 + (-32L)) = 4209817UL;
                                indirect_placeholder_4(0UL, var_39, storemerge12, 0UL, 0UL, var_40, var_41);
                                *(uint64_t *)(local_sp_5_ph260 + (-40L)) = 4209827UL;
                                indirect_placeholder_6(var_8, 1UL);
                                abort();
                            }
                            if (*var_17 == '\x01') {
                                *(uint64_t *)4297776UL = *var_10;
                            } else {
                                if (*(uint64_t *)4297776UL == 0UL) {
                                    *(uint64_t *)4297776UL = *var_10;
                                }
                            }
                            *var_17 = (unsigned char)'\x01';
                            if (*var_16 == '\x00') {
                                *var_14 = (unsigned char)'\x01';
                            } else {
                                *var_15 = (unsigned char)'\x01';
                            }
                            var_28 = *var_13;
                            if (var_28 <= 1844674407370955161UL) {
                                loop_state_var = 0U;
                                switch_state_var = 1;
                                break;
                            }
                            var_29 = helper_cc_compute_c_wrapper(((var_28 * 10UL) + (uint64_t)((long)(((uint64_t)**var_20 << 32UL) + (-206158430208L)) >> (long)32UL)) - var_28, var_28, var_3, 17U);
                            if (var_29 != 0UL) {
                                loop_state_var = 0U;
                                switch_state_var = 1;
                                break;
                            }
                            var_30 = (uint64_t)((long)(((uint64_t)**var_20 << 32UL) + (-206158430208L)) >> (long)32UL) + (*var_13 * 10UL);
                            *var_13 = var_30;
                            if (var_30 != 18446744073709551615UL) {
                                loop_state_var = 0U;
                                switch_state_var = 1;
                                break;
                            }
                            *var_10 = (*var_10 + 1UL);
                            continue;
                        }
                        break;
                      case 1U:
                        {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    var_31 = *(uint64_t *)4297776UL;
                    *(uint64_t *)(local_sp_5_ph260 + (-24L)) = 4209641UL;
                    indirect_placeholder();
                    *(uint64_t *)(var_0 + (-48L)) = var_31;
                    *(uint64_t *)(local_sp_5_ph260 + (-32L)) = 4209667UL;
                    var_32 = indirect_placeholder_5();
                    *(uint64_t *)(var_0 + (-56L)) = var_32;
                    *(uint64_t *)(local_sp_5_ph260 + (-40L)) = 4209683UL;
                    var_33 = indirect_placeholder_17(var_31, var_32);
                    var_34 = var_33.field_0;
                    var_35 = var_33.field_1;
                    var_36 = var_33.field_2;
                    storemerge11 = ((*var_11 & 4U) == 0U) ? 4279630UL : 4279592UL;
                    *(uint64_t *)(local_sp_5_ph260 + (-48L)) = 4209728UL;
                    indirect_placeholder_4(0UL, var_34, storemerge11, 0UL, 0UL, var_35, var_36);
                    *(uint64_t *)(local_sp_5_ph260 + (-56L)) = 4209740UL;
                    indirect_placeholder();
                    *(uint64_t *)(local_sp_5_ph260 + (-64L)) = 4209750UL;
                    indirect_placeholder_6(var_8, 1UL);
                    abort();
                }
                break;
              case 1U:
                {
                    *var_17 = (unsigned char)'\x00';
                    if (*var_16 == '\x00') {
                        var_51 = *var_13;
                        if (var_51 != 0UL) {
                            spec_select296 = ((*var_11 & 4U) == 0U) ? 4279493UL : 4279448UL;
                            *(uint64_t *)(local_sp_6 + (-8L)) = 4209352UL;
                            indirect_placeholder_4(0UL, rcx_5_ph, spec_select296, 0UL, 0UL, r9_2_ph, r8_2_ph);
                            *(uint64_t *)(local_sp_6 + (-16L)) = 4209362UL;
                            indirect_placeholder_6(var_8, 1UL);
                            abort();
                        }
                        var_52 = local_sp_6 + (-8L);
                        *(uint64_t *)var_52 = 4209381UL;
                        var_53 = indirect_placeholder_21(rcx_5_ph, var_51, var_51, r10_2_ph, r9_2_ph, r8_2_ph, rbx_2_ph);
                        var_54 = var_53.field_0;
                        var_55 = var_53.field_1;
                        var_56 = var_53.field_2;
                        var_57 = var_53.field_3;
                        var_58 = var_53.field_4;
                        *var_13 = 0UL;
                        r9_1 = var_56;
                        r8_1 = var_57;
                        rbx_1 = var_58;
                        rcx_4 = var_54;
                        local_sp_4 = var_52;
                        r10_1 = var_55;
                    } else {
                        *var_16 = (unsigned char)'\x00';
                        if (*var_14 != '\x01' & *var_15 != '\x01') {
                            if ((*var_11 & 1U) != 0U) {
                                *(uint64_t *)(local_sp_6 + (-8L)) = 4209182UL;
                                indirect_placeholder_4(0UL, rcx_5_ph, 4279520UL, 0UL, 0UL, r9_2_ph, r8_2_ph);
                                *(uint64_t *)(local_sp_6 + (-16L)) = 4209192UL;
                                indirect_placeholder_6(var_8, 1UL);
                                abort();
                            }
                            *var_12 = 1UL;
                        }
                        if (*var_15 == '\x01') {
                            var_42 = *var_12;
                            var_43 = local_sp_6 + (-8L);
                            *(uint64_t *)var_43 = 4209222UL;
                            var_44 = indirect_placeholder_19(rcx_5_ph, 18446744073709551615UL, var_42, r10_2_ph, r9_2_ph, r8_2_ph, rbx_2_ph);
                            rcx_3 = var_44.field_0;
                            local_sp_3 = var_43;
                            r10_0 = var_44.field_1;
                            r9_0 = var_44.field_2;
                            r8_0 = var_44.field_3;
                            rbx_0 = var_44.field_4;
                        } else {
                            var_45 = *var_13;
                            var_46 = *var_12;
                            var_47 = (var_46 > var_45);
                            var_48 = local_sp_6 + (-8L);
                            var_49 = (uint64_t *)var_48;
                            local_sp_3 = var_48;
                            if (!var_47) {
                                *var_49 = 4209259UL;
                                indirect_placeholder_4(0UL, rcx_5_ph, 4279554UL, 0UL, 0UL, r9_2_ph, r8_2_ph);
                                *(uint64_t *)(local_sp_6 + (-16L)) = 4209269UL;
                                indirect_placeholder_6(var_8, 1UL);
                                abort();
                            }
                            *var_49 = 4209288UL;
                            var_50 = indirect_placeholder_20(rcx_5_ph, var_45, var_46, r10_2_ph, r9_2_ph, r8_2_ph, rbx_2_ph);
                            rcx_3 = var_50.field_0;
                            r10_0 = var_50.field_1;
                            r9_0 = var_50.field_2;
                            r8_0 = var_50.field_3;
                            rbx_0 = var_50.field_4;
                        }
                        *var_13 = 0UL;
                        r9_1 = r9_0;
                        r8_1 = r8_0;
                        rbx_1 = rbx_0;
                        rcx_4 = rcx_3;
                        local_sp_4 = local_sp_3;
                        r10_1 = r10_0;
                    }
                    rcx_5_ph = rcx_4;
                    local_sp_5_ph258 = local_sp_4;
                    r10_2_ph = r10_1;
                    r9_2_ph = r9_1;
                    r8_2_ph = r8_1;
                    rbx_2_ph = rbx_1;
                    if (**var_20 == '\x00') {
                        *var_10 = (*var_10 + 1UL);
                        *var_14 = (unsigned char)'\x00';
                        *var_15 = (unsigned char)'\x00';
                        continue;
                    }
                    if (*(uint64_t *)4298216UL != 0UL) {
                        var_59 = local_sp_4 + (-8L);
                        *(uint64_t *)var_59 = 4209932UL;
                        indirect_placeholder();
                        var_60 = (uint64_t *)(var_0 + (-32L));
                        *var_60 = 0UL;
                        var_61 = (uint64_t *)(var_0 + (-40L));
                        local_sp_0 = var_59;
                        switch_state_var = 1;
                        break;
                    }
                    spec_select = ((*var_11 & 4U) == 0U) ? 4279769UL : 4279728UL;
                    *(uint64_t *)(local_sp_4 + (-8L)) = 4209890UL;
                    indirect_placeholder_4(0UL, rcx_4, spec_select, 0UL, 0UL, r9_1, r8_1);
                    *(uint64_t *)(local_sp_4 + (-16L)) = 4209900UL;
                    indirect_placeholder_6(var_8, 1UL);
                    abort();
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    var_63 = *(uint64_t *)4298216UL;
    var_64 = helper_cc_compute_c_wrapper(var_62 - var_63, var_63, var_3, 17U);
    local_sp_1 = local_sp_0;
    rcx_1 = rcx_0;
    local_sp_2 = local_sp_0;
    while (var_64 != 0UL)
        {
            var_65 = *var_60 + 1UL;
            *var_61 = var_65;
            var_66 = var_65;
            var_67 = *(uint64_t *)4298216UL;
            while (1U)
                {
                    var_68 = helper_cc_compute_c_wrapper(var_66 - var_67, var_67, var_3, 17U);
                    local_sp_0 = local_sp_2;
                    rcx_2 = rcx_1;
                    if (var_68 != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_69 = *(uint64_t *)4298208UL;
                    var_70 = var_69 + (*var_61 << 4UL);
                    var_71 = *(uint64_t *)var_70;
                    var_72 = *var_60;
                    var_73 = var_72 << 4UL;
                    var_74 = (uint64_t *)((var_69 + var_73) + 8UL);
                    var_75 = *var_74;
                    var_81 = var_72;
                    rcx_2 = var_73;
                    if (var_71 <= var_75) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_76 = *(uint64_t *)(var_70 + 8UL);
                    var_77 = helper_cc_compute_c_wrapper(var_75 - var_76, var_76, var_3, 17U);
                    *var_74 = ((var_77 == 0UL) ? var_75 : var_76);
                    var_78 = ((*var_61 << 4UL) + 16UL) + *(uint64_t *)4298208UL;
                    var_79 = local_sp_2 + (-8L);
                    *(uint64_t *)var_79 = 4210160UL;
                    indirect_placeholder();
                    var_80 = *(uint64_t *)4298216UL + (-1L);
                    *(uint64_t *)4298216UL = var_80;
                    var_66 = *var_61;
                    var_67 = var_80;
                    rcx_1 = var_78;
                    local_sp_2 = var_79;
                    continue;
                }
            switch (loop_state_var) {
              case 0U:
                {
                    var_81 = *var_60;
                }
                break;
              case 1U:
                {
                }
                break;
            }
            var_82 = var_81 + 1UL;
            *var_60 = var_82;
            var_62 = var_82;
            rcx_0 = rcx_2;
            var_63 = *(uint64_t *)4298216UL;
            var_64 = helper_cc_compute_c_wrapper(var_62 - var_63, var_63, var_3, 17U);
            local_sp_1 = local_sp_0;
            rcx_1 = rcx_0;
            local_sp_2 = local_sp_0;
        }
    if ((*var_11 & 2U) == 0U) {
        var_84 = *(uint64_t *)4298216UL + 1UL;
        *(uint64_t *)4298216UL = var_84;
        var_85 = var_84 << 4UL;
        var_86 = *(uint64_t *)4298208UL;
        *(uint64_t *)(local_sp_1 + (-8L)) = 4210295UL;
        var_87 = indirect_placeholder_1(var_85, var_86);
        *(uint64_t *)4298208UL = var_87;
        *(uint64_t *)((var_87 + ((*(uint64_t *)4298216UL << 4UL) + (-16L))) + 8UL) = 18446744073709551615UL;
        *(uint64_t *)(*(uint64_t *)4298208UL + ((*(uint64_t *)4298216UL << 4UL) + (-16L))) = 18446744073709551615UL;
        return;
    }
    var_83 = local_sp_0 + (-8L);
    *(uint64_t *)var_83 = 4210245UL;
    indirect_placeholder_16(rcx_0, r10_1, r9_1, r8_1, rbx_1);
    local_sp_1 = var_83;
    var_84 = *(uint64_t *)4298216UL + 1UL;
    *(uint64_t *)4298216UL = var_84;
    var_85 = var_84 << 4UL;
    var_86 = *(uint64_t *)4298208UL;
    *(uint64_t *)(local_sp_1 + (-8L)) = 4210295UL;
    var_87 = indirect_placeholder_1(var_85, var_86);
    *(uint64_t *)4298208UL = var_87;
    *(uint64_t *)((var_87 + ((*(uint64_t *)4298216UL << 4UL) + (-16L))) + 8UL) = 18446744073709551615UL;
    *(uint64_t *)(*(uint64_t *)4298208UL + ((*(uint64_t *)4298216UL << 4UL) + (-16L))) = 18446744073709551615UL;
    return;
}
