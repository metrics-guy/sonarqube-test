typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_12_ret_type;
struct indirect_placeholder_12_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_11(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_12_ret_type indirect_placeholder_12(uint64_t param_0);
uint64_t bb_getndelim2(uint64_t rcx, uint64_t rdx, uint64_t rsi, uint64_t rdi, uint64_t r9, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    uint32_t *var_8;
    uint32_t *var_9;
    uint64_t *var_10;
    uint64_t **var_11;
    uint64_t var_12;
    uint64_t *var_13;
    uint64_t **var_14;
    uint64_t var_15;
    uint64_t *var_16;
    uint64_t var_22;
    uint64_t var_74;
    uint64_t local_sp_1;
    uint64_t var_63;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t local_sp_6;
    uint64_t local_sp_0;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t storemerge6;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_61;
    uint64_t var_60;
    uint64_t var_62;
    uint64_t var_65;
    uint64_t var_23;
    uint64_t var_66;
    uint64_t _pre129;
    uint64_t var_64;
    uint64_t var_67;
    uint64_t var_69;
    uint64_t var_68;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_75;
    uint64_t local_sp_3;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t local_sp_2;
    uint64_t local_sp_4;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    struct indirect_placeholder_12_ret_type var_20;
    uint64_t var_21;
    uint64_t local_sp_5;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t *var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t *var_29;
    uint32_t var_30;
    bool var_31;
    uint32_t var_32;
    unsigned char *var_33;
    uint64_t var_34;
    uint64_t *var_35;
    uint64_t *var_36;
    uint64_t *var_37;
    uint32_t *var_38;
    uint64_t *var_39;
    uint64_t *var_40;
    uint64_t *var_41;
    uint64_t *var_42;
    uint64_t *var_43;
    uint64_t var_44;
    uint32_t var_45;
    uint64_t var_83;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_3 = var_0 + (-168L);
    var_4 = var_0 + (-128L);
    *(uint64_t *)var_4 = rdi;
    var_5 = var_0 + (-136L);
    *(uint64_t *)var_5 = rsi;
    var_6 = (uint64_t *)(var_0 + (-144L));
    *var_6 = rdx;
    var_7 = (uint64_t *)(var_0 + (-152L));
    *var_7 = rcx;
    var_8 = (uint32_t *)(var_0 + (-156L));
    *var_8 = (uint32_t)r8;
    var_9 = (uint32_t *)(var_0 + (-160L));
    *var_9 = (uint32_t)r9;
    var_10 = (uint64_t *)(var_0 + (-32L));
    *var_10 = 18446744073709551615UL;
    var_11 = (uint64_t **)var_4;
    var_12 = **var_11;
    var_13 = (uint64_t *)(var_0 + (-40L));
    *var_13 = var_12;
    var_14 = (uint64_t **)var_5;
    var_15 = **var_14;
    var_16 = (uint64_t *)(var_0 + (-48L));
    *var_16 = var_15;
    var_22 = var_15;
    local_sp_5 = var_3;
    if (*var_13 != 0UL) {
        var_17 = *var_7;
        var_18 = (var_17 > 64UL) ? 64UL : var_17;
        *var_16 = var_18;
        var_19 = var_0 + (-176L);
        *(uint64_t *)var_19 = 4210829UL;
        var_20 = indirect_placeholder_12(var_18);
        var_21 = var_20.field_0;
        *var_13 = var_21;
        local_sp_5 = var_19;
        if (var_21 != 0UL) {
            return 18446744073709551615UL;
        }
        var_22 = *var_16;
    }
    var_23 = *var_6;
    var_24 = helper_cc_compute_c_wrapper(var_22 - var_23, var_23, var_2, 17U);
    local_sp_6 = local_sp_5;
    if (var_24 == 0UL) {
        **var_11 = *var_13;
        **var_14 = *var_16;
        var_83 = *var_10;
        return (var_83 == 0UL) ? 18446744073709551615UL : var_83;
    }
    var_25 = *var_16 - *var_6;
    var_26 = (uint64_t *)(var_0 + (-16L));
    *var_26 = var_25;
    var_27 = *var_6 + *var_13;
    var_28 = var_0 + (-24L);
    var_29 = (uint64_t *)var_28;
    *var_29 = var_27;
    if (*var_26 != 0UL) {
        if (*var_7 > *var_16) {
            **var_11 = *var_13;
            **var_14 = *var_16;
            var_83 = *var_10;
            return (var_83 == 0UL) ? 18446744073709551615UL : var_83;
        }
    }
    var_30 = *var_8;
    var_31 = (var_30 == 4294967295U);
    var_32 = *var_9;
    if (var_31) {
        *var_8 = var_32;
    } else {
        if (var_32 == 4294967295U) {
            *var_9 = var_30;
        }
    }
    var_33 = (unsigned char *)(var_0 + (-49L));
    *var_33 = (unsigned char)'\x00';
    var_34 = var_0 + (-112L);
    var_35 = (uint64_t *)(var_0 + (-80L));
    var_36 = (uint64_t *)var_34;
    var_37 = (uint64_t *)(var_0 + (-88L));
    var_38 = (uint32_t *)(var_0 + (-56L));
    var_39 = (uint64_t *)(var_0 + (-64L));
    var_40 = (uint64_t *)(var_0 + (-96L));
    var_41 = (uint64_t *)(var_0 + (-104L));
    var_42 = (uint64_t *)(var_0 + (-72L));
    var_43 = (uint64_t *)(var_0 | 8UL);
    while (1U)
        {
            var_44 = local_sp_6 + (-8L);
            *(uint64_t *)var_44 = 4210990UL;
            indirect_placeholder();
            *var_35 = var_34;
            var_45 = *var_8;
            local_sp_0 = var_44;
            var_46 = *var_36;
            var_47 = (uint64_t)*var_9;
            var_48 = (uint64_t)var_45;
            var_49 = local_sp_6 + (-16L);
            *(uint64_t *)var_49 = 4211042UL;
            var_50 = indirect_placeholder_11(var_46, var_47, var_48);
            *var_37 = var_50;
            local_sp_0 = var_49;
            if (var_45 != 4294967295U & var_50 == 0UL) {
                *var_36 = ((var_50 - *var_35) + 1UL);
                *var_33 = (unsigned char)'\x01';
            }
            var_51 = *var_36 + 1UL;
            var_52 = helper_cc_compute_c_wrapper(*var_26 - var_51, var_51, var_2, 17U);
            local_sp_1 = local_sp_0;
            var_53 = *var_16;
            var_54 = *var_7;
            var_55 = helper_cc_compute_c_wrapper(var_53 - var_54, var_54, var_2, 17U);
            if (var_52 != 0UL & var_55 != 0UL) {
                var_56 = *var_16;
                storemerge6 = (var_56 > 63UL) ? (var_56 << 1UL) : (var_56 + 64UL);
                *var_39 = storemerge6;
                var_57 = storemerge6 + (*var_13 - *var_29);
                var_58 = *var_36 + 1UL;
                var_59 = helper_cc_compute_c_wrapper(var_57 - var_58, var_58, var_2, 17U);
                if (var_59 == 0UL) {
                    var_61 = *var_39;
                } else {
                    var_60 = (*var_36 + (*var_29 - *var_13)) + 1UL;
                    *var_39 = var_60;
                    var_61 = var_60;
                }
                var_62 = helper_cc_compute_c_wrapper(*var_16 - var_61, var_61, var_2, 17U);
                if (var_62 == 0UL) {
                    _pre129 = *var_7;
                    var_65 = _pre129;
                    *var_39 = var_65;
                    var_66 = var_65;
                } else {
                    var_63 = *var_39;
                    var_64 = *var_7;
                    var_65 = var_64;
                    var_66 = var_63;
                    if (var_63 > var_64) {
                        *var_39 = var_65;
                        var_66 = var_65;
                    }
                }
                var_67 = *var_6;
                var_69 = var_66;
                if ((long)(var_66 - var_67) <= (long)18446744073709551615UL) {
                    var_68 = var_67 ^ (-9223372036854775808L);
                    *var_40 = var_68;
                    var_69 = var_68;
                    if (*var_16 != var_68) {
                        break;
                    }
                    *var_39 = var_68;
                }
                *var_26 = (var_69 + (*var_13 - *var_29));
                var_70 = *var_39;
                var_71 = *var_13;
                var_72 = local_sp_0 + (-8L);
                *(uint64_t *)var_72 = 4211406UL;
                var_73 = indirect_placeholder_1(var_70, var_71);
                *var_41 = var_73;
                local_sp_1 = var_72;
                if (var_73 != 0UL) {
                    break;
                }
                *var_13 = var_73;
                var_74 = *var_39;
                *var_16 = var_74;
                *var_29 = (*var_13 + (var_74 - *var_26));
            }
            var_75 = *var_26;
            local_sp_2 = local_sp_1;
            local_sp_3 = local_sp_1;
            if (var_75 <= 1UL) {
                var_76 = var_75 + (-1L);
                *var_42 = var_76;
                var_77 = *var_36;
                *var_42 = ((var_76 > var_77) ? var_77 : var_76);
                if (*var_35 == 0UL) {
                    **(unsigned char **)var_28 = (unsigned char)*var_38;
                } else {
                    var_78 = local_sp_1 + (-8L);
                    *(uint64_t *)var_78 = 4211526UL;
                    indirect_placeholder();
                    local_sp_2 = var_78;
                }
                *var_29 = (*var_29 + *var_42);
                *var_26 = (*var_26 - *var_42);
                local_sp_3 = local_sp_2;
            }
            local_sp_4 = local_sp_3;
            if (*var_35 != 0UL) {
                var_79 = *var_36;
                var_80 = *var_43;
                var_81 = local_sp_3 + (-8L);
                *(uint64_t *)var_81 = 4211578UL;
                var_82 = indirect_placeholder_1(var_79, var_80);
                local_sp_4 = var_81;
                if ((uint64_t)(uint32_t)var_82 == 0UL) {
                    break;
                }
            }
            local_sp_6 = local_sp_4;
            if (*var_33 == '\x01') {
                continue;
            }
            **(unsigned char **)var_28 = (unsigned char)'\x00';
            *var_10 = (*var_29 - (*var_13 + *var_6));
            break;
        }
}
