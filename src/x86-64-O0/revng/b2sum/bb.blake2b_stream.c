typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_32_ret_type;
struct indirect_placeholder_32_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_25(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_32_ret_type indirect_placeholder_32(uint64_t param_0);
uint64_t bb_blake2b_stream(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_25;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t *var_2;
    uint64_t *var_3;
    uint64_t *var_4;
    uint32_t *var_5;
    uint64_t var_6;
    struct indirect_placeholder_32_ret_type var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t storemerge;
    uint64_t local_sp_2;
    uint64_t local_sp_0;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_26;
    uint64_t var_27;
    bool var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t *var_21;
    uint64_t var_15;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t local_sp_3;
    uint64_t local_sp_4;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t *var_13;
    uint64_t *var_14;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = (uint64_t *)(var_0 + (-304L));
    *var_2 = rdi;
    var_3 = (uint64_t *)(var_0 + (-312L));
    *var_3 = rsi;
    var_4 = (uint64_t *)(var_0 + (-320L));
    *var_4 = rdx;
    var_5 = (uint32_t *)(var_0 + (-12L));
    *var_5 = 4294967295U;
    var_6 = *(uint64_t *)4317248UL;
    *(uint64_t *)(var_0 + (-336L)) = 4241464UL;
    var_7 = indirect_placeholder_32(var_6);
    var_8 = var_7.field_0;
    var_9 = (uint64_t *)(var_0 + (-32L));
    *var_9 = var_8;
    var_15 = 0UL;
    storemerge = 4294967295UL;
    if (var_8 == 0UL) {
        return storemerge;
    }
    var_10 = *var_4;
    var_11 = var_0 + (-296L);
    var_12 = var_0 + (-344L);
    *(uint64_t *)var_12 = 4241510UL;
    indirect_placeholder_1(var_11, var_10);
    var_13 = (uint64_t *)(var_0 + (-24L));
    var_14 = (uint64_t *)(var_0 + (-40L));
    local_sp_3 = var_12;
    while (1U)
        {
            *var_13 = 0UL;
            local_sp_4 = local_sp_3;
            while (1U)
                {
                    var_16 = *(uint64_t *)4317248UL - var_15;
                    *(uint64_t *)(local_sp_4 + (-8L)) = 4241564UL;
                    indirect_placeholder();
                    *var_14 = var_16;
                    var_17 = *var_13 + var_16;
                    *var_13 = var_17;
                    if (var_17 != *(uint64_t *)4317248UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_18 = (*var_14 == 0UL);
                    var_19 = *var_2;
                    var_20 = local_sp_4 + (-16L);
                    var_21 = (uint64_t *)var_20;
                    local_sp_0 = var_20;
                    local_sp_2 = var_20;
                    local_sp_4 = var_20;
                    if (!var_18) {
                        *var_21 = 4241611UL;
                        indirect_placeholder();
                        if ((uint64_t)(uint32_t)var_19 != 0UL) {
                            loop_state_var = 2U;
                            break;
                        }
                        loop_state_var = 1U;
                        break;
                    }
                    *var_21 = 4241635UL;
                    indirect_placeholder();
                    if ((uint64_t)(uint32_t)var_19 != 0UL) {
                        loop_state_var = 2U;
                        break;
                    }
                    var_15 = *var_13;
                    continue;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    var_28 = *var_9;
                    var_29 = local_sp_4 + (-16L);
                    *(uint64_t *)var_29 = 4241671UL;
                    indirect_placeholder_25(var_17, var_11, var_28);
                    local_sp_3 = var_29;
                    continue;
                }
                break;
              case 2U:
                {
                    var_22 = *var_13;
                    if (var_22 != 0UL) {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    var_23 = *var_9;
                    var_24 = var_20 + (-8L);
                    *(uint64_t *)var_24 = 4241713UL;
                    indirect_placeholder_25(var_22, var_11, var_23);
                    local_sp_2 = var_24;
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 1U:
                {
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(uint64_t *)(local_sp_0 + (-8L)) = 4241764UL;
            indirect_placeholder();
            storemerge = (uint64_t)*var_5;
        }
        break;
      case 1U:
        {
            var_25 = *var_4;
            var_26 = *var_3;
            var_27 = local_sp_2 + (-8L);
            *(uint64_t *)var_27 = 4241745UL;
            indirect_placeholder_25(var_25, var_11, var_26);
            *var_5 = 0U;
            local_sp_0 = var_27;
        }
        break;
    }
}
