typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_9_ret_type;
struct indirect_placeholder_8_ret_type;
struct indirect_placeholder_11_ret_type;
struct indirect_placeholder_10_ret_type;
struct indirect_placeholder_13_ret_type;
struct indirect_placeholder_7_ret_type;
struct indirect_placeholder_6_ret_type;
struct indirect_placeholder_5_ret_type;
struct indirect_placeholder_4_ret_type;
struct indirect_placeholder_15_ret_type;
struct indirect_placeholder_3_ret_type;
struct indirect_placeholder_18_ret_type;
struct indirect_placeholder_19_ret_type;
struct indirect_placeholder_17_ret_type;
struct indirect_placeholder_23_ret_type;
struct indirect_placeholder_22_ret_type;
struct indirect_placeholder_24_ret_type;
struct indirect_placeholder_21_ret_type;
struct indirect_placeholder_9_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_8_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_11_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_10_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_13_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_7_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_6_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_5_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_4_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_15_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_3_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_18_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_19_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_17_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_23_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_22_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_24_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_21_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_20(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t indirect_placeholder_25(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_16(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_12(void);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_14(uint64_t param_0);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_9_ret_type indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_8_ret_type indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_11_ret_type indirect_placeholder_11(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_10_ret_type indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_13_ret_type indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_7_ret_type indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_6_ret_type indirect_placeholder_6(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_5_ret_type indirect_placeholder_5(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_4_ret_type indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_15_ret_type indirect_placeholder_15(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_3_ret_type indirect_placeholder_3(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_18_ret_type indirect_placeholder_18(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_19_ret_type indirect_placeholder_19(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_17_ret_type indirect_placeholder_17(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_23_ret_type indirect_placeholder_23(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_22_ret_type indirect_placeholder_22(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_24_ret_type indirect_placeholder_24(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_21_ret_type indirect_placeholder_21(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
uint64_t bb_digest_check(uint64_t rcx, uint64_t rdi, uint64_t r10, uint64_t r9, uint64_t r8) {
    uint64_t var_57;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    unsigned char *var_9;
    unsigned char *var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t *var_13;
    uint64_t var_14;
    uint64_t var_15;
    bool var_16;
    unsigned char *var_17;
    unsigned char var_18;
    uint64_t r94_4;
    uint64_t var_118;
    struct indirect_placeholder_9_ret_type var_119;
    uint64_t var_120;
    uint64_t var_121;
    uint64_t var_122;
    uint64_t var_123;
    uint64_t var_111;
    struct indirect_placeholder_11_ret_type var_112;
    uint64_t var_113;
    uint64_t var_114;
    uint64_t var_115;
    uint64_t var_133;
    uint64_t local_sp_0;
    uint64_t var_116;
    uint64_t var_117;
    uint64_t var_124;
    struct indirect_placeholder_13_ret_type var_125;
    uint64_t var_126;
    uint64_t var_127;
    uint64_t var_128;
    uint64_t local_sp_1;
    uint64_t var_129;
    uint64_t storemerge7;
    uint64_t var_130;
    uint64_t var_131;
    uint64_t r85_4;
    struct indirect_placeholder_6_ret_type var_132;
    uint64_t r94_0;
    uint64_t r85_0;
    uint64_t local_sp_2;
    uint64_t var_134;
    uint64_t storemerge8;
    uint64_t var_135;
    uint64_t var_136;
    struct indirect_placeholder_5_ret_type var_137;
    uint64_t r94_1;
    uint64_t r85_1;
    uint64_t var_138;
    uint64_t local_sp_3;
    uint64_t var_139;
    uint64_t storemerge9;
    uint64_t var_140;
    uint64_t var_141;
    uint64_t var_142;
    struct indirect_placeholder_15_ret_type var_143;
    uint64_t var_144;
    uint64_t var_145;
    uint64_t var_146;
    uint64_t var_59;
    uint64_t local_sp_16;
    uint64_t var_107;
    uint64_t r94_3;
    uint64_t var_108;
    uint64_t local_sp_17;
    uint64_t var_93;
    uint64_t var_81;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t local_sp_14;
    unsigned char var_77;
    unsigned char storemerge10;
    uint64_t var_76;
    uint64_t local_sp_4;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t local_sp_5;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_94;
    uint64_t local_sp_6_ph;
    uint64_t local_sp_6;
    uint64_t var_95;
    unsigned char var_97;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t local_sp_7;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t var_96;
    uint64_t local_sp_8;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t var_100;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t local_sp_9;
    uint64_t r94_6;
    uint64_t var_58;
    uint64_t local_sp_10;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t local_sp_11;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    struct indirect_placeholder_18_ret_type var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    unsigned char var_75;
    uint64_t var_54;
    uint64_t local_sp_12;
    uint64_t local_sp_13;
    uint64_t var_60;
    struct indirect_placeholder_19_ret_type var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    struct indirect_placeholder_17_ret_type var_65;
    uint64_t r85_6;
    uint64_t r94_2;
    uint64_t r85_2;
    uint64_t var_52;
    uint64_t var_53;
    unsigned char *_cast1;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t r85_3;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t local_sp_18;
    uint64_t local_sp_15;
    uint64_t var_109;
    uint64_t var_110;
    uint64_t rax_0;
    uint64_t var_147;
    struct indirect_placeholder_23_ret_type var_148;
    uint64_t var_149;
    uint64_t var_150;
    uint64_t var_151;
    uint64_t var_152;
    uint64_t var_19;
    uint64_t *var_20;
    uint64_t *_pre_phi290;
    uint64_t *var_25;
    uint64_t var_26;
    uint64_t *var_27;
    uint64_t var_28;
    uint64_t *var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t *var_33;
    unsigned char *var_34;
    uint64_t var_35;
    unsigned char *var_36;
    unsigned char *var_37;
    uint64_t *var_38;
    uint64_t *var_39;
    uint64_t *var_40;
    uint64_t r94_5;
    uint64_t r85_5;
    uint64_t var_41;
    uint64_t var_42;
    struct indirect_placeholder_24_ret_type var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    struct indirect_placeholder_21_ret_type var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t *var_24;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_cc_src2();
    var_2 = init_rbp();
    var_3 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    var_4 = (uint64_t *)(var_0 + (-256L));
    *var_4 = rdi;
    var_5 = (uint64_t *)(var_0 + (-40L));
    *var_5 = 0UL;
    var_6 = (uint64_t *)(var_0 + (-48L));
    *var_6 = 0UL;
    var_7 = (uint64_t *)(var_0 + (-56L));
    *var_7 = 0UL;
    var_8 = (uint64_t *)(var_0 + (-64L));
    *var_8 = 0UL;
    var_9 = (unsigned char *)(var_0 + (-65L));
    *var_9 = (unsigned char)'\x00';
    var_10 = (unsigned char *)(var_0 + (-66L));
    *var_10 = (unsigned char)'\x00';
    var_11 = var_0 + (-200L);
    *(uint64_t *)(var_0 + (-272L)) = 4207818UL;
    var_12 = indirect_placeholder_2(rcx, var_11, 8UL, r10, r9, r8, var_3);
    var_13 = (uint64_t *)(var_0 + (-104L));
    *var_13 = var_12;
    var_14 = *var_4;
    var_15 = var_0 + (-280L);
    *(uint64_t *)var_15 = 4207842UL;
    indirect_placeholder();
    var_16 = ((uint64_t)(uint32_t)var_14 == 0UL);
    var_17 = (unsigned char *)(var_0 + (-105L));
    var_18 = var_16;
    *var_17 = var_18;
    local_sp_16 = var_15;
    var_81 = 0UL;
    var_77 = (unsigned char)'\x00';
    storemerge10 = (unsigned char)'\x00';
    var_97 = (unsigned char)'\x00';
    rax_0 = 0UL;
    r94_5 = r9;
    r85_5 = r8;
    if (var_18 == '\x00') {
        var_21 = *var_4;
        var_22 = var_0 + (-288L);
        *(uint64_t *)var_22 = 4207907UL;
        var_23 = indirect_placeholder_1(var_21, 4315479UL);
        var_24 = (uint64_t *)(var_0 + (-32L));
        *var_24 = var_23;
        _pre_phi290 = var_24;
        local_sp_16 = var_22;
        if (var_23 != 0UL) {
            var_147 = *var_4;
            *(uint64_t *)(var_0 + (-296L)) = 4207943UL;
            var_148 = indirect_placeholder_23(var_147, 0UL, 3UL);
            var_149 = var_148.field_0;
            var_150 = var_148.field_1;
            var_151 = var_148.field_2;
            *(uint64_t *)(var_0 + (-304L)) = 4207951UL;
            indirect_placeholder();
            var_152 = (uint64_t)*(uint32_t *)var_149;
            *(uint64_t *)(var_0 + (-312L)) = 4207978UL;
            indirect_placeholder_22(0UL, var_149, 4315481UL, 0UL, var_152, var_150, var_151);
            return rax_0;
        }
    }
    *(unsigned char *)4338672UL = (unsigned char)'\x01';
    *var_4 = 4315484UL;
    var_19 = *(uint64_t *)4337224UL;
    var_20 = (uint64_t *)(var_0 + (-32L));
    *var_20 = var_19;
    _pre_phi290 = var_20;
    var_25 = (uint64_t *)(var_0 + (-80L));
    *var_25 = 0UL;
    var_26 = var_0 + (-208L);
    var_27 = (uint64_t *)var_26;
    *var_27 = 0UL;
    var_28 = var_0 + (-216L);
    *(uint64_t *)var_28 = 0UL;
    var_29 = (uint64_t *)(var_0 + (-88L));
    var_30 = var_0 + (-224L);
    var_31 = var_0 + (-228L);
    var_32 = var_0 + (-240L);
    var_33 = (uint64_t *)var_30;
    var_34 = (unsigned char *)(var_0 + (-106L));
    var_35 = var_0 + (-241L);
    var_36 = (unsigned char *)(var_0 + (-107L));
    var_37 = (unsigned char *)var_35;
    var_38 = (uint64_t *)(var_0 + (-120L));
    var_39 = (uint64_t *)(var_0 + (-96L));
    var_40 = (uint64_t *)var_32;
    r85_2 = var_30;
    rax_0 = 1UL;
    local_sp_17 = local_sp_16;
    while (1U)
        {
            var_41 = *var_25 + 1UL;
            *var_25 = var_41;
            local_sp_18 = local_sp_17;
            r94_6 = r94_5;
            r85_6 = r85_5;
            if (var_41 == 0UL) {
                var_42 = *var_4;
                *(uint64_t *)(local_sp_17 + (-8L)) = 4208055UL;
                var_43 = indirect_placeholder_24(var_42, 0UL, 3UL);
                var_44 = var_43.field_0;
                var_45 = var_43.field_1;
                var_46 = var_43.field_2;
                var_47 = local_sp_17 + (-16L);
                *(uint64_t *)var_47 = 4208083UL;
                var_48 = indirect_placeholder_21(0UL, var_44, 4315499UL, 1UL, 0UL, var_45, var_46);
                local_sp_18 = var_47;
                r94_6 = var_48.field_2;
                r85_6 = var_48.field_3;
            }
            var_49 = *_pre_phi290;
            var_50 = local_sp_18 + (-8L);
            *(uint64_t *)var_50 = 4208112UL;
            var_51 = indirect_placeholder_25(var_49, var_26, var_28);
            *var_29 = var_51;
            r94_4 = r94_6;
            r85_4 = r85_6;
            r94_3 = r94_6;
            local_sp_14 = var_50;
            r94_2 = r94_6;
            var_55 = var_51;
            r85_3 = r85_6;
            local_sp_15 = var_50;
            if ((long)var_51 > (long)0UL) {
                break;
            }
            if (**(unsigned char **)var_26 == '#') {
                var_105 = *_pre_phi290;
                var_106 = local_sp_14 + (-8L);
                *(uint64_t *)var_106 = 4208949UL;
                indirect_placeholder();
                r94_4 = r94_3;
                r85_4 = r85_3;
                local_sp_15 = var_106;
                r94_5 = r94_3;
                r85_5 = r85_3;
                if ((uint64_t)(uint32_t)var_105 == 0UL) {
                    break;
                }
                var_107 = *_pre_phi290;
                var_108 = local_sp_14 + (-16L);
                *(uint64_t *)var_108 = 4208965UL;
                indirect_placeholder();
                local_sp_15 = var_108;
                local_sp_17 = var_108;
                if ((uint64_t)(uint32_t)var_107 != 0UL) {
                    continue;
                }
                break;
            }
            var_52 = *var_27;
            var_53 = var_51 + (-1L);
            _cast1 = (unsigned char *)(var_52 + var_53);
            var_54 = var_52;
            if (*_cast1 == '\n') {
                *var_29 = var_53;
                *_cast1 = (unsigned char)'\x00';
                var_54 = *var_27;
                var_55 = *var_29;
            }
            var_56 = local_sp_18 + (-16L);
            *(uint64_t *)var_56 = 4208235UL;
            var_57 = indirect_placeholder_20(var_31, var_32, var_54, var_55, var_30);
            local_sp_9 = var_56;
            local_sp_12 = var_56;
            if ((uint64_t)(unsigned char)var_57 == 1UL) {
                if (*var_17 != '\x00') {
                    var_58 = *var_33;
                    var_59 = local_sp_18 + (-24L);
                    *(uint64_t *)var_59 = 4208268UL;
                    indirect_placeholder();
                    local_sp_9 = var_59;
                    local_sp_12 = var_59;
                    if ((uint64_t)(uint32_t)var_58 != 0UL) {
                        *var_5 = (*var_5 + 1UL);
                        local_sp_13 = local_sp_12;
                        if (*(unsigned char *)4338697UL == '\x00') {
                            var_60 = *var_4;
                            *(uint64_t *)(local_sp_12 + (-8L)) = 4208313UL;
                            var_61 = indirect_placeholder_19(var_60, 0UL, 3UL);
                            var_62 = var_61.field_0;
                            var_63 = *var_25;
                            var_64 = local_sp_12 + (-16L);
                            *(uint64_t *)var_64 = 4208354UL;
                            var_65 = indirect_placeholder_17(0UL, var_62, 4315528UL, 0UL, 0UL, 4314151UL, var_63);
                            local_sp_13 = var_64;
                            r94_2 = var_65.field_2;
                            r85_2 = var_65.field_3;
                        }
                        *var_6 = (*var_6 + 1UL);
                        local_sp_14 = local_sp_13;
                        r94_3 = r94_2;
                        r85_3 = r85_2;
                        var_105 = *_pre_phi290;
                        var_106 = local_sp_14 + (-8L);
                        *(uint64_t *)var_106 = 4208949UL;
                        indirect_placeholder();
                        r94_4 = r94_3;
                        r85_4 = r85_3;
                        local_sp_15 = var_106;
                        r94_5 = r94_3;
                        r85_5 = r85_3;
                        if ((uint64_t)(uint32_t)var_105 == 0UL) {
                            break;
                        }
                        var_107 = *_pre_phi290;
                        var_108 = local_sp_14 + (-16L);
                        *(uint64_t *)var_108 = 4208965UL;
                        indirect_placeholder();
                        local_sp_15 = var_108;
                        local_sp_17 = var_108;
                        if ((uint64_t)(uint32_t)var_107 != 0UL) {
                            continue;
                        }
                        break;
                    }
                }
                local_sp_10 = local_sp_9;
                if (*(unsigned char *)4338696UL == '\x01') {
                    local_sp_11 = local_sp_10;
                } else {
                    var_66 = *var_33;
                    var_67 = local_sp_9 + (-8L);
                    *(uint64_t *)var_67 = 4208398UL;
                    indirect_placeholder();
                    local_sp_10 = var_67;
                    local_sp_11 = var_67;
                    storemerge10 = (unsigned char)'\x01';
                    if (var_66 == 0UL) {
                        local_sp_11 = local_sp_10;
                    }
                }
                *var_34 = (storemerge10 & '\x01');
                *var_9 = (unsigned char)'\x01';
                var_68 = *var_33;
                var_69 = *var_13;
                var_70 = local_sp_11 + (-8L);
                *(uint64_t *)var_70 = 4208459UL;
                var_71 = indirect_placeholder_18(var_35, var_69, var_68, var_31);
                var_72 = var_71.field_0;
                var_73 = var_71.field_3;
                var_74 = var_71.field_4;
                var_75 = (unsigned char)var_72;
                *var_36 = var_75;
                r94_3 = var_73;
                local_sp_14 = var_70;
                local_sp_4 = var_70;
                local_sp_5 = var_70;
                r85_3 = var_74;
                if (var_75 == '\x01') {
                    if (*(unsigned char *)4338698UL != '\x00') {
                        if (*var_37 != '\x00') {
                            var_105 = *_pre_phi290;
                            var_106 = local_sp_14 + (-8L);
                            *(uint64_t *)var_106 = 4208949UL;
                            indirect_placeholder();
                            r94_4 = r94_3;
                            r85_4 = r85_3;
                            local_sp_15 = var_106;
                            r94_5 = r94_3;
                            r85_5 = r85_3;
                            if ((uint64_t)(uint32_t)var_105 == 0UL) {
                                break;
                            }
                            var_107 = *_pre_phi290;
                            var_108 = local_sp_14 + (-16L);
                            *(uint64_t *)var_108 = 4208965UL;
                            indirect_placeholder();
                            local_sp_15 = var_108;
                            local_sp_17 = var_108;
                            if ((uint64_t)(uint32_t)var_107 != 0UL) {
                                continue;
                            }
                            break;
                        }
                    }
                    *var_38 = (*(uint64_t *)4338688UL >> 1UL);
                    *var_39 = 0UL;
                    while (1U)
                        {
                            var_82 = *var_38;
                            var_83 = helper_cc_compute_c_wrapper(var_81 - var_82, var_82, var_1, 17U);
                            local_sp_6 = local_sp_5;
                            if (var_83 != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            var_84 = (uint64_t)*(unsigned char *)(*var_40 + (*var_39 << 1UL));
                            var_85 = local_sp_5 + (-8L);
                            *(uint64_t *)var_85 = 4208641UL;
                            indirect_placeholder();
                            var_86 = *var_13;
                            var_87 = *var_39;
                            var_92 = var_87;
                            local_sp_6_ph = var_85;
                            if ((uint64_t)((uint32_t)var_84 - (uint32_t)(uint64_t)*(unsigned char *)((uint64_t)(*(unsigned char *)(var_87 + var_86) >> '\x04') | 4316976UL)) != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            var_88 = (uint64_t)*(unsigned char *)(*var_40 + ((var_87 << 1UL) | 1UL));
                            var_89 = local_sp_5 + (-16L);
                            *(uint64_t *)var_89 = 4208712UL;
                            indirect_placeholder();
                            var_90 = *var_13;
                            var_91 = *var_39;
                            local_sp_5 = var_89;
                            var_92 = var_91;
                            local_sp_6_ph = var_89;
                            if ((uint64_t)((uint32_t)var_88 - (uint32_t)(uint64_t)*(unsigned char *)((uint64_t)(*(unsigned char *)(var_91 + var_90) & '\x0f') | 4316976UL)) != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            var_93 = var_91 + 1UL;
                            *var_39 = var_93;
                            var_81 = var_93;
                            continue;
                        }
                    switch (loop_state_var) {
                      case 0U:
                        {
                            var_94 = var_92;
                            local_sp_6 = local_sp_6_ph;
                        }
                        break;
                      case 1U:
                        {
                            var_94 = *var_39;
                        }
                        break;
                    }
                    local_sp_7 = local_sp_6;
                    local_sp_8 = local_sp_6;
                    local_sp_14 = local_sp_6;
                    if (var_94 == *var_38) {
                        *var_10 = (unsigned char)'\x01';
                    } else {
                        *var_7 = (*var_7 + 1UL);
                    }
                    if (*(unsigned char *)4338696UL != '\x01') {
                        var_95 = *var_39;
                        var_101 = var_95;
                        var_102 = var_95;
                        if (var_95 == *var_38) {
                            if (*(unsigned char *)4338699UL != '\x01') {
                                if (*var_34 != '\x00') {
                                    var_96 = local_sp_6 + (-8L);
                                    *(uint64_t *)var_96 = 4208847UL;
                                    indirect_placeholder();
                                    var_97 = *var_34;
                                    local_sp_8 = var_96;
                                }
                                var_98 = (uint64_t)var_97;
                                var_99 = *var_33;
                                var_100 = local_sp_8 + (-8L);
                                *(uint64_t *)var_100 = 4208868UL;
                                indirect_placeholder_16(var_99, var_98);
                                var_101 = *var_38;
                                var_102 = *var_39;
                                local_sp_7 = var_100;
                            }
                        } else {
                            if (*var_34 == '\x00') {
                                var_96 = local_sp_6 + (-8L);
                                *(uint64_t *)var_96 = 4208847UL;
                                indirect_placeholder();
                                var_97 = *var_34;
                                local_sp_8 = var_96;
                            }
                            var_98 = (uint64_t)var_97;
                            var_99 = *var_33;
                            var_100 = local_sp_8 + (-8L);
                            *(uint64_t *)var_100 = 4208868UL;
                            indirect_placeholder_16(var_99, var_98);
                            var_101 = *var_38;
                            var_102 = *var_39;
                            local_sp_7 = var_100;
                        }
                        local_sp_14 = local_sp_7;
                        if (var_102 == var_101) {
                            var_103 = local_sp_7 + (-8L);
                            *(uint64_t *)var_103 = 4208898UL;
                            indirect_placeholder();
                            local_sp_14 = var_103;
                        } else {
                            if (*(unsigned char *)4338699UL == '\x01') {
                                var_104 = local_sp_7 + (-8L);
                                *(uint64_t *)var_104 = 4208934UL;
                                indirect_placeholder();
                                local_sp_14 = var_104;
                            }
                        }
                    }
                } else {
                    *var_8 = (*var_8 + 1UL);
                    if (*(unsigned char *)4338696UL != '\x01') {
                        if (*var_34 != '\x00') {
                            var_76 = local_sp_11 + (-16L);
                            *(uint64_t *)var_76 = 4208512UL;
                            indirect_placeholder();
                            var_77 = *var_34;
                            local_sp_4 = var_76;
                        }
                        var_78 = (uint64_t)var_77;
                        var_79 = *var_33;
                        *(uint64_t *)(local_sp_4 + (-8L)) = 4208533UL;
                        indirect_placeholder_16(var_79, var_78);
                        var_80 = local_sp_4 + (-16L);
                        *(uint64_t *)var_80 = 4208553UL;
                        indirect_placeholder();
                        local_sp_14 = var_80;
                    }
                }
            } else {
                *var_5 = (*var_5 + 1UL);
                local_sp_13 = local_sp_12;
                if (*(unsigned char *)4338697UL == '\x00') {
                    var_60 = *var_4;
                    *(uint64_t *)(local_sp_12 + (-8L)) = 4208313UL;
                    var_61 = indirect_placeholder_19(var_60, 0UL, 3UL);
                    var_62 = var_61.field_0;
                    var_63 = *var_25;
                    var_64 = local_sp_12 + (-16L);
                    *(uint64_t *)var_64 = 4208354UL;
                    var_65 = indirect_placeholder_17(0UL, var_62, 4315528UL, 0UL, 0UL, 4314151UL, var_63);
                    local_sp_13 = var_64;
                    r94_2 = var_65.field_2;
                    r85_2 = var_65.field_3;
                }
                *var_6 = (*var_6 + 1UL);
                local_sp_14 = local_sp_13;
                r94_3 = r94_2;
                r85_3 = r85_2;
            }
        }
    *(uint64_t *)(local_sp_15 + (-8L)) = 4208991UL;
    indirect_placeholder();
    var_109 = *_pre_phi290;
    var_110 = local_sp_15 + (-16L);
    *(uint64_t *)var_110 = 4209003UL;
    indirect_placeholder();
    local_sp_0 = var_110;
    r94_0 = r94_4;
    r85_0 = r85_4;
    if ((uint64_t)(uint32_t)var_109 != 0UL) {
        var_111 = *var_4;
        *(uint64_t *)(local_sp_15 + (-24L)) = 4209032UL;
        var_112 = indirect_placeholder_11(var_111, 0UL, 3UL);
        var_113 = var_112.field_0;
        var_114 = var_112.field_1;
        var_115 = var_112.field_2;
        *(uint64_t *)(local_sp_15 + (-32L)) = 4209060UL;
        indirect_placeholder_10(0UL, var_113, 4315611UL, 0UL, 0UL, var_114, var_115);
        return rax_0;
    }
    if (*var_17 != '\x01') {
        var_116 = local_sp_15 + (-24L);
        *(uint64_t *)var_116 = 4209093UL;
        var_117 = indirect_placeholder_12();
        local_sp_0 = var_116;
        if ((uint64_t)(uint32_t)var_117 != 0UL) {
            var_118 = *var_4;
            *(uint64_t *)(local_sp_15 + (-32L)) = 4209122UL;
            var_119 = indirect_placeholder_9(var_118, 0UL, 3UL);
            var_120 = var_119.field_0;
            var_121 = var_119.field_1;
            var_122 = var_119.field_2;
            *(uint64_t *)(local_sp_15 + (-40L)) = 4209130UL;
            indirect_placeholder();
            var_123 = (uint64_t)*(uint32_t *)var_120;
            *(uint64_t *)(local_sp_15 + (-48L)) = 4209157UL;
            indirect_placeholder_8(0UL, var_120, 4315481UL, 0UL, var_123, var_121, var_122);
            return rax_0;
        }
    }
    local_sp_1 = local_sp_0;
    if (*var_9 == '\x01') {
        var_124 = *var_4;
        *(uint64_t *)(local_sp_0 + (-8L)) = 4209203UL;
        var_125 = indirect_placeholder_13(var_124, 0UL, 3UL);
        var_126 = var_125.field_0;
        var_127 = var_125.field_1;
        *(uint64_t *)(local_sp_0 + (-16L)) = 4209237UL;
        indirect_placeholder_7(0UL, var_126, 4315632UL, 0UL, 0UL, var_127, 4314151UL);
    } else {
        if (*(unsigned char *)4338696UL != '\x01') {
            var_128 = *var_5;
            if (var_128 == 0UL) {
                *(uint64_t *)(local_sp_0 + (-8L)) = 4209279UL;
                var_129 = indirect_placeholder_14(var_128);
                storemerge7 = (var_129 == 1UL) ? 4315688UL : 4315736UL;
                var_130 = *var_5;
                var_131 = local_sp_0 + (-16L);
                *(uint64_t *)var_131 = 4209324UL;
                var_132 = indirect_placeholder_6(0UL, var_130, storemerge7, 0UL, 0UL, r94_4, r85_4);
                local_sp_1 = var_131;
                r94_0 = var_132.field_2;
                r85_0 = var_132.field_3;
            }
            var_133 = *var_8;
            local_sp_2 = local_sp_1;
            r94_1 = r94_0;
            r85_1 = r85_0;
            if (var_133 == 0UL) {
                *(uint64_t *)(local_sp_1 + (-8L)) = 4209343UL;
                var_134 = indirect_placeholder_14(var_133);
                storemerge8 = (var_134 == 1UL) ? 4315784UL : 4315832UL;
                var_135 = *var_8;
                var_136 = local_sp_1 + (-16L);
                *(uint64_t *)var_136 = 4209388UL;
                var_137 = indirect_placeholder_5(0UL, var_135, storemerge8, 0UL, 0UL, r94_0, r85_0);
                local_sp_2 = var_136;
                r94_1 = var_137.field_2;
                r85_1 = var_137.field_3;
            }
            var_138 = *var_7;
            local_sp_3 = local_sp_2;
            if (var_138 == 0UL) {
                *(uint64_t *)(local_sp_2 + (-8L)) = 4209407UL;
                var_139 = indirect_placeholder_14(var_138);
                storemerge9 = (var_139 == 1UL) ? 4315880UL : 4315928UL;
                var_140 = *var_7;
                var_141 = local_sp_2 + (-16L);
                *(uint64_t *)var_141 = 4209452UL;
                indirect_placeholder_4(0UL, var_140, storemerge9, 0UL, 0UL, r94_1, r85_1);
                local_sp_3 = var_141;
            }
            if (*(unsigned char *)4338698UL != '\x00' & *var_10 == '\x01') {
                var_142 = *var_4;
                *(uint64_t *)(local_sp_3 + (-8L)) = 4209499UL;
                var_143 = indirect_placeholder_15(var_142, 0UL, 3UL);
                var_144 = var_143.field_0;
                var_145 = var_143.field_1;
                var_146 = var_143.field_2;
                *(uint64_t *)(local_sp_3 + (-16L)) = 4209527UL;
                indirect_placeholder_3(0UL, var_144, 4315974UL, 0UL, 0UL, var_145, var_146);
            }
        }
    }
    if (*var_9 == '\x00') {
    } else {
        if (*var_10 == '\x00') {
        } else {
            if (*var_7 == 0UL) {
            } else {
                if (*var_8 == 0UL) {
                } else {
                    if (*(unsigned char *)4338700UL != '\x01' & *var_6 == 0UL) {
                    }
                }
            }
        }
    }
}
