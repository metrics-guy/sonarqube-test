typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_33_ret_type;
struct indirect_placeholder_33_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern struct indirect_placeholder_33_ret_type indirect_placeholder_33(uint64_t param_0);
uint64_t bb_process_long_option(uint64_t rcx, uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t r9, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint32_t *var_4;
    uint64_t *var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint32_t *var_11;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t *var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t *var_17;
    uint32_t var_56;
    uint32_t var_53;
    uint64_t local_sp_0;
    uint32_t **var_62;
    uint32_t *var_63;
    uint64_t var_54;
    uint64_t local_sp_8;
    uint64_t var_55;
    uint64_t local_sp_1;
    uint32_t var_57;
    uint64_t local_sp_2;
    uint64_t var_47;
    uint64_t var_40;
    uint32_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    struct indirect_placeholder_33_ret_type var_45;
    uint64_t var_46;
    uint64_t var_48;
    uint64_t local_sp_3;
    uint64_t local_sp_7;
    uint64_t local_sp_6;
    uint64_t var_29;
    uint64_t var_27;
    uint64_t var_30;
    uint64_t local_sp_4;
    uint64_t local_sp_5;
    uint64_t var_18;
    unsigned char **var_19;
    uint64_t var_78;
    uint64_t var_20;
    uint64_t *var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t *var_24;
    uint32_t *var_25;
    uint64_t **var_26;
    uint64_t var_31;
    uint64_t var_61;
    uint64_t *var_32;
    uint32_t *var_33;
    uint32_t *var_34;
    uint32_t *var_35;
    uint32_t *var_36;
    uint32_t *var_37;
    uint64_t var_38;
    bool var_49;
    uint64_t var_50;
    uint64_t *var_51;
    uint64_t var_52;
    uint64_t local_sp_9;
    uint64_t var_58;
    uint64_t var_59;
    uint32_t *var_60;
    uint64_t rax_0;
    uint64_t var_39;
    uint64_t local_sp_10;
    uint64_t local_sp_11;
    uint64_t var_75;
    uint64_t var_76;
    uint32_t *var_77;
    bool var_64;
    uint64_t var_65;
    uint32_t var_66;
    uint64_t var_67;
    uint32_t *var_68;
    uint32_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_28;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_3 = var_0 + (-136L);
    var_4 = (uint32_t *)(var_0 + (-92L));
    *var_4 = (uint32_t)rdi;
    var_5 = (uint64_t *)(var_0 + (-104L));
    *var_5 = rsi;
    var_6 = var_0 + (-112L);
    var_7 = (uint64_t *)var_6;
    *var_7 = rdx;
    var_8 = (uint64_t *)(var_0 + (-120L));
    *var_8 = rcx;
    var_9 = var_0 + (-128L);
    var_10 = (uint64_t *)var_9;
    *var_10 = r8;
    var_11 = (uint32_t *)(var_0 + (-96L));
    *var_11 = (uint32_t)r9;
    var_12 = (uint64_t *)(var_0 + (-48L));
    *var_12 = 0UL;
    var_13 = var_0 | 8UL;
    var_14 = (uint64_t *)var_13;
    var_15 = *(uint64_t *)(*var_14 + 32UL);
    var_16 = var_0 + (-32L);
    var_17 = (uint64_t *)var_16;
    *var_17 = var_15;
    var_53 = 0U;
    var_18 = var_15;
    local_sp_5 = var_3;
    rax_0 = 63UL;
    while (1U)
        {
            switch_state_var = 1;
            break;
        }
    var_20 = var_18 - *(uint64_t *)(*var_14 + 32UL);
    var_21 = (uint64_t *)(var_0 + (-88L));
    *var_21 = var_20;
    var_22 = *var_8;
    var_23 = var_0 + (-40L);
    var_24 = (uint64_t *)var_23;
    *var_24 = var_22;
    var_25 = (uint32_t *)(var_0 + (-52L));
    *var_25 = 0U;
    var_26 = (uint64_t **)var_23;
    var_27 = **var_26;
    local_sp_6 = local_sp_5;
    while (var_27 != 0UL)
        {
            var_28 = local_sp_5 + (-8L);
            *(uint64_t *)var_28 = 4255321UL;
            indirect_placeholder();
            local_sp_4 = var_28;
            if ((uint64_t)(uint32_t)var_27 != 0UL) {
                var_29 = **var_26;
                var_30 = local_sp_5 + (-16L);
                *(uint64_t *)var_30 = 4255340UL;
                indirect_placeholder();
                local_sp_4 = var_30;
                local_sp_6 = var_30;
                if (*var_21 != var_29) {
                    *var_12 = *var_24;
                    *(uint32_t *)(var_0 + (-56L)) = *var_25;
                    break;
                }
            }
            *var_24 = (*var_24 + 32UL);
            *var_25 = (*var_25 + 1U);
            local_sp_5 = local_sp_4;
            var_26 = (uint64_t **)var_23;
            var_27 = **var_26;
            local_sp_6 = local_sp_5;
        }
    var_31 = *var_12;
    local_sp_7 = local_sp_6;
    var_61 = var_31;
    local_sp_10 = local_sp_6;
    if (var_31 != 0UL) {
        var_32 = (uint64_t *)(var_0 + (-64L));
        *var_32 = 0UL;
        var_33 = (uint32_t *)(var_0 + (-68L));
        *var_33 = 0U;
        var_34 = (uint32_t *)(var_0 + (-72L));
        *var_34 = 0U;
        var_35 = (uint32_t *)(var_0 + (-76L));
        *var_35 = 4294967295U;
        *var_24 = *var_8;
        var_36 = (uint32_t *)(var_0 + (-56L));
        *var_36 = 0U;
        var_37 = (uint32_t *)(var_0 | 16UL);
        var_38 = **var_26;
        local_sp_8 = local_sp_7;
        local_sp_10 = local_sp_7;
        while (var_38 != 0UL)
            {
                var_39 = local_sp_7 + (-8L);
                *(uint64_t *)var_39 = 4255473UL;
                indirect_placeholder();
                local_sp_2 = var_39;
                local_sp_3 = var_39;
                if ((uint64_t)(uint32_t)var_38 == 0UL) {
                    *var_24 = (*var_24 + 32UL);
                    *var_36 = (*var_36 + 1U);
                    local_sp_7 = local_sp_3;
                    var_38 = **var_26;
                    local_sp_8 = local_sp_7;
                    local_sp_10 = local_sp_7;
                    continue;
                }
                var_40 = *var_12;
                if (var_40 == 0UL) {
                    *var_12 = *var_24;
                    *var_35 = *var_36;
                } else {
                    if (*var_11 != 0U) {
                        var_41 = *(uint32_t *)(var_40 + 8UL);
                        var_42 = *var_24;
                        if ((uint64_t)(var_41 - *(uint32_t *)(var_42 + 8UL)) != 0UL & *(uint64_t *)(var_40 + 16UL) != *(uint64_t *)(var_42 + 16UL) & (uint64_t)(*(uint32_t *)(var_40 + 24UL) - *(uint32_t *)(var_42 + 24UL)) != 0UL) {
                            *var_24 = (*var_24 + 32UL);
                            *var_36 = (*var_36 + 1U);
                            local_sp_7 = local_sp_3;
                            var_38 = **var_26;
                            local_sp_8 = local_sp_7;
                            local_sp_10 = local_sp_7;
                            continue;
                        }
                    }
                    if (*var_34 != 0U) {
                        if (*var_37 == 0U) {
                            *var_34 = 1U;
                        } else {
                            if (*var_32 != 0UL) {
                                var_43 = (uint64_t)*var_25;
                                var_44 = local_sp_7 + (-16L);
                                *(uint64_t *)var_44 = 4255619UL;
                                var_45 = indirect_placeholder_33(var_43);
                                var_46 = var_45.field_0;
                                *var_32 = var_46;
                                local_sp_2 = var_44;
                                if (var_46 == 0UL) {
                                    *var_34 = 1U;
                                } else {
                                    *var_33 = 1U;
                                }
                                if (*var_32 == 0UL) {
                                    var_47 = local_sp_7 + (-24L);
                                    *(uint64_t *)var_47 = 4255676UL;
                                    indirect_placeholder();
                                    *(unsigned char *)(*var_32 + (uint64_t)*var_35) = (unsigned char)'\x01';
                                    local_sp_2 = var_47;
                                }
                            }
                        }
                        var_48 = *var_32;
                        local_sp_3 = local_sp_2;
                        if (var_48 == 0UL) {
                            *(unsigned char *)(var_48 + (uint64_t)*var_36) = (unsigned char)'\x01';
                        }
                    }
                }
            }
        if (*var_32 != 0UL) {
            if (*var_37 != 0U) {
                var_49 = (*var_34 == 0U);
                var_50 = local_sp_7 + (-8L);
                var_51 = (uint64_t *)var_50;
                local_sp_8 = var_50;
                if (var_49) {
                    *var_51 = 4255820UL;
                    indirect_placeholder();
                } else {
                    *var_51 = 4255840UL;
                    indirect_placeholder();
                    var_52 = local_sp_7 + (-16L);
                    *(uint64_t *)var_52 = 4255887UL;
                    indirect_placeholder();
                    *var_36 = 0U;
                    local_sp_0 = var_52;
                    var_56 = var_53;
                    local_sp_1 = local_sp_0;
                    while ((long)((uint64_t)var_53 << 32UL) >= (long)((uint64_t)*var_25 << 32UL))
                        {
                            if (*(unsigned char *)(*var_32 + (uint64_t)var_53) == '\x00') {
                                var_55 = local_sp_0 + (-8L);
                                *(uint64_t *)var_55 = 4255967UL;
                                indirect_placeholder();
                                var_56 = *var_36;
                                local_sp_1 = var_55;
                            }
                            var_57 = var_56 + 1U;
                            *var_36 = var_57;
                            var_53 = var_57;
                            local_sp_0 = local_sp_1;
                            var_56 = var_53;
                            local_sp_1 = local_sp_0;
                        }
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4255999UL;
                    indirect_placeholder();
                    var_54 = local_sp_0 + (-16L);
                    *(uint64_t *)var_54 = 4256014UL;
                    indirect_placeholder();
                    local_sp_8 = var_54;
                }
            }
            local_sp_9 = local_sp_8;
            if (*var_33 == 0U) {
                var_58 = local_sp_8 + (-8L);
                *(uint64_t *)var_58 = 4256032UL;
                indirect_placeholder();
                local_sp_9 = var_58;
            }
            var_59 = *(uint64_t *)(*var_14 + 32UL);
            *(uint64_t *)(local_sp_9 + (-8L)) = 4256056UL;
            indirect_placeholder();
            *(uint64_t *)(*var_14 + 32UL) = (var_59 + var_59);
            var_60 = *(uint32_t **)var_13;
            *var_60 = (*var_60 + 1U);
            *(uint32_t *)(*var_14 + 8UL) = 0U;
            return rax_0;
        }
        if (*var_34 != 0U) {
            if (*var_37 != 0U) {
                var_49 = (*var_34 == 0U);
                var_50 = local_sp_7 + (-8L);
                var_51 = (uint64_t *)var_50;
                local_sp_8 = var_50;
                if (var_49) {
                    *var_51 = 4255820UL;
                    indirect_placeholder();
                } else {
                    *var_51 = 4255840UL;
                    indirect_placeholder();
                    var_52 = local_sp_7 + (-16L);
                    *(uint64_t *)var_52 = 4255887UL;
                    indirect_placeholder();
                    *var_36 = 0U;
                    local_sp_0 = var_52;
                    var_56 = var_53;
                    local_sp_1 = local_sp_0;
                    while ((long)((uint64_t)var_53 << 32UL) >= (long)((uint64_t)*var_25 << 32UL))
                        {
                            if (*(unsigned char *)(*var_32 + (uint64_t)var_53) == '\x00') {
                                var_55 = local_sp_0 + (-8L);
                                *(uint64_t *)var_55 = 4255967UL;
                                indirect_placeholder();
                                var_56 = *var_36;
                                local_sp_1 = var_55;
                            }
                            var_57 = var_56 + 1U;
                            *var_36 = var_57;
                            var_53 = var_57;
                            local_sp_0 = local_sp_1;
                            var_56 = var_53;
                            local_sp_1 = local_sp_0;
                        }
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4255999UL;
                    indirect_placeholder();
                    var_54 = local_sp_0 + (-16L);
                    *(uint64_t *)var_54 = 4256014UL;
                    indirect_placeholder();
                    local_sp_8 = var_54;
                }
            }
            local_sp_9 = local_sp_8;
            if (*var_33 == 0U) {
                var_58 = local_sp_8 + (-8L);
                *(uint64_t *)var_58 = 4256032UL;
                indirect_placeholder();
                local_sp_9 = var_58;
            }
            var_59 = *(uint64_t *)(*var_14 + 32UL);
            *(uint64_t *)(local_sp_9 + (-8L)) = 4256056UL;
            indirect_placeholder();
            *(uint64_t *)(*var_14 + 32UL) = (var_59 + var_59);
            var_60 = *(uint32_t **)var_13;
            *var_60 = (*var_60 + 1U);
            *(uint32_t *)(*var_14 + 8UL) = 0U;
            return rax_0;
        }
        *var_36 = *var_35;
        var_61 = *var_12;
    }
    local_sp_11 = local_sp_10;
    rax_0 = 0UL;
    if (var_61 != 0UL) {
        var_62 = (uint32_t **)var_13;
        var_63 = *var_62;
        *var_63 = (*var_63 + 1U);
        *(uint64_t *)(*var_14 + 32UL) = 0UL;
        var_64 = (**var_19 == '\x00');
        var_65 = *var_12;
        var_66 = *(uint32_t *)(var_65 + 8UL);
        var_67 = var_65;
        var_70 = var_65;
        if (var_64) {
            if (var_66 != 1U) {
                var_68 = *var_62;
                var_69 = *var_68;
                if ((int)var_69 >= (int)*var_4) {
                    if (*(uint32_t *)(var_0 | 16UL) != 0U) {
                        *(uint64_t *)(local_sp_10 + (-8L)) = 4256578UL;
                        indirect_placeholder();
                        var_70 = *var_12;
                    }
                    *(uint32_t *)(*var_14 + 8UL) = *(uint32_t *)(var_70 + 24UL);
                    return (**(unsigned char **)var_6 == ':') ? 58UL : 63UL;
                }
                var_71 = (uint64_t)var_69;
                *var_68 = (var_69 + 1U);
                *(uint64_t *)(*var_14 + 16UL) = *(uint64_t *)(*var_5 + (uint64_t)((long)(var_71 << 32UL) >> (long)29UL));
            }
        } else {
            if (var_66 != 0U) {
                if (*(uint32_t *)(var_0 | 16UL) == 0U) {
                    *(uint64_t *)(local_sp_10 + (-8L)) = 4256430UL;
                    indirect_placeholder();
                    var_67 = *var_12;
                }
                *(uint32_t *)(*var_14 + 8UL) = *(uint32_t *)(var_67 + 24UL);
                return rax_0;
            }
            *(uint64_t *)(*var_14 + 16UL) = (*var_17 + 1UL);
        }
        if (*var_10 == 0UL) {
            **(uint32_t **)var_9 = *(uint32_t *)(var_0 + (-56L));
        }
        var_72 = *var_12;
        var_73 = var_72 + 16UL;
        if (*(uint64_t *)var_73 == 0UL) {
            var_74 = (uint64_t)*(uint32_t *)(var_72 + 24UL);
            rax_0 = var_74;
        } else {
            **(uint32_t **)var_73 = *(uint32_t *)(var_72 + 24UL);
        }
        return rax_0;
    }
    rax_0 = 4294967295UL;
    if (*var_11 != 0U) {
        var_75 = *var_7;
        var_76 = local_sp_10 + (-8L);
        *(uint64_t *)var_76 = 4256192UL;
        indirect_placeholder();
        local_sp_11 = var_76;
        if (*(unsigned char *)(*(uint64_t *)(*var_5 + ((uint64_t)**(uint32_t **)var_13 << 3UL)) + 1UL) != '-' & var_75 == 0UL) {
            return rax_0;
        }
    }
    if (*(uint32_t *)(var_0 | 16UL) != 0U) {
        *(uint64_t *)(local_sp_11 + (-8L)) = 4256250UL;
        indirect_placeholder();
    }
    *(uint64_t *)(*var_14 + 32UL) = 0UL;
    var_77 = *(uint32_t **)var_13;
    *var_77 = (*var_77 + 1U);
    *(uint32_t *)(*var_14 + 8UL) = 0U;
}
