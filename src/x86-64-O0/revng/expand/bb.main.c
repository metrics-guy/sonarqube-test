typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_10_ret_type;
struct indirect_placeholder_10_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r10(void);
extern void indirect_placeholder_8(uint64_t param_0, uint64_t param_1);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_7(uint64_t param_0);
extern struct indirect_placeholder_10_ret_type indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern void indirect_placeholder_5(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
typedef _Bool bool;
void bb_main(uint64_t rsi, uint64_t rdi) {
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint32_t var_22;
    struct indirect_placeholder_10_ret_type var_17;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint32_t *var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint32_t *var_10;
    uint64_t var_11;
    unsigned char *var_12;
    unsigned char *var_13;
    uint64_t local_sp_1;
    uint64_t storemerge;
    uint32_t var_25;
    uint64_t local_sp_0;
    uint64_t var_14;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t local_sp_1_be;
    uint64_t var_23;
    uint64_t var_24;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = init_r10();
    var_4 = var_0 + (-8L);
    *(uint64_t *)var_4 = var_1;
    var_5 = (uint32_t *)(var_0 + (-28L));
    *var_5 = (uint32_t)rdi;
    var_6 = var_0 + (-40L);
    var_7 = (uint64_t *)var_6;
    *var_7 = rsi;
    var_8 = **(uint64_t **)var_6;
    *(uint64_t *)(var_0 + (-48L)) = 4205319UL;
    indirect_placeholder_7(var_8);
    *(uint64_t *)(var_0 + (-56L)) = 4205334UL;
    indirect_placeholder();
    var_9 = var_0 + (-64L);
    *(uint64_t *)var_9 = 4205344UL;
    indirect_placeholder();
    *(unsigned char *)4289424UL = (unsigned char)'\x01';
    var_10 = (uint32_t *)(var_0 + (-12L));
    var_11 = var_0 + (-14L);
    var_12 = (unsigned char *)var_11;
    var_13 = (unsigned char *)(var_0 + (-13L));
    storemerge = 0UL;
    local_sp_1 = var_9;
    while (1U)
        {
            var_14 = *var_7;
            var_15 = (uint64_t)*var_5;
            var_16 = local_sp_1 + (-8L);
            *(uint64_t *)var_16 = 4205628UL;
            var_17 = indirect_placeholder_10(4272704UL, 4272640UL, var_14, var_15, 0UL);
            var_18 = var_17.field_0;
            var_19 = var_17.field_1;
            var_20 = var_17.field_2;
            var_21 = var_17.field_3;
            var_22 = (uint32_t)var_18;
            *var_10 = var_22;
            local_sp_1_be = var_16;
            local_sp_0 = var_16;
            switch_state_var = 0;
            switch (var_22) {
              case 116U:
                {
                    var_23 = *(uint64_t *)4289952UL;
                    var_24 = local_sp_1 + (-16L);
                    *(uint64_t *)var_24 = 4205454UL;
                    indirect_placeholder_9(var_19, var_23, var_2, var_3, var_20, var_21);
                    local_sp_1_be = var_24;
                    local_sp_1 = local_sp_1_be;
                    continue;
                }
                break;
              case 4294967295U:
                {
                    *(uint64_t *)(local_sp_1 + (-16L)) = 4205646UL;
                    indirect_placeholder_5(var_19, var_20, var_21);
                    var_25 = *(uint32_t *)4289272UL;
                    if ((int)var_25 >= (int)*var_5) {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    storemerge = *var_7 + ((uint64_t)var_25 << 3UL);
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              default:
                {
                    if ((int)var_22 <= (int)116U) {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    if (var_22 == 105U) {
                        *(unsigned char *)4289424UL = (unsigned char)'\x00';
                    } else {
                        if ((int)var_22 <= (int)57U) {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        if ((int)var_22 >= (int)48U) {
                            switch_state_var = 0;
                            switch (var_22) {
                              case 4294967166U:
                                {
                                    *(uint64_t *)(local_sp_1 + (-16L)) = 4205526UL;
                                    indirect_placeholder_8(var_4, 0UL);
                                    abort();
                                }
                                break;
                              case 4294967165U:
                                {
                                    var_30 = *(uint64_t *)4289136UL;
                                    *(uint64_t *)(local_sp_1 + (-16L)) = 4205578UL;
                                    indirect_placeholder_9(0UL, var_30, 4272408UL, 4273230UL, 0UL, 4273272UL);
                                    var_31 = local_sp_1 + (-24L);
                                    *(uint64_t *)var_31 = 4205588UL;
                                    indirect_placeholder();
                                    local_sp_0 = var_31;
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              default:
                                {
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                            }
                            if (switch_state_var)
                                break;
                        }
                        var_26 = *(uint64_t *)4289952UL;
                        if (var_26 == 0UL) {
                            *var_12 = (unsigned char)var_22;
                            *var_13 = (unsigned char)'\x00';
                            var_29 = local_sp_1 + (-16L);
                            *(uint64_t *)var_29 = 4205514UL;
                            indirect_placeholder_9(var_19, var_11, var_2, var_3, var_20, var_21);
                            local_sp_1_be = var_29;
                        } else {
                            var_27 = var_26 + (-1L);
                            var_28 = local_sp_1 + (-16L);
                            *(uint64_t *)var_28 = 4205490UL;
                            indirect_placeholder_9(var_19, var_27, var_2, var_3, var_20, var_21);
                            local_sp_1_be = var_28;
                        }
                    }
                    local_sp_1 = local_sp_1_be;
                    continue;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(uint64_t *)(local_sp_1 + (-24L)) = 4205695UL;
            indirect_placeholder_7(storemerge);
            *(uint64_t *)(local_sp_1 + (-32L)) = 4205700UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_1 + (-40L)) = 4205705UL;
            indirect_placeholder();
            return;
        }
        break;
      case 1U:
        {
            *(uint64_t *)(local_sp_0 + (-8L)) = 4205598UL;
            indirect_placeholder_8(var_4, 1UL);
            abort();
        }
        break;
    }
}
