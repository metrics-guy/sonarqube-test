typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_18_ret_type;
struct indirect_placeholder_20_ret_type;
struct indirect_placeholder_21_ret_type;
struct indirect_placeholder_22_ret_type;
struct indirect_placeholder_23_ret_type;
struct indirect_placeholder_24_ret_type;
struct indirect_placeholder_18_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_20_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_21_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_22_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_23_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_24_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern struct indirect_placeholder_18_ret_type indirect_placeholder_18(uint64_t param_0);
extern struct indirect_placeholder_20_ret_type indirect_placeholder_20(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_21_ret_type indirect_placeholder_21(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_22_ret_type indirect_placeholder_22(uint64_t param_0);
extern struct indirect_placeholder_23_ret_type indirect_placeholder_23(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_24_ret_type indirect_placeholder_24(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
typedef _Bool bool;
void bb_expand(void) {
    struct indirect_placeholder_21_ret_type var_41;
    struct indirect_placeholder_20_ret_type var_52;
    uint64_t r9_1;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    struct indirect_placeholder_18_ret_type var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t r10_2;
    uint64_t rcx_2;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t local_sp_2;
    uint64_t r8_2;
    uint64_t rcx_1;
    uint64_t var_36;
    uint64_t local_sp_9_ph;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t r9_7;
    uint64_t var_40;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t rax_0;
    uint64_t local_sp_1;
    uint64_t r9_2;
    uint64_t r8_7;
    uint64_t r10_1;
    uint64_t var_20;
    uint64_t r8_1;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t rcx_4;
    uint64_t var_49;
    uint64_t r10_7;
    uint64_t var_23;
    uint64_t var_24;
    struct indirect_placeholder_22_ret_type var_25;
    uint64_t var_26;
    uint64_t local_sp_9;
    uint64_t rcx_3;
    uint64_t local_sp_3;
    uint64_t r10_3;
    uint64_t r9_3;
    uint64_t r8_3;
    uint64_t rcx_5;
    uint64_t var_29;
    uint64_t var_30;
    struct indirect_placeholder_23_ret_type var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_53;
    uint64_t var_54;
    struct indirect_placeholder_24_ret_type var_55;
    uint64_t local_sp_4;
    uint64_t r10_4;
    uint64_t r9_4;
    uint64_t r8_4;
    unsigned char var_56;
    uint64_t var_57;
    uint64_t local_sp_5;
    uint32_t var_58;
    uint64_t var_59;
    uint64_t local_sp_6;
    unsigned char storemerge;
    uint64_t rbx_0;
    uint64_t local_sp_7;
    uint64_t r10_5;
    uint64_t r9_5;
    uint64_t r8_5;
    uint64_t var_60;
    uint64_t rcx_6;
    uint64_t rcx_7_ph;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    unsigned char *var_12;
    uint64_t *var_13;
    uint64_t var_14;
    uint64_t *var_15;
    uint32_t *var_16;
    uint64_t var_17;
    uint64_t *var_18;
    unsigned char *var_19;
    uint64_t rbx_1;
    uint64_t local_sp_8;
    uint64_t r10_6;
    uint64_t r9_6;
    uint64_t r8_6;
    uint64_t rbx_2_ph;
    uint64_t r10_7_ph;
    uint64_t r9_7_ph;
    uint64_t r8_7_ph;
    uint64_t rcx_7;
    uint64_t var_21;
    uint32_t var_22;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_4 = var_0 + (-80L);
    *(uint64_t *)var_4 = 4204825UL;
    var_5 = indirect_placeholder_18(0UL);
    var_6 = var_5.field_0;
    var_7 = (uint64_t *)(var_0 + (-32L));
    *var_7 = var_6;
    rax_0 = 0UL;
    storemerge = (unsigned char)'\x01';
    rbx_1 = var_2;
    local_sp_8 = var_4;
    if (var_6 == 0UL) {
        return;
    }
    var_8 = var_5.field_4;
    var_9 = var_5.field_3;
    var_10 = var_5.field_2;
    var_11 = var_5.field_1;
    var_12 = (unsigned char *)(var_0 + (-37L));
    var_13 = (uint64_t *)(var_0 + (-48L));
    var_14 = var_0 + (-64L);
    var_15 = (uint64_t *)var_14;
    var_16 = (uint32_t *)(var_0 + (-36L));
    var_17 = var_0 + (-65L);
    var_18 = (uint64_t *)(var_0 + (-56L));
    var_19 = (unsigned char *)var_17;
    rcx_6 = var_11;
    r10_6 = var_10;
    r9_6 = var_9;
    r8_6 = var_8;
    loop_state_var = 2U;
    while (1U)
        {
            switch_state_var = 0;
            switch (loop_state_var) {
              case 2U:
              case 1U:
                {
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 2U:
                        {
                            *var_12 = (unsigned char)'\x01';
                            *var_13 = 0UL;
                            *var_15 = 0UL;
                            local_sp_9_ph = local_sp_8;
                            rcx_7_ph = rcx_6;
                            rbx_2_ph = rbx_1;
                            r10_7_ph = r10_6;
                            r9_7_ph = r9_6;
                            r8_7_ph = r8_6;
                        }
                        break;
                      case 1U:
                        {
                            r9_7 = r9_7_ph;
                            r8_7 = r8_7_ph;
                            var_20 = *var_7;
                            r10_7 = r10_7_ph;
                            local_sp_9 = local_sp_9_ph;
                            rbx_0 = rbx_2_ph;
                            rcx_7 = rcx_7_ph;
                            while (1U)
                                {
                                    var_21 = local_sp_9 + (-8L);
                                    *(uint64_t *)var_21 = 4204875UL;
                                    indirect_placeholder();
                                    var_22 = (uint32_t)var_20;
                                    *var_16 = var_22;
                                    rcx_3 = rcx_7;
                                    local_sp_3 = var_21;
                                    r10_3 = r10_7;
                                    r9_3 = r9_7;
                                    r8_3 = r8_7;
                                    if ((int)var_22 <= (int)4294967295U) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    var_23 = *var_7;
                                    var_24 = local_sp_9 + (-16L);
                                    *(uint64_t *)var_24 = 4204896UL;
                                    var_25 = indirect_placeholder_22(var_23);
                                    var_26 = var_25.field_0;
                                    *var_7 = var_26;
                                    local_sp_3 = var_24;
                                    var_20 = var_26;
                                    local_sp_9 = var_24;
                                    if (var_26 != 0UL) {
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    rcx_7 = var_25.field_1;
                                    r10_7 = var_25.field_2;
                                    r9_7 = var_25.field_3;
                                    r8_7 = var_25.field_4;
                                    continue;
                                }
                            switch_state_var = 0;
                            switch (loop_state_var) {
                              case 1U:
                                {
                                    rcx_3 = var_25.field_1;
                                    r10_3 = var_25.field_2;
                                    r9_3 = var_25.field_3;
                                    r8_3 = var_25.field_4;
                                }
                                break;
                              case 0U:
                                {
                                    r10_1 = r10_3;
                                    rcx_4 = rcx_3;
                                    rcx_5 = rcx_3;
                                    local_sp_4 = local_sp_3;
                                    r10_4 = r10_3;
                                    r9_4 = r9_3;
                                    r8_4 = r8_3;
                                    local_sp_7 = local_sp_3;
                                    r10_5 = r10_3;
                                    r9_5 = r9_3;
                                    r8_5 = r8_3;
                                    if (*var_12 != '\x00') {
                                        rcx_6 = rcx_5;
                                        rcx_7_ph = rcx_5;
                                        rbx_1 = rbx_0;
                                        r10_6 = r10_5;
                                        r9_6 = r9_5;
                                        r8_6 = r8_5;
                                        rbx_2_ph = rbx_0;
                                        r10_7_ph = r10_5;
                                        r9_7_ph = r9_5;
                                        r8_7_ph = r8_5;
                                        if ((int)*var_16 < (int)0U) {
                                            switch_state_var = 1;
                                            break;
                                        }
                                        var_60 = local_sp_7 + (-8L);
                                        *(uint64_t *)var_60 = 4205230UL;
                                        indirect_placeholder();
                                        local_sp_8 = var_60;
                                        local_sp_9_ph = var_60;
                                        if (*var_16 != 10U) {
                                            loop_state_var = 2U;
                                            continue;
                                        }
                                        loop_state_var = 1U;
                                        continue;
                                    }
                                    var_29 = *var_13;
                                    var_30 = local_sp_3 + (-8L);
                                    *(uint64_t *)var_30 = 4204950UL;
                                    var_31 = indirect_placeholder_23(var_17, var_14, var_29, rbx_2_ph, r10_3, r9_3, r8_3);
                                    var_32 = var_31.field_0;
                                    var_33 = var_31.field_1;
                                    var_34 = var_31.field_2;
                                    var_35 = var_31.field_3;
                                    *var_18 = var_32;
                                    r9_1 = var_34;
                                    rcx_1 = var_33;
                                    var_37 = var_32;
                                    local_sp_1 = var_30;
                                    r8_1 = var_35;
                                    if (*var_19 == '\x00') {
                                        var_36 = *var_13 + 1UL;
                                        *var_18 = var_36;
                                        var_37 = var_36;
                                    }
                                    var_38 = *var_13;
                                    var_39 = helper_cc_compute_c_wrapper(var_37 - var_38, var_38, var_3, 17U);
                                    if (var_39 != 0UL) {
                                        var_40 = local_sp_3 + (-16L);
                                        *(uint64_t *)var_40 = 4205009UL;
                                        var_41 = indirect_placeholder_21(0UL, var_33, 4273237UL, 0UL, 1UL, var_34, var_35);
                                        var_42 = var_41.field_0;
                                        var_43 = var_41.field_1;
                                        var_44 = var_41.field_2;
                                        var_45 = var_41.field_3;
                                        r10_2 = var_43;
                                        rcx_2 = var_42;
                                        local_sp_2 = var_40;
                                        r8_2 = var_45;
                                        r9_2 = var_44;
                                        var_49 = local_sp_2 + (-8L);
                                        *(uint64_t *)var_49 = 4205019UL;
                                        indirect_placeholder();
                                        r9_1 = r9_2;
                                        rcx_1 = rcx_2;
                                        local_sp_1 = var_49;
                                        r10_1 = r10_2;
                                        r8_1 = r8_2;
                                        if ((int)(uint32_t)rax_0 <= (int)4294967295U) {
                                            loop_state_var = 0U;
                                            continue;
                                        }
                                        *(uint64_t *)(local_sp_2 + (-16L)) = 4205028UL;
                                        indirect_placeholder();
                                        var_50 = (uint64_t)*(uint32_t *)rax_0;
                                        var_51 = local_sp_2 + (-24L);
                                        *(uint64_t *)var_51 = 4205052UL;
                                        var_52 = indirect_placeholder_20(0UL, rcx_2, 4273260UL, var_50, 1UL, r9_2, r8_2);
                                        r9_1 = var_52.field_2;
                                        rcx_1 = var_52.field_0;
                                        local_sp_1 = var_51;
                                        r10_1 = var_52.field_1;
                                        r8_1 = var_52.field_3;
                                        loop_state_var = 0U;
                                        continue;
                                    }
                                    var_46 = *var_13 + 1UL;
                                    *var_13 = var_46;
                                    var_47 = *var_18;
                                    var_48 = helper_cc_compute_c_wrapper(var_46 - var_47, var_47, var_3, 17U);
                                    r10_2 = r10_1;
                                    rcx_2 = rcx_1;
                                    local_sp_2 = local_sp_1;
                                    r8_2 = r8_1;
                                    rax_0 = var_46;
                                    r9_2 = r9_1;
                                    rcx_4 = rcx_1;
                                    local_sp_4 = local_sp_1;
                                    r10_4 = r10_1;
                                    r9_4 = r9_1;
                                    r8_4 = r8_1;
                                    if (var_48 != 0UL) {
                                        var_49 = local_sp_2 + (-8L);
                                        *(uint64_t *)var_49 = 4205019UL;
                                        indirect_placeholder();
                                        r9_1 = r9_2;
                                        rcx_1 = rcx_2;
                                        local_sp_1 = var_49;
                                        r10_1 = r10_2;
                                        r8_1 = r8_2;
                                        if ((int)(uint32_t)rax_0 <= (int)4294967295U) {
                                            loop_state_var = 0U;
                                            continue;
                                        }
                                        *(uint64_t *)(local_sp_2 + (-16L)) = 4205028UL;
                                        indirect_placeholder();
                                        var_50 = (uint64_t)*(uint32_t *)rax_0;
                                        var_51 = local_sp_2 + (-24L);
                                        *(uint64_t *)var_51 = 4205052UL;
                                        var_52 = indirect_placeholder_20(0UL, rcx_2, 4273260UL, var_50, 1UL, r9_2, r8_2);
                                        r9_1 = var_52.field_2;
                                        rcx_1 = var_52.field_0;
                                        local_sp_1 = var_51;
                                        r10_1 = var_52.field_1;
                                        r8_1 = var_52.field_3;
                                        loop_state_var = 0U;
                                        continue;
                                    }
                                    *var_16 = 32U;
                                }
                                break;
                            }
                            if (switch_state_var)
                                break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
                break;
              case 0U:
                {
                    var_46 = *var_13 + 1UL;
                    *var_13 = var_46;
                    var_47 = *var_18;
                    var_48 = helper_cc_compute_c_wrapper(var_46 - var_47, var_47, var_3, 17U);
                    r10_2 = r10_1;
                    rcx_2 = rcx_1;
                    local_sp_2 = local_sp_1;
                    r8_2 = r8_1;
                    rax_0 = var_46;
                    r9_2 = r9_1;
                    rcx_4 = rcx_1;
                    local_sp_4 = local_sp_1;
                    r10_4 = r10_1;
                    r9_4 = r9_1;
                    r8_4 = r8_1;
                    if (var_48 != 0UL) {
                        *var_16 = 32U;
                        var_56 = *var_12;
                        var_57 = (uint64_t)var_56;
                        rcx_5 = rcx_4;
                        local_sp_5 = local_sp_4;
                        rbx_0 = var_57;
                        r10_5 = r10_4;
                        r9_5 = r9_4;
                        r8_5 = r8_4;
                        if (*(unsigned char *)4289424UL == '\x00') {
                            local_sp_6 = local_sp_5;
                        } else {
                            var_58 = *var_16;
                            var_59 = local_sp_4 + (-8L);
                            *(uint64_t *)var_59 = 4205188UL;
                            indirect_placeholder();
                            local_sp_5 = var_59;
                            local_sp_6 = var_59;
                            storemerge = (unsigned char)'\x00';
                            if (var_58 == 0U) {
                                local_sp_6 = local_sp_5;
                            }
                        }
                        *var_12 = (storemerge & var_56);
                        local_sp_7 = local_sp_6;
                        rcx_6 = rcx_5;
                        rcx_7_ph = rcx_5;
                        rbx_1 = rbx_0;
                        r10_6 = r10_5;
                        r9_6 = r9_5;
                        r8_6 = r8_5;
                        rbx_2_ph = rbx_0;
                        r10_7_ph = r10_5;
                        r9_7_ph = r9_5;
                        r8_7_ph = r8_5;
                        if ((int)*var_16 < (int)0U) {
                            switch_state_var = 1;
                            break;
                        }
                        var_60 = local_sp_7 + (-8L);
                        *(uint64_t *)var_60 = 4205230UL;
                        indirect_placeholder();
                        local_sp_8 = var_60;
                        local_sp_9_ph = var_60;
                        if (*var_16 != 10U) {
                            loop_state_var = 1U;
                            continue;
                        }
                        loop_state_var = 2U;
                        continue;
                    }
                    var_49 = local_sp_2 + (-8L);
                    *(uint64_t *)var_49 = 4205019UL;
                    indirect_placeholder();
                    r9_1 = r9_2;
                    rcx_1 = rcx_2;
                    local_sp_1 = var_49;
                    r10_1 = r10_2;
                    r8_1 = r8_2;
                    if ((int)(uint32_t)rax_0 > (int)4294967295U) {
                        loop_state_var = 0U;
                        continue;
                    }
                    *(uint64_t *)(local_sp_2 + (-16L)) = 4205028UL;
                    indirect_placeholder();
                    var_50 = (uint64_t)*(uint32_t *)rax_0;
                    var_51 = local_sp_2 + (-24L);
                    *(uint64_t *)var_51 = 4205052UL;
                    var_52 = indirect_placeholder_20(0UL, rcx_2, 4273260UL, var_50, 1UL, r9_2, r8_2);
                    r9_1 = var_52.field_2;
                    rcx_1 = var_52.field_0;
                    local_sp_1 = var_51;
                    r10_1 = var_52.field_1;
                    r8_1 = var_52.field_3;
                    loop_state_var = 0U;
                    continue;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    return;
}
