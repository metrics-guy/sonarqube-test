typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_23_ret_type;
struct indirect_placeholder_26_ret_type;
struct indirect_placeholder_18_ret_type;
struct indirect_placeholder_17_ret_type;
struct indirect_placeholder_21_ret_type;
struct indirect_placeholder_19_ret_type;
struct indirect_placeholder_22_ret_type;
struct indirect_placeholder_20_ret_type;
struct indirect_placeholder_23_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_26_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_18_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_17_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_21_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_19_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_22_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_20_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern struct indirect_placeholder_23_ret_type indirect_placeholder_23(uint64_t param_0, uint64_t param_1);
extern uint64_t init_rcx(void);
extern struct indirect_placeholder_26_ret_type indirect_placeholder_26(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_18_ret_type indirect_placeholder_18(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_17_ret_type indirect_placeholder_17(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_21_ret_type indirect_placeholder_21(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_19_ret_type indirect_placeholder_19(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_22_ret_type indirect_placeholder_22(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_20_ret_type indirect_placeholder_20(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
uint64_t bb_sync_arg(uint64_t rsi, uint64_t rdi) {
    struct indirect_placeholder_23_ret_type var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint32_t *var_6;
    uint64_t *var_7;
    unsigned char *var_8;
    uint32_t *var_9;
    uint64_t var_10;
    struct indirect_placeholder_26_ret_type var_11;
    uint32_t *var_12;
    uint32_t var_13;
    uint64_t local_sp_3;
    uint64_t var_46;
    struct indirect_placeholder_18_ret_type var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    unsigned char var_52;
    unsigned char var_28;
    uint64_t local_sp_0;
    uint64_t local_sp_2;
    uint64_t var_21;
    uint32_t *var_29;
    uint32_t var_30;
    uint32_t var_37;
    uint32_t var_31;
    uint64_t var_32;
    uint32_t var_33;
    uint64_t var_34;
    uint64_t local_sp_1;
    uint64_t var_38;
    struct indirect_placeholder_21_ret_type var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint32_t var_35;
    uint64_t var_36;
    uint32_t var_45;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    struct indirect_placeholder_22_ret_type var_20;
    uint64_t var_26;
    uint64_t var_27;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rcx();
    var_3 = init_rbx();
    var_4 = init_r9();
    var_5 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    var_6 = (uint32_t *)(var_0 + (-60L));
    *var_6 = (uint32_t)rdi;
    var_7 = (uint64_t *)(var_0 + (-72L));
    *var_7 = rsi;
    var_8 = (unsigned char *)(var_0 + (-25L));
    *var_8 = (unsigned char)'\x01';
    *(uint32_t *)(var_0 + (-40L)) = 2048U;
    *(uint64_t *)(var_0 + (-80L)) = 4204732UL;
    indirect_placeholder();
    var_9 = (uint32_t *)(var_0 + (-32L));
    *var_9 = 0U;
    var_10 = var_0 + (-88L);
    *(uint64_t *)var_10 = 4204869UL;
    var_11 = indirect_placeholder_26(0UL, var_2, 2048UL, 3UL, 0UL, var_4, var_5);
    var_12 = (uint32_t *)(var_0 + (-44L));
    var_13 = (uint32_t)var_11.field_0;
    *var_12 = var_13;
    var_52 = (unsigned char)'\x00';
    var_28 = (unsigned char)'\x00';
    var_37 = 4294967295U;
    local_sp_3 = var_10;
    if (var_13 == 4294967295U) {
        var_21 = *var_7;
        *(uint64_t *)(local_sp_3 + (-8L)) = 4204927UL;
        var_22 = indirect_placeholder_23(var_21, 4UL);
        var_23 = var_22.field_0;
        var_24 = var_22.field_1;
        var_25 = var_22.field_2;
        *(uint64_t *)(local_sp_3 + (-16L)) = 4204935UL;
        indirect_placeholder();
        var_26 = (uint64_t)*(uint32_t *)var_23;
        var_27 = local_sp_3 + (-24L);
        *(uint64_t *)var_27 = 4204962UL;
        indirect_placeholder_20(0UL, var_23, 4273192UL, var_26, 0UL, var_24, var_25);
        *var_8 = (unsigned char)'\x00';
        local_sp_0 = var_27;
    } else {
        var_28 = *var_8;
    }
    local_sp_1 = local_sp_0;
    local_sp_2 = local_sp_0;
    if (var_28 == '\x00') {
        var_45 = *var_9;
        *(uint64_t *)(local_sp_2 + (-8L)) = 4205123UL;
        indirect_placeholder();
        if ((int)var_45 > (int)4294967295U) {
            var_52 = *var_8;
        } else {
            var_46 = *var_7;
            *(uint64_t *)(local_sp_2 + (-16L)) = 4205144UL;
            var_47 = indirect_placeholder_18(var_46, 4UL);
            var_48 = var_47.field_0;
            var_49 = var_47.field_1;
            var_50 = var_47.field_2;
            *(uint64_t *)(local_sp_2 + (-24L)) = 4205152UL;
            indirect_placeholder();
            var_51 = (uint64_t)*(uint32_t *)var_48;
            *(uint64_t *)(local_sp_2 + (-32L)) = 4205179UL;
            indirect_placeholder_17(0UL, var_48, 4273245UL, var_51, 0UL, var_49, var_50);
            *var_8 = (unsigned char)'\x00';
        }
        return (uint64_t)var_52;
    }
    var_29 = (uint32_t *)(var_0 + (-36L));
    *var_29 = 4294967295U;
    var_30 = *var_6;
    if (var_30 == 2U) {
        var_35 = *var_9;
        var_36 = local_sp_0 + (-8L);
        *(uint64_t *)var_36 = 4205047UL;
        indirect_placeholder();
        *var_29 = var_35;
        var_37 = var_35;
        local_sp_1 = var_36;
    } else {
        if (var_30 <= 2U) {
            switch (var_30) {
              case 0U:
                {
                    var_33 = *var_9;
                    var_34 = local_sp_0 + (-8L);
                    *(uint64_t *)var_34 = 4205032UL;
                    indirect_placeholder();
                    *var_29 = var_33;
                    var_37 = var_33;
                    local_sp_1 = var_34;
                }
                break;
              case 1U:
                {
                    var_31 = *var_9;
                    var_32 = local_sp_0 + (-8L);
                    *(uint64_t *)var_32 = 4205017UL;
                    indirect_placeholder();
                    *var_29 = var_31;
                    var_37 = var_31;
                    local_sp_1 = var_32;
                }
                break;
              default:
                {
                    break;
                }
                break;
            }
        }
    }
}
