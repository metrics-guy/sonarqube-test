typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
uint64_t bb_critical_factorization(uint64_t rdx, uint64_t rsi, uint64_t rdi) {
    uint64_t var_27;
    uint64_t var_25;
    uint64_t var_16;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t *var_4;
    uint64_t var_5;
    uint64_t rax_0;
    uint64_t *var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    uint64_t *var_9;
    unsigned char *var_10;
    unsigned char *var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t **var_14;
    uint64_t *var_15;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    unsigned char var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t _pre76;
    uint64_t var_26;
    unsigned char var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t _pre;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_32;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_cc_src2();
    var_2 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    var_3 = (uint64_t *)(var_0 + (-64L));
    *var_3 = rdi;
    var_4 = (uint64_t *)(var_0 + (-72L));
    *var_4 = rsi;
    var_5 = var_0 + (-80L);
    *(uint64_t *)var_5 = rdx;
    var_12 = 1UL;
    var_16 = 1UL;
    _pre76 = 1UL;
    _pre = 1UL;
    if (*var_4 > 2UL) {
        **(uint64_t **)var_5 = 1UL;
        rax_0 = *var_4 + (-1L);
    } else {
        var_6 = (uint64_t *)(var_0 + (-16L));
        *var_6 = 18446744073709551615UL;
        var_7 = (uint64_t *)(var_0 + (-32L));
        *var_7 = 0UL;
        var_8 = (uint64_t *)(var_0 + (-48L));
        *var_8 = 1UL;
        var_9 = (uint64_t *)(var_0 + (-40L));
        *var_9 = 1UL;
        var_10 = (unsigned char *)(var_0 + (-49L));
        var_11 = (unsigned char *)(var_0 + (-50L));
        var_13 = var_12 + *var_7;
        while (*var_4 <= var_13)
            {
                *var_10 = *(unsigned char *)(*var_3 + var_13);
                var_28 = *(unsigned char *)(*var_3 + (*var_6 + *var_9));
                *var_11 = var_28;
                var_29 = (uint64_t)*var_10;
                var_30 = (uint64_t)var_28;
                var_31 = helper_cc_compute_c_wrapper(var_29 - var_30, var_30, var_1, 14U);
                if (var_31 == 0UL) {
                    *var_7 = (*var_7 + *var_9);
                    *var_9 = 1UL;
                    *var_8 = (*var_7 - *var_6);
                    _pre = *var_9;
                } else {
                    if ((uint64_t)(*var_10 - *var_11) == 0UL) {
                        var_32 = *var_7;
                        *var_7 = (var_32 + 1UL);
                        *var_6 = var_32;
                        *var_8 = 1UL;
                        *var_9 = 1UL;
                    } else {
                        var_33 = *var_9;
                        if (var_33 == *var_8) {
                            *var_7 = (*var_7 + var_33);
                            *var_9 = 1UL;
                        } else {
                            var_34 = var_33 + 1UL;
                            *var_9 = var_34;
                            _pre = var_34;
                        }
                    }
                }
                var_12 = _pre;
                var_13 = var_12 + *var_7;
            }
        var_14 = (uint64_t **)var_5;
        **var_14 = *var_8;
        var_15 = (uint64_t *)(var_0 + (-24L));
        *var_15 = 18446744073709551615UL;
        *var_7 = 0UL;
        *var_8 = 1UL;
        *var_9 = 1UL;
        var_17 = var_16 + *var_7;
        while (*var_4 <= var_17)
            {
                *var_10 = *(unsigned char *)(*var_3 + var_17);
                var_21 = *(unsigned char *)(*var_3 + (*var_15 + *var_9));
                *var_11 = var_21;
                var_22 = (uint64_t)var_21;
                var_23 = (uint64_t)*var_10;
                var_24 = helper_cc_compute_c_wrapper(var_22 - var_23, var_23, var_1, 14U);
                if (var_24 == 0UL) {
                    *var_7 = (*var_7 + *var_9);
                    *var_9 = 1UL;
                    *var_8 = (*var_7 - *var_15);
                    _pre76 = *var_9;
                } else {
                    if ((uint64_t)(*var_10 - *var_11) == 0UL) {
                        var_25 = *var_7;
                        *var_7 = (var_25 + 1UL);
                        *var_15 = var_25;
                        *var_8 = 1UL;
                        *var_9 = 1UL;
                    } else {
                        var_26 = *var_9;
                        if (var_26 == *var_8) {
                            *var_7 = (*var_7 + var_26);
                            *var_9 = 1UL;
                        } else {
                            var_27 = var_26 + 1UL;
                            *var_9 = var_27;
                            _pre76 = var_27;
                        }
                    }
                }
                var_16 = _pre76;
                var_17 = var_16 + *var_7;
            }
        var_18 = *var_15 + 1UL;
        var_19 = *var_6 + 1UL;
        var_20 = helper_cc_compute_c_wrapper(var_18 - var_19, var_19, var_1, 17U);
        if (var_20 == 0UL) {
            **var_14 = *var_8;
            rax_0 = *var_15 + 1UL;
        } else {
            rax_0 = *var_6 + 1UL;
        }
    }
    return rax_0;
}
