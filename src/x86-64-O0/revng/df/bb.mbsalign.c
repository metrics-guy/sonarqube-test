typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_130_ret_type;
struct indirect_placeholder_130_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_29(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_130_ret_type indirect_placeholder_130(uint64_t param_0);
uint64_t bb_mbsalign(uint64_t rcx, uint64_t rdx, uint64_t rsi, uint64_t rdi, uint64_t r9, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint64_t var_6;
    uint32_t *var_7;
    uint32_t *var_8;
    uint64_t *var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t *var_13;
    uint64_t *var_14;
    uint64_t *var_15;
    uint64_t var_16;
    uint64_t *var_17;
    uint64_t var_18;
    uint64_t *var_19;
    uint64_t *var_20;
    uint64_t *var_21;
    unsigned char *var_22;
    unsigned char *var_23;
    unsigned char var_25;
    uint64_t local_sp_4;
    uint64_t local_sp_2;
    uint64_t var_42;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_24;
    uint64_t local_sp_0;
    bool var_26;
    uint64_t var_30;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t local_sp_1;
    uint64_t var_31;
    struct indirect_placeholder_130_ret_type var_32;
    uint64_t var_33;
    uint64_t **var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t local_sp_3;
    uint64_t var_43;
    uint64_t var_44;
    uint32_t var_47;
    uint64_t var_45;
    uint32_t var_46;
    uint64_t var_48;
    uint64_t *_pre_phi154;
    uint64_t var_49;
    uint64_t *var_50;
    uint64_t *var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t *var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t *_pre_phi150;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_cc_src2();
    var_2 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    var_3 = (uint64_t *)(var_0 + (-128L));
    *var_3 = rdi;
    var_4 = (uint64_t *)(var_0 + (-136L));
    *var_4 = rsi;
    var_5 = (uint64_t *)(var_0 + (-144L));
    *var_5 = rdx;
    var_6 = var_0 + (-152L);
    *(uint64_t *)var_6 = rcx;
    var_7 = (uint32_t *)(var_0 + (-156L));
    *var_7 = (uint32_t)r8;
    var_8 = (uint32_t *)(var_0 + (-160L));
    *var_8 = (uint32_t)r9;
    var_9 = (uint64_t *)(var_0 + (-16L));
    *var_9 = 18446744073709551615UL;
    var_10 = *var_3;
    var_11 = var_0 + (-176L);
    *(uint64_t *)var_11 = 4230681UL;
    indirect_placeholder();
    var_12 = var_10 + 1UL;
    var_13 = (uint64_t *)(var_0 + (-24L));
    *var_13 = var_12;
    var_14 = (uint64_t *)(var_0 + (-32L));
    *var_14 = 0UL;
    var_15 = (uint64_t *)(var_0 + (-40L));
    *var_15 = 0UL;
    var_16 = *var_3;
    var_17 = (uint64_t *)(var_0 + (-48L));
    *var_17 = var_16;
    var_18 = *var_13 + (-1L);
    var_19 = (uint64_t *)(var_0 + (-56L));
    *var_19 = var_18;
    var_20 = (uint64_t *)(var_0 + (-64L));
    *var_20 = var_18;
    var_21 = (uint64_t *)(var_0 + (-72L));
    *var_21 = 0UL;
    var_22 = (unsigned char *)(var_0 + (-73L));
    *var_22 = (unsigned char)'\x00';
    var_23 = (unsigned char *)(var_0 + (-74L));
    *var_23 = (unsigned char)'\x00';
    var_25 = (unsigned char)'\x00';
    local_sp_0 = var_11;
    if ((*var_8 & 2U) == 0U) {
        var_24 = var_0 + (-184L);
        *(uint64_t *)var_24 = 4230771UL;
        indirect_placeholder();
        var_25 = *var_23;
        local_sp_0 = var_24;
    }
    local_sp_1 = local_sp_0;
    local_sp_2 = local_sp_0;
    if (var_25 == '\x00') {
        var_39 = (uint64_t **)var_6;
        var_40 = **var_39;
        var_41 = *var_19;
        var_42 = var_41;
        var_43 = var_40;
        local_sp_3 = local_sp_2;
        if (var_41 > var_40) {
            *var_19 = var_40;
            *var_20 = var_40;
            var_42 = *var_19;
            var_43 = **var_39;
        }
        var_44 = helper_cc_compute_c_wrapper(var_42 - var_43, var_43, var_1, 17U);
        if (var_44 == 0UL) {
            *var_21 = (**var_39 - *var_19);
        }
        **var_39 = *var_19;
        switch (*var_7) {
          case 1U:
            {
                *(uint64_t *)(var_0 + (-88L)) = *var_21;
                *(uint64_t *)(var_0 + (-96L)) = 0UL;
            }
            break;
          case 0U:
            {
                *(uint64_t *)(var_0 + (-88L)) = 0UL;
                *(uint64_t *)(var_0 + (-96L)) = *var_21;
            }
            break;
          default:
            {
                var_45 = *var_21;
                *(uint64_t *)(var_0 + (-88L)) = ((var_45 & 1UL) + (var_45 >> 1UL));
                *(uint64_t *)(var_0 + (-96L)) = (*var_21 >> 1UL);
            }
            break;
        }
        var_46 = *var_8;
        var_47 = var_46;
        if ((var_46 & 4U) == 0U) {
            *(uint64_t *)(var_0 + (-88L)) = 0UL;
            var_47 = *var_8;
        }
        if ((var_47 & 8U) == 0U) {
            *(uint64_t *)(var_0 + (-96L)) = 0UL;
        }
        var_48 = *var_5;
        if (var_48 == 0UL) {
            var_49 = *var_4 + (var_48 + (-1L));
            var_50 = (uint64_t *)(var_0 + (-112L));
            *var_50 = var_49;
            var_51 = (uint64_t *)(var_0 + (-88L));
            var_52 = *var_51;
            var_53 = *var_4;
            *(uint64_t *)(local_sp_2 + (-8L)) = 4231443UL;
            var_54 = indirect_placeholder_29(var_52, var_49, var_53);
            *var_4 = var_54;
            *(uint64_t *)(var_0 + (-120L)) = (*var_50 - var_54);
            var_55 = *var_4;
            *(uint64_t *)(local_sp_2 + (-16L)) = 4231494UL;
            indirect_placeholder();
            *var_4 = var_55;
            var_56 = (uint64_t *)(var_0 + (-96L));
            var_57 = *var_56;
            var_58 = *var_50;
            var_59 = local_sp_2 + (-24L);
            *(uint64_t *)var_59 = 4231521UL;
            indirect_placeholder_29(var_57, var_58, var_55);
            _pre_phi154 = var_56;
            _pre_phi150 = var_51;
            local_sp_3 = var_59;
        } else {
            _pre_phi154 = (uint64_t *)(var_0 + (-96L));
            _pre_phi150 = (uint64_t *)(var_0 + (-88L));
        }
        *var_9 = (*var_20 + (*_pre_phi150 + *_pre_phi154));
        local_sp_4 = local_sp_3;
        *(uint64_t *)(local_sp_4 + (-8L)) = 4231564UL;
        indirect_placeholder();
        *(uint64_t *)(local_sp_4 + (-16L)) = 4231576UL;
        indirect_placeholder();
        return *var_9;
    }
    var_26 = (*var_22 == '\x00');
    if (var_26) {
        if (*var_19 <= **(uint64_t **)var_6) {
            if (var_26) {
                var_30 = *var_13;
            } else {
                var_27 = *var_15;
                var_28 = local_sp_0 + (-8L);
                *(uint64_t *)var_28 = 4231051UL;
                indirect_placeholder();
                var_29 = var_27 + 1UL;
                *var_13 = var_29;
                var_30 = var_29;
                local_sp_1 = var_28;
            }
            var_31 = local_sp_1 + (-8L);
            *(uint64_t *)var_31 = 4231071UL;
            var_32 = indirect_placeholder_130(var_30);
            var_33 = var_32.field_0;
            *var_14 = var_33;
            local_sp_2 = var_31;
            local_sp_4 = var_31;
            if (var_33 == 0UL) {
                if ((*var_8 & 1U) == 0U) {
                    *(uint64_t *)(local_sp_4 + (-8L)) = 4231564UL;
                    indirect_placeholder();
                    *(uint64_t *)(local_sp_4 + (-16L)) = 4231576UL;
                    indirect_placeholder();
                    return *var_9;
                }
            }
            *var_17 = var_33;
            var_34 = **(uint64_t **)var_6;
            var_35 = *var_15;
            *(uint64_t *)(local_sp_1 + (-16L)) = 4231134UL;
            var_36 = indirect_placeholder_1(var_34, var_35);
            *var_19 = var_36;
            var_37 = *var_14;
            var_38 = local_sp_1 + (-24L);
            *(uint64_t *)var_38 = 4231161UL;
            indirect_placeholder();
            *var_20 = var_37;
            local_sp_2 = var_38;
        }
    } else {
        if (var_26) {
            var_27 = *var_15;
            var_28 = local_sp_0 + (-8L);
            *(uint64_t *)var_28 = 4231051UL;
            indirect_placeholder();
            var_29 = var_27 + 1UL;
            *var_13 = var_29;
            var_30 = var_29;
            local_sp_1 = var_28;
        } else {
            var_30 = *var_13;
        }
        var_31 = local_sp_1 + (-8L);
        *(uint64_t *)var_31 = 4231071UL;
        var_32 = indirect_placeholder_130(var_30);
        var_33 = var_32.field_0;
        *var_14 = var_33;
        local_sp_2 = var_31;
        local_sp_4 = var_31;
        if (var_33 != 0UL) {
            if ((*var_8 & 1U) == 0U) {
                *(uint64_t *)(local_sp_4 + (-8L)) = 4231564UL;
                indirect_placeholder();
                *(uint64_t *)(local_sp_4 + (-16L)) = 4231576UL;
                indirect_placeholder();
                return *var_9;
            }
        }
        *var_17 = var_33;
        var_34 = **(uint64_t **)var_6;
        var_35 = *var_15;
        *(uint64_t *)(local_sp_1 + (-16L)) = 4231134UL;
        var_36 = indirect_placeholder_1(var_34, var_35);
        *var_19 = var_36;
        var_37 = *var_14;
        var_38 = local_sp_1 + (-24L);
        *(uint64_t *)var_38 = 4231161UL;
        indirect_placeholder();
        *var_20 = var_37;
        local_sp_2 = var_38;
    }
}
