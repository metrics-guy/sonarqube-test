typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct helper_divq_EAX_wrapper_ret_type;
struct type_5;
struct indirect_placeholder_8_ret_type;
struct indirect_placeholder_10_ret_type;
struct indirect_placeholder_9_ret_type;
struct indirect_placeholder_12_ret_type;
struct indirect_placeholder_11_ret_type;
struct indirect_placeholder_15_ret_type;
struct indirect_placeholder_14_ret_type;
struct indirect_placeholder_17_ret_type;
struct indirect_placeholder_16_ret_type;
struct indirect_placeholder_19_ret_type;
struct indirect_placeholder_18_ret_type;
struct indirect_placeholder_20_ret_type;
struct helper_divq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint32_t field_3;
    uint64_t field_4;
    uint32_t field_5;
    uint32_t field_6;
    uint64_t field_7;
    uint32_t field_8;
    uint64_t field_9;
    uint32_t field_10;
    unsigned char field_11;
    uint32_t field_12;
};
struct type_5 {
};
struct indirect_placeholder_8_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_10_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_9_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_12_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_11_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_15_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_14_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_17_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_16_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_19_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_18_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_20_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_r10(void);
extern uint64_t init_rbx(void);
extern uint32_t init_state_0x8248(void);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern struct helper_divq_EAX_wrapper_ret_type helper_divq_EAX_wrapper(struct type_5 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint32_t param_13, uint64_t param_14, uint32_t param_15, uint64_t param_16, uint64_t param_17, uint64_t param_18, uint32_t param_19);
extern uint64_t indirect_placeholder_13(void);
extern uint64_t indirect_placeholder_4(uint64_t param_0);
extern void indirect_placeholder_2(uint64_t param_0);
extern struct indirect_placeholder_8_ret_type indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_10_ret_type indirect_placeholder_10(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_9_ret_type indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_12_ret_type indirect_placeholder_12(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_11_ret_type indirect_placeholder_11(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_15_ret_type indirect_placeholder_15(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_14_ret_type indirect_placeholder_14(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_17_ret_type indirect_placeholder_17(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_16_ret_type indirect_placeholder_16(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_19_ret_type indirect_placeholder_19(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_18_ret_type indirect_placeholder_18(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_20_ret_type indirect_placeholder_20(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
uint64_t bb_find_mount_point(uint64_t rcx, uint64_t rdx, uint64_t rsi, uint64_t rdi, uint64_t r9, uint64_t r8) {
    uint64_t var_16;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint32_t var_4;
    uint64_t var_5;
    uint32_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint32_t var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t *var_13;
    uint64_t *var_14;
    uint64_t var_15;
    uint64_t var_149;
    uint64_t var_150;
    uint64_t r95_0;
    uint64_t r86_0;
    uint32_t *rax_0;
    uint64_t local_sp_2_ph;
    uint64_t local_sp_0;
    uint64_t var_151;
    uint64_t rax_1;
    struct indirect_placeholder_10_ret_type var_124;
    uint64_t var_125;
    uint64_t var_126;
    uint64_t var_127;
    uint64_t var_128;
    uint64_t var_129;
    struct indirect_placeholder_9_ret_type var_130;
    uint64_t var_131;
    uint64_t var_132;
    uint64_t var_133;
    uint64_t rcx1_1_ph;
    uint64_t var_134;
    uint64_t *_pre_phi251;
    uint64_t var_135;
    uint64_t *_pre_phi255;
    uint64_t *_pre_phi259;
    uint64_t var_136;
    uint64_t *_pre_phi263;
    uint64_t *_pre_phi267;
    uint64_t var_137;
    uint64_t *_pre_phi271;
    uint64_t *_pre_phi275;
    uint64_t var_138;
    uint64_t *_pre_phi279;
    uint64_t *_pre_phi283;
    uint64_t var_139;
    uint64_t *_pre_phi287;
    uint64_t *_pre_phi291;
    uint64_t var_140;
    uint64_t *_pre_phi295;
    uint64_t *_pre_phi299;
    uint64_t var_141;
    uint64_t *_pre_phi303;
    uint64_t *_pre_phi307;
    uint64_t var_142;
    uint64_t *_pre_phi311;
    uint64_t *_pre_phi315;
    uint64_t local_sp_2;
    struct indirect_placeholder_12_ret_type var_111;
    uint64_t var_112;
    uint64_t var_113;
    uint64_t var_114;
    uint64_t var_115;
    uint64_t var_116;
    struct indirect_placeholder_11_ret_type var_117;
    uint64_t var_118;
    uint64_t var_119;
    uint64_t var_120;
    uint64_t var_121;
    uint64_t var_122;
    uint64_t var_123;
    uint64_t var_143;
    uint64_t var_144;
    uint32_t *phitmp;
    uint64_t rcx1_0;
    uint64_t local_sp_1;
    uint32_t var_145;
    uint32_t *var_146;
    uint64_t var_147;
    uint64_t var_148;
    uint64_t var_37;
    struct indirect_placeholder_15_ret_type var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    struct indirect_placeholder_14_ret_type var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_29;
    struct indirect_placeholder_17_ret_type var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_86;
    struct indirect_placeholder_19_ret_type var_87;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t *var_92;
    uint64_t *var_93;
    uint64_t *_pre_phi319;
    uint64_t *var_94;
    uint64_t *var_95;
    uint64_t *var_96;
    uint64_t *var_97;
    uint64_t *var_98;
    uint64_t *var_99;
    uint64_t *var_100;
    uint64_t *var_101;
    uint64_t *var_102;
    uint64_t *var_103;
    uint64_t *var_104;
    uint64_t *var_105;
    uint64_t *var_106;
    uint64_t *var_107;
    uint64_t *var_108;
    uint64_t *var_109;
    uint64_t var_110;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t *var_50;
    uint64_t *var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t *var_54;
    uint64_t *var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t *var_58;
    uint64_t *var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t *var_62;
    uint64_t *var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t *var_66;
    uint64_t *var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t *var_70;
    uint64_t *var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t *var_74;
    uint64_t *var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t *var_78;
    uint64_t *var_79;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t *var_82;
    uint64_t *var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t *var_22;
    uint64_t var_23;
    uint64_t var_25;
    struct helper_divq_EAX_wrapper_ret_type var_24;
    uint64_t var_26;
    uint64_t *var_27;
    uint64_t var_28;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = init_r10();
    var_4 = init_state_0x8248();
    var_5 = init_state_0x9018();
    var_6 = init_state_0x9010();
    var_7 = init_state_0x8408();
    var_8 = init_state_0x8328();
    var_9 = init_state_0x82d8();
    var_10 = init_state_0x9080();
    var_11 = var_0 + (-8L);
    *(uint64_t *)var_11 = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_12 = (uint64_t *)(var_0 + (-368L));
    *var_12 = rdi;
    var_13 = (uint64_t *)(var_0 + (-376L));
    *var_13 = rsi;
    var_14 = (uint64_t *)(var_0 + (-32L));
    *var_14 = 0UL;
    var_15 = var_0 + (-72L);
    *(uint64_t *)(var_0 + (-384L)) = 4218273UL;
    var_16 = indirect_placeholder_7(rcx, rdx, var_15, r9, r8);
    r95_0 = r9;
    r86_0 = r8;
    rax_0 = (uint32_t *)0UL;
    rax_1 = 0UL;
    rcx1_1_ph = rcx;
    if ((uint64_t)(uint32_t)var_16 != 0UL) {
        *(uint64_t *)(var_0 + (-392L)) = 4218282UL;
        indirect_placeholder();
        var_17 = (uint64_t)*(uint32_t *)var_16;
        *(uint64_t *)(var_0 + (-400L)) = 4218306UL;
        indirect_placeholder_20(0UL, rcx, 4318176UL, var_17, 0UL, r9, r8);
        return rax_1;
    }
    var_18 = *var_13;
    var_19 = var_18 + 24UL;
    if ((uint32_t)((uint16_t)*(uint32_t *)var_19 & (unsigned short)61440U) == 16384U) {
        var_48 = *(uint64_t *)var_18;
        var_49 = *(uint64_t *)(var_18 + 8UL);
        var_50 = (uint64_t *)(var_0 + (-216L));
        *var_50 = var_48;
        var_51 = (uint64_t *)(var_0 + (-208L));
        *var_51 = var_49;
        var_52 = *(uint64_t *)(var_18 + 16UL);
        var_53 = *(uint64_t *)var_19;
        var_54 = (uint64_t *)(var_0 + (-200L));
        *var_54 = var_52;
        var_55 = (uint64_t *)(var_0 + (-192L));
        *var_55 = var_53;
        var_56 = *(uint64_t *)(var_18 + 32UL);
        var_57 = *(uint64_t *)(var_18 + 40UL);
        var_58 = (uint64_t *)(var_0 + (-184L));
        *var_58 = var_56;
        var_59 = (uint64_t *)(var_0 + (-176L));
        *var_59 = var_57;
        var_60 = *(uint64_t *)(var_18 + 48UL);
        var_61 = *(uint64_t *)(var_18 + 56UL);
        var_62 = (uint64_t *)(var_0 + (-168L));
        *var_62 = var_60;
        var_63 = (uint64_t *)(var_0 + (-160L));
        *var_63 = var_61;
        var_64 = *(uint64_t *)(var_18 + 64UL);
        var_65 = *(uint64_t *)(var_18 + 72UL);
        var_66 = (uint64_t *)(var_0 + (-152L));
        *var_66 = var_64;
        var_67 = (uint64_t *)(var_0 + (-144L));
        *var_67 = var_65;
        var_68 = *(uint64_t *)(var_18 + 80UL);
        var_69 = *(uint64_t *)(var_18 + 88UL);
        var_70 = (uint64_t *)(var_0 + (-136L));
        *var_70 = var_68;
        var_71 = (uint64_t *)(var_0 + (-128L));
        *var_71 = var_69;
        var_72 = *(uint64_t *)(var_18 + 96UL);
        var_73 = *(uint64_t *)(var_18 + 104UL);
        var_74 = (uint64_t *)(var_0 + (-120L));
        *var_74 = var_72;
        var_75 = (uint64_t *)(var_0 + (-112L));
        *var_75 = var_73;
        var_76 = *(uint64_t *)(var_18 + 112UL);
        var_77 = *(uint64_t *)(var_18 + 120UL);
        var_78 = (uint64_t *)(var_0 + (-104L));
        *var_78 = var_76;
        var_79 = (uint64_t *)(var_0 + (-96L));
        *var_79 = var_77;
        var_80 = *(uint64_t *)(var_18 + 136UL);
        var_81 = *(uint64_t *)(var_18 + 128UL);
        var_82 = (uint64_t *)(var_0 + (-88L));
        *var_82 = var_81;
        var_83 = (uint64_t *)(var_0 + (-80L));
        *var_83 = var_80;
        var_84 = *var_12;
        var_85 = var_0 + (-392L);
        *(uint64_t *)var_85 = 4218543UL;
        indirect_placeholder();
        local_sp_2_ph = var_85;
        rcx1_1_ph = var_76;
        _pre_phi251 = var_50;
        _pre_phi255 = var_51;
        _pre_phi259 = var_54;
        _pre_phi263 = var_55;
        _pre_phi267 = var_58;
        _pre_phi271 = var_59;
        _pre_phi275 = var_62;
        _pre_phi279 = var_63;
        _pre_phi283 = var_66;
        _pre_phi287 = var_67;
        _pre_phi291 = var_70;
        _pre_phi295 = var_71;
        _pre_phi299 = var_74;
        _pre_phi303 = var_75;
        _pre_phi307 = var_78;
        _pre_phi311 = var_79;
        _pre_phi315 = var_82;
        _pre_phi319 = var_83;
        if ((int)(uint32_t)var_84 <= (int)4294967295U) {
            var_86 = *var_12;
            *(uint64_t *)(var_0 + (-400L)) = 4218571UL;
            var_87 = indirect_placeholder_19(var_86, 4UL);
            var_88 = var_87.field_0;
            var_89 = var_87.field_1;
            var_90 = var_87.field_2;
            *(uint64_t *)(var_0 + (-408L)) = 4218579UL;
            indirect_placeholder();
            var_91 = (uint64_t)*(uint32_t *)var_88;
            *(uint64_t *)(var_0 + (-416L)) = 4218606UL;
            indirect_placeholder_18(0UL, var_88, 4318205UL, var_91, 0UL, var_89, var_90);
            return rax_1;
        }
    }
    var_20 = *var_12;
    *(uint64_t *)(var_0 + (-392L)) = 4218631UL;
    var_21 = indirect_placeholder_4(var_20);
    var_22 = (uint64_t *)(var_0 + (-40L));
    *var_22 = var_21;
    var_23 = var_0 + (-400L);
    *(uint64_t *)var_23 = 4218647UL;
    indirect_placeholder();
    var_24 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), 16UL, 4218677UL, var_21 + 24UL, var_11, rcx, 0UL, 16UL, rsi, var_21, var_3, r9, r8, var_4, var_5, var_6, var_7, var_8, var_9, var_10);
    var_25 = var_23 - (var_24.field_1 << 4UL);
    var_26 = *var_22;
    *(uint64_t *)(var_25 + (-8L)) = 4218720UL;
    indirect_placeholder();
    var_27 = (uint64_t *)(var_0 + (-48L));
    *var_27 = var_26;
    *(uint64_t *)(var_25 + (-16L)) = 4218736UL;
    indirect_placeholder();
    var_28 = *var_27;
    *(uint64_t *)(var_25 + (-24L)) = 4218748UL;
    indirect_placeholder();
    if ((int)(uint32_t)var_28 <= (int)4294967295U) {
        var_29 = *var_27;
        *(uint64_t *)(var_25 + (-32L)) = 4218769UL;
        var_30 = indirect_placeholder_17(var_29, 4UL);
        var_31 = var_30.field_0;
        var_32 = var_30.field_1;
        var_33 = var_30.field_2;
        *(uint64_t *)(var_25 + (-40L)) = 4218777UL;
        indirect_placeholder();
        var_34 = (uint64_t)*(uint32_t *)var_31;
        *(uint64_t *)(var_25 + (-48L)) = 4218804UL;
        indirect_placeholder_16(0UL, var_31, 4318205UL, var_34, 0UL, var_32, var_33);
        return rax_1;
    }
    var_35 = var_25 + (-32L);
    *(uint64_t *)var_35 = 4218834UL;
    var_36 = indirect_placeholder_4(4318235UL);
    local_sp_2_ph = var_35;
    if ((int)(uint32_t)var_36 <= (int)4294967295U) {
        var_37 = *var_27;
        *(uint64_t *)(var_25 + (-40L)) = 4218855UL;
        var_38 = indirect_placeholder_15(var_37, 4UL);
        var_39 = var_38.field_0;
        var_40 = var_38.field_1;
        var_41 = var_38.field_2;
        *(uint64_t *)(var_25 + (-48L)) = 4218863UL;
        indirect_placeholder();
        var_42 = (uint64_t)*(uint32_t *)var_39;
        var_43 = var_25 + (-56L);
        *(uint64_t *)var_43 = 4218890UL;
        var_44 = indirect_placeholder_14(0UL, var_39, 4318240UL, var_42, 0UL, var_40, var_41);
        var_45 = var_44.field_0;
        var_46 = var_44.field_1;
        var_47 = var_44.field_2;
        rcx1_0 = var_45;
        r95_0 = var_46;
        r86_0 = var_47;
        local_sp_1 = var_43;
        *(uint64_t *)(local_sp_1 + (-8L)) = 4219336UL;
        indirect_placeholder();
        var_145 = *rax_0;
        var_146 = (uint32_t *)(var_0 + (-52L));
        *var_146 = var_145;
        var_147 = local_sp_1 + (-16L);
        *(uint64_t *)var_147 = 4219353UL;
        var_148 = indirect_placeholder_4(var_15);
        local_sp_0 = var_147;
        if ((uint64_t)(uint32_t)var_148 == 0UL) {
            *(uint64_t *)(local_sp_1 + (-24L)) = 4219362UL;
            indirect_placeholder();
            var_149 = (uint64_t)*(uint32_t *)var_148;
            var_150 = local_sp_1 + (-32L);
            *(uint64_t *)var_150 = 4219386UL;
            indirect_placeholder_8(0UL, rcx1_0, 4318304UL, var_149, 1UL, r95_0, r86_0);
            local_sp_0 = var_150;
        }
        *(uint64_t *)(local_sp_0 + (-8L)) = 4219398UL;
        indirect_placeholder_2(var_15);
        *(uint64_t *)(local_sp_0 + (-16L)) = 4219403UL;
        indirect_placeholder();
        *(uint32_t *)var_15 = *var_146;
        var_151 = *var_14;
        rax_1 = var_151;
        return rax_1;
    }
    _pre_phi251 = (uint64_t *)(var_0 + (-216L));
    _pre_phi255 = (uint64_t *)(var_0 + (-208L));
    _pre_phi259 = (uint64_t *)(var_0 + (-200L));
    _pre_phi263 = (uint64_t *)(var_0 + (-192L));
    _pre_phi267 = (uint64_t *)(var_0 + (-184L));
    _pre_phi271 = (uint64_t *)(var_0 + (-176L));
    _pre_phi275 = (uint64_t *)(var_0 + (-168L));
    _pre_phi279 = (uint64_t *)(var_0 + (-160L));
    _pre_phi283 = (uint64_t *)(var_0 + (-152L));
    _pre_phi287 = (uint64_t *)(var_0 + (-144L));
    _pre_phi291 = (uint64_t *)(var_0 + (-136L));
    _pre_phi295 = (uint64_t *)(var_0 + (-128L));
    _pre_phi299 = (uint64_t *)(var_0 + (-120L));
    _pre_phi303 = (uint64_t *)(var_0 + (-112L));
    _pre_phi307 = (uint64_t *)(var_0 + (-104L));
    _pre_phi311 = (uint64_t *)(var_0 + (-96L));
    _pre_phi315 = (uint64_t *)(var_0 + (-88L));
    _pre_phi319 = (uint64_t *)(var_0 + (-80L));
    var_92 = (uint64_t *)(var_0 + (-360L));
    var_93 = (uint64_t *)(var_0 + (-352L));
    var_94 = (uint64_t *)(var_0 + (-344L));
    var_95 = (uint64_t *)(var_0 + (-336L));
    var_96 = (uint64_t *)(var_0 + (-328L));
    var_97 = (uint64_t *)(var_0 + (-320L));
    var_98 = (uint64_t *)(var_0 + (-312L));
    var_99 = (uint64_t *)(var_0 + (-304L));
    var_100 = (uint64_t *)(var_0 + (-296L));
    var_101 = (uint64_t *)(var_0 + (-288L));
    var_102 = (uint64_t *)(var_0 + (-280L));
    var_103 = (uint64_t *)(var_0 + (-272L));
    var_104 = (uint64_t *)(var_0 + (-264L));
    var_105 = (uint64_t *)(var_0 + (-256L));
    var_106 = (uint64_t *)(var_0 + (-248L));
    var_107 = (uint64_t *)(var_0 + (-240L));
    var_108 = (uint64_t *)(var_0 + (-232L));
    var_109 = (uint64_t *)(var_0 + (-224L));
    rcx1_0 = rcx1_1_ph;
    local_sp_2 = local_sp_2_ph;
    while (1U)
        {
            *(uint64_t *)(local_sp_2 + (-8L)) = 4218915UL;
            var_110 = indirect_placeholder_4(4318279UL);
            if ((int)(uint32_t)var_110 <= (int)4294967295U) {
                *(uint64_t *)(local_sp_2 + (-16L)) = 4218934UL;
                var_111 = indirect_placeholder_12(4318279UL, 4UL);
                var_112 = var_111.field_0;
                var_113 = var_111.field_1;
                var_114 = var_111.field_2;
                *(uint64_t *)(local_sp_2 + (-24L)) = 4218942UL;
                indirect_placeholder();
                var_115 = (uint64_t)*(uint32_t *)var_112;
                var_116 = local_sp_2 + (-32L);
                *(uint64_t *)var_116 = 4218969UL;
                var_117 = indirect_placeholder_11(0UL, var_112, 4318282UL, var_115, 0UL, var_113, var_114);
                var_118 = var_117.field_0;
                var_119 = var_117.field_1;
                var_120 = var_117.field_2;
                rcx1_0 = var_118;
                r95_0 = var_119;
                r86_0 = var_120;
                local_sp_1 = var_116;
                loop_state_var = 1U;
                break;
            }
            if (*var_92 != *_pre_phi251) {
                loop_state_var = 0U;
                break;
            }
            var_121 = *var_93;
            var_122 = *_pre_phi255;
            if (var_121 != var_122) {
                loop_state_var = 0U;
                break;
            }
            var_123 = local_sp_2 + (-16L);
            *(uint64_t *)var_123 = 4219030UL;
            indirect_placeholder();
            local_sp_2 = var_123;
            if ((int)(uint32_t)var_122 <= (int)4294967295U) {
                *(uint64_t *)(local_sp_2 + (-24L)) = 4219049UL;
                var_124 = indirect_placeholder_10(4318279UL, 4UL);
                var_125 = var_124.field_0;
                var_126 = var_124.field_1;
                var_127 = var_124.field_2;
                *(uint64_t *)(local_sp_2 + (-32L)) = 4219057UL;
                indirect_placeholder();
                var_128 = (uint64_t)*(uint32_t *)var_125;
                var_129 = local_sp_2 + (-40L);
                *(uint64_t *)var_129 = 4219084UL;
                var_130 = indirect_placeholder_9(0UL, var_125, 4318205UL, var_128, 0UL, var_126, var_127);
                var_131 = var_130.field_0;
                var_132 = var_130.field_1;
                var_133 = var_130.field_2;
                rcx1_0 = var_131;
                r95_0 = var_132;
                r86_0 = var_133;
                local_sp_1 = var_129;
                loop_state_var = 1U;
                break;
            }
            var_134 = *var_93;
            *_pre_phi251 = *var_92;
            *_pre_phi255 = var_134;
            var_135 = *var_95;
            *_pre_phi259 = *var_94;
            *_pre_phi263 = var_135;
            var_136 = *var_97;
            *_pre_phi267 = *var_96;
            *_pre_phi271 = var_136;
            var_137 = *var_99;
            *_pre_phi275 = *var_98;
            *_pre_phi279 = var_137;
            var_138 = *var_101;
            *_pre_phi283 = *var_100;
            *_pre_phi287 = var_138;
            var_139 = *var_103;
            *_pre_phi291 = *var_102;
            *_pre_phi295 = var_139;
            var_140 = *var_105;
            *_pre_phi299 = *var_104;
            *_pre_phi303 = var_140;
            var_141 = *var_107;
            *_pre_phi307 = *var_106;
            *_pre_phi311 = var_141;
            var_142 = *var_109;
            *_pre_phi315 = *var_108;
            *_pre_phi319 = var_142;
            continue;
        }
    switch (loop_state_var) {
      case 0U:
        {
            var_143 = local_sp_2 + (-16L);
            *(uint64_t *)var_143 = 4219327UL;
            var_144 = indirect_placeholder_13();
            *var_14 = var_144;
            phitmp = (uint32_t *)var_144;
            rax_0 = phitmp;
            local_sp_1 = var_143;
        }
        break;
      case 1U:
        {
            *(uint64_t *)(local_sp_1 + (-8L)) = 4219336UL;
            indirect_placeholder();
            var_145 = *rax_0;
            var_146 = (uint32_t *)(var_0 + (-52L));
            *var_146 = var_145;
            var_147 = local_sp_1 + (-16L);
            *(uint64_t *)var_147 = 4219353UL;
            var_148 = indirect_placeholder_4(var_15);
            local_sp_0 = var_147;
            if ((uint64_t)(uint32_t)var_148 != 0UL) {
                *(uint64_t *)(local_sp_1 + (-24L)) = 4219362UL;
                indirect_placeholder();
                var_149 = (uint64_t)*(uint32_t *)var_148;
                var_150 = local_sp_1 + (-32L);
                *(uint64_t *)var_150 = 4219386UL;
                indirect_placeholder_8(0UL, rcx1_0, 4318304UL, var_149, 1UL, r95_0, r86_0);
                local_sp_0 = var_150;
            }
            *(uint64_t *)(local_sp_0 + (-8L)) = 4219398UL;
            indirect_placeholder_2(var_15);
            *(uint64_t *)(local_sp_0 + (-16L)) = 4219403UL;
            indirect_placeholder();
            *(uint32_t *)var_15 = *var_146;
            var_151 = *var_14;
            rax_1 = var_151;
        }
        break;
    }
}
