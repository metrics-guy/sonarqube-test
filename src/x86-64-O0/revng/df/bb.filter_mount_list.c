typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_58_ret_type;
struct indirect_placeholder_59_ret_type;
struct indirect_placeholder_58_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_59_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_13(void);
extern uint64_t indirect_placeholder_4(uint64_t param_0);
extern void indirect_placeholder_27(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_58_ret_type indirect_placeholder_58(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_59_ret_type indirect_placeholder_59(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_filter_mount_list(uint64_t rdi) {
    struct indirect_placeholder_59_ret_type var_17;
    uint64_t local_sp_2;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    unsigned char *var_5;
    uint64_t *var_6;
    uint32_t *var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_66;
    uint64_t _pre;
    uint64_t local_sp_6_be;
    uint64_t var_27;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t local_sp_0;
    uint64_t local_sp_8;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t local_sp_5;
    uint64_t local_sp_1;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_69;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t local_sp_4;
    uint64_t local_sp_3;
    unsigned char storemerge;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_44;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t local_sp_7;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_19;
    uint64_t *var_20;
    uint64_t *var_21;
    uint64_t *var_22;
    unsigned char *var_23;
    unsigned char *var_24;
    uint64_t var_25;
    uint64_t *var_26;
    uint64_t local_sp_6;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_28;
    unsigned char var_29;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    struct indirect_placeholder_58_ret_type var_65;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_11;
    bool var_12;
    uint32_t var_13;
    uint64_t var_18;
    uint64_t var_14;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_cc_src2();
    var_2 = init_rbp();
    var_3 = init_rbx();
    var_4 = var_0 + (-8L);
    *(uint64_t *)var_4 = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    var_5 = (unsigned char *)(var_0 + (-236L));
    *var_5 = (unsigned char)rdi;
    var_6 = (uint64_t *)(var_0 + (-40L));
    *var_6 = 0UL;
    var_7 = (uint32_t *)(var_0 + (-44L));
    *var_7 = 0U;
    var_8 = *(uint64_t *)4343536UL;
    var_9 = var_0 + (-32L);
    var_10 = (uint64_t *)var_9;
    *var_10 = var_8;
    storemerge = (unsigned char)'\x00';
    var_11 = var_8;
    var_12 = (var_11 == 0UL);
    var_13 = *var_7;
    while (!var_12)
        {
            *var_7 = (var_13 + 1U);
            var_14 = *(uint64_t *)(*var_10 + 48UL);
            *var_10 = var_14;
            var_11 = var_14;
            var_12 = (var_11 == 0UL);
            var_13 = *var_7;
        }
    var_15 = (uint64_t)var_13;
    var_16 = var_0 + (-256L);
    *(uint64_t *)var_16 = 4208180UL;
    var_17 = indirect_placeholder_59(4207935UL, 4207894UL, 0UL, var_15, 4208048UL);
    var_18 = var_17.field_0;
    *(uint64_t *)4343488UL = var_18;
    local_sp_6 = var_16;
    if (var_18 == 0UL) {
        var_72 = var_17.field_2;
        var_73 = var_17.field_1;
        *(uint64_t *)(var_0 + (-264L)) = 4208204UL;
        indirect_placeholder_27(var_4, var_73, var_72);
        abort();
    }
    var_19 = *(uint64_t *)4343536UL;
    *var_10 = var_19;
    var_20 = (uint64_t *)(var_0 + (-56L));
    var_21 = (uint64_t *)(var_0 + (-232L));
    var_22 = (uint64_t *)(var_0 + (-64L));
    var_23 = (unsigned char *)(var_0 + (-65L));
    var_24 = (unsigned char *)(var_0 + (-66L));
    var_25 = var_0 + (-80L);
    var_26 = (uint64_t *)var_25;
    var_27 = var_19;
    while (1U)
        {
            local_sp_7 = local_sp_6;
            if (var_27 != 0UL) {
                if (*var_5 != '\x01') {
                    loop_state_var = 0U;
                    break;
                }
                *(uint64_t *)4343536UL = 0UL;
                var_69 = *var_6;
                loop_state_var = 1U;
                break;
            }
            *var_20 = 0UL;
            var_28 = *var_10;
            var_29 = *(unsigned char *)(var_28 + 40UL);
            if ((var_29 & '\x02') != '\x00') {
                if (*(unsigned char *)4343497UL != '\x00') {
                    *var_21 = *(uint64_t *)(*var_10 + 32UL);
                    local_sp_8 = local_sp_7;
                    local_sp_6_be = local_sp_8;
                    if (*var_20 == 0UL) {
                        *(uint64_t *)(local_sp_8 + (-8L)) = 4208877UL;
                        var_61 = indirect_placeholder_4(24UL);
                        *var_26 = var_61;
                        *(uint64_t *)(var_61 + 8UL) = *var_10;
                        **(uint64_t **)var_25 = *var_21;
                        *(uint64_t *)(*var_26 + 16UL) = *var_6;
                        *var_6 = *var_26;
                        var_62 = *(uint64_t *)4343488UL;
                        var_63 = *var_26;
                        var_64 = local_sp_8 + (-16L);
                        *(uint64_t *)var_64 = 4208949UL;
                        var_65 = indirect_placeholder_58(var_63, var_62);
                        local_sp_6_be = var_64;
                        if (var_65.field_0 != 0UL) {
                            var_67 = var_65.field_1;
                            var_68 = var_65.field_2;
                            *(uint64_t *)(local_sp_8 + (-24L)) = 4208959UL;
                            indirect_placeholder_27(var_4, var_67, var_68);
                            abort();
                        }
                        var_66 = *(uint64_t *)(*var_10 + 48UL);
                        *var_10 = var_66;
                        _pre = var_66;
                    } else {
                        var_59 = *(uint64_t *)(*var_10 + 48UL);
                        *var_10 = var_59;
                        _pre = var_59;
                        if (*var_5 == '\x01') {
                            var_60 = local_sp_8 + (-8L);
                            *(uint64_t *)var_60 = 4208865UL;
                            indirect_placeholder();
                            _pre = *var_10;
                            local_sp_6_be = var_60;
                        }
                    }
                    var_27 = _pre;
                    local_sp_6 = local_sp_6_be;
                    continue;
                }
            }
            if ((var_29 & '\x01') != '\x00') {
                if (*(unsigned char *)4343496UL != '\x01' & *(unsigned char *)4343498UL != '\x01') {
                    *var_21 = *(uint64_t *)(*var_10 + 32UL);
                    local_sp_8 = local_sp_7;
                    local_sp_6_be = local_sp_8;
                    if (*var_20 == 0UL) {
                        *(uint64_t *)(local_sp_8 + (-8L)) = 4208877UL;
                        var_61 = indirect_placeholder_4(24UL);
                        *var_26 = var_61;
                        *(uint64_t *)(var_61 + 8UL) = *var_10;
                        **(uint64_t **)var_25 = *var_21;
                        *(uint64_t *)(*var_26 + 16UL) = *var_6;
                        *var_6 = *var_26;
                        var_62 = *(uint64_t *)4343488UL;
                        var_63 = *var_26;
                        var_64 = local_sp_8 + (-16L);
                        *(uint64_t *)var_64 = 4208949UL;
                        var_65 = indirect_placeholder_58(var_63, var_62);
                        local_sp_6_be = var_64;
                        if (var_65.field_0 != 0UL) {
                            var_67 = var_65.field_1;
                            var_68 = var_65.field_2;
                            *(uint64_t *)(local_sp_8 + (-24L)) = 4208959UL;
                            indirect_placeholder_27(var_4, var_67, var_68);
                            abort();
                        }
                        var_66 = *(uint64_t *)(*var_10 + 48UL);
                        *var_10 = var_66;
                        _pre = var_66;
                    } else {
                        var_59 = *(uint64_t *)(*var_10 + 48UL);
                        *var_10 = var_59;
                        _pre = var_59;
                        if (*var_5 == '\x01') {
                            var_60 = local_sp_8 + (-8L);
                            *(uint64_t *)var_60 = 4208865UL;
                            indirect_placeholder();
                            _pre = *var_10;
                            local_sp_6_be = var_60;
                        }
                    }
                    var_27 = _pre;
                    local_sp_6 = local_sp_6_be;
                    continue;
                }
            }
            var_30 = *(uint64_t *)(var_28 + 24UL);
            var_31 = local_sp_6 + (-8L);
            *(uint64_t *)var_31 = 4208313UL;
            var_32 = indirect_placeholder_4(var_30);
            local_sp_7 = var_31;
            if ((uint64_t)(unsigned char)var_32 != 1UL) {
                *var_21 = *(uint64_t *)(*var_10 + 32UL);
                local_sp_8 = local_sp_7;
                local_sp_6_be = local_sp_8;
                if (*var_20 == 0UL) {
                    *(uint64_t *)(local_sp_8 + (-8L)) = 4208877UL;
                    var_61 = indirect_placeholder_4(24UL);
                    *var_26 = var_61;
                    *(uint64_t *)(var_61 + 8UL) = *var_10;
                    **(uint64_t **)var_25 = *var_21;
                    *(uint64_t *)(*var_26 + 16UL) = *var_6;
                    *var_6 = *var_26;
                    var_62 = *(uint64_t *)4343488UL;
                    var_63 = *var_26;
                    var_64 = local_sp_8 + (-16L);
                    *(uint64_t *)var_64 = 4208949UL;
                    var_65 = indirect_placeholder_58(var_63, var_62);
                    local_sp_6_be = var_64;
                    if (var_65.field_0 != 0UL) {
                        var_67 = var_65.field_1;
                        var_68 = var_65.field_2;
                        *(uint64_t *)(local_sp_8 + (-24L)) = 4208959UL;
                        indirect_placeholder_27(var_4, var_67, var_68);
                        abort();
                    }
                    var_66 = *(uint64_t *)(*var_10 + 48UL);
                    *var_10 = var_66;
                    _pre = var_66;
                } else {
                    var_59 = *(uint64_t *)(*var_10 + 48UL);
                    *var_10 = var_59;
                    _pre = var_59;
                    if (*var_5 == '\x01') {
                        var_60 = local_sp_8 + (-8L);
                        *(uint64_t *)var_60 = 4208865UL;
                        indirect_placeholder();
                        _pre = *var_10;
                        local_sp_6_be = var_60;
                    }
                }
                var_27 = _pre;
                local_sp_6 = local_sp_6_be;
                continue;
            }
            var_33 = local_sp_6 + (-16L);
            *(uint64_t *)var_33 = 4208336UL;
            var_34 = indirect_placeholder_13();
            local_sp_7 = var_33;
            if ((uint64_t)(unsigned char)var_34 == 0UL) {
                var_35 = *(uint64_t *)(*var_10 + 8UL);
                var_36 = local_sp_6 + (-24L);
                *(uint64_t *)var_36 = 4208366UL;
                var_37 = indirect_placeholder_4(var_35);
                local_sp_7 = var_36;
                if ((uint64_t)((uint32_t)var_37 + 1U) == 0UL) {
                    *var_21 = *(uint64_t *)(*var_10 + 32UL);
                    local_sp_8 = local_sp_7;
                } else {
                    var_38 = *var_21;
                    var_39 = local_sp_6 + (-32L);
                    *(uint64_t *)var_39 = 4208406UL;
                    var_40 = indirect_placeholder_4(var_38);
                    *var_22 = var_40;
                    local_sp_8 = var_39;
                    if (var_40 != 0UL) {
                        var_41 = *(uint64_t *)(*(uint64_t *)(var_40 + 8UL) + 8UL);
                        *(uint64_t *)(local_sp_6 + (-40L)) = 4208441UL;
                        indirect_placeholder();
                        var_42 = *(uint64_t *)(*var_10 + 8UL);
                        var_43 = local_sp_6 + (-48L);
                        *(uint64_t *)var_43 = 4208460UL;
                        indirect_placeholder();
                        *var_23 = (var_41 > var_42);
                        var_44 = *(uint64_t *)(*(uint64_t *)(*var_22 + 8UL) + 16UL);
                        local_sp_3 = var_43;
                        if (var_44 == 0UL) {
                            local_sp_4 = local_sp_3;
                        } else {
                            storemerge = (unsigned char)'\x01';
                            if (*(uint64_t *)(*var_10 + 16UL) == 0UL) {
                                local_sp_4 = local_sp_3;
                            } else {
                                *(uint64_t *)(local_sp_6 + (-56L)) = 4208519UL;
                                indirect_placeholder();
                                var_45 = *(uint64_t *)(*var_10 + 16UL);
                                var_46 = local_sp_6 + (-64L);
                                *(uint64_t *)var_46 = 4208538UL;
                                indirect_placeholder();
                                var_47 = helper_cc_compute_c_wrapper(var_44 - var_45, var_45, var_1, 17U);
                                local_sp_3 = var_46;
                                local_sp_4 = var_46;
                                if (var_47 == 0UL) {
                                    local_sp_4 = local_sp_3;
                                }
                            }
                        }
                        *var_24 = (storemerge & '\x01');
                        local_sp_5 = local_sp_4;
                        if (*(unsigned char *)4343545UL != '\x01') {
                            var_48 = *var_22 + 8UL;
                            var_49 = **(uint64_t **)var_48;
                            var_50 = local_sp_4 + (-8L);
                            *(uint64_t *)var_50 = 4208639UL;
                            indirect_placeholder();
                            local_sp_5 = var_50;
                            local_sp_8 = var_50;
                            if ((*(unsigned char *)(*var_10 + 40UL) & '\x02') != '\x00' & (*(unsigned char *)(*(uint64_t *)var_48 + 40UL) & '\x02') != '\x00' & (uint64_t)(uint32_t)var_49 != 0UL) {
                                local_sp_6_be = local_sp_8;
                                if (*var_20 == 0UL) {
                                    *(uint64_t *)(local_sp_8 + (-8L)) = 4208877UL;
                                    var_61 = indirect_placeholder_4(24UL);
                                    *var_26 = var_61;
                                    *(uint64_t *)(var_61 + 8UL) = *var_10;
                                    **(uint64_t **)var_25 = *var_21;
                                    *(uint64_t *)(*var_26 + 16UL) = *var_6;
                                    *var_6 = *var_26;
                                    var_62 = *(uint64_t *)4343488UL;
                                    var_63 = *var_26;
                                    var_64 = local_sp_8 + (-16L);
                                    *(uint64_t *)var_64 = 4208949UL;
                                    var_65 = indirect_placeholder_58(var_63, var_62);
                                    local_sp_6_be = var_64;
                                    if (var_65.field_0 != 0UL) {
                                        var_67 = var_65.field_1;
                                        var_68 = var_65.field_2;
                                        *(uint64_t *)(local_sp_8 + (-24L)) = 4208959UL;
                                        indirect_placeholder_27(var_4, var_67, var_68);
                                        abort();
                                    }
                                    var_66 = *(uint64_t *)(*var_10 + 48UL);
                                    *var_10 = var_66;
                                    _pre = var_66;
                                } else {
                                    var_59 = *(uint64_t *)(*var_10 + 48UL);
                                    *var_10 = var_59;
                                    _pre = var_59;
                                    if (*var_5 == '\x01') {
                                        var_60 = local_sp_8 + (-8L);
                                        *(uint64_t *)var_60 = 4208865UL;
                                        indirect_placeholder();
                                        _pre = *var_10;
                                        local_sp_6_be = var_60;
                                    }
                                }
                                var_27 = _pre;
                                local_sp_6 = local_sp_6_be;
                                continue;
                            }
                        }
                        var_51 = **(uint64_t **)var_9;
                        var_52 = local_sp_5 + (-8L);
                        *(uint64_t *)var_52 = 4208667UL;
                        indirect_placeholder();
                        local_sp_1 = var_52;
                        if (var_51 == 0UL) {
                            var_53 = **(uint64_t **)(*var_22 + 8UL);
                            var_54 = local_sp_5 + (-16L);
                            *(uint64_t *)var_54 = 4208696UL;
                            indirect_placeholder();
                            local_sp_1 = var_54;
                            local_sp_2 = var_54;
                            if (var_53 == 0UL) {
                                *var_20 = *(uint64_t *)(*var_22 + 8UL);
                                *(uint64_t *)(*var_22 + 8UL) = *var_10;
                                local_sp_8 = local_sp_2;
                            } else {
                                local_sp_2 = local_sp_1;
                                if (*var_23 == '\x00') {
                                    if (*var_24 == '\x01') {
                                        *var_20 = *(uint64_t *)(*var_22 + 8UL);
                                        *(uint64_t *)(*var_22 + 8UL) = *var_10;
                                        local_sp_8 = local_sp_2;
                                    } else {
                                        var_55 = **(uint64_t **)(*var_22 + 8UL);
                                        var_56 = local_sp_1 + (-8L);
                                        *(uint64_t *)var_56 = 4208747UL;
                                        indirect_placeholder();
                                        local_sp_0 = var_56;
                                        if ((uint64_t)(uint32_t)var_55 == 0UL) {
                                            *var_20 = *var_10;
                                            local_sp_8 = local_sp_0;
                                        } else {
                                            var_57 = *(uint64_t *)(*var_10 + 8UL);
                                            var_58 = local_sp_1 + (-16L);
                                            *(uint64_t *)var_58 = 4208782UL;
                                            indirect_placeholder();
                                            local_sp_0 = var_58;
                                            local_sp_2 = var_58;
                                            if ((uint64_t)(uint32_t)var_57 == 0UL) {
                                                *var_20 = *(uint64_t *)(*var_22 + 8UL);
                                                *(uint64_t *)(*var_22 + 8UL) = *var_10;
                                                local_sp_8 = local_sp_2;
                                            } else {
                                                *var_20 = *var_10;
                                                local_sp_8 = local_sp_0;
                                            }
                                        }
                                    }
                                } else {
                                    var_55 = **(uint64_t **)(*var_22 + 8UL);
                                    var_56 = local_sp_1 + (-8L);
                                    *(uint64_t *)var_56 = 4208747UL;
                                    indirect_placeholder();
                                    local_sp_0 = var_56;
                                    if ((uint64_t)(uint32_t)var_55 == 0UL) {
                                        *var_20 = *var_10;
                                        local_sp_8 = local_sp_0;
                                    } else {
                                        var_57 = *(uint64_t *)(*var_10 + 8UL);
                                        var_58 = local_sp_1 + (-16L);
                                        *(uint64_t *)var_58 = 4208782UL;
                                        indirect_placeholder();
                                        local_sp_0 = var_58;
                                        local_sp_2 = var_58;
                                        if ((uint64_t)(uint32_t)var_57 == 0UL) {
                                            *var_20 = *(uint64_t *)(*var_22 + 8UL);
                                            *(uint64_t *)(*var_22 + 8UL) = *var_10;
                                            local_sp_8 = local_sp_2;
                                        } else {
                                            *var_20 = *var_10;
                                            local_sp_8 = local_sp_0;
                                        }
                                    }
                                }
                            }
                        } else {
                            local_sp_2 = local_sp_1;
                            if (*var_23 == '\x00') {
                                if (*var_24 == '\x01') {
                                    *var_20 = *(uint64_t *)(*var_22 + 8UL);
                                    *(uint64_t *)(*var_22 + 8UL) = *var_10;
                                    local_sp_8 = local_sp_2;
                                } else {
                                    var_55 = **(uint64_t **)(*var_22 + 8UL);
                                    var_56 = local_sp_1 + (-8L);
                                    *(uint64_t *)var_56 = 4208747UL;
                                    indirect_placeholder();
                                    local_sp_0 = var_56;
                                    if ((uint64_t)(uint32_t)var_55 == 0UL) {
                                        *var_20 = *var_10;
                                        local_sp_8 = local_sp_0;
                                    } else {
                                        var_57 = *(uint64_t *)(*var_10 + 8UL);
                                        var_58 = local_sp_1 + (-16L);
                                        *(uint64_t *)var_58 = 4208782UL;
                                        indirect_placeholder();
                                        local_sp_0 = var_58;
                                        local_sp_2 = var_58;
                                        if ((uint64_t)(uint32_t)var_57 == 0UL) {
                                            *var_20 = *(uint64_t *)(*var_22 + 8UL);
                                            *(uint64_t *)(*var_22 + 8UL) = *var_10;
                                            local_sp_8 = local_sp_2;
                                        } else {
                                            *var_20 = *var_10;
                                            local_sp_8 = local_sp_0;
                                        }
                                    }
                                }
                            } else {
                                var_55 = **(uint64_t **)(*var_22 + 8UL);
                                var_56 = local_sp_1 + (-8L);
                                *(uint64_t *)var_56 = 4208747UL;
                                indirect_placeholder();
                                local_sp_0 = var_56;
                                if ((uint64_t)(uint32_t)var_55 == 0UL) {
                                    *var_20 = *var_10;
                                    local_sp_8 = local_sp_0;
                                } else {
                                    var_57 = *(uint64_t *)(*var_10 + 8UL);
                                    var_58 = local_sp_1 + (-16L);
                                    *(uint64_t *)var_58 = 4208782UL;
                                    indirect_placeholder();
                                    local_sp_0 = var_58;
                                    local_sp_2 = var_58;
                                    if ((uint64_t)(uint32_t)var_57 == 0UL) {
                                        *var_20 = *(uint64_t *)(*var_22 + 8UL);
                                        *(uint64_t *)(*var_22 + 8UL) = *var_10;
                                        local_sp_8 = local_sp_2;
                                    } else {
                                        *var_20 = *var_10;
                                        local_sp_8 = local_sp_0;
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                *var_21 = *(uint64_t *)(*var_10 + 32UL);
                local_sp_8 = local_sp_7;
            }
        }
    switch (loop_state_var) {
      case 0U:
        {
            return;
        }
        break;
      case 1U:
        {
            while (var_69 != 0UL)
                {
                    var_70 = *(uint64_t *)(var_69 + 8UL);
                    *var_10 = var_70;
                    *(uint64_t *)(var_70 + 48UL) = *(uint64_t *)4343536UL;
                    *(uint64_t *)4343536UL = *var_10;
                    var_71 = *(uint64_t *)(*var_6 + 16UL);
                    *var_6 = var_71;
                    var_69 = var_71;
                }
            *(uint64_t *)(local_sp_6 + (-8L)) = 4209081UL;
            indirect_placeholder();
            *(uint64_t *)4343488UL = 0UL;
        }
        break;
    }
}
