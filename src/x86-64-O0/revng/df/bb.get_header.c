typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_get_header_ret_type;
struct indirect_placeholder_79_ret_type;
struct indirect_placeholder_78_ret_type;
struct indirect_placeholder_77_ret_type;
struct bb_get_header_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_79_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_78_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_77_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rcx(void);
extern void indirect_placeholder_2(uint64_t param_0);
extern void indirect_placeholder_27(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_79_ret_type indirect_placeholder_79(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_78_ret_type indirect_placeholder_78(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_77_ret_type indirect_placeholder_77(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
struct bb_get_header_ret_type bb_get_header(uint64_t r9, uint64_t r8) {
    struct indirect_placeholder_78_ret_type var_48;
    struct indirect_placeholder_79_ret_type var_43;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint32_t *var_13;
    uint64_t *var_14;
    uint64_t *var_15;
    unsigned char *var_16;
    unsigned char *var_17;
    uint64_t var_18;
    uint64_t *var_19;
    uint64_t var_20;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t *_cast6;
    uint64_t r84_1;
    uint64_t *_cast5_pre_phi;
    uint64_t _pre_phi;
    uint64_t local_sp_0;
    uint64_t storemerge;
    uint64_t var_75;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t _pre114;
    uint64_t _pre116;
    uint32_t var_40;
    uint64_t local_sp_1;
    uint64_t r93_0;
    uint64_t var_28;
    uint64_t r84_0;
    uint64_t var_62;
    bool var_63;
    uint64_t *var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t local_sp_2;
    uint64_t rcx_0;
    uint64_t r93_1;
    uint64_t var_21;
    uint64_t var_22;
    struct bb_get_header_ret_type mrv;
    struct bb_get_header_ret_type mrv1;
    struct bb_get_header_ret_type mrv2;
    uint64_t var_23;
    bool var_24;
    uint32_t var_52;
    uint32_t _pre113;
    uint32_t var_25;
    uint32_t var_27;
    uint32_t _pre;
    uint32_t var_26;
    unsigned char var_29;
    unsigned char var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint32_t var_38;
    uint32_t var_37;
    uint32_t var_39;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    struct indirect_placeholder_77_ret_type var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_53;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_cc_src2();
    var_2 = init_rbp();
    var_3 = init_rcx();
    var_4 = init_rbx();
    var_5 = var_0 + (-8L);
    *(uint64_t *)var_5 = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    var_6 = var_0 + (-336L);
    *(uint64_t *)var_6 = 4206878UL;
    indirect_placeholder_1(r9, r8);
    var_7 = (uint64_t *)(var_0 + (-32L));
    *var_7 = 0UL;
    var_8 = var_0 + (-96L);
    var_9 = (uint64_t *)var_8;
    var_10 = (uint64_t *)(var_0 + (-64L));
    var_11 = var_0 + (-120L);
    var_12 = (uint64_t *)(var_0 + (-88L));
    var_13 = (uint32_t *)(var_0 + (-36L));
    var_14 = (uint64_t *)(var_0 + (-48L));
    var_15 = (uint64_t *)(var_0 + (-56L));
    var_16 = (unsigned char *)(var_0 + (-65L));
    var_17 = (unsigned char *)(var_0 + (-66L));
    var_18 = var_0 + (-328L);
    var_19 = (uint64_t *)(var_0 + (-80L));
    var_20 = 0UL;
    r84_1 = r8;
    local_sp_2 = var_6;
    rcx_0 = var_3;
    r93_1 = r9;
    while (1U)
        {
            var_21 = *(uint64_t *)4343624UL;
            var_22 = helper_cc_compute_c_wrapper(var_20 - var_21, var_21, var_1, 17U);
            r93_0 = r93_1;
            r84_0 = r84_1;
            if (var_22 == 0UL) {
                mrv.field_0 = rcx_0;
                mrv1 = mrv;
                mrv1.field_1 = r93_1;
                mrv2 = mrv1;
                mrv2.field_2 = r84_1;
                return mrv2;
            }
            *var_9 = 0UL;
            var_23 = *(uint64_t *)(*(uint64_t *)(*(uint64_t *)4343616UL + (*var_7 << 3UL)) + 24UL);
            *var_10 = var_23;
            var_24 = (**(uint32_t **)(*(uint64_t *)4343616UL + (*var_7 << 3UL)) == 2U);
            if (!var_24) {
                _pre113 = *(uint32_t *)4343608UL;
                var_52 = _pre113;
                if (var_52 == 3U) {
                    var_53 = local_sp_2 + (-8L);
                    *(uint64_t *)var_53 = 4207438UL;
                    indirect_placeholder();
                    *var_9 = var_23;
                    local_sp_1 = var_53;
                } else {
                    if (var_24) {
                        var_53 = local_sp_2 + (-8L);
                        *(uint64_t *)var_53 = 4207438UL;
                        indirect_placeholder();
                        *var_9 = var_23;
                        local_sp_1 = var_53;
                    } else {
                        var_54 = *(uint64_t *)4343504UL;
                        *(uint64_t *)(local_sp_2 + (-8L)) = 4207377UL;
                        var_55 = indirect_placeholder_1(var_11, var_54);
                        *var_12 = var_55;
                        var_56 = *var_10;
                        var_57 = local_sp_2 + (-16L);
                        *(uint64_t *)var_57 = 4207411UL;
                        var_58 = indirect_placeholder_77(0UL, var_56, var_55, 4315160UL, var_8, r93_1, r84_1);
                        var_59 = var_58.field_0;
                        var_60 = var_58.field_1;
                        var_61 = var_58.field_2;
                        local_sp_1 = var_57;
                        r93_0 = var_60;
                        r84_0 = var_61;
                        if ((uint64_t)((uint32_t)var_59 + 1U) == 0UL) {
                            *var_9 = 0UL;
                        }
                    }
                }
                var_62 = *var_9;
                var_63 = (var_62 == 0UL);
                var_64 = (uint64_t *)(local_sp_1 + (-8L));
                r93_1 = r93_0;
                r84_1 = r84_0;
                if (var_63) {
                    *var_64 = 4207462UL;
                    indirect_placeholder_27(var_5, r93_0, r84_0);
                    abort();
                }
                *var_64 = 4207474UL;
                indirect_placeholder_2(var_62);
                *(uint64_t *)((*var_7 << 3UL) + *(uint64_t *)(*(uint64_t *)4343632UL + ((*(uint64_t *)4343640UL << 3UL) + (-8L)))) = *var_9;
                var_65 = *(uint64_t *)(*(uint64_t *)(*(uint64_t *)4343616UL + (*var_7 << 3UL)) + 32UL);
                var_66 = *var_9;
                var_67 = local_sp_1 + (-16L);
                *(uint64_t *)var_67 = 4207562UL;
                var_68 = indirect_placeholder_1(0UL, var_66);
                local_sp_0 = var_67;
                if (var_65 > (uint64_t)((long)(var_68 << 32UL) >> (long)32UL)) {
                    var_73 = *(uint64_t *)4343616UL;
                    var_74 = *var_7 << 3UL;
                    _cast6 = (uint64_t *)(var_73 + var_74);
                    _cast5_pre_phi = _cast6;
                    _pre_phi = var_74;
                    storemerge = *(uint64_t *)(*_cast6 + 32UL);
                } else {
                    var_69 = *var_9;
                    var_70 = local_sp_1 + (-24L);
                    *(uint64_t *)var_70 = 4207613UL;
                    var_71 = indirect_placeholder_1(0UL, var_69);
                    var_72 = (uint64_t)((long)(var_71 << 32UL) >> (long)32UL);
                    _pre114 = *(uint64_t *)4343616UL;
                    _pre116 = *var_7 << 3UL;
                    _cast5_pre_phi = (uint64_t *)(_pre114 + _pre116);
                    _pre_phi = _pre116;
                    local_sp_0 = var_70;
                    storemerge = var_72;
                }
                *(uint64_t *)(*_cast5_pre_phi + 32UL) = storemerge;
                var_75 = *var_7 + 1UL;
                *var_7 = var_75;
                var_20 = var_75;
                local_sp_2 = local_sp_0;
                rcx_0 = _pre_phi;
                continue;
            }
            var_25 = *(uint32_t *)4343608UL;
            var_52 = var_25;
            switch (var_25) {
              case 0U:
                {
                    _pre = *(uint32_t *)4343500UL;
                    var_27 = _pre;
                    *var_13 = ((uint32_t)((uint16_t)var_27 & (unsigned short)292U) | 152U);
                    *var_14 = *(uint64_t *)4343504UL;
                    *var_15 = *(uint64_t *)4343504UL;
                    var_28 = *var_14;
                    *var_16 = (var_28 == ((uint64_t)(((unsigned __int128)(var_28 >> 3UL) * 2361183241434822607ULL) >> 68ULL) * 1000UL));
                    *var_14 = (uint64_t)(((unsigned __int128)(*var_14 >> 3UL) * 2361183241434822607ULL) >> 68ULL);
                    *var_17 = ((uint64_t)((uint16_t)*var_15 & (unsigned short)1023U) == 0UL);
                    *var_15 = (*var_15 >> 10UL);
                    var_29 = *var_16;
                    var_30 = *var_17;
                    do {
                        var_28 = *var_14;
                        *var_16 = (var_28 == ((uint64_t)(((unsigned __int128)(var_28 >> 3UL) * 2361183241434822607ULL) >> 68ULL) * 1000UL));
                        *var_14 = (uint64_t)(((unsigned __int128)(*var_14 >> 3UL) * 2361183241434822607ULL) >> 68ULL);
                        *var_17 = ((uint64_t)((uint16_t)*var_15 & (unsigned short)1023U) == 0UL);
                        *var_15 = (*var_15 >> 10UL);
                        var_29 = *var_16;
                        var_30 = *var_17;
                    } while ((var_30 & var_29) != '\x00');
                    var_31 = (uint64_t)var_29;
                    var_32 = (uint64_t)var_30;
                    var_33 = helper_cc_compute_c_wrapper(var_31 - var_32, var_32, var_1, 14U);
                    if (var_33 == 0UL) {
                        *var_13 = (*var_13 | 32U);
                    }
                    var_34 = (uint64_t)*var_17;
                    var_35 = (uint64_t)*var_16;
                    var_36 = helper_cc_compute_c_wrapper(var_34 - var_35, var_35, var_1, 14U);
                    if (var_36 == 0UL) {
                        var_38 = *var_13;
                    } else {
                        var_37 = *var_13 & (-33);
                        *var_13 = var_37;
                        var_38 = var_37;
                    }
                    var_40 = var_38;
                    if ((var_38 & 32U) == 0U) {
                        var_39 = var_38 | 256U;
                        *var_13 = var_39;
                        var_40 = var_39;
                    }
                    var_41 = *(uint64_t *)4343504UL;
                    var_42 = (uint64_t)var_40;
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4207252UL;
                    var_43 = indirect_placeholder_79(1UL, var_42, var_18, var_41, 1UL);
                    var_44 = var_43.field_1;
                    var_45 = var_43.field_2;
                    *var_19 = var_43.field_0;
                    *var_10 = 4313950UL;
                    var_46 = *var_19;
                    var_47 = local_sp_2 + (-16L);
                    *(uint64_t *)var_47 = 4207294UL;
                    var_48 = indirect_placeholder_78(0UL, 4313950UL, var_46, 4315160UL, var_8, var_44, var_45);
                    var_49 = var_48.field_0;
                    var_50 = var_48.field_1;
                    var_51 = var_48.field_2;
                    local_sp_1 = var_47;
                    r93_0 = var_50;
                    r84_0 = var_51;
                    if ((uint64_t)((uint32_t)var_49 + 1U) == 0UL) {
                        *var_9 = 0UL;
                    }
                }
                break;
              case 4U:
                {
                    var_26 = *(uint32_t *)4343500UL;
                    var_27 = var_26;
                    var_52 = 4U;
                    if ((var_26 & 16U) == 0U) {
                        if (var_52 == 3U) {
                            var_53 = local_sp_2 + (-8L);
                            *(uint64_t *)var_53 = 4207438UL;
                            indirect_placeholder();
                            *var_9 = var_23;
                            local_sp_1 = var_53;
                        } else {
                            if (var_24) {
                                var_53 = local_sp_2 + (-8L);
                                *(uint64_t *)var_53 = 4207438UL;
                                indirect_placeholder();
                                *var_9 = var_23;
                                local_sp_1 = var_53;
                            } else {
                                var_54 = *(uint64_t *)4343504UL;
                                *(uint64_t *)(local_sp_2 + (-8L)) = 4207377UL;
                                var_55 = indirect_placeholder_1(var_11, var_54);
                                *var_12 = var_55;
                                var_56 = *var_10;
                                var_57 = local_sp_2 + (-16L);
                                *(uint64_t *)var_57 = 4207411UL;
                                var_58 = indirect_placeholder_77(0UL, var_56, var_55, 4315160UL, var_8, r93_1, r84_1);
                                var_59 = var_58.field_0;
                                var_60 = var_58.field_1;
                                var_61 = var_58.field_2;
                                local_sp_1 = var_57;
                                r93_0 = var_60;
                                r84_0 = var_61;
                                if ((uint64_t)((uint32_t)var_59 + 1U) == 0UL) {
                                    *var_9 = 0UL;
                                }
                            }
                        }
                        var_62 = *var_9;
                        var_63 = (var_62 == 0UL);
                        var_64 = (uint64_t *)(local_sp_1 + (-8L));
                        r93_1 = r93_0;
                        r84_1 = r84_0;
                        if (var_63) {
                            *var_64 = 4207462UL;
                            indirect_placeholder_27(var_5, r93_0, r84_0);
                            abort();
                        }
                        *var_64 = 4207474UL;
                        indirect_placeholder_2(var_62);
                        *(uint64_t *)((*var_7 << 3UL) + *(uint64_t *)(*(uint64_t *)4343632UL + ((*(uint64_t *)4343640UL << 3UL) + (-8L)))) = *var_9;
                        var_65 = *(uint64_t *)(*(uint64_t *)(*(uint64_t *)4343616UL + (*var_7 << 3UL)) + 32UL);
                        var_66 = *var_9;
                        var_67 = local_sp_1 + (-16L);
                        *(uint64_t *)var_67 = 4207562UL;
                        var_68 = indirect_placeholder_1(0UL, var_66);
                        local_sp_0 = var_67;
                        if (var_65 > (uint64_t)((long)(var_68 << 32UL) >> (long)32UL)) {
                            var_73 = *(uint64_t *)4343616UL;
                            var_74 = *var_7 << 3UL;
                            _cast6 = (uint64_t *)(var_73 + var_74);
                            _cast5_pre_phi = _cast6;
                            _pre_phi = var_74;
                            storemerge = *(uint64_t *)(*_cast6 + 32UL);
                        } else {
                            var_69 = *var_9;
                            var_70 = local_sp_1 + (-24L);
                            *(uint64_t *)var_70 = 4207613UL;
                            var_71 = indirect_placeholder_1(0UL, var_69);
                            var_72 = (uint64_t)((long)(var_71 << 32UL) >> (long)32UL);
                            _pre114 = *(uint64_t *)4343616UL;
                            _pre116 = *var_7 << 3UL;
                            _cast5_pre_phi = (uint64_t *)(_pre114 + _pre116);
                            _pre_phi = _pre116;
                            local_sp_0 = var_70;
                            storemerge = var_72;
                        }
                        *(uint64_t *)(*_cast5_pre_phi + 32UL) = storemerge;
                        var_75 = *var_7 + 1UL;
                        *var_7 = var_75;
                        var_20 = var_75;
                        local_sp_2 = local_sp_0;
                        rcx_0 = _pre_phi;
                        continue;
                    }
                    *var_13 = ((uint32_t)((uint16_t)var_27 & (unsigned short)292U) | 152U);
                    *var_14 = *(uint64_t *)4343504UL;
                    *var_15 = *(uint64_t *)4343504UL;
                    var_28 = *var_14;
                    *var_16 = (var_28 == ((uint64_t)(((unsigned __int128)(var_28 >> 3UL) * 2361183241434822607ULL) >> 68ULL) * 1000UL));
                    *var_14 = (uint64_t)(((unsigned __int128)(*var_14 >> 3UL) * 2361183241434822607ULL) >> 68ULL);
                    *var_17 = ((uint64_t)((uint16_t)*var_15 & (unsigned short)1023U) == 0UL);
                    *var_15 = (*var_15 >> 10UL);
                    var_29 = *var_16;
                    var_30 = *var_17;
                    do {
                        var_28 = *var_14;
                        *var_16 = (var_28 == ((uint64_t)(((unsigned __int128)(var_28 >> 3UL) * 2361183241434822607ULL) >> 68ULL) * 1000UL));
                        *var_14 = (uint64_t)(((unsigned __int128)(*var_14 >> 3UL) * 2361183241434822607ULL) >> 68ULL);
                        *var_17 = ((uint64_t)((uint16_t)*var_15 & (unsigned short)1023U) == 0UL);
                        *var_15 = (*var_15 >> 10UL);
                        var_29 = *var_16;
                        var_30 = *var_17;
                    } while ((var_30 & var_29) != '\x00');
                    var_31 = (uint64_t)var_29;
                    var_32 = (uint64_t)var_30;
                    var_33 = helper_cc_compute_c_wrapper(var_31 - var_32, var_32, var_1, 14U);
                    if (var_33 == 0UL) {
                        *var_13 = (*var_13 | 32U);
                    }
                    var_34 = (uint64_t)*var_17;
                    var_35 = (uint64_t)*var_16;
                    var_36 = helper_cc_compute_c_wrapper(var_34 - var_35, var_35, var_1, 14U);
                    if (var_36 == 0UL) {
                        var_37 = *var_13 & (-33);
                        *var_13 = var_37;
                        var_38 = var_37;
                    } else {
                        var_38 = *var_13;
                    }
                    var_40 = var_38;
                    if ((var_38 & 32U) == 0U) {
                        var_39 = var_38 | 256U;
                        *var_13 = var_39;
                        var_40 = var_39;
                    }
                    var_41 = *(uint64_t *)4343504UL;
                    var_42 = (uint64_t)var_40;
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4207252UL;
                    var_43 = indirect_placeholder_79(1UL, var_42, var_18, var_41, 1UL);
                    var_44 = var_43.field_1;
                    var_45 = var_43.field_2;
                    *var_19 = var_43.field_0;
                    *var_10 = 4313950UL;
                    var_46 = *var_19;
                    var_47 = local_sp_2 + (-16L);
                    *(uint64_t *)var_47 = 4207294UL;
                    var_48 = indirect_placeholder_78(0UL, 4313950UL, var_46, 4315160UL, var_8, var_44, var_45);
                    var_49 = var_48.field_0;
                    var_50 = var_48.field_1;
                    var_51 = var_48.field_2;
                    local_sp_1 = var_47;
                    r93_0 = var_50;
                    r84_0 = var_51;
                    if ((uint64_t)((uint32_t)var_49 + 1U) == 0UL) {
                        *var_9 = 0UL;
                    }
                }
                break;
              default:
                {
                    if (var_52 == 3U) {
                        var_53 = local_sp_2 + (-8L);
                        *(uint64_t *)var_53 = 4207438UL;
                        indirect_placeholder();
                        *var_9 = var_23;
                        local_sp_1 = var_53;
                    } else {
                        if (var_24) {
                            var_53 = local_sp_2 + (-8L);
                            *(uint64_t *)var_53 = 4207438UL;
                            indirect_placeholder();
                            *var_9 = var_23;
                            local_sp_1 = var_53;
                        } else {
                            var_54 = *(uint64_t *)4343504UL;
                            *(uint64_t *)(local_sp_2 + (-8L)) = 4207377UL;
                            var_55 = indirect_placeholder_1(var_11, var_54);
                            *var_12 = var_55;
                            var_56 = *var_10;
                            var_57 = local_sp_2 + (-16L);
                            *(uint64_t *)var_57 = 4207411UL;
                            var_58 = indirect_placeholder_77(0UL, var_56, var_55, 4315160UL, var_8, r93_1, r84_1);
                            var_59 = var_58.field_0;
                            var_60 = var_58.field_1;
                            var_61 = var_58.field_2;
                            local_sp_1 = var_57;
                            r93_0 = var_60;
                            r84_0 = var_61;
                            if ((uint64_t)((uint32_t)var_59 + 1U) == 0UL) {
                                *var_9 = 0UL;
                            }
                        }
                    }
                }
                break;
            }
        }
}
