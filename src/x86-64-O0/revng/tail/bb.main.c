typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct helper_movq_mm_T0_xmm_wrapper_ret_type;
struct type_3;
struct indirect_placeholder_132_ret_type;
struct indirect_placeholder_126_ret_type;
struct indirect_placeholder_125_ret_type;
struct indirect_placeholder_127_ret_type;
struct indirect_placeholder_130_ret_type;
struct indirect_placeholder_129_ret_type;
struct indirect_placeholder_128_ret_type;
struct indirect_placeholder_131_ret_type;
struct helper_movq_mm_T0_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_3 {
};
struct indirect_placeholder_132_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_126_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_125_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_127_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_130_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_129_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_128_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_131_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_7(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct helper_movq_mm_T0_xmm_wrapper_ret_type helper_movq_mm_T0_xmm_wrapper(struct type_3 *param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_30(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder_5(uint64_t param_0);
extern void indirect_placeholder_3(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_14(uint64_t param_0);
extern struct indirect_placeholder_132_ret_type indirect_placeholder_132(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_126_ret_type indirect_placeholder_126(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_125_ret_type indirect_placeholder_125(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_127_ret_type indirect_placeholder_127(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_130_ret_type indirect_placeholder_130(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_129_ret_type indirect_placeholder_129(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_128_ret_type indirect_placeholder_128(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_131_ret_type indirect_placeholder_131(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
typedef _Bool bool;
uint64_t bb_main(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_126_ret_type var_108;
    struct indirect_placeholder_132_ret_type var_23;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint32_t *var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint64_t var_6;
    uint32_t *var_7;
    unsigned char *var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t *var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    unsigned char *var_18;
    unsigned char var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_126;
    uint64_t var_118;
    uint64_t local_sp_0;
    uint64_t var_119;
    uint64_t var_120;
    uint64_t local_sp_10;
    uint64_t var_121;
    uint64_t var_122;
    uint64_t _pre_phi238;
    uint64_t local_sp_3;
    uint64_t r9_0;
    uint64_t var_123;
    uint64_t _pre;
    uint64_t var_124;
    uint64_t local_sp_1;
    uint64_t var_125;
    uint64_t local_sp_2;
    uint64_t var_127;
    uint64_t var_106;
    uint64_t var_107;
    uint64_t r9_2;
    uint64_t r8_2;
    uint64_t local_sp_8;
    uint64_t r8_0;
    uint64_t var_109;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t var_112;
    uint64_t var_113;
    uint64_t var_114;
    uint64_t var_115;
    uint64_t rax_1;
    uint64_t var_116;
    uint64_t var_117;
    uint64_t local_sp_9;
    unsigned char storemerge3;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t var_100;
    uint64_t var_101;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t local_sp_5;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t r8_6;
    struct indirect_placeholder_127_ret_type var_79;
    uint64_t local_sp_6;
    uint64_t rcx_1;
    uint64_t local_sp_7;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t local_sp_15;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    unsigned char var_102;
    uint32_t *var_103;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t var_128;
    uint64_t var_129;
    uint64_t var_130;
    uint64_t local_sp_12;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t rcx_6;
    uint64_t var_63;
    uint64_t local_sp_11;
    uint64_t rcx_2;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_131;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t *_pre_phi232;
    uint64_t var_56;
    uint64_t r9_6;
    uint64_t rcx_4;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_62;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_42;
    uint64_t var_37;
    uint64_t var_26;
    uint32_t var_27;
    uint32_t var_28;
    uint64_t var_31;
    uint64_t *var_32;
    uint64_t var_33;
    uint64_t *var_34;
    uint64_t *_pre_phi234;
    uint64_t *var_29;
    uint64_t *var_30;
    unsigned char *var_35;
    uint64_t *var_36;
    uint64_t local_sp_13;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t local_sp_14;
    struct indirect_placeholder_130_ret_type var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    struct indirect_placeholder_129_ret_type var_48;
    uint64_t rcx_5;
    uint64_t r9_5;
    uint64_t r8_5;
    uint64_t local_sp_17;
    uint64_t var_49;
    uint64_t var_50;
    struct indirect_placeholder_128_ret_type var_51;
    uint64_t var_52;
    struct indirect_placeholder_131_ret_type var_53;
    uint64_t var_54;
    uint64_t *var_55;
    uint64_t var_40;
    uint64_t var_41;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_cc_src2();
    var_2 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    var_3 = (uint32_t *)(var_0 + (-236L));
    *var_3 = (uint32_t)rdi;
    var_4 = var_0 + (-248L);
    var_5 = (uint64_t *)var_4;
    *var_5 = rsi;
    var_6 = var_0 + (-64L);
    var_7 = (uint32_t *)var_6;
    *var_7 = 0U;
    var_8 = (unsigned char *)(var_0 + (-9L));
    *var_8 = (unsigned char)'\x01';
    var_9 = var_0 + (-72L);
    var_10 = (uint64_t *)var_9;
    *var_10 = 10UL;
    var_11 = *(uint64_t *)4315304UL;
    var_12 = var_0 + (-80L);
    var_13 = (uint64_t *)var_12;
    *var_13 = var_11;
    var_14 = **(uint64_t **)var_4;
    *(uint64_t *)(var_0 + (-256L)) = 4229364UL;
    indirect_placeholder_14(var_14);
    *(uint64_t *)(var_0 + (-264L)) = 4229379UL;
    indirect_placeholder();
    *(uint64_t *)(var_0 + (-272L)) = 4229389UL;
    indirect_placeholder();
    *(unsigned char *)4338748UL = (unsigned char)'\x00';
    *(unsigned char *)4338737UL = (unsigned char)'\x01';
    *(unsigned char *)4338741UL = (unsigned char)'\x00';
    *(unsigned char *)4338740UL = (unsigned char)'\x00';
    *(unsigned char *)4338738UL = (unsigned char)'\x00';
    *(unsigned char *)4338742UL = (unsigned char)'\n';
    var_15 = *var_5;
    var_16 = (uint64_t)*var_3;
    *(uint64_t *)(var_0 + (-280L)) = 4229470UL;
    var_17 = indirect_placeholder_30(var_9, var_16, var_15);
    var_18 = (unsigned char *)(var_0 + (-42L));
    var_19 = (unsigned char)var_17;
    *var_18 = var_19;
    *var_3 = (*var_3 - (uint32_t)var_19);
    var_20 = *var_5 + ((uint64_t)*var_18 << 3UL);
    *var_5 = var_20;
    var_21 = (uint64_t)*var_3;
    var_22 = var_0 + (-288L);
    *(uint64_t *)var_22 = 4229533UL;
    var_23 = indirect_placeholder_132(var_6, var_9, var_21, var_20, var_12);
    var_24 = var_23.field_0;
    var_25 = var_23.field_1;
    var_118 = 0UL;
    rax_1 = 1UL;
    storemerge3 = (unsigned char)'\x01';
    var_63 = 0UL;
    var_56 = 0UL;
    var_37 = 0UL;
    local_sp_13 = var_22;
    rcx_5 = var_6;
    r9_5 = var_24;
    r8_5 = var_25;
    var_26 = *var_10;
    if (*(unsigned char *)4338740UL != '\x00' & var_26 == 0UL) {
        *var_10 = (var_26 + (-1L));
    }
    var_27 = *(uint32_t *)4338584UL;
    var_28 = *var_3;
    if ((int)var_27 < (int)var_28) {
        var_31 = (uint64_t)((long)(((uint64_t)var_28 - (uint64_t)var_27) << 32UL) >> (long)32UL);
        var_32 = (uint64_t *)(var_0 + (-24L));
        *var_32 = var_31;
        var_33 = *var_5 + ((uint64_t)*(uint32_t *)4338584UL << 3UL);
        var_34 = (uint64_t *)(var_0 + (-32L));
        *var_34 = var_33;
        _pre_phi234 = var_34;
        _pre_phi232 = var_32;
    } else {
        var_29 = (uint64_t *)(var_0 + (-24L));
        *var_29 = 1UL;
        var_30 = (uint64_t *)(var_0 + (-32L));
        *var_30 = 4338424UL;
        _pre_phi234 = var_30;
        _pre_phi232 = var_29;
    }
    var_35 = (unsigned char *)(var_0 + (-41L));
    *var_35 = (unsigned char)'\x00';
    var_36 = (uint64_t *)(var_0 + (-40L));
    *var_36 = 0UL;
    var_38 = *_pre_phi232;
    var_39 = helper_cc_compute_c_wrapper(var_37 - var_38, var_38, var_1, 17U);
    local_sp_14 = local_sp_13;
    while (var_39 != 0UL)
        {
            var_40 = *(uint64_t *)(*_pre_phi234 + (*var_36 << 3UL));
            var_41 = local_sp_13 + (-8L);
            *(uint64_t *)var_41 = 4229698UL;
            indirect_placeholder();
            local_sp_13 = var_41;
            if ((uint64_t)(uint32_t)var_40 == 0UL) {
                *var_35 = (unsigned char)'\x01';
            }
            var_42 = *var_36 + 1UL;
            *var_36 = var_42;
            var_37 = var_42;
            var_38 = *_pre_phi232;
            var_39 = helper_cc_compute_c_wrapper(var_37 - var_38, var_38, var_1, 17U);
            local_sp_14 = local_sp_13;
        }
    if (*var_35 != '\x00' & *(uint32_t *)4338400UL == 1U) {
        *(uint64_t *)(local_sp_13 + (-8L)) = 4229753UL;
        var_43 = indirect_placeholder_130(4UL, 4312730UL);
        var_44 = var_43.field_0;
        var_45 = var_43.field_1;
        var_46 = var_43.field_2;
        var_47 = local_sp_13 + (-16L);
        *(uint64_t *)var_47 = 4229781UL;
        var_48 = indirect_placeholder_129(0UL, var_44, 4315120UL, 1UL, 0UL, var_45, var_46);
        local_sp_14 = var_47;
        rcx_5 = var_48.field_0;
        r9_5 = var_48.field_1;
        r8_5 = var_48.field_2;
    }
    r8_6 = r8_5;
    local_sp_15 = local_sp_14;
    rcx_6 = rcx_5;
    r9_6 = r9_5;
    local_sp_17 = local_sp_14;
    if (*(unsigned char *)4338738UL != '\x00' & *var_35 != '\x00') {
        local_sp_17 = (uint64_t)0L;
        rcx_6 = (uint64_t)0L;
        r9_6 = (uint64_t)0L;
        r8_6 = (uint64_t)0L;
        if (*(uint32_t *)4338744UL != 0U & *(uint32_t *)4338400UL != 2U & *_pre_phi232 == 1UL) {
            var_49 = local_sp_14 + (-8L);
            *(uint64_t *)var_49 = 4229854UL;
            indirect_placeholder();
            local_sp_15 = var_49;
        }
        *(unsigned char *)(var_0 + (-43L)) = (unsigned char)'\x00';
        if (0) {
        } else {
            *(uint64_t *)(local_sp_15 + (-8L)) = 4229916UL;
            indirect_placeholder();
            var_50 = local_sp_15 + (-16L);
            *(uint64_t *)var_50 = 4229945UL;
            var_51 = indirect_placeholder_128(0UL, rcx_5, 4315152UL, 0UL, 0UL, r9_5, r8_5);
            local_sp_17 = var_50;
            rcx_6 = var_51.field_0;
            r9_6 = var_51.field_1;
            r8_6 = var_51.field_2;
        }
    }
    r9_2 = r9_6;
    r8_2 = r8_6;
    rcx_4 = rcx_6;
    if (*var_10 != 0UL) {
        rax_1 = 0UL;
        if (*(unsigned char *)4338738UL != '\x01' & *(unsigned char *)4338740UL == '\x01') {
            return rax_1;
        }
    }
    var_52 = *_pre_phi232;
    *(uint64_t *)(local_sp_17 + (-8L)) = 4230009UL;
    var_53 = indirect_placeholder_131(var_52, 96UL, r9_6, r8_6);
    var_54 = var_53.field_0;
    var_55 = (uint64_t *)(var_0 + (-56L));
    *var_55 = var_54;
    *var_36 = 0UL;
    var_57 = *_pre_phi232;
    var_58 = helper_cc_compute_c_wrapper(var_56 - var_57, var_57, var_1, 17U);
    rcx_2 = rcx_4;
    while (var_58 != 0UL)
        {
            var_59 = *var_36;
            var_60 = *_pre_phi234 + (var_59 << 3UL);
            *(uint64_t *)((var_59 * 96UL) + *var_55) = *(uint64_t *)var_60;
            var_61 = *var_36 + 1UL;
            *var_36 = var_61;
            var_56 = var_61;
            rcx_4 = var_60;
            var_57 = *_pre_phi232;
            var_58 = helper_cc_compute_c_wrapper(var_56 - var_57, var_57, var_1, 17U);
            rcx_2 = rcx_4;
        }
    switch (*var_7) {
      case 1U:
        {
            *(unsigned char *)4338741UL = (unsigned char)'\x01';
        }
        break;
      case 0U:
        {
            if (*_pre_phi232 > 1UL) {
                *(unsigned char *)4338741UL = (unsigned char)'\x01';
            }
        }
        break;
      default:
        {
            var_62 = local_sp_17 + (-16L);
            *(uint64_t *)var_62 = 4230135UL;
            indirect_placeholder_3(1UL, 0UL);
            *var_36 = 0UL;
            local_sp_11 = var_62;
            var_64 = *_pre_phi232;
            var_65 = helper_cc_compute_c_wrapper(var_63 - var_64, var_64, var_1, 17U);
            rcx_1 = rcx_2;
            local_sp_12 = local_sp_11;
            while (var_65 != 0UL)
                {
                    var_66 = *var_10;
                    var_67 = *var_55 + (*var_36 * 96UL);
                    var_68 = local_sp_11 + (-8L);
                    *(uint64_t *)var_68 = 4230187UL;
                    var_69 = indirect_placeholder_7(var_67, var_66);
                    *var_8 = ((var_69 & (uint64_t)*var_8) != 0UL);
                    var_70 = *var_36 + 1UL;
                    *var_36 = var_70;
                    var_63 = var_70;
                    local_sp_11 = var_68;
                    rcx_2 = var_66;
                    var_64 = *_pre_phi232;
                    var_65 = helper_cc_compute_c_wrapper(var_63 - var_64, var_64, var_1, 17U);
                    rcx_1 = rcx_2;
                    local_sp_12 = local_sp_11;
                }
            var_71 = *_pre_phi232;
            var_72 = *var_55;
            var_73 = local_sp_11 + (-8L);
            *(uint64_t *)var_73 = 4230253UL;
            var_74 = indirect_placeholder_7(var_72, var_71);
            local_sp_12 = var_73;
            if (*(unsigned char *)4338738UL != '\x00' & var_74 != 0UL) {
                var_75 = var_0 + (-232L);
                var_76 = local_sp_11 + (-16L);
                *(uint64_t *)var_76 = 4230282UL;
                indirect_placeholder();
                local_sp_6 = var_76;
                if ((int)(uint32_t)var_75 > (int)4294967295U) {
                    *(uint64_t *)(local_sp_11 + (-24L)) = 4230291UL;
                    indirect_placeholder();
                    var_77 = (uint64_t)*(uint32_t *)var_75;
                    var_78 = local_sp_11 + (-32L);
                    *(uint64_t *)var_78 = 4230315UL;
                    var_79 = indirect_placeholder_127(0UL, rcx_2, 4312782UL, 1UL, var_77, r9_6, r8_6);
                    local_sp_6 = var_78;
                    rcx_1 = var_79.field_0;
                    r9_2 = var_79.field_1;
                    r8_2 = var_79.field_2;
                }
                r9_0 = r9_2;
                r8_0 = r8_2;
                local_sp_7 = local_sp_6;
                if ((uint32_t)((uint16_t)*(uint32_t *)(var_0 + (-208L)) & (unsigned short)61440U) == 4096U) {
                    local_sp_8 = local_sp_7;
                } else {
                    var_80 = local_sp_6 + (-8L);
                    *(uint64_t *)var_80 = 4230343UL;
                    var_81 = indirect_placeholder_5(1UL);
                    local_sp_7 = var_80;
                    local_sp_8 = var_80;
                    storemerge3 = (unsigned char)'\x00';
                    if ((uint64_t)(uint32_t)var_81 == 0UL) {
                        local_sp_8 = local_sp_7;
                    }
                }
                *(unsigned char *)4338739UL = storemerge3;
                local_sp_9 = local_sp_8;
                if (*(unsigned char *)4338750UL != '\x01') {
                    var_82 = *_pre_phi232;
                    var_83 = *var_55;
                    var_84 = local_sp_8 + (-8L);
                    *(uint64_t *)var_84 = 4230405UL;
                    var_85 = indirect_placeholder_7(var_83, var_82);
                    local_sp_5 = var_84;
                    if ((uint64_t)(unsigned char)var_85 == 0UL) {
                        *(unsigned char *)4338750UL = (unsigned char)'\x01';
                        local_sp_9 = local_sp_5;
                    } else {
                        var_86 = *_pre_phi232;
                        var_87 = *var_55;
                        var_88 = local_sp_8 + (-16L);
                        *(uint64_t *)var_88 = 4230428UL;
                        var_89 = indirect_placeholder_7(var_87, var_86);
                        local_sp_5 = var_88;
                        if ((uint64_t)(unsigned char)var_89 == 0UL) {
                            *(unsigned char *)4338750UL = (unsigned char)'\x01';
                            local_sp_9 = local_sp_5;
                        } else {
                            var_90 = *_pre_phi232;
                            var_91 = *var_55;
                            var_92 = local_sp_8 + (-24L);
                            *(uint64_t *)var_92 = 4230451UL;
                            var_93 = indirect_placeholder_7(var_91, var_90);
                            local_sp_5 = var_92;
                            if ((uint64_t)(unsigned char)var_93 == 1UL) {
                                *(unsigned char *)4338750UL = (unsigned char)'\x01';
                                local_sp_9 = local_sp_5;
                            } else {
                                var_94 = *_pre_phi232;
                                var_95 = *var_55;
                                var_96 = local_sp_8 + (-32L);
                                *(uint64_t *)var_96 = 4230477UL;
                                var_97 = indirect_placeholder_7(var_95, var_94);
                                local_sp_5 = var_96;
                                if ((uint64_t)(unsigned char)var_97 == 0UL) {
                                    *(unsigned char *)4338750UL = (unsigned char)'\x01';
                                    local_sp_9 = local_sp_5;
                                } else {
                                    var_98 = *_pre_phi232;
                                    var_99 = *var_55;
                                    var_100 = local_sp_8 + (-40L);
                                    *(uint64_t *)var_100 = 4230500UL;
                                    var_101 = indirect_placeholder_7(var_99, var_98);
                                    local_sp_5 = var_100;
                                    local_sp_9 = var_100;
                                    if ((uint64_t)(unsigned char)var_101 == 0UL) {
                                        *(unsigned char *)4338750UL = (unsigned char)'\x01';
                                        local_sp_9 = local_sp_5;
                                    } else {
                                        if (*var_8 != '\x01' & *(uint32_t *)4338400UL == 2U) {
                                            *(unsigned char *)4338750UL = (unsigned char)'\x01';
                                            local_sp_9 = local_sp_5;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                var_102 = *(unsigned char *)4338750UL ^ '\x01';
                local_sp_10 = local_sp_9;
                if (var_102 != '\x00') {
                    *(uint64_t *)(local_sp_9 + (-8L)) = 4230556UL;
                    indirect_placeholder();
                    var_103 = (uint32_t *)(var_0 + (-60L));
                    *var_103 = (uint32_t)var_102;
                    var_104 = *(uint64_t *)4337216UL;
                    var_105 = local_sp_9 + (-16L);
                    *(uint64_t *)var_105 = 4230580UL;
                    indirect_placeholder();
                    local_sp_3 = var_105;
                    if ((uint64_t)(uint32_t)var_104 == 0UL) {
                        *(uint64_t *)(local_sp_9 + (-24L)) = 4230589UL;
                        indirect_placeholder();
                        var_106 = (uint64_t)*(uint32_t *)var_104;
                        var_107 = local_sp_9 + (-32L);
                        *(uint64_t *)var_107 = 4230613UL;
                        var_108 = indirect_placeholder_126(0UL, rcx_1, 4313513UL, 1UL, var_106, r9_2, r8_2);
                        local_sp_3 = var_107;
                        r9_0 = var_108.field_1;
                        r8_0 = var_108.field_2;
                    }
                    var_109 = *var_13;
                    var_110 = *_pre_phi232;
                    var_111 = *var_55;
                    var_112 = (uint64_t)*var_103;
                    helper_movq_mm_T0_xmm_wrapper((struct type_3 *)(776UL), var_109);
                    var_113 = local_sp_3 + (-8L);
                    *(uint64_t *)var_113 = 4230643UL;
                    var_114 = indirect_placeholder_30(var_110, var_112, var_111);
                    var_115 = (uint64_t)(uint32_t)var_114 ^ 1UL;
                    if ((uint64_t)(unsigned char)var_115 == 0UL) {
                        return rax_1;
                    }
                    *(uint64_t *)(var_113 + (-8L)) = 4230665UL;
                    indirect_placeholder();
                    var_116 = (uint64_t)*(uint32_t *)var_115;
                    var_117 = var_113 + (-16L);
                    *(uint64_t *)var_117 = 4230689UL;
                    indirect_placeholder_125(0UL, var_111, 4315216UL, 0UL, var_116, r9_0, r8_0);
                    *var_36 = 0UL;
                    local_sp_0 = var_117;
                    var_119 = *_pre_phi232;
                    var_120 = helper_cc_compute_c_wrapper(var_118 - var_119, var_119, var_1, 17U);
                    local_sp_1 = local_sp_0;
                    local_sp_10 = local_sp_0;
                    while (var_120 != 0UL)
                        {
                            var_121 = *var_36;
                            var_122 = *var_55 + (var_121 * 96UL);
                            _pre_phi238 = var_122;
                            var_124 = var_121;
                            if (*(uint32_t *)(var_122 + 68UL) == 4294967295U) {
                                var_123 = local_sp_0 + (-8L);
                                *(uint64_t *)var_123 = 4230779UL;
                                indirect_placeholder();
                                _pre = *var_36;
                                _pre_phi238 = *var_55 + (_pre * 96UL);
                                var_124 = _pre;
                                local_sp_1 = var_123;
                            }
                            var_126 = var_124;
                            local_sp_2 = local_sp_1;
                            if (*(uint32_t *)(_pre_phi238 + 72UL) == 4294967295U) {
                                var_125 = local_sp_1 + (-8L);
                                *(uint64_t *)var_125 = 4230856UL;
                                indirect_placeholder();
                                var_126 = *var_36;
                                local_sp_2 = var_125;
                            }
                            var_127 = var_126 + 1UL;
                            *var_36 = var_127;
                            var_118 = var_127;
                            local_sp_0 = local_sp_2;
                            var_119 = *_pre_phi232;
                            var_120 = helper_cc_compute_c_wrapper(var_118 - var_119, var_119, var_1, 17U);
                            local_sp_1 = local_sp_0;
                            local_sp_10 = local_sp_0;
                        }
                }
                *(unsigned char *)4338750UL = (unsigned char)'\x01';
                var_128 = *_pre_phi232;
                var_129 = *var_55;
                helper_movq_mm_T0_xmm_wrapper((struct type_3 *)(776UL), *var_13);
                var_130 = local_sp_10 + (-8L);
                *(uint64_t *)var_130 = 4230910UL;
                indirect_placeholder_3(var_129, var_128);
                local_sp_12 = var_130;
            }
            if (*(unsigned char *)4338748UL == '\x00') {
                *(uint64_t *)(local_sp_12 + (-8L)) = 4230931UL;
                indirect_placeholder();
            }
            var_131 = (uint64_t)(*var_8 ^ '\x01');
            rax_1 = var_131;
            return rax_1;
        }
        break;
    }
}
