typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_41_ret_type;
struct indirect_placeholder_40_ret_type;
struct indirect_placeholder_45_ret_type;
struct indirect_placeholder_44_ret_type;
struct indirect_placeholder_47_ret_type;
struct indirect_placeholder_46_ret_type;
struct indirect_placeholder_43_ret_type;
struct indirect_placeholder_42_ret_type;
struct indirect_placeholder_38_ret_type;
struct indirect_placeholder_37_ret_type;
struct indirect_placeholder_36_ret_type;
struct indirect_placeholder_35_ret_type;
struct indirect_placeholder_34_ret_type;
struct indirect_placeholder_33_ret_type;
struct indirect_placeholder_49_ret_type;
struct indirect_placeholder_48_ret_type;
struct indirect_placeholder_41_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_40_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_45_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_44_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_47_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_46_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_43_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_42_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_38_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_37_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_36_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_35_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_34_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_33_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_49_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_48_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_7(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_3(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_11(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern uint64_t init_rcx(void);
extern uint64_t indirect_placeholder_50(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_39(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_41_ret_type indirect_placeholder_41(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_40_ret_type indirect_placeholder_40(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_45_ret_type indirect_placeholder_45(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_44_ret_type indirect_placeholder_44(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_47_ret_type indirect_placeholder_47(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_46_ret_type indirect_placeholder_46(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_43_ret_type indirect_placeholder_43(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_42_ret_type indirect_placeholder_42(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_38_ret_type indirect_placeholder_38(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_37_ret_type indirect_placeholder_37(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_36_ret_type indirect_placeholder_36(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_35_ret_type indirect_placeholder_35(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_34_ret_type indirect_placeholder_34(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_33_ret_type indirect_placeholder_33(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_49_ret_type indirect_placeholder_49(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_48_ret_type indirect_placeholder_48(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
void bb_recheck(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t *var_7;
    unsigned char *var_8;
    unsigned char *var_9;
    uint64_t **var_10;
    uint64_t var_11;
    uint64_t var_12;
    bool var_13;
    unsigned char *var_14;
    unsigned char var_15;
    unsigned char *var_16;
    uint32_t var_17;
    uint32_t *var_18;
    uint64_t local_sp_7;
    uint64_t var_112;
    uint64_t local_sp_0;
    uint64_t var_72;
    uint64_t storemerge4;
    uint64_t var_119;
    uint64_t var_120;
    uint64_t var_121;
    uint64_t var_122;
    uint64_t var_123;
    uint64_t var_124;
    uint64_t rax_0;
    uint64_t var_49;
    uint64_t var_50;
    struct indirect_placeholder_41_ret_type var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t rax_1;
    uint64_t var_73;
    uint64_t var_74;
    struct indirect_placeholder_45_ret_type var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_65;
    struct indirect_placeholder_47_ret_type var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t local_sp_5;
    uint64_t var_108;
    uint64_t var_109;
    unsigned char storemerge5;
    uint64_t _pre;
    uint64_t var_57;
    uint64_t var_56;
    uint64_t spec_select;
    uint64_t var_58;
    struct indirect_placeholder_43_ret_type var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t local_sp_2;
    unsigned char *var_81;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t local_sp_3;
    uint64_t var_113;
    struct indirect_placeholder_38_ret_type var_114;
    uint64_t var_115;
    uint64_t var_116;
    uint64_t var_117;
    uint64_t var_118;
    uint64_t var_89;
    uint64_t var_103;
    uint64_t var_104;
    struct indirect_placeholder_36_ret_type var_105;
    uint64_t var_106;
    uint64_t var_107;
    uint64_t var_100;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t var_90;
    uint64_t var_91;
    struct indirect_placeholder_34_ret_type var_92;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t var_98;
    uint64_t var_99;
    uint32_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    struct indirect_placeholder_49_ret_type var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t rax_2;
    uint64_t var_26;
    uint64_t local_sp_4;
    unsigned char storemerge3;
    unsigned char var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint32_t var_40;
    uint64_t local_sp_6;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t spec_select283;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint32_t *var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rcx();
    var_3 = init_r9();
    var_4 = init_r8();
    var_5 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    var_6 = var_0 + (-192L);
    var_7 = (uint64_t *)var_6;
    *var_7 = rdi;
    var_8 = (unsigned char *)(var_0 + (-196L));
    *var_8 = (unsigned char)rsi;
    var_9 = (unsigned char *)(var_0 + (-25L));
    *var_9 = (unsigned char)'\x01';
    var_10 = (uint64_t **)var_6;
    var_11 = **var_10;
    var_12 = var_0 + (-208L);
    *(uint64_t *)var_12 = 4215039UL;
    indirect_placeholder();
    var_13 = ((uint64_t)(uint32_t)var_11 == 0UL);
    var_14 = (unsigned char *)(var_0 + (-27L));
    *var_14 = var_13;
    var_15 = *(unsigned char *)(*var_7 + 54UL);
    var_16 = (unsigned char *)(var_0 + (-28L));
    *var_16 = var_15;
    var_17 = *(uint32_t *)(*var_7 + 60UL);
    var_18 = (uint32_t *)(var_0 + (-32L));
    *var_18 = var_17;
    local_sp_7 = var_12;
    storemerge4 = 4294967295UL;
    storemerge5 = (unsigned char)'\x01';
    rax_2 = 0UL;
    storemerge3 = (unsigned char)'\x01';
    if (*var_14 == '\x00') {
        spec_select283 = (*var_8 == '\x00') ? 2048UL : 0UL;
        var_19 = **var_10;
        var_20 = var_0 + (-216L);
        *(uint64_t *)var_20 = 4215126UL;
        var_21 = indirect_placeholder_50(0UL, var_2, spec_select283, var_19, spec_select283, var_3, var_4);
        local_sp_7 = var_20;
        rax_2 = var_21;
    }
    var_22 = (uint32_t *)(var_0 + (-36L));
    *var_22 = (uint32_t)rax_2;
    var_23 = *var_7;
    var_24 = local_sp_7 + (-8L);
    *(uint64_t *)var_24 = 4215151UL;
    var_25 = indirect_placeholder_5(var_23);
    local_sp_4 = var_24;
    if ((uint64_t)(unsigned char)var_25 == 0UL) {
        var_26 = local_sp_7 + (-16L);
        *(uint64_t *)var_26 = 4215180UL;
        indirect_placeholder();
        local_sp_4 = var_26;
    }
    local_sp_5 = local_sp_4;
    if (*(unsigned char *)4338736UL == '\x01') {
    }
    *(unsigned char *)(*var_7 + 54UL) = storemerge3;
    var_27 = *(unsigned char *)4338750UL ^ '\x01';
    rax_0 = (uint64_t)var_27;
    if (var_27 == '\x00') {
        var_40 = *var_22;
        local_sp_6 = local_sp_5;
        rax_1 = rax_0;
        if (var_40 == 4294967295U) {
            var_41 = (uint64_t)var_40;
            var_42 = local_sp_5 + (-8L);
            *(uint64_t *)var_42 = 4215409UL;
            indirect_placeholder();
            local_sp_2 = var_42;
            local_sp_6 = var_42;
            rax_1 = var_41;
            if ((int)var_40 > (int)4294967295U) {
                var_43 = *var_7;
                *(uint64_t *)(local_sp_5 + (-16L)) = 4215924UL;
                var_44 = indirect_placeholder_5(var_43);
                var_45 = (uint64_t)*var_22;
                var_46 = local_sp_5 + (-24L);
                *(uint64_t *)var_46 = 4215940UL;
                var_47 = indirect_placeholder_7(var_45, var_44);
                *(unsigned char *)(*var_7 + 53UL) = (unsigned char)var_47;
                var_48 = *var_7;
                local_sp_2 = var_46;
                if (*(unsigned char *)(var_48 + 53UL) == '\x00') {
                    *(uint32_t *)(var_48 + 60UL) = 0U;
                } else {
                    if (*(unsigned char *)4338750UL == '\x01') {
                        *(uint32_t *)(var_48 + 60UL) = 0U;
                    } else {
                        *var_9 = (unsigned char)'\x00';
                        *(uint32_t *)(*var_7 + 60UL) = 4294967295U;
                        var_49 = *var_7;
                        *(uint64_t *)(local_sp_5 + (-32L)) = 4216012UL;
                        var_50 = indirect_placeholder_5(var_49);
                        *(uint64_t *)(local_sp_5 + (-40L)) = 4216025UL;
                        var_51 = indirect_placeholder_41(4UL, var_50);
                        var_52 = var_51.field_0;
                        var_53 = var_51.field_1;
                        var_54 = var_51.field_2;
                        var_55 = local_sp_5 + (-48L);
                        *(uint64_t *)var_55 = 4216053UL;
                        indirect_placeholder_40(0UL, var_52, 4313256UL, 0UL, 0UL, var_53, var_54);
                        *(unsigned char *)(*var_7 + 52UL) = (unsigned char)'\x01';
                        *(unsigned char *)(*var_7 + 53UL) = (unsigned char)'\x01';
                        local_sp_2 = var_55;
                    }
                }
            } else {
                *var_9 = (unsigned char)'\x00';
                var_63 = local_sp_6 + (-8L);
                *(uint64_t *)var_63 = 4215426UL;
                indirect_placeholder();
                *(uint32_t *)(*var_7 + 60UL) = *(uint32_t *)rax_1;
                var_64 = *var_7;
                local_sp_2 = var_63;
                if ((*(unsigned char *)(var_64 + 54UL) ^ '\x01') == '\x00') {
                    var_72 = local_sp_6 + (-16L);
                    *(uint64_t *)var_72 = 4215542UL;
                    indirect_placeholder();
                    local_sp_2 = var_72;
                    if ((uint64_t)(*var_18 - *(volatile uint32_t *)(uint32_t *)0UL) == 0UL) {
                        var_73 = *var_7;
                        *(uint64_t *)(local_sp_6 + (-24L)) = 4215568UL;
                        var_74 = indirect_placeholder_5(var_73);
                        *(uint64_t *)(local_sp_6 + (-32L)) = 4215586UL;
                        var_75 = indirect_placeholder_45(var_74, 0UL, 3UL);
                        var_76 = var_75.field_0;
                        var_77 = var_75.field_1;
                        var_78 = var_75.field_2;
                        *(uint64_t *)(local_sp_6 + (-40L)) = 4215594UL;
                        indirect_placeholder();
                        var_79 = (uint64_t)*(uint32_t *)var_76;
                        var_80 = local_sp_6 + (-48L);
                        *(uint64_t *)var_80 = 4215621UL;
                        indirect_placeholder_44(0UL, var_76, 4313177UL, 0UL, var_79, var_77, var_78);
                        local_sp_2 = var_80;
                    }
                } else {
                    if (*var_16 == '\x00') {
                        *(uint64_t *)(local_sp_6 + (-16L)) = 4215481UL;
                        var_65 = indirect_placeholder_5(var_64);
                        *(uint64_t *)(local_sp_6 + (-24L)) = 4215494UL;
                        var_66 = indirect_placeholder_47(4UL, var_65);
                        var_67 = var_66.field_0;
                        var_68 = var_66.field_1;
                        var_69 = var_66.field_2;
                        var_70 = (uint64_t)*(uint32_t *)(*var_7 + 60UL);
                        var_71 = local_sp_6 + (-32L);
                        *(uint64_t *)var_71 = 4215532UL;
                        indirect_placeholder_46(0UL, var_67, 4313150UL, 0UL, var_70, var_68, var_69);
                        local_sp_2 = var_71;
                    }
                }
            }
        } else {
            *var_9 = (unsigned char)'\x00';
            var_63 = local_sp_6 + (-8L);
            *(uint64_t *)var_63 = 4215426UL;
            indirect_placeholder();
            *(uint32_t *)(*var_7 + 60UL) = *(uint32_t *)rax_1;
            var_64 = *var_7;
            local_sp_2 = var_63;
            if ((*(unsigned char *)(var_64 + 54UL) ^ '\x01') == '\x00') {
                if (*var_16 == '\x00') {
                    *(uint64_t *)(local_sp_6 + (-16L)) = 4215481UL;
                    var_65 = indirect_placeholder_5(var_64);
                    *(uint64_t *)(local_sp_6 + (-24L)) = 4215494UL;
                    var_66 = indirect_placeholder_47(4UL, var_65);
                    var_67 = var_66.field_0;
                    var_68 = var_66.field_1;
                    var_69 = var_66.field_2;
                    var_70 = (uint64_t)*(uint32_t *)(*var_7 + 60UL);
                    var_71 = local_sp_6 + (-32L);
                    *(uint64_t *)var_71 = 4215532UL;
                    indirect_placeholder_46(0UL, var_67, 4313150UL, 0UL, var_70, var_68, var_69);
                    local_sp_2 = var_71;
                }
            } else {
                var_72 = local_sp_6 + (-16L);
                *(uint64_t *)var_72 = 4215542UL;
                indirect_placeholder();
                local_sp_2 = var_72;
                if ((uint64_t)(*var_18 - *(volatile uint32_t *)(uint32_t *)0UL) == 0UL) {
                    var_73 = *var_7;
                    *(uint64_t *)(local_sp_6 + (-24L)) = 4215568UL;
                    var_74 = indirect_placeholder_5(var_73);
                    *(uint64_t *)(local_sp_6 + (-32L)) = 4215586UL;
                    var_75 = indirect_placeholder_45(var_74, 0UL, 3UL);
                    var_76 = var_75.field_0;
                    var_77 = var_75.field_1;
                    var_78 = var_75.field_2;
                    *(uint64_t *)(local_sp_6 + (-40L)) = 4215594UL;
                    indirect_placeholder();
                    var_79 = (uint64_t)*(uint32_t *)var_76;
                    var_80 = local_sp_6 + (-48L);
                    *(uint64_t *)var_80 = 4215621UL;
                    indirect_placeholder_44(0UL, var_76, 4313177UL, 0UL, var_79, var_77, var_78);
                    local_sp_2 = var_80;
                }
            }
        }
        var_81 = (unsigned char *)(var_0 + (-26L));
        *var_81 = (unsigned char)'\x00';
        local_sp_3 = local_sp_2;
        if (*var_9 != '\x01') {
            var_82 = *var_7;
            *(uint64_t *)(local_sp_2 + (-8L)) = 4216127UL;
            var_83 = indirect_placeholder_5(var_82);
            var_84 = (uint64_t)*var_22;
            *(uint64_t *)(local_sp_2 + (-16L)) = 4216143UL;
            indirect_placeholder_3(var_84, var_83);
            var_85 = *var_7;
            *(uint64_t *)(local_sp_2 + (-24L)) = 4216158UL;
            var_86 = indirect_placeholder_5(var_85);
            var_87 = (uint64_t)*(uint32_t *)(*var_7 + 56UL);
            var_88 = local_sp_2 + (-32L);
            *(uint64_t *)var_88 = 4216181UL;
            indirect_placeholder_3(var_87, var_86);
            *(uint32_t *)(*var_7 + 56UL) = 4294967295U;
            local_sp_0 = var_88;
            if (*var_81 != '\x00') {
                if (*var_14 == '\x00') {
                    storemerge4 = (uint64_t)*var_8;
                }
                var_119 = var_0 + (-184L);
                var_120 = (uint64_t)*var_22;
                var_121 = *var_7;
                *(uint64_t *)(local_sp_0 + (-8L)) = 4216636UL;
                indirect_placeholder_39(var_119, 0UL, var_121, var_120, storemerge4);
                var_122 = *var_7;
                *(uint64_t *)(local_sp_0 + (-16L)) = 4216651UL;
                var_123 = indirect_placeholder_5(var_122);
                var_124 = (uint64_t)*var_22;
                *(uint64_t *)(local_sp_0 + (-24L)) = 4216677UL;
                indirect_placeholder_11(var_123, 0UL, var_124, 0UL);
            }
            return;
        }
        var_89 = *var_7;
        if (*(uint32_t *)(var_89 + 56UL) == 4294967295U) {
            *var_81 = (unsigned char)'\x01';
            var_103 = *var_7;
            *(uint64_t *)(local_sp_2 + (-8L)) = 4216351UL;
            var_104 = indirect_placeholder_5(var_103);
            *(uint64_t *)(local_sp_2 + (-16L)) = 4216364UL;
            var_105 = indirect_placeholder_36(4UL, var_104);
            var_106 = var_105.field_0;
            var_107 = var_105.field_1;
            var_108 = var_105.field_2;
            var_109 = local_sp_2 + (-24L);
            *(uint64_t *)var_109 = 4216392UL;
            indirect_placeholder_35(0UL, var_106, 4313352UL, 0UL, 0UL, var_107, var_108);
            local_sp_0 = var_109;
        } else {
            if (*(uint64_t *)(var_89 + 40UL) != *(uint64_t *)(var_0 + (-176L))) {
                if (*(uint64_t *)(var_89 + 32UL) != *(uint64_t *)(var_0 + (-184L))) {
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4216558UL;
                    var_100 = indirect_placeholder_5(var_89);
                    var_101 = (uint64_t)*var_22;
                    var_102 = local_sp_2 + (-16L);
                    *(uint64_t *)var_102 = 4216574UL;
                    indirect_placeholder_3(var_101, var_100);
                    local_sp_0 = var_102;
                    if (*var_81 != '\x00') {
                        if (*var_14 == '\x00') {
                            storemerge4 = (uint64_t)*var_8;
                        }
                        var_119 = var_0 + (-184L);
                        var_120 = (uint64_t)*var_22;
                        var_121 = *var_7;
                        *(uint64_t *)(local_sp_0 + (-8L)) = 4216636UL;
                        indirect_placeholder_39(var_119, 0UL, var_121, var_120, storemerge4);
                        var_122 = *var_7;
                        *(uint64_t *)(local_sp_0 + (-16L)) = 4216651UL;
                        var_123 = indirect_placeholder_5(var_122);
                        var_124 = (uint64_t)*var_22;
                        *(uint64_t *)(local_sp_0 + (-24L)) = 4216677UL;
                        indirect_placeholder_11(var_123, 0UL, var_124, 0UL);
                    }
                    return;
                }
            }
            *var_81 = (unsigned char)'\x01';
            var_90 = *var_7;
            *(uint64_t *)(local_sp_2 + (-8L)) = 4216462UL;
            var_91 = indirect_placeholder_5(var_90);
            *(uint64_t *)(local_sp_2 + (-16L)) = 4216475UL;
            var_92 = indirect_placeholder_34(4UL, var_91);
            var_93 = var_92.field_0;
            var_94 = var_92.field_1;
            var_95 = var_92.field_2;
            *(uint64_t *)(local_sp_2 + (-24L)) = 4216503UL;
            indirect_placeholder_33(0UL, var_93, 4313392UL, 0UL, 0UL, var_94, var_95);
            var_96 = *var_7;
            *(uint64_t *)(local_sp_2 + (-32L)) = 4216518UL;
            var_97 = indirect_placeholder_5(var_96);
            var_98 = (uint64_t)*(uint32_t *)(*var_7 + 56UL);
            var_99 = local_sp_2 + (-40L);
            *(uint64_t *)var_99 = 4216541UL;
            indirect_placeholder_3(var_98, var_97);
            local_sp_0 = var_99;
        }
    } else {
        var_28 = **var_10;
        var_29 = var_0 + (-184L);
        var_30 = local_sp_4 + (-8L);
        *(uint64_t *)var_30 = 4215271UL;
        var_31 = indirect_placeholder_5(var_28);
        local_sp_5 = var_30;
        rax_0 = var_31;
        var_32 = (uint32_t)((uint16_t)*(uint32_t *)(var_0 + (-160L)) & (unsigned short)61440U);
        rax_0 = (uint64_t)var_32;
        if ((uint64_t)(uint32_t)var_31 != 0UL & (uint64_t)((var_32 + (-40960)) & (-4096)) != 0UL) {
            *var_9 = (unsigned char)'\x00';
            *(uint32_t *)(*var_7 + 60UL) = 4294967295U;
            *(unsigned char *)(*var_7 + 52UL) = (unsigned char)'\x01';
            var_33 = *var_7;
            *(uint64_t *)(local_sp_4 + (-16L)) = 4215337UL;
            var_34 = indirect_placeholder_7(var_33, var_29);
            *(uint64_t *)(local_sp_4 + (-24L)) = 4215350UL;
            var_35 = indirect_placeholder_49(4UL, var_34);
            var_36 = var_35.field_0;
            var_37 = var_35.field_1;
            var_38 = var_35.field_2;
            var_39 = local_sp_4 + (-32L);
            *(uint64_t *)var_39 = 4215378UL;
            indirect_placeholder_48(0UL, var_36, 4313096UL, 0UL, 0UL, var_37, var_38);
            local_sp_2 = var_39;
            var_81 = (unsigned char *)(var_0 + (-26L));
            *var_81 = (unsigned char)'\x00';
            local_sp_3 = local_sp_2;
            if (*var_9 != '\x01') {
                var_82 = *var_7;
                *(uint64_t *)(local_sp_2 + (-8L)) = 4216127UL;
                var_83 = indirect_placeholder_5(var_82);
                var_84 = (uint64_t)*var_22;
                *(uint64_t *)(local_sp_2 + (-16L)) = 4216143UL;
                indirect_placeholder_3(var_84, var_83);
                var_85 = *var_7;
                *(uint64_t *)(local_sp_2 + (-24L)) = 4216158UL;
                var_86 = indirect_placeholder_5(var_85);
                var_87 = (uint64_t)*(uint32_t *)(*var_7 + 56UL);
                var_88 = local_sp_2 + (-32L);
                *(uint64_t *)var_88 = 4216181UL;
                indirect_placeholder_3(var_87, var_86);
                *(uint32_t *)(*var_7 + 56UL) = 4294967295U;
                local_sp_0 = var_88;
                if (*var_81 != '\x00') {
                    if (*var_14 == '\x00') {
                        storemerge4 = (uint64_t)*var_8;
                    }
                    var_119 = var_0 + (-184L);
                    var_120 = (uint64_t)*var_22;
                    var_121 = *var_7;
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4216636UL;
                    indirect_placeholder_39(var_119, 0UL, var_121, var_120, storemerge4);
                    var_122 = *var_7;
                    *(uint64_t *)(local_sp_0 + (-16L)) = 4216651UL;
                    var_123 = indirect_placeholder_5(var_122);
                    var_124 = (uint64_t)*var_22;
                    *(uint64_t *)(local_sp_0 + (-24L)) = 4216677UL;
                    indirect_placeholder_11(var_123, 0UL, var_124, 0UL);
                }
                return;
            }
            var_89 = *var_7;
            if (*(uint32_t *)(var_89 + 56UL) == 4294967295U) {
                *var_81 = (unsigned char)'\x01';
                var_103 = *var_7;
                *(uint64_t *)(local_sp_2 + (-8L)) = 4216351UL;
                var_104 = indirect_placeholder_5(var_103);
                *(uint64_t *)(local_sp_2 + (-16L)) = 4216364UL;
                var_105 = indirect_placeholder_36(4UL, var_104);
                var_106 = var_105.field_0;
                var_107 = var_105.field_1;
                var_108 = var_105.field_2;
                var_109 = local_sp_2 + (-24L);
                *(uint64_t *)var_109 = 4216392UL;
                indirect_placeholder_35(0UL, var_106, 4313352UL, 0UL, 0UL, var_107, var_108);
                local_sp_0 = var_109;
            } else {
                if (*(uint64_t *)(var_89 + 40UL) != *(uint64_t *)(var_0 + (-176L))) {
                    if (*(uint64_t *)(var_89 + 32UL) != *(uint64_t *)(var_0 + (-184L))) {
                        *(uint64_t *)(local_sp_2 + (-8L)) = 4216558UL;
                        var_100 = indirect_placeholder_5(var_89);
                        var_101 = (uint64_t)*var_22;
                        var_102 = local_sp_2 + (-16L);
                        *(uint64_t *)var_102 = 4216574UL;
                        indirect_placeholder_3(var_101, var_100);
                        local_sp_0 = var_102;
                        if (*var_81 != '\x00') {
                            if (*var_14 == '\x00') {
                                storemerge4 = (uint64_t)*var_8;
                            }
                            var_119 = var_0 + (-184L);
                            var_120 = (uint64_t)*var_22;
                            var_121 = *var_7;
                            *(uint64_t *)(local_sp_0 + (-8L)) = 4216636UL;
                            indirect_placeholder_39(var_119, 0UL, var_121, var_120, storemerge4);
                            var_122 = *var_7;
                            *(uint64_t *)(local_sp_0 + (-16L)) = 4216651UL;
                            var_123 = indirect_placeholder_5(var_122);
                            var_124 = (uint64_t)*var_22;
                            *(uint64_t *)(local_sp_0 + (-24L)) = 4216677UL;
                            indirect_placeholder_11(var_123, 0UL, var_124, 0UL);
                        }
                        return;
                    }
                }
                *var_81 = (unsigned char)'\x01';
                var_90 = *var_7;
                *(uint64_t *)(local_sp_2 + (-8L)) = 4216462UL;
                var_91 = indirect_placeholder_5(var_90);
                *(uint64_t *)(local_sp_2 + (-16L)) = 4216475UL;
                var_92 = indirect_placeholder_34(4UL, var_91);
                var_93 = var_92.field_0;
                var_94 = var_92.field_1;
                var_95 = var_92.field_2;
                *(uint64_t *)(local_sp_2 + (-24L)) = 4216503UL;
                indirect_placeholder_33(0UL, var_93, 4313392UL, 0UL, 0UL, var_94, var_95);
                var_96 = *var_7;
                *(uint64_t *)(local_sp_2 + (-32L)) = 4216518UL;
                var_97 = indirect_placeholder_5(var_96);
                var_98 = (uint64_t)*(uint32_t *)(*var_7 + 56UL);
                var_99 = local_sp_2 + (-40L);
                *(uint64_t *)var_99 = 4216541UL;
                indirect_placeholder_3(var_98, var_97);
                local_sp_0 = var_99;
            }
        }
    }
}
