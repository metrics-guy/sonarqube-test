typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_75_ret_type;
struct indirect_placeholder_77_ret_type;
struct indirect_placeholder_76_ret_type;
struct indirect_placeholder_78_ret_type;
struct indirect_placeholder_75_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_77_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_76_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_78_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_15(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t indirect_placeholder_5(uint64_t param_0);
extern void indirect_placeholder_3(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_11(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern struct indirect_placeholder_75_ret_type indirect_placeholder_75(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_77_ret_type indirect_placeholder_77(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_76_ret_type indirect_placeholder_76(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_78_ret_type indirect_placeholder_78(uint64_t param_0);
void bb_check_fspec(uint64_t rdi, uint64_t rsi) {
    uint64_t var_24;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t r9_0;
    struct indirect_placeholder_76_ret_type var_30;
    uint64_t var_45;
    uint64_t r8_0;
    uint64_t var_44;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    bool var_15;
    uint64_t var_25;
    struct indirect_placeholder_77_ret_type var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t local_sp_0;
    uint64_t var_16;
    struct indirect_placeholder_78_ret_type var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    unsigned char storemerge;
    unsigned char *var_36;
    unsigned char var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t *var_42;
    uint64_t *var_43;
    uint64_t var_7;
    uint64_t *var_8;
    uint32_t var_9;
    uint64_t var_10;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r9();
    var_3 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_4 = (uint64_t *)(var_0 + (-192L));
    *var_4 = rdi;
    var_5 = var_0 + (-200L);
    *(uint64_t *)var_5 = rsi;
    var_6 = *var_4;
    r9_0 = var_2;
    r8_0 = var_3;
    storemerge = (unsigned char)'\x00';
    if (*(uint32_t *)(var_6 + 56UL) == 4294967295U) {
        return;
    }
    *(uint64_t *)(var_0 + (-208L)) = 4219992UL;
    var_7 = indirect_placeholder_5(var_6);
    var_8 = (uint64_t *)(var_0 + (-16L));
    *var_8 = var_7;
    var_9 = *(uint32_t *)(*var_4 + 56UL);
    var_10 = var_0 + (-216L);
    *(uint64_t *)var_10 = 4220023UL;
    indirect_placeholder();
    local_sp_0 = var_10;
    if (var_9 == 0U) {
        var_14 = *var_4;
        var_15 = ((uint32_t)((uint16_t)*(uint32_t *)(var_14 + 48UL) & (unsigned short)61440U) == 32768U);
        if (!var_15) {
            if (!var_15) {
                if (*(unsigned char *)4338741UL == '\x00') {
                } else {
                    storemerge = (unsigned char)'\x01';
                    if (*var_4 == **(uint64_t **)var_5) {
                    }
                }
                var_36 = (unsigned char *)(var_0 + (-17L));
                var_37 = storemerge & '\x01';
                *var_36 = var_37;
                var_38 = (uint64_t)*(uint32_t *)(*var_4 + 56UL);
                var_39 = (uint64_t)var_37;
                var_40 = *var_8;
                *(uint64_t *)(local_sp_0 + (-8L)) = 4220402UL;
                var_41 = indirect_placeholder_11(18446744073709551615UL, var_38, var_39, var_40);
                var_42 = (uint64_t *)(var_0 + (-32L));
                *var_42 = var_41;
                var_43 = (uint64_t *)(*var_4 + 8UL);
                *var_43 = (var_41 + *var_43);
                **(uint64_t **)var_5 = *var_4;
                var_44 = *(uint64_t *)4337216UL;
                *(uint64_t *)(local_sp_0 + (-16L)) = 4220480UL;
                indirect_placeholder();
                if (*var_42 != 0UL & (uint64_t)(uint32_t)var_44 == 0UL) {
                    *(uint64_t *)(local_sp_0 + (-24L)) = 4220489UL;
                    indirect_placeholder();
                    var_45 = (uint64_t)*(uint32_t *)var_44;
                    *(uint64_t *)(local_sp_0 + (-32L)) = 4220513UL;
                    indirect_placeholder_75(0UL, 18446744073709551615UL, 4313513UL, 1UL, var_45, r9_0, r8_0);
                }
                return;
            }
            if (*(uint64_t *)(var_0 + (-136L)) != *(uint64_t *)(var_14 + 8UL)) {
                var_16 = var_0 + (-184L);
                *(uint64_t *)(var_0 + (-224L)) = 4220287UL;
                var_17 = indirect_placeholder_78(var_16);
                var_18 = var_17.field_0;
                var_19 = var_17.field_1;
                var_20 = *var_4;
                var_21 = *(uint64_t *)(var_20 + 16UL);
                var_22 = *(uint64_t *)(var_20 + 24UL);
                var_23 = var_0 + (-232L);
                *(uint64_t *)var_23 = 4220313UL;
                var_24 = indirect_placeholder_15(var_19, var_18, var_21, var_22, var_2);
                local_sp_0 = var_23;
                if ((uint64_t)(uint32_t)var_24 == 0UL) {
                    return;
                }
            }
            if (*(unsigned char *)4338741UL == '\x00') {
            } else {
                storemerge = (unsigned char)'\x01';
                if (*var_4 == **(uint64_t **)var_5) {
                }
            }
            var_36 = (unsigned char *)(var_0 + (-17L));
            var_37 = storemerge & '\x01';
            *var_36 = var_37;
            var_38 = (uint64_t)*(uint32_t *)(*var_4 + 56UL);
            var_39 = (uint64_t)var_37;
            var_40 = *var_8;
            *(uint64_t *)(local_sp_0 + (-8L)) = 4220402UL;
            var_41 = indirect_placeholder_11(18446744073709551615UL, var_38, var_39, var_40);
            var_42 = (uint64_t *)(var_0 + (-32L));
            *var_42 = var_41;
            var_43 = (uint64_t *)(*var_4 + 8UL);
            *var_43 = (var_41 + *var_43);
            **(uint64_t **)var_5 = *var_4;
            var_44 = *(uint64_t *)4337216UL;
            *(uint64_t *)(local_sp_0 + (-16L)) = 4220480UL;
            indirect_placeholder();
            if (*var_42 != 0UL & (uint64_t)(uint32_t)var_44 == 0UL) {
                *(uint64_t *)(local_sp_0 + (-24L)) = 4220489UL;
                indirect_placeholder();
                var_45 = (uint64_t)*(uint32_t *)var_44;
                *(uint64_t *)(local_sp_0 + (-32L)) = 4220513UL;
                indirect_placeholder_75(0UL, 18446744073709551615UL, 4313513UL, 1UL, var_45, r9_0, r8_0);
            }
            return;
        }
        if ((long)*(uint64_t *)(var_0 + (-136L)) >= (long)*(uint64_t *)(var_14 + 8UL)) {
            var_16 = var_0 + (-184L);
            *(uint64_t *)(var_0 + (-224L)) = 4220287UL;
            var_17 = indirect_placeholder_78(var_16);
            var_18 = var_17.field_0;
            var_19 = var_17.field_1;
            var_20 = *var_4;
            var_21 = *(uint64_t *)(var_20 + 16UL);
            var_22 = *(uint64_t *)(var_20 + 24UL);
            var_23 = var_0 + (-232L);
            *(uint64_t *)var_23 = 4220313UL;
            var_24 = indirect_placeholder_15(var_19, var_18, var_21, var_22, var_2);
            local_sp_0 = var_23;
            if (!var_15 & *(uint64_t *)(var_0 + (-136L)) != *(uint64_t *)(var_14 + 8UL) && (uint64_t)(uint32_t)var_24 == 0UL) {
                return;
            }
        }
        var_25 = *var_8;
        *(uint64_t *)(var_0 + (-224L)) = 4220151UL;
        var_26 = indirect_placeholder_77(var_25, 0UL, 3UL);
        var_27 = var_26.field_0;
        var_28 = var_26.field_1;
        var_29 = var_26.field_2;
        *(uint64_t *)(var_0 + (-232L)) = 4220179UL;
        var_30 = indirect_placeholder_76(0UL, var_27, 4313475UL, 0UL, 0UL, var_28, var_29);
        var_31 = var_30.field_1;
        var_32 = var_30.field_2;
        var_33 = (uint64_t)*(uint32_t *)(*var_4 + 56UL);
        var_34 = *var_8;
        var_35 = var_0 + (-240L);
        *(uint64_t *)var_35 = 4220213UL;
        indirect_placeholder_11(var_34, 0UL, var_33, 0UL);
        *(uint64_t *)(*var_4 + 8UL) = 0UL;
        local_sp_0 = var_35;
        r9_0 = var_31;
        r8_0 = var_32;
    } else {
        var_11 = (uint64_t)var_9;
        *(uint64_t *)(var_0 + (-224L)) = 4220032UL;
        indirect_placeholder();
        *(uint32_t *)(*var_4 + 60UL) = *(uint32_t *)var_11;
        var_12 = (uint64_t)*(uint32_t *)(*var_4 + 56UL);
        var_13 = *var_8;
        *(uint64_t *)(var_0 + (-232L)) = 4220068UL;
        indirect_placeholder_3(var_12, var_13);
        *(uint32_t *)(*var_4 + 56UL) = 4294967295U;
    }
}
