typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_movq_mm_T0_xmm_wrapper_ret_type;
struct type_4;
struct indirect_placeholder_8_ret_type;
struct indirect_placeholder_9_ret_type;
struct indirect_placeholder_10_ret_type;
struct indirect_placeholder_13_ret_type;
struct indirect_placeholder_12_ret_type;
struct indirect_placeholder_17_ret_type;
struct indirect_placeholder_16_ret_type;
struct indirect_placeholder_18_ret_type;
struct indirect_placeholder_19_ret_type;
struct indirect_placeholder_21_ret_type;
struct indirect_placeholder_22_ret_type;
struct indirect_placeholder_20_ret_type;
struct indirect_placeholder_23_ret_type;
struct helper_movq_mm_T0_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_4 {
};
struct indirect_placeholder_8_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_9_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_10_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_13_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_12_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_17_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_16_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_18_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_19_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_21_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_22_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_20_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_23_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_7(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_state_0x8558(void);
extern struct helper_movq_mm_T0_xmm_wrapper_ret_type helper_movq_mm_T0_xmm_wrapper(struct type_4 *param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_15(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_3(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_6(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0);
extern void indirect_placeholder_14(uint64_t param_0);
extern uint64_t indirect_placeholder_11(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern uint64_t init_rcx(void);
extern struct indirect_placeholder_8_ret_type indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_9_ret_type indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_10_ret_type indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_13_ret_type indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_12_ret_type indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_17_ret_type indirect_placeholder_17(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_16_ret_type indirect_placeholder_16(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_18_ret_type indirect_placeholder_18(uint64_t param_0);
extern struct indirect_placeholder_19_ret_type indirect_placeholder_19(uint64_t param_0);
extern struct indirect_placeholder_21_ret_type indirect_placeholder_21(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_22_ret_type indirect_placeholder_22(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_20_ret_type indirect_placeholder_20(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_23_ret_type indirect_placeholder_23(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
void bb_tail_forever(uint64_t rdi, uint64_t rsi) {
    uint64_t var_94;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t *var_10;
    struct indirect_placeholder_9_ret_type var_140;
    uint64_t *var_11;
    unsigned char storemerge;
    uint64_t var_148;
    uint64_t r9_0;
    uint64_t r8_0;
    uint64_t local_sp_1;
    uint64_t local_sp_2;
    uint64_t local_sp_12_be;
    uint64_t local_sp_12;
    uint64_t rcx_1;
    uint64_t r9_7;
    uint64_t r8_7;
    uint32_t var_142;
    uint64_t local_sp_0;
    uint64_t rcx_0;
    uint64_t var_143;
    uint64_t var_144;
    unsigned char var_145;
    uint64_t var_146;
    uint64_t var_147;
    uint64_t r9_9;
    uint64_t var_138;
    uint64_t var_139;
    uint64_t r8_9;
    uint64_t var_141;
    uint64_t var_136;
    uint64_t var_137;
    uint64_t var_98;
    struct indirect_placeholder_13_ret_type var_99;
    uint64_t var_100;
    uint64_t var_101;
    uint64_t var_102;
    struct indirect_placeholder_12_ret_type var_103;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t var_107;
    uint64_t var_108;
    uint64_t _pre_phi295;
    uint64_t local_sp_3;
    uint64_t r8_5;
    uint64_t r9_1;
    uint64_t r8_1;
    uint64_t var_109;
    uint64_t var_113;
    uint64_t var_112;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t local_sp_4;
    uint64_t *var_123;
    uint64_t var_124;
    uint64_t var_125;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_126;
    uint64_t var_127;
    uint64_t local_sp_14;
    uint64_t r9_5;
    uint64_t var_128;
    uint64_t var_129;
    uint64_t var_130;
    uint64_t var_74;
    struct indirect_placeholder_17_ret_type var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    struct indirect_placeholder_16_ret_type var_80;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint32_t var_85;
    uint32_t var_86;
    struct indirect_placeholder_18_ret_type var_87;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t local_sp_5;
    uint64_t var_95;
    struct indirect_placeholder_19_ret_type var_96;
    uint64_t var_97;
    uint64_t var_51;
    uint64_t local_sp_7;
    uint64_t var_52;
    uint64_t var_53;
    struct indirect_placeholder_21_ret_type var_54;
    uint64_t r9_3;
    uint64_t r8_3;
    uint64_t local_sp_8;
    uint64_t r9_4;
    uint64_t r8_4;
    uint32_t var_58;
    uint64_t var_59;
    uint64_t local_sp_9;
    uint64_t var_60;
    uint64_t var_61;
    struct indirect_placeholder_22_ret_type var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    struct indirect_placeholder_20_ret_type var_68;
    uint64_t var_41;
    uint64_t var_42;
    struct indirect_placeholder_23_ret_type var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint32_t var_48;
    uint32_t var_49;
    uint32_t var_50;
    uint64_t local_sp_10;
    uint64_t var_69;
    uint32_t var_70;
    uint64_t var_71;
    bool var_72;
    uint64_t var_73;
    uint64_t local_sp_11;
    uint64_t r9_6;
    uint64_t r8_6;
    uint64_t var_114;
    uint64_t var_116;
    uint64_t var_115;
    uint64_t var_117;
    uint64_t var_118;
    uint64_t var_119;
    uint64_t var_120;
    uint64_t *var_121;
    uint64_t var_122;
    uint64_t var_12;
    unsigned char storemerge1;
    unsigned char *var_13;
    unsigned char *var_14;
    uint64_t var_15;
    uint64_t *var_16;
    unsigned char *var_17;
    uint64_t *var_18;
    uint32_t *var_19;
    uint64_t *var_20;
    uint32_t *var_21;
    uint32_t *var_22;
    uint32_t *var_23;
    uint32_t *var_24;
    uint64_t *var_25;
    uint64_t var_26;
    uint64_t *var_27;
    uint64_t *var_28;
    uint64_t var_29;
    uint64_t rcx_3;
    uint64_t local_sp_13;
    uint64_t rcx_2;
    uint64_t r9_8;
    uint64_t r8_8;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_132;
    uint64_t var_133;
    uint64_t var_134;
    uint64_t var_135;
    uint64_t var_32;
    uint32_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint32_t var_39;
    uint64_t var_40;
    uint64_t var_131;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_cc_src2();
    var_2 = init_rbp();
    var_3 = init_rcx();
    var_4 = init_state_0x8558();
    var_5 = init_r9();
    var_6 = init_r8();
    var_7 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_7;
    var_8 = var_0 + (-280L);
    var_9 = (uint64_t *)(var_0 + (-256L));
    *var_9 = rdi;
    var_10 = (uint64_t *)(var_0 + (-264L));
    *var_10 = rsi;
    var_11 = (uint64_t *)(var_0 + (-272L));
    *var_11 = var_4;
    storemerge = (unsigned char)'\x00';
    local_sp_12 = var_8;
    rcx_1 = var_3;
    r9_7 = var_5;
    r8_7 = var_6;
    var_116 = 18446744073709551615UL;
    storemerge1 = (unsigned char)'\x00';
    var_29 = 0UL;
    if (*(uint32_t *)4338744UL == 0U) {
    }
    var_13 = (unsigned char *)(var_0 + (-65L));
    *var_13 = (storemerge1 & '\x01');
    var_14 = (unsigned char *)(var_0 + (-33L));
    *var_14 = (unsigned char)'\x00';
    var_15 = *var_10 + (-1L);
    var_16 = (uint64_t *)(var_0 + (-32L));
    *var_16 = var_15;
    var_17 = (unsigned char *)(var_0 + (-49L));
    var_18 = (uint64_t *)(var_0 + (-48L));
    var_19 = (uint32_t *)(var_0 + (-72L));
    var_20 = (uint64_t *)(var_0 + (-80L));
    var_21 = (uint32_t *)(var_0 + (-84L));
    var_22 = (uint32_t *)(var_0 + (-88L));
    var_23 = (uint32_t *)(var_0 + (-92L));
    var_24 = (uint32_t *)(var_0 + (-224L));
    var_25 = (uint64_t *)(var_0 + (-200L));
    var_26 = var_0 + (-248L);
    var_27 = (uint64_t *)(var_0 + (-64L));
    var_28 = (uint64_t *)(var_0 + (-104L));
    while (1U)
        {
            *var_17 = (unsigned char)'\x00';
            *var_18 = 0UL;
            local_sp_13 = local_sp_12;
            rcx_2 = rcx_1;
            r9_8 = r9_7;
            r8_8 = r8_7;
            var_30 = *var_10;
            var_31 = helper_cc_compute_c_wrapper(var_29 - var_30, var_30, var_1, 17U);
            r9_0 = r9_8;
            r8_0 = r8_8;
            rcx_0 = rcx_2;
            r9_9 = r9_8;
            r8_9 = r8_8;
            r8_5 = r8_8;
            local_sp_14 = local_sp_13;
            r9_5 = r9_8;
            rcx_3 = rcx_2;
            while (var_31 != 0UL)
                {
                    var_32 = *var_9 + (*var_18 * 96UL);
                    if (*(unsigned char *)(var_32 + 52UL) != '\x00') {
                        var_33 = *(uint32_t *)(var_32 + 56UL);
                        if ((int)var_33 > (int)4294967295U) {
                            var_34 = (uint64_t)*var_13;
                            var_35 = local_sp_13 + (-8L);
                            *(uint64_t *)var_35 = 4217138UL;
                            indirect_placeholder_3(var_32, var_34);
                            local_sp_14 = var_35;
                            rcx_3 = var_34;
                        } else {
                            *var_19 = var_33;
                            var_36 = *var_9 + (*var_18 * 96UL);
                            var_37 = local_sp_13 + (-8L);
                            *(uint64_t *)var_37 = 4217217UL;
                            var_38 = indirect_placeholder_5(var_36);
                            *var_20 = var_38;
                            *var_21 = *(uint32_t *)((*var_9 + (*var_18 * 96UL)) + 48UL);
                            var_39 = *(uint32_t *)((*var_9 + (*var_18 * 96UL)) + 64UL);
                            var_40 = (uint64_t)var_39;
                            local_sp_10 = var_37;
                            if ((uint64_t)(var_39 - (uint32_t)(uint64_t)*var_13) != 0UL) {
                                var_41 = (uint64_t)*var_19;
                                var_42 = local_sp_13 + (-16L);
                                *(uint64_t *)var_42 = 4217322UL;
                                var_43 = indirect_placeholder_23(0UL, rcx_2, var_40, var_41, 3UL, r9_8, r8_8);
                                var_44 = var_43.field_0;
                                var_45 = var_43.field_1;
                                var_46 = var_43.field_2;
                                var_47 = var_43.field_3;
                                var_48 = (uint32_t)var_44;
                                *var_22 = var_48;
                                var_49 = ((*var_13 == '\x00') ? 2048U : 0U) | var_48;
                                *var_23 = var_49;
                                var_50 = *var_22;
                                local_sp_7 = var_42;
                                r9_3 = var_46;
                                r8_3 = var_47;
                                local_sp_8 = var_42;
                                r9_4 = var_46;
                                r8_4 = var_47;
                                if ((int)var_50 < (int)0U) {
                                    var_51 = (uint64_t)var_49;
                                    if ((uint64_t)(var_49 - var_50) == 0UL) {
                                        *(uint32_t *)(((*var_18 * 96UL) + *var_9) + 64UL) = (uint32_t)*var_13;
                                        local_sp_10 = local_sp_7;
                                        r9_5 = r9_3;
                                        r8_5 = r8_3;
                                    } else {
                                        var_52 = (uint64_t)*var_19;
                                        var_53 = local_sp_13 + (-24L);
                                        *(uint64_t *)var_53 = 4217390UL;
                                        var_54 = indirect_placeholder_21(0UL, var_45, var_51, var_52, 4UL, var_46, var_47);
                                        var_55 = var_54.field_0;
                                        var_56 = var_54.field_2;
                                        var_57 = var_54.field_3;
                                        local_sp_7 = var_53;
                                        r9_3 = var_56;
                                        r8_3 = var_57;
                                        local_sp_8 = var_53;
                                        r9_4 = var_56;
                                        r8_4 = var_57;
                                        if ((uint64_t)((uint32_t)var_55 + 1U) == 0UL) {
                                            *(uint32_t *)(((*var_18 * 96UL) + *var_9) + 64UL) = (uint32_t)*var_13;
                                            local_sp_10 = local_sp_7;
                                            r9_5 = r9_3;
                                            r8_5 = r8_3;
                                        } else {
                                            var_58 = (uint32_t)((uint16_t)*(uint32_t *)((*var_9 + (*var_18 * 96UL)) + 48UL) & (unsigned short)61440U);
                                            var_59 = (uint64_t)var_58;
                                            local_sp_9 = local_sp_8;
                                            r9_5 = r9_4;
                                            r8_5 = r8_4;
                                            if ((uint64_t)((var_58 + (-32768)) & (-4096)) == 0UL) {
                                                var_61 = *var_20;
                                                *(uint64_t *)(local_sp_9 + (-8L)) = 4217474UL;
                                                var_62 = indirect_placeholder_22(var_61, 0UL, 3UL);
                                                var_63 = var_62.field_0;
                                                var_64 = var_62.field_1;
                                                var_65 = var_62.field_2;
                                                *(uint64_t *)(local_sp_9 + (-16L)) = 4217482UL;
                                                indirect_placeholder();
                                                var_66 = (uint64_t)*(uint32_t *)var_63;
                                                var_67 = local_sp_9 + (-24L);
                                                *(uint64_t *)var_67 = 4217509UL;
                                                var_68 = indirect_placeholder_20(0UL, var_63, 4313440UL, 1UL, var_66, var_64, var_65);
                                                local_sp_10 = var_67;
                                                r9_5 = var_68.field_1;
                                                r8_5 = var_68.field_2;
                                            } else {
                                                var_60 = local_sp_8 + (-8L);
                                                *(uint64_t *)var_60 = 4217445UL;
                                                indirect_placeholder();
                                                local_sp_9 = var_60;
                                                local_sp_10 = var_60;
                                                if (*(uint32_t *)var_59 == 1U) {
                                                    var_61 = *var_20;
                                                    *(uint64_t *)(local_sp_9 + (-8L)) = 4217474UL;
                                                    var_62 = indirect_placeholder_22(var_61, 0UL, 3UL);
                                                    var_63 = var_62.field_0;
                                                    var_64 = var_62.field_1;
                                                    var_65 = var_62.field_2;
                                                    *(uint64_t *)(local_sp_9 + (-16L)) = 4217482UL;
                                                    indirect_placeholder();
                                                    var_66 = (uint64_t)*(uint32_t *)var_63;
                                                    var_67 = local_sp_9 + (-24L);
                                                    *(uint64_t *)var_67 = 4217509UL;
                                                    var_68 = indirect_placeholder_20(0UL, var_63, 4313440UL, 1UL, var_66, var_64, var_65);
                                                    local_sp_10 = var_67;
                                                    r9_5 = var_68.field_1;
                                                    r8_5 = var_68.field_2;
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    var_58 = (uint32_t)((uint16_t)*(uint32_t *)((*var_9 + (*var_18 * 96UL)) + 48UL) & (unsigned short)61440U);
                                    var_59 = (uint64_t)var_58;
                                    local_sp_9 = local_sp_8;
                                    r9_5 = r9_4;
                                    r8_5 = r8_4;
                                    if ((uint64_t)((var_58 + (-32768)) & (-4096)) == 0UL) {
                                        var_61 = *var_20;
                                        *(uint64_t *)(local_sp_9 + (-8L)) = 4217474UL;
                                        var_62 = indirect_placeholder_22(var_61, 0UL, 3UL);
                                        var_63 = var_62.field_0;
                                        var_64 = var_62.field_1;
                                        var_65 = var_62.field_2;
                                        *(uint64_t *)(local_sp_9 + (-16L)) = 4217482UL;
                                        indirect_placeholder();
                                        var_66 = (uint64_t)*(uint32_t *)var_63;
                                        var_67 = local_sp_9 + (-24L);
                                        *(uint64_t *)var_67 = 4217509UL;
                                        var_68 = indirect_placeholder_20(0UL, var_63, 4313440UL, 1UL, var_66, var_64, var_65);
                                        local_sp_10 = var_67;
                                        r9_5 = var_68.field_1;
                                        r8_5 = var_68.field_2;
                                    } else {
                                        var_60 = local_sp_8 + (-8L);
                                        *(uint64_t *)var_60 = 4217445UL;
                                        indirect_placeholder();
                                        local_sp_9 = var_60;
                                        local_sp_10 = var_60;
                                        if (*(uint32_t *)var_59 == 1U) {
                                            var_61 = *var_20;
                                            *(uint64_t *)(local_sp_9 + (-8L)) = 4217474UL;
                                            var_62 = indirect_placeholder_22(var_61, 0UL, 3UL);
                                            var_63 = var_62.field_0;
                                            var_64 = var_62.field_1;
                                            var_65 = var_62.field_2;
                                            *(uint64_t *)(local_sp_9 + (-16L)) = 4217482UL;
                                            indirect_placeholder();
                                            var_66 = (uint64_t)*(uint32_t *)var_63;
                                            var_67 = local_sp_9 + (-24L);
                                            *(uint64_t *)var_67 = 4217509UL;
                                            var_68 = indirect_placeholder_20(0UL, var_63, 4313440UL, 1UL, var_66, var_64, var_65);
                                            local_sp_10 = var_67;
                                            r9_5 = var_68.field_1;
                                            r8_5 = var_68.field_2;
                                        }
                                    }
                                }
                            }
                            var_69 = *var_18;
                            r9_9 = r9_5;
                            r8_9 = r8_5;
                            r9_1 = r9_5;
                            r8_1 = r8_5;
                            var_113 = var_69;
                            local_sp_11 = local_sp_10;
                            r9_6 = r9_5;
                            r8_6 = r8_5;
                            if (*(uint32_t *)((*var_9 + (var_69 * 96UL)) + 64UL) != 0U) {
                                var_70 = *var_19;
                                var_71 = local_sp_10 + (-8L);
                                *(uint64_t *)var_71 = 4217609UL;
                                indirect_placeholder();
                                var_72 = (var_70 == 0U);
                                var_73 = *var_9 + (*var_18 * 96UL);
                                _pre_phi295 = var_73;
                                local_sp_5 = var_71;
                                if (!var_72) {
                                    *(uint32_t *)(var_73 + 56UL) = 4294967295U;
                                    *(uint64_t *)(local_sp_10 + (-16L)) = 4217659UL;
                                    indirect_placeholder();
                                    *(uint32_t *)(((*var_18 * 96UL) + *var_9) + 60UL) = *(uint32_t *)var_73;
                                    var_74 = *var_20;
                                    *(uint64_t *)(local_sp_10 + (-24L)) = 4217719UL;
                                    var_75 = indirect_placeholder_17(var_74, 0UL, 3UL);
                                    var_76 = var_75.field_0;
                                    var_77 = var_75.field_1;
                                    var_78 = var_75.field_2;
                                    *(uint64_t *)(local_sp_10 + (-32L)) = 4217727UL;
                                    indirect_placeholder();
                                    var_79 = (uint64_t)*(uint32_t *)var_76;
                                    *(uint64_t *)(local_sp_10 + (-40L)) = 4217754UL;
                                    var_80 = indirect_placeholder_16(0UL, var_76, 4313177UL, 0UL, var_79, var_77, var_78);
                                    var_81 = var_80.field_0;
                                    var_82 = var_80.field_1;
                                    var_83 = var_80.field_2;
                                    var_84 = local_sp_10 + (-48L);
                                    *(uint64_t *)var_84 = 4217764UL;
                                    indirect_placeholder();
                                    local_sp_14 = var_84;
                                    rcx_3 = var_81;
                                    r9_9 = var_82;
                                    r8_9 = var_83;
                                    var_131 = *var_18 + 1UL;
                                    *var_18 = var_131;
                                    var_29 = var_131;
                                    local_sp_13 = local_sp_14;
                                    rcx_2 = rcx_3;
                                    r9_8 = r9_9;
                                    r8_8 = r8_9;
                                    var_30 = *var_10;
                                    var_31 = helper_cc_compute_c_wrapper(var_29 - var_30, var_30, var_1, 17U);
                                    r9_0 = r9_8;
                                    r8_0 = r8_8;
                                    rcx_0 = rcx_2;
                                    r9_9 = r9_8;
                                    r8_9 = r8_8;
                                    r8_5 = r8_8;
                                    local_sp_14 = local_sp_13;
                                    r9_5 = r9_8;
                                    rcx_3 = rcx_2;
                                    continue;
                                }
                                var_85 = *(uint32_t *)(var_73 + 48UL);
                                var_86 = *var_24;
                                if ((uint64_t)(var_85 - var_86) != 0UL) {
                                    if ((uint32_t)((uint16_t)var_86 & (unsigned short)61440U) == 32768U) {
                                        if (*(uint64_t *)(var_73 + 8UL) != *var_25) {
                                            *(uint64_t *)(local_sp_10 + (-16L)) = 4217899UL;
                                            var_87 = indirect_placeholder_18(var_26);
                                            var_88 = var_87.field_0;
                                            var_89 = var_87.field_1;
                                            var_90 = *var_9 + (*var_18 * 96UL);
                                            var_91 = *(uint64_t *)(var_90 + 16UL);
                                            var_92 = *(uint64_t *)(var_90 + 24UL);
                                            var_93 = local_sp_10 + (-24L);
                                            *(uint64_t *)var_93 = 4217960UL;
                                            var_94 = indirect_placeholder_15(var_89, var_88, var_91, var_92, r9_5);
                                            local_sp_5 = var_93;
                                            local_sp_14 = var_93;
                                            if ((uint64_t)(uint32_t)var_94 != 0UL) {
                                                var_123 = (uint64_t *)(((*var_18 * 96UL) + *var_9) + 88UL);
                                                var_124 = *var_123;
                                                var_125 = var_124 + 1UL;
                                                *var_123 = var_125;
                                                var_126 = *(uint64_t *)4338408UL;
                                                var_127 = helper_cc_compute_c_wrapper(var_124 - var_126, var_126, var_1, 17U);
                                                rcx_3 = var_125;
                                                if (var_127 != 0UL & *(uint32_t *)4338400UL == 1U) {
                                                    var_128 = *var_9 + (*var_18 * 96UL);
                                                    var_129 = (*(uint32_t *)(var_128 + 64UL) != 0U);
                                                    var_130 = local_sp_10 + (-32L);
                                                    *(uint64_t *)var_130 = 4218122UL;
                                                    indirect_placeholder_3(var_128, var_129);
                                                    *(uint64_t *)((*var_9 + (*var_18 * 96UL)) + 88UL) = 0UL;
                                                    local_sp_14 = var_130;
                                                    rcx_3 = var_129;
                                                }
                                                var_131 = *var_18 + 1UL;
                                                *var_18 = var_131;
                                                var_29 = var_131;
                                                local_sp_13 = local_sp_14;
                                                rcx_2 = rcx_3;
                                                r9_8 = r9_9;
                                                r8_8 = r8_9;
                                                var_30 = *var_10;
                                                var_31 = helper_cc_compute_c_wrapper(var_29 - var_30, var_30, var_1, 17U);
                                                r9_0 = r9_8;
                                                r8_0 = r8_8;
                                                rcx_0 = rcx_2;
                                                r9_9 = r9_8;
                                                r8_9 = r8_8;
                                                r8_5 = r8_8;
                                                local_sp_14 = local_sp_13;
                                                r9_5 = r9_8;
                                                rcx_3 = rcx_2;
                                                continue;
                                            }
                                            _pre_phi295 = *var_9 + (*var_18 * 96UL);
                                        }
                                    } else {
                                        *(uint64_t *)(local_sp_10 + (-16L)) = 4217899UL;
                                        var_87 = indirect_placeholder_18(var_26);
                                        var_88 = var_87.field_0;
                                        var_89 = var_87.field_1;
                                        var_90 = *var_9 + (*var_18 * 96UL);
                                        var_91 = *(uint64_t *)(var_90 + 16UL);
                                        var_92 = *(uint64_t *)(var_90 + 24UL);
                                        var_93 = local_sp_10 + (-24L);
                                        *(uint64_t *)var_93 = 4217960UL;
                                        var_94 = indirect_placeholder_15(var_89, var_88, var_91, var_92, r9_5);
                                        local_sp_5 = var_93;
                                        local_sp_14 = var_93;
                                        if ((uint64_t)(uint32_t)var_94 != 0UL) {
                                            var_123 = (uint64_t *)(((*var_18 * 96UL) + *var_9) + 88UL);
                                            var_124 = *var_123;
                                            var_125 = var_124 + 1UL;
                                            *var_123 = var_125;
                                            var_126 = *(uint64_t *)4338408UL;
                                            var_127 = helper_cc_compute_c_wrapper(var_124 - var_126, var_126, var_1, 17U);
                                            rcx_3 = var_125;
                                            if (var_127 != 0UL & *(uint32_t *)4338400UL == 1U) {
                                                var_128 = *var_9 + (*var_18 * 96UL);
                                                var_129 = (*(uint32_t *)(var_128 + 64UL) != 0U);
                                                var_130 = local_sp_10 + (-32L);
                                                *(uint64_t *)var_130 = 4218122UL;
                                                indirect_placeholder_3(var_128, var_129);
                                                *(uint64_t *)((*var_9 + (*var_18 * 96UL)) + 88UL) = 0UL;
                                                local_sp_14 = var_130;
                                                rcx_3 = var_129;
                                            }
                                            var_131 = *var_18 + 1UL;
                                            *var_18 = var_131;
                                            var_29 = var_131;
                                            local_sp_13 = local_sp_14;
                                            rcx_2 = rcx_3;
                                            r9_8 = r9_9;
                                            r8_8 = r8_9;
                                            var_30 = *var_10;
                                            var_31 = helper_cc_compute_c_wrapper(var_29 - var_30, var_30, var_1, 17U);
                                            r9_0 = r9_8;
                                            r8_0 = r8_8;
                                            rcx_0 = rcx_2;
                                            r9_9 = r9_8;
                                            r8_9 = r8_8;
                                            r8_5 = r8_8;
                                            local_sp_14 = local_sp_13;
                                            r9_5 = r9_8;
                                            rcx_3 = rcx_2;
                                            continue;
                                        }
                                        _pre_phi295 = *var_9 + (*var_18 * 96UL);
                                    }
                                }
                                var_95 = local_sp_5 + (-8L);
                                *(uint64_t *)var_95 = 4218211UL;
                                var_96 = indirect_placeholder_19(var_26);
                                var_97 = var_96.field_1;
                                *(uint64_t *)(_pre_phi295 + 16UL) = var_96.field_0;
                                *(uint64_t *)(_pre_phi295 + 24UL) = var_97;
                                *(uint32_t *)(((*var_18 * 96UL) + *var_9) + 48UL) = *var_24;
                                *(uint64_t *)((*var_9 + (*var_18 * 96UL)) + 88UL) = 0UL;
                                local_sp_3 = var_95;
                                if ((uint32_t)((uint16_t)*var_21 & (unsigned short)61440U) != 32768U & (long)*var_25 < (long)*(uint64_t *)((*var_9 + (*var_18 * 96UL)) + 8UL)) {
                                    var_98 = *var_20;
                                    *(uint64_t *)(local_sp_5 + (-16L)) = 4218383UL;
                                    var_99 = indirect_placeholder_13(var_98, 0UL, 3UL);
                                    var_100 = var_99.field_0;
                                    var_101 = var_99.field_1;
                                    var_102 = var_99.field_2;
                                    *(uint64_t *)(local_sp_5 + (-24L)) = 4218411UL;
                                    var_103 = indirect_placeholder_12(0UL, var_100, 4313475UL, 0UL, 0UL, var_101, var_102);
                                    var_104 = var_103.field_1;
                                    var_105 = var_103.field_2;
                                    var_106 = *var_20;
                                    var_107 = (uint64_t)*var_19;
                                    var_108 = local_sp_5 + (-32L);
                                    *(uint64_t *)var_108 = 4218438UL;
                                    indirect_placeholder_11(var_106, 0UL, var_107, 0UL);
                                    *(uint64_t *)((*var_9 + (*var_18 * 96UL)) + 8UL) = 0UL;
                                    local_sp_3 = var_108;
                                    r9_1 = var_104;
                                    r8_1 = var_105;
                                }
                                var_109 = *var_18;
                                var_113 = var_109;
                                var_112 = var_109;
                                local_sp_4 = local_sp_3;
                                local_sp_11 = local_sp_3;
                                r9_6 = r9_1;
                                r8_6 = r8_1;
                                if (var_109 != *var_16) {
                                    if (*(unsigned char *)4338741UL != '\x00') {
                                        var_110 = *var_20;
                                        var_111 = local_sp_3 + (-8L);
                                        *(uint64_t *)var_111 = 4218509UL;
                                        indirect_placeholder_14(var_110);
                                        var_112 = *var_18;
                                        local_sp_4 = var_111;
                                    }
                                    *var_16 = var_112;
                                    var_113 = *var_18;
                                    local_sp_11 = local_sp_4;
                                }
                            }
                            var_114 = *var_9 + (var_113 * 96UL);
                            r9_9 = r9_6;
                            r8_9 = r8_6;
                            if (*(uint32_t *)(var_114 + 64UL) == 0U) {
                                *var_27 = 18446744073709551614UL;
                                var_116 = 18446744073709551614UL;
                            } else {
                                if ((uint32_t)((uint16_t)*var_21 & (unsigned short)61440U) == 32768U) {
                                    *var_27 = 18446744073709551615UL;
                                } else {
                                    if (*(unsigned char *)(var_114 + 53UL) == '\x00') {
                                        *var_27 = 18446744073709551615UL;
                                    } else {
                                        var_115 = *var_25 - *(uint64_t *)(var_114 + 8UL);
                                        *var_27 = var_115;
                                        var_116 = var_115;
                                    }
                                }
                            }
                            var_117 = (uint64_t)*var_19;
                            var_118 = *var_20;
                            var_119 = local_sp_11 + (-8L);
                            *(uint64_t *)var_119 = 4218702UL;
                            var_120 = indirect_placeholder_11(var_116, var_117, 0UL, var_118);
                            *var_28 = var_120;
                            *var_17 = (((uint64_t)*var_17 | (var_120 != 0UL)) != 0UL);
                            var_121 = (uint64_t *)((*var_9 + (*var_18 * 96UL)) + 8UL);
                            var_122 = *var_28 + *var_121;
                            *var_121 = var_122;
                            local_sp_14 = var_119;
                            rcx_3 = var_122;
                        }
                    }
                    var_131 = *var_18 + 1UL;
                    *var_18 = var_131;
                    var_29 = var_131;
                    local_sp_13 = local_sp_14;
                    rcx_2 = rcx_3;
                    r9_8 = r9_9;
                    r8_8 = r8_9;
                    var_30 = *var_10;
                    var_31 = helper_cc_compute_c_wrapper(var_29 - var_30, var_30, var_1, 17U);
                    r9_0 = r9_8;
                    r8_0 = r8_8;
                    rcx_0 = rcx_2;
                    r9_9 = r9_8;
                    r8_9 = r8_8;
                    r8_5 = r8_8;
                    local_sp_14 = local_sp_13;
                    r9_5 = r9_8;
                    rcx_3 = rcx_2;
                }
            var_132 = *var_10;
            var_133 = *var_9;
            var_134 = local_sp_13 + (-8L);
            *(uint64_t *)var_134 = 4218866UL;
            var_135 = indirect_placeholder_7(var_133, var_132);
            local_sp_2 = var_134;
            if ((uint64_t)(unsigned char)var_135 != 1UL) {
                *(uint64_t *)(local_sp_13 + (-16L)) = 4218898UL;
                indirect_placeholder_10(0UL, rcx_2, 4313494UL, 0UL, 0UL, r9_8, r8_8);
                break;
            }
            if (*var_17 == '\x01') {
                var_136 = *(uint64_t *)4337216UL;
                var_137 = local_sp_13 + (-16L);
                *(uint64_t *)var_137 = 4218935UL;
                indirect_placeholder();
                local_sp_2 = var_137;
                if (*var_13 != '\x00' & (uint64_t)(uint32_t)var_136 == 0UL) {
                    *(uint64_t *)(local_sp_13 + (-24L)) = 4218944UL;
                    indirect_placeholder();
                    var_138 = (uint64_t)*(uint32_t *)var_136;
                    var_139 = local_sp_13 + (-32L);
                    *(uint64_t *)var_139 = 4218968UL;
                    var_140 = indirect_placeholder_9(0UL, rcx_2, 4313513UL, 1UL, var_138, r9_8, r8_8);
                    local_sp_2 = var_139;
                    rcx_0 = var_140.field_0;
                    r9_0 = var_140.field_1;
                    r8_0 = var_140.field_2;
                }
            } else {
                var_136 = *(uint64_t *)4337216UL;
                var_137 = local_sp_13 + (-16L);
                *(uint64_t *)var_137 = 4218935UL;
                indirect_placeholder();
                local_sp_2 = var_137;
                if ((uint64_t)(uint32_t)var_136 == 0UL) {
                    *(uint64_t *)(local_sp_13 + (-24L)) = 4218944UL;
                    indirect_placeholder();
                    var_138 = (uint64_t)*(uint32_t *)var_136;
                    var_139 = local_sp_13 + (-32L);
                    *(uint64_t *)var_139 = 4218968UL;
                    var_140 = indirect_placeholder_9(0UL, rcx_2, 4313513UL, 1UL, var_138, r9_8, r8_8);
                    local_sp_2 = var_139;
                    rcx_0 = var_140.field_0;
                    r9_0 = var_140.field_1;
                    r8_0 = var_140.field_2;
                }
            }
            var_141 = local_sp_2 + (-8L);
            *(uint64_t *)var_141 = 4218973UL;
            indirect_placeholder();
            local_sp_12_be = var_141;
            rcx_1 = rcx_0;
            r9_7 = r9_0;
            r8_7 = r8_0;
            local_sp_0 = var_141;
            if (*var_17 == '\x01') {
                local_sp_12 = local_sp_12_be;
                continue;
            }
            if (*var_14 == '\x00') {
                break;
            }
            var_142 = *(uint32_t *)4338744UL;
            if (var_142 == 0U) {
                local_sp_1 = local_sp_0;
            } else {
                *(uint64_t *)(local_sp_2 + (-16L)) = 4219026UL;
                indirect_placeholder();
                var_143 = (uint64_t)var_142;
                var_144 = local_sp_2 + (-24L);
                *(uint64_t *)var_144 = 4219035UL;
                indirect_placeholder();
                local_sp_0 = var_144;
                local_sp_1 = var_144;
                storemerge = (unsigned char)'\x01';
                if (*(uint32_t *)var_143 == 1U) {
                    local_sp_1 = local_sp_0;
                }
            }
            var_145 = storemerge & '\x01';
            *var_14 = var_145;
            local_sp_12_be = local_sp_1;
            helper_movq_mm_T0_xmm_wrapper((struct type_4 *)(776UL), *var_11);
            var_146 = local_sp_1 + (-8L);
            *(uint64_t *)var_146 = 4219093UL;
            var_147 = indirect_placeholder_6();
            local_sp_12_be = var_146;
            if (var_145 != '\x01' & (uint64_t)(uint32_t)var_147 != 0UL) {
                *(uint64_t *)(local_sp_1 + (-16L)) = 4219106UL;
                indirect_placeholder();
                var_148 = (uint64_t)*(uint32_t *)var_147;
                *(uint64_t *)(local_sp_1 + (-24L)) = 4219130UL;
                indirect_placeholder_8(0UL, rcx_0, 4313525UL, 1UL, var_148, r9_0, r8_0);
                break;
            }
        }
    return;
}
