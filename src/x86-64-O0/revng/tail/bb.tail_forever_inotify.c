typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_mulsd_wrapper_ret_type;
struct type_5;
struct type_7;
struct helper_subsd_wrapper_ret_type;
struct indirect_placeholder_81_ret_type;
struct indirect_placeholder_82_ret_type;
struct indirect_placeholder_84_ret_type;
struct indirect_placeholder_83_ret_type;
struct indirect_placeholder_85_ret_type;
struct indirect_placeholder_87_ret_type;
struct indirect_placeholder_88_ret_type;
struct helper_cvttsd2sq_wrapper_ret_type;
struct helper_cvtsq2sd_wrapper_ret_type;
struct indirect_placeholder_89_ret_type;
struct indirect_placeholder_86_ret_type;
struct indirect_placeholder_91_ret_type;
struct indirect_placeholder_90_ret_type;
struct indirect_placeholder_93_ret_type;
struct indirect_placeholder_92_ret_type;
struct indirect_placeholder_94_ret_type;
struct indirect_placeholder_95_ret_type;
struct indirect_placeholder_97_ret_type;
struct indirect_placeholder_96_ret_type;
struct indirect_placeholder_98_ret_type;
struct helper_mulsd_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct type_5 {
};
struct type_7 {
};
struct helper_subsd_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct indirect_placeholder_81_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_82_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_84_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_83_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_85_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_87_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_88_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct helper_cvttsd2sq_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvtsq2sd_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct indirect_placeholder_89_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_86_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_91_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_90_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_93_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_92_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_94_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_95_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_97_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_96_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_98_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_7(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_state_0x8558(void);
extern unsigned char init_state_0x8549(void);
extern unsigned char init_state_0x854c(void);
extern unsigned char init_state_0x8548(void);
extern unsigned char init_state_0x854b(void);
extern unsigned char init_state_0x8547(void);
extern unsigned char init_state_0x854d(void);
extern void indirect_placeholder(void);
extern struct helper_mulsd_wrapper_ret_type helper_mulsd_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_subsd_wrapper_ret_type helper_subsd_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_3(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_6(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0);
extern uint64_t indirect_placeholder_11(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern void indirect_placeholder_56(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_81_ret_type indirect_placeholder_81(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_82_ret_type indirect_placeholder_82(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_84_ret_type indirect_placeholder_84(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_83_ret_type indirect_placeholder_83(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_85_ret_type indirect_placeholder_85(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_87_ret_type indirect_placeholder_87(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_88_ret_type indirect_placeholder_88(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct helper_cvttsd2sq_wrapper_ret_type helper_cvttsd2sq_wrapper(struct type_5 *param_0, struct type_7 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4);
extern struct helper_cvtsq2sd_wrapper_ret_type helper_cvtsq2sd_wrapper(struct type_5 *param_0, struct type_7 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6);
extern struct indirect_placeholder_89_ret_type indirect_placeholder_89(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_86_ret_type indirect_placeholder_86(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void llvm_trap(void);
extern struct indirect_placeholder_91_ret_type indirect_placeholder_91(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_90_ret_type indirect_placeholder_90(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_93_ret_type indirect_placeholder_93(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_92_ret_type indirect_placeholder_92(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_94_ret_type indirect_placeholder_94(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_95_ret_type indirect_placeholder_95(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_97_ret_type indirect_placeholder_97(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_96_ret_type indirect_placeholder_96(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_98_ret_type indirect_placeholder_98(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
uint64_t bb_tail_forever_inotify(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t *var_136;
    uint64_t var_137;
    uint64_t *var_138;
    uint64_t *var_139;
    uint64_t *var_140;
    uint32_t *var_141;
    unsigned char *var_142;
    unsigned char *var_143;
    uint64_t *var_144;
    uint32_t *var_145;
    uint64_t var_121;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t var_112;
    uint64_t var_122;
    uint64_t var_123;
    uint32_t var_166;
    uint32_t var_169;
    uint64_t *var_170;
    uint64_t spec_select;
    uint32_t var_171;
    uint64_t var_172;
    uint64_t var_173;
    uint64_t var_174;
    uint32_t var_175;
    uint64_t var_167;
    uint32_t var_168;
    struct helper_cvttsd2sq_wrapper_ret_type var_164;
    struct indirect_placeholder_83_ret_type var_237;
    struct indirect_placeholder_81_ret_type var_27;
    struct helper_cvttsd2sq_wrapper_ret_type var_158;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    unsigned char var_5;
    unsigned char var_6;
    unsigned char var_7;
    unsigned char var_8;
    unsigned char var_9;
    unsigned char var_10;
    uint64_t var_11;
    uint32_t *var_12;
    uint64_t *var_13;
    uint64_t *var_14;
    uint64_t *var_15;
    uint32_t *var_16;
    unsigned char *var_17;
    unsigned char *var_18;
    unsigned char *var_19;
    unsigned char *var_20;
    unsigned char *var_21;
    uint64_t *var_22;
    uint64_t *var_23;
    uint64_t *var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t *var_32;
    uint64_t var_73;
    uint64_t var_251;
    uint64_t var_250;
    uint64_t local_sp_0;
    uint64_t var_252;
    uint64_t var_253;
    uint64_t var_254;
    uint64_t var_255;
    uint64_t local_sp_1;
    uint64_t var_256;
    uint64_t var_257;
    uint64_t var_258;
    struct indirect_placeholder_82_ret_type var_259;
    uint64_t var_260;
    uint64_t var_261;
    uint64_t var_262;
    uint64_t r9_6;
    uint64_t var_230;
    struct indirect_placeholder_84_ret_type var_231;
    uint64_t var_232;
    uint64_t var_233;
    uint64_t var_234;
    uint64_t var_235;
    uint64_t var_236;
    unsigned char var_239;
    uint64_t var_229;
    uint64_t local_sp_2;
    uint64_t var_180;
    uint64_t var_179;
    uint64_t local_sp_14;
    uint64_t r8_4;
    uint64_t rcx_4;
    uint32_t *var_238;
    uint64_t rax_1;
    uint64_t var_187;
    uint32_t var_188;
    uint64_t rax_0;
    uint64_t local_sp_3;
    uint32_t var_189;
    uint64_t var_190;
    uint64_t var_191;
    uint64_t var_192;
    uint64_t var_193;
    uint64_t var_194;
    uint64_t r9_1_be;
    uint64_t local_sp_4;
    uint64_t r9_4;
    uint64_t var_195;
    uint64_t var_196;
    uint64_t r8_3;
    unsigned char state_0x8549_2;
    struct indirect_placeholder_87_ret_type var_197;
    uint64_t var_176;
    uint64_t var_177;
    uint64_t r8_2_ph;
    struct indirect_placeholder_88_ret_type var_178;
    uint64_t r9_0;
    uint64_t r8_0;
    uint64_t rcx_0;
    uint64_t local_sp_5;
    uint64_t rcx_6;
    uint64_t var_200;
    uint64_t rcx_5;
    uint64_t var_201;
    uint64_t r9_2_ph;
    uint64_t var_155;
    uint64_t var_156;
    uint64_t local_sp_7;
    uint32_t var_153;
    uint64_t local_sp_6;
    unsigned char storemerge21;
    unsigned char var_157;
    uint64_t rcx_2;
    unsigned char state_0x8549_3;
    uint64_t var_159;
    unsigned char var_160;
    struct helper_cvtsq2sd_wrapper_ret_type var_161;
    struct helper_subsd_wrapper_ret_type var_162;
    struct helper_mulsd_wrapper_ret_type var_163;
    unsigned char var_165;
    uint64_t rcx_16;
    uint64_t rcx_3;
    uint64_t r8_11;
    uint64_t r8_1_be;
    uint64_t r9_1;
    uint64_t rcx_1_be;
    uint64_t r8_1;
    unsigned char state_0x8549_0;
    uint64_t local_sp_28;
    uint64_t local_sp_8_be;
    uint64_t rcx_1;
    uint64_t local_sp_8;
    uint64_t local_sp_9_ph;
    uint64_t var_146;
    uint64_t var_147;
    uint64_t var_148;
    uint64_t local_sp_23;
    unsigned char state_0x8549_1_ph;
    uint64_t rcx_2_ph;
    uint64_t local_sp_9_ph543;
    uint64_t r8_2;
    unsigned char state_0x8549_1;
    uint64_t local_sp_9;
    uint64_t var_149;
    uint64_t var_150;
    uint64_t var_182;
    uint64_t var_183;
    uint64_t var_181;
    uint64_t r9_3;
    uint64_t local_sp_10;
    uint64_t var_184;
    uint64_t var_185;
    uint64_t var_186;
    uint64_t local_sp_11;
    uint64_t var_198;
    uint64_t var_199;
    uint64_t var_207;
    uint64_t var_202;
    uint32_t var_203;
    uint64_t var_204;
    uint64_t var_205;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_206;
    uint64_t var_216;
    uint64_t var_208;
    uint64_t local_sp_21;
    uint64_t rcx_7;
    uint64_t local_sp_12;
    uint64_t var_209;
    uint64_t var_210;
    uint64_t rcx_8;
    uint64_t local_sp_13;
    uint64_t var_218;
    unsigned char var_219;
    unsigned char var_223;
    uint64_t var_220;
    uint32_t var_221;
    uint64_t var_222;
    uint64_t rcx_9;
    unsigned char var_224;
    uint64_t var_225;
    uint64_t r9_5;
    uint64_t var_226;
    uint32_t var_227;
    uint64_t var_228;
    uint64_t r8_5;
    uint64_t rcx_10;
    uint64_t local_sp_15;
    uint32_t var_240;
    unsigned char storemerge23;
    unsigned char var_241;
    uint32_t *var_242;
    uint32_t *_pre_phi616;
    uint64_t var_243;
    uint64_t var_244;
    uint64_t var_245;
    uint64_t local_sp_16;
    uint64_t var_246;
    uint64_t var_247;
    uint64_t var_248;
    uint64_t var_249;
    uint64_t r8_6;
    uint64_t local_sp_17;
    uint64_t r9_7;
    uint64_t var_263;
    uint64_t var_264;
    uint64_t r8_7;
    uint64_t rcx_11;
    uint64_t local_sp_18;
    uint64_t var_268;
    uint64_t var_108;
    uint64_t var_109;
    uint16_t var_269;
    uint64_t var_273;
    uint64_t var_270;
    uint64_t var_271;
    uint64_t var_272;
    uint64_t local_sp_19;
    uint64_t var_274;
    uint64_t var_275;
    uint64_t var_211;
    uint64_t var_212;
    uint64_t var_213;
    uint64_t var_214;
    uint64_t var_215;
    uint64_t rcx_13;
    uint64_t var_217;
    uint64_t var_265;
    uint64_t var_266;
    uint64_t var_267;
    uint32_t var_151;
    uint64_t var_152;
    uint64_t local_sp_22;
    uint64_t var_154;
    uint64_t var_113;
    uint64_t local_sp_30;
    uint64_t var_114;
    struct indirect_placeholder_91_ret_type var_115;
    uint64_t var_116;
    uint64_t var_117;
    uint64_t var_118;
    uint64_t var_119;
    uint32_t *var_120;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t r9_12;
    uint64_t var_80;
    struct indirect_placeholder_93_ret_type var_81;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    struct indirect_placeholder_92_ret_type var_87;
    uint64_t var_76;
    uint32_t var_77;
    uint32_t var_74;
    uint64_t var_75;
    uint64_t local_sp_24;
    uint64_t var_88;
    uint64_t rcx_17;
    uint64_t local_sp_32;
    struct indirect_placeholder_94_ret_type var_89;
    uint64_t r9_11;
    uint32_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint32_t var_71;
    uint64_t var_72;
    uint64_t var_90;
    uint64_t var_91;
    struct indirect_placeholder_95_ret_type var_92;
    uint64_t r9_9;
    uint64_t r8_9;
    uint64_t rcx_14;
    uint64_t local_sp_25;
    uint64_t var_55;
    struct indirect_placeholder_97_ret_type var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    struct indirect_placeholder_96_ret_type var_62;
    uint64_t var_63;
    struct indirect_placeholder_98_ret_type var_64;
    uint64_t var_54;
    uint64_t var_49;
    uint32_t var_50;
    uint64_t local_sp_26;
    uint64_t _pre_phi620;
    uint64_t storemerge;
    uint64_t var_51;
    uint64_t var_52;
    uint32_t var_53;
    uint64_t _pre_phi624;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_65;
    uint32_t var_66;
    uint64_t var_67;
    uint32_t *var_33;
    uint64_t *var_34;
    uint64_t *var_35;
    uint64_t *var_36;
    unsigned char *var_37;
    uint64_t var_38;
    uint64_t r8_12;
    uint64_t r9_10;
    uint64_t r8_10;
    uint64_t rcx_15;
    uint64_t local_sp_27;
    uint64_t var_39;
    uint64_t var_40;
    bool var_98;
    uint64_t var_100;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t *var_103;
    uint64_t *var_104;
    uint64_t var_124;
    uint64_t var_105;
    uint64_t local_sp_29;
    uint64_t var_106;
    uint64_t var_107;
    uint64_t var_126;
    uint64_t var_127;
    uint64_t var_128;
    uint64_t *var_129;
    uint64_t *var_130;
    uint64_t var_131;
    uint64_t *var_132;
    uint64_t *var_133;
    uint32_t *var_134;
    uint32_t *var_135;
    uint64_t local_sp_31;
    uint64_t var_125;
    uint32_t *var_99;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_97;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_cc_src2();
    var_2 = init_rbp();
    var_3 = init_state_0x8558();
    var_4 = init_rbx();
    var_5 = init_state_0x8549();
    var_6 = init_state_0x854c();
    var_7 = init_state_0x8548();
    var_8 = init_state_0x854b();
    var_9 = init_state_0x8547();
    var_10 = init_state_0x854d();
    var_11 = var_0 + (-8L);
    *(uint64_t *)var_11 = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    var_12 = (uint32_t *)(var_0 + (-364L));
    *var_12 = (uint32_t)rdi;
    var_13 = (uint64_t *)(var_0 + (-376L));
    *var_13 = rsi;
    var_14 = (uint64_t *)(var_0 + (-384L));
    *var_14 = rdx;
    var_15 = (uint64_t *)(var_0 + (-392L));
    *var_15 = var_3;
    var_16 = (uint32_t *)(var_0 + (-28L));
    *var_16 = 3U;
    var_17 = (unsigned char *)(var_0 + (-29L));
    *var_17 = (unsigned char)'\x00';
    var_18 = (unsigned char *)(var_0 + (-30L));
    *var_18 = (unsigned char)'\x00';
    var_19 = (unsigned char *)(var_0 + (-31L));
    *var_19 = (unsigned char)'\x00';
    var_20 = (unsigned char *)(var_0 + (-32L));
    *var_20 = (unsigned char)'\x00';
    var_21 = (unsigned char *)(var_0 + (-33L));
    *var_21 = (unsigned char)'\x00';
    var_22 = (uint64_t *)(var_0 + (-48L));
    *var_22 = 0UL;
    var_23 = (uint64_t *)(var_0 + (-64L));
    *var_23 = 0UL;
    var_24 = (uint64_t *)(var_0 + (-72L));
    *var_24 = 0UL;
    var_25 = *var_14;
    var_26 = var_0 + (-400L);
    *(uint64_t *)var_26 = 4220646UL;
    var_27 = indirect_placeholder_81(4219884UL, 4219841UL, var_25, 0UL, 0UL);
    var_28 = var_27.field_0;
    var_29 = var_27.field_1;
    var_30 = var_27.field_2;
    var_31 = var_0 + (-136L);
    var_32 = (uint64_t *)var_31;
    *var_32 = var_28;
    var_166 = 16U;
    rax_1 = 1UL;
    rcx_0 = 0UL;
    var_200 = 0UL;
    storemerge21 = (unsigned char)'\x00';
    state_0x8549_0 = var_5;
    var_208 = 0UL;
    var_223 = (unsigned char)'\x01';
    storemerge23 = (unsigned char)'\x01';
    storemerge = 4313552UL;
    var_38 = 0UL;
    r9_10 = var_29;
    r8_10 = var_30;
    rcx_15 = 4219884UL;
    local_sp_27 = var_26;
    var_105 = 0UL;
    if (var_28 == 0UL) {
        *(uint64_t *)(var_0 + (-408L)) = 4220662UL;
        indirect_placeholder_56(var_11, var_29, var_30);
        abort();
    }
    var_33 = (uint32_t *)(var_0 + (-76L));
    *var_33 = 2U;
    *var_33 = ((*(uint32_t *)4338400UL == 1U) ? 3078U : 2U);
    var_34 = (uint64_t *)(var_0 + (-88L));
    *var_34 = 0UL;
    var_35 = (uint64_t *)(var_0 + (-144L));
    var_36 = (uint64_t *)(var_0 + (-152L));
    var_37 = (unsigned char *)(var_0 + (-153L));
    while (1U)
        {
            var_39 = *var_14;
            var_40 = helper_cc_compute_c_wrapper(var_38 - var_39, var_39, var_1, 17U);
            rcx_16 = rcx_15;
            r8_11 = r8_10;
            local_sp_28 = local_sp_27;
            r9_12 = r9_10;
            rcx_17 = rcx_15;
            local_sp_32 = local_sp_27;
            r9_11 = r9_10;
            r8_12 = r8_10;
            if (var_40 != 0UL) {
                loop_state_var = 2U;
                break;
            }
            var_41 = *var_13 + (*var_34 * 96UL);
            if (*(unsigned char *)(var_41 + 52UL) == '\x01') {
                var_97 = *var_34 + 1UL;
                *var_34 = var_97;
                var_38 = var_97;
                r9_10 = r9_12;
                r8_10 = r8_12;
                rcx_15 = rcx_17;
                local_sp_27 = local_sp_32;
                continue;
            }
            var_42 = *(uint64_t *)var_41;
            var_43 = local_sp_27 + (-8L);
            *(uint64_t *)var_43 = 4220786UL;
            indirect_placeholder();
            *var_35 = var_42;
            var_44 = helper_cc_compute_c_wrapper(*var_22 - var_42, var_42, var_1, 17U);
            local_sp_26 = var_43;
            if (var_44 == 0UL) {
                *var_22 = *var_35;
            }
            *(uint32_t *)((*var_13 + (*var_34 * 96UL)) + 68UL) = 4294967295U;
            if (*(uint32_t *)4338400UL == 1U) {
                *(uint64_t *)(local_sp_27 + (-16L)) = 4220910UL;
                var_45 = indirect_placeholder_6();
                *var_36 = var_45;
                *var_37 = *(unsigned char *)(var_45 + *(uint64_t *)(*var_13 + (*var_34 * 96UL)));
                var_46 = *(uint64_t *)(*var_13 + (*var_34 * 96UL));
                *(uint64_t *)(local_sp_27 + (-24L)) = 4221010UL;
                var_47 = indirect_placeholder_5(var_46);
                var_48 = *var_13 + (*var_34 * 96UL);
                *(uint64_t *)(var_48 + 80UL) = (var_47 - *(uint64_t *)var_48);
                *(unsigned char *)(*var_36 + *(uint64_t *)(*var_13 + (*var_34 * 96UL))) = (unsigned char)'\x00';
                if (*var_36 == 0UL) {
                    _pre_phi620 = *var_13 + (*var_34 * 96UL);
                } else {
                    var_49 = *var_13 + (*var_34 * 96UL);
                    _pre_phi620 = var_49;
                    storemerge = *(uint64_t *)var_49;
                }
                var_50 = *var_12;
                var_51 = local_sp_27 + (-32L);
                *(uint64_t *)var_51 = 4221234UL;
                indirect_placeholder();
                *(uint32_t *)(_pre_phi620 + 72UL) = var_50;
                *(unsigned char *)(*(uint64_t *)(*var_13 + (*var_34 * 96UL)) + *var_36) = *var_37;
                var_52 = *var_13 + (*var_34 * 96UL);
                var_53 = *(uint32_t *)(var_52 + 72UL);
                _pre_phi624 = var_52;
                local_sp_26 = var_51;
                if ((int)var_53 <= (int)4294967295U) {
                    var_54 = (uint64_t)var_53;
                    *(uint64_t *)(local_sp_27 + (-40L)) = 4221335UL;
                    indirect_placeholder();
                    if (*(uint32_t *)var_54 == 28U) {
                        var_63 = local_sp_27 + (-48L);
                        *(uint64_t *)var_63 = 4221450UL;
                        var_64 = indirect_placeholder_98(0UL, storemerge, 4313596UL, 0UL, 0UL, r9_10, r8_10);
                        r9_9 = var_64.field_1;
                        r8_9 = var_64.field_2;
                        rcx_14 = var_64.field_0;
                        local_sp_25 = var_63;
                        loop_state_var = 0U;
                        break;
                    }
                    var_55 = *(uint64_t *)(*var_13 + (*var_34 * 96UL));
                    *(uint64_t *)(local_sp_27 + (-48L)) = 4221388UL;
                    var_56 = indirect_placeholder_97(4UL, var_55);
                    var_57 = var_56.field_0;
                    var_58 = var_56.field_1;
                    var_59 = var_56.field_2;
                    *(uint64_t *)(local_sp_27 + (-56L)) = 4221396UL;
                    indirect_placeholder();
                    var_60 = (uint64_t)*(uint32_t *)var_57;
                    var_61 = local_sp_27 + (-64L);
                    *(uint64_t *)var_61 = 4221423UL;
                    var_62 = indirect_placeholder_96(0UL, var_57, 4313560UL, 0UL, var_60, var_58, var_59);
                    r9_9 = var_62.field_1;
                    r8_9 = var_62.field_2;
                    rcx_14 = var_62.field_0;
                    local_sp_25 = var_61;
                    loop_state_var = 0U;
                    break;
                }
            }
            _pre_phi624 = *var_13 + (*var_34 * 96UL);
            var_65 = *(uint64_t *)_pre_phi624;
            var_66 = *var_12;
            *(uint64_t *)(local_sp_26 + (-8L)) = 4221542UL;
            indirect_placeholder();
            *(uint32_t *)(_pre_phi624 + 68UL) = var_66;
            var_67 = *var_13 + (*var_34 * 96UL);
            rcx_17 = var_65;
            if ((int)*(uint32_t *)(var_67 + 68UL) > (int)4294967295U) {
                var_90 = *var_32;
                var_91 = local_sp_26 + (-16L);
                *(uint64_t *)var_91 = 4221862UL;
                var_92 = indirect_placeholder_95(var_90, var_67);
                local_sp_32 = var_91;
                if (var_92.field_0 != 0UL) {
                    var_95 = var_92.field_1;
                    var_96 = var_92.field_2;
                    *(uint64_t *)(local_sp_26 + (-24L)) = 4221872UL;
                    indirect_placeholder_56(var_11, var_95, var_96);
                    abort();
                }
                var_93 = var_92.field_2;
                var_94 = var_92.field_1;
                *var_17 = (unsigned char)'\x01';
                r9_12 = var_94;
                r8_12 = var_93;
            } else {
                var_68 = *(uint32_t *)(var_67 + 56UL);
                var_69 = (uint64_t)var_68;
                if ((uint64_t)(var_68 + 1U) == 0UL) {
                    *var_18 = (unsigned char)'\x01';
                }
                var_70 = local_sp_26 + (-16L);
                *(uint64_t *)var_70 = 4221633UL;
                indirect_placeholder();
                var_71 = *(uint32_t *)var_69;
                var_72 = (uint64_t)var_71;
                local_sp_24 = var_70;
                if ((uint64_t)(var_71 + (-28)) != 0UL) {
                    loop_state_var = 1U;
                    break;
                }
                var_73 = local_sp_26 + (-24L);
                *(uint64_t *)var_73 = 4221645UL;
                indirect_placeholder();
                var_74 = *(uint32_t *)var_72;
                var_75 = (uint64_t)var_74;
                local_sp_24 = var_73;
                if ((uint64_t)(var_74 + (-12)) != 0UL) {
                    loop_state_var = 1U;
                    break;
                }
                var_76 = local_sp_26 + (-32L);
                *(uint64_t *)var_76 = 4221691UL;
                indirect_placeholder();
                var_77 = *(uint32_t *)var_75;
                var_78 = (uint64_t)var_77;
                var_79 = *var_13 + (*var_34 * 96UL);
                rcx_17 = var_78;
                local_sp_32 = var_76;
                if ((uint64_t)(var_77 - *(uint32_t *)(var_79 + 60UL)) == 0UL) {
                    var_80 = *(uint64_t *)var_79;
                    *(uint64_t *)(local_sp_26 + (-40L)) = 4221780UL;
                    var_81 = indirect_placeholder_93(4UL, var_80);
                    var_82 = var_81.field_0;
                    var_83 = var_81.field_1;
                    var_84 = var_81.field_2;
                    *(uint64_t *)(local_sp_26 + (-48L)) = 4221788UL;
                    indirect_placeholder();
                    var_85 = (uint64_t)*(uint32_t *)var_82;
                    var_86 = local_sp_26 + (-56L);
                    *(uint64_t *)var_86 = 4221815UL;
                    var_87 = indirect_placeholder_92(0UL, var_82, 4313624UL, 0UL, var_85, var_83, var_84);
                    r9_12 = var_87.field_1;
                    r8_12 = var_87.field_2;
                    rcx_17 = var_87.field_0;
                    local_sp_32 = var_86;
                }
            }
        }
    switch (loop_state_var) {
      case 0U:
        {
            *var_19 = (unsigned char)'\x01';
            r9_11 = r9_9;
            r8_11 = r8_9;
            rcx_16 = rcx_14;
            local_sp_28 = local_sp_25;
        }
        break;
      case 1U:
        {
            *var_20 = (unsigned char)'\x01';
            var_88 = local_sp_24 + (-8L);
            *(uint64_t *)var_88 = 4221681UL;
            var_89 = indirect_placeholder_94(0UL, var_65, 4313596UL, 0UL, 0UL, r9_10, r8_10);
            r9_11 = var_89.field_1;
            r8_11 = var_89.field_2;
            rcx_16 = var_89.field_0;
            local_sp_28 = var_88;
        }
        break;
      case 2U:
        {
            r9_1 = r9_11;
            r8_1 = r8_11;
            rcx_1 = rcx_16;
            local_sp_29 = local_sp_28;
            if (*var_20 == '\x00') {
                var_99 = *(uint32_t **)var_31;
                *(uint64_t *)(local_sp_28 + (-8L)) = 4221942UL;
                indirect_placeholder();
                *(uint64_t *)(local_sp_28 + (-16L)) = 4221947UL;
                indirect_placeholder();
                *var_99 = 0U;
                return rax_1;
            }
            rax_1 = 0UL;
            if (*var_19 != '\x00') {
                var_98 = (*(uint32_t *)4338400UL == 2U);
                if (!var_98) {
                    if (*var_18 != '\x00') {
                        var_99 = *(uint32_t **)var_31;
                        *(uint64_t *)(local_sp_28 + (-8L)) = 4221942UL;
                        indirect_placeholder();
                        *(uint64_t *)(local_sp_28 + (-16L)) = 4221947UL;
                        indirect_placeholder();
                        *var_99 = 0U;
                        return rax_1;
                    }
                }
                if (!var_98) {
                    if (*var_17 == '\x01') {
                        return rax_1;
                    }
                }
                var_100 = *var_13 + ((*var_14 * 96UL) + (-96L));
                var_101 = var_0 + (-200L);
                *(uint64_t *)var_101 = var_100;
                *var_34 = 0UL;
                var_102 = var_0 + (-360L);
                var_103 = (uint64_t *)var_102;
                var_104 = (uint64_t *)(var_0 + (-352L));
                while (1U)
                    {
                        var_106 = *var_14;
                        var_107 = helper_cc_compute_c_wrapper(var_105 - var_106, var_106, var_1, 17U);
                        local_sp_30 = local_sp_29;
                        local_sp_31 = local_sp_29;
                        if (var_107 != 0UL) {
                            var_126 = *var_22 + 17UL;
                            *var_22 = var_126;
                            var_127 = local_sp_29 + (-8L);
                            *(uint64_t *)var_127 = 4222552UL;
                            var_128 = indirect_placeholder_5(var_126);
                            var_129 = (uint64_t *)(var_0 + (-56L));
                            *var_129 = var_128;
                            var_130 = (uint64_t *)(var_0 + (-208L));
                            var_131 = var_0 + (-216L);
                            var_132 = (uint64_t *)var_131;
                            var_133 = (uint64_t *)(var_0 + (-112L));
                            var_134 = (uint32_t *)(var_0 + (-100L));
                            var_135 = (uint32_t *)(var_0 + (-160L));
                            var_136 = (uint64_t *)(var_0 + (-168L));
                            var_137 = var_0 + (-176L);
                            var_138 = (uint64_t *)var_137;
                            var_139 = (uint64_t *)(var_0 + (-120L));
                            var_140 = (uint64_t *)(var_0 + (-96L));
                            var_141 = (uint32_t *)(var_0 + (-124L));
                            var_142 = (unsigned char *)(var_0 + (-177L));
                            var_143 = (unsigned char *)(var_0 + (-178L));
                            var_144 = (uint64_t *)(var_0 + (-192L));
                            var_145 = (uint32_t *)(var_0 + (-292L));
                            local_sp_8 = var_127;
                            while (1U)
                                {
                                    r8_2_ph = r8_1;
                                    r9_2_ph = r9_1;
                                    local_sp_9_ph = local_sp_8;
                                    state_0x8549_1_ph = state_0x8549_0;
                                    rcx_2_ph = rcx_1;
                                    if (*(uint32_t *)4338400UL != 1U) {
                                        var_146 = *var_32;
                                        var_147 = local_sp_8 + (-8L);
                                        *(uint64_t *)var_147 = 4222601UL;
                                        var_148 = indirect_placeholder_5(var_146);
                                        local_sp_9_ph = var_147;
                                        if (*(unsigned char *)4338736UL != '\x01' & var_148 != 0UL) {
                                            *(uint64_t *)(local_sp_8 + (-16L)) = 4222635UL;
                                            indirect_placeholder_89(0UL, rcx_1, 4313494UL, 0UL, 0UL, r9_1, r8_1);
                                            loop_state_var = 1U;
                                            break;
                                        }
                                    }
                                    local_sp_9_ph543 = local_sp_9_ph;
                                    while (1U)
                                        {
                                            r9_0 = r9_2_ph;
                                            rcx_2 = rcx_2_ph;
                                            r8_2 = r8_2_ph;
                                            state_0x8549_1 = state_0x8549_1_ph;
                                            local_sp_9 = local_sp_9_ph543;
                                            r9_3 = r9_2_ph;
                                            while (1U)
                                                {
                                                    var_149 = *var_24;
                                                    var_150 = *var_23;
                                                    var_180 = var_150;
                                                    r8_3 = r8_2;
                                                    state_0x8549_2 = state_0x8549_1;
                                                    rcx_2 = 0UL;
                                                    state_0x8549_3 = state_0x8549_1;
                                                    rcx_3 = rcx_2;
                                                    local_sp_23 = local_sp_9;
                                                    var_181 = var_149;
                                                    local_sp_10 = local_sp_9;
                                                    local_sp_22 = local_sp_9;
                                                    if (var_149 <= var_150) {
                                                        loop_state_var = 0U;
                                                        break;
                                                    }
                                                    var_151 = *(uint32_t *)4338744UL;
                                                    var_153 = var_151;
                                                    if (var_151 != 0U) {
                                                        if (*var_21 == '\x00') {
                                                            var_152 = local_sp_9 + (-8L);
                                                            *(uint64_t *)var_152 = 4222675UL;
                                                            indirect_placeholder();
                                                            var_153 = *(uint32_t *)4338744UL;
                                                            local_sp_22 = var_152;
                                                        }
                                                        var_154 = local_sp_22 + (-8L);
                                                        *(uint64_t *)var_154 = 4222693UL;
                                                        indirect_placeholder();
                                                        local_sp_6 = var_154;
                                                        if (var_153 == 0U) {
                                                            local_sp_7 = local_sp_6;
                                                        } else {
                                                            var_155 = (uint64_t)var_153;
                                                            var_156 = local_sp_22 + (-16L);
                                                            *(uint64_t *)var_156 = 4222702UL;
                                                            indirect_placeholder();
                                                            local_sp_6 = var_156;
                                                            local_sp_7 = var_156;
                                                            storemerge21 = (unsigned char)'\x01';
                                                            if (*(uint32_t *)var_155 == 1U) {
                                                                local_sp_7 = local_sp_6;
                                                            }
                                                        }
                                                        var_157 = storemerge21 & '\x01';
                                                        *var_21 = var_157;
                                                        local_sp_23 = local_sp_7;
                                                        if (var_157 == '\x00') {
                                                            var_158 = helper_cvttsd2sq_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), *var_15, state_0x8549_1, var_6);
                                                            var_159 = var_158.field_0;
                                                            var_160 = var_158.field_1;
                                                            *var_132 = var_159;
                                                            var_161 = helper_cvtsq2sd_wrapper((struct type_5 *)(0UL), (struct type_7 *)(840UL), var_159, var_160, var_7, var_8, var_9);
                                                            var_162 = helper_subsd_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(840UL), var_161.field_0, *var_15, var_161.field_1, var_6, var_7, var_8, var_9, var_10);
                                                            var_163 = helper_mulsd_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(840UL), var_162.field_0, *(uint64_t *)4315296UL, var_162.field_1, var_6, var_7, var_8, var_9, var_10);
                                                            var_164 = helper_cvttsd2sq_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_163.field_0, var_163.field_1, var_6);
                                                            var_165 = var_164.field_1;
                                                            *var_130 = var_164.field_0;
                                                            state_0x8549_3 = var_165;
                                                        } else {
                                                            *var_130 = 0UL;
                                                            *var_132 = 0UL;
                                                        }
                                                    }
                                                    *var_133 = var_102;
                                                    *var_134 = 16U;
                                                    state_0x8549_1_ph = state_0x8549_3;
                                                    state_0x8549_1 = state_0x8549_3;
                                                    state_0x8549_2 = state_0x8549_3;
                                                    while (var_166 != 0U)
                                                        {
                                                            var_167 = *var_133;
                                                            *var_133 = (var_167 + 8UL);
                                                            *(uint64_t *)var_167 = 0UL;
                                                            var_168 = *var_134 + (-1);
                                                            *var_134 = var_168;
                                                            var_166 = var_168;
                                                        }
                                                    var_169 = *var_12;
                                                    var_170 = (uint64_t *)(((((uint64_t)var_169 >> 6UL) << 3UL) + var_11) + (-352L));
                                                    *var_170 = (*var_170 | (1UL << (uint64_t)(var_169 & 63U)));
                                                    if (*(unsigned char *)4338739UL == '\x00') {
                                                        *var_103 = (*var_103 | 2UL);
                                                    }
                                                    spec_select = (*(uint32_t *)4338744UL == 0U) ? 0UL : var_131;
                                                    var_171 = *var_12;
                                                    var_172 = (uint64_t)(((int)var_171 < (int)1U) ? 2U : (var_171 + 1U));
                                                    var_173 = local_sp_23 + (-8L);
                                                    *(uint64_t *)var_173 = 4223036UL;
                                                    var_174 = indirect_placeholder_11(0UL, 0UL, var_172, var_102);
                                                    var_175 = (uint32_t)var_174;
                                                    *var_135 = var_175;
                                                    r8_0 = spec_select;
                                                    local_sp_5 = var_173;
                                                    r8_2 = spec_select;
                                                    local_sp_9 = var_173;
                                                    switch_state_var = 0;
                                                    switch (var_175) {
                                                      case 0U:
                                                        {
                                                            continue;
                                                        }
                                                        break;
                                                      case 4294967295U:
                                                        {
                                                            loop_state_var = 1U;
                                                            switch_state_var = 1;
                                                            break;
                                                        }
                                                        break;
                                                      default:
                                                        {
                                                            loop_state_var = 2U;
                                                            switch_state_var = 1;
                                                            break;
                                                        }
                                                        break;
                                                    }
                                                    if (switch_state_var)
                                                        break;
                                                }
                                            switch_state_var = 0;
                                            switch (loop_state_var) {
                                              case 0U:
                                                {
                                                    loop_state_var = 0U;
                                                    switch_state_var = 1;
                                                    break;
                                                }
                                                break;
                                              case 2U:
                                              case 1U:
                                                {
                                                    switch_state_var = 0;
                                                    switch (loop_state_var) {
                                                      case 1U:
                                                        {
                                                            *(uint64_t *)(local_sp_23 + (-16L)) = 4223067UL;
                                                            indirect_placeholder();
                                                            var_176 = (uint64_t)*(uint32_t *)var_174;
                                                            var_177 = local_sp_23 + (-24L);
                                                            *(uint64_t *)var_177 = 4223091UL;
                                                            var_178 = indirect_placeholder_88(0UL, 0UL, 4313656UL, 1UL, var_176, r9_2_ph, spec_select);
                                                            r9_0 = var_178.field_1;
                                                            r8_0 = var_178.field_2;
                                                            rcx_0 = var_178.field_0;
                                                            local_sp_5 = var_177;
                                                        }
                                                        break;
                                                      case 2U:
                                                        {
                                                            r8_3 = r8_0;
                                                            r8_2_ph = r8_0;
                                                            r9_2_ph = r9_0;
                                                            rcx_3 = rcx_0;
                                                            rcx_2_ph = rcx_0;
                                                            r9_3 = r9_0;
                                                            local_sp_10 = local_sp_5;
                                                            if ((*var_103 & 2UL) != 0UL) {
                                                                loop_state_var = 1U;
                                                                switch_state_var = 1;
                                                                break;
                                                            }
                                                            var_179 = local_sp_5 + (-8L);
                                                            *(uint64_t *)var_179 = 4223116UL;
                                                            indirect_placeholder();
                                                            local_sp_9_ph543 = var_179;
                                                            continue;
                                                        }
                                                        break;
                                                    }
                                                    if (switch_state_var)
                                                        break;
                                                }
                                                break;
                                            }
                                            if (switch_state_var)
                                                break;
                                        }
                                    switch_state_var = 0;
                                    switch (loop_state_var) {
                                      case 1U:
                                        {
                                            var_180 = *var_23;
                                            var_181 = *var_24;
                                        }
                                        break;
                                      case 0U:
                                        {
                                            r8_4 = r8_3;
                                            rcx_4 = rcx_3;
                                            r9_1_be = r9_3;
                                            r9_4 = r9_3;
                                            r8_1_be = r8_3;
                                            state_0x8549_0 = state_0x8549_2;
                                            local_sp_11 = local_sp_10;
                                            if (var_181 > var_180) {
                                                var_198 = *var_23 + *var_129;
                                                *var_136 = var_198;
                                                *var_138 = var_198;
                                                *var_23 = ((*var_23 + (uint64_t)*(uint32_t *)(var_198 + 12UL)) + 16UL);
                                                var_199 = *var_138;
                                                r9_1_be = r9_4;
                                                rcx_6 = rcx_4;
                                                rcx_5 = rcx_4;
                                                r8_1_be = r8_4;
                                                var_207 = var_199;
                                                local_sp_12 = local_sp_11;
                                                r9_5 = r9_4;
                                                r8_5 = r8_4;
                                                r9_7 = r9_4;
                                                r8_7 = r8_4;
                                                if ((uint32_t)((uint16_t)*(uint32_t *)(var_199 + 4UL) & (unsigned short)1024U) != 0U & *(uint32_t *)(var_199 + 12UL) != 0U) {
                                                    *var_34 = 0UL;
                                                    var_201 = *var_14;
                                                    var_202 = helper_cc_compute_c_wrapper(var_200 - var_201, var_201, var_1, 17U);
                                                    rcx_6 = rcx_5;
                                                    while (var_202 != 0UL)
                                                        {
                                                            var_203 = **(uint32_t **)var_137;
                                                            var_204 = (uint64_t)var_203;
                                                            var_205 = *var_34;
                                                            rcx_5 = var_204;
                                                            if ((uint64_t)(var_203 - *(uint32_t *)((*var_13 + (var_205 * 96UL)) + 72UL)) != 0UL) {
                                                                var_206 = var_205 + 1UL;
                                                                *var_34 = var_206;
                                                                var_200 = var_206;
                                                                var_201 = *var_14;
                                                                var_202 = helper_cc_compute_c_wrapper(var_200 - var_201, var_201, var_1, 17U);
                                                                rcx_6 = rcx_5;
                                                                continue;
                                                            }
                                                            *(uint64_t *)(local_sp_11 + (-8L)) = 4223473UL;
                                                            indirect_placeholder();
                                                            *(uint64_t *)(local_sp_11 + (-16L)) = 4223498UL;
                                                            indirect_placeholder_86(0UL, var_204, 4313728UL, 0UL, 0UL, r9_4, r8_4);
                                                            *(uint64_t *)(local_sp_11 + (-24L)) = 4223503UL;
                                                            indirect_placeholder();
                                                            llvm_trap();
                                                            abort();
                                                        }
                                                    var_207 = *var_138;
                                                }
                                                rcx_7 = rcx_6;
                                                rcx_11 = rcx_6;
                                                if (*(uint32_t *)(var_207 + 12UL) == 0U) {
                                                    *var_145 = **(uint32_t **)var_137;
                                                    var_265 = *var_32;
                                                    var_266 = local_sp_11 + (-8L);
                                                    *(uint64_t *)var_266 = 4224455UL;
                                                    var_267 = indirect_placeholder_7(var_265, var_102);
                                                    *var_140 = var_267;
                                                    local_sp_18 = var_266;
                                                } else {
                                                    *var_139 = 0UL;
                                                    var_209 = *var_14;
                                                    var_210 = helper_cc_compute_c_wrapper(var_208 - var_209, var_209, var_1, 17U);
                                                    rcx_8 = rcx_7;
                                                    local_sp_13 = local_sp_12;
                                                    rcx_13 = rcx_7;
                                                    local_sp_21 = local_sp_12;
                                                    while (var_210 != 0UL)
                                                        {
                                                            var_211 = *var_139;
                                                            var_212 = *var_13 + (var_211 * 96UL);
                                                            var_216 = var_211;
                                                            if ((uint64_t)(*(uint32_t *)(var_212 + 72UL) - **(uint32_t **)var_137) != 0UL) {
                                                                var_213 = *(uint64_t *)var_212;
                                                                var_214 = (uint64_t)((uint32_t)*var_138 + 16U);
                                                                var_215 = local_sp_12 + (-8L);
                                                                *(uint64_t *)var_215 = 4223707UL;
                                                                indirect_placeholder();
                                                                rcx_8 = var_213;
                                                                local_sp_13 = var_215;
                                                                rcx_13 = var_213;
                                                                local_sp_21 = var_215;
                                                                if (var_214 != 0UL) {
                                                                    break;
                                                                }
                                                                var_216 = *var_139;
                                                            }
                                                            var_217 = var_216 + 1UL;
                                                            *var_139 = var_217;
                                                            var_208 = var_217;
                                                            rcx_7 = rcx_13;
                                                            local_sp_12 = local_sp_21;
                                                            var_209 = *var_14;
                                                            var_210 = helper_cc_compute_c_wrapper(var_208 - var_209, var_209, var_1, 17U);
                                                            rcx_8 = rcx_7;
                                                            local_sp_13 = local_sp_12;
                                                            rcx_13 = rcx_7;
                                                            local_sp_21 = local_sp_12;
                                                        }
                                                    var_218 = *var_139;
                                                    rcx_9 = rcx_8;
                                                    local_sp_14 = local_sp_13;
                                                    rcx_1_be = rcx_8;
                                                    local_sp_8_be = local_sp_13;
                                                    if (var_218 != *var_14) {
                                                        r9_1 = r9_1_be;
                                                        r8_1 = r8_1_be;
                                                        rcx_1 = rcx_1_be;
                                                        local_sp_8 = local_sp_8_be;
                                                        continue;
                                                    }
                                                    *var_140 = (*var_13 + (var_218 * 96UL));
                                                    *var_141 = 4294967295U;
                                                    var_219 = (unsigned char)(*(uint32_t *)(*var_138 + 4UL) >> 9U) & '\x01';
                                                    *var_142 = var_219;
                                                    if (var_219 == '\x01') {
                                                        var_220 = *(uint64_t *)(*var_13 + (*var_139 * 96UL));
                                                        var_221 = *var_12;
                                                        var_222 = local_sp_13 + (-8L);
                                                        *(uint64_t *)var_222 = 4223886UL;
                                                        indirect_placeholder();
                                                        *var_141 = var_221;
                                                        var_223 = *var_142;
                                                        rcx_9 = var_220;
                                                        local_sp_14 = var_222;
                                                    }
                                                    var_224 = var_223 ^ '\x01';
                                                    var_225 = (uint64_t)var_224;
                                                    var_239 = var_223;
                                                    rcx_10 = rcx_9;
                                                    local_sp_15 = local_sp_14;
                                                    if (var_224 != '\x00' & (int)*var_141 <= (int)4294967295U) {
                                                        var_226 = local_sp_14 + (-8L);
                                                        *(uint64_t *)var_226 = 4223922UL;
                                                        indirect_placeholder();
                                                        var_227 = *(uint32_t *)var_225;
                                                        var_228 = (uint64_t)var_227;
                                                        local_sp_2 = var_226;
                                                        if ((uint64_t)(var_227 + (-28)) != 0UL) {
                                                            loop_state_var = 0U;
                                                            switch_state_var = 1;
                                                            break;
                                                        }
                                                        var_229 = local_sp_14 + (-16L);
                                                        *(uint64_t *)var_229 = 4223934UL;
                                                        indirect_placeholder();
                                                        local_sp_2 = var_229;
                                                        if (*(uint32_t *)var_228 != 12U) {
                                                            loop_state_var = 0U;
                                                            switch_state_var = 1;
                                                            break;
                                                        }
                                                        var_230 = *(uint64_t *)(*var_13 + (*var_139 * 96UL));
                                                        *(uint64_t *)(local_sp_14 + (-24L)) = 4224045UL;
                                                        var_231 = indirect_placeholder_84(4UL, var_230);
                                                        var_232 = var_231.field_0;
                                                        var_233 = var_231.field_1;
                                                        var_234 = var_231.field_2;
                                                        *(uint64_t *)(local_sp_14 + (-32L)) = 4224053UL;
                                                        indirect_placeholder();
                                                        var_235 = (uint64_t)*(uint32_t *)var_232;
                                                        var_236 = local_sp_14 + (-40L);
                                                        *(uint64_t *)var_236 = 4224080UL;
                                                        var_237 = indirect_placeholder_83(0UL, var_232, 4313624UL, 0UL, var_235, var_233, var_234);
                                                        var_239 = *var_142;
                                                        r9_5 = var_237.field_1;
                                                        r8_5 = var_237.field_2;
                                                        rcx_10 = var_237.field_0;
                                                        local_sp_15 = var_236;
                                                    }
                                                    r9_6 = r9_5;
                                                    r9_1_be = r9_5;
                                                    r8_1_be = r8_5;
                                                    rcx_1_be = rcx_10;
                                                    local_sp_16 = local_sp_15;
                                                    r8_6 = r8_5;
                                                    local_sp_17 = local_sp_15;
                                                    rcx_11 = rcx_10;
                                                    if (var_239 == '\x01') {
                                                        storemerge23 = (unsigned char)'\x00';
                                                    } else {
                                                        var_240 = *(uint32_t *)(*var_140 + 68UL);
                                                        if ((int)var_240 >= (int)0U & (uint64_t)(*var_141 - var_240) == 0UL) {
                                                            storemerge23 = (unsigned char)'\x00';
                                                        }
                                                    }
                                                    var_241 = storemerge23 & '\x01';
                                                    *var_143 = var_241;
                                                    if (var_241 != '\x00') {
                                                        var_242 = (uint32_t *)(*var_140 + 68UL);
                                                        _pre_phi616 = var_242;
                                                        if ((int)*var_242 < (int)0U) {
                                                            *(uint64_t *)(local_sp_15 + (-8L)) = 4224188UL;
                                                            indirect_placeholder();
                                                            var_243 = *var_140;
                                                            var_244 = *var_32;
                                                            var_245 = local_sp_15 + (-16L);
                                                            *(uint64_t *)var_245 = 4224207UL;
                                                            indirect_placeholder_7(var_244, var_243);
                                                            _pre_phi616 = (uint32_t *)(*var_140 + 68UL);
                                                            local_sp_16 = var_245;
                                                        }
                                                        *_pre_phi616 = *var_141;
                                                        local_sp_8_be = local_sp_16;
                                                        if (*var_141 != 4294967295U) {
                                                            r9_1 = r9_1_be;
                                                            r8_1 = r8_1_be;
                                                            rcx_1 = rcx_1_be;
                                                            local_sp_8 = local_sp_8_be;
                                                            continue;
                                                        }
                                                        var_246 = *var_140;
                                                        var_247 = *var_32;
                                                        var_248 = local_sp_16 + (-8L);
                                                        *(uint64_t *)var_248 = 4224246UL;
                                                        var_249 = indirect_placeholder_7(var_247, var_246);
                                                        *var_144 = var_249;
                                                        var_251 = var_249;
                                                        local_sp_0 = var_248;
                                                        local_sp_1 = var_248;
                                                        if (var_249 != 0UL & var_249 != *var_140) {
                                                            if (*(uint32_t *)4338400UL == 1U) {
                                                                var_250 = local_sp_16 + (-16L);
                                                                *(uint64_t *)var_250 = 4224307UL;
                                                                indirect_placeholder_3(var_249, 0UL);
                                                                var_251 = *var_144;
                                                                local_sp_0 = var_250;
                                                            }
                                                            *(uint32_t *)(var_251 + 68UL) = 4294967295U;
                                                            var_252 = *var_144;
                                                            *(uint64_t *)(local_sp_0 + (-8L)) = 4224336UL;
                                                            var_253 = indirect_placeholder_5(var_252);
                                                            var_254 = (uint64_t)*(uint32_t *)(*var_144 + 56UL);
                                                            var_255 = local_sp_0 + (-16L);
                                                            *(uint64_t *)var_255 = 4224359UL;
                                                            indirect_placeholder_3(var_254, var_253);
                                                            local_sp_1 = var_255;
                                                        }
                                                        var_256 = *var_140;
                                                        var_257 = *var_32;
                                                        var_258 = local_sp_1 + (-8L);
                                                        *(uint64_t *)var_258 = 4224378UL;
                                                        var_259 = indirect_placeholder_82(var_257, var_256);
                                                        var_260 = var_259.field_0;
                                                        var_261 = var_259.field_1;
                                                        var_262 = var_259.field_2;
                                                        r9_6 = var_261;
                                                        r8_6 = var_262;
                                                        local_sp_17 = var_258;
                                                        if (var_260 == 0UL) {
                                                            *(uint64_t *)(local_sp_1 + (-16L)) = 4224388UL;
                                                            indirect_placeholder_56(var_11, var_261, var_262);
                                                            abort();
                                                        }
                                                    }
                                                    r9_7 = r9_6;
                                                    r8_7 = r8_6;
                                                    local_sp_18 = local_sp_17;
                                                    if (*(uint32_t *)4338400UL == 1U) {
                                                        var_263 = *var_140;
                                                        var_264 = local_sp_17 + (-8L);
                                                        *(uint64_t *)var_264 = 4224416UL;
                                                        indirect_placeholder_3(var_263, 0UL);
                                                        local_sp_18 = var_264;
                                                    }
                                                }
                                            } else {
                                                var_182 = *var_22;
                                                var_183 = *var_129;
                                                var_184 = (uint64_t)*var_12;
                                                var_185 = local_sp_10 + (-8L);
                                                *(uint64_t *)var_185 = 4223171UL;
                                                var_186 = indirect_placeholder_7(var_182, var_184);
                                                *var_24 = var_186;
                                                *var_23 = 0UL;
                                                rcx_4 = var_183;
                                                rax_0 = var_186;
                                                local_sp_3 = var_185;
                                                local_sp_4 = var_185;
                                                rcx_1_be = var_183;
                                                switch (*var_24) {
                                                  case 0UL:
                                                    {
                                                        var_189 = *var_16;
                                                        var_190 = (uint64_t)var_189;
                                                        *var_16 = (var_189 + (-1));
                                                        rax_0 = var_190;
                                                        local_sp_4 = local_sp_3;
                                                        if (var_189 != 0U) {
                                                            *var_24 = 0UL;
                                                            var_191 = *var_22 << 1UL;
                                                            *var_22 = var_191;
                                                            var_192 = *var_129;
                                                            var_193 = local_sp_3 + (-8L);
                                                            *(uint64_t *)var_193 = 4223253UL;
                                                            var_194 = indirect_placeholder_7(var_192, var_191);
                                                            *var_129 = var_194;
                                                            local_sp_8_be = var_193;
                                                            r9_1 = r9_1_be;
                                                            r8_1 = r8_1_be;
                                                            rcx_1 = rcx_1_be;
                                                            local_sp_8 = local_sp_8_be;
                                                            continue;
                                                        }
                                                    }
                                                    break;
                                                  case 18446744073709551615UL:
                                                    {
                                                        var_187 = local_sp_10 + (-16L);
                                                        *(uint64_t *)var_187 = 4223202UL;
                                                        indirect_placeholder();
                                                        var_188 = *(uint32_t *)var_186;
                                                        local_sp_3 = var_187;
                                                        rax_0 = (uint64_t)var_188;
                                                        local_sp_4 = var_187;
                                                        var_189 = *var_16;
                                                        var_190 = (uint64_t)var_189;
                                                        *var_16 = (var_189 + (-1));
                                                        rax_0 = var_190;
                                                        local_sp_4 = local_sp_3;
                                                        if ((uint64_t)(var_188 + (-22)) != 0UL & var_189 != 0U) {
                                                            *var_24 = 0UL;
                                                            var_191 = *var_22 << 1UL;
                                                            *var_22 = var_191;
                                                            var_192 = *var_129;
                                                            var_193 = local_sp_3 + (-8L);
                                                            *(uint64_t *)var_193 = 4223253UL;
                                                            var_194 = indirect_placeholder_7(var_192, var_191);
                                                            *var_129 = var_194;
                                                            local_sp_8_be = var_193;
                                                            r9_1 = r9_1_be;
                                                            r8_1 = r8_1_be;
                                                            rcx_1 = rcx_1_be;
                                                            local_sp_8 = local_sp_8_be;
                                                            continue;
                                                        }
                                                    }
                                                    break;
                                                  default:
                                                    {
                                                        *(uint64_t *)(local_sp_4 + (-8L)) = 4223281UL;
                                                        indirect_placeholder();
                                                        var_195 = (uint64_t)*(uint32_t *)rax_0;
                                                        var_196 = local_sp_4 + (-16L);
                                                        *(uint64_t *)var_196 = 4223305UL;
                                                        var_197 = indirect_placeholder_87(0UL, var_183, 4313700UL, 1UL, var_195, r9_3, r8_3);
                                                        r9_4 = var_197.field_1;
                                                        r8_4 = var_197.field_2;
                                                        rcx_4 = var_197.field_0;
                                                        local_sp_11 = var_196;
                                                    }
                                                    break;
                                                }
                                            }
                                        }
                                        break;
                                    }
                                    if (switch_state_var)
                                        break;
                                }
                            switch_state_var = 0;
                            switch (loop_state_var) {
                              case 0U:
                                {
                                    *(uint64_t *)(local_sp_2 + (-8L)) = 4223966UL;
                                    indirect_placeholder_85(0UL, rcx_9, 4313596UL, 0UL, 0UL, r9_4, r8_4);
                                    var_238 = *(uint32_t **)var_31;
                                    *(uint64_t *)(local_sp_2 + (-16L)) = 4223978UL;
                                    indirect_placeholder();
                                    *(uint64_t *)(local_sp_2 + (-24L)) = 4223983UL;
                                    indirect_placeholder();
                                    *var_238 = 0U;
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              case 1U:
                                {
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                            }
                            if (switch_state_var)
                                break;
                        }
                        var_108 = *var_34;
                        var_109 = *var_13 + (var_108 * 96UL);
                        var_124 = var_108;
                        if (*(unsigned char *)(var_109 + 52UL) == '\x01') {
                            var_125 = var_124 + 1UL;
                            *var_34 = var_125;
                            var_105 = var_125;
                            local_sp_29 = local_sp_31;
                            continue;
                        }
                        if (*(uint32_t *)4338400UL != 1U) {
                            var_110 = *(uint64_t *)var_109;
                            var_111 = local_sp_29 + (-8L);
                            *(uint64_t *)var_111 = 4222246UL;
                            var_112 = indirect_placeholder_5(var_110);
                            local_sp_30 = var_111;
                            if (*(uint32_t *)(var_109 + 56UL) != 4294967295U & (uint64_t)(uint32_t)var_112 != 0UL) {
                                var_113 = *var_13 + (*var_34 * 96UL);
                                if (*(uint64_t *)(var_113 + 32UL) != *var_103) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                if (*(uint64_t *)(var_113 + 40UL) != *var_104) {
                                    loop_state_var = 0U;
                                    break;
                                }
                            }
                        }
                        var_121 = local_sp_29 + (-8L);
                        *(uint64_t *)var_121 = 4222148UL;
                        indirect_placeholder_3(var_109, 0UL);
                        local_sp_30 = var_121;
                    }
                switch (loop_state_var) {
                  case 0U:
                    {
                        *(uint64_t *)(local_sp_29 + (-16L)) = 4222384UL;
                        var_114 = indirect_placeholder_5(var_113);
                        *(uint64_t *)(local_sp_29 + (-24L)) = 4222397UL;
                        var_115 = indirect_placeholder_91(4UL, var_114);
                        var_116 = var_115.field_0;
                        var_117 = var_115.field_1;
                        var_118 = var_115.field_2;
                        *(uint64_t *)(local_sp_29 + (-32L)) = 4222405UL;
                        indirect_placeholder();
                        var_119 = (uint64_t)*(uint32_t *)var_116;
                        *(uint64_t *)(local_sp_29 + (-40L)) = 4222432UL;
                        indirect_placeholder_90(0UL, var_116, 4313640UL, 0UL, var_119, var_117, var_118);
                        var_120 = *(uint32_t **)var_31;
                        *(uint64_t *)(local_sp_29 + (-48L)) = 4222444UL;
                        indirect_placeholder();
                        *(uint64_t *)(local_sp_29 + (-56L)) = 4222449UL;
                        indirect_placeholder();
                        *var_120 = 0U;
                    }
                    break;
                  case 1U:
                    {
                        return rax_1;
                    }
                    break;
                }
            }
        }
        break;
    }
}
