typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_25_ret_type;
struct indirect_placeholder_24_ret_type;
struct indirect_placeholder_25_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_24_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_7(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_3(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_25_ret_type indirect_placeholder_25(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_24_ret_type indirect_placeholder_24(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
typedef _Bool bool;
uint64_t bb_pipe_bytes(uint64_t rcx, uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint32_t *var_5;
    uint64_t *var_6;
    uint64_t var_7;
    uint64_t *var_8;
    unsigned char *var_9;
    uint64_t var_10;
    uint64_t *var_11;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t *var_15;
    uint64_t *var_16;
    uint64_t local_sp_3;
    uint64_t *_pre_phi113;
    uint64_t var_37;
    uint64_t local_sp_0;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t local_sp_1;
    uint64_t var_30;
    uint64_t var_48;
    uint64_t local_sp_2;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_41;
    struct indirect_placeholder_25_ret_type var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_28;
    uint64_t *var_29;
    uint64_t *var_27;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_24;
    uint64_t *var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t *var_56;
    uint64_t local_sp_3_be;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_cc_src2();
    var_2 = init_rbp();
    var_3 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    var_4 = (uint64_t *)(var_0 + (-96L));
    *var_4 = rdi;
    var_5 = (uint32_t *)(var_0 + (-100L));
    *var_5 = (uint32_t)rsi;
    var_6 = (uint64_t *)(var_0 + (-112L));
    *var_6 = rdx;
    var_7 = var_0 + (-120L);
    *(uint64_t *)var_7 = rcx;
    var_8 = (uint64_t *)(var_0 + (-64L));
    *var_8 = 0UL;
    var_9 = (unsigned char *)(var_0 + (-65L));
    *var_9 = (unsigned char)'\x01';
    *(uint64_t *)(var_0 + (-128L)) = 4213356UL;
    var_10 = indirect_placeholder_5(1040UL);
    var_11 = (uint64_t *)(var_0 + (-40L));
    *var_11 = var_10;
    var_12 = (uint64_t *)(var_0 + (-32L));
    *var_12 = var_10;
    *(uint64_t *)(var_10 + 1024UL) = 0UL;
    *(uint64_t *)(*var_12 + 1032UL) = 0UL;
    var_13 = var_0 + (-136L);
    *(uint64_t *)var_13 = 4213408UL;
    var_14 = indirect_placeholder_5(1040UL);
    var_15 = (uint64_t *)(var_0 + (-48L));
    *var_15 = var_14;
    var_16 = (uint64_t *)(var_0 + (-80L));
    local_sp_3 = var_13;
    while (1U)
        {
            switch_state_var = 1;
            break;
        }
    *(uint64_t *)(local_sp_3 + (-16L)) = 4213775UL;
    indirect_placeholder();
    if (*var_16 != 18446744073709551615UL) {
        var_20 = *var_12;
        *var_15 = var_20;
        var_21 = var_20;
        var_22 = *var_8 - *(uint64_t *)(var_21 + 1024UL);
        var_23 = helper_cc_compute_c_wrapper(*var_6 - var_22, var_22, var_1, 17U);
        while (var_23 != 0UL)
            {
                *var_8 = (*var_8 - *(uint64_t *)(*var_15 + 1024UL));
                var_24 = *(uint64_t *)(*var_15 + 1032UL);
                *var_15 = var_24;
                var_21 = var_24;
                var_22 = *var_8 - *(uint64_t *)(var_21 + 1024UL);
                var_23 = helper_cc_compute_c_wrapper(*var_6 - var_22, var_22, var_1, 17U);
            }
        var_25 = *var_8;
        var_26 = *var_6;
        if (var_25 > var_26) {
            var_28 = var_25 - var_26;
            var_29 = (uint64_t *)(var_0 + (-56L));
            *var_29 = var_28;
            _pre_phi113 = var_29;
        } else {
            var_27 = (uint64_t *)(var_0 + (-56L));
            *var_27 = 0UL;
            _pre_phi113 = var_27;
        }
        var_30 = *var_15;
        var_31 = *(uint64_t *)(var_30 + 1024UL);
        var_32 = *_pre_phi113;
        var_33 = var_31 - var_32;
        var_34 = var_32 + var_30;
        var_35 = local_sp_3 + (-24L);
        *(uint64_t *)var_35 = 4213979UL;
        indirect_placeholder_3(var_34, var_33);
        var_36 = *(uint64_t *)(*var_15 + 1032UL);
        *var_15 = var_36;
        var_37 = var_36;
        local_sp_0 = var_35;
        local_sp_1 = local_sp_0;
        while (var_37 != 0UL)
            {
                var_38 = *(uint64_t *)(var_37 + 1024UL);
                var_39 = local_sp_0 + (-8L);
                *(uint64_t *)var_39 = 4214022UL;
                indirect_placeholder_3(var_37, var_38);
                var_40 = *(uint64_t *)(*var_15 + 1032UL);
                *var_15 = var_40;
                var_37 = var_40;
                local_sp_0 = var_39;
                local_sp_1 = local_sp_0;
            }
    }
    var_41 = *var_4;
    *(uint64_t *)(local_sp_3 + (-24L)) = 4213799UL;
    var_42 = indirect_placeholder_25(4UL, var_41);
    var_43 = var_42.field_0;
    var_44 = var_42.field_1;
    var_45 = var_42.field_2;
    *(uint64_t *)(local_sp_3 + (-32L)) = 4213807UL;
    indirect_placeholder();
    var_46 = (uint64_t)*(uint32_t *)var_43;
    var_47 = local_sp_3 + (-40L);
    *(uint64_t *)var_47 = 4213834UL;
    indirect_placeholder_24(0UL, var_43, 4312815UL, 0UL, var_46, var_44, var_45);
    *var_9 = (unsigned char)'\x00';
    local_sp_1 = var_47;
    var_48 = *var_12;
    local_sp_2 = local_sp_1;
    while (var_48 != 0UL)
        {
            *var_15 = *(uint64_t *)(var_48 + 1032UL);
            var_49 = local_sp_2 + (-8L);
            *(uint64_t *)var_49 = 4214074UL;
            indirect_placeholder();
            var_50 = *var_15;
            *var_12 = var_50;
            var_48 = var_50;
            local_sp_2 = var_49;
        }
    return (uint64_t)*var_9;
}
