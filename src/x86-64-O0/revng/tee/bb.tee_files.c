typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_46_ret_type;
struct indirect_placeholder_43_ret_type;
struct indirect_placeholder_44_ret_type;
struct indirect_placeholder_45_ret_type;
struct indirect_placeholder_46_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_43_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_44_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_45_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern void indirect_placeholder_35(uint64_t param_0, uint64_t param_1);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_4(void);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern uint64_t indirect_placeholder_21(uint64_t param_0);
extern void indirect_placeholder_39(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_46_ret_type indirect_placeholder_46(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_43_ret_type indirect_placeholder_43(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_44_ret_type indirect_placeholder_44(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_45_ret_type indirect_placeholder_45(uint64_t param_0, uint64_t param_1, uint64_t param_2);
typedef _Bool bool;
uint64_t bb_tee_files(uint64_t rsi, uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint32_t *var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    uint64_t *var_9;
    unsigned char *var_10;
    uint64_t spec_select;
    uint64_t *var_11;
    uint64_t var_12;
    uint64_t var_13;
    struct indirect_placeholder_46_ret_type var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t *var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint32_t *var_20;
    uint32_t var_21;
    uint64_t var_69;
    struct indirect_placeholder_43_ret_type var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t local_sp_11;
    unsigned char storemerge;
    unsigned char var_51;
    uint64_t r9_0;
    uint64_t r9_5_ph;
    unsigned char var_53;
    uint64_t var_52;
    uint64_t local_sp_0;
    uint64_t local_sp_1;
    uint64_t var_54;
    struct indirect_placeholder_44_ret_type var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t spec_select191;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t rcx_0;
    uint64_t r8_0;
    uint32_t var_61;
    uint64_t rax_1;
    uint64_t var_42;
    uint32_t var_43;
    uint64_t var_44;
    uint64_t local_sp_7;
    uint64_t var_45;
    uint64_t local_sp_2;
    uint64_t rax_0;
    uint64_t var_63;
    uint32_t var_46;
    uint64_t rcx_2;
    uint64_t local_sp_3;
    uint64_t r9_2;
    uint64_t rcx_1;
    uint64_t r8_5_ph;
    uint64_t r8_2;
    uint64_t r9_1;
    uint64_t r8_1;
    uint64_t local_sp_7_ph;
    uint64_t var_47;
    uint32_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t local_sp_4;
    uint32_t var_62;
    uint64_t rcx_4;
    uint64_t local_sp_5;
    uint64_t rcx_3;
    uint64_t r8_4;
    uint64_t r9_3;
    uint64_t r8_3;
    uint32_t var_36;
    uint64_t var_29;
    struct indirect_placeholder_45_ret_type var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t spec_select194;
    uint64_t var_35;
    uint64_t var_28;
    uint64_t local_sp_6;
    uint64_t r9_4;
    uint64_t var_37;
    uint32_t *var_38;
    uint32_t *var_39;
    unsigned char *var_40;
    uint32_t rax_1_ph_in;
    uint64_t rcx_5_ph;
    uint64_t local_sp_8;
    uint64_t rax_2;
    uint64_t local_sp_9;
    uint64_t var_64;
    uint64_t var_65;
    uint32_t var_66;
    uint64_t local_sp_10;
    uint64_t var_67;
    uint64_t var_68;
    uint32_t var_76;
    uint64_t var_41;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r9();
    var_3 = init_r8();
    var_4 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    var_5 = (uint32_t *)(var_0 + (-1100L));
    *var_5 = (uint32_t)rdi;
    var_6 = var_0 + (-1112L);
    var_7 = (uint64_t *)var_6;
    *var_7 = rsi;
    var_8 = (uint64_t *)(var_0 + (-32L));
    *var_8 = 0UL;
    var_9 = (uint64_t *)(var_0 + (-40L));
    *var_9 = 0UL;
    var_10 = (unsigned char *)(var_0 + (-45L));
    *var_10 = (unsigned char)'\x01';
    spec_select = (*(unsigned char *)4289424UL == '\x00') ? 4274019UL : 4274017UL;
    var_11 = (uint64_t *)(var_0 + (-56L));
    *var_11 = spec_select;
    *(uint64_t *)(var_0 + (-1120L)) = 4205370UL;
    indirect_placeholder_35(0UL, 0UL);
    *(uint64_t *)(var_0 + (-1128L)) = 4205385UL;
    indirect_placeholder_35(0UL, 1UL);
    var_12 = *(uint64_t *)4288072UL;
    *(uint64_t *)(var_0 + (-1136L)) = 4205405UL;
    indirect_placeholder_35(2UL, var_12);
    var_13 = (uint64_t)((long)(((uint64_t)*var_5 << 32UL) + 4294967296UL) >> (long)32UL);
    *(uint64_t *)(var_0 + (-1144L)) = 4205429UL;
    var_14 = indirect_placeholder_46(8UL, var_13, var_2, var_3);
    var_15 = var_14.field_0;
    var_16 = var_0 + (-64L);
    var_17 = (uint64_t *)var_16;
    *var_17 = var_15;
    *var_7 = (*var_7 + (-8L));
    **(uint64_t **)var_16 = *(uint64_t *)4288064UL;
    *(uint64_t *)(var_0 + (-1152L)) = 4205465UL;
    var_18 = indirect_placeholder_21(4274021UL);
    **(uint64_t **)var_6 = var_18;
    var_19 = var_0 + (-1160L);
    *(uint64_t *)var_19 = 4205505UL;
    indirect_placeholder();
    *var_8 = (*var_8 + 1UL);
    var_20 = (uint32_t *)(var_0 + (-44L));
    *var_20 = 1U;
    var_21 = 1U;
    storemerge = (unsigned char)'\x01';
    var_46 = 0U;
    rcx_4 = 0UL;
    rcx_3 = 0UL;
    r8_4 = var_3;
    local_sp_6 = var_19;
    r9_4 = var_2;
    var_66 = 1U;
    r9_5_ph = r9_4;
    r8_5_ph = r8_4;
    local_sp_7_ph = local_sp_6;
    r9_3 = r9_4;
    r8_3 = r8_4;
    rax_1_ph_in = var_21;
    rcx_5_ph = rcx_4;
    while ((long)((uint64_t)var_21 << 32UL) <= (long)((uint64_t)*var_5 << 32UL))
        {
            var_22 = (uint64_t)var_21 << 3UL;
            var_23 = *(uint64_t *)(*var_7 + var_22);
            var_24 = *var_17 + var_22;
            var_25 = *var_11;
            *(uint64_t *)(local_sp_6 + (-8L)) = 4205585UL;
            var_26 = indirect_placeholder_1(var_25, var_23);
            *(uint64_t *)var_24 = var_26;
            var_27 = (uint64_t)*var_20 << 3UL;
            if (*(uint64_t *)(*var_17 + var_27) == 0UL) {
                var_29 = *(uint64_t *)(*var_7 + var_27);
                *(uint64_t *)(local_sp_6 + (-16L)) = 4205660UL;
                var_30 = indirect_placeholder_45(var_29, 3UL, 0UL);
                var_31 = var_30.field_0;
                var_32 = var_30.field_1;
                var_33 = var_30.field_2;
                *(uint64_t *)(local_sp_6 + (-24L)) = 4205668UL;
                indirect_placeholder();
                var_34 = (uint64_t)*(uint32_t *)var_31;
                spec_select194 = ((*(uint32_t *)4289428UL + (-3)) < 2U) ? 1UL : 0UL;
                var_35 = local_sp_6 + (-32L);
                *(uint64_t *)var_35 = 4205724UL;
                indirect_placeholder_39(0UL, var_31, 4274014UL, var_34, spec_select194, var_32, var_33);
                *var_10 = (unsigned char)'\x00';
                local_sp_5 = var_35;
                rcx_3 = var_31;
                r9_3 = var_32;
                r8_3 = var_33;
            } else {
                var_28 = local_sp_6 + (-16L);
                *(uint64_t *)var_28 = 4205776UL;
                indirect_placeholder();
                *var_8 = (*var_8 + 1UL);
                local_sp_5 = var_28;
            }
            var_36 = *var_20 + 1U;
            *var_20 = var_36;
            var_21 = var_36;
            rcx_4 = rcx_3;
            r8_4 = r8_3;
            local_sp_6 = local_sp_5;
            r9_4 = r9_3;
            r9_5_ph = r9_4;
            r8_5_ph = r8_4;
            local_sp_7_ph = local_sp_6;
            r9_3 = r9_4;
            r8_3 = r8_4;
            rax_1_ph_in = var_21;
            rcx_5_ph = rcx_4;
        }
    var_37 = var_0 + (-1096L);
    var_38 = (uint32_t *)var_37;
    var_39 = (uint32_t *)(var_0 + (-68L));
    var_40 = (unsigned char *)(var_0 + (-69L));
    var_45 = var_37;
    rax_0 = var_37;
    while (1U)
        {
            rax_1 = (uint64_t)rax_1_ph_in;
            local_sp_7 = local_sp_7_ph;
            rcx_1 = rcx_5_ph;
            r9_1 = r9_5_ph;
            r8_1 = r8_5_ph;
            while (1U)
                {
                    local_sp_8 = local_sp_7;
                    rax_2 = rax_1;
                    if (*var_8 != 0UL) {
                        var_63 = *var_9;
                        loop_state_var = 1U;
                        break;
                    }
                    var_41 = local_sp_7 + (-8L);
                    *(uint64_t *)var_41 = 4205830UL;
                    indirect_placeholder();
                    *var_9 = var_37;
                    local_sp_2 = var_41;
                    if ((long)var_37 <= (long)18446744073709551615UL) {
                        loop_state_var = 2U;
                        break;
                    }
                    var_42 = local_sp_7 + (-16L);
                    *(uint64_t *)var_42 = 4205846UL;
                    indirect_placeholder();
                    var_43 = *var_38;
                    var_44 = (uint64_t)var_43;
                    local_sp_2 = var_42;
                    rax_0 = var_44;
                    local_sp_7 = var_42;
                    rax_1 = var_44;
                    if ((uint64_t)(var_43 + (-4)) == 0UL) {
                        continue;
                    }
                    loop_state_var = 0U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 2U:
              case 0U:
                {
                    switch (loop_state_var) {
                      case 0U:
                        {
                            var_45 = *var_9;
                        }
                        break;
                      case 2U:
                        {
                        }
                        break;
                    }
                    local_sp_3 = local_sp_2;
                    var_63 = var_45;
                    local_sp_8 = local_sp_2;
                    rax_2 = rax_0;
                    if ((long)var_45 > (long)0UL) {
                        switch_state_var = 1;
                        break;
                    }
                    *var_20 = 0U;
                    r9_0 = r9_1;
                    r9_5_ph = r9_1;
                    r8_0 = r8_1;
                    var_61 = var_46;
                    rcx_2 = rcx_1;
                    r9_2 = r9_1;
                    r8_5_ph = r8_1;
                    r8_2 = r8_1;
                    local_sp_7_ph = local_sp_3;
                    local_sp_4 = local_sp_3;
                    rax_1_ph_in = var_46;
                    rcx_5_ph = rcx_1;
                    while ((long)((uint64_t)var_46 << 32UL) <= (long)((uint64_t)*var_5 << 32UL))
                        {
                            var_47 = *(uint64_t *)(*var_17 + ((uint64_t)var_46 << 3UL));
                            rcx_0 = var_47;
                            if (var_47 == 0UL) {
                                var_62 = var_61 + 1U;
                                *var_20 = var_62;
                                var_46 = var_62;
                                local_sp_3 = local_sp_4;
                                rcx_1 = rcx_2;
                                r9_1 = r9_2;
                                r8_1 = r8_2;
                                r9_0 = r9_1;
                                r9_5_ph = r9_1;
                                r8_0 = r8_1;
                                var_61 = var_46;
                                rcx_2 = rcx_1;
                                r9_2 = r9_1;
                                r8_5_ph = r8_1;
                                r8_2 = r8_1;
                                local_sp_7_ph = local_sp_3;
                                local_sp_4 = local_sp_3;
                                rax_1_ph_in = var_46;
                                rcx_5_ph = rcx_1;
                                continue;
                            }
                            *(uint64_t *)(local_sp_3 + (-8L)) = 4205963UL;
                            indirect_placeholder();
                            *(uint64_t *)(local_sp_3 + (-16L)) = 4205978UL;
                            indirect_placeholder();
                            var_48 = *var_38;
                            var_49 = (uint64_t)var_48;
                            *var_39 = var_48;
                            var_50 = local_sp_3 + (-24L);
                            *(uint64_t *)var_50 = 4205988UL;
                            indirect_placeholder();
                            local_sp_0 = var_50;
                            if (*(uint32_t *)var_49 == 32U) {
                            } else {
                                var_51 = storemerge & '\x01';
                                *var_40 = var_51;
                                var_53 = var_51;
                                if (*(uint64_t *)(*var_17 + ((uint64_t)*var_20 << 3UL)) == *(uint64_t *)4288064UL) {
                                    var_52 = local_sp_3 + (-32L);
                                    *(uint64_t *)var_52 = 4206086UL;
                                    indirect_placeholder();
                                    var_53 = *var_40;
                                    local_sp_0 = var_52;
                                }
                                local_sp_1 = local_sp_0;
                                if (var_53 == '\x00') {
                                    var_54 = *(uint64_t *)(*var_7 + ((uint64_t)*var_20 << 3UL));
                                    *(uint64_t *)(local_sp_0 + (-8L)) = 4206136UL;
                                    var_55 = indirect_placeholder_44(var_54, 3UL, 0UL);
                                    var_56 = var_55.field_0;
                                    var_57 = var_55.field_1;
                                    var_58 = var_55.field_2;
                                    spec_select191 = ((*(uint32_t *)4289428UL + (-3)) < 2U) ? 1UL : 0UL;
                                    var_59 = (uint64_t)*var_39;
                                    var_60 = local_sp_0 + (-16L);
                                    *(uint64_t *)var_60 = 4206196UL;
                                    indirect_placeholder_39(0UL, var_56, 4274014UL, var_59, spec_select191, var_57, var_58);
                                    local_sp_1 = var_60;
                                    rcx_0 = var_56;
                                    r9_0 = var_57;
                                    r8_0 = var_58;
                                }
                                *(uint64_t *)(*var_17 + ((uint64_t)*var_20 << 3UL)) = 0UL;
                                local_sp_4 = local_sp_1;
                                rcx_2 = rcx_0;
                                r9_2 = r9_0;
                                r8_2 = r8_0;
                                if (*var_40 != '\x00') {
                                    *var_10 = (unsigned char)'\x00';
                                }
                                *var_8 = (*var_8 + (-1L));
                                var_61 = *var_20;
                            }
                        }
                    continue;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    local_sp_9 = local_sp_8;
    if (var_63 == 18446744073709551615UL) {
        *(uint64_t *)(local_sp_8 + (-8L)) = 4206283UL;
        indirect_placeholder();
        var_64 = (uint64_t)*(uint32_t *)rax_2;
        var_65 = local_sp_8 + (-16L);
        *(uint64_t *)var_65 = 4206307UL;
        indirect_placeholder_39(0UL, rcx_5_ph, 4274037UL, var_64, 0UL, r9_5_ph, r8_5_ph);
        *var_10 = (unsigned char)'\x00';
        local_sp_9 = var_65;
    }
    *var_20 = 1U;
    local_sp_10 = local_sp_9;
    local_sp_11 = local_sp_10;
    while ((long)((uint64_t)var_66 << 32UL) <= (long)((uint64_t)*var_5 << 32UL))
        {
            var_67 = local_sp_10 + (-8L);
            *(uint64_t *)var_67 = 4206382UL;
            var_68 = indirect_placeholder_4();
            local_sp_11 = var_67;
            if (*(uint64_t *)(*var_17 + ((uint64_t)var_66 << 3UL)) != 0UL & (uint64_t)(uint32_t)var_68 == 0UL) {
                var_69 = *(uint64_t *)(*var_7 + ((uint64_t)*var_20 << 3UL));
                *(uint64_t *)(local_sp_10 + (-16L)) = 4206430UL;
                var_70 = indirect_placeholder_43(var_69, 3UL, 0UL);
                var_71 = var_70.field_0;
                var_72 = var_70.field_1;
                var_73 = var_70.field_2;
                *(uint64_t *)(local_sp_10 + (-24L)) = 4206438UL;
                indirect_placeholder();
                var_74 = (uint64_t)*(uint32_t *)var_71;
                var_75 = local_sp_10 + (-32L);
                *(uint64_t *)var_75 = 4206465UL;
                indirect_placeholder_39(0UL, var_71, 4274014UL, var_74, 0UL, var_72, var_73);
                *var_10 = (unsigned char)'\x00';
                local_sp_11 = var_75;
            }
            var_76 = *var_20 + 1U;
            *var_20 = var_76;
            var_66 = var_76;
            local_sp_10 = local_sp_11;
            local_sp_11 = local_sp_10;
        }
    *(uint64_t *)(local_sp_10 + (-8L)) = 4206500UL;
    indirect_placeholder();
    return (uint64_t)*var_10;
}
