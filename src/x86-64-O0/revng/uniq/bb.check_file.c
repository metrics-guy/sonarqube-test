typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_31_ret_type;
struct indirect_placeholder_29_ret_type;
struct indirect_placeholder_32_ret_type;
struct indirect_placeholder_33_ret_type;
struct indirect_placeholder_31_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_29_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_32_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_33_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_1(void);
extern uint64_t indirect_placeholder_27(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_7(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_30(void);
extern uint64_t indirect_placeholder_28(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint64_t init_rbx(void);
extern uint64_t init_r8(void);
extern uint64_t init_r9(void);
extern uint64_t indirect_placeholder_6(uint64_t param_0);
extern void indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_31_ret_type indirect_placeholder_31(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_29_ret_type indirect_placeholder_29(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_32_ret_type indirect_placeholder_32(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_33_ret_type indirect_placeholder_33(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
void bb_check_file(uint64_t rdx, uint64_t rsi, uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    unsigned char *var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t local_sp_13;
    uint64_t local_sp_0;
    uint64_t var_115;
    uint64_t var_116;
    uint64_t local_sp_1;
    uint64_t var_117;
    struct indirect_placeholder_31_ret_type var_118;
    uint64_t var_119;
    uint64_t var_120;
    uint64_t var_121;
    uint64_t var_122;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t local_sp_3;
    uint64_t local_sp_4_be;
    uint64_t var_100;
    uint32_t var_101;
    uint64_t local_sp_2;
    uint64_t var_102;
    unsigned char var_103;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t var_107;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t var_112;
    uint64_t var_108;
    uint64_t var_109;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t var_97;
    unsigned char var_98;
    uint64_t var_99;
    uint64_t local_sp_4;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_26;
    uint64_t *var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t *var_78;
    uint64_t *var_79;
    uint64_t *var_80;
    unsigned char *var_81;
    uint64_t *var_82;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t local_sp_8;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t local_sp_6;
    uint64_t local_sp_5;
    unsigned char storemerge;
    unsigned char var_59;
    unsigned char var_62;
    uint32_t var_60;
    uint64_t local_sp_7;
    uint64_t var_61;
    uint64_t local_sp_11_be;
    uint64_t var_63;
    uint64_t local_sp_11;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_64;
    uint64_t local_sp_10;
    uint64_t var_113;
    uint64_t var_114;
    unsigned char *var_35;
    uint64_t *var_36;
    uint64_t *var_37;
    uint64_t *var_38;
    uint64_t *var_39;
    unsigned char *var_40;
    uint64_t *var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t *var_65;
    unsigned char *var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t r8_0;
    uint64_t var_25;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t r9_0;
    uint64_t local_sp_12;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t *var_31;
    uint64_t var_32;
    uint64_t *var_33;
    uint64_t var_34;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    struct indirect_placeholder_33_ret_type var_18;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_19;
    uint64_t var_20;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r8();
    var_3 = init_r9();
    var_4 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    var_5 = (uint64_t *)(var_0 + (-224L));
    *var_5 = rdi;
    var_6 = (uint64_t *)(var_0 + (-232L));
    *var_6 = rsi;
    var_7 = (unsigned char *)(var_0 + (-236L));
    *var_7 = (unsigned char)rdx;
    var_8 = *var_5;
    var_9 = var_0 + (-256L);
    *(uint64_t *)var_9 = 4205915UL;
    indirect_placeholder_1();
    local_sp_13 = var_9;
    storemerge = (unsigned char)'\x01';
    var_62 = (unsigned char)'\x00';
    r8_0 = var_2;
    r9_0 = var_3;
    var_10 = *(uint64_t *)4296264UL;
    var_11 = *var_5;
    var_12 = var_0 + (-264L);
    *(uint64_t *)var_12 = 4205946UL;
    var_13 = indirect_placeholder_27(var_10, 4279008UL, var_11);
    local_sp_13 = var_12;
    if ((uint64_t)(uint32_t)var_8 != 0UL & var_13 == 0UL) {
        var_14 = *var_5;
        *(uint64_t *)(var_0 + (-272L)) = 4205976UL;
        var_15 = indirect_placeholder_27(var_14, 3UL, 0UL);
        *(uint64_t *)(var_0 + (-280L)) = 4205984UL;
        indirect_placeholder_1();
        var_16 = (uint64_t)*(uint32_t *)var_15;
        var_17 = var_0 + (-288L);
        *(uint64_t *)var_17 = 4206011UL;
        var_18 = indirect_placeholder_33(0UL, var_15, 4279010UL, var_16, var_2, 1UL, var_3);
        local_sp_13 = var_17;
        r8_0 = var_18.field_1;
        r9_0 = var_18.field_2;
    }
    var_19 = *var_6;
    var_20 = local_sp_13 + (-8L);
    *(uint64_t *)var_20 = 4206031UL;
    indirect_placeholder_1();
    local_sp_12 = var_20;
    var_21 = *(uint64_t *)4296256UL;
    var_22 = *var_6;
    var_23 = local_sp_13 + (-16L);
    *(uint64_t *)var_23 = 4206062UL;
    var_24 = indirect_placeholder_27(var_21, 4279013UL, var_22);
    local_sp_12 = var_23;
    if ((uint64_t)(uint32_t)var_19 != 0UL & var_24 == 0UL) {
        var_25 = *var_6;
        *(uint64_t *)(local_sp_13 + (-24L)) = 4206092UL;
        var_26 = indirect_placeholder_27(var_25, 3UL, 0UL);
        *(uint64_t *)(local_sp_13 + (-32L)) = 4206100UL;
        indirect_placeholder_1();
        var_27 = (uint64_t)*(uint32_t *)var_26;
        var_28 = local_sp_13 + (-40L);
        *(uint64_t *)var_28 = 4206127UL;
        indirect_placeholder_32(0UL, var_26, 4279010UL, var_27, r8_0, 1UL, r9_0);
        local_sp_12 = var_28;
    }
    var_29 = *(uint64_t *)4296264UL;
    *(uint64_t *)(local_sp_12 + (-8L)) = 4206147UL;
    indirect_placeholder_7(2UL, var_29);
    var_30 = var_0 + (-184L);
    var_31 = (uint64_t *)(var_0 + (-32L));
    *var_31 = var_30;
    var_32 = var_0 + (-216L);
    var_33 = (uint64_t *)(var_0 + (-40L));
    *var_33 = var_32;
    *(uint64_t *)(local_sp_12 + (-16L)) = 4206181UL;
    indirect_placeholder_1();
    var_34 = local_sp_12 + (-24L);
    *(uint64_t *)var_34 = 4206193UL;
    indirect_placeholder_1();
    local_sp_11 = var_34;
    if (*(unsigned char *)4297684UL == '\x00') {
        var_65 = (uint64_t *)(var_0 + (-88L));
        *var_65 = 0UL;
        var_66 = (unsigned char *)(var_0 + (-89L));
        *var_66 = (unsigned char)'\x01';
        var_67 = (uint64_t)(uint32_t)(uint64_t)*var_7;
        var_68 = *(uint64_t *)4296264UL;
        var_69 = *var_33;
        var_70 = local_sp_12 + (-32L);
        *(uint64_t *)var_70 = 4206699UL;
        var_71 = indirect_placeholder_27(var_67, var_68, var_69);
        local_sp_10 = var_70;
        if (var_71 == 0UL) {
            var_113 = *(uint64_t *)4296264UL;
            var_114 = local_sp_10 + (-8L);
            *(uint64_t *)var_114 = 4207247UL;
            indirect_placeholder_1();
            local_sp_1 = var_114;
            if ((uint64_t)(uint32_t)var_113 != 0UL) {
                var_115 = local_sp_10 + (-16L);
                *(uint64_t *)var_115 = 4207266UL;
                var_116 = indirect_placeholder_30();
                local_sp_0 = var_115;
                local_sp_1 = var_115;
                if ((uint64_t)(uint32_t)var_116 == 0UL) {
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4207333UL;
                    indirect_placeholder_1();
                    *(uint64_t *)(local_sp_0 + (-16L)) = 4207348UL;
                    indirect_placeholder_1();
                    return;
                }
            }
            var_117 = *var_5;
            *(uint64_t *)(local_sp_1 + (-8L)) = 4207290UL;
            var_118 = indirect_placeholder_31(var_117, 4UL);
            var_119 = var_118.field_0;
            var_120 = var_118.field_1;
            var_121 = var_118.field_2;
            var_122 = local_sp_1 + (-16L);
            *(uint64_t *)var_122 = 4207318UL;
            indirect_placeholder_29(0UL, var_119, 4279015UL, 0UL, var_120, 1UL, var_121);
            local_sp_0 = var_122;
            *(uint64_t *)(local_sp_0 + (-8L)) = 4207333UL;
            indirect_placeholder_1();
            *(uint64_t *)(local_sp_0 + (-16L)) = 4207348UL;
            indirect_placeholder_1();
            return;
        }
        var_72 = *var_33;
        var_73 = local_sp_12 + (-40L);
        *(uint64_t *)var_73 = 4206720UL;
        var_74 = indirect_placeholder_6(var_72);
        var_75 = (uint64_t *)(var_0 + (-72L));
        *var_75 = var_74;
        var_76 = *var_33;
        var_77 = (*(uint64_t *)(var_76 + 8UL) + (*(uint64_t *)(var_76 + 16UL) - var_74)) + (-1L);
        var_78 = (uint64_t *)(var_0 + (-80L));
        *var_78 = var_77;
        var_79 = (uint64_t *)(var_0 + (-136L));
        var_80 = (uint64_t *)(var_0 + (-144L));
        var_81 = (unsigned char *)(var_0 + (-145L));
        var_82 = (uint64_t *)(var_0 + (-160L));
        local_sp_4 = var_73;
        while (1U)
            {
                var_83 = *(uint64_t *)4296264UL;
                var_84 = local_sp_4 + (-8L);
                *(uint64_t *)var_84 = 4207194UL;
                indirect_placeholder_1();
                local_sp_3 = var_84;
                if ((uint64_t)(uint32_t)var_83 != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                var_85 = (uint64_t)(uint32_t)(uint64_t)*var_7;
                var_86 = *(uint64_t *)4296264UL;
                var_87 = *var_31;
                *(uint64_t *)(local_sp_4 + (-16L)) = 4206798UL;
                var_88 = indirect_placeholder_27(var_85, var_86, var_87);
                if (var_88 != 0UL) {
                    var_108 = *(uint64_t *)4296264UL;
                    var_109 = local_sp_4 + (-24L);
                    *(uint64_t *)var_109 = 4206818UL;
                    indirect_placeholder_1();
                    local_sp_3 = var_109;
                    local_sp_10 = var_109;
                    if ((uint64_t)(uint32_t)var_108 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    loop_state_var = 0U;
                    break;
                }
                var_89 = *var_31;
                *(uint64_t *)(local_sp_4 + (-24L)) = 4206843UL;
                var_90 = indirect_placeholder_6(var_89);
                *var_79 = var_90;
                var_91 = *var_31;
                var_92 = (*(uint64_t *)(var_91 + 8UL) + (*(uint64_t *)(var_91 + 16UL) - var_90)) + (-1L);
                *var_80 = var_92;
                var_93 = *var_78;
                var_94 = *var_75;
                var_95 = *var_79;
                var_96 = local_sp_4 + (-32L);
                *(uint64_t *)var_96 = 4206917UL;
                var_97 = indirect_placeholder_28(var_93, var_92, var_94, var_95);
                var_98 = ((uint64_t)(unsigned char)var_97 == 0UL) & '\x01';
                *var_81 = var_98;
                var_99 = *var_65 + (uint64_t)var_98;
                *var_65 = var_99;
                var_100 = var_99;
                local_sp_2 = var_96;
                if (var_99 == 18446744073709551615UL) {
                    *var_65 = 18446744073709551614UL;
                    var_100 = 18446744073709551614UL;
                }
                var_101 = *(uint32_t *)4297688UL;
                if (var_101 == 0U) {
                    var_103 = *var_81;
                    local_sp_4_be = local_sp_2;
                    if (var_103 != '\x01') {
                        if (*(unsigned char *)4297686UL != '\x00') {
                            local_sp_4 = local_sp_4_be;
                            continue;
                        }
                    }
                    var_104 = (uint64_t)var_103;
                    var_105 = *var_65;
                    var_106 = *var_33;
                    var_107 = local_sp_2 + (-8L);
                    *(uint64_t *)var_107 = 4207108UL;
                    indirect_placeholder_4(var_105, var_104, var_106);
                    *var_82 = *var_33;
                    *var_33 = *var_31;
                    *var_31 = *var_82;
                    *var_75 = *var_79;
                    *var_78 = *var_80;
                    local_sp_4_be = var_107;
                    if (*var_81 == '\x01') {
                        *var_65 = 0UL;
                    }
                    local_sp_4 = local_sp_4_be;
                    continue;
                }
                if (*var_81 == '\x01') {
                    if (var_100 != 1UL) {
                        switch (var_101) {
                          case 1U:
                            {
                                var_102 = local_sp_4 + (-40L);
                                *(uint64_t *)var_102 = 4207058UL;
                                indirect_placeholder_1();
                                local_sp_2 = var_102;
                            }
                            break;
                          case 2U:
                            {
                                if (*var_66 == '\x01') {
                                    var_102 = local_sp_4 + (-40L);
                                    *(uint64_t *)var_102 = 4207058UL;
                                    indirect_placeholder_1();
                                    local_sp_2 = var_102;
                                }
                            }
                            break;
                          default:
                            {
                                break;
                            }
                            break;
                        }
                    }
                } else {
                    if (var_100 == 0UL) {
                        *var_66 = (unsigned char)'\x00';
                    }
                }
            }
        var_110 = *var_65;
        var_111 = *var_33;
        var_112 = local_sp_3 + (-8L);
        *(uint64_t *)var_112 = 4207226UL;
        indirect_placeholder_4(var_110, 0UL, var_111);
        local_sp_10 = var_112;
    } else {
        if (*(unsigned char *)4297685UL != '\x00' & *(uint32_t *)4297680UL != 1U) {
            var_35 = (unsigned char *)(var_0 + (-57L));
            *var_35 = (unsigned char)'\x00';
            var_36 = (uint64_t *)(var_0 + (-104L));
            var_37 = (uint64_t *)(var_0 + (-112L));
            var_38 = (uint64_t *)(var_0 + (-56L));
            var_39 = (uint64_t *)(var_0 + (-48L));
            var_40 = (unsigned char *)(var_0 + (-113L));
            var_41 = (uint64_t *)(var_0 + (-128L));
            var_42 = *(uint64_t *)4296264UL;
            var_43 = local_sp_11 + (-8L);
            *(uint64_t *)var_43 = 4206592UL;
            indirect_placeholder_1();
            local_sp_8 = var_43;
            while ((uint64_t)(uint32_t)var_42 != 0UL)
                {
                    var_44 = (uint64_t)(uint32_t)(uint64_t)*var_7;
                    var_45 = *(uint64_t *)4296264UL;
                    var_46 = *var_31;
                    var_47 = local_sp_11 + (-16L);
                    *(uint64_t *)var_47 = 4206276UL;
                    var_48 = indirect_placeholder_27(var_44, var_45, var_46);
                    local_sp_8 = var_47;
                    if (var_48 == 0UL) {
                        break;
                    }
                    var_49 = *var_31;
                    var_50 = local_sp_11 + (-24L);
                    *(uint64_t *)var_50 = 4206297UL;
                    var_51 = indirect_placeholder_6(var_49);
                    *var_36 = var_51;
                    var_52 = *var_31;
                    var_53 = (*(uint64_t *)(var_52 + 8UL) + (*(uint64_t *)(var_52 + 16UL) - var_51)) + (-1L);
                    *var_37 = var_53;
                    local_sp_5 = var_50;
                    if (*(uint64_t *)(*var_33 + 8UL) == 0UL) {
                        local_sp_6 = local_sp_5;
                    } else {
                        var_54 = *var_38;
                        var_55 = *var_39;
                        var_56 = *var_36;
                        var_57 = local_sp_11 + (-32L);
                        *(uint64_t *)var_57 = 4206378UL;
                        var_58 = indirect_placeholder_28(var_54, var_53, var_55, var_56);
                        local_sp_5 = var_57;
                        local_sp_6 = var_57;
                        storemerge = (unsigned char)'\x00';
                        if ((uint64_t)(unsigned char)var_58 == 0UL) {
                            local_sp_6 = local_sp_5;
                        }
                    }
                    var_59 = storemerge & '\x01';
                    *var_40 = var_59;
                    local_sp_7 = local_sp_6;
                    if (var_59 == '\x00') {
                        local_sp_11_be = local_sp_7;
                        if (var_62 != '\x00') {
                            if (*(uint32_t *)4297692UL != 0U) {
                                local_sp_11 = local_sp_11_be;
                                var_42 = *(uint64_t *)4296264UL;
                                var_43 = local_sp_11 + (-8L);
                                *(uint64_t *)var_43 = 4206592UL;
                                indirect_placeholder_1();
                                local_sp_8 = var_43;
                                continue;
                            }
                        }
                        var_63 = local_sp_7 + (-8L);
                        *(uint64_t *)var_63 = 4206533UL;
                        indirect_placeholder_1();
                        *var_41 = *var_33;
                        *var_33 = *var_31;
                        *var_31 = *var_41;
                        *var_39 = *var_36;
                        *var_38 = *var_37;
                        *var_35 = (unsigned char)'\x01';
                        local_sp_11_be = var_63;
                        local_sp_11 = local_sp_11_be;
                        var_42 = *(uint64_t *)4296264UL;
                        var_43 = local_sp_11 + (-8L);
                        *(uint64_t *)var_43 = 4206592UL;
                        indirect_placeholder_1();
                        local_sp_8 = var_43;
                        continue;
                    }
                    var_60 = *(uint32_t *)4297692UL;
                    var_62 = var_59;
                    switch (var_60) {
                      case 4U:
                      case 1U:
                        {
                            var_61 = local_sp_6 + (-8L);
                            *(uint64_t *)var_61 = 4206481UL;
                            indirect_placeholder_1();
                            var_62 = *var_40;
                            local_sp_7 = var_61;
                        }
                        break;
                      case 0U:
                        {
                            break;
                        }
                        break;
                      default:
                        {
                            if ((*var_35 != '\x00') && ((var_60 + (-2)) < 2U)) {
                                var_61 = local_sp_6 + (-8L);
                                *(uint64_t *)var_61 = 4206481UL;
                                indirect_placeholder_1();
                                var_62 = *var_40;
                                local_sp_7 = var_61;
                            }
                        }
                        break;
                    }
                }
            if (*var_35 == '\x00') {
                var_64 = local_sp_8 + (-8L);
                *(uint64_t *)var_64 = 4206653UL;
                indirect_placeholder_1();
                local_sp_10 = var_64;
            }
        }
    }
}
