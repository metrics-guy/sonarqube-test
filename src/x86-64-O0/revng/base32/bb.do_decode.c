typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_22_ret_type;
struct indirect_placeholder_23_ret_type;
struct indirect_placeholder_25_ret_type;
struct indirect_placeholder_22_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_23_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_25_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_1(void);
extern uint64_t indirect_placeholder_24(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_rdi(void);
extern void indirect_placeholder_3(uint64_t param_0);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern uint64_t indirect_placeholder_21(uint64_t param_0);
extern struct indirect_placeholder_22_ret_type indirect_placeholder_22(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_23_ret_type indirect_placeholder_23(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_25_ret_type indirect_placeholder_25(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
typedef _Bool bool;
void bb_do_decode(uint64_t rdx, uint64_t rsi) {
    uint64_t var_56;
    uint64_t var_46;
    struct indirect_placeholder_22_ret_type var_53;
    uint64_t r9_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    unsigned char *var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t *var_14;
    uint64_t *var_15;
    uint64_t var_16;
    uint32_t *var_17;
    uint32_t *var_18;
    uint64_t var_19;
    unsigned char *var_20;
    uint32_t *var_21;
    uint64_t local_sp_7;
    uint64_t r9_3;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t r8_3;
    uint64_t storemerge;
    uint64_t rcx_0;
    uint64_t local_sp_0;
    uint64_t r8_0;
    uint64_t local_sp_1;
    uint64_t var_54;
    struct indirect_placeholder_23_ret_type var_55;
    uint64_t r9_1;
    uint64_t r8_1;
    uint64_t local_sp_4;
    uint64_t var_57;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_26;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t local_sp_6;
    uint64_t r8_5;
    struct indirect_placeholder_25_ret_type var_41;
    uint64_t local_sp_2;
    uint64_t r9_2;
    uint64_t r8_2;
    uint64_t local_sp_3;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_22;
    uint64_t var_44;
    uint64_t storemerge1;
    uint32_t var_45;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t rcx_2;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t rcx_1_ph;
    uint64_t var_35;
    uint64_t r9_4;
    uint64_t local_sp_5;
    uint64_t local_sp_5_ph;
    uint64_t var_27;
    uint64_t r8_4;
    uint64_t var_36;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t local_sp_8;
    uint64_t r9_5;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rdi();
    var_3 = init_cc_src2();
    var_4 = init_r9();
    var_5 = init_r8();
    var_6 = var_0 + (-8L);
    *(uint64_t *)var_6 = var_1;
    var_7 = (uint64_t *)(var_0 + (-13392L));
    *var_7 = var_2;
    var_8 = (uint64_t *)(var_0 + (-13400L));
    *var_8 = rsi;
    var_9 = (unsigned char *)(var_0 + (-13404L));
    *var_9 = (unsigned char)rdx;
    var_10 = var_0 + (-13364L);
    var_11 = var_0 + (-13424L);
    *(uint64_t *)var_11 = 4205681UL;
    indirect_placeholder_3(var_10);
    var_12 = (uint64_t *)(var_0 + (-16L));
    var_13 = var_0 + (-13376L);
    var_14 = (uint64_t *)var_13;
    var_15 = (uint64_t *)(var_0 + (-32L));
    var_16 = var_0 + (-8232L);
    var_17 = (uint32_t *)(var_0 + (-20L));
    var_18 = (uint32_t *)var_10;
    var_19 = var_0 + (-13352L);
    var_20 = (unsigned char *)(var_0 + (-33L));
    var_21 = (uint32_t *)var_19;
    local_sp_7 = var_11;
    storemerge = 0UL;
    r8_0 = var_13;
    var_22 = 0UL;
    r9_4 = var_4;
    r8_4 = var_5;
    var_56 = *var_7;
    var_57 = local_sp_4 + (-16L);
    *(uint64_t *)var_57 = 4206318UL;
    indirect_placeholder_1();
    local_sp_7 = var_57;
    do {
        *var_12 = 0UL;
        local_sp_8 = local_sp_7;
        r9_5 = r9_4;
        r8_5 = r8_4;
        while (1U)
            {
                var_23 = 8192UL - var_22;
                var_24 = *var_7;
                var_25 = local_sp_8 + (-8L);
                *(uint64_t *)var_25 = 4205736UL;
                indirect_placeholder_1();
                *var_14 = var_23;
                local_sp_6 = var_25;
                r9_2 = r9_5;
                r8_2 = r8_5;
                rcx_2 = var_24;
                rcx_1_ph = var_24;
                local_sp_5_ph = var_25;
                var_36 = var_23;
                if (*var_9 == '\x00') {
                    *var_12 = (*var_12 + var_36);
                    var_37 = *var_7;
                    var_38 = local_sp_6 + (-8L);
                    *(uint64_t *)var_38 = 4205974UL;
                    indirect_placeholder_1();
                    local_sp_2 = var_38;
                    if ((uint64_t)(uint32_t)var_37 == 0UL) {
                        *(uint64_t *)(local_sp_6 + (-16L)) = 4205983UL;
                        indirect_placeholder_1();
                        var_39 = (uint64_t)*(uint32_t *)var_37;
                        var_40 = local_sp_6 + (-24L);
                        *(uint64_t *)var_40 = 4206007UL;
                        var_41 = indirect_placeholder_25(0UL, rcx_2, 4277744UL, 1UL, var_39, r9_5, r8_5);
                        local_sp_2 = var_40;
                        r9_2 = var_41.field_1;
                        r8_2 = var_41.field_2;
                    }
                    r9_3 = r9_2;
                    r8_3 = r8_2;
                    r8_5 = r8_2;
                    local_sp_3 = local_sp_2;
                    r9_5 = r9_2;
                    if (*var_12 > 8191UL) {
                        break;
                    }
                    var_42 = *var_7;
                    var_43 = local_sp_2 + (-8L);
                    *(uint64_t *)var_43 = 4206032UL;
                    indirect_placeholder_1();
                    local_sp_3 = var_43;
                    local_sp_8 = var_43;
                    if ((uint64_t)(uint32_t)var_42 == 0UL) {
                        break;
                    }
                    var_22 = *var_12;
                    continue;
                }
                *var_15 = 0UL;
                var_36 = 0UL;
                while (1U)
                    {
                        local_sp_5 = local_sp_5_ph;
                        rcx_2 = rcx_1_ph;
                        while (1U)
                            {
                                var_26 = *var_14;
                                local_sp_6 = local_sp_5;
                                if (var_26 != 0UL) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                var_27 = helper_cc_compute_c_wrapper(*var_15 - var_26, var_26, var_3, 17U);
                                if (var_27 != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                var_28 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(((*var_15 + *var_12) + var_6) + (-8224L));
                                var_29 = local_sp_5 + (-8L);
                                *(uint64_t *)var_29 = 4205798UL;
                                var_30 = indirect_placeholder_21(var_28);
                                local_sp_5 = var_29;
                                if ((uint64_t)(unsigned char)var_30 == 0UL) {
                                    var_31 = *var_12;
                                    var_32 = *var_15;
                                    var_35 = var_32;
                                    if (*(unsigned char *)(((var_32 + var_31) + var_6) + (-8224L)) != '=') {
                                        loop_state_var = 2U;
                                        break;
                                    }
                                }
                                var_35 = *var_15;
                                *var_15 = (var_35 + 1UL);
                                continue;
                            }
                        switch_state_var = 0;
                        switch (loop_state_var) {
                          case 2U:
                            {
                                *var_14 = (*var_14 + (-1L));
                                var_33 = (*var_12 + *var_15) + var_16;
                                var_34 = local_sp_5 + (-16L);
                                *(uint64_t *)var_34 = 4205919UL;
                                indirect_placeholder_1();
                                rcx_1_ph = var_33;
                                local_sp_5_ph = var_34;
                                continue;
                            }
                            break;
                          case 0U:
                            {
                                loop_state_var = 0U;
                                switch_state_var = 1;
                                break;
                            }
                            break;
                          case 1U:
                            {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            break;
                        }
                        if (switch_state_var)
                            break;
                    }
                var_36 = *var_14;
            }
        *var_17 = 0U;
        local_sp_4 = local_sp_3;
        var_44 = *var_7;
        *(uint64_t *)(local_sp_4 + (-8L)) = 4206275UL;
        indirect_placeholder_1();
        storemerge1 = ((uint64_t)(uint32_t)var_44 == 0UL) ? 1UL : 2UL;
        var_45 = *var_17;
        r9_0 = r9_3;
        r9_4 = r9_3;
        r8_4 = r8_3;
        while (storemerge1 <= (uint64_t)var_45)
            {
                if (var_45 != 1U) {
                    if (*var_18 == 0U) {
                        break;
                    }
                }
                *var_14 = 5120UL;
                if (*var_17 == 0U) {
                    storemerge = *var_12;
                }
                *(uint64_t *)(local_sp_4 + (-16L)) = 4206142UL;
                var_46 = indirect_placeholder_24(var_19, storemerge, var_10, var_16, var_13);
                *var_20 = (unsigned char)var_46;
                var_47 = *var_8;
                var_48 = local_sp_4 + (-24L);
                *(uint64_t *)var_48 = 4206179UL;
                indirect_placeholder_1();
                var_49 = *var_14;
                var_50 = helper_cc_compute_c_wrapper(var_19 - var_49, var_49, var_3, 17U);
                rcx_0 = var_47;
                local_sp_0 = var_48;
                if (var_50 == 0UL) {
                    *(uint64_t *)(local_sp_4 + (-32L)) = 4206196UL;
                    indirect_placeholder_1();
                    var_51 = (uint64_t)*var_21;
                    var_52 = local_sp_4 + (-40L);
                    *(uint64_t *)var_52 = 4206220UL;
                    var_53 = indirect_placeholder_22(0UL, var_47, 4277732UL, 1UL, var_51, r9_3, var_13);
                    rcx_0 = var_53.field_0;
                    local_sp_0 = var_52;
                    r9_0 = var_53.field_1;
                    r8_0 = var_53.field_2;
                }
                local_sp_1 = local_sp_0;
                r9_1 = r9_0;
                r8_1 = r8_0;
                if (*var_20 == '\x01') {
                    var_54 = local_sp_0 + (-8L);
                    *(uint64_t *)var_54 = 4206256UL;
                    var_55 = indirect_placeholder_23(0UL, rcx_0, 4277755UL, 1UL, 0UL, r9_0, r8_0);
                    local_sp_1 = var_54;
                    r9_1 = var_55.field_1;
                    r8_1 = var_55.field_2;
                }
                *var_17 = (*var_17 + 1U);
                local_sp_4 = local_sp_1;
                r9_3 = r9_1;
                r8_3 = r8_1;
                var_44 = *var_7;
                *(uint64_t *)(local_sp_4 + (-8L)) = 4206275UL;
                indirect_placeholder_1();
                storemerge1 = ((uint64_t)(uint32_t)var_44 == 0UL) ? 1UL : 2UL;
                var_45 = *var_17;
                r9_0 = r9_3;
                r9_4 = r9_3;
                r8_4 = r8_3;
            }
        var_56 = *var_7;
        var_57 = local_sp_4 + (-16L);
        *(uint64_t *)var_57 = 4206318UL;
        indirect_placeholder_1();
        local_sp_7 = var_57;
    } while ((uint64_t)(uint32_t)var_56 != 0UL);
    return;
}
